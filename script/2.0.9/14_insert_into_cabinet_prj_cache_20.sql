﻿/*
	insert into cabinet.prj_cache 20
*/

insert into cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
values (20, 'cachedVersionTypeList', 'Типы версий', 'version_type_id', 'version_type_name');

update cabinet.prj_claim_field
set field_name = 'version_type_id', field_name_rus = 'Версия исправления', cache_id = 20
where field_id = 16;

-- select * from cabinet.prj_cache order by cache_id
-- select * from cabinet.prj_claim_field order by field_id