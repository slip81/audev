﻿/*
	CREATE TABLE cabinet.prj_cache
*/

-- DROP TABLE cabinet.prj_cache;

CREATE TABLE cabinet.prj_cache
(
  cache_id integer NOT NULL,
  cache_name character varying,
  cache_name_rus character varying,  
  value_field character varying,
  text_field character varying,
  CONSTRAINT prj_cache_pkey PRIMARY KEY (cache_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_cache OWNER TO pavlov;


INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (1, 'cachedPrjList', 'Группы', 'prj_id', 'prj_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (2, 'cachedCabUserList', 'Пользователи', 'user_id', 'user_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (3, 'cachedProjectList', 'Проекты', 'project_id', 'project_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (4, 'cachedClientList', 'Клиенты', 'client_id', 'client_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (5, 'cachedPriorityList', 'Приоритеты', 'priority_id', 'priority_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (6, 'cachedModuleList', 'Модули', 'module_id', 'module_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (7, 'cachedModulePartList', 'Разделы модулей', 'module_part_id', 'module_part_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (8, 'cachedModuleVersionList', 'Версии модулей', 'module_version_id', 'module_version_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (9, 'cachedWorkTypeList', 'Типы работ', 'work_type_id', 'work_type_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (10, 'cachedWorkStateTypeActiveList', 'Активные статусы работ', 'state_type_id', 'state_type_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (11, 'cachedWorkStateTypeList', 'Статусы работ', 'state_type_id', 'state_type_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (12, 'cachedNotifyTypeList', 'Типы уведомлений', 'notify_type_id', 'notify_type_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (13, 'cachedUserSettingsVMList', 'Пользовательские конфигурации', 'setting_id', 'setting_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (14, 'cachedClaimTypeList', 'Типы задач', 'claim_type_id', 'claim_type_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (15, 'cachedClaimStageList', 'Этапы задач', 'claim_stage_id', 'claim_stage_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (16, 'cachedTaskUserStateTypeList', 'Статусы заданий', 'state_type_id', 'state_type_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (17, 'cachedVersionMainList', 'Версии', 'version_main_id', 'version_main_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (18, 'cachedProjectVersionList', 'Версии проектов', 'version_id', 'version_name');

INSERT INTO cabinet.prj_cache (cache_id, cache_name, cache_name_rus, value_field, text_field)
VALUES (19, 'cachedClaimStateTypeList', 'Статусы задач', 'state_type_id', 'state_type_name');
