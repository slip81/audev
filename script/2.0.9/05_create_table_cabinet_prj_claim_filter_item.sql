﻿/*
	CREATE TABLE cabinet.prj_claim_filter_item
*/

-- DROP TABLE cabinet.prj_claim_filter_item;

CREATE TABLE cabinet.prj_claim_filter_item
(
  item_id serial NOT NULL,
  filter_id integer NOT NULL,
  field_id integer NOT NULL,
  op_type_id integer NOT NULL,
  field_id_value character varying,
  field_name_value character varying,  
  is_value_fixed boolean NOT NULL DEFAULT true,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),
  CONSTRAINT prj_claim_filter_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_claim_filter_item_filter_id_fkey FOREIGN KEY (filter_id)
      REFERENCES cabinet.prj_claim_filter (filter_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_filter_item_field_id_fkey FOREIGN KEY (field_id)
      REFERENCES cabinet.prj_claim_field (field_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT prj_claim_filter_item_op_type_id_fkey FOREIGN KEY (op_type_id)
      REFERENCES cabinet.prj_claim_filter_op_type (op_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_filter_item OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_claim_filter_item_set_stamp_trg ON cabinet.prj_claim_filter_item;

CREATE TRIGGER cabinet_prj_claim_filter_item_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_claim_filter_item
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

