﻿/*
	CREATE TABLE cabinet.prj_claim_filter
*/

-- DROP TABLE cabinet.prj_claim_filter;

CREATE TABLE cabinet.prj_claim_filter
(
  filter_id serial NOT NULL,  
  filter_name character varying,
  user_id integer,
  is_active boolean NOT NULL DEFAULT true,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),  
  CONSTRAINT prj_claim_filter_pkey PRIMARY KEY (filter_id),
  CONSTRAINT prj_claim_filter_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_filter OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_claim_filter_set_stamp_trg ON cabinet.prj_claim_filter;

CREATE TRIGGER cabinet_prj_claim_filter_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_claim_filter
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

