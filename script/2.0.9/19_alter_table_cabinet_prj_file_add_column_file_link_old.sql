﻿/*
	ALTER TABLE cabinet.prj_file ADD COLUMN file_link_old
*/

ALTER TABLE cabinet.prj_file ADD COLUMN file_link_old character varying;

UPDATE cabinet.prj_file SET file_link_old = file_link;

UPDATE cabinet.prj_file SET file_link = 'https://cab.aptekaural.ru/PrjFileGet?file_id=' || file_id::text;

-- select * from cabinet.prj_file order by file_id desc limit 100