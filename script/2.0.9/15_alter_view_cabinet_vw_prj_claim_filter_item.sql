﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim_filter_item
*/

DROP VIEW cabinet.vw_prj_claim_filter_item;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_filter_item AS 
 SELECT t1.field_id,
    t1.field_name,
    t1.field_name_rus,
    t1.field_type_id,
    t1.ord,
    t1.cache_id,
    t2.field_type_name,
    t2.field_type_name_rus,
    false::boolean as is_field_in_filter,
    null::int as filter_id,
    null::int as op_type_id,
    null::character varying as field_id_value,
    null::character varying as field_name_value,
    false::boolean as is_value_fixed,
    null::character varying as filter_name,
    null::int as user_id,
    false::boolean as is_active,
    null::character varying as user_name,
    null::character varying as op_type_name,
    null::character varying as op_type_name_rus,
    null::character varying as op_type_symbol,
    t15.cache_name,
    t15.value_field AS cache_value_field,
    t15.text_field AS cache_text_field
   FROM cabinet.prj_claim_field t1
     JOIN cabinet.prj_claim_field_type t2 ON t1.field_type_id = t2.field_type_id
     LEFT JOIN cabinet.prj_cache t15 ON t1.cache_id = t15.cache_id

     UNION

 SELECT t11.field_id,
    t11.field_name,
    t11.field_name_rus,
    t11.field_type_id,
    t11.ord,
    t11.cache_id,
    t12.field_type_name,
    t12.field_type_name_rus,
        CASE
            WHEN t13.field_id IS NULL THEN false
            ELSE true
        END AS is_field_in_filter,
    t1.filter_id,
    t13.op_type_id,
    t13.field_id_value,
    t13.field_name_value,
    t13.is_value_fixed,
    t1.filter_name,
    t1.user_id,
    t1.is_active,
    t14.user_name,
    t15.op_type_name,
    t15.op_type_name_rus,
    t15.op_type_symbol,
    t16.cache_name,
    t16.value_field AS cache_value_field,
    t16.text_field AS cache_text_field
   FROM cabinet.prj_claim_filter t1
     LEFT JOIN cabinet.prj_claim_field t11 ON 1=1
     LEFT JOIN cabinet.prj_claim_field_type t12 ON t11.field_type_id = t12.field_type_id
     LEFT JOIN cabinet.prj_claim_filter_item t13 ON t11.field_id = t13.field_id AND t1.filter_id = t13.filter_id
     LEFT JOIN cabinet.cab_user t14 ON t1.user_id = t14.user_id
     LEFT JOIN cabinet.prj_claim_filter_op_type t15 ON t13.op_type_id = t15.op_type_id
     LEFT JOIN cabinet.prj_cache t16 ON t11.cache_id = t16.cache_id;     

ALTER TABLE cabinet.vw_prj_claim_filter_item OWNER TO pavlov;
