﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim_filter_item
*/

DROP VIEW cabinet.vw_prj_claim_filter_item;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_filter_item AS 
 SELECT t1.field_id,
    t1.field_name,
    t1.field_name_rus,
    t1.field_type_id,
    t1.ord,
    t1.cache_id,
    t2.field_type_name,
    t2.field_type_name_rus,
        CASE
            WHEN t11.field_id IS NULL THEN false
            ELSE true
        END AS is_field_in_filter,
    t11.filter_id,
    t11.op_type_id,
    t11.field_id_value,
    t11.field_name_value,
    t11.is_value_fixed,
    t12.filter_name,
    t12.user_id,
    t12.is_active,
    t13.user_name,
    t14.op_type_name,
    t14.op_type_name_rus,
    t14.op_type_symbol,
    t15.cache_name,
    t15.value_field AS cache_value_field,
    t15.text_field AS cache_text_field
   FROM cabinet.prj_claim_field t1
     JOIN cabinet.prj_claim_field_type t2 ON t1.field_type_id = t2.field_type_id
     LEFT JOIN cabinet.prj_claim_filter_item t11 ON t1.field_id = t11.field_id
     LEFT JOIN cabinet.prj_claim_filter t12 ON t11.filter_id = t12.filter_id
     LEFT JOIN cabinet.cab_user t13 ON t12.user_id = t13.user_id
     LEFT JOIN cabinet.prj_claim_filter_op_type t14 ON t11.op_type_id = t14.op_type_id
     LEFT JOIN cabinet.prj_cache t15 ON t1.cache_id = t15.cache_id;

ALTER TABLE cabinet.vw_prj_claim_filter_item OWNER TO pavlov;
