﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim_filter_item
*/

-- DROP VIEW cabinet.vw_prj_claim_filter_item;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_filter_item AS 
 SELECT 
  t1.user_id,
  t1.user_name,
  t2.filter_id,
  t2.filter_name,
  t2.user_id as filter_user_id,
  t2.is_active,
  t11.field_id,
  t11.field_name,
  t11.field_name_rus,
  t11.field_type_id,
  t12.field_type_name,
  t12.field_type_name_rus,  
  case when t13.field_id is null then false else true end as is_field_in_filter,
  t13.op_type_id,
  t13.field_id_value,
  t13.field_name_value,
  t13.is_value_fixed,
  t14.op_type_name,
  t14.op_type_name_rus,
  t14.op_type_symbol
   FROM cabinet.cab_user t1
   INNER JOIN cabinet.prj_claim_filter t2 ON t1.user_id = coalesce(t2.user_id, t1.user_id)
   LEFT JOIN cabinet.prj_claim_field t11 ON 1=1
   LEFT JOIN cabinet.prj_claim_field_type t12 ON t11.field_type_id = t12.field_type_id
   LEFT JOIN cabinet.prj_claim_filter_item t13 ON t2.filter_id = t13.filter_id AND t11.field_id = t13.field_id
   LEFT JOIN cabinet.prj_claim_filter_op_type t14 ON t13.op_type_id = t14.op_type_id
   WHERE t1.is_active = true AND t1.is_crm_user = true
   ;


ALTER TABLE cabinet.vw_prj_claim_filter_item OWNER TO pavlov;

-- select * from cabinet.cab_user order by user_id