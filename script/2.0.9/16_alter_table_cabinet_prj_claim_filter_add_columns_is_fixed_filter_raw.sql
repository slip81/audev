﻿/*
	ALTER TABLE cabinet.prj_claim_filter ADD COLUMN is_fixed, filter_raw
*/

ALTER TABLE cabinet.prj_claim_filter ADD COLUMN is_fixed boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.prj_claim_filter ADD COLUMN filter_raw character varying;;
