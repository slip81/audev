﻿/*
	insert into cabinet.prj_claim_filter_op_type 9, 10
*/

insert into cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
values (9, 'contains', 'содержит', '~');

insert into cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
values (10, 'doesnotcontain', 'не содержит', '!~');

-- select * from cabinet.prj_claim_filter_op_type order by op_type_id;