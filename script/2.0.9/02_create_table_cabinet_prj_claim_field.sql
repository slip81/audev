﻿/*
	CREATE TABLE cabinet.prj_claim_field
*/

-- DROP TABLE cabinet.prj_claim_field;

CREATE TABLE cabinet.prj_claim_field
(
  field_id integer NOT NULL,  
  field_name character varying,
  field_name_rus character varying,
  field_type_id integer NOT NULL,
  ord integer NOT NULL,
  CONSTRAINT prj_claim_field_pkey PRIMARY KEY (field_id),
  CONSTRAINT prj_claim_field_field_type_id_fkey FOREIGN KEY (field_type_id)
      REFERENCES cabinet.prj_claim_field_type (field_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_field OWNER TO pavlov;

-- select * from cabinet.prj_claim_field_type order by field_type_id;
-- select * from cabinet.vw_prj_claim order by claim_id desc limit 100;

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (1, 'claim_num', '№ задачи', 2, 1);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (2, 'project_id', 'Программа', 1, 2);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (3, 'client_id', 'Клиент', 1, 3);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (4, 'priority_id', 'Приоритет', 1, 4);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (5, 'resp_user_id', 'Ответственный', 1, 5);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (6, 'crt_date', 'Дата создания', 4, 6);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (7, 'upd_date', 'Дата изменения', 4, 7);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (8, 'claim_state_type_id', 'Статус', 1, 8);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (9, 'active_work_type_id', 'Стадия', 1, 9);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (10, 'exec_user_name_list', 'Исполнители', 2, 10);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (11, 'claim_type_id', 'Тип', 1, 11);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (12, 'claim_stage_id', 'Этап', 1, 12);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (13, 'summary_rate', 'Рейтинг', 6, 13);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (14, 'is_active_state', 'Активна', 5, 14);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (15, 'prj_id_list', 'Группы', 2, 15);

INSERT INTO cabinet.prj_claim_field (field_id, field_name, field_name_rus, field_type_id, ord)
VALUES (16, 'version_id', 'Версия исправления', 1, 16);

-- select * from cabinet.prj_claim_field order by field_id
