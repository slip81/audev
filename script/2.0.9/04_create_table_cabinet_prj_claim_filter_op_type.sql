﻿/*
	CREATE TABLE cabinet.prj_claim_filter_op_type
*/

-- DROP TABLE cabinet.prj_claim_filter_op_type;

CREATE TABLE cabinet.prj_claim_filter_op_type
(
  op_type_id integer NOT NULL,
  op_type_name character varying,
  op_type_name_rus character varying,
  op_type_symbol character varying,
  CONSTRAINT prj_claim_filter_op_type_pkey PRIMARY KEY (op_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_filter_op_type OWNER TO pavlov;


INSERT INTO cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
VALUES (1, 'eq', 'равно', '=');

INSERT INTO cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
VALUES (2, 'neq', 'не равно', '!=');

INSERT INTO cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
VALUES (3, 'gt', 'больше', '>');

INSERT INTO cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
VALUES (4, 'gte', 'больше равно', '>=');

INSERT INTO cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
VALUES (5, 'lt', 'меньше', '<');

INSERT INTO cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
VALUES (6, 'lte', 'меньше равно', '<=');

INSERT INTO cabinet.prj_claim_filter_op_type (op_type_id, op_type_name, op_type_name_rus, op_type_symbol)
VALUES (7, 'in', 'диапазон', '[]');

-- select * from cabinet.prj_claim_filter_op_type order by op_type_id
