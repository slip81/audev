﻿/*
	ALTER TABLE cabinet.prj_claim_field ADD COLUMN cache_id
*/

ALTER TABLE cabinet.prj_claim_field ADD COLUMN cache_id integer;

ALTER TABLE cabinet.prj_claim_field
  ADD CONSTRAINT prj_claim_field_cache_id_fkey FOREIGN KEY (cache_id)
      REFERENCES cabinet.prj_cache (cache_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE cabinet.prj_claim_field
SET field_name_rus = 'Проект'
WHERE field_id = 2;      

UPDATE cabinet.prj_claim_field
SET cache_id = 3
WHERE field_id = 2;
-- "Проект"

UPDATE cabinet.prj_claim_field
SET cache_id = 4
WHERE field_id = 3;
-- "Клиент"

UPDATE cabinet.prj_claim_field
SET cache_id = 5
WHERE field_id = 4;
-- "Приоритет"

UPDATE cabinet.prj_claim_field
SET cache_id = 2
WHERE field_id = 5;
-- "Ответственный"

UPDATE cabinet.prj_claim_field
SET cache_id = 19
WHERE field_id = 8;
-- "Статус"

UPDATE cabinet.prj_claim_field
SET cache_id = 9
WHERE field_id = 9;
-- "Стадия"

UPDATE cabinet.prj_claim_field
SET cache_id = 14
WHERE field_id = 11;
-- "Тип"

UPDATE cabinet.prj_claim_field
SET cache_id = 15
WHERE field_id = 12;
-- "Этап"

UPDATE cabinet.prj_claim_field
SET cache_id = 17
WHERE field_id = 16;
-- "Версия исправления"

-- select * from cabinet.prj_cache order by cache_id

-- select * from cabinet.prj_claim_field order by field_id
