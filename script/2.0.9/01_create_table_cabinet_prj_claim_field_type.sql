﻿/*
	CREATE TABLE cabinet.prj_claim_field_type
*/

-- DROP TABLE cabinet.prj_claim_field_type;

CREATE TABLE cabinet.prj_claim_field_type
(
  field_type_id integer NOT NULL,  
  field_type_name character varying,
  field_type_name_rus character varying,
  CONSTRAINT prj_claim_field_type_pkey PRIMARY KEY (field_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_field_type OWNER TO pavlov;

INSERT INTO cabinet.prj_claim_field_type (field_type_id, field_type_name, field_type_name_rus)
VALUES (1, 'ref', 'ссылка');

INSERT INTO cabinet.prj_claim_field_type (field_type_id, field_type_name, field_type_name_rus)
VALUES (2, 'string', 'строка');

INSERT INTO cabinet.prj_claim_field_type (field_type_id, field_type_name, field_type_name_rus)
VALUES (3, 'integer', 'целое');

INSERT INTO cabinet.prj_claim_field_type (field_type_id, field_type_name, field_type_name_rus)
VALUES (4, 'date', 'дата');

INSERT INTO cabinet.prj_claim_field_type (field_type_id, field_type_name, field_type_name_rus)
VALUES (5, 'boolean', 'да-нет');

INSERT INTO cabinet.prj_claim_field_type (field_type_id, field_type_name, field_type_name_rus)
VALUES (6, 'numeric', 'дробное');