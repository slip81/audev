﻿/*
	CREATE TABLE esn.datasource_exchange_item
*/

CREATE TABLE esn.datasource_exchange_item
(
  rel_id bigserial NOT NULL,
  ds_id bigint NOT NULL,
  item_id bigint NOT NULL,
  for_send boolean NOT NULL DEFAULT true,
  for_get boolean NOT NULL DEFAULT true,
  CONSTRAINT datasource_exchange_item_pkey PRIMARY KEY (rel_id),
  CONSTRAINT datasource_exchange_item_ds_id_fkey FOREIGN KEY (ds_id)
      REFERENCES esn.datasource (ds_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT datasource_exchange_item_item_id_fkey FOREIGN KEY (item_id)
      REFERENCES esn.exchange_item (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.datasource_exchange_item OWNER TO pavlov;
