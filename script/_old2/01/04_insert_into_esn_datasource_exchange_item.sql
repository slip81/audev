﻿/*
	insert into esn.datasource_exchange_item
*/

/*
insert into esn.datasource (ds_id, ds_name)
values (102, 'АСНА');
*/

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 1, true, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 2, true, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 3, true, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 4, false, true);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 5, false, true);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 6, false, true);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 7, false, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 8, false, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 1, true, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 2, true, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 3, false, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 4, false, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 5, false, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 6, false, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 7, true, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 8, true, false);


-- select * from esn.datasource_exchange_item
