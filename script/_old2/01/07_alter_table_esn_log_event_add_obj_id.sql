﻿/*
	ALTER TABLE esn.log_event ADD COLUMN obj_id bigint;
*/

ALTER TABLE esn.log_event ADD COLUMN obj_id bigint;