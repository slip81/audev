﻿/*
	CREATE TABLE esn.exchange_item
*/

CREATE TABLE esn.exchange_item
(
  item_id bigserial NOT NULL,
  item_name character varying,
  item_shortname character varying,
  CONSTRAINT exchange_item_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_item OWNER TO pavlov;
