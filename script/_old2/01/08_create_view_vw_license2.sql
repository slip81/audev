﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_license2
*/

-- DROP VIEW cabinet.vw_license2;

CREATE OR REPLACE VIEW cabinet.vw_license2 AS 
select row_number() over(order by t1.id) as id, t3.client_id, t1.id as license_id, t1.dt_start, t1.dt_end, t1.service_registration_id
, t4.activation_key, t5.id as workplace_id, t5.registration_key, t5.sales_id
, t2.id as order_item_id, t2.service_id, t2.version_id, t2.quantity
, t3.id as order_id, t3.is_conformed, t3.active_status
from cabinet.license t1
inner join cabinet.order_item t2 on t1.order_item_id = t2.id
inner join cabinet."order" t3 on t2.order_id = t3.id
--and t3.client_id = 80
left join cabinet.service_registration t4 on t1.service_registration_id = t4.id
left join cabinet.workplace t5 on t4.workplace_id = t5.id
where t1.is_deleted != 1
;

ALTER TABLE cabinet.vw_license2  OWNER TO pavlov;
