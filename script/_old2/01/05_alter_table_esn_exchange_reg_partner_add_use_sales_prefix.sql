﻿/*
	ALTER TABLE esn.exchange_reg_partner ADD COLUMN use_sales_prefix integer NOT NULL DEFAULT 0;
*/

ALTER TABLE esn.exchange_reg_partner ADD COLUMN use_sales_prefix integer NOT NULL DEFAULT 0;
