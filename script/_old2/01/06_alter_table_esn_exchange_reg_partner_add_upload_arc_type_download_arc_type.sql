﻿/*
	ALTER TABLE esn.exchange_reg_partner ADD COLUMNS
*/

ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_arc_type integer NOT NULL DEFAULT 0;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_arc_type integer NOT NULL DEFAULT 0;

