﻿/*
	insert into esn.exchange_item
*/

insert into esn.exchange_item (item_name, item_shortname)
values ('Остатки', 'ost');

insert into esn.exchange_item (item_name, item_shortname)
values ('Движения', 'move');

insert into esn.exchange_item (item_name, item_shortname)
values ('Заказ', 'que');

insert into esn.exchange_item (item_name, item_shortname)
values ('Товары ЦП', 'plan');

insert into esn.exchange_item (item_name, item_shortname)
values ('Товары по акциям', 'discount');

insert into esn.exchange_item (item_name, item_shortname)
values ('Товары ПР', 'recommend');

insert into esn.exchange_item (item_name, item_shortname)
values ('Справочник КА', 'client');

insert into esn.exchange_item (item_name, item_shortname)
values ('Справочник номенклатуры', 'prep');