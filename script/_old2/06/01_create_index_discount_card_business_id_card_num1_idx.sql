﻿/*
	CREATE INDEX discount_card_business_id_card_num1_idx
*/

CREATE INDEX discount_card_business_id_card_num1_idx
  ON discount.card
  USING btree
  (business_id, card_num);
