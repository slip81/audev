﻿/*
	CREATE INDEX discount_business_org_business_id1_idx
	CREATE INDEX discount_business_org_user_org_id_user_name1_idx
*/

CREATE INDEX discount_business_org_business_id1_idx
  ON discount.business_org
  USING btree
  (business_id);

CREATE INDEX discount_business_org_user_org_id_user_name1_idx
  ON discount.business_org_user
  USING btree
  (org_id, user_name);