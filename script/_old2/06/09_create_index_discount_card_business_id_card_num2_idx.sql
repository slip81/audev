﻿/*
	CREATE INDEX discount_card_business_id_card_num2_idx
*/

CREATE INDEX discount_card_business_id_card_num2_idx
  ON discount.card
  USING btree
  (business_id, card_num2);
