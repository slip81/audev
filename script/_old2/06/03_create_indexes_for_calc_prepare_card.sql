﻿/*
	CREATE INDEXes for calc_prepare_card
*/

CREATE INDEX discount_card_curr_card_status_id1_idx
  ON discount.card
  USING btree
  (curr_card_status_id);

CREATE INDEX discount_card_curr_card_type_id1_idx
  ON discount.card
  USING btree
  (curr_card_type_id);

CREATE INDEX discount_card_type_programm_rel_card_type_id1_idx
  ON discount.card_type_programm_rel
  USING btree
  (card_type_id, date_end);

 