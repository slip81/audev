﻿/*
	CREATE OR REPLACE VIEW discount.vw_business_org_user
*/

-- DROP VIEW discount.vw_business_org_user;

CREATE OR REPLACE VIEW discount.vw_business_org_user AS 
select
  t2.org_user_id,
  t2.user_name, 
  t2.user_descr,
  t1.org_id,
  t1.org_name,
  t1.business_id,
  t1.add_to_timezone,
  t1.card_scan_only,
  t1.scanner_equals_reader,
  t1.allow_card_add
from discount.business_org t1
inner join discount.business_org_user t2 on t1.org_id = t2.org_id
;

ALTER TABLE discount.vw_business_org_user OWNER TO pavlov;