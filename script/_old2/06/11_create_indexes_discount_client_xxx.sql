﻿/*
	CREATE INDEX discount_client_card_rel_card_id_client_id_date_end1_idx
	CREATE INDEX discount_client_business_id_client_surname1_idx
*/

CREATE INDEX discount_client_card_rel_card_id_client_id_date_end1_idx
  ON discount.client_card_rel
  USING btree
  (card_id, client_id, date_end);

CREATE INDEX discount_client_business_id_client_surname1_idx
  ON discount.client
  USING btree
  (business_id, client_surname);  
