﻿
CREATE INDEX discount_card_additional_num_card_id_card_num1_idx
  ON discount.card_additional_num
  USING btree
  (card_id, card_num);
