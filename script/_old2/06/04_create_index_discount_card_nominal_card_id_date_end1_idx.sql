﻿/*
	CREATE INDEX discount_card_nominal_card_id_date_end1_idx
*/

CREATE INDEX discount_card_nominal_card_id_date_end1_idx
  ON discount.card_nominal
  USING btree
  (card_id, date_end);
