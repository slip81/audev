﻿/*
	ALTER TABLE log_event ADD COLUMNs
*/

ALTER TABLE discount.log_event ADD COLUMN date_end timestamp without time zone;
ALTER TABLE discount.log_event ADD COLUMN mess2 character varying;

ALTER TABLE esn.log_event ADD COLUMN date_end timestamp without time zone;
ALTER TABLE esn.log_event ADD COLUMN mess2 character varying;