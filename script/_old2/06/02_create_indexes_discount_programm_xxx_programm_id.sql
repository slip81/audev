﻿/*
	CREATE INDEX discount_programm_xxx_programm_id
*/

CREATE INDEX discount_programm_param_programm_id1_idx
  ON discount.programm_param
  USING btree
  (programm_id);

CREATE INDEX discount_programm_step_programm_id1_idx
  ON discount.programm_step
  USING btree
  (programm_id);  

CREATE INDEX discount_programm_step_param_programm_id1_idx
  ON discount.programm_step_param
  USING btree
  (programm_id);   

CREATE INDEX discount_programm_step_condition_programm_id1_idx
  ON discount.programm_step_condition
  USING btree
  (programm_id);   

CREATE INDEX discount_programm_step_logic_programm_id1_idx
  ON discount.programm_step_logic
  USING btree
  (programm_id);   