﻿/*
	CREATE INDEX discount_card_type_unit_card_type_id1_idx
*/

-- DROP INDEX discount.discount_card_type_unit_card_type_id1_idx;

CREATE INDEX discount_card_type_unit_card_type_id1_idx
  ON discount.card_type_unit
  USING btree
  (card_type_id);
