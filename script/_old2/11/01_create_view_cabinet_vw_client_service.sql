﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service
*/

-- DROP VIEW cabinet.vw_client_service;

CREATE OR REPLACE VIEW cabinet.vw_client_service AS 
	select row_number() over (order by t1.id) as id, t1.id as client_id, t1.name as client_name, t2.id as sales_id, t2.name as sales_name, t2.adress as sales_address
	, t3.id as workplace_id, t3.registration_key as workplace_registration_key, t3.description as workplace_description
	, t8.id as service_id, t8.name as service_name, t8.description as service_description
	, t9.id as version_id, t9.name as version_name
	from cabinet.client t1
	inner join cabinet.sales t2 on t1.id = t2.client_id
	inner join cabinet.workplace t3 on t2.id = t3.sales_id
	inner join cabinet.service_registration t4 on t3.id = t4.workplace_id
	inner join cabinet.license t5 on t4.license_id = t5.id
	inner join cabinet.order_item t6 on t5.order_item_id = t6.id
	inner join cabinet.order t7 on t6.order_id = t7.id
	inner join cabinet.service t8 on t6.service_id = t8.id
	left join cabinet.version t9 on t6.version_id = t9.id
	;

ALTER TABLE cabinet.vw_client_service OWNER TO pavlov;
