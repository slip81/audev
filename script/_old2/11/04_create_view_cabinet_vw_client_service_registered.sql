﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service_registered
*/

-- DROP VIEW cabinet.vw_client_service_registered;

CREATE OR REPLACE VIEW cabinet.vw_client_service_registered AS 
	SELECT row_number() OVER (ORDER BY t1.id) AS id
	, t3.client_id, t3.id as sales_id, t2.id as workplace_id, t2.description as workplace_description
	, t1.activation_key, t1.license_id, t6.id as service_id, t10.id as version_id, t6.name as service_name, t10.name as version_name
	from cabinet.service_registration t1
	inner join cabinet.workplace t2 on t1.workplace_id = t2.id
	inner join cabinet.sales t3 on t2.sales_id = t3.id
	inner join cabinet.license t4 on t1.id = t4.service_registration_id
	inner join cabinet.order_item t5 on t4.order_item_id = t5.id
	inner join cabinet.service t6 on t5.service_id = t6.id
	left join cabinet.version t10 on t5.version_id = t10.id;

ALTER TABLE cabinet.vw_client_service_registered OWNER TO pavlov;
