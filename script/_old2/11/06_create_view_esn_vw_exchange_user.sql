﻿/*
	CREATE OR REPLACE VIEW esn.vw_exchange_user
*/

-- DROP VIEW esn.vw_exchange_user;

CREATE OR REPLACE VIEW esn.vw_exchange_user AS
	select t1.user_id, t1.login, t1.client_id as exchange_client_id
	, t10.client_id, t10.client_name, t10.sales_id, coalesce(t10.sales_address, t10.sales_name) as sales_name
	, t10.workplace_id, t10.description as workplace_description, t10.registration_key as workplace
	from esn.exchange_user t1
	left join cabinet.vw_workplace t10 on t1.cabinet_workplace_id = t10.workplace_id
	;

ALTER TABLE esn.vw_exchange_user OWNER TO pavlov;
