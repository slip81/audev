﻿/*
	ALTER TABLE cabinet.service ADD COLUMN is_active
*/

ALTER TABLE cabinet.service ADD COLUMN is_active boolean NOT NULL DEFAULT true;

UPDATE cabinet.service
SET is_active = false
WHERE id in (26, 32, 33);

UPDATE cabinet.service SET name = 'Закачка брака' WHERE id = 34;

UPDATE cabinet.service SET description = 'Модуль управления клиентами' WHERE id = 42;

-- select * from cabinet.service order by id