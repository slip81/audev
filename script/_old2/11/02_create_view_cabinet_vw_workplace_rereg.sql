﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_workplace_rereg AS 
*/

-- DROP VIEW cabinet.vw_workplace_rereg;

CREATE OR REPLACE VIEW cabinet.vw_workplace_rereg AS 
 SELECT 
    t4.id as rereg_id,
    t4.person,
    t4.reason,
    t4.manager_activation,
    t4.created_on,
    t4.created_by,
    t4.updated_on,
    t4.updated_by,
    t4.is_deleted,
    t4.is_used,
    t1.id AS workplace_id,
    t1.registration_key,
    t1.description,
    t1.sales_id,
    t2.name AS sales_name,
    t2.adress AS sales_address,
    t2.client_id,
    t3.name as client_name
   FROM cabinet.workplace t1
     JOIN cabinet.sales t2 ON t1.sales_id = t2.id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
     JOIN cabinet.reregistration_order t4 ON t1.id = t4.workplace_id
     ;
     

ALTER TABLE cabinet.vw_workplace_rereg OWNER TO pavlov;
