﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service_unregistered
*/

-- DROP VIEW cabinet.vw_client_service_unregistered;

CREATE OR REPLACE VIEW cabinet.vw_client_service_unregistered AS 
	SELECT row_number() OVER (ORDER BY t1.id) AS id
	, t1.client_id, t2.service_id, t2.version_id, t3.name as service_name, t12.name as version_name
	from cabinet.order t1
	inner join cabinet.order_item t2 on t1.id = t2.order_id
	inner join cabinet.service t3 on t2.service_id = t3.id
	inner join cabinet.license t10 on t2.id = t10.order_item_id
	left join cabinet.service_registration t11 on t10.id = t11.license_id
	left join cabinet.version t12 on t2.version_id = t12.id
	where t11.id is null;	

ALTER TABLE cabinet.vw_client_service_unregistered OWNER TO pavlov;
