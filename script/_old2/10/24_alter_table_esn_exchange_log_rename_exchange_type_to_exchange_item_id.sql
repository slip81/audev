﻿/*
	ALTER TABLE esn.exchange_log RENAME COLUMN exchange_type TO exchange_item_id
*/

ALTER TABLE esn.exchange_log RENAME COLUMN exchange_type TO exchange_item_id;


ALTER TABLE esn.exchange_log
  ADD CONSTRAINT exchange_log_exchange_item_id_fkey FOREIGN KEY (exchange_item_id)
      REFERENCES esn.exchange_item (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
