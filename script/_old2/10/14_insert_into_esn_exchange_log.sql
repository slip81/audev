﻿/*
	insert into esn.exchange_log
*/

insert into esn.exchange_log (date_beg, date_end, user_id, login, exchange_direction
, exchange_type, ds_id, exchange_id, error_level, mess, mess2)
select date_beg, date_beg, null, lower(user_name), case when coalesce(t2.is_upload, false) = false then 1 else 0 end
, case when log_event_type_id < 100 then log_event_type_id else coalesce(t2.exchange_type, 1) end, coalesce(t2.ds_id, 101), t2.exchange_id, case when log_event_type_id < 100 then 0 else 2 end, mess, ''
from esn.log_event t1
left join esn.exchange t2 on t1.obj_id = t2.exchange_id
where t1.scope = 4
;


-- select * from esn.log_event where scope = 4 order by log_id desc
-- select * from esn.exchange_log order by log_id desc