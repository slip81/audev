﻿/*
	CREATE TABLE esn.exchange_log	
*/

-- DROP TABLE esn.exchange_log;

CREATE TABLE esn.exchange_log
(
  log_id bigserial NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  user_id bigint,
  login character varying,
  exchange_direction integer NOT NULL DEFAULT 0,
  exchange_type integer NOT NULL DEFAULT 0,
  ds_id bigint,
  exchange_id bigint,
  error_level integer NOT NULL DEFAULT 0,
  mess character varying,
  mess2 character varying,
  CONSTRAINT exchange_log_pkey PRIMARY KEY (log_id),
  CONSTRAINT exchange_log_ds_id_fkey FOREIGN KEY (ds_id)
      REFERENCES esn.datasource (ds_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT exchange_log_exchange_id_fkey FOREIGN KEY (exchange_id)
      REFERENCES esn.exchange (exchange_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT exchange_log_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES esn.exchange_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_log OWNER TO pavlov;
