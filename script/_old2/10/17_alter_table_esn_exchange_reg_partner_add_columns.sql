﻿/*
	ALTER TABLE esn.exchange_reg_partner ADD COLUMNs
*/

ALTER TABLE esn.exchange_reg_partner ADD COLUMN prepared_date timestamp without time zone;

ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_last_date timestamp without time zone;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_last_date timestamp without time zone;

ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_last_mess character varying;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_last_mess character varying;

-- UPDATE esn.exchange_reg_partner SET prepared_date = '2015-06-05 11:45:42.984932';
-- UPDATE esn.exchange_reg_partner SET upload_last_date = current_timestamp - '1 days'::interval;
-- UPDATE esn.exchange_reg_partner SET download_last_date = current_timestamp - '1 days'::interval;

-- UPDATE esn.exchange_reg_partner SET upload_last_mess = 'Тестовое сообщение для отправки';
-- UPDATE esn.exchange_reg_partner SET download_last_mess = 'Тестовое сообщение для приема';

-- select * from esn.exchange_reg_partner