﻿/*
	ALTER TABLE esn.ved_reestr_region_item ADD COLUMNs
*/

ALTER TABLE esn.ved_reestr_region_item ADD COLUMN match_date timestamp without time zone;
ALTER TABLE esn.ved_reestr_region_item ADD COLUMN match_user character varying;
ALTER TABLE esn.ved_reestr_region_item ADD COLUMN is_hand_match boolean NOT NULL DEFAULT false;