﻿/*
	ALTER TABLE discount.business ADD COLUMN cabinet_client_id
*/

ALTER TABLE discount.business ADD COLUMN cabinet_client_id integer;