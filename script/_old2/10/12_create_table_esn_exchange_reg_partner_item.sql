﻿/*
	CREATE TABLE esn.exchange_reg_partner_item
*/

-- DROP TABLE esn.exchange_reg_partner_item;

CREATE TABLE esn.exchange_reg_partner_item
(
  item_id bigserial NOT NULL,
  reg_id bigint NOT NULL,
  exchange_item_id bigint NOT NULL,
  is_required boolean NOT NULL DEFAULT false,
  days_cnt_for_wait integer NOT NULL DEFAULT 1,
  CONSTRAINT exchange_reg_partner_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT exchange_reg_partner_item_exchange_item_id_fkey FOREIGN KEY (exchange_item_id)
      REFERENCES esn.exchange_item (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT exchange_reg_partner_item_reg_id_fkey FOREIGN KEY (reg_id)
      REFERENCES esn.exchange_reg (reg_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


ALTER TABLE esn.exchange_reg_partner_item OWNER TO pavlov;
