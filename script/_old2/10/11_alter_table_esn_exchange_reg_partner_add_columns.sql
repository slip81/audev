﻿/*
	ALTER TABLE esn.exchange_reg_partner ADD COLUMNs
*/

ALTER TABLE esn.exchange_reg_partner ADD COLUMN is_prepared boolean NOT NULL DEFAULT true;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_state integer NOT NULL DEFAULT 0;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_state integer NOT NULL DEFAULT 0;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_err_mess character varying;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_err_mess character varying;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_ok_date timestamp without time zone;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_ok_date timestamp without time zone;

UPDATE esn.exchange_reg_partner SET upload_ok_date = current_timestamp;
UPDATE esn.exchange_reg_partner SET download_ok_date = current_timestamp;

--UPDATE esn.exchange_reg_partner SET upload_ok_date = current_timestamp - '50 days'::interval;
--UPDATE esn.exchange_reg_partner SET download_ok_date = current_timestamp - '50 days'::interval;

--UPDATE esn.exchange_reg_partner SET upload_ok_date = '2015-06-06 11:45:42.984932';
--UPDATE esn.exchange_reg_partner SET download_ok_date = '2015-06-06 11:45:42.984932';


--select * from esn.exchange_reg_partner;
