﻿/*
	ALTER TABLE esn.exchange_user ADD COLUMNs
*/

ALTER TABLE esn.exchange_user ADD COLUMN cabinet_sales_id bigint;
ALTER TABLE esn.exchange_user ADD COLUMN cabinet_workplace_id bigint;
