﻿/*
	ALTER TABLE esn.datasource ADD COLUMN ds_shortname
*/

ALTER TABLE esn.datasource ADD COLUMN ds_shortname character varying;

UPDATE esn.datasource SET ds_shortname = ds_name;

UPDATE esn.datasource SET ds_shortname = 'МЗ' where ds_id = 101;

-- select * from esn.datasource
