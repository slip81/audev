﻿/*
	insert into esn.exchange_reg_partner_item
*/

delete from esn.datasource_exchange_item where item_id = 0;

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 0, false, false);

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 0, false, false);

delete from esn.datasource_exchange_item where item_id = 9 and ds_id = 101;

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (101, 9, false, false);

delete from esn.exchange_reg_partner_item;

insert into esn.exchange_reg_partner_item (reg_id, exchange_item_id, is_required, days_cnt_for_wait)
select t1.reg_id, t2.item_id, true, 1
from esn.exchange_reg_partner t1
inner join esn.datasource_exchange_item t2 on t1.ds_id = t2.ds_id and ((t2.for_send = true) or (t2.for_get = true));


-- select * from esn.exchange_reg_partner_item order by reg_id
