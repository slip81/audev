﻿/*
	update discount.business
*/

select is_deleted, * from cabinet.client where 1=1
and (
(name like '%ТРИТ%')
or (name like '%Биона%')
or (name like '%Медуница%')
or (name like '%Смирнова ИП%') -- !!!
or (name like '%Аурит%')
or (name like '%Никофарм%')
or (name like '%Югра-Фарм-Сервис%')
or (name like '%Левзея%')
or (name like '%Евро%')
or (name like '%Элита%')
or (name like '%Столица%')
or (name like '%дизайн"%')
or (name like '%Уралпромснаб%')
or (name like '%среда%')
or (name like '%Главврач%')
or (name like '%Синергия%')
)
order by name
;
/*
1000			"АУРИТ"
700			"ООО "А-мега  дизайн""
169			"ООО "Биона""
340			"ООО "Главврач и партнеры""
259			"ООО "Евро-фарм""
76			"ООО "Левзея""
308			"ООО "Медуница""
3450			"ООО "Синергия""
410			"ООО "ТРИТ""
80			"ООО "Уралпромснаб-СпК""
264			"ООО "Югра-Фарм-Сервис""
1325			"ООО Аптека "Никофарм""
3876			"ООО Доступная среда"
4675			"Столица"
1170			"Элита - 1"
*/

-- 410

select  * from discount.business order by business_id

/*
update discount.business 
set cabinet_client_id = 1000 
where business_id = 1007661065565111774;

update discount.business 
set cabinet_client_id = 700
where business_id = 1110069207732585509;

update discount.business 
set cabinet_client_id = 169 
where business_id = 939659093734327298;

update discount.business 
set cabinet_client_id = 340
where business_id = 1141757255301989802;

update discount.business 
set cabinet_client_id = 259 
where business_id = 1030849061529323115;

update discount.business 
set cabinet_client_id = 76 
where business_id = 1020043408959342414;

update discount.business 
set cabinet_client_id = 308 
where business_id = 958528103041205513;

update discount.business 
set cabinet_client_id = 3450 
where business_id = 1180339545837668336;

update discount.business 
set cabinet_client_id = 410 
where business_id = 957862396381103979;

update discount.business 
set cabinet_client_id = 80 
where business_id = 1113650283650483295;

update discount.business 
set cabinet_client_id = 264 
where business_id = 1015010146583053792;

update discount.business 
set cabinet_client_id = 1325 
where business_id = 1013489512715650905;

update discount.business 
set cabinet_client_id = 3876 
where business_id = 1129696061368043060;

update discount.business 
set cabinet_client_id = 4675
where business_id = 1105604012503205132;

update discount.business 
set cabinet_client_id = 1170 
where business_id = 1078787132467709775;
*/