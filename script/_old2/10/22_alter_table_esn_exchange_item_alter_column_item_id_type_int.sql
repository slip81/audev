﻿/*
	ALTER TABLE esn.exchange_item ALTER COLUMN item_id TYPE int
*/

ALTER TABLE esn.exchange_item ALTER COLUMN item_id TYPE int;

