﻿/*
	CREATE TABLE esn.ved_reestr_column_match
*/

-- DROP TABLE esn.ved_reestr_column_match;

CREATE TABLE esn.ved_reestr_column_match
(
  item_id bigserial NOT NULL,
  reestr_id bigint NOT NULL,
  region_id integer,
  column_id integer NOT NULL,
  is_out boolean NOT NULL DEFAULT false,
  CONSTRAINT ved_reestr_column_match_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_reestr_column_match OWNER TO pavlov;
