﻿/*
	ALTER TABLE esn.exchange_reg_partner ADD COLUMNs
*/

ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_ok_cnt integer NOT NULL DEFAULT 0;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_warn_cnt integer NOT NULL DEFAULT 0;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN download_err_cnt integer NOT NULL DEFAULT 0;

ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_ok_cnt integer NOT NULL DEFAULT 0;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_warn_cnt integer NOT NULL DEFAULT 0;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_err_cnt integer NOT NULL DEFAULT 0;

/*
UPDATE esn.exchange_reg_partner AS t1
SET download_ok_cnt = (SELECT count(x1.*) FROM esn.exchange_log x1 WHERE lower(x1.login) = lower(t3.login) AND x1.exchange_direction = 1 AND x1.error_level = 0)
FROM esn.exchange_user_reg t2
INNER JOIN esn.exchange_user t3 ON t2.user_id = t3.user_id
WHERE t1.reg_id = t2.reg_id;

UPDATE esn.exchange_reg_partner AS t1
SET download_warn_cnt = (SELECT count(x1.*) FROM esn.exchange_log x1 WHERE lower(x1.login) = lower(t3.login) AND x1.exchange_direction = 1 AND x1.error_level = 1)
FROM esn.exchange_user_reg t2
INNER JOIN esn.exchange_user t3 ON t2.user_id = t3.user_id
WHERE t1.reg_id = t2.reg_id;

UPDATE esn.exchange_reg_partner AS t1
SET download_err_cnt = (SELECT count(x1.*) FROM esn.exchange_log x1 WHERE lower(x1.login) = lower(t3.login) AND x1.exchange_direction = 1 AND x1.error_level = 2)
FROM esn.exchange_user_reg t2
INNER JOIN esn.exchange_user t3 ON t2.user_id = t3.user_id
WHERE t1.reg_id = t2.reg_id;


UPDATE esn.exchange_reg_partner AS t1
SET upload_ok_cnt = (SELECT count(x1.*) FROM esn.exchange_log x1 WHERE lower(x1.login) = lower(t3.login) AND x1.exchange_direction = 0 AND x1.error_level = 0)
FROM esn.exchange_user_reg t2
INNER JOIN esn.exchange_user t3 ON t2.user_id = t3.user_id
WHERE t1.reg_id = t2.reg_id;

UPDATE esn.exchange_reg_partner AS t1
SET upload_warn_cnt = (SELECT count(x1.*) FROM esn.exchange_log x1 WHERE lower(x1.login) = lower(t3.login) AND x1.exchange_direction = 0 AND x1.error_level = 1)
FROM esn.exchange_user_reg t2
INNER JOIN esn.exchange_user t3 ON t2.user_id = t3.user_id
WHERE t1.reg_id = t2.reg_id;

UPDATE esn.exchange_reg_partner AS t1
SET upload_err_cnt = (SELECT count(x1.*) FROM esn.exchange_log x1 WHERE lower(x1.login) = lower(t3.login) AND x1.exchange_direction = 0 AND x1.error_level = 2)
FROM esn.exchange_user_reg t2
INNER JOIN esn.exchange_user t3 ON t2.user_id = t3.user_id
WHERE t1.reg_id = t2.reg_id;
*/

--SELECT * FROM esn.exchange_reg_partner ORDER BY reg_id