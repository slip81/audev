﻿/*
	ALTER TABLE esn.exchange_log ADD COLUMN reg_id bigint
*/

ALTER TABLE esn.exchange_log DROP CONSTRAINT exchange_log_user_id_fkey;

UPDATE esn.exchange_log SET user_id = null;

ALTER TABLE esn.exchange_log RENAME COLUMN user_id TO reg_id;

ALTER TABLE esn.exchange_log
  ADD CONSTRAINT exchange_log_reg_id_fkey FOREIGN KEY (reg_id)
      REFERENCES esn.exchange_reg_partner (reg_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

