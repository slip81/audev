﻿/*
	ALTER TABLE esn.datasource_exchange_item ALTER COLUMN item_id TYPE int
*/

ALTER TABLE esn.datasource_exchange_item ALTER COLUMN item_id TYPE int;
