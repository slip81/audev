﻿/*
	update esn.exchange_user set cabinet_sales_id
*/


update esn.exchange_user t1
set cabinet_sales_id = t2.id
from cabinet.sales t2
inner join cabinet.my_aspnet_users t3 on t2.user_id = t3.id
where coalesce(t1.cabinet_sales_id, 0) <= 0
and lower(t3.name) = lower(t1.login);

update esn.exchange_user t1
set cabinet_workplace_id = t2.id
from cabinet.workplace t2
where coalesce(t1.cabinet_workplace_id, 0) <= 0
and coalesce(t1.cabinet_sales_id, 0) > 0
and t1.cabinet_sales_id = t2.sales_id;

-- select * from esn.exchange_user