﻿/*
	ALTER TABLE esn.exchange_reg_partner_item ALTER COLUMN exchange_item_id TYPE int
*/

ALTER TABLE esn.exchange_reg_partner_item ALTER COLUMN exchange_item_id TYPE int;
