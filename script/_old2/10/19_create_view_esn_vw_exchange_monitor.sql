﻿/*
	CREATE OR REPLACE VIEW esn.vw_exchange_monitor AS
*/

-- DROP VIEW esn.vw_exchange_monitor;

CREATE OR REPLACE VIEW esn.vw_exchange_monitor AS
select 
row_number() over (order by t1.user_id) as id, t1.user_id, t1.login, t1.workplace, t1.client_id, t2.cabinet_client_id
, t1.cabinet_sales_id, t1.cabinet_workplace_id, coalesce(t5.name, 'Клиент [' || t1.login || ']') as client_name
, coalesce(t6.adress, 'Торговая точка [' || t1.login || ']') as sales_name
, coalesce(t7.description, 'Раб. место [' || t1.login || ']') as workplace_name
, t4.reg_id, t4.ds_id, t4.is_prepared, t4.upload_state, t4.download_state, t4.upload_err_mess, t4.download_err_mess, t4.upload_ok_date, t4.download_ok_date
, t4.prepared_date, t4.upload_last_date, t4.download_last_date, t4.upload_last_mess, t4.download_last_mess
from esn.exchange_user t1
inner join esn.exchange_client t2 on t1.client_id = t2.client_id
inner join esn.exchange_user_reg t3 on t1.user_id = t3.user_id
inner join esn.exchange_reg_partner t4 on t3.reg_id = t4.reg_id
left join cabinet.client t5 on t2.cabinet_client_id = t5.id
left join cabinet.sales t6 on t1.cabinet_sales_id = t6.id
left join cabinet.workplace t7 on t1.cabinet_workplace_id = t7.id;

ALTER TABLE esn.vw_exchange_monitor OWNER TO pavlov;
