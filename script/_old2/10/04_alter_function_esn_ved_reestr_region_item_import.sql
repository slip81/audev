﻿/*
	CREATE OR REPLACE FUNCTION esn.ved_reestr_region_item_import
*/

-- DROP FUNCTION esn.ved_reestr_region_item_import(bigint);

CREATE OR REPLACE FUNCTION esn.ved_reestr_region_item_import(IN _source_id bigint)
  RETURNS TABLE(_reestr_id bigint, _new_item_cnt integer, _out_item_cnt integer, _err_item_cnt integer) AS
$BODY$
DECLARE
    tmp_item_cnt integer;    
    tmp_new_item_cnt integer;    
    inserted_item_cnt integer;
    err_item_cnt integer;
    curr_reestr_id bigint;
    parent_reestr_id bigint;
    reestr_is_closed boolean;    
    new_reestr_id bigint;
    user_name character varying;
    now timestamp without time zone;
    today date;
    arow record;
    --
    col_id_MNN CONSTANT integer := 1;
    col_id_COMM_NAME CONSTANT integer := 2;
    col_id_PACK_NAME CONSTANT integer := 3;
    col_id_PRODUCER CONSTANT integer := 4;
    col_id_PACK_COUNT CONSTANT integer := 5;
    col_id_LIMIT_PRICE CONSTANT integer := 6;
    col_id_PRICE_FOR_INIT_PACK CONSTANT integer := 7;
    col_id_REG_NUM CONSTANT integer := 8;
    col_id_PRICE_REG_DATE CONSTANT integer := 9;
    col_id_EAN13 CONSTANT integer := 10;
    col_id_OUT_REAZON CONSTANT integer := 15;
    col_id_OUT_DATE CONSTANT integer := 16;
    --
    col_match_MNN boolean := false;
    col_match_COMM_NAME boolean := false;    
    col_match_PACK_NAME boolean := false;
    col_match_PRODUCER boolean := false;
    col_match_PACK_COUNT boolean := false;
    col_match_LIMIT_PRICE boolean := false;
    col_match_PRICE_FOR_INIT_PACK boolean := false;
    col_match_REG_NUM boolean := false;
    col_match_PRICE_REG_DATE boolean := false;
    col_match_EAN13 boolean := false;
    col_match_OUT_REAZON boolean := false;
    col_match_OUT_DATE boolean := false;    
BEGIN
	set search_path to 'esn';
	
	--_result := -1;
	-- return query select -1::bigint as _reestr_id, 0::integer as _new_item_cnt, 0::integer as _out_item_cnt, 0::integer as _err_item_cnt;
	
	now = current_timestamp;
	today = current_date;
	user_name = 'downloader-ved';	

	tmp_item_cnt := (select count(*) from tmp_ved_reestr_item);	

	if (tmp_item_cnt <=0) then
		return;
	end if;

	for arow in
		select t2.column_id, coalesce(t2.is_for_match, false) as is_for_match
		from ved_source_region t1
		inner join ved_template_column t2 on t1.template_id = t2.template_id and coalesce(t2.is_for_match, false) = true
		where t1.item_id = _source_id
	loop
		case arow.column_id
			when col_id_MNN then
				col_match_MNN := arow.is_for_match;
			when col_id_COMM_NAME then
				col_match_COMM_NAME := arow.is_for_match;
			when col_id_PACK_NAME then
				col_match_PACK_NAME := arow.is_for_match;
			when col_id_PRODUCER then
				col_match_PRODUCER := arow.is_for_match;
			when col_id_PACK_COUNT then
				col_match_PACK_COUNT := arow.is_for_match;
			when col_id_LIMIT_PRICE then
				col_match_LIMIT_PRICE := arow.is_for_match;
			when col_id_PRICE_FOR_INIT_PACK then
				col_match_PRICE_FOR_INIT_PACK := arow.is_for_match;
			when col_id_REG_NUM then
				col_match_REG_NUM := arow.is_for_match;
			when col_id_PRICE_REG_DATE then
				col_match_PRICE_REG_DATE := arow.is_for_match;
			when col_id_EAN13 then
				col_match_EAN13 := arow.is_for_match;
			when col_id_OUT_REAZON then
				col_match_OUT_REAZON := arow.is_for_match;
			when col_id_OUT_DATE then
				col_match_OUT_DATE := arow.is_for_match;
			else
		end case;
	end loop;	
	
	update tmp_ved_reestr_item t1
	set existing_item_id = t2.item_id
	from ved_reestr_region_item t2	
	inner join ved_reestr_region t3 on t2.reestr_id = t3.reestr_id and t3.is_closed = false
	and t3.source_region_id = _source_id
	where 1=1
	and (((col_match_MNN = true) and (trim(coalesce(t1.mnn, '')) = trim(coalesce(t2.mnn, '')))) OR (col_match_MNN = false))
	and (((col_match_COMM_NAME = true) and (trim(coalesce(t1.comm_name, '')) = trim(coalesce(t2.comm_name, '')))) OR (col_match_COMM_NAME = false))
	and (((col_match_PRODUCER = true) and (trim(coalesce(t1.producer, '')) = trim(coalesce(t2.producer, '')))) OR (col_match_PRODUCER = false))
	and (((col_match_PACK_NAME = true) and (trim(coalesce(t1.pack_name, '')) = trim(coalesce(t2.pack_name, '')))) OR (col_match_PACK_NAME = false))
	and (((col_match_PACK_COUNT = true) and (coalesce(t1.pack_count, 0) = coalesce(t2.pack_count, 0))) OR (col_match_PACK_COUNT = false))
	and (((col_match_LIMIT_PRICE = true) and (coalesce(t1.limit_price, 0) = coalesce(t2.limit_price, 0))) OR (col_match_LIMIT_PRICE = false))
	and (((col_match_PRICE_FOR_INIT_PACK = true) and (coalesce(t1.price_for_init_pack, false) = coalesce(t2.price_for_init_pack, false))) OR (col_match_PRICE_FOR_INIT_PACK = false))
	and (((col_match_REG_NUM = true) and (trim(coalesce(t1.reg_num, '')) = trim(coalesce(t2.reg_num, '')))) OR (col_match_REG_NUM = false))
	and (((col_match_PRICE_REG_DATE = true) and (trim(coalesce(t1.price_reg_date_full, '')) = trim(coalesce(t2.price_reg_date_full, '')))) OR (col_match_PRICE_REG_DATE = false))
	and (((col_match_EAN13 = true) and (trim(coalesce(t1.ean13, '')) = trim(coalesce(t2.ean13, '')))) OR (col_match_EAN13 = false))
	;	

	tmp_new_item_cnt := (select count(*) from tmp_ved_reestr_item where coalesce(existing_item_id, 0) <= 0);

	if (tmp_new_item_cnt <=0 ) then
		return;
	end if;		

	curr_reestr_id := coalesce((select max(reestr_id) from ved_reestr_region where is_closed = false and source_region_id = _source_id), 0);
	if (curr_reestr_id <= 0) then
		parent_reestr_id := null;
	else
		parent_reestr_id := (select coalesce(parent_id, reestr_id) from ved_reestr_region where reestr_id = curr_reestr_id);
	end if;

	insert into ved_reestr_region (reestr_name, date_beg, is_closed, source_region_id, crt_date, crt_user, parent_id)
	values ('Региональный реестр от ' || to_char(now, 'YYYY-MM-DD'), today, false, _source_id, now, user_name, parent_reestr_id)	
	returning reestr_id into new_reestr_id;

	with new_items as 
	(insert into ved_reestr_region_item (reestr_id, item_code2, mnn, comm_name, producer, pack_name, pack_count, limit_price, price_for_init_pack, reg_num, price_reg_date_full, price_reg_date, price_reg_ndoc, ean13,
	limit_opt_incr, limit_rozn_incr, limit_rozn_price, limit_rozn_price_with_nds, crt_date, crt_user, state)
	select 
	new_reestr_id, item_code2, mnn, comm_name, producer, pack_name, pack_count, limit_price, price_for_init_pack, reg_num, price_reg_date_full, price_reg_date, price_reg_ndoc, ean13, 
	limit_opt_incr, limit_rozn_incr, limit_rozn_price, limit_rozn_price_with_nds, now, user_name, state
	from tmp_ved_reestr_item
	where coalesce(existing_item_id, 0) <= 0
	returning 1
	)
	select count(*) into inserted_item_cnt from new_items;

	update ved_reestr_region_item t1
	set gos_reestr_item_id = t2.item_id
	from vw_ved_reestr_item t2	
	where t1.reestr_id = new_reestr_id
	and coalesce(t1.gos_reestr_item_id, 0) <= 0
	and (((col_match_MNN = true) and (trim(coalesce(t1.mnn, '')) = trim(coalesce(t2.mnn, '')))) OR (col_match_MNN = false))
	and (((col_match_COMM_NAME = true) and (trim(coalesce(t1.comm_name, '')) = trim(coalesce(t2.comm_name, '')))) OR (col_match_COMM_NAME = false))
	and (((col_match_PRODUCER = true) and (trim(coalesce(t1.producer, '')) = trim(coalesce(t2.producer, '')))) OR (col_match_PRODUCER = false))
	and (((col_match_PACK_NAME = true) and (trim(coalesce(t1.pack_name, '')) = trim(coalesce(t2.pack_name, '')))) OR (col_match_PACK_NAME = false))
	and (((col_match_PACK_COUNT = true) and (coalesce(t1.pack_count, 0) = coalesce(t2.pack_count, 0))) OR (col_match_PACK_COUNT = false))
	and (((col_match_LIMIT_PRICE = true) and (coalesce(t1.limit_price, 0) = coalesce(t2.limit_price, 0))) OR (col_match_LIMIT_PRICE = false))
	and (((col_match_PRICE_FOR_INIT_PACK = true) and (coalesce(t1.price_for_init_pack, false) = coalesce(t2.price_for_init_pack, false))) OR (col_match_PRICE_FOR_INIT_PACK = false))
	and (((col_match_REG_NUM = true) and (trim(coalesce(t1.reg_num, '')) = trim(coalesce(t2.reg_num, '')))) OR (col_match_REG_NUM = false))
	and (((col_match_PRICE_REG_DATE = true) and (trim(coalesce(t1.price_reg_date_full, '')) = trim(coalesce(t2.price_reg_date_full, '')))) OR (col_match_PRICE_REG_DATE = false))
	and (((col_match_EAN13 = true) and (trim(coalesce(t1.ean13, '')) = trim(coalesce(t2.ean13, '')))) OR (col_match_EAN13 = false))	
	;

	select count(*) into err_item_cnt from ved_reestr_region_item where reestr_id = new_reestr_id and coalesce(gos_reestr_item_id, 0) <= 0;

	insert into ved_reestr_column_match (reestr_id, region_id, column_id, is_out)
	select new_reestr_id, t1.region_id, t2.column_id, false
	from ved_source_region t1
	inner join ved_template_column t2 on t1.template_id = t2.template_id and coalesce(t2.is_for_match, false) = true
	where t1.item_id = _source_id;	

	--_result := new_reestr_id;
	return query select new_reestr_id as _reestr_id, inserted_item_cnt as _new_item_cnt, 0::integer as _out_item_cnt, err_item_cnt as _err_item_cnt;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION esn.ved_reestr_region_item_import(bigint) OWNER TO pavlov;
