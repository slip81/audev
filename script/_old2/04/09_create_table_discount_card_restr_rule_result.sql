﻿/*
	CREATE TABLE discount.card_restr_rule_result
*/

-- DROP TABLE discount.card_restr_rule_result;

CREATE TABLE discount.card_restr_rule_result
(
  item_id bigserial NOT NULL,
  card_id bigint NOT NULL,
  restr_id bigint NOT NULL,
  restr_value_id bigint NOT NULL,
  rule_id bigint NOT NULL,
  is_value_reached boolean NOT NULL DEFAULT false,
  is_period_ended boolean NOT NULL DEFAULT false,
  CONSTRAINT card_restr_rule_result_pkey PRIMARY KEY (item_id),
  CONSTRAINT card_restr_rule_result_card_id_fkey FOREIGN KEY (card_id)
      REFERENCES discount.card (card_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT card_restr_rule_result_restr_id_fkey FOREIGN KEY (restr_id)
      REFERENCES discount.card_type_restr (restr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT card_restr_rule_result_restr_value_id_fkey FOREIGN KEY (restr_value_id)
      REFERENCES discount.card_type_restr_value (restr_value_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT card_restr_rule_result_rule_id_fkey FOREIGN KEY (rule_id)
      REFERENCES discount.card_type_restr_rule (rule_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_restr_rule_result OWNER TO pavlov;
