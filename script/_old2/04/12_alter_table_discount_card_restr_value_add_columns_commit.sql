﻿/*
	ALTER TABLE discount.card_restr_value ADD COLUMNs
*/

ALTER TABLE discount.card_restr_value ADD COLUMN is_committed boolean NOT NULL DEFAULT false;
ALTER TABLE discount.card_restr_value ADD COLUMN commit_date timestamp without time zone;