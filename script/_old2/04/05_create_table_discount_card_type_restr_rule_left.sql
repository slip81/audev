﻿/*
	CREATE TABLE discount.card_type_restr_rule_left
*/

-- DROP TABLE discount.card_type_restr_rule_left;

CREATE TABLE discount.card_type_restr_rule_left
(
  rule_id bigint NOT NULL,
  move_to_next_period_num integer,
  kill_in_next_period_num integer,
  move_to_next_period_value numeric,
  kill_in_next_period_value numeric,
  is_absolute_move_value boolean NOT NULL DEFAULT true,
  is_absolute_kill_value boolean NOT NULL DEFAULT true,  
  CONSTRAINT card_type_restr_rule_left_pkey PRIMARY KEY (rule_id),
  CONSTRAINT card_type_restr_rule_left_rule_id_fkey FOREIGN KEY (rule_id)
      REFERENCES discount.card_type_restr_rule (rule_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


ALTER TABLE discount.card_type_restr_rule_left OWNER TO pavlov;
