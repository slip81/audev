﻿/*
	CREATE TABLE discount.card_restr_value
*/

-- DROP TABLE discount.card_restr_value;

CREATE TABLE discount.card_restr_value
(
  item_id bigserial NOT NULL,
  card_id bigint NOT NULL,
  restr_id bigint NOT NULL,
  restr_value_id bigint NOT NULL,
  value_used numeric,
  value_restr numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT card_restr_value_pkey PRIMARY KEY (item_id),
  CONSTRAINT card_restr_value_card_id_fkey FOREIGN KEY (card_id)
      REFERENCES discount.card (card_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT card_restr_value_restr_id_fkey FOREIGN KEY (restr_id)
      REFERENCES discount.card_type_restr (restr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT card_restr_value_restr_value_id_fkey FOREIGN KEY (restr_value_id)
      REFERENCES discount.card_type_restr_value (restr_value_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_restr_value OWNER TO pavlov;
