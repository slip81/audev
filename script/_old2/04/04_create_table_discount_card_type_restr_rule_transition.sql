﻿/*
	CREATE TABLE discount.card_type_restr_rule_transition
*/

-- DROP TABLE discount.card_type_restr_rule_transition;

CREATE TABLE discount.card_type_restr_rule_transition
(
  rule_id bigint NOT NULL,
  value_to_reach numeric,
  is_absolute_value boolean NOT NULL DEFAULT true,
  is_action_on_success boolean NOT NULL DEFAULT true,
  disc_percent_on_success numeric,
  bonus_percent_on_success numeric,
  trans_sum_on_success numeric,
  trans_count_on_success integer,
  card_status_id_on_success bigint,
  is_absolute_value_on_success boolean NOT NULL DEFAULT true,
  is_action_on_fail boolean NOT NULL DEFAULT true,
  disc_percent_on_fail numeric,
  bonus_percent_on_fail numeric,
  trans_sum_on_fail numeric,
  trans_count_on_fail integer,
  card_status_id_on_fail bigint,
  is_absolute_value_on_fail boolean NOT NULL DEFAULT true,  
  CONSTRAINT card_type_restr_rule_transition_pkey PRIMARY KEY (rule_id),
  CONSTRAINT card_type_restr_rule_transition_rule_id_fkey FOREIGN KEY (rule_id)
      REFERENCES discount.card_type_restr_rule (rule_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_type_restr_rule_transition OWNER TO pavlov;
