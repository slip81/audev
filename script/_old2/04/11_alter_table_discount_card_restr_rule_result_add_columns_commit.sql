﻿/*
	ALTER TABLE discount.card_restr_rule_result ADD COLUMNs
*/

ALTER TABLE discount.card_restr_rule_result ADD COLUMN is_committed boolean NOT NULL DEFAULT false;
ALTER TABLE discount.card_restr_rule_result ADD COLUMN commit_date timestamp without time zone;
