﻿/*
	ALTER TABLE discount.card_restr_rule_result ADD COLUMN card_trans_id bigint;
*/

ALTER TABLE discount.card_restr_rule_result ADD COLUMN card_trans_id bigint;