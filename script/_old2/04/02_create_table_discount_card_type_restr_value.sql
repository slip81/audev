﻿/*
	CREATE TABLE discount.card_type_restr_value
*/

-- DROP TABLE discount.card_type_restr_value;

CREATE TABLE discount.card_type_restr_value
(
  restr_value_id bigserial NOT NULL,
  restr_id bigint NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  value_type integer NOT NULL DEFAULT 1,
  restr_value numeric,
  ignore_for_first_period boolean NOT NULL DEFAULT false,
  activation_mode integer,
  payment_type integer,
  card_read_type integer,
  CONSTRAINT card_type_restr_value_pkey PRIMARY KEY (restr_value_id),
  CONSTRAINT card_type_restr_value_restr_id_fkey FOREIGN KEY (restr_id)
      REFERENCES discount.card_type_restr (restr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_type_restr_value OWNER TO pavlov;
