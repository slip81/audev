﻿/*
	ALTER TABLE discount.card_type_restr_rule ADD COLUMNs
*/

ALTER TABLE discount.card_type_restr_rule ADD COLUMN trigger_on_value_end boolean NOT NULL DEFAULT true;
ALTER TABLE discount.card_type_restr_rule ADD COLUMN trigger_on_period_end boolean NOT NULL DEFAULT true;
