﻿/*
	ALTER TABLE discount.card_type_restr_value ADD COLUMNs
*/

ALTER TABLE discount.card_type_restr_value ADD COLUMN is_absolute_period boolean NOT NULL DEFAULT true;
ALTER TABLE discount.card_type_restr_value ADD COLUMN rel_period_unit_value integer;
ALTER TABLE discount.card_type_restr_value ADD COLUMN rel_period_unit_type integer;
