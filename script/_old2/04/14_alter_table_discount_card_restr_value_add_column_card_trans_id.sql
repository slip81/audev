﻿/*
	ALTER TABLE discount.card_restr_value ADD COLUMN card_trans_id bigint;
*/

ALTER TABLE discount.card_restr_value ADD COLUMN card_trans_id bigint;