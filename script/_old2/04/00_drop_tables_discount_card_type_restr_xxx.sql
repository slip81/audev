﻿/*
	DROP TABLE discount.card_type_restr_...
*/

DROP TABLE discount.card_type_restr_rule_transition;
DROP TABLE discount.card_type_restr_rule_left;
DROP TABLE discount.card_type_restr_rule_spent;
DROP TABLE discount.card_type_restr_rule;
DROP TABLE discount.card_type_restr_value;
DROP TABLE discount.card_type_restr;