﻿/*
	CREATE TABLE discount.card_type_restr_rule
*/

-- DROP TABLE discount.card_type_restr_rule;

CREATE TABLE discount.card_type_restr_rule
(
  rule_id bigserial NOT NULL,
  restr_id bigint NOT NULL,
  rule_type integer NOT NULL DEFAULT 1,
  is_active boolean NOT NULL DEFAULT true,
  descr character varying,
  CONSTRAINT card_type_restr_rule_pkey PRIMARY KEY (rule_id),
  CONSTRAINT card_type_restr_rule_restr_id_fkey FOREIGN KEY (restr_id)
      REFERENCES discount.card_type_restr (restr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_type_restr_rule OWNER TO pavlov;
