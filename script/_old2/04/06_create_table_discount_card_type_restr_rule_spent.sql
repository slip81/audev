﻿/*
	CREATE TABLE discount.card_type_restr_rule_spent
*/

-- DROP TABLE discount.card_type_restr_rule_spent;

CREATE TABLE discount.card_type_restr_rule_spent
(
  rule_id bigint NOT NULL,
  activate_in_next_period_num integer,
  activate_in_next_period_value numeric,
  is_absolute_activate_value boolean NOT NULL DEFAULT true,  
  CONSTRAINT card_type_restr_rule_spent_pkey PRIMARY KEY (rule_id),
  CONSTRAINT card_type_restr_rule_spent_rule_id_fkey FOREIGN KEY (rule_id)
      REFERENCES discount.card_type_restr_rule (rule_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


ALTER TABLE discount.card_type_restr_rule_spent OWNER TO pavlov;
