﻿/*
	CREATE TABLE discount.card_type_restr
*/

-- DROP TABLE discount.card_type_restr;

CREATE TABLE discount.card_type_restr
(
  restr_id bigserial NOT NULL,
  card_type_id bigint NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  descr character varying,
  restr_type integer NOT NULL DEFAULT 1,
  period_type integer NOT NULL DEFAULT 1,
  CONSTRAINT card_type_restr_pkey PRIMARY KEY (restr_id),
  CONSTRAINT card_type_restr_card_type_id_fkey FOREIGN KEY (card_type_id)
      REFERENCES discount.card_type (card_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_type_restr OWNER TO pavlov;
