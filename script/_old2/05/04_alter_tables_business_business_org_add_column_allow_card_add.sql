﻿/*
	ALTER TABLE discount.business ADD COLUMN allow_card_add
	ALTER TABLE discount.business_org ADD COLUMN allow_card_add
*/

ALTER TABLE discount.business ADD COLUMN allow_card_add boolean NOT NULL DEFAULT false;
ALTER TABLE discount.business_org ADD COLUMN allow_card_add boolean NOT NULL DEFAULT false;