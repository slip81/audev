﻿/*
	CREATE TABLE discount.card_type_check_info
*/

-- DROP TABLE discount.card_type_check_info;

CREATE TABLE discount.card_type_check_info
(
  card_type_id bigint NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  print_sum_trans boolean NOT NULL DEFAULT true,
  print_bonus_value_trans boolean NOT NULL DEFAULT true,
  print_bonus_value_card boolean NOT NULL DEFAULT true,
  print_bonus_percent_card boolean NOT NULL DEFAULT true,
  print_disc_percent_card boolean NOT NULL DEFAULT true,
  CONSTRAINT card_type_check_info_pkey PRIMARY KEY (card_type_id),
  CONSTRAINT card_type_check_info_card_type_id_fkey FOREIGN KEY (card_type_id)
      REFERENCES discount.card_type (card_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_type_check_info OWNER TO pavlov;
