﻿/*
	insert into discount.card_type_check_info
*/

delete from discount.card_type_check_info;

insert into discount.card_type_check_info (card_type_id, is_active, print_sum_trans, print_bonus_value_trans, print_bonus_value_card, print_bonus_percent_card, print_disc_percent_card)
select card_type_id, false, false, false, false, false, false
from discount.card_type;