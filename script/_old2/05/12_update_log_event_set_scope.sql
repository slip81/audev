﻿/*
	update log_event set scope = 1
*/

update discount.log_event set scope = 1 where coalesce(scope, 0) <= 0;
update esn.log_event set scope = 3 where coalesce(scope, 0) <= 0 and log_event_type_id in (2,3);
update esn.log_event set scope = 4 where coalesce(scope, 0) <= 0 and log_event_type_id in (4,5,6,101);
update esn.log_event set scope = 5 where coalesce(scope, 0) <= 0 and log_event_type_id in (7,8,9);