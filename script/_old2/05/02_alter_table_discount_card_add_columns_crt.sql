﻿/*
	ALTER TABLE discount.card ADD COLUMNs	
*/

ALTER TABLE discount.card ADD COLUMN crt_date timestamp without time zone;
ALTER TABLE discount.card ADD COLUMN crt_user character varying;
ALTER TABLE discount.card ADD COLUMN from_au boolean NOT NULL DEFAULT false;