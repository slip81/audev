﻿/*
	ALTER TABLE esn.exchange_reg_partner ADD COLUMNs
*/

ALTER TABLE esn.exchange_reg_partner ADD COLUMN pop3_address character varying;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN pop3_port integer;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN pop3_use_ssl boolean NOT NULL DEFAULT false;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_equals_download boolean NOT NULL DEFAULT false;
ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_url_alias character varying;
