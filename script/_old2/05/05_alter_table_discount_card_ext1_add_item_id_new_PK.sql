﻿/*
	ALTER TABLE discount.card_ext1 ADD COLUMN item_id
*/

ALTER TABLE discount.card_ext1 DROP CONSTRAINT card_ext1_pkey;
ALTER TABLE discount.card_ext1 ADD COLUMN item_id bigserial NOT NULL;
ALTER TABLE discount.card_ext1 ADD CONSTRAINT card_ext1_pkey PRIMARY KEY(item_id);