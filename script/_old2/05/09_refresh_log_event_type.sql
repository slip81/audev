﻿/*
	refresh log_event_type
*/

insert into discount.log_event_type (type_id, type_name)
values (101, 'Ошибка');

insert into esn.log_event_type (type_id, type_name)
values (101, 'Ошибка');

update discount.log_event_type 
set type_name = 'Ошибка (old)'
where type_id = 32;

update esn.log_event_type 
set type_name = 'Ошибка (old)'
where type_id = 1;

-- select * from discount.log_event_type
-- select * from esn.log_event_type