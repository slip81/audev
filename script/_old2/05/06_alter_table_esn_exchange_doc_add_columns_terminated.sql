﻿/*
	ALTER TABLE esn.exchange_doc ADD COLUMNs
*/

ALTER TABLE esn.exchange_doc ADD COLUMN is_terminated boolean NOT NULL DEFAULT false;
ALTER TABLE esn.exchange_doc ADD COLUMN terminated_date timestamp without time zone;
