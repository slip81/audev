﻿/*
	CREATE TABLE esn.exchange_doc_target
*/

-- DROP TABLE esn.exchange_doc_target;

CREATE TABLE esn.exchange_doc_target
(
  item_id bigserial NOT NULL,
  exchange_doc_id bigint NOT NULL,
  target_sales_id integer,
  target_workplace_id integer,
  curr_state_id integer NOT NULL DEFAULT 0,
  sent_to_email character varying,
  CONSTRAINT exchange_doc_target_pkey PRIMARY KEY (item_id),
  CONSTRAINT exchange_doc_target_exchange_doc_id_fkey FOREIGN KEY (exchange_doc_id)
      REFERENCES esn.exchange_doc (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_doc_target OWNER TO pavlov;
