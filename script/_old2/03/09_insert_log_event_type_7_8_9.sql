﻿/*
	insert into esn.log_event_type
*/

-- delete from esn.log_event_type where type_id in (7, 8, 9)

insert into esn.log_event_type (type_id, type_name)
values (7, 'EXCHANGE_DOC: Отправка документа');

insert into esn.log_event_type (type_id, type_name)
values (8, 'EXCHANGE_DOC: Получение документов');

insert into esn.log_event_type (type_id, type_name)
values (9, 'EXCHANGE_DOC: Смена статуса документов');

-- select * from esn.log_event_type
