﻿/*
	CREATE TABLE esn.exchange_doc_data
*/

-- DROP TABLE esn.exchange_doc_data;

CREATE TABLE esn.exchange_doc_data
(
  item_id bigint NOT NULL,
  data_text character varying,
  data_binary bytea,
  CONSTRAINT exchange_doc_data_pkey PRIMARY KEY (item_id),
  CONSTRAINT exchange_doc_data_item_id_fkey FOREIGN KEY (item_id)
      REFERENCES esn.exchange_doc (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_doc_data OWNER TO pavlov;
