﻿/*
	CREATE TABLE esn.exchange_doc_target_state
*/

-- DROP TABLE esn.exchange_doc_target_state;

CREATE TABLE esn.exchange_doc_target_state
(
  item_id bigserial NOT NULL,
  target_id bigint NOT NULL,
  doc_state integer NOT NULL DEFAULT 0,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT exchange_doc_target_state_pkey PRIMARY KEY (item_id),
  CONSTRAINT exchange_doc_target_state_target_id_fkey FOREIGN KEY (target_id)
      REFERENCES esn.exchange_doc_target (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_doc_target_state OWNER TO pavlov;
