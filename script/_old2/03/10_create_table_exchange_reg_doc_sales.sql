﻿/*
	CREATE TABLE esn.exchange_reg_doc_sales
*/

-- DROP TABLE esn.exchange_reg_doc_sales;

CREATE TABLE esn.exchange_reg_doc_sales
(
  item_id bigserial NOT NULL,
  reg_id bigint NOT NULL,
  sales_id integer,
  sales_name character varying,
  is_active boolean NOT NULL DEFAULT true,
  email_for_docs character varying,
  CONSTRAINT exchange_reg_doc_sales_pkey PRIMARY KEY (item_id),
  CONSTRAINT exchange_reg_doc_sales_reg_id_fkey FOREIGN KEY (reg_id)
      REFERENCES esn.exchange_reg_doc (reg_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_reg_doc_sales OWNER TO pavlov;
