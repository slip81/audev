﻿/*
	CREATE TABLE esn.exchange_doc
*/

-- DROP TABLE esn.exchange_doc;

CREATE TABLE esn.exchange_doc
(
  item_id bigserial NOT NULL,
  user_id bigint NOT NULL,
  sales_id integer,
  workplace_id integer,
  doc_id integer,
  doc_name character varying,
  doc_format character varying,
  doc_comment character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  act_date timestamp without time zone,
  act_user character varying,
  CONSTRAINT exchange_doc_pkey PRIMARY KEY (item_id),
  CONSTRAINT exchange_doc_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES esn.exchange_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_doc OWNER TO pavlov;
