﻿/*
	CREATE TABLE esn.exchange_reg_doc
*/

-- DROP TABLE esn.exchange_reg_doc;

CREATE TABLE esn.exchange_reg_doc
(
  reg_id bigint NOT NULL,
  days_to_keep_data integer,
  CONSTRAINT exchange_reg_doc_pkey PRIMARY KEY (reg_id),
  CONSTRAINT exchange_reg_doc_reg_id_fkey FOREIGN KEY (reg_id)
      REFERENCES esn.exchange_reg (reg_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_reg_doc OWNER TO pavlov;
