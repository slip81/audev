﻿/*
	ALTER TABLE esn.exchange_doc ADD COLUMN terminate_date timestamp without time zone;	
*/

ALTER TABLE esn.exchange_doc ADD COLUMN terminate_date timestamp without time zone;