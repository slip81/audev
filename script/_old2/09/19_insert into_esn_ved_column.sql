﻿/*
	insert into esn.ved_column
*/

-- select * from  esn.ved_column

insert into esn.ved_column (column_id, column_name)
values (1, 'МНН');

insert into esn.ved_column (column_id, column_name)
values (2, 'Торговое наименование');

insert into esn.ved_column (column_id, column_name)
values (3, 'Упаковка');

insert into esn.ved_column (column_id, column_name)
values (4, 'Производитель');

insert into esn.ved_column (column_id, column_name)
values (5, 'Кол-во в упаковке');

insert into esn.ved_column (column_id, column_name)
values (6, 'Предельная цена');

insert into esn.ved_column (column_id, column_name)
values (7, 'Цена для первич. упаковки');

insert into esn.ved_column (column_id, column_name)
values (8, '№ РУ');

insert into esn.ved_column (column_id, column_name)
values (9, 'Дата и номер решения');

insert into esn.ved_column (column_id, column_name)
values (10, 'Штрих-код');

insert into esn.ved_column (column_id, column_name)
values (11, 'Предельная опт. надбавка');

insert into esn.ved_column (column_id, column_name)
values (12, 'Предельная розн. надбавка');

insert into esn.ved_column (column_id, column_name)
values (13, 'Предельная розн. цена без НДС');

insert into esn.ved_column (column_id, column_name)
values (14, 'Предельная розн. цена с НДС');

insert into esn.ved_column (column_id, column_name)
values (15, 'Причина исключения');

insert into esn.ved_column (column_id, column_name)
values (16, 'Дата исключения');