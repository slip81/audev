﻿/*
	CREATE TABLE esn.ved_reestr_region_item
*/

-- DROP TABLE esn.ved_reestr_region_item;

CREATE TABLE esn.ved_reestr_region_item
(
  item_id bigserial NOT NULL,
  reestr_id bigint NOT NULL,
  item_code bigint,
  item_code2 character varying,
  mnn character varying,
  comm_name character varying,
  producer character varying,
  pack_name character varying,
  pack_count integer,
  limit_price numeric,
  price_for_init_pack boolean NOT NULL DEFAULT false,
  reg_num character varying,
  price_reg_date character varying,
  ean13 character varying,
  limit_opt_incr numeric,
  limit_rozn_incr numeric,
  limit_rozn_price numeric,
  limit_rozn_price_with_nds numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  state integer,
  gos_reestr_item_id bigint,
  tmp_to_out boolean NOT NULL DEFAULT false,
  tmp_out_date date,
  tmp_out_reazon character varying,
  CONSTRAINT ved_reestr_region_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT ved_reestr_region_item_reestr_id_fkey FOREIGN KEY (reestr_id)
      REFERENCES esn.ved_reestr_region (reestr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_reestr_region_item OWNER TO pavlov;
