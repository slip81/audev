﻿/*
	ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMNs
*/

ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN existing_item_id bigint;
ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN mnn_id bigint;
ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN comm_name_id bigint;
ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN producer_id bigint;
ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN limit_opt_incr numeric;
ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN limit_rozn_incr numeric;
ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN limit_rozn_price numeric;
ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN limit_rozn_price_with_nds numeric;
