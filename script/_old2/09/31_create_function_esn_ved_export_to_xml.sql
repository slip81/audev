﻿/*
	CREATE OR REPLACE FUNCTION esn.ved_export_to_xml()
*/

-- DROP FUNCTION esn.ved_export_to_xml();

CREATE OR REPLACE FUNCTION esn.ved_export_to_xml()
  RETURNS void AS
$BODY$
begin        
	COPY (
		SELECT
		    XMLFOREST(tb1."xml-val" AS "Reestr")
		FROM
		    (
			SELECT
			    XMLAGG((esn.to_russian_symbols((XMLELEMENT(NAME Ved, XMLATTRIBUTES(t.item_code AS item_code, 
						trim(replace(replace(t.mnn, '"', ''''), '&', 'and')) AS mnn, 
						trim(replace(replace(t.comm_name, '"', ''''), '&', 'and')) AS comm_name, 					
						trim(replace(replace(t.pack_name, '"', ''''), '&', 'and')) AS pack_name,
						t.pack_count AS pack_count,
						trim(replace(replace(t.producer, '"', ''''), '&', 'and')) as producer
						)))::text))::xml) AS "xml-val"
			FROM
			    esn.vw_ved_reestr_item t 
			    --where t.item_code = 598611

		    ) AS tb1
	)
	TO 'c:\111.xml';
end;
$BODY$
  LANGUAGE plpgsql IMMUTABLE
  COST 100;

ALTER FUNCTION esn.ved_export_to_xml() OWNER TO pavlov;
