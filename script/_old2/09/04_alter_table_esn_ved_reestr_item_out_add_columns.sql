﻿/*
	ALTER TABLE esn.ved_reestr_item_out ADD COLUMNs
*/

ALTER TABLE esn.ved_reestr_item_out ADD COLUMN crt_date timestamp without time zone;
ALTER TABLE esn.ved_reestr_item_out ADD COLUMN crt_user character varying;