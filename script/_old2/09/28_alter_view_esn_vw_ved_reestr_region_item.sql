﻿/*
	CREATE OR REPLACE VIEW esn.vw_ved_reestr_region_item AS 
*/

-- DROP VIEW esn.vw_ved_reestr_region_item;

CREATE OR REPLACE VIEW esn.vw_ved_reestr_region_item AS 
 SELECT t1.reestr_id,
    t1.reestr_name,
    t1.date_beg,
    t1.crt_date AS reestr_crt_date,
    COALESCE(t1.parent_id, t1.reestr_id) AS parent_id,
    t2.item_id,
    t2.item_code,
    t2.pack_name,
    t2.pack_count,
    t2.limit_price,
    t2.price_for_init_pack,
    t2.reg_num,
    t2.price_reg_date,
    t2.ean13,
    t2.limit_opt_incr,
    t2.limit_rozn_incr,
    t2.limit_rozn_price,
    t2.limit_rozn_price_with_nds,
    t2.crt_date,
    t2.mnn,
    t2.producer,
    t2.comm_name,
    t2.gos_reestr_item_id,
    t3.region_id,
    t2.price_reg_ndoc,
    t2.price_reg_date_full
   FROM esn.ved_reestr_region t1
     JOIN esn.ved_reestr_region_item t2 ON t1.reestr_id = t2.reestr_id
     LEFT JOIN esn.ved_source_region t3 ON t1.source_region_id = t3.item_id
  WHERE NOT (EXISTS ( SELECT x1.out_id
           FROM esn.ved_reestr_region_item_out x1
          WHERE t2.item_id = x1.reestr_item_id));


ALTER TABLE esn.vw_ved_reestr_region_item OWNER TO pavlov;
