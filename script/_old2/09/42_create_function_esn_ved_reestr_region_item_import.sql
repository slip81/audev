﻿/*
	CREATE OR REPLACE FUNCTION esn.ved_reestr_region_item_import
*/

-- DROP FUNCTION esn.ved_reestr_region_item_import(bigint);

CREATE OR REPLACE FUNCTION esn.ved_reestr_region_item_import(
    _source_id bigint
    )
  RETURNS TABLE (_reestr_id bigint, _new_item_cnt integer, _out_item_cnt integer, _err_item_cnt integer) 
  AS
$BODY$
DECLARE
    tmp_item_cnt integer;    
    tmp_new_item_cnt integer;    
    inserted_item_cnt integer;
    err_item_cnt integer;
    curr_reestr_id bigint;
    parent_reestr_id bigint;
    reestr_is_closed boolean;    
    new_reestr_id bigint;
    user_name character varying;
    now timestamp without time zone;
    today date;
BEGIN
	set search_path to 'esn';
	
	--_result := -1;
	-- return query select -1::bigint as _reestr_id, 0::integer as _new_item_cnt, 0::integer as _out_item_cnt, 0::integer as _err_item_cnt;
	
	now = current_timestamp;
	today = current_date;
	user_name = 'downloader-ved';	

	tmp_item_cnt := (select count(*) from tmp_ved_reestr_item);	

	if (tmp_item_cnt <=0) then
		return;
	end if;

	-- !!! стыковать по всем первым колонкам
	update tmp_ved_reestr_item t1
	set existing_item_id = t2.item_id
	from ved_reestr_region_item t2	
	inner join ved_reestr_region t3 on t2.reestr_id = t3.reestr_id and t3.is_closed = false
	and t3.source_region_id = _source_id
	where trim(coalesce(t1.ean13, '')) = trim(coalesce(t2.ean13, ''))
	and coalesce(t1.limit_price, 0) = coalesce(t2.limit_price, 0)
	and trim(coalesce(t1.reg_num, '')) = trim(coalesce(t2.reg_num, ''))
	and trim(coalesce(t1.price_reg_date_full, '')) = trim(coalesce(t2.price_reg_date_full, ''))
	;	

	tmp_new_item_cnt := (select count(*) from tmp_ved_reestr_item where coalesce(existing_item_id, 0) <= 0);

	if (tmp_new_item_cnt <=0 ) then
		return;
	end if;		

	curr_reestr_id := coalesce((select max(reestr_id) from ved_reestr_region where is_closed = false and source_region_id = _source_id), 0);
	if (curr_reestr_id <= 0) then
		parent_reestr_id := null;
	else
		parent_reestr_id := (select coalesce(parent_id, reestr_id) from ved_reestr_region where reestr_id = curr_reestr_id);
	end if;

	insert into ved_reestr_region (reestr_name, date_beg, is_closed, source_region_id, crt_date, crt_user, parent_id)
	values ('Региональный реестр от ' || to_char(now, 'YYYY-MM-DD'), today, false, _source_id, now, user_name, parent_reestr_id)	
	returning reestr_id into new_reestr_id;

	with new_items as 
	(insert into ved_reestr_region_item (reestr_id, item_code2, mnn, comm_name, producer, pack_name, pack_count, limit_price, price_for_init_pack, reg_num, price_reg_date_full, price_reg_date, price_reg_ndoc, ean13,
	limit_opt_incr, limit_rozn_incr, limit_rozn_price, limit_rozn_price_with_nds, crt_date, crt_user, state)
	select 
	new_reestr_id, item_code2, mnn, comm_name, producer, pack_name, pack_count, limit_price, price_for_init_pack, reg_num, price_reg_date_full, price_reg_date, price_reg_ndoc, ean13, 
	limit_opt_incr, limit_rozn_incr, limit_rozn_price, limit_rozn_price_with_nds, now, user_name, state
	from tmp_ved_reestr_item
	where coalesce(existing_item_id, 0) <= 0
	returning 1
	)
	select count(*) into inserted_item_cnt from new_items;

	update ved_reestr_region_item t1
	set gos_reestr_item_id = t2.item_id
	from ved_reestr_item t2	
	where t1.reestr_id = new_reestr_id
	and trim(coalesce(t1.ean13, '')) = trim(coalesce(t2.ean13, ''))
	and coalesce(t1.limit_price, 0) = coalesce(t2.limit_price, 0)
	and trim(coalesce(t1.reg_num, '')) = trim(coalesce(t2.reg_num, ''))
	and trim(coalesce(t1.price_reg_date_full, '')) = trim(coalesce(t2.price_reg_date_full, ''))	
	and coalesce(t1.gos_reestr_item_id, 0) <= 0
	;

	select count(*) into err_item_cnt from ved_reestr_region_item where reestr_id = new_reestr_id and coalesce(gos_reestr_item_id, 0) <= 0;

	--_result := new_reestr_id;
	return query select new_reestr_id as _reestr_id, inserted_item_cnt as _new_item_cnt, 0::integer as _out_item_cnt, err_item_cnt as _err_item_cnt;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION esn.ved_reestr_region_item_import(bigint) OWNER TO pavlov;