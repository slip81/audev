﻿/*
	CREATE OR REPLACE VIEW esn.vw_ved_reestr_region_item AS 
*/

-- DROP VIEW esn.vw_ved_reestr_region_item;

CREATE OR REPLACE VIEW esn.vw_ved_reestr_region_item AS 

	select t1.reestr_id, t1.reestr_name, t1.date_beg, t1.crt_date as reestr_crt_date, coalesce(t1.parent_id, t1.reestr_id) as parent_id
	, t2.item_id, t2.item_code, t2.pack_name, t2.pack_count, t2.limit_price, t2.price_for_init_pack, t2.reg_num, t2.price_reg_date, t2.ean13
	, t2.limit_opt_incr, t2.limit_rozn_incr, t2.limit_rozn_price, t2.limit_rozn_price_with_nds, t2.crt_date
	, t2.mnn, t2.producer, t2.comm_name
	, t2.gos_reestr_item_id, t3.region_id
	from esn.ved_reestr_region t1
	inner join esn.ved_reestr_region_item t2 on t1.reestr_id = t2.reestr_id	
	left join esn.ved_source_region t3 on t1.source_region_id = t3.item_id
	where not exists(select x1.out_id from esn.ved_reestr_region_item_out x1 where t2.item_id = x1.reestr_item_id)
	;

ALTER TABLE esn.vw_ved_reestr_region_item OWNER TO pavlov;

-- select * from esn.vw_ved_reestr_region_item
