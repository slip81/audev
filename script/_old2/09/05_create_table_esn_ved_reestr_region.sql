﻿/*
	CREATE TABLE esn.ved_reestr_region
*/

-- DROP TABLE esn.ved_reestr_region;

CREATE TABLE esn.ved_reestr_region
(
  reestr_id bigserial NOT NULL,  
  reestr_name character varying,
  date_beg date,
  date_end date,
  is_closed boolean NOT NULL DEFAULT false,
  source_region_id bigint NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  parent_id bigint,
  CONSTRAINT ved_reestr_region_pkey PRIMARY KEY (reestr_id),
  CONSTRAINT ved_reestr_region_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES esn.ved_reestr_region (reestr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ved_reestr_region_source_region_id_fkey FOREIGN KEY (source_region_id)
      REFERENCES esn.ved_source_region (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_reestr_region OWNER TO pavlov;
