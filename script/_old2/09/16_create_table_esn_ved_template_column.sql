﻿/*
	CREATE TABLE esn.ved_template_column
*/

-- DROP TABLE esn.ved_template_column;

CREATE TABLE esn.ved_template_column
(
  item_id bigserial NOT NULL,
  template_id bigint NOT NULL,
  column_id integer NOT NULL,
  column_num integer NOT NULL DEFAULT 1,
  is_active boolean NOT NULL DEFAULT true,
  is_for_match boolean NOT NULL DEFAULT false,
  CONSTRAINT ved_template_column_pkey PRIMARY KEY (item_id),
  CONSTRAINT ved_template_column_column_id_fkey FOREIGN KEY (column_id)
      REFERENCES esn.ved_column (column_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ved_template_column_template_id_fkey FOREIGN KEY (template_id)
      REFERENCES esn.ved_template (template_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_template_column OWNER TO pavlov;
