﻿/*
	ALTER TABLE esn.ved_reestr_item ADD COLUMNs
*/

ALTER TABLE esn.ved_reestr_item ADD COLUMN tmp_to_out boolean NOT NULL DEFAULT false;
ALTER TABLE esn.ved_reestr_item ADD COLUMN tmp_out_date date;
ALTER TABLE esn.ved_reestr_item ADD COLUMN tmp_out_reazon character varying;
