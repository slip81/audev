﻿/*
	ALTER TABLE esn.ved_source_region DROP COLUMN ved_source_id
*/

ALTER TABLE esn.ved_source_region DROP CONSTRAINT ved_source_region_ved_source_id_fkey;
ALTER TABLE esn.ved_source_region DROP COLUMN ved_source_id;