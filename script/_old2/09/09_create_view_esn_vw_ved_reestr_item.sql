﻿/*
	CREATE OR REPLACE VIEW esn.vw_ved_reestr_item AS 
*/

CREATE OR REPLACE VIEW esn.vw_ved_reestr_item AS 

	select t1.reestr_id, t1.reestr_name, t1.date_beg, t1.crt_date as reestr_crt_date, coalesce(t1.parent_id, t1.reestr_id) as parent_id
	, t2.item_id, t2.item_code, t2.mnn_id, t2.comm_name_id, t2.producer_id, t2.pack_name, t2.pack_count, t2.limit_price, t2.price_for_init_pack, t2.reg_num, t2.price_reg_date, t2.ean13, t2.crt_date
	, t3.name as mnn, t4.name as producer, t5.name as comm_name
	from esn.ved_reestr t1
	inner join esn.ved_reestr_item t2 on t1.reestr_id = t2.reestr_id
	left join esn.prep_mnn t3 on t2.mnn_id = t3.mnn_id
	left join esn.prep_producer t4 on t2.producer_id = t4.producer_id
	left join esn.prep_commercial_name t5 on t2.comm_name_id = t5.commercial_name_id
	where not exists(select x1.out_id from esn.ved_reestr_item_out x1 where t2.item_id = x1.reestr_item_id)
	;

ALTER TABLE esn.vw_ved_reestr_item OWNER TO pavlov;

-- select * from  esn.vw_ved_reestr_item
