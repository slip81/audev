﻿/*
	ALTER TABLE esn.ved_source_region ADD COLUMNs
*/

-- ALTER TABLE esn.ved_source_region DROP CONSTRAINT ved_source_region_template_id_fkey

ALTER TABLE esn.ved_source_region ADD COLUMN url_file_a_content character varying;
ALTER TABLE esn.ved_source_region ADD COLUMN template_id bigint;
ALTER TABLE esn.ved_source_region ADD COLUMN out_template_id bigint;
ALTER TABLE esn.ved_source_region
  ADD CONSTRAINT ved_source_region_template_id_fkey FOREIGN KEY (template_id)
      REFERENCES esn.ved_template (template_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE esn.ved_source_region
  ADD CONSTRAINT ved_source_region_out_template_id_fkey FOREIGN KEY (out_template_id)
      REFERENCES esn.ved_template (template_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      
