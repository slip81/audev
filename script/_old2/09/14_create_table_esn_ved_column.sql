﻿/*
	CREATE TABLE esn.ved_column
*/

-- DROP TABLE esn.ved_column;

CREATE TABLE esn.ved_column
(
  column_id integer NOT NULL,
  column_name character varying,
  CONSTRAINT ved_column_pkey PRIMARY KEY (column_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_column  OWNER TO pavlov;
