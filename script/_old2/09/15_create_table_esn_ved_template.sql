﻿/*
	CREATE TABLE esn.ved_template
*/

-- DROP TABLE esn.ved_template;

CREATE TABLE esn.ved_template
(
  template_id bigserial NOT NULL,
  template_name character varying,
  start_row_num int NOT NULL DEFAULT 1,
  start_col_num int NOT NULL DEFAULT 1,
  list_name character varying,  
  CONSTRAINT ved_template_pkey PRIMARY KEY (template_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_template OWNER TO pavlov;
