﻿/*
	CREATE TABLE esn.ved_reestr_region_item_out
*/

-- DROP TABLE esn.ved_reestr_region_item_out;

CREATE TABLE esn.ved_reestr_region_item_out
(
  out_id bigserial NOT NULL,
  reestr_item_id bigint,
  out_date date,
  out_reazon character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT ved_reestr_region_item_out_pkey PRIMARY KEY (out_id),
  CONSTRAINT ved_reestr_region_item_out_reestr_item_id_fkey FOREIGN KEY (reestr_item_id)
      REFERENCES esn.ved_reestr_region_item (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE esn.ved_reestr_region_item_out OWNER TO pavlov;
