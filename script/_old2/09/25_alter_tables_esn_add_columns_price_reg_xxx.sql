﻿/*
	ALTER TABLEs ADD COLUMNs price_reg_xxx
*/

ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN price_reg_ndoc character varying;
ALTER TABLE esn.tmp_ved_reestr_item ADD COLUMN price_reg_date_full character varying;

ALTER TABLE esn.tmp_ved_reestr_item_out ADD COLUMN price_reg_ndoc character varying;
ALTER TABLE esn.tmp_ved_reestr_item_out ADD COLUMN price_reg_date_full character varying;

ALTER TABLE esn.ved_reestr_item ADD COLUMN price_reg_ndoc character varying;
ALTER TABLE esn.ved_reestr_item ADD COLUMN price_reg_date_full character varying;

ALTER TABLE esn.ved_reestr_item_out ADD COLUMN price_reg_ndoc character varying;
ALTER TABLE esn.ved_reestr_item_out ADD COLUMN price_reg_date_full character varying;

ALTER TABLE esn.ved_reestr_region_item ADD COLUMN price_reg_ndoc character varying;
ALTER TABLE esn.ved_reestr_region_item ADD COLUMN price_reg_date_full character varying;

ALTER TABLE esn.ved_reestr_region_item_out ADD COLUMN price_reg_ndoc character varying;
ALTER TABLE esn.ved_reestr_region_item_out ADD COLUMN price_reg_date_full character varying;