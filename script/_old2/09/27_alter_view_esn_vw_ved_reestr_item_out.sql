﻿/*
	CREATE OR REPLACE VIEW esn.vw_ved_reestr_item_out AS 
*/

-- DROP VIEW esn.vw_ved_reestr_item_out;

CREATE OR REPLACE VIEW esn.vw_ved_reestr_item_out AS
 SELECT t1.reestr_id,
    t1.reestr_name,
    t1.date_beg,
    t1.crt_date AS reestr_crt_date,
    COALESCE(t1.parent_id, t1.reestr_id) AS parent_id,
    t2.item_id,
    t2.item_code,
    t2.mnn_id,
    t2.comm_name_id,
    t2.producer_id,
    t2.pack_name,
    t2.pack_count,
    t2.limit_price,
    t2.price_for_init_pack,
    t2.reg_num,
    t2.price_reg_date,
    t2.ean13,
    t2.crt_date,
    t3.out_date,
    t3.out_reazon,
    t4.name AS mnn,
    t5.name AS producer,
    t6.name AS comm_name,
    t2.price_reg_ndoc,
    t2.price_reg_date_full
   FROM esn.ved_reestr t1
     JOIN esn.ved_reestr_item t2 ON t1.reestr_id = t2.reestr_id
     JOIN esn.ved_reestr_item_out t3 ON t2.item_id = t3.reestr_item_id
     LEFT JOIN esn.prep_mnn t4 ON t2.mnn_id = t4.mnn_id
     LEFT JOIN esn.prep_producer t5 ON t2.producer_id = t5.producer_id
     LEFT JOIN esn.prep_commercial_name t6 ON t2.comm_name_id = t6.commercial_name_id;

ALTER TABLE esn.vw_ved_reestr_item_out OWNER TO pavlov;
