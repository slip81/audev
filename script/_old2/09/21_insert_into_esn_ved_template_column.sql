﻿/*
	insert into esn.ved_template_column
*/

-- select * from esn.ved_column
-- select * from esn.ved_template
-- select * from esn.ved_template_column order by template_id, column_num

-- delete from esn.ved_template_column 

-- Ш1: госреестр, основные строки
insert into esn.ved_template_column (template_id, column_id, column_num, is_active, is_for_match)
select 1, column_id, column_id, true, true
from esn.ved_column;

update esn.ved_template_column
set is_active = false, is_for_match = false
where template_id = 1
and column_id > 10;

update esn.ved_template_column
set is_for_match = false
where template_id = 1
and column_id not in (6, 8, 9, 10);

-- Ш2: госреестр, исключенные строки
insert into esn.ved_template_column (template_id, column_id, column_num, is_active, is_for_match)
select 2, column_id, column_id, true, true
from esn.ved_column;

update esn.ved_template_column
set is_active = false, is_for_match = false
where template_id = 2
and column_id in (11, 12, 13, 14);

update esn.ved_template_column
set is_for_match = false
where template_id = 2
and column_id not in (6, 8, 9, 10);

update esn.ved_template_column
set column_num = case when column_id <= 10 then column_id when column_id = 15 then 11 when column_id = 16 then 12 else 0 end
where template_id = 2;

update esn.ved_template_column
set column_num = column_num + 1
where template_id = 2
and column_id = 16;

-- Ш3: региональный реестр, основные строки
insert into esn.ved_template_column (template_id, column_id, column_num, is_active, is_for_match)
select 3, column_id, column_id, true, true
from esn.ved_column;

update esn.ved_template_column
set is_active = false, is_for_match = false
where template_id = 3
and column_id > 15;

update esn.ved_template_column
set is_for_match = false
where template_id = 3
and column_id not in (6, 8, 9, 10);

update esn.ved_template_column
set column_num = case when column_id <= 5 then column_id when column_id = 6 then 9 when column_id = 7 then 10 when column_id = 8 then 6 when column_id = 9 then 7 when column_id = 10 then 8
when column_id >= 11 and column_id <= 14 then column_id else 0 end
where template_id = 3;


-- Ш4: региональный реестр, исключенные строки
insert into esn.ved_template_column (template_id, column_id, column_num, is_active, is_for_match)
select 4, column_id, column_id, true, true
from esn.ved_column;

update esn.ved_template_column
set is_active = false, is_for_match = false
where template_id = 4
and column_id in (11, 12, 13, 14);

update esn.ved_template_column
set is_for_match = false
where template_id = 4
and column_id not in (6, 8, 9, 10);

update esn.ved_template_column
set column_num = case when column_id <= 10 then column_id when column_id = 15 then 11 when column_id = 16 then 12 else 0 end
where template_id = 4;

update esn.ved_template_column
set column_num = column_num + 1
where template_id = 4
and column_id = 16;