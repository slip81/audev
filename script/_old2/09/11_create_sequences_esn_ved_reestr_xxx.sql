﻿/*
	CREATE SEQUENCEs esn.ved_reestr_xxx
*/


CREATE SEQUENCE esn.ved_reestr_item_item_code_seq START 1;
ALTER TABLE esn.ved_reestr_item ALTER COLUMN item_code SET NOT NULL;
ALTER TABLE esn.ved_reestr_item ALTER COLUMN item_code SET DEFAULT nextval('esn.ved_reestr_item_item_code_seq'::regclass);

CREATE SEQUENCE esn.ved_reestr_region_item_item_code_seq START 1;
ALTER TABLE esn.ved_reestr_region_item ALTER COLUMN item_code SET NOT NULL;
ALTER TABLE esn.ved_reestr_region_item ALTER COLUMN item_code SET DEFAULT nextval('esn.ved_reestr_region_item_item_code_seq'::regclass);