﻿/*
	ALTER VIEW esn.vw_ved_source_region AS
*/

-- DROP VIEW esn.vw_ved_source_region;

CREATE OR REPLACE VIEW esn.vw_ved_source_region AS 
 SELECT t1.item_id,    
    t1.region_id,
    t1.url_site,
    t1.url_file_main,
    t1.url_file_res,
    t1.descr,
    t1.arc_type,
    t2.name AS region_name,
    t1.url_file_a_content,
    t1.template_id,
    t1.out_template_id
   FROM esn.ved_source_region t1
     LEFT JOIN cabinet.region t2 ON t1.region_id = t2.id;


ALTER TABLE esn.vw_ved_source_region OWNER TO pavlov;
