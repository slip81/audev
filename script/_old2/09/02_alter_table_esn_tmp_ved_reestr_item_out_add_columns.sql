﻿/*
	ALTER TABLE esn.tmp_ved_reestr_item_out ADD COLUMNs
*/

ALTER TABLE esn.tmp_ved_reestr_item_out ADD COLUMN existing_item_id bigint;
ALTER TABLE esn.tmp_ved_reestr_item_out ADD COLUMN mnn_id bigint;
ALTER TABLE esn.tmp_ved_reestr_item_out ADD COLUMN comm_name_id bigint;
ALTER TABLE esn.tmp_ved_reestr_item_out ADD COLUMN producer_id bigint;
ALTER TABLE esn.tmp_ved_reestr_item_out ADD COLUMN existing_item_out_id bigint;