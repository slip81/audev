﻿/*
	insert into esn.ved_template	
*/

-- select * from esn.ved_template 

insert into esn.ved_template (template_name, start_row_num, start_col_num, list_name)
values ('Ш1: госреестр, основные строки', 4, 1, 'лист 1');

insert into esn.ved_template (template_name, start_row_num, start_col_num, list_name)
values ('Ш2: госреестр, исключенные строки', 4, 1, 'иск');

insert into esn.ved_template (template_name, start_row_num, start_col_num, list_name)
values ('Ш3: региональный реестр, основные строки', 9, 1, 'реестр');

insert into esn.ved_template (template_name, start_row_num, start_col_num, list_name)
values ('Ш4: региональный реестр, исключенные строки', 4, 1, 'исключенные');