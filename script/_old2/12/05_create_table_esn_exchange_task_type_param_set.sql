﻿/*
	CREATE TABLE esn.exchange_task_type_param_set
*/

-- DROP TABLE esn.exchange_task_type_param_set;

CREATE TABLE esn.exchange_task_type_param_set
(  
  set_id integer NOT NULL,  
  set_name character varying,  
  CONSTRAINT exchange_task_type_param_set_pkey PRIMARY KEY (set_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE esn.exchange_task_type_param_set OWNER TO pavlov;

INSERT INTO esn.exchange_task_type_param_set (set_id, set_name)
VALUES (1, 'Период выгрузки');

ALTER TABLE esn.exchange_task_type ADD COLUMN param_set_id integer;

ALTER TABLE esn.exchange_task_type
  ADD CONSTRAINT exchange_task_type_param_set_id_fkey FOREIGN KEY (param_set_id)
      REFERENCES esn.exchange_task_type_param_set (set_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE esn.exchange_task_type SET param_set_id = 1 WHERE type_id = 1;

-- select * from esn.exchange_task_type

