﻿/*
	CREATE TABLE esn.exchange_reg_task
*/

-- DROP TABLE esn.exchange_reg_task;

CREATE TABLE esn.exchange_reg_task
(
  task_id bigserial NOT NULL,
  reg_id bigint NOT NULL,
  task_type_id integer NOT NULL,
  param_date_beg date,
  param_date_end date,
  state integer NOT NULL DEFAULT 0,
  state_date timestamp without time zone,
  comment character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT exchange_reg_task_pkey PRIMARY KEY (task_id),
  CONSTRAINT exchange_reg_task_reg_id_fkey FOREIGN KEY (reg_id)
      REFERENCES esn.exchange_reg (reg_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT exchange_reg_task_task_type_id_fkey FOREIGN KEY (task_type_id)
      REFERENCES esn.exchange_task_type (type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_reg_task OWNER TO pavlov;
