﻿/*
	CREATE TABLE esn.exchange_task_type
*/

-- DROP TABLE esn.exchange_task_type;

CREATE TABLE esn.exchange_task_type
(
  type_id integer NOT NULL,
  type_name character varying,
  CONSTRAINT exchange_task_type_pkey PRIMARY KEY (type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_task_type OWNER TO pavlov;
