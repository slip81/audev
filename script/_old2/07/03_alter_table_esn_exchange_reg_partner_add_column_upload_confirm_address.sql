﻿/*
	ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_confirm_address character varying;
*/

ALTER TABLE esn.exchange_reg_partner ADD COLUMN upload_confirm_address character varying;