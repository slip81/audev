﻿/*
	ALTER TABLE esn.exchange_reg_partner ADD COLUMN days_cnt_for_mov integer NOT NULL DEFAULT 1;
*/

ALTER TABLE esn.exchange_reg_partner ADD COLUMN days_cnt_for_mov integer NOT NULL DEFAULT 1;
