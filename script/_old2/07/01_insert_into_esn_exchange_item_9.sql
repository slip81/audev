﻿/*
	insert into esn.exchange_item
*/

insert into esn.exchange_item (item_id, item_name, item_shortname)
values (9, 'Данные накопительным итогом', 'avg');

-- select * from esn.exchange_item

insert into esn.datasource_exchange_item (ds_id, item_id, for_send, for_get)
values (102, 9, true, false);

-- select * from esn.datasource_exchange_item