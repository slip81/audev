﻿/*
	CREATE TABLE esn.ved_reestr_item
*/

-- DROP TABLE esn.ved_reestr_item;

CREATE TABLE esn.ved_reestr_item
(
  item_id bigserial NOT NULL,
  reestr_id bigint NOT NULL,
  item_code bigint,
  item_code2 character varying,
  mnn_id bigint,
  comm_name_id bigint,
  producer_id bigint,
  pack_name character varying,
  pack_count integer,
  limit_price numeric,
  price_for_init_pack boolean NOT NULL DEFAULT false,
  reg_num character varying,
  price_reg_date character varying,
  ean13 character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  state integer,
  CONSTRAINT ved_reestr_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT ved_reestr_item_comm_name_id_fkey FOREIGN KEY (comm_name_id)
      REFERENCES esn.prep_commercial_name (commercial_name_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ved_reestr_item_mnn_id_fkey FOREIGN KEY (mnn_id)
      REFERENCES esn.prep_mnn (mnn_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ved_reestr_item_producer_id_fkey FOREIGN KEY (producer_id)
      REFERENCES esn.prep_producer (producer_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ved_reestr_item_reestr_id_fkey FOREIGN KEY (reestr_id)
      REFERENCES esn.ved_reestr (reestr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_reestr_item OWNER TO pavlov;
