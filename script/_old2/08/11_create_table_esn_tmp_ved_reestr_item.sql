﻿/*
	CREATE TABLE esn.tmp_ved_reestr_item
*/

-- DROP TABLE esn.tmp_ved_reestr_item;

CREATE TABLE esn.tmp_ved_reestr_item
(
  item_id bigserial NOT NULL,  
  item_code bigint,
  item_code2 character varying,
  mnn character varying,
  comm_name character varying,
  producer character varying,
  pack_name character varying,
  pack_count integer,
  limit_price numeric,
  price_for_init_pack boolean NOT NULL DEFAULT false,
  reg_num character varying,
  price_reg_date character varying,
  ean13 character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  state integer,
  CONSTRAINT tmp_ved_reestr_item_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.tmp_ved_reestr_item OWNER TO pavlov;
