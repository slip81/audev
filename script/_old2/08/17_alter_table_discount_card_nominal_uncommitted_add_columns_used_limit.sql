﻿/*
	ALTER TABLE discount.card_nominal_uncommitted ADD COLUMNs
*/

ALTER TABLE discount.card_nominal_uncommitted ADD COLUMN money_used_limit_value numeric;
ALTER TABLE discount.card_nominal_uncommitted ADD COLUMN bonus_used_limit_value numeric;