﻿/*
	CREATE TABLE esn.ved_source
*/

-- DROP TABLE esn.ved_source;

CREATE TABLE esn.ved_source
(
  ved_source_id bigint NOT NULL,
  url_site character varying,
  url_file_main character varying,
  url_file_res character varying,
  descr character varying,
  arc_type integer NOT NULL DEFAULT 0,
  num integer NOT NULL DEFAULT 1,
  CONSTRAINT ved_source_pkey PRIMARY KEY (ved_source_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_source OWNER TO pavlov;
