﻿/*
	CREATE OR REPLACE VIEW esn.vw_ved_source_region
*/

-- DROP VIEW esn.vw_ved_source_region;

CREATE OR REPLACE VIEW esn.vw_ved_source_region AS 
  select 
  t1.item_id,
  t1.ved_source_id,
  t1.region_id,
  t1.url_site,
  t1.url_file_main,
  t1.url_file_res,
  t1.descr,  
  t1.arc_type, 
  t2.name as region_name
  from esn.ved_source_region t1
  left join cabinet.region t2 on t1.region_id = t2.id
  ;

ALTER TABLE esn.vw_ved_source_region OWNER TO pavlov; 