﻿/*
	DROP TABLES esn.ved_xxx
*/

DROP TABLE esn.ved_reestr_item_region;
DROP TABLE esn.ved_reestr_item_out;
DROP TABLE esn.ved_reestr_item;
DROP TABLE esn.tmp_ved_reestr_item;
DROP TABLE esn.ved_reestr;

DROP TABLE esn.ved_source_region;
DROP TABLE esn.ved_source;


DROP VIEW esn.vw_ved_source_region;