﻿/*
	ALTER TABLE discount.card_status ADD COLUMN no_manual_change
*/

ALTER TABLE discount.card_status ADD COLUMN no_manual_change BOOLEAN NOT NULL DEFAULT False;