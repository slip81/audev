﻿/*
	delete from esn
*/

delete from esn.esn_syn;
delete from esn.esn_word;
delete from esn.esn_sprav;
delete from esn.esn_required;
delete from esn.esn_scheme;
delete from esn.esn_scheme;
delete from esn.esn_import_xml_tmp;
delete from esn.esn_import_xml;

delete from esn.prep_batch_unit_similar_tmp;
delete from esn.prep_batch_unit_similar;
delete from esn.prep_batch_unit;
delete from esn.prep_batch_parse;
delete from esn.prep_batch;

delete from esn.esn;

delete from esn.prep_commercial_name_mnn;
delete from esn.prep_commercial_name_syn;
delete from esn.prep_commercial_name;
delete from esn.prep_country_syn;
--delete from esn.prep_country;
delete from esn.prep_dosage_syn;
delete from esn.prep_dosage;
delete from esn.prep_ean13_syn;
delete from esn.prep_ean13;
delete from esn.prep_farm_group_syn;
delete from esn.prep_farm_group;
delete from esn.prep_form_series_syn;
delete from esn.prep_group_name_type;
delete from esn.prep_group_name_syn;
delete from esn.prep_group_name;
delete from esn.prep_merch_type_syn;
delete from esn.prep_merch_type;
delete from esn.prep_mnn_syn;
delete from esn.prep_mnn;
delete from esn.prep_producer_syn;
delete from esn.prep_producer;
delete from esn.prep_quantity_syn;
delete from esn.prep_quantity;
delete from esn.prep_size_syn;
delete from esn.prep_size;
