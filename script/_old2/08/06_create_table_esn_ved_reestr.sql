﻿/*
	CREATE TABLE esn.ved_reestr
*/

-- DROP TABLE esn.ved_reestr;

CREATE TABLE esn.ved_reestr
(
  reestr_id bigserial NOT NULL,
  reestr_name character varying,
  date_beg date,
  date_end date,
  is_closed boolean NOT NULL DEFAULT false,
  source_id bigint,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  parent_id bigint,
  CONSTRAINT ved_reestr_pkey PRIMARY KEY (reestr_id),
  CONSTRAINT ved_reestr_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES esn.ved_reestr (reestr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ved_reestr_source_id_fkey FOREIGN KEY (source_id)
      REFERENCES esn.ved_source (ved_source_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_reestr OWNER TO pavlov;
