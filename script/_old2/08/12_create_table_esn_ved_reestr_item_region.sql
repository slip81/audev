﻿/*
	CREATE TABLE esn.ved_reestr_item_region
*/

-- DROP TABLE esn.ved_reestr_item_region;

CREATE TABLE esn.ved_reestr_item_region
(
  item_id bigserial NOT NULL,
  source_region_id bigint NOT NULL,
  item_code bigint,
  item_code2 character varying,
  mnn character varying,
  comm_name character varying,
  producer character varying,
  pack_name character varying,
  pack_count integer,
  limit_price numeric,
  price_for_init_pack boolean NOT NULL DEFAULT false,
  reg_num character varying,
  price_reg_date character varying,
  ean13 character varying,
  limit_opt_incr numeric,
  limit_rozn_incr numeric,
  limit_rozn_price numeric,
  limit_rozn_price_with_nds numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  state integer,
  gos_reestr_item_id bigint,
  CONSTRAINT ved_reestr_item_region_pkey PRIMARY KEY (item_id),
  CONSTRAINT ved_reestr_item_region_source_region_id_fkey FOREIGN KEY (source_region_id)
      REFERENCES esn.ved_source_region (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_reestr_item_region OWNER TO pavlov;
