﻿/*
	CREATE TABLE esn.ved_source_region
*/

-- DROP TABLE esn.ved_source_region;

CREATE TABLE esn.ved_source_region
(
  item_id bigserial NOT NULL,
  ved_source_id bigint NOT NULL,
  region_id integer NOT NULL,
  url_site character varying,
  url_file_main character varying,
  url_file_res character varying,
  descr character varying,
  arc_type integer NOT NULL DEFAULT 0,
  CONSTRAINT ved_source_region_pkey PRIMARY KEY (item_id),
  CONSTRAINT ved_source_region_region_id_fkey FOREIGN KEY (region_id)
      REFERENCES cabinet.region (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ved_source_region_ved_source_id_fkey FOREIGN KEY (ved_source_id)
      REFERENCES esn.ved_source (ved_source_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.ved_source_region OWNER TO pavlov;
