﻿/*
	ALTER TABLE esn.request ADD COLUMN scope
*/

ALTER TABLE esn.request ADD COLUMN scope integer NOT NULL DEFAULT 3;