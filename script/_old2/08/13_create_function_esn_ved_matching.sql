﻿/*
	CREATE FUNCTION esn.ved_matching
*/

-- DROP FUNCTION esn.ved_matching(integer);

CREATE OR REPLACE FUNCTION esn.ved_matching(region_id integer)
  RETURNS void AS
$BODY$
BEGIN

set search_path to 'esn';

update ved_reestr_item_region ved_r
set gos_reestr_item_id = ved.item_id
-- !!!
-- todo: заменить tmp_ved_reestr_item на ved_reestr_item
--from ved_reestr_item ved
from tmp_ved_reestr_item ved
where ved_r.source_region_id = region_id
and ved_r.ean13 = ved.ean13
;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION esn.ved_matching(integer) OWNER TO pavlov;
