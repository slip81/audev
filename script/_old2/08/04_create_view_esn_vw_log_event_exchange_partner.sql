﻿/*
	CREATE OR REPLACE VIEW esn.vw_log_event_exchange_partner
*/

-- DROP VIEW esn.vw_log_event_exchange_partner;

CREATE OR REPLACE VIEW esn.vw_log_event_exchange_partner AS 
	select t3.client_id, t3.client_name, t2.user_id, date_trunc('minute',t1.date_beg) as date_beg_minutes
	, extract(epoch from date_trunc('minute',t1.date_beg))::bigint as date_id
	, t1.log_id, t1.date_beg, t1.mess, t1.user_name, t1.log_event_type_id, t1.obj_id, t1.scope, t1.date_end, t1.mess2 
	from esn.log_event t1
	inner join esn.exchange_user t2 on t1.user_name = t2.login
	inner join esn.exchange_client t3 on t2.client_id = t3.client_id
	where t1.scope = 4
	;

ALTER TABLE esn.vw_log_event_exchange_partner OWNER TO pavlov;
