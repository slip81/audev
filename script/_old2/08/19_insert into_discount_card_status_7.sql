﻿/*
	insert into discount.card_status
*/

insert into discount.card_status (card_status_name, card_status_group_id, no_manual_change)
values ('Исчерпан лимит', 1, true);

update discount.card_status set no_manual_change = true where card_status_id = 3;

-- select * from discount.card_status