﻿/*
	ALTER TABLE esn.exchange ADD COLUMN sales_prefix bigint
*/

ALTER TABLE esn.exchange ADD COLUMN sales_prefix bigint;