﻿/*
	CREATE TABLE discount.card_additional_num
*/

-- DROP TABLE discount.card_additional_num;

CREATE TABLE discount.card_additional_num
(
  item_id bigserial NOT NULL,
  card_id bigint NOT NULL,
  card_num character varying,
  CONSTRAINT card_additional_num_pkey PRIMARY KEY (item_id),
  CONSTRAINT card_additional_num_card_id_fkey FOREIGN KEY (card_id)
      REFERENCES discount.card (card_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_additional_num  OWNER TO pavlov;