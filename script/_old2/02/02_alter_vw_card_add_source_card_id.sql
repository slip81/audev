﻿/*
	CREATE OR REPLACE VIEW discount.vw_card AS 
*/

-- DROP VIEW discount.vw_card;

CREATE OR REPLACE VIEW discount.vw_card AS 
 SELECT t1.card_id,
    t1.date_beg,
    t1.date_end,
    t1.curr_card_status_id,
    t1.reestr_id,
    t1.business_id,
    t1.curr_trans_count,
    t1.card_num,
    t1.card_num2,
    t1.curr_trans_sum,
    t1.curr_client_id,
    t1.curr_card_type_id,
    t1.mess,
    t2.unit_value AS curr_bonus_value,
    t3.unit_value AS curr_bonus_percent,
    t4.unit_value AS curr_discount_percent,
    t5.unit_value AS curr_money_value,
    t6.unit_value AS curr_discount_value,
    NULL::numeric AS curr_money_percent,
    t7.card_type_name AS curr_card_type_name,
    t8.card_status_name AS curr_card_status_name,
    t9.client_surname AS curr_client_name,
    cast(t1.card_id as character varying) AS card_id_str,
    cast(t1.card_num as character varying) AS card_num_str,
    t7.card_type_group_id,
    t1.source_card_id
   FROM discount.card t1
     LEFT JOIN discount.card_nominal t2 ON t1.card_id = t2.card_id AND t2.unit_type = 1 AND t2.date_end IS NULL
     LEFT JOIN discount.card_nominal t3 ON t1.card_id = t3.card_id AND t3.unit_type = 4 AND t3.date_end IS NULL
     LEFT JOIN discount.card_nominal t4 ON t1.card_id = t4.card_id AND t4.unit_type = 2 AND t4.date_end IS NULL
     LEFT JOIN discount.card_nominal t5 ON t1.card_id = t5.card_id AND t5.unit_type = 3 AND t5.date_end IS NULL
     LEFT JOIN discount.card_nominal t6 ON t1.card_id = t6.card_id AND t6.unit_type = 5 AND t6.date_end IS NULL
     LEFT JOIN discount.card_type t7 ON t1.curr_card_type_id = t7.card_type_id
     LEFT JOIN discount.card_status t8 ON t1.curr_card_status_id = t8.card_status_id
     LEFT JOIN discount.client t9 ON t1.curr_client_id = t9.client_id
     ;

ALTER TABLE discount.vw_card OWNER TO pavlov;
