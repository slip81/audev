﻿/*
	ALTER TABLE discount.card ADD COLUMN source_card_id bigint;
*/

ALTER TABLE discount.card ADD COLUMN source_card_id bigint;
