﻿/*
	ALTER TABLE cabinet.prj_claim ADD COLUMN old_client_id
*/

ALTER TABLE cabinet.prj_claim ADD COLUMN old_client_id integer;

UPDATE cabinet.prj_claim
SET old_client_id = (SELECT x1.client_id FROM bkp.prj_claim x1 WHERE cabinet.prj_claim.claim_id = x1.claim_id)
WHERE old_client_id IS NULL;

-- select * from cabinet.prj_claim order by claim_id desc limit 100;

insert into cabinet.prj_mess (mess, user_id, claim_id, crt_date, crt_user, upd_date, upd_user, upd_num)
select '<u>Клиент:</u> <strong>' || t2.client_name || '</strong>', 1, t1.claim_id, current_timestamp, 'ПМС', current_timestamp, 'ПМС', 0
from cabinet.prj_claim t1
inner join cabinet.crm_client_bkp t2 ON t1.old_client_id = t2.client_id
where t1.old_client_id IS NOT NULL;

