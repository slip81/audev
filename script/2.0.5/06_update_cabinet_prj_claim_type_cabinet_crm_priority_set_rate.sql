﻿/*
	update cabinet.prj_claim_type, cabinet.crm_priority set rate
*/

update cabinet.prj_claim_type
set rate = 0.1
where rate is null and claim_type_id > 0;

update cabinet.crm_priority
set rate = 0.1
where rate is null and priority_id > 0;

-- select * from cabinet.prj_claim_type
-- select * from cabinet.crm_priority


