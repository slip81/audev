﻿/*
	update cabinet.prj_user_settings where page_type_id = 9
*/

update cabinet.prj_user_settings 
set settings = null, settings_grid = null, is_selected = true
where page_type_id = 9;

-- select * from cabinet.prj_user_settings where page_type_id = 9