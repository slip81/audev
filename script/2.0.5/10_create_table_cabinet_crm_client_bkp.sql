﻿/*
	CREATE TABLE cabinet.crm_client_bkp
*/

-- DROP TABLE cabinet.crm_client_bkp;

CREATE TABLE cabinet.crm_client_bkp
(
  client_id integer NOT NULL,
  client_name character varying,
  cab_client_id integer,
  CONSTRAINT crm_client_bkp_pkey PRIMARY KEY (client_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_client_bkp OWNER TO pavlov;


INSERT INTO cabinet.crm_client_bkp (client_id, client_name, cab_client_id)
SELECT client_id, client_name, cab_client_id
FROM cabinet.crm_client;

INSERT INTO cabinet.crm_client (client_id, client_name)
SELECT max(client_id) + 1, 'Клиент'
FROM cabinet.crm_client;

UPDATE cabinet.prj_claim 
SET client_id = (SELECT max(client_id) FROM cabinet.crm_client WHERE client_name = 'Клиент') 
WHERE coalesce(client_id, 0) not in (0, 4);

UPDATE cabinet.crm_task 
SET client_id = (SELECT max(client_id) FROM cabinet.crm_client WHERE client_name = 'Клиент') 
WHERE coalesce(client_id, 0) not in (0, 4);

DELETE FROM cabinet.crm_client
WHERE client_name NOT IN
('[н/о]','Аурит','Клиент');

--SELECT * from cabinet.crm_client order by client_id
--SELECT * from cabinet.crm_client order by client_name
--"Аурит"
--4

--SELECT max(client_id) from cabinet.crm_client
--117

--SELECT * from cabinet.crm_client_bkp order by client_name