﻿/*
	CREATE TABLE cabinet.service_esn2
*/

-- DROP TABLE cabinet.service_esn2;

CREATE TABLE cabinet.service_esn2
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  comp_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),
  CONSTRAINT service_esn2_pkey PRIMARY KEY (item_id),
  CONSTRAINT service_esn2_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_esn2_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_esn2 OWNER TO pavlov;

-- DROP TRIGGER cabinet_service_esn2_set_stamp_trg ON cabinet.service_esn2;

CREATE TRIGGER cabinet_service_esn2_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.service_esn2
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

