﻿/*
	ALTER TABLEs cabinet.prj_claim_type, cabinet.crm_priority ADD COLUMN rate
*/

ALTER TABLE cabinet.prj_claim_type ADD COLUMN rate numeric;
ALTER TABLE cabinet.crm_priority ADD COLUMN rate numeric;

-- select * from cabinet.prj_claim_type
-- select * from cabinet.crm_priority