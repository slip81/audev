﻿/*
	update cabinet.prj_user_settings where page_type_id = 2
*/

update cabinet.prj_user_settings
set settings_grid = replace(settings_grid, '],"pageSize"', ',{"encoded":true,"hidden":true,"title":"Формулировка","width":"300px","field":"claim_text","filterable":{"messages":{"info":"Строки со значениями","isTrue":"истина","isFalse":"ложь","filter":"фильтровать","clear":"очистить фильтр","and":"и","or":"или","selectValue":"-выберите-","operator":"Оператор","value":"Значение","cancel":"Отмена"},"operators":{"string":{"eq":"равно","neq":"не равно","startswith":"начинающимися на","endswith":"оканчивается на","contains":"содержащими","doesnotcontain":"не содержит"},"number":{"eq":"равно","neq":"не равно","gte":"больше или равно","gt":"больше","lte":"меньше или равно","lt":"меньше"},"date":{"eq":"равна","neq":"не равна","gte":"после или равна","gt":"после","lte":"до или равна","lt":"до"},"enums":{"eq":"равно","neq":"не равно"}}},"headerAttributes":{"id":"1b5786e8-6e07-4cb5-a0e9-dc4391ed30e1","style":"display:none"},"attributes":{"style":"display:none"},"footerAttributes":{"style":"display:none"}}],"pageSize"')
where page_type_id = 2;