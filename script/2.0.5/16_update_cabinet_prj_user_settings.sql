﻿/*
	update cabinet.prj_user_settings
*/

update cabinet.prj_user_settings
set settings_grid = replace(settings_grid, '"title":"Название"', '"title":"Название","attributes":{"class":"prj-claim-name"}')
where page_type_id = 2;

update cabinet.prj_user_settings
set settings_grid = replace(settings_grid, 'pull-right', 'prj-claim-mess-info')
where page_type_id = 2;

-- select * from cabinet.prj_user_settings where user_id = 1 and page_type_id = 2