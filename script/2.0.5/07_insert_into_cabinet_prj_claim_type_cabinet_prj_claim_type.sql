﻿/*
	insert into cabinet.prj_claim_type, cabinet.prj_claim_type
*/

insert into cabinet.prj_claim_type (claim_type_id, claim_type_name, rate)
values (3, 'Доработка', 0.1);

insert into cabinet.prj_claim_type (claim_type_id, claim_type_name, rate)
values (4, 'Новый функционал', 0.1);

update cabinet.crm_priority set priority_name = 'Низкий' where priority_id = 1;

update cabinet.crm_priority set priority_name = 'Средний' where priority_id = 2;

insert into cabinet.crm_priority (priority_id, priority_name, ord, is_control, is_boss, rate)
values (3, 'Высокий', 3, false, false, 0.1);

insert into cabinet.crm_priority (priority_id, priority_name, ord, is_control, is_boss, rate)
values (4, 'Наивысший', 4, false, false, 0.1);

update cabinet.prj_claim set priority_id = 3 where priority_id = 1;

--select * from cabinet.prj_claim_type order by claim_type_id
--select * from cabinet.crm_priority order by priority_id

/*
1;"Критическая"
2;"Планово-проектная"
*/