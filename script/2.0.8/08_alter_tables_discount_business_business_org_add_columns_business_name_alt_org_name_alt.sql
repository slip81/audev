﻿/*
	ALTER TABLEs discount.business, business_org ADD COLUMNs business_name_alt, org_name_alt
*/


ALTER TABLE discount.business ADD COLUMN business_name_alt character varying;
ALTER TABLE discount.business_org ADD COLUMN org_name_alt character varying;
