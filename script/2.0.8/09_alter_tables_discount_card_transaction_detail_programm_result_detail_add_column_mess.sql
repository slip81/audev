﻿/*
	ALTER TABLEs discount.card_transaction_detail, programm_result_detail ADD COLUMN mess
*/


ALTER TABLE discount.card_transaction_detail ADD COLUMN mess character varying;
ALTER TABLE discount.programm_result_detail ADD COLUMN mess character varying;