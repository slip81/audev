﻿/*
	update discount.business set business_name_alt = business_name
*/

update discount.business set business_name_alt = business_name where trim(coalesce(business_name_alt, '')) = '';
update discount.business_org set org_name_alt = org_name where trim(coalesce(org_name_alt, '')) = '';