﻿/*
	CREATE OR REPLACE FUNCTION esn.del_old_data()
*/

-- DROP FUNCTION esn.del_old_data();

CREATE OR REPLACE FUNCTION esn.del_old_data()
  RETURNS integer AS
$BODY$
DECLARE
	_exchange_del_interval integer;
	_exchange_log_del_interval integer;
	_discount_log_del_interval integer;
	_cva_data_del_interval integer;
	_sender_log_del_interval integer;
	_cab_log_del_interval integer;
	_stock_del_interval integer;	
	_esn_log_del_interval integer;
	_asna_del_interval integer;
BEGIN
	_exchange_del_interval := 62;
	
	delete from esn.exchange_data t1 using esn.exchange t2
	where t1.exchange_id = t2.exchange_id
	and t2.crt_date <= (current_date - _exchange_del_interval);

	_exchange_log_del_interval := 62;

	-- delete from esn.log_esn where scope = 4 and date_beg <= (current_date - _exchange_log_del_interval);
	delete from esn.exchange_log where date_beg <= (current_date - _exchange_log_del_interval);
	delete from esn.exchange_file_log where crt_date <= (current_date - _exchange_log_del_interval);

	_discount_log_del_interval := 93;

	delete from discount.log_event where scope = 1 and date_beg <= (current_date - _discount_log_del_interval);	

	_cva_data_del_interval := 62;

	delete from cabinet.service_cva_data_ext t1 using cabinet.service_cva_data t2
	where t1.item_id = t2.item_id
	and t2.batch_date <= (current_date - _cva_data_del_interval);

	_sender_log_del_interval := 62;

	delete from cabinet.service_sender_log where date_beg <= (current_date - _sender_log_del_interval);

	_cab_log_del_interval := 186;

	delete from cabinet.log_cab where date_beg <= (current_date - _cab_log_del_interval);

	_stock_del_interval := 1;	

	delete from und.stock_row where state = 1 and upd_date <= (current_date - _stock_del_interval);	

	_asna_del_interval := 0;

	delete from und.asna_stock_row where crt_date <= (current_date - _asna_del_interval);

	_esn_log_del_interval := 93;

	delete from esn.log_esn_data t1 using esn.log_esn t2
	where t1.log_id = t2.log_id
	and t2.date_beg <= (current_date - _esn_log_del_interval);
	
	delete from esn.log_esn where date_beg <= (current_date - _esn_log_del_interval);

	return 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION esn.del_old_data() OWNER TO pavlov;
