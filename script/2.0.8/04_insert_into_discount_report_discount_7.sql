﻿/*
	insert into discount.report_discount 7
*/

insert into discount.report_discount (report_id, report_name, report_descr)
values (7, 'АСНА: отчет по дисконтам', 'Отчет формирует список проданных позиций по дисконтным акциям АСНА, с указанием наименований и адресов аптек и юридических лиц.');

-- select * from discount.report_discount order by report_id