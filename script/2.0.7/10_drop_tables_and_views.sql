﻿/*
	DROP TABLES and VIEWS
*/

DROP VIEW cabinet.vw_crm_event_overdue_dates;
DROP VIEW cabinet.vw_crm_event;
DROP VIEW cabinet.vw_crm_task;
DROP VIEW cabinet.vw_crm_task_arc;
DROP VIEW cabinet.vw_crm_task_curr_exec;
DROP VIEW cabinet.vw_crm_task_filter;
DROP VIEW cabinet.vw_crm_task_note;
DROP VIEW cabinet.vw_crm_task_rel;
DROP VIEW cabinet.vw_crm_state;
DROP VIEW cabinet.vw_log_crm;

DROP VIEW cabinet.vw_planner_event;
DROP VIEW cabinet.vw_planner_note;

DROP VIEW cabinet.vw_prj_plan;
DROP VIEW cabinet.vw_prj_event_notify;

-------------------------------

DROP TABLE cabinet.planner_task_event;
DROP TABLE cabinet.planner_event;
DROP TABLE cabinet.planner_event_priority;
DROP TABLE cabinet.planner_note;

DROP TABLE cabinet.crm_event;
DROP TABLE cabinet.crm_project_user;
DROP TABLE cabinet.crm_state_user_settings;

DROP TABLE cabinet.crm_task_note_file;
DROP TABLE cabinet.crm_task_note;
ALTER TABLE cabinet.crm_task_file DROP CONSTRAINT crm_task_file_subtask_id_fkey;
DROP TABLE cabinet.crm_subtask;
DROP TABLE cabinet.crm_task_file;

ALTER TABLE cabinet.story DROP CONSTRAINT story_task_id_fkey;
DROP TABLE cabinet.crm_group_operation;
DROP TABLE cabinet.crm_user_task_follow;
DROP TABLE cabinet.crm_task_control;
DROP TABLE cabinet.crm_task_rel;
DROP TABLE cabinet.crm_task_user_filter;
DROP TABLE cabinet.crm_task;
DROP TABLE cabinet.crm_task_operation;
DROP TABLE cabinet.log_crm;

ALTER TABLE cabinet.prj_user_group_item DROP CONSTRAINT prj_user_group_item_event_id_fkey;
ALTER TABLE cabinet.prj_notify DROP CONSTRAINT prj_notify_event_id_fkey;
DROP TABLE cabinet.prj_event_party;
DROP TABLE cabinet.prj_event;
DROP TABLE cabinet.prj_set_approved;
DROP TABLE cabinet.prj_set_new;
DROP TABLE cabinet.prj_work_exec;
