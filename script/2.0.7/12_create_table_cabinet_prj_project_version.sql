﻿/*
	CREATE TABLE cabinet.prj_project_version
*/

-- DROP TABLE cabinet.prj_version_type;

CREATE TABLE cabinet.prj_version_type
(
  version_type_id integer NOT NULL,
  version_type_name character varying,
  CONSTRAINT prj_version_type_pkey PRIMARY KEY (version_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_version_type OWNER TO pavlov;

INSERT INTO cabinet.prj_version_type (version_type_id, version_type_name)
VALUES (0, '[не рассмотрено]');

INSERT INTO cabinet.prj_version_type (version_type_id, version_type_name)
VALUES (1, 'Старые');

INSERT INTO cabinet.prj_version_type (version_type_id, version_type_name)
VALUES (2, 'Текущая');

INSERT INTO cabinet.prj_version_type (version_type_id, version_type_name)
VALUES (3, 'Следующая');

INSERT INTO cabinet.prj_version_type (version_type_id, version_type_name)
VALUES (4, 'Следующая+1');

INSERT INTO cabinet.prj_version_type (version_type_id, version_type_name)
VALUES (5, 'Будущие');

-- DROP TABLE cabinet.prj_project_version;

CREATE TABLE cabinet.prj_project_version
(
  version_id serial NOT NULL,
  project_id integer NOT NULL,
  version_type_id integer NOT NULL,
  version_name character varying,
  version_value character varying,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),
  CONSTRAINT prj_project_version_pkey PRIMARY KEY (version_id),  
  CONSTRAINT prj_project_version_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_project_version_version_type_id_fkey FOREIGN KEY (version_type_id)
      REFERENCES cabinet.prj_version_type (version_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_project_version OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_project_version_set_stamp_trg ON cabinet.prj_project_version;

CREATE TRIGGER cabinet_prj_project_version_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_project_version
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_project_version (
  project_id,
  version_type_id,
  version_name,
  version_value,  
  crt_date,
  crt_user,
  upd_date,
  upd_user
)
SELECT 
  t1.project_id,
  t2.version_type_id,
  t2.version_type_name,
  '-',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov'
FROM cabinet.crm_project t1
INNER JOIN cabinet.prj_version_type t2 ON 1=1 AND t2.version_type_id = 0
WHERE t1.project_id = 0;

INSERT INTO cabinet.prj_project_version (
  project_id,
  version_type_id,
  version_name,
  version_value,  
  crt_date,
  crt_user,
  upd_date,
  upd_user
)
SELECT 
  t1.project_id,
  t2.version_type_id,
  t2.version_type_name,
  '-',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov'
FROM cabinet.crm_project t1
INNER JOIN cabinet.prj_version_type t2 ON 1=1
WHERE t1.project_id > 0;

-- select * from cabinet.prj_version_type order by version_type_id
-- select * from cabinet.prj_project_version order by project_id, version_type_id