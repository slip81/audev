﻿/*
	CREATE OR REPLACE FUNCTION cabinet.del_client
*/

-- DROP FUNCTION cabinet.del_client(integer, text);

CREATE OR REPLACE FUNCTION cabinet.del_client(
    _client_id integer,
    _schema_name text DEFAULT 'cabinet'::text)
  RETURNS integer AS
$BODY$
DECLARE
 _exists boolean;
 _result integer;
BEGIN
  _result := 0;
  _exists := false;

  --SET search_path TO _schema_name;
  -- SET search_path TO cabinet;
  EXECUTE 'SET search_path TO ' || _schema_name;

  _exists := (SELECT EXISTS (
	SELECT 1
	FROM   information_schema.tables 
	WHERE  table_schema = _schema_name
	AND    table_name = 'client_user_widget'
  ));

  if (_exists = true) then

    delete from client_user_widget t1 using client_user t2
    where t1.user_id = t2.user_id
    and t2.client_id = _client_id;

    delete from client_user where client_id = _client_id;
    
  end if;

  delete from license t1 using order_item t2, "order" t3
  where t1.order_item_id = t2.id
  and t2.order_id = t3.id
  and t3.client_id = _client_id;

  delete from order_item t1 using "order" t2
  where t1.order_id = t2.id
  and t2.client_id = _client_id;

  delete from "order" where client_id = _client_id;

  _exists := (SELECT EXISTS (
	SELECT 1
	FROM   information_schema.tables 
	WHERE  table_schema = _schema_name
	AND    table_name = 'service_cva_data_ext'
  ));

  if (_exists = true) then

    delete from service_cva_data_ext t1 using service_cva_data t2, workplace t3, sales t4
    where t1.item_id = t2.item_id
    and t2.workplace_id = t3.id
    and t3.sales_id = t4.id
    and t4.client_id = _client_id;

    delete from service_cva_data t1 using workplace t3, sales t4
    where t1.workplace_id = t3.id
    and t3.sales_id = t4.id
    and t4.client_id = _client_id;

    delete from service_cva t1 using workplace t3, sales t4
    where t1.workplace_id = t3.id
    and t3.sales_id = t4.id
    and t4.client_id = _client_id;

    delete from service_defect t1 using workplace t3, sales t4
    where t1.workplace_id = t3.id
    and t3.sales_id = t4.id
    and t4.client_id = _client_id;

    delete from service_sender_log t1 using workplace t3, sales t4
    where t1.workplace_id = t3.id
    and t3.sales_id = t4.id
    and t4.client_id = _client_id;

    delete from service_sender t1 using workplace t3, sales t4
    where t1.workplace_id = t3.id
    and t3.sales_id = t4.id
    and t4.client_id = _client_id;

  end if;

    _exists := (SELECT EXISTS (
	SELECT 1
	FROM   information_schema.tables 
	WHERE  table_schema = _schema_name
	AND    table_name = 'client_service_param'
  ));

  if (_exists = true) then

    delete from client_service_param where client_id = _client_id;

    delete from client_service where client_id = _client_id;

  end if;

  delete from sales where client_id = _client_id;

  _exists := (SELECT EXISTS (
	SELECT 1
	FROM   information_schema.tables 
	WHERE  table_schema = _schema_name
	AND    table_name = 'client_services'
  ));

  if (_exists = true) then

    delete from client_services where client__id = _client_id;    

  end if;

  delete from client where id = _client_id;    
  
  return _result;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION cabinet.del_client(integer, text) OWNER TO pavlov;
