﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_user_group_item
*/

-- DROP VIEW cabinet.vw_prj_user_group_item;

CREATE OR REPLACE VIEW cabinet.vw_prj_user_group_item AS 
 SELECT t1.item_id,
    t1.group_id,
    t1.item_type_id,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_id
            WHEN 4 THEN t14.claim_id
            ELSE NULL::integer
        END AS claim_id,
    t1.event_id,
    t1.note_id,
    t1.ord,
    t1.task_id,
    t2.user_id,
    t2.group_name,
    t2.fixed_kind,
    t3.item_type_name,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_num            
            WHEN 3 THEN t13.note_num::character varying
            WHEN 4 THEN ((t14.claim_num::text || '/'::text) || t14.task_num::character varying::text)::character varying
            ELSE NULL::character varying
        END AS item_num,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_name            
            WHEN 3 THEN t13.note_name
            WHEN 4 THEN t14.task_text
            ELSE NULL::character varying
        END AS item_name,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_text            
            WHEN 3 THEN t13.note_text
            WHEN 4 THEN t14.claim_text
            ELSE NULL::character varying
        END AS item_text,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_state_type_id            
            WHEN 4 THEN t14.task_state_type_id
            ELSE NULL::integer
        END AS item_state_type_id,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_state_type_name            
            WHEN 4 THEN t14.task_state_type_name
            ELSE NULL::character varying
        END AS item_state_type_name,
        NULL::timestamp without time zone AS item_date_beg,
        NULL::timestamp without time zone AS item_date_end,
    t1.ord_prev,
    t1.group_id_prev
   FROM cabinet.prj_user_group_item t1
     JOIN cabinet.prj_user_group t2 ON t1.group_id = t2.group_id
     JOIN cabinet.prj_user_group_item_type t3 ON t1.item_type_id = t3.item_type_id
     LEFT JOIN cabinet.vw_prj_claim t11 ON t1.claim_id = t11.claim_id     
     LEFT JOIN cabinet.vw_prj_note t13 ON t1.note_id = t13.note_id
     LEFT JOIN cabinet.vw_prj_task t14 ON t1.task_id = t14.task_id;

ALTER TABLE cabinet.vw_prj_user_group_item OWNER TO pavlov;
