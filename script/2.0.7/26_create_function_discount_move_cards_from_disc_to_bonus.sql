﻿/*
	CREATE OR REPLACE FUNCTION discount.move_cards_from_disc_to_bonus
*/

-- DROP FUNCTION discount.move_cards_from_disc_to_bonus(bigint, bigint);

CREATE OR REPLACE FUNCTION discount.move_cards_from_disc_to_bonus(
    IN _source_card_type_id bigint,
    IN _target_card_type_id bigint,
    OUT result integer)
  RETURNS integer AS
$BODY$
DECLARE
  _curr_card_type_id bigint;
BEGIN
	result := 0;

	UPDATE discount.card_card_type_rel
	SET date_end = current_date
	WHERE card_type_id = _source_card_type_id AND date_end IS NULL;
			
	INSERT INTO discount.card_card_type_rel (card_id, card_type_id, date_beg, ord)
	SELECT card_id, _target_card_type_id, current_date, 1
	FROM discount.card
	WHERE curr_card_type_id = _source_card_type_id;

	INSERT INTO discount.card_nominal (card_id, date_beg, date_end, unit_type, unit_value, crt_date, crt_user)
	SELECT t1.card_id, current_date, null, 4, 3, current_timestamp, 'pavlov'
	FROM discount.card t1
	WHERE t1.curr_card_type_id = _source_card_type_id;
	
	UPDATE discount.card_nominal t1
	SET date_end = current_date
	FROM discount.card t2
	WHERE t1.card_id = t2.card_id
	AND t2.curr_card_type_id = _source_card_type_id	
	AND t1.unit_type = 2
	AND t1.date_end IS NOT NULL;

	UPDATE discount.card
	SET curr_card_type_id = _target_card_type_id
	WHERE curr_card_type_id = _source_card_type_id;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.move_cards_from_disc_to_bonus(bigint, bigint) OWNER TO pavlov;
