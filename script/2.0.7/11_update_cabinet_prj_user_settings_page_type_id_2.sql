﻿/*
	update cabinet.prj_user_settings page_type_id = 2
*/

update cabinet.prj_user_settings
set settings_grid = replace(settings_grid, '"prj_name"', '"prj_name_list"')
where page_type_id = 2;

update cabinet.prj_user_settings
set settings_grid = replace(settings_grid, '"Группа"', '"Группы"')
where page_type_id = 2;