﻿/*
	ALTER TABLE cabinet.prj_claim ADD COLUMNs version_xxx
*/

ALTER TABLE cabinet.prj_claim ADD COLUMN version_id integer;
ALTER TABLE cabinet.prj_claim ADD COLUMN version_value character varying;
ALTER TABLE cabinet.prj_claim ADD COLUMN is_version_fixed boolean NOT NULL DEFAULT false;

UPDATE cabinet.prj_claim
SET version_id = (SELECT max(x1.version_id) FROM cabinet.prj_project_version x1 WHERE cabinet.prj_claim.project_id = x1.project_id AND x1.version_type_id = 1)
WHERE version_id IS NULL;

UPDATE cabinet.prj_claim
SET version_id = (SELECT max(x1.version_id) FROM cabinet.prj_project_version x1 WHERE cabinet.prj_claim.project_id = x1.project_id AND x1.version_type_id = 0)
WHERE version_id IS NULL;

UPDATE cabinet.prj_claim
SET is_version_fixed = true;

UPDATE cabinet.prj_claim
SET repair_version_id = module_version_id
WHERE coalesce(repair_version_id, 0) <= 0;

UPDATE cabinet.prj_claim t1
SET version_value = t2.module_version_name
FROM cabinet.crm_module_version t2
WHERE t1.repair_version_id = t2.module_version_id
AND t1.version_id IS NOT NULL;

ALTER TABLE cabinet.prj_claim
  ADD CONSTRAINT prj_claim_version_id_fkey FOREIGN KEY (version_id)
      REFERENCES cabinet.prj_project_version (version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cabinet.prj_claim ALTER COLUMN version_id SET NOT NULL;

-- select * from cabinet.prj_claim where version_id is null
-- select * from cabinet.prj_claim order by claim_id desc limit 100