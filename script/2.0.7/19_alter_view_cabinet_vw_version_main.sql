﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_version_main
*/

DROP VIEW cabinet.vw_version_main;

CREATE OR REPLACE VIEW cabinet.vw_version_main AS 
 SELECT row_number() OVER (ORDER BY x1.version_main_name) AS version_main_id, 
    x1.version_main_name,
    null::integer as project_id,
    1::integer as version_type
   FROM ( SELECT DISTINCT substr(t1.name::text, 1, 3) AS version_main_name
           FROM cabinet.version t1
          WHERE t1.service_id = 1) x1

UNION ALL

SELECT version_id + 1000 as version_main_id, version_name as version_main_name, project_id, 2::integer as version_type
FROM cabinet.vw_prj_project_version;

ALTER TABLE cabinet.vw_version_main OWNER TO pavlov;
