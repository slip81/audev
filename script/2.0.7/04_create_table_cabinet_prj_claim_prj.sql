﻿/*
	CREATE TABLE cabinet.prj_claim_prj
*/

-- DROP TABLE cabinet.prj_claim_prj;

CREATE TABLE cabinet.prj_claim_prj
(
  item_id serial NOT NULL,
  claim_id integer NOT NULL,
  prj_id integer NOT NULL,
  CONSTRAINT prj_claim_prj_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_claim_prj_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_prj_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_prj OWNER TO pavlov;

INSERT INTO cabinet.prj_claim_prj (claim_id, prj_id)
SELECT claim_id, prj_id
FROM cabinet.prj_claim;

-- select * from cabinet.prj_claim_prj order by item_id desc limit 100