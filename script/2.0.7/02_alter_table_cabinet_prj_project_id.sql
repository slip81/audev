﻿/*
	ALTER TABLE cabinet.prj ADD COLUMN project_id
*/

-- ALTER TABLE cabinet.prj DROP COLUMN project_id;

ALTER TABLE cabinet.prj ADD COLUMN project_id integer;

-- ALTER TABLE cabinet.prj_event DROP CONSTRAINT prj_event_notify_type_id_fkey;

ALTER TABLE cabinet.prj
  ADD CONSTRAINT prj_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
