﻿/*
	update cabinet.prj_xxx
*/

update cabinet.prj_version_type
set version_type_name = 'Заявка'
where version_type_id = 0;

update cabinet.prj_version_type
set version_type_name = 'Старые'
where version_type_id = 1;

update cabinet.prj_version_type
set version_type_name = 'Выпущенная'
where version_type_id = 2;

update cabinet.prj_version_type
set version_type_name = 'Текущая'
where version_type_id = 3;

update cabinet.prj_version_type
set version_type_name = 'Следующая'
where version_type_id = 4;

update cabinet.prj_version_type
set version_type_name = 'Будущие'
where version_type_id = 5;

----------------------------------------

update cabinet.prj_project_version
set version_name = 'Заявка'
where version_type_id = 0;

update cabinet.prj_project_version
set version_name = 'Старые'
where version_type_id = 1;

update cabinet.prj_project_version
set version_name = 'Выпущенная'
where version_type_id = 2;

update cabinet.prj_project_version
set version_name = 'Текущая'
where version_type_id = 3;

update cabinet.prj_project_version
set version_name = 'Следующая'
where version_type_id = 4;

update cabinet.prj_project_version
set version_name = 'Будущие'
where version_type_id = 5;

----------------------------------------

update cabinet.prj_project_version
set version_value = '3.8.1'
where version_type_id = 2 and project_id = 1;

update cabinet.prj_project_version
set version_value = '3.9.0'
where version_type_id = 3 and project_id = 1;

update cabinet.prj_project_version
set version_value = '4.0.0'
where version_type_id = 4 and project_id = 1;

----------------------------------------

update cabinet.prj_claim t1
set version_id = (select max(x1.version_id) from cabinet.prj_project_version x1 where t1.project_id = x1.project_id and x1.version_type_id = 4)
, version_value = null, is_version_fixed = false
from cabinet.vw_prj_claim t2
where t1.claim_id = t2.claim_id
and t2.is_active_state = true
and t1.is_version_fixed = true
and ((t1.version_value like '3.8%') or (t1.version_value like '3.9%') or (t1.version_value like '4.0%'));

-- select * from cabinet.vw_prj_claim order by claim_id desc limit 100;
-- select * from cabinet.prj_version_type order by version_type_id
-- select * from cabinet.prj_project_version
-- select * from cabinet.vw_prj_project_version
