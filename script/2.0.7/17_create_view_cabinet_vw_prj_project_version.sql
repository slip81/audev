﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_project_version
*/

-- DROP VIEW cabinet.vw_prj_project_version;

CREATE OR REPLACE VIEW cabinet.vw_prj_project_version AS 
 SELECT t1.version_id,
  t1.project_id,
  t1.version_type_id,
  t1.version_name,
  t1.version_value,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t2.project_name,
  t3.version_type_name
   FROM cabinet.prj_project_version t1
   INNER JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
   INNER JOIN cabinet.prj_version_type t3 ON t1.version_type_id = t3.version_type_id;

ALTER TABLE cabinet.vw_prj_project_version OWNER TO pavlov;
