﻿/*
	CREATE INDEXes und_xxx
*/

CREATE INDEX und_stock_row_state1_idx
  ON und.stock_row  
  (state)
  WHERE state != 1;

CREATE INDEX und_asna_stock_row_asna_sku1_idx
  ON und.asna_stock_row  
  USING btree
  (asna_sku);

CREATE INDEX und_asna_link_row_asna_sku1_idx
  ON und.asna_link_row
  USING btree
  (asna_sku);

CREATE INDEX und_asna_stock_row_actual_asna_sku1_idx
  ON und.asna_stock_row_actual
  USING btree
  (asna_sku);

-- delete from und.asna_stock_row where crt_date < '2018-01-31 00:00:00';

-- VACUUM ANALYZE und.asna_stock_row;
 