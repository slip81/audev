﻿/*
	insert into cabinet.crm_module_part, crm_module_version
*/

insert into cabinet.crm_module_part (module_part_id, module_part_name, module_id)
select ((select max(module_part_id) + 1 from cabinet.crm_module_part) + row_number() over (order by t1.module_id)), '[н/о]', t1.module_id
from cabinet.crm_module t1 where not exists (select x1.module_part_id from cabinet.crm_module_part x1 where x1.module_id = t1.module_id);

insert into cabinet.crm_module_version (module_version_id, module_version_name, module_id)
select ((select max(module_version_id) + 1 from cabinet.crm_module_version) + row_number() over (order by t1.module_id)), '[н/о]', t1.module_id
from cabinet.crm_module t1 where not exists (select x1.module_version_id from cabinet.crm_module_version x1 where x1.module_id = t1.module_id);