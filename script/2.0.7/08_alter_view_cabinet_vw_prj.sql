﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj
*/

-- DROP VIEW cabinet.vw_prj;

CREATE OR REPLACE VIEW cabinet.vw_prj AS 
 SELECT t1.prj_id,
    t1.prj_name,
    t1.date_beg,
    t1.date_end,
    t1.curr_state_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    COALESCE(t11.claim_cnt, 0::bigint) AS claim_cnt,
    0::bigint AS new_claim_cnt,
    0::bigint AS active_claim_cnt,
    0::bigint AS done_claim_cnt,
    0::bigint AS canceled_claim_cnt,
    t12.state_type_id,
    t13.state_type_name,
    t13.templ AS state_type_name_templ,
    0::bigint AS claim_approved_cnt,
    0::bigint AS claim_new_cnt,
    t1.is_archive,
    t1.project_id,
    t16.project_name
   FROM cabinet.prj t1
     LEFT JOIN ( SELECT x1.prj_id,
            count(x1.claim_id) AS claim_cnt
           FROM cabinet.prj_claim_prj x1
          GROUP BY x1.prj_id) t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_state t12 ON t1.curr_state_id = t12.state_id
     LEFT JOIN cabinet.prj_state_type t13 ON t12.state_type_id = t13.state_type_id
     LEFT JOIN cabinet.crm_project t16 ON t1.project_id = t16.project_id;

ALTER TABLE cabinet.vw_prj OWNER TO pavlov;
