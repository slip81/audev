﻿/*
	UPDATE cabinet.crm_module_version
*/

UPDATE cabinet.crm_module_version
SET module_version_name = regexp_replace(module_version_name, '[ \s]*', '', 'g');

UPDATE cabinet.crm_module
SET module_name = regexp_replace(module_name, '[ \s]*', '', 'g');

UPDATE cabinet.prj_claim
SET version_value = regexp_replace(version_value, '[ \s]*', '', 'g');