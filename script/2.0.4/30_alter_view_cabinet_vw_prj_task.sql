﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_task
*/

-- DROP VIEW cabinet.vw_prj_task;

CREATE OR REPLACE VIEW cabinet.vw_prj_task AS 
 SELECT t1.task_id,
    t1.claim_id,
    t1.task_num,
    t1.task_text,
    t1.date_plan,
    t1.date_fact,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.claim_num,
    t2.claim_name,
    t2.claim_text,
    t2.prj_id AS claim_prj_id,
    t2.project_id AS claim_project_id,
    t2.module_id AS claim_module_id,
    t2.module_part_id AS claim_module_part_id,
    t2.module_version_id AS claim_module_version_id,
    t2.client_id AS claim_client_id,
    t2.priority_id AS claim_priority_id,
    t2.resp_user_id AS claim_resp_user_id,
    t2.date_plan AS claim_date_plan,
    t2.date_fact AS claim_date_fact,
    t2.repair_version_id AS claim_repair_version_id,
    t2.is_archive AS claim_is_archive,
    t2.is_control AS claim_is_control,
    t2.project_name AS claim_project_name,
    t2.module_name AS claim_module_name,
    t2.module_part_name AS claim_module_part_name,
    t2.module_version_name AS claim_module_version_name,
    t2.client_name AS claim_client_name,
    t2.priority_name AS claim_priority_name,
    t2.resp_user_name AS claim_resp_user_name,
    t2.repair_version_name AS claim_repair_version_name,
    t2.claim_state_type_id,
    t2.claim_state_type_name,
    t2.claim_state_type_templ,
    t2.prj_name,
    t2.prj_date_beg,
    t2.prj_date_end,
    t2.prj_curr_state_id,
    t2.prj_state_type_id,
    t2.prj_state_type_name,
    t11.task_state_type_id,
    COALESCE(t11.work_cnt, 0::bigint) AS work_cnt,
    COALESCE(t11.active_work_cnt, 0::bigint) AS active_work_cnt,
    COALESCE(t11.done_work_cnt, 0::bigint) AS done_work_cnt,
    COALESCE(t11.canceled_work_cnt, 0::bigint) AS canceled_work_cnt,
    t12.state_type_name AS task_state_type_name,
    t12.templ AS task_state_type_templ,
    t1.old_state_id,
    t13.state_name AS old_state_name,
    t11.active_work_type_id,
    t14.work_type_name AS active_work_type_name,
    false AS is_overdue,
    t2.claim_type_id,
    t2.claim_stage_id,
    t2.claim_type_name,
    t2.claim_stage_name,
    t15.task_mess_cnt
   FROM cabinet.prj_task t1
     JOIN cabinet.vw_prj_claim t2 ON t1.claim_id = t2.claim_id
     LEFT JOIN cabinet.vw_prj_task_simple t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.prj_task_state_type t12 ON t11.task_state_type_id = t12.state_type_id
     LEFT JOIN cabinet.crm_state t13 ON t1.old_state_id = t13.state_id
     LEFT JOIN cabinet.prj_work_type t14 ON t11.active_work_type_id = t14.work_type_id
     LEFT JOIN ( SELECT x1.task_id, COUNT(x1.mess_id) AS task_mess_cnt FROM cabinet.prj_mess x1 GROUP BY x1.task_id) t15 ON t15.task_id = t1.task_id;

ALTER TABLE cabinet.vw_prj_task OWNER TO pavlov;
