﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_task_user_state
*/

-- DROP VIEW cabinet.vw_prj_task_user_state;

CREATE OR REPLACE VIEW cabinet.vw_prj_task_user_state AS 
 SELECT t1.item_id,
  t1.user_id,
  t1.task_id,
  t1.state_type_id,
  t1.from_user_id,
  t1.is_current,
  t1.mess,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t2.user_name,
  t3.task_num,
  t3.claim_id,
  t4.state_type_name,
  t11.user_name as from_user_name
   FROM cabinet.prj_task_user_state t1
   INNER JOIN cabinet.cab_user t2 ON t1.user_id = t2.user_id
   INNER JOIN cabinet.prj_task t3 ON t1.task_id = t3.task_id
   INNER JOIN cabinet.prj_task_user_state_type t4 ON t1.state_type_id = t4.state_type_id
   LEFT JOIN cabinet.cab_user t11 ON t1.from_user_id = t11.user_id
   ;

ALTER TABLE cabinet.vw_prj_task_user_state OWNER TO pavlov;

