﻿/*
	CREATE TABLE cabinet.prj_task_user_state_type
*/

-- DROP TABLE cabinet.prj_task_user_state_type;

CREATE TABLE cabinet.prj_task_user_state_type
(
  state_type_id integer NOT NULL,
  state_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),
  CONSTRAINT prj_task_user_state_type_pkey PRIMARY KEY (state_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_task_user_state_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_task_user_state_type_set_stamp_trg ON cabinet.prj_task_user_state_type;

CREATE TRIGGER cabinet_prj_task_user_state_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_task_user_state_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_task_user_state_type (state_type_id, state_type_name)
VALUES (1, 'Передано в работу');

INSERT INTO cabinet.prj_task_user_state_type (state_type_id, state_type_name)
VALUES (2, 'Принято в работу');

INSERT INTO cabinet.prj_task_user_state_type (state_type_id, state_type_name)
VALUES (3, 'Выполнено');

INSERT INTO cabinet.prj_task_user_state_type (state_type_id, state_type_name)
VALUES (4, 'Отменено');


-- select * from cabinet.prj_task_user_state_type order by state_type_id