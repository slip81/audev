﻿/*
	update cabinet.prj_user_settings where page_type_id = 2
*/

update cabinet.prj_user_settings
set settings_grid = replace(settings_grid, '<span#if (is_overdue) {# class=''text-danger''#}#>#:claim_num#</span>', '<span#if (is_overdue) {# class=''text-danger''#}#>#:claim_num##if (mess_cnt) {#<span class=''pull-right'' title=''Комментарии''><button type=''button'' class=''btn btn-default btn-xs'' style=''font-size: 0.65em;'' onclick=''prjClaimListPartial.prjClaimGridMessCntClick(#:claim_id#)''>#:mess_cnt#</button></span>#}#</span>')
where page_type_id = 2;

-- select * from cabinet.prj_user_settings where user_id = 1 order by page_type_id

/*
update cabinet.prj_user_settings
set settings_grid = replace(settings_grid, '#if (is_overdue) {# class=''text-danger''#}#>#:claim_num##if (mess_cnt) {#<span class=''pull-right'' title=''Комментарии''><button type=''button'' class=''btn btn-default btn-xs'' style=''font-size: 0.65em;'' onclick=''prjClaimListPartial.prjClaimGridMessCntClick(#:claim_id#)''>#:mess_cnt#</button></span>#}#</span>', '<span#if (is_overdue) {# class=''text-danger''#}#>#:claim_num#</span>')
where page_type_id = 2;
*/
