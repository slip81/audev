﻿/*
	DROP TABLE cabinet.prj_sys_group_type
*/

DELETE FROM cabinet.prj_user_group_item;
DELETE FROM cabinet.prj_user_group;

ALTER TABLE cabinet.prj_user_group DROP COLUMN sys_group_type_id;

DROP TABLE cabinet.prj_sys_group_type;

