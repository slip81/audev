﻿/*
	update cabinet.prj_task_user_state_type set state_type_name
*/

update cabinet.prj_task_user_state_type
set state_type_name = '1.Принято в работу'
where state_type_id = 2;

update cabinet.prj_task_user_state_type
set state_type_name = '2.Передано в работу'
where state_type_id = 1;

update cabinet.prj_task_user_state_type
set state_type_name = '3.Отменено'
where state_type_id = 4;

update cabinet.prj_task_user_state_type
set state_type_name = '4.Выполнено'
where state_type_id = 3;

-- select * from cabinet.prj_task_user_state_type order by state_type_name desc
-- select * from cabinet.prj_task_user_state_type order by state_type_name