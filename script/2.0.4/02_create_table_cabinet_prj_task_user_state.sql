﻿/*
	CREATE TABLE cabinet.prj_task_user_state
*/

-- DROP TABLE cabinet.prj_task_user_state;

CREATE TABLE cabinet.prj_task_user_state
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  task_id integer NOT NULL,
  state_type_id integer NOT NULL,
  from_user_id integer,
  is_current boolean NOT NULL DEFAULT false,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),  
  CONSTRAINT prj_task_user_state_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_task_user_state_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_task_user_state_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.prj_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,   
  CONSTRAINT prj_task_user_state_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES cabinet.prj_task_user_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,          
  CONSTRAINT prj_task_user_state_from_user_id_fkey FOREIGN KEY (from_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_task_user_state OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_task_user_state_set_stamp_trg ON cabinet.prj_task_user_state;

CREATE TRIGGER cabinet_prj_task_user_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_task_user_state
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();