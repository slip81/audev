﻿/*
	CREATE OR REPLACE FUNCTION cabinet.prj_task_user_state_update_current
*/

-- DROP FUNCTION cabinet.prj_task_user_state_update_current();

CREATE OR REPLACE FUNCTION cabinet.prj_task_user_state_update_current()
  RETURNS trigger AS
$BODY$
BEGIN   
    IF TG_OP = 'INSERT' THEN
        UPDATE cabinet.prj_task_user_state
        SET is_current = false
        WHERE task_id = NEW.task_id AND user_id = NEW.user_id AND item_id != NEW.item_id;
        RETURN NEW;   
    END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.prj_task_user_state_update_current() OWNER TO pavlov;
