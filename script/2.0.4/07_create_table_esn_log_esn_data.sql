﻿/*
	CREATE TABLE esn.log_esn_data
*/

-- DROP TABLE esn.log_esn_data;

CREATE TABLE esn.log_esn_data
(
  log_id bigint NOT NULL,
  data_text text,
  CONSTRAINT log_esn_data_pkey PRIMARY KEY (log_id),
  CONSTRAINT log_esn_data_log_id_fkey FOREIGN KEY (log_id)
      REFERENCES esn.log_esn (log_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.log_esn OWNER TO pavlov;

CREATE INDEX esn_log_esn_data_log_id1_idx
  ON esn.log_esn_data
  USING btree
  (log_id);

INSERT INTO esn.log_esn_data (log_id, data_text)
SELECT t1.log_id, t1.mess2
FROM esn.log_esn t1
WHERE trim(coalesce(t1.mess2, '')) != ''
AND NOT EXISTS (select x1.log_id from esn.log_esn_data x1 where x1.log_id = t1.log_id);

-- delete from esn.log_esn_data;

-- select * from esn.log_esn_data order by log_id desc limit 100