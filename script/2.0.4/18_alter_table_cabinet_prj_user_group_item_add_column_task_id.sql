﻿/*
	ALTER TABLE cabinet.prj_user_group_item ADD COLUMN task_id
*/

INSERT INTO cabinet.prj_user_group_item_type (item_type_id, item_type_name)
VALUES (4, 'Задание');

ALTER TABLE cabinet.prj_user_group_item ADD COLUMN task_id integer;

ALTER TABLE cabinet.prj_user_group_item
  ADD CONSTRAINT prj_user_group_item_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.prj_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

-- select * from cabinet.prj_user_group_item_type