﻿/*
	CREATE OR REPLACE FUNCTION cabinet.create_prj_user_group_fixed
*/

-- DROP FUNCTION cabinet.create_prj_user_group_fixed(text, integer);

CREATE OR REPLACE FUNCTION cabinet.create_prj_user_group_fixed(
    _group_name text,
    _fixed_kind integer
    )
  RETURNS void AS
$BODY$
BEGIN
 INSERT INTO cabinet.prj_user_group (  
  user_id,
  group_name,
  ord,
  fixed_kind
 )
 SELECT 
  t1.user_id,
  _group_name,
  COALESCE((SELECT max(y1.ord) FROM cabinet.prj_user_group y1 WHERE y1.user_id = t1.user_id), 0) + 1,
  _fixed_kind
 FROM cabinet.cab_user t1
 WHERE t1.is_active = true AND t1.is_crm_user = true
 AND NOT EXISTS (SELECT x1.group_id FROM cabinet.prj_user_group x1 WHERE x1.user_id =  t1.user_id AND x1.group_name = _group_name AND x1.fixed_kind = _fixed_kind);
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.create_prj_user_group_fixed(text, integer) OWNER TO pavlov;
