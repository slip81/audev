﻿/*
	update discount.programm_step_param
*/

-- select * from discount.programm_step_param_type;

update discount.programm_step_param
set param_type_id = 1
where coalesce(param_type_id, 0) <= 0
and coalesce(card_param_type, 0) > 0;

update discount.programm_step_param
set param_type_id = 2
where coalesce(param_type_id, 0) <= 0
and coalesce(trans_param_type, 0) > 0;

update discount.programm_step_param
set param_type_id = 3
where coalesce(param_type_id, 0) <= 0
and coalesce(programm_param_id, 0) > 0;

update discount.programm_step_param
set param_type_id = 4
where coalesce(param_type_id, 0) <= 0
and coalesce(prev_step_param_id, 0) > 0;


/*
select * from discount.programm where programm_id in (
select distinct programm_id from discount.programm_step_param where coalesce(param_type_id, 0) <= 0
)
order by programm_id desc
*/