﻿/*
	CREATE OR REPLACE FUNCTION discount.import_programm_from_tmp
*/

-- DROP FUNCTION discount.import_programm_from_tmp(bigint);

CREATE OR REPLACE FUNCTION discount.import_programm_from_tmp(
    IN in_business_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
    new_programm_id bigint;
BEGIN
	SET search_path to 'discount';
	
	result := 0;    

	/*
	select * from programm
	select * from tmp_programm
	*/

	INSERT INTO programm (programm_name, date_beg, date_end, descr_short, business_id, descr_full, parent_template_id, card_type_group_id) 
	SELECT programm_name, date_beg, date_end, descr_short, in_business_id, descr_full, parent_template_id, card_type_group_id
	FROM tmp_programm
	RETURNING programm_id INTO new_programm_id;
	-- select * from programm
	-- programm_id 954043925037844443


	INSERT INTO programm_param (programm_id, param_num, param_type, string_value, int_value, float_value, date_value, param_name, is_internal, for_unit_pos, group_id, group_internal_num)
	SELECT new_programm_id, param_num, param_type, string_value, int_value, float_value, date_value, param_name, is_internal, for_unit_pos, group_id, group_internal_num
	FROM tmp_programm_param;


	INSERT INTO programm_step (programm_id, step_num, descr_short, descr_full, single_use_only) 
	SELECT new_programm_id, step_num, descr_short, descr_full, single_use_only
	FROM tmp_programm_step;
	-- select * from programm_step where programm_id = 954043925037844443

	INSERT INTO programm_step_param (programm_id, step_id, card_param_type, trans_param_type, programm_param_id, prev_step_param_id, param_num, param_name, is_programm_output, param_type_id) 
	SELECT new_programm_id, t3.step_id, t1.card_param_type, t1.trans_param_type, NULL, NULL, t1.param_num, t1.param_name, t1.is_programm_output, t1.param_type_id
	FROM tmp_programm_step_param t1
	INNER JOIN tmp_programm_step t2 ON t1.step_id = t2.step_id
	INNER JOIN programm_step t3 ON t3.programm_id = new_programm_id AND t3.step_num = t2.step_num;
	-- select * from programm_step_param where programm_id = 954043925037844443
	-- select * from tmp_programm_step_param

	UPDATE programm_step_param
	SET programm_param_id =
	(
	SELECT MAX(t5.param_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_param t3 ON t2.step_id = t3.step_id AND t3.param_num = programm_step_param.param_num
	AND t3.programm_param_id is not null
	INNER JOIN tmp_programm_param t4 ON t3.programm_param_id = t4.param_id
	INNER JOIN programm_param t5 ON t5.programm_id = new_programm_id AND t4.param_num = t5.param_num
	WHERE t1.step_id = programm_step_param.step_id
	)
	WHERE programm_step_param.programm_id = new_programm_id
	AND programm_step_param.programm_param_id is null;

	UPDATE programm_step_param
	SET prev_step_param_id =
	(
	SELECT MAX(t7.param_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_param t3 ON t2.step_id = t3.step_id AND t3.param_num = programm_step_param.param_num
	AND t3.prev_step_param_id is not null
	INNER JOIN tmp_programm_step_param t4 ON t3.prev_step_param_id = t4.param_id
	INNER JOIN tmp_programm_step t5 ON t4.step_id = t5.step_id
	INNER JOIN programm_step t6 ON t6.programm_id = new_programm_id AND t6.step_num = t5.step_num
	INNER JOIN programm_step_param t7 ON t6.step_id = t7.step_id AND t4.param_num = t7.param_num
	WHERE t1.step_id = programm_step_param.step_id
	)
	WHERE programm_step_param.programm_id = new_programm_id
	AND programm_step_param.prev_step_param_id is null;


	INSERT INTO programm_step_condition (programm_id, step_id, condition_num, op_type, op1_param_id, op2_param_id, condition_name) 
	SELECT new_programm_id, t3.step_id, t1.condition_num, t1.op_type, NULL, NULL, t1.condition_name
	FROM tmp_programm_step_condition t1
	INNER JOIN tmp_programm_step t2 ON t1.step_id = t2.step_id
	INNER JOIN programm_step t3 ON t3.programm_id = new_programm_id AND t3.step_num = t2.step_num;
	-- select * from programm_step_condition where programm_id = 954043925037844443
	-- select * from tmp_programm_step_condition

	UPDATE programm_step_condition
	SET op1_param_id =
	(
	SELECT MAX(t7.param_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_condition t3 ON t2.step_id = t3.step_id AND programm_step_condition.condition_num = t3.condition_num
	AND t3.op1_param_id is not null
	INNER JOIN tmp_programm_step_param t4 ON t3.op1_param_id = t4.param_id
	INNER JOIN tmp_programm_step t5 ON t4.step_id = t5.step_id
	INNER JOIN programm_step t6 ON t6.programm_id = new_programm_id AND t6.step_num = t5.step_num
	INNER JOIN programm_step_param t7 ON t6.step_id = t7.step_id AND t4.param_num = t7.param_num
	WHERE t1.step_id = programm_step_condition.step_id
	)
	WHERE programm_step_condition.programm_id = new_programm_id
	AND programm_step_condition.op1_param_id is null;

	UPDATE programm_step_condition
	SET op2_param_id =
	(
	SELECT MAX(t7.param_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_condition t3 ON t2.step_id = t3.step_id AND programm_step_condition.condition_num = t3.condition_num
	AND t3.op2_param_id is not null
	INNER JOIN tmp_programm_step_param t4 ON t3.op2_param_id = t4.param_id
	INNER JOIN tmp_programm_step t5 ON t4.step_id = t5.step_id
	INNER JOIN programm_step t6 ON t6.programm_id = new_programm_id AND t6.step_num = t5.step_num
	INNER JOIN programm_step_param t7 ON t6.step_id = t7.step_id AND t4.param_num = t7.param_num
	WHERE t1.step_id = programm_step_condition.step_id
	)
	WHERE programm_step_condition.programm_id = new_programm_id
	AND programm_step_condition.op2_param_id is null;


	INSERT INTO programm_step_logic (programm_id, step_id, logic_num, op_type, op1_param_id, op2_param_id, op3_param_id, op4_param_id, condition_id) 
	SELECT new_programm_id, t3.step_id, t1.logic_num, t1.op_type, NULL, NULL, NULL, NULL, NULL
	FROM tmp_programm_step_logic t1
	INNER JOIN tmp_programm_step t2 ON t1.step_id = t2.step_id
	INNER JOIN programm_step t3 ON t3.programm_id = new_programm_id AND t3.step_num = t2.step_num;
	-- select * from programm_step_logic where programm_id = 954043925037844443
	-- select * from tmp_programm_step_logic

	UPDATE programm_step_logic
	SET op1_param_id =
	(
	SELECT MAX(t7.param_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_logic t3 ON t2.step_id = t3.step_id AND programm_step_logic.logic_num = t3.logic_num
	AND t3.op1_param_id is not null
	INNER JOIN tmp_programm_step_param t4 ON t3.op1_param_id = t4.param_id
	INNER JOIN tmp_programm_step t5 ON t4.step_id = t5.step_id
	INNER JOIN programm_step t6 ON t6.programm_id = new_programm_id AND t6.step_num = t5.step_num
	INNER JOIN programm_step_param t7 ON t6.step_id = t7.step_id AND t4.param_num = t7.param_num
	WHERE t1.step_id = programm_step_logic.step_id
	)
	WHERE programm_step_logic.programm_id = new_programm_id
	AND programm_step_logic.op1_param_id is null;

	UPDATE programm_step_logic
	SET op2_param_id =
	(
	SELECT MAX(t7.param_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_logic t3 ON t2.step_id = t3.step_id AND programm_step_logic.logic_num = t3.logic_num
	AND t3.op2_param_id is not null
	INNER JOIN tmp_programm_step_param t4 ON t3.op2_param_id = t4.param_id
	INNER JOIN tmp_programm_step t5 ON t4.step_id = t5.step_id
	INNER JOIN programm_step t6 ON t6.programm_id = new_programm_id AND t6.step_num = t5.step_num
	INNER JOIN programm_step_param t7 ON t6.step_id = t7.step_id AND t4.param_num = t7.param_num
	WHERE t1.step_id = programm_step_logic.step_id
	)
	WHERE programm_step_logic.programm_id = new_programm_id
	AND programm_step_logic.op2_param_id is null;

	UPDATE programm_step_logic
	SET op3_param_id =
	(
	SELECT MAX(t7.param_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_logic t3 ON t2.step_id = t3.step_id AND programm_step_logic.logic_num = t3.logic_num
	AND t3.op3_param_id is not null
	INNER JOIN tmp_programm_step_param t4 ON t3.op3_param_id = t4.param_id
	INNER JOIN tmp_programm_step t5 ON t4.step_id = t5.step_id
	INNER JOIN programm_step t6 ON t6.programm_id = new_programm_id AND t6.step_num = t5.step_num
	INNER JOIN programm_step_param t7 ON t6.step_id = t7.step_id AND t4.param_num = t7.param_num
	WHERE t1.step_id = programm_step_logic.step_id
	)
	WHERE programm_step_logic.programm_id = new_programm_id
	AND programm_step_logic.op3_param_id is null;

	UPDATE programm_step_logic
	SET op4_param_id =
	(
	SELECT MAX(t7.param_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_logic t3 ON t2.step_id = t3.step_id AND programm_step_logic.logic_num = t3.logic_num
	AND t3.op4_param_id is not null
	INNER JOIN tmp_programm_step_param t4 ON t3.op4_param_id = t4.param_id
	INNER JOIN tmp_programm_step t5 ON t4.step_id = t5.step_id
	INNER JOIN programm_step t6 ON t6.programm_id = new_programm_id AND t6.step_num = t5.step_num
	INNER JOIN programm_step_param t7 ON t6.step_id = t7.step_id AND t4.param_num = t7.param_num
	WHERE t1.step_id = programm_step_logic.step_id
	)
	WHERE programm_step_logic.programm_id = new_programm_id
	AND programm_step_logic.op4_param_id is null;


	UPDATE programm_step_logic
	SET condition_id =
	(
	SELECT MAX(t7.condition_id) 
	FROM programm_step t1
	INNER JOIN tmp_programm_step t2 ON t1.step_num = t2.step_num
	INNER JOIN tmp_programm_step_logic t3 ON t2.step_id = t3.step_id AND programm_step_logic.logic_num = t3.logic_num
	AND t3.condition_id is not null
	INNER JOIN tmp_programm_step_condition t4 ON t3.condition_id = t4.condition_id
	INNER JOIN tmp_programm_step t5 ON t4.step_id = t5.step_id
	INNER JOIN programm_step t6 ON t6.programm_id = new_programm_id AND t6.step_num = t5.step_num
	INNER JOIN programm_step_condition t7 ON t6.step_id = t7.step_id AND t4.condition_num = t7.condition_num
	WHERE t1.step_id = programm_step_logic.step_id
	)
	WHERE programm_step_logic.programm_id = new_programm_id
	AND programm_step_logic.condition_id is null;

	result:= new_programm_id;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.import_programm_from_tmp(bigint) OWNER TO pavlov;
