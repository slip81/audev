﻿/*
	insert prj_user_group Принятые Переданные
*/

DELETE FROM cabinet.prj_user_group_item;
DELETE FROM cabinet.prj_user_group;

SELECT cabinet.create_prj_user_group_fixed('Принято в работу', 1);
SELECT cabinet.create_prj_user_group_fixed('Передано в работу', 2);

-- select * from cabinet.prj_user_group order by user_id, group_id;
-- select * from  cabinet.prj_user_group_item_type
-- select * from  cabinet.prj_task_user_state_type

INSERT INTO cabinet.prj_user_group_item (
  group_id,
  item_type_id,
  claim_id,
  ord,
  task_id
)
SELECT
  t1.group_id,
  4,
  t3.claim_id,
  row_number() over (order by t3.task_id),
  t3.task_id
FROM cabinet.prj_user_group t1
INNER JOIN cabinet.prj_task_user_state t2 ON t1.user_id = t2.user_id
AND t2.is_current = true AND t2.state_type_id = 2
INNER JOIN cabinet.prj_task t3 ON t2.task_id = t3.task_id
WHERE t1.fixed_kind = 1; -- Принято в работу

INSERT INTO cabinet.prj_user_group_item (
  group_id,
  item_type_id,
  claim_id,
  ord,
  task_id
)
SELECT
  t1.group_id,
  4,
  t3.claim_id,
  row_number() over (order by t3.task_id),
  t3.task_id
FROM cabinet.prj_user_group t1
INNER JOIN cabinet.prj_task_user_state t2 ON t1.user_id = t2.user_id
AND t2.is_current = true AND t2.state_type_id = 1
INNER JOIN cabinet.prj_task t3 ON t2.task_id = t3.task_id
WHERE t1.fixed_kind = 2; -- Передано в работу

-- select * from cabinet.prj_user_group_item order by group_id, item_id;