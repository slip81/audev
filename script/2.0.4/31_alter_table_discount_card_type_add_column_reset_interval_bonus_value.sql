﻿/*
	ALTER TABLE discount.card_type ADD COLUMNs reset_interval_bonus_value, reset_date
*/

ALTER TABLE discount.card_type ADD COLUMN reset_interval_bonus_value smallint;
ALTER TABLE discount.card_type ADD COLUMN reset_date timestamp without time zone;
