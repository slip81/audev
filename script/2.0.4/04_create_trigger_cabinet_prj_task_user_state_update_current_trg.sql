﻿/*
	CREATE TRIGGER cabinet_prj_task_user_state_update_current_trg
*/

-- DROP TRIGGER cabinet_prj_task_user_state_update_current_trg ON cabinet.prj_task_user_state;

CREATE TRIGGER cabinet_prj_task_user_state_update_current_trg
  AFTER INSERT
  ON cabinet.prj_task_user_state
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.prj_task_user_state_update_current();
