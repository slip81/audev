﻿/*
	ALTER TABLE cabinet.prj_work ADD COLUMN date_send
*/

ALTER TABLE cabinet.prj_work ADD COLUMN date_send timestamp without time zone;