﻿/*
	CREATE OR REPLACE FUNCTION cabinet.fill_prj_user_group_fixed
*/

-- DROP FUNCTION cabinet.fill_prj_user_group_fixed(integer);

CREATE OR REPLACE FUNCTION cabinet.fill_prj_user_group_fixed(    
    _user_id integer)
  RETURNS void AS
$BODY$
BEGIN
	DELETE FROM cabinet.prj_user_group_item WHERE group_id IN
	(SELECT group_id FROM cabinet.prj_user_group WHERE fixed_kind in (1,2) AND user_id = _user_id);

	INSERT INTO cabinet.prj_user_group_item (
	  group_id,
	  item_type_id,
	  claim_id,
	  ord,
	  task_id
	)
	SELECT
	  t1.group_id,
	  4,
	  t3.claim_id,
	  row_number() over (order by t3.task_id),
	  t3.task_id
	FROM cabinet.prj_user_group t1
	INNER JOIN cabinet.prj_task_user_state t2 ON t1.user_id = t2.user_id
	AND t2.is_current = true AND t2.state_type_id = 2
	INNER JOIN cabinet.prj_task t3 ON t2.task_id = t3.task_id
	WHERE t1.fixed_kind = 1 -- Принято в работу
	AND t1.user_id = _user_id;

	INSERT INTO cabinet.prj_user_group_item (
	  group_id,
	  item_type_id,
	  claim_id,
	  ord,
	  task_id
	)
	SELECT
	  t1.group_id,
	  4,
	  t3.claim_id,
	  row_number() over (order by t3.task_id),
	  t3.task_id
	FROM cabinet.prj_user_group t1
	INNER JOIN cabinet.prj_task_user_state t2 ON t1.user_id = t2.user_id
	AND t2.is_current = true AND t2.state_type_id = 1
	INNER JOIN cabinet.prj_task t3 ON t2.task_id = t3.task_id
	WHERE t1.fixed_kind = 2 -- Передано в работу
	AND t1.user_id = _user_id;
 
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.fill_prj_user_group_fixed(integer) OWNER TO pavlov;
