﻿/*
	DROP TABLE cabinet.document_xxx
*/

DROP TABLE cabinet.documents_files;
DROP TABLE cabinet.documents_regions;
DROP TABLE cabinet.document_sites;
DROP TABLE cabinet.normative_document;
DROP TABLE cabinet.document_type;