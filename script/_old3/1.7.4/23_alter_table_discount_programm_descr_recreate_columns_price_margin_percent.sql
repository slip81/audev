﻿/*
	ALTER TABLE discount.programm_descr RECREATE COLUMNS price_margin_percent_lte_forbid_zhv_disc, price_margin_percent_gte_forbid_zhv_disc
*/

ALTER TABLE discount.programm_descr DROP COLUMN price_margin_percent_lte_forbid_zhv_disc;
ALTER TABLE discount.programm_descr DROP COLUMN price_margin_percent_gte_forbid_zhv_disc;

-- на товары ЖНВЛП с процентом розничной наценки <= заданного скидка не распространяется
ALTER TABLE discount.programm_descr ADD COLUMN price_margin_percent_lte_forbid_zhv_disc boolean NOT NULL DEFAULT false;

-- на товары ЖНВЛП с процентом розничной наценки >= заданного скидка не распространяется
ALTER TABLE discount.programm_descr ADD COLUMN price_margin_percent_gte_forbid_zhv_disc boolean NOT NULL DEFAULT false;