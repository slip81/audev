﻿/*
	CREATE TABLE discount.programm_order
*/

-- DROP TABLE discount.programm_order;

CREATE TABLE discount.programm_order
(
  order_id serial NOT NULL,
  business_id bigint,    
  mess character varying,
  descr character varying,
  crt_user character varying,
  crt_date timestamp without time zone,
  done_user character varying,
  done_date timestamp without time zone,  
  programm_id bigint,  
  card_type_group_id bigint,
  date_beg date,
  trans_sum_min_disc numeric,
  trans_sum_max_disc numeric,
  trans_sum_min_bonus_for_card numeric,
  trans_sum_max_bonus_for_card numeric,
  trans_sum_min_bonus_for_pay numeric,
  trans_sum_max_bonus_for_pay numeric,  
  proc_const_disc numeric,
  proc_step_disc character varying,
  sum_step_card_disc boolean NOT NULL DEFAULT false,
  sum_step_trans_disc boolean NOT NULL DEFAULT false,
  proc_const_bonus_for_card numeric,
  proc_step_bonus_for_card character varying,
  sum_step_card_bonus_for_card boolean NOT NULL DEFAULT false,
  sum_step_trans_bonus_for_card boolean NOT NULL DEFAULT false,
  use_max_percent_disc boolean NOT NULL DEFAULT false,
  allow_zero_disc_bonus_for_card boolean NOT NULL DEFAULT false,
  allow_zero_disc_bonus_for_pay boolean NOT NULL DEFAULT false,  
  same_for_zhv_disc boolean NOT NULL DEFAULT false,
  same_for_zhv_bonus boolean NOT NULL DEFAULT false,  
  proc_const_zhv_disc numeric,
  proc_step_zhv_disc character varying,
  sum_step_card_zhv_disc boolean NOT NULL DEFAULT false,
  sum_step_trans_zhv_disc boolean NOT NULL DEFAULT false,
  proc_const_zhv_bonus_for_card numeric,
  proc_step_zhv_bonus_for_card character varying,
  sum_step_card_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  sum_step_trans_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  use_max_percent_zhv_disc boolean NOT NULL DEFAULT false,
  allow_zero_disc_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  allow_zero_disc_zhv_bonus_for_pay boolean NOT NULL DEFAULT false,  
  allow_order_disc integer,
  allow_order_bonus_for_card integer,
  allow_order_bonus_for_pay integer,  
  add_to_card_trans_sum boolean NOT NULL DEFAULT false,
  add_to_card_trans_sum_with_disc boolean NOT NULL DEFAULT false,
  add_to_card_trans_sum_with_bonus_for_pay boolean NOT NULL DEFAULT false,
  CONSTRAINT programm_order_pkey PRIMARY KEY (order_id),
  CONSTRAINT programm_order_business_id_fkey FOREIGN KEY (business_id)
      REFERENCES discount.business (business_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT programm_order_programm_id_fkey FOREIGN KEY (programm_id)
      REFERENCES discount.programm (programm_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT programm_order_card_type_group_id_fkey FOREIGN KEY (card_type_group_id)
      REFERENCES discount.card_type_group (card_type_group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_order OWNER TO pavlov;
