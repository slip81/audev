﻿/*
	ALTER TABLE discount.tmp_programm ADD COLUMN card_type_group_id
*/

ALTER TABLE discount.tmp_programm ADD COLUMN card_type_group_id bigint;