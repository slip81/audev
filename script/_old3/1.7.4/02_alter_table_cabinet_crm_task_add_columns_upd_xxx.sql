﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN upd_xxx
*/

ALTER TABLE cabinet.crm_task ADD COLUMN upd_num integer NOT NULL DEFAULT 0;
ALTER TABLE cabinet.crm_task ADD COLUMN upd_user character varying;

COMMENT ON COLUMN cabinet.crm_task.upd_num IS 'Номер обновления';
COMMENT ON COLUMN cabinet.crm_task.upd_user IS 'Автор обновления';