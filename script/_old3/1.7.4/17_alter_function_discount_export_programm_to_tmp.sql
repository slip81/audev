﻿/*
	CREATE OR REPLACE FUNCTION discount.export_programm_to_tmp
*/

-- DROP FUNCTION discount.export_programm_to_tmp(bigint);

CREATE OR REPLACE FUNCTION discount.export_programm_to_tmp(
    IN in_programm_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
	SET search_path to 'discount';
	
	result := 0;    
	
	TRUNCATE TABLE tmp_programm_step_logic;
	TRUNCATE TABLE tmp_programm_step_condition;
	TRUNCATE TABLE tmp_programm_step_param;	
	TRUNCATE TABLE tmp_programm_step;
	TRUNCATE TABLE tmp_programm_param;
	TRUNCATE TABLE tmp_programm;

	INSERT INTO tmp_programm (programm_id, programm_name, date_beg, date_end, descr_short, business_id, descr_full, parent_template_id, card_type_group_id) 
	SELECT programm_id, programm_name, date_beg, date_end, descr_short, business_id, descr_full, parent_template_id, card_type_group_id
	FROM programm
	WHERE programm_id = in_programm_id;

	INSERT INTO tmp_programm_param (param_id, programm_id, param_num, param_type, string_value, int_value, float_value, date_value, param_name, is_internal, for_unit_pos) 
	SELECT param_id, programm_id, param_num, param_type, string_value, int_value, float_value, date_value, param_name, is_internal, for_unit_pos
	FROM programm_param
	WHERE programm_id = in_programm_id;	

	INSERT INTO tmp_programm_step (step_id, programm_id, step_num, descr_short, descr_full, single_use_only) 
	SELECT step_id, programm_id, step_num, descr_short, descr_full, single_use_only
	FROM programm_step
	WHERE programm_id = in_programm_id;

	INSERT INTO tmp_programm_step_param (param_id, programm_id, step_id, card_param_type, trans_param_type, programm_param_id, prev_step_param_id, param_num, param_name, is_programm_output) 
	SELECT param_id, programm_id, step_id, card_param_type, trans_param_type, programm_param_id, prev_step_param_id, param_num, param_name, is_programm_output
	FROM programm_step_param
	WHERE programm_id = in_programm_id;

	INSERT INTO tmp_programm_step_condition (condition_id, programm_id, step_id, condition_num, op_type, op1_param_id, op2_param_id, condition_name) 
	SELECT condition_id, programm_id, step_id, condition_num, op_type, op1_param_id, op2_param_id, condition_name
	FROM programm_step_condition
	WHERE programm_id = in_programm_id;
	
	INSERT INTO tmp_programm_step_logic (logic_id, programm_id, step_id, logic_num, op_type, op1_param_id, op2_param_id, op3_param_id, op4_param_id, condition_id) 
	SELECT logic_id, programm_id, step_id, logic_num, op_type, op1_param_id, op2_param_id, op3_param_id, op4_param_id, condition_id
	FROM programm_step_logic
	WHERE programm_id = in_programm_id;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.export_programm_to_tmp(bigint) OWNER TO pavlov;
