﻿/*
	ALTER TABLE cabinet.cab_user ADD COLUMN cookie
*/

ALTER TABLE cabinet.cab_user ADD COLUMN cookie_date timestamp without time zone;
ALTER TABLE cabinet.cab_user ADD COLUMN cookie_exp_date timestamp without time zone;

COMMENT ON COLUMN cabinet.cab_user.cookie_date IS 'Дата создания куки';
COMMENT ON COLUMN cabinet.cab_user.cookie_exp_date IS 'Дата сгорания куки';