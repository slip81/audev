﻿/*
	ALTER TABLE cabinet.story ADD COLUMN task_id integer
*/

ALTER TABLE cabinet.story ADD COLUMN task_id integer;
ALTER TABLE cabinet.story ADD COLUMN task_num character varying;

ALTER TABLE cabinet.story
  ADD CONSTRAINT story_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

COMMENT ON COLUMN cabinet.story.task_id IS 'Код задачи';
COMMENT ON COLUMN cabinet.story.task_num IS 'Номер задачи';
