﻿/*
	CREATE TABLE cabinet.cab_action_xxx
*/

--------------------------------

CREATE TABLE cabinet.cab_action_group
(
  group_id bigint NOT NULL,
  group_name character varying,
  CONSTRAINT cab_action_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_action_group OWNER TO pavlov;

INSERT INTO cabinet.cab_action_group (group_id, group_name)
SELECT group_id, group_name 
FROM discount.cab_action_group;

--------------------------------

CREATE TABLE cabinet.cab_action
(
  action_id bigint NOT NULL,
  group_id bigint NOT NULL,
  action_name character varying,
  sys_action_name character varying,
  CONSTRAINT cab_action_pkey PRIMARY KEY (action_id),
  CONSTRAINT cab_action_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.cab_action_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_action OWNER TO pavlov;

INSERT INTO cabinet.cab_action (action_id, group_id, action_name, sys_action_name)
SELECT action_id, group_id, action_name, sys_action_name 
FROM discount.cab_action;

--------------------------------

CREATE TABLE cabinet.cab_role
(
  role_id bigint NOT NULL,
  role_name character varying,
  CONSTRAINT cab_role_pkey PRIMARY KEY (role_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_role OWNER TO pavlov;

INSERT INTO cabinet.cab_role (role_id, role_name)
SELECT role_id, role_name
FROM discount.cab_role;

--------------------------------

CREATE TABLE cabinet.cab_role_action
(
  item_id bigserial NOT NULL,
  role_id bigint NOT NULL,
  group_id bigint,
  action_id bigint,
  CONSTRAINT cab_role_action_pkey PRIMARY KEY (item_id),
  CONSTRAINT cab_role_action_action_id_fkey FOREIGN KEY (action_id)
      REFERENCES cabinet.cab_action (action_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cab_role_action_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.cab_action_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cab_role_action_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cabinet.cab_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


ALTER TABLE cabinet.cab_role_action OWNER TO pavlov;

INSERT INTO cabinet.cab_role_action (item_id, role_id, group_id, action_id)
SELECT item_id, role_id, group_id, action_id
FROM discount.cab_role_action;

SELECT setval('cabinet.cab_role_action_item_id_seq', coalesce((SELECT max(item_id) FROM cabinet.cab_role_action), 0));

--------------------------------

CREATE TABLE cabinet.cab_user_role
(
  item_id bigserial NOT NULL,
  user_name character varying,
  role_id bigint NOT NULL,
  use_defaults boolean NOT NULL DEFAULT true,
  CONSTRAINT cab_user_role_pkey PRIMARY KEY (item_id),
  CONSTRAINT cab_user_role_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cabinet.cab_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_user_role OWNER TO pavlov;

INSERT INTO cabinet.cab_user_role (item_id, user_name, role_id, use_defaults)
SELECT item_id, user_name, role_id, use_defaults
FROM discount.cab_user_role;

SELECT setval('cabinet.cab_user_role_item_id_seq', coalesce((SELECT max(item_id) FROM cabinet.cab_user_role), 0));

--------------------------------

CREATE TABLE cabinet.cab_user_role_action
(
  item_id bigserial NOT NULL,
  user_name character varying,
  role_id bigint NOT NULL,
  group_id bigint,
  action_id bigint,
  CONSTRAINT cab_user_role_action_pkey PRIMARY KEY (item_id),
  CONSTRAINT cab_user_role_action_action_id_fkey FOREIGN KEY (action_id)
      REFERENCES cabinet.cab_action (action_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cab_user_role_action_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.cab_action_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cab_user_role_action_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cabinet.cab_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_user_role_action OWNER TO pavlov;

INSERT INTO cabinet.cab_user_role_action (item_id, user_name, role_id, group_id, action_id)
SELECT item_id, user_name, role_id, group_id, action_id
FROM discount.cab_user_role_action;

SELECT setval('cabinet.cab_user_role_action_item_id_seq', coalesce((SELECT max(item_id) FROM cabinet.cab_user_role_action), 0));

--------------------------------

CREATE OR REPLACE VIEW cabinet.vw_cab_user AS 
 SELECT t1.item_id,
    t1.user_name,
    t1.role_id,
    t1.use_defaults,
    t2.role_name,
    t4.org_name,
    t5.business_name,
    t5.business_id,
    t3.user_descr
   FROM cabinet.cab_user_role t1
     JOIN cabinet.cab_role t2 ON t1.role_id = t2.role_id
     LEFT JOIN discount.business_org_user t3 ON btrim(COALESCE(t1.user_name, ''::character varying)::text) = btrim(t3.user_name::text)
     LEFT JOIN discount.business_org t4 ON t3.org_id = t4.org_id
     LEFT JOIN discount.business t5 ON t4.business_id = t5.business_id;

ALTER TABLE cabinet.vw_cab_user OWNER TO pavlov;

--------------------------------

DROP VIEW discount.vw_cab_user;
DROP TABLE discount.cab_user_role_action;
DROP TABLE discount.cab_user_role;
DROP TABLE discount.cab_role_action;
DROP TABLE discount.cab_role;
DROP TABLE discount.cab_action;
DROP TABLE discount.cab_action_group;

--------------------------------