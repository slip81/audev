﻿/*
	CREATE TABLE discount.programm_descr
*/

-- DROP TABLE discount.programm_descr;

CREATE TABLE discount.programm_descr
(
  programm_id bigint NOT NULL,
  card_type_group_id bigint,
  trans_sum_min_disc boolean NOT NULL DEFAULT false,
  trans_sum_max_disc boolean NOT NULL DEFAULT false,
  trans_sum_min_bonus_for_card boolean NOT NULL DEFAULT false,
  trans_sum_max_bonus_for_card boolean NOT NULL DEFAULT false,
  trans_sum_min_bonus_for_pay boolean NOT NULL DEFAULT false,
  trans_sum_max_bonus_for_pay boolean NOT NULL DEFAULT false, 
  proc_const_disc boolean NOT NULL DEFAULT false,
  proc_step_count_disc integer,
  sum_step_card_disc boolean NOT NULL DEFAULT false,
  sum_step_trans_disc boolean NOT NULL DEFAULT false,  
  proc_const_bonus_for_card boolean NOT NULL DEFAULT false,
  proc_step_count_bonus_for_card integer,
  sum_step_card_bonus_for_card boolean NOT NULL DEFAULT false,
  sum_step_trans_bonus_for_card boolean NOT NULL DEFAULT false,    
  use_max_percent_disc boolean NOT NULL DEFAULT false,
  allow_zero_disc_bonus_for_card boolean NOT NULL DEFAULT false,
  allow_zero_disc_bonus_for_pay boolean NOT NULL DEFAULT false,
  proc_const_zhv_disc boolean NOT NULL DEFAULT false,
  proc_step_count_zhv_disc integer,
  sum_step_card_zhv_disc boolean NOT NULL DEFAULT false,
  sum_step_trans_zhv_disc boolean NOT NULL DEFAULT false,  
  proc_const_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  proc_step_count_zhv_bonus_for_card integer,
  sum_step_card_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  sum_step_trans_zhv_bonus_for_card boolean NOT NULL DEFAULT false,    
  use_max_percent_zhv_disc boolean NOT NULL DEFAULT false,
  allow_zero_disc_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  allow_zero_disc_zhv_bonus_for_pay boolean NOT NULL DEFAULT false,  
  allow_order_disc integer,
  allow_order_bonus_for_card integer,
  allow_order_bonus_for_pay integer,
  add_to_card_trans_sum boolean NOT NULL DEFAULT false,
  add_to_card_trans_sum_with_disc boolean NOT NULL DEFAULT false,
  add_to_card_trans_sum_with_bonus_for_pay boolean NOT NULL DEFAULT false,  
  CONSTRAINT programm_descr_pkey PRIMARY KEY (programm_id),
  CONSTRAINT programm_descr_card_type_group_id_fkey FOREIGN KEY (card_type_group_id)
      REFERENCES discount.card_type_group (card_type_group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT programm_descr_programm_id_fkey FOREIGN KEY (programm_id)
      REFERENCES discount.programm (programm_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_descr OWNER TO pavlov;
