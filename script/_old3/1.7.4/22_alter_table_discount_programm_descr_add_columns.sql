﻿/*
	ALTER TABLE discount.programm_descr ADD COLUMNs
*/


-- процент скидки постоянный, задается на картах
ALTER TABLE discount.programm_descr ADD COLUMN proc_const_from_card_disc boolean NOT NULL DEFAULT false;
-- процент скидки для ЖНВЛП постоянный, задается на картах
ALTER TABLE discount.programm_descr ADD COLUMN proc_const_from_card_zhv_disc boolean NOT NULL DEFAULT false;
-- бонусный процент постоянный, задается на картах
ALTER TABLE discount.programm_descr ADD COLUMN proc_const_from_card_bonus_for_card boolean NOT NULL DEFAULT false;
-- бонусный процент для ЖНВЛП постоянный, задается на картах
ALTER TABLE discount.programm_descr ADD COLUMN proc_const_from_card_zhv_bonus_for_card boolean NOT NULL DEFAULT false;

-- макс. сумма покупки для оплаты бонусами задается в процентах от суммы покупки
ALTER TABLE discount.programm_descr ADD COLUMN trans_sum_max_percent_bonus_for_pay boolean NOT NULL DEFAULT false;
-- макс. сумма покупки для оплаты бонусами задается в процентах от суммы позиций, у которых макс. скидка не равна 0
ALTER TABLE discount.programm_descr ADD COLUMN trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay boolean NOT NULL DEFAULT false;

-- на товары с процентом розничной наценки <= заданного скидка не распространяется
ALTER TABLE discount.programm_descr ADD COLUMN price_margin_percent_lte_forbid_disc boolean NOT NULL DEFAULT false;
-- на товары ЖНВЛП с процентом розничной наценки <= заданного скидка не распространяется
ALTER TABLE discount.programm_descr ADD COLUMN price_margin_percent_lte_forbid_zhv_disc numeric;
-- на товары с процентом розничной наценки >= заданного скидка не распространяется
ALTER TABLE discount.programm_descr ADD COLUMN price_margin_percent_gte_forbid_disc boolean NOT NULL DEFAULT false;
-- на товары ЖНВЛП с процентом розничной наценки >= заданного скидка не распространяется
ALTER TABLE discount.programm_descr ADD COLUMN price_margin_percent_gte_forbid_zhv_disc numeric;

-- макс. скидка явно задана
ALTER TABLE discount.programm_descr ADD COLUMN use_explicit_max_percent_disc boolean NOT NULL DEFAULT false;
-- макс. скидка по ЖНВЛП явно задана
ALTER TABLE discount.programm_descr ADD COLUMN use_explicit_max_percent_zhv_disc boolean NOT NULL DEFAULT false;
