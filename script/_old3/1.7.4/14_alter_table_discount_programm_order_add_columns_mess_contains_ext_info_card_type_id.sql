﻿/*
	ALTER TABLE discount.programm_order ADD COLUMN mess_contains_ext_info, card_type_id
*/

ALTER TABLE discount.programm_order ADD COLUMN mess_contains_ext_info boolean NOT NULL DEFAULT false;
ALTER TABLE discount.programm_order ADD COLUMN card_type_id bigint;
