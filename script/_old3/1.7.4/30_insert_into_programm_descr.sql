﻿/*
select * from discount.business order by business_id desc

select * from discount.programm where business_id = 957862396381103979 order by programm_id

select * from discount.card_transaction_detail where programm_id = 954096126649173931 order by detail_id desc

select * from discount.card_transaction where card_trans_id = 1297127789232653611

select * from discount.vw_card_transaction where business_id = 1013489512715650905 order by card_trans_id desc limit 100

*/

-- delete from discount.programm_descr;

/*
	ООО "Таблетка Плюс"

	Дисконтная накопительная система.
	Скидка предоставляется в зависимости от накопленной суммы, 5 порогов.
	Учитываются ограничения:
	- максимальная скидка по позициям
*/

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   5,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   true,  -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   true,  -- same_for_zhv_disc
   false -- same_for_zhv_bonus      
 from discount.programm 
 where programm_id = 1243397593396938709
 ;

 /*
	"ООО "Фаворит""
	
	Дисконтная накопительная система. 
	Скидка предоставляется в зависимости от накопленной суммы на карте, 6 порогов.
	Учитываются ограничения:
	- максимальная скидка по позициям
*/

update discount.programm set card_type_group_id = 1 where programm_id = 1217103335568442417;

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   6,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   false, -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   true,  -- same_for_zhv_disc
   false -- same_for_zhv_bonus      
 from discount.programm 
 where programm_id = 1217103335568442417
 ;

 /*	
	Синергия

	Дисконтная накопительная система. 
	Скидка предоставляется в зависимости от накопленной суммы на карте, 3 порога.
	Учитываются ограничения:
	- для ЖВ товаров скидка постоянная 1%
*/

update discount.programm set card_type_group_id = 1 where programm_id = 1185485225149335090;

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   3,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   false, -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   true,  -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   true,  -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   false, -- same_for_zhv_disc
   false  -- same_for_zhv_bonus      
 from discount.programm 
 where programm_id = 1185485225149335090
 ;

/*
	Никофарм, ООО

	Дисконтная накопительная система.
	Скидка предоставляется в зависимости от накопленной суммы, 3 порога.
	Учитываются ограничения:
	- максимальная скидка по позициям
*/

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   3,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   true,  -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   true,  -- same_for_zhv_disc
   false -- same_for_zhv_bonus      
 from discount.programm 
 where programm_id = 1013493439297226694
 ;

 /*
	Медуница, ООО

	Дисконтная накопительная система. 
	Скидка предоставляется в зависимости от накопленной суммы, 4 порога.
	Учитываются ограничения:
	- максимальная скидка по позициям
*/

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   4,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   true,  -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   true,  -- same_for_zhv_disc
   false -- same_for_zhv_bonus      
 from discount.programm 
 where programm_id = 958535312714237234
 ;

 /*
	ТРИТ, ООО

	Дисконтная накопительная система.
	Скидка предоставляется в зависимости от накопленной суммы на карте, 5 порогов для обычных товаров и 3 порога для ЖНВЛП.
	Учитываются ограничения:
	- максимальная скидка по позициям
*/

update discount.programm set card_type_group_id = 1 where programm_id = 1193273863354451327;

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   5,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   true,  -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   3,     -- proc_step_count_zhv_disc
   true,  -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   true,  -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   false, -- same_for_zhv_disc
   false  -- same_for_zhv_bonus      
 from discount.programm 
 where programm_id = 1193273863354451327
 ;

/*
	Аптека В1, ООО

	Дисконтная накопительная система. 
	Скидка предоставляется в зависимости от накопленной суммы, 5 порогов.
	Учитываются ограничения:
	- на товары с наценкой меньше или равно 20 % скидка не распространяется
*/

update discount.programm set card_type_group_id = 1 where programm_id = 1202133525197751368;

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus,
  -- new
  proc_const_from_card_disc,
  proc_const_from_card_bonus_for_card,
  trans_sum_max_percent_bonus_for_pay,
  trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
  price_margin_percent_lte_forbid_disc,
  price_margin_percent_lte_forbid_zhv_disc,
  price_margin_percent_gte_forbid_disc,  
  price_margin_percent_gte_forbid_zhv_disc,  
  use_explicit_max_percent_disc,
  use_explicit_max_percent_zhv_disc,
  proc_const_from_card_zhv_disc,
  proc_const_from_card_zhv_bonus_for_card
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   5,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   false, -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   true,  -- same_for_zhv_disc
   false, -- same_for_zhv_bonus      
   -- new
   false, -- proc_const_from_card_disc,
   false, -- proc_const_from_card_bonus_for_card,
   false, -- trans_sum_max_percent_bonus_for_pay,
   false, -- trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
   true,  -- price_margin_percent_lte_forbid_disc,
   false, -- price_margin_percent_lte_forbid_zhv_disc,
   false, -- price_margin_percent_gte_forbid_disc,  
   false, -- price_margin_percent_gte_forbid_zhv_disc,  
   false, -- use_explicit_max_percent_disc,
   false, -- use_explicit_max_percent_zhv_disc,
   false, -- proc_const_from_card_zhv_disc,
   false  -- proc_const_from_card_zhv_bonus_for_card   
 from discount.programm 
 where programm_id = 1202133525197751368
 ;

/*
	Халилова ИП

	Дисконтная накопительная система.
	Скидка предоставляется в зависимости от накопленной суммы, 4 порога.
	Учитываются ограничения:
	- для ЖВ товаров макс. скидка = 5%
	- максимальная скидка по позициям
*/

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus,
  -- new
  proc_const_from_card_disc,
  proc_const_from_card_bonus_for_card,
  trans_sum_max_percent_bonus_for_pay,
  trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
  price_margin_percent_lte_forbid_disc,
  price_margin_percent_lte_forbid_zhv_disc,
  price_margin_percent_gte_forbid_disc,  
  price_margin_percent_gte_forbid_zhv_disc,  
  use_explicit_max_percent_disc,
  use_explicit_max_percent_zhv_disc,
  proc_const_from_card_zhv_disc,
  proc_const_from_card_zhv_bonus_for_card
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   4,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   true,  -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   4,     -- proc_step_count_zhv_disc
   true,  -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   true,  -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   false, -- same_for_zhv_disc
   false, -- same_for_zhv_bonus      
   -- new
   false, -- proc_const_from_card_disc,
   false, -- proc_const_from_card_bonus_for_card,
   false, -- trans_sum_max_percent_bonus_for_pay,
   false, -- trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
   false, -- price_margin_percent_lte_forbid_disc,
   false, -- price_margin_percent_lte_forbid_zhv_disc,
   false, -- price_margin_percent_gte_forbid_disc,  
   false, -- price_margin_percent_gte_forbid_zhv_disc,  
   false, -- use_explicit_max_percent_disc,
   true,  -- use_explicit_max_percent_zhv_disc,
   false, -- proc_const_from_card_zhv_disc,
   false  -- proc_const_from_card_zhv_bonus_for_card   
 from discount.programm 
 where programm_id = 1186898622071964808
 ;

/*
	Главврач и Партнеры, ООО

	Дисконтная накопительная система.
	Скидка постоянная, берется с карты.
	Учитываются ограничения:	
	- максимальная скидка по позициям
*/

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus,
  -- new
  proc_const_from_card_disc,
  proc_const_from_card_bonus_for_card,
  trans_sum_max_percent_bonus_for_pay,
  trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
  price_margin_percent_lte_forbid_disc,
  price_margin_percent_lte_forbid_zhv_disc,
  price_margin_percent_gte_forbid_disc,  
  price_margin_percent_gte_forbid_zhv_disc,  
  use_explicit_max_percent_disc,
  use_explicit_max_percent_zhv_disc,
  proc_const_from_card_zhv_disc,
  proc_const_from_card_zhv_bonus_for_card
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   null,  -- proc_step_count_disc
   false, -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   true,  -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   true,  -- same_for_zhv_disc
   false, -- same_for_zhv_bonus      
   -- new
   true,  -- proc_const_from_card_disc,
   false, -- proc_const_from_card_bonus_for_card,
   false, -- trans_sum_max_percent_bonus_for_pay,
   false, -- trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
   true,  -- price_margin_percent_lte_forbid_disc,
   false, -- price_margin_percent_lte_forbid_zhv_disc,
   false, -- price_margin_percent_gte_forbid_disc,  
   false, -- price_margin_percent_gte_forbid_zhv_disc,  
   false, -- use_explicit_max_percent_disc,
   false, -- use_explicit_max_percent_zhv_disc,
   false, -- proc_const_from_card_zhv_disc,
   false  -- proc_const_from_card_zhv_bonus_for_card   
 from discount.programm 
 where programm_id = 1141758208247858607
 ;

/*
	ООО "Столица"

	Дисконтная накопительная система.
	Скидка предоставляется в зависимости от накопленной суммы, 4 порога.
	Учитываются ограничения:
	- на товары ЖВ скидка не делается
	- на товары с наценкой меньше 16 % скидка не делается
*/

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus,
  -- new
  proc_const_from_card_disc,
  proc_const_from_card_bonus_for_card,
  trans_sum_max_percent_bonus_for_pay,
  trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
  price_margin_percent_lte_forbid_disc,
  price_margin_percent_lte_forbid_zhv_disc,
  price_margin_percent_gte_forbid_disc,  
  price_margin_percent_gte_forbid_zhv_disc,  
  use_explicit_max_percent_disc,
  use_explicit_max_percent_zhv_disc,
  proc_const_from_card_zhv_disc,
  proc_const_from_card_zhv_bonus_for_card
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   4,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   false, -- use_max_percent_disc
   false, -- allow_zero_disc_bonus_for_card
   false, -- allow_zero_disc_bonus_for_pay
   true,  -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   null,  -- allow_order_bonus_for_card
   null,  -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   false, -- same_for_zhv_disc
   false, -- same_for_zhv_bonus      
   -- new
   false, -- proc_const_from_card_disc,
   false, -- proc_const_from_card_bonus_for_card,
   false, -- trans_sum_max_percent_bonus_for_pay,
   false, -- trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
   true,  -- price_margin_percent_lte_forbid_disc,
   false, -- price_margin_percent_lte_forbid_zhv_disc,
   false, -- price_margin_percent_gte_forbid_disc,  
   false, -- price_margin_percent_gte_forbid_zhv_disc,  
   false, -- use_explicit_max_percent_disc,
   true,  -- use_explicit_max_percent_zhv_disc,
   false, -- proc_const_from_card_zhv_disc,
   false  -- proc_const_from_card_zhv_bonus_for_card   
 from discount.programm 
 where programm_id = 1127323590446810461
 ;

/*
	ТП Элита-1

	Дисконтно-бонусная система.
	Скидка предоставляется в зависимости от накопленной суммы, 4 порога.
	Сумма покупки с учетом скидки может быть оплачена накопленными бонусами, но не более 40 % от суммы с учетом скидки.
	С оставшейся к оплате суммы накапливаются бонусы, процент накопления бонусов равен проценту скидки по карте.
	Учитываются ограничения:	
	- максимальная скидка по позициям	
*/

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus,
  -- new
  proc_const_from_card_disc,
  proc_const_from_card_bonus_for_card,
  trans_sum_max_percent_bonus_for_pay,
  trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
  price_margin_percent_lte_forbid_disc,
  price_margin_percent_lte_forbid_zhv_disc,
  price_margin_percent_gte_forbid_disc,  
  price_margin_percent_gte_forbid_zhv_disc,  
  use_explicit_max_percent_disc,
  use_explicit_max_percent_zhv_disc,
  proc_const_from_card_zhv_disc,
  proc_const_from_card_zhv_bonus_for_card
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   4,  	  -- proc_step_count_disc
   true,  -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   4,     -- proc_step_count_bonus_for_card
   true,  -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   true,  -- use_max_percent_disc
   true,  -- allow_zero_disc_bonus_for_card
   true,  -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   1,     -- allow_order_disc
   3,     -- allow_order_bonus_for_card
   2,     -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   true,  -- add_to_card_trans_sum_with_disc
   true,  -- add_to_card_trans_sum_with_bonus_for_pay
   true,  -- same_for_zhv_disc
   true,  -- same_for_zhv_bonus      
   -- new
   false, -- proc_const_from_card_disc,
   false, -- proc_const_from_card_bonus_for_card,
   true,  -- trans_sum_max_percent_bonus_for_pay,
   false, -- trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
   false, -- price_margin_percent_lte_forbid_disc,
   false, -- price_margin_percent_lte_forbid_zhv_disc,
   false, -- price_margin_percent_gte_forbid_disc,  
   false, -- price_margin_percent_gte_forbid_zhv_disc,  
   false, -- use_explicit_max_percent_disc,
   false, -- use_explicit_max_percent_zhv_disc,
   false, -- proc_const_from_card_zhv_disc,
   false  -- proc_const_from_card_zhv_bonus_for_card   
 from discount.programm 
 where programm_id = 1078849080945280646
 ;

/*
	Смирнова, ИП

	Бонусная система.	
	Сумма покупки может быть полностью оплачена накопленными бонусами.
	С оставшейся к оплате суммы накапливаются бонусы, процент накопления бонусов берется с карты.
*/

insert into discount.programm_descr (
  programm_id,
  card_type_group_id,
  trans_sum_min_disc,
  trans_sum_max_disc,
  trans_sum_min_bonus_for_card,
  trans_sum_max_bonus_for_card,
  trans_sum_min_bonus_for_pay,
  trans_sum_max_bonus_for_pay,
  proc_const_disc,
  proc_step_count_disc,
  sum_step_card_disc,
  sum_step_trans_disc,
  proc_const_bonus_for_card,
  proc_step_count_bonus_for_card,
  sum_step_card_bonus_for_card,
  sum_step_trans_bonus_for_card,
  use_max_percent_disc,
  allow_zero_disc_bonus_for_card,
  allow_zero_disc_bonus_for_pay,
  proc_const_zhv_disc,
  proc_step_count_zhv_disc,
  sum_step_card_zhv_disc,
  sum_step_trans_zhv_disc,
  proc_const_zhv_bonus_for_card,
  proc_step_count_zhv_bonus_for_card,
  sum_step_card_zhv_bonus_for_card,
  sum_step_trans_zhv_bonus_for_card,
  use_max_percent_zhv_disc,
  allow_zero_disc_zhv_bonus_for_card,
  allow_zero_disc_zhv_bonus_for_pay,
  allow_order_disc,
  allow_order_bonus_for_card,
  allow_order_bonus_for_pay,
  add_to_card_trans_sum,
  add_to_card_trans_sum_with_disc,
  add_to_card_trans_sum_with_bonus_for_pay,
  same_for_zhv_disc,
  same_for_zhv_bonus,
  -- new
  proc_const_from_card_disc,
  proc_const_from_card_bonus_for_card,
  trans_sum_max_percent_bonus_for_pay,
  trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
  price_margin_percent_lte_forbid_disc,
  price_margin_percent_lte_forbid_zhv_disc,
  price_margin_percent_gte_forbid_disc,  
  price_margin_percent_gte_forbid_zhv_disc,  
  use_explicit_max_percent_disc,
  use_explicit_max_percent_zhv_disc,
  proc_const_from_card_zhv_disc,
  proc_const_from_card_zhv_bonus_for_card
  )
 select 
   programm_id,
   card_type_group_id,
   false, -- trans_sum_min_disc
   false, -- trans_sum_max_disc
   false, -- trans_sum_min_bonus_for_card
   false, -- trans_sum_max_bonus_for_card
   false, -- trans_sum_min_bonus_for_pay
   false, -- trans_sum_max_bonus_for_pay
   false, -- proc_const_disc
   null,  -- proc_step_count_disc
   false, -- sum_step_card_disc
   false, -- sum_step_trans_disc
   false, -- proc_const_bonus_for_card
   null,  -- proc_step_count_bonus_for_card
   false, -- sum_step_card_bonus_for_card
   false, -- sum_step_trans_bonus_for_card
   false, -- use_max_percent_disc
   true,  -- allow_zero_disc_bonus_for_card
   true,  -- allow_zero_disc_bonus_for_pay
   false, -- proc_const_zhv_disc
   null,  -- proc_step_count_zhv_disc
   false, -- sum_step_card_zhv_disc
   false, -- sum_step_trans_zhv_disc
   false, -- proc_const_zhv_bonus_for_card
   null,  -- proc_step_count_zhv_bonus_for_card
   false, -- sum_step_card_zhv_bonus_for_card
   false, -- sum_step_trans_zhv_bonus_for_card
   false, -- use_max_percent_zhv_disc
   false, -- allow_zero_disc_zhv_bonus_for_card
   false, -- allow_zero_disc_zhv_bonus_for_pay
   null,  -- allow_order_disc
   2,     -- allow_order_bonus_for_card
   1,     -- allow_order_bonus_for_pay
   false, -- add_to_card_trans_sum
   false, -- add_to_card_trans_sum_with_disc
   false, -- add_to_card_trans_sum_with_bonus_for_pay
   false, -- same_for_zhv_disc
   true,  -- same_for_zhv_bonus      
   -- new
   false, -- proc_const_from_card_disc,
   true,  -- proc_const_from_card_bonus_for_card,
   false, -- trans_sum_max_percent_bonus_for_pay,
   false, -- trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
   false, -- price_margin_percent_lte_forbid_disc,
   false, -- price_margin_percent_lte_forbid_zhv_disc,
   false, -- price_margin_percent_gte_forbid_disc,  
   false, -- price_margin_percent_gte_forbid_zhv_disc,  
   false, -- use_explicit_max_percent_disc,
   false, -- use_explicit_max_percent_zhv_disc,
   false, -- proc_const_from_card_zhv_disc,
   false  -- proc_const_from_card_zhv_bonus_for_card   
 from discount.programm 
 where programm_id = 1013709302667412750
 ;

-- select * from discount.programm_descr order by programm_id desc;
