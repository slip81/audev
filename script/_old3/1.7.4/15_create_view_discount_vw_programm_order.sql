﻿/*
	CREATE OR REPLACE VIEW discount.vw_programm_order
*/

-- DROP VIEW discount.vw_programm_order;

CREATE OR REPLACE VIEW discount.vw_programm_order AS 
 SELECT 
  t1.order_id,
  t1.business_id,
  t1.mess,
  t1.descr,
  t1.crt_user,
  t1.crt_date,
  t1.done_user,
  t1.done_date,
  t1.programm_id,
  t1.card_type_group_id,
  t1.date_beg,
  t1.trans_sum_min_disc,
  t1.trans_sum_max_disc,
  t1.trans_sum_min_bonus_for_card,
  t1.trans_sum_max_bonus_for_card,
  t1.trans_sum_min_bonus_for_pay,
  t1.trans_sum_max_bonus_for_pay,
  t1.proc_const_disc,
  t1.proc_step_disc,
  t1.sum_step_card_disc,
  t1.sum_step_trans_disc,
  t1.proc_const_bonus_for_card,
  t1.proc_step_bonus_for_card,
  t1.sum_step_card_bonus_for_card,
  t1.sum_step_trans_bonus_for_card,
  t1.use_max_percent_disc,
  t1.allow_zero_disc_bonus_for_card,
  t1.allow_zero_disc_bonus_for_pay,
  t1.same_for_zhv_disc,
  t1.same_for_zhv_bonus,
  t1.proc_const_zhv_disc,
  t1.proc_step_zhv_disc,
  t1.sum_step_card_zhv_disc,
  t1.sum_step_trans_zhv_disc,
  t1.proc_const_zhv_bonus_for_card,
  t1.proc_step_zhv_bonus_for_card,
  t1.sum_step_card_zhv_bonus_for_card,
  t1.sum_step_trans_zhv_bonus_for_card,
  t1.use_max_percent_zhv_disc,
  t1.allow_zero_disc_zhv_bonus_for_card,
  t1.allow_zero_disc_zhv_bonus_for_pay,
  t1.allow_order_disc,
  t1.allow_order_bonus_for_card,
  t1.allow_order_bonus_for_pay,
  t1.add_to_card_trans_sum,
  t1.add_to_card_trans_sum_with_disc,
  t1.add_to_card_trans_sum_with_bonus_for_pay,
  t1.mess_contains_ext_info,
  t1.card_type_id,
  case when t1.done_date is not null then 'Исполнена' else 'Ожидает исполнения' end as order_state,
  t2.business_name,
  t3.group_name as card_type_group_name,
  t11.card_type_name
 FROM discount.programm_order t1
 INNER JOIN discount.business t2 ON t1.business_id = t2.business_id
 INNER JOIN discount.card_type_group t3 ON t1.card_type_group_id = t3.card_type_group_id
 LEFT JOIN discount.card_type t11 ON t1.card_type_id = t11.card_type_id
 ;

ALTER TABLE discount.vw_programm_order OWNER TO pavlov;

-- select * from discount.vw_programm_order
