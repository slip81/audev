﻿/*
	ALTER TABLE esn.story SET SCHEMA cabinet
*/

ALTER TABLE esn.story_state SET SCHEMA cabinet;
ALTER TABLE esn.story_state_rule SET SCHEMA cabinet;
ALTER TABLE esn.story_lc SET SCHEMA cabinet;
ALTER TABLE esn.story SET SCHEMA cabinet;


