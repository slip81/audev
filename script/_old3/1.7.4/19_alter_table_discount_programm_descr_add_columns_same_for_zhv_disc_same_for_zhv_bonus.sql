﻿/*
	ALTER TABLE discount.programm_descr ADD COLUMN same_for_zhv_disc, same_for_zhv_bonus
*/

ALTER TABLE discount.programm_descr ADD COLUMN same_for_zhv_disc boolean NOT NULL DEFAULT false;
ALTER TABLE discount.programm_descr ADD COLUMN same_for_zhv_bonus boolean NOT NULL DEFAULT false;
