﻿/*
	insert into cabinet.client_user
*/

insert into cabinet.client_user (user_id, client_id, sales_id, user_name, user_name_full, is_main_for_sales)
select t1.user_id, max(t1.client_id), max(t1.id), max(t2.name), max(t2.name), true
from cabinet.sales t1
inner join cabinet.my_aspnet_users t2 on t1.user_id = t2.id
where t1.user_id is not null
and not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = t1.user_id)
group by t1.user_id;

insert into cabinet.client_user (user_id, client_id, sales_id, user_name, user_name_full, is_main_for_sales)
select t1.user_id, t1.id, null, t2.name, t2.name, false
from cabinet.client t1
inner join cabinet.my_aspnet_users t2 on t1.user_id = t2.id
where t1.user_id is not null
and not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = t1.user_id);

delete from cabinet.my_aspnet_membership
where not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = cabinet.my_aspnet_membership."userId");

delete from cabinet.my_aspnet_users
where not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = cabinet.my_aspnet_users.id);

-- select * from cabinet.client_user order by user_id desc

