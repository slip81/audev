﻿/*
	ALTER TABLE cabinet.crm_state, crm_priority ADD COLUMN ord
*/

ALTER TABLE cabinet.crm_state ADD COLUMN ord integer NOT NULL DEFAULT 0;
ALTER TABLE cabinet.crm_priority ADD COLUMN ord integer NOT NULL DEFAULT 0;
