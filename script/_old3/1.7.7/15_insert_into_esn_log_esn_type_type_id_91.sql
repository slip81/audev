﻿/*
	insert into esn.log_esn_type (type_id, type_name)
*/

insert into esn.log_esn_type (type_id, type_name)
select 91, 'Сервис отчетности'
from esn.log_esn_type
where not exists(select type_id from esn.log_esn_type where type_id = 91)
limit 1;

-- select * from esn.log_esn_type order by type_id;