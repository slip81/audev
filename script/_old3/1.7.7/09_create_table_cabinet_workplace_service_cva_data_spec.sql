﻿/*
	CREATE TABLE cabinet.workplace_service_cva_data_spec
*/

-- DROP TABLE cabinet.workplace_service_cva_data_spec;

CREATE TABLE cabinet.workplace_service_cva_data_spec
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  cva_data text,
  cva_cons_data text,
  cva_full_data text,
  batch_num integer, /* порядковый номер пакета для workplace_id */
  batch_date timestamp without time zone, /* дата создания записи в БД */
  batch_state integer NOT NULL DEFAULT 1, /* 1 - не отправлен, 2 - отправляется, 3 - ошибка при отправке, 4 - отправлен */
  sending_date timestamp without time zone, /* дата попытки отправки */
  sent_date timestamp without time zone, /* дата удавшейся отправки */  
  CONSTRAINT workplace_service_cva_data_spec_pkey PRIMARY KEY (item_id),
  CONSTRAINT workplace_service_cva_data_spec_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT workplace_service_cva_data_spec_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.workplace_service_cva_data_spec OWNER TO pavlov;