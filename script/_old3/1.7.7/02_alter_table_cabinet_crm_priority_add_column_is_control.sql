﻿/*
	ALTER TABLE cabinet.crm_priority ADD COLUMN is_control boolean NOT NULL DEFAULT false
*/

ALTER TABLE cabinet.crm_priority ADD COLUMN is_control boolean NOT NULL DEFAULT false;
