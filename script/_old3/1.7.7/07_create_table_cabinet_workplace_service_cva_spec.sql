﻿/*
	CREATE TABLE cabinet.workplace_service_cva_spec
*/

-- DROP TABLE cabinet.workplace_service_cva_spec;

CREATE TABLE cabinet.workplace_service_cva_spec
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  target_url character varying,
  pharm_login character varying,
  pharm_pwd character varying,
  send_interval int,
  pharm_code character varying,
  active smallint NOT NULL DEFAULT 1,
  info_internet_trade character varying,
  info_name character varying,
  info_bank_cards character varying,
  info_sale_phone character varying,
  info_additional_info character varying,
  info_www character varying,
  info_city character varying,
  info_city_area character varying,
  info_address character varying,
  info_sale_shedule character varying,
  info_map_gps character varying,
  info_map_caption character varying,
  CONSTRAINT workplace_service_cva_spec_pkey PRIMARY KEY (item_id),
  CONSTRAINT workplace_service_cva_spec_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT workplace_service_cva_spec_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.workplace_service_cva_spec OWNER TO pavlov;
