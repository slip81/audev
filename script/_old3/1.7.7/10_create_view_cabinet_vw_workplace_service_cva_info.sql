﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_workplace_service_cva_info
*/

-- DROP VIEW cabinet.vw_workplace_service_cva_info;

CREATE OR REPLACE VIEW cabinet.vw_workplace_service_cva_info AS 
	SELECT t1.workplace_id, t1.send_interval, 
		max(t2.sent_date) as last_sent_date, 
		max(case when t2.batch_state in (1, 3) then t2.batch_num else 0 end) as batch_num_for_send,
		max(case when t2.batch_state in (1, 3) then t2.batch_date else null end) as batch_date_for_send,
		count(t2.sent_date) as batch_sent_count		
	FROM cabinet.workplace_service_cva_spec t1
		LEFT JOIN cabinet.workplace_service_cva_data_spec t2 ON t1.workplace_id = t2.workplace_id AND t1.service_id = t2.service_id
		GROUP BY t1.workplace_id, t1.send_interval
	;

ALTER TABLE cabinet.vw_workplace_service_cva_info OWNER TO pavlov;
