﻿/*
	insert into cabinet.service
*/

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, branch_id, price, need_key, "NeedCheckVersionLicense", is_service, priority, have_spec)
values (60, '{c80f000e-6f8a-41c3-afa1-41dbb034e434}', 'Отчетность', 'Отчетность', 12, 0, 'rptsvc', 0, 1, 0, 0, 0, true, null, true);

-- select * from cabinet.service order by id desc

insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('{119d807a-264a-4ffa-a42c-adf72240046e}', '1.0.0.0', '1.0.0.0', 60, 0, 0);

-- select * from cabinet.version order by service_id desc, id desc