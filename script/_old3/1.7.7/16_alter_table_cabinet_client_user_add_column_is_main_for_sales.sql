﻿/*
	ALTER TABLE cabinet.client_user ADD COLUMN is_main_for_sales
*/

ALTER TABLE cabinet.client_user ADD COLUMN is_main_for_sales boolean NOT NULL DEFAULT false;
