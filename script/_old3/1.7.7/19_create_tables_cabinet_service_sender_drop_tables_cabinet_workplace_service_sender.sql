﻿/*
	CREATE TABLE cabinet.service_sender
*/

-- DROP TABLE cabinet.service_sender;

CREATE TABLE cabinet.service_sender
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  is_disabled boolean NOT NULL DEFAULT false,
  date_beg date,
  date_end date,
  interval_in_days integer NOT NULL DEFAULT 1,
  email character varying,
  schedule_weekly character varying,
  schedule_monthly character varying,
  schedule_type integer NOT NULL DEFAULT 1,
  CONSTRAINT service_sender_pkey PRIMARY KEY (item_id),
  CONSTRAINT service_sender_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_sender_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_sender OWNER TO pavlov;


-- DROP TABLE cabinet.service_sender_log;

CREATE TABLE cabinet.service_sender_log
(
  log_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  date_beg timestamp without time zone,
  mess character varying,
  CONSTRAINT service_sender_log_pkey PRIMARY KEY (log_id),
  CONSTRAINT service_sender_log_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_sender_log_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cabinet.service_sender_log OWNER TO pavlov;

insert into cabinet.service_sender (workplace_id, service_id, is_disabled, date_beg, date_end, interval_in_days, email, schedule_weekly, schedule_monthly, schedule_type)
select workplace_id, service_id, is_disabled, date_beg, date_end, interval_in_days, email, schedule_weekly, schedule_monthly, schedule_type
from cabinet.workplace_service_sender_spec;

insert into cabinet.service_sender_log (workplace_id, service_id, date_beg, mess)
select workplace_id, service_id, date_beg, mess
from cabinet.workplace_service_sender_log_spec;

DROP TABLE cabinet.workplace_service_sender_log_spec;
DROP TABLE cabinet.workplace_service_sender_spec;