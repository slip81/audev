﻿/*
	CREATE TABLE cabinet.service_cva
*/

-- DROP TABLE cabinet.service_cva;

CREATE TABLE cabinet.service_cva
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  target_url character varying,
  pharm_login character varying,
  pharm_pwd character varying,
  send_interval integer,
  pharm_code character varying,
  active smallint NOT NULL DEFAULT 1,
  info_internet_trade character varying,
  info_name character varying,
  info_bank_cards character varying,
  info_sale_phone character varying,
  info_additional_info character varying,
  info_www character varying,
  info_city character varying,
  info_city_area character varying,
  info_address character varying,
  info_sale_shedule character varying,
  info_map_gps character varying,
  info_map_caption character varying,
  CONSTRAINT service_cva_pkey PRIMARY KEY (item_id),
  CONSTRAINT service_cva_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_cva_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_cva OWNER TO pavlov;


-- DROP TABLE cabinet.service_cva_data;

CREATE TABLE cabinet.service_cva_data
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  batch_num integer,
  batch_date timestamp without time zone,
  batch_state integer NOT NULL DEFAULT 1,
  sending_date timestamp without time zone,
  sent_date timestamp without time zone,
  mess character varying,
  CONSTRAINT service_cva_data_pkey PRIMARY KEY (item_id),
  CONSTRAINT service_cva_data_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_cva_data_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_cva_data  OWNER TO pavlov;


CREATE TABLE cabinet.service_cva_data_ext
(
  item_id integer NOT NULL,
  cva_data text,
  cva_cons_data text,
  cva_full_data text,
  CONSTRAINT service_cva_data_ext_pkey PRIMARY KEY (item_id),
  CONSTRAINT service_cva_data_ext_item_id_fkey FOREIGN KEY (item_id)
      REFERENCES cabinet.service_cva_data (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_cva_data_ext  OWNER TO pavlov;

insert into cabinet.service_cva (
  workplace_id,
  service_id,
  target_url,
  pharm_login,
  pharm_pwd,
  send_interval,
  pharm_code,
  active,
  info_internet_trade,
  info_name,
  info_bank_cards,
  info_sale_phone,
  info_additional_info,
  info_www,
  info_city,
  info_city_area,
  info_address,
  info_sale_shedule,
  info_map_gps,
  info_map_caption
)
select
  workplace_id,
  service_id,
  target_url,
  pharm_login,
  pharm_pwd,
  send_interval,
  pharm_code,
  active,
  info_internet_trade,
  info_name,
  info_bank_cards,
  info_sale_phone,
  info_additional_info,
  info_www,
  info_city,
  info_city_area,
  info_address,
  info_sale_shedule,
  info_map_gps,
  info_map_caption
from cabinet.workplace_service_cva_spec;

insert into cabinet.service_cva_data (
  workplace_id,
  service_id,
  batch_num,
  batch_date,
  batch_state,
  sending_date,
  sent_date,
  mess
)
select
  workplace_id,
  service_id,
  batch_num,
  batch_date,
  batch_state,
  sending_date,
  sent_date,
  mess
from cabinet.workplace_service_cva_data_spec;

insert into cabinet.service_cva_data_ext (
  item_id,
  cva_data,
  cva_cons_data,
  cva_full_data
)
select
  t1.item_id,
  t2.cva_data,
  t2.cva_cons_data,
  t2.cva_full_data
from cabinet.service_cva_data t1
inner join cabinet.workplace_service_cva_data_spec t2 on t1.workplace_id = t2.workplace_id and t1.service_id = t2.service_id and t1.batch_num = t2.batch_num;


-- DROP VIEW cabinet.vw_service_cva_data;

CREATE OR REPLACE VIEW cabinet.vw_service_cva_data AS 
 SELECT t2.client_id,
    t2.client_name,
    t2.sales_id,
    t2.sales_address,
    t2.description AS workplace_description,
    t1.item_id,
    t1.workplace_id,
    t1.service_id,
    t1.batch_num,
    t1.batch_date,
    t1.batch_state,
    t1.sending_date,
    t1.sent_date,
    t1.mess
   FROM cabinet.service_cva_data t1
     JOIN cabinet.vw_workplace t2 ON t1.workplace_id = t2.workplace_id;

ALTER TABLE cabinet.vw_service_cva_data OWNER TO pavlov;

-- DROP VIEW cabinet.vw_service_cva_info;

CREATE OR REPLACE VIEW cabinet.vw_service_cva_info AS 
 SELECT t1.workplace_id,
    t1.send_interval,
    max(t2.sent_date) AS last_sent_date,
    max(
        CASE
            WHEN t2.batch_state = ANY (ARRAY[1, 3]) THEN t2.batch_num
            ELSE 0
        END) AS batch_num_for_send,
    max(
        CASE
            WHEN t2.batch_state = ANY (ARRAY[1, 3]) THEN t2.batch_date
            ELSE NULL::timestamp without time zone
        END) AS batch_date_for_send,
    count(t2.sent_date) AS batch_sent_count
   FROM cabinet.service_cva t1
     LEFT JOIN cabinet.service_cva_data t2 ON t1.workplace_id = t2.workplace_id AND t1.service_id = t2.service_id
  GROUP BY t1.workplace_id, t1.send_interval;

ALTER TABLE cabinet.vw_service_cva_info OWNER TO pavlov;

DROP VIEW cabinet.vw_workplace_service_cva_info;
DROP VIEW cabinet.vw_workplace_service_cva_data;
DROP TABLE cabinet.workplace_service_cva_data_spec;
DROP TABLE cabinet.workplace_service_cva_spec;

