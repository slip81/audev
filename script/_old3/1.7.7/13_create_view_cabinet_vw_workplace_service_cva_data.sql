﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_workplace_service_cva_data
*/

-- DROP VIEW cabinet.vw_workplace_service_cva_data;

CREATE OR REPLACE VIEW cabinet.vw_workplace_service_cva_data AS 
 SELECT t2.client_id, 
  t2.client_name,
  t2.sales_id,
  t2.sales_address,
  t2.description as workplace_description,
  t1.item_id,
  t1.workplace_id,
  t1.service_id,
  t1.batch_num,
  t1.batch_date,
  t1.batch_state,
  t1.sending_date,
  t1.sent_date,
  t1.mess
  FROM cabinet.workplace_service_cva_data_spec t1
  INNER JOIN cabinet.vw_workplace t2 ON t1.workplace_id = t2.workplace_id
 ;

ALTER TABLE cabinet.vw_workplace_service_cva_data OWNER TO pavlov;
