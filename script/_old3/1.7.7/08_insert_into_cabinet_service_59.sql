﻿/*
	insert into cabinet.service
*/

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, branch_id, price, need_key, "NeedCheckVersionLicense", is_service, priority, have_spec)
values (59, '{7c62d8aa-edce-47f3-8cfe-e3c12dfd8530}', 'ЦеныВАптеках.РФ', 'Отправка данных на ЦеныВАптеках.РФ', 12, 0, 'cva', 0, 1, 0, 0, 0, true, null, true);

-- select * from cabinet.service order by id desc

insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('{6106129a-c366-47b1-b096-a1106b025a1c}', '1.0.0.0', '1.0.0.0', 59, 0, 0);

-- select * from cabinet.version order by service_id desc, id desc