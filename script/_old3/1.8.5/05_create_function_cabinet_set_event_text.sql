﻿/*
	CREATE OR REPLACE FUNCTION cabinet.set_event_text
*/

-- DROP FUNCTION cabinet.set_event_text();

CREATE OR REPLACE FUNCTION cabinet.set_event_text()
  RETURNS trigger AS
$BODY$
BEGIN
    NEW.event_name := regexp_replace(NEW.event_name, E'[\\n\\r]+', ' / ... / ', 'g' );
    NEW.event_text := regexp_replace(NEW.event_text, E'[\\n\\r]+', ' / ... / ', 'g' );
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.set_event_text() OWNER TO postgres;


-- DROP TRIGGER cabinet_crm_event_set_event_text_trg ON cabinet.crm_event;

CREATE TRIGGER cabinet_crm_event_set_event_text_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.crm_event
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_event_text();
