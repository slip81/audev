﻿/*
	UPDATE cabinet.cab_help SET url
*/

UPDATE cabinet.cab_help SET url = '/CabHelpEmpty' WHERE trim(coalesce(url, '')) = '';

UPDATE cabinet.cab_help SET url = '/StorageHelpPartial' WHERE help_id = 21;


-- select * from cabinet.cab_help order by help_id