﻿/*
	insert into cabinet.cab_help
*/


insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Личный кабинет', null, 0);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Структура', 1, 1);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Разделы', 1, 1);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Главная', 3, 2);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Услуги', 3, 2);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Клиенты', 3, 2);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Сервис', 3, 2);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Админ', 3, 2);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Мои страницы', 3, 2);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Меню пользователя', 3, 2);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Дисконтные карты', 5, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Обмен с партнерами', 5, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Брак', 5, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('ЦВА', 5, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Клиенты', 6, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Заявки на активацию', 6, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Справочники', 6, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Рассылка', 6, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('HelpDesk', 7, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Задачи', 7, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Хранилище', 7, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Журнал событий', 8, 3);

insert into cabinet.cab_help (help_name, parent_id, help_level)
values ('Полномочия', 8, 3);

-- select * from cabinet.cab_help order by help_id