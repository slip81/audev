﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_storage_file
*/

DROP VIEW cabinet.vw_storage_file;

CREATE OR REPLACE VIEW cabinet.vw_storage_file AS 
 SELECT t1.file_id,
    t1.file_name,
    t1.file_nom,
    t1.folder_id,
    t1.ord,
    t1.file_size,
    t1.physical_path,
    t1.physical_name,
    t1.download_link,
    t1.file_type_id,
    t1.file_state_id,
    t1.has_version,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.state_date,
    t1.state_user,
    t2.folder_name,
    t2.folder_level,
    t3.file_type_name,
    t3.group_id,
    t4.group_name,
    t5.file_state_name,
        CASE
            WHEN btrim(COALESCE(t4.img, ''::character varying)::text) = ''::text THEN 'storage storage-file'::text
            ELSE 'storage storage-'::text || btrim(t4.img::text)
        END::character varying AS sprite_css_class,
    (((((t1.file_name::text || '.'::text) || t3.file_type_name::text) || '  /  '::text) || btrim(to_char(t1.file_size::bigint, '9G999G999G999G999G999'::text))) || ' байт'::text)::character varying AS file_name_full,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str,
    to_char(t1.upd_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS upd_date_str,
    to_char(t1.state_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS state_date_str,
    (btrim(to_char(t1.file_size::bigint, '9G999G999G999G999G999'::text)) || ' байт'::text)::character varying AS file_size_str,
    ((btrim(t1.file_name::text) || '.'::text) || btrim(t3.file_type_name::text))::character varying AS file_name_with_extention,
        CASE
            WHEN t11.file_version_id IS NOT NULL THEN true
            ELSE false
        END AS v1_exists,
    t11.file_version_id AS v1_file_version_id,
    t11.file_size AS v1_file_size,
    t11.physical_path AS v1_physical_path,
    t11.physical_name AS v1_physical_name,
    t11.download_link AS v1_download_link,
    t11.crt_date AS v1_crt_date,
    t11.crt_user AS v1_crt_user,
    (((((t1.file_name::text || '.'::text) || t3.file_type_name::text) || '  /  '::text) || btrim(to_char(t11.file_size::bigint, '9G999G999G999G999G999'::text))) || ' байт'::text)::character varying AS v1_file_name_full,
    to_char(t11.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS v1_crt_date_str,
    to_char(t11.upd_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS v1_upd_date_str,
    (btrim(to_char(t11.file_size::bigint, '9G999G999G999G999G999'::text)) || ' байт'::text)::character varying AS v1_file_size_str,
    t11.version_num_actual AS v1_version_num_actual,
        CASE
            WHEN t12.file_version_id IS NOT NULL THEN true
            ELSE false
        END AS v2_exists,
    t12.file_version_id AS v2_file_version_id,
    t12.file_size AS v2_file_size,
    t12.physical_path AS v2_physical_path,
    t12.physical_name AS v2_physical_name,
    t12.download_link AS v2_download_link,
    t12.crt_date AS v2_crt_date,
    t12.crt_user AS v2_crt_user,
    (((((t1.file_name::text || '.'::text) || t3.file_type_name::text) || '  /  '::text) || btrim(to_char(t11.file_size::bigint, '9G999G999G999G999G999'::text))) || ' байт'::text)::character varying AS v2_file_name_full,
    to_char(t12.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS v2_crt_date_str,
    to_char(t12.upd_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS v2_upd_date_str,
    (btrim(to_char(t11.file_size::bigint, '9G999G999G999G999G999'::text)) || ' байт'::text)::character varying AS v2_file_size_str,
    t12.version_num_actual AS v2_version_num_actual,
    false AS is_local_link,
    t1.file_date,
    to_char(t1.file_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS file_date_str,
    1 AS item_type
   FROM cabinet.storage_file t1
     JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
     JOIN cabinet.storage_file_type t3 ON t1.file_type_id = t3.file_type_id
     JOIN cabinet.storage_file_type_group t4 ON t3.group_id = t4.group_id
     JOIN cabinet.storage_file_state t5 ON t1.file_state_id = t5.file_state_id
     LEFT JOIN cabinet.storage_file_version t11 ON t1.file_id = t11.file_id AND t11.version_num = 1
     LEFT JOIN cabinet.storage_file_version t12 ON t1.file_id = t12.file_id AND t12.version_num = 2
  WHERE t1.is_deleted = false
UNION ALL
 SELECT t1.link_id AS file_id,
    t1.link_name AS file_name,
    0 AS file_nom,
    t1.folder_id,
    0 AS ord,
    ''::character varying AS file_size,
    ''::character varying AS physical_path,
    ''::character varying AS physical_name,
    t1.download_link,
    0 AS file_type_id,
    0 AS file_state_id,
    false AS has_version,
    t1.crt_date,
    t1.crt_user,
    NULL::timestamp without time zone AS upd_date,
    ''::character varying AS upd_user,
    NULL::timestamp without time zone AS state_date,
    ''::character varying AS state_user,
    t2.folder_name,
    t2.folder_level,
    'url'::character varying AS file_type_name,
    0 AS group_id,
    'Внешние ссылки'::character varying AS group_name,
    ''::character varying AS file_state_name,
    'storage storage-url'::character varying AS sprite_css_class,
    t1.link_name AS file_name_full,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str,
    ''::text AS upd_date_str,
    ''::text AS state_date_str,
    ''::character varying AS file_size_str,
    t1.link_name AS file_name_with_extention,
    false AS v1_exists,
    NULL::integer AS v1_file_version_id,
    ''::character varying AS v1_file_size,
    ''::character varying AS v1_physical_path,
    ''::character varying AS v1_physical_name,
    ''::character varying AS v1_download_link,
    NULL::timestamp without time zone AS v1_crt_date,
    ''::character varying AS v1_crt_user,
    ''::character varying AS v1_file_name_full,
    ''::text AS v1_crt_date_str,
    ''::text AS v1_upd_date_str,
    ''::character varying AS v1_file_size_str,
    0 AS v1_version_num_actual,
    false AS v2_exists,
    NULL::integer AS v2_file_version_id,
    ''::character varying AS v2_file_size,
    ''::character varying AS v2_physical_path,
    ''::character varying AS v2_physical_name,
    ''::character varying AS v2_download_link,
    NULL::timestamp without time zone AS v2_crt_date,
    ''::character varying AS v2_crt_user,
    ''::character varying AS v2_file_name_full,
    ''::text AS v2_crt_date_str,
    ''::text AS v2_upd_date_str,
    ''::character varying AS v2_file_size_str,
    0 AS v2_version_num_actual,
        CASE
            WHEN t1.download_link::text ~~ 'file:%'::text THEN true
            ELSE false
        END AS is_local_link,
    NULL::timestamp without time zone AS file_date,
    ''::character varying AS file_date_str,
    2 AS item_type
   FROM cabinet.storage_link t1
     JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
  WHERE t1.is_deleted = false;

ALTER TABLE cabinet.vw_storage_file OWNER TO pavlov;
