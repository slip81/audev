﻿/*
	CREATE TABLE cabinet.cab_help
*/

-- DROP TABLE cabinet.cab_help;

CREATE TABLE cabinet.cab_help
(
  help_id serial NOT NULL,
  help_name character varying,
  parent_id integer,
  help_level integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),  
  CONSTRAINT cab_help_pkey PRIMARY KEY (help_id),
  CONSTRAINT cab_help_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES cabinet.cab_help (help_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_help OWNER TO pavlov;

CREATE TRIGGER cabinet_cab_help_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.cab_help
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();
