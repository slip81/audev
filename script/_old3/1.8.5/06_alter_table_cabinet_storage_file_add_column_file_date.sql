﻿/*
	ALTER TABLE cabinet.storage_file ADD COLUMN file_date
*/

ALTER TABLE cabinet.storage_file ADD COLUMN file_date timestamp without time zone;