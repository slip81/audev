﻿/*
	ALTER TABLE cabinet.cab_help ALTER COLUMN help_id TYPE integer
*/

ALTER TABLE cabinet.cab_help ALTER COLUMN help_id TYPE integer;
ALTER TABLE cabinet.cab_help ALTER COLUMN help_id DROP DEFAULT;