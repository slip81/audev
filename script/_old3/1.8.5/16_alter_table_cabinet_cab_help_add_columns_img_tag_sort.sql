﻿/*
	ALTER TABLE cabinet.cab_help ADD COLUMNs img, tag, sort
*/

ALTER TABLE cabinet.cab_help ADD COLUMN img character varying;
ALTER TABLE cabinet.cab_help ADD COLUMN tag character varying;
ALTER TABLE cabinet.cab_help ADD COLUMN sort integer;

UPDATE cabinet.cab_help
SET sort = help_id;

UPDATE cabinet.cab_help
SET img = 'storage storage-folder'
, tag = help_name;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-start'
WHERE help_id = 4;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-accom'
WHERE help_id = 5;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-clients-manage'
WHERE help_id = 6;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-service'
WHERE help_id = 7;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-admin'
WHERE help_id = 8;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-my'
WHERE help_id = 9;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-user-profile'
WHERE help_id = 10;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-card'
WHERE help_id = 11;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-exchange'
WHERE help_id = 12;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-defect'
WHERE help_id = 13;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-cva'
WHERE help_id = 14;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-clients'
WHERE help_id = 15;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-user-reg'
WHERE help_id = 16;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-cab-sprav'
WHERE help_id = 17;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-delivery'
WHERE help_id = 18;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-helpdesk'
WHERE help_id = 19;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-task'
WHERE help_id = 20;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-storage-folder-tree'
WHERE help_id = 21;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-log'
WHERE help_id = 22;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-auth'
WHERE help_id = 23;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-user-profile-edit'
WHERE help_id = 24;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-user-profile-edit'
WHERE help_id = 25;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-change'
WHERE help_id = 26;

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-exit'
WHERE help_id = 27;

-- SELECT * FROM cabinet.cab_help ORDER BY help_id;
