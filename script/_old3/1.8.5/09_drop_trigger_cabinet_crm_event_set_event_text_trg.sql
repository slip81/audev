﻿/*
	DROP TRIGGER cabinet_crm_event_set_event_text_trg
*/

DROP TRIGGER cabinet_crm_event_set_event_text_trg ON cabinet.crm_event;

DROP FUNCTION cabinet.set_event_text();
update cabinet.crm_event
set event_name = replace(event_name, '/ ... /', E'\n');

update cabinet.crm_event
set event_text = replace(event_text, '/ ... /', E'\n');


-- select * from cabinet.crm_event order by event_id desc limit 100

