﻿/*
	CREATE OR REPLACE FUNCTION discount.clear_card_trans_sum_and_percent
*/

-- DROP FUNCTION discount.clear_card_trans_sum_and_percent(bigint, bigint);

CREATE OR REPLACE FUNCTION discount.clear_card_trans_sum_and_percent(
    IN _business_id bigint,
    IN _card_type_id bigint,
    OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN
	result := 0;    

	update discount.card
	set curr_trans_sum = 0, curr_trans_count = 0
	where business_id = _business_id
	and curr_card_type_id = _card_type_id;

	update discount.card_nominal
	set date_end = current_date
	from discount.card t2
	where discount.card_nominal.card_id = t2.card_id
	and t2.business_id = _business_id
	and t2.curr_card_type_id = _card_type_id
	and discount.card_nominal.date_end is null
	and discount.card_nominal.unit_type = 2;

	insert into discount.card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
	select card_id, current_date, 2, 0, current_timestamp, 'pavlov'
	from discount.card
	where business_id = _business_id
	and curr_card_type_id = _card_type_id;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.clear_card_trans_sum_and_percent(bigint, bigint) OWNER TO pavlov;
