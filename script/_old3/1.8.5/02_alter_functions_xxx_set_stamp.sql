﻿/*
	CREATE OR REPLACE FUNCTION xxx.set_stamp()
*/

-- DROP FUNCTION cabinet.set_stamp();

CREATE OR REPLACE FUNCTION cabinet.set_stamp()
  RETURNS trigger AS
$BODY$
BEGIN
    NEW.sysrowstamp := (extract(epoch from clock_timestamp()) * 1000)::bigint;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.set_stamp() OWNER TO postgres;


/* ------------------------------- */

-- DROP FUNCTION discount.set_stamp();

CREATE OR REPLACE FUNCTION discount.set_stamp()
  RETURNS trigger AS
$BODY$
BEGIN
    NEW.sysrowstamp := (extract(epoch from clock_timestamp()) * 1000)::bigint;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.set_stamp() OWNER TO postgres;
