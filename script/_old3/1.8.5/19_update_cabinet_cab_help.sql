﻿/*
	UPDATE cabinet.cab_help
*/

UPDATE cabinet.cab_help
SET img = 'cab-png cab-png-main'
WHERE help_id = 1;

UPDATE cabinet.cab_help
SET url = '/MainHelpPartial'
WHERE help_id = 4;

UPDATE cabinet.cab_help
SET url = '/ServiceHelpPartial'
WHERE help_id = 5;

UPDATE cabinet.cab_help
SET url = '/ClientHelpPartial'
WHERE help_id = 6;

UPDATE cabinet.cab_help
SET url = '/ServHelpPartial'
WHERE help_id = 7;

UPDATE cabinet.cab_help
SET url = '/AdminHelpPartial'
WHERE help_id = 8;

UPDATE cabinet.cab_help
SET url = '/MyPagesHelpPartial'
WHERE help_id = 9;

UPDATE cabinet.cab_help
SET url = '/UserProfileHelpPartial'
WHERE help_id = 10;

UPDATE cabinet.cab_help
SET url = '/DiscountHelpPartial'
WHERE help_id = 11;

UPDATE cabinet.cab_help
SET url = '/ExchangeHelpPartial'
WHERE help_id = 12;

UPDATE cabinet.cab_help
SET url = '/DefectHelpPartial'
WHERE help_id = 13;

UPDATE cabinet.cab_help
SET url = '/CvaHelpPartial'
WHERE help_id = 14;

UPDATE cabinet.cab_help
SET url = '/ClientListHelpPartial'
WHERE help_id = 15;

UPDATE cabinet.cab_help
SET url = '/RegHelpPartial'
WHERE help_id = 16;

UPDATE cabinet.cab_help
SET url = '/CabSpravHelpPartial'
WHERE help_id = 17;

UPDATE cabinet.cab_help
SET url = '/DeliveryHelpPartial'
WHERE help_id = 18;

UPDATE cabinet.cab_help
SET url = '/HelpDeskHelpPartial'
WHERE help_id = 19;

UPDATE cabinet.cab_help
SET url = '/CrmTaskHelpPartial'
WHERE help_id = 20;

UPDATE cabinet.cab_help
SET url = '/StorageHelpPartial'
WHERE help_id = 21;

UPDATE cabinet.cab_help
SET url = '/LogHelpPartial'
WHERE help_id = 22;

UPDATE cabinet.cab_help
SET url = '/UserProfileHelpPartial'
WHERE help_id = 24;

UPDATE cabinet.cab_help
SET url = '/UserProfileHelpPartial'
WHERE help_id = 25;

UPDATE cabinet.cab_help
SET url = '/ChangePasswordHelpPartial'
WHERE help_id = 26;

UPDATE cabinet.cab_help
SET url = '/LogoutHelpPartial'
WHERE help_id = 27;

-- select * from cabinet.cab_help order by help_level, sort