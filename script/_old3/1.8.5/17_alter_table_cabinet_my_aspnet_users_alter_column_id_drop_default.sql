﻿/*
	ALTER TABLE cabinet.my_aspnet_users ALTER COLUMN id DROP DEFAULT
*/

ALTER TABLE cabinet.my_aspnet_users ALTER COLUMN id DROP DEFAULT;