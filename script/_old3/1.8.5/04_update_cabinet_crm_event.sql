﻿/*
	update cabinet.crm_event
*/

-- select * from cabinet.crm_event where event_id = 375

update cabinet.crm_event
set event_name = regexp_replace(event_name, E'[\\n\\r]+', ' / ... / ', 'g' ),
event_text =  regexp_replace(event_text, E'[\\n\\r]+', ' / ... / ', 'g' )
;
--where event_id = 375

/*
select * from cabinet.crm_event where event_id = 375

-- "Тест 1 Тест 1. Тест 2"

select regexp_replace(event_name, E'[\\n\\r]+', '<br/>', 'g' ), * from cabinet.crm_event where event_id = 375
select regexp_replace(event_name, E'[\\n\\r]+', '\\\\n\\\\r', 'g' ), * from cabinet.crm_event where event_id = 375
select regexp_replace(event_name, E'[\\n\\r]+', '             ', 'g' ), * from cabinet.crm_event where event_id = 375

update cabinet.crm_event
set event_name = regexp_replace(event_name, E'[\\n\\r]+', '&ltbr/&gt', 'g' ),
event_text =  regexp_replace(event_text, E'[\\n\\r]+', '&ltbr/&gt', 'g' )
where event_id = 375

update cabinet.crm_event
set event_name = regexp_replace(event_name, E'[\\n\\r]+', '\\\\n\\\\r', 'g' ),
event_text =  regexp_replace(event_text, E'[\\n\\r]+', '\\\\n\\\\r', 'g' )
where event_id = 375

update cabinet.crm_event
set event_name =  regexp_replace(event_name, E'[\\n\\r]+', '             ', 'g' ),
event_text =   regexp_replace(event_text, E'[\\n\\r]+', '             ', 'g' )
where event_id = 375
*/

/*
update cabinet.crm_event
set event_name =  regexp_replace(event_name, E'[\\n\\r]+', '[]             []', 'g' ),
event_text =   regexp_replace(event_text, E'[\\n\\r]+', '[]             []', 'g' )
--where event_id = 375
*/


/*
update cabinet.crm_event
set event_name = 'Тест 1 Тест 1. Тест 2',
event_text =  'Тест 1 Тест 1. Тест 2'
where event_id = 375

update cabinet.crm_event
set event_name = 'Тест 1 Тест 1.
Тест 2',
event_text = 'Тест 1 Тест 1.
Тест 2'
where event_id = 375
*/