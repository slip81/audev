﻿/*
	CREATE OR REPLACE VIEW discount.vw_card_transaction
*/

-- DROP VIEW discount.vw_card_transaction;

CREATE OR REPLACE VIEW discount.vw_card_transaction AS 
 SELECT t1.card_trans_id,
    t1.card_id,
    t1.trans_num,
    t1.date_beg,
    t1.trans_sum,
    t1.trans_kind,
    t1.status,
    t1.canceled_trans_id,
    t1.programm_result_id,
    t1.user_name,
    t1.date_check,
    t2.business_id,
    t1.status_date,
    t1.is_delayed,
    t2.curr_trans_sum AS curr_card_sum,
    t2.card_num,
    t2.card_num2,
    ''::character varying AS curr_card_type_name,
    date(t1.date_check) AS date_check_date_only,
    0::numeric AS curr_card_bonus_value,
    0::numeric AS curr_card_percent_value,
    0::numeric AS trans_card_bonus_value,
    0::numeric AS trans_card_percent_value,
    sum(t6.unit_value_discount) AS trans_sum_discount,
    sum(t6.unit_value_with_discount) AS trans_sum_with_discount,
    sum(t6.unit_value_bonus_for_card) AS trans_sum_bonus_for_card,
    sum(t6.unit_value_bonus_for_pay) AS trans_sum_bonus_for_pay,
    y2.org_name
   FROM discount.card_transaction t1
     JOIN discount.card t2 ON t1.card_id = t2.card_id
     LEFT JOIN discount.business_org_user y1 ON btrim(lower(COALESCE(y1.user_name, ''::character varying)::text)) = btrim(lower(COALESCE(t1.user_name, ''::character varying)::text))
     LEFT JOIN discount.business_org y2 ON y1.org_id = y2.org_id
     LEFT JOIN discount.card_transaction_detail t6 ON t1.card_trans_id = t6.card_trans_id AND t6.unit_type = 0
  GROUP BY t1.card_trans_id, t1.card_id, t1.trans_num, t1.date_beg, t1.trans_sum, t1.trans_kind, t1.status, t1.canceled_trans_id, t1.programm_result_id, t1.user_name, t1.date_check, t2.business_id, t2.curr_trans_sum, t2.card_num, t2.card_num2, y2.org_name;

ALTER TABLE discount.vw_card_transaction OWNER TO pavlov;
