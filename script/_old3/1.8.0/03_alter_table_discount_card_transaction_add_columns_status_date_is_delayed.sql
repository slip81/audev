﻿/*
	ALTER TABLE discount.card_transaction ADD COLUMN status_date, is_delayed
*/

ALTER TABLE discount.card_transaction ADD COLUMN status_date timestamp without time zone;
ALTER TABLE discount.card_transaction ADD COLUMN is_delayed smallint;

UPDATE discount.card_transaction
SET status_date = date_beg
WHERE status_date IS NULL;

