﻿/*
	CREATE OR REPLACE FUNCTION cabinet.del_clients_without_sales
*/

-- DROP FUNCTION cabinet.del_clients_without_sales(text);

CREATE OR REPLACE FUNCTION cabinet.del_clients_without_sales(_schema_name text = 'cabinet')
  RETURNS integer AS
$BODY$
DECLARE
 _client_id integer; 
 _result integer;
BEGIN
  _result := 0;

  -- SET search_path TO _schema_name;
  -- SET search_path TO cabinet;
  EXECUTE 'SET search_path TO ' || _schema_name;

  FOR _client_id IN
    select distinct t1.id
    from client t1
    where not exists(select x1.id from sales x1 where x1.client_id = t1.id)
  LOOP
    perform cabinet.del_client(_client_id, _schema_name);
    _result := _result + 1;
  END LOOP;
  
  return _result;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.del_clients_without_sales(text) OWNER TO pavlov;
