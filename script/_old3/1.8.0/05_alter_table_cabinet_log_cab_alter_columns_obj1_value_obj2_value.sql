﻿/*
	ALTER TABLE cabinet.log_cab ALTER COLUMN obj1_value, obj2_value
*/

ALTER TABLE cabinet.log_cab ALTER COLUMN obj1_value TYPE text;
ALTER TABLE cabinet.log_cab ALTER COLUMN obj2_value TYPE text;