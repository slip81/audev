﻿/*
	CREATE OR REPLACE VIEW discount.vw_report_unused_cards
*/

-- DROP VIEW discount.vw_report_unused_cards;

CREATE OR REPLACE VIEW discount.vw_report_unused_cards AS 
 SELECT row_number() OVER (ORDER BY t2.business_id) AS id,
    t2.business_id,
    t2.card_id,
    t2.card_num,
    t2.curr_discount_percent,
    t2.curr_bonus_percent,
    t2.curr_bonus_value,
    t2.curr_trans_sum,
    t5.card_holder_surname AS card_holder_name,
    t1.last_date_check
	from
	(
	select x1.card_id, max(x2.date_check) as last_date_check
	from discount.card x1
	left join discount.card_transaction x2 on x1.card_id = x2.card_id
	and x2.trans_kind = 1 and x2.status = 0
	group by x1.card_id
	) t1
	inner join discount.vw_card t2 on t1.card_id = t2.card_id
	LEFT JOIN discount.card_holder t5 ON t2.curr_holder_id = t5.card_holder_id
  ;  

ALTER TABLE discount.vw_report_unused_cards OWNER TO pavlov;
