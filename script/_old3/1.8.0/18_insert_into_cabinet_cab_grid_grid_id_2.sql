﻿/*
	insert into cabinet.cab_grid grid_id 2
*/


insert into cabinet.cab_grid (grid_id, grid_name)
values (2, 'clientGrid');

SELECT setval('cabinet.cab_grid_column_column_id_seq', (select max(column_id) from cabinet.cab_grid_column));

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'id', 'Код', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'client_name', 'Название организации', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'phone', 'Телефон', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'site', 'Электронная почта', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'contact_person', 'Контактное лицо', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'is_phis_str', 'Физ. лицо', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'inn', 'ИНН', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'city_name', 'Город', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'region_name', 'Регион', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'legal_address', 'Юр. адрес', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'absolute_address', 'Адрес', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'reg_date', 'Дата регистрации', '{0:dd.MM.yyyy HH:mm}');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'last_activity_date', 'Дата активности', '{0:dd.MM.yyyy HH:mm}');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'created_by', 'Создал', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'is_activated_str', 'Активирован', null);

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus, format)
values (2, 'sales_count', 'Кол-во точек', null);


-- select * from cabinet.cab_grid
-- select * from cabinet.cab_grid_column where grid_id = 2 order by column_id;