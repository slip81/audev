﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_event_overdue_dates
*/

-- DROP VIEW cabinet.vw_crm_event_overdue_dates;

CREATE OR REPLACE VIEW cabinet.vw_crm_event_overdue_dates AS 
 SELECT row_number() OVER (ORDER BY t1.event_date) AS id,
    t1.event_date
   FROM ( SELECT DISTINCT vw_crm_event.date_beg AS event_date
           FROM cabinet.vw_crm_event
          WHERE vw_crm_event.is_overdue = true
        UNION
         SELECT DISTINCT vw_crm_event.date_end AS event_date
           FROM cabinet.vw_crm_event
          WHERE vw_crm_event.is_overdue = true
        UNION
         SELECT 'now'::text::date AS event_date
        UNION
         SELECT 'now'::text::date + 1 AS event_date
        UNION
         SELECT 'now'::text::date + 2 AS event_date
        UNION
         SELECT 'now'::text::date + 3 AS event_date) t1
  ORDER BY t1.event_date;

ALTER TABLE cabinet.vw_crm_event_overdue_dates OWNER TO pavlov;
