﻿/*
	ALTER TABLE cabinet._transfer_log ADD COLUMNs
*/

ALTER TABLE cabinet._transfer_log ADD COLUMN max_order_id integer;
ALTER TABLE cabinet._transfer_log ADD COLUMN max_order_item_id integer;
ALTER TABLE cabinet._transfer_log ADD COLUMN max_license_id integer;
ALTER TABLE cabinet._transfer_log ADD COLUMN max_service_registration_id integer;
