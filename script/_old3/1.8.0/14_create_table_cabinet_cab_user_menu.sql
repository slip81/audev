﻿/*
	CREATE TABLE cabinet.cab_user_menu
*/

-- DROP TABLE cabinet.cab_user_menu;

CREATE TABLE cabinet.cab_user_menu
(
  menu_id serial NOT NULL,
  user_name character varying,
  sort_num integer NOT NULL DEFAULT 0,
  title character varying,
  controller character varying,
  action character varying,
  img character varying,  
  CONSTRAINT cab_user_menu_pkey PRIMARY KEY (menu_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_user_menu OWNER TO pavlov;
