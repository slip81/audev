﻿/*
	CREATE TABLE cabinet._transfer_log
*/

-- DROP TABLE cabinet._transfer_log;

CREATE TABLE cabinet._transfer_log
(
  log_id serial NOT NULL,
  crt_date timestamp without time zone,
  max_client_id integer,
  max_sales_id integer,
  max_workplace_id integer,
  mess text,
  CONSTRAINT _transfer_log_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet._transfer_log OWNER TO pavlov;
