﻿/*
	CREATE TABLE cabinet.crm_task_file
*/

-- DROP TABLE cabinet.crm_task_file;

CREATE TABLE cabinet.crm_task_file
(
  file_id serial NOT NULL,
  task_id integer NOT NULL,
  subtask_id integer,
  crt_date timestamp without time zone,
  owner_user_id integer NOT NULL,  
  file_name character varying,
  file_path character varying,
  CONSTRAINT crm_task_file_pkey PRIMARY KEY (file_id),
  CONSTRAINT crm_task_file_owner_user_id_fkey FOREIGN KEY (owner_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_file_subtask_id_fkey FOREIGN KEY (subtask_id)
      REFERENCES cabinet.crm_subtask (subtask_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_file_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_file OWNER TO pavlov;
