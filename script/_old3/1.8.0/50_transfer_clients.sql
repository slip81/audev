﻿/*
	transfer clients
*/

delete from cabinet._transfer_log;

select count(*) from cabinet2.client;
-- 733

select cabinet.del_clients_without_sales('cabinet2');


select count(*) from cabinet.client;
-- 685

select *
from cabinet.client
order by id desc limit 100

select *
from cabinet2.client
order by id desc limit 100

-- select cabinet.del_client(4993);
-- select cabinet.del_client(4998);


select t1.*
from cabinet2.client t1
where t1.id > (select max(x1.id) from cabinet.client x1)
order by t1.id desc;
- 47

/*
	**********************
	Часть 1. Добавление новых клиентов
	**********************
*/

-- insert city

select * from cabinet2.city order by id desc
-- 217 (max id 240)
select * from cabinet.city order by id desc
-- 216 (max id 239)

-- delete from cabinet.city where id > 239;

insert into cabinet.city (id, guid, name, region_id, is_deleted, is_capital)
select id, guid, name, region_id, is_deleted, is_capital
from cabinet2.city
where id > (select max(x1.id) from cabinet.city x1);

-- insert version

insert into cabinet.version (id, guid, name, description, service_id, is_deleted, file_size, file_path_id)
select distinct t1.version_id, t2.guid, t2.name, t2.description, t2.service_id, t2.is_deleted, t2.file_size, t2.file_path_id
from cabinet2.order_item t1
inner join cabinet2.version t2 on t1.version_id = t2.id
where t1.version_id is not null
and not exists (select x1.id from cabinet.version x1 where t1.version_id = x1.id);

-- insert client

insert into cabinet._transfer_log (crt_date, max_client_id)
values (current_timestamp, (select max(x1.id) from cabinet.client x1));

-- select * from cabinet._transfer_log;
-- max_client_id 4990

insert into cabinet.client (
  id,
  guid,
  name,
  city_id,
  full_name,
  inn,
  kpp,
  legal_address,
  absolute_address,
  organisation_type_id,
  foundation_document,
  bank,
  city_bank,
  bik,
  loro_account,
  settlement_account,
  cell,
  contact_person,
  user_id,
  is_deleted,
  zip_code,
  post,
  person_nominative_case,
  person_genitive_case,
  created_on,
  created_by,
  updated_on,
  updated_by,
  license_number,
  license_date,
  sm_id,
  activated_on,
  activated_by,
  branch_id,
  site,
  client_role,
  summary_order_id,
  dt_sm_sync,
  balance,
  is_in_summary_order,
  hardware_id,
  summary_order_settings_id,
  is_phis,
  is_moderated,
  moderated_by,
  moderated_on,
  send_notification,
  profession_id,
  dt_birth,
  profession_name
  )
select
  id,
  guid,
  name,
  city_id,
  full_name,
  inn,
  kpp,
  legal_address,
  absolute_address,
  organisation_type_id,
  foundation_document,
  bank,
  city_bank,
  bik,
  loro_account,
  settlement_account,
  cell,
  contact_person,
  user_id,
  is_deleted,
  zip_code,
  post,
  person_nominative_case,
  person_genitive_case,
  created_on,
  created_by,
  updated_on,
  updated_by,
  license_number,
  license_date,
  sm_id,
  activated_on,
  activated_by,
  branch_id,
  site,
  client_role,
  summary_order_id,
  dt_sm_sync,
  balance,
  is_in_summary_order,
  hardware_id,
  summary_order_settings_id,
  is_phis,
  is_moderated,
  moderated_by,
  moderated_on,
  send_notification,
  profession_id,
  dt_birth,
  profession_name
from cabinet2.client
where id > (select max_client_id from cabinet._transfer_log)
;


-- insert sales

update cabinet._transfer_log
set max_sales_id = (select max(id) from cabinet.sales);

-- select * from cabinet._transfer_log;
-- max_client_id 2045

select * from cabinet.sales order by id desc limit 10
select * from cabinet2.sales order by id desc;

insert into cabinet.sales (
  id,
  guid,
  name,
  client_id,
  city_id,
  cell,
  contact_person,
  work_mode,
  adress,
  is_deleted,
  sm_id,
  shedule_id,
  district_id,
  street_id,
  pharmacy_type_id,
  brand,
  email,
  site,
  location_map,
  is_published,
  image_path,
  map_code,
  map_image_path,
  hardware_id,
  prep_limit,
  sales_role,
  is_in_summary_order,
  summary_order_settings_id,
  user_id
)
select
  t1.id,
  t1.guid,
  t1.name,
  t1.client_id,
  t1.city_id,
  t1.cell,
  t1.contact_person,
  t1.work_mode,
  t1.adress,
  t1.is_deleted,
  t1.sm_id,
  t1.shedule_id,
  t1.district_id,
  t1.street_id,
  t1.pharmacy_type_id,
  t1.brand,
  t1.email,
  t1.site,
  t1.location_map,
  t1.is_published,
  t1.image_path,
  t1.map_code,
  t1.map_image_path,
  t1.hardware_id,
  t1.prep_limit,
  t1.sales_role,
  t1.is_in_summary_order,
  t1.summary_order_settings_id,
  t1.user_id
from cabinet2.sales t1
inner join cabinet2.client t2 on t1.client_id = t2.id
where t2.id > (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.sales x1 where x1.id = t1.id)
;

-- insert workplace

update cabinet._transfer_log
set max_workplace_id = (select max(id) from cabinet.workplace);

-- select * from cabinet._transfer_log;
-- max_workplace_id 4622

insert into cabinet.workplace (
  id,
  guid,
  registration_key,
  description,
  sales_id,
  is_deleted,
  account
)
select
  t1.id,
  t1.guid,
  t1.registration_key,
  t1.description,
  t1.sales_id,
  t1.is_deleted,
  t1.account
from cabinet2.workplace t1
inner join cabinet2.sales t2 on t1.sales_id = t2.id
inner join cabinet2.client t3 on t2.client_id = t3.id
where t3.id > (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.workplace x1 where x1.id = t1.id)
;


-- insert order

update cabinet._transfer_log
set max_order_id = (select max(id) from cabinet."order");

-- select * from cabinet._transfer_log;
-- max_order_id 5863

insert into cabinet."order" (
  id,
  guid,
  client_id,
  answer_set_id,
  created_on,
  created_by,
  updated_on,
  updated_by,
  is_deleted,
  commentary,
  commentary_manager,
  order_status,
  is_conformed,
  active_status,
  order_name
)
select
  t1.id,
  t1.guid,
  t1.client_id,
  t1.answer_set_id,
  t1.created_on,
  t1.created_by,
  t1.updated_on,
  t1.updated_by,
  t1.is_deleted,
  t1.commentary,
  t1.commentary_manager,
  t1.order_status,
  t1.is_conformed,
  t1.active_status,
  null
from cabinet2."order" t1
inner join cabinet2.client t2 on t1.client_id = t2.id
where t2.id > (select max_client_id from cabinet._transfer_log)
--order by t1.id
and not exists(select x1.id from cabinet."order" x1 where x1.id = t1.id)
;

-- insert order_item

update cabinet._transfer_log
set max_order_item_id = (select max(id) from cabinet.order_item);

-- select * from cabinet._transfer_log;
-- max_order_item_id 14794

insert into cabinet.order_item (
  id,
  guid,
  service_id,
  version_id,
  order_id,
  quantity,
  is_deleted,
  additional_data
)
select
  t1.id,
  t1.guid,
  t1.service_id,
  t1.version_id,
  t1.order_id,
  t1.quantity,
  t1.is_deleted,
  t1.additional_data
from cabinet2.order_item t1
inner join cabinet2."order" t2 on t1.order_id = t2.id
inner join cabinet2.client t3 on t2.client_id = t3.id
where t3.id > (select max_client_id from cabinet._transfer_log)
-- order by t1.id
and not exists(select x1.id from cabinet.order_item x1 where x1.id = t1.id)
;

-- insert license

update cabinet._transfer_log
set max_license_id = (select max(id) from cabinet.license);

-- select * from cabinet._transfer_log;
-- max_license_id 31717

insert into cabinet.license (
  id,
  guid,
  order_item_id,
  service_registration_id,
  created_on,
  created_by,
  updated_on,
  updated_by,
  is_deleted,
  dt_start,
  dt_end
)
select
  t1.id,
  t1.guid,
  t1.order_item_id,
  null,
  t1.created_on,
  t1.created_by,
  t1.updated_on,
  t1.updated_by,
  t1.is_deleted,
  t1.dt_start,
  t1.dt_end
from cabinet2.license t1
inner join cabinet2.order_item t2 on t1.order_item_id = t2.id
inner join cabinet2."order" t3 on t2.order_id = t3.id
inner join cabinet2.client t4 on t3.client_id = t4.id
where t4.id > (select max_client_id from cabinet._transfer_log)
-- order by t1.id
;


-- insert service_registration

update cabinet._transfer_log
set max_service_registration_id = (select max(id) from cabinet.service_registration);

-- select * from cabinet._transfer_log;
-- max_service_registration_id 20492

insert into cabinet.service_registration (
  id,
  guid,
  activation_key,
  workplace_id,
  license_id,
  created_on,
  created_by,
  updated_on,
  updated_by,
  is_deleted
)
select
  t1.id,
  t1.guid,
  t1.activation_key,
  t1.workplace_id,
  t1.license_id,
  t1.created_on,
  t1.created_by,
  t1.updated_on,
  t1.updated_by,
  t1.is_deleted
from cabinet2.service_registration t1
inner join cabinet2.license t2 on t1.license_id = t2.id
inner join cabinet2.order_item t3 on t2.order_item_id = t3.id
inner join cabinet2."order" t4 on t3.order_id = t4.id
inner join cabinet2.client t5 on t4.client_id = t5.id
where t5.id > (select max_client_id from cabinet._transfer_log)
-- order by t1.id
;

update cabinet.license t1
set service_registration_id = 
(
select max(x1.id )
from cabinet.service_registration x1 
inner join cabinet.license x2 on x1.license_id = x2.id
inner join cabinet.order_item x3 on x2.order_item_id = x3.id
where x1.license_id = t1.id and x2.order_item_id = t2.id
)
-- set t1.service_registration_id = null
from cabinet.order_item t2
where t1.order_item_id = t2.id
and t1.id > (select max_license_id from cabinet._transfer_log);

-- insert client_service

insert into cabinet.client_service (client_id, service_id, state)
select distinct client_id, service_id, 0
from cabinet.vw_client_service
where client_id > (select max_client_id from cabinet._transfer_log);

-- insert client_user

insert into cabinet.client_user (user_id, client_id, sales_id, user_name, user_name_full, is_main_for_sales)
select t1.user_id, max(t1.client_id), max(t1.id), max(t2.name), max(t2.name), true
from cabinet.sales t1
inner join cabinet.my_aspnet_users t2 on t1.user_id = t2.id
where t1.user_id is not null
and not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = t1.user_id)
and t1.client_id > (select max_client_id from cabinet._transfer_log)
group by t1.user_id;

insert into cabinet.client_user (user_id, client_id, sales_id, user_name, user_name_full, is_main_for_sales)
select t1.user_id, t1.id, null, t2.name, t2.name, false
from cabinet.client t1
inner join cabinet.my_aspnet_users t2 on t1.user_id = t2.id
where t1.user_id is not null
and not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = t1.user_id)
and t1.id > (select max_client_id from cabinet._transfer_log);

delete from cabinet.my_aspnet_membership
where not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = cabinet.my_aspnet_membership."userId");

delete from cabinet.my_aspnet_users
where not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = cabinet.my_aspnet_users.id);

/*
	**********************
	Часть 2. Добавление новых торговых точек к существующим клиентам
	**********************
*/


select t1.*
from cabinet2.sales t1
where t1.client_id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.sales x1 where t1.id = x1.id)
order by t1.client_id, t1.id
;

-- insert sales

insert into cabinet.sales (
  id,
  guid,
  name,
  client_id,
  city_id,
  cell,
  contact_person,
  work_mode,
  adress,
  is_deleted,
  sm_id,
  shedule_id,
  district_id,
  street_id,
  pharmacy_type_id,
  brand,
  email,
  site,
  location_map,
  is_published,
  image_path,
  map_code,
  map_image_path,
  hardware_id,
  prep_limit,
  sales_role,
  is_in_summary_order,
  summary_order_settings_id,
  user_id
)
select
  t1.id,
  t1.guid,
  t1.name,
  t1.client_id,
  t1.city_id,
  t1.cell,
  t1.contact_person,
  t1.work_mode,
  coalesce(t1.adress, 'адрес не задан'),
  t1.is_deleted,
  t1.sm_id,
  t1.shedule_id,
  t1.district_id,
  t1.street_id,
  t1.pharmacy_type_id,
  t1.brand,
  t1.email,
  t1.site,
  t1.location_map,
  t1.is_published,
  t1.image_path,
  t1.map_code,
  t1.map_image_path,
  t1.hardware_id,
  t1.prep_limit,
  t1.sales_role,
  t1.is_in_summary_order,
  t1.summary_order_settings_id,
  t1.user_id
from cabinet2.sales t1
where t1.client_id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.sales x1 where t1.id = x1.id)
;



-- select * from cabinet2.client where id = 4774
-- select cabinet.del_client(4774, 'cabinet2')

insert into cabinet.workplace (
  id,
  guid,
  registration_key,
  description,
  sales_id,
  is_deleted,
  account
)
select
  t1.id,
  t1.guid,
  t1.registration_key,
  t1.description,
  t1.sales_id,
  t1.is_deleted,
  t1.account
from cabinet2.workplace t1
inner join cabinet2.sales t2 on t1.sales_id = t2.id
where t2.client_id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.workplace x1 where t1.id = x1.id)
;

insert into cabinet."order" (
  id,
  guid,
  client_id,
  answer_set_id,
  created_on,
  created_by,
  updated_on,
  updated_by,
  is_deleted,
  commentary,
  commentary_manager,
  order_status,
  is_conformed,
  active_status,
  order_name
)
select
  t1.id,
  t1.guid,
  t1.client_id,
  t1.answer_set_id,
  t1.created_on,
  t1.created_by,
  t1.updated_on,
  t1.updated_by,
  t1.is_deleted,
  t1.commentary,
  t1.commentary_manager,
  t1.order_status,
  t1.is_conformed,
  t1.active_status,
  null
from cabinet2."order" t1
inner join cabinet2.client t2 on t1.client_id = t2.id
where t2.id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet."order" x1 where t1.id = x1.id)
--order by t1.id
;


-- insert order_item
insert into cabinet.order_item (
  id,
  guid,
  service_id,
  version_id,
  order_id,
  quantity,
  is_deleted,
  additional_data
)
select
  t1.id,
  t1.guid,
  t1.service_id,
  t1.version_id,
  t1.order_id,
  t1.quantity,
  t1.is_deleted,
  t1.additional_data
from cabinet2.order_item t1
inner join cabinet2."order" t2 on t1.order_id = t2.id
inner join cabinet2.client t3 on t2.client_id = t3.id
where t3.id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.order_item x1 where t1.id = x1.id)
-- order by t1.id
;

-- insert license

insert into cabinet.license (
  id,
  guid,
  order_item_id,
  service_registration_id,
  created_on,
  created_by,
  updated_on,
  updated_by,
  is_deleted,
  dt_start,
  dt_end
)
select
  t1.id,
  t1.guid,
  t1.order_item_id,
  null,
  t1.created_on,
  t1.created_by,
  t1.updated_on,
  t1.updated_by,
  t1.is_deleted,
  t1.dt_start,
  t1.dt_end
from cabinet2.license t1
inner join cabinet2.order_item t2 on t1.order_item_id = t2.id
inner join cabinet2."order" t3 on t2.order_id = t3.id
inner join cabinet2.client t4 on t3.client_id = t4.id
where t4.id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.license x1 where t1.id = x1.id)
-- order by t1.id
;

-- insert service_registration

insert into cabinet.service_registration (
  id,
  guid,
  activation_key,
  workplace_id,
  license_id,
  created_on,
  created_by,
  updated_on,
  updated_by,
  is_deleted
)
select
  t1.id,
  t1.guid,
  t1.activation_key,
  t1.workplace_id,
  t1.license_id,
  t1.created_on,
  t1.created_by,
  t1.updated_on,
  t1.updated_by,
  t1.is_deleted
from cabinet2.service_registration t1
inner join cabinet2.license t2 on t1.license_id = t2.id
inner join cabinet2.order_item t3 on t2.order_item_id = t3.id
inner join cabinet2."order" t4 on t3.order_id = t4.id
inner join cabinet2.client t5 on t4.client_id = t5.id
where t5.id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.service_registration x1 where t1.id = x1.id)
-- order by t1.id
;

update cabinet.license t1
set service_registration_id = 
(
select max(x1.id )
from cabinet.service_registration x1 
inner join cabinet.license x2 on x1.license_id = x2.id
inner join cabinet.order_item x3 on x2.order_item_id = x3.id
where x1.license_id = t1.id and x2.order_item_id = t2.id
)
-- set t1.service_registration_id = null
from cabinet.order_item t2
where t1.order_item_id = t2.id
and t1.id <= (select max_license_id from cabinet._transfer_log)
and service_registration_id is null;

-- insert client_service

insert into cabinet.client_service (client_id, service_id, state)
select distinct t1.client_id, t1.service_id, 0
from cabinet.vw_client_service t1
where t1.client_id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.client_id from cabinet.client_service x1 where x1.client_id = t1.client_id and x1.service_id = t1.service_id);

-- insert client_user

insert into cabinet.client_user (user_id, client_id, sales_id, user_name, user_name_full, is_main_for_sales)
select t1.user_id, max(t1.client_id), max(t1.id), max(t2.name), max(t2.name), true
from cabinet.sales t1
inner join cabinet.my_aspnet_users t2 on t1.user_id = t2.id
where t1.user_id is not null
and not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = t1.user_id)
and t1.client_id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.user_id from cabinet.client_user x1 where x1.user_id = t1.user_id)
group by t1.user_id;

delete from cabinet.my_aspnet_membership
where not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = cabinet.my_aspnet_membership."userId");

delete from cabinet.my_aspnet_users
where not exists (select x1.user_id from cabinet.client_user x1 where x1.user_id = cabinet.my_aspnet_users.id);

/*
	**********************
	Часть 3. Добавление новых рабочих мест к существующим торговым точкам
	**********************
*/

select t1.*
from cabinet2.workplace t1
inner join cabinet2.sales t2 on t1.sales_id = t2.id
where t2.client_id <= (select max_client_id from cabinet._transfer_log)
and not exists(select x1.id from cabinet.workplace x1 where t1.id = x1.id)
order by t1.sales_id, t1.id
;

-- нет таких

/*
	**********************
	Часть 4. Смена железных ключей на существующих рабочих местах
	**********************
*/

select t1.*, t2.registration_key as old_registration_key, t2.description as old_description
from cabinet2.workplace t1
inner join cabinet.workplace t2 on t1.id = t2.id
and trim(t1.registration_key) <> trim(t2.registration_key)
where t1.id <= (select max_workplace_id from cabinet._transfer_log)
;
-- 372 из 3769

update cabinet.workplace t2
set registration_key = t1.registration_key, description = t1.description
from cabinet2.workplace t1
where t1.id = t2.id
and trim(t1.registration_key) <> trim(t2.registration_key)
and t1.id <= (select max_workplace_id from cabinet._transfer_log);

-- update_order
select cabinet.update_order();


-- update counters

SELECT setval('cabinet.city_id_seq', (select max(id) from cabinet.city));
SELECT setval('cabinet.client_id_seq', (select max(id) from cabinet.client));
SELECT setval('cabinet.sales_id_seq', (select max(id) from cabinet.sales));
SELECT setval('cabinet.workplace_id_seq', (select max(id) from cabinet.workplace));
SELECT setval('cabinet.order_id_seq', (select max(id) from cabinet."order"));
SELECT setval('cabinet.order_item_id_seq', (select max(id) from cabinet.order_item));
SELECT setval('cabinet.license_id_seq', (select max(id) from cabinet.license));
SELECT setval('cabinet.service_registration_id_seq', (select max(id) from cabinet.service_registration));
SELECT setval('cabinet.client_service_item_id_seq', (select max(item_id) from cabinet.client_service));
SELECT setval('cabinet.version_id_seq', (select max(id) from cabinet.version));
