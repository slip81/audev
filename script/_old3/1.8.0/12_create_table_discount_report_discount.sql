﻿/*
	CREATE TABLE discount.report_discount
*/

-- DROP TABLE discount.report_discount;

CREATE TABLE discount.report_discount
(
  report_id integer NOT NULL,
  report_name character varying,
  report_filename character varying,
  report_descr character varying,
  report_params character varying,
  CONSTRAINT report_discount_pkey PRIMARY KEY (report_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.report_discount OWNER TO pavlov;


insert into discount.report_discount (report_id, report_name, report_filename, report_descr, report_params)
values (1, 'Общий отчет по картам', '', 'Общий отчет по картам', '');

insert into discount.report_discount (report_id, report_name, report_filename, report_descr, report_params)
values (2, 'Количество чеков за период', '', 'Количество чеков за период', '');

insert into discount.report_discount (report_id, report_name, report_filename, report_descr, report_params)
values (3, 'Неиспользуемые карты', '', 'Неиспользуемые карты', '');

update discount.report_discount
set report_descr = 'Отчет формирует список карт, по которым последняя продажа была в указанный период. Если период не задан - формируется список карт по которым вообще не было продаж.'
where report_id = 3;

-- SELECT * FROM discount.report_discount