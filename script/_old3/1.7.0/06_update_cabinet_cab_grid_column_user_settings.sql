﻿/*
	update cabinet.cab_grid_column_user_settings
*/

update cabinet.cab_grid_column_user_settings set is_filterable = true where column_id = 20;

update cabinet.cab_grid_column_user_settings set is_visible = false where column_id = 19;