﻿/*
	CREATE SEQUENCEs
*/

-- DROP SEQUENCE cabinet.my_aspnet_applications_id_seq;

CREATE SEQUENCE cabinet.my_aspnet_applications_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE cabinet.my_aspnet_applications_id_seq OWNER TO pavlov;

ALTER TABLE cabinet.my_aspnet_applications ALTER COLUMN id SET DEFAULT nextval('cabinet.my_aspnet_applications_id_seq'::regclass);

DROP SEQUENCE cabinet."my_aspnet_Applications_id_seq";


-- DROP SEQUENCE cabinet.my_aspnet_roles_id_seq;

CREATE SEQUENCE cabinet.my_aspnet_roles_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10
  CACHE 1;
ALTER TABLE cabinet.my_aspnet_roles_id_seq
  OWNER TO pavlov;

ALTER TABLE cabinet.my_aspnet_roles ALTER COLUMN id SET DEFAULT nextval('cabinet.my_aspnet_roles_id_seq'::regclass);

DROP SEQUENCE cabinet."my_aspnet_Roles_id_seq";


-- DROP SEQUENCE cabinet.my_aspnet_users_id_seq;

CREATE SEQUENCE cabinet.my_aspnet_users_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 6822
  CACHE 1;
ALTER TABLE cabinet.my_aspnet_users_id_seq
  OWNER TO pavlov;

ALTER TABLE cabinet.my_aspnet_users ALTER COLUMN id SET DEFAULT nextval('cabinet.my_aspnet_users_id_seq'::regclass);  

DROP SEQUENCE cabinet."my_aspnet_Users_id_seq";


-- DROP SEQUENCE esn."defect_dbf_ID_seq";

CREATE SEQUENCE esn.defect_dbf_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE esn.defect_dbf_id_seq
  OWNER TO pavlov;

ALTER TABLE esn.defect_dbf ALTER COLUMN "ID" SET DEFAULT nextval('esn.defect_dbf_id_seq'::regclass);  

DROP SEQUENCE esn."defect_dbf_ID_seq";


-- SELECT * FROM cabinet."my_aspnet_Applications_id_seq";
-- SELECT * FROM cabinet.my_aspnet_applications_id_seq;
-- SELECT * FROM cabinet."my_aspnet_Roles_id_seq"
-- SELECT * FROM cabinet.my_aspnet_Roles_id_seq
-- SELECT * FROM cabinet."my_aspnet_Users_id_seq"
-- SELECT * FROM cabinet.my_aspnet_users_id_seq
-- SELECT * FROM esn."defect_dbf_ID_seq"
-- SELECT * FROM esn.defect_dbf_ID_seq