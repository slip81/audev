﻿/*
	ALTER TABLE cabinet.crm_event ADD COLUMNs
*/


ALTER TABLE cabinet.crm_event ADD COLUMN event_progress character varying;
ALTER TABLE cabinet.crm_event ADD COLUMN event_result character varying;
