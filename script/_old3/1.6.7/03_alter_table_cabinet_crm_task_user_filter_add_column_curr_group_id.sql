﻿/*
	ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN curr_group_id
*/

ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN curr_group_id integer;