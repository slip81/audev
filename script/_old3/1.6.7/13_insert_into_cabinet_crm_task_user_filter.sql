﻿/*
	insert into cabinet.crm_task_user_filter
*/

-- select * from cabinet.crm_task_user_filter

insert into cabinet.crm_task_user_filter (user_id, grid_id, raw_filter, curr_group_id)
select user_id, 1, '{"logic":"and","filters":[]}', 0
from cabinet.cab_user
where user_id not in (select user_id from cabinet.crm_task_user_filter);

-- select * from cabinet.cab_grid_user_settings

insert into cabinet.cab_grid_user_settings (grid_id, user_id, sort_options, filter_options)
select 1, user_id, '[{"field":"task_num","dir":"desc"}]', '{"logic":"and","filters":[]}'
from cabinet.cab_user
where user_id not in (select user_id from cabinet.cab_grid_user_settings);

