﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_task_curr_exec
*/

-- DROP VIEW cabinet.vw_crm_task_curr_exec;

CREATE OR REPLACE VIEW cabinet.vw_crm_task_curr_exec AS 
	select count(t1.task_id) as cnt_task_id, t1.exec_user_id, t2.user_name as exec_user_name
	from cabinet.crm_task t1
	inner join cabinet.crm_user t2 on t1.exec_user_id = t2.user_id
	where t1.exec_user_id > 0
	and t1.group_id = 3
	and t1.is_archive = false
	group by t1.exec_user_id, t2.user_name;

ALTER TABLE cabinet.vw_crm_task_curr_exec OWNER TO pavlov;

