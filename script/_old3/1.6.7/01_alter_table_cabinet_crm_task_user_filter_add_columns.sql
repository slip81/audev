﻿/*
	ALTER TABLE cabinet.crm_task_user_filter ADD COLUMNs
*/

ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN crt_date1_filter timestamp without time zone;
ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN crt_date2_filter timestamp without time zone;

ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN required_date1_filter timestamp without time zone;
ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN required_date2_filter timestamp without time zone;

ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN repair_date1_filter timestamp without time zone;
ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN repair_date2_filter timestamp without time zone;
