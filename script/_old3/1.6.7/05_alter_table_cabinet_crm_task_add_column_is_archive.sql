﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN is_archive
*/

ALTER TABLE cabinet.crm_task ADD COLUMN is_archive boolean NOT NULL DEFAULT false;
