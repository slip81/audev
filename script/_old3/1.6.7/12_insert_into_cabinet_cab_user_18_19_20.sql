﻿/*
	insert into cabinet.cab_user
*/

insert into cabinet.cab_user (user_id, user_name, user_login, is_admin, is_superadmin, full_name)
values (18, 'КЯС', 'kozlov', true, false, 'КЯС');

insert into cabinet.cab_user (user_id, user_name, user_login, is_admin, is_superadmin, full_name)
values (19, 'ДДС', 'damyan', true, false, 'ДДС');

insert into cabinet.cab_user (user_id, user_name, user_login, is_admin, is_superadmin, full_name)
values (20, 'СОВ', 'sozinova', true, false, 'СОВ');

insert into cabinet.crm_user (user_id, user_name, cab_user_id)
values (19, 'КЯС', 18);

insert into cabinet.crm_user (user_id, user_name, cab_user_id)
values (20, 'ДДС', 19);

insert into cabinet.crm_user (user_id, user_name, cab_user_id)
values (21, 'СОВ', 20);

--select * from cabinet.crm_user order by user_id;
--select * from cabinet.vw_cab_user2
--select * from cabinet.cab_user order by user_id;
