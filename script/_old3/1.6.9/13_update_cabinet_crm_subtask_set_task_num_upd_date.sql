﻿/*
	UPDATE cabinet.crm_subtask
*/

WITH crm_subtask_cte AS
(
SELECT t1.task_id, t1.subtask_id, row_number() over (partition by t1.task_id order by t1.subtask_id) as task_num_new
FROM cabinet.crm_subtask t1
)
-- SELECT * FROM crm_subtask_cte ORDER BY task_id, task_num_new
UPDATE cabinet.crm_subtask
SET task_num = (SELECT task_num_new FROM crm_subtask_cte WHERE cabinet.crm_subtask.subtask_id = crm_subtask_cte.subtask_id)
;

UPDATE cabinet.crm_subtask
SET upd_date = repair_date
;

/* local fix */
INSERT INTO cabinet.crm_state (state_id, state_name)
SELECT 5, 'Выполнено'
WHERE NOT EXISTS (SELECT state_id FROM cabinet.crm_state WHERE state_id = 5)
;

-- select * from cabinet.crm_state;

UPDATE cabinet.crm_subtask
SET state_id = 5 where state_id = 1
;

UPDATE cabinet.crm_subtask
SET state_id = 1 where state_id = 0
;

-- select * from cabinet.crm_subtask;

