﻿/*
	ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN fin_cost_filter
*/

ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN fin_cost_filter numeric;
