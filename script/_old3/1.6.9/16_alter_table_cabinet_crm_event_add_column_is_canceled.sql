﻿/*
	ALTER TABLE cabinet.crm_event ADD COLUMN is_canceled
*/

ALTER TABLE cabinet.crm_event ADD COLUMN is_canceled boolean NOT NULL DEFAULT false;
