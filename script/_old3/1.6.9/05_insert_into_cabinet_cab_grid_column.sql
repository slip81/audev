﻿/*
	insert into cabinet.cab_grid_column
*/

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus)
values (19, 1, 'is_fin_name', 'Фин. знач.');

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus)
values (20, 1, 'fin_cost', 'Цена');

-- select * from cabinet.cab_grid_column order by column_id

insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
select 19, user_id, 18, true, 200, false, true
from cabinet.cab_user;

insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
select 20, user_id, 19, true, 200, false, true
from cabinet.cab_user;

--select * from cabinet.cab_grid_column_user_settings order by user_id, column_id;

--select * from cabinet.crm_user