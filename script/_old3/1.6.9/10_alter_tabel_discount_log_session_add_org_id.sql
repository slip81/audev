﻿/*
	ALTER TABLE discount.log_session ADD COLUMN org_id
*/

ALTER TABLE discount.log_session ADD COLUMN org_id bigint;

ALTER TABLE discount.log_session
  ADD CONSTRAINT log_session_org_id_fkey FOREIGN KEY (org_id)
      REFERENCES discount.business_org (org_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE discount.log_session
SET org_id = (SELECT x1.org_id FROM discount.business_org_user x1 WHERE coalesce(trim(x1.user_name), '') = coalesce(trim(discount.log_session.user_name), '') LIMIT 1)
WHERE coalesce(org_id, 0) <= 0;

-- select * from discount.log_session order by log_id desc;