﻿/*
	CREATE OR REPLACE VIEW discount.vw_business_summary
*/


-- DROP VIEW discount.vw_business_summary;

CREATE OR REPLACE VIEW discount.vw_business_summary AS 
 SELECT t1.business_id,
    count(t1.card_id) AS card_cnt,
    sum(
        CASE
            WHEN t2.card_type_group_id = 1 THEN 1
            ELSE 0
        END) AS card_type_group_1_cnt,
    sum(
        CASE
            WHEN t2.card_type_group_id = 2 THEN 1
            ELSE 0
        END) AS card_type_group_2_cnt,
    sum(
        CASE
            WHEN t2.card_type_group_id = 3 THEN 1
            ELSE 0
        END) AS card_type_group_3_cnt,
    t11.card_holder_cnt,
    COALESCE(t12.card_trans_cnt, 0::bigint) AS card_trans_cnt,
    COALESCE(t12.card_trans_today_cnt, 0::bigint) AS card_trans_today_cnt
   FROM discount.card t1
     JOIN discount.card_type t2 ON t1.curr_card_type_id = t2.card_type_id
     LEFT JOIN ( SELECT x1.business_id,
            count(x1.card_holder_id) AS card_holder_cnt
           FROM discount.card_holder x1
          WHERE 1 = 1
          GROUP BY x1.business_id) t11 ON t1.business_id = t11.business_id
     LEFT JOIN ( SELECT x2.business_id,
            sum(
                CASE
                    WHEN x1.date_beg::date = 'now'::text::date THEN 1
                    ELSE 0
                END) AS card_trans_today_cnt,
            count(x1.card_trans_id) AS card_trans_cnt
           FROM discount.card_transaction x1
             JOIN discount.card x2 ON x1.card_id = x2.card_id
          WHERE x1.trans_kind = 1
          AND x1.status = 0
          GROUP BY x2.business_id) t12 ON t1.business_id = t12.business_id
  WHERE t2.card_type_group_id = ANY (ARRAY[1::bigint, 2::bigint, 3::bigint])
  GROUP BY t1.business_id, t11.card_holder_cnt, t12.card_trans_cnt, t12.card_trans_today_cnt;

ALTER TABLE discount.vw_business_summary OWNER TO pavlov;
