﻿/*
	CREATE OR REPLACE VIEWs
*/

-- DROP VIEW cabinet.vw_crm_priority;

CREATE OR REPLACE VIEW cabinet.vw_crm_priority AS 
 SELECT t1.priority_id,
    t1.priority_name,
    t11.user_id,
        CASE
            WHEN t12.fore_color IS NULL THEN 'black'::character varying
            ELSE t12.fore_color
        END AS fore_color
   FROM cabinet.crm_priority t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1
     LEFT JOIN cabinet.crm_priority_user_settings t12 ON t1.priority_id = t12.priority_id AND t11.user_id = COALESCE(t12.user_id, t11.user_id);

ALTER TABLE cabinet.vw_crm_priority OWNER TO pavlov;


-- DROP VIEW cabinet.vw_crm_project;

CREATE OR REPLACE VIEW cabinet.vw_crm_project AS 
 SELECT t1.project_id,
    t1.project_name,
    t11.user_id,
        CASE
            WHEN t12.fore_color IS NULL THEN 'black'::character varying
            ELSE t12.fore_color
        END AS fore_color
   FROM cabinet.crm_project t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1
     LEFT JOIN cabinet.crm_project_user_settings t12 ON t1.project_id = t12.project_id AND t11.user_id = COALESCE(t12.user_id, t11.user_id);

ALTER TABLE cabinet.vw_crm_project OWNER TO pavlov;

-- DROP VIEW cabinet.vw_crm_state;

CREATE OR REPLACE VIEW cabinet.vw_crm_state AS 
 SELECT t1.state_id,
    t1.state_name,
    t11.user_id,
        CASE
            WHEN t12.fore_color IS NULL THEN 'black'::character varying
            ELSE t12.fore_color
        END AS fore_color
   FROM cabinet.crm_state t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1
     LEFT JOIN cabinet.crm_state_user_settings t12 ON t1.state_id = t12.state_id AND t11.user_id = COALESCE(t12.user_id, t11.user_id);

ALTER TABLE cabinet.vw_crm_state OWNER TO pavlov;

-- DROP VIEW cabinet.vw_crm_user;

CREATE OR REPLACE VIEW cabinet.vw_crm_user AS 
 SELECT t1.user_id AS crm_user_id,
    t1.user_name AS crm_user_name,
    t1.cab_user_id,
    t1.is_boss,
    t1.allow_bucket_read,
    t1.allow_bucket_write,
    t1.allow_prepared_read,
    t1.allow_prepared_write,
    t1.allow_fixed_read,
    t1.allow_fixed_write,
    t1.allow_approved_read,
    t1.allow_approved_write,
    t11.user_id,
        CASE
            WHEN t12.fore_color IS NULL THEN 'black'::character varying
            ELSE t12.fore_color
        END AS fore_color
   FROM cabinet.crm_user t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1
     LEFT JOIN cabinet.crm_user_user_settings t12 ON t1.user_id = t12.crm_user_id AND t11.user_id = COALESCE(t12.user_id, t11.user_id);

ALTER TABLE cabinet.vw_crm_user OWNER TO pavlov;

