﻿/*
	CREATE OR REPLACE VIEW discount.vw_card_type_business_org
*/

-- DROP VIEW discount.vw_card_type_business_org;

CREATE OR REPLACE VIEW discount.vw_card_type_business_org AS 
 SELECT t1.card_type_id,
    t1.card_type_name,
    t2.org_id as business_org_id,
    t2.org_name,
        CASE
            WHEN COALESCE(t3.card_type_id, 0::bigint) <= 0 THEN false
            ELSE true
        END AS is_org_ok
   FROM discount.card_type t1
     LEFT JOIN discount.business_org t2 ON t1.business_id = t2.business_id
     LEFT JOIN discount.card_type_business_org t3 ON t2.org_id = t3.business_org_id AND t1.card_type_id = t3.card_type_id
     ;

ALTER TABLE discount.vw_card_type_business_org OWNER TO pavlov;

-- select * from discount.vw_card_type_business_org order by card_type_id, business_org_id
