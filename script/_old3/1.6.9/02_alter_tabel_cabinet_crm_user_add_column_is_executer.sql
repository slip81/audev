﻿/*
	ALTER TABLE cabinet.crm_user ADD COLUMN is_executer boolean NOT NULL DEFAULT false;
*/

ALTER TABLE cabinet.crm_user ADD COLUMN is_executer boolean NOT NULL DEFAULT false;

UPDATE cabinet.crm_user SET is_executer = true WHERE user_id in (2, 5, 7, 11);

-- select * from cabinet.crm_user order by user_id;
