﻿/*
	CREATE OR REPLACE VIEW discount.vw_business_summary_graph
*/

--DROP VIEW discount.vw_business_summary_graph;

CREATE OR REPLACE VIEW discount.vw_business_summary_graph AS 
	select row_number() over (order by t2.business_id) as id, t2.business_id, (t1.date_beg::date) as date_beg_date, sum(t1.trans_sum) as trans_sum_total
	from discount.card_transaction t1
	inner join discount.card t2 on t1.card_id = t2.card_id
	/*inner join discount.card_type t3 on t2.curr_card_type_id = t3.card_type_id*/
	where t1.trans_kind = 1
	and t1.status = 0
	and t1.date_beg >= current_date - integer '180'
	group by t2.business_id, (t1.date_beg::date)
	order by t2.business_id, (t1.date_beg::date)
	;

ALTER TABLE discount.vw_business_summary_graph OWNER TO pavlov;
