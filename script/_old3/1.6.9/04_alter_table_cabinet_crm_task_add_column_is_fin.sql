﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN is_fin
*/

ALTER TABLE cabinet.crm_task ADD COLUMN is_fin boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_task ADD COLUMN fin_cost numeric;
