﻿/*
	CREATE TABLE cabinet.crm_event
*/

-- DROP TABLE cabinet.crm_event;

CREATE TABLE cabinet.crm_event
(
  event_id serial NOT NULL,
  event_name character varying,
  event_text character varying,
  task_id integer,
  date_beg date NOT NULL,
  date_end date NOT NULL,
  exec_user_id integer,
  is_done boolean NOT NULL DEFAULT False,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT crm_event_pkey PRIMARY KEY (event_id),
  CONSTRAINT crm_event_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_event_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_event OWNER TO pavlov;
