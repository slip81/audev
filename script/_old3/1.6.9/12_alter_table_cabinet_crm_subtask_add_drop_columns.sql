﻿/*
	ALTER TABLE cabinet.crm_subtask
*/

ALTER TABLE cabinet.crm_subtask DROP CONSTRAINT crm_subtask_exec_user_id_fkey;
ALTER TABLE cabinet.crm_subtask DROP CONSTRAINT crm_subtask_priority_id_fkey;

ALTER TABLE cabinet.crm_subtask DROP COLUMN exec_user_id;
ALTER TABLE cabinet.crm_subtask DROP COLUMN priority_id;

ALTER TABLE cabinet.crm_subtask ADD COLUMN task_num integer NOT NULL DEFAULT 0;
ALTER TABLE cabinet.crm_subtask ADD COLUMN upd_date timestamp without time zone;