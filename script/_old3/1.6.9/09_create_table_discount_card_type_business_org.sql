﻿/*
	CREATE TABLE discount.card_type_business_org
*/

-- DROP TABLE discount.card_type_business_org;

CREATE TABLE discount.card_type_business_org
(
  item_id bigserial NOT NULL,
  card_type_id bigint NOT NULL,
  business_org_id bigint NOT NULL,
  CONSTRAINT card_type_business_org_pkey PRIMARY KEY (item_id),
  CONSTRAINT card_type_business_org_business_org_id_fkey FOREIGN KEY (business_org_id)
      REFERENCES discount.business_org (org_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT card_type_business_org_card_type_id_fkey FOREIGN KEY (card_type_id)
      REFERENCES discount.card_type (card_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_type_business_org OWNER TO pavlov;


CREATE INDEX discount_card_type_business_org1_idx
  ON discount.card_type_business_org
  USING btree
  (card_type_id, business_org_id);



insert into discount.card_type_business_org (card_type_id, business_org_id)
select t1.card_type_id, t2.org_id
from discount.card_type t1
inner join discount.business_org t2 on t1.business_id = t2.business_id
;


-- select * from discount.card_type_business_org order by card_type_id, business_org_id;
