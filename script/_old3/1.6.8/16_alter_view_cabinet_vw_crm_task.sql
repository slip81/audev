﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_task
*/

-- DROP VIEW cabinet.vw_crm_task;

CREATE OR REPLACE VIEW cabinet.vw_crm_task AS 
 SELECT t1.task_id,
    t1.task_name,
    t1.task_text,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.module_version_id,
    t1.client_id,
    t1.priority_id,
    t1.state_id,
    t1.owner_user_id,
    t1.exec_user_id,
    ((t1.task_num::text ||
        CASE
            WHEN t1.rel_child_cnt > 0 THEN (' [M-'::text || t1.rel_child_cnt::character varying::text) || ']'::text
            ELSE ''::text
        END) ||
        CASE
            WHEN t1.rel_parent_cnt > 0 THEN (' [S-'::text || t1.rel_parent_cnt::character varying::text) || ']'::text
            ELSE ''::text
        END)::character varying AS task_num,
    t1.crt_date,
    t1.required_date,
    t1.repair_date,
    t1.repair_version_id,
    t2.client_name,
    t3.module_name,
    t4.module_part_name,
    t5.module_version_name,
    t6.priority_name,
    t7.project_name,
    t8.state_name,
    t9.user_name AS owner_user_name,
    t10.user_name AS exec_user_name,
    t21.module_version_name AS repair_version_name,
    replace(t1.task_num::text, 'AU-'::text, ''::text)::integer AS task_num_int,
    t1.group_id,
    t1.point,
    t1.prepared_user_id,
    t1.fixed_user_id,
    t1.approved_user_id,
    t11.user_name AS prepared_user_name,
    t12.user_name AS fixed_user_name,
    t13.user_name AS approved_user_name,
    t1.assigned_user_id,
    t14.user_name AS assigned_user_name,
    t15.group_name,
    t1.rel_child_cnt,
    t1.rel_parent_cnt,
    t22.user_id as view_user_id,
    t23.fore_color as state_fore_color,
    t24.fore_color as project_fore_color,
    t25.fore_color as priority_fore_color,
    t26.fore_color as owner_user_fore_color,
    t27.fore_color as exec_user_fore_color,
    t28.fore_color as assigned_user_fore_color
   FROM cabinet.crm_task t1
     JOIN cabinet.crm_client t2 ON t1.client_id = t2.client_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id
     JOIN cabinet.crm_priority t6 ON t1.priority_id = t6.priority_id
     JOIN cabinet.crm_project t7 ON t1.project_id = t7.project_id
     JOIN cabinet.crm_state t8 ON t1.state_id = t8.state_id
     JOIN cabinet.crm_user t9 ON t1.owner_user_id = t9.user_id
     JOIN cabinet.crm_user t10 ON t1.exec_user_id = t10.user_id
     JOIN cabinet.crm_user t11 ON t1.prepared_user_id = t11.user_id
     JOIN cabinet.crm_user t12 ON t1.fixed_user_id = t12.user_id
     JOIN cabinet.crm_user t13 ON t1.approved_user_id = t13.user_id
     JOIN cabinet.crm_user t14 ON t1.assigned_user_id = t14.user_id
     JOIN cabinet.crm_group t15 ON t1.group_id = t15.group_id
     LEFT JOIN cabinet.crm_module_version t21 ON t1.repair_version_id = t21.module_version_id
     LEFT JOIN cabinet.cab_user t22 on 1=1
     LEFT JOIN cabinet.crm_state_user_settings t23 on t1.state_id = t23.state_id and t22.user_id = coalesce(t23.user_id, t22.user_id)
     LEFT JOIN cabinet.crm_project_user_settings t24 on t1.project_id = t24.project_id and t22.user_id = coalesce(t24.user_id, t22.user_id)
     LEFT JOIN cabinet.crm_priority_user_settings t25 on t1.priority_id = t25.priority_id and t22.user_id = coalesce(t25.user_id, t22.user_id)
     LEFT JOIN cabinet.crm_user_user_settings t26 on t1.owner_user_id = t26.crm_user_id and t22.user_id = coalesce(t26.user_id, t22.user_id)
     LEFT JOIN cabinet.crm_user_user_settings t27 on t1.exec_user_id = t27.crm_user_id and t22.user_id = coalesce(t27.user_id, t22.user_id)
     LEFT JOIN cabinet.crm_user_user_settings t28 on t1.assigned_user_id = t28.crm_user_id and t22.user_id = coalesce(t28.user_id, t22.user_id)
  WHERE t1.is_archive = false;

ALTER TABLE cabinet.vw_crm_task OWNER TO pavlov;

--select * from cabinet.vw_crm_task order by task_num desc;
