﻿/*
	CREATE TABLE esn.exchange_item_type
*/

-- DROP TABLE esn.exchange_item_type;

CREATE TABLE esn.exchange_item_type
(
  type_id integer NOT NULL,
  type_name character varying,
  type_descr character varying,
  CONSTRAINT exchange_item_type_pkey PRIMARY KEY (type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_item_type OWNER TO pavlov;


insert into esn.exchange_item_type (type_id, type_name, type_descr)
values (1, 'txt', 'Формат TXT');

insert into esn.exchange_item_type (type_id, type_name, type_descr)
values (2, 'csv', 'Формат CSV');

insert into esn.exchange_item_type (type_id, type_name, type_descr)
values (3, 'dbf', 'Формат DBF');

insert into esn.exchange_item_type (type_id, type_name, type_descr)
values (4, 'xls', 'Формат XLS');

insert into esn.exchange_item_type (type_id, type_name, type_descr)
values (5, 'json', 'Формат JSON');

-- select * from esn.exchange_item_type order by type_id;

-- select * from esn.exchange_item order by item_id;

ALTER TABLE esn.datasource_exchange_item ADD COLUMN item_type_id integer;

ALTER TABLE esn.datasource_exchange_item
  ADD CONSTRAINT datasource_exchange_item_item_type_id_fkey FOREIGN KEY (item_type_id)
      REFERENCES esn.exchange_item_type (type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

-- select * from esn.datasource_exchange_item order by ds_id, item_id; 

UPDATE esn.datasource_exchange_item SET item_type_id = 1;
UPDATE esn.datasource_exchange_item SET item_type_id = 3 WHERE item_id in (7, 8);

ALTER TABLE esn.datasource_exchange_item ADD COLUMN item_file_name character varying;
ALTER TABLE esn.datasource_exchange_item ADD COLUMN sql_text character varying;
ALTER TABLE esn.datasource_exchange_item ADD COLUMN param character varying;

UPDATE esn.datasource_exchange_item SET item_file_name = 'test_file_name.txt';
