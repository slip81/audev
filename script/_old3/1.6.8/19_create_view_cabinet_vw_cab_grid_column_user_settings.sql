﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_cab_grid_column_user_settings
*/

-- DROP VIEW cabinet.vw_cab_grid_column_user_settings;

CREATE OR REPLACE VIEW cabinet.vw_cab_grid_column_user_settings AS 
	SELECT (row_number() OVER (PARTITION BY t1.user_id, t1.is_visible ORDER BY t1.column_num)) - 1 AS column_index,
	t1.item_id, t1.column_id, t1.user_id, t1.column_num, t1.is_visible,
	t1.width, t1.is_filterable, t1.is_sortable
	FROM cabinet.cab_grid_column_user_settings t1;

ALTER TABLE cabinet.vw_cab_grid_column_user_settings OWNER TO pavlov;


-- select * from cabinet.cab_grid_column_user_settings order by user_id, column_num;
