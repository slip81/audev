﻿/*
	CREATE TABLE cabinet.crm_user_user_settings
*/

-- DROP TABLE cabinet.crm_user_user_settings;

CREATE TABLE cabinet.crm_user_user_settings
(
  item_id serial NOT NULL,
  crm_user_id integer NOT NULL,
  user_id integer NOT NULL,
  fore_color character varying,
  CONSTRAINT crm_user_user_settings_pkey PRIMARY KEY (item_id),
  CONSTRAINT crm_user_user_settings_crm_user_id_fkey FOREIGN KEY (crm_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_user_user_settings_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_user_user_settings OWNER TO pavlov;