﻿/*
	ALTER TABLE cabinet.cab_grid_user_settings ADD COLUMN back_color_column_id integer
*/

ALTER TABLE cabinet.cab_grid_user_settings ADD COLUMN back_color_column_id integer;

UPDATE cabinet.cab_grid_user_settings SET back_color_column_id = 13;