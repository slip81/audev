﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_priority
*/

-- DROP VIEW cabinet.vw_crm_priority;

CREATE OR REPLACE VIEW cabinet.vw_crm_priority AS
	SELECT t1.priority_id, t1.priority_name, t11.user_id
	, case when t12.fore_color is null then '#000000' else t12.fore_color end as fore_color
	FROM cabinet.crm_priority t1
	LEFT JOIN cabinet.cab_user t11 ON 1=1
	LEFT JOIN cabinet.crm_priority_user_settings t12 ON t1.priority_id = t12.priority_id and t11.user_id = coalesce(t12.user_id, t11.user_id)
	;

ALTER TABLE cabinet.vw_crm_priority OWNER TO pavlov;


-- select * from cabinet.vw_crm_priority order by user_id, priority_id, priority_name, fore_color;