﻿/*
	CREATE OR REPLACE FUNCTION esn.del_old_data
*/

-- DROP FUNCTION esn.del_old_data();

CREATE OR REPLACE FUNCTION esn.del_old_data()
  RETURNS integer AS
$BODY$
DECLARE
	_exchange_del_interval integer;
	_exchange_log_del_interval integer;
	_discount_log_del_interval integer;
BEGIN
	_exchange_del_interval := 31;
	
	delete from esn.exchange_data t1
	using esn.exchange t2
	where t1.exchange_id = t2.exchange_id
	and t2.crt_date <= (current_date - _exchange_del_interval);

	_exchange_log_del_interval := 31;

	delete from esn.log_esn where scope = 4 and date_beg <= (current_date - _exchange_log_del_interval);
	delete from esn.exchange_log where date_beg <= (current_date - _exchange_log_del_interval);

	_discount_log_del_interval := 62;

	delete from discount.log_event where scope = 1 and date_beg <= (current_date - _discount_log_del_interval);	

	return 0;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION esn.del_old_data() OWNER TO pavlov;
