﻿/*
	insert into cabinet.crm_group
*/

insert into cabinet.crm_group (group_id, group_name)
values (5, 'В версии');


-- select * from cabinet.crm_group order by group_id;
/*
insert into cabinet.crm_task_operation (operation_id, operation_name)
values (5, 'В версию');
*/
insert into cabinet.crm_task_operation (operation_id, operation_name)
values (13, 'Архивировать');

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (14, 'Вернуть из архива');

update cabinet.crm_task_operation set operation_name = 'В версию' where operation_id = 5;

delete from cabinet.crm_task_operation  where operation_id = 6;
delete from cabinet.crm_task_operation where operation_id = 7;


-- select * from cabinet.crm_task_operation order by operation_id

/*
	insert into cabinet.crm_group_operation
*/

delete from cabinet.crm_group_operation;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select -1, operation_id, operation_name
from cabinet.crm_task_operation;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 0, operation_id, operation_name
from cabinet.crm_task_operation;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 1, operation_id, operation_name
from cabinet.crm_task_operation;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 2, operation_id, operation_name
from cabinet.crm_task_operation;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 3, operation_id, operation_name
from cabinet.crm_task_operation;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 4, operation_id, operation_name
from cabinet.crm_task_operation;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 5, operation_id, operation_name
from cabinet.crm_task_operation;

delete from cabinet.crm_group_operation where group_id = -1 and operation_id in (14);
delete from cabinet.crm_group_operation where group_id = 0 and operation_id in (0, 4, 5, 13, 14);
delete from cabinet.crm_group_operation where group_id = 1 and operation_id in (1,4,5,13,14);
delete from cabinet.crm_group_operation where group_id = 2 and operation_id in (2,4,5,13,14);
delete from cabinet.crm_group_operation where group_id = 3 and operation_id in (3,13,14);
delete from cabinet.crm_group_operation where group_id = 4 and operation_id not in (13);
delete from cabinet.crm_group_operation where group_id = 5 and operation_id in (5,13,14);

-- select * from cabinet.crm_group_operation order by group_id, operation_id;
