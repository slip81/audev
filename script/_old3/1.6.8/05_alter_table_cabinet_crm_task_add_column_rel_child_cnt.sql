﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN rel_child_cnt, rel_parent_cnt
*/

ALTER TABLE cabinet.crm_task ADD COLUMN rel_child_cnt integer NOT NULL DEFAULT 0;
ALTER TABLE cabinet.crm_task ADD COLUMN rel_parent_cnt integer NOT NULL DEFAULT 0;

