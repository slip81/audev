﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_project
*/

-- DROP VIEW cabinet.vw_crm_project;

CREATE OR REPLACE VIEW cabinet.vw_crm_project AS
	SELECT t1.project_id, t1.project_name, t11.user_id
	, case when t12.fore_color is null then '#000000' else t12.fore_color end as fore_color
	FROM cabinet.crm_project t1
	LEFT JOIN cabinet.cab_user t11 ON 1=1
	LEFT JOIN cabinet.crm_project_user_settings t12 ON t1.project_id = t12.project_id and t11.user_id = coalesce(t12.user_id, t11.user_id)
	;

ALTER TABLE cabinet.vw_crm_project OWNER TO pavlov;


-- select * from cabinet.vw_crm_project order by user_id, project_id, project_name, fore_color;
