﻿/*
	insert into cabinet.crm_xxx_user_settings
*/

-- crm_state
insert into cabinet.crm_state_user_settings (state_id, user_id, fore_color)
select t2.state_id, t1.user_id, t2.fore_color
from cabinet.cab_user t1
left join cabinet.crm_state_user_settings t2 on t2.user_id = 1 --t1.user_id = t2.user_id
where t1.user_id > 0 and t1.user_id <> 1
order by t1.user_id, t2.state_id;

select * from cabinet.crm_state_user_settings order by user_id, state_id;

-- crm_priority
insert into cabinet.crm_priority_user_settings (priority_id, user_id, fore_color)
select t2.priority_id, t1.user_id, t2.fore_color
from cabinet.cab_user t1
left join cabinet.crm_priority_user_settings t2 on t2.user_id = 1
where t1.user_id > 0 and t1.user_id <> 1
order by t1.user_id, t2.priority_id;

-- select * from cabinet.crm_priority_user_settings order by user_id, priority_id;

-- crm_project
insert into cabinet.crm_project_user_settings (project_id, user_id, fore_color)
select t2.project_id, t1.user_id, t2.fore_color
from cabinet.cab_user t1
left join cabinet.crm_project_user_settings t2 on t2.user_id = 1
where t1.user_id > 0 and t1.user_id <> 1
order by t1.user_id, t2.project_id;

-- select * from cabinet.crm_project_user_settings order by user_id, project_id;

-- crm_user
insert into cabinet.crm_user_user_settings (crm_user_id, user_id, fore_color)
select t2.crm_user_id, t1.user_id, t2.fore_color
from cabinet.cab_user t1
left join cabinet.crm_user_user_settings t2 on t2.user_id = 1
where t1.user_id > 0 and t1.user_id <> 1
order by t1.user_id, t2.crm_user_id;

-- select * from cabinet.crm_user_user_settings order by crm_user_id, user_id;
