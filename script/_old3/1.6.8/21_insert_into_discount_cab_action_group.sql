﻿/*
	insert into discount.cab_action_group
*/

-- select * from discount.cab_action_group order by group_id;
-- select * from discount.cab_action order by group_id, action_id;

insert into discount.cab_action_group (group_id, group_name)
values (7, 'Сервис обмена');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (24, 7, 'Монитор обмена', 'ExchangeMonitor_Read');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (25, 7, 'Просмотр участников обмена', 'ExchangeUser_Read');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (26, 7, 'Добавление участника обмена', 'ExchangeUser_Add');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (27, 7, 'Редактирование участника обмена', 'ExchangeUser_Edit');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (28, 7, 'Удаление участника обмена', 'ExchangeUser_Del');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (29, 7, 'Обмен с партнерами', 'ExchangeRegPartner_Edit');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (30, 7, 'Обмен накладными', 'ExchangeRegDoc_Edit');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (31, 7, 'Просмотр партнеров', 'ExchangePartner_Read');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (32, 7, 'Добавление партнера', 'ExchangePartner_Add');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (33, 7, 'Редактирование партнера', 'ExchangePartner_Edit');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (34, 7, 'Удаление партнера', 'ExchangePartner_Del');

insert into discount.cab_action (action_id, group_id, action_name, sys_action_name)
values (35, 7, 'Просмотр журнала событий', 'ExchangeLog_Read');