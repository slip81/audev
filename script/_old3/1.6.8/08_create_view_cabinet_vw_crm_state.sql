﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_state
*/

-- DROP VIEW cabinet.vw_crm_state;

CREATE OR REPLACE VIEW cabinet.vw_crm_state AS
	SELECT t1.state_id, t1.state_name, t11.user_id
	, case when t12.fore_color is null then '#000000' else t12.fore_color end as fore_color
	FROM cabinet.crm_state t1
	LEFT JOIN cabinet.cab_user t11 ON 1=1
	LEFT JOIN cabinet.crm_state_user_settings t12 ON t1.state_id = t12.state_id and t11.user_id = coalesce(t12.user_id, t11.user_id)
	;

ALTER TABLE cabinet.vw_crm_state OWNER TO pavlov;


-- select * from cabinet.vw_crm_state order by user_id, state_id, state_name, fore_color;
