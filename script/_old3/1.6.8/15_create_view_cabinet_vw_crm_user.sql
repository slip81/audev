﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_user
*/

-- DROP VIEW cabinet.vw_crm_user;

CREATE OR REPLACE VIEW cabinet.vw_crm_user AS
	SELECT t1.user_id as crm_user_id, t1.user_name as crm_user_name, 
	t1.cab_user_id, t1.is_boss, t1.allow_bucket_read, t1.allow_bucket_write, t1.allow_prepared_read,
	t1.allow_prepared_write, t1.allow_fixed_read, t1.allow_fixed_write, t1.allow_approved_read, t1.allow_approved_write,  	
	t11.user_id,
	case when t12.fore_color is null then '#000000' else t12.fore_color end as fore_color
	FROM cabinet.crm_user t1
	LEFT JOIN cabinet.cab_user t11 ON 1=1
	LEFT JOIN cabinet.crm_user_user_settings t12 ON t1.user_id = t12.crm_user_id and t11.user_id = coalesce(t12.user_id, t11.user_id)
	;

ALTER TABLE cabinet.vw_crm_user OWNER TO pavlov;


-- select * from cabinet.vw_crm_user order by user_id, priority_id, priority_name, fore_color;