﻿/*
	CREATE TABLE cabinet.crm_task_rel_type
*/

-- DROP TABLE cabinet.crm_task_rel_type;

CREATE TABLE cabinet.crm_task_rel_type
(
  type_id integer NOT NULL,
  type_name character varying,
  CONSTRAINT crm_task_rel_type_pkey PRIMARY KEY (type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_rel_type OWNER TO pavlov;


insert into cabinet.crm_task_rel_type (type_id, type_name)
values (1, 'Дубль');

insert into cabinet.crm_task_rel_type (type_id, type_name)
values (2, 'Схожая задача');

-- select * from cabinet.crm_task_rel_type;

-- DROP TABLE cabinet.crm_task_rel;

CREATE TABLE cabinet.crm_task_rel
(
  item_id bigserial NOT NULL,
  task_id integer NOT NULL,
  child_task_id integer NOT NULL,
  rel_type_id integer NOT NULL,
  child_task_num integer,
  CONSTRAINT crm_task_rel_pkey PRIMARY KEY (item_id),
  CONSTRAINT crm_task_rel_child_task_id_fkey FOREIGN KEY (child_task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_rel_rel_type_id_fkey FOREIGN KEY (rel_type_id)
      REFERENCES cabinet.crm_task_rel_type (type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_rel_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_rel OWNER TO pavlov;

-- select * from cabinet.crm_task_rel;