﻿/*
	insert into cabinet.cab_grid_column
*/

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus, format)
values (21, 1, 'upd_date', 'Дата изменения', '{0: dd.MM.yyyy HH:mm:ss}');

-- select * from cabinet.cab_grid_column order by column_id

insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
select distinct 21, user_id, 20, false, 200, true, true
from cabinet.cab_grid_column_user_settings;

-- select * from cabinet.cab_grid_column_user_settings order by user_id, column_num

-- select * from cabinet.vw_cab_grid_column_user_settings order by user_id, column_num