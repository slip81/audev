﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN upd_date
*/

ALTER TABLE cabinet.crm_task ADD COLUMN upd_date timestamp without time zone;
COMMENT ON COLUMN cabinet.crm_task.upd_date IS 'Дата изменения задачи';
