﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_task_rel
*/

-- DROP VIEW cabinet.vw_crm_task_rel;

CREATE OR REPLACE VIEW cabinet.vw_crm_task_rel AS 
 SELECT t0.item_id,
    t0.task_id,
    t0.child_task_num,
    t0.rel_type_id,
    t16.type_name AS rel_type_name,
    t1.task_id AS child_task_id,
    t1.task_name,
    t1.task_text,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.module_version_id,
    t1.client_id,
    t1.priority_id,
    t1.state_id,
    t1.owner_user_id,
    t1.exec_user_id,
    t1.task_num,
    t1.crt_date,
    t1.required_date,
    t1.repair_date,
    t1.repair_version_id,
    t2.client_name,
    t3.module_name,
    t4.module_part_name,
    t5.module_version_name,
    t6.priority_name,
    t7.project_name,
    t8.state_name,
    t9.user_name AS owner_user_name,
    t10.user_name AS exec_user_name,
    t21.module_version_name AS repair_version_name,
    replace(t1.task_num::text, 'AU-'::text, ''::text)::integer AS task_num_int,
    t1.group_id,
    t1.point,
    t1.prepared_user_id,
    t1.fixed_user_id,
    t1.approved_user_id,
    t11.user_name AS prepared_user_name,
    t12.user_name AS fixed_user_name,
    t13.user_name AS approved_user_name,
    t1.assigned_user_id,
    t14.user_name AS assigned_user_name,
    t15.group_name
   FROM cabinet.crm_task_rel t0
     JOIN cabinet.crm_task t1 ON t0.child_task_id = t1.task_id
     JOIN cabinet.crm_client t2 ON t1.client_id = t2.client_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id
     JOIN cabinet.crm_priority t6 ON t1.priority_id = t6.priority_id
     JOIN cabinet.crm_project t7 ON t1.project_id = t7.project_id
     JOIN cabinet.crm_state t8 ON t1.state_id = t8.state_id
     JOIN cabinet.cab_user t9 ON t1.owner_user_id = t9.user_id
     JOIN cabinet.cab_user t10 ON t1.exec_user_id = t10.user_id
     JOIN cabinet.cab_user t11 ON t1.prepared_user_id = t11.user_id
     JOIN cabinet.cab_user t12 ON t1.fixed_user_id = t12.user_id
     JOIN cabinet.cab_user t13 ON t1.approved_user_id = t13.user_id
     JOIN cabinet.cab_user t14 ON t1.assigned_user_id = t14.user_id
     JOIN cabinet.crm_group t15 ON t1.group_id = t15.group_id
     JOIN cabinet.crm_task_rel_type t16 ON t0.rel_type_id = t16.type_id
     LEFT JOIN cabinet.crm_module_version t21 ON t1.repair_version_id = t21.module_version_id;

ALTER TABLE cabinet.vw_crm_task_rel OWNER TO pavlov;
