﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_task_curr_exec
*/

-- DROP VIEW cabinet.vw_crm_task_curr_exec;

CREATE OR REPLACE VIEW cabinet.vw_crm_task_curr_exec AS 
 SELECT count(t1.task_id) AS cnt_task_id,
    t1.exec_user_id,
    t2.user_name AS exec_user_name
   FROM cabinet.crm_task t1
     JOIN cabinet.cab_user t2 ON t1.exec_user_id = t2.user_id
  WHERE t1.exec_user_id > 0 AND t1.group_id = 3 AND t1.is_archive = false
  GROUP BY t1.exec_user_id, t2.user_name;

ALTER TABLE cabinet.vw_crm_task_curr_exec OWNER TO pavlov;
