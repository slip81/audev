﻿/*
	ALTER TABLE cabinet.cab_user
*/

ALTER TABLE cabinet.cab_user DROP COLUMN is_admin;

ALTER TABLE cabinet.cab_user ADD COLUMN is_crm_user boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.cab_user ADD COLUMN is_executer boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.cab_user ADD COLUMN crm_user_role_id integer;
ALTER TABLE cabinet.cab_user ADD COLUMN old_crm_user_id integer;

COMMENT ON COLUMN cabinet.cab_user.is_crm_user IS 'True - в задачах';
COMMENT ON COLUMN cabinet.cab_user.is_executer IS 'True - в календаре';
COMMENT ON COLUMN cabinet.cab_user.crm_user_role_id IS 'Код роли пользователя в задачах';

ALTER TABLE cabinet.cab_user
  ADD CONSTRAINT cab_user_crm_user_role_id_fkey FOREIGN KEY (crm_user_role_id)
      REFERENCES cabinet.crm_user_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE cabinet.cab_user SET old_crm_user_id = 
(SELECT x1.user_id FROM cabinet.crm_user x1 WHERE x1.cab_user_id = cabinet.cab_user.user_id)
;

UPDATE cabinet.cab_user SET crm_user_role_id = 
(SELECT x1.role_id FROM cabinet.crm_user x1 WHERE x1.user_id = cabinet.cab_user.old_crm_user_id)
;

UPDATE cabinet.cab_user SET is_executer = 
coalesce((SELECT coalesce(x1.is_executer, false) FROM cabinet.crm_user x1 WHERE x1.user_id = cabinet.cab_user.old_crm_user_id), false)
;

UPDATE cabinet.cab_user SET is_crm_user = true WHERE coalesce(old_crm_user_id, -1) > -1
;


/*
select * from cabinet.crm_user where user_id not in
(select old_crm_user_id from cabinet.cab_user)
order by user_id;
*/

----------------------------

ALTER TABLE cabinet.crm_user_task_follow DROP CONSTRAINT crm_user_task_follow_user_id_fkey;

UPDATE cabinet.crm_user_task_follow SET user_id = 
(SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_user_task_follow.user_id)
;

ALTER TABLE cabinet.crm_user_task_follow
  ADD CONSTRAINT crm_user_task_follow_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

----------------------------      

ALTER TABLE cabinet.crm_task_note DROP CONSTRAINT crm_task_note_owner_user_id_fkey;

UPDATE cabinet.crm_task_note SET owner_user_id = 
coalesce((SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_task_note.owner_user_id), 0)
;

ALTER TABLE cabinet.crm_task_note
  ADD CONSTRAINT crm_task_note_owner_user_id_fkey FOREIGN KEY (owner_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      

----------------------------

ALTER TABLE cabinet.crm_task DROP CONSTRAINT crm_task_approved_user_id_fkey;
ALTER TABLE cabinet.crm_task DROP CONSTRAINT crm_task_assigned_user_id_fkey;
ALTER TABLE cabinet.crm_task DROP CONSTRAINT crm_task_event_user_id_fkey;
ALTER TABLE cabinet.crm_task DROP CONSTRAINT crm_task_fixed_user_id_fkey;
ALTER TABLE cabinet.crm_task DROP CONSTRAINT crm_task_owner_user_id_fkey;
ALTER TABLE cabinet.crm_task DROP CONSTRAINT crm_task_prepared_user_id_fkey;

UPDATE cabinet.crm_task SET approved_user_id = 
coalesce((SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_task.approved_user_id), 0)
;
UPDATE cabinet.crm_task SET assigned_user_id = 
coalesce((SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_task.assigned_user_id), 0)
;
UPDATE cabinet.crm_task SET event_user_id = 
(SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_task.event_user_id)
;
UPDATE cabinet.crm_task SET fixed_user_id = 
coalesce((SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_task.fixed_user_id), 0)
;
UPDATE cabinet.crm_task SET owner_user_id = 
coalesce((SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_task.owner_user_id), 0)
;
UPDATE cabinet.crm_task SET prepared_user_id = 
coalesce((SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_task.prepared_user_id), 0)
;
UPDATE cabinet.crm_task SET exec_user_id = 
coalesce((SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_task.exec_user_id), 0)
;

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_approved_user_id_fkey FOREIGN KEY (approved_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_assigned_user_id_fkey FOREIGN KEY (assigned_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_event_user_id_fkey FOREIGN KEY (event_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_fixed_user_id_fkey FOREIGN KEY (fixed_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_owner_user_id_fkey FOREIGN KEY (owner_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_prepared_user_id_fkey FOREIGN KEY (prepared_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;                            
            
----------------------------

ALTER TABLE cabinet.crm_subtask DROP CONSTRAINT crm_subtask_owner_user_id_fkey;

UPDATE cabinet.crm_subtask SET owner_user_id = 
(SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_subtask.owner_user_id)
;

ALTER TABLE cabinet.crm_subtask
  ADD CONSTRAINT crm_subtask_owner_user_id_fkey FOREIGN KEY (owner_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

----------------------------

ALTER TABLE cabinet.crm_project_user DROP CONSTRAINT crm_project_user_user_id_fkey;

UPDATE cabinet.crm_project_user SET user_id = 
(SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_project_user.user_id)
;

ALTER TABLE cabinet.crm_project_user
  ADD CONSTRAINT crm_project_user_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
      
----------------------------

ALTER TABLE cabinet.crm_event DROP CONSTRAINT crm_event_exec_user_id_fkey;

UPDATE cabinet.crm_event SET exec_user_id = 
(SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.crm_event.exec_user_id)
;

ALTER TABLE cabinet.crm_event
  ADD CONSTRAINT crm_event_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
      
----------------------------

ALTER TABLE cabinet.cab_grid_user_settings
  ADD CONSTRAINT cab_grid_user_settings_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
      
----------------------------

ALTER TABLE cabinet.cab_grid_column_user_settings
  ADD CONSTRAINT cab_grid_column_user_settings_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
      
----------------------------

ALTER TABLE cabinet.notify DROP CONSTRAINT notify_target_user_id_fkey;

UPDATE cabinet.notify SET target_user_id = 
(SELECT x1.user_id FROM cabinet.cab_user x1 WHERE x1.old_crm_user_id = cabinet.notify.target_user_id)
;

ALTER TABLE cabinet.notify
  ADD CONSTRAINT notify_target_user_id_fkey FOREIGN KEY (target_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

----------------------------      

-- VACUUM ANALYZE;
-- select * from cabinet.cab_user order by user_id;

