﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_cab_user2
*/

DROP VIEW cabinet.vw_cab_user2;

CREATE OR REPLACE VIEW cabinet.vw_cab_user2 AS 
 SELECT t1.user_id,
    t1.user_name,
    t1.user_login,
    t1.is_superadmin,
    t1.full_name,
    t1.email,
    t1.is_crm_user,
    t1.is_executer,
    t1.crm_user_role_id,
    t1.icq,
    t1.is_active,
    t2.role_name as crm_user_role_name
   FROM cabinet.cab_user t1
   LEFT JOIN cabinet.crm_user_role t2 ON t1.crm_user_role_id = t2.role_id
   ;

ALTER TABLE cabinet.vw_cab_user2 OWNER TO pavlov;
