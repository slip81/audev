﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_event
*/

-- DROP VIEW cabinet.vw_crm_event;

CREATE OR REPLACE VIEW cabinet.vw_crm_event AS 
 SELECT t1.event_id,
    t1.event_name,
    t1.event_text,
    t1.task_id,
    t1.date_beg,
    t1.date_end,
    t1.exec_user_id,
    t1.is_done,
    t1.crt_date,
    t1.crt_user,
    t1.is_canceled,
    t1.date_plan,
    t1.event_progress,
    t1.event_result,
    t1.task_state_id,
    t1.date_fact,
    t1.event_group_id,
    t11.task_num,
    t11.task_name,
    t11.task_text,
    t12.user_name AS exec_user_name,
        CASE
            WHEN COALESCE(t1.task_id, 0) > 0 THEN t13.state_name
            ELSE
            CASE
                WHEN t1.is_done THEN 'Выполнено'::character varying
                ELSE
                CASE
                    WHEN t1.is_canceled THEN 'Отменено'::character varying
                    ELSE 'Активно'::character varying
                END
            END
        END AS state_name,
    t14.group_name AS event_group_name,
    t12.crm_user_role_id AS exec_user_role_id,
    COALESCE(t15.ignore_date_plan, false) AS ignore_date_plan,
        CASE
            WHEN COALESCE(t12.crm_user_role_id, 0) > 0 THEN
            CASE
                WHEN COALESCE(t16.state_id, (-1)) = COALESCE(t1.task_state_id, 0) THEN true
                ELSE false
            END
            ELSE t1.is_done
        END AS is_done_user_role
   FROM cabinet.crm_event t1
     LEFT JOIN cabinet.crm_task t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.cab_user t12 ON t1.exec_user_id = t12.user_id
     LEFT JOIN cabinet.crm_state t13 ON t1.task_state_id = t13.state_id
     LEFT JOIN cabinet.crm_event_group t14 ON t1.event_group_id = t14.group_id
     LEFT JOIN cabinet.crm_user_role t15 ON t12.crm_user_role_id = t15.role_id
     LEFT JOIN cabinet.crm_user_role_state_done t16 ON t15.role_id = t16.role_id AND t1.task_state_id = t16.state_id;

ALTER TABLE cabinet.vw_crm_event OWNER TO pavlov;
