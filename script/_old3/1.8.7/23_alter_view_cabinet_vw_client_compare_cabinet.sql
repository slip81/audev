﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_compare_cabinet
*/

-- DROP VIEW cabinet.vw_client_compare_cabinet;

CREATE OR REPLACE VIEW cabinet.vw_client_compare_cabinet AS 
 SELECT t1.id,
    t1.city_id,
    t1.full_name,
    t1.inn,
    t1.kpp,
    t1.legal_address,
    t1.absolute_address,
    t1.bik,
    t1.cell AS phone,
    t1.site AS email,
    t1.contact_person,
    t1.zip_code,
    t1.post,
    t1.sm_id,
    t2.name AS city_name,
    t3.name AS region_name,
    t4.state_id,
    t4.state_name,
    t5.name_company_full AS crm_name_company_full,
    t5.name_region AS crm_name_region,
    t5.name_city AS crm_name_city,
    t5.address AS crm_address,
    t5.phone AS crm_phone,
    t5.email AS crm_email,
    t5.inn AS crm_inn,
    t5.kpp AS crm_kpp,
    t5.face_fio AS crm_face_fio,
    t5.p_address AS crm_p_address
   FROM cabinet.client t1
     LEFT JOIN cabinet.city t2 ON t1.city_id = t2.id
     LEFT JOIN cabinet.region t3 ON t2.region_id = t3.id
     LEFT JOIN cabinet.client_state t4 ON t1.state_id = t4.state_id
     LEFT JOIN cabinet.company t5 ON t1.sm_id = t5.id_company
  WHERE t1.is_deleted = 0;

ALTER TABLE cabinet.vw_client_compare_cabinet OWNER TO pavlov;
