﻿/*
	insert into cabinet.client_state
*/

insert into cabinet.client_state (state_id, state_name)
select distinct id_state, name_state
from cabinet.company
where id_state is not null
and not exists(select x1.state_id from cabinet.client_state x1 where x1.state_id = cabinet.company.id_state)
;

-- select distinct id_state, name_state from cabinet.company;
-- select * from cabinet.client_state;