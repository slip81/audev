﻿/*
	ALTER TABLE cabinet.client ADD COLUMN state_id
*/

ALTER TABLE cabinet.client ADD COLUMN state_id integer;

ALTER TABLE cabinet.client
  ADD CONSTRAINT client_state_id_fkey FOREIGN KEY (state_id)
      REFERENCES cabinet.client_state (state_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;