﻿/*
	CREATE TABLE cabinet.delivery_template_file
*/

-- DROP TABLE cabinet.delivery_template_file;

CREATE TABLE cabinet.delivery_template_file
(
  file_id serial NOT NULL,
  template_id integer NOT NULL,
  file_name character varying,
  file_size bigint NOT NULL DEFAULT 0,
  file_ext character varying,
  file_content bytea,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT delivery_template_file_pkey PRIMARY KEY (file_id),
  CONSTRAINT delivery_template_file_template_id_fkey FOREIGN KEY (template_id)
      REFERENCES cabinet.delivery_template (template_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.delivery_template_file OWNER TO pavlov;


-- DROP TRIGGER cabinet_delivery_template_file_set_stamp_trg ON cabinet.delivery_template_file;

CREATE TRIGGER cabinet_delivery_template_file_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.delivery_template_file
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

