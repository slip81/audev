﻿/*
	CREATE OR REPLACE VIEW cabinet.allclients
*/

-- DROP VIEW cabinet.allclients;

CREATE OR REPLACE VIEW cabinet.allclients AS 
 SELECT t1.id,
    t1.name AS client_name,
    t1.cell AS phone,
    t2.id AS user_id,
    t2.name AS user_name,
    t4.id AS city_id,
    t4.name AS city_name,
    t4.region_id,
    t5.name AS region_name,
    t1.created_on AS reg_date,
    t2."lastActivityDate" AS last_activity_date,
    t1.created_by,
    1::smallint AS is_activated,
    t6.sales_count,
    t1.is_phis,
    t1.is_moderated,
    t1.contact_person,
    t7.business_id::text AS business_id,
    t1.inn,
    t1.legal_address,
    t1.absolute_address,
    t1.site,
    t1.kpp
   FROM cabinet.client t1
     LEFT JOIN cabinet.my_aspnet_users t2 ON t1.user_id = t2.id
     LEFT JOIN cabinet.my_aspnet_membership t3 ON t2.id = t3."userId"
     LEFT JOIN cabinet.city t4 ON t1.city_id = t4.id
     LEFT JOIN cabinet.region t5 ON t4.region_id = t5.id
     LEFT JOIN ( SELECT x1.client_id,
            count(x1.id) AS sales_count
           FROM cabinet.sales x1
          GROUP BY x1.client_id) t6 ON t1.id = t6.client_id
     LEFT JOIN discount.business t7 ON t1.id = t7.cabinet_client_id
  WHERE t1.is_deleted = 0;

ALTER TABLE cabinet.allclients OWNER TO pavlov;
