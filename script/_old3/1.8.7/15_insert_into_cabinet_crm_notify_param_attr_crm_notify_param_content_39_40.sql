﻿/*
	insert into cabinet.crm_notify_param_attr, cabinet.crm_notify_param_content 39, 40
*/

insert into cabinet.crm_notify_param_attr (notify_id, transport_type, column_id, is_active)
select distinct t1.notify_id, t1.transport_type, 39, true
from cabinet.crm_notify_param_attr t1;

insert into cabinet.crm_notify_param_attr (notify_id, transport_type, column_id, is_active)
select distinct t1.notify_id, t1.transport_type, 40, true
from cabinet.crm_notify_param_attr t1;

insert into cabinet.crm_notify_param_content (notify_id, transport_type, column_id, is_active)
select distinct t1.notify_id, t1.transport_type, 39, false
from cabinet.crm_notify_param_content t1;

insert into cabinet.crm_notify_param_content (notify_id, transport_type, column_id, is_active)
select distinct t1.notify_id, t1.transport_type, 40, false
from cabinet.crm_notify_param_content t1;
