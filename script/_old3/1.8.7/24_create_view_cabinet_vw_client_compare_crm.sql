﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_compare_crm
*/

-- DROP VIEW cabinet.vw_client_compare_crm;

CREATE OR REPLACE VIEW cabinet.vw_client_compare_crm AS 
	SELECT t1.id_company, t1.name_company_full, t1.name_region, t1.name_city,
	t1.address, t1.phone, t1.email, t1.inn, t1.kpp, t1.face_fio, t1.p_address,
	t1.id_client, t1.id_state, t1.name_state, t2.cabinet_client_id
	FROM cabinet.company t1
	LEFT JOIN (
	SELECT max(x1.id) as cabinet_client_id, x1.sm_id
	FROM cabinet.client x1 WHERE x1.sm_id is not null GROUP BY x1.sm_id
	) t2 ON t1.id_company = t2.sm_id
	;

ALTER TABLE cabinet.vw_client_compare_crm OWNER TO pavlov;