﻿/*
	update cabinet.cab_grid_column_user_settings 41, 42, 43
*/

update cabinet.cab_grid_column_user_settings
set column_num = 18, is_visible = false
where column_id = 41;

update cabinet.cab_grid_column_user_settings
set column_num = 19, is_visible = false
where column_id = 42;

update cabinet.cab_grid_column_user_settings
set column_num = 20, is_visible = false
where column_id = 43;