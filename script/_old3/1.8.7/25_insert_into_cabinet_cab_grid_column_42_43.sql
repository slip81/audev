﻿/*
	insert into cabinet.cab_grid_column 42, 43
*/

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus)
values (42, 2, 'state_name', 'Статус');

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus)
values (43, 2, 'sm_id', 'Код в CRM');

-- select * from cabinet.cab_grid_column order by column_id