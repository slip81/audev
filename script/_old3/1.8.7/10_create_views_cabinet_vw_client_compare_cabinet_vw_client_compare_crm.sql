﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_compare_cabinet, cabinet.vw_client_compare_crm
*/

-- DROP VIEW cabinet.vw_client_compare_cabinet;

CREATE OR REPLACE VIEW cabinet.vw_client_compare_cabinet AS 
	SELECT t1.id, t1.city_id, t1.full_name, t1.inn, t1.kpp, t1.legal_address, t1.absolute_address,
	t1.bik, t1.cell as phone, t1.site as email, t1.contact_person, t1.zip_code, t1.post, t1.sm_id,
	t2.name as city_name, t3.name as region_name
	FROM cabinet.client t1   
	LEFT JOIN cabinet.city t2 ON t1.city_id = t2.id
	LEFT JOIN cabinet.region t3 ON t2.region_id = t3.id
	WHERE t1.is_deleted = 0;

ALTER TABLE cabinet.vw_client_compare_cabinet OWNER TO pavlov;


------------------------------------------

-- DROP VIEW cabinet.vw_client_compare_crm;

CREATE OR REPLACE VIEW cabinet.vw_client_compare_crm AS 
	SELECT t1.id_company, t1.name_company_full, t1.name_region, t1.name_city,
	t1.address, t1.phone, t1.email, t1.inn, t1.kpp, t1.face_fio, t1.p_address	
	FROM cabinet.company t1;

ALTER TABLE cabinet.vw_client_compare_crm OWNER TO pavlov;