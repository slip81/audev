﻿/*
	ALTER TABLE cabinet.delivery ADD COLUMN num
*/

ALTER TABLE cabinet.delivery ADD COLUMN num integer NOT NULL DEFAULT 1;