﻿/*
	CREATE OR REPLACE FUNCTION cabinet.report_crm_task1
*/

DROP FUNCTION cabinet.report_crm_task1(integer, date, date);

CREATE OR REPLACE FUNCTION cabinet.report_crm_task1(
    IN _user_id integer,
    IN _date_beg date,
    IN _date_end date)
  RETURNS TABLE(id bigint, user_id integer, user_name character varying, date_beg timestamp without time zone, date_end timestamp without time zone, task_id integer, mess text, task_num character varying, task_name character varying, required_date timestamp without time zone, repair_date timestamp without time zone, state_name character varying, client_name character varying, project_name character varying, module_name character varying, module_version_name character varying, repair_version_name character varying, priority_name character varying, group_name character varying, subtasks_text text, comments_text text) AS
$BODY$
BEGIN
	RETURN QUERY
		select row_number() over (order by t1.task_id) as id, t1.user_id, t3.user_name
		-- , t1.date_beg::date as date_beg_date_only, t1.date_beg
		, min(t1.date_beg) as date_beg, max(t1.date_beg) as date_end
		, t1.task_id
		-- , t1.mess
		, string_agg('[' || to_char(t1.date_beg,'YYYY-MM-DD HH24:MI:SS') || '] ' || t1.mess, (E'\n') ORDER BY t1.task_id DESC, t1.date_beg DESC) as mess
		, t2.task_num, t2.task_name, t2.required_date, t2.repair_date
		, t4.state_name, t5.client_name, t6.project_name, t7.module_name, t8.module_version_name
		, t9.module_version_name as repair_version_name, t10.priority_name, t11.group_name
		, null::text as subtasks_text, null::text as comments_text
		from cabinet.log_crm t1
		left join cabinet.crm_task t2 on t1.task_id = t2.task_id
		left join cabinet.cab_user t3 on t1.user_id = t3.user_id
		left join cabinet.crm_state t4 on t2.state_id = t4.state_id
		left join cabinet.crm_client t5 on t2.client_id = t5.client_id
		left join cabinet.crm_project t6 on t2.project_id = t6.project_id
		left join cabinet.crm_module t7 on t2.module_id = t7.module_id
		left join cabinet.crm_module_version t8 on t2.module_version_id = t8.module_version_id
		left join cabinet.crm_module_version t9 on t2.repair_version_id = t9.module_version_id
		left join cabinet.crm_priority t10 on t2.priority_id = t10.priority_id
		left join cabinet.crm_group t11 on t2.group_id = t11.group_id
		where t1.user_id = _user_id
		and (((t1.date_beg::date >= _date_beg) and (_date_beg is not null)) or (_date_beg is null))
		and (((t1.date_beg::date < _date_end) and (_date_end is not null)) or (_date_end is null))		
		group by t1.user_id, t3.user_name, /*t1.date_beg::date, t1.date_beg,*/ t1.task_id		
		, t2.task_num, t2.task_name, t2.required_date, t2.repair_date
		, t4.state_name, t5.client_name, t6.project_name, t7.module_name, t8.module_version_name
		, t9.module_version_name, t10.priority_name, t11.group_name		
		;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION cabinet.report_crm_task1(integer, date, date) OWNER TO pavlov;
