﻿/*
	CREATE TABLE cabinet.client_state
*/

-- DROP TABLE cabinet.client_state;

CREATE TABLE cabinet.client_state
(
  state_id integer NOT NULL,
  state_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT client_state_pkey PRIMARY KEY (state_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.client_state OWNER TO pavlov;

-- DROP TRIGGER cabinet_client_state_set_stamp_trg ON cabinet.client_state;

CREATE TRIGGER cabinet_client_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.client_state
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

