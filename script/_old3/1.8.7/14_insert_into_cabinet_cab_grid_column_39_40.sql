﻿/*
	insert into cabinet.cab_grid_column 39, 40
*/

/*
insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus, templ, title)
values (39, 1, 'subtasks_text', 'Подзадачи', '#if(subtasks_text != null){# #= subtasks_count # #} else {# # } #', '#: subtasks_text #');

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus, templ, title)
values (40, 1, 'notes_text', 'Комментарии', '#if(notes_text != null){# #= notes_count # #} else {# # } #', '#: notes_text #');
*/

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus, templ, title)
values (39, 1, 'subtasks_count', 'Подзадачи', '#if(subtasks_text != null){# #= subtasks_count # #} else {# # } #', '#: subtasks_text #');

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus, templ, title)
values (40, 1, 'notes_count', 'Комментарии', '#if(notes_text != null){# #= notes_count # #} else {# # } #', '#: notes_text #');


-- select * from cabinet.cab_grid_column where grid_id = 1 order by column_id

-- delete from cabinet.cab_grid_column where column_id in (39, 40);

