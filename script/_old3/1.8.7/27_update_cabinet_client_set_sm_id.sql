﻿/*
select * from cabinet.client;
select * from cabinet.client where sm_id is null;
select sm_id, * from cabinet.client where sm_id is not null order by sm_id;
*/


update cabinet.client
set sm_id = (select max(x1.id_company) from cabinet.company x1 where trim(coalesce(x1.inn,'')) <> '' and trim(x1.inn) = trim(cabinet.client.inn))
where sm_id is null 
and trim(coalesce(inn,'')) <> '';

update cabinet.client
set state_id = (select x1.id_state from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
where sm_id is not null
and state_id is null;

update cabinet.client
set kpp = (select x1.kpp from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
where sm_id is not null
and trim(coalesce(kpp,'')) = '';

update cabinet.client
set contact_person = (select x1.face_fio from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
where sm_id is not null
and trim(coalesce(contact_person,'')) = '';

update cabinet.client
set cell = (select x1.phone from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
where sm_id is not null
and trim(coalesce(cell,'')) = '';

update cabinet.client
set site = (select x1.email from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
where sm_id is not null
and trim(coalesce(site,'')) = '';

update cabinet.client
set legal_address = (select x1.address from cabinet.company x1 where x1.id_company = cabinet.client.sm_id and trim(coalesce(x1.p_address,'')) = '')
where sm_id is not null
and trim(coalesce(legal_address,'')) = '';

update cabinet.client
set legal_address = (select x1.p_address from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
where sm_id is not null
and trim(coalesce(legal_address,'')) = '';

update cabinet.client
set absolute_address = (select x1.address from cabinet.company x1 where x1.id_company = cabinet.client.sm_id and trim(coalesce(x1.p_address,'')) = '')
where sm_id is not null
and trim(coalesce(absolute_address,'')) = '';

update cabinet.client
set absolute_address = (select x1.p_address from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
where sm_id is not null
and trim(coalesce(absolute_address,'')) = '';