﻿/*
	insert simple users
*/

-- select * from cabinet.my_aspnet_users order by id  limit 100
-- select * from cabinet.my_aspnet_membership order by "userId" desc limit 100
-- select * from cabinet.my_aspnet_membership order by "userId" limit 100
-- select * from cabinet.vw_client_user  where client_id = 1000

insert into cabinet.my_aspnet_users ("applicationId", name, "isAnonymous", "lastActivityDate")
select distinct 1, trim(coalesce(user_name, '')), 0, current_timestamp
from cabinet.cab_user_role
where trim(coalesce(user_name, '')) != ''
and trim(coalesce(user_name, '')) != 'elita1'
;

insert into cabinet.my_aspnet_membership ("userId", "Email", "Password", "PasswordKey", "PasswordFormat", "IsApproved")
select t1.id, '', 'gGwGiG', 'gGwGiG', 2, 1
from cabinet.my_aspnet_users t1
inner join cabinet.cab_user_role t2 on trim(coalesce(t1.name, '')) = trim(coalesce(t2.user_name, ''))
and trim(coalesce(t2.user_name, '')) != 'elita1'
;

insert into cabinet.client_user (user_id, client_id, user_name, user_name_full, is_main_for_sales, need_change_pwd)
select t1.id, t5.cabinet_client_id, trim(coalesce(t2.user_name, '')), trim(coalesce(t2.user_name, '')), false, true
from cabinet.my_aspnet_users t1
inner join cabinet.cab_user_role t2 on trim(coalesce(t1.name, '')) = trim(coalesce(t2.user_name, ''))
and trim(coalesce(t2.user_name, '')) != 'elita1'
inner join discount.business_org_user t3 on trim(coalesce(t2.user_name, '')) = trim(coalesce(t3.user_name, ''))
inner join discount.business_org t4 on t3.org_id = t4.org_id
inner join discount.business t5 on t4.business_id = t5.business_id
inner join cabinet.client t6 on t5.cabinet_client_id = t6.id
;

-- select * from cabinet.cab_user order by user_id
-- select * from cabinet.cab_user_role order by user_name

/*
select t1.*, t4.cabinet_client_id
from cabinet.cab_user_role t1
inner join discount.business_org_user t2 on trim(coalesce(t1.user_name, '')) = trim(coalesce(t2.user_name, ''))
inner join discount.business_org t3 on t2.org_id = t3.org_id
inner join discount.business t4 on t3.business_id = t4.business_id
*/

--select * from cabinet.my_aspnet_users where id = 2299