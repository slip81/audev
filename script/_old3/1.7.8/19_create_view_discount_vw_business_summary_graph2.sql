﻿/*
	CREATE OR REPLACE VIEW discount.vw_business_summary_graph2
*/

-- DROP VIEW discount.vw_business_summary_graph;

CREATE OR REPLACE VIEW discount.vw_business_summary_graph2 AS 
 SELECT row_number() OVER (ORDER BY t2.business_id) AS id,
    t2.business_id,
    t1.date_beg::date AS date_beg_date,
    sum(t6.unit_value) AS trans_sum_total,
    sum(t6.unit_value_discount) AS trans_sum_discount,
    sum(t6.unit_value_with_discount) AS trans_sum_with_discount,
    sum(t6.unit_value_bonus_for_card) AS trans_sum_bonus_for_card,
    sum(t6.unit_value_bonus_for_pay) AS trans_sum_bonus_for_pay
   FROM discount.card_transaction t1
     JOIN discount.card t2 ON t1.card_id = t2.card_id
     LEFT JOIN discount.card_transaction_detail t6 ON t1.card_trans_id = t6.card_trans_id
  WHERE t1.trans_kind = 1 AND t1.status = 0 AND t1.date_beg >= ('now'::text::date - 7) 
  GROUP BY t2.business_id, t1.date_beg::date
  ORDER BY t2.business_id, t1.date_beg::date;

ALTER TABLE discount.vw_business_summary_graph2 OWNER TO pavlov;
