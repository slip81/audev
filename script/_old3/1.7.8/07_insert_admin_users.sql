﻿/*
	insert admin users
*/

-- select * from cabinet.my_aspnet_users order by id  limit 100
-- select * from cabinet.my_aspnet_membership order by "userId" desc limit 100
-- select * from cabinet.my_aspnet_membership order by "userId" limit 100
-- select * from cabinet.vw_client_user  where client_id = 1000

update cabinet.client_user 
set need_change_pwd = true
where user_id not in 
(select "userId" from cabinet.my_aspnet_membership where "PasswordFormat" = 2);

insert into cabinet.my_aspnet_users (id, "applicationId", name, "isAnonymous", "lastActivityDate")
select user_id, 1, user_login, 0, current_timestamp
from cabinet.cab_user
where user_id > 0;

insert into cabinet.my_aspnet_membership ("userId", "Email", "Password", "PasswordKey", "PasswordFormat", "IsApproved")
select t1.id, t2.email, 'gGwGiG', 'gGwGiG', 2, case when t2.is_active then 1 else 0 end
from cabinet.my_aspnet_users t1
inner join cabinet.cab_user t2 on t1.id = t2.user_id;

insert into cabinet.client_user (user_id, client_id, user_name, user_name_full, is_main_for_sales, need_change_pwd)
select t1.id, 1000, t2.user_name, t2.full_name, false, true
from cabinet.my_aspnet_users t1
inner join cabinet.cab_user t2 on t1.id = t2.user_id;

-- select * from cabinet.cab_user order by user_id