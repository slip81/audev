﻿/*
	INSERT INTO cabinet.client_service
*/

DELETE FROM cabinet.client_service;

INSERT INTO cabinet.client_service (client_id, service_id, state)
SELECT DISTINCT client_id, service_id, 0 FROM cabinet.vw_client_service;

-- select * FROM cabinet.client_service;
