﻿/*
	ADD CONSTRAINT client_user_widget_user_id_fkey
*/

ALTER TABLE cabinet.client_user_widget
  ADD CONSTRAINT client_user_widget_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.client_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
