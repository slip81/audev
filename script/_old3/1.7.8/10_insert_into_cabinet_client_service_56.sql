﻿/*
	insert into cabinet.client_service 56
*/

insert into cabinet.client_service (client_id, service_id, state)
select distinct cabinet_client_id, 56, 0 
from discount.business t1
inner join cabinet.client t2 on t1.cabinet_client_id = t2.id
where coalesce(cabinet_client_id, 0) > 0
;

-- select distinct cabinet_client_id from discount.business where coalesce(cabinet_client_id, 0) > 0
-- select * from cabinet.service order by id desc