﻿/*
	CREATE TABLE cabinet.client_user_widget
*/

-- DROP TABLE cabinet.client_user_widget;

CREATE TABLE cabinet.client_user_widget
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  widget_id integer NOT NULL,
  sort_num integer,
  CONSTRAINT client_user_widget_pkey PRIMARY KEY (item_id),
  /*
  CONSTRAINT client_user_widget_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.client_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  */
  CONSTRAINT client_user_widget_widget_id_fkey FOREIGN KEY (widget_id)
      REFERENCES cabinet.widget (widget_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.client_user_widget OWNER TO pavlov;
