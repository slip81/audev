﻿/*
	CREATE TABLE cabinet.service_defect
*/

-- DROP TABLE cabinet.service_defect;

CREATE TABLE cabinet.service_defect
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  is_allow_download boolean NOT NULL DEFAULT false,
  CONSTRAINT service_defect_pkey PRIMARY KEY (item_id),
  CONSTRAINT service_defect_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_defect_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_defect OWNER TO pavlov;
