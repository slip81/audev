﻿/*
	CREATE TABLE cabinet.widget
*/

-- DROP TABLE cabinet.widget;

CREATE TABLE cabinet.widget
(
  widget_id integer NOT NULL,
  widget_name character varying,
  widget_descr character varying,
  widget_element_id character varying, 
  widget_header character varying,
  widget_content character varying,
  sort_num integer,
  is_active boolean NOT NULL DEFAULT true,
  service_id integer,
  CONSTRAINT widget_pkey PRIMARY KEY (widget_id),
  CONSTRAINT widget_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.widget OWNER TO pavlov;

insert into cabinet.widget (widget_id, widget_name, widget_descr, widget_element_id, widget_header, widget_content, sort_num, is_active, service_id)
values (1, 'Дисконтные карты', 'Дисконтные карты', 'widget-discount', null, null, 1, true, 56);

insert into cabinet.widget (widget_id, widget_name, widget_descr, widget_element_id, widget_header, widget_content, sort_num, is_active, service_id)
values (2, 'Брак', 'Брак', 'widget-defect', null, null, 2, true, 34);

insert into cabinet.widget (widget_id, widget_name, widget_descr, widget_element_id, widget_header, widget_content, sort_num, is_active, service_id)
values (3, 'Обмен', 'Обмен', 'widget-exchnage', null, null, 3, true, 52);

-- select * from cabinet.service order by id desc
-- select * from cabinet.widget order by widget_id