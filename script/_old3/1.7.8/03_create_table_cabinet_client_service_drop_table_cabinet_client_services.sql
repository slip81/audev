﻿/*
	CREATE TABLE cabinet.client_service
*/

DROP TABLE cabinet.client_services;

CREATE TABLE cabinet.client_service
(
  item_id serial NOT NULL,
  client_id integer NOT NULL,
  service_id integer NOT NULL,
  state integer NOT NULL DEFAULT 0,
  CONSTRAINT client_service_pkey PRIMARY KEY (item_id),
  CONSTRAINT client_service_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT client_service_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.client_service OWNER TO pavlov;
