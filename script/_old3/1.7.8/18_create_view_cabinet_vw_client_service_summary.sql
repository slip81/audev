﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service_summary
*/

-- DROP VIEW cabinet.vw_client_service_summary;

CREATE OR REPLACE VIEW cabinet.vw_client_service_summary AS 
 SELECT t1.id as client_id, t11.service_id, count(distinct t12.sales_id) as sales_cnt
 FROM cabinet.client t1
 LEFT JOIN cabinet.client_service t11 ON t1.id = t11.client_id
 LEFT JOIN cabinet.vw_client_service t12 ON t11.client_id = t12.client_id AND t11.service_id = t12.service_id
 WHERE t1.is_deleted = 0
 GROUP BY t1.id, t11.service_id
 ;

ALTER TABLE cabinet.vw_client_service_summary OWNER TO pavlov;
