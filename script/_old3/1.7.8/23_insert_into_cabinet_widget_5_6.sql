﻿/*
	insert into cabinet.widget 5, 6
*/

insert into cabinet.widget (widget_id, widget_name, widget_descr, widget_element_id, sort_num, is_active, service_id)
values (5, 'Брак на почту', 'Брак на почту', 'widget-senddefect', 5, true, 57);

insert into cabinet.widget (widget_id, widget_name, widget_descr, widget_element_id, sort_num, is_active, service_id)
values (6, 'Госреестр на почту', 'Госреестр на почту', 'widget-sendgrls', 6, true, 58);

-- select * from cabinet.service order by id desc
-- select * from cabinet.widget order by widget_id