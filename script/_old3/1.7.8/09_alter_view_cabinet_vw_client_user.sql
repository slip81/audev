﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_user
*/

DROP VIEW cabinet.vw_client_user;

CREATE OR REPLACE VIEW cabinet.vw_client_user AS 
 SELECT t1.user_id,
    t1.client_id,
    t1.sales_id,
    t1.user_name,
    t1.user_name_full,
    t1.is_main_for_sales,
    t1.need_change_pwd,
    t3.name AS client_name,
    t12.adress AS sales_address,
    t2.name AS user_login,
    t11."Email" AS user_email,
    t11."IsApproved" AS is_active
   FROM cabinet.client_user t1
     JOIN cabinet.my_aspnet_users t2 ON t1.user_id = t2.id
     JOIN cabinet.client t3 ON t1.client_id = t3.id
     LEFT JOIN cabinet.my_aspnet_membership t11 ON t2.id = t11."userId"
     LEFT JOIN cabinet.sales t12 ON t1.sales_id = t12.id;

ALTER TABLE cabinet.vw_client_user OWNER TO pavlov;
