﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_info
*/

-- DROP VIEW cabinet.vw_client_info;

CREATE OR REPLACE VIEW cabinet.vw_client_info AS 
	SELECT 
	t1.id as client_id,
	t1.name as client_name,
	(case when trim(coalesce(t12.name, '')) != '' then trim(coalesce(t12.name, '')) || ', ' else '' end) || (case when trim(coalesce(t11.name, '')) != '' then trim(coalesce(t11.name, '')) else '' end)
	as client_address,
	t1.cell as client_phone,
	t1.contact_person as client_contact,	
	count(distinct t13.id) as sales_count,
	count(distinct t14.service_id) as installed_service_cnt,
	count(distinct t15.service_id) as ordered_service_cnt
	FROM cabinet.client t1	
	LEFT JOIN cabinet.city t11 ON t1.city_id = t11.id	
	LEFT JOIN cabinet.region t12 ON t11.region_id = t12.id
	INNER JOIN cabinet.sales t13 ON t1.id = t13.client_id
	LEFT JOIN cabinet.client_service t14 ON t1.id = t14.client_id AND t14.state = 0
	LEFT JOIN cabinet.client_service t15 ON t1.id = t15.client_id AND t15.state = 1
	GROUP BY t1.id, t1.name, trim(coalesce(t12.name, '')), trim(coalesce(t11.name, '')), t1.cell, t1.contact_person
	;
	
ALTER TABLE cabinet.vw_client_info  OWNER TO pavlov;

