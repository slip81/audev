﻿/*
	insert into cabinet.service_defect
*/

insert into cabinet.service_defect (workplace_id, service_id, is_allow_download)
select distinct t2.workplace_id, 34, true
from esn.defect_client_allow t1 
inner join cabinet.vw_client_service t2 on t1.client_id = t2.client_id and t2.service_id = 34
and t2.workplace_id is not null;

-- select * from esn.defect_client_allow 
-- select * from cabinet.service order by id desc
-- select * from cabinet.service_defect