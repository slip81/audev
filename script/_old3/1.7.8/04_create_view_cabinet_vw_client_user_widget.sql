﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_user_widget
*/

-- DROP VIEW cabinet.vw_client_user_widget;

CREATE OR REPLACE VIEW cabinet.vw_client_user_widget AS 
 SELECT t1.widget_id,
  t1.widget_name,
  t1.widget_descr,
  t1.widget_element_id,
  t1.widget_header,
  t1.widget_content,
  t11.sort_num,
  t1.is_active,
  t1.service_id,
  t11.user_id,
  (case when t11.widget_id is null then false else true end) as is_installed,
  t12.client_id,
  t12.sales_id,
  t13.state
 FROM cabinet.widget t1
 LEFT JOIN cabinet.client_user_widget t11 ON t1.widget_id = t11.widget_id
 LEFT JOIN cabinet.client_user t12 ON t11.user_id = t12.user_id
 LEFT JOIN cabinet.client_service t13 ON t12.client_id = t13.client_id AND t1.service_id = t13.service_id
 ;


ALTER TABLE cabinet.vw_client_user_widget  OWNER TO pavlov;
