﻿/*
	ALTER TABLE cabinet.client_user ADD COLUMN need_change_pwd
*/

ALTER TABLE cabinet.client_user ADD COLUMN need_change_pwd boolean NOT NULL DEFAULT false;
