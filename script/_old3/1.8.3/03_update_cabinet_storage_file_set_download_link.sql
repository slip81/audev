﻿/*
	update cabinet.storage_file
*/

update cabinet.storage_file
set download_link = replace(download_link, 'http://192.168.0.101:5000/shara/storage/', 'http://shara.aptekaural.ru/shara/storage/')
where download_link like 'http://192.168.0.101:5000/shara/storage/%';


-- select * from cabinet.storage_file