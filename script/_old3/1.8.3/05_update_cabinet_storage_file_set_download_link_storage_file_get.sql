﻿/*
	UPDATE cabinet.storage_file SET download_link
*/

ALTER TABLE cabinet.storage_file ADD COLUMN download_link_old character varying;

UPDATE cabinet.storage_file
SET download_link_old = download_link;

UPDATE cabinet.storage_file
SET download_link = ('http://cab.aptekaural.ru/StorageFileGet?file_id=' || file_id::text);

-- select  * from cabinet.vw_storage_file order by file_id desc

ALTER TABLE cabinet.storage_file_version ADD COLUMN download_link_old character varying;

UPDATE cabinet.storage_file_version
SET download_link_old = download_link;

UPDATE cabinet.storage_file_version
SET download_link = ('http://cab.aptekaural.ru/StorageFileGet?file_id=' || file_id::text || '&version=' || version_num::text);


-- select  * from cabinet.storage_file_version order by file_id desc