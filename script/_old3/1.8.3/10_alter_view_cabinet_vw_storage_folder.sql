﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_storage_folder
*/

-- DROP VIEW cabinet.vw_storage_folder;

CREATE OR REPLACE VIEW cabinet.vw_storage_folder AS 
 SELECT t1.folder_id,
    t1.folder_name,
    t1.parent_id,
    t1.folder_level,
    t1.crt_date,
    t1.crt_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    coalesce(count(t11.file_id), 0) + coalesce(count(t13.link_id), 0) AS file_cnt,
    count(t12.folder_id) AS folder_cnt,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str
   FROM cabinet.storage_folder t1
     LEFT JOIN cabinet.storage_file t11 ON t1.folder_id = t11.folder_id AND t11.is_deleted = false
     LEFT JOIN cabinet.storage_folder t12 ON t1.folder_id = t12.parent_id AND t12.is_deleted = false
     LEFT JOIN cabinet.storage_link t13 ON t1.folder_id = t13.folder_id AND t13.is_deleted = false
  WHERE t1.is_deleted = false
  GROUP BY t1.folder_id, t1.folder_name, t1.parent_id, t1.folder_level, t1.crt_date, t1.crt_user, t1.is_deleted, t1.del_date, t1.del_user;

ALTER TABLE cabinet.vw_storage_folder OWNER TO pavlov;
