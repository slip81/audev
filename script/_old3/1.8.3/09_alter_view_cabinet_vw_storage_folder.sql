﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_storage_folder
*/

-- DROP VIEW cabinet.vw_storage_folder;

CREATE OR REPLACE VIEW cabinet.vw_storage_folder AS 
select t1.folder_id,
  t1.folder_name,
  t1.parent_id,
  t1.folder_level,
  t1.crt_date,
  t1.crt_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  count(t11.file_id) as file_cnt,
  count(t12.folder_id) as folder_cnt,
  to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str
from cabinet.storage_folder t1
left join cabinet.storage_file t11 on t1.folder_id = t11.folder_id and t11.is_deleted = false
left join cabinet.storage_folder t12 on t1.folder_id = t12.parent_id and t12.is_deleted = false
where t1.is_deleted = false
group by t1.folder_id,
  t1.folder_name,
  t1.parent_id,
  t1.folder_level,
  t1.crt_date,
  t1.crt_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user;

ALTER TABLE cabinet.vw_storage_folder OWNER TO pavlov;
