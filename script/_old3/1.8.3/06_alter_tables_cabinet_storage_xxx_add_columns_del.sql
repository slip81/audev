﻿/*
	ALTER TABLE cabinet.storage_file ADD COLUMN is_deleted
*/

ALTER TABLE cabinet.storage_file ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.storage_file ADD COLUMN del_date timestamp without time zone;
ALTER TABLE cabinet.storage_file ADD COLUMN del_user character varying;

ALTER TABLE cabinet.storage_folder ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.storage_folder ADD COLUMN del_date timestamp without time zone;
ALTER TABLE cabinet.storage_folder ADD COLUMN del_user character varying;

ALTER TABLE cabinet.storage_link ADD COLUMN is_deleted boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.storage_link ADD COLUMN del_date timestamp without time zone;
ALTER TABLE cabinet.storage_link ADD COLUMN del_user character varying;