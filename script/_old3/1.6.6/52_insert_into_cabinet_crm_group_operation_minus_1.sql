﻿/*
	insert into cabinet.crm_group_operation
*/

insert into cabinet.crm_group (group_id, group_name)
values (-1, 'Все');

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select -1, operation_id, operation_name
from cabinet.crm_task_operation;

update cabinet.crm_group_operation set operation_name = 'В неразобранные' where group_id = -1 and operation_id = 0;

-- select * from cabinet.crm_group_operation order by group_id, operation_id;