﻿/*
	insert into cabinet.crm_group (group_id, group_name)
*/

insert into cabinet.crm_group (group_id, group_name)
values (3, 'В работе');

update cabinet.crm_group
set group_name = 'Неразобранные'
where group_id = 0;

update cabinet.crm_group
set group_name = 'Обработанные'
where group_id = 1;

update cabinet.crm_group
set group_name = 'К утверждению'
where group_id = 2;

-- select * from cabinet.crm_group;