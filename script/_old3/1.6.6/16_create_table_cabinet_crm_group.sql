﻿/*
	CREATE TABLE cabinet.crm_group
*/

-- DROP TABLE cabinet.crm_group;

CREATE TABLE cabinet.crm_group
(
  group_id integer NOT NULL,
  group_name character varying,
  CONSTRAINT crm_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_group OWNER TO pavlov;

INSERT INTO cabinet.crm_group (group_id, group_name)
VALUES (0, 'Корзина');

INSERT INTO cabinet.crm_group (group_id, group_name)
VALUES (1, 'Фиксированные');

INSERT INTO cabinet.crm_group (group_id, group_name)
VALUES (2, 'В работе');

ALTER TABLE cabinet.crm_task ADD COLUMN group_id integer NOT NULL DEFAULT 0;

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.crm_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
