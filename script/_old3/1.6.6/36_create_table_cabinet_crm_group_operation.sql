﻿/*
	CREATE TABLE cabinet.crm_group_operation
*/

-- DROP TABLE cabinet.crm_group_operation;

CREATE TABLE cabinet.crm_group_operation
(
  item_id serial NOT NULL,
  group_id integer NOT NULL,
  operation_id integer NOT NULL,
  operation_name character varying,
  CONSTRAINT crm_group_operation_pkey PRIMARY KEY (item_id),
  CONSTRAINT crm_group_operation_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.crm_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_group_operation_operation_id_fkey FOREIGN KEY (operation_id)
      REFERENCES cabinet.crm_task_operation (operation_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_group_operation OWNER TO pavlov;
