﻿/*
	CREATE TABLE cabinet.cab_grid_user_settings
*/

-- DROP TABLE cabinet.cab_grid_user_settings;

CREATE TABLE cabinet.cab_grid_user_settings
(
  item_id serial NOT NULL,
  grid_id integer NOT NULL,
  user_id integer NOT NULL DEFAULT 0,
  grid_options text,
  CONSTRAINT cab_grid_user_settings_pkey PRIMARY KEY (item_id),
  CONSTRAINT cab_grid_user_settings_grid_id_fkey FOREIGN KEY (grid_id)
      REFERENCES cabinet.cab_grid (grid_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_grid_user_settings OWNER TO pavlov;
