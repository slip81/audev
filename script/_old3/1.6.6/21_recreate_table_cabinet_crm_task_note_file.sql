﻿/*
	CREATE TABLE cabinet.crm_task_note_file
*/

ALTER TABLE cabinet.crm_task_note ADD COLUMN file_name character varying;

DROP TABLE cabinet.crm_task_note_file;

CREATE TABLE cabinet.crm_task_note_file
(  
  note_id integer NOT NULL,  
  file_body bytea,
  CONSTRAINT crm_task_note_file_pkey PRIMARY KEY (note_id),
  CONSTRAINT crm_task_note_file_note_id_fkey FOREIGN KEY (note_id)
      REFERENCES cabinet.crm_task_note (note_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_note_file OWNER TO pavlov;
