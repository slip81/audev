﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_task_note AS 
*/

-- DROP VIEW cabinet.vw_crm_task_note;

CREATE OR REPLACE VIEW cabinet.vw_crm_task_note AS 
 SELECT t1.note_id,
  t1.task_id,
  t1.subtask_id,
  t1.crt_date,
  t1.owner_user_id,
  t1.note_text,
  t1.file_name,
  (case when coalesce(t2.note_id, 0) > 0 then true else false end) as is_file_exists,
  t3.user_name,
  t3.cab_user_id
   FROM cabinet.crm_task_note t1
   LEFT JOIN cabinet.crm_task_note_file t2 ON t1.note_id = t2.note_id
   LEFT JOIN cabinet.crm_user t3 ON t1.owner_user_id = t3.user_id
   ;


ALTER TABLE cabinet.vw_crm_task_note  OWNER TO pavlov;

-- select * from cabinet.vw_crm_task_note
