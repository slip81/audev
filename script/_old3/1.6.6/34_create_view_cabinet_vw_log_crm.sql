﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_log_crm
*/

-- DROP VIEW cabinet.vw_log_crm

CREATE OR REPLACE VIEW cabinet.vw_log_crm AS 
 SELECT t1.log_id, t1.date_beg, t1.date_end, t1.user_id, t1.mess, t1.task_id
	, t2.user_name, t2.full_name
	, t3.task_num, t3.task_name
	FROM cabinet.log_crm t1
	LEFT JOIN cabinet.cab_user t2 ON t1.user_id = t2.user_id
	LEFT JOIN cabinet.crm_task t3 ON t1.task_id = t3.task_id;

ALTER TABLE cabinet.vw_log_crm OWNER TO pavlov;


-- select * from cabinet.vw_log_crm