﻿/*
	CREATE TABLE cabinet.crm_task_user_filter
*/

-- DROP TABLE cabinet.crm_task_user_filter;

CREATE TABLE cabinet.crm_task_user_filter
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  grid_id integer NOT NULL,
  raw_filter character varying,
  project_filter character varying,
  client_filter character varying,
  priority_filter character varying,
  state_filter character varying,
  exec_user_filter character varying,
  owner_user_filter character varying,
  CONSTRAINT crm_task_user_filter_pkey PRIMARY KEY (item_id),
  CONSTRAINT crm_task_user_filter_grid_id_fkey FOREIGN KEY (grid_id)
      REFERENCES cabinet.cab_grid (grid_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_user_filter_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_user_filter OWNER TO pavlov;
