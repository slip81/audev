﻿/*
	CREATE TABLE cabinet.crm_project_user
*/

-- DROP TABLE cabinet.crm_project_user;

CREATE TABLE cabinet.crm_project_user
(
  item_id serial NOT NULL,
  project_id integer NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT crm_project_user_pkey PRIMARY KEY (item_id),
  CONSTRAINT crm_project_user_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_project_user_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_project_user OWNER TO pavlov;
