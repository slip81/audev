﻿/*
	ALTER TABLE cabinet.crm_user ADD COLUMN allow_xxx
*/

ALTER TABLE cabinet.crm_user ADD COLUMN allow_bucket boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_prepared boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_fixed boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_approved boolean NOT NULL DEFAULT false;