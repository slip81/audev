﻿/*
	insert into cabinet.crm_task_operation
*/

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (11, 'В контроль');

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (12, 'Из контроля');

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select group_id, 11, 'В контроль'
from cabinet.crm_group;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select group_id, 12, 'Из контроля'
from cabinet.crm_group;

-- select * from cabinet.crm_task_operation
-- select * from cabinet.crm_group_operation;