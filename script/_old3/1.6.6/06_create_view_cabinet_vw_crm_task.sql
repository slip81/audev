﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_task
*/

-- DROP VIEW cabinet.vw_crm_task;

CREATE OR REPLACE VIEW cabinet.vw_crm_task AS 
	SELECT t1.task_id,
	t1.task_name,
	t1.task_text,
	t1.project_id,
	t1.module_id,
	t1.module_part_id,
	t1.module_version_id,
	t1.client_id,
	t1.priority_id,
	t1.state_id,
	t1.owner_user_id,
	t1.exec_user_id,
	t1.task_num,
	t1.crt_date,
	t1.required_date,
	t1.repair_date,
	t1.repair_version_id,
	t2.client_name,
	t3.module_name,
	t4.module_part_name,
	t5.module_version_name,
	t6.priority_name,
	t7.project_name,
	t8.state_name,
	t9.user_name as owner_user_name,
	t10.user_name as exec_user_name,
	t11.module_version_name as repair_version_name
	FROM cabinet.crm_task t1
	INNER JOIN cabinet.crm_client t2 ON t1.client_id = t2.client_id
	INNER JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
	INNER JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
	INNER JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id 
	INNER JOIN cabinet.crm_priority t6 ON t1.priority_id = t6.priority_id
	INNER JOIN cabinet.crm_project t7 ON t1.project_id = t7.project_id
	INNER JOIN cabinet.crm_state t8 ON t1.state_id = t8.state_id
	INNER JOIN cabinet.crm_user t9 ON t1.owner_user_id = t9.user_id
	INNER JOIN cabinet.crm_user t10 ON t1.exec_user_id = t10.user_id
	LEFT JOIN cabinet.crm_module_version t11 ON t1.repair_version_id = t11.module_version_id 
	;
 

ALTER TABLE cabinet.vw_crm_task OWNER TO pavlov;
