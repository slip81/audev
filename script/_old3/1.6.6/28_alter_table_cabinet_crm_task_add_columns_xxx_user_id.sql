﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN xxx_user_id
*/

ALTER TABLE cabinet.crm_task ADD COLUMN prepared_user_id integer NOT NULL DEFAULT 0;
ALTER TABLE cabinet.crm_task ADD COLUMN fixed_user_id integer NOT NULL DEFAULT 0;
ALTER TABLE cabinet.crm_task ADD COLUMN approved_user_id integer NOT NULL DEFAULT 0;

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_prepared_user_id_fkey FOREIGN KEY (prepared_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_fixed_user_id_fkey FOREIGN KEY (fixed_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_approved_user_id_fkey FOREIGN KEY (approved_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;            