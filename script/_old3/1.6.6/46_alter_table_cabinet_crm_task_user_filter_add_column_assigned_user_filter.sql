﻿/*
	ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN assigned_user_filter character varying;
*/

ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN assigned_user_filter character varying;
