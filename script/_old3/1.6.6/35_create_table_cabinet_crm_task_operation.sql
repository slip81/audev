﻿/*
	CREATE TABLE cabinet.crm_task_operation
*/

-- DROP TABLE cabinet.crm_task_operation;

CREATE TABLE cabinet.crm_task_operation
(
  operation_id integer NOT NULL,
  operation_name character varying,
  CONSTRAINT crm_task_operation_pkey PRIMARY KEY (operation_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_operation OWNER TO pavlov;
