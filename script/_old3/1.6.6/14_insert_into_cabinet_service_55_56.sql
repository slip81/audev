﻿/*
	insert into cabinet.service
*/

insert into cabinet.service (guid, name, description, parent_id, is_deleted, exe_name, is_site, branch_id, price, need_key, "NeedCheckVersionLicense", is_active)
values ('1642c3d0-7c16-4ef6-853f-a525ce95024b', 'LivePrep', 'Накладных v2, проверка ЖНВЛП', 12, 0, 'LivePrep', 0, 1, 0, 1, 1, true);

insert into cabinet.service (guid, name, description, parent_id, is_deleted, exe_name, is_site, branch_id, price, need_key, "NeedCheckVersionLicense", is_active)
values (lower('0D6893B2-35FA-4663-826B-A609E3E6B6AC'), 'Дисконтный сервер', 'Дисконтный сервер', 12, 0, 'discount', 0, 1, 0, 0, 0, true);

update cabinet.service set is_service = true where id in (56, 55, 54, 52, 34);

update cabinet.service set priority = 1000 where id = 56;
update cabinet.service set priority = 950 where id = 52;
update cabinet.service set priority = 900 where id = 53;
update cabinet.service set priority = 850 where id = 55;
update cabinet.service set priority = 800 where id = 54;
update cabinet.service set priority = 750 where id = 34;
update cabinet.service set priority = 700 where id = 1;
update cabinet.service set priority = 650 where id = 11;
update cabinet.service set priority = 600 where id = 15;
update cabinet.service set priority = 550 where id = 16;
update cabinet.service set priority = 500 where id = 17;
update cabinet.service set priority = 450 where id = 18;
update cabinet.service set priority = 400 where id = 35;
update cabinet.service set priority = 350 where id = 43;

-- select * from cabinet.service order by priority desc
-- select * from cabinet.service order by id desc
-- select * from cabinet.service order by is_service desc
