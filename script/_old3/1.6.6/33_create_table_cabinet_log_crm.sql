﻿/*
	CREATE TABLE cabinet.log_crm
*/

-- DROP TABLE cabinet.log_crm;

CREATE TABLE cabinet.log_crm
(
  log_id bigserial NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  user_id integer,
  mess character varying,
  task_id integer,
  CONSTRAINT log_crm_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.log_crm OWNER TO pavlov;
