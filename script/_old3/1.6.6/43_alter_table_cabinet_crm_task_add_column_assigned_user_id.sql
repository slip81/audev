﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN assigned_user_id integer NOT NULL DEFAULT 0;
*/

ALTER TABLE cabinet.crm_task ADD COLUMN assigned_user_id integer NOT NULL DEFAULT 0;


ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_assigned_user_id_fkey FOREIGN KEY (assigned_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE cabinet.crm_task SET assigned_user_id = exec_user_id;