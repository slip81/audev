﻿/*
	ALTER TABLE cabinet.cab_grid_column ADD COLUMN format
*/

ALTER TABLE cabinet.cab_grid_column ADD COLUMN format character varying;

update cabinet.cab_grid_column
set format = '{0: dd.MM.yyyy}'
where column_id in (14, 15);

update cabinet.cab_grid_column
set format = '{0: dd.MM.yyyy HH:mm:ss}'
where column_id in (2);

delete from cabinet.cab_grid_column_user_settings;

delete from cabinet.cab_grid_column
where column_id in (9);

ALTER TABLE cabinet.cab_grid_column_user_settings DROP COLUMN format;

-- select * from cabinet.cab_grid_column