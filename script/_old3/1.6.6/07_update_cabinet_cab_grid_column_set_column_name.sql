﻿/*
	update cabinet.cab_grid_column set column_name
*/



update cabinet.cab_grid_column set column_name = 'state_name' where column_id = 5;
update cabinet.cab_grid_column set column_name = 'client_name' where column_id = 6;
update cabinet.cab_grid_column set column_name = 'exec_user_name' where column_id = 7;
update cabinet.cab_grid_column set column_name = 'module_name' where column_id = 8;
update cabinet.cab_grid_column set column_name = 'module_part_name' where column_id = 9;
update cabinet.cab_grid_column set column_name = 'module_version_name' where column_id = 10;
update cabinet.cab_grid_column set column_name = 'owner_user_name' where column_id = 11;
update cabinet.cab_grid_column set column_name = 'priority_name' where column_id = 12;
update cabinet.cab_grid_column set column_name = 'project_name' where column_id = 13;
update cabinet.cab_grid_column set column_name = 'repair_version_name' where column_id = 16;


/*
select * from cabinet.cab_grid
select * from cabinet.cab_grid_column order by column_id
select * from cabinet.cab_grid_column_user_settings
*/