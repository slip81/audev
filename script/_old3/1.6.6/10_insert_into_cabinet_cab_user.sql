﻿/*
	insert into cabinet.cab_user
*/

insert into cabinet.cab_user (user_id, user_name, user_login, is_admin, is_superadmin)
values (0, '[не определен]', 'anonym', false, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ПМС', 'pavlov', true, true);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('КОВ', 'kov', true, true);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ЗПВ', 'zolot', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ШВВ', 'shkoda', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ПАП', 'pap', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('БАИ', 'bareluk', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ГОЛ', 'golubev', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('МИН', 'min', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ОИС', 'osoryev', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ПАВ', 'pavlukova', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ПНЮ', 'panov', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('СА', 'solodova', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ТАА', 'tulkina', true, false);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ПМС-суперадмин', 'esadmin', true, true);

insert into cabinet.cab_user (user_name, user_login, is_admin, is_superadmin)
values ('ПМС-админ', 'eadmin', true, false);


update cabinet.crm_user set cab_user_id = (select x1.user_id from cabinet.cab_user x1 where trim(cabinet.crm_user.user_name) = trim(x1.user_name))
where coalesce(cab_user_id, 0) <= 0;

update cabinet.crm_user set cab_user_id = 0 where coalesce(cab_user_id, 0) <= 0;

-- select * from cabinet.cab_user;
-- select * from cabinet.crm_user order by user_name;