﻿/*
	ALTER TABLE esn.exchange_doc ADD COLUMNs date_beg, date_end
*/

ALTER TABLE esn.exchange_doc ADD COLUMN date_beg date;
ALTER TABLE esn.exchange_doc ADD COLUMN date_end date;