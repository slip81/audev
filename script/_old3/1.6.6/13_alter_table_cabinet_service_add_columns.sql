﻿/*
	ALTER TABLE cabinet.service ADD COLUMNs
*/

ALTER TABLE cabinet.service ADD COLUMN is_service boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.service ADD COLUMN priority integer;