﻿/*
	ALTER TABLE cabinet.crm_client ALTER COLUMN cab_client_id DROP NOT NULL
*/

ALTER TABLE cabinet.crm_client ALTER COLUMN cab_client_id DROP NOT NULL;