﻿/*
	ALTER TABLE cabinet.crm_task ALTER COLUMN point
*/

UPDATE cabinet.crm_task SET point = 0 WHERE point is null;

ALTER TABLE cabinet.crm_task ALTER COLUMN point SET DEFAULT 0;
ALTER TABLE cabinet.crm_task ALTER COLUMN point SET NOT NULL;