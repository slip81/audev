﻿/*
	ALTER TABLE cabinet.cab_user ADD COLUMN full_name email
*/

ALTER TABLE cabinet.cab_user ADD COLUMN full_name character varying;
ALTER TABLE cabinet.cab_user ADD COLUMN email character varying;

UPDATE cabinet.cab_user SET full_name = user_name;

-- select * from cabinet.cab_user