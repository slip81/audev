﻿/*
	insert into cabinet.crm_task_operation
*/

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (0, 'Назад в неразобранные');

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (1, 'В обработанные');

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (2, 'К утверждению');

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (3, 'В работу');

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (4, 'Удалить');

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (5, 'Архивировать');

delete from cabinet.crm_group_operation;

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 0, operation_id, operation_name
from cabinet.crm_task_operation
where operation_id in (1, 4);

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 1, operation_id, operation_name
from cabinet.crm_task_operation
where operation_id in (0, 2);

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 2, operation_id, case when operation_id = 1 then 'Назад в обработанные' else operation_name end
from cabinet.crm_task_operation
where operation_id in (1, 3);

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 3, operation_id, case when operation_id = 2 then 'Назад к утверждению' else operation_name end
from cabinet.crm_task_operation
where operation_id in (2, 5);

-- select * from cabinet.crm_group order by group_id;
-- select * from cabinet.crm_group_operation order by group_id;