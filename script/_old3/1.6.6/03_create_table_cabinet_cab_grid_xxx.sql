﻿/*
	CREATE TABLE cabinet.cab_grid_xxx
*/

-- DROP TABLE cabinet.cab_grid;

CREATE TABLE cabinet.cab_grid
(
  grid_id integer NOT NULL,
  grid_name character varying,
  CONSTRAINT cab_grid_pkey PRIMARY KEY (grid_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_grid OWNER TO pavlov;

-- DROP TABLE cabinet.cab_grid_column;

CREATE TABLE cabinet.cab_grid_column
(
  column_id serial NOT NULL,
  grid_id integer NOT NULL,
  column_name character varying,
  column_name_rus character varying,
  CONSTRAINT cab_grid_column_pkey PRIMARY KEY (column_id),
  CONSTRAINT cab_grid_column_grid_id_fkey FOREIGN KEY (grid_id)
      REFERENCES cabinet.cab_grid (grid_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_grid_column OWNER TO pavlov;

-- DROP TABLE cabinet.cab_grid_column_user_settings;

CREATE TABLE cabinet.cab_grid_column_user_settings
(
  item_id serial NOT NULL,
  column_id integer NOT NULL,
  user_id integer NOT NULL DEFAULT 0,
  column_num integer,
  is_visible boolean NOT NULL DEFAULT true,
  width integer,
  format character varying,
  is_filterable boolean NOT NULL DEFAULT true,
  is_sortable boolean NOT NULL DEFAULT true,  
  CONSTRAINT cab_grid_column_user_settings_pkey PRIMARY KEY (item_id),
  CONSTRAINT cab_grid_column_user_settings_column_id_fkey FOREIGN KEY (column_id)
      REFERENCES cabinet.cab_grid_column (column_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_grid_column_user_settings OWNER TO pavlov;
