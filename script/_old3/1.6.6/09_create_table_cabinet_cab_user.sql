﻿/*
	CREATE TABLE cabinet.cab_user
*/

-- DROP TABLE cabinet.cab_user;

CREATE TABLE cabinet.cab_user
(
  user_id serial NOT NULL,
  user_name character varying,
  user_login character varying,
  is_admin boolean NOT NULL DEFAULT false,
  is_superadmin boolean NOT NULL DEFAULT false,
  CONSTRAINT cab_user_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_user OWNER TO pavlov;
