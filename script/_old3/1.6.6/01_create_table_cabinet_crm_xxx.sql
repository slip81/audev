﻿/*
	CREATE TABLE cabinet.crm_xxx
*/

-- DROP TABLE cabinet.crm_project;

CREATE TABLE cabinet.crm_project
(
  project_id integer NOT NULL,
  project_name character varying,
  CONSTRAINT crm_project_pkey PRIMARY KEY (project_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_project OWNER TO pavlov;

-- DROP TABLE cabinet.crm_module;

CREATE TABLE cabinet.crm_module
(
  module_id integer NOT NULL,
  module_name character varying,
  project_id integer NOT NULL,
  CONSTRAINT crm_module_pkey PRIMARY KEY (module_id),
  CONSTRAINT crm_module_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_module  OWNER TO pavlov;


-- DROP TABLE cabinet.crm_module_part;

CREATE TABLE cabinet.crm_module_part
(
  module_part_id integer NOT NULL,
  module_part_name character varying,
  module_id integer NOT NULL,
  CONSTRAINT crm_module_part_pkey PRIMARY KEY (module_part_id),
  CONSTRAINT crm_module_part_module_id_fkey FOREIGN KEY (module_id)
      REFERENCES cabinet.crm_module (module_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_module_part OWNER TO pavlov;


-- DROP TABLE cabinet.crm_module_version;

CREATE TABLE cabinet.crm_module_version
(
  module_version_id integer NOT NULL,
  module_version_name character varying,
  module_id integer NOT NULL,
  CONSTRAINT crm_module_version_pkey PRIMARY KEY (module_version_id),
  CONSTRAINT crm_module_version_module_id_fkey FOREIGN KEY (module_id)
      REFERENCES cabinet.crm_module (module_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cabinet.crm_module_version
  OWNER TO pavlov;


-- DROP TABLE cabinet.crm_client;

CREATE TABLE cabinet.crm_client
(
  client_id integer NOT NULL,
  client_name character varying,
  cab_client_id integer NOT NULL,
  CONSTRAINT crm_client_pkey PRIMARY KEY (client_id),
  CONSTRAINT crm_client_cab_client_id_fkey FOREIGN KEY (cab_client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_client OWNER TO pavlov;


-- DROP TABLE cabinet.crm_priority;

CREATE TABLE cabinet.crm_priority
(
  priority_id integer NOT NULL,
  priority_name character varying,
  CONSTRAINT crm_priority_pkey PRIMARY KEY (priority_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_priority OWNER TO pavlov;


-- DROP TABLE cabinet.crm_state;

CREATE TABLE cabinet.crm_state
(
  state_id integer NOT NULL,
  state_name character varying,
  CONSTRAINT crm_state_pkey PRIMARY KEY (state_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_state  OWNER TO pavlov;


-- DROP TABLE cabinet.crm_user;

CREATE TABLE cabinet.crm_user
(
  user_id integer NOT NULL,
  user_name character varying,
  cab_user_id integer,
  CONSTRAINT crm_user_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_user OWNER TO pavlov;


-- DROP TABLE cabinet.crm_task;

CREATE TABLE cabinet.crm_task
(
  task_id serial NOT NULL,
  task_name character varying,
  task_text character varying,
  project_id integer NOT NULL,
  module_id integer NOT NULL,
  module_part_id integer NOT NULL,
  module_version_id integer NOT NULL,
  client_id integer NOT NULL,
  priority_id integer NOT NULL,
  state_id integer NOT NULL,
  owner_user_id integer NOT NULL,
  exec_user_id integer NOT NULL,
  task_num character varying,
  crt_date timestamp without time zone,
  required_date timestamp without time zone,
  repair_date timestamp without time zone,
  repair_version_id integer,
  CONSTRAINT crm_task_pkey PRIMARY KEY (task_id),
  CONSTRAINT crm_task_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.crm_client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_module_id_fkey FOREIGN KEY (module_id)
      REFERENCES cabinet.crm_module (module_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_module_part_id_fkey FOREIGN KEY (module_part_id)
      REFERENCES cabinet.crm_module_part (module_part_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_module_version_id_fkey FOREIGN KEY (module_version_id)
      REFERENCES cabinet.crm_module_version (module_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_owner_user_id_fkey FOREIGN KEY (owner_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_priority_id_fkey FOREIGN KEY (priority_id)
      REFERENCES cabinet.crm_priority (priority_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_repair_version_id_fkey FOREIGN KEY (repair_version_id)
      REFERENCES cabinet.crm_module_version (module_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_state_id_fkey FOREIGN KEY (state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task OWNER TO pavlov;


-- DROP TABLE cabinet.crm_subtask;

CREATE TABLE cabinet.crm_subtask
(
  subtask_id serial NOT NULL,
  task_id integer NOT NULL,
  subtask_text character varying,
  priority_id integer NOT NULL,
  state_id integer NOT NULL,
  owner_user_id integer NOT NULL,
  exec_user_id integer NOT NULL,
  crt_date timestamp without time zone,
  repair_date timestamp without time zone,
  CONSTRAINT crm_subtask_pkey PRIMARY KEY (subtask_id),
  CONSTRAINT crm_subtask_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_subtask_owner_user_id_fkey FOREIGN KEY (owner_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_subtask_priority_id_fkey FOREIGN KEY (priority_id)
      REFERENCES cabinet.crm_priority (priority_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_subtask_state_id_fkey FOREIGN KEY (state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_subtask_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_subtask  OWNER TO pavlov;


-- DROP TABLE cabinet.crm_task_note;

CREATE TABLE cabinet.crm_task_note
(
  note_id serial NOT NULL,
  task_id integer NOT NULL,
  subtask_id integer,
  crt_date timestamp without time zone,
  owner_user_id integer NOT NULL,
  note_text character varying,
  CONSTRAINT crm_task_note_pkey PRIMARY KEY (note_id),
  CONSTRAINT crm_task_note_owner_user_id_fkey FOREIGN KEY (owner_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_note_subtask_id_fkey FOREIGN KEY (subtask_id)
      REFERENCES cabinet.crm_subtask (subtask_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_task_note_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_note  OWNER TO pavlov;


-- DROP TABLE cabinet.crm_task_note_file;

CREATE TABLE cabinet.crm_task_note_file
(
  note_file_id serial NOT NULL,
  note_id integer NOT NULL,
  file_name character varying,
  file_size integer,
  file_ext character varying,
  file_body bytea,
  CONSTRAINT crm_task_note_file_pkey PRIMARY KEY (note_file_id),
  CONSTRAINT crm_task_note_file_note_id_fkey FOREIGN KEY (note_id)
      REFERENCES cabinet.crm_task_note (note_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_note_file  OWNER TO pavlov;
