﻿/*
	CREATE TABLE cabinet.crm_user_task_follow
*/

-- DROP TABLE cabinet.crm_user_task_follow;

CREATE TABLE cabinet.crm_user_task_follow
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  task_id integer NOT NULL,
  CONSTRAINT crm_user_task_follow_pkey PRIMARY KEY (item_id),
  CONSTRAINT crm_user_task_follow_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_user_task_follow_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_user_task_follow OWNER TO pavlov;
