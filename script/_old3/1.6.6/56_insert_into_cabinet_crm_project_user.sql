﻿/*
	insert into cabinet.crm_project_user (project_id, user_id)
*/

insert into cabinet.crm_project_user (project_id, user_id)
select 1, user_id
from cabinet.crm_user
where user_id in (2, 5, 6, 7, 17);

insert into cabinet.crm_project_user (project_id, user_id)
select 2, user_id
from cabinet.crm_user
where user_id in (2, 5, 6, 7, 17);

insert into cabinet.crm_project_user (project_id, user_id)
select 3, user_id
from cabinet.crm_user
where user_id in (2, 5, 6, 7, 17);


-- select * from cabinet.crm_project;
-- select * from cabinet.crm_user order by user_id;
-- select * from cabinet.crm_project_user order by project_id, user_id;