﻿/*
	insert into cabinet.cab_user
*/

insert into cabinet.cab_user (user_id, user_name, user_login, is_admin, is_superadmin, full_name)
values (16, 'КАВ', 'kobelev', true, false, 'КАВ');

insert into cabinet.cab_user (user_id, user_name, user_login, is_admin, is_superadmin, full_name)
values (17, 'СОН', 'son', true, false, 'СОН');

insert into cabinet.crm_user (user_id, user_name, cab_user_id)
select user_id + 1, user_name, user_id
from cabinet.cab_user
where user_id in (16, 17);

-- select * from cabinet.cab_user order by user_id;
-- select * from cabinet.crm_user order by user_id;
