﻿/*
	ALTER TABLE cabinet.cab_grid_user_settings ADD COLUMN filter_options
*/

ALTER TABLE cabinet.cab_grid_user_settings RENAME grid_options TO sort_options;
ALTER TABLE cabinet.cab_grid_user_settings ADD COLUMN filter_options text;