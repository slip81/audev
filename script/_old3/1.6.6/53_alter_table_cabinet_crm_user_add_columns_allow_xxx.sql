﻿/*
	ALTER TABLE cabinet.crm_user ADD COLUMNs allow_xxx
*/

DROP VIEW cabinet.vw_cab_user2;

ALTER TABLE cabinet.crm_user DROP COLUMN allow_bucket;
ALTER TABLE cabinet.crm_user DROP COLUMN allow_prepared;
ALTER TABLE cabinet.crm_user DROP COLUMN allow_fixed;
ALTER TABLE cabinet.crm_user DROP COLUMN allow_approved;

ALTER TABLE cabinet.crm_user ADD COLUMN is_boss boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_bucket_read boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_bucket_write boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_prepared_read boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_prepared_write boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_fixed_read boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_fixed_write boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_approved_read boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.crm_user ADD COLUMN allow_approved_write boolean NOT NULL DEFAULT false;

CREATE OR REPLACE VIEW cabinet.vw_cab_user2 AS 
 SELECT t1.user_id,
    t1.user_name,
    t1.user_login,
    t1.is_admin,
    t1.is_superadmin,
    t1.full_name,
    t1.email,
    t2.user_id AS crm_user_id,
    t2.user_name AS crm_user_name,
    t2.is_boss,
    t2.allow_bucket_read,
    t2.allow_prepared_read,
    t2.allow_fixed_read,
    t2.allow_approved_read,
    t2.allow_bucket_write,
    t2.allow_prepared_write,
    t2.allow_fixed_write,
    t2.allow_approved_write
   FROM cabinet.cab_user t1
     LEFT JOIN cabinet.crm_user t2 ON t1.user_id = t2.cab_user_id;

ALTER TABLE cabinet.vw_cab_user2 OWNER TO pavlov;


-- select * from cabinet.crm_user;