﻿/*
	insert into crm_xxx
*/

delete from cabinet.crm_module_version;
delete from cabinet.crm_module_part;
delete from cabinet.crm_module;
delete from cabinet.crm_project;
delete from cabinet.crm_client;
delete from cabinet.crm_user;
delete from cabinet.crm_state;
delete from cabinet.crm_priority;

-- crm_client
insert into cabinet.crm_client (client_id, client_name, cab_client_id)
values (0, '[не указан]', 1000);

-- crm_project
insert into cabinet.crm_project (project_id, project_name)
values (0, '[не указан]');

-- crm_module
insert into cabinet.crm_module (module_id, module_name, project_id)
values (0, '[не указан]', 0);

-- crm_module_part
insert into cabinet.crm_module_part (module_part_id, module_part_name, module_id)
values (0, '[не указан]', 0);

-- crm_module_version
insert into cabinet.crm_module_version (module_version_id, module_version_name, module_id)
values (0, '[не указан]', 0);

-- crm_priority
insert into cabinet.crm_priority (priority_id, priority_name)
values (0, '[не указан]');

-- crm_state
insert into cabinet.crm_state (state_id, state_name)
values (0, '[не указан]');

-- crm_user
insert into cabinet.crm_user (user_id, user_name)
values (0, '[не указан]');
