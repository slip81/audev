﻿/*
	insert into cabinet.crm_group_operation
*/

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
values (0, 2, 'К утверждению');

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
values (0, 3, 'В работу');

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
values (1, 3, 'В работу');

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
values (2, 0, 'Назад в неразобранные');

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
values (3, 0, 'Назад в неразобранные');

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
values (3, 1, 'Назад в обработанные');

-- select * from cabinet.crm_group_operation order by group_id, operation_id;