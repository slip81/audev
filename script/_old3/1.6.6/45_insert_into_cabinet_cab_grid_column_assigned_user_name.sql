﻿/*
	insert into cabinet.cab_grid_column
*/

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'assigned_user_name', 'Отвественный');

insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
select 18, user_id, 17, true, 200, true, true
from cabinet.cab_grid_column_user_settings
where column_id = 1;

-- select * from cabinet.cab_grid_column order by column_id;
-- select * from cabinet.cab_grid_column_user_settings order by column_num;