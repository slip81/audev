﻿/*
	insert into cabinet.cab_grid_column
*/

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'point', 'Важность');

insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
values (17, 15, 16, true, 200, true, true);

-- select * from cabinet.cab_grid_column;
-- select * from cabinet.cab_grid_column_user_settings;