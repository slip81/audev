﻿/*
	insert into cabinet.cab_grid
*/

insert into cabinet.cab_grid (grid_id, grid_name)
values (1, 'taskGrid');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'task_id', 'Код задачи');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'crt_date', 'Дата создания');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'task_num', '№ задачи');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'task_name', 'Кратко');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'state_id', 'Статус');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'client_id', 'Клиент');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'exec_user_id', 'Исполнитель');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'module_id', 'Модуль');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'module_part_id', 'Раздел модуля');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'module_version_id', 'Версия модуля');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'owner_user_id', 'Автор');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'priority_id', 'Приоритет');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'project_id', 'Раздел');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'required_date', 'Требуемая дата');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'repair_date', 'Дата исправления');

insert into cabinet.cab_grid_column (grid_id, column_name, column_name_rus)
values (1, 'repair_version_id', 'Версия исправления');

delete from cabinet.cab_grid_column_user_settings;
insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width)
select column_id, 1, row_number() over(order by column_id), true, 200
from cabinet.cab_grid_column;

update cabinet.cab_grid_column_user_settings set format = 'dd.MM.yyyy HH:mm:ss' where column_id in (2, 15);
update cabinet.cab_grid_column_user_settings set format = 'dd.MM.yyyy' where column_id = 14;

/*
select * from cabinet.cab_grid
select * from cabinet.cab_grid_column
select * from cabinet.cab_grid_column_user_settings
*/