﻿/*
	ALTER TABLE cabinet.crm_user ADD CONSTRAINT crm_user_cab_user_id_fkey FOREIGN KEY (cab_user_id)
*/

ALTER TABLE cabinet.crm_user
  ADD CONSTRAINT crm_user_cab_user_id_fkey FOREIGN KEY (cab_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
