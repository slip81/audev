﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_info
*/

-- DROP VIEW cabinet.vw_client_info;

CREATE OR REPLACE VIEW cabinet.vw_client_info AS 
 SELECT t1.id AS client_id,
    t1.name AS client_name,
        CASE
            WHEN btrim(COALESCE(t12.name, ''::character varying)::text) <> ''::text THEN btrim(COALESCE(t12.name, ''::character varying)::text) || ', '::text
            ELSE ''::text
        END ||
        CASE
            WHEN btrim(COALESCE(t11.name, ''::character varying)::text) <> ''::text THEN btrim(COALESCE(t11.name, ''::character varying)::text)
            ELSE ''::text
        END AS client_address,
    t1.cell AS client_phone,
    t1.contact_person AS client_contact,
    count(DISTINCT t13.id) AS sales_count,
    count(DISTINCT t14.service_id) AS installed_service_cnt,
    count(DISTINCT t15.service_id) AS ordered_service_cnt
   FROM cabinet.client t1
     LEFT JOIN cabinet.city t11 ON t1.city_id = t11.id
     LEFT JOIN cabinet.region t12 ON t11.region_id = t12.id
     LEFT JOIN cabinet.sales t13 ON t1.id = t13.client_id
     LEFT JOIN cabinet.client_service t14 ON t1.id = t14.client_id AND t14.state = 0
     LEFT JOIN cabinet.client_service t15 ON t1.id = t15.client_id AND t15.state = 1
  GROUP BY t1.id, t1.name, btrim(COALESCE(t12.name, ''::character varying)::text), btrim(COALESCE(t11.name, ''::character varying)::text), t1.cell, t1.contact_person;

ALTER TABLE cabinet.vw_client_info OWNER TO pavlov;
