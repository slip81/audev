﻿/*
	ALTER TABLE cabinet.service ADD COLUMN group_id integer
*/

ALTER TABLE cabinet.service ADD COLUMN group_id integer;

ALTER TABLE cabinet.service
  ADD CONSTRAINT service_service_group_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.service_group (group_id) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;
