﻿/*
	CREATE TABLE cabinet.delivery_detail
*/

-- DROP TABLE cabinet.delivery_detail;

CREATE TABLE cabinet.delivery_detail
(
  detail_id serial NOT NULL,
  delivery_id integer NOT NULL,
  client_id integer,
  state integer,
  state_date timestamp without time zone,
  address character varying,
  subject character varying,
  body character varying,
  mess character varying,
  CONSTRAINT delivery_detail_pkey PRIMARY KEY (detail_id),
  CONSTRAINT delivery_detail_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT delivery_detail_delivery_id_fkey FOREIGN KEY (delivery_id)
      REFERENCES cabinet.delivery (delivery_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.delivery_detail OWNER TO pavlov;
