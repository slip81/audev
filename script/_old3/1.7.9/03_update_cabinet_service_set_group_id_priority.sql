﻿/*
	update cabinet.service set group_id = 1
*/

update cabinet.service
set group_id = 1
where id in (1, 11, 15, 18, 16, 19, 49, 17, 22, 27, 41, 44, 35, 53);

update cabinet.service
set group_id = 2
where id in (45, 48, 51, 50, 47, 46);

update cabinet.service
set group_id = 3
where id in (56, 52, 34, 43, 57, 58, 59, 60);

update cabinet.service
set priority = 1
where id = 11;

update cabinet.service
set priority = 2
where id = 1;

update cabinet.service
set priority = 3
where id = 15;

update cabinet.service
set priority = 4
where id = 18;

update cabinet.service
set priority = 5
where id = 16;

update cabinet.service
set priority = 6
where id = 49;

update cabinet.service
set priority = 7
where id = 19;

update cabinet.service
set priority = 8
where id = 17;

update cabinet.service
set priority = 9
where id = 22;

update cabinet.service
set priority = 10
where id = 27;

update cabinet.service
set priority = 11
where id = 41;

update cabinet.service
set priority = 12
where id = 44;

update cabinet.service
set priority = 13
where id = 35;

update cabinet.service
set priority = 14
where id = 53;

------------

update cabinet.service
set priority = 15
where id = 45;

update cabinet.service
set priority = 16
where id = 48;

update cabinet.service
set priority = 17
where id = 51;

update cabinet.service
set priority = 18
where id = 50;

update cabinet.service
set priority = 19
where id = 47;

update cabinet.service
set priority = 20
where id = 46;

------------

update cabinet.service
set priority = 21
where id = 56;

update cabinet.service
set priority = 22
where id = 52;

update cabinet.service
set priority = 23
where id = 34;

update cabinet.service
set priority = 24
where id = 43;

update cabinet.service
set priority = 25
where id = 57;

update cabinet.service
set priority = 26
where id = 58;

update cabinet.service
set priority = 27
where id = 59;

update cabinet.service
set priority = 28
where id = 60;

------------

-- select * from cabinet.service where is_deleted = 0 and parent_id is not null and group_id = 1 order by priority;
-- select * from cabinet.service where is_deleted = 0 and parent_id is not null and group_id = 2 order by priority;
-- select * from cabinet.service where is_deleted = 0 and parent_id is not null and group_id = 3 order by priority;

-- select * from cabinet.service where is_deleted = 0 and parent_id is not null order by group_id, id;