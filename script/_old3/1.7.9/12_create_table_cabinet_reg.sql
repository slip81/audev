﻿/*
	CREATE TABLE cabinet.reg
*/

-- DROP TABLE cabinet.reg;

CREATE TABLE cabinet.reg
(
  reg_id serial NOT NULL,
  login character varying,
  pwd character varying,
  email character varying,
  org_name character varying,
  org_address character varying,
  fio character varying,
  phone character varying,
  state integer NOT NULL DEFAULT 0,
  crt_date timestamp without time zone,
  ip_address character varying,
  CONSTRAINT reg_pkey PRIMARY KEY (reg_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.reg OWNER TO pavlov;
