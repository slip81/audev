﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service
*/

-- DROP VIEW cabinet.vw_client_service;

CREATE OR REPLACE VIEW cabinet.vw_client_service AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS id,
    t1.id AS client_id,
    t1.name AS client_name,
    t2.id AS sales_id,
    t2.name AS sales_name,
    t2.adress AS sales_address,
    t3.id AS workplace_id,
    t3.registration_key AS workplace_registration_key,
    t3.description AS workplace_description,
    t8.id AS service_id,
    t8.name AS service_name,
    t8.description AS service_description,
    t9.id AS version_id,
    t9.name AS version_name,
    t8.is_service,
    t8.priority AS service_priority,
    t4.activation_key,
    t8.have_spec,
    t8.max_workplace_cnt
   FROM cabinet.client t1
     JOIN cabinet.sales t2 ON t1.id = t2.client_id
     JOIN cabinet.workplace t3 ON t2.id = t3.sales_id
     JOIN cabinet.service_registration t4 ON t3.id = t4.workplace_id
     JOIN cabinet.license t5 ON t4.license_id = t5.id
     JOIN cabinet.order_item t6 ON t5.order_item_id = t6.id
     JOIN cabinet."order" t7 ON t6.order_id = t7.id
     JOIN cabinet.service t8 ON t6.service_id = t8.id
     LEFT JOIN cabinet.version t9 ON t6.version_id = t9.id;

ALTER TABLE cabinet.vw_client_service OWNER TO pavlov;
