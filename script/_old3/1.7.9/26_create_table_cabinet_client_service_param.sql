﻿/*
	CREATE TABLE cabinet.client_service_param
*/

-- DROP TABLE cabinet.client_service_param;

CREATE TABLE cabinet.client_service_param
(
  param_id serial NOT NULL,
  client_id integer NOT NULL,
  service_id integer NOT NULL,
  max_workplace_cnt integer,
  CONSTRAINT client_service_param_pkey PRIMARY KEY (param_id),
  CONSTRAINT client_service_param_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT client_service_param_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.client_service_param OWNER TO pavlov;
