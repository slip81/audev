﻿/*
	ALTER TABLE cabinet.delivery ADD COLUMN need_stop
*/

ALTER TABLE cabinet.delivery ADD COLUMN need_stop boolean NOT NULL DEFAULT false;
