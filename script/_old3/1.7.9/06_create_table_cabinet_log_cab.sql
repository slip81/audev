﻿/*
	CREATE TABLE cabinet.log_cab
*/

-- DROP TABLE cabinet.log_cab;

CREATE TABLE cabinet.log_cab
(
  log_id bigserial NOT NULL,
  date_beg timestamp without time zone NOT NULL,
  mess character varying,
  user_name character varying,
  obj_type integer,
  obj_type_name character varying,
  obj1_id bigint,
  obj1_value character varying,  
  obj2_id bigint,
  obj2_value character varying,
  CONSTRAINT log_cab_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.log_cab OWNER TO pavlov;


INSERT INTO cabinet.log_cab (date_beg, mess, user_name, obj_type, obj_type_name, obj1_id, obj1_value, obj2_id, obj2_value)
SELECT date_beg, mess, user_name, obj1_type, '', obj1_id, obj1_value, obj2_id, obj2_value
FROM discount.log_cab;

DROP TABLE discount.log_cab;