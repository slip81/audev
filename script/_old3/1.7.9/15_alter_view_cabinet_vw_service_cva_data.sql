﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_service_cva_data
*/

-- DROP VIEW cabinet.vw_service_cva_data;

CREATE OR REPLACE VIEW cabinet.vw_service_cva_data AS 
 SELECT t2.client_id,
    t2.client_name,
    t2.sales_id,
    t2.sales_address,
    t2.description AS workplace_description,
    t1.item_id,
    t1.workplace_id,
    t1.service_id,
    t1.batch_num,
    t1.batch_date,
    t1.batch_state,
    t1.sending_date,
    t1.sent_date,
    t1.mess,
    t1.batch_size
   FROM cabinet.service_cva_data t1
     JOIN cabinet.vw_workplace t2 ON t1.workplace_id = t2.workplace_id;

ALTER TABLE cabinet.vw_service_cva_data OWNER TO pavlov;
