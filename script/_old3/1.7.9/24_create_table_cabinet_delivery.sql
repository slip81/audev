﻿/*
	CREATE TABLE cabinet.delivery
*/

-- DROP TABLE cabinet.delivery;

CREATE TABLE cabinet.delivery
(
  delivery_id serial NOT NULL,
  template_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  state integer,
  state_date timestamp without time zone,  
  detail_cnt integer,
  success_cnt integer,
  error_cnt integer,
  mess character varying,  
  CONSTRAINT delivery_pkey PRIMARY KEY (delivery_id),
  CONSTRAINT delivery_template_id_fkey FOREIGN KEY (template_id)
      REFERENCES cabinet.delivery_template (template_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.delivery OWNER TO pavlov;
