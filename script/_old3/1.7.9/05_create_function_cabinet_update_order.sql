﻿/*
	CREATE OR REPLACE FUNCTION cabinet.update_order
*/

-- DROP FUNCTION cabinet.update_order();

CREATE OR REPLACE FUNCTION cabinet.update_order(
    OUT result int)
  RETURNS int AS
$BODY$
DECLARE
    _curr_order_id integer;
    _curr_service_name character varying;
    _prev_order_id integer;
    _full_service_name character varying;
    _full_service_name1 character varying;    
    _first boolean;
    _cnt integer;
BEGIN
	
	result := 0;
	_curr_order_id := 0;
	_prev_order_id := 0;
	_curr_service_name := '';
	_full_service_name := '';
	_first := true;
	_cnt := 0;
	
	FOR _curr_order_id, _curr_service_name IN 
		SELECT t1.id, t3.name
		FROM cabinet."order" t1
		INNER JOIN cabinet.order_item t2 ON t1.id = t2.order_id
		LEFT JOIN cabinet.service t3 ON t2.service_id = t3.id
		/*WHERE t1.id = 5172*/
		ORDER BY t1.id DESC, t3.name /*LIMIT 500*/
	LOOP
		if (_first = true) then
			_prev_order_id := _curr_order_id;
			_full_service_name:= _curr_service_name;
			_full_service_name1:= _curr_service_name;
		end if;		
				
		if (_prev_order_id != _curr_order_id) then			
			update cabinet."order" set order_name = _full_service_name where id = _prev_order_id;			
			_full_service_name:= _curr_service_name;
			_full_service_name1:= '';
			_cnt := 1;
		else
			if (_first != true) then
				if (_cnt >= 3) then
					_full_service_name := _full_service_name1 || ' и еще ' || ((_cnt - 2)::text);
				else
					_full_service_name := _full_service_name || ', ' || _curr_service_name;					
					_full_service_name1 := _full_service_name;
				end if;							
			end if;
			_cnt := _cnt + 1;
		end if;
		
		_first := false;
		_prev_order_id := _curr_order_id;
		
	END LOOP;

	if (trim(coalesce(_full_service_name, '')) != '') then
		update cabinet."order" set order_name = _full_service_name where id = _curr_order_id;
	end if;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.update_order() OWNER TO pavlov;


select cabinet.update_order();