﻿/*
	ALTER TABLE cabinet.delivery ADD COLUMNs
*/

ALTER TABLE cabinet.delivery ADD COLUMN descr character varying;
ALTER TABLE cabinet.delivery ADD COLUMN processed_cnt integer;
