﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service_param
*/

-- DROP VIEW cabinet.vw_client_service_param;

CREATE OR REPLACE VIEW cabinet.vw_client_service_param AS 
 SELECT row_number() over (order by t1.id) as param_id, t1.id as client_id, t11.id as service_id, 
   (case when t12.service_id is null then t11.max_workplace_cnt else t12.max_workplace_cnt end) as max_workplace_cnt,
   t11.name as service_name
   FROM cabinet.client t1
   LEFT JOIN cabinet.service t11 ON 1 = 1
   LEFT JOIN cabinet.client_service_param t12 ON t1.id = t12.client_id AND t11.id = t12.service_id
   WHERE t11.max_workplace_cnt IS NOT NULL
   ;

ALTER TABLE cabinet.vw_client_service_param OWNER TO pavlov;
