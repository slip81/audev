﻿/*
	CREATE TABLE cabinet.delivery_template_group, cabinet.delivery_template
*/

-- DROP TABLE cabinet.delivery_template_group;

CREATE TABLE cabinet.delivery_template_group
(
  group_id serial NOT NULL,
  group_name character varying,
  CONSTRAINT delivery_template_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.delivery_template_group OWNER TO pavlov;

-- DROP TABLE cabinet.delivery_template;

CREATE TABLE cabinet.delivery_template
(
  template_id serial NOT NULL,
  group_id integer,
  template_name character varying,
  subject character varying,
  body character varying,
  CONSTRAINT delivery_template_pkey PRIMARY KEY (template_id),
  CONSTRAINT delivery_template_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.delivery_template_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.delivery_template OWNER TO pavlov;
