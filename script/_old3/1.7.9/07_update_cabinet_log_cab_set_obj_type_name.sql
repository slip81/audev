﻿/*
	update cabinet.log_cab set obj_type_name
*/

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardViewModel'
where obj_type = 11;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardAdditionalNumViewModel'
where obj_type = 12;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardTypeViewModel'
where obj_type = 21;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.BusinessViewModel'
where obj_type = 41;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.BusinessOrgViewModel'
where obj_type = 51;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.BusinessOrgUserViewModel'
where obj_type = 61;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.ProgrammViewModel'
where obj_type = 71;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardClientViewModel'
where obj_type = 91;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardTransactionViewModel'
where obj_type = 101;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardTransactionDetailViewModel'
where obj_type = 111;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardStatusViewModel'
where obj_type = 121;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardTypeGroupViewModel'
where obj_type = 131;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.ReportViewModel'
where obj_type = 141;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.ProgrammParamViewModel'
where obj_type = 151;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardBatchOperationsViewModel'
where obj_type = 161;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardBatchOperationsErrorViewModel'
where obj_type = 171;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardImportViewModel'
where obj_type = 181;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardTypeLimitViewModel'
where obj_type = 191;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.BusinessSummaryViewModel'
where obj_type = 201;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.HelpDesk.StoryViewModel'
where obj_type = 211;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.HelpDesk.StoryLcViewModel'
where obj_type = 221;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.HelpDesk.StoryStateViewModel'
where obj_type = 231;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Adm.CabActionGroupViewModel'
where obj_type = 241;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Adm.CabActionViewModel'
where obj_type = 242;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Adm.CabRoleViewModel'
where obj_type = 243;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Adm.CabRoleActionViewModel'
where obj_type = 244;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Adm.CabUserRoleViewModel'
where obj_type = 245;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Adm.CabUserRoleActionViewModel'
where obj_type = 246;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Adm.CabUserViewModel'
where obj_type = 253;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Discount.CardTypeBusinessOrgViewModel'
where obj_type = 254;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.User.UserProfileViewModel'
where obj_type = 255;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ClientViewModel'
where obj_type = 301;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.SalesViewModel'
where obj_type = 311;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.LicenseViewModel'
where obj_type = 321;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ServiceViewModel'
where obj_type = 322;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.WorkplaceViewModel'
where obj_type = 323;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.OrderViewModel'
where obj_type = 324;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.OrderItemViewModel'
where obj_type = 325;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.VersionViewModel'
where obj_type = 326;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ClientServiceViewModel'
where obj_type = 327;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.WorkplaceReregViewModel'
where obj_type = 328;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ClientServiceUnregViewModel'
where obj_type = 329;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ServiceSenderViewModel'
where obj_type = 330;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ServiceSenderLogViewModel'
where obj_type = 331;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ServiceCvaViewModel'
where obj_type = 332;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ServiceCvaDataViewModel'
where obj_type = 333;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ClientUserViewModel'
where obj_type = 334;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.WidgetViewModel'
where obj_type = 335;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ClientUserWidgetViewModel'
where obj_type = 336;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ServiceDefectViewModel'
where obj_type = 337;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ClientInfoViewModel'
where obj_type = 338;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ClientServiceSummaryViewModel'
where obj_type = 339;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Cabinet.ServiceGroupViewModel'
where obj_type = 340;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Defect.DefectViewModel'
where obj_type = 501;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Defect.DefectRequestViewModel'
where obj_type = 502;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Defect.DefectLogViewModel'
where obj_type = 503;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeLogViewModel'
where obj_type = 601;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeUserViewModel'
where obj_type = 602;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeRegPartnerViewModel'
where obj_type = 604;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeClientViewModel'
where obj_type = 605;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangePartnerViewModel'
where obj_type = 606;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeDataViewModel'
where obj_type = 608;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeRegDocViewModel'
where obj_type = 609;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeRegDocSalesViewModel'
where obj_type = 610;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeDocDataViewModel'
where obj_type = 612;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeMonitorViewModel'
where obj_type = 613;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeRegPartnerItemViewModel'
where obj_type = 614;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeMonitorLogViewModel'
where obj_type = 615;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeTaskViewModel'
where obj_type = 616;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeTaskTypeViewModel'
where obj_type = 617;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeFileLogViewModel'
where obj_type = 618;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeFileNodeViewModel'
where obj_type = 619;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangeFileNodeItemViewModel'
where obj_type = 620;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Exchange.ExchangePartnerItemViewModel'
where obj_type = 621;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedSourceViewModel'
where obj_type = 701;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedSourceRegionViewModel'
where obj_type = 702;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedTemplateViewModel'
where obj_type = 703;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedTemplateColumnViewModel'
where obj_type = 704;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedReestrItemViewModel'
where obj_type = 706;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedReestrItemOutViewModel'
where obj_type = 707;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedReestrRegionItemViewModel'
where obj_type = 709;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedReestrRegionItemOutViewModel'
where obj_type = 710;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedReestrRegionItemErrorViewModel'
where obj_type = 711;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Ved.VedRequestViewModel'
where obj_type = 712;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmTaskViewModel'
where obj_type = 801;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmSubtaskViewModel'
where obj_type = 802;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmTaskNoteViewModel'
where obj_type = 803;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmTaskNoteFileViewModel'
where obj_type = 804;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmTaskUserFilterViewModel'
where obj_type = 805;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmLogViewModel'
where obj_type = 806;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmUserTaskFollowViewModel'
where obj_type = 807;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmTaskGroupOperationViewModel'
where obj_type = 808;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmGroupViewModel'
where obj_type = 809;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmTaskRelTypeViewModel'
where obj_type = 810;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmTaskRelViewModel'
where obj_type = 811;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Crm.CrmSchedulerViewModel'
where obj_type = 812;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Sys.CabGridViewModel'
where obj_type = 901;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Sys.CabGridColumnViewModel'
where obj_type = 902;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Sys.CabGridColumnUserSettingsViewModel'
where obj_type = 903;

update cabinet.log_cab
set obj_type_name = 'CabinetMvc.ViewModel.Sys.CabGridUserSettingsViewModel'
where obj_type = 904;

-- select * from cabinet.log_cab order by log_id desc limit 1000