﻿/*
	ALTER TABLE cabinet."order" ADD COLUMN order_name character varying
*/

ALTER TABLE cabinet."order" ADD COLUMN order_name character varying;