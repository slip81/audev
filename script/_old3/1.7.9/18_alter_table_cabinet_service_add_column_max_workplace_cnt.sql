﻿/*
	ALTER TABLE cabinet.service ADD COLUMN max_workplace_cnt
*/

ALTER TABLE cabinet.service ADD COLUMN max_workplace_cnt integer;

UPDATE cabinet.service SET max_workplace_cnt = 1 WHERE id in (59, 60);


-- select * from cabinet.service order by id desc