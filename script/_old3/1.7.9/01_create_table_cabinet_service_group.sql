﻿/*
	CREATE TABLE cabinet.service_group
*/

-- DROP TABLE cabinet.service_group;

CREATE TABLE cabinet.service_group
(
  group_id integer NOT NULL,
  group_name character varying,
  CONSTRAINT service_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cabinet.service_group
  OWNER TO pavlov;


INSERT INTO cabinet.service_group (group_id, group_name)
VALUES (1, 'Модули');

INSERT INTO cabinet.service_group (group_id, group_name)
VALUES (2, 'Ключи');

INSERT INTO cabinet.service_group (group_id, group_name)
VALUES (3, 'Сервисы');

-- select * from cabinet.service_group  order by group_id;