﻿/*
	ALTER TABLE cabinet.auth_user_perm, auth_role_perm DROP COLUMN section_id
*/

ALTER TABLE cabinet.auth_user_perm DROP CONSTRAINT auth_user_perm_section_id_fkey;

ALTER TABLE cabinet.auth_user_perm DROP COLUMN section_id;

ALTER TABLE cabinet.auth_role_perm DROP CONSTRAINT auth_role_perm_section_id_fkey;

ALTER TABLE cabinet.auth_role_perm DROP COLUMN section_id;

DELETE FROM cabinet.auth_user_perm;
DELETE FROM cabinet.auth_role_perm;

ALTER TABLE cabinet.auth_user_perm ALTER COLUMN part_id SET NOT NULL;
ALTER TABLE cabinet.auth_role_perm ALTER COLUMN part_id SET NOT NULL;