﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_auth_role
*/

-- DROP VIEW cabinet.vw_auth_role;

CREATE OR REPLACE VIEW cabinet.vw_auth_role AS 
select row_number() over (order by t1.role_name) as id, t1.role_name, 
(case when t2.group_id is not null then true else false end) as is_group1,
(case when t3.group_id is not null then true else false end) as is_group2
from
(
select distinct x1.role_name 
from cabinet.auth_role x1
) t1
left join cabinet.auth_role t2 on t1.role_name = t2.role_name and t2.group_id = 1
left join cabinet.auth_role t3 on t1.role_name = t3.role_name and t3.group_id = 2
;

ALTER TABLE cabinet.vw_auth_role OWNER TO pavlov;
