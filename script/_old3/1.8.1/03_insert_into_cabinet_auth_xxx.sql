﻿/*
	insert into cabinet.auth_xxx
*/

/* ------------------ */

insert into cabinet.auth_group (group_id, group_name)
values (1, 'Сотрудники');

insert into cabinet.auth_group (group_id, group_name)
values (2, 'Клиенты');

/* ------------------ */

insert into cabinet.auth_section (section_id, section_name, group_id)
values (1, 'Дисконтные карты', null);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (2, 'Обмен с партнерами', null);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (3, 'Брак', null);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (4, 'ЦВА', null);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (5, 'Клиенты', 1);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (6, 'Заявки на активацию', 1);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (7, 'Справочники', 1);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (8, 'Рассылки', 1);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (9, 'HelpDesk', 1);

insert into cabinet.auth_section (section_id, section_name, group_id)
values (10, 'Задачи', 1);

/* ------------------ */

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 1, 'Карты', section_id, group_id
from cabinet.auth_section
where section_id = 1;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 2, 'Пакетные операции', section_id, group_id
from cabinet.auth_section
where section_id = 1;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 3, 'Типы карт', section_id, group_id
from cabinet.auth_section
where section_id = 1;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 4, 'Алгоритмы', section_id, group_id
from cabinet.auth_section
where section_id = 1;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 5, 'Владельцы карт', section_id, group_id
from cabinet.auth_section
where section_id = 1;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 6, 'Продажи по картам', section_id, group_id
from cabinet.auth_section
where section_id = 1;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 7, 'Отчеты', section_id, group_id
from cabinet.auth_section
where section_id = 1;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 8, 'Организации', section_id, 1
from cabinet.auth_section
where section_id = 1;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 9, 'Импорт карт', section_id, 1
from cabinet.auth_section
where section_id = 1;

/* ------------------ */

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 10, 'Журнал обмена', section_id, group_id
from cabinet.auth_section
where section_id = 2;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 11, 'Монитор обмена', section_id, group_id
from cabinet.auth_section
where section_id = 2;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 12, 'Участники обмена', section_id, group_id
from cabinet.auth_section
where section_id = 2;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 13, 'Партнеры', section_id, group_id
from cabinet.auth_section
where section_id = 2;

/* ------------------ */

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 14, 'Список брака', section_id, group_id
from cabinet.auth_section
where section_id = 3;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 15, 'Журнал скачивания', section_id, group_id
from cabinet.auth_section
where section_id = 3;

/* ------------------ */

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 16, 'Услуги', section_id, group_id
from cabinet.auth_section
where section_id = 7;

/* ------------------ */

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 17, 'Все задачи', section_id, group_id
from cabinet.auth_section
where section_id = 10;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 18, 'Мои задачи', section_id, group_id
from cabinet.auth_section
where section_id = 10;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 19, 'Календарь задач', section_id, group_id
from cabinet.auth_section
where section_id = 10;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 20, 'Архив задач', section_id, group_id
from cabinet.auth_section
where section_id = 10;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 21, 'Справочники задач', section_id, group_id
from cabinet.auth_section
where section_id = 10;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
select 22, 'История задач', section_id, group_id
from cabinet.auth_section
where section_id = 10;

/* ------------------ */

insert into cabinet.auth_role (role_id, role_name, group_id)
values (1, 'Директор', 1);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (2, 'Руководитель', 1);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (3, 'Менеджер', 1);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (4, 'Сервис', 1);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (5, 'Тестировщик', 1);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (6, 'Разработчик', 1);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (11, 'Директор', 2);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (12, 'Руководитель', 2);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (13, 'Заведующий', 2);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (14, 'Менеджер', 2);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (15, 'Кассир', 2);

insert into cabinet.auth_role (role_id, role_name, group_id)
values (16, 'Разработчик', 2);

/* ------------------ */

insert into cabinet.auth_user_role (user_id, role_id)
select t1.user_id, 6
from cabinet.client_user t1
inner join cabinet.cab_user t2 on t1.user_id = t2.user_id;

insert into cabinet.auth_user_role (user_id, role_id)
select t1.user_id, 14
from cabinet.client_user t1
where not exists (select x1.user_id from cabinet.cab_user x1 where t1.user_id = x1.user_id);

-- todo:
-- update cabinet.auth_user_role set role_id = ...

/* ------------------ */