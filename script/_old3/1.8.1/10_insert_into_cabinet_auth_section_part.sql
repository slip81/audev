﻿/*
	insert into cabinet.auth_section_part
*/

insert into cabinet.auth_section_part (part_id, section_id, part_name, group_id)
select row_number() over (order by t1.section_id) + (select max(part_id) from cabinet.auth_section_part), t1.section_id, t1.section_name, t1.group_id
from cabinet.auth_section t1
where not exists(select x1.section_id from cabinet.auth_section_part x1 where x1.section_id = t1.section_id);
