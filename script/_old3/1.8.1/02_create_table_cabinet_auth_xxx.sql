﻿/*
	CREATE TABLE cabinet.auth_xxx
*/

-- DROP TABLE cabinet.auth_group;

CREATE TABLE cabinet.auth_group
(
  group_id integer NOT NULL,
  group_name character varying,
  CONSTRAINT auth_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);


ALTER TABLE cabinet.auth_group OWNER TO pavlov;

-- DROP TABLE cabinet.auth_section;

CREATE TABLE cabinet.auth_section
(
  section_id integer NOT NULL,
  section_name character varying,
  group_id integer,
  CONSTRAINT auth_section_pkey PRIMARY KEY (section_id),
  CONSTRAINT auth_section_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.auth_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.auth_section OWNER TO pavlov;

-- DROP TABLE cabinet.auth_section_part;

CREATE TABLE cabinet.auth_section_part
(
  part_id integer NOT NULL,
  part_name character varying,
  section_id integer NOT NULL,
  group_id integer,
  CONSTRAINT auth_section_part_pkey PRIMARY KEY (part_id),
  CONSTRAINT auth_section_part_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.auth_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT auth_section_part_section_id_fkey FOREIGN KEY (section_id)
      REFERENCES cabinet.auth_section (section_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.auth_section_part OWNER TO pavlov;

-- DROP TABLE cabinet.auth_role;

CREATE TABLE cabinet.auth_role
(
  role_id integer NOT NULL,
  role_name character varying,
  group_id integer NOT NULL,
  CONSTRAINT auth_role_pkey PRIMARY KEY (role_id),
  CONSTRAINT auth_role_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.auth_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.auth_role OWNER TO pavlov;


-- DROP TABLE cabinet.auth_user_role;

CREATE TABLE cabinet.auth_user_role
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  role_id integer NOT NULL,  
  CONSTRAINT auth_user_role_pkey PRIMARY KEY (item_id),
  CONSTRAINT auth_user_role_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cabinet.auth_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT auth_user_role_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.my_aspnet_users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.auth_user_role OWNER TO pavlov;

-- DROP TABLE cabinet.auth_role_perm;

CREATE TABLE cabinet.auth_role_perm
(
  perm_id serial NOT NULL,
  role_id integer NOT NULL,
  section_id integer,
  part_id integer,
  allow_view boolean NOT NULL DEFAULT false,
  allow_edit boolean NOT NULL DEFAULT false,
  CONSTRAINT auth_role_perm_pkey PRIMARY KEY (perm_id),
  CONSTRAINT auth_role_perm_part_id_fkey FOREIGN KEY (part_id)
      REFERENCES cabinet.auth_section_part (part_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT auth_role_perm_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cabinet.auth_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT auth_role_perm_section_id_fkey FOREIGN KEY (section_id)
      REFERENCES cabinet.auth_section (section_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.auth_role_perm OWNER TO pavlov;

-- DROP TABLE cabinet.auth_user_perm;

CREATE TABLE cabinet.auth_user_perm
(
  perm_id serial NOT NULL,
  user_id integer NOT NULL,
  section_id integer,
  part_id integer,
  allow_view boolean NOT NULL DEFAULT false,
  allow_edit boolean NOT NULL DEFAULT false,
  CONSTRAINT auth_user_perm_pkey PRIMARY KEY (perm_id),
  CONSTRAINT auth_user_perm_part_id_fkey FOREIGN KEY (part_id)
      REFERENCES cabinet.auth_section_part (part_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT auth_user_perm_section_id_fkey FOREIGN KEY (section_id)
      REFERENCES cabinet.auth_section (section_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT auth_user_perm_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.my_aspnet_users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


ALTER TABLE cabinet.auth_user_perm OWNER TO pavlov;
