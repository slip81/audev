﻿/*
	ALTER TABLE cabinet.reg ADD COLUMNs
*/

ALTER TABLE cabinet.reg ADD COLUMN upd_date timestamp without time zone;
ALTER TABLE cabinet.reg ADD COLUMN upd_user character varying;
