﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_auth_role_perm
*/

DROP VIEW cabinet.vw_auth_role_section_perm;

DROP VIEW cabinet.vw_auth_role_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_role_perm AS 
 SELECT row_number() OVER (ORDER BY t1.section_id) AS id,
    t1.section_id,
    t1.section_name,
    t2.part_id,
    t2.part_name,
    t3.role_id,
    t3.role_name,
    COALESCE(t5.allow_view, false) AS allow_view,
    COALESCE(t5.allow_edit, false) AS allow_edit,
    t4.group_id AS role_group_id
   FROM cabinet.auth_section t1
     LEFT JOIN cabinet.auth_section_part t2 ON t1.section_id = t2.section_id
     LEFT JOIN cabinet.auth_role t3 ON 1 = 1
     LEFT JOIN cabinet.auth_role_group t4 ON t3.role_id = t4.role_id AND COALESCE(t2.group_id, COALESCE(t1.group_id, t4.group_id)) = t4.group_id
     LEFT JOIN cabinet.auth_role_perm t5 ON t4.role_id = t5.role_id AND t2.part_id = t5.part_id;

ALTER TABLE cabinet.vw_auth_role_perm OWNER TO pavlov;
