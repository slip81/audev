﻿/*
	insert into cabinet.auth_role_perm	
*/

delete from cabinet.auth_user_perm;
delete from cabinet.auth_role_perm;

insert into cabinet.auth_role_perm (role_id, part_id, allow_view, allow_edit)
select
t1.role_id, t2.part_id, true, true
from cabinet.auth_role t1
inner join cabinet.auth_section_part t2 on 1 = 1;
