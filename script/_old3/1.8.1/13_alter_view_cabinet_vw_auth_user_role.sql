﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_auth_user_role
*/

-- DROP VIEW cabinet.vw_auth_user_role;

CREATE OR REPLACE VIEW cabinet.vw_auth_user_role AS 
 SELECT t1.user_id,
    t1.user_name,
    t1.user_name_full,
    t1.client_id,
    t3.role_id,
    t3.role_name,
    t3.group_id
   FROM cabinet.client_user t1
     LEFT JOIN cabinet.auth_user_role t2 ON t1.user_id = t2.user_id
     LEFT JOIN cabinet.auth_role t3 ON t2.role_id = t3.role_id
     LEFT JOIN cabinet.my_aspnet_membership t4 ON t1.user_id = t4."userId"
     WHERE coalesce(t4."IsApproved", 1) = 1
     ;

ALTER TABLE cabinet.vw_auth_user_role OWNER TO pavlov;
