﻿/*
	ALTER VIEWS cabinet.vw_auth_user_perm, cabinet.vw_auth_role_perm
*/

-- DROP VIEW cabinet.vw_auth_user_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_user_perm AS 
 SELECT row_number() OVER (ORDER BY t1.section_id) AS id,
    t1.section_id,
    t1.section_name,
    t2.part_id,
    t2.part_name,
    t3.user_id,
    t3.user_name,
    t4.role_id,
    t5.role_name,
        CASE
            WHEN t6.perm_id IS NOT NULL AND t7.perm_id IS NULL THEN true
            ELSE false
        END AS role_perm,
        CASE
            WHEN t7.perm_id IS NOT NULL THEN true
            ELSE false
        END AS user_perm,
        CASE
            WHEN t7.perm_id IS NOT NULL THEN t7.allow_view
            ELSE COALESCE(t6.allow_view, false)
        END AS allow_view,
        CASE
            WHEN t7.perm_id IS NOT NULL THEN t7.allow_edit
            ELSE COALESCE(t6.allow_edit, false)
        END AS allow_edit
   FROM cabinet.auth_section t1
     LEFT JOIN cabinet.auth_section_part t2 ON t1.section_id = t2.section_id
     LEFT JOIN cabinet.client_user t3 ON 1 = 1
     LEFT JOIN cabinet.auth_user_role t4 ON t3.user_id = t4.user_id
     LEFT JOIN cabinet.auth_role t5 ON t4.role_id = t5.role_id
     LEFT JOIN cabinet.auth_role_perm t6 ON t5.role_id = t6.role_id AND t2.part_id = t6.part_id
     LEFT JOIN cabinet.auth_user_perm t7 ON t3.user_id = t7.user_id AND t2.part_id = t7.part_id
  WHERE COALESCE(t2.group_id, COALESCE(t1.group_id, t5.group_id)) = t5.group_id;

ALTER TABLE cabinet.vw_auth_user_perm
  OWNER TO pavlov;

/* -------------------------------- */

-- DROP VIEW cabinet.vw_auth_role_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_role_perm AS 
 SELECT row_number() OVER (ORDER BY t1.section_id) AS id,
    t1.section_id,
    t1.section_name,
    t2.part_id,
    t2.part_name,
    t3.role_id,
    t3.role_name,
    COALESCE(t4.allow_view, false) AS allow_view,
    COALESCE(t4.allow_edit, false) AS allow_edit
   FROM cabinet.auth_section t1
     LEFT JOIN cabinet.auth_section_part t2 ON t1.section_id = t2.section_id
     LEFT JOIN cabinet.auth_role t3 ON 1 = 1
     LEFT JOIN cabinet.auth_role_perm t4 ON t3.role_id = t4.role_id AND t2.part_id = t4.part_id
  WHERE COALESCE(t2.group_id, COALESCE(t1.group_id, t3.group_id)) = t3.group_id;

ALTER TABLE cabinet.vw_auth_role_perm OWNER TO pavlov;

/* -------------------------------- */