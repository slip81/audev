﻿/*
	CREATE TABLE cabinet.auth_role_group
*/

-- DROP TABLE cabinet.auth_role_group;

CREATE TABLE cabinet.auth_role_group
(
  item_id serial NOT NULL,
  role_id integer NOT NULL,
  group_id integer NOT NULL,
  CONSTRAINT auth_role_group_pkey PRIMARY KEY (item_id),
  CONSTRAINT auth_role_group_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.auth_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT auth_role_group_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cabinet.auth_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT auth_role_group_role_id_group_id_key UNIQUE (role_id, group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.auth_role_group OWNER TO pavlov;

delete from cabinet.auth_role_group;

insert into cabinet.auth_role_group (role_id, group_id)
select role_id, 1
from cabinet.auth_role
where group_id = 1;

delete from cabinet.auth_role_perm t1 using cabinet.auth_role t2
where t1.role_id = t2.role_id
and t2.group_id = 2;

delete from cabinet.auth_user_role t1 using cabinet.auth_role t2
where t1.role_id = t2.role_id
and t2.group_id = 2;

delete from cabinet.auth_role where group_id = 2;

-- select * from cabinet.auth_role order by group_id, role_id
-- select * from cabinet.auth_role_group