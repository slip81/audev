﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_auth_role
*/

DROP VIEW cabinet.vw_auth_role;

CREATE OR REPLACE VIEW cabinet.vw_auth_role AS 
 SELECT t1.role_id,
    t1.role_name,
        CASE
            WHEN t2.group_id IS NOT NULL THEN true
            ELSE false
        END AS is_group1,
        CASE
            WHEN t3.group_id IS NOT NULL THEN true
            ELSE false
        END AS is_group2
   FROM cabinet.auth_role t1
     LEFT JOIN cabinet.auth_role_group t2 ON t1.role_id = t2.role_id AND t2.group_id = 1
     LEFT JOIN cabinet.auth_role_group t3 ON t1.role_id = t3.role_id AND t3.group_id = 2;

ALTER TABLE cabinet.vw_auth_role OWNER TO pavlov;
