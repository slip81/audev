﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_auth_user_section_perm
*/

-- DROP VIEW cabinet.vw_auth_user_section_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_user_section_perm AS 
 SELECT row_number() OVER (ORDER BY vw_auth_user_perm.section_id) AS id,
    vw_auth_user_perm.section_id,
    vw_auth_user_perm.user_id,
    vw_auth_user_perm.section_name,
    count(DISTINCT vw_auth_user_perm.allow_view)::integer AS allow_view_cnt,
    count(DISTINCT vw_auth_user_perm.allow_edit)::integer AS allow_edit_cnt,
        CASE
            WHEN count(DISTINCT vw_auth_user_perm.allow_view)::integer > 1 THEN 2
            ELSE max(vw_auth_user_perm.allow_view::integer)
        END AS allow_view_section,
        CASE
            WHEN count(DISTINCT vw_auth_user_perm.allow_edit)::integer > 1 THEN 2
            ELSE max(vw_auth_user_perm.allow_edit::integer)
        END AS allow_edit_section
   FROM cabinet.vw_auth_user_perm
  GROUP BY vw_auth_user_perm.section_id, vw_auth_user_perm.user_id, vw_auth_user_perm.section_name;


ALTER TABLE cabinet.vw_auth_user_section_perm OWNER TO pavlov;
