﻿/*
	ALTER TABLE cabinet.auth_role DROP COLUMN group_id
*/

ALTER TABLE cabinet.auth_role DROP CONSTRAINT auth_role_group_id_fkey;

ALTER TABLE cabinet.auth_role DROP COLUMN group_id;