﻿/*
	CREATE OR REPLACE FUNCTION cabinet.change_pwd
*/

-- DROP FUNCTION cabinet.change_pwd(text, text);

CREATE OR REPLACE FUNCTION cabinet.change_pwd(_login text, _pwd text, OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN	
	result := 0;


	update cabinet.my_aspnet_membership
	set "Password" = _pwd
	from cabinet.my_aspnet_users t2
	where cabinet.my_aspnet_membership."userId" = t2.id
	and t2.name = _login
	;	
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.change_pwd(text, text)  OWNER TO pavlov;
