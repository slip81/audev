﻿/*
	insert into cabinet.auth_section
*/

insert into cabinet.auth_section (section_id, section_name, group_id)
values (11, 'Администрирование', 1);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (28, 'Роли', 11, 1);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (29, 'Пользователи', 11, 1);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (30, 'Журнал событий', 11, 1);


-- select * from cabinet.auth_section order by section_id
-- select * from cabinet.auth_section_part order by part_id