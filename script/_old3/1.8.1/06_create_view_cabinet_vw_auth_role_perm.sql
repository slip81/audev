﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_auth_role_perm
*/

-- DROP VIEW cabinet.vw_auth_role_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_role_perm AS 
 SELECT row_number() OVER (ORDER BY t1.section_id) AS id,
    t1.section_id,
    t1.section_name,
    t2.part_id,
    t2.part_name,
    t3.role_id,
    t3.role_name,
        COALESCE(t4.allow_view, false) AS allow_view,
        COALESCE(t4.allow_edit, false) AS allow_edit
   FROM cabinet.auth_section t1
     LEFT JOIN cabinet.auth_section_part t2 ON t1.section_id = t2.section_id
     LEFT JOIN cabinet.auth_role t3 ON 1 = 1
     LEFT JOIN cabinet.auth_role_perm t4 ON t3.role_id = t4.role_id AND t4.section_id = t1.section_id AND COALESCE(t4.part_id, t2.part_id) = t2.part_id     
  WHERE COALESCE(t2.group_id, COALESCE(t1.group_id, t3.group_id)) = t3.group_id;


ALTER TABLE cabinet.vw_auth_role_perm OWNER TO pavlov;
