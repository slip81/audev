﻿/*
	CREATE OR REPLACE FUNCTION discount.del_card_transaction
*/

-- DROP FUNCTION discount.del_card_transaction(interval);

CREATE OR REPLACE FUNCTION discount.del_card_transaction(
    IN hours interval,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
  _date_beg_for_del timestamp without time zone;
BEGIN
	_date_beg_for_del := current_timestamp - hours;
	
	result := (select count(*)
	from discount.card_transaction t1
	where t1.status = 1
	and t1.date_beg <= _date_beg_for_del);

	delete from discount.card_nominal_uncommitted t1 using discount.card_transaction t2
	where t1.card_trans_id = t2.card_trans_id
	and t2.status = 1
	and t2.date_beg <= _date_beg_for_del;

	delete from discount.programm_step_result t1 using discount.card_transaction t2
	where t1.result_id = t2.programm_result_id
	and t2.programm_result_id is not null
	and t2.status = 1
	and t2.date_beg <= _date_beg_for_del;

	delete from discount.programm_result_detail t1 using discount.card_transaction t2
	where t1.result_id = t2.programm_result_id
	and t2.programm_result_id is not null
	and t2.status = 1
	and t2.date_beg <= _date_beg_for_del;

	delete from discount.programm_result t1 using discount.card_transaction t2
	where t1.result_id = t2.programm_result_id
	and t2.programm_result_id is not null
	and t2.status = 1
	and t2.date_beg <= _date_beg_for_del;

	delete from discount.card_transaction_detail t1 using discount.card_transaction t2
	where t1.card_trans_id = t2.card_trans_id
	and t2.status = 1
	and t2.date_beg <= _date_beg_for_del;

	delete from discount.card_transaction_unit t1 using discount.card_transaction t2
	where t1.card_trans_id = t2.card_trans_id
	and t2.status = 1
	and t2.date_beg <= _date_beg_for_del;

	delete from discount.card_transaction t1
	where t1.status = 1
	and t1.date_beg <= _date_beg_for_del;	

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.del_card_transaction(interval) OWNER TO pavlov;
