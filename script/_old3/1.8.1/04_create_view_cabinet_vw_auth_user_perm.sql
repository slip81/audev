﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_auth_user_perm
*/


-- DROP VIEW cabinet.vw_auth_user_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_user_perm AS 

SELECT row_number() over (ORDER BY t1.section_id) as id, t1.section_id, t1.section_name, t2.part_id, t2.part_name,
  t3.user_id, t3.user_name, t4.role_id, t5.role_name,
  case when t6.perm_id is not null and t7.perm_id is null then true else false end as role_perm,
  case when t7.perm_id is not null then true else false end as user_perm,
  case when t7.perm_id is not null then t7.allow_view else coalesce(t6.allow_view, false) end as allow_view,
  case when t7.perm_id is not null then t7.allow_edit else coalesce(t6.allow_edit, false) end as allow_edit
FROM cabinet.auth_section t1
LEFT JOIN cabinet.auth_section_part t2 ON t1.section_id = t2.section_id
LEFT JOIN cabinet.client_user t3 ON 1 = 1
LEFT JOIN cabinet.auth_user_role t4 ON t3.user_id = t4.user_id
LEFT JOIN cabinet.auth_role t5 ON t4.role_id = t5.role_id
LEFT JOIN cabinet.auth_role_perm t6 ON t5.role_id = t6.role_id AND t6.section_id = t1.section_id AND coalesce(t6.part_id, t2.part_id) = t2.part_id
LEFT JOIN cabinet.auth_user_perm t7 ON t3.user_id = t7.user_id AND t7.section_id = t1.section_id AND coalesce(t7.part_id, t2.part_id) = t2.part_id
WHERE coalesce(t2.group_id, coalesce(t1.group_id, t5.group_id)) = t5.group_id
;

ALTER TABLE cabinet.vw_auth_user_perm OWNER TO pavlov;
