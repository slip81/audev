﻿/*
	CREATE OR REPLACE VIEW esn.vw_exchange_user
*/

-- DROP VIEW esn.vw_exchange_user;

CREATE OR REPLACE VIEW esn.vw_exchange_user AS 
 SELECT t1.user_id,
    t1.login,
    t1.client_id AS exchange_client_id,
    t10.client_id,
    t10.client_name,
    t10.sales_id,
    COALESCE(t10.sales_address, t10.sales_name) AS sales_name,
    t10.workplace_id,
    t10.description AS workplace_description,
    t10.registration_key AS workplace,
    t11.task_cnt,
    t11.task_cnt_completed,
    (t11.task_cnt_completed::text || '/' || t11.task_cnt::text) as task_info
   FROM esn.exchange_user t1
     LEFT JOIN cabinet.vw_workplace t10 ON t1.cabinet_workplace_id = t10.workplace_id
     LEFT JOIN (select x2.user_id, count(x1.task_id) as task_cnt, sum(case when x1.state in (2,3) then 1 else 0 end) as task_cnt_completed
		from esn.exchange_reg_task x1
		inner join esn.exchange_user_reg x2 on x1.reg_id = x2.reg_id
		group by x2.user_id) t11 ON t1.user_id = t11.user_id
     ;

ALTER TABLE esn.vw_exchange_user OWNER TO pavlov;


-- select * from esn.exchange_reg_task order by task_id;
