﻿/*
	CREATE OR REPLACE VIEW esn.vw_exchange_client
*/

-- DROP VIEW esn.vw_exchange_client;

CREATE OR REPLACE VIEW esn.vw_exchange_client AS 
	SELECT t1.client_id, t2.name as client_name, t1.cabinet_client_id
	FROM esn.exchange_client t1
	INNER JOIN cabinet.client t2 ON t1.cabinet_client_id = t2.id;

ALTER TABLE esn.vw_exchange_client OWNER TO pavlov;
