﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_cab_help_tag
*/

-- DROP VIEW cabinet.vw_cab_help_tag;

CREATE OR REPLACE VIEW cabinet.vw_cab_help_tag AS 
	SELECT DISTINCT trim(regexp_split_to_table(tag, E'\\|')) as tag
	FROM cabinet.cab_help;

ALTER TABLE cabinet.vw_cab_help_tag OWNER TO pavlov;
