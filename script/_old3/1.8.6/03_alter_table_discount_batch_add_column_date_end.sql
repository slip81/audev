﻿/*
	ALTER TABLE discount.batch ADD COLUMN date_end
*/

ALTER TABLE discount.batch ADD COLUMN date_end date;