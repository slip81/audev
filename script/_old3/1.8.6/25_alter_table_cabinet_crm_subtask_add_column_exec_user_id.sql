﻿/*
	ALTER TABLE cabinet.crm_subtask ADD COLUMN exec_user_id
*/

ALTER TABLE cabinet.crm_subtask ADD COLUMN exec_user_id integer NOT NULL DEFAULT 0;

ALTER TABLE cabinet.crm_subtask
  ADD CONSTRAINT crm_subtask_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE cabinet.crm_subtask SET exec_user_id = (SELECT exec_user_id FROM cabinet.crm_task WHERE cabinet.crm_subtask.task_id = cabinet.crm_task.task_id);