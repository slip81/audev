﻿/*
	CREATE OR REPLACE FUNCTION discount.report_discount_svod1
*/

-- DROP FUNCTION discount.report_discount_svod1(bigint, bigint, character varying);

CREATE OR REPLACE FUNCTION discount.report_discount_svod1(
    IN _business_id bigint,
    IN _card_type_group_id bigint,
    IN _date_beg date,
    IN _date_end date)
  RETURNS TABLE(org_name character varying, org_id bigint, num bigint, card_type_name character varying, unit_percent_discount numeric, sum_unit_value_discount numeric, pos_count bigint, check_count bigint) AS
$BODY$
BEGIN
	RETURN QUERY
		select t6.org_name, t6.org_id, row_number() over(partition by t6.org_id order by t2.unit_percent_discount) as num, t4.card_type_name, t2.unit_percent_discount
		, sum(t2.unit_value_discount) as sum_unit_value_discount, count(t2.detail_id) as pos_count, count(DISTINCT t1.card_trans_id) as check_count
		from discount.card_transaction t1
		inner join discount.card_transaction_detail t2 on t1.card_trans_id = t2.card_trans_id
		inner join discount.card t3 on t1.card_id = t3.card_id
		inner join discount.card_type t4 on t3.curr_card_type_id = t4.card_type_id
		left join discount.business_org_user t5 on t1.user_name = t5.user_name
		left join discount.business_org t6 on t5.org_id = t6.org_id
		where 1=1
		and t1.status = 0
		and t1.trans_kind = 1
		and t3.business_id = _business_id		
		and (((t1.date_check >= _date_beg) and (_date_beg is not null)) or (_date_beg is null))
		and (((t1.date_check <= _date_end) and (_date_end is not null)) or (_date_end is null))
		and t4.card_type_group_id = _card_type_group_id
		group by t6.org_name, t6.org_id, t4.card_type_name, t2.unit_percent_discount
		having sum(t2.unit_value_discount) > 0
		;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION discount.report_discount_svod1(bigint, bigint, date, date) OWNER TO pavlov;
