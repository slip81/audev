﻿/*
	insert into cabinet.crm_notify_param_attr, crm_notify_param_content
*/


delete from cabinet.crm_notify_param_attr;

-- icq
insert into cabinet.crm_notify_param_attr (notify_id, transport_type, column_id, is_active)
select t1.notify_id, 0, t2.column_id, true
from cabinet.crm_notify t1
inner join cabinet.cab_grid_column t2 on 1 = 1 and t2.grid_id = 1;

-- email
insert into cabinet.crm_notify_param_attr (notify_id, transport_type, column_id, is_active)
select t1.notify_id, 1, t2.column_id, true
from cabinet.crm_notify t1
inner join cabinet.cab_grid_column t2 on 1 = 1 and t2.grid_id = 1;

-- select * from cabinet.cab_grid_column
-- select * from cabinet.crm_notify_param_attr order by notify_id, transport_type, column_id;

delete from cabinet.crm_notify_param_content;

-- icq
insert into cabinet.crm_notify_param_content (notify_id, transport_type, column_id, is_active)
select t1.notify_id, 0, t2.column_id, true
from cabinet.crm_notify t1
inner join cabinet.cab_grid_column t2 on 1 = 1 and t2.grid_id = 1;

-- email
insert into cabinet.crm_notify_param_content (notify_id, transport_type, column_id, is_active)
select t1.notify_id, 1, t2.column_id, true
from cabinet.crm_notify t1
inner join cabinet.cab_grid_column t2 on 1 = 1 and t2.grid_id = 1;

-- select * from cabinet.cab_grid_column where grid_id = 1 order by column_id
-- select * from cabinet.crm_notify_param_content order by notify_id, transport_type, column_id;
