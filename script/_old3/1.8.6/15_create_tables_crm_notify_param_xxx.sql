﻿/*
	CREATE TABLE cabinet.crm_notify_param_user
*/

-- DROP TABLE cabinet.crm_notify_param_user;

CREATE TABLE cabinet.crm_notify_param_user
(
  param_id serial NOT NULL,
  notify_id integer NOT NULL,
  transport_type integer NOT NULL DEFAULT 0,
  user_id integer NOT NULL,
  is_executer boolean NOT NULL DEFAULT true,
  is_owner boolean NOT NULL DEFAULT true,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT crm_notify_param_user_pkey PRIMARY KEY (param_id),
  CONSTRAINT crm_notify_param_user_notify_id_fkey FOREIGN KEY (notify_id)
      REFERENCES cabinet.crm_notify (notify_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT crm_notify_param_user_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_notify_param_user OWNER TO pavlov;

-- DROP TRIGGER cabinet_crm_notify_param_user_set_stamp_trg ON cabinet.crm_notify_param_user;

CREATE TRIGGER cabinet_crm_notify_param_user_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.crm_notify_param_user
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

-- DROP TABLE cabinet.crm_notify_param_attr;

CREATE TABLE cabinet.crm_notify_param_attr
(
  param_id serial NOT NULL,
  notify_id integer NOT NULL,
  transport_type integer NOT NULL DEFAULT 0,
  column_id integer NOT NULL,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT crm_notify_param_attr_pkey PRIMARY KEY (param_id),
  CONSTRAINT crm_notify_param_attr_notify_id_fkey FOREIGN KEY (notify_id)
      REFERENCES cabinet.crm_notify (notify_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT crm_notify_param_attr_column_id_fkey FOREIGN KEY (column_id)
      REFERENCES cabinet.cab_grid_column (column_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_notify_param_attr OWNER TO pavlov;

-- DROP TRIGGER cabinet_crm_notify_param_attr_set_stamp_trg ON cabinet.crm_notify_param_attr;

CREATE TRIGGER cabinet_crm_notify_param_attr_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.crm_notify_param_attr
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();  

-- DROP TABLE cabinet.crm_notify_param_content;

CREATE TABLE cabinet.crm_notify_param_content
(
  param_id serial NOT NULL,
  notify_id integer NOT NULL,
  transport_type integer NOT NULL DEFAULT 0,
  column_id integer NOT NULL,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT crm_notify_param_content_pkey PRIMARY KEY (param_id),
  CONSTRAINT crm_notify_param_content_notify_id_fkey FOREIGN KEY (notify_id)
      REFERENCES cabinet.crm_notify (notify_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT crm_notify_param_content_column_id_fkey FOREIGN KEY (column_id)
      REFERENCES cabinet.cab_grid_column (column_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_notify_param_content OWNER TO pavlov;

-- DROP TRIGGER cabinet_crm_notify_param_content_set_stamp_trg ON cabinet.crm_notify_param_content;

CREATE TRIGGER cabinet_crm_notify_param_content_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.crm_notify_param_content
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();  
