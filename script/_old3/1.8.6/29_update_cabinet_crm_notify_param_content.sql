﻿/*
	update cabinet.crm_notify_param_content
*/

update cabinet.crm_notify_param_content
set is_active = false
where column_id not in (3, 4);