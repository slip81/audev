﻿/*
	ALTER TABLE cabinet.crm_notify_param_attr, crm_notify_param_content ADD COLUMN is_active
*/

ALTER TABLE cabinet.crm_notify_param_attr ADD COLUMN is_active boolean NOT NULL DEFAULT true;
ALTER TABLE cabinet.crm_notify_param_content ADD COLUMN is_active boolean NOT NULL DEFAULT true;
