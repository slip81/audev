﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_user
*/

-- DROP VIEW cabinet.vw_crm_notify_param_user;

CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_user AS 
 SELECT row_number() OVER (ORDER BY t1.notify_id) AS adr,
    t1.notify_id,
    t1.user_id,
    t12.transport_type,
    t11.user_id AS changer_user_id,
    t11.user_name,
    t12.is_executer,
    t12.is_owner,
        CASE
            WHEN t12.user_id IS NULL THEN false
            ELSE true
        END AS is_active
   FROM cabinet.crm_notify t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1 AND t11.is_crm_user
     LEFT JOIN cabinet.crm_notify_param_user t12 ON t1.notify_id = t12.notify_id AND t11.user_id = t12.user_id;

ALTER TABLE cabinet.vw_crm_notify_param_user OWNER TO pavlov;
