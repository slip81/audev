﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_attr
*/

-- DROP VIEW cabinet.vw_crm_notify_param_attr;

CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_attr AS 
 SELECT row_number() OVER (ORDER BY t1.notify_id) AS adr,
    t1.notify_id,
    t1.user_id,
    t12.transport_type,
    t11.column_id,
    t11.column_name,
    t11.column_name_rus,
        CASE
            WHEN t12.column_id IS NULL THEN false
            ELSE true
        END AS is_active
   FROM cabinet.crm_notify t1
     LEFT JOIN cabinet.cab_grid_column t11 ON 1 = 1 AND t11.grid_id = 1
     LEFT JOIN cabinet.crm_notify_param_attr t12 ON t1.notify_id = t12.notify_id AND t11.column_id = t12.column_id;

ALTER TABLE cabinet.vw_crm_notify_param_attr OWNER TO pavlov;
