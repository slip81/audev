﻿/*
	insert into cabinet.crm_notify
*/

delete from cabinet.crm_notify;

insert into cabinet.crm_notify (user_id, is_active_icq, is_active_email, icq, email)
select user_id, trim(coalesce(icq,'')) != '', false, icq, email
from cabinet.cab_user
where is_active = true;

-- select * from cabinet.crm_notify;