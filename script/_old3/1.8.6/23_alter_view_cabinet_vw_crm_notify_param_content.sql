﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_content
*/

-- DROP VIEW cabinet.vw_crm_notify_param_content;

CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_content AS 
 SELECT row_number() OVER (ORDER BY t1.notify_id) AS adr,
    t1.notify_id,
    t1.user_id,
    t11.transport_type,
    t11.column_id,
    t12.column_name,
    t12.column_name_rus,
    t11.is_active
   FROM cabinet.crm_notify t1     
     LEFT JOIN cabinet.crm_notify_param_content t11 ON t1.notify_id = t11.notify_id
     LEFT JOIN cabinet.cab_grid_column t12 ON t11.column_id = t12.column_id AND t12.grid_id = 1;

ALTER TABLE cabinet.vw_crm_notify_param_content OWNER TO pavlov;
