﻿/*
	ALTER TABLE cabinet.notify ADD COLUMN transport_type
*/

ALTER TABLE cabinet.notify ADD COLUMN transport_type integer NOT NULL DEFAULT 0;
COMMENT ON COLUMN cabinet.notify.transport_type IS '0 - icq, 1 - email';