﻿/*
	insert into discount.report_discount
*/

insert into discount.report_discount (report_id, report_name, report_descr)
values (4, 'Сводный отчет по скидкам', 'Отчет формирует сгруппированную по отделениям информацию о скидках по картам за заданный период');