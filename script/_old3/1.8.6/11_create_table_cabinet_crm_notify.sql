﻿/*
	CREATE TABLE cabinet.crm_notify
*/

-- DROP TABLE cabinet.crm_notify;

CREATE TABLE cabinet.crm_notify
(
  notify_id serial NOT NULL,
  user_id integer NOT NULL DEFAULT 0,
  is_active_icq boolean NOT NULL DEFAULT true,
  is_active_email boolean NOT NULL DEFAULT true,
  icq character varying,
  email character varying,   
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT crm_notify_pkey PRIMARY KEY (notify_id),
  CONSTRAINT crm_notify_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_notify OWNER TO pavlov;

-- DROP TRIGGER cabinet_crm_notify_set_stamp_trg ON cabinet.crm_notify;

CREATE TRIGGER cabinet_crm_notify_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.crm_notify
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

