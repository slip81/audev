﻿/*
	ALTER TABLE discount.card_transaction_detail, programm_result_detail ADD COLUMN tax_summa
*/

ALTER TABLE discount.card_transaction_detail ADD COLUMN tax_summa numeric;

ALTER TABLE discount.programm_result_detail ADD COLUMN tax_summa numeric;