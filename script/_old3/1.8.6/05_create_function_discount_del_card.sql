﻿/*
	CREATE OR REPLACE FUNCTION discount.del_card
*/

-- DROP FUNCTION discount.del_card(interval);

CREATE OR REPLACE FUNCTION discount.del_card(
    IN _card_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
	delete from discount.card_nominal_uncommitted where card_id = _card_id;

	delete from discount.card_nominal where card_id = _card_id;
	
	delete from discount.card_transaction_detail
	where card_trans_id in (select card_trans_id from discount.card_transaction where card_id = _card_id);

	delete from discount.card_transaction where card_id = _card_id;

	delete from discount.card_card_status_rel where card_id = _card_id;

	delete from discount.card_card_type_rel where card_id = _card_id;

	delete from discount.card_holder_card_rel where card_id = _card_id;

	delete from discount.card where card_id = _card_id;

	result:= 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


ALTER FUNCTION discount.del_card(bigint) OWNER TO pavlov;
