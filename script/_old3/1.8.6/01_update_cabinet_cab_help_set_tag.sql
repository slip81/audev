﻿/*
	update cabinet.cab_help set tag
*/

-- select * from cabinet.cab_help order by help_level, sort;

update cabinet.cab_help
set tag = tag || ' | виджеты | услуги'
where help_id = 4;

update cabinet.cab_help
set tag = tag || ' | дисконтные карты | обмен с партнерами | брак | цва | карты | дисконты | бонусы | мз | асна | цены в аптеках'
where help_id = 5;

update cabinet.cab_help
set tag = tag || ' | лицензии | заявки | торговые точки | рабочие места | пользователи | логины | услуги | сервисы'
where help_id = 6;

update cabinet.cab_help
set tag = tag || ' | helpdesk | задачи | хранилище | хелпдеск'
where help_id = 7;

update cabinet.cab_help
set tag = tag || ' | журнал событий | полномочия | лог | права | роли | сотрудники'
where help_id = 8;

update cabinet.cab_help
set tag = tag || ' | меню пользователя | стартовая страница'
where help_id = 9;

update cabinet.cab_help
set tag = tag || ' | стартовая страница'
where help_id = 10;

update cabinet.cab_help
set tag = tag || ' | карты | дисконты | бонусы | сертификаты | скидки | проценты | деньги | алгоритмы | продажи | организации | отделения | типы карт'
where help_id = 11;

update cabinet.cab_help
set tag = tag || ' | владельцы карт | держатели карт | покупатели'
where help_id = 11;

update cabinet.cab_help
set tag = tag || ' | мз | мелодия здоровья | асна | журнал обмена | участники обмена | партнеры'
where help_id = 12;

update cabinet.cab_help
set tag = tag || ' | цены в аптеках'
where help_id = 14;

update cabinet.cab_help
set tag = tag || ' | лицензии | заявки | торговые точки | рабочие места | пользователи | логины | услуги | сервисы'
where help_id = 15;

update cabinet.cab_help
set tag = tag || ' | активация | регистрация'
where help_id = 16;

update cabinet.cab_help
set tag = tag || ' | услуги | версии'
where help_id = 17;

update cabinet.cab_help
set tag = tag || ' | шаблоны рассылки'
where help_id = 18;

update cabinet.cab_help
set tag = tag || ' | хелпдеск'
where help_id = 19;

update cabinet.cab_help
set tag = tag || ' | календарь'
where help_id = 20;

update cabinet.cab_help
set tag = tag || ' | папки | файлы | ссылки'
where help_id = 21;

update cabinet.cab_help
set tag = tag || ' | лог | история'
where help_id = 22;

update cabinet.cab_help
set tag = tag || ' | права | роли | пользователи | сотрудники'
where help_id = 23;

update cabinet.cab_help
set tag = tag || ' | меню пользователя | стартовая страница'
where help_id = 24;

update cabinet.cab_help
set tag = tag || ' | меню пользователя | стартовая страница'
where help_id = 25;

update cabinet.cab_help
set tag = tag || ' | смена пароля'
where help_id = 26;