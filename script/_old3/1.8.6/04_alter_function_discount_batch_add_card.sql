﻿/*
	CREATE OR REPLACE FUNCTION discount.batch_add_card
*/

-- DROP FUNCTION discount.batch_add_card(bigint);

CREATE OR REPLACE FUNCTION discount.batch_add_card(
    IN _batch_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
    _business_id bigint; -- код организации, для которой создаем карты
    _card_type_id bigint; -- код типа карты, с которым создаем карты
    _card_status_id bigint; -- код статуса карты, с которым создаем карты
    _date_beg date; -- дата начала действия карт
    _date_end date; -- дата окончания действия карт
    _card_count integer; -- кол-во карт
    _card_prefix bigint; -- префикс (от 1 до 3-х символов) Д.б. до 4-х символов (часто делают год выпуска карт как префикс)
    _num_count integer; -- кол-во символов
    _card_num_first bigint; -- номер первой карты
    _card_num_step bigint; -- шаг увеличения номера
    _use_check_value boolean; -- 1 - использовать контрольный символ, 0 - не использовать
    _card_num_last bigint; -- номер последней карты
    _card_ean_null bigint; -- штрих-код нулевой карты с учетом префикса
    _card_ean_curr bigint; -- штрих-код текущей карты
    _card_num_curr bigint; -- номер текущей карты
    _card_ean_limit bigint; -- штрих-код, меньше к-ого должны быть штрих-кода карт. Иначе префикс и номер карты пересекутся
    _card_num_limit bigint; -- номер карты, меньше или равные к-ому должны быть номера генерируемых карт. Иначе префикс и номер карты пересекутся
    _card_num_max bigint; -- максимальный допустимый номер карт, не превышающий предельный номер карт _card_num_limit
    _power_with_prefix integer; -- степень, в которую требуется возвести число 10, чтобы получить ШК карты нужной длины	
    _power_multipl_prefix bigint; --  = power(10, _power_with_prefix). Если не считать отдельно, для больших номеров карт выходит ошибка записи в целое число (например, для 17-значного штрих-кода) 
    _power_check_digit integer; -- = power(10, cast(_use_check_value as integer)). Если не считать отдельно, для больших номеров карт выходит ошибка записи в целое число (например, для 17-значного штрих-кода)
    _card_id bigint; -- код текущей карты
    _user_name character varying; -- пользователь, под которым создаются карты
    _error_cnt integer; -- количество карт, к-ые не удалось добавить
BEGIN
    SET search_path to 'discount';
    
    result := 0; 

    update batch set batch_name = 'Создание карт (' || cast(card_count as character varying) || ')' where batch_id = _batch_id;
	
    select business_id, card_type_id, card_status_id, date_beg, date_end, card_count, 
                num_first, num_count, next_num_first, next_num_step, use_check_value, crt_user 
      INTO _business_id, _card_type_id, _card_status_id, _date_beg, _date_end, _card_count, 
                _card_prefix, _num_count, _card_num_first, _card_num_step, _use_check_value, _user_name		   
      from batch where batch_id = _batch_id;	
	
    _card_num_last = _card_num_first + (_card_count - 1)*_card_num_step;
    _power_with_prefix = (_num_count - 1) - floor(log(_card_prefix));
    _power_multipl_prefix = power(10, _power_with_prefix);
    _power_check_digit = power(10, cast(_use_check_value as integer));
    -- формирование ШК нулевой карты с учетом префикса (есть префикс, номер карты = 0)
    _card_ean_null = _card_prefix * _power_multipl_prefix;
    _card_ean_limit = (_card_prefix + 1) * _power_multipl_prefix;
    _card_num_limit = floor((_card_ean_limit - 1 - _card_ean_null)/(1.0000*_power_check_digit));
    IF ( _card_num_last > _card_num_limit ) THEN
      _card_num_max = _card_num_first + _card_num_step * floor((_card_num_limit - _card_num_first)/(1.0000*_card_num_step));
    ELSE
      _card_num_max = _card_num_last;
    END IF;
	
    -- добавляемые карты
    _card_num_curr = _card_num_first;
    WHILE (_card_num_curr <= _card_num_max) LOOP -- FOR LOOP не подходит, если номер карты превышаеи макс. значение для integer	  
      -- формирование ШК карты c текущим номером карты с учетом наличия/отсутствия контрольного символа
      _card_ean_curr = _card_ean_null + _card_num_curr * _power_check_digit;
	  
      -- формирование контрольного символа, если он нужен
      IF _use_check_value THEN
        _card_ean_curr = check_digit_generate(_card_ean_curr, true); 
      END IF;	  
	  
      IF (EXISTS(select 1 from card where business_id = _business_id and card_num = _card_ean_curr)) THEN
        insert into batch_error (batch_id, card_num, card_num2, mess) values
                                (_batch_id, _card_ean_curr, _card_num_curr, 'Уже существует карта с номером ' || cast(_card_ean_curr as character varying));		                 
      ELSE
        -- добавляем карту
        insert into card (date_beg, date_end, business_id, curr_trans_count, card_num, card_num2, curr_trans_sum, curr_card_status_id, curr_card_type_id, batch_id) values
                         (_date_beg, _date_end, _business_id, 0, _card_ean_curr, _card_num_curr, 0, _card_status_id, _card_type_id, _batch_id) 
			 returning card_id into _card_id;
        insert into card_card_status_rel(card_id, card_status_id, date_beg, date_end) values
                                        (_card_id, _card_status_id, _date_beg, null);
        insert into card_card_type_rel (card_id, card_type_id, date_beg, date_end, ord) values
		                       (_card_id, _card_type_id, _date_beg, null, 1);
        insert into card_transaction (card_id, trans_num, date_beg, trans_sum, trans_kind, status, user_name, batch_id) values
                                     (_card_id, 1, current_date, 0, 4, 0, _user_name, _batch_id);
		
        -- добавляем все номиналы, к-ые есть для данного типа карты (закомментировано, т.к. пока нулевые номиналы не добавляем)				 
--        FOR _card_type_unit IN SELECT unit_type FROM card_type_unit 
--                               WHERE card_type_id = _card_type_id
--        LOOP
--          insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user) values
--  				     (_card_id, current_date, _card_type_unit, 0, current_timestamp, _user_name);	  
--        END LOOP;		
      END IF;
      _card_num_curr = _card_num_curr + _card_num_step;	   	  
    END LOOP;
	
    -- карты с пересечением номера карты и префикса: добавление сообщения об ошибке в batch_error
    WHILE (_card_num_curr <= _card_num_last) LOOP
      insert into batch_error (batch_id, card_num, card_num2, mess) values
                              (_batch_id, _card_num_curr, _card_num_curr, 'Недопустимый номер карты: номер карты и префикс пересекаются.');		
      _card_num_curr = _card_num_curr + _card_num_step;	
    END LOOP;

    select count(*) INTO _error_cnt from batch_error where batch_id = _batch_id; 
    update batch set state = 2, mess = 'Операция завершена', state_date = clock_timestamp(), state_user = _user_name,
                     record_cnt = _card_count, processed_cnt = _card_count, error_cnt = _error_cnt where batch_id = _batch_id;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.batch_add_card(bigint) OWNER TO pavlov;
