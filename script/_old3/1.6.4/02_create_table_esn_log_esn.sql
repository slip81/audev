﻿/*
	CREATE TABLE esn.log_esn
*/

-- DROP TABLE esn.log_esn;

CREATE TABLE esn.log_esn
(
  log_id bigserial NOT NULL,
  date_beg timestamp without time zone NOT NULL,
  mess character varying,
  user_name character varying,
  log_esn_type_id bigint,
  obj_id bigint,
  scope integer,
  date_end timestamp without time zone,
  mess2 character varying,
  CONSTRAINT log_esn_pkey PRIMARY KEY (log_id),
  CONSTRAINT log_esn_log_esn_type_id_fkey FOREIGN KEY (log_esn_type_id)
      REFERENCES esn.log_esn_type (type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.log_esn OWNER TO pavlov;
