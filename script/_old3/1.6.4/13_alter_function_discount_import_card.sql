﻿/*
	CREATE OR REPLACE FUNCTION discount.import_card
*/

-- DROP FUNCTION discount.import_card(bigint, bigint, bigint, integer, integer, integer, character varying);

CREATE OR REPLACE FUNCTION discount.import_card(
    IN _business_id bigint,
    IN _card_type_id bigint,
    IN _card_status_id bigint,
    IN _action_for_existing_summ integer,
    IN _action_for_existing_disc_percent integer,
    IN _action_for_existing_bonus integer,
    IN _user_name character varying,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
    _card_type_unit integer; -- unit_types in card_type _card_type_id    
BEGIN
	SET search_path to 'discount';
	
	result := 0;    
    
	update import_discount_card
	set barcode = 0
	where barcode is null;

	update import_discount_card
	set barcode = 0
	where barcode = '_';
		
	delete from import_discount_card
	where barcode = '0';

	-- is_card_exists = 1: карты, которые уже есть в БД
	update import_discount_card
	set is_card_exists = 1
	where cast(barcode as bigint) in
	(select card_num from card where business_id = _business_id);

	-- part1: импортируем карты, которых нет в БД
	insert into card (date_beg, date_end, business_id, curr_trans_count, card_num, card_num2, curr_trans_sum)
	select date_card, null, _business_id, 0, cast(barcode as bigint), num_card, total_sum
	from import_discount_card
	where is_card_exists = 0;
	
	insert into card_card_status_rel(card_id, card_status_id, date_beg, date_end)
	select t1.card_id, _card_status_id, t1.date_beg, t1.date_end 
	from card t1
	inner join import_discount_card t2 on t1.card_num = cast(t2.barcode as bigint)
	and t2.is_card_exists = 0
	where t1.business_id = _business_id;
	
	insert into card_card_type_rel (card_id, card_type_id, date_beg, date_end, ord)
	select t1.card_id, _card_type_id, t1.date_beg, t1.date_end, 1 
	from card t1
	inner join import_discount_card t2 on t1.card_num = cast(t2.barcode as bigint)
	and t2.is_card_exists = 0
	where t1.business_id = _business_id;
	
	update card set curr_card_status_id = 
	(select max(card_status_id) from card_card_status_rel where card_card_status_rel.card_id = card.card_id)
	where business_id = _business_id
	and card_num in (select cast(barcode as bigint) from import_discount_card where is_card_exists = 0);

	update card set curr_card_type_id = 
	(select max(card_type_id) from card_card_type_rel where card_card_type_rel.card_id = card.card_id)
	where business_id = _business_id
	and card_num in (select cast(barcode as bigint) from import_discount_card where is_card_exists = 0);	
	
	insert into card_transaction (card_id, trans_num, date_beg, trans_sum, trans_kind, status, user_name)
	select t1.card_id, 1, current_date, t1.curr_trans_sum, 4, 0, _user_name
	from card t1
	inner join import_discount_card t2 on t1.card_num = cast(t2.barcode as bigint)
	and t2.is_card_exists = 0
	where t1.business_id = _business_id;

	-- добавляем все возможные значения unit_type 
	FOR _card_type_unit IN SELECT t1.unit_type FROM card_type_unit t1 
				WHERE t1.card_type_id = _card_type_id /*AND t1.unit_type <> _unit_type*/
	LOOP
		CASE _card_type_unit 
		   WHEN 1 THEN -- Bonus qty
			insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
			select distinct t2.card_id, current_date, _card_type_unit, t1.total_bonus, current_timestamp, _user_name
			from import_discount_card t1  
			inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
			where t2.business_id = _business_id
			and t1.is_card_exists = 0;
		   WHEN 2 THEN -- Discount proc
			insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
			select distinct t3.card_id, current_date, _card_type_unit, t2.proc_discount, current_timestamp, _user_name
			from import_discount_card t1
			inner join import_discount t2 on t1.id_discount = t2.id_discount
			inner join card t3 on cast(t1.barcode as bigint) = t3.card_num
			where t3.business_id = _business_id
			and t1.is_card_exists = 0;
		   WHEN 3 THEN -- Money qty
			insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
			select distinct t2.card_id, current_date, _card_type_unit, t1.total_sum, current_timestamp, _user_name
			from import_discount_card t1  
			inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
			where t2.business_id = _business_id
			and t1.is_card_exists = 0;
		   ELSE
		   --WHEN 4 THEN -- Bonus proc
		   --WHEN 5 THEN -- Discount qty	
		END CASE;
	END LOOP;

	insert into card_holder (card_holder_surname, card_holder_name, card_holder_fname, tmp_card_id, business_id)
	select t1.lname, t1.fname, t1.mname, t2.card_id, t2.business_id
	from import_discount_card t1
	inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
	where ((coalesce(t1.lname, '_') <> '_') or (coalesce(t1.fname, '_') <> '_') or (coalesce(t1.mname, '_') <> '_'))
	and t2.business_id = _business_id
	and t1.is_card_exists = 0;

	insert into card_holder_card_rel (card_holder_id, card_id, date_beg)
	select t1.card_holder_id, t1.tmp_card_id, current_date
	from card_holder t1
	inner join card t2 on t1.tmp_card_id = t2.card_id
	inner join import_discount_card t3 on t2.card_num = cast(t3.barcode as bigint)
	and t3.is_card_exists = 0
	where t1.business_id = _business_id;

	update card set curr_holder_id = 
	(select card_holder_card_rel.card_holder_id from card_holder_card_rel where card_holder_card_rel.card_id = card.card_id limit 1)
	where business_id = _business_id
	and card_num in (select cast(barcode as bigint) from import_discount_card where is_card_exists = 0);

	update card_holder set tmp_card_id = null where business_id = _business_id;

	-- part2: обновляем суммы и карточные единицы по картам, которые уже были в БД
		
	CASE _action_for_existing_summ
	-- для накопленных сумм: 0 - оставлять, 1 - заменять, 2 - суммировать
	WHEN 1 THEN
		update card
		set curr_trans_sum =
		coalesce((select max(x1.total_sum) from import_discount_card x1 where cast(x1.barcode as bigint) = card.card_num and x1.is_card_exists = 1), 0)
		where business_id = _business_id
		and card_num in (select cast(barcode as bigint) from import_discount_card where is_card_exists = 1);		
	WHEN 2 THEN
		update card
		set curr_trans_sum = coalesce(curr_trans_sum, 0) + 
		coalesce((select max(x1.total_sum) from import_discount_card x1 where cast(x1.barcode as bigint) = card.card_num and x1.is_card_exists = 1), 0)
		where business_id = _business_id
		and card_num in (select cast(barcode as bigint) from import_discount_card where is_card_exists = 1);
	ELSE
	END CASE;

	-- карточные единицы для всех существующих карт
	-- 0 - оставлять, 1 - заменять, 2 - суммировать
	FOR _card_type_unit IN SELECT t1.unit_type FROM card_type_unit t1 
				WHERE t1.card_type_id = _card_type_id /*AND t1.unit_type <> _unit_type*/
	LOOP			
		CASE _card_type_unit 
		   WHEN 1 THEN -- Bonus qty			   
			if (_action_for_existing_bonus = 1) then
				update card_nominal 
				set date_end = current_date
				from import_discount_card t1
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where card_nominal.card_id = t2.card_id
				and card_nominal.unit_type = _card_type_unit
				and card_nominal.date_end is null
				and t2.business_id = _business_id
				and t1.is_card_exists = 1;
				
				insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
				select distinct t2.card_id, current_date, _card_type_unit, t1.total_bonus, current_timestamp, _user_name
				from import_discount_card t1  
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where t2.business_id = _business_id
				and t1.is_card_exists = 1;
			end if;
			if (_action_for_existing_bonus = 2) then
				update card_nominal 
				set date_end = current_date
				from import_discount_card t1
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where card_nominal.card_id = t2.card_id
				and card_nominal.unit_type = _card_type_unit
				and card_nominal.date_end is null
				and t2.business_id = _business_id
				and t1.is_card_exists = 1;
							
				insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
				select distinct t2.card_id, current_date, _card_type_unit
				, coalesce(t1.total_bonus, 0) + coalesce((select x1.unit_value from card_nominal x1 where x1.card_id = t2.card_id and x1.unit_type = _card_type_unit				
				and x1.date_end is not null order by x1.card_nominal_id desc limit 1), 0)
				, current_timestamp, _user_name
				from import_discount_card t1  
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where t2.business_id = _business_id
				and t1.is_card_exists = 1;
			end if;
		   WHEN 2 THEN -- Discount proc
			if (_action_for_existing_disc_percent = 1) then
				update card_nominal 
				set date_end = current_date
				from import_discount_card t1
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where card_nominal.card_id = t2.card_id
				and card_nominal.unit_type = _card_type_unit
				and card_nominal.date_end is null
				and t2.business_id = _business_id
				and t1.is_card_exists = 1;
							
				insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
				select distinct t3.card_id, current_date, _card_type_unit, t2.proc_discount, current_timestamp, _user_name
				from import_discount_card t1
				inner join import_discount t2 on t1.id_discount = t2.id_discount
				inner join card t3 on cast(t1.barcode as bigint) = t3.card_num
				where t3.business_id = _business_id
				and t1.is_card_exists = 1;
			end if;
			if (_action_for_existing_disc_percent = 2) then
				update card_nominal 
				set date_end = current_date
				from import_discount_card t1
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where card_nominal.card_id = t2.card_id
				and card_nominal.unit_type = _card_type_unit
				and card_nominal.date_end is null
				and t2.business_id = _business_id
				and t1.is_card_exists = 1;
							
				insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
				select distinct t3.card_id, current_date, _card_type_unit
				, coalesce(t2.proc_discount, 0) + coalesce((select x1.unit_value from card_nominal x1 where x1.card_id = t3.card_id and x1.unit_type = _card_type_unit				
				and x1.date_end is not null order by x1.card_nominal_id desc limit 1), 0)
				, current_timestamp, _user_name
				from import_discount_card t1
				inner join import_discount t2 on t1.id_discount = t2.id_discount
				inner join card t3 on cast(t1.barcode as bigint) = t3.card_num
				where t3.business_id = _business_id
				and t1.is_card_exists = 1;
			end if;			
		   WHEN 3 THEN -- Money qty
			if (_action_for_existing_summ = 1) then
				update card_nominal 
				set date_end = current_date
				from import_discount_card t1
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where card_nominal.card_id = t2.card_id
				and card_nominal.unit_type = _card_type_unit
				and card_nominal.date_end is null
				and t2.business_id = _business_id
				and t1.is_card_exists = 1;

				insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
				select distinct t2.card_id, current_date, _card_type_unit, t1.total_sum, current_timestamp, _user_name
				from import_discount_card t1  
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where t2.business_id = _business_id
				and t1.is_card_exists = 1;				
			end if;
			if (_action_for_existing_summ = 2) then
				update card_nominal 
				set date_end = current_date
				from import_discount_card t1
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where card_nominal.card_id = t2.card_id
				and card_nominal.unit_type = _card_type_unit
				and card_nominal.date_end is null
				and t2.business_id = _business_id
				and t1.is_card_exists = 1;

				insert into card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
				select distinct t2.card_id, current_date, _card_type_unit
				, coalesce(t1.total_sum, 0) + coalesce((select x1.unit_value from card_nominal x1 where x1.card_id = t2.card_id and x1.unit_type = _card_type_unit				
				and x1.date_end is not null order by x1.card_nominal_id desc limit 1), 0)
				, current_timestamp, _user_name
				from import_discount_card t1  
				inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
				where t2.business_id = _business_id
				and t1.is_card_exists = 1;				
			end if;				
		   ELSE
		   --WHEN 4 THEN -- Bonus proc
		   --WHEN 5 THEN -- Discount qty	
		END CASE;
	END LOOP;	
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.import_card(bigint, bigint, bigint, integer, integer, integer, character varying) OWNER TO pavlov;
