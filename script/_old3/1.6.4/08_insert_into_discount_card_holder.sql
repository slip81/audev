﻿/*
	insert into discount.card_holder
*/

insert into discount.card_holder(card_holder_id, card_holder_surname, card_holder_name, card_holder_fname, date_birth, phone_num, address, email, comment, business_id, tmp_card_id)
select
client_id, client_surname, client_name, client_fname, date_birth, phone_num, address, email, comment, business_id, tmp_card_id
from discount.client;

insert into discount.card_holder_card_rel(rel_id, card_holder_id, card_id, date_beg, date_end)
select rel_id, client_id, card_id, date_beg, date_end
from discount.client_card_rel;

SELECT setval('discount.card_holder_card_rel_rel_id_seq', (SELECT MAX(rel_id)+1 FROM discount.card_holder_card_rel));