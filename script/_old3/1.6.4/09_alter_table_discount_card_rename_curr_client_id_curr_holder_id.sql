﻿/*
	ALTER TABLE discount.card RENAME curr_client_id TO curr_holder_id
*/

ALTER TABLE discount.card DROP CONSTRAINT card_curr_client_id_fkey;

ALTER TABLE discount.card RENAME curr_client_id TO curr_holder_id;

ALTER TABLE discount.card
  ADD CONSTRAINT card_curr_holder_id_fkey FOREIGN KEY (curr_holder_id)
      REFERENCES discount.card_holder (card_holder_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
