﻿/*
	CREATE OR REPLACE VIEW esn.vw_log_esn_exchange_partner
*/

-- DROP VIEW esn.vw_log_esn_exchange_partner;

DROP VIEW esn.vw_log_event_exchange_partner;

CREATE OR REPLACE VIEW esn.vw_log_esn_exchange_partner AS
 SELECT t3.client_id,
    t3.client_name,
    t2.user_id,
    date_trunc('minute'::text, t1.date_beg) AS date_beg_minutes,
    date_part('epoch'::text, date_trunc('minute'::text, t1.date_beg))::bigint AS date_id,
    t1.log_id,
    t1.date_beg,
    t1.mess,
    t1.user_name,
    t1.log_esn_type_id,
    t1.obj_id,
    t1.scope,
    t1.date_end,
    t1.mess2
   FROM esn.log_esn t1
     JOIN esn.exchange_user t2 ON t1.user_name::text = t2.login::text
     JOIN esn.exchange_client t3 ON t2.client_id = t3.client_id
  WHERE t1.scope = 4;

ALTER TABLE esn.vw_log_esn_exchange_partner OWNER TO pavlov;
