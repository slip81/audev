﻿/*
	CREATE TABLE discount.card_holder_card_rel
*/

-- DROP TABLE discount.card_holder_card_rel;

CREATE TABLE discount.card_holder_card_rel
(
  rel_id bigserial NOT NULL,
  card_holder_id bigint NOT NULL,
  card_id bigint NOT NULL,
  date_beg date,
  date_end date,
  CONSTRAINT card_holder_card_rel_pkey PRIMARY KEY (rel_id),
  CONSTRAINT card_holder_card_rel_card_id_fkey FOREIGN KEY (card_id)
      REFERENCES discount.card (card_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT card_holder_card_rel_card_holder_id_fkey FOREIGN KEY (card_holder_id)
      REFERENCES discount.card_holder (card_holder_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_holder_card_rel OWNER TO pavlov;


-- DROP INDEX discount.discount_card_holder_card_rel_card_id_card_holder_id_date_end1_idx;

CREATE INDEX discount_card_holder_idx
  ON discount.card_holder_card_rel
  USING btree
  (card_id, card_holder_id, date_end);

