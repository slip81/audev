﻿/*
	CREATE TABLE discount.card_holder
*/

-- DROP TABLE discount.card_holder;

CREATE TABLE discount.card_holder
(
  card_holder_id bigint NOT NULL DEFAULT discount.id_generator(), -- PK
  card_holder_surname character varying, -- Фамилия
  card_holder_name character varying, -- Имя
  card_holder_fname character varying, -- Отчество
  date_birth date, -- Дата рождения
  phone_num character varying, -- Номер телефона
  address character varying, -- Адрес
  email character varying, -- Эл почта
  comment character varying, -- Комментарий
  business_id bigint NOT NULL, -- FK business
  tmp_card_id bigint,
  CONSTRAINT card_holder_pkey PRIMARY KEY (card_holder_id),
  CONSTRAINT card_holder_business_id_fkey FOREIGN KEY (business_id)
      REFERENCES discount.business (business_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_holder OWNER TO pavlov;

-- DROP INDEX discount.discount_client_business_id_client_surname1_idx;

CREATE INDEX discount_card_holder_business_id_card_holder_surname1_idx
  ON discount.card_holder
  USING btree
  (business_id, card_holder_surname COLLATE pg_catalog."default");

