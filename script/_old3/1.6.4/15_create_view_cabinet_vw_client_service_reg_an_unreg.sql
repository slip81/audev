﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service_reg_an_unreg
*/

-- DROP VIEW cabinet.vw_client_service_reg_an_unreg;

CREATE OR REPLACE VIEW cabinet.vw_client_service_reg_an_unreg AS 
	select 1::integer as is_registered, t1.client_id, t1.workplace_id, t1.service_id, t1.version_id, t1.service_name, t1.version_name
	from cabinet.vw_client_service_registered t1	
	union
	select 0::integer as is_registered, t1.client_id, null as workplace_id, t1.service_id, t1.version_id, t1.service_name, t1.version_name
	from cabinet.vw_client_service_unregistered t1;

ALTER TABLE cabinet.vw_client_service_reg_an_unreg OWNER TO pavlov;
