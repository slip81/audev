﻿/*
	CREATE TABLE esn.log_esn_type
*/

-- DROP TABLE esn.log_esn_type;

CREATE TABLE esn.log_esn_type
(
  type_id bigint NOT NULL,
  type_name character varying,
  CONSTRAINT log_esn_type_pkey PRIMARY KEY (type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.log_esn_type OWNER TO pavlov;
