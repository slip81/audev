﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service_reg_an_unreg
*/

-- DROP VIEW cabinet.vw_client_service_reg_an_unreg;

CREATE OR REPLACE VIEW cabinet.vw_client_service_reg_an_unreg AS 
 SELECT 1 AS is_registered,
    t1.client_id,
    t1.workplace_id,
    t1.service_id,
    t1.version_id,
    t1.service_name,
    t1.version_name,
    t1.activation_key
   FROM cabinet.vw_client_service_registered t1
UNION
 SELECT 0 AS is_registered,
    t1.client_id,
    NULL::integer AS workplace_id,
    t1.service_id,
    t1.version_id,
    t1.service_name,
    t1.version_name,
    NULL::character varying AS activation_key
   FROM cabinet.vw_client_service_unregistered t1;


ALTER TABLE cabinet.vw_client_service_reg_an_unreg OWNER TO pavlov;
