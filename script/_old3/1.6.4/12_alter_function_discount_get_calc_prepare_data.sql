﻿/*
	CREATE OR REPLACE FUNCTION discount.get_calc_prepare_data
*/

-- DROP FUNCTION discount.get_calc_prepare_data(bigint, bigint, character varying);

CREATE OR REPLACE FUNCTION discount.get_calc_prepare_data(
    IN _business_id bigint,
    IN _card_id bigint,
    IN _user_name character varying)
  RETURNS TABLE(card_id bigint, date_beg date, date_end date, curr_card_status_id bigint, business_id bigint, curr_trans_count bigint, card_num bigint, card_num2 character varying, curr_trans_sum numeric, curr_client_id bigint, curr_card_type_id bigint, warn character varying, source_card_id bigint, from_au boolean, card_status_group_id bigint, programm_id bigint, print_is_active boolean, print_sum_trans boolean, print_bonus_value_trans boolean, print_bonus_value_card boolean, print_bonus_percent_card boolean, print_disc_percent_card boolean, add_to_timezone integer, card_scan_only integer, scanner_equals_reader smallint, allow_card_add boolean, org_id bigint, bo_add_to_timezone integer, bo_card_scan_only integer, bo_scanner_equals_reader smallint, bo_allow_card_add integer) AS
$BODY$
BEGIN
	RETURN QUERY
		select
		t1.card_id,
		t1.date_beg,
		t1.date_end,
		t1.curr_card_status_id,
		t1.business_id,
		t1.curr_trans_count,
		t1.card_num,
		t1.card_num2,
		t1.curr_trans_sum,
		t1.curr_holder_id as curr_client_id,
		t1.curr_card_type_id,
		t1.warn,
		t1.source_card_id,
		t1.from_au,
		t3.card_status_group_id,
		t4.programm_id,
		t5.is_active as print_is_active,
		t5.print_sum_trans,
		t5.print_bonus_value_trans,
		t5.print_bonus_value_card,
		t5.print_bonus_percent_card,
		t5.print_disc_percent_card,		
		t2.add_to_timezone,
		t2.card_scan_only,
		t2.scanner_equals_reader,
		t2.allow_card_add, 
		t6.org_id,
		t6.add_to_timezone as bo_add_to_timezone,
		t6.card_scan_only as bo_card_scan_only,
		t6.scanner_equals_reader as bo_scanner_equals_reader,
		case when coalesce(t6.allow_card_add, false) = false then 0 else 1 end as bo_allow_card_add
		from discount.card t1
		inner join discount.business t2 on t1.business_id = t2.business_id
		inner join discount.card_status t3 on t1.curr_card_status_id = t3.card_status_id
		inner join discount.card_type_programm_rel t4 on t1.curr_card_type_id = t4.card_type_id and t4.date_end is null
		inner join discount.card_type_check_info t5 on t1.curr_card_type_id = t5.card_type_id
		left join discount.vw_business_org_user t6 on t6.business_id = _business_id and t6.user_name = _user_name
		where t1.card_id = _card_id and t1.business_id = _business_id
		;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION discount.get_calc_prepare_data(bigint, bigint, character varying) OWNER TO pavlov;
