﻿/*
	insert into esn.log_esn	
*/


insert into esn.log_esn_type (type_id, type_name)
select type_id, type_name from esn.log_event_type;

insert into esn.log_esn (date_beg, mess, user_name, log_esn_type_id, obj_id, scope, date_end, mess2)
select date_beg, mess, user_name, log_event_type_id, obj_id, scope, date_end, mess2 from esn.log_event;