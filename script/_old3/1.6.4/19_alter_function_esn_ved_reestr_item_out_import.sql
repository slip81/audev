﻿/*
	CREATE OR REPLACE FUNCTION esn.ved_reestr_item_out_import
*/

-- DROP FUNCTION esn.ved_reestr_item_out_import(bigint);

CREATE OR REPLACE FUNCTION esn.ved_reestr_item_out_import(IN _source_id bigint)
  RETURNS TABLE(_reestr_id bigint, _new_item_cnt integer, _out_item_cnt integer, _err_item_cnt integer) AS
$BODY$
DECLARE    
    tmp_item_cnt integer;
    tmp_new_item_cnt integer;    
    inserted_item_cnt integer;
    inserted_item_cnt2 integer;
    curr_reestr_id bigint;
    parent_reestr_id bigint;
    reestr_is_closed boolean;    
    new_reestr_id bigint;
    user_name character varying;
    now timestamp without time zone;
    today date;
    arow record;
    --
    col_id_MNN CONSTANT integer := 1;
    col_id_COMM_NAME CONSTANT integer := 2;
    col_id_PACK_NAME CONSTANT integer := 3;
    col_id_PRODUCER CONSTANT integer := 4;
    col_id_PACK_COUNT CONSTANT integer := 5;
    col_id_LIMIT_PRICE CONSTANT integer := 6;
    col_id_PRICE_FOR_INIT_PACK CONSTANT integer := 7;
    col_id_REG_NUM CONSTANT integer := 8;
    col_id_PRICE_REG_DATE CONSTANT integer := 9;
    col_id_EAN13 CONSTANT integer := 10;
    col_id_OUT_REAZON CONSTANT integer := 15;
    col_id_OUT_DATE CONSTANT integer := 16;
    --
    col_match_MNN boolean := false;
    col_match_COMM_NAME boolean := false;    
    col_match_PACK_NAME boolean := false;
    col_match_PRODUCER boolean := false;
    col_match_PACK_COUNT boolean := false;
    col_match_LIMIT_PRICE boolean := false;
    col_match_PRICE_FOR_INIT_PACK boolean := false;
    col_match_REG_NUM boolean := false;
    col_match_PRICE_REG_DATE boolean := false;
    col_match_EAN13 boolean := false;
    col_match_OUT_REAZON boolean := false;
    col_match_OUT_DATE boolean := false;    
BEGIN
	set search_path to 'esn';
	
	--_result := -1;
	-- return query select -1::bigint as _reestr_id, 0::integer as _new_item_cnt, 0::integer as _out_item_cnt, 0::integer as _err_item_cnt;
	
	now = current_timestamp;
	today = current_date;
	user_name = 'downloader-ved';	
	
	tmp_item_cnt := (select count(*) from tmp_ved_reestr_item_out);

	if (tmp_item_cnt <= 0) then
		return;
	end if;

	for arow in
		select t2.column_id, coalesce(t2.is_for_match, false) as is_for_match
		from ved_source t1
		inner join ved_template_column t2 on t1.out_template_id = t2.template_id and coalesce(t2.is_for_match, false) = true
		where t1.ved_source_id = _source_id
	loop
		case arow.column_id
			when col_id_MNN then
				col_match_MNN := arow.is_for_match;
			when col_id_COMM_NAME then
				col_match_COMM_NAME := arow.is_for_match;
			when col_id_PACK_NAME then
				col_match_PACK_NAME := arow.is_for_match;
			when col_id_PRODUCER then
				col_match_PRODUCER := arow.is_for_match;
			when col_id_PACK_COUNT then
				col_match_PACK_COUNT := arow.is_for_match;
			when col_id_LIMIT_PRICE then
				col_match_LIMIT_PRICE := arow.is_for_match;
			when col_id_PRICE_FOR_INIT_PACK then
				col_match_PRICE_FOR_INIT_PACK := arow.is_for_match;
			when col_id_REG_NUM then
				col_match_REG_NUM := arow.is_for_match;
			when col_id_PRICE_REG_DATE then
				col_match_PRICE_REG_DATE := arow.is_for_match;
			when col_id_EAN13 then
				col_match_EAN13 := arow.is_for_match;
			when col_id_OUT_REAZON then
				col_match_OUT_REAZON := arow.is_for_match;
			when col_id_OUT_DATE then
				col_match_OUT_DATE := arow.is_for_match;
			else
		end case;
	end loop;

	-- проставляем ссылки на существующие строки в ved_reestr_item и на существующие строки в ved_reestr_item_out
	update tmp_ved_reestr_item_out t1
	set existing_item_id = t2.item_id, existing_item_out_id = t4.out_id
	from vw_ved_reestr_item t2	
	inner join ved_reestr t3 on t2.reestr_id = t3.reestr_id and t3.is_closed = false
	left join ved_reestr_item_out t4 on t2.item_id = t4.reestr_item_id	
	where 1=1
	and (((col_match_MNN = true) and (trim(coalesce(t1.mnn, '')) = trim(coalesce(t2.mnn, '')))) OR (col_match_MNN = false))
	and (((col_match_COMM_NAME = true) and (trim(coalesce(t1.comm_name, '')) = trim(coalesce(t2.comm_name, '')))) OR (col_match_COMM_NAME = false))
	and (((col_match_PRODUCER = true) and (trim(coalesce(t1.producer, '')) = trim(coalesce(t2.producer, '')))) OR (col_match_PRODUCER = false))
	and (((col_match_PACK_NAME = true) and (trim(coalesce(t1.pack_name, '')) = trim(coalesce(t2.pack_name, '')))) OR (col_match_PACK_NAME = false))
	and (((col_match_PACK_COUNT = true) and (coalesce(t1.pack_count, 0) = coalesce(t2.pack_count, 0))) OR (col_match_PACK_COUNT = false))
	and (((col_match_LIMIT_PRICE = true) and (coalesce(t1.limit_price, 0) = coalesce(t2.limit_price, 0))) OR (col_match_LIMIT_PRICE = false))
	and (((col_match_PRICE_FOR_INIT_PACK = true) and (coalesce(t1.price_for_init_pack, false) = coalesce(t2.price_for_init_pack, false))) OR (col_match_PRICE_FOR_INIT_PACK = false))
	and (((col_match_REG_NUM = true) and (trim(coalesce(t1.reg_num, '')) = trim(coalesce(t2.reg_num, '')))) OR (col_match_REG_NUM = false))
	and (((col_match_PRICE_REG_DATE = true) and (trim(coalesce(t1.price_reg_date_full, '')) = trim(coalesce(t2.price_reg_date_full, '')))) OR (col_match_PRICE_REG_DATE = false))
	and (((col_match_EAN13 = true) and (trim(coalesce(t1.ean13, '')) = trim(coalesce(t2.ean13, '')))) OR (col_match_EAN13 = false))
	and (((col_match_OUT_REAZON = true) and (trim(coalesce(t1.out_reazon, '')) = trim(coalesce(t4.out_reazon, '')))) OR (col_match_OUT_REAZON = false))
	and (((col_match_OUT_DATE = true) and (t1.out_date = t4.out_date)) OR (col_match_OUT_DATE = false))
	;	

	-- если все новые строки есть в ved_reestr_item_out - выходим
	tmp_new_item_cnt := (select count(*) from tmp_ved_reestr_item_out where coalesce(existing_item_out_id, 0) <= 0);
	if (tmp_new_item_cnt <=0 ) then
		return;
	end if;	

	-- строки, которые есть в ved_reestr_item, но нет в ved_reestr_item_out - добавляем в ved_reestr_item_out
	with new_items as 
	(insert into ved_reestr_item_out (reestr_item_id, out_date, out_reazon, crt_date, crt_user)
	select existing_item_id, out_date, out_reazon, now, user_name
	from tmp_ved_reestr_item_out
	where coalesce(existing_item_id, 0) > 0
	and coalesce(existing_item_out_id, 0) <= 0
	returning 1
	)
	select count(*) into inserted_item_cnt from new_items;

	-- строки, которых нет в ved_reestr_item - добавляем в ved_reestr_item и в ved_reestr_item_out
	
	update tmp_ved_reestr_item_out t1
	set mnn_id = t2.mnn_id
	from prep_mnn t2
	where coalesce(t1.existing_item_id, 0) <= 0
	and trim(coalesce(t1.mnn, '')) = trim(coalesce(t2.name, ''))
	and trim(coalesce(t1.mnn, '')) != '';

	update tmp_ved_reestr_item_out t1
	set producer_id = t2.producer_id
	from prep_producer t2
	where coalesce(t1.existing_item_id, 0) <= 0
	and trim(coalesce(t1.producer, '')) = trim(coalesce(t2.name, ''))
	and trim(coalesce(t1.producer, '')) != '';

	update tmp_ved_reestr_item_out t1
	set comm_name_id = t2.commercial_name_id
	from prep_commercial_name t2
	where coalesce(t1.existing_item_id, 0) <= 0
	and trim(coalesce(t1.comm_name, '')) = trim(coalesce(t2.name, ''))
	and trim(coalesce(t1.comm_name, '')) != '';

	insert into prep_mnn(name, created_on, created_by)
	select distinct trim(coalesce(t1.mnn, '')), now, user_name
	from tmp_ved_reestr_item_out t1
	where coalesce(t1.existing_item_id, 0) <= 0
	and coalesce(t1.mnn_id, 0) <= 0
	and trim(coalesce(t1.mnn, '')) != '';

	insert into prep_producer(name, created_on, created_by)
	select distinct trim(coalesce(t1.producer, '')), now, user_name
	from tmp_ved_reestr_item_out t1
	where coalesce(t1.existing_item_id, 0) <= 0
	and coalesce(t1.producer_id, 0) <= 0
	and trim(coalesce(t1.producer, '')) != '';

	insert into prep_commercial_name(name, created_on, created_by)
	select distinct trim(coalesce(t1.comm_name, '')), now, user_name
	from tmp_ved_reestr_item_out t1
	where coalesce(t1.existing_item_id, 0) <= 0
	and coalesce(t1.comm_name_id, 0) <= 0
	and trim(coalesce(t1.comm_name, '')) != '';

	update tmp_ved_reestr_item_out t1
	set mnn_id = t2.mnn_id
	from prep_mnn t2
	where coalesce(t1.existing_item_id, 0) <= 0
	and trim(coalesce(t1.mnn, '')) = trim(coalesce(t2.name, ''))
	and trim(coalesce(t1.mnn, '')) != ''
	and coalesce(t1.mnn_id, 0) <= 0;

	update tmp_ved_reestr_item_out t1
	set producer_id = t2.producer_id
	from prep_producer t2
	where coalesce(t1.existing_item_id, 0) <= 0
	and trim(coalesce(t1.producer, '')) = trim(coalesce(t2.name, ''))
	and trim(coalesce(t1.producer, '')) != ''
	and coalesce(t1.producer_id, 0) <= 0;	

	update tmp_ved_reestr_item_out t1
	set comm_name_id = t2.commercial_name_id
	from prep_commercial_name t2
	where coalesce(t1.existing_item_id, 0) <= 0
	and trim(coalesce(t1.comm_name, '')) = trim(coalesce(t2.name, ''))
	and trim(coalesce(t1.comm_name, '')) != ''
	and coalesce(t1.comm_name_id, 0) <= 0;	

	curr_reestr_id := coalesce((select max(reestr_id) from ved_reestr where is_closed = false), 0);
	if (curr_reestr_id <= 0) then
		parent_reestr_id := null;
		insert into ved_reestr (reestr_name, date_beg, is_closed, source_id, crt_date, crt_user, parent_id)
		values ('Госреестр от ' || to_char(now, 'YYYY-MM-DD'), today, false, _source_id, now, user_name, parent_reestr_id)	
		returning reestr_id into new_reestr_id;		
	else				
		parent_reestr_id := (select coalesce(parent_id, reestr_id) from ved_reestr where reestr_id = curr_reestr_id);
		new_reestr_id := curr_reestr_id;
	end if;

	update ved_reestr_item set tmp_to_out = false, tmp_out_date = null, tmp_out_reazon = null;
		
	insert into ved_reestr_item (reestr_id, item_code2, mnn_id, comm_name_id, producer_id, pack_name, pack_count, limit_price, price_for_init_pack, reg_num, price_reg_date_full, price_reg_date, price_reg_ndoc, ean13, crt_date, crt_user, state, tmp_to_out, tmp_out_date, tmp_out_reazon)
	select 
	new_reestr_id, item_code2, mnn_id, comm_name_id, producer_id, pack_name, pack_count, limit_price, price_for_init_pack, reg_num, price_reg_date_full, price_reg_date, price_reg_ndoc, ean13, now, user_name, state, true, out_date, out_reazon
	from tmp_ved_reestr_item_out
	where coalesce(existing_item_id, 0) <= 0;

	with new_items2 as 
	(insert into ved_reestr_item_out (reestr_item_id, out_date, out_reazon, crt_date, crt_user)
	select item_id, tmp_out_date, tmp_out_reazon, now, user_name
	from ved_reestr_item
	where tmp_to_out = true
	returning 1
	)
	select count(*) into inserted_item_cnt2 from new_items2;

	insert into ved_reestr_column_match (reestr_id, region_id, column_id, is_out)
	select new_reestr_id, null, column_id, true
	from ved_source t1
	inner join ved_template_column t2 on t1.out_template_id = t2.template_id and coalesce(t2.is_for_match, false) = true
	where t1.ved_source_id = _source_id;		

	--_result := new_reestr_id;
	return query select new_reestr_id as _reestr_id, 0::integer as _new_item_cnt, inserted_item_cnt + inserted_item_cnt2 as _out_item_cnt, 0::integer as _err_item_cnt;
	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION esn.ved_reestr_item_out_import(bigint)
  OWNER TO pavlov;
