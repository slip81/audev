﻿/*
	ALTER TABLE esn.exchange_reg_task ADD COLUMN ds_id bigint
*/

ALTER TABLE esn.exchange_reg_task ADD COLUMN ds_id bigint;

ALTER TABLE esn.exchange_reg_task
  ADD CONSTRAINT exchange_reg_task_ds_id_fkey FOREIGN KEY (ds_id)
      REFERENCES esn.datasource (ds_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE esn.exchange_reg_task SET ds_id = (SELECT t1.ds_id FROM esn.exchange_reg_partner t1 WHERE esn.exchange_reg_task.reg_id = t1.reg_id)
WHERE coalesce(ds_id, 0) <= 0;

-- select * from esn.exchange_reg_task