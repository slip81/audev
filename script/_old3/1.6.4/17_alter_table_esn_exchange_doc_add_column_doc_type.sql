﻿/*
	ALTER TABLE esn.exchange_doc ADD COLUMN doc_type
*/

ALTER TABLE esn.exchange_doc ADD COLUMN doc_type integer NOT NULL DEFAULT 0;