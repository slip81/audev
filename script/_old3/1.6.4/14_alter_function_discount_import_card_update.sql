﻿/*
	CREATE OR REPLACE FUNCTION discount.import_card_update
*/

-- DROP FUNCTION discount.import_card_update(bigint, integer, integer);

CREATE OR REPLACE FUNCTION discount.import_card_update(
    IN _business_id bigint,
    IN _update_card_summ integer,
    IN _update_card_client integer,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
	SET search_path to 'discount';
	
	result := 0;    
    
	update import_discount_card
	set barcode = 0
	where barcode is null;

	update import_discount_card
	set barcode = 0
	where barcode = '_';

	delete from import_discount_card
	where barcode = '0';

	-- обновляем суммы по всем пришедшим картам
	if (coalesce(_update_card_summ, 0) != 0) then	

		update card
		set curr_trans_sum = curr_trans_sum + 
		(select max(x1.total_sum) from import_discount_card x1 where cast(x1.barcode as bigint) = card.card_num)
		where business_id = _business_id
		and card_num in (select cast(barcode as bigint) from import_discount_card);
	
	end if;

	-- добавляем клиентов только по тем картам, по которым в БД еще нет клиентов 
	-- и по которым в пришедших картах заполнено lname, fname или mname
	if (coalesce(_update_card_client, 0) != 0) then
	
		insert into card_holder (card_holder_surname, card_holder_name, card_holder_fname, tmp_card_id, business_id)
		select t1.lname, t1.fname, t1.mname, t2.card_id, t2.business_id
		from import_discount_card t1
		inner join card t2 on cast(t1.barcode as bigint) = t2.card_num
		where ((coalesce(t1.lname, '_') <> '_') or (coalesce(t1.fname, '_') <> '_') or (coalesce(t1.mname, '_') <> '_'))
		and t2.business_id = _business_id
		and not exists(select x1.card_id from card_holder_card_rel x1 where x1.card_id = t2.card_id);

		insert into card_holder_card_rel (card_holder_id, card_id, date_beg)
		select card_holder_id, tmp_card_id, current_date
		from card_holder
		where business_id = _business_id
		and tmp_card_id is not null
		and tmp_card_id in
		(select x1.card_id 
		from card x1 
		inner join import_discount_card x2 on cast(x2.barcode as bigint) = x1.card_num
		where x1.business_id = _business_id
		)
		;

		update card set curr_holder_id = 
		(select card_holder_card_rel.card_holder_id from card_holder_card_rel 
		where card_holder_card_rel.card_id = card.card_id
		order by card_holder_card_rel.date_beg desc
		limit 1)
		where business_id = _business_id
		and card_id in
		(select tmp_card_id from card_holder where tmp_card_id is not null and business_id = _business_id)
		and curr_holder_id is null;

		update card_holder set tmp_card_id = null where business_id = _business_id;	

	end if;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.import_card_update(bigint, integer, integer) OWNER TO pavlov;
