﻿/*
	CREATE TABLE esn.defect_client_allow
*/

-- DROP TABLE esn.defect_client_allow;

CREATE TABLE esn.defect_client_allow
(
  item_id serial NOT NULL,  
  client_id integer NOT NULL,
  CONSTRAINT defect_client_allow_pkey PRIMARY KEY (item_id),
  CONSTRAINT defect_client_allow_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

COMMENT ON TABLE esn.defect_client_allow IS 'Клиенты, которым разрешено скачивать брак напрямую';
COMMENT ON COLUMN esn.defect_client_allow.item_id IS 'PK';
COMMENT ON COLUMN esn.defect_client_allow.client_id IS 'Код клиента';

ALTER TABLE esn.defect_client_allow OWNER TO pavlov;
