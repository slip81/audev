﻿/*
	ALTER TABLE esn.story ADD COLUMN version_name
*/

ALTER TABLE esn.story ADD COLUMN version_name character varying;
COMMENT ON COLUMN esn.story.version_name IS 'Номер версии';