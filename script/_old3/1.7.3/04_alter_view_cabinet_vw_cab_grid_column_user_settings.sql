﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_cab_grid_column_user_settings
*/

-- DROP VIEW cabinet.vw_cab_grid_column_user_settings;

CREATE OR REPLACE VIEW cabinet.vw_cab_grid_column_user_settings AS 
 SELECT row_number() OVER (PARTITION BY t3.user_id, coalesce(t2.is_visible, true) ORDER BY coalesce(t2.column_num, 0)) - 1 AS column_index,
    t1.column_id as item_id,
    t1.column_id,
    t3.user_id,
    coalesce(t2.column_num, 0) as column_num,
    coalesce(t2.is_visible, true) as is_visible,
    coalesce(t2.width, 200) as width,
    coalesce(t2.is_filterable, true) as is_filterable,
    coalesce(t2.is_sortable, true) as is_sortable,
    t1.grid_id,
    t1.column_name,
    t1.column_name_rus,
    t1.format
   FROM cabinet.cab_grid_column t1
	LEFT JOIN cabinet.cab_user t3 ON 1=1
	LEFT JOIN cabinet.cab_grid_column_user_settings t2 ON t1.column_id = t2.column_id AND coalesce(t2.user_id, 0) = t3.user_id
	;

ALTER TABLE cabinet.vw_cab_grid_column_user_settings OWNER TO pavlov;
