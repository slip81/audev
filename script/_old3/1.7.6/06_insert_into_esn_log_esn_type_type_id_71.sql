﻿/*
	insert into esn.log_esn_type (type_id, type_name)
*/

insert into esn.log_esn_type (type_id, type_name)
select 71, 'Sender'
from esn.log_esn_type
where not exists(select type_id from esn.log_esn_type where type_id = 71)
limit 1;

-- select * from esn.log_esn_type order by type_id;