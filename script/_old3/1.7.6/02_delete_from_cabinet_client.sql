﻿/*
	delete from cabinet.client
*/

delete from cabinet.client_branches t1 using cabinet.client t2
where t1.client_id = t2.id
and not exists(select x1.client_id from cabinet.sales x1 where x1.client_id = t2.id);

delete from cabinet.client_services t1 using cabinet.client t2
where t1.client__id = t2.id
and not exists(select x1.client_id from cabinet.sales x1 where x1.client_id = t2.id);

delete from cabinet.license t1 using cabinet.order_item t2, cabinet.order t3, cabinet.client t4
where t1.order_item_id = t2.id and t2.order_id = t3.id and t3.client_id = t4.id
and not exists(select x1.client_id from cabinet.sales x1 where x1.client_id = t4.id);

delete from cabinet.order_item t1 using cabinet.order t2, cabinet.client t3
where t1.order_id = t2.id and t2.client_id = t3.id
and not exists(select x1.client_id from cabinet.sales x1 where x1.client_id = t3.id);

delete from cabinet.order t1 using cabinet.client t2
where t1.client_id = t2.id
and not exists(select x1.client_id from cabinet.sales x1 where x1.client_id = t2.id);

delete from cabinet.client_document t1 using cabinet.client t2
where t1.client_id = t2.id
and not exists(select x1.client_id from cabinet.sales x1 where x1.client_id = t2.id);

delete from cabinet.client_contract t1 using cabinet.client t2
where t1.client_id = t2.id
and not exists(select x1.client_id from cabinet.sales x1 where x1.client_id = t2.id);

delete from cabinet.client_product_groups t1 using cabinet.client t2
where t1.client__id = t2.id
and not exists(select x1.client_id from cabinet.sales x1 where x1.client_id = t2.id);

delete from cabinet.client where not exists(select t2.client_id from cabinet.sales t2 where t2.client_id = cabinet.client.id);
