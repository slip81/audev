﻿/*
	insert into cabinet.service
*/

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, branch_id, price, need_key, "NeedCheckVersionLicense", is_active, is_service)
values (57, '{28c0a0ab-c3fc-4676-a08a-904e838c9839}', 'Брак на почту', 'Отправка брака на почту', 12, 0, 'mail_defect', 0, 1, 0, 0, 0, true, true );

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, branch_id, price, need_key, "NeedCheckVersionLicense", is_active, is_service)
values (58, '{f1a7d24f-b76c-4805-a7ac-775f6d1a933c}', 'Госреестр на почту', 'Отправка госреестра на почту', 12, 0, 'mail_grls', 0, 1, 0, 0, 0, true, true );

-- select * from cabinet.service order by id desc


insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('{ddca5765-5419-4b4e-a1e6-3a24ac06082a}', '1.0.0.0', '1.0.0.0', 57, 0, 0);

insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('{e5e69cb0-e068-4ad2-9772-821516cd9d0c}', '1.0.0.0', '1.0.0.0', 58, 0, 0);

-- select * from cabinet.version order by service_id desc, id desc