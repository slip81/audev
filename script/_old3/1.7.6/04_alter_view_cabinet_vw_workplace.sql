﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_workplace
*/

-- DROP VIEW cabinet.vw_workplace;

CREATE OR REPLACE VIEW cabinet.vw_workplace AS 
 SELECT t1.id AS workplace_id,
    t1.registration_key,
    t1.description,
    t1.sales_id,
    t2.name AS sales_name,
    t2.adress AS sales_address,
    t2.client_id,
    t3.client_name,
    t4.name AS user_name
   FROM cabinet.workplace t1
     JOIN cabinet.sales t2 ON t1.sales_id = t2.id
     JOIN cabinet.allclients t3 ON t2.client_id = t3.id
     LEFT JOIN cabinet.my_aspnet_users t4 ON t2.user_id = t4.id
     WHERE t1.is_deleted = 0
     ;

ALTER TABLE cabinet.vw_workplace OWNER TO pavlov;
