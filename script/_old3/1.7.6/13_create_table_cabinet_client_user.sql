﻿/*
	CREATE TABLE cabinet.client_user
*/

-- DROP TABLE cabinet.client_user;

CREATE TABLE cabinet.client_user
(
  user_id integer NOT NULL,
  client_id integer NOT NULL,
  sales_id integer,
  user_name character varying,
  user_name_full character varying,
  CONSTRAINT client_user_pkey PRIMARY KEY (user_id),
  CONSTRAINT client_user_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT client_user_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT client_user_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.my_aspnet_users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.client_user OWNER TO pavlov;
