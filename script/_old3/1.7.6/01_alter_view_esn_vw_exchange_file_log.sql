﻿/*
	CREATE OR REPLACE VIEW esn.vw_exchange_file_log
*/

-- backup old version

-- DROP VIEW esn.vw_exchange_file_log_old;

CREATE OR REPLACE VIEW esn.vw_exchange_file_log_old AS 
 SELECT row_number() OVER (ORDER BY t1.node_id) AS id,
    t1.node_id,
    t1.partner_id,
    t1.client_id,
    t1.sales_id,
    t1.partner_name,
    t1.client_name,
    t1.sales_address,
    t1.node_name,
    t2.exchange_item_id,
    t3.item_name,
    t11.filename AS filename_last,
    t11.period AS period_last,
    t12.filename AS filename_prev,
    t12.period AS period_prev,
    'now'::text::date - t11.period::date AS days_cnt
   FROM esn.vw_exchange_file_node t1
     JOIN esn.exchange_file_node_item t2 ON t1.node_id = t2.node_id
     JOIN esn.exchange_item t3 ON t2.exchange_item_id = t3.item_id
     LEFT JOIN ( SELECT max(exchange_file_log.log_num) AS log_num,
            max(exchange_file_log.node::text) AS node,
            max(exchange_file_log.filename::text) AS filename,
            max(exchange_file_log.period::text) AS period,
            exchange_file_log.exchange_item_id
           FROM esn.exchange_file_log
          GROUP BY exchange_file_log.node, exchange_file_log.exchange_item_id) t11 ON btrim(t1.node_name::text) = btrim(t11.node) AND t2.exchange_item_id = t11.exchange_item_id
     LEFT JOIN ( SELECT max(exchange_file_log.log_num) AS log_num,
            max(exchange_file_log.node::text) AS node,
            max(exchange_file_log.filename::text) AS filename,
            max(exchange_file_log.period::text) AS period,
            exchange_file_log.exchange_item_id
           FROM esn.exchange_file_log
          WHERE NOT (exchange_file_log.log_id IN ( SELECT max(exchange_file_log_1.log_id) AS max
                   FROM esn.exchange_file_log exchange_file_log_1
                  GROUP BY exchange_file_log_1.node, exchange_file_log_1.exchange_item_id))
          GROUP BY exchange_file_log.node, exchange_file_log.exchange_item_id) t12 ON btrim(t1.node_name::text) = btrim(t12.node) AND t2.exchange_item_id = t12.exchange_item_id;

ALTER TABLE esn.vw_exchange_file_log_old OWNER TO pavlov;

-- create new version

DROP VIEW esn.vw_exchange_file_log;

CREATE OR REPLACE VIEW esn.vw_exchange_file_log AS 
 SELECT row_number() OVER (ORDER BY t1.node_id) AS id,
    t1.node_id,
    t1.partner_id,
    t1.client_id,
    t1.sales_id,
    t1.partner_name,
    t1.client_name,
    t1.sales_address,
    t1.node_name,
    t2.exchange_item_id,
    t3.item_name,
    t11.filename_last,
    t11.period_last,
    t11.filename_prev,
    t11.period_prev,    
    'now'::text::date - t11.period_last::date AS days_cnt
    FROM esn.vw_exchange_file_node t1
	INNER JOIN esn.exchange_file_node_item t2 ON t1.node_id = t2.node_id
	INNER JOIN esn.exchange_item t3 ON t2.exchange_item_id = t3.item_id
	LEFT JOIN
	(
		SELECT split_part(nodeexchange_name, '--**--', 1) as node, split_part(nodeexchange_name, '--**--', 2)::int as exchange_item_id, 
		split_part(fileperiod_last, '--**--', 1) as filename_last, split_part(fileperiod_last, '--**--', 2)::timestamp without time zone as period_last,
		split_part(fileperiod_prev, '--**--', 1) as filename_prev, split_part(fileperiod_prev, '--**--', 2)::timestamp without time zone as period_prev
		FROM esn.crosstab('select (t1.node || ''--**--'' || t1.exchange_item_id::text)::text, t1.log_num::text, (t1.filename || ''--**--'' || t1.period)::text
		from esn.exchange_file_log t1
		where coalesce(t1.exchange_item_id, 0) > 0
		order by t1.node, t1.exchange_item_id, t1.log_num desc
	') AS ct(nodeexchange_name text, fileperiod_last text, fileperiod_prev text)
	) t11 ON trim(coalesce(t1.node_name, '')) = trim(coalesce(t11.node, '-')) AND t2.exchange_item_id = t11.exchange_item_id 
	;

ALTER TABLE esn.vw_exchange_file_log OWNER TO pavlov;
