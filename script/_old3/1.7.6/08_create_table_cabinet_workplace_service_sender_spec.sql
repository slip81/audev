﻿/*
	CREATE TABLE cabinet.workplace_service_sender_spec
*/

-- DROP TABLE cabinet.workplace_service_sender_spec;

CREATE TABLE cabinet.workplace_service_sender_spec
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  is_disabled boolean NOT NULL DEFAULT false,
  date_beg date,
  date_end date,
  interval_in_days integer NOT NULL DEFAULT 1,
  email character varying,
  CONSTRAINT workplace_service_sender_spec_pkey PRIMARY KEY (item_id),
  CONSTRAINT workplace_service_sender_spec_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT workplace_service_sender_spec_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.workplace_service_sender_spec OWNER TO pavlov;
