﻿/*
	update cabinet.service set is_deleted = 1
*/

update cabinet.service set is_deleted = 1
where id in (6, 7, 14, 20, 21, 23, 26, 29, 30, 31, 32, 33, 36, 37, 38, 39, 40, 42, 54, 55)
;

-- select * from cabinet.service where is_deleted = 0 and parent_id is not null order by id