﻿/*
	CREATE TABLE cabinet.workplace_service_sender_log_spec
*/

-- DROP TABLE cabinet.workplace_service_sender_log_spec;

CREATE TABLE cabinet.workplace_service_sender_log_spec
(
  log_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  date_beg timestamp without time zone,
  mess character varying,
  CONSTRAINT workplace_service_sender_log_spec_pkey PRIMARY KEY (log_id),
  CONSTRAINT workplace_service_sender_log_spec_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT workplace_service_sender_log_spec_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cabinet.workplace_service_sender_log_spec  OWNER TO pavlov;
