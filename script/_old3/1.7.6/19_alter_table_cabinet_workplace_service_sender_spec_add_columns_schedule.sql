﻿/*
	ALTER TABLE cabinet.workplace_service_sender_spec ADD COLUMN schedule_weekly, schedule_monthly, schedule_type
*/

ALTER TABLE cabinet.workplace_service_sender_spec ADD COLUMN schedule_weekly character varying;
ALTER TABLE cabinet.workplace_service_sender_spec ADD COLUMN schedule_monthly character varying;
ALTER TABLE cabinet.workplace_service_sender_spec ADD COLUMN schedule_type int NOT NULL DEFAULT 1;