﻿/*
	ALTER TABLE cabinet.service DROP COLUMN is_active, ADD COLUMN have_spec
*/

ALTER TABLE cabinet.service DROP COLUMN is_active;
ALTER TABLE cabinet.service ADD COLUMN have_spec boolean NOT NULL DEFAULT false;


UPDATE cabinet.service SET have_spec = true WHERE id in (57, 58);

-- select * from cabinet.service order by id desc