﻿/*
	CREATE TABLE cabinet.storage_link
*/

-- DROP TABLE cabinet.storage_link;

CREATE TABLE cabinet.storage_link
(
  link_id integer NOT NULL DEFAULT nextval('cabinet.storage_folder_folder_id_seq'::regclass),
  link_name character varying,  
  folder_id integer NOT NULL,    
  download_link character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT storage_link_pkey PRIMARY KEY (link_id),
  CONSTRAINT storage_file_folder_id_fkey FOREIGN KEY (folder_id)
      REFERENCES cabinet.storage_folder (folder_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.storage_link  OWNER TO pavlov;
