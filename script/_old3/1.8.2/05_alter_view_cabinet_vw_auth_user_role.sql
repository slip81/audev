﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_auth_user_role
*/

-- DROP VIEW cabinet.vw_auth_user_role;

CREATE OR REPLACE VIEW cabinet.vw_auth_user_role AS 
 SELECT t1.user_id,
    t1.user_name,
    t1.user_name_full,
    t1.client_id,
    t3.role_id,
    t3.role_name,
    t4.group_id
   FROM cabinet.client_user t1
     LEFT JOIN cabinet.auth_user_role t2 ON t1.user_id = t2.user_id
     LEFT JOIN cabinet.auth_role t3 ON t2.role_id = t3.role_id
     LEFT JOIN cabinet.auth_role_group t4 ON t3.role_id = t4.role_id
     AND ((t4.group_id = 1 AND t1.client_id = 1000) OR (t4.group_id = 2 AND t1.client_id != 1000))
     LEFT JOIN cabinet.my_aspnet_membership t5 ON t1.user_id = t5."userId"
  WHERE COALESCE(t5."IsApproved"::integer, 1) = 1;

ALTER TABLE cabinet.vw_auth_user_role OWNER TO pavlov;
