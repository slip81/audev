﻿/*
	insert into cabinet.auth_role_perm
*/

insert into cabinet.auth_section (section_id, section_name, group_id)
values (12, 'Хранилище', 1);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (31, 'Папки', 12, 1);

insert into cabinet.auth_role_perm (role_id, part_id, allow_view, allow_edit)
select role_id, 31, true, true
from cabinet.auth_role;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (32, 'Файлы', 12, 1);

insert into cabinet.auth_role_perm (role_id, part_id, allow_view, allow_edit)
select role_id, 32, true, true
from cabinet.auth_role;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (33, 'История', 12, 1);

insert into cabinet.auth_role_perm (role_id, part_id, allow_view, allow_edit)
select role_id, 33, true, true
from cabinet.auth_role;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (34, 'Настройки', 12, 1);

insert into cabinet.auth_role_perm (role_id, part_id, allow_view, allow_edit)
select role_id, 34, true, true
from cabinet.auth_role;

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (35, 'Типы файлов', 12, 1);

insert into cabinet.auth_role_perm (role_id, part_id, allow_view, allow_edit)
select role_id, 35, true, true
from cabinet.auth_role;

/*
select * from cabinet.auth_section order by section_id
select * from cabinet.auth_section_part order by section_id, part_id
select * from cabinet.auth_role
select * from cabinet.auth_role_group
select * from cabinet.auth_role_perm where
*/
