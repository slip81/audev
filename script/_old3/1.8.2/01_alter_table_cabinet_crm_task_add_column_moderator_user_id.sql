﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN moderator_user_id
*/

ALTER TABLE cabinet.crm_task ADD COLUMN moderator_user_id integer NOT NULL DEFAULT 0;

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_moderator_user_id_fkey FOREIGN KEY (moderator_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;