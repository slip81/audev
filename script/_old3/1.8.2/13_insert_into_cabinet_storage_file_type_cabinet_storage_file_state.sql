﻿/*
	insert into cabinet.storage_file_type_group, insert into cabinet.storage_file_type, insert into cabinet.storage_file_state
*/


insert into cabinet.storage_param (storage_id, descr, max_file_size)
values (1, 'Хранилище', '20971520')
;

/*
'Документ Word'
'Документ Pdf'
'Документ Excel'
'Документ PowerPoint'
'Документ XML'
'Документ текстовый'
'Картинка'
'Видео'
'Аудио'
'SQL запрос'
'Архив'
*/

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (1, 'Документ Word', 'word')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (2, 'Документ Pdf', 'pdf')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (3, 'Документ Excel', 'excel')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (4, 'Документ PowerPoint', 'ppt')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (5, 'Документ XML', 'xml')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (6, 'Документ текстовый', 'text')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (7, 'Картинка', 'image')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (8, 'Видео', 'film')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (9, 'Аудио', 'music')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (10, 'SQL запрос', 'sql')
;

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (11, 'Архив', 'arc')
;

/* --------------------- */

/*
'Документ Word'
'Документ Pdf'
'Документ Excel'
'Документ PowerPoint'
'Документ XML'
'Документ текстовый'
'Картинка'
'Видео'
'Аудио'
'SQL запрос'
'Архив'
*/

/*
            ".doc",
            ".docx",
            ".txt",
            ".xml",
            ".sql",
            ".xls",
            ".xlsx",
            ".7z",
            ".zip",
            ".rar",
            ".arj",
            ".pdf",
            ".jpg", 
            ".jpeg",
            ".png",            
            ".bmp",
            ".avi",
            ".mpeg",
            ".mpg",
            ".divx",
            ".mkv",
            ".mp3",
            ".wav",
*/

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (1, 'doc', 1);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (2, 'docx', 1);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (3, 'pdf', 2);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (4, 'xls', 3);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (5, 'xlsx', 3);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (6, 'ppt', 4);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (7, 'xml', 5);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (8, 'txt', 6);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (9, 'jpg', 7);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (10, 'jpeg', 7);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (11, 'png', 7);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (12, 'bmp', 7);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (13, 'avi', 8);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (14, 'mpeg', 8);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (15, 'mpg', 8);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (16, 'divx', 8);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (17, 'mkv', 8);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (18, 'mp3', 9);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (19, 'wav', 9);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (20, 'sql', 10);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (21, '7z', 11);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (22, 'zip', 11);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (23, 'rar', 11);

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (24, 'arj', 11);

/* --------------------- */

insert into cabinet.storage_file_state (file_state_id, file_state_name)
values (1, 'Доступен');

insert into cabinet.storage_file_state (file_state_id, file_state_name)
values (2, 'Загружается');

insert into cabinet.storage_file_state (file_state_id, file_state_name)
values (3, 'Блокировван');

insert into cabinet.storage_file_state (file_state_id, file_state_name)
values (4, 'Недоступен');