﻿/*
	insert into cabinet.storage_file_type_group, insert into cabinet.storage_file_type, insert into cabinet.storage_file_state
*/

insert into cabinet.storage_file_type_group (group_id, group_name, img)
values (0, 'не определено', 'url');

insert into cabinet.storage_file_type (file_type_id, file_type_name, group_id)
values (0, 'не определено', 0);

insert into cabinet.storage_file_state (file_state_id, file_state_name)
values (0, 'не определено');