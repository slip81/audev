﻿/*
	delete from cabinet.auth_section_part where part_id = 35
*/

delete from cabinet.auth_role_perm where part_id = 35;
delete from cabinet.auth_user_perm where part_id = 35;
delete from cabinet.auth_section_part where part_id = 35;


/*
select * from cabinet.auth_section order by section_id
select * from cabinet.auth_section_part order by section_id, part_id
*/