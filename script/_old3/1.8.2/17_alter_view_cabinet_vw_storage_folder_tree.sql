﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_storage_folder_tree
*/

-- DROP VIEW cabinet.vw_storage_folder_tree;

CREATE OR REPLACE VIEW cabinet.vw_storage_folder_tree AS 
with recursive storage_folder_cte as
(
	select t1.folder_id, null::int as parent_id, t1.folder_name, null::character varying as parent_name, t1.folder_name as full_path
	from cabinet.storage_folder t1
	where t1.folder_level = 0
	union
	select t1.folder_id, t2.folder_id, t1.folder_name, t2.folder_name, t2.full_path || '\' || t1.folder_name as full_path
	from cabinet.storage_folder t1
	inner join storage_folder_cte t2 on t1.parent_id = t2.folder_id
)
select * from storage_folder_cte;

ALTER TABLE cabinet.vw_storage_folder_tree OWNER TO pavlov;
