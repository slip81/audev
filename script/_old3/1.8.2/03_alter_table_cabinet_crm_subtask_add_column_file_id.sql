﻿/*
	ALTER TABLE cabinet.crm_subtask ADD COLUMN file_id integer
*/

ALTER TABLE cabinet.crm_subtask ADD COLUMN file_id integer;

ALTER TABLE cabinet.crm_subtask
  ADD CONSTRAINT crm_subtask_file_id_fkey FOREIGN KEY (file_id)
      REFERENCES cabinet.crm_task_file (file_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

