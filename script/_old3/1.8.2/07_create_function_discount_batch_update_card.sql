﻿/*
	CREATE OR REPLACE FUNCTION discount.batch_update_card
*/

-- DROP FUNCTION discount.batch_update_card(bigint);

CREATE OR REPLACE FUNCTION discount.batch_update_card(
    IN _batch_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
  _trans_sum_add boolean;
  _trans_sum_add_value numeric;
  _trans_sum_add_mode integer;
  _disc_percent_add boolean;
  _disc_percent_add_value numeric;
  _disc_percent_add_mode integer;
  _bonus_percent_add boolean;
  _bonus_percent_add_value numeric;
  _bonus_percent_add_mode integer;
  _bonus_add boolean;
  _bonus_add_value numeric;
  _bonus_add_mode integer;
  _mess_add boolean;
  _mess_add_value character varying;
  _mess_add_mode integer;  
  _del_cards boolean;
  _change_card_type boolean;
  _change_card_status boolean;  
  _trans_num integer;
  _unit_type integer;
  _curr_card_type_id bigint;
  _curr_card_status_id bigint;
  _card_num bigint;
  _prev_unit_value numeric;
  _prev_card_nominal_id bigint;
  _prev_mess character varying;
  _new_mess character varying;
  _new_unit_value numeric;  
  _new_card_type_id bigint;
  _new_card_status_id bigint;  
  _card_count integer;
  _card_id bigint;
  _user_name character varying;
  _error_cnt integer;
BEGIN
    SET search_path to 'discount';
    
    result := 0;     
	
    SELECT trans_sum_add, trans_sum_add_value, trans_sum_add_mode, disc_percent_add, disc_percent_add_value, disc_percent_add_mode, 
	bonus_percent_add, bonus_percent_add_value, bonus_percent_add_mode, bonus_add, bonus_add_value, bonus_add_mode, 
	mess_add, mess_add_value, mess_add_mode, change_card_status, card_status_id, change_card_type, card_type_id, del_cards,
	crt_user 
    INTO _trans_sum_add, _trans_sum_add_value, _trans_sum_add_mode, _disc_percent_add, _disc_percent_add_value, _disc_percent_add_mode, 
	_bonus_percent_add, _bonus_percent_add_value, _bonus_percent_add_mode, _bonus_add, _bonus_add_value, _bonus_add_mode,
	_mess_add, _mess_add_value, _mess_add_mode, _change_card_status, _new_card_status_id, _change_card_type, _new_card_type_id, _del_cards,
	_user_name		   
    FROM batch 
    WHERE batch_id = _batch_id;		

    IF (_del_cards = true) THEN

	DELETE FROM card_card_status_rel t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;

	DELETE FROM card_card_type_rel t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;	

	DELETE FROM card_nominal t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;

	DELETE FROM card_holder_card_rel t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;

	DELETE FROM card_transaction_detail t1 USING batch_card t2, card_transaction t3
	WHERE t1.card_trans_id = t3.card_trans_id
	AND t3.card_id = t2.card_id
	AND t2.batch_id = _batch_id;

	DELETE FROM card_transaction_unit t1 USING batch_card t2, card_transaction t3
	WHERE t1.card_trans_id = t3.card_trans_id
	AND t3.card_id = t2.card_id
	AND t2.batch_id = _batch_id;	

	DELETE FROM card_transaction t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;

	DELETE FROM card_additional_num t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;

	DELETE FROM card_ext1 t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;	

	DELETE FROM card_nominal_inactive t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;	

	DELETE FROM card_nominal_uncommitted t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;		

	DELETE FROM card t1 USING batch_card t2
	WHERE t1.card_id = t2.card_id
	AND t2.batch_id = _batch_id;	
		
    ELSE

	    FOR _card_id IN SELECT card_id FROM batch_card WHERE batch_id = _batch_id
	    LOOP
		SELECT curr_card_type_id, curr_card_status_id, card_num, mess INTO _curr_card_type_id, _curr_card_status_id, _card_num, _prev_mess FROM card WHERE card_id = _card_id;

	    
		IF (_trans_sum_add = true) THEN
			IF (_trans_sum_add_mode = 1) THEN
				UPDATE card SET curr_trans_sum = COALESCE(_trans_sum_add_value, 0) WHERE card_id = _card_id;
			ELSE
				UPDATE card SET curr_trans_sum = COALESCE(curr_trans_sum, 0) + COALESCE(_trans_sum_add_value, 0) WHERE card_id = _card_id;
			END IF;

			SELECT trans_num INTO _trans_num FROM card_transaction WHERE card_id = _card_id ORDER BY card_trans_id DESC LIMIT 1;

			INSERT INTO card_transaction (card_id, date_beg, date_check, status, trans_kind, trans_num, user_name, batch_id)
			VALUES (_card_id, clock_timestamp(), clock_timestamp(), 0, 3, COALESCE(_trans_num, 0) + 1, _user_name, _batch_id);		
		END IF;
		IF (_disc_percent_add = true) THEN
			SELECT unit_type INTO _unit_type FROM card_type_unit WHERE card_type_id = _curr_card_type_id AND unit_type = 2;

			IF COALESCE(_unit_type, 0) <= 0 THEN
				INSERT INTO batch_error (batch_id, card_num, mess) 
				VALUES (_batch_id, _card_num, 'Нет карточной единицы Процент скидки на этой карте');
			ELSE

				SELECT unit_value, card_nominal_id INTO _prev_unit_value, _prev_card_nominal_id FROM card_nominal WHERE card_id = _card_id AND unit_type = _unit_type AND date_end IS NULL ORDER BY date_beg desc LIMIT 1;
				IF COALESCE(_prev_card_nominal_id, 0) > 0 THEN
					UPDATE card_nominal SET date_end = current_date WHERE card_nominal_id = _prev_card_nominal_id;
				END IF;
				
				IF (_disc_percent_add_mode = 1) THEN
					_new_unit_value = _disc_percent_add_value;
				ELSE
					_new_unit_value = COALESCE(_prev_unit_value, 0) + COALESCE(_disc_percent_add_value, 0);
				END IF;		

				INSERT INTO card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user, batch_id)
				VALUES (_card_id, current_date, _unit_type, _new_unit_value, clock_timestamp(), _user_name, _batch_id);
			END IF;
		END IF;	
		IF (_bonus_percent_add = true) THEN
			SELECT unit_type INTO _unit_type FROM card_type_unit WHERE card_type_id = _curr_card_type_id AND unit_type = 4;

			IF COALESCE(_unit_type, 0) <= 0 THEN
				INSERT INTO batch_error (batch_id, card_num, mess) 
				VALUES (_batch_id, _card_num, 'Нет карточной единицы Бонусный процент на этой карте');
			ELSE

				SELECT unit_value, card_nominal_id INTO _prev_unit_value, _prev_card_nominal_id FROM card_nominal WHERE card_id = _card_id AND unit_type = _unit_type AND date_end IS NULL ORDER BY date_beg desc LIMIT 1;
				IF COALESCE(_prev_card_nominal_id, 0) > 0 THEN
					UPDATE card_nominal SET date_end = current_date WHERE card_nominal_id = _prev_card_nominal_id;
				END IF;
				
				IF (_bonus_percent_add_mode = 1) THEN
					_new_unit_value = _bonus_percent_add_value;
				ELSE
					_new_unit_value = COALESCE(_prev_unit_value, 0) + COALESCE(_bonus_percent_add_value, 0);
				END IF;		

				INSERT INTO card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user, batch_id)
				VALUES (_card_id, current_date, _unit_type, _new_unit_value, clock_timestamp(), _user_name, _batch_id);
			END IF;
		END IF;		
		IF (_bonus_add = true) THEN
			SELECT unit_type INTO _unit_type FROM card_type_unit WHERE card_type_id = _curr_card_type_id AND unit_type = 1;

			IF COALESCE(_unit_type, 0) <= 0 THEN
				INSERT INTO batch_error (batch_id, card_num, mess) 
				VALUES (_batch_id, _card_num, 'Нет карточной единицы Бонусы на этой карте');
			ELSE

				SELECT unit_value, card_nominal_id INTO _prev_unit_value, _prev_card_nominal_id FROM card_nominal WHERE card_id = _card_id AND unit_type = _unit_type AND date_end IS NULL ORDER BY date_beg desc LIMIT 1;
				IF COALESCE(_prev_card_nominal_id, 0) > 0 THEN
					UPDATE card_nominal SET date_end = current_date WHERE card_nominal_id = _prev_card_nominal_id;
				END IF;
				
				IF (_bonus_add_mode = 1) THEN
					_new_unit_value = _bonus_add_value;
				ELSE
					_new_unit_value = COALESCE(_prev_unit_value, 0) + COALESCE(_bonus_add_value, 0);
				END IF;		

				INSERT INTO card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user, batch_id)
				VALUES (_card_id, current_date, _unit_type, _new_unit_value, clock_timestamp(), _user_name, _batch_id);
			END IF;
		END IF;	
		IF (_mess_add = true) THEN
			IF (_mess_add_mode = 1) THEN
				_new_mess = _mess_add_value;
			ELSE
				_new_mess = COALESCE(_prev_mess, '') || ' ' || COALESCE(_mess_add_value, '');
			END IF;		

			UPDATE card SET mess = _new_mess WHERE card_id = _card_id;
		END IF;	
		IF (_change_card_status = true) THEN
			IF COALESCE(_curr_card_status_id, 0) = COALESCE(_new_card_status_id, -1) THEN
				INSERT INTO batch_error (batch_id, card_num, mess) 
				VALUES (_batch_id, _card_num, 'Данный статус уже установлен на этой карте');
			ELSE
		
				UPDATE card SET curr_card_status_id = _new_card_status_id WHERE card_id = _card_id;

				UPDATE card_card_status_rel
				SET date_end = current_date
				WHERE card_id = _card_id AND date_end IS NULL;
			
				INSERT INTO card_card_status_rel (card_id, card_status_id, date_beg)
				VALUES (_card_id, _new_card_status_id, current_date);
			END IF;
		END IF;	
		IF (_change_card_type = true) THEN
			IF COALESCE(_curr_card_type_id, 0) = COALESCE(_new_card_type_id, -1) THEN
				INSERT INTO batch_error (batch_id, card_num, mess) 
				VALUES (_batch_id, _card_num, 'Данный тип уже установлен на этой карте');
			ELSE
		
				UPDATE card SET curr_card_type_id = _new_card_type_id WHERE card_id = _card_id;

				UPDATE card_card_type_rel
				SET date_end = current_date
				WHERE card_id = _card_id AND date_end IS NULL;
			
				INSERT INTO card_card_type_rel (card_id, card_type_id, date_beg, ord)
				VALUES (_card_id, _new_card_type_id, current_date, 1);
			END IF;
		END IF;

	    END LOOP;    

    END IF;
    

    SELECT count(*) INTO _error_cnt FROM batch_error WHERE batch_id = _batch_id; 
    SELECT count(*) INTO _card_count FROM batch_card WHERE batch_id = _batch_id; 
    
    UPDATE batch 
    SET state = 2, mess = 'Операция завершена', state_date = clock_timestamp(), state_user = _user_name,
    record_cnt = _card_count, processed_cnt = _card_count, error_cnt = _error_cnt 
    WHERE batch_id = _batch_id;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.batch_update_card(bigint) OWNER TO pavlov;
