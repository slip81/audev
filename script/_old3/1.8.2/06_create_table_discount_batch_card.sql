﻿/*
	CREATE TABLE discount.batch_card
*/

-- DROP TABLE discount.batch_card;

CREATE TABLE discount.batch_card
(
  item_id serial NOT NULL,
  batch_id bigint NOT NULL,
  card_id bigint NOT NULL,
  CONSTRAINT batch_card_pkey PRIMARY KEY (item_id),
  CONSTRAINT batch_card_batch_id_fkey FOREIGN KEY (batch_id)
      REFERENCES discount.batch (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);


ALTER TABLE discount.batch_card OWNER TO pavlov;
