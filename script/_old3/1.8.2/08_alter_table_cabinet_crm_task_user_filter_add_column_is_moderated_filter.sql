﻿/*
	ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN is_moderated_filter
*/


ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN is_moderated_filter boolean NOT NULL DEFAULT false;
