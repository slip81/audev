﻿/*
	ALTER TABLE discount.batch ADD COLUMN mess_add
*/

ALTER TABLE discount.batch ADD COLUMN mess_add boolean NOT NULL DEFAULT false;
ALTER TABLE discount.batch ADD COLUMN mess_add_value character varying;
ALTER TABLE discount.batch ADD COLUMN mess_add_mode integer NOT NULL DEFAULT 0;
