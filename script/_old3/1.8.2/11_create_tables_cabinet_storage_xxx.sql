﻿/*
	CREATE TABLEs cabinet.storage_xxx
*/

/* ------------------------------- */

-- DROP TABLE cabinet.storage_param;

CREATE TABLE cabinet.storage_param
(
  storage_id integer NOT NULL,
  descr character varying,
  max_file_size character varying,
  max_folder_file_count integer,
  CONSTRAINT storage_param_pkey PRIMARY KEY (storage_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.storage_param OWNER TO pavlov;

/* ------------------------------- */

-- DROP TABLE cabinet.storage_folder;

CREATE TABLE cabinet.storage_folder
(
  /*folder_id integer NOT NULL DEFAULT nextval('cabinet.storage_folder_folder_id_seq'::regclass),*/
  folder_id serial NOT NULL,
  folder_name character varying,
  parent_id integer,
  folder_level integer NOT NULL DEFAULT 0,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT storage_folder_pkey PRIMARY KEY (folder_id),
  CONSTRAINT storage_folder_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES cabinet.storage_folder (folder_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.storage_folder OWNER TO pavlov;


/* ------------------------------- */

CREATE TABLE cabinet.storage_file_type_group
(
  group_id serial NOT NULL,
  group_name character varying,
  img character varying,
  CONSTRAINT storage_file_type_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cabinet.storage_file_type_group OWNER TO pavlov;
  

/* ------------------------------- */

-- DROP TABLE cabinet.storage_file_type;

CREATE TABLE cabinet.storage_file_type
(
  file_type_id integer NOT NULL,
  file_type_name character varying,
  group_id integer NOT NULL,
  CONSTRAINT storage_file_type_pkey PRIMARY KEY (file_type_id),
  CONSTRAINT storage_file_type_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.storage_file_type_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.storage_file_type OWNER TO pavlov;


/* ------------------------------- */

-- DROP TABLE cabinet.storage_file_state;

CREATE TABLE cabinet.storage_file_state
(
  file_state_id serial NOT NULL,
  file_state_name character varying,
  CONSTRAINT storage_file_state_pkey PRIMARY KEY (file_state_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.storage_file_state OWNER TO pavlov;


/* ------------------------------- */

-- DROP TABLE cabinet.storage_file;

CREATE TABLE cabinet.storage_file
(
  file_id integer NOT NULL DEFAULT nextval('cabinet.storage_folder_folder_id_seq'::regclass),
  file_name character varying,
  file_nom integer NOT NULL,
  folder_id integer NOT NULL,
  ord integer NOT NULL DEFAULT 0,  
  file_size character varying,
  physical_path character varying,
  physical_name character varying,
  download_link character varying,
  file_type_id integer NOT NULL,
  file_state_id integer NOT NULL,
  has_version boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  state_date timestamp without time zone,
  state_user character varying,
  CONSTRAINT storage_file_pkey PRIMARY KEY (file_id),
  CONSTRAINT storage_file_file_state_id_fkey FOREIGN KEY (file_state_id)
      REFERENCES cabinet.storage_file_state (file_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT storage_file_file_type_id_fkey FOREIGN KEY (file_type_id)
      REFERENCES cabinet.storage_file_type (file_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT storage_file_folder_id_fkey FOREIGN KEY (folder_id)
      REFERENCES cabinet.storage_folder (folder_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.storage_file OWNER TO pavlov;


/* ------------------------------- */

-- DROP TABLE cabinet.storage_file_version;

CREATE TABLE cabinet.storage_file_version
(
  file_version_id serial NOT NULL,
  file_id integer NOT NULL,
  version_num integer NOT NULL,  
  file_size character varying,
  physical_path character varying,
  physical_name character varying,
  download_link character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  CONSTRAINT storage_file_version_pkey PRIMARY KEY (file_version_id),
  CONSTRAINT storage_file_version_file_id_fkey FOREIGN KEY (file_id)
      REFERENCES cabinet.storage_file (file_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.storage_file_version OWNER TO pavlov;


/* ------------------------------- */

-- DROP TABLE cabinet.log_storage;

CREATE TABLE cabinet.log_storage
(
  log_id bigserial NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  user_id integer,
  mess character varying,
  file_id integer,
  folder_id integer,
  CONSTRAINT log_storage_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.log_storage OWNER TO pavlov;

/* ------------------------------- */

-- DROP VIEW cabinet.vw_storage_file;

CREATE OR REPLACE VIEW cabinet.vw_storage_file AS 
 SELECT t1.file_id,
  t1.file_name,
  t1.file_nom,
  t1.folder_id,
  t1.ord,
  t1.file_size,
  t1.physical_path,
  t1.physical_name,
  t1.download_link,
  t1.file_type_id,
  t1.file_state_id,
  t1.has_version,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.state_date,
  t1.state_user,
  t2.folder_name,
  t2.folder_level,
  t3.file_type_name,
  t3.group_id,
  t4.group_name,
  t5.file_state_name,
  (case when trim(coalesce(t4.img, '')) = '' then 'storage storage-file' else 'storage storage-' || trim(t4.img) end)::character varying as sprite_css_class,
  (trim(t1.file_name) || '.' || trim(t3.file_type_name))::character varying as file_name_with_extention,
  (case when t11.file_version_id is not null then true else false end) as v1_exists,
  t11.file_version_id as v1_file_version_id,
  t11.file_size as v1_file_size,
  t11.physical_path as v1_physical_path,
  t11.physical_name as v1_physical_name,
  t11.download_link as v1_download_link,
  t11.crt_date as v1_crt_date,
  t11.crt_user as v1_crt_user,
  (case when t12.file_version_id is not null then true else false end) as v2_exists,
  t12.file_version_id as v2_file_version_id,
  t12.file_size as v2_file_size,
  t12.physical_path as v2_physical_path,
  t12.physical_name as v2_physical_name,
  t12.download_link as v2_download_link,
  t12.crt_date as v2_crt_date,
  t12.crt_user as v2_crt_user  
    FROM cabinet.storage_file t1
	INNER JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
	INNER JOIN cabinet.storage_file_type t3 ON t1.file_type_id = t3.file_type_id
	INNER JOIN cabinet.storage_file_type_group t4 ON t3.group_id = t4.group_id
	INNER JOIN cabinet.storage_file_state t5 ON t1.file_state_id = t5.file_state_id
	LEFT JOIN cabinet.storage_file_version t11 ON t1.file_id = t11.file_id AND t11.version_num = 1
	LEFT JOIN cabinet.storage_file_version t12 ON t1.file_id = t12.file_id AND t12.version_num = 2
	;

ALTER TABLE cabinet.vw_storage_file OWNER TO pavlov;

/* ------------------------------- */

-- DROP VIEW cabinet.vw_log_storage;

CREATE OR REPLACE VIEW cabinet.vw_log_storage AS 
 SELECT t1.log_id,
    t1.date_beg,
    t1.date_end,
    t1.user_id,
    t1.mess,
    t1.folder_id,
    t1.file_id,
    t2.folder_name,
    t3.file_name,
    t4.user_name,
    t4.full_name
   FROM cabinet.log_storage t1
     LEFT JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
     LEFT JOIN cabinet.storage_file t3 ON t1.file_id = t3.file_id
     LEFT JOIN cabinet.cab_user t4 ON t1.user_id = t4.user_id;

ALTER TABLE cabinet.vw_log_storage OWNER TO pavlov;

/* ------------------------------- */