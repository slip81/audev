﻿/*
	ALTER TABLE cabinet.cab_user ADD COLUMN is_active
*/

ALTER TABLE cabinet.cab_user ADD COLUMN is_active boolean NOT NULL DEFAULT True;
COMMENT ON COLUMN cabinet.cab_user.is_active IS 'True - пользователь активен';
