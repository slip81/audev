﻿/*
	CREATE TABLE cabinet.crm_event_group
*/

-- DROP TABLE cabinet.crm_event_group;

CREATE TABLE cabinet.crm_event_group
(
  group_id integer NOT NULL,
  group_name character varying,
  CONSTRAINT crm_event_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);

COMMENT ON TABLE cabinet.crm_event_group IS 'Группа задач ЛК в календаре задач';
COMMENT ON COLUMN cabinet.crm_event_group.group_id IS 'PK';
COMMENT ON COLUMN cabinet.crm_event_group.group_name IS 'Наименование группы';

ALTER TABLE cabinet.crm_event_group OWNER TO pavlov;

INSERT INTO cabinet.crm_event_group (group_id, group_name)
VALUES (1, 'Обычные');

INSERT INTO cabinet.crm_event_group (group_id, group_name)
VALUES (2, 'Важные');

INSERT INTO cabinet.crm_event_group (group_id, group_name)
VALUES (3, 'Срочные');