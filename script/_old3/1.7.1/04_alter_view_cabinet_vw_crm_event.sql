﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_event
*/

DROP VIEW cabinet.vw_crm_event;

CREATE OR REPLACE VIEW cabinet.vw_crm_event AS 
 SELECT t1.event_id,
    t1.event_name,
    t1.event_text,
    t1.task_id,
    t1.date_beg,
    t1.date_end,
    t1.exec_user_id,
    t1.is_done,
    t1.crt_date,
    t1.crt_user,
    t1.is_canceled,
    t1.date_plan,
    t1.event_progress,
    t1.event_result,
    t1.task_state_id,
    t11.task_num,
    t11.task_name,
    t11.task_text,
    t12.user_name AS exec_user_name
   FROM cabinet.crm_event t1
     LEFT JOIN cabinet.crm_task t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.crm_user t12 ON t1.exec_user_id = t12.user_id;

ALTER TABLE cabinet.vw_crm_event
  OWNER TO pavlov;
