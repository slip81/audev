﻿/*
	ALTER TABLE cabinet.crm_event ADD COLUMN date_fact
*/

ALTER TABLE cabinet.crm_event ADD COLUMN date_fact date;
COMMENT ON COLUMN cabinet.crm_event.date_fact IS 'Фактическая дата выполнения события';


UPDATE cabinet.crm_event
SET date_fact = date_end
WHERE is_done = true AND coalesce(task_id, 0) <= 0 AND date_fact is null;

UPDATE cabinet.crm_event
SET date_fact = (SELECT coalesce(x1.repair_date::date, current_date) FROM cabinet.crm_task x1 WHERE x1.task_id = cabinet.crm_event.task_id)
WHERE is_done = true AND coalesce(task_id, 0) > 0 AND date_fact is null;

--select * from cabinet.crm_event order by event_id desc

