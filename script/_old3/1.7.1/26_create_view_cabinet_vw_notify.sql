﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_notify
*/

-- DROP VIEW cabinet.vw_notify;

CREATE OR REPLACE VIEW cabinet.vw_notify AS 
 SELECT t1.notify_id,
  t1.scope,
  t1.target_user_id,
  t1.message,
  t1.message_sent,
  t1.crt_date,
  t1.sent_date,
  t3.icq
  FROM cabinet.notify t1
  LEFT JOIN cabinet.crm_user t2 ON t1.target_user_id = t2.user_id
  LEFT JOIN cabinet.cab_user t3 ON t2.cab_user_id = t3.user_id
  ;

ALTER TABLE cabinet.vw_notify OWNER TO pavlov;
