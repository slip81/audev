﻿/*
	ALTER TABLE cabinet.cab_user ADD COLUMN icq
*/

ALTER TABLE cabinet.cab_user ADD COLUMN icq character varying;
COMMENT ON COLUMN cabinet.cab_user.icq IS 'Номер ICQ';
