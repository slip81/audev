﻿/*
	ALTER TABLE cabinet.crm_event ADD COLUMN task_state_id
*/

ALTER TABLE cabinet.crm_event ADD COLUMN task_state_id integer NOT NULL DEFAULT 0;
COMMENT ON COLUMN cabinet.crm_event.task_state_id IS 'Код статуса задачи';

ALTER TABLE cabinet.crm_event
  ADD CONSTRAINT crm_event_task_state_id_fkey FOREIGN KEY (task_state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE cabinet.crm_event SET task_state_id = (SELECT x1.state_id FROM cabinet.crm_task x1 WHERE x1.task_id = cabinet.crm_event.task_id)
WHERE coalesce(task_id, 0) > 0;

--select * from cabinet.crm_event order by event_id;