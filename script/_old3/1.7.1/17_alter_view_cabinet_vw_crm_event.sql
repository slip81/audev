﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_event
*/

-- DROP VIEW cabinet.vw_crm_event;

CREATE OR REPLACE VIEW cabinet.vw_crm_event AS 
 SELECT t1.event_id,
    t1.event_name,
    t1.event_text,
    t1.task_id,
    t1.date_beg,
    t1.date_end,
    t1.exec_user_id,
    t1.is_done,
    t1.crt_date,
    t1.crt_user,
    t1.is_canceled,
    t1.date_plan,
    t1.event_progress,
    t1.event_result,
    t1.task_state_id,
    t1.date_fact,
    t1.event_group_id,
    t11.task_num,
    t11.task_name,
    t11.task_text,
    t12.user_name AS exec_user_name,
    case when coalesce(t1.task_id, 0) > 0 then t13.state_name else
	case when t1.is_done then 'Выполнено'::character varying else
		case when t1.is_canceled then 'Отменено'::character varying else 'Активно'::character varying end
	end
    end as state_name,
    t14.group_name AS event_group_name,
    t12.role_id as exec_user_role_id,
    coalesce(t15.ignore_date_plan, false) as ignore_date_plan,
    case when coalesce(t12.role_id, 0) > 0 then 
	case when coalesce(t16.state_id, -1) = coalesce(t1.task_state_id, 0) then true else false end 
    else t1.is_done end	as is_done_user_role
   FROM cabinet.crm_event t1
     LEFT JOIN cabinet.crm_task t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.crm_user t12 ON t1.exec_user_id = t12.user_id
     LEFT JOIN cabinet.crm_state t13 ON t1.task_state_id = t13.state_id
     LEFT JOIN cabinet.crm_event_group t14 ON t1.event_group_id = t14.group_id
     LEFT JOIN cabinet.crm_user_role t15 ON t12.role_id = t15.role_id
     LEFT JOIN cabinet.crm_user_role_state_done t16 ON t15.role_id = t16.role_id and t1.task_state_id = t16.state_id
     ;

ALTER TABLE cabinet.vw_crm_event OWNER TO pavlov;
