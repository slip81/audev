﻿/*
	ALTER TABLE cabinet.cab_grid_user_settings ADD COLUMN page_size
*/

ALTER TABLE cabinet.cab_grid_user_settings ADD COLUMN page_size integer;
COMMENT ON COLUMN cabinet.cab_grid_user_settings.page_size IS 'Количество строк на странице';
