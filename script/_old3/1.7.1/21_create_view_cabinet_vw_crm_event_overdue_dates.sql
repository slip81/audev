﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_event_overdue_dates
*/

-- DROP VIEW cabinet.vw_crm_event_overdue_dates

CREATE OR REPLACE VIEW cabinet.vw_crm_event_overdue_dates AS 
	select row_number() over (order by event_date) as id, event_date
	from
	(
	select distinct date_beg as event_date
	from cabinet.vw_crm_event
	where date_plan is not null and date_plan < current_date and is_done_user_role = false and ignore_date_plan = false
	union
	select distinct date_end as event_date
	from cabinet.vw_crm_event
	where date_plan is not null and date_plan < current_date and is_done_user_role = false and ignore_date_plan = false
	union
	select current_date as event_date
	union
	select current_date+1 as event_date
	union
	select current_date+2 as event_date
	union
	select current_date+3 as event_date
	) t1
	order by event_date
	;

ALTER TABLE cabinet.vw_crm_event_overdue_dates OWNER TO pavlov;
