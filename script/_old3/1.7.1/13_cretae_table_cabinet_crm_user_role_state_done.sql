﻿/*
	CREATE TABLE cabinet.crm_user_role_state_done
*/

-- DROP TABLE cabinet.crm_user_role_state_done;

CREATE TABLE cabinet.crm_user_role_state_done
(
  item_id serial NOT NULL,
  role_id integer NOT NULL,
  state_id integer NOT NULL,
  CONSTRAINT crm_user_role_state_done_pkey PRIMARY KEY (item_id),
  CONSTRAINT crm_user_role_state_done_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cabinet.crm_user_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT crm_user_role_state_done_state_id_fkey FOREIGN KEY (state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

COMMENT ON TABLE cabinet.crm_user_role_state_done IS 'Статусы для выполненных задач ЛК в разрезе ролей';
COMMENT ON COLUMN cabinet.crm_user_role_state_done.item_id IS 'PK';
COMMENT ON COLUMN cabinet.crm_user_role_state_done.role_id IS 'Код роли';
COMMENT ON COLUMN cabinet.crm_user_role_state_done.state_id IS 'Код статуса';

ALTER TABLE cabinet.crm_user_role_state_done OWNER TO pavlov;

-- select * from cabinet.crm_state order by state_id
-- select * from cabinet.crm_user_role order by role_id;

-- local fix
INSERT INTO cabinet.crm_state (state_id, state_name)
SELECT 6, 'Комлп. тест'
FROM cabinet.crm_state 
WHERE NOT EXISTS(SELECT state_id FROM cabinet.crm_state WHERE state_id = 6)
LIMIT 1;

INSERT INTO cabinet.crm_user_role_state_done (role_id, state_id)
VALUES (1, 2);

INSERT INTO cabinet.crm_user_role_state_done (role_id, state_id)
VALUES (1, 5);

INSERT INTO cabinet.crm_user_role_state_done (role_id, state_id)
VALUES (1, 6);

INSERT INTO cabinet.crm_user_role_state_done (role_id, state_id)
VALUES (2, 5);

INSERT INTO cabinet.crm_user_role_state_done (role_id, state_id)
VALUES (2, 6);

INSERT INTO cabinet.crm_user_role_state_done (role_id, state_id)
VALUES (3, 5);

-- select * from cabinet.crm_user_role_state_done order by role_id, state_id;