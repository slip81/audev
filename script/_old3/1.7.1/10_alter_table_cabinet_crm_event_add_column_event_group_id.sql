﻿/*
	ALTER TABLE cabinet.crm_event ADD COLUMN event_group_id
*/

-- ALTER TABLE cabinet.crm_event DROP COLUMN event_group_id;

ALTER TABLE cabinet.crm_event ADD COLUMN event_group_id integer;
COMMENT ON COLUMN cabinet.crm_event.event_group_id IS 'Код группы события';

ALTER TABLE cabinet.crm_event
  ADD CONSTRAINT crm_event_event_group_id_fkey FOREIGN KEY (event_group_id)
      REFERENCES cabinet.crm_event_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


UPDATE cabinet.crm_event SET event_group_id = 1 WHERE coalesce(event_group_id, 0) <= 0;