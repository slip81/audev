﻿/*
	CREATE TABLE cabinet.notify
*/

-- DROP TABLE cabinet.notify;

CREATE TABLE cabinet.notify
(
  notify_id serial NOT NULL,
  scope integer NOT NULL DEFAULT 1,
  target_user_id integer,
  message character varying,
  message_sent boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  sent_date timestamp without time zone,
  CONSTRAINT notify_pkey PRIMARY KEY (notify_id),
  CONSTRAINT notify_target_user_id_fkey FOREIGN KEY (target_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

COMMENT ON TABLE cabinet.notify IS 'Уведомления';
COMMENT ON COLUMN cabinet.notify.notify_id IS 'PK';
COMMENT ON COLUMN cabinet.notify.scope IS '1 - задачи ЛК';
COMMENT ON COLUMN cabinet.notify.target_user_id IS 'Код пользователя, для которого сообщение';
COMMENT ON COLUMN cabinet.notify.message IS 'Текст сообщения';
COMMENT ON COLUMN cabinet.notify.message_sent IS 'True - сообщение отправлено пользователю';
COMMENT ON COLUMN cabinet.notify.crt_date IS 'Дата создания строки';
COMMENT ON COLUMN cabinet.notify.sent_date IS 'Дата отправки сообщения';

ALTER TABLE cabinet.notify OWNER TO pavlov;
