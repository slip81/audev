﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN is_event
*/

ALTER TABLE cabinet.crm_task ADD COLUMN is_event boolean NOT NULL DEFAULT false;
COMMENT ON COLUMN cabinet.crm_task.is_event IS 'True - задача есть в календаре';

UPDATE cabinet.crm_task SET is_event = true WHERE task_id in
(SELECT DISTINCT task_id FROM cabinet.crm_event WHERE coalesce(task_id, 0) > 0);