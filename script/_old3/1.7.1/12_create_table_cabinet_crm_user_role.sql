﻿/*
	CREATE TABLE cabinet.crm_user_role
*/

-- DROP TABLE cabinet.crm_user_role;

CREATE TABLE cabinet.crm_user_role
(
  role_id integer NOT NULL,
  role_name character varying,
  ignore_date_plan boolean NOT NULL DEFAULT false,
  add_days_date_plan integer,
  CONSTRAINT crm_user_role_pkey PRIMARY KEY (role_id)
)
WITH (
  OIDS=FALSE
);

COMMENT ON TABLE cabinet.crm_user_role IS 'Роли пользователей в задачах ЛК';
COMMENT ON COLUMN cabinet.crm_user_role.role_id IS 'PK';
COMMENT ON COLUMN cabinet.crm_user_role.role_name IS 'Наименование роли';
COMMENT ON COLUMN cabinet.crm_user_role.ignore_date_plan IS 'True - у пользователей с этой ролью задачи никогда не бывают просроченными';
COMMENT ON COLUMN cabinet.crm_user_role.add_days_date_plan IS 'Сколько дней можно добавить к плановой дате выполнения задачи, чтобы не считать ее просроченной';

ALTER TABLE cabinet.crm_user_role OWNER TO pavlov;

INSERT INTO cabinet.crm_user_role (role_id, role_name, ignore_date_plan, add_days_date_plan)
VALUES (1, 'Разработчик', false, 0);

INSERT INTO cabinet.crm_user_role (role_id, role_name, ignore_date_plan, add_days_date_plan)
VALUES (2, 'Тестировщик', true, 0);

INSERT INTO cabinet.crm_user_role (role_id, role_name, ignore_date_plan, add_days_date_plan)
VALUES (3, 'Сервис', true, 0);