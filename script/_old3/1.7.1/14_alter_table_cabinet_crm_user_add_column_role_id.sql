﻿/*
	ALTER TABLE cabinet.crm_user ADD COLUMN role_id
*/

ALTER TABLE cabinet.crm_user ADD COLUMN role_id integer;
COMMENT ON COLUMN cabinet.crm_user.role_id IS 'Код роли пользователя в задачах ЛК';

ALTER TABLE cabinet.crm_user
  ADD CONSTRAINT crm_user_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cabinet.crm_user_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;