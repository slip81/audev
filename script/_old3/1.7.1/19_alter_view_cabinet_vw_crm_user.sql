﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_user
*/

-- DROP VIEW cabinet.vw_crm_user;

CREATE OR REPLACE VIEW cabinet.vw_crm_user AS 
 SELECT t1.user_id AS crm_user_id,
    t1.user_name AS crm_user_name,
    t1.cab_user_id,
    t1.is_boss,
    t1.allow_bucket_read,
    t1.allow_bucket_write,
    t1.allow_prepared_read,
    t1.allow_prepared_write,
    t1.allow_fixed_read,
    t1.allow_fixed_write,
    t1.allow_approved_read,
    t1.allow_approved_write,
    t11.user_id,
        CASE
            WHEN t12.fore_color IS NULL THEN 'black'::character varying
            ELSE t12.fore_color
        END AS fore_color,
    t1.role_id,
    t13.role_name,
    t1.is_executer
   FROM cabinet.crm_user t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1
     LEFT JOIN cabinet.crm_user_user_settings t12 ON t1.user_id = t12.crm_user_id AND t11.user_id = COALESCE(t12.user_id, t11.user_id)
     LEFT JOIN cabinet.crm_user_role t13 ON t1.role_id = t13.role_id;

ALTER TABLE cabinet.vw_crm_user OWNER TO pavlov;
