﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN event_user_id
*/

ALTER TABLE cabinet.crm_task ADD COLUMN event_user_id integer;
COMMENT ON COLUMN cabinet.crm_task.event_user_id IS 'Код пользователя, у которого эта задача в календаре';

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_event_user_id_fkey FOREIGN KEY (event_user_id)
      REFERENCES cabinet.crm_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE cabinet.crm_task 
SET event_user_id = exec_user_id
WHERE is_event = true AND coalesce(event_user_id, 0) <= 0; 
 