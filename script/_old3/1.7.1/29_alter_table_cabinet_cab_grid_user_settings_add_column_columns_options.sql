﻿/*
	ALTER TABLE cabinet.cab_grid_user_settings ADD COLUMN columns_options
*/

ALTER TABLE cabinet.cab_grid_user_settings ADD COLUMN columns_options text;
COMMENT ON COLUMN cabinet.cab_grid_user_settings.columns_options IS 'Настройки колонок грида';
