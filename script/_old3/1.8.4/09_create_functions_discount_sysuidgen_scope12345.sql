﻿/*
	CREATE FUNCTIONs discount.sysuidgen_scopeN
*/

-- DROP FUNCTION discount.sysuidgen_scope1();

CREATE OR REPLACE FUNCTION discount.sysuidgen_scope1(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (1 << 10) | (nextval('discount.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.sysuidgen_scope1() OWNER TO pavlov;


-- DROP FUNCTION discount.sysuidgen_scope2();

CREATE OR REPLACE FUNCTION discount.sysuidgen_scope2(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (2 << 10) | (nextval('discount.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.sysuidgen_scope2() OWNER TO pavlov;

-- DROP FUNCTION discount.sysuidgen_scope3();

CREATE OR REPLACE FUNCTION discount.sysuidgen_scope3(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (3 << 10) | (nextval('discount.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.sysuidgen_scope3() OWNER TO pavlov;

-- DROP FUNCTION discount.sysuidgen_scope4();

CREATE OR REPLACE FUNCTION discount.sysuidgen_scope4(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (4 << 10) | (nextval('discount.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.sysuidgen_scope4() OWNER TO pavlov;

-- DROP FUNCTION discount.sysuidgen_scope5();

CREATE OR REPLACE FUNCTION discount.sysuidgen_scope5(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (5 << 10) | (nextval('discount.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.sysuidgen_scope5() OWNER TO pavlov;