﻿/*
	CREATE TABLE cabinet.service_kit
*/

-- DROP TABLE cabinet.service_kit;

CREATE TABLE cabinet.service_kit
(
  service_kit_id serial NOT NULL,
  service_kit_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT service_kit_pkey PRIMARY KEY (service_kit_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_kit OWNER TO pavlov;


CREATE TRIGGER cabinet_service_kit_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.service_kit
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();


-- DROP TABLE cabinet.service_kit_item;

CREATE TABLE cabinet.service_kit_item
(
  item_id serial NOT NULL,
  service_kit_id integer NOT NULL,
  service_id integer NOT NULL,
  version_id integer,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT service_kit_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT service_kit_item_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_kit_item_service_kit_id_fkey FOREIGN KEY (service_kit_id)
      REFERENCES cabinet.service_kit (service_kit_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_kit_item_version_id_fkey FOREIGN KEY (version_id)
      REFERENCES cabinet.version (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_kit_item OWNER TO pavlov;


CREATE TRIGGER cabinet_service_kit_item_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.service_kit_item
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();