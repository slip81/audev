﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN storage_folder_id integer
*/

ALTER TABLE cabinet.crm_task ADD COLUMN storage_folder_id integer;

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_storage_folder_id_fkey FOREIGN KEY (storage_folder_id)
      REFERENCES cabinet.storage_folder (folder_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;