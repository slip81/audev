﻿/*
	CREATE SEQUENCE discount.sys_uid_sequence
*/

-- DROP SEQUENCE discount.sys_uid_sequence;

CREATE SEQUENCE discount.sys_uid_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
ALTER TABLE discount.sys_uid_sequence OWNER TO pavlov;

-- select nextval('discount.sys_uid_sequence')