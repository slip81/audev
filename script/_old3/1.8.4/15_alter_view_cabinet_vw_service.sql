﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_service
*/

DROP VIEW cabinet.vw_service;

CREATE OR REPLACE VIEW cabinet.vw_service AS 
 SELECT t1.id,
    t1.guid,
    t1.name,
    t1.description,
    t1.parent_id,
    t1.is_deleted,
    t1.exe_name,
    t1.is_site,    
    t1.price,
    t1.need_key,
    t1."NeedCheckVersionLicense",
    t1.is_service,
    t1.priority,
    t1.have_spec,
    t1.max_workplace_cnt,
    t1.group_id,
    t2.group_name
   FROM cabinet.service t1
     LEFT JOIN cabinet.service_group t2 ON t1.group_id = t2.group_id
  WHERE t1.is_deleted <> 1 AND t1.parent_id IS NOT NULL;

ALTER TABLE cabinet.vw_service OWNER TO pavlov;
