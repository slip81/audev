﻿/*
	CREATE OR REPLACE FUNCTION cabinet.add_column_to_all_tables
*/

-- DROP FUNCTION cabinet.add_column_to_all_tables(text, text, text);

CREATE OR REPLACE FUNCTION cabinet.add_column_to_all_tables(
    _schema_name text,
    _column_name text,
    _column_type text)
  RETURNS void AS
$BODY$
DECLARE
 my_row RECORD;
BEGIN
 EXECUTE 'SET search_path TO ' || _schema_name;

 FOR my_row IN 
 SELECT table_name
 FROM information_schema.tables
 WHERE table_schema = _schema_name
 and table_type = 'BASE TABLE'
 and table_name not like 'tmp_%'
 order by table_name
 LOOP
 IF NOT EXISTS
 (
 SELECT attname FROM pg_attribute WHERE attrelid = 
 (SELECT oid FROM pg_class WHERE relname = my_row.table_name LIMIT 1)
 AND attname = _column_name
 )
 THEN
 EXECUTE('ALTER TABLE "' || my_row.table_name || '" ADD COLUMN ' || _column_name || ' ' || _column_type || ';');
 END IF;
 END LOOP;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.add_column_to_all_tables(text, text, text) OWNER TO pavlov;
