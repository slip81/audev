﻿/*
	add_column_to_all_tables sysrowowner sysrowstamp
*/ 

SELECT cabinet.add_column_to_all_tables('discount', 'sysrowuid', 'bigint');
SELECT cabinet.add_column_to_all_tables('discount', 'sysrowstamp', 'bigint');