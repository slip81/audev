﻿/*
	DROP FUNCTIONS discount.xxx
*/

DROP FUNCTION discount.import_card_del(bigint, bigint, bigint, integer, integer, integer, character varying);

DROP FUNCTION discount.import_card_update(bigint, integer, integer);