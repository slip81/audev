﻿/*
	select www, https, http in cabinet.crm_task
*/

select * from cabinet.crm_task_note where note_text like '%www%' order by subtask_id desc;
select * from cabinet.crm_subtask where subtask_text like '%www%' order by subtask_id desc;

select * from cabinet.crm_task_note where note_text like '%https%' order by subtask_id desc;
select * from cabinet.crm_subtask where subtask_text like '%https%' order by subtask_id desc;

select * from cabinet.crm_task_note where note_text like '%http%' order by subtask_id desc;
select * from cabinet.crm_subtask where subtask_text like '%http%' order by subtask_id desc;