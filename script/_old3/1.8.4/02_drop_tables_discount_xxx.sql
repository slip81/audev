﻿/*
	DROP TABLES discount.xxx
*/

DROP TABLE discount.card_restr_rule_result;

DROP TABLE discount.card_restr_value;

DROP TABLE discount.card_test_generator;

DROP TABLE discount.card_type_restr_rule_transition;

DROP TABLE discount.card_type_restr_rule_spent;

DROP TABLE discount.card_type_restr_rule_left;

DROP TABLE discount.card_type_restr_rule;

DROP TABLE discount.card_type_restr_value;

DROP TABLE discount.card_type_restr;

DROP TABLE discount.test1;

DROP TABLE discount.reestr_rule;

ALTER TABLE discount.card DROP CONSTRAINT card_reestr_id_fkey;

ALTER TABLE discount.card DROP COLUMN reestr_id;

DROP TABLE discount.reestr;

DROP TABLE discount.business_rel;