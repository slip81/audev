﻿/*
	update cabinet.version
*/

update cabinet.version
set file_size = 0
where file_size is null;