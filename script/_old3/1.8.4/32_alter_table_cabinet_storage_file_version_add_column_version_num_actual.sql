﻿/*
	ALTER TABLE cabinet.storage_file_version ADD COLUMN version_num_actual
*/

ALTER TABLE cabinet.storage_file_version ADD COLUMN version_num_actual integer;

UPDATE cabinet.storage_file_version SET version_num_actual = version_num;

ALTER TABLE cabinet.storage_file_version ALTER COLUMN version_num_actual SET NOT NULL;