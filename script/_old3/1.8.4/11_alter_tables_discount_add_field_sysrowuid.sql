﻿/*
	ALTER TABLEs discount.xxx ADD COLUMN sysrowuid
*/

ALTER TABLE discount.batch ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.batch_card ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.batch_error ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.business ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.business_org ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.business_org_user ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.card ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.card_additional_num ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.card_card_status_rel ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();
ALTER TABLE discount.card_card_type_rel ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope1();

ALTER TABLE discount.card_ext1 ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_holder ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_holder_card_rel ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_nominal ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_nominal_inactive ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_nominal_uncommitted ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_status ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_status_group ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_transaction ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();
ALTER TABLE discount.card_transaction_detail ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope2();

ALTER TABLE discount.card_transaction_unit ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.card_type ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.card_type_business_org ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.card_type_check_info ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.card_type_group ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.card_type_limit ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.card_type_programm_rel ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.card_type_unit ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.import_discount ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();
ALTER TABLE discount.import_discount_card ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope3();

ALTER TABLE discount.programm ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_descr ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_order ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_param ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_param_group ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_param_group_scope ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_result ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_result_detail ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_step ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();
ALTER TABLE discount.programm_step_condition ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope4();

ALTER TABLE discount.programm_step_logic ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope5();
ALTER TABLE discount.programm_step_param ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope5();
ALTER TABLE discount.programm_step_result ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope5();
ALTER TABLE discount.programm_template ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope5();
ALTER TABLE discount.programm_template_param ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope5();
ALTER TABLE discount.report_discount ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope5();
ALTER TABLE discount.sale_summary ADD COLUMN sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope5();
