﻿/*
	CREATE TRIGGERS cabinet_xxx_set_stamp
*/

-- DROP FUNCTION cabinet.set_stamp();

CREATE OR REPLACE FUNCTION cabinet.set_stamp()
  RETURNS trigger AS
$BODY$
BEGIN
    NEW.sysrowstamp := extract(epoch from clock_timestamp())::bigint;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.set_stamp() OWNER TO postgres;

/* ************************** */

CREATE TRIGGER cabinet_auth_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.auth_group
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_auth_role_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.auth_role
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_auth_role_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.auth_role_group
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_auth_role_perm_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.auth_role_perm
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_auth_section_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.auth_section
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_auth_section_part_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.auth_section_part
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_auth_user_perm_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.auth_user_perm
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_auth_user_role_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.auth_user_role
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_action_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_action
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_action_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_action_group
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_grid_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_grid
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_grid_column_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_grid_column
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_grid_column_user_settings_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_grid_column_user_settings
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_grid_user_settings_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_grid_user_settings
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_role_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_role
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_role_action_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_role_action
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_user_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_user
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_user_menu_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_user_menu
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_user_profile_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_user_profile
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_user_role_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_user_role
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_cab_user_role_action_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.cab_user_role_action
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_city_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.city
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_client_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.client
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_client_service_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.client_service
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_client_service_param_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.client_service_param
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_client_user_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.client_user
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_client_user_widget_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.client_user_widget
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_country_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.country
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_client_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_client
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_event_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_event
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_event_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_event_group
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_group
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_group_operation_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_group_operation
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_module_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_module
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_module_part_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_module_part
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_module_version_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_module_version
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_priority_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_priority
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_priority_user_settings_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_priority_user_settings
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_project_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_project
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_project_user_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_project_user
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_project_user_settings_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_project_user_settings
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_state_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_state
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_state_user_settings_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_state_user_settings
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_subtask_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_subtask
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_task_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_task
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_task_file_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_task_file
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_task_note_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_task_note
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_task_note_file_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_task_note_file
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_task_operation_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_task_operation
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_task_rel_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_task_rel
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_task_rel_type_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_task_rel_type
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_task_user_filter_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_task_user_filter
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_user_role_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_user_role
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_user_role_state_done_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_user_role_state_done
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_crm_user_task_follow_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.crm_user_task_follow
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_delivery_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.delivery
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_delivery_detail_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.delivery_detail
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_delivery_template_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.delivery_template
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_delivery_template_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.delivery_template_group
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_district_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.district
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_file_dic_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.file_dic
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_file_path_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.file_path
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_license_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.license
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_log_cab_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.log_cab
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_log_crm_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.log_crm
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_log_storage_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.log_storage
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_applications_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_applications
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_membership_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_membership
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_profiles_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_profiles
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_roles_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_roles
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_schemaversion_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_schemaversion
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_sessioncleanup_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_sessioncleanup
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_sessions_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_sessions
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_users_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_users
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_my_aspnet_usersinroles_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.my_aspnet_usersinroles
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_notify_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.notify
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_order_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet."order"
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_order_item_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.order_item
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_reg_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.reg
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_region_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.region
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_reregistration_order_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.reregistration_order
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_sales_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.sales
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_cva_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service_cva
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_cva_data_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service_cva_data
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_cva_data_ext_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service_cva_data_ext
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_defect_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service_defect
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service_group
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_registration_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service_registration
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_sender_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service_sender
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_service_sender_log_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.service_sender_log
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_storage_file_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.storage_file
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_storage_file_state_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.storage_file_state
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_storage_file_type_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.storage_file_type
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_storage_file_type_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.storage_file_type_group
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_storage_file_version_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.storage_file_version
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_storage_folder_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.storage_folder
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_storage_link_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.storage_link
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_storage_param_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.storage_param
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_story_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.story
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_story_lc_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.story_lc
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_story_state_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.story_state
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_story_state_rule_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.story_state_rule
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_street_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.street
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_version_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.version
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_widget_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.widget
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();

CREATE TRIGGER cabinet_workplace_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON cabinet.workplace
    FOR EACH ROW
    EXECUTE PROCEDURE cabinet.set_stamp();