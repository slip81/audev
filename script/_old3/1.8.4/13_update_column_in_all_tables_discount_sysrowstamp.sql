﻿/*
	cabinet.update_column_in_all_tables sysrowstamp extract(epoch from clock_timestamp())::bigint
*/

SELECT cabinet.update_column_in_all_tables('discount', 'sysrowstamp', 'extract(epoch from clock_timestamp())::bigint');