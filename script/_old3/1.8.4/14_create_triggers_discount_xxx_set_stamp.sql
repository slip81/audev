﻿/*
	CREATE TRIGGERS discount_xxx_set_stamp
*/

-- DROP FUNCTION discount.set_stamp();

CREATE OR REPLACE FUNCTION discount.set_stamp()
  RETURNS trigger AS
$BODY$
BEGIN
    NEW.sysrowstamp := extract(epoch from clock_timestamp())::bigint;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.set_stamp() OWNER TO postgres;

/* ************************** */

CREATE TRIGGER discount_batch_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.batch
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_batch_card_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.batch_card
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_batch_error_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.batch_error
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_business_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.business
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_business_org_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.business_org
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_business_org_user_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.business_org_user
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_additional_num_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_additional_num
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_card_status_rel_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_card_status_rel
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();    

CREATE TRIGGER discount_card_card_type_rel_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_card_type_rel
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();        

CREATE TRIGGER discount_card_ext1_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_ext1
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_holder_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_holder
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_holder_card_rel_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_holder_card_rel
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_nominal_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_nominal
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_nominal_inactive_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_nominal_inactive
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();    

CREATE TRIGGER discount_card_nominal_uncommitted_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_nominal_uncommitted
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_status_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_status
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_status_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_status_group
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_transaction_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_transaction
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_transaction_detail_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_transaction_detail
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_transaction_unit_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_transaction_unit
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_type_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_type
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_type_business_org_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_type_business_org
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_type_check_info_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_type_check_info
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_type_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_type_group
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_type_limit_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_type_limit
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_type_programm_rel_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_type_programm_rel
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_card_type_unit_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.card_type_unit
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_import_discount_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.import_discount
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_import_discount_card_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.import_discount_card
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_log_event_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.log_event
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_log_event_type_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.log_event_type
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_log_session_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.log_session
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_descr_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_descr
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_order_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_order
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();
    
CREATE TRIGGER discount_programm_param_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_param
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_param_group_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_param_group
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_param_group_scope_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_param_group_scope
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_result_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_result
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_result_detail_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_result_detail
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_step_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_step
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_step_condition_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_step_condition
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_step_logic_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_step_logic
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_step_param_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_step_param
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_step_result_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_step_result
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_template_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_template
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_programm_template_param_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.programm_template_param
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_report_discount_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.report_discount
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_sale_summary_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.sale_summary
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();

CREATE TRIGGER discount_version_db_set_stamp_trg
    BEFORE INSERT OR UPDATE
    ON discount.version_db
    FOR EACH ROW
    EXECUTE PROCEDURE discount.set_stamp();    