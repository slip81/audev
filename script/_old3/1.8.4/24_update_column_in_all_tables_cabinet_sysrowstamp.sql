﻿/*
	cabinet.update_column_in_all_tables sysrowstamp extract(epoch from clock_timestamp())::bigint
*/

SELECT cabinet.update_column_in_all_tables('cabinet', 'sysrowstamp', 'extract(epoch from clock_timestamp())::bigint');