﻿/*
	ALTER TABLE cabinet.cab_grid_column ADD COLUMN title
*/

ALTER TABLE cabinet.cab_grid_column ADD COLUMN title character varying;