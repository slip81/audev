﻿/*
	ALTER TABLE cabinet.crm_task_file ADD COLUMN physical_path
*/

ALTER TABLE cabinet.crm_task_file ADD COLUMN physical_path character varying;


UPDATE cabinet.crm_task_file 
SET physical_path = '\\192.168.0.101\files\' || (task_id::text)
WHERE trim(coalesce(physical_path, '')) = '';