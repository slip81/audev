﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_service_kit_item
*/

-- DROP VIEW cabinet.service_kit_item;

CREATE OR REPLACE VIEW cabinet.vw_service_kit_item AS 
 SELECT t1.item_id,
	t1.service_kit_id,
	t1.service_id,
	t1.version_id,
	t2.service_kit_name,
	t3.name as service_name,
	coalesce(t11.name, 'Максимальная версия') as version_name
   FROM cabinet.service_kit_item t1
     INNER JOIN cabinet.service_kit t2 ON t1.service_kit_id = t2.service_kit_id
     INNER JOIN cabinet.service t3 ON t1.service_id = t3.id
     LEFT JOIN cabinet.version t11 ON t1.version_id = t11.id
     ;
     
ALTER TABLE cabinet.vw_service_kit_item OWNER TO pavlov;
