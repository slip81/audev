﻿/*
	ALTER TABLE esn.exchange_file_node ADD COLUMN client_id, sales_id
*/

ALTER TABLE esn.exchange_file_node ADD COLUMN client_id integer;
ALTER TABLE esn.exchange_file_node ADD COLUMN sales_id integer;


ALTER TABLE esn.exchange_file_node
  ADD CONSTRAINT exchange_file_node_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE esn.exchange_file_node
  ADD CONSTRAINT exchange_file_node_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      