﻿/*
	CREATE OR REPLACE VIEW esn.vw_exchange_file_log
*/

-- DROP VIEW esn.vw_exchange_file_log;

CREATE OR REPLACE VIEW esn.vw_exchange_file_log AS 
 SELECT 
 t1.log_id, t1.log_num, t1.node, t1.filename, t1.period, t1.crt_user, t1.crt_date,
 coalesce(t2.partner_name, '[не задан]') as partner_name, coalesce(t2.node_parent, '[не задан]') as node_parent, 
 coalesce(t2.node_descr, '[не задан]') as node_descr, coalesce(t2.node_name , '[не задан]') as node_name
 FROM esn.exchange_file_log t1
 LEFT JOIN esn.exchange_file_node t2 ON trim(coalesce(t1.node, '')) = trim(coalesce(t2.node_name, ''))
 ;

ALTER TABLE esn.vw_exchange_file_log OWNER TO pavlov;
