﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service_unregistered
*/

-- DROP VIEW cabinet.vw_client_service_unregistered;

CREATE OR REPLACE VIEW cabinet.vw_client_service_unregistered AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS id,
    t1.client_id,
    t2.service_id,
    t2.version_id,
    t3.name AS service_name,
    t12.name AS version_name,
    t3.description AS service_description,     
    t10.id as license_id,
    t2.id as order_item_id,
    t1.id as order_id
   FROM cabinet."order" t1
     JOIN cabinet.order_item t2 ON t1.id = t2.order_id
     JOIN cabinet.service t3 ON t2.service_id = t3.id
     JOIN cabinet.license t10 ON t2.id = t10.order_item_id
     LEFT JOIN cabinet.service_registration t11 ON t10.id = t11.license_id
     LEFT JOIN cabinet.version t12 ON t2.version_id = t12.id
  WHERE t11.id IS NULL;

ALTER TABLE cabinet.vw_client_service_unregistered OWNER TO pavlov;
