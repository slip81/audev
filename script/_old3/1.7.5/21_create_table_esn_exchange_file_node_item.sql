﻿/*
	CREATE TABLE esn.exchange_file_node_item
*/

-- DROP TABLE esn.exchange_file_node_item;

CREATE TABLE esn.exchange_file_node_item
(
  item_id serial NOT NULL,
  node_id integer NOT NULL,
  exchange_item_id integer NOT NULL,
  file_name_mask character varying,
  CONSTRAINT exchange_file_node_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT exchange_file_node_item_exchange_item_id_fkey FOREIGN KEY (exchange_item_id)
      REFERENCES esn.exchange_item (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT exchange_file_node_item_node_id_fkey FOREIGN KEY (node_id)
      REFERENCES esn.exchange_file_node (node_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_file_node_item OWNER TO pavlov;
