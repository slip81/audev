﻿/*
	CREATE TABLE discount.programm_param_group
*/

-- DROP TABLE discount.programm_param_group;

CREATE TABLE discount.programm_param_group
(
  group_id integer NOT NULL,
  group_name character varying,
  scope_id integer NOT NULL DEFAULT 1,  
  CONSTRAINT programm_param_group_pkey PRIMARY KEY (group_id),
  CONSTRAINT programm_param_group_scope_id_fkey FOREIGN KEY (scope_id)
      REFERENCES discount.programm_param_group_scope (scope_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_param_group OWNER TO pavlov;

-- select * from discount.programm_param_group_scope order by scope_id

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (1, 'Порог скидки (сумма)', 1 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (2, 'Порог скидки (процент)', 1 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (3, 'Постоянный процент скидки', 1 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (4, 'Макс. процент скидки', 1 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (5, 'Макс. % розн. наценки для скидки', 1 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (6, 'Мин. % розн. наценки для скидки)', 1 );


INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (7, 'Порог бонусного процента (сумма)', 2 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (8, 'Порог бонусного процента (процент)', 2 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (9, 'Постоянный бонусный процент', 2 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (10, 'Макс. бонусный процент', 2 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (11, 'Макс. сумма покупки для оплаты бонусами', 2 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (12, 'Макс. % покупки для оплаты бонусами)', 2 );


INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (13, 'Порог скидки (сумма)', 3 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (14, 'Порог скидки (процент)', 3 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (15, 'Постоянный процент скидки', 3 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (16, 'Макс. процент скидки', 3 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (17, 'Макс. % розн. наценки для скидки', 3 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (18, 'Мин. % розн. наценки для скидки)', 3 );


INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (19, 'Порог бонусного процента (сумма)', 4 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (20, 'Порог бонусного процента (процент)', 4 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (21, 'Постоянный бонусный процент', 4 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (22, 'Макс. бонусный процент', 4 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (23, 'Макс. сумма покупки для оплаты бонусами', 4 );

INSERT INTO discount.programm_param_group(group_id, group_name, scope_id)
VALUES (24, 'Макс. % покупки для оплаты бонусами)', 4 );

-- select * from discount.programm_param_group order by group_id