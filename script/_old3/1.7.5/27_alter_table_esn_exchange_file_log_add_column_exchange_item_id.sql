﻿/*
	ALTER TABLE esn.exchange_file_log ADD COLUMN exchange_item_id integer;
*/

ALTER TABLE esn.exchange_file_log ADD COLUMN exchange_item_id integer;

ALTER TABLE esn.exchange_file_log
  ADD CONSTRAINT exchange_file_log_exchange_item_id_fkey FOREIGN KEY (exchange_item_id)
      REFERENCES esn.exchange_item (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
