﻿/*
	ALTER TABLE discount.card_type_check_info ADD COLUMN print_inactive_bonus_value_card
*/

ALTER TABLE discount.card_type_check_info ADD COLUMN print_inactive_bonus_value_card boolean NOT NULL DEFAULT false;

-- select * from discount.card_type_check_info where card_type_id = 31