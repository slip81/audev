﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_service_registered
*/

-- DROP VIEW cabinet.vw_client_service_registered;

CREATE OR REPLACE VIEW cabinet.vw_client_service_registered AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS id,
    t3.client_id,
    t3.id AS sales_id,
    t2.id AS workplace_id,
    t2.description AS workplace_description,
    t1.activation_key,
    t1.license_id,
    t6.id AS service_id,
    t10.id AS version_id,
    t6.name AS service_name,    
    t10.name AS version_name,
    t6.description AS service_description
   FROM cabinet.service_registration t1
     JOIN cabinet.workplace t2 ON t1.workplace_id = t2.id
     JOIN cabinet.sales t3 ON t2.sales_id = t3.id
     JOIN cabinet.license t4 ON t1.id = t4.service_registration_id
     JOIN cabinet.order_item t5 ON t4.order_item_id = t5.id
     JOIN cabinet.service t6 ON t5.service_id = t6.id
     LEFT JOIN cabinet.version t10 ON t5.version_id = t10.id;

ALTER TABLE cabinet.vw_client_service_registered OWNER TO pavlov;
