﻿/*
	CREATE OR REPLACE VIEW esn.vw_exchange_file_log
*/

DROP VIEW esn.vw_exchange_file_log;

CREATE OR REPLACE VIEW esn.vw_exchange_file_log AS 
select 
row_number() over(order by t1.node_id) as id,
t1.node_id, t1.partner_id, t1.client_id, t1.sales_id, t1.partner_name, t1.client_name, t1.sales_address, t1.node_name, t2.exchange_item_id, t3.item_name,
t11.filename as filename_last, t11.period as period_last, t12.filename as filename_prev, t12.period as period_prev, (current_date - t11.period::date)::integer as days_cnt
from esn.vw_exchange_file_node t1
inner join esn.exchange_file_node_item t2 on t1.node_id = t2.node_id
inner join esn.exchange_item t3 on t2.exchange_item_id = t3.item_id
left join 
(
select max(log_num) as log_num, max(node) as node, max(filename) as filename, max(period) as period, exchange_item_id
from esn.exchange_file_log
group by node, exchange_item_id
) t11 on trim(t1.node_name) = trim(t11.node) and t2.exchange_item_id = t11.exchange_item_id
left join
(
select max(log_num) as log_num, max(node) as node, max(filename) as filename, max(period) as period, exchange_item_id
from esn.exchange_file_log
where log_id not in
(
select max(log_id)
from esn.exchange_file_log
group by node, exchange_item_id
)
group by node, exchange_item_id
) t12 on trim(t1.node_name) = trim(t12.node) and t2.exchange_item_id = t12.exchange_item_id
;

ALTER TABLE esn.vw_exchange_file_log OWNER TO pavlov;