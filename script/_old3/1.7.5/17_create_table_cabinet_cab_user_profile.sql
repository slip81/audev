﻿/*
	CREATE TABLE cabinet.cab_user_profile
*/

-- DROP TABLE cabinet.cab_user_profile;

CREATE TABLE cabinet.cab_user_profile
(
  profile_id serial NOT NULL,
  user_name character varying NOT NULL,
  start_page character varying,
  avatar character varying,
  CONSTRAINT cab_user_profile_pkey PRIMARY KEY (profile_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.cab_user_profile OWNER TO pavlov;
