﻿/*
	CREATE OR REPLACE FUNCTION discount.copy_programm
*/

-- DROP FUNCTION discount.copy_programm(bigint, bigint);

CREATE OR REPLACE FUNCTION discount.copy_programm(
    IN in_programm_id bigint,
    IN out_business_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
	SET search_path to 'discount';
	
	result := 0;    

	PERFORM export_programm_to_tmp(in_programm_id);

	result:= (SELECT import_programm_from_tmp(out_business_id));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.copy_programm(bigint, bigint) OWNER TO pavlov;
