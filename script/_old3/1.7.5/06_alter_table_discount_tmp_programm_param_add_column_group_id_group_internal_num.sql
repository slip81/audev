﻿/*
	ALTER TABLE discount.tmp_programm_param ADD COLUMN group_id
*/

ALTER TABLE discount.tmp_programm_param ADD COLUMN group_id integer;
ALTER TABLE discount.tmp_programm_param ADD COLUMN group_internal_num integer;
