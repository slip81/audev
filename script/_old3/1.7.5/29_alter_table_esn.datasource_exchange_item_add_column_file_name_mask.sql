﻿/*
	ALTER TABLE esn.datasource_exchange_item ADD COLUMN file_name_mask
*/

ALTER TABLE esn.datasource_exchange_item ADD COLUMN file_name_mask character varying;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.*' WHERE item_id = 0;

-- МЗ
UPDATE esn.datasource_exchange_item SET file_name_mask = '*.ost' WHERE ds_id = 101 and item_id = 1;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.mov' WHERE ds_id = 101 and item_id = 2;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.que' WHERE ds_id = 101 and item_id = 3;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*$CP_list$*.*' WHERE ds_id = 101 and item_id = 4;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*$Disc$*.*' WHERE ds_id = 101 and item_id = 5;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*$PR_List$*.*' WHERE ds_id = 101 and item_id = 6;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.*' WHERE ds_id = 101 and item_id > 6;

-- АСНА
UPDATE esn.datasource_exchange_item SET file_name_mask = '*_RST.txt' WHERE ds_id = 102 and item_id = 1;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.txt' WHERE ds_id = 102 and item_id = 2;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.*' WHERE ds_id = 102 and item_id = 3;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.*' WHERE ds_id = 102 and item_id = 4;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.*' WHERE ds_id = 102 and item_id = 5;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*.*' WHERE ds_id = 102 and item_id = 6;

UPDATE esn.datasource_exchange_item SET file_name_mask = 'vendor.dbf' WHERE ds_id = 102 and item_id = 7;

UPDATE esn.datasource_exchange_item SET file_name_mask = 'goods.dbf' WHERE ds_id = 102 and item_id = 8;

UPDATE esn.datasource_exchange_item SET file_name_mask = '*_AVG.txt' WHERE ds_id = 102 and item_id = 9;


-- select * from esn.datasource_exchange_item order by ds_id, item_id;

-- select * from esn.exchange_item order by item_id;
/*
0;"не определено";"undefined"
1;"Остатки";"ost"
2;"Движения";"move"
3;"Заказ";"que"
4;"Товары ЦП";"plan"
5;"Товары по акциям";"discount"
6;"Товары ПР";"recommend"
7;"Справочник КА";"client"
8;"Справочник номенклатуры";"prep"
9;"Данные накопительным итогом";"avg"
*/