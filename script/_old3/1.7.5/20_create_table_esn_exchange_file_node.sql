﻿/*
	CREATE TABLE esn.exchange_file_node
*/

-- DROP TABLE esn.exchange_file_node;

CREATE TABLE esn.exchange_file_node
(
  node_id serial NOT NULL,
  node_name character varying,
  node_descr character varying,
  node_parent character varying,
  partner_name character varying,
  CONSTRAINT exchange_file_node_pkey PRIMARY KEY (node_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_file_node OWNER TO pavlov;
