﻿/*
	CREATE TABLE discount.programm_param_group_scope
*/

-- DROP TABLE discount.programm_param_group_scope;

CREATE TABLE discount.programm_param_group_scope
(
  scope_id integer NOT NULL,
  scope_name character varying,
  CONSTRAINT programm_param_group_scope_pkey PRIMARY KEY (scope_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_param_group_scope OWNER TO pavlov;


INSERT INTO discount.programm_param_group_scope (scope_id, scope_name)
VALUES (1, 'Дисконтные параметры, обычные товары');

INSERT INTO discount.programm_param_group_scope (scope_id, scope_name)
VALUES (2, 'Бонусные параметры, обычные товары');

INSERT INTO discount.programm_param_group_scope (scope_id, scope_name)
VALUES (3, 'Дисконтные параметры, ЖНВЛП');

INSERT INTO discount.programm_param_group_scope (scope_id, scope_name)
VALUES (4, 'Бонусные параметры, ЖНВЛП');

-- select * from discount.programm_param_group_scope