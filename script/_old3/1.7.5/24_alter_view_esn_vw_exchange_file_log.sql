﻿/*
	CREATE OR REPLACE VIEW esn.vw_exchange_file_log
*/

CREATE OR REPLACE VIEW esn.vw_exchange_file_log AS 
 SELECT t1.log_id,
    t1.log_num,
    t1.node,
    t1.filename,
    t1.period,
    t1.crt_user,
    t1.crt_date,
    t12.ds_name AS partner_name,
    COALESCE(t11.node_parent, '[не задан]'::character varying) AS node_parent,
    COALESCE(t11.node_descr, '[не задан]'::character varying) AS node_descr,
    COALESCE(t11.node_name, '[не задан]'::character varying) AS node_name
   FROM esn.exchange_file_log t1
     LEFT JOIN esn.exchange_file_node t11 ON btrim(COALESCE(t1.node, ''::character varying)::text) = btrim(COALESCE(t11.node_name, ''::character varying)::text)
     LEFT JOIN esn.datasource t12 ON t11.partner_id = t12.ds_id
     ;
