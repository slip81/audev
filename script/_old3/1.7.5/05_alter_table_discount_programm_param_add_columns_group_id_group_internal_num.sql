﻿/*
	ALTER TABLE discount.programm_param ADD COLUMN group_id
*/

ALTER TABLE discount.programm_param ADD COLUMN group_id integer;
ALTER TABLE discount.programm_param ADD COLUMN group_internal_num integer;

ALTER TABLE discount.programm_param
  ADD CONSTRAINT programm_param_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES discount.programm_param_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
