﻿/*
	ALTER TABLE esn.exchange_file_node
*/

CREATE OR REPLACE VIEW esn.vw_exchange_file_log AS 
 SELECT t1.log_id,
    t1.log_num,
    t1.node,
    t1.filename,
    t1.period,
    t1.crt_user,
    t1.crt_date,
    '[не задан]'::character varying AS partner_name,
    COALESCE(t2.node_parent, '[не задан]'::character varying) AS node_parent,
    COALESCE(t2.node_descr, '[не задан]'::character varying) AS node_descr,
    COALESCE(t2.node_name, '[не задан]'::character varying) AS node_name
   FROM esn.exchange_file_log t1
     LEFT JOIN esn.exchange_file_node t2 ON btrim(COALESCE(t1.node, ''::character varying)::text) = btrim(COALESCE(t2.node_name, ''::character varying)::text);

ALTER TABLE esn.vw_exchange_file_log OWNER TO pavlov;

ALTER TABLE esn.exchange_file_node ADD COLUMN partner_id bigint;

ALTER TABLE esn.exchange_file_node
  ADD CONSTRAINT exchange_file_node_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES esn.datasource (ds_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE esn.exchange_file_node
SET partner_id = 101
WHERE partner_name like '%елодия%';

UPDATE esn.exchange_file_node
SET partner_id = 102
WHERE partner_name like '%АСНА%';

ALTER TABLE esn.exchange_file_node DROP COLUMN partner_name;

-- select * from esn.datasource