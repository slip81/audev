﻿/*
	CREATE OR REPLACE VIEW esn.vw_datasource_exchange_item
*/

-- DROP VIEW esn.vw_datasource_exchange_item;

CREATE OR REPLACE VIEW esn.vw_datasource_exchange_item AS 
 SELECT t1.rel_id,
  t1.ds_id,
  t1.item_id,
  t1.for_send,
  t1.for_get,
  t1.item_type_id,
  t1.item_file_name,
  t1.sql_text,
  t1.param,
  t1.file_name_mask,
  t2.ds_name,
  t3.item_name
  FROM esn.datasource_exchange_item t1
  INNER JOIN esn.datasource t2 ON t1.ds_id = t2.ds_id
  INNER JOIN esn.exchange_item t3 ON t1.item_id = t3.item_id
  ;

ALTER TABLE esn.vw_datasource_exchange_item OWNER TO pavlov;
