﻿/*
	RECREATE esn.exchange_file_...
*/

DROP VIEW esn.vw_exchange_file_log;

DROP TABLE esn.exchange_file_node_item;

DROP TABLE esn.exchange_file_node;

-------------------------

CREATE TABLE esn.exchange_file_node
(
  node_id serial NOT NULL,
  partner_id bigint NOT NULL,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  node_name character varying,
  CONSTRAINT exchange_file_node_pkey PRIMARY KEY (node_id),
  CONSTRAINT exchange_file_node_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT exchange_file_node_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES esn.datasource (ds_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT exchange_file_node_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_file_node OWNER TO pavlov;

-------------------------

CREATE TABLE esn.exchange_file_node_item
(
  item_id serial NOT NULL,
  node_id integer NOT NULL,
  exchange_item_id integer NOT NULL,
  file_name_mask character varying,
  CONSTRAINT exchange_file_node_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT exchange_file_node_item_exchange_item_id_fkey FOREIGN KEY (exchange_item_id)
      REFERENCES esn.exchange_item (item_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT exchange_file_node_item_node_id_fkey FOREIGN KEY (node_id)
      REFERENCES esn.exchange_file_node (node_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_file_node_item OWNER TO pavlov;

-------------------------

CREATE OR REPLACE VIEW esn.vw_exchange_file_node AS 
 SELECT t1.node_id,
    t1.partner_id,
    t1.client_id,
    t1.sales_id,
    t1.node_name,
    t2.name as client_name,
    coalesce(t3.adress, t3.name) as sales_address,
    t4.ds_name as partner_name    
   FROM esn.exchange_file_node t1
     INNER JOIN cabinet.client t2 ON t1.client_id = t2.id
     INNER JOIN cabinet.sales t3 ON t1.sales_id = t3.id
     INNER JOIN esn.datasource t4 ON t1.partner_id = t4.ds_id
     ;

ALTER TABLE esn.vw_exchange_file_node OWNER TO pavlov;

-------------------------

CREATE OR REPLACE VIEW esn.vw_exchange_file_log AS 
 SELECT t1.log_id,
    t1.log_num,
    t1.node,
    t1.filename,
    t1.period,
    t1.crt_user,
    t1.crt_date,
    t11.partner_name,
    COALESCE(t11.client_name, '[не задан]'::character varying) AS client_name,
    COALESCE(t11.sales_address, '[не задан]'::character varying) AS sales_address,
    COALESCE(t11.node_name, '[не задан]'::character varying) AS node_name
   FROM esn.exchange_file_log t1
     LEFT JOIN esn.vw_exchange_file_node t11 ON btrim(COALESCE(t1.node, ''::character varying)::text) = btrim(COALESCE(t11.node_name, ''::character varying)::text)
     ;

ALTER TABLE esn.vw_exchange_file_log OWNER TO pavlov;

-------------------------