﻿/*
	CREATE TABLE esn.exchange_file_log
*/

-- DROP TABLE esn.exchange_file_log;

CREATE TABLE esn.exchange_file_log
(
  log_id serial NOT NULL,
  log_num integer NOT NULL,
  node character varying,
  filename character varying,
  period character varying,
  crt_user character varying,
  crt_date timestamp without time zone,
  CONSTRAINT exchange_file_log_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.exchange_file_log OWNER TO pavlov;
