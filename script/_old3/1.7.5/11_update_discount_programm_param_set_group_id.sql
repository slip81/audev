﻿/*
	update discount.programm_param set group_id, group_internal_num
*/

-- select * from discount.programm_descr order by programm_id

/*
958535312714237234
1013493439297226694
1013709302667412750
1078849080945280646
1127323590446810461
1141758208247858607
1185485225149335090
1186898622071964808
1193273863354451327
1202133525197751368
1217103335568442417
1243397593396938709
*/

/*
select t2.descr_full,t1.* 
from discount.programm_param t1
inner join discount.programm t2 on t1.programm_id = t2.programm_id
where t2.programm_id = 1243397593396938709 and t1.is_internal = 0
order by t1.param_id
*/

/*
select * from discount.programm_param_group_scope
select * from discount.programm_param_group order by scope_id, group_id
*/

-- programm_id = 958535312714237234

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 958535312714237234 and param_id = 958535711257003355;

update discount.programm_param
set group_id = 1, group_internal_num = 2
where programm_id = 958535312714237234 and param_id = 958535711257003356;

update discount.programm_param
set group_id = 1, group_internal_num = 3
where programm_id = 958535312714237234 and param_id = 958535711257003357;

update discount.programm_param
set group_id = 1, group_internal_num = 4
where programm_id = 958535312714237234 and param_id = 958535711257003358;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 958535312714237234 and param_id = 958535711257003359;

update discount.programm_param
set group_id = 2, group_internal_num = 2 
where programm_id = 958535312714237234 and param_id = 958535711257003360;

update discount.programm_param
set group_id = 2, group_internal_num = 3 
where programm_id = 958535312714237234 and param_id = 958535711257003361;

update discount.programm_param
set group_id = 2, group_internal_num = 4 
where programm_id = 958535312714237234 and param_id = 958535711257003362;

-- programm_id = 1013493439297226694

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1013493439297226694 and param_id = 1013493439322392520;

update discount.programm_param
set group_id = 1, group_internal_num = 2
where programm_id = 1013493439297226694 and param_id = 1013493439330781129;

update discount.programm_param
set group_id = 1, group_internal_num = 3 
where programm_id = 1013493439297226694 and param_id = 1013493439339169738;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 1013493439297226694 and param_id = 1013493439339169739;

update discount.programm_param
set group_id = 2, group_internal_num = 2 
where programm_id = 1013493439297226694 and param_id = 1013493439347558348;

update discount.programm_param
set group_id = 2, group_internal_num = 3 
where programm_id = 1013493439297226694 and param_id = 1013493439355946957;

-- programm_id = 1078849080945280646

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1078849080945280646 and param_id = 1013709302717744515;

update discount.programm_param
set group_id = 1, group_internal_num = 2
where programm_id = 1078849080945280646 and param_id = 1013709302717744516;

update discount.programm_param
set group_id = 1, group_internal_num = 3
where programm_id = 1078849080945280646 and param_id = 1013709302717744517;

update discount.programm_param
set group_id = 1, group_internal_num = 4
where programm_id = 1078849080945280646 and param_id = 1013709302717744518;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 1078849080945280646 and param_id = 1013709302717744519;

update discount.programm_param
set group_id = 2, group_internal_num = 2
where programm_id = 1078849080945280646 and param_id = 1013709302717744520;

update discount.programm_param
set group_id = 2, group_internal_num = 3
where programm_id = 1078849080945280646 and param_id = 1013709302717744521;

update discount.programm_param
set group_id = 2, group_internal_num = 4
where programm_id = 1078849080945280646 and param_id = 1013709302717744522;

update discount.programm_param
set group_id = 12
where programm_id = 1078849080945280646 and param_id = 1013709302717744523;

-- programm_id = 1127323590446810461

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1127323590446810461 and param_id = 1013709302717744697;

update discount.programm_param
set group_id = 1, group_internal_num = 2 
where programm_id = 1127323590446810461 and param_id = 1013709302717744698;

update discount.programm_param
set group_id = 1, group_internal_num = 3 
where programm_id = 1127323590446810461 and param_id = 1013709302717744699;

update discount.programm_param
set group_id = 1, group_internal_num = 4 
where programm_id = 1127323590446810461 and param_id = 1013709302717744700;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 1127323590446810461 and param_id = 1013709302717744701;

update discount.programm_param
set group_id = 2, group_internal_num = 2 
where programm_id = 1127323590446810461 and param_id = 1013709302717744702;

update discount.programm_param
set group_id = 2, group_internal_num = 3 
where programm_id = 1127323590446810461 and param_id = 1013709302717744703;

update discount.programm_param
set group_id = 2, group_internal_num = 4 
where programm_id = 1127323590446810461 and param_id = 1013709302717744704;

update discount.programm_param
set group_id = 6
where programm_id = 1127323590446810461 and param_id = 1013709302717744706;

-- programm_id = 1185485225149335090

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1185485225149335090 and param_id = 1013709302717744848;

update discount.programm_param
set group_id = 1, group_internal_num = 2
where programm_id = 1185485225149335090 and param_id = 1013709302717744849;

update discount.programm_param
set group_id = 1, group_internal_num = 3
where programm_id = 1185485225149335090 and param_id = 1013709302717744850;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 1185485225149335090 and param_id = 1013709302717744851;

update discount.programm_param
set group_id = 2, group_internal_num = 2
where programm_id = 1185485225149335090 and param_id = 1013709302717744847;

update discount.programm_param
set group_id = 2, group_internal_num = 3 
where programm_id = 1185485225149335090 and param_id = 1013709302717744852;

update discount.programm_param
set group_id = 15 
where programm_id = 1185485225149335090 and param_id = 1013709302717744853;

-- programm_id =  1186898622071964808

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1186898622071964808 and param_id = 1013709302717744871;

update discount.programm_param
set group_id = 1, group_internal_num = 2
where programm_id = 1186898622071964808 and param_id = 1013709302717744872;

update discount.programm_param
set group_id = 1, group_internal_num = 3
where programm_id = 1186898622071964808 and param_id = 1013709302717744873;

update discount.programm_param
set group_id = 1, group_internal_num = 4
where programm_id = 1186898622071964808 and param_id = 1013709302717744883;

update discount.programm_param
set group_id = 2, group_internal_num = 1
where programm_id = 1186898622071964808 and param_id = 1013709302717744877;

update discount.programm_param
set group_id = 2, group_internal_num = 2
where programm_id = 1186898622071964808 and param_id = 1013709302717744878;

update discount.programm_param
set group_id = 2, group_internal_num = 3
where programm_id = 1186898622071964808 and param_id = 1013709302717744885;

update discount.programm_param
set group_id = 2, group_internal_num = 4
where programm_id = 1186898622071964808 and param_id = 1013709302717744879;

update discount.programm_param
set group_id = 16
where programm_id = 1186898622071964808 and param_id = 1013709302717744870;

-- programm_id = 1193273863354451327

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1193273863354451327 and param_id = 1013709302717744939;

update discount.programm_param
set group_id = 1, group_internal_num = 2
where programm_id = 1193273863354451327 and param_id = 1013709302717744940;

update discount.programm_param
set group_id = 1, group_internal_num = 3
where programm_id = 1193273863354451327 and param_id = 1013709302717744941;

update discount.programm_param
set group_id = 1, group_internal_num = 4
where programm_id = 1193273863354451327 and param_id = 1013709302717744942;

update discount.programm_param
set group_id = 1, group_internal_num = 5
where programm_id = 1193273863354451327 and param_id = 1013709302717744947;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 1193273863354451327 and param_id = 1013709302717744957;

update discount.programm_param
set group_id = 2, group_internal_num = 2
where programm_id = 1193273863354451327 and param_id = 1013709302717744943;

update discount.programm_param
set group_id = 2, group_internal_num = 3
where programm_id = 1193273863354451327 and param_id = 1013709302717744958;

update discount.programm_param
set group_id = 2, group_internal_num = 4
where programm_id = 1193273863354451327 and param_id = 1013709302717744944;

update discount.programm_param
set group_id = 2, group_internal_num = 5
where programm_id = 1193273863354451327 and param_id = 1013709302717744945;

update discount.programm_param
set group_id = 13, group_internal_num = 1 
where programm_id = 1193273863354451327 and param_id = 1013709302717744953;

update discount.programm_param
set group_id = 13, group_internal_num = 2
where programm_id = 1193273863354451327 and param_id = 1013709302717744954;

update discount.programm_param
set group_id = 13, group_internal_num = 3 
where programm_id = 1193273863354451327 and param_id = 1013709302717744955;

update discount.programm_param
set group_id = 14, group_internal_num = 1 
where programm_id = 1193273863354451327 and param_id = 1013709302717744950;

update discount.programm_param
set group_id = 14, group_internal_num = 2
where programm_id = 1193273863354451327 and param_id = 1013709302717744951;

update discount.programm_param
set group_id = 14, group_internal_num = 3
where programm_id = 1193273863354451327 and param_id = 1013709302717744952;

-- programm_id = 1202133525197751368

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1202133525197751368 and param_id = 1013709302717744969;

update discount.programm_param
set group_id = 1, group_internal_num = 2
where programm_id = 1202133525197751368 and param_id = 1013709302717744968;

update discount.programm_param
set group_id = 1, group_internal_num = 3 
where programm_id = 1202133525197751368 and param_id = 1013709302717744967;

update discount.programm_param
set group_id = 1, group_internal_num = 4 
where programm_id = 1202133525197751368 and param_id = 1013709302717744961;

update discount.programm_param
set group_id = 1, group_internal_num = 5 
where programm_id = 1202133525197751368 and param_id = 1013709302717744966;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 1202133525197751368 and param_id = 1013709302717744965;

update discount.programm_param
set group_id = 2, group_internal_num = 2
where programm_id = 1202133525197751368 and param_id = 1013709302717744964;

update discount.programm_param
set group_id = 2, group_internal_num = 3
where programm_id = 1202133525197751368 and param_id = 1013709302717744960;

update discount.programm_param
set group_id = 2, group_internal_num = 4
where programm_id = 1202133525197751368 and param_id = 1013709302717744963;

update discount.programm_param
set group_id = 2, group_internal_num = 5
where programm_id = 1202133525197751368 and param_id = 1013709302717744962;

update discount.programm_param
set group_id = 6
where programm_id = 1202133525197751368 and param_id = 1013709302717744959;

-- programm_id = 1217103335568442417

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1217103335568442417 and param_id = 1013709302717745010;

update discount.programm_param
set group_id = 1, group_internal_num = 2 
where programm_id = 1217103335568442417 and param_id = 1013709302717745011;

update discount.programm_param
set group_id = 1, group_internal_num = 3 
where programm_id = 1217103335568442417 and param_id = 1013709302717745012;

update discount.programm_param
set group_id = 1, group_internal_num = 4 
where programm_id = 1217103335568442417 and param_id = 1013709302717745018;

update discount.programm_param
set group_id = 1, group_internal_num = 5 
where programm_id = 1217103335568442417 and param_id = 1013709302717745013;

update discount.programm_param
set group_id = 1, group_internal_num = 6
where programm_id = 1217103335568442417 and param_id = 1013709302717745021;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 1217103335568442417 and param_id = 1013709302717745014;

update discount.programm_param
set group_id = 2, group_internal_num = 2
where programm_id = 1217103335568442417 and param_id = 1013709302717745015;

update discount.programm_param
set group_id = 2, group_internal_num = 3
where programm_id = 1217103335568442417 and param_id = 1013709302717745019;

update discount.programm_param
set group_id = 2, group_internal_num = 4
where programm_id = 1217103335568442417 and param_id = 1013709302717745016;

update discount.programm_param
set group_id = 2, group_internal_num = 5
where programm_id = 1217103335568442417 and param_id = 1013709302717745017;

update discount.programm_param
set group_id = 2, group_internal_num = 6
where programm_id = 1217103335568442417 and param_id = 1013709302717745022;

-- programm_id = 1243397593396938709

update discount.programm_param
set group_id = 1, group_internal_num = 1 
where programm_id = 1243397593396938709 and param_id = 1013709302717745047;

update discount.programm_param
set group_id = 1, group_internal_num = 2
where programm_id = 1243397593396938709 and param_id = 1013709302717745048;

update discount.programm_param
set group_id = 1, group_internal_num = 3
where programm_id = 1243397593396938709 and param_id = 1013709302717745049;

update discount.programm_param
set group_id = 1, group_internal_num = 4
where programm_id = 1243397593396938709 and param_id = 1013709302717745050;

update discount.programm_param
set group_id = 1, group_internal_num = 5
where programm_id = 1243397593396938709 and param_id = 1013709302717745052;

update discount.programm_param
set group_id = 2, group_internal_num = 1 
where programm_id = 1243397593396938709 and param_id = 1013709302717745043;

update discount.programm_param
set group_id = 2, group_internal_num = 2
where programm_id = 1243397593396938709 and param_id = 1013709302717745044;

update discount.programm_param
set group_id = 2, group_internal_num = 3
where programm_id = 1243397593396938709 and param_id = 1013709302717745045;

update discount.programm_param
set group_id = 2, group_internal_num = 4
where programm_id = 1243397593396938709 and param_id = 1013709302717745046;

update discount.programm_param
set group_id = 2, group_internal_num = 5
where programm_id = 1243397593396938709 and param_id = 1013709302717745051;