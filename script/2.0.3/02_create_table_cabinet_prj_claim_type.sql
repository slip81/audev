﻿/*
	CREATE TABLE cabinet.prj_claim_type
*/

-- DROP TABLE cabinet.prj_claim_type;

CREATE TABLE cabinet.prj_claim_type
(
  claim_type_id integer NOT NULL,
  claim_type_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),
  CONSTRAINT prj_claim_type_pkey PRIMARY KEY (claim_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_claim_type_set_stamp_trg ON cabinet.prj_claim_type;

CREATE TRIGGER cabinet_prj_claim_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_claim_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_claim_type (claim_type_id, claim_type_name)
VALUES (0, '[н/о]');

INSERT INTO cabinet.prj_claim_type (claim_type_id, claim_type_name)
VALUES (1, 'Ошибка');

INSERT INTO cabinet.prj_claim_type (claim_type_id, claim_type_name)
VALUES (2, 'Требование клиента');

-- delete from cabinet.prj_claim_type;
-- select * from cabinet.prj_claim_type order by claim_type_id;