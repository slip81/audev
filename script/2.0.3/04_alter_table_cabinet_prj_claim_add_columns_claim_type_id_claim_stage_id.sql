﻿/*
	ALTER TABLE cabinet.prj_claim ADD COLUMNs claim_type_id, claim_stage_id
*/

ALTER TABLE cabinet.prj_claim ADD COLUMN claim_type_id integer;

UPDATE cabinet.prj_claim SET claim_type_id = 0;
UPDATE cabinet.prj_claim SET claim_type_id = 1 WHERE priority_id in (1,2);

ALTER TABLE cabinet.prj_claim ALTER COLUMN claim_type_id SET NOT NULL;

ALTER TABLE cabinet.prj_claim
  ADD CONSTRAINT prj_claim_claim_type_id_fkey FOREIGN KEY (claim_type_id)
      REFERENCES cabinet.prj_claim_type (claim_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cabinet.prj_claim ADD COLUMN claim_stage_id integer;

UPDATE cabinet.prj_claim SET claim_stage_id = 0;
UPDATE cabinet.prj_claim SET claim_stage_id = 3 WHERE is_verified = true;

ALTER TABLE cabinet.prj_claim ALTER COLUMN claim_stage_id SET NOT NULL;

ALTER TABLE cabinet.prj_claim
  ADD CONSTRAINT prj_claim_claim_stage_id_fkey FOREIGN KEY (claim_stage_id)
      REFERENCES cabinet.prj_claim_stage (claim_stage_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      

UPDATE cabinet.prj_claim SET priority_id = 1 WHERE priority_id in (1,2);
UPDATE cabinet.prj_claim SET priority_id = 2 WHERE priority_id in (4);

UPDATE cabinet.crm_priority
SET priority_name = 'Критическая'
WHERE priority_id = 1;

UPDATE cabinet.crm_priority
SET priority_name = 'Планово-проектная'
WHERE priority_id = 2;

DELETE FROM cabinet.crm_priority WHERE priority_id NOT IN (0, 1, 2);

-- select * from cabinet.crm_priority order by priority_id;
-- select * from cabinet.prj_claim_type order by claim_type_id;
-- select * from cabinet.prj_claim_stage order by claim_stage_id;