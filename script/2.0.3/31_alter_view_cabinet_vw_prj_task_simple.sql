﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_task_simple
*/

-- DROP VIEW cabinet.vw_prj_task_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_task_simple AS 
 SELECT t1.task_id,
    t1.claim_id,
        CASE
            WHEN COALESCE(t11.work_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND COALESCE(t11.active_work_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND t11.work_cnt = t11.done_work_cnt THEN 3
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND COALESCE(t11.active_work_cnt, 0::bigint) <= 0 AND COALESCE(t11.done_work_cnt, 0::bigint) > 0 AND COALESCE(t11.canceled_work_cnt, 0::bigint) > 0 THEN 3
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND t11.work_cnt = t11.canceled_work_cnt THEN 4
            ELSE 2
        END AS task_state_type_id,
    t11.work_cnt,
    t11.active_work_cnt,
    t11.done_work_cnt,
    t11.canceled_work_cnt,
    t11.active_work_type_id,
    t11.exec_user_name_list
   FROM cabinet.prj_task t1
     LEFT JOIN ( SELECT count(x1.work_id) AS work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 0 THEN 1
                    ELSE 0
                END) AS active_work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 1 THEN 1
                    ELSE 0
                END) AS done_work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 2 THEN 1
                    ELSE 0
                END) AS canceled_work_cnt,
            min(
                CASE
                    WHEN x1.done_or_canceled = 0 THEN x1.work_type_id
                    ELSE NULL::integer
                END) AS active_work_type_id,
            array_to_string(array_agg(distinct x1.exec_user_name), ','::text) AS exec_user_name_list,
            x1.task_id
           FROM cabinet.vw_prj_work_simple x1
          GROUP BY x1.task_id) t11 ON t1.task_id = t11.task_id;

ALTER TABLE cabinet.vw_prj_task_simple OWNER TO pavlov;
