﻿/*
	ALTER TABLE discount.card_transaction_detail ALTER COLUMN unit_count NUMERIC
*/

ALTER TABLE discount.card_transaction_detail ALTER COLUMN unit_count TYPE numeric;
ALTER TABLE discount.programm_result_detail ALTER COLUMN unit_count TYPE numeric;