﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_log
*/

-- DROP VIEW cabinet.vw_prj_log;

CREATE OR REPLACE VIEW cabinet.vw_prj_log AS 
 SELECT t1.log_id,
    t1.mess,
    t1.prj_id,
    t1.claim_id,
    t1.task_id,
    t1.work_id,
    t1.crt_date,
    t1.crt_user,
    t12.claim_num,
    t13.task_num,
    t14.work_num,
    t12.prj_id AS claim_prj_id,
    t12.is_archive AS claim_is_archive,
    t11.is_archive AS prj_is_archive
   FROM cabinet.prj_log t1
     LEFT JOIN cabinet.prj t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_claim t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_task t13 ON t1.task_id = t13.task_id
     LEFT JOIN cabinet.prj_work t14 ON t1.work_id = t14.work_id;

ALTER TABLE cabinet.vw_prj_log OWNER TO pavlov;
