﻿/*
	CREATE TABLE cabinet.prj_claim_stage
*/

-- DROP TABLE cabinet.prj_claim_stage;

CREATE TABLE cabinet.prj_claim_stage
(
  claim_stage_id integer NOT NULL,
  claim_stage_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),
  CONSTRAINT prj_claim_stage_pkey PRIMARY KEY (claim_stage_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_stage OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_claim_stage_set_stamp_trg ON cabinet.prj_claim_stage;

CREATE TRIGGER cabinet_prj_claim_stage_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_claim_stage
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_claim_stage (claim_stage_id, claim_stage_name)
VALUES (0, '[н/о]');

INSERT INTO cabinet.prj_claim_stage (claim_stage_id, claim_stage_name)
VALUES (1, 'Новая');

INSERT INTO cabinet.prj_claim_stage (claim_stage_id, claim_stage_name)
VALUES (2, 'Утверждено');

INSERT INTO cabinet.prj_claim_stage (claim_stage_id, claim_stage_name)
VALUES (3, 'Принято');

-- delete from cabinet.prj_claim_stage;
-- select * from cabinet.prj_claim_stage order by claim_stage_id;  