﻿/*
	CREATE OR REPLACE FUNCTION cabinet.prj_user_group_item_update_sort
*/

-- DROP FUNCTION cabinet.prj_user_group_item_update_sort();

CREATE OR REPLACE FUNCTION cabinet.prj_user_group_item_update_sort()
  RETURNS trigger AS
$BODY$
BEGIN   
    IF TG_OP = 'INSERT' THEN
        RETURN NEW;
    ELSIF TG_OP = 'UPDATE' THEN
	--IF NEW.ord_prev IS NOT NULL AND NEW.ord >= NEW.ord_prev THEN -- переместили вниз
	IF NEW.group_id = NEW.group_id_prev AND NEW.ord >= NEW.ord_prev THEN -- переместили вниз
		UPDATE cabinet.prj_user_group_item
		SET ord = ord - 1
		WHERE group_id = NEW.group_id AND ord <= NEW.ord AND ord >= NEW.ord_prev AND item_id != NEW.item_id;		
	--ELSIF NEW.ord_prev IS NOT NULL AND NEW.ord < NEW.ord_prev THEN -- переместили вверх
	ELSIF NEW.group_id = NEW.group_id_prev AND NEW.ord < NEW.ord_prev THEN -- переместили вверх
		UPDATE cabinet.prj_user_group_item
		SET ord = ord + 1
		WHERE group_id = NEW.group_id AND ord >= NEW.ord  AND ord <= NEW.ord_prev AND item_id != NEW.item_id;
	ELSE -- переместили в другую группу
		UPDATE cabinet.prj_user_group_item
		SET ord = ord + 1
		WHERE group_id = NEW.group_id AND ord >= NEW.ord AND item_id != NEW.item_id;
		UPDATE cabinet.prj_user_group_item
		SET ord = ord - 1
		WHERE group_id = NEW.group_id_prev AND ord >= NEW.ord_prev AND item_id != NEW.item_id;		
	END IF;		
        RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN

	UPDATE cabinet.prj_user_group_item
	SET ord = ord - 1
	WHERE group_id = OLD.group_id AND ord >= OLD.ord AND item_id != OLD.item_id;
	    
        RETURN OLD;        
    END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.prj_user_group_item_update_sort() OWNER TO pavlov;
