﻿/*
	CREATE TRIGGER cabinet_prj_user_group_item_update_sort_trg
*/

-- DROP TRIGGER cabinet_prj_user_group_item_update_sort_trg ON cabinet.prj_user_group_item;

CREATE TRIGGER cabinet_prj_user_group_item_update_sort_trg
  AFTER INSERT OR DELETE OR UPDATE OF trigger_flag
  ON cabinet.prj_user_group_item
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.prj_user_group_item_update_sort();
