﻿/*
	CREATE OR REPLACE FUNCTION cabinet.report_client_version1
*/

-- DROP FUNCTION cabinet.report_client_version1(character varying, integer);

CREATE OR REPLACE FUNCTION cabinet.report_client_version1(
    IN _service_name character varying,
    IN _is_client_only integer)
  RETURNS TABLE(id integer, client_name character varying, sales_count bigint, region_name character varying, city_name character varying, version_min character varying, version_max character varying, date_reg_min date, date_reg_max date, sales_name character varying, kind integer) AS
$BODY$
DECLARE
 _service_id integer;
BEGIN
       _service_id := (select t1.id from cabinet.service t1 where t1.name = _service_name limit 1);
       --_service_id := 1;
       
	RETURN QUERY
 SELECT DISTINCT t1.id,
    t1.client_name,
    t1.sales_count,
    t1.region_name,
    t1.city_name,
    t2.version_name::character varying AS version_min,
    t3.version_name::character varying AS version_max,
    t4.date_reg_min::date AS date_reg_min,
    t4.date_reg_max::date AS date_reg_max,
    ''::character varying AS sales_name,
    1 AS kind
   FROM cabinet.allclients t1
     LEFT JOIN ( SELECT vw_client_min_version.id,
            min(vw_client_min_version.version_name) AS version_name
           FROM cabinet.vw_client_min_version
           WHERE service_name = _service_name
          GROUP BY vw_client_min_version.id) t2 ON t1.id = t2.id
     LEFT JOIN ( SELECT vw_client_max_version.id,
            max(vw_client_max_version.version_name) AS version_name
           FROM cabinet.vw_client_max_version
           WHERE service_name = _service_name
          GROUP BY vw_client_max_version.id) t3 ON t1.id = t3.id
     LEFT JOIN ( SELECT t2_1.client_id,
            min(t2_1.updated_on) AS date_reg_min,
            max(t2_1.updated_on) AS date_reg_max
           FROM cabinet.order_item t1_1
             JOIN cabinet."order" t2_1 ON t1_1.order_id = t2_1.id AND t2_1.active_status <> 1 AND t2_1.is_deleted <> 1
             JOIN cabinet.order_item t2_2 ON t2_1.id = t2_2.order_id AND t2_2.service_id = _service_id
          WHERE t1_1.is_deleted <> 1
          GROUP BY t2_1.client_id) t4 ON t1.id = t4.client_id
  WHERE _is_client_only = 1
  AND (t2.version_name IS NOT NULL OR t3.version_name IS NOT NULL)
UNION ALL
 SELECT DISTINCT t1.sales_id AS id,
    t1.client_name,
    1 AS sales_count,
    t1.region_name,
    t1.city_name,
    t2.version_name::character varying AS version_min,
    t3.version_name::character varying AS version_max,
    t4.date_reg_min::date AS date_reg_min,
    t4.date_reg_max::date AS date_reg_max,
    t1.adress::character varying AS sales_name,
    2 AS kind
   FROM cabinet.vw_sales t1
     LEFT JOIN ( SELECT vw_client_min_version.sales_id,
            min(vw_client_min_version.version_name) AS version_name
           FROM cabinet.vw_client_min_version
           WHERE service_name = _service_name
          GROUP BY vw_client_min_version.sales_id) t2 ON t1.sales_id = t2.sales_id
     LEFT JOIN ( SELECT vw_client_max_version.sales_id,
            max(vw_client_max_version.version_name) AS version_name
           FROM cabinet.vw_client_max_version
           WHERE service_name = _service_name
          GROUP BY vw_client_max_version.sales_id) t3 ON t1.sales_id = t3.sales_id
     LEFT JOIN ( SELECT t2_1.client_id,
            min(t2_1.updated_on) AS date_reg_min,
            max(t2_1.updated_on) AS date_reg_max
           FROM cabinet.order_item t1_1
             JOIN cabinet."order" t2_1 ON t1_1.order_id = t2_1.id AND t2_1.active_status <> 1 AND t2_1.is_deleted <> 1
             JOIN cabinet.order_item t2_2 ON t2_1.id = t2_2.order_id AND t2_2.service_id = _service_id
          WHERE t1_1.is_deleted <> 1
          GROUP BY t2_1.client_id) t4 ON t1.client_id = t4.client_id
  WHERE  _is_client_only != 1
  AND (t2.version_name IS NOT NULL OR t3.version_name IS NOT NULL);
		
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION cabinet.report_client_version1(character varying, integer) OWNER TO pavlov;
