﻿/*
	CREATE SEQUENCE esn.sys_uid_sequence
*/

-- DROP SEQUENCE esn.sys_uid_sequence;

CREATE SEQUENCE esn.sys_uid_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE esn.sys_uid_sequence OWNER TO pavlov;
