﻿/*
	CREATE OR REPLACE VIEW esn.vw_medical
*/

DROP VIEW esn.vw_medical;

CREATE OR REPLACE VIEW esn.vw_medical AS 
 SELECT t1.medical_id,
    t1.document_id,
    t1.publish_date,
    t1.medical_name,
    t1.reg_num,
    t1.reg_date,
    t1.producer,
    t1.add_document_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t11.document_name,
    t11.document_num,
    t11.link as document_link,
    t11.document_type_id,    
    t11.accept_date as document_accept_date,
    t11.create_date as document_create_date,
    t12.type_name AS document_type_name,    
    t13.document_name AS add_document_name,
    t13.document_num AS add_document_num,
    t13.link AS add_document_link,
    t13.document_type_id AS add_document_type_id,
    t13.accept_date as add_document_accept_date,
    t13.create_date as add_document_create_date,    
    t14.type_name AS add_document_type_name
   FROM esn.medical t1
     LEFT JOIN esn.document t11 ON t1.document_id = t11.document_id
     LEFT JOIN esn.document_type t12 ON t11.document_type_id = t12.type_id
     LEFT JOIN esn.document t13 ON t1.add_document_id = t13.document_id
     LEFT JOIN esn.document_type t14 ON t13.document_type_id = t14.type_id;

ALTER TABLE esn.vw_medical OWNER TO pavlov;
