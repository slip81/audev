﻿/*
	ALTER TABLE cabinet.prj ADD COLUMN is_archive
*/

ALTER TABLE cabinet.prj ADD COLUMN is_archive boolean NOT NULL DEFAULT false;
