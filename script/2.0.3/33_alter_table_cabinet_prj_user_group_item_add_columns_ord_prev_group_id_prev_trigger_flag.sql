﻿/*
	ALTER TABLE cabinet.prj_user_group_item ADD COLUMNs ord_prev,group_id_prev,trigger_flag
*/

ALTER TABLE cabinet.prj_user_group_item ADD COLUMN ord_prev integer;
ALTER TABLE cabinet.prj_user_group_item ADD COLUMN group_id_prev integer;
ALTER TABLE cabinet.prj_user_group_item ADD COLUMN trigger_flag boolean NOT NULL DEFAULT false;

ALTER TABLE cabinet.prj_user_group_item ALTER COLUMN ord SET NOT NULL;