﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim
*/

/*
        CASE
            WHEN t17.claim_id IS NULL THEN false
            ELSE true
        END AS is_overdue,
        CASE
            WHEN t1.crt_date >= ('now'::text::date - 1) THEN 1
            ELSE t18.state_type_id
        END AS brief_state_type_id,
        CASE
            WHEN t1.crt_date >= ('now'::text::date - 1) THEN 'Новые'::character varying
            ELSE t18.state_type_name
        END AS brief_state_type_name,
*/

-- DROP VIEW cabinet.vw_prj_claim;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim AS 
 SELECT t1.claim_id,
    t1.claim_num,
    t1.claim_name,
    t1.claim_text,
    t1.prj_id,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.module_version_id,
    t1.client_id,
    t1.priority_id,
    t1.resp_user_id,
    t1.date_plan,
    t1.date_fact,
    t1.repair_version_id,
    t1.is_archive,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.is_control,
    t1.old_task_id,
    t2.project_name,
    t3.module_name,
    t4.module_part_name,
    t5.module_version_name,
    t6.client_name,
    t7.priority_name,
    t8.user_name AS resp_user_name,
    t11.module_version_name AS repair_version_name,
    COALESCE(t12.task_cnt, 0::bigint) AS task_cnt,
    COALESCE(t12.new_task_cnt, 0::bigint) AS new_task_cnt,
    COALESCE(t12.active_task_cnt, 0::bigint) AS active_task_cnt,
    COALESCE(t12.done_task_cnt, 0::bigint) AS done_task_cnt,
    COALESCE(t12.canceled_task_cnt, 0::bigint) AS canceled_task_cnt,
    t12.claim_state_type_id,
    t13.state_type_name AS claim_state_type_name,
    t13.templ AS claim_state_type_templ,
    t9.prj_name,
    t9.date_beg AS prj_date_beg,
    t9.date_end AS prj_date_end,
    t9.curr_state_id AS prj_curr_state_id,
    t9.state_type_id AS prj_state_type_id,
    t9.state_type_name AS prj_state_type_name,
    t1.old_group_id,
    t1.old_state_id,
    t14.group_name AS old_group_name,
    t15.state_name AS old_state_name,
    t12.active_work_type_id,
    t16.work_type_name AS active_work_type_name,
        CASE
            WHEN t17.claim_id IS NULL THEN false
            ELSE true
        END AS is_overdue,
    t18.state_type_id AS brief_state_type_id,
    t18.state_type_name AS brief_state_type_name,
    t12.exec_user_name_list,
    t1.claim_type_id,
    t1.claim_stage_id,
    t50.claim_type_name,
    t51.claim_stage_name
   FROM cabinet.prj_claim t1
     JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id
     JOIN cabinet.crm_client t6 ON t1.client_id = t6.client_id
     JOIN cabinet.crm_priority t7 ON t1.priority_id = t7.priority_id
     JOIN cabinet.cab_user t8 ON t1.resp_user_id = t8.user_id
     JOIN cabinet.vw_prj t9 ON t1.prj_id = t9.prj_id
     JOIN cabinet.prj_claim_type t50 ON t1.claim_type_id = t50.claim_type_id
     JOIN cabinet.prj_claim_stage t51 ON t1.claim_stage_id = t51.claim_stage_id
     LEFT JOIN cabinet.crm_module_version t11 ON t1.repair_version_id = t11.module_version_id
     LEFT JOIN cabinet.vw_prj_claim_simple t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_claim_state_type t13 ON t12.claim_state_type_id = t13.state_type_id
     LEFT JOIN cabinet.crm_group t14 ON t1.old_group_id = t14.group_id
     LEFT JOIN cabinet.crm_state t15 ON t1.old_state_id = t15.state_id
     LEFT JOIN cabinet.prj_work_type t16 ON t12.active_work_type_id = t16.work_type_id
     LEFT JOIN LATERAL ( SELECT x2.claim_id,
            min(x1.date_plan) AS min_date_plan
           FROM cabinet.prj_task x1
             JOIN cabinet.prj_work x2 ON x1.task_id = x2.task_id
             JOIN cabinet.prj_work_state_type x3 ON x2.state_type_id = x3.state_type_id
          WHERE x3.done_or_canceled = 0
          GROUP BY x2.claim_id) t17 ON t1.claim_id = t17.claim_id AND GREATEST(COALESCE(t1.date_plan, '2000-01-01'::date), COALESCE(t17.min_date_plan, '2000-01-01'::date)) < 'now'::text::date
     LEFT JOIN cabinet.prj_brief_state_type t18 ON t13.brief_state_type_id = t18.state_type_id;

ALTER TABLE cabinet.vw_prj_claim OWNER TO pavlov;
