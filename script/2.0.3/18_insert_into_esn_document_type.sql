﻿/*
	insert into esn.document_type
*/


insert into esn.document_type (type_id, type_name)
values (2, 'Незарегистрированное МИ');

insert into esn.document_type (type_id, type_name)
values (3, 'С истекшим сроком годности');

insert into esn.document_type (type_id, type_name)
values (4, 'Не соответствующее установленным требованиям');

insert into esn.document_type (type_id, type_name)
values (5, 'Отзыв производителем');

insert into esn.document_type (type_id, type_name)
values (6, 'Приостановление применения МИ');

insert into esn.document_type (type_id, type_name)
values (7, 'Изъятие из обращения МИ');

insert into esn.document_type (type_id, type_name)
values (8, 'Возобновление применения МИ');

insert into esn.document_type (type_id, type_name)
values (9, 'Отмена информационного письма Росздравнадзора');

insert into esn.document_type (type_id, type_name)
values (10, 'Изменение письма Росздравнадзора');

insert into esn.document_type (type_id, type_name)
values (11, 'Фальсифицированное медицинское изделие');

insert into esn.document_type (type_id, type_name)
values (12, 'Недоброкачественное медицинское изделие');

insert into esn.document_type (type_id, type_name)
values (13, 'О безопасности медицинского изделия');

-- select * from esn.document_type order by type_id