﻿/*
	ALTER TABLE cabinet.prj_user_settings ADD COLUMN tag
*/

ALTER TABLE cabinet.prj_user_settings ADD COLUMN tag character varying;
