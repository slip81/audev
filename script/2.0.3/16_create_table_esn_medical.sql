﻿/*
	CREATE TABLE esn.medical
*/

-- DROP TABLE esn.medical;

CREATE TABLE esn.medical
(
  medical_id bigserial NOT NULL,
  document_id bigint,
  publish_date timestamp without time zone, 
  medical_name character varying,
  reg_num character varying,
  reg_date timestamp without time zone, 
  producer character varying,
  add_document_id bigint, 
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT esn.sysuidgen_scope1(),  
  CONSTRAINT medical_pkey PRIMARY KEY (medical_id),
  CONSTRAINT medical_document_id_fkey FOREIGN KEY (document_id)
      REFERENCES esn.document (document_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT medical_add_document_id_fkey FOREIGN KEY (add_document_id)
      REFERENCES esn.document (document_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.medical OWNER TO pavlov;
