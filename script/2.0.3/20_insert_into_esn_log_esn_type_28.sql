﻿/*
	insert into esn.log_esn_type 28
*/

insert into esn.log_esn_type (type_id, type_name)
values (28, 'Брак: скачивание мед.изделий');

update esn.log_esn_type
set type_name = 'Брак: скачивание с сайта'
where type_id = 2;

update esn.log_esn_type
set type_name = 'Брак: формирование XML после скачивания'
where type_id = 3;

-- select * from esn.log_esn_type order by type_id