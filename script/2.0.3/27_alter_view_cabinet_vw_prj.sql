﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj
*/

-- DROP VIEW cabinet.vw_prj;

CREATE OR REPLACE VIEW cabinet.vw_prj AS 
 SELECT t1.prj_id,
    t1.prj_name,
    t1.date_beg,
    t1.date_end,
    t1.curr_state_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    COALESCE(t11.claim_cnt, 0::bigint) AS claim_cnt,
    COALESCE(t11.new_claim_cnt, 0::bigint) AS new_claim_cnt,
    COALESCE(t11.active_claim_cnt, 0::bigint) AS active_claim_cnt,
    COALESCE(t11.done_claim_cnt, 0::bigint) AS done_claim_cnt,
    COALESCE(t11.canceled_claim_cnt, 0::bigint) AS canceled_claim_cnt,
    t12.state_type_id,
    t13.state_type_name,
    t13.templ AS state_type_name_templ,
    COALESCE(t14.claim_approved_cnt, 0::bigint) AS claim_approved_cnt,
    COALESCE(t15.claim_new_cnt, 0::bigint) AS claim_new_cnt,
    t1.is_archive
   FROM cabinet.prj t1
     LEFT JOIN ( SELECT x1.prj_id,
            count(x1.claim_id) AS claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 1 THEN 1
                    ELSE 0
                END) AS new_claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 2 THEN 1
                    ELSE 0
                END) AS active_claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 3 THEN 1
                    ELSE 0
                END) AS done_claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 4 THEN 1
                    ELSE 0
                END) AS canceled_claim_cnt
           FROM cabinet.vw_prj_claim_simple x1
          GROUP BY x1.prj_id) t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_state t12 ON t1.curr_state_id = t12.state_id
     LEFT JOIN cabinet.prj_state_type t13 ON t12.state_type_id = t13.state_type_id
     LEFT JOIN ( SELECT count(x1.claim_id) AS claim_approved_cnt,
            x1.prj_id
           FROM cabinet.prj_set_approved x1
          GROUP BY x1.prj_id) t14 ON t1.prj_id = t14.prj_id
     LEFT JOIN ( SELECT count(x1.claim_id) AS claim_new_cnt,
            x1.prj_id
           FROM cabinet.prj_set_new x1
          GROUP BY x1.prj_id) t15 ON t1.prj_id = t15.prj_id
     WHERE t1.is_archive = false;

ALTER TABLE cabinet.vw_prj OWNER TO pavlov;
