﻿/*
	ALTER TABLE cabinet.prj_claim DROP COLUMNs rate, is_verified, verified_date
*/

ALTER TABLE cabinet.prj_claim DROP COLUMN rate;
ALTER TABLE cabinet.prj_claim DROP COLUMN is_verified;
ALTER TABLE cabinet.prj_claim DROP COLUMN verified_date;