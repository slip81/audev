﻿/*
	insert into cabinet.prj_brief_interval_type 1
*/

insert into cabinet.prj_brief_interval_type (interval_type_id, interval_type_name)
select distinct 1, 'за 2 месяца'
from cabinet.prj_brief_interval_type t1 where not exists
(select interval_type_id from cabinet.prj_brief_interval_type where interval_type_id = 1);

-- select * from cabinet.prj_brief_interval_type order by interval_type_id;

update cabinet.prj_brief_type
set brief_type_name = 'Мониторинг задач'
where brief_type_id = 1;

-- select * from cabinet.prj_brief_type order by brief_type_id;