﻿/*
	update cabinet.service
*/

update cabinet.order_item
set version_id = (select max(x1.id) from cabinet.version x1 where x1.service_id = 27)
where version_id in
(
select id from cabinet.version where service_id = 41
);

update cabinet.order_item
set service_id = 27 where service_id = 41;

update cabinet.client_service
set service_id = 27 where service_id = 41;

delete from cabinet.version where service_id = 41;
delete from cabinet.service where id = 41;

update cabinet.service set description = 'ЕСН'
where id = 27;

update cabinet.service set is_deleted = 1
where id in (35, 44, 50);

-- select * from cabinet.service order by id;