﻿/*
	ALTER TABLE cabinet.prj_work_state_type ADD COLUMN brief_state_type_id
*/

ALTER TABLE cabinet.prj_work_state_type ADD COLUMN brief_state_type_id integer;

ALTER TABLE cabinet.prj_work_state_type
  ADD CONSTRAINT prj_work_state_type_brief_state_type_id_fkey FOREIGN KEY (brief_state_type_id)
      REFERENCES cabinet.prj_brief_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE cabinet.prj_work_state_type
SET brief_state_type_id = 2
WHERE state_type_id in (1, 4);

UPDATE cabinet.prj_work_state_type
SET brief_state_type_id = 4
WHERE state_type_id in (2, 3);

-- select * from cabinet.prj_brief_state_type order by state_type_id
-- select * from cabinet.prj_work_state_type order by state_type_id
