﻿/*
	CREATE OR REPLACE FUNCTION esn.sysuidgen_scope1
*/

-- DROP FUNCTION esn.sysuidgen_scope1();

CREATE OR REPLACE FUNCTION esn.sysuidgen_scope1(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (1 << 10) | (nextval('esn.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION esn.sysuidgen_scope1() OWNER TO pavlov;
