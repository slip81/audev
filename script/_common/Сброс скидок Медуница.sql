﻿/*
Дисконтная накопительная система. 
Скидка предоставляется в зависимости от накопленной суммы на карте:
от 0.01 руб - 3 %,
от 4 501 руб - 5 %,
от 7 501 руб - 6 %,
от 10 001 руб - 7 %.
В новогоднюю ночь все накопления сгорают и у всех карт скидка становится опять 3%.
*/

update discount.card_nominal
set date_end = '2018-01-01'
where card_id in
(
select t1.card_id
from discount.card t1
where t1.business_id = 958528103041205513
/*and coalesce(t1.curr_trans_sum, 0) >= 10001*/
)
and date_end is null 
and unit_type = 2
/*and unit_value != 7*/
;

insert into discount.card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
select t1.card_id, '2018-01-01', 2, 3, current_timestamp, 'pavlov'
from discount.card t1
where t1.business_id = 958528103041205513
/*and coalesce(t1.curr_trans_sum, 0) >= 10001*/
and not exists (select x1.card_nominal_id from discount.card_nominal x1 where x1.card_id = t1.card_id and x1.date_end is null and x1.unit_value = 3 and x1.unit_type = 2)
;

update discount.card
set curr_trans_count = 0, curr_trans_sum = 0
where t1.business_id = 958528103041205513;