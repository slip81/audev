﻿----------------------------------------
DROP TABLE cabinet.vw_license;
DROP TABLE cabinet.vw_license2;
DROP TABLE cabinet.vw_workplace;
DROP TABLE cabinet.allclients;
DROP TABLE esn.vw_defect;
DROP TABLE esn.vw_defect_dbf;
DROP TABLE cabinet.vw_cab_user;
DROP TABLE discount.vw_programm;
DROP TABLE discount.vw_report_check_count;
DROP TABLE discount.vw_report_card_info;
DROP TABLE discount.vw_card_type;
DROP TABLE discount.vw_card_transaction;
DROP TABLE discount.vw_card;
DROP TABLE discount.vw_business_org_user;
DROP TABLE esn.vw_log_esn_exchange_partner;
DROP TABLE esn.vw_ved_source_region;
DROP TABLE esn.vw_ved_reestr_item;
DROP TABLE esn.vw_ved_reestr_region_item;
DROP TABLE esn.vw_ved_reestr_item_out;
DROP TABLE esn.vw_ved_reestr_region_item_out;
DROP TABLE esn.vw_exchange_monitor;
DROP TABLE cabinet.vw_client_service;
DROP TABLE cabinet.vw_workplace_rereg;
DROP TABLE cabinet.vw_client_service_registered;
DROP TABLE cabinet.vw_client_service_unregistered;
DROP TABLE esn.vw_exchange_user;
DROP TABLE cabinet.vw_client_service_reg_an_unreg;
DROP TABLE esn.vw_exchange_client;
DROP TABLE cabinet.vw_cab_user2;
DROP TABLE cabinet.vw_crm_project;
DROP TABLE cabinet.vw_crm_priority;
DROP TABLE cabinet.vw_cab_grid_column_user_settings;
DROP TABLE discount.vw_card_type_business_org;
DROP TABLE discount.vw_business_summary;
DROP TABLE discount.vw_business_summary_graph;
DROP TABLE cabinet.vw_notify;
DROP TABLE discount.vw_programm_order;
DROP TABLE esn.vw_exchange_file_node;
DROP TABLE esn.vw_exchange_file_log;
DROP TABLE esn.vw_datasource_exchange_item;
DROP TABLE cabinet.vw_sales;
DROP TABLE cabinet.vw_client_user;
DROP TABLE cabinet.vw_service_cva_info;
DROP TABLE cabinet.vw_service_cva_data;
DROP TABLE cabinet.vw_client_user_widget;
DROP TABLE cabinet.vw_client_info;
DROP TABLE cabinet.vw_client_service_summary;
DROP TABLE discount.vw_business_summary_graph2;
DROP TABLE cabinet.vw_service;
DROP TABLE cabinet.vw_client_service_param;
DROP TABLE discount.vw_report_unused_cards;
DROP TABLE cabinet.vw_auth_user_perm;
DROP TABLE cabinet.vw_auth_user_role;
DROP TABLE cabinet.vw_auth_role_perm;
DROP TABLE cabinet.vw_auth_role_section_perm;
DROP TABLE cabinet.vw_auth_user_section_perm;
DROP TABLE cabinet.vw_auth_role;
DROP TABLE cabinet.vw_storage_file;
DROP TABLE cabinet.vw_log_storage;
DROP TABLE cabinet.vw_storage_folder_tree;
DROP TABLE cabinet.vw_storage_trash;
DROP TABLE cabinet.vw_storage_folder;
DROP TABLE cabinet.vw_service_kit_item;
DROP TABLE cabinet.vw_cab_help_tag;
DROP TABLE cabinet.vw_crm_notify_param_attr;
DROP TABLE cabinet.vw_crm_notify_param_content;
DROP TABLE cabinet.vw_crm_notify_param_user;
DROP TABLE cabinet.vw_client_compare_cabinet;
DROP TABLE cabinet.vw_client_compare_crm;
DROP TABLE cabinet.vw_client_max_version;
DROP TABLE cabinet.vw_client_min_version;
DROP TABLE discount.vw_campaign;
DROP TABLE discount.vw_campaign_business_org;
DROP TABLE cabinet.vw_update_batch;
DROP TABLE cabinet.vw_update_batch_item;
DROP TABLE cabinet.vw_prj_work_simple;
DROP TABLE cabinet.vw_prj_task_simple;
DROP TABLE cabinet.vw_prj_claim_simple;
DROP TABLE cabinet.vw_prj_work;
DROP TABLE cabinet.vw_prj_task;
DROP TABLE cabinet.vw_prj_claim;
DROP TABLE cabinet.vw_prj;
DROP TABLE cabinet.vw_prj_mess;
DROP TABLE cabinet.vw_prj_log;
DROP TABLE cabinet.vw_prj_work_default;
DROP TABLE und.vw_doc_product_filter;
DROP TABLE discount.vw_report_card_fstsale;
DROP TABLE discount.vw_card_holder;
DROP TABLE und.vw_partner_good;
DROP TABLE und.vw_product;
DROP TABLE und.vw_partner_good_filter;
DROP TABLE und.vw_product_and_good;
DROP TABLE und.vw_doc;
DROP TABLE und.vw_doc_import_match;
DROP TABLE und.vw_doc_import_match_variant;
DROP TABLE und.vw_doc_import;
DROP TABLE cabinet.vw_prj_file;
DROP TABLE und.vw_stock_row;
DROP TABLE und.vw_stock;
DROP TABLE und.vw_stock_batch;
DROP TABLE und.vw_stock_sales_download;
DROP TABLE cabinet.vw_update_soft;
DROP TABLE und.vw_asna_user;
DROP TABLE und.vw_asna_client;
DROP TABLE und.vw_wa_task;
DROP TABLE und.vw_asna_user_log;
DROP TABLE und.vw_wa_task_lc;
DROP TABLE und.vw_asna_stock_row;
DROP TABLE und.vw_asna_stock_row_linked;
DROP TABLE und.vw_asna_stock_chain;
DROP TABLE und.vw_asna_stock_row_actual;
DROP TABLE und.vw_asna_order;
DROP TABLE und.vw_asna_order_row;
DROP TABLE und.vw_asna_order_row_state;
DROP TABLE und.vw_asna_order_state;
DROP TABLE und.vw_asna_price_row;
DROP TABLE und.vw_asna_price_row_linked;
DROP TABLE und.vw_asna_price_chain;
DROP TABLE und.vw_asna_price_row_actual;
DROP TABLE cabinet.vw_region;
DROP TABLE cabinet.vw_region_zone;
DROP TABLE cabinet.vw_region_price_limit;
DROP TABLE cabinet.vw_region_zone_price_limit;
DROP TABLE discount.vw_programm_step_param;
DROP TABLE discount.vw_programm_step_condition;
DROP TABLE discount.vw_programm_step_logic;
DROP TABLE cabinet.vw_prj_user_group_item;
DROP TABLE cabinet.vw_prj_note;
DROP TABLE cabinet.vw_client_report1;
DROP TABLE esn.vw_medical;
DROP TABLE cabinet.vw_prj_task_user_state;
DROP TABLE cabinet.vw_service_esn2;
DROP TABLE cabinet.vw_version_main;
DROP TABLE cabinet.vw_prj_project_version;
DROP TABLE cabinet.vw_prj_claim_filter_item;
DROP TABLE cabinet.vw_prj_report1;
DROP TABLE und.vw_stock_log;
----------------------------------------
-- DROP VIEW cabinet.allclients;

CREATE OR REPLACE VIEW cabinet.allclients AS 
 SELECT t1.id,
    t1.name AS client_name,
    t1.cell AS phone,
    t2.id AS user_id,
    t2.name AS user_name,
    t4.id AS city_id,
    t4.name AS city_name,
    t4.region_id,
    t5.name AS region_name,
    t1.created_on AS reg_date,
    t2."lastActivityDate" AS last_activity_date,
    t1.created_by,
    1::smallint AS is_activated,
    t6.sales_count,
    t1.is_phis,
    t1.is_moderated,
    t1.contact_person,
    t7.business_id::text AS business_id,
    t1.inn,
    t1.legal_address,
    t1.absolute_address,
    t1.site,
    t1.kpp,
    t8.state_id,
    t8.state_name,
    t1.sm_id,
    t1.region_zone_id,
    t1.tax_system_type_id
   FROM cabinet.client t1
     LEFT JOIN cabinet.my_aspnet_users t2 ON t1.user_id = t2.id
     LEFT JOIN cabinet.my_aspnet_membership t3 ON t2.id = t3."userId"
     LEFT JOIN cabinet.city t4 ON t1.city_id = t4.id
     LEFT JOIN cabinet.region t5 ON t4.region_id = t5.id
     LEFT JOIN ( SELECT x1.client_id,
            count(x1.id) AS sales_count
           FROM cabinet.sales x1
          GROUP BY x1.client_id) t6 ON t1.id = t6.client_id
     LEFT JOIN discount.business t7 ON t1.id = t7.cabinet_client_id
     LEFT JOIN cabinet.client_state t8 ON t1.state_id = t8.state_id
  WHERE t1.is_deleted = 0;

ALTER TABLE cabinet.allclients OWNER TO pavlov;
----------------------------------------

-- DROP VIEW cabinet.vw_license;

CREATE OR REPLACE VIEW cabinet.vw_license AS 
select
        t5.id AS client_id,
        t5.full_name AS client_full_name,
        t4.id AS sales_id,
        t4.name AS sales_name,
        t3.id AS workplace_id,
        t3.registration_key AS workplace_reg_key,
        t3.description AS workplace_descr,
        t7.id AS order_id,
        t7.order_status AS order_status,
        t7.is_conformed AS order_is_confirmed,
        t7.active_status AS order_active_status,
        t8.id AS version_id,
        t8.name AS version_name,
        t9.id AS service_id,
        t9.name AS service_name,
        t6.id AS order_item_id,
        t6.quantity AS order_item_quantity,
        t2.id AS service_registration_id,
        t1.id AS license_id,
        t1.created_on AS license_created_on,
        t1.dt_start AS license_date_start,
        t1.dt_end AS license_date_end,
        t4.adress AS sales_address
from cabinet.license t1
inner join cabinet.service_registration t2 on t1.service_registration_id = t2.id
inner join cabinet.workplace t3 on t2.workplace_id = t3.id
inner join cabinet.sales t4 on t3.sales_id = t4.id
inner join cabinet.client t5 on t4.client_id = t5.id
inner join cabinet.order_item t6 on t1.order_item_id = t6.id
inner join cabinet.order t7 on t6.order_id = t7.id
inner join cabinet.version t8 on t6.version_id = t8.id
inner join cabinet.service t9 on t8.service_id = t9.id
;

ALTER TABLE cabinet.vw_license OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_license2;

CREATE OR REPLACE VIEW cabinet.vw_license2 AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS id,
    t3.client_id,
    t1.id AS license_id,
    t1.dt_start,
    t1.dt_end,
    t1.service_registration_id,
    t4.activation_key,
    t5.id AS workplace_id,
    t5.registration_key,
    t5.sales_id,
    t2.id AS order_item_id,
    t2.service_id,
    t2.version_id,
    t2.quantity,
    t3.id AS order_id,
    t3.is_conformed,
    t3.active_status,
    t11.name AS user_name,
    t5.workplace_type
   FROM cabinet.license t1
     JOIN cabinet.order_item t2 ON t1.order_item_id = t2.id
     JOIN cabinet."order" t3 ON t2.order_id = t3.id
     JOIN cabinet.service_registration t4 ON t1.service_registration_id = t4.id
     JOIN cabinet.workplace t5 ON t4.workplace_id = t5.id
     JOIN cabinet.sales t6 ON t5.sales_id = t6.id
     LEFT JOIN cabinet.my_aspnet_users t11 ON t6.user_id = t11.id
  WHERE t1.is_deleted <> 1;

ALTER TABLE cabinet.vw_license2 OWNER TO pavlov;

----------------------------------------
-- DROP VIEW cabinet.vw_workplace;

CREATE OR REPLACE VIEW cabinet.vw_workplace AS 
 SELECT t1.id AS workplace_id,
    t1.registration_key,
    t1.description,
    t1.sales_id,
    t2.name AS sales_name,
    t2.adress AS sales_address,
    t2.client_id,
    t3.client_name,
    t4.name AS user_name,
    t1.workplace_type
   FROM cabinet.workplace t1
     JOIN cabinet.sales t2 ON t1.sales_id = t2.id
     JOIN cabinet.allclients t3 ON t2.client_id = t3.id
     LEFT JOIN cabinet.my_aspnet_users t4 ON t2.user_id = t4.id
  WHERE t1.is_deleted = 0;

ALTER TABLE cabinet.vw_workplace OWNER TO pavlov;

----------------------------------------

-- DROP VIEW esn.vw_defect;

CREATE OR REPLACE VIEW esn.vw_defect AS 
 SELECT   
  t1.defect_id,
  t1.comm_name,
  t1.pack_name,
  t1.full_name,
  t1.series,
  t1.producer,
  t1.defect_status_id,
  t1.defect_type,
  t1.country_id,
  t1.document_id,
  t1.parent_defect_id,
  t1.defect_status_text,
  t1.scope,
  t1.is_drug,
  t1.old_id,
  t1.created_on,
  t1.created_by,
  t1.updated_on,
  t1.updated_by,
  t1.defect_source_id,
  t2.document_name,
  t2.document_num,
  t2.accept_date,
  t2.create_date,
  t2.link,
  t2.comment,
  t2.document_type_id,
  t2.body_size_kb,
  t2.created_on as doc_created_on,
  t2.created_by as doc_created_by,
  t2.updated_on as doc_updated_on,
  t2.updated_by as doc_updated_by,
  t2.old,
  t3.status_name,
  t4.source_name,
  t5.name as country_name
  FROM esn.defect t1
     LEFT JOIN esn.document t2 ON t1.document_id = t2.document_id
     LEFT JOIN esn.defect_status t3 ON t1.defect_status_id = t3.status_id
     LEFT JOIN esn.defect_source t4 ON t1.defect_source_id = t4.source_id
     LEFT JOIN esn.prep_country t5 ON t1.country_id = t5.country_id;

ALTER TABLE esn.vw_defect  OWNER TO pavlov;

----------------------------------------

-- DROP VIEW esn.vw_defect_dbf;

CREATE OR REPLACE VIEW esn.vw_defect_dbf
AS
select 
cast(defect_id as bigint) AS "ID",
cast(0 as bigint) AS "MNFGRNX",
cast(0 as bigint) AS "MNFNX",
cast(coalesce(producer, '_') as character varying(254)) AS "MNFNMR",
cast(coalesce(country_name, '_') as character varying(150)) AS "COUNTRYR",
cast(coalesce(full_name, '_') as character varying(254)) AS "DRUGTXT",
cast(coalesce(series, '_') as character varying(50)) AS "SERNM",
cast(coalesce(document_num, '_') as character varying(50)) AS "LETTERSNR",
cast((coalesce(accept_date, '01.01.1980')) as date) AS "LETTERSDT",
cast('_' as character varying(200)) AS "LABNMR",
--cast(coalesce(case coalesce(defect_status_id, 0) when 101 then trim((coalesce(status_name, '') || ' ' || coalesce(defect_status_text, '')), '_') as character varying(254)) AS "QUALNMR",
cast(case coalesce(defect_status_id, 0) when 101 then trim(coalesce(defect_status_text, '_')) else trim(coalesce(defect_status_text, '') || ' ' || coalesce(status_name, '')) end  as character varying(254)) AS "QUALNMR",
cast(0 as bigint) AS "TRADENMNX",
cast(0 as bigint) AS "INNNX",
cast(0 as bigint) AS "ISALLS",
cast('_' as character varying(80)) AS "DISTRIB",
cast(coalesce(defect_type, '_') as character varying(80)) AS "STATDEF"
from esn.vw_defect
where is_drug = 1
;

ALTER TABLE esn.vw_defect_dbf OWNER TO pavlov;

--------------------

-- DROP VIEW discount.vw_card_holder;

CREATE OR REPLACE VIEW discount.vw_card_holder AS 
 SELECT t1.card_holder_id,
    t1.card_holder_surname,
    t1.card_holder_name,
    t1.card_holder_fname,
    t1.date_birth,
    t1.phone_num,
    t1.address,
    t1.email,
    t1.comment,
    t1.business_id,
    btrim((((t1.card_holder_surname::text || ' '::text) || t1.card_holder_name::text) || ' '::text) || t1.card_holder_fname::text) AS card_holder_full_name
   FROM discount.card_holder t1;

ALTER TABLE discount.vw_card_holder OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW discount.vw_card;

CREATE OR REPLACE VIEW discount.vw_card AS 
 SELECT t1.card_id,
    t1.date_beg,
    t1.date_end,
    t1.curr_card_status_id,
    t1.business_id,
    t1.curr_trans_count,
    t1.card_num,
    t1.card_num2,
    t1.curr_trans_sum,
    t1.curr_holder_id,
    t1.curr_card_type_id,
    t1.mess,
    t2.unit_value AS curr_bonus_value,
    t3.unit_value AS curr_bonus_percent,
    t4.unit_value AS curr_discount_percent,
    t5.unit_value AS curr_money_value,
    t6.unit_value AS curr_discount_value,
    NULL::numeric AS curr_money_percent,
    t7.card_type_name AS curr_card_type_name,
    t8.card_status_name AS curr_card_status_name,
    t9.card_holder_surname AS curr_holder_name,
    t1.card_id::character varying AS card_id_str,
    t1.card_num::character varying AS card_num_str,
    t7.card_type_group_id,
    t1.source_card_id,
    t9.card_holder_name AS curr_holder_first_name,
    t9.card_holder_fname AS curr_holder_second_name,
    t9.date_birth AS curr_holder_date_birth,
    t1.crt_date,
    t1.crt_user,
    t1.from_au,
    t10.unit_value AS curr_inactive_bonus_value,
    NULLIF(COALESCE(t2.unit_value, 0::numeric) + COALESCE(t10.unit_value, 0::numeric), 0::numeric) AS curr_all_bonus_value,
    t9.card_holder_full_name::character varying AS curr_holder_full_name,
    t9.phone_num AS curr_holder_phone_num,
    t9.address AS curr_holder_address,
    t9.email AS curr_holder_email
   FROM discount.card t1
     LEFT JOIN discount.card_nominal t2 ON t1.card_id = t2.card_id AND t2.unit_type = 1 AND t2.date_end IS NULL
     LEFT JOIN discount.card_nominal t3 ON t1.card_id = t3.card_id AND t3.unit_type = 4 AND t3.date_end IS NULL
     LEFT JOIN discount.card_nominal t4 ON t1.card_id = t4.card_id AND t4.unit_type = 2 AND t4.date_end IS NULL
     LEFT JOIN discount.card_nominal t5 ON t1.card_id = t5.card_id AND t5.unit_type = 3 AND t5.date_end IS NULL
     LEFT JOIN discount.card_nominal t6 ON t1.card_id = t6.card_id AND t6.unit_type = 5 AND t6.date_end IS NULL
     LEFT JOIN discount.card_type t7 ON t1.curr_card_type_id = t7.card_type_id
     LEFT JOIN discount.card_status t8 ON t1.curr_card_status_id = t8.card_status_id
     LEFT JOIN discount.vw_card_holder t9 ON t1.curr_holder_id = t9.card_holder_id
     LEFT JOIN ( SELECT sum(x1.unit_value) AS unit_value,
            x1.card_id,
            x1.unit_type
           FROM discount.card_nominal_inactive x1
          GROUP BY x1.card_id, x1.unit_type) t10 ON t1.card_id = t10.card_id AND t10.unit_type = 1;

ALTER TABLE discount.vw_card OWNER TO pavlov;
--------------------
-- DROP VIEW discount.vw_report_check_count;

CREATE OR REPLACE VIEW discount.vw_report_check_count AS 
 SELECT row_number() OVER (ORDER BY t1.business_id) AS id,
    t1.business_id,
    t1.card_id,
    t1.card_num,
    t1.curr_discount_percent,
    t1.curr_trans_sum,
    t2.date_check::date AS date_beg,
    t2.card_trans_id,
    t3.unit_value_with_discount,
    t3.unit_value_discount,
    t3.unit_value,
    t3.detail_id,
    t5.card_holder_surname AS card_holder_name,
    t6.card_type_name,
    t1.curr_bonus_percent,
    t3.unit_value_bonus_for_card,
    t3.unit_value_bonus_for_pay,
    t1.curr_bonus_value
   FROM discount.vw_card t1
     JOIN discount.card_transaction t2 ON t1.card_id = t2.card_id AND t2.trans_kind = 1 AND COALESCE(t2.status, 0) = 0
     JOIN discount.card_transaction_detail t3 ON t2.card_trans_id = t3.card_trans_id
     LEFT JOIN discount.card_holder t5 ON t1.curr_holder_id = t5.card_holder_id
     LEFT JOIN discount.card_type t6 ON t1.curr_card_type_id = t6.card_type_id
  WHERE 1 = 1
  ORDER BY t1.business_id, t1.card_id, t1.card_num, t1.curr_discount_percent, t1.curr_trans_sum, t2.date_check::date;

ALTER TABLE discount.vw_report_check_count OWNER TO pavlov;

--------------------


--DROP VIEW discount.vw_report_card_info;

CREATE OR REPLACE VIEW discount.vw_report_card_info AS 
 SELECT row_number() OVER (ORDER BY t1.business_id) AS id,
    t1.business_id,
    t1.card_id,
    t1.card_num,
    t1.curr_discount_percent,
    t1.curr_trans_sum,
    t5.card_holder_surname AS card_holder_name,
    count(DISTINCT t2.card_trans_id) AS curr_trans_count
   FROM discount.vw_card t1
     LEFT JOIN discount.card_transaction t2 ON t1.card_id = t2.card_id AND t2.trans_kind = 1 AND COALESCE(t2.status, 0) = 0
     /*LEFT JOIN discount.card_holder_card_rel t4 ON t1.card_id = t4.card_id AND t4.date_beg <= 'now'::text::date AND (t4.date_end IS NULL OR t4.date_end > 'now'::text::date)*/
     LEFT JOIN discount.card_holder t5 ON t1.curr_holder_id = t5.card_holder_id
  WHERE 1 = 1
  GROUP BY t1.business_id, t1.card_id, t1.card_num, t1.curr_discount_percent, t1.curr_trans_sum, t5.card_holder_surname;

ALTER TABLE discount.vw_report_card_info OWNER TO pavlov;

--------------------
-- DROP VIEW discount.vw_card_type;

CREATE OR REPLACE VIEW discount.vw_card_type AS 
 SELECT t1.card_type_id,
    t1.card_type_name,
    t1.business_id,
    t1.is_fin_spec,
    t1.card_type_group_id,
    t2.group_name,
    t4.programm_id,
    t4.programm_name,
    t4.descr_short,
    t4.descr_full,
    t1.update_nominal,
    t1.reset_interval_bonus_value,
    t1.reset_date,
    t1.is_reset_without_sales
   FROM discount.card_type t1
     LEFT JOIN discount.card_type_group t2 ON COALESCE(t1.card_type_group_id, 0::bigint) = t2.card_type_group_id
     LEFT JOIN discount.card_type_programm_rel t3 ON t1.card_type_id = t3.card_type_id AND t3.date_beg <= 'now'::text::date AND (t3.date_end IS NULL OR t3.date_end > 'now'::text::date)
     LEFT JOIN discount.programm t4 ON t3.programm_id = t4.programm_id;

ALTER TABLE discount.vw_card_type OWNER TO pavlov;

--------------------
-- DROP VIEW discount.vw_card_transaction;

CREATE OR REPLACE VIEW discount.vw_card_transaction AS 
 SELECT t1.card_trans_id,
    t1.card_id,
    t1.trans_num,
    t1.date_beg,
    t1.trans_sum,
    t1.trans_kind,
    t1.status,
    t1.canceled_trans_id,
    t1.programm_result_id,
    t1.user_name,
    t1.date_check,
    t2.business_id,
    t1.status_date,
    t1.is_delayed,
    t2.curr_trans_sum AS curr_card_sum,
    t2.card_num,
    t2.card_num2,
    ''::character varying AS curr_card_type_name,
    date(t1.date_check) AS date_check_date_only,
    0::numeric AS curr_card_bonus_value,
    0::numeric AS curr_card_percent_value,
    0::numeric AS trans_card_bonus_value,
    0::numeric AS trans_card_percent_value,
    sum(t6.unit_value_discount) AS trans_sum_discount,
    sum(
        CASE t6.unit_type
            WHEN 1 THEN 0::numeric
            ELSE t6.unit_value_with_discount
        END) AS trans_sum_with_discount,
    sum(t6.unit_value_bonus_for_card) AS trans_sum_bonus_for_card,
    sum(t6.unit_value_bonus_for_pay) AS trans_sum_bonus_for_pay,
    y2.org_name
   FROM discount.card_transaction t1
     JOIN discount.card t2 ON t1.card_id = t2.card_id
     LEFT JOIN discount.business_org_user y1 ON btrim(lower(COALESCE(y1.user_name, ''::character varying)::text)) = btrim(lower(COALESCE(t1.user_name, ''::character varying)::text))
     LEFT JOIN discount.business_org y2 ON y1.org_id = y2.org_id
     LEFT JOIN discount.card_transaction_detail t6 ON t1.card_trans_id = t6.card_trans_id
  GROUP BY t1.card_trans_id, t1.card_id, t1.trans_num, t1.date_beg, t1.trans_sum, t1.trans_kind, t1.status, t1.canceled_trans_id, t1.programm_result_id, t1.user_name, t1.date_check, t2.business_id, t2.curr_trans_sum, t2.card_num, t2.card_num2, y2.org_name;

ALTER TABLE discount.vw_card_transaction OWNER TO pavlov;

--------------------

-- DROP VIEW discount.vw_programm;

CREATE OR REPLACE VIEW discount.vw_programm AS 
SELECT 
t1.programm_id,
t1.programm_name,
t1.date_beg,
t1.date_end,
t1.descr_short,
t1.business_id,
t1.descr_full,
t1.parent_template_id,
t1.card_type_group_id,
t2.group_name
FROM discount.programm t1
LEFT JOIN discount.card_type_group t2 ON coalesce(t1.card_type_group_id, 0) = t2.card_type_group_id
;

ALTER TABLE discount.vw_programm OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_cab_user;

CREATE OR REPLACE VIEW cabinet.vw_cab_user AS 
 SELECT t1.item_id,
    t1.user_name,
    t1.role_id,
    t1.use_defaults,
    t2.role_name,
    t4.org_name,
    t5.business_name,
    t5.business_id,
    t3.user_descr
   FROM cabinet.cab_user_role t1
     JOIN cabinet.cab_role t2 ON t1.role_id = t2.role_id
     LEFT JOIN discount.business_org_user t3 ON btrim(COALESCE(t1.user_name, ''::character varying)::text) = btrim(t3.user_name::text)
     LEFT JOIN discount.business_org t4 ON t3.org_id = t4.org_id
     LEFT JOIN discount.business t5 ON t4.business_id = t5.business_id;

ALTER TABLE cabinet.vw_cab_user OWNER TO pavlov;

--------------------

-- DROP VIEW discount.vw_business_org_user;

CREATE OR REPLACE VIEW discount.vw_business_org_user AS 
select
  t2.org_user_id,
  t2.user_name, 
  t2.user_descr,
  t1.org_id,
  t1.org_name,
  t1.business_id,
  t1.add_to_timezone,
  t1.card_scan_only,
  t1.scanner_equals_reader,
  t1.allow_card_add
from discount.business_org t1
inner join discount.business_org_user t2 on t1.org_id = t2.org_id
;

ALTER TABLE discount.vw_business_org_user OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_log_esn_exchange_partner;

CREATE OR REPLACE VIEW esn.vw_log_esn_exchange_partner AS
 SELECT t3.client_id,
    t3.client_name,
    t2.user_id,
    date_trunc('minute'::text, t1.date_beg) AS date_beg_minutes,
    date_part('epoch'::text, date_trunc('minute'::text, t1.date_beg))::bigint AS date_id,
    t1.log_id,
    t1.date_beg,
    t1.mess,
    t1.user_name,
    t1.log_esn_type_id,
    t1.obj_id,
    t1.scope,
    t1.date_end,
    t1.mess2
   FROM esn.log_esn t1
     JOIN esn.exchange_user t2 ON t1.user_name::text = t2.login::text
     JOIN esn.exchange_client t3 ON t2.client_id = t3.client_id
  WHERE t1.scope = 4;

ALTER TABLE esn.vw_log_esn_exchange_partner OWNER TO pavlov;


--------------------

-- DROP VIEW esn.vw_ved_source_region;

CREATE OR REPLACE VIEW esn.vw_ved_source_region AS 
 SELECT t1.item_id,    
    t1.region_id,
    t1.url_site,
    t1.url_file_main,
    t1.url_file_res,
    t1.descr,
    t1.arc_type,
    t2.name AS region_name,
    t1.url_file_a_content,
    t1.template_id,
    t1.out_template_id
   FROM esn.ved_source_region t1
     LEFT JOIN cabinet.region t2 ON t1.region_id = t2.id;


ALTER TABLE esn.vw_ved_source_region OWNER TO pavlov;


--------------------

-- DROP VIEW esn.vw_ved_reestr_item;

CREATE OR REPLACE VIEW esn.vw_ved_reestr_item AS 
 SELECT t1.reestr_id,
    t1.reestr_name,
    t1.date_beg,
    t1.crt_date AS reestr_crt_date,
    COALESCE(t1.parent_id, t1.reestr_id) AS parent_id,
    t2.item_id,
    t2.item_code,
    t2.mnn_id,
    t2.comm_name_id,
    t2.producer_id,
    t2.pack_name,
    t2.pack_count,
    t2.limit_price,
    t2.price_for_init_pack,
    t2.reg_num,
    t2.price_reg_date,
    t2.ean13,
    t2.crt_date,
    t3.name AS mnn,
    t4.name AS producer,
    t5.name AS comm_name,
    t2.price_reg_ndoc,
    t2.price_reg_date_full
   FROM esn.ved_reestr t1
     JOIN esn.ved_reestr_item t2 ON t1.reestr_id = t2.reestr_id
     LEFT JOIN esn.prep_mnn t3 ON t2.mnn_id = t3.mnn_id
     LEFT JOIN esn.prep_producer t4 ON t2.producer_id = t4.producer_id
     LEFT JOIN esn.prep_commercial_name t5 ON t2.comm_name_id = t5.commercial_name_id
  WHERE NOT (EXISTS ( SELECT x1.out_id
           FROM esn.ved_reestr_item_out x1
          WHERE t2.item_id = x1.reestr_item_id));

ALTER TABLE esn.vw_ved_reestr_item OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_ved_reestr_region_item;

CREATE OR REPLACE VIEW esn.vw_ved_reestr_region_item AS 
 SELECT t1.reestr_id,
    t1.reestr_name,
    t1.date_beg,
    t1.crt_date AS reestr_crt_date,
    COALESCE(t1.parent_id, t1.reestr_id) AS parent_id,
    t2.item_id,
    t2.item_code,
    t2.pack_name,
    t2.pack_count,
    t2.limit_price,
    t2.price_for_init_pack,
    t2.reg_num,
    t2.price_reg_date,
    t2.ean13,
    t2.limit_opt_incr,
    t2.limit_rozn_incr,
    t2.limit_rozn_price,
    t2.limit_rozn_price_with_nds,
    t2.crt_date,
    t2.mnn,
    t2.producer,
    t2.comm_name,
    t2.gos_reestr_item_id,
    t3.region_id,
    t2.price_reg_ndoc,
    t2.price_reg_date_full,
    t2.match_date,
    t2.match_user,
    t2.is_hand_match
   FROM esn.ved_reestr_region t1
     JOIN esn.ved_reestr_region_item t2 ON t1.reestr_id = t2.reestr_id
     LEFT JOIN esn.ved_source_region t3 ON t1.source_region_id = t3.item_id
  WHERE NOT (EXISTS ( SELECT x1.out_id
           FROM esn.ved_reestr_region_item_out x1
          WHERE t2.item_id = x1.reestr_item_id));

ALTER TABLE esn.vw_ved_reestr_region_item OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_ved_reestr_item_out;

CREATE OR REPLACE VIEW esn.vw_ved_reestr_item_out AS
 SELECT t1.reestr_id,
    t1.reestr_name,
    t1.date_beg,
    t1.crt_date AS reestr_crt_date,
    COALESCE(t1.parent_id, t1.reestr_id) AS parent_id,
    t2.item_id,
    t2.item_code,
    t2.mnn_id,
    t2.comm_name_id,
    t2.producer_id,
    t2.pack_name,
    t2.pack_count,
    t2.limit_price,
    t2.price_for_init_pack,
    t2.reg_num,
    t2.price_reg_date,
    t2.ean13,
    t2.crt_date,
    t3.out_date,
    t3.out_reazon,
    t4.name AS mnn,
    t5.name AS producer,
    t6.name AS comm_name,
    t2.price_reg_ndoc,
    t2.price_reg_date_full
   FROM esn.ved_reestr t1
     JOIN esn.ved_reestr_item t2 ON t1.reestr_id = t2.reestr_id
     JOIN esn.ved_reestr_item_out t3 ON t2.item_id = t3.reestr_item_id
     LEFT JOIN esn.prep_mnn t4 ON t2.mnn_id = t4.mnn_id
     LEFT JOIN esn.prep_producer t5 ON t2.producer_id = t5.producer_id
     LEFT JOIN esn.prep_commercial_name t6 ON t2.comm_name_id = t6.commercial_name_id;

ALTER TABLE esn.vw_ved_reestr_item_out OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_ved_reestr_region_item_out;

CREATE OR REPLACE VIEW esn.vw_ved_reestr_region_item_out AS 
 SELECT t1.reestr_id,
    t1.reestr_name,
    t1.date_beg,
    t1.crt_date AS reestr_crt_date,
    COALESCE(t1.parent_id, t1.reestr_id) AS parent_id,
    t2.item_id,
    t2.item_code,
    t2.pack_name,
    t2.pack_count,
    t2.limit_price,
    t2.price_for_init_pack,
    t2.reg_num,
    t2.price_reg_date,
    t2.ean13,
    t2.limit_opt_incr,
    t2.limit_rozn_incr,
    t2.limit_rozn_price,
    t2.limit_rozn_price_with_nds,
    t2.crt_date,
    t3.out_date,
    t3.out_reazon,
    t2.mnn,
    t2.producer,
    t2.comm_name,
    t2.gos_reestr_item_id,
    t4.region_id,
    t2.price_reg_ndoc,
    t2.price_reg_date_full,
    t2.match_date,
    t2.match_user,
    t2.is_hand_match
   FROM esn.ved_reestr_region t1
     JOIN esn.ved_reestr_region_item t2 ON t1.reestr_id = t2.reestr_id
     JOIN esn.ved_reestr_region_item_out t3 ON t2.item_id = t3.reestr_item_id
     LEFT JOIN esn.ved_source_region t4 ON t1.source_region_id = t4.item_id;

ALTER TABLE esn.vw_ved_reestr_region_item_out OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_exchange_monitor;

CREATE OR REPLACE VIEW esn.vw_exchange_monitor AS
select 
row_number() over (order by t1.user_id) as id, t1.user_id, t1.login, t1.workplace, t1.client_id, t2.cabinet_client_id
, t1.cabinet_sales_id, t1.cabinet_workplace_id, coalesce(t7.name, 'Клиент [' || t1.login || ']') as client_name
, coalesce(t7.adress, 'Торговая точка [' || t1.login || ']') || ' [' || coalesce(t8.description, '') || ']' as sales_name
, coalesce(t8.description, 'Раб. место [' || t1.login || ']') as workplace_name
, t4.reg_id, t4.ds_id, t4.is_prepared, t4.upload_state, t4.download_state, t4.upload_err_mess, t4.download_err_mess, t4.upload_ok_date, t4.download_ok_date
, t4.prepared_date, t4.upload_last_date, t4.download_last_date, t4.upload_last_mess, t4.download_last_mess
, t4.download_ok_cnt, t4.download_warn_cnt, t4.download_err_cnt
, t4.upload_ok_cnt, t4.upload_warn_cnt, t4.upload_err_cnt, t5.ds_shortname as ds_name, t3.item_id as user_reg_item_id
from esn.exchange_user t1
inner join esn.exchange_client t2 on t1.client_id = t2.client_id
inner join esn.exchange_user_reg t3 on t1.user_id = t3.user_id
inner join esn.exchange_reg_partner t4 on t3.reg_id = t4.reg_id
inner join esn.datasource t5 on t4.ds_id = t5.ds_id
left join cabinet.client t6 on t2.cabinet_client_id = t6.id
left join cabinet.sales t7 on t1.cabinet_sales_id = t7.id
left join cabinet.workplace t8 on t1.cabinet_workplace_id = t8.id;

ALTER TABLE esn.vw_exchange_monitor OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_service;

CREATE OR REPLACE VIEW cabinet.vw_client_service AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS id,
    t1.id AS client_id,
    t1.name AS client_name,
    t2.id AS sales_id,
    t2.name AS sales_name,
    t2.adress AS sales_address,
    t3.id AS workplace_id,
    t3.registration_key AS workplace_registration_key,
    t3.description AS workplace_description,
    t8.id AS service_id,
    t8.name AS service_name,
    t8.description AS service_description,
    t9.id AS version_id,
    t9.name AS version_name,
    t8.is_service,
    t8.priority AS service_priority,
    t4.activation_key,
    t8.have_spec,
    t8.max_workplace_cnt
   FROM cabinet.client t1
     JOIN cabinet.sales t2 ON t1.id = t2.client_id
     JOIN cabinet.workplace t3 ON t2.id = t3.sales_id
     JOIN cabinet.service_registration t4 ON t3.id = t4.workplace_id
     JOIN cabinet.license t5 ON t4.license_id = t5.id
     JOIN cabinet.order_item t6 ON t5.order_item_id = t6.id
     JOIN cabinet."order" t7 ON t6.order_id = t7.id
     JOIN cabinet.service t8 ON t6.service_id = t8.id
     LEFT JOIN cabinet.version t9 ON t6.version_id = t9.id;
     
ALTER TABLE cabinet.vw_client_service OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_workplace_rereg;

CREATE OR REPLACE VIEW cabinet.vw_workplace_rereg AS 
 SELECT 
    t4.id as rereg_id,
    t4.person,
    t4.reason,
    t4.manager_activation,
    t4.created_on,
    t4.created_by,
    t4.updated_on,
    t4.updated_by,
    t4.is_deleted,
    t4.is_used,
    t1.id AS workplace_id,
    t1.registration_key,
    t1.description,
    t1.sales_id,
    t2.name AS sales_name,
    t2.adress AS sales_address,
    t2.client_id,
    t3.name as client_name
   FROM cabinet.workplace t1
     JOIN cabinet.sales t2 ON t1.sales_id = t2.id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
     JOIN cabinet.reregistration_order t4 ON t1.id = t4.workplace_id
     ;
     

ALTER TABLE cabinet.vw_workplace_rereg OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_service_registered;

CREATE OR REPLACE VIEW cabinet.vw_client_service_registered AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS id,
    t3.client_id,
    t3.id AS sales_id,
    t2.id AS workplace_id,
    t2.description AS workplace_description,
    t1.activation_key,
    t1.license_id,
    t6.id AS service_id,
    t10.id AS version_id,
    t6.name AS service_name,
    t10.name AS version_name,
    t6.description AS service_description,
    t6.have_spec,
    t6.max_workplace_cnt
   FROM cabinet.service_registration t1
     JOIN cabinet.workplace t2 ON t1.workplace_id = t2.id
     JOIN cabinet.sales t3 ON t2.sales_id = t3.id
     JOIN cabinet.license t4 ON t1.id = t4.service_registration_id
     JOIN cabinet.order_item t5 ON t4.order_item_id = t5.id
     JOIN cabinet.service t6 ON t5.service_id = t6.id
     LEFT JOIN cabinet.version t10 ON t5.version_id = t10.id;

ALTER TABLE cabinet.vw_client_service_registered OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_service_unregistered;

CREATE OR REPLACE VIEW cabinet.vw_client_service_unregistered AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS id,
    t1.client_id,
    t2.service_id,
    t2.version_id,
    t3.name AS service_name,
    t12.name AS version_name,
    t3.description AS service_description,
    t10.id AS license_id,
    t2.id AS order_item_id,
    t1.id AS order_id,
    t3.have_spec,
    t3.max_workplace_cnt
   FROM cabinet."order" t1
     JOIN cabinet.order_item t2 ON t1.id = t2.order_id
     JOIN cabinet.service t3 ON t2.service_id = t3.id
     JOIN cabinet.license t10 ON t2.id = t10.order_item_id
     LEFT JOIN cabinet.service_registration t11 ON t10.id = t11.license_id
     LEFT JOIN cabinet.version t12 ON t2.version_id = t12.id
  WHERE t11.id IS NULL;
ALTER TABLE cabinet.vw_client_service_unregistered OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_exchange_user;

CREATE OR REPLACE VIEW esn.vw_exchange_user AS 
 SELECT t1.user_id,
    t1.login,
    t1.client_id AS exchange_client_id,
    t10.client_id,
    t10.client_name,
    t10.sales_id,
    COALESCE(t10.sales_address, t10.sales_name) AS sales_name,
    t10.workplace_id,
    t10.description AS workplace_description,
    t10.registration_key AS workplace,
    t11.task_cnt,
    t11.task_cnt_completed,
    (t11.task_cnt_completed::text || '/' || t11.task_cnt::text) as task_info
   FROM esn.exchange_user t1
     LEFT JOIN cabinet.vw_workplace t10 ON t1.cabinet_workplace_id = t10.workplace_id
     LEFT JOIN (select x2.user_id, count(x1.task_id) as task_cnt, sum(case when x1.state in (2,3) then 1 else 0 end) as task_cnt_completed
		from esn.exchange_reg_task x1
		inner join esn.exchange_user_reg x2 on x1.reg_id = x2.reg_id
		group by x2.user_id) t11 ON t1.user_id = t11.user_id
     ;

ALTER TABLE esn.vw_exchange_user OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_service_reg_an_unreg;

CREATE OR REPLACE VIEW cabinet.vw_client_service_reg_an_unreg AS 
 SELECT 1 AS is_registered,
    t1.client_id,
    t1.workplace_id,
    t1.service_id,
    t1.version_id,
    t1.service_name,
    t1.version_name,
    t1.activation_key,
    t1.have_spec,
    t1.max_workplace_cnt
   FROM cabinet.vw_client_service_registered t1
UNION
 SELECT 0 AS is_registered,
    t1.client_id,
    NULL::integer AS workplace_id,
    t1.service_id,
    t1.version_id,
    t1.service_name,
    t1.version_name,
    NULL::character varying AS activation_key,
    t1.have_spec,
    t1.max_workplace_cnt
   FROM cabinet.vw_client_service_unregistered t1;

ALTER TABLE cabinet.vw_client_service_reg_an_unreg OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_exchange_client;

CREATE OR REPLACE VIEW esn.vw_exchange_client AS 
 SELECT t1.client_id,
    t2.name AS client_name,
    t1.cabinet_client_id
   FROM esn.exchange_client t1
     JOIN cabinet.client t2 ON t1.cabinet_client_id = t2.id;

ALTER TABLE esn.vw_exchange_client OWNER TO pavlov;

--------------------
-- DROP VIEW cabinet.vw_cab_user2;

CREATE OR REPLACE VIEW cabinet.vw_cab_user2 AS 
 SELECT t1.user_id,
    t1.user_name,
    t1.user_login,
    t1.is_superadmin,
    t1.full_name,
    t1.email,
    t1.is_crm_user,
    t1.is_executer,
    t1.crm_user_role_id,
    t1.icq,
    t1.is_active,
    t11.role_name AS crm_user_role_name,
    t13.role_id AS auth_role_id,
    t13.role_name AS auth_role_name,
    t1.is_male,
    t1.is_notify_prj
   FROM cabinet.cab_user t1
     LEFT JOIN cabinet.crm_user_role t11 ON t1.crm_user_role_id = t11.role_id
     LEFT JOIN cabinet.auth_user_role t12 ON t1.user_id = t12.user_id
     LEFT JOIN cabinet.auth_role t13 ON t12.role_id = t13.role_id;

ALTER TABLE cabinet.vw_cab_user2 OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_crm_project;

CREATE OR REPLACE VIEW cabinet.vw_crm_project AS
	SELECT t1.project_id, t1.project_name, t11.user_id
	, case when t12.fore_color is null then 'black' else t12.fore_color end as fore_color
	FROM cabinet.crm_project t1
	LEFT JOIN cabinet.cab_user t11 ON 1=1
	LEFT JOIN cabinet.crm_project_user_settings t12 ON t1.project_id = t12.project_id and t11.user_id = coalesce(t12.user_id, t11.user_id)
	;

ALTER TABLE cabinet.vw_crm_project OWNER TO pavlov;

--------------------
-- DROP VIEW cabinet.vw_crm_priority;

CREATE OR REPLACE VIEW cabinet.vw_crm_priority AS 
 SELECT t1.priority_id,
    t1.priority_name,
    t11.user_id,
        CASE
            WHEN t12.fore_color IS NULL THEN 'black'::character varying
            ELSE t12.fore_color
        END AS fore_color,
    t1.ord,
    t1.is_control,
    t1.is_boss,
    t1.rate
   FROM cabinet.crm_priority t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1
     LEFT JOIN cabinet.crm_priority_user_settings t12 ON t1.priority_id = t12.priority_id AND t11.user_id = COALESCE(t12.user_id, t11.user_id);

ALTER TABLE cabinet.vw_crm_priority OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_cab_grid_column_user_settings;

CREATE OR REPLACE VIEW cabinet.vw_cab_grid_column_user_settings AS 
 SELECT row_number() OVER (PARTITION BY t3.user_id, COALESCE(t2.is_visible, true) ORDER BY COALESCE(t2.column_num, 0)) - 1 AS column_index,
    t1.column_id AS item_id,
    t1.column_id,
    t3.user_id,
    COALESCE(t2.column_num, 0) AS column_num,
    COALESCE(t2.is_visible, true) AS is_visible,
    COALESCE(t2.width, 200) AS width,
    COALESCE(t2.is_filterable, true) AS is_filterable,
    COALESCE(t2.is_sortable, true) AS is_sortable,
    t1.grid_id,
    t1.column_name,
    t1.column_name_rus,
    t1.format,
    t1.templ,
    t1.title
   FROM cabinet.cab_grid_column t1
     LEFT JOIN cabinet.cab_user t3 ON 1 = 1
     LEFT JOIN cabinet.cab_grid_column_user_settings t2 ON t1.column_id = t2.column_id AND COALESCE(t2.user_id, 0) = t3.user_id;

ALTER TABLE cabinet.vw_cab_grid_column_user_settings OWNER TO pavlov;
--------------------

-- DROP VIEW discount.vw_card_type_business_org;

CREATE OR REPLACE VIEW discount.vw_card_type_business_org AS 
 SELECT t1.card_type_id,
    t1.card_type_name,
    t2.org_id AS business_org_id,
    t2.org_name,
        CASE
            WHEN COALESCE(t3.card_type_id, 0::bigint) <= 0 THEN false
            ELSE true
        END AS is_org_ok,
        CASE
            WHEN COALESCE(t3.card_type_id, 0::bigint) <= 0 THEN false
            ELSE t3.allow_discount
        END AS allow_discount,
        CASE
            WHEN COALESCE(t3.card_type_id, 0::bigint) <= 0 THEN false
            ELSE t3.allow_bonus_for_pay
        END AS allow_bonus_for_pay,
        CASE
            WHEN COALESCE(t3.card_type_id, 0::bigint) <= 0 THEN false
            ELSE t3.allow_bonus_for_card
        END AS allow_bonus_for_card
   FROM discount.card_type t1
     LEFT JOIN discount.business_org t2 ON t1.business_id = t2.business_id
     LEFT JOIN discount.card_type_business_org t3 ON t2.org_id = t3.business_org_id AND t1.card_type_id = t3.card_type_id;

ALTER TABLE discount.vw_card_type_business_org OWNER TO pavlov;
--------------------

-- DROP VIEW discount.vw_business_summary;

CREATE OR REPLACE VIEW discount.vw_business_summary AS 
 SELECT t1.business_id,
    count(t1.card_id) AS card_cnt,
    sum(
        CASE
            WHEN t2.card_type_group_id = 1 THEN 1
            ELSE 0
        END) AS card_type_group_1_cnt,
    sum(
        CASE
            WHEN t2.card_type_group_id = 2 THEN 1
            ELSE 0
        END) AS card_type_group_2_cnt,
    sum(
        CASE
            WHEN t2.card_type_group_id = 3 THEN 1
            ELSE 0
        END) AS card_type_group_3_cnt,
    t11.card_holder_cnt,
    COALESCE(t12.card_trans_cnt, 0::bigint) AS card_trans_cnt,
    COALESCE(t12.card_trans_today_cnt, 0::bigint) AS card_trans_today_cnt
   FROM discount.card t1
     JOIN discount.card_type t2 ON t1.curr_card_type_id = t2.card_type_id
     LEFT JOIN ( SELECT x1.business_id,
            count(x1.card_holder_id) AS card_holder_cnt
           FROM discount.card_holder x1
          WHERE 1 = 1
          GROUP BY x1.business_id) t11 ON t1.business_id = t11.business_id
     LEFT JOIN ( SELECT x2.business_id,
            sum(
                CASE
                    WHEN x1.date_beg::date = 'now'::text::date THEN 1
                    ELSE 0
                END) AS card_trans_today_cnt,
            count(x1.card_trans_id) AS card_trans_cnt
           FROM discount.card_transaction x1
             JOIN discount.card x2 ON x1.card_id = x2.card_id
          WHERE x1.trans_kind = 1
          AND x1.status = 0
          GROUP BY x2.business_id) t12 ON t1.business_id = t12.business_id
  WHERE t2.card_type_group_id = ANY (ARRAY[1::bigint, 2::bigint, 3::bigint])
  GROUP BY t1.business_id, t11.card_holder_cnt, t12.card_trans_cnt, t12.card_trans_today_cnt;

ALTER TABLE discount.vw_business_summary OWNER TO pavlov;

--------------------

-- DROP VIEW discount.vw_business_summary_graph;

CREATE OR REPLACE VIEW discount.vw_business_summary_graph AS 
	select row_number() over (order by t2.business_id) as id, t2.business_id, (t1.date_beg::date) as date_beg_date, sum(t1.trans_sum) as trans_sum_total
	from discount.card_transaction t1
	inner join discount.card t2 on t1.card_id = t2.card_id
	/*inner join discount.card_type t3 on t2.curr_card_type_id = t3.card_type_id*/
	where t1.trans_kind = 1
	and t1.status = 0
	and t1.date_beg >= current_date - integer '180'
	group by t2.business_id, (t1.date_beg::date)
	order by t2.business_id, (t1.date_beg::date)
	;

ALTER TABLE discount.vw_business_summary_graph OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_notify;

CREATE OR REPLACE VIEW cabinet.vw_notify AS 
 SELECT t1.notify_id,
    t1.scope,
    t1.target_user_id,
    t1.message,
    t1.message_sent,
    t1.crt_date,
    t1.sent_date,
    t1.transport_type,
    t11.icq,
    t11.email,
    t11.is_active_icq,
    t11.is_active_email,
    t1.message_sent_email
   FROM cabinet.notify t1
     LEFT JOIN cabinet.crm_notify t11 ON t1.target_user_id = t11.user_id;

ALTER TABLE cabinet.vw_notify OWNER TO pavlov;
--------------------

-- DROP VIEW discount.vw_programm_order;

CREATE OR REPLACE VIEW discount.vw_programm_order AS 
 SELECT t1.order_id,
    t1.business_id,
    t1.mess,
    t1.descr,
    t1.crt_user,
    t1.crt_date,
    t1.done_user,
    t1.done_date,
    t1.programm_id,
    t1.card_type_group_id,
    t1.date_beg,
    t1.trans_sum_min_disc,
    t1.trans_sum_max_disc,
    t1.trans_sum_min_bonus_for_card,
    t1.trans_sum_max_bonus_for_card,
    t1.trans_sum_min_bonus_for_pay,
    t1.trans_sum_max_bonus_for_pay,
    t1.proc_const_disc,
    t1.proc_step_disc,
    t1.sum_step_card_disc,
    t1.sum_step_trans_disc,
    t1.proc_const_bonus_for_card,
    t1.proc_step_bonus_for_card,
    t1.sum_step_card_bonus_for_card,
    t1.sum_step_trans_bonus_for_card,
    t1.use_max_percent_disc,
    t1.allow_zero_disc_bonus_for_card,
    t1.allow_zero_disc_bonus_for_pay,
    t1.same_for_zhv_disc,
    t1.same_for_zhv_bonus,
    t1.proc_const_zhv_disc,
    t1.proc_step_zhv_disc,
    t1.sum_step_card_zhv_disc,
    t1.sum_step_trans_zhv_disc,
    t1.proc_const_zhv_bonus_for_card,
    t1.proc_step_zhv_bonus_for_card,
    t1.sum_step_card_zhv_bonus_for_card,
    t1.sum_step_trans_zhv_bonus_for_card,
    t1.use_max_percent_zhv_disc,
    t1.allow_zero_disc_zhv_bonus_for_card,
    t1.allow_zero_disc_zhv_bonus_for_pay,
    t1.allow_order_disc,
    t1.allow_order_bonus_for_card,
    t1.allow_order_bonus_for_pay,
    t1.add_to_card_trans_sum,
    t1.add_to_card_trans_sum_with_disc,
    t1.add_to_card_trans_sum_with_bonus_for_pay,
    t1.mess_contains_ext_info,
    t1.card_type_id,
    t1.proc_const_from_card_disc,
    t1.proc_const_from_card_zhv_disc,
    t1.proc_const_from_card_bonus_for_card,
    t1.proc_const_from_card_zhv_bonus_for_card,
    t1.trans_sum_max_percent_bonus_for_pay,
    t1.trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
    t1.price_margin_percent_lte_forbid_disc,
    t1.price_margin_percent_lte_forbid_zhv_disc,
    t1.price_margin_percent_gte_forbid_disc,
    t1.price_margin_percent_gte_forbid_zhv_disc,
    t1.use_explicit_max_percent_disc,
    t1.use_explicit_max_percent_zhv_disc,
        CASE
            WHEN t1.done_date IS NOT NULL THEN 'Исполнена'::text
            ELSE 'Ожидает исполнения'::text
        END AS order_state,
    t2.business_name,
    t3.group_name AS card_type_group_name,
    t11.card_type_name,
    t12.programm_name
   FROM discount.programm_order t1
     JOIN discount.business t2 ON t1.business_id = t2.business_id
     JOIN discount.card_type_group t3 ON t1.card_type_group_id = t3.card_type_group_id
     LEFT JOIN discount.card_type t11 ON t1.card_type_id = t11.card_type_id
     LEFT JOIN discount.programm t12 ON t1.programm_id = t12.programm_id;

ALTER TABLE discount.vw_programm_order OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_exchange_file_node;

CREATE OR REPLACE VIEW esn.vw_exchange_file_node AS 
 SELECT t1.node_id,
    t1.partner_id,
    t1.client_id,
    t1.sales_id,
    t1.node_name,
    t2.name AS client_name,
    COALESCE(t3.adress, t3.name) AS sales_address,
    t4.ds_name AS partner_name
   FROM esn.exchange_file_node t1
     JOIN cabinet.client t2 ON t1.client_id = t2.id
     JOIN cabinet.sales t3 ON t1.sales_id = t3.id
     JOIN esn.datasource t4 ON t1.partner_id = t4.ds_id;

ALTER TABLE esn.vw_exchange_file_node OWNER TO pavlov;

--------------------

-- DROP VIEW esn.vw_exchange_file_log;

CREATE OR REPLACE VIEW esn.vw_exchange_file_log AS 
 SELECT row_number() OVER (ORDER BY t1.node_id) AS id,
    t1.node_id,
    t1.partner_id,
    t1.client_id,
    t1.sales_id,
    t1.partner_name,
    t1.client_name,
    t1.sales_address,
    t1.node_name,
    t2.exchange_item_id,
    t3.item_name,
    t11.filename AS filename_last,
    t11.period AS period_last,
    t12.filename AS filename_prev,
    t12.period AS period_prev,
    'now'::text::date - t11.period::date AS days_cnt
   FROM esn.vw_exchange_file_node t1
     JOIN esn.exchange_file_node_item t2 ON t1.node_id = t2.node_id
     JOIN esn.exchange_item t3 ON t2.exchange_item_id = t3.item_id
     LEFT JOIN ( SELECT max(exchange_file_log.log_num) AS log_num,
            max(exchange_file_log.node::text) AS node,
            max(exchange_file_log.filename::text) AS filename,
            max(exchange_file_log.period::text) AS period,
            exchange_file_log.exchange_item_id
           FROM esn.exchange_file_log
          GROUP BY exchange_file_log.node, exchange_file_log.exchange_item_id) t11 ON btrim(t1.node_name::text) = btrim(t11.node) AND t2.exchange_item_id = t11.exchange_item_id
     LEFT JOIN ( SELECT max(exchange_file_log.log_num) AS log_num,
            max(exchange_file_log.node::text) AS node,
            max(exchange_file_log.filename::text) AS filename,
            max(exchange_file_log.period::text) AS period,
            exchange_file_log.exchange_item_id
           FROM esn.exchange_file_log
          WHERE NOT (exchange_file_log.log_id IN ( SELECT max(exchange_file_log_1.log_id) AS max
                   FROM esn.exchange_file_log exchange_file_log_1
                  GROUP BY exchange_file_log_1.node, exchange_file_log_1.exchange_item_id))
          GROUP BY exchange_file_log.node, exchange_file_log.exchange_item_id) t12 ON btrim(t1.node_name::text) = btrim(t12.node) AND t2.exchange_item_id = t12.exchange_item_id;

ALTER TABLE esn.vw_exchange_file_log OWNER TO pavlov;
--------------------

-- DROP VIEW esn.vw_datasource_exchange_item;

CREATE OR REPLACE VIEW esn.vw_datasource_exchange_item AS 
 SELECT t1.rel_id,
    t1.ds_id,
    t1.item_id,
    t1.for_send,
    t1.for_get,
    t1.item_type_id,
    t1.item_file_name,
    t1.sql_text,
    t1.param,
    t1.file_name_mask,
    t2.ds_name,
    t3.item_name
   FROM esn.datasource_exchange_item t1
     JOIN esn.datasource t2 ON t1.ds_id = t2.ds_id
     JOIN esn.exchange_item t3 ON t1.item_id = t3.item_id;

ALTER TABLE esn.vw_datasource_exchange_item OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_sales;

CREATE OR REPLACE VIEW cabinet.vw_sales AS 
 SELECT t1.id AS sales_id,
    t1.name,
    t1.client_id,
    t1.city_id,
    t1.cell,
    t1.contact_person,
    t1.adress,
    t1.is_deleted,
    t1.sm_id,
    t1.email,
    t13.user_id,
    t2.name AS client_name,
    t11.name AS city_name,
    t11.region_id,
    t12.name AS region_name,
    t15.name AS user_name,
    t1.region_zone_id,
    t1.tax_system_type_id
   FROM cabinet.sales t1
     JOIN cabinet.client t2 ON t1.client_id = t2.id
     LEFT JOIN cabinet.city t11 ON t1.city_id = t11.id
     LEFT JOIN cabinet.region t12 ON t11.region_id = t12.id
     LEFT JOIN cabinet.client_user t13 ON t1.id = t13.sales_id AND t13.is_main_for_sales = true
     LEFT JOIN cabinet.my_aspnet_users t15 ON t13.user_id = t15.id
  WHERE t1.is_deleted = 0;

ALTER TABLE cabinet.vw_sales OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_user;

CREATE OR REPLACE VIEW cabinet.vw_client_user AS 
 SELECT t1.user_id,
    t1.client_id,
    t1.sales_id,
    t1.user_name,
    t1.user_name_full,
    t1.is_main_for_sales,
    t1.need_change_pwd,
    t3.name AS client_name,
    t12.adress AS sales_address,
    t2.name AS user_login,
    t11."Email" AS user_email,
    t11."IsApproved" AS is_active,
    t13.role_id,
    t14.role_name,
        CASE
            WHEN t15.user_id IS NULL THEN false
            ELSE true
        END AS is_admin,
    COALESCE(t15.is_superadmin, false) AS is_superadmin
   FROM cabinet.client_user t1
     JOIN cabinet.my_aspnet_users t2 ON t1.user_id = t2.id
     JOIN cabinet.client t3 ON t1.client_id = t3.id
     LEFT JOIN cabinet.my_aspnet_membership t11 ON t2.id = t11."userId"
     LEFT JOIN cabinet.sales t12 ON t1.sales_id = t12.id
     LEFT JOIN cabinet.auth_user_role t13 ON t1.user_id = t13.user_id
     LEFT JOIN cabinet.auth_role t14 ON t13.role_id = t14.role_id
     LEFT JOIN cabinet.cab_user t15 ON t1.user_id = t15.user_id;

ALTER TABLE cabinet.vw_client_user OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_service_cva_info;

CREATE OR REPLACE VIEW cabinet.vw_service_cva_info AS 
 SELECT t1.workplace_id,
    t1.send_interval,
    max(t2.sent_date) AS last_sent_date,
    max(
        CASE
            WHEN t2.batch_state = ANY (ARRAY[1, 3]) THEN t2.batch_num
            ELSE 0
        END) AS batch_num_for_send,
    max(
        CASE
            WHEN t2.batch_state = ANY (ARRAY[1, 3]) THEN t2.batch_date
            ELSE NULL::timestamp without time zone
        END) AS batch_date_for_send,
    count(t2.sent_date) AS batch_sent_count
   FROM cabinet.service_cva t1
     LEFT JOIN cabinet.service_cva_data t2 ON t1.workplace_id = t2.workplace_id AND t1.service_id = t2.service_id
  GROUP BY t1.workplace_id, t1.send_interval;

ALTER TABLE cabinet.vw_service_cva_info OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_service_cva_data;

CREATE OR REPLACE VIEW cabinet.vw_service_cva_data AS 
 SELECT t2.client_id,
    t2.client_name,
    t2.sales_id,
    t2.sales_address,
    t2.description AS workplace_description,
    t1.item_id,
    t1.workplace_id,
    t1.service_id,
    t1.batch_num,
    t1.batch_date,
    t1.batch_state,
    t1.sending_date,
    t1.sent_date,
    t1.mess,
    t1.batch_size,
    t1.shortformat
   FROM cabinet.service_cva_data t1
     JOIN cabinet.vw_workplace t2 ON t1.workplace_id = t2.workplace_id;

ALTER TABLE cabinet.vw_service_cva_data OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_user_widget;

CREATE OR REPLACE VIEW cabinet.vw_client_user_widget AS 
 SELECT t1.widget_id,
    t1.widget_name,
    t1.widget_descr,
    t1.widget_element_id,
    t1.widget_header,
    t1.widget_content,
    t11.sort_num,
    t1.is_active,
    t1.service_id,
    t11.user_id,
        CASE
            WHEN t11.widget_id IS NULL THEN false
            ELSE true
        END AS is_installed,
    t12.client_id,
    t12.sales_id,
    t13.state,
    t11.param
   FROM cabinet.widget t1
     LEFT JOIN cabinet.client_user_widget t11 ON t1.widget_id = t11.widget_id
     LEFT JOIN cabinet.client_user t12 ON t11.user_id = t12.user_id
     LEFT JOIN cabinet.client_service t13 ON t12.client_id = t13.client_id AND t1.service_id = t13.service_id;

ALTER TABLE cabinet.vw_client_user_widget OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_info;

CREATE OR REPLACE VIEW cabinet.vw_client_info AS 
 SELECT t1.id AS client_id,
    t1.name AS client_name,
        CASE
            WHEN btrim(COALESCE(t12.name, ''::character varying)::text) <> ''::text THEN btrim(COALESCE(t12.name, ''::character varying)::text) || ', '::text
            ELSE ''::text
        END ||
        CASE
            WHEN btrim(COALESCE(t11.name, ''::character varying)::text) <> ''::text THEN btrim(COALESCE(t11.name, ''::character varying)::text)
            ELSE ''::text
        END AS client_address,
    t1.cell AS client_phone,
    t1.contact_person AS client_contact,
    count(DISTINCT t13.id) AS sales_count,
    count(DISTINCT t14.service_id) AS installed_service_cnt,
    count(DISTINCT t15.service_id) AS ordered_service_cnt
   FROM cabinet.client t1
     LEFT JOIN cabinet.city t11 ON t1.city_id = t11.id
     LEFT JOIN cabinet.region t12 ON t11.region_id = t12.id
     LEFT JOIN cabinet.sales t13 ON t1.id = t13.client_id
     LEFT JOIN cabinet.client_service t14 ON t1.id = t14.client_id AND t14.state = 0
     LEFT JOIN cabinet.client_service t15 ON t1.id = t15.client_id AND t15.state = 1
  GROUP BY t1.id, t1.name, btrim(COALESCE(t12.name, ''::character varying)::text), btrim(COALESCE(t11.name, ''::character varying)::text), t1.cell, t1.contact_person;

ALTER TABLE cabinet.vw_client_info OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_service_summary;

CREATE OR REPLACE VIEW cabinet.vw_client_service_summary AS 
 SELECT t1.id AS client_id,
    t11.service_id,
    count(DISTINCT t12.sales_id) AS sales_cnt
   FROM cabinet.client t1
     LEFT JOIN cabinet.client_service t11 ON t1.id = t11.client_id
     LEFT JOIN cabinet.vw_client_service t12 ON t11.client_id = t12.client_id AND t11.service_id = t12.service_id
  WHERE t1.is_deleted = 0
  GROUP BY t1.id, t11.service_id;

ALTER TABLE cabinet.vw_client_service_summary OWNER TO pavlov;

--------------------

-- DROP VIEW discount.vw_business_summary_graph2;

CREATE OR REPLACE VIEW discount.vw_business_summary_graph2 AS 
 SELECT row_number() OVER (ORDER BY t2.business_id) AS id,
    t2.business_id,
    t1.date_beg::date AS date_beg_date,
    sum(t6.unit_value) AS trans_sum_total,
    sum(t6.unit_value_discount) AS trans_sum_discount,
    sum(t6.unit_value_with_discount) AS trans_sum_with_discount,
    sum(t6.unit_value_bonus_for_card) AS trans_sum_bonus_for_card,
    sum(t6.unit_value_bonus_for_pay) AS trans_sum_bonus_for_pay
   FROM discount.card_transaction t1
     JOIN discount.card t2 ON t1.card_id = t2.card_id
     LEFT JOIN discount.card_transaction_detail t6 ON t1.card_trans_id = t6.card_trans_id
  WHERE t1.trans_kind = 1 AND t1.status = 0 AND t1.date_beg >= ('now'::text::date - 7)
  GROUP BY t2.business_id, t1.date_beg::date
  ORDER BY t2.business_id, t1.date_beg::date;

ALTER TABLE discount.vw_business_summary_graph2 OWNER TO pavlov;

--------------------

--DROP VIEW cabinet.vw_service;

CREATE OR REPLACE VIEW cabinet.vw_service AS 
 SELECT t1.id,
    t1.guid,
    t1.name,
    t1.description,
    t1.parent_id,
    t1.is_deleted,
    t1.exe_name,
    t1.is_site,    
    t1.price,
    t1.need_key,
    t1."NeedCheckVersionLicense",
    t1.is_service,
    t1.priority,
    t1.have_spec,
    t1.max_workplace_cnt,
    t1.group_id,
    t2.group_name
   FROM cabinet.service t1
     LEFT JOIN cabinet.service_group t2 ON t1.group_id = t2.group_id
  WHERE t1.is_deleted <> 1 AND t1.parent_id IS NOT NULL;

ALTER TABLE cabinet.vw_service OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_service_param;

CREATE OR REPLACE VIEW cabinet.vw_client_service_param AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS param_id,
    t1.id AS client_id,
    t11.id AS service_id,
        CASE
            WHEN t12.service_id IS NULL THEN t11.max_workplace_cnt
            ELSE t12.max_workplace_cnt
        END AS max_workplace_cnt,
    t11.name AS service_name
   FROM cabinet.client t1
     LEFT JOIN cabinet.service t11 ON 1 = 1
     LEFT JOIN cabinet.client_service_param t12 ON t1.id = t12.client_id AND t11.id = t12.service_id
  WHERE t11.max_workplace_cnt IS NOT NULL;
  
ALTER TABLE cabinet.vw_client_service_param OWNER TO pavlov;

--------------------

-- DROP VIEW discount.vw_report_unused_cards;

CREATE OR REPLACE VIEW discount.vw_report_unused_cards AS 
 SELECT row_number() OVER (ORDER BY t2.business_id) AS id,
    t2.business_id,
    t2.card_id,
    t2.card_num,
    t2.curr_discount_percent,
    t2.curr_bonus_percent,
    t2.curr_bonus_value,
    t2.curr_trans_sum,
    t5.card_holder_surname AS card_holder_name,
    t1.last_date_check
   FROM ( SELECT x1.card_id,
            max(x2.date_check) AS last_date_check
           FROM discount.card x1
             LEFT JOIN discount.card_transaction x2 ON x1.card_id = x2.card_id AND x2.trans_kind = 1 AND x2.status = 0
          GROUP BY x1.card_id) t1
     JOIN discount.vw_card t2 ON t1.card_id = t2.card_id
     LEFT JOIN discount.card_holder t5 ON t2.curr_holder_id = t5.card_holder_id;

ALTER TABLE discount.vw_report_unused_cards OWNER TO pavlov;

--------------------
-- DROP VIEW cabinet.vw_auth_user_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_user_perm AS 
 SELECT (random() * 100000::double precision)::bigint AS id,
    t1.section_id,
    t1.section_name,
    t2.part_id,
    t2.part_name,
    t3.user_id,
    t3.user_name,
    t4.role_id,
    t4_1.role_name,
        CASE
            WHEN t6.perm_id IS NOT NULL AND t7.perm_id IS NULL THEN true
            ELSE false
        END AS role_perm,
        CASE
            WHEN t7.perm_id IS NOT NULL THEN true
            ELSE false
        END AS user_perm,
        CASE
            WHEN t7.perm_id IS NOT NULL THEN t7.allow_view
            ELSE COALESCE(t6.allow_view, false)
        END AS allow_view,
        CASE
            WHEN t7.perm_id IS NOT NULL THEN t7.allow_edit
            ELSE COALESCE(t6.allow_edit, false)
        END AS allow_edit,
    t5.group_id AS role_group_id
   FROM cabinet.auth_section t1
     LEFT JOIN cabinet.auth_section_part t2 ON t1.section_id = t2.section_id
     LEFT JOIN cabinet.client_user t3 ON 1 = 1
     LEFT JOIN cabinet.auth_user_role t4 ON t3.user_id = t4.user_id
     LEFT JOIN cabinet.auth_role t4_1 ON t4.role_id = t4_1.role_id
     LEFT JOIN cabinet.auth_role_group t5 ON t4.role_id = t5.role_id
     LEFT JOIN cabinet.auth_role_perm t6 ON t5.role_id = t6.role_id AND t2.part_id = t6.part_id
     LEFT JOIN cabinet.auth_user_perm t7 ON t3.user_id = t7.user_id AND t2.part_id = t7.part_id
  WHERE COALESCE(t2.group_id, COALESCE(t1.group_id, t5.group_id)) = t5.group_id;

ALTER TABLE cabinet.vw_auth_user_perm OWNER TO pavlov;

--------------------
-- DROP VIEW cabinet.vw_auth_user_role;

CREATE OR REPLACE VIEW cabinet.vw_auth_user_role AS 
 SELECT t1.user_id,
    t1.user_name,
    t1.user_name_full,
    t1.client_id,
    t3.role_id,
    t3.role_name,
    t4.group_id,
    t6.user_login,
    t6.is_superadmin,
    t6.full_name,
    t6.email,
    t6.is_crm_user,
    t6.is_executer,
    t6.crm_user_role_id,
    t6.icq,
    t6.is_active,
    t7.role_name AS crm_user_role_name,
    t6.is_notify_prj
   FROM cabinet.client_user t1
     LEFT JOIN cabinet.auth_user_role t2 ON t1.user_id = t2.user_id
     LEFT JOIN cabinet.auth_role t3 ON t2.role_id = t3.role_id
     LEFT JOIN cabinet.auth_role_group t4 ON t3.role_id = t4.role_id AND (t4.group_id = 1 AND t1.client_id = 1000 OR t4.group_id = 2 AND t1.client_id <> 1000)
     LEFT JOIN cabinet.my_aspnet_membership t5 ON t1.user_id = t5."userId"
     LEFT JOIN cabinet.cab_user t6 ON t1.user_id = t6.user_id
     LEFT JOIN cabinet.crm_user_role t7 ON t6.crm_user_role_id = t7.role_id;

ALTER TABLE cabinet.vw_auth_user_role OWNER TO pavlov;
--------------------

-- DROP VIEW cabinet.vw_auth_role_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_role_perm AS 
 SELECT row_number() OVER (ORDER BY t1.section_id) AS id,
    t1.section_id,
    t1.section_name,
    t2.part_id,
    t2.part_name,
    t3.role_id,
    t3.role_name,
    COALESCE(t5.allow_view, false) AS allow_view,
    COALESCE(t5.allow_edit, false) AS allow_edit,
    t4.group_id AS role_group_id
   FROM cabinet.auth_section t1
     LEFT JOIN cabinet.auth_section_part t2 ON t1.section_id = t2.section_id
     LEFT JOIN cabinet.auth_role t3 ON 1 = 1
     LEFT JOIN cabinet.auth_role_group t4 ON t3.role_id = t4.role_id AND COALESCE(t2.group_id, COALESCE(t1.group_id, t4.group_id)) = t4.group_id
     LEFT JOIN cabinet.auth_role_perm t5 ON t4.role_id = t5.role_id AND t2.part_id = t5.part_id;

ALTER TABLE cabinet.vw_auth_role_perm OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_auth_role_section_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_role_section_perm AS 
 SELECT row_number() OVER (ORDER BY vw_auth_role_perm.section_id) AS id,
    vw_auth_role_perm.section_id,
    vw_auth_role_perm.role_id,
    vw_auth_role_perm.role_group_id,
    vw_auth_role_perm.section_name,
    count(DISTINCT vw_auth_role_perm.allow_view)::integer AS allow_view_cnt,
    count(DISTINCT vw_auth_role_perm.allow_edit)::integer AS allow_edit_cnt,
        CASE
            WHEN count(DISTINCT vw_auth_role_perm.allow_view)::integer > 1 THEN 2
            ELSE max(vw_auth_role_perm.allow_view::integer)
        END AS allow_view_section,
        CASE
            WHEN count(DISTINCT vw_auth_role_perm.allow_edit)::integer > 1 THEN 2
            ELSE max(vw_auth_role_perm.allow_edit::integer)
        END AS allow_edit_section
   FROM cabinet.vw_auth_role_perm
  GROUP BY vw_auth_role_perm.section_id, vw_auth_role_perm.role_id, vw_auth_role_perm.role_group_id, vw_auth_role_perm.section_name;

ALTER TABLE cabinet.vw_auth_role_section_perm OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_auth_user_section_perm;

CREATE OR REPLACE VIEW cabinet.vw_auth_user_section_perm AS 
 SELECT row_number() OVER (ORDER BY vw_auth_user_perm.section_id) AS id,
    vw_auth_user_perm.section_id,
    vw_auth_user_perm.user_id,
    vw_auth_user_perm.role_group_id,
    vw_auth_user_perm.section_name,
    count(DISTINCT vw_auth_user_perm.allow_view)::integer AS allow_view_cnt,
    count(DISTINCT vw_auth_user_perm.allow_edit)::integer AS allow_edit_cnt,
        CASE
            WHEN count(DISTINCT vw_auth_user_perm.allow_view)::integer > 1 THEN 2
            ELSE max(vw_auth_user_perm.allow_view::integer)
        END AS allow_view_section,
        CASE
            WHEN count(DISTINCT vw_auth_user_perm.allow_edit)::integer > 1 THEN 2
            ELSE max(vw_auth_user_perm.allow_edit::integer)
        END AS allow_edit_section
   FROM cabinet.vw_auth_user_perm
  GROUP BY vw_auth_user_perm.section_id, vw_auth_user_perm.user_id, vw_auth_user_perm.role_group_id, vw_auth_user_perm.section_name;

ALTER TABLE cabinet.vw_auth_user_section_perm  OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_auth_role;

CREATE OR REPLACE VIEW cabinet.vw_auth_role AS 
 SELECT t1.role_id,
    t1.role_name,
        CASE
            WHEN t2.group_id IS NOT NULL THEN true
            ELSE false
        END AS is_group1,
        CASE
            WHEN t3.group_id IS NOT NULL THEN true
            ELSE false
        END AS is_group2
   FROM cabinet.auth_role t1
     LEFT JOIN cabinet.auth_role_group t2 ON t1.role_id = t2.role_id AND t2.group_id = 1
     LEFT JOIN cabinet.auth_role_group t3 ON t1.role_id = t3.role_id AND t3.group_id = 2;

ALTER TABLE cabinet.vw_auth_role OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_storage_file;

CREATE OR REPLACE VIEW cabinet.vw_storage_file AS 
 SELECT t1.file_id,
    t1.file_name,
    t1.file_nom,
    t1.folder_id,
    t1.ord,
    t1.file_size,
    t1.physical_path,
    t1.physical_name,
    t1.download_link,
    t1.file_type_id,
    t1.file_state_id,
    t1.has_version,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.state_date,
    t1.state_user,
    t2.folder_name,
    t2.folder_level,
    t3.file_type_name,
    t3.group_id,
    t4.group_name,
    t5.file_state_name,
        CASE
            WHEN btrim(COALESCE(t4.img, ''::character varying)::text) = ''::text THEN 'storage storage-file'::text
            ELSE 'storage storage-'::text || btrim(t4.img::text)
        END::character varying AS sprite_css_class,
    (((((t1.file_name::text || '.'::text) || t3.file_type_name::text) || '  /  '::text) || btrim(to_char(t1.file_size::bigint, '9G999G999G999G999G999'::text))) || ' байт'::text)::character varying AS file_name_full,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str,
    to_char(t1.upd_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS upd_date_str,
    to_char(t1.state_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS state_date_str,
    (btrim(to_char(t1.file_size::bigint, '9G999G999G999G999G999'::text)) || ' байт'::text)::character varying AS file_size_str,
    ((btrim(t1.file_name::text) || '.'::text) || btrim(t3.file_type_name::text))::character varying AS file_name_with_extention,
        CASE
            WHEN t11.file_version_id IS NOT NULL THEN true
            ELSE false
        END AS v1_exists,
    t11.file_version_id AS v1_file_version_id,
    t11.file_size AS v1_file_size,
    t11.physical_path AS v1_physical_path,
    t11.physical_name AS v1_physical_name,
    t11.download_link AS v1_download_link,
    t11.crt_date AS v1_crt_date,
    t11.crt_user AS v1_crt_user,
    (((((t1.file_name::text || '.'::text) || t3.file_type_name::text) || '  /  '::text) || btrim(to_char(t11.file_size::bigint, '9G999G999G999G999G999'::text))) || ' байт'::text)::character varying AS v1_file_name_full,
    to_char(t11.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS v1_crt_date_str,
    to_char(t11.upd_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS v1_upd_date_str,
    (btrim(to_char(t11.file_size::bigint, '9G999G999G999G999G999'::text)) || ' байт'::text)::character varying AS v1_file_size_str,
    t11.version_num_actual AS v1_version_num_actual,
        CASE
            WHEN t12.file_version_id IS NOT NULL THEN true
            ELSE false
        END AS v2_exists,
    t12.file_version_id AS v2_file_version_id,
    t12.file_size AS v2_file_size,
    t12.physical_path AS v2_physical_path,
    t12.physical_name AS v2_physical_name,
    t12.download_link AS v2_download_link,
    t12.crt_date AS v2_crt_date,
    t12.crt_user AS v2_crt_user,
    (((((t1.file_name::text || '.'::text) || t3.file_type_name::text) || '  /  '::text) || btrim(to_char(t11.file_size::bigint, '9G999G999G999G999G999'::text))) || ' байт'::text)::character varying AS v2_file_name_full,
    to_char(t12.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS v2_crt_date_str,
    to_char(t12.upd_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS v2_upd_date_str,
    (btrim(to_char(t11.file_size::bigint, '9G999G999G999G999G999'::text)) || ' байт'::text)::character varying AS v2_file_size_str,
    t12.version_num_actual AS v2_version_num_actual,
    false AS is_local_link,
    t1.file_date,
    to_char(t1.file_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS file_date_str,
    1 AS item_type
   FROM cabinet.storage_file t1
     JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
     JOIN cabinet.storage_file_type t3 ON t1.file_type_id = t3.file_type_id
     JOIN cabinet.storage_file_type_group t4 ON t3.group_id = t4.group_id
     JOIN cabinet.storage_file_state t5 ON t1.file_state_id = t5.file_state_id
     LEFT JOIN cabinet.storage_file_version t11 ON t1.file_id = t11.file_id AND t11.version_num = 1
     LEFT JOIN cabinet.storage_file_version t12 ON t1.file_id = t12.file_id AND t12.version_num = 2
  WHERE t1.is_deleted = false
UNION ALL
 SELECT t1.link_id AS file_id,
    t1.link_name AS file_name,
    0 AS file_nom,
    t1.folder_id,
    0 AS ord,
    ''::character varying AS file_size,
    ''::character varying AS physical_path,
    ''::character varying AS physical_name,
    t1.download_link,
    0 AS file_type_id,
    0 AS file_state_id,
    false AS has_version,
    t1.crt_date,
    t1.crt_user,
    NULL::timestamp without time zone AS upd_date,
    ''::character varying AS upd_user,
    NULL::timestamp without time zone AS state_date,
    ''::character varying AS state_user,
    t2.folder_name,
    t2.folder_level,
    'url'::character varying AS file_type_name,
    0 AS group_id,
    'Внешние ссылки'::character varying AS group_name,
    ''::character varying AS file_state_name,
    'storage storage-url'::character varying AS sprite_css_class,
    t1.link_name AS file_name_full,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str,
    ''::text AS upd_date_str,
    ''::text AS state_date_str,
    ''::character varying AS file_size_str,
    t1.link_name AS file_name_with_extention,
    false AS v1_exists,
    NULL::integer AS v1_file_version_id,
    ''::character varying AS v1_file_size,
    ''::character varying AS v1_physical_path,
    ''::character varying AS v1_physical_name,
    ''::character varying AS v1_download_link,
    NULL::timestamp without time zone AS v1_crt_date,
    ''::character varying AS v1_crt_user,
    ''::character varying AS v1_file_name_full,
    ''::text AS v1_crt_date_str,
    ''::text AS v1_upd_date_str,
    ''::character varying AS v1_file_size_str,
    0 AS v1_version_num_actual,
    false AS v2_exists,
    NULL::integer AS v2_file_version_id,
    ''::character varying AS v2_file_size,
    ''::character varying AS v2_physical_path,
    ''::character varying AS v2_physical_name,
    ''::character varying AS v2_download_link,
    NULL::timestamp without time zone AS v2_crt_date,
    ''::character varying AS v2_crt_user,
    ''::character varying AS v2_file_name_full,
    ''::text AS v2_crt_date_str,
    ''::text AS v2_upd_date_str,
    ''::character varying AS v2_file_size_str,
    0 AS v2_version_num_actual,
        CASE
            WHEN t1.download_link::text ~~ 'file:%'::text THEN true
            ELSE false
        END AS is_local_link,
    NULL::timestamp without time zone AS file_date,
    ''::character varying AS file_date_str,
    2 AS item_type
   FROM cabinet.storage_link t1
     JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
  WHERE t1.is_deleted = false;

ALTER TABLE cabinet.vw_storage_file OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_log_storage;

CREATE OR REPLACE VIEW cabinet.vw_log_storage AS 
 SELECT t1.log_id,
    t1.date_beg,
    t1.date_end,
    t1.user_id,
    t1.mess,
    t1.folder_id,
    t1.file_id,
    t2.folder_name,
    t3.file_name,
    t4.user_name,
    t4.full_name
   FROM cabinet.log_storage t1
     LEFT JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
     LEFT JOIN cabinet.storage_file t3 ON t1.file_id = t3.file_id
     LEFT JOIN cabinet.cab_user t4 ON t1.user_id = t4.user_id;

ALTER TABLE cabinet.vw_log_storage OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_storage_folder_tree;

CREATE OR REPLACE VIEW cabinet.vw_storage_folder_tree AS 
 WITH RECURSIVE storage_folder_cte AS (
         SELECT t1.folder_id,
            NULL::integer AS parent_id,
            t1.folder_name,
            NULL::character varying AS parent_name,
            t1.folder_name AS full_path
           FROM cabinet.storage_folder t1
          WHERE t1.folder_level = 0
        UNION
         SELECT t1.folder_id,
            t2.folder_id,
            t1.folder_name,
            t2.folder_name,
            (t2.full_path::text || '\'::text) || t1.folder_name::text AS full_path
           FROM cabinet.storage_folder t1
             JOIN storage_folder_cte t2 ON t1.parent_id = t2.folder_id
        )
 SELECT storage_folder_cte.folder_id,
    storage_folder_cte.parent_id,
    storage_folder_cte.folder_name,
    storage_folder_cte.parent_name,
    storage_folder_cte.full_path
   FROM storage_folder_cte;

ALTER TABLE cabinet.vw_storage_folder_tree OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_storage_trash;

CREATE OR REPLACE VIEW cabinet.vw_storage_trash AS 
 SELECT t1.file_id AS item_id,
    t1.file_name AS item_name,
    t1.file_nom,
    t1.folder_id,
    t1.ord,
    t1.file_size,
    t1.physical_path,
    t1.physical_name,
    t1.download_link,
    t1.file_type_id,
    t1.file_state_id,
    t1.has_version,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.state_date,
    t1.state_user,
    t2.folder_name,
    t2.folder_level,
    t3.file_type_name,
    t3.group_id,
    t4.group_name,
    t5.file_state_name,
        CASE
            WHEN btrim(COALESCE(t4.img, ''::character varying)::text) = ''::text THEN 'storage storage-file'::text
            ELSE 'storage storage-'::text || btrim(t4.img::text)
        END::character varying AS sprite_css_class,
    (((((t1.file_name::text || '.'::text) || t3.file_type_name::text) || '  /  '::text) || btrim(to_char(t1.file_size::bigint, '9G999G999G999G999G999'::text))) || ' байт'::text)::character varying AS file_name_full,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str,
    to_char(t1.upd_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS upd_date_str,
    to_char(t1.state_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS state_date_str,
    (btrim(to_char(t1.file_size::bigint, '9G999G999G999G999G999'::text)) || ' байт'::text)::character varying AS file_size_str,
    ((btrim(t1.file_name::text) || '.'::text) || btrim(t3.file_type_name::text))::character varying AS item_name_with_extention,
    t1.del_date,
    to_char(t1.del_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS del_date_str,
    t1.del_user,
    1 AS item_type
   FROM cabinet.storage_file t1
     JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
     JOIN cabinet.storage_file_type t3 ON t1.file_type_id = t3.file_type_id
     JOIN cabinet.storage_file_type_group t4 ON t3.group_id = t4.group_id
     JOIN cabinet.storage_file_state t5 ON t1.file_state_id = t5.file_state_id
     LEFT JOIN cabinet.storage_file_version t11 ON t1.file_id = t11.file_id AND t11.version_num = 1
     LEFT JOIN cabinet.storage_file_version t12 ON t1.file_id = t12.file_id AND t12.version_num = 2
  WHERE t1.is_deleted = true
UNION ALL
 SELECT t1.link_id AS item_id,
    t1.link_name AS item_name,
    0 AS file_nom,
    t1.folder_id,
    0 AS ord,
    ''::character varying AS file_size,
    ''::character varying AS physical_path,
    ''::character varying AS physical_name,
    t1.download_link,
    0 AS file_type_id,
    0 AS file_state_id,
    false AS has_version,
    t1.crt_date,
    t1.crt_user,
    NULL::timestamp without time zone AS upd_date,
    ''::character varying AS upd_user,
    NULL::timestamp without time zone AS state_date,
    ''::character varying AS state_user,
    t2.folder_name,
    t2.folder_level,
    'url'::character varying AS file_type_name,
    0 AS group_id,
    'Внешние ссылки'::character varying AS group_name,
    ''::character varying AS file_state_name,
    'storage storage-url'::character varying AS sprite_css_class,
    t1.link_name AS file_name_full,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str,
    ''::text AS upd_date_str,
    ''::text AS state_date_str,
    ''::character varying AS file_size_str,
    t1.link_name AS item_name_with_extention,
    t1.del_date,
    to_char(t1.del_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS del_date_str,
    t1.del_user,
    2 AS item_type
   FROM cabinet.storage_link t1
     JOIN cabinet.storage_folder t2 ON t1.folder_id = t2.folder_id
  WHERE t1.is_deleted = true
UNION ALL
 SELECT t1.folder_id AS item_id,
    t1.folder_name AS item_name,
    0 AS file_nom,
    t1.folder_id,
    0 AS ord,
    ''::character varying AS file_size,
    ''::character varying AS physical_path,
    ''::character varying AS physical_name,
    ''::character varying AS download_link,
    0 AS file_type_id,
    0 AS file_state_id,
    false AS has_version,
    t1.crt_date,
    t1.crt_user,
    NULL::timestamp without time zone AS upd_date,
    ''::character varying AS upd_user,
    NULL::timestamp without time zone AS state_date,
    ''::character varying AS state_user,
    t1.folder_name,
    t1.folder_level,
    'folder'::character varying AS file_type_name,
    0 AS group_id,
    'Папки'::character varying AS group_name,
    ''::character varying AS file_state_name,
    'storage storage-folder'::character varying AS sprite_css_class,
    t1.folder_name AS file_name_full,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str,
    ''::text AS upd_date_str,
    ''::text AS state_date_str,
    ''::character varying AS file_size_str,
    t1.folder_name AS item_name_with_extention,
    t1.del_date,
    to_char(t1.del_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS del_date_str,
    t1.del_user,
    3 AS item_type
   FROM cabinet.storage_folder t1
  WHERE t1.is_deleted = true;

ALTER TABLE cabinet.vw_storage_trash OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_storage_folder;

CREATE OR REPLACE VIEW cabinet.vw_storage_folder AS 
 SELECT t1.folder_id,
    t1.folder_name,
    t1.parent_id,
    t1.folder_level,
    t1.crt_date,
    t1.crt_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    COALESCE(count(t11.file_id), 0::bigint) + COALESCE(count(t13.link_id), 0::bigint) AS file_cnt,
    count(t12.folder_id) AS folder_cnt,
    to_char(t1.crt_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS crt_date_str
   FROM cabinet.storage_folder t1
     LEFT JOIN cabinet.storage_file t11 ON t1.folder_id = t11.folder_id AND t11.is_deleted = false
     LEFT JOIN cabinet.storage_folder t12 ON t1.folder_id = t12.parent_id AND t12.is_deleted = false
     LEFT JOIN cabinet.storage_link t13 ON t1.folder_id = t13.folder_id AND t13.is_deleted = false
  WHERE t1.is_deleted = false
  GROUP BY t1.folder_id, t1.folder_name, t1.parent_id, t1.folder_level, t1.crt_date, t1.crt_user, t1.is_deleted, t1.del_date, t1.del_user;

ALTER TABLE cabinet.vw_storage_folder OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_service_kit_item;

CREATE OR REPLACE VIEW cabinet.vw_service_kit_item AS 
 SELECT t1.item_id,
    t1.service_kit_id,
    t1.service_id,
    t1.version_id,
    t2.service_kit_name,
    t3.name AS service_name,
    COALESCE(t11.name, 'Максимальная версия'::character varying) AS version_name
   FROM cabinet.service_kit_item t1
     JOIN cabinet.service_kit t2 ON t1.service_kit_id = t2.service_kit_id
     JOIN cabinet.service t3 ON t1.service_id = t3.id
     LEFT JOIN cabinet.version t11 ON t1.version_id = t11.id;

ALTER TABLE cabinet.vw_service_kit_item OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_cab_help_tag;

CREATE OR REPLACE VIEW cabinet.vw_cab_help_tag AS 
 SELECT DISTINCT btrim(regexp_split_to_table(cab_help.tag::text, '\|'::text)) AS tag
   FROM cabinet.cab_help;

ALTER TABLE cabinet.vw_cab_help_tag OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_crm_notify_param_attr;

CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_attr AS 
 SELECT row_number() OVER (ORDER BY t1.notify_id) AS adr,
    t1.notify_id,
    t1.user_id,
    t11.transport_type,
    t11.column_id,
    t12.column_name,
    t12.column_name_rus,
    t11.is_active
   FROM cabinet.crm_notify t1
     LEFT JOIN cabinet.crm_notify_param_attr t11 ON t1.notify_id = t11.notify_id
     LEFT JOIN cabinet.cab_grid_column t12 ON t11.column_id = t12.column_id AND t12.grid_id = 1;

ALTER TABLE cabinet.vw_crm_notify_param_attr OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_crm_notify_param_content;

CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_content AS 
 SELECT row_number() OVER (ORDER BY t1.notify_id) AS adr,
    t1.notify_id,
    t1.user_id,
    t11.transport_type,
    t11.column_id,
    t12.column_name,
    t12.column_name_rus,
    t11.is_active
   FROM cabinet.crm_notify t1
     LEFT JOIN cabinet.crm_notify_param_content t11 ON t1.notify_id = t11.notify_id
     LEFT JOIN cabinet.cab_grid_column t12 ON t11.column_id = t12.column_id AND t12.grid_id = 1;

ALTER TABLE cabinet.vw_crm_notify_param_content OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_crm_notify_param_user;

CREATE OR REPLACE VIEW cabinet.vw_crm_notify_param_user AS 
 SELECT row_number() OVER (ORDER BY t1.notify_id) AS adr,
    t1.notify_id,
    t1.user_id,
    t12.transport_type,
    t11.user_id AS changer_user_id,
    t11.user_name,
    t12.is_executer,
    t12.is_owner,
        CASE
            WHEN t12.user_id IS NULL THEN false
            ELSE true
        END AS is_active
   FROM cabinet.crm_notify t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1 AND t11.is_crm_user
     LEFT JOIN cabinet.crm_notify_param_user t12 ON t1.notify_id = t12.notify_id AND t11.user_id = t12.user_id;

ALTER TABLE cabinet.vw_crm_notify_param_user OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_compare_cabinet;

CREATE OR REPLACE VIEW cabinet.vw_client_compare_cabinet AS 
 SELECT t1.id,
    t1.city_id,
    t1.full_name,
    t1.inn,
    t1.kpp,
    t1.legal_address,
    t1.absolute_address,
    t1.bik,
    t1.cell AS phone,
    t1.site AS email,
    t1.contact_person,
    t1.zip_code,
    t1.post,
    t1.sm_id,
    t2.name AS city_name,
    t3.name AS region_name,
    t4.state_id,
    t4.state_name,
    t5.name_company_full AS crm_name_company_full,
    t5.name_region AS crm_name_region,
    t5.name_city AS crm_name_city,
    t5.address AS crm_address,
    t5.phone AS crm_phone,
    t5.email AS crm_email,
    t5.inn AS crm_inn,
    t5.kpp AS crm_kpp,
    t5.face_fio AS crm_face_fio,
    t5.p_address AS crm_p_address
   FROM cabinet.client t1
     LEFT JOIN cabinet.city t2 ON t1.city_id = t2.id
     LEFT JOIN cabinet.region t3 ON t2.region_id = t3.id
     LEFT JOIN cabinet.client_state t4 ON t1.state_id = t4.state_id
     LEFT JOIN cabinet.company t5 ON t1.sm_id = t5.id_company
  WHERE t1.is_deleted = 0;

ALTER TABLE cabinet.vw_client_compare_cabinet OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_compare_crm;

CREATE OR REPLACE VIEW cabinet.vw_client_compare_crm AS 
 SELECT t1.id_company,
    t1.name_company_full,
    t1.name_region,
    t1.name_city,
    t1.address,
    t1.phone,
    t1.email,
    t1.inn,
    t1.kpp,
    t1.face_fio,
    t1.p_address,
    t1.id_client,
    t1.id_state,
    t1.name_state,
    t2.cabinet_client_id
   FROM cabinet.company t1
     LEFT JOIN ( SELECT max(x1.id) AS cabinet_client_id,
            x1.sm_id
           FROM cabinet.client x1
          WHERE x1.sm_id IS NOT NULL
          GROUP BY x1.sm_id) t2 ON t1.id_company = t2.sm_id;

ALTER TABLE cabinet.vw_client_compare_crm OWNER TO pavlov;

--------------------

-- DROP VIEW cabinet.vw_client_max_version;

CREATE OR REPLACE VIEW cabinet.vw_client_max_version AS 
 SELECT row_number() OVER (ORDER BY ( SELECT 0)) AS adr,
    t1.id,
    t1.name,
    t1.inn,
    t1.kpp,
    t2.id AS sales_id,
    t2.adress,
    0 AS workplace_id,
    ''::text AS description,
    ''::character varying(255) AS registration_key,
    t4.service_name,
    t4.version_name
   FROM cabinet.client t1
     LEFT JOIN cabinet.sales t2 ON t1.id = t2.client_id AND t2.is_deleted = 0
     LEFT JOIN ( SELECT max(rpad(replace(t1_1.version_name::text, '.'::text, ''::text), 6, '0'::text)::integer) AS version_name_int_max,
            max(t1_1.version_name::text) AS version_name,
            max(t1_1.service_name::text) AS service_name,
            t1_1.client_id,
            t1_1.sales_id,
            t1_1.service_id
           FROM cabinet.vw_client_service_registered t1_1
          WHERE t1_1.version_id <> 86
          GROUP BY t1_1.client_id, t1_1.sales_id, t1_1.service_id) t4 ON t2.id = t4.sales_id
  WHERE t1.is_deleted = 0 AND t1.id <> 1000 AND t1.id <> 4990 AND t1.id <> 936;

ALTER TABLE cabinet.vw_client_max_version OWNER TO pavlov;

--------------------
-- DROP VIEW discount.vw_campaign;

CREATE OR REPLACE VIEW discount.vw_campaign AS 
 SELECT t1.campaign_id,
    t1.business_id,
    t1.campaign_name,
    t1.campaign_descr,
    t1.campaign_check_text,
    t1.date_beg,
    t1.date_end,
    t1.max_card_cnt,
    t1.card_type_id,
    t1.auto_create_card,
    t1.card_money_value,
    t1.state,
    t1.mess,
    t1.issue_date_beg,
    t1.issue_date_end,
    t1.issue_rule,
    t1.issue_condition_type,
    t1.issue_condition_sale_sum,
    t1.issue_condition_pos_count,
    t1.apply_condition_sale_sum,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t11.card_type_name,
    t12.card_count,
    t12.active_card_count
   FROM discount.campaign t1
     LEFT JOIN discount.card_type t11 ON t1.card_type_id = t11.card_type_id
     LEFT JOIN ( SELECT count(x1.card_id) AS card_count,
            sum(
                CASE x1.curr_card_status_id
                    WHEN 2 THEN 1
                    ELSE 0
                END) AS active_card_count,
            x1.curr_card_type_id
           FROM discount.card x1
          GROUP BY x1.curr_card_type_id) t12 ON t11.card_type_id = t12.curr_card_type_id;

ALTER TABLE discount.vw_campaign OWNER TO pavlov;
--------------------
-- DROP VIEW discount.vw_campaign_business_org;

CREATE OR REPLACE VIEW discount.vw_campaign_business_org AS 
 SELECT t1.campaign_id,
    t1.business_id,
    t2.org_id,
    t2.org_name,
    t3.item_id,
    t3.card_num_default,
        CASE
            WHEN COALESCE(t3.item_id, 0) <= 0 THEN false
            ELSE true
        END AS is_in_campaign
   FROM discount.campaign t1
     LEFT JOIN discount.business_org t2 ON t1.business_id = t2.business_id
     LEFT JOIN discount.campaign_business_org t3 ON t1.campaign_id = t3.campaign_id AND t2.org_id = t3.org_id;

ALTER TABLE discount.vw_campaign_business_org OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_update_batch;

CREATE OR REPLACE VIEW cabinet.vw_update_batch AS 
 SELECT t1.batch_id,
    t1.batch_name,
    t1.client_id,
    t1.crt_date,
    t1.crt_user,
    t1.last_download_date,
    t10.name AS client_name
   FROM cabinet.update_batch t1
     LEFT JOIN cabinet.client t10 ON t1.client_id = t10.id;

ALTER TABLE cabinet.vw_update_batch OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_update_batch_item;

CREATE OR REPLACE VIEW cabinet.vw_update_batch_item AS 
 SELECT t1.soft_id,
    t1.soft_name,
    t2.batch_id,
    t2.client_id,
        CASE
            WHEN COALESCE(t3.soft_id, 0) > 0 THEN true
            ELSE false
        END AS is_subscribed,
    t3.ord,
    t3.last_download_date
   FROM cabinet.update_soft t1
     LEFT JOIN cabinet.update_batch t2 ON 1 = 1
     LEFT JOIN cabinet.update_batch_item t3 ON t2.batch_id = t3.batch_id AND t1.soft_id = t3.soft_id
UNION ALL
 SELECT t1.soft_id,
    t1.soft_name,
    0 AS batch_id,
    NULL::integer AS client_id,
    false AS is_subscribed,
    NULL::integer AS ord,
    NULL::timestamp without time zone AS last_download_date
   FROM cabinet.update_soft t1;

ALTER TABLE cabinet.vw_update_batch_item OWNER TO pavlov;

--------------------
-- DROP VIEW cabinet.vw_prj_work_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_work_simple AS 
 SELECT t1.work_id,
    t1.task_id,
    t1.claim_id,
    t1.state_type_id,
    t2.done_or_canceled,
    t1.work_type_id,
    t1.work_num,
    t1.exec_user_id,
    t3.user_name AS exec_user_name
   FROM cabinet.prj_work t1
     JOIN cabinet.prj_work_state_type t2 ON t1.state_type_id = t2.state_type_id
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id;

ALTER TABLE cabinet.vw_prj_work_simple OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_prj_task_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_task_simple AS 
 SELECT t1.task_id,
    t1.claim_id,
        CASE
            WHEN COALESCE(t11.work_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND COALESCE(t11.active_work_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND t11.work_cnt = t11.done_work_cnt THEN 3
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND COALESCE(t11.active_work_cnt, 0::bigint) <= 0 AND COALESCE(t11.done_work_cnt, 0::bigint) > 0 AND COALESCE(t11.canceled_work_cnt, 0::bigint) > 0 THEN 3
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND t11.work_cnt = t11.canceled_work_cnt THEN 4
            ELSE 2
        END AS task_state_type_id,
    t11.work_cnt,
    t11.active_work_cnt,
    t11.done_work_cnt,
    t11.canceled_work_cnt,
    t11.active_work_type_id,
    t11.exec_user_name_list
   FROM cabinet.prj_task t1
     LEFT JOIN ( SELECT count(x1.work_id) AS work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 0 THEN 1
                    ELSE 0
                END) AS active_work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 1 THEN 1
                    ELSE 0
                END) AS done_work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 2 THEN 1
                    ELSE 0
                END) AS canceled_work_cnt,
            min(
                CASE
                    WHEN x1.done_or_canceled = 0 THEN x1.work_type_id
                    ELSE NULL::integer
                END) AS active_work_type_id,
            array_to_string(array_agg(distinct x1.exec_user_name), ','::text) AS exec_user_name_list,
            x1.task_id
           FROM cabinet.vw_prj_work_simple x1
          GROUP BY x1.task_id) t11 ON t1.task_id = t11.task_id;

ALTER TABLE cabinet.vw_prj_task_simple OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_prj_claim_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple AS 
 SELECT t1.claim_id,
        CASE
            WHEN COALESCE(t11.task_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.new_task_cnt THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.done_task_cnt THEN 3
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t11.new_task_cnt, 0::bigint) <= 0 AND t11.task_cnt <> t11.canceled_task_cnt AND t11.task_cnt <> t11.done_task_cnt THEN 3
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t11.new_task_cnt, 0::bigint) > 0 AND t11.task_cnt <> t11.canceled_task_cnt AND t11.task_cnt <> t11.done_task_cnt THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.canceled_task_cnt THEN 4
            ELSE 2
        END AS claim_state_type_id,
    t11.task_cnt,
    t11.new_task_cnt,
    t11.active_task_cnt,
    t11.done_task_cnt,
    t11.canceled_task_cnt,
    t11.active_work_type_id,
    t11.exec_user_name_list
   FROM cabinet.prj_claim t1
     LEFT JOIN ( SELECT x1.claim_id,
            count(x1.task_id) AS task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 1 THEN 1
                    ELSE 0
                END) AS new_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 2 THEN 1
                    ELSE 0
                END) AS active_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 3 THEN 1
                    ELSE 0
                END) AS done_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 4 THEN 1
                    ELSE 0
                END) AS canceled_task_cnt,
            min(x1.active_work_type_id) AS active_work_type_id,
            min(x1.exec_user_name_list) AS exec_user_name_list
           FROM cabinet.vw_prj_task_simple x1
          GROUP BY x1.claim_id) t11 ON t1.claim_id = t11.claim_id;

ALTER TABLE cabinet.vw_prj_claim_simple OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_prj;

CREATE OR REPLACE VIEW cabinet.vw_prj AS 
 SELECT t1.prj_id,
    t1.prj_name,
    t1.date_beg,
    t1.date_end,
    t1.curr_state_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    COALESCE(t11.claim_cnt, 0::bigint) AS claim_cnt,
    0::bigint AS new_claim_cnt,
    0::bigint AS active_claim_cnt,
    0::bigint AS done_claim_cnt,
    0::bigint AS canceled_claim_cnt,
    t12.state_type_id,
    t13.state_type_name,
    t13.templ AS state_type_name_templ,
    0::bigint AS claim_approved_cnt,
    0::bigint AS claim_new_cnt,
    t1.is_archive,
    t1.project_id,
    t16.project_name
   FROM cabinet.prj t1
     LEFT JOIN ( SELECT x1.prj_id,
            count(x1.claim_id) AS claim_cnt
           FROM cabinet.prj_claim_prj x1
          GROUP BY x1.prj_id) t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_state t12 ON t1.curr_state_id = t12.state_id
     LEFT JOIN cabinet.prj_state_type t13 ON t12.state_type_id = t13.state_type_id
     LEFT JOIN cabinet.crm_project t16 ON t1.project_id = t16.project_id;

ALTER TABLE cabinet.vw_prj OWNER TO pavlov;

--------------------
-- DROP VIEW cabinet.vw_prj_claim;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim AS 
 SELECT t1.claim_id,
    t1.claim_num,
    t1.claim_name,
    t1.claim_text,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.project_version_id,
    t1.client_id,
    t1.priority_id,
    t1.resp_user_id,
    t1.date_plan,
    t1.date_fact,
    t1.project_repair_version_id,
    t1.is_archive,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.is_control,
    t1.old_task_id,
    t2.project_name,
    t3.module_name,
    t4.module_part_name,
    t5.project_version_name,
    t6.client_name,
    t7.priority_name,
    t8.user_name AS resp_user_name,
    t11.project_version_name AS project_repair_version_name,
    COALESCE(t12.task_cnt, 0::bigint) AS task_cnt,
    COALESCE(t12.new_task_cnt, 0::bigint) AS new_task_cnt,
    COALESCE(t12.active_task_cnt, 0::bigint) AS active_task_cnt,
    COALESCE(t12.done_task_cnt, 0::bigint) AS done_task_cnt,
    COALESCE(t12.canceled_task_cnt, 0::bigint) AS canceled_task_cnt,
    t12.claim_state_type_id,
    t13.state_type_name AS claim_state_type_name,
    t13.templ AS claim_state_type_templ,
    t1.old_group_id,
    t1.old_state_id,
    t14.group_name AS old_group_name,
    t15.state_name AS old_state_name,
    t12.active_work_type_id,
    t16.work_type_name AS active_work_type_name,
        CASE
            WHEN t17.claim_id IS NULL THEN false
            ELSE true
        END AS is_overdue,
    t18.state_type_id AS brief_state_type_id,
    t18.state_type_name AS brief_state_type_name,
    t12.exec_user_name_list,
    t1.claim_type_id,
    t1.claim_stage_id,
    t50.claim_type_name,
    t51.claim_stage_name,
    t19.mess_cnt,
    t7.rate AS priority_rate,
    t50.rate AS claim_type_rate,
    COALESCE(t7.rate, 0::numeric) * COALESCE(t50.rate, 0::numeric) AS summary_rate,
        CASE t12.claim_state_type_id
            WHEN 1 THEN true
            WHEN 2 THEN true
            ELSE false
        END AS is_active_state,
    t20.prj_id_list,
    t20.prj_name_list,
    t1.version_id,
    t1.is_version_fixed,
        CASE t1.is_version_fixed
            WHEN true THEN t1.version_value
            ELSE t52.version_name
        END AS version_name,
        CASE t1.is_version_fixed
            WHEN true THEN t1.version_value
            ELSE t52.version_value
        END AS version_value,
    (t52.version_name::text || ' '::text) ||
        CASE t1.is_version_fixed
            WHEN true THEN t1.version_value
            ELSE t52.version_value
        END::text AS version_value_full,
    t53.version_type_id,
    t53.version_type_name,
    t1.arc_date,
    t1.arc_user
   FROM cabinet.prj_claim t1
     JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_project_version t5 ON t1.project_version_id = t5.project_version_id
     JOIN cabinet.crm_client t6 ON t1.client_id = t6.client_id
     JOIN cabinet.crm_priority t7 ON t1.priority_id = t7.priority_id
     JOIN cabinet.cab_user t8 ON t1.resp_user_id = t8.user_id
     JOIN cabinet.prj_claim_type t50 ON t1.claim_type_id = t50.claim_type_id
     JOIN cabinet.prj_claim_stage t51 ON t1.claim_stage_id = t51.claim_stage_id
     JOIN cabinet.prj_project_version t52 ON t1.version_id = t52.version_id
     JOIN cabinet.prj_version_type t53 ON t52.version_type_id = t53.version_type_id
     LEFT JOIN cabinet.crm_project_version t11 ON t1.project_repair_version_id = t11.project_version_id
     LEFT JOIN cabinet.vw_prj_claim_simple t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_claim_state_type t13 ON t12.claim_state_type_id = t13.state_type_id
     LEFT JOIN cabinet.crm_group t14 ON t1.old_group_id = t14.group_id
     LEFT JOIN cabinet.crm_state t15 ON t1.old_state_id = t15.state_id
     LEFT JOIN cabinet.prj_work_type t16 ON t12.active_work_type_id = t16.work_type_id
     LEFT JOIN LATERAL ( SELECT x2.claim_id,
            min(x1.date_plan) AS min_date_plan
           FROM cabinet.prj_task x1
             JOIN cabinet.prj_work x2 ON x1.task_id = x2.task_id
             JOIN cabinet.prj_work_state_type x3 ON x2.state_type_id = x3.state_type_id
          WHERE x3.done_or_canceled = 0
          GROUP BY x2.claim_id) t17 ON t1.claim_id = t17.claim_id AND GREATEST(COALESCE(t1.date_plan, '2000-01-01'::date), COALESCE(t17.min_date_plan, '2000-01-01'::date)) < 'now'::text::date
     LEFT JOIN cabinet.prj_brief_state_type t18 ON t13.brief_state_type_id = t18.state_type_id
     LEFT JOIN ( SELECT x1.claim_id,
            count(x1.mess_id) AS mess_cnt
           FROM cabinet.prj_mess x1
          GROUP BY x1.claim_id) t19 ON t19.claim_id = t1.claim_id
     LEFT JOIN ( SELECT x1.claim_id,
            array_to_string(array_agg(x1.prj_id), ','::text) AS prj_id_list,
            array_to_string(array_agg(x2.prj_name), ','::text) AS prj_name_list
           FROM cabinet.prj_claim_prj x1
             JOIN cabinet.prj x2 ON x1.prj_id = x2.prj_id
          GROUP BY x1.claim_id) t20 ON t1.claim_id = t20.claim_id;

ALTER TABLE cabinet.vw_prj_claim OWNER TO pavlov;

--------------------
-- DROP VIEW cabinet.vw_prj_task;

CREATE OR REPLACE VIEW cabinet.vw_prj_task AS 
 SELECT t1.task_id,
    t1.claim_id,
    t1.task_num,
    t1.task_text,
    t1.date_plan,
    t1.date_fact,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.claim_num,
    t2.claim_name,
    t2.claim_text,
    t2.project_id AS claim_project_id,
    t2.module_id AS claim_module_id,
    t2.module_part_id AS claim_module_part_id,
    t2.project_version_id AS claim_project_version_id,
    t2.client_id AS claim_client_id,
    t2.priority_id AS claim_priority_id,
    t2.resp_user_id AS claim_resp_user_id,
    t2.date_plan AS claim_date_plan,
    t2.date_fact AS claim_date_fact,
    t2.project_repair_version_id AS claim_project_repair_version_id,
    t2.is_archive AS claim_is_archive,
    t2.is_control AS claim_is_control,
    t2.project_name AS claim_project_name,
    t2.module_name AS claim_module_name,
    t2.module_part_name AS claim_module_part_name,
    t2.project_version_name AS claim_project_version_name,
    t2.client_name AS claim_client_name,
    t2.priority_name AS claim_priority_name,
    t2.resp_user_name AS claim_resp_user_name,
    t2.project_repair_version_name AS claim_project_repair_version_name,
    t2.claim_state_type_id,
    t2.claim_state_type_name,
    t2.claim_state_type_templ,
    t11.task_state_type_id,
    COALESCE(t11.work_cnt, 0::bigint) AS work_cnt,
    COALESCE(t11.active_work_cnt, 0::bigint) AS active_work_cnt,
    COALESCE(t11.done_work_cnt, 0::bigint) AS done_work_cnt,
    COALESCE(t11.canceled_work_cnt, 0::bigint) AS canceled_work_cnt,
    t12.state_type_name AS task_state_type_name,
    t12.templ AS task_state_type_templ,
    t1.old_state_id,
    t13.state_name AS old_state_name,
    t11.active_work_type_id,
    t14.work_type_name AS active_work_type_name,
    false AS is_overdue,
    t2.claim_type_id,
    t2.claim_stage_id,
    t2.claim_type_name,
    t2.claim_stage_name,
    t15.task_mess_cnt,
    t2.prj_id_list,
    t2.prj_name_list
   FROM cabinet.prj_task t1
     JOIN cabinet.vw_prj_claim t2 ON t1.claim_id = t2.claim_id
     LEFT JOIN cabinet.vw_prj_task_simple t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.prj_task_state_type t12 ON t11.task_state_type_id = t12.state_type_id
     LEFT JOIN cabinet.crm_state t13 ON t1.old_state_id = t13.state_id
     LEFT JOIN cabinet.prj_work_type t14 ON t11.active_work_type_id = t14.work_type_id
     LEFT JOIN ( SELECT x1.task_id,
            count(x1.mess_id) AS task_mess_cnt
           FROM cabinet.prj_mess x1
          GROUP BY x1.task_id) t15 ON t15.task_id = t1.task_id;

ALTER TABLE cabinet.vw_prj_task OWNER TO pavlov;
--------------------

-- DROP VIEW cabinet.vw_prj_work;

CREATE OR REPLACE VIEW cabinet.vw_prj_work AS 
 SELECT t1.work_id,
    t1.task_id,
    t1.claim_id,
    t1.work_num,
    t1.work_type_id,
    t1.exec_user_id,
    t1.state_type_id,
    t1.is_plan,
    t1.date_beg,
    t1.date_end,
    t1.is_accept,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.cost_plan,
    t1.cost_fact,
    t1.mess,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.work_type_name,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    t4.templ AS state_type_templ,
    t5.task_num,
    t5.task_text,
    t5.task_state_type_id,
    t5.date_plan AS task_date_plan,
    t5.date_fact AS task_date_fact,
    t5.task_state_type_name,
    t5.task_state_type_templ,
    t5.claim_num,
    t5.claim_name,
    t5.claim_text,
    t5.claim_project_id,
    t5.claim_module_id,
    t5.claim_module_part_id,
    t5.claim_project_version_id,
    t5.claim_client_id,
    t5.claim_priority_id,
    t5.claim_resp_user_id,
    t5.claim_date_plan,
    t5.claim_date_fact,
    t5.claim_project_repair_version_id,
    t5.claim_is_archive,
    t5.claim_is_control,
    t5.claim_project_name,
    t5.claim_module_name,
    t5.claim_module_part_name,
    t5.claim_project_version_name,
    t5.claim_client_name,
    t5.claim_priority_name,
    t5.claim_resp_user_name,
    t5.claim_project_repair_version_name,
    t5.claim_state_type_id,
    t5.claim_state_type_name,
    t5.claim_state_type_templ,
        CASE
            WHEN t4.done_or_canceled = 0 AND GREATEST(COALESCE(t5.claim_date_plan, '2000-01-01'::date), COALESCE(t5.date_plan, '2000-01-01'::date)) < 'now'::text::date THEN true
            ELSE false
        END AS is_overdue,
    t1.date_time_beg,
    t1.date_time_end,
    t1.is_all_day,
    t5.claim_type_id,
    t5.claim_stage_id,
    t5.claim_type_name,
    t5.claim_stage_name,
    t11.state_type_id AS brief_state_type_id,
    t11.state_type_name AS brief_state_type_name,
    t1.date_send,
    t5.prj_id_list,
    t5.prj_name_list
   FROM cabinet.prj_work t1
     JOIN cabinet.prj_work_type t2 ON t1.work_type_id = t2.work_type_id
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id
     JOIN cabinet.vw_prj_task t5 ON t1.task_id = t5.task_id
     LEFT JOIN cabinet.prj_brief_state_type t11 ON t4.brief_state_type_id = t11.state_type_id;

ALTER TABLE cabinet.vw_prj_work  OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_prj_mess;

CREATE OR REPLACE VIEW cabinet.vw_prj_mess AS 
 SELECT t1.mess_id,
    t1.mess,
    t1.mess_num,
    t1.user_id,
    t1.prj_id,
    t1.claim_id,
    t1.task_id,
    t1.work_id,
    t1.crt_date,
    t2.user_name,
    t12.claim_num,
    t13.task_num,
    t14.work_num
   FROM cabinet.prj_mess t1
     JOIN cabinet.cab_user t2 ON t1.user_id = t2.user_id
     LEFT JOIN cabinet.prj t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_claim t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_task t13 ON t1.task_id = t13.task_id
     LEFT JOIN cabinet.prj_work t14 ON t1.work_id = t14.work_id;

ALTER TABLE cabinet.vw_prj_mess OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_prj_log;

CREATE OR REPLACE VIEW cabinet.vw_prj_log AS 
 SELECT t1.log_id,
    t1.mess,
    t1.prj_id,
    t1.claim_id,
    t1.task_id,
    t1.work_id,
    t1.crt_date,
    t1.crt_user,
    t12.claim_num,
    t13.task_num,
    t14.work_num,
    t12.is_archive AS claim_is_archive,
    t11.is_archive AS prj_is_archive
   FROM cabinet.prj_log t1
     LEFT JOIN cabinet.prj t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_claim t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_task t13 ON t1.task_id = t13.task_id
     LEFT JOIN cabinet.prj_work t14 ON t1.work_id = t14.work_id;

ALTER TABLE cabinet.vw_prj_log OWNER TO pavlov;
--------------------
-- DROP VIEW cabinet.vw_prj_work_default;

CREATE OR REPLACE VIEW cabinet.vw_prj_work_default AS 
 SELECT t2.user_id,
    t1.work_type_id,
        CASE
            WHEN t3.item_id IS NOT NULL THEN true
            ELSE false
        END AS is_default,
        CASE
            WHEN t3.item_id IS NOT NULL THEN t3.is_accept
            ELSE false
        END AS is_accept,
        CASE
            WHEN t3.item_id IS NOT NULL THEN t3.exec_user_id
            ELSE 0
        END AS exec_user_id,
    t4.work_type_name,
    t5.user_name AS exec_user_name
   FROM cabinet.prj_work_type t1
     JOIN cabinet.cab_user t2 ON 1 = 1
     LEFT JOIN cabinet.prj_work_default t3 ON t1.work_type_id = t3.work_type_id AND t2.user_id = t3.user_id
     LEFT JOIN cabinet.prj_work_type t4 ON t1.work_type_id = t4.work_type_id
     LEFT JOIN cabinet.cab_user t5 ON COALESCE(t3.exec_user_id, 0) = t5.user_id;

ALTER TABLE cabinet.vw_prj_work_default OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_doc_product_filter;

CREATE OR REPLACE VIEW und.vw_doc_product_filter AS 
 SELECT t2.doc_id, (doc_name || ' (' || count(t1.product_id)::character varying || ')')::character varying as doc_name, count(t1.product_id) as product_cnt
 FROM und.product t1
 INNER JOIN und.doc t2 ON t1.doc_id = t2.doc_id
 GROUP BY t2.doc_id, t2.doc_name
 UNION ALL
 SELECT 0::int as doc_id, ('Ручной ввод (' || count(t1.product_id)::character varying || ')')::character varying as doc_name, count(t1.product_id) as product_cnt
 FROM und.product t1
 WHERE t1.doc_id is null;

ALTER TABLE und.vw_doc_product_filter OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW discount.vw_report_card_fstsale;

CREATE OR REPLACE VIEW discount.vw_report_card_fstsale AS 
 SELECT t1.business_id,
    t1.card_id,
    t1.card_num,
    t1.card_num2,
    t1.curr_holder_id,
    t9.card_holder_surname AS curr_holder_name,
    t9.card_holder_name AS curr_holder_first_name,
    t9.card_holder_fname AS curr_holder_second_name,
    t9.phone_num AS curr_holder_phone_num,
    t5.org_name AS card_fstsale_org_name
   FROM discount.card t1
     LEFT JOIN discount.card_holder t9 ON t1.curr_holder_id = t9.card_holder_id
     LEFT JOIN LATERAL ( SELECT t2.card_trans_id,
            t2.card_id,
            y2.org_name
           FROM discount.card_transaction t2
             JOIN discount.business_org_user y1 ON btrim(lower(COALESCE(y1.user_name, ''::character varying)::text)) = btrim(lower(COALESCE(t2.user_name, ''::character varying)::text))
             JOIN discount.business_org y2 ON y1.org_id = y2.org_id
          WHERE t1.card_id = t2.card_id AND t2.trans_kind = 1 AND COALESCE(t2.status, 0) = 0
          ORDER BY t2.card_trans_id
         LIMIT 1) t5 ON t5.card_id = t1.card_id;

ALTER TABLE discount.vw_report_card_fstsale OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_partner_good;

CREATE OR REPLACE VIEW und.vw_partner_good AS 
 SELECT t1.item_id,
    t1.partner_id,
    t1.good_id,
    t1.partner_code,
    t1.partner_name,
    t2.partner_name AS source_partner_name,
    t3.product_id,
    t3.mfr_id,
    t3.barcode,
    t3.doc_id,
    t3.doc_row_id,
    t4.product_name,
    t5.mfr_name,
    t3.is_deleted,
    t4.apply_group_id,
    t11.apply_group_name,
    t3.is_main
   FROM und.good t3
     JOIN und.manufacturer t5 ON t3.mfr_id = t5.mfr_id
     JOIN und.partner_good t1 ON t1.good_id = t3.good_id
     JOIN und.partner t2 ON t1.partner_id = t2.partner_id
     JOIN und.product t4 ON t3.product_id = t4.product_id
     LEFT JOIN und.apply_group t11 ON t4.apply_group_id = t11.apply_group_id;

ALTER TABLE und.vw_partner_good OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_product;

CREATE OR REPLACE VIEW und.vw_product AS 
 SELECT t1.product_id,
    t1.product_form_id,
    t1.pharm_market_id,
    t1.inn_id,
    t1.dosage_id,
    t1.brand_id,
    t1.space_id,
    t1.metaname_id,
    t1.product_name,
    t1.trade_name,
    t1.trade_name_lat,
    t1.trade_name_eng,
    t1.storage_period,
    t1.is_vital,
    t1.is_powerful,
    t1.is_poison,
    t1.is_receipt,
    t1.drug,
    t1.size,
    t1.potion_amount,
    t1.pack_count,
    t1.link,
    t1.mess,
    t1.doc_id,
    t1.doc_row_id,
    t1.is_deleted,
    COALESCE(t11.good_cnt, 0::bigint) AS good_cnt,
    t1.apply_group_id,
    t12.apply_group_name
   FROM und.product t1
     LEFT JOIN ( SELECT x1.product_id,
            count(x1.item_id) AS good_cnt
           FROM und.vw_partner_good x1
          GROUP BY x1.product_id) t11 ON t1.product_id = t11.product_id
     LEFT JOIN und.apply_group t12 ON t1.apply_group_id = t12.apply_group_id;

ALTER TABLE und.vw_product OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_partner_good_filter;

CREATE OR REPLACE VIEW und.vw_partner_good_filter AS 
 SELECT t1.partner_id,
    (((t1.source_partner_name::text || ' ('::text) || count(t1.good_id)::character varying::text) || ')'::text)::character varying AS partner_name,
    count(t1.good_id) AS good_cnt
   FROM und.vw_partner_good t1
  GROUP BY t1.partner_id, t1.source_partner_name;

ALTER TABLE und.vw_partner_good_filter OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_product_and_good;

CREATE OR REPLACE VIEW und.vw_product_and_good AS 
 SELECT row_number() OVER (ORDER BY NULL::text) AS item_id,
    t1.product_id,
    t1.product_name,
    t1.doc_id,
    t1.doc_row_id,
    t1.is_deleted,
    count(t11.*) OVER (PARTITION BY t1.product_id) AS good_cnt,
    t11.partner_id,
    t11.good_id,
    t11.partner_code,
    t11.partner_name,
    t11.source_partner_name,
    t11.mfr_id,
    t11.barcode,
    t11.doc_id AS good_doc_id,
    t11.doc_row_id AS good_doc_row_id,
    t11.mfr_name,
    t11.is_deleted AS good_is_deleted,
    t1.apply_group_id,
    t12.apply_group_name,
    t11.is_main
   FROM und.product t1
     LEFT JOIN und.vw_partner_good t11 ON t1.product_id = t11.product_id
     LEFT JOIN und.apply_group t12 ON t1.apply_group_id = t12.apply_group_id;

ALTER TABLE und.vw_product_and_good OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_doc;

CREATE OR REPLACE VIEW und.vw_doc AS 
 SELECT t1.doc_id,
    t1.doc_num,
    t1.doc_name,
    t1.doc_date,
    t1.doc_type_id,
    t1.doc_state_id,
    t1.partner_id,
    t1.partner_target_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t11.doc_type_name,
    t12.doc_state_name,
    t13.partner_name,
    t15.row_cnt,
    t15.unmatch_cnt,
    t15.new_cnt,
    t15.syn_cnt
   FROM und.doc t1
     LEFT JOIN und.doc_type t11 ON t1.doc_type_id = t11.doc_type_id
     LEFT JOIN und.doc_state t12 ON t1.doc_state_id = t12.doc_state_id
     LEFT JOIN und.partner t13 ON t1.partner_id = t13.partner_id
     LEFT JOIN und.doc_import t14 ON t1.doc_id = t14.doc_id
     LEFT JOIN LATERAL ( SELECT count(x1.row_id) AS row_cnt,
            sum(
                CASE
                    WHEN x1.state = 0 THEN 1
                    ELSE 0
                END) AS unmatch_cnt,
            sum(
                CASE
                    WHEN x1.state = 1 THEN 1
                    ELSE 0
                END) AS new_cnt,
            sum(
                CASE
                    WHEN x1.state = 2 THEN 1
                    ELSE 0
                END) AS syn_cnt,
            x1.import_id
           FROM und.doc_import_match x1
          WHERE x1.import_id = t14.import_id
          GROUP BY x1.import_id) t15 ON t14.import_id = t15.import_id;

ALTER TABLE und.vw_doc OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_doc_import_match;

CREATE OR REPLACE VIEW und.vw_doc_import_match AS 
 SELECT t1.row_id,
    t1.import_id,
    t1.row_num,
    t1.source_name,
    t1.code,
    t1.mfr,
    t1.mfr_group,
    t1.barcode,
    t1.state,
    t1.match_product_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t3.doc_id,
    t3.doc_num,
    t3.doc_name,
    t3.doc_date,
    t3.doc_type_id,
    t3.doc_state_id,
    t3.partner_id,
    t3.doc_type_name,
    t3.doc_state_name,
    t3.partner_name,
    t11.product_name AS match_product_name,
    t1.match_good_id,
    t12.barcode AS match_barcode,
    t12.mfr_id AS match_mfr_id,
    t13.mfr_name AS match_mfr_name,
    t1.is_approved,
    t1.percent,
    t1.is_auto_match,
    t1.name_percent,
    t1.barcode_percent,
    t1.mfr_percent,
    t1.init_source_name,
    t1.apply_group_name
   FROM und.doc_import_match t1
     JOIN und.doc_import t2 ON t1.import_id = t2.import_id
     JOIN und.vw_doc t3 ON t2.doc_id = t3.doc_id
     LEFT JOIN und.product t11 ON t1.match_product_id = t11.product_id
     LEFT JOIN und.good t12 ON t1.match_good_id = t12.good_id
     LEFT JOIN und.manufacturer t13 ON t12.mfr_id = t13.mfr_id;

ALTER TABLE und.vw_doc_import_match OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_doc_import_match_variant;

CREATE OR REPLACE VIEW und.vw_doc_import_match_variant AS 
 SELECT t1.variant_id,
    t1.row_id,
    t1.match_product_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.match_good_id,
    t1.percent,
    t1.is_match_name,
    t1.is_match_barcode,
    t1.is_match_mfr,
    t1.name_similarity,
    t1.barcode_similarity,
    t1.mfr_similarity,
    t1.avg_similarity,
    t2.doc_id,
    t11.product_name AS match_product_name,
    t12.barcode AS match_barcode,
    t12.mfr_id AS match_mfr_id,
    t13.mfr_name AS match_mfr_name,
    t1.barcode_percent,
    t1.name_percent,
    t1.mfr_percent
   FROM und.doc_import_match_variant t1
     JOIN und.vw_doc_import_match t2 ON t1.row_id = t2.row_id
     LEFT JOIN und.product t11 ON t1.match_product_id = t11.product_id
     LEFT JOIN und.good t12 ON t1.match_good_id = t12.good_id
     LEFT JOIN und.manufacturer t13 ON t12.mfr_id = t13.mfr_id;

ALTER TABLE und.vw_doc_import_match_variant OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_doc_import;

CREATE OR REPLACE VIEW und.vw_doc_import AS 
 SELECT t1.import_id,
    t1.config_type_id,
    t1.file_path,
    t1.file_name,
    t1.doc_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.source_partner_id,
    t1.row_cnt,
    t1.state,
    COALESCE(t11.row_done_cnt, 0::bigint) AS row_done_cnt,
    t12.doc_name
   FROM und.doc_import t1
     LEFT JOIN LATERAL ( SELECT x1.import_id,
            count(x1.row_id) AS row_done_cnt
           FROM und.doc_import_row x1
          WHERE x1.import_id = t1.import_id
          GROUP BY x1.import_id) t11 ON t1.import_id = t11.import_id
     LEFT JOIN und.doc t12 ON t1.doc_id = t12.doc_id;

ALTER TABLE und.vw_doc_import OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_prj_file;

CREATE OR REPLACE VIEW cabinet.vw_prj_file AS 
 SELECT t1.file_id,
    t1.file_name,
    t1.file_link,
    t1.file_path,
    t1.prj_id,
    t1.claim_id,
    t1.task_id,
    t1.work_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    ((((('<a href="'::text || t1.file_link::text) || '" target="_blank" title="'::text) || t1.file_name::text) || '">'::text) || t1.file_name::text) || '</a>'::text AS file_name_url,
    t11.claim_num
   FROM cabinet.prj_file t1
     LEFT JOIN cabinet.prj_claim t11 ON t1.claim_id = t11.claim_id;

ALTER TABLE cabinet.vw_prj_file OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_stock_row;

CREATE OR REPLACE VIEW und.vw_stock_row AS 
 SELECT t1.row_id,
    t1.stock_id,
    t1.batch_id,
    t1.artikul,
    t1.row_stock_id,
    t1.esn_id,
    t1.prep_id,
    t1.prep_name,
    t1.firm_id,
    t1.firm_name,
    t1.country_name,
    t1.unpacked_cnt,
    t1.all_cnt,
    t1.price,
    t1.valid_date,
    t1.series,
    t1.is_vital,
    t1.barcode,
    t1.price_firm,
    t1.price_gr,
    t1.percent_gross,
    t1.price_gross,
    t1.sum_gross,
    t1.percent_nds_gross,
    t1.price_nds_gross,
    t1.sum_nds_gross,
    t1.percent,
    t1.sum,
    t1.supplier_name,
    t1.supplier_doc_num,
    t1.gr_date,
    t1.farm_group_name,
    t1.supplier_doc_date,
    t1.profit,
    t1.mess,
    t1.state,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.client_id,
    t2.sales_id,
    t2.depart_id,
    t2.depart_name,
    t2.pharmacy_name,
    t3.batch_num,
    t3.part_cnt,
    t3.part_num,
    t3.row_cnt,
    t3.crt_date AS batch_crt_date,
    t4.name AS client_name,
    t5.adress AS sales_name,
    t2.depart_address,
    t3.is_active,
    t3.inactive_date AS batch_inactive_date,
    t3.is_confirmed AS batch_is_confirmed,
    t3.confirm_date AS batch_confirm_date
   FROM und.stock_row t1
     JOIN und.stock t2 ON t1.stock_id = t2.stock_id
     JOIN und.stock_batch t3 ON t1.batch_id = t3.batch_id
     JOIN cabinet.client t4 ON t2.client_id = t4.id
     JOIN cabinet.sales t5 ON t2.sales_id = t5.id
     WHERE t1.is_deleted != true;

ALTER TABLE und.vw_stock_row OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_stock;

CREATE OR REPLACE VIEW und.vw_stock AS 
 SELECT t1.stock_id,
    t1.client_id,
    t1.sales_id,
    t1.depart_id,
    t1.depart_name,
    t1.pharmacy_name,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.name AS client_name,
    t3.adress AS sales_name,
    t1.depart_address
   FROM und.stock t1
     JOIN cabinet.client t2 ON t1.client_id = t2.id
     JOIN cabinet.sales t3 ON t1.sales_id = t3.id;

ALTER TABLE und.vw_stock OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_stock_batch;

CREATE OR REPLACE VIEW und.vw_stock_batch AS 
 SELECT t1.batch_id,
    t1.stock_id,
    t1.batch_num,
    t1.part_cnt,
    t1.part_num,
    t1.row_cnt,
    t1.is_active,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.client_id,
    t2.sales_id,
    t2.depart_id,
    t2.depart_name,
    t2.pharmacy_name,
    t3.name AS client_name,
    t4.adress AS sales_name,
    t1.inactive_date,
    t1.is_confirmed,
    t1.confirm_date
   FROM und.stock_batch t1
     JOIN und.stock t2 ON t1.stock_id = t2.stock_id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
     JOIN cabinet.sales t4 ON t2.sales_id = t4.id
  WHERE t1.is_deleted = false;

ALTER TABLE und.vw_stock_batch OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_stock_sales_download;

CREATE OR REPLACE VIEW und.vw_stock_sales_download AS 
 SELECT t1.download_id,
    t1.sales_id,
    t1.part_cnt,
    t1.part_num,
    t1.row_cnt,
    t1.is_active,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.download_batch_date_beg,
    t1.download_batch_date_end,
    t1.batch_row_cnt,
    t2.adress AS sales_name,
    t3.id AS client_id,
    t3.name AS client_name,
    t1.skip_row_cnt,
    t1.new_row_cnt,
    t1.force_all,
    t1.inactive_date,
    t1.is_confirmed,
    t1.confirm_date
   FROM und.stock_sales_download t1
     JOIN cabinet.sales t2 ON t1.sales_id = t2.id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
  WHERE t1.is_deleted = false;

ALTER TABLE und.vw_stock_sales_download OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_update_soft;

CREATE OR REPLACE VIEW cabinet.vw_update_soft AS 
 SELECT t1.soft_id,
    t1.soft_name,
    t1.service_id,
    t1.items_exists,
    t1.soft_guid,
    t1.group_id,
    t2.group_name
   FROM cabinet.update_soft t1
     JOIN cabinet.update_soft_group t2 ON t1.group_id = t2.group_id;

ALTER TABLE cabinet.vw_update_soft OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_user;

CREATE OR REPLACE VIEW und.vw_asna_user AS 
 SELECT t1.asna_user_id,
    t1.workplace_id,
    t1.is_active,
    t1.asna_store_id,
    t1.asna_pwd,
    t1.asna_legal_id,
    t1.asna_rr_id,
    t1.auth,
    t1.auth_date_beg,
    t1.auth_date_end,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.registration_key AS workplace_name,
    t2.sales_id,
    t3.adress AS sales_name,
    t3.client_id,
    t4.name AS client_name,
        CASE t1.is_active
            WHEN true THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_active_str,
    t11.schedule_id AS sch_full_schedule_id,
    t11.is_active AS sch_full_is_active,
    t11.interval_type_id AS sch_full_interval_type_id,
    t11.start_time_type_id AS sch_full_start_time_type_id,
    t11.end_time_type_id AS sch_full_end_time_type_id,
    t11.is_day1 AS sch_full_is_day1,
    t11.is_day2 AS sch_full_is_day2,
    t11.is_day3 AS sch_full_is_day3,
    t11.is_day4 AS sch_full_is_day4,
    t11.is_day5 AS sch_full_is_day5,
    t11.is_day6 AS sch_full_is_day6,
    t11.is_day7 AS sch_full_is_day7,
    t12.schedule_id AS sch_ch_schedule_id,
    t12.is_active AS sch_ch_is_active,
    t12.interval_type_id AS sch_ch_interval_type_id,
    t12.start_time_type_id AS sch_ch_start_time_type_id,
    t12.end_time_type_id AS sch_ch_end_time_type_id,
    t12.is_day1 AS sch_ch_is_day1,
    t12.is_day2 AS sch_ch_is_day2,
    t12.is_day3 AS sch_ch_is_day3,
    t12.is_day4 AS sch_ch_is_day4,
    t12.is_day5 AS sch_ch_is_day5,
    t12.is_day6 AS sch_ch_is_day6,
    t12.is_day7 AS sch_ch_is_day7,
    t13.time_type_value AS sch_full_start_time_type_value,
    t14.time_type_value AS sch_full_end_time_type_value,
    t15.interval_type_value AS sch_full_interval_type_value,
    t16.time_type_value AS sch_ch_start_time_type_value,
    t17.time_type_value AS sch_ch_end_time_type_value,
    t18.interval_type_value AS sch_ch_interval_type_value,
    t3.tax_system_type_id AS sales_tax_system_type_id,
    t3.region_zone_id AS sales_region_zone_id,
    t20.id AS sales_region_id,
    t4.tax_system_type_id AS client_tax_system_type_id,
    t4.region_zone_id AS client_region_zone_id,
    t22.id AS client_region_id,
    t23.user_id,
    t24.name AS user_name
   FROM und.asna_user t1
     JOIN cabinet.workplace t2 ON t1.workplace_id = t2.id
     JOIN cabinet.sales t3 ON t2.sales_id = t3.id
     JOIN cabinet.client t4 ON t3.client_id = t4.id
     LEFT JOIN und.asna_schedule t11 ON t1.asna_user_id = t11.asna_user_id AND t11.schedule_type_id = 1 AND t11.is_deleted = false
     LEFT JOIN und.asna_schedule t12 ON t1.asna_user_id = t12.asna_user_id AND t12.schedule_type_id = 2 AND t11.is_deleted = false
     LEFT JOIN und.asna_schedule_time_type t13 ON t11.start_time_type_id = t13.time_type_id
     LEFT JOIN und.asna_schedule_time_type t14 ON t11.end_time_type_id = t14.time_type_id
     LEFT JOIN und.asna_schedule_interval_type t15 ON t11.interval_type_id = t15.interval_type_id
     LEFT JOIN und.asna_schedule_time_type t16 ON t12.start_time_type_id = t16.time_type_id
     LEFT JOIN und.asna_schedule_time_type t17 ON t12.end_time_type_id = t17.time_type_id
     LEFT JOIN und.asna_schedule_interval_type t18 ON t12.interval_type_id = t18.interval_type_id
     LEFT JOIN cabinet.city t19 ON t3.city_id = t19.id
     LEFT JOIN cabinet.region t20 ON t19.region_id = t20.id
     LEFT JOIN cabinet.city t21 ON t4.city_id = t21.id
     LEFT JOIN cabinet.region t22 ON t21.region_id = t22.id
     LEFT JOIN cabinet.client_user t23 ON t3.id = t23.sales_id AND t23.is_main_for_sales = true
     LEFT JOIN cabinet.my_aspnet_users t24 ON t23.user_id = t24.id;

ALTER TABLE und.vw_asna_user OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_client;

CREATE OR REPLACE VIEW und.vw_asna_client AS 
 SELECT DISTINCT t1.client_id,
    t1.client_name
   FROM und.vw_asna_user t1;

ALTER TABLE und.vw_asna_client OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_wa_task;

CREATE OR REPLACE VIEW und.vw_wa_task AS 
 SELECT t1.task_id,
    t1.request_type_id,
    t1.task_state_id,
    t1.client_id,
    t1.sales_id,
    t1.workplace_id,
    t1.ord,
    t1.ord_client,
    t1.state_date,
    t1.state_user,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.is_auto_created,
    t1.mess,
    t2.request_type_name,
    t2.request_type_group1_id,
    t2.request_type_group2_id,
    t14.request_type_group1_name,
    t15.request_type_group2_name,
    t3.task_state_name,
    t11.name AS client_name,
    t12.adress AS sales_name,
    t13.registration_key AS workplace_name,
        CASE t1.is_auto_created
            WHEN true THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_auto_created_str,
    t2.is_inner,
    t2.is_outer,
    t1.handling_server_cnt,
    t2.handling_server_max_cnt
   FROM und.wa_task t1
     JOIN und.wa_request_type t2 ON t1.request_type_id = t2.request_type_id
     JOIN und.wa_task_state t3 ON t1.task_state_id = t3.task_state_id
     LEFT JOIN cabinet.client t11 ON t1.client_id = t11.id
     LEFT JOIN cabinet.sales t12 ON t1.sales_id = t12.id
     LEFT JOIN cabinet.workplace t13 ON t1.client_id = t13.id
     LEFT JOIN und.wa_request_type_group1 t14 ON t2.request_type_group1_id = t14.request_type_group1_id
     LEFT JOIN und.wa_request_type_group2 t15 ON t2.request_type_group2_id = t15.request_type_group2_id;

ALTER TABLE und.vw_wa_task OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_user_log;

CREATE OR REPLACE VIEW und.vw_asna_user_log AS 
 SELECT t1.log_id,
    t1.asna_user_id,
    t1.mess,
    t1.request_id,
    t1.chain_id,
    t1.in_batch_id,
    t1.out_batch_id,
    t1.info_id,
    t1.crt_date,
    t1.crt_user,
    t2.workplace_id,
    t2.is_active,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
        CASE t2.is_active
            WHEN true THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_active_str
   FROM und.asna_user_log t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id;

ALTER TABLE und.vw_asna_user_log OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_wa_task_lc;

CREATE OR REPLACE VIEW und.vw_wa_task_lc AS 
 SELECT t1.task_lc_id,
    t1.task_id,
    t1.task_state_id,
    t1.state_date,
    t1.state_user,
    t1.mess,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.request_type_id,
    t2.client_id,
    t2.sales_id,
    t2.workplace_id,
    t2.ord,
    t2.ord_client,
    t2.is_auto_created,
    t3.request_type_name,
    t3.request_type_group1_id,
    t3.request_type_group2_id,
    t11.request_type_group1_name,
    t12.request_type_group2_name,
    t4.task_state_name,
        CASE t2.is_auto_created
            WHEN true THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_auto_created_str,
    t3.is_inner,
    t3.is_outer
   FROM und.wa_task_lc t1
     JOIN und.wa_task t2 ON t1.task_id = t2.task_id
     JOIN und.wa_request_type t3 ON t2.request_type_id = t3.request_type_id
     JOIN und.wa_task_state t4 ON t1.task_state_id = t4.task_state_id
     LEFT JOIN und.wa_request_type_group1 t11 ON t3.request_type_group1_id = t11.request_type_group1_id
     LEFT JOIN und.wa_request_type_group2 t12 ON t3.request_type_group2_id = t12.request_type_group2_id;

ALTER TABLE und.vw_wa_task_lc OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_stock_row;

CREATE OR REPLACE VIEW und.vw_asna_stock_row AS 
 SELECT t1.row_id,
    t1.asna_user_id,
    t1.in_batch_id,
    t1.out_batch_id,
    t1.artikul,
    t1.row_stock_id,
    t1.esn_id,
    t1.prep_id,
    t1.prep_name,
    t1.firm_id,
    t1.firm_name,
    t1.barcode,
    t1.asna_prt_id,
    t1.asna_sku,
    t1.asna_qnt,
    t1.asna_doc,
    t1.asna_sup_inn,
    t1.asna_sup_id,
    t1.asna_sup_date,
    t1.asna_nds,
    t1.asna_gnvls,
    t1.asna_dp,
    t1.asna_man,
    t1.asna_series,
    t1.asna_exp,
    t1.asna_prc_man,
    t1.asna_prc_opt,
    t1.asna_prc_opt_nds,
    t1.asna_prc_ret,
    t1.asna_prc_reestr,
    t1.asna_prc_gnvls_max,
    t1.asna_sum_opt,
    t1.asna_sum_opt_nds,
    t1.asna_mr_gnvls,
    t1.asna_tag,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.asna_store_id,
    t2.asna_legal_id,
    t2.asna_rr_id,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    t7.chain_id,
    t10.asna_nnt,
    t10.asna_name,
        CASE
            WHEN t10.asna_nnt IS NOT NULL THEN true
            ELSE false
        END AS is_linked,
    t10.is_unique
   FROM und.asna_stock_row t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id
     LEFT JOIN und.asna_stock_batch_in t6 ON t1.in_batch_id = t6.batch_id
     LEFT JOIN und.asna_stock_chain t7 ON t6.chain_id = t7.chain_id
     LEFT JOIN und.asna_stock_batch_out t8 ON t1.out_batch_id = t8.batch_id
     LEFT JOIN und.asna_link t9 ON t1.asna_user_id = t9.asna_user_id
     LEFT JOIN und.asna_link_row t10 ON t9.link_id = t10.link_id AND t1.asna_sku::text = t10.asna_sku::text;

ALTER TABLE und.vw_asna_stock_row OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_stock_row_linked;

CREATE OR REPLACE VIEW und.vw_asna_stock_row_linked AS 
 SELECT t1.row_id,
    t1.asna_prt_id,
    t1.asna_sku,
    t1.asna_qnt,
    t1.asna_doc,
    t1.asna_sup_inn,
    t1.asna_sup_id,
    t1.asna_sup_date,
    t1.asna_nds,
    t1.asna_gnvls,
    t1.asna_dp,
    t1.asna_man,
    t1.asna_series,
    t1.asna_exp,
    t1.asna_prc_man,
    t1.asna_prc_opt,
    t1.asna_prc_opt_nds,
    t1.asna_prc_ret,
    t1.asna_prc_reestr,
    t1.asna_prc_gnvls_max,
    t1.asna_sum_opt,
    t1.asna_sum_opt_nds,
    t1.asna_mr_gnvls,
    t1.asna_tag,
    t3.asna_nnt,
    t12.chain_id,
    t12.stock_date AS chain_stock_date,
    t12.is_full AS chain_is_full,
    t3.is_unique
   FROM und.asna_stock_row t1
     JOIN und.asna_link t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN und.asna_link_row t3 ON t2.link_id = t3.link_id AND t1.asna_sku::text = t3.asna_sku::text
     LEFT JOIN und.asna_stock_batch_in t11 ON t1.in_batch_id = t11.batch_id
     LEFT JOIN und.asna_stock_chain t12 ON t11.chain_id = t12.chain_id;

ALTER TABLE und.vw_asna_stock_row_linked OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_stock_chain;

CREATE OR REPLACE VIEW und.vw_asna_stock_chain AS 
 SELECT t1.chain_id,
    t1.asna_user_id,
    t1.chain_num,
    t1.stock_date,
    t1.is_full,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.is_active,
    t1.inactive_date,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    (((('№ '::text || t1.chain_id::text) || ' ['::text) ||
        CASE t1.is_full
            WHEN true THEN 'полные'::text
            ELSE 'изменения'::text
        END) || '] '::text) || to_char(t1.stock_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS chain_name
   FROM und.asna_stock_chain t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id;

ALTER TABLE und.vw_asna_stock_chain OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_stock_row_actual;

CREATE OR REPLACE VIEW und.vw_asna_stock_row_actual AS 
 SELECT t1.row_id,
    t1.asna_user_id,
    t1.firm_name,
    t1.asna_prt_id,
    t1.asna_sku,
    t1.asna_qnt,
    t1.asna_doc,
    t1.asna_sup_inn,
    t1.asna_sup_id,
    t1.asna_sup_date,
    COALESCE(t1.asna_nds, 0) AS asna_nds,
    t1.asna_gnvls,
    t1.asna_dp,
    t1.asna_man,
    t1.asna_series,
    t1.asna_exp,
    t1.asna_prc_man,
    t1.asna_prc_opt,
    t1.asna_prc_opt_nds,
    t1.asna_prc_ret,
    t1.asna_prc_reestr,
    t1.asna_prc_gnvls_max,
    COALESCE(t1.asna_sum_opt, 0::numeric) AS asna_sum_opt,
    COALESCE(t1.asna_sum_opt_nds, 0::numeric) AS asna_sum_opt_nds,
    t1.asna_mr_gnvls,
    t1.asna_tag,
    t1.asna_stock_date,
    t1.asna_load_date,
    t1.asna_rv,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.asna_store_id,
    t2.asna_legal_id,
    t2.asna_rr_id,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    t10.asna_nnt,
    t10.asna_name,
        CASE
            WHEN t10.asna_nnt IS NOT NULL THEN true
            ELSE false
        END AS is_linked,
    t10.is_unique
   FROM und.asna_stock_row_actual t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id
     LEFT JOIN und.asna_link t9 ON t1.asna_user_id = t9.asna_user_id
     LEFT JOIN und.asna_link_row t10 ON t9.link_id = t10.link_id AND t1.asna_sku::text = t10.asna_sku::text;

ALTER TABLE und.vw_asna_stock_row_actual OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_order;

CREATE OR REPLACE VIEW und.vw_asna_order AS 
 SELECT t1.order_id,
    t1.asna_user_id,
    t1.issuer_asna_user_id,
    t1.state_type_id,
    t1.asna_order_id,
    t1.asna_src,
    t1.asna_num,
    t1.asna_date,
    t1.asna_name,
    t1.asna_m_phone,
    t1.asna_pay_type,
    t1.asna_pay_type_id,
    t1.asna_d_card,
    t1.asna_ae,
    t1.asna_union_id,
    t1.asna_ts,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    t6.workplace_id AS issuer_workplace_id,
    t7.registration_key AS issuer_workplace_name,
    t7.sales_id AS issuer_sales_id,
    t8.adress AS issuer_sales_name,
    t8.client_id AS issuer_client_id,
    t9.name AS issuer_client_name,
    t11.state_type_name
   FROM und.asna_order t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id
     JOIN und.asna_user t6 ON t1.issuer_asna_user_id = t6.asna_user_id
     JOIN cabinet.workplace t7 ON t6.workplace_id = t7.id
     JOIN cabinet.sales t8 ON t7.sales_id = t8.id
     JOIN cabinet.client t9 ON t8.client_id = t9.id
     LEFT JOIN und.asna_order_state_type t11 ON t1.state_type_id = t11.state_type_id;

ALTER TABLE und.vw_asna_order OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_order_row;

CREATE OR REPLACE VIEW und.vw_asna_order_row AS 
 SELECT t1.row_id,
    t1.order_id,
    t1.state_type_id,
    t1.asna_row_id,
    t1.asna_order_id,
    t1.asna_row_type,
    t1.asna_prt_id,
    t1.asna_nnt,
    t1.asna_qnt,
    t1.asna_prc,
    t1.asna_prc_dsc,
    t1.asna_dsc_union,
    t1.asna_dtn,
    t1.asna_prc_loyal,
    t1.asna_prc_opt_nds,
    t1.asna_supp_inn,
    t1.asna_dlv_date,
    t1.asna_qnt_unrsv,
    t1.asna_prc_fix,
    t1.asna_ts,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t11.state_type_name,
    t1.asna_sku,
    t13.asna_name
   FROM und.asna_order_row t1
     JOIN und.asna_order t2 ON t1.order_id = t2.order_id
     LEFT JOIN und.asna_order_state_type t11 ON t1.state_type_id = t11.state_type_id
     LEFT JOIN und.asna_link t12 ON t2.issuer_asna_user_id = t12.asna_user_id
     LEFT JOIN und.asna_link_row t13 ON t1.asna_nnt = t13.asna_nnt AND t12.link_id = t13.link_id;

ALTER TABLE und.vw_asna_order_row OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_order_row_state;

CREATE OR REPLACE VIEW und.vw_asna_order_row_state AS 
 SELECT t1.row_state_id,
    t1.row_id,
    t1.state_type_id,
    t1.asna_status_id,
    t1.asna_order_id,
    t1.asna_row_id,
    t1.asna_date,
    t1.asna_rc_date,
    t1.asna_cmnt,
    t1.asna_ts,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t11.state_type_name
   FROM und.asna_order_row_state t1
     LEFT JOIN und.asna_order_state_type t11 ON t1.state_type_id = t11.state_type_id;

ALTER TABLE und.vw_asna_order_row_state OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_order_state;

CREATE OR REPLACE VIEW und.vw_asna_order_state AS 
 SELECT t1.order_state_id,
    t1.order_id,
    t1.state_type_id,
    t1.asna_status_id,
    t1.asna_order_id,
    t1.asna_date,
    t1.asna_rc_date,
    t1.asna_cmnt,
    t1.asna_ts,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.state_type_name
   FROM und.asna_order_state t1
     JOIN und.asna_order_state_type t2 ON t1.state_type_id = t2.state_type_id;

ALTER TABLE und.vw_asna_order_state OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_price_chain;

CREATE OR REPLACE VIEW und.vw_asna_price_chain AS 
 SELECT t1.chain_id,
    t1.asna_user_id,
    t1.chain_num,
    t1.price_date,
    t1.is_full,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.is_active,
    t1.inactive_date,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    (((('№ '::text || t1.chain_id::text) || ' ['::text) ||
        CASE t1.is_full
            WHEN true THEN 'полные'::text
            ELSE 'изменения'::text
        END) || '] '::text) || to_char(t1.price_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS chain_name
   FROM und.asna_price_chain t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id;

ALTER TABLE und.vw_asna_price_chain OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_price_row;

CREATE OR REPLACE VIEW und.vw_asna_price_row AS 
 SELECT t1.row_id,
    t1.asna_user_id,
    t1.in_batch_id,
    t1.out_batch_id,
    t1.artikul,
    t1.esn_id,
    t1.prep_id,
    t1.prep_name,
    t1.firm_id,
    t1.firm_name,
    t1.barcode,
    t1.asna_sku,
    t1.asna_sup_inn,
    t1.asna_prc_opt_nds,
    t1.asna_prc_gnvls_max,
    t1.asna_status,
    t1.asna_exp,
    t1.asna_sup_date,
    t1.asna_prcl_date,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.asna_store_id,
    t2.asna_legal_id,
    t2.asna_rr_id,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    t7.chain_id,
    t10.asna_nnt,
    t10.asna_name,
        CASE
            WHEN t10.asna_nnt IS NOT NULL THEN true
            ELSE false
        END AS is_linked,
    t10.is_unique
   FROM und.asna_price_row t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id
     LEFT JOIN und.asna_price_batch_in t6 ON t1.in_batch_id = t6.batch_id
     LEFT JOIN und.asna_price_chain t7 ON t6.chain_id = t7.chain_id
     LEFT JOIN und.asna_price_batch_out t8 ON t1.out_batch_id = t8.batch_id
     LEFT JOIN und.asna_link t9 ON t1.asna_user_id = t9.asna_user_id
     LEFT JOIN und.asna_link_row t10 ON t9.link_id = t10.link_id AND t1.asna_sku::text = t10.asna_sku::text;

ALTER TABLE und.vw_asna_price_row OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_price_row_actual;

CREATE OR REPLACE VIEW und.vw_asna_price_row_actual AS 
 SELECT t1.row_id,
    t1.asna_user_id,
    t1.firm_name,
    t1.asna_sku,
    t1.asna_sup_inn,
    t1.asna_prc_opt_nds,
    t1.asna_prc_gnvls_max,
    t1.asna_status,
    t1.asna_exp,
    t1.asna_sup_date,
    t1.asna_prcl_date,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.asna_store_id,
    t2.asna_legal_id,
    t2.asna_rr_id,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    t10.asna_nnt,
    t10.asna_name,
        CASE
            WHEN t10.asna_nnt IS NOT NULL THEN true
            ELSE false
        END AS is_linked,
    t10.is_unique
   FROM und.asna_price_row_actual t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id
     LEFT JOIN und.asna_link t9 ON t1.asna_user_id = t9.asna_user_id
     LEFT JOIN und.asna_link_row t10 ON t9.link_id = t10.link_id AND t1.asna_sku::text = t10.asna_sku::text;

ALTER TABLE und.vw_asna_price_row_actual OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_asna_price_row_linked;

CREATE OR REPLACE VIEW und.vw_asna_price_row_linked AS 
 SELECT t1.row_id,
    t1.asna_sku,
    t1.asna_sup_inn,
    t1.asna_prc_opt_nds,
    t1.asna_prc_gnvls_max,
    t1.asna_status,
    t1.asna_exp,
    t1.asna_sup_date,
    t1.asna_prcl_date,
    t3.asna_nnt,
    t12.chain_id,
    t12.price_date AS chain_price_date,
    t12.is_full AS chain_is_full,
    t3.is_unique
   FROM und.asna_price_row t1
     JOIN und.asna_link t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN und.asna_link_row t3 ON t2.link_id = t3.link_id AND t1.asna_sku::text = t3.asna_sku::text
     LEFT JOIN und.asna_price_batch_in t11 ON t1.in_batch_id = t11.batch_id
     LEFT JOIN und.asna_price_chain t12 ON t11.chain_id = t12.chain_id;

ALTER TABLE und.vw_asna_price_row_linked OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_region;

CREATE OR REPLACE VIEW cabinet.vw_region AS 
 WITH region_price_limit_cte AS (
         SELECT row_number() OVER (PARTITION BY region_price_limit.region_id ORDER BY region_price_limit.limit_type_id) AS num,
            region_price_limit.region_id
           FROM cabinet.region_price_limit
        )
 SELECT t1.id AS region_id,
    t1.name AS region_name,
    t1.is_deleted,
        CASE
            WHEN t2.region_id IS NOT NULL THEN true
            ELSE false
        END AS is_price_limit_exists
   FROM cabinet.region t1
     LEFT JOIN region_price_limit_cte t2 ON t1.id = t2.region_id AND t2.num = 1;

ALTER TABLE cabinet.vw_region OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_region_zone;

CREATE OR REPLACE VIEW cabinet.vw_region_zone AS 
 WITH region_zone_price_limit_cte AS (
         SELECT row_number() OVER (PARTITION BY region_zone_price_limit.zone_id ORDER BY region_zone_price_limit.limit_type_id) AS num,
            region_zone_price_limit.zone_id
           FROM cabinet.region_zone_price_limit
        )
 SELECT t1.zone_id,
    t1.zone_name,
    t1.region_id,
    t2.name AS region_name,
    t1.is_deleted,
        CASE
            WHEN t11.zone_id IS NOT NULL THEN true
            ELSE false
        END AS is_price_limit_exists
   FROM cabinet.region_zone t1
     JOIN cabinet.region t2 ON t1.region_id = t2.id
     LEFT JOIN region_zone_price_limit_cte t11 ON t1.zone_id = t11.zone_id AND t11.num = 1;

ALTER TABLE cabinet.vw_region_zone OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_region_price_limit;

CREATE OR REPLACE VIEW cabinet.vw_region_price_limit AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS item_id,
    t1.id AS region_id,
    t11.limit_type_id,
    t11.limit_type_name,
    t12.percent_value,
        CASE
            WHEN t12.region_id IS NOT NULL THEN true
            ELSE false
        END AS is_price_limit_exists
   FROM cabinet.region t1
     LEFT JOIN cabinet.price_limit_type t11 ON 1 = 1
     LEFT JOIN cabinet.region_price_limit t12 ON t1.id = t12.region_id AND t11.limit_type_id = t12.limit_type_id;

ALTER TABLE cabinet.vw_region_price_limit OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_region_zone_price_limit;

CREATE OR REPLACE VIEW cabinet.vw_region_zone_price_limit AS 
 SELECT row_number() OVER (ORDER BY t1.zone_id) AS item_id,
    t1.zone_id,
    t11.limit_type_id,
    t11.limit_type_name,
    t12.percent_value,
        CASE
            WHEN t12.zone_id IS NOT NULL THEN true
            ELSE false
        END AS is_price_limit_exists
   FROM cabinet.region_zone t1
     LEFT JOIN cabinet.price_limit_type t11 ON 1 = 1
     LEFT JOIN cabinet.region_zone_price_limit t12 ON t1.zone_id = t12.zone_id AND t11.limit_type_id = t12.limit_type_id;

ALTER TABLE cabinet.vw_region_zone_price_limit OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW discount.vw_programm_step_param;

CREATE OR REPLACE VIEW discount.vw_programm_step_param AS 
 SELECT t1.param_id,
    t1.programm_id,
    t1.step_id,
    t1.card_param_type,
    t1.trans_param_type,
    t1.programm_param_id,
    t1.prev_step_param_id,
    t1.param_num,
    t1.param_name,
    t1.is_programm_output,
    t1.param_type_id,
    t2.step_num,
    t3.business_id,
    t11.param_type_name,
    t12.card_param_type_name,
    t13.trans_param_type_name,
    t14.param_name AS programm_param_name,
    t15.param_name AS prev_step_param_name,
        CASE t1.is_programm_output
            WHEN 1 THEN true
            ELSE false
        END AS is_programm_output_chb,
        CASE t1.param_type_id
            WHEN 1 THEN t12.card_param_type_name
            WHEN 2 THEN t13.trans_param_type_name
            WHEN 3 THEN t14.param_name
            WHEN 4 THEN t15.param_name
            ELSE ''::character varying
        END AS curr_param_name,
    t1.param_id::character varying AS param_id_str,
    t1.programm_id::character varying AS programm_id_str,
    t1.step_id::character varying AS step_id_str,
    t1.programm_param_id::character varying AS programm_param_id_str,
    t1.prev_step_param_id::character varying AS prev_step_param_id_str,
    t3.business_id::character varying AS business_id_str
   FROM discount.programm_step_param t1
     JOIN discount.programm_step t2 ON t1.step_id = t2.step_id
     JOIN discount.programm t3 ON t2.programm_id = t3.programm_id
     LEFT JOIN discount.programm_step_param_type t11 ON t1.param_type_id = t11.param_type_id
     LEFT JOIN discount.programm_card_param_type t12 ON t1.card_param_type = t12.card_param_type
     LEFT JOIN discount.programm_trans_param_type t13 ON t1.trans_param_type = t13.trans_param_type
     LEFT JOIN discount.programm_param t14 ON t1.programm_param_id = t14.param_id
     LEFT JOIN discount.programm_step_param t15 ON t1.prev_step_param_id = t15.param_id;

ALTER TABLE discount.vw_programm_step_param OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW discount.vw_programm_step_condition;

CREATE OR REPLACE VIEW discount.vw_programm_step_condition AS 
 SELECT t1.condition_id,
    t1.programm_id,
    t1.step_id,
    t1.condition_num,
    t1.op_type,
    t1.op1_param_id,
    t1.op2_param_id,
    t1.condition_name,
    t2.step_num,
    t3.business_id,
    t11.condition_type_id,
    t11.condition_type_name,
    t12.param_name AS op1_param_name,
    t13.param_name AS op2_param_name,
    t1.condition_id::character varying AS condition_id_str,
    t1.programm_id::character varying AS programm_id_str,
    t1.step_id::character varying AS step_id_str,
    t1.op1_param_id::character varying AS op1_param_id_str,
    t1.op2_param_id::character varying AS op2_param_id_str,
    t3.business_id::character varying AS business_id_str
   FROM discount.programm_step_condition t1
     JOIN discount.programm_step t2 ON t1.step_id = t2.step_id
     JOIN discount.programm t3 ON t2.programm_id = t3.programm_id
     LEFT JOIN discount.programm_step_condition_type t11 ON t1.op_type = t11.condition_type_id
     LEFT JOIN discount.programm_step_param t12 ON t1.op1_param_id = t12.param_id AND t1.step_id = t12.step_id
     LEFT JOIN discount.programm_step_param t13 ON t1.op2_param_id = t13.param_id AND t1.step_id = t13.step_id;

ALTER TABLE discount.vw_programm_step_condition OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW discount.vw_programm_step_logic;

CREATE OR REPLACE VIEW discount.vw_programm_step_logic AS 
 SELECT t1.logic_id,
    t1.programm_id,
    t1.step_id,
    t1.logic_num,
    t1.op_type,
    t1.op1_param_id,
    t1.op2_param_id,
    t1.op3_param_id,
    t1.op4_param_id,
    t1.condition_id,
    t2.step_num,
    t3.business_id,
    t4.logic_type_id,
    t4.logic_type_name,
    t12.param_name AS op1_param_name,
    t13.param_name AS op2_param_name,
    t14.param_name AS op3_param_name,
    t15.param_name AS op4_param_name,
    t16.condition_name,
    t1.logic_id::character varying AS logic_id_str,
    t1.programm_id::character varying AS programm_id_str,
    t1.step_id::character varying AS step_id_str,
    t1.condition_id::character varying AS condition_id_str,
    t1.op1_param_id::character varying AS op1_param_id_str,
    t1.op2_param_id::character varying AS op2_param_id_str,
    t1.op3_param_id::character varying AS op3_param_id_str,
    t1.op4_param_id::character varying AS op4_param_id_str,
    t3.business_id::character varying AS business_id_str
   FROM discount.programm_step_logic t1
     JOIN discount.programm_step t2 ON t1.step_id = t2.step_id
     JOIN discount.programm t3 ON t2.programm_id = t3.programm_id
     JOIN discount.programm_step_logic_type t4 ON t1.op_type = t4.logic_type_id
     LEFT JOIN discount.programm_step_param t12 ON t1.op1_param_id = t12.param_id AND t1.step_id = t12.step_id
     LEFT JOIN discount.programm_step_param t13 ON t1.op2_param_id = t13.param_id AND t1.step_id = t13.step_id
     LEFT JOIN discount.programm_step_param t14 ON t1.op3_param_id = t14.param_id AND t1.step_id = t14.step_id
     LEFT JOIN discount.programm_step_param t15 ON t1.op4_param_id = t15.param_id AND t1.step_id = t15.step_id
     LEFT JOIN discount.programm_step_condition t16 ON t1.condition_id = t16.condition_id;

ALTER TABLE discount.vw_programm_step_logic OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_prj_note;

CREATE OR REPLACE VIEW cabinet.vw_prj_note AS 
 SELECT t1.note_id,
    t1.note_name,
    t1.note_text,
    'N-'::text || t1.note_id::text AS note_num
   FROM cabinet.prj_note t1;

ALTER TABLE cabinet.vw_prj_note OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_prj_user_group_item;

CREATE OR REPLACE VIEW cabinet.vw_prj_user_group_item AS 
 SELECT t1.item_id,
    t1.group_id,
    t1.item_type_id,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_id
            WHEN 4 THEN t14.claim_id
            ELSE NULL::integer
        END AS claim_id,
    t1.event_id,
    t1.note_id,
    t1.ord,
    t1.task_id,
    t2.user_id,
    t2.group_name,
    t2.fixed_kind,
    t3.item_type_name,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_num
            WHEN 3 THEN t13.note_num::character varying
            WHEN 4 THEN ((t14.claim_num::text || '/'::text) || t14.task_num::character varying::text)::character varying
            ELSE NULL::character varying
        END AS item_num,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_name
            WHEN 3 THEN t13.note_name
            WHEN 4 THEN t14.task_text
            ELSE NULL::character varying
        END AS item_name,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_text
            WHEN 3 THEN t13.note_text
            WHEN 4 THEN t14.claim_text
            ELSE NULL::character varying
        END AS item_text,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_state_type_id
            WHEN 4 THEN t14.task_state_type_id
            ELSE NULL::integer
        END AS item_state_type_id,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_state_type_name
            WHEN 4 THEN t14.task_state_type_name
            ELSE NULL::character varying
        END AS item_state_type_name,
    NULL::timestamp without time zone AS item_date_beg,
    NULL::timestamp without time zone AS item_date_end,
    t1.ord_prev,
    t1.group_id_prev
   FROM cabinet.prj_user_group_item t1
     JOIN cabinet.prj_user_group t2 ON t1.group_id = t2.group_id
     JOIN cabinet.prj_user_group_item_type t3 ON t1.item_type_id = t3.item_type_id
     LEFT JOIN cabinet.vw_prj_claim t11 ON t1.claim_id = t11.claim_id
     LEFT JOIN cabinet.vw_prj_note t13 ON t1.note_id = t13.note_id
     LEFT JOIN cabinet.vw_prj_task t14 ON t1.task_id = t14.task_id;

ALTER TABLE cabinet.vw_prj_user_group_item OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_client_min_version;

CREATE OR REPLACE VIEW cabinet.vw_client_min_version AS 
 SELECT row_number() OVER (ORDER BY ( SELECT 0)) AS adr,
    t1.id,
    t1.name,
    t1.inn,
    t1.kpp,
    t2.id AS sales_id,
    t2.adress,
    0 AS workplace_id,
    ''::text AS description,
    ''::character varying(255) AS registration_key,
    t4.service_name,
    t4.version_name
   FROM cabinet.client t1
     LEFT JOIN cabinet.sales t2 ON t1.id = t2.client_id AND t2.is_deleted = 0
     LEFT JOIN ( SELECT min(rpad(replace(t1_1.version_name::text, '.'::text, ''::text), 6, '0'::text)::integer) AS version_name_int_min,
            min(t1_1.version_name::text) AS version_name,
            min(t1_1.service_name::text) AS service_name,
            t1_1.client_id,
            t1_1.sales_id,
            t1_1.service_id
           FROM cabinet.vw_client_service_registered t1_1
          WHERE t1_1.version_id <> 86
          GROUP BY t1_1.client_id, t1_1.sales_id, t1_1.service_id) t4 ON t2.id = t4.sales_id
  WHERE t1.is_deleted = 0 AND t1.id <> 1000 AND t1.id <> 4990 AND t1.id <> 936;

ALTER TABLE cabinet.vw_client_min_version OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_client_report1;

CREATE OR REPLACE VIEW cabinet.vw_client_report1 AS 
 SELECT DISTINCT t1.id,
    t1.client_name,
    t1.sales_count,
    t1.region_name,
    t1.city_name,
    t2.version_name::character varying AS version_min,
    t3.version_name::character varying AS version_max,
    t4.date_reg_min::date AS date_reg_min,
    t4.date_reg_max::date AS date_reg_max,
    ''::character varying AS sales_name,
    1 AS kind
   FROM cabinet.allclients t1
     LEFT JOIN ( SELECT vw_client_min_version.id,
            min(vw_client_min_version.version_name) AS version_name
           FROM cabinet.vw_client_min_version
          GROUP BY vw_client_min_version.id) t2 ON t1.id = t2.id
     LEFT JOIN ( SELECT vw_client_max_version.id,
            max(vw_client_max_version.version_name) AS version_name
           FROM cabinet.vw_client_max_version
          GROUP BY vw_client_max_version.id) t3 ON t1.id = t3.id
     LEFT JOIN ( SELECT t2_1.client_id,
            min(t2_1.updated_on) AS date_reg_min,
            max(t2_1.updated_on) AS date_reg_max
           FROM cabinet.order_item t1_1
             JOIN cabinet."order" t2_1 ON t1_1.order_id = t2_1.id AND t2_1.active_status <> 1 AND t2_1.is_deleted <> 1
          WHERE t1_1.is_deleted <> 1
          GROUP BY t2_1.client_id) t4 ON t1.id = t4.client_id
  WHERE t2.version_name IS NOT NULL OR t3.version_name IS NOT NULL OR t4.date_reg_min IS NOT NULL OR t4.date_reg_max IS NOT NULL
UNION ALL
 SELECT DISTINCT t1.sales_id AS id,
    t1.client_name,
    1 AS sales_count,
    t1.region_name,
    t1.city_name,
    t2.version_name::character varying AS version_min,
    t3.version_name::character varying AS version_max,
    t4.date_reg_min::date AS date_reg_min,
    t4.date_reg_max::date AS date_reg_max,
    t1.adress::character varying AS sales_name,
    2 AS kind
   FROM cabinet.vw_sales t1
     LEFT JOIN ( SELECT vw_client_min_version.sales_id,
            min(vw_client_min_version.version_name) AS version_name
           FROM cabinet.vw_client_min_version
          GROUP BY vw_client_min_version.sales_id) t2 ON t1.sales_id = t2.sales_id
     LEFT JOIN ( SELECT vw_client_max_version.sales_id,
            max(vw_client_max_version.version_name) AS version_name
           FROM cabinet.vw_client_max_version
          GROUP BY vw_client_max_version.sales_id) t3 ON t1.sales_id = t3.sales_id
     LEFT JOIN ( SELECT t2_1.client_id,
            min(t2_1.updated_on) AS date_reg_min,
            max(t2_1.updated_on) AS date_reg_max
           FROM cabinet.order_item t1_1
             JOIN cabinet."order" t2_1 ON t1_1.order_id = t2_1.id AND t2_1.active_status <> 1 AND t2_1.is_deleted <> 1
          WHERE t1_1.is_deleted <> 1
          GROUP BY t2_1.client_id) t4 ON t1.client_id = t4.client_id
  WHERE t2.version_name IS NOT NULL OR t3.version_name IS NOT NULL OR t4.date_reg_min IS NOT NULL OR t4.date_reg_max IS NOT NULL;

ALTER TABLE cabinet.vw_client_report1 OWNER TO pavlov;
------------------------------------------------------
-- View: esn.vw_medical

-- DROP VIEW esn.vw_medical;

CREATE OR REPLACE VIEW esn.vw_medical AS 
 SELECT t1.medical_id,
    t1.document_id,
    t1.publish_date,
    t1.medical_name,
    t1.reg_num,
    t1.reg_date,
    t1.producer,
    t1.add_document_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t11.document_name,
    t11.document_num,
    t11.link AS document_link,
    t11.document_type_id,
    t11.accept_date AS document_accept_date,
    t11.create_date AS document_create_date,
    t12.type_name AS document_type_name,
    t13.document_name AS add_document_name,
    t13.document_num AS add_document_num,
    t13.link AS add_document_link,
    t13.document_type_id AS add_document_type_id,
    t13.accept_date AS add_document_accept_date,
    t13.create_date AS add_document_create_date,
    t14.type_name AS add_document_type_name
   FROM esn.medical t1
     LEFT JOIN esn.document t11 ON t1.document_id = t11.document_id
     LEFT JOIN esn.document_type t12 ON t11.document_type_id = t12.type_id
     LEFT JOIN esn.document t13 ON t1.add_document_id = t13.document_id
     LEFT JOIN esn.document_type t14 ON t13.document_type_id = t14.type_id;

ALTER TABLE esn.vw_medical OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_prj_task_user_state;

CREATE OR REPLACE VIEW cabinet.vw_prj_task_user_state AS 
 SELECT t1.item_id,
    t1.user_id,
    t1.task_id,
    t1.state_type_id,
    t1.from_user_id,
    t1.is_current,
    t1.mess,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.user_name,
    t3.task_num,
    t3.claim_id,
    t4.state_type_name,
    t11.user_name AS from_user_name
   FROM cabinet.prj_task_user_state t1
     JOIN cabinet.cab_user t2 ON t1.user_id = t2.user_id
     JOIN cabinet.prj_task t3 ON t1.task_id = t3.task_id
     JOIN cabinet.prj_task_user_state_type t4 ON t1.state_type_id = t4.state_type_id
     LEFT JOIN cabinet.cab_user t11 ON t1.from_user_id = t11.user_id;

ALTER TABLE cabinet.vw_prj_task_user_state OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_service_esn2;

CREATE OR REPLACE VIEW cabinet.vw_service_esn2 AS 
 SELECT t1.item_id,
    t1.workplace_id,
    t1.service_id,
    t1.comp_name,
    t2.registration_key,
    t3.id AS sales_id,
    t3.client_id,
    t3.adress AS sales_name,
    t4.name AS client_name,
    t2.workplace_type
   FROM cabinet.service_esn2 t1
     JOIN cabinet.workplace t2 ON t1.workplace_id = t2.id AND t2.is_deleted <> 1
     JOIN cabinet.sales t3 ON t2.sales_id = t3.id AND t3.is_deleted <> 1
     JOIN cabinet.client t4 ON t3.client_id = t4.id AND t4.is_deleted <> 1;

ALTER TABLE cabinet.vw_service_esn2 OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_prj_project_version;

CREATE OR REPLACE VIEW cabinet.vw_prj_project_version AS 
 SELECT t1.version_id,
    t1.project_id,
    t1.version_type_id,
    t1.version_name,
    t1.version_value,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.project_name,
    t3.version_type_name
   FROM cabinet.prj_project_version t1
     JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
     JOIN cabinet.prj_version_type t3 ON t1.version_type_id = t3.version_type_id;

ALTER TABLE cabinet.vw_prj_project_version OWNER TO pavlov;
------------------------------------------------------

-- DROP VIEW cabinet.vw_version_main;

CREATE OR REPLACE VIEW cabinet.vw_version_main AS 
 SELECT row_number() OVER (ORDER BY x1.version_main_name) AS version_main_id,
    x1.version_main_name,
    NULL::integer AS project_id,
    1 AS version_type
   FROM ( SELECT DISTINCT substr(t1.name::text, 1, 3) AS version_main_name
           FROM cabinet.version t1
          WHERE t1.service_id = 1) x1
UNION ALL
 SELECT vw_prj_project_version.version_id + 1000 AS version_main_id,
    vw_prj_project_version.version_name AS version_main_name,
    vw_prj_project_version.project_id,
    2 AS version_type
   FROM cabinet.vw_prj_project_version;

ALTER TABLE cabinet.vw_version_main OWNER TO pavlov;

------------------------------------------------------
-- DROP VIEW cabinet.vw_prj_claim_filter_item;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_filter_item AS 
 SELECT t1.field_id,
    t1.field_name,
    t1.field_name_rus,
    t1.field_type_id,
    t1.ord,
    t1.cache_id,
    t2.field_type_name,
    t2.field_type_name_rus,
    false AS is_field_in_filter,
    NULL::integer AS filter_id,
    NULL::integer AS op_type_id,
    NULL::character varying AS field_id_value,
    NULL::character varying AS field_name_value,
    false AS is_value_fixed,
    NULL::character varying AS filter_name,
    NULL::integer AS user_id,
    false AS is_active,
    NULL::character varying AS user_name,
    NULL::character varying AS op_type_name,
    NULL::character varying AS op_type_name_rus,
    NULL::character varying AS op_type_symbol,
    t15.cache_name,
    t15.value_field AS cache_value_field,
    t15.text_field AS cache_text_field
   FROM cabinet.prj_claim_field t1
     JOIN cabinet.prj_claim_field_type t2 ON t1.field_type_id = t2.field_type_id
     LEFT JOIN cabinet.prj_cache t15 ON t1.cache_id = t15.cache_id
UNION
 SELECT t11.field_id,
    t11.field_name,
    t11.field_name_rus,
    t11.field_type_id,
    t11.ord,
    t11.cache_id,
    t12.field_type_name,
    t12.field_type_name_rus,
        CASE
            WHEN t13.field_id IS NULL THEN false
            ELSE true
        END AS is_field_in_filter,
    t1.filter_id,
    t13.op_type_id,
    t13.field_id_value,
    t13.field_name_value,
    t13.is_value_fixed,
    t1.filter_name,
    t1.user_id,
    t1.is_active,
    t14.user_name,
    t15.op_type_name,
    t15.op_type_name_rus,
    t15.op_type_symbol,
    t16.cache_name,
    t16.value_field AS cache_value_field,
    t16.text_field AS cache_text_field
   FROM cabinet.prj_claim_filter t1
     LEFT JOIN cabinet.prj_claim_field t11 ON 1 = 1
     LEFT JOIN cabinet.prj_claim_field_type t12 ON t11.field_type_id = t12.field_type_id
     LEFT JOIN cabinet.prj_claim_filter_item t13 ON t11.field_id = t13.field_id AND t1.filter_id = t13.filter_id
     LEFT JOIN cabinet.cab_user t14 ON t1.user_id = t14.user_id
     LEFT JOIN cabinet.prj_claim_filter_op_type t15 ON t13.op_type_id = t15.op_type_id
     LEFT JOIN cabinet.prj_cache t16 ON t11.cache_id = t16.cache_id;

ALTER TABLE cabinet.vw_prj_claim_filter_item OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW cabinet.vw_prj_report1;

CREATE OR REPLACE VIEW cabinet.vw_prj_report1 AS 
 SELECT row_number() OVER (ORDER BY x1.gr, x1.task_num) AS id,
    x1.gr,
    x1.gr_name,
    x1.task_num,
    x1.active_first_exec_user_name,
    x1.done_last_date_end,
    count(x1.*) OVER (PARTITION BY x1.gr) AS gr_cnt,
    sum(
        CASE
            WHEN x1.gr = 4 THEN 1
            ELSE 0
        END) OVER (PARTITION BY NULL::text) AS done_cnt,
    count(x1.*) OVER (PARTITION BY NULL::text) AS cnt
   FROM ( WITH cte1 AS (
                 SELECT t1.claim_id,
                    t1.claim_num,
                    t1.task_num,
                    t1.work_num,
                    t1.work_type_id,
                    t1.work_type_name,
                    t1.state_type_id,
                    t1.state_type_name,
                    t1.exec_user_id,
                    t1.exec_user_name,
                    first_value(t1.exec_user_name) OVER (PARTITION BY t1.claim_id, t1.task_num ORDER BY
                        CASE
                            WHEN t1.state_type_id = ANY (ARRAY[1, 4]) THEN 0
                            ELSE 1
                        END) AS active_first_exec_user_name,
                    t1.date_end,
                    max(t1.date_end) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_last_date_end,
                    count(*) OVER (PARTITION BY t1.claim_id, t1.task_num) AS work_cnt,
                    sum(
                        CASE
                            WHEN t1.state_type_id = ANY (ARRAY[1, 4]) THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS active_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[1, 4])) AND t1.work_type_id = 3 THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS active_progr_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[1, 4])) AND t1.work_type_id = 4 THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS active_test_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[1, 4])) AND (t1.work_type_id <> ALL (ARRAY[3, 4])) THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS active_other_work_cnt,
                    sum(
                        CASE
                            WHEN t1.state_type_id = ANY (ARRAY[2, 3]) THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[2, 3])) AND t1.work_type_id = 3 THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_progr_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[2, 3])) AND t1.work_type_id = 4 THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_test_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[2, 3])) AND (t1.work_type_id <> ALL (ARRAY[3, 4])) THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_other_work_cnt
                   FROM cabinet.vw_prj_work t1
                     JOIN cabinet.vw_prj_claim t2 ON t1.claim_id = t2.claim_id AND t2.project_id = 1 AND t2.version_type_id = 3
                )
         SELECT DISTINCT 1 AS gr,
            'на тестировании'::text AS gr_name,
            (cte1.claim_num::text || '/'::text) || cte1.task_num AS task_num,
            cte1.active_first_exec_user_name,
            NULL::timestamp without time zone AS done_last_date_end
           FROM cte1
          WHERE cte1.active_test_work_cnt > 0
        UNION ALL
         SELECT DISTINCT 2 AS gr,
            'передано на доработку'::text AS gr_name,
            (cte1.claim_num::text || '/'::text) || cte1.task_num AS task_num,
            cte1.active_first_exec_user_name,
            NULL::timestamp without time zone AS done_last_date_end
           FROM cte1
          WHERE cte1.active_test_work_cnt <= 0 AND cte1.done_test_work_cnt > 0 AND cte1.active_progr_work_cnt > 0
        UNION ALL
         SELECT DISTINCT 3 AS gr,
            'в разработке'::text AS gr_name,
            (cte1.claim_num::text || '/'::text) || cte1.task_num AS task_num,
            cte1.active_first_exec_user_name,
            NULL::timestamp without time zone AS done_last_date_end
           FROM cte1
          WHERE cte1.done_test_work_cnt <= 0 AND cte1.active_progr_work_cnt > 0
        UNION ALL
         SELECT DISTINCT 4 AS gr,
            'выполнено'::text AS gr_name,
            (cte1.claim_num::text || '/'::text) || cte1.task_num AS task_num,
            NULL::text AS active_first_exec_user_name,
            cte1.done_last_date_end
           FROM cte1
          WHERE cte1.active_work_cnt <= 0) x1;

ALTER TABLE cabinet.vw_prj_report1 OWNER TO pavlov;
------------------------------------------------------
-- DROP VIEW und.vw_stock_log;

CREATE OR REPLACE VIEW und.vw_stock_log AS 
 SELECT t1.client_id,
    t1.client_name,
    t1.sales_id,
    t1.sales_name,
    t11.crt_date AS last_upl_date,
    t12.crt_date AS last_downl_date,
    t13.crt_date AS last_del_date
   FROM cabinet.vw_client_service t1
     LEFT JOIN ( SELECT t2.sales_id,
            max(t1_1.crt_date) AS crt_date
           FROM und.stock_batch t1_1
             JOIN und.stock t2 ON t1_1.stock_id = t2.stock_id
          GROUP BY t2.sales_id) t11 ON t1.sales_id = t11.sales_id
     LEFT JOIN ( SELECT downl.sales_id,
            max(downl.crt_date) AS crt_date
           FROM und.stock_sales_download downl
          GROUP BY downl.sales_id) t12 ON t1.sales_id = t12.sales_id
     LEFT JOIN ( SELECT t1_1.obj_id AS sales_id,
            max(t1_1.date_beg) AS crt_date
           FROM esn.log_esn t1_1
          WHERE t1_1.log_esn_type_id = 31
          GROUP BY t1_1.obj_id) t13 ON t1.sales_id = t12.sales_id
  WHERE t1.service_id = 65;

ALTER TABLE und.vw_stock_log OWNER TO pavlov;
------------------------------------------------------