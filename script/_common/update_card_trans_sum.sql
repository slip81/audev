DO 
$$DECLARE 
  _business_id bigint := 1013489512715650905; -- ��� �����������
  _card_id bigint; -- ������� ����� ����� � ����� 
  _sum_sales numeric; -- ����� �������� �� ����� (�������, ��������, ���. ������, ������ ����)
BEGIN
    FOR _card_id IN SELECT card_id from card
             where business_id = _business_id 
    LOOP
		select  
		  sum(
            case when t3.trans_kind = 1 then coalesce(t4.unit_value_with_discount, 0)
			     when (t3.trans_kind = 2 and t4.unit_value_with_discount is NULL) then coalesce(t4.unit_value, 0)
                 when (t3.trans_kind = 2 and t4.unit_value_with_discount is not NULL) then coalesce(t4.unit_value_with_discount, 0)
				 when t3.trans_kind in (3, 4) then coalesce(t3.trans_sum, 0)
				 else 0
            end
          ) INTO _sum_sales		
          from card_transaction t3 left join card_transaction_detail t4 on t4.card_trans_id = t3.card_trans_id
          where t3.card_id = _card_id;

        update card set curr_trans_sum = _sum_sales where card_id = _card_id;         	       
    END LOOP;
END$$;