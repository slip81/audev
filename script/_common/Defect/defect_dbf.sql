﻿-- Table: defect_dbf

-- DROP TABLE defect_dbf;

CREATE TABLE defect_dbf
(  
  "MNFGRNX" bigint NOT NULL,
  "MNFNX" bigint NOT NULL,
  "MNFNMR" character varying(254) NOT NULL,
  "COUNTRYR" character varying(150) NOT NULL,
  "DRUGTXT" character varying(254) NOT NULL,
  "SERNM" character varying(50) NOT NULL,
  "LETTERSNR" character varying(50) NOT NULL,
  "LETTERSDT" date NOT NULL,
  "LABNMR" character varying(200) NOT NULL,
  "QUALNMR" character varying(254) NOT NULL,
  "TRADENMNX" bigint NOT NULL,
  "INNNX" bigint NOT NULL,
  "ISALLS" bigint NOT NULL,
  "DISTRIB" character varying(80) NOT NULL,
  "STATDEF" character varying(80) NOT NULL  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE defect_dbf OWNER TO pavlov;

delete from defect_dbf;

insert into defect_dbf ("MNFGRNX", "MNFNX", "MNFNMR", "COUNTRYR", "DRUGTXT", "SERNM", "LETTERSNR", "LETTERSDT", "LABNMR", "QUALNMR", "TRADENMNX", "INNNX", "ISALLS", "DISTRIB", "STATDEF")
values (0, 0, '"Beijing second pharmaceutical factory"', 'Китай', 'Bendazol (Дибазол), субстанция', 'P9008', '293-22а/60', cast('15.10.99' as date), 'Государственный научно-исследовательский институт по стандартизации и контролю лекарственных средств', '1,2-Фенилдиамин, Органические примеси, Прозрачность раствора, Растворимость в воде.', 0, 0, 0, '_', 'Возвращено поставщикам');

insert into defect_dbf ("MNFGRNX", "MNFNX", "MNFNMR", "COUNTRYR", "DRUGTXT", "SERNM", "LETTERSNR", "LETTERSDT", "LABNMR", "QUALNMR", "TRADENMNX", "INNNX", "ISALLS", "DISTRIB", "STATDEF")
values (0, 0, 'some text', 'страна 2', 'some text', 'some text', 'some text', current_date, 'some text', 'some text', 0, 0, 0, 'some text', 'some text');

insert into defect_dbf ("MNFGRNX", "MNFNX", "MNFNMR", "COUNTRYR", "DRUGTXT", "SERNM", "LETTERSNR", "LETTERSDT", "LABNMR", "QUALNMR", "TRADENMNX", "INNNX", "ISALLS", "DISTRIB", "STATDEF")
select 0, 0, cast(producer as character varying(254)), country_name, cast(full_name as character varying(254)), series, document_num, cast(accept_date as date), '-', cast(status_name as character varying(254)), 0, 0, 0, '_', cast(defect_type as  character varying(80))
from vw_defect where defect_id = 39280;


/*
select * from defect_dbf;

select * from vw_defect order by defect_id desc limit 100;

select * from log_event order by log_id desc;
*/