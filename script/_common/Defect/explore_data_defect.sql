﻿select * from synchronizer.defect_status

select * from synchronizer.defect order by id desc

select distinct defect_status from synchronizer.defect order by defect_status
select distinct defect_reason from synchronizer.defect order by defect_reason

/*

Стыковка полей в synchronizer.defect с полями на сайте http://www.roszdravnadzor.ru:

ТН + Упаковка - preparation_name
Серия - series
Производитель - producer
Страна - country_id
Статус ЛС - defect_reason (не всегда, бывает что там значения, занесенные вручную)
Тип - defect_status (141 различных не пустых наименований)
Масштаб - то что в скобках в defect_status
Инф. письмо - normative_document_id + normative_document_number + normative_document_accept_date

Оставшиеся поля в synchronizer.defect:

labaratory
distributor
parent_defect_id

region_id
region_name

*/
