﻿/*
select * from defect;
select * from defect order by defect_id desc limit 100;
select * from synchronizer.defect order by id desc limit 100;
*/

/*
select * from defect_status

select * from defect_status where lower(trim(status_name)) = lower(trim('контрафактное ЛС'))


insert into defect_status (status_id, status_name)
values (1, 'Контрафактное ЛС');
insert into defect_status (status_id, status_name)
values (2, 'Недоброкачественное ЛС');
insert into defect_status (status_id, status_name)
values (3, 'Незарегистрированное ЛС');
insert into defect_status (status_id, status_name)
values (4, 'Сомнительное ЛС');
insert into defect_status (status_id, status_name)
values (5, 'Фальсифицированное ЛС');
insert into defect_status (status_id, status_name)
values (6, 'Оригинальное ЛС');
insert into defect_status (status_id, status_name)
values (101, 'Прочее');
*/

/*

select * from document_type

insert into document_type (type_id, type_name)
values (1, 'Информационное письмо');
*/

/*
select * from document order by document_num

insert into document (document_name, document_num, accept_date, create_date, link, comment, document_type_id, created_on, created_by, old)
select distinct 'Информационное письмо ' || trim(t1.normative_document_number) || ' от ' || trim(cast(date(t1.normative_document_accept_date) as character varying))
, trim(t1.normative_document_number), t1.normative_document_accept_date, t1.normative_document_accept_date, '', '', 1, current_date, 'pavlov', 1
from synchronizer.defect t1;
-- 7677

delete from document where document_num is null
*/

/*
select * from prep_country

insert into prep_country (country_id, name, created_by, created_on)
select t1.id, t1.name, 'pavlov', current_date
from synchronizer.country t1;

SELECT setval('prep_country_country_id_seq', (SELECT MAX(country_id) FROM prep_country));
*/

/*
select * from defect_source

insert into defect_source (source_id, source_name)
values (1, 'Ручной ввод');
insert into defect_source (source_id, source_name)
values (2, 'synchronizer');
insert into defect_source (source_id, source_name)
values (3, 'roszdravnadzor.ru');
*/

/*
insert into defect (full_name, series, producer, defect_status_id, defect_type, country_id, document_id, defect_status_text, is_drug, old_id, defect_source_id, created_on, created_by)
select preparation_name, series, producer, (select x1.status_id from defect_status x1 where lower(trim(x1.status_name)) = lower(trim(t1.defect_reason)) limit 1)
, t1.defect_status, t1.country_id, (select x2.document_id from document x2 where lower(trim(x2.document_num)) = lower(trim(t1.normative_document_number)) limit 1)
, t1.defect_reason, 1, t1.id, 2, current_date, 'pavlov'
from synchronizer.defect t1;

--select distinct defect_reason from synchronizer.defect where defect_reason like '%мед%' and defect_reason like '%изд%' order by defect_reason
--select distinct defect_status from synchronizer.defect order by defect_status

update defect set defect_status_id = 101 where coalesce(defect_status_id, 0) <= 0
update defect set defect_status_text = null where coalesce(defect_status_id, 0) < 101

select * from defect where coalesce(defect_status_id, 0) = 101 order by defect_status_text;
select * from defect where coalesce(defect_status_id, 0) < 101 order by defect_status_text;

select * from defect where coalesce(document_id, 0) <= 0
select * from synchronizer.defect where id in (69826, 69825, 69827, 69832)

select * from synchronizer.defect where coalesce(normative_document_id, 0) <= 0

--select count(*) from defect
--select count(*) from synchronizer.defect
-- 38691
*/