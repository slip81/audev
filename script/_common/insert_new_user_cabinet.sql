﻿

-- select * from cabinet.cab_user order by user_id;

insert into cabinet.cab_user (user_id, user_name, user_login, is_admin, is_superadmin, full_name)
values (21, 'ХЮВ', 'horkov', true, false, 'ХЮВ');


-- select * from cabinet.crm_user order by user_id;

insert into cabinet.crm_user (user_id, user_name, cab_user_id, is_boss, is_executer)
values (20, 'ХЮВ', 21, false, true);


--select * from cabinet.cab_grid_column_user_settings order by user_id;

delete from cabinet.cab_grid_column_user_settings where user_id = 21;

insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
select
column_id, 21, column_num, is_visible, width, is_filterable, is_sortable
from cabinet.cab_grid_column_user_settings
where user_id = 1
;


--select * from cabinet.cab_grid_user_settings

delete from cabinet.cab_grid_user_settings where user_id = 21;

insert into cabinet.cab_grid_user_settings (grid_id, user_id, sort_options, filter_options, back_color_column_id)
select 
grid_id, 21, sort_options, filter_options, back_color_column_id
from cabinet.cab_grid_user_settings
where user_id = 1
;

-- select * from cabinet.crm_project_user_settings;

delete from cabinet.crm_project_user_settings where user_id = 21;

insert into cabinet.crm_project_user_settings (project_id, user_id, fore_color)
select
project_id, 21, fore_color
from cabinet.crm_project_user_settings
where user_id = 1;


-- select * from cabinet.crm_state_user_settings;

delete from cabinet.crm_state_user_settings where user_id = 21;

insert into cabinet.crm_state_user_settings (state_id, user_id, fore_color)
select
state_id, 21, fore_color
from cabinet.crm_state_user_settings
where user_id = 1;

-- select * from cabinet.crm_task_user_filter

delete from cabinet.crm_task_user_filter where user_id = 21;

insert into cabinet.crm_task_user_filter (user_id, grid_id, raw_filter, curr_group_id)
select
21, grid_id, raw_filter, curr_group_id
from cabinet.crm_task_user_filter
where user_id = 1;

-- select * from cabinet.crm_user_user_settings;

delete from cabinet.crm_user_user_settings where user_id = 21;

insert into cabinet.crm_user_user_settings (crm_user_id, user_id, fore_color)
select
crm_user_id, 21, fore_color
from cabinet.crm_user_user_settings
where user_id = 1;