
DO 
$$DECLARE 
  _business_id bigint := 958528103041205513; -- ��� �����������
  _card_id bigint; -- ������� ����� ����� � ����� 
  _sum_sales numeric; -- ����� �������� �� ����� (�������, ��������, ���. ������, ������ ����)
  _date_beg date := '2016-01-01'; -- � ����� ���� �������� �������� �������� �� �����
BEGIN
    set search_path to 'discount';
	
    FOR _card_id IN SELECT card_id from card
             where business_id = _business_id 
             --and card_id = 974577071785247884
    LOOP
		select  
		  sum(
            case when t3.trans_kind = 1 then coalesce(t4.unit_value_with_discount, 0)
			     when (t3.trans_kind = 2 and t4.unit_value_with_discount is NULL) then coalesce(t4.unit_value, 0)
                 when (t3.trans_kind = 2 and t4.unit_value_with_discount is not NULL) then coalesce(t4.unit_value_with_discount, 0)
				 when t3.trans_kind in (3, 4) then coalesce(t3.trans_sum, 0)
				 else 0
            end
          ) INTO _sum_sales		
          from card_transaction t3 left join card_transaction_detail t4 on t4.card_trans_id = t3.card_trans_id
          where t3.card_id = _card_id
          and t3.status = 0
          and t3.date_beg >= _date_beg;

        update card set curr_trans_sum = _sum_sales where card_id = _card_id;         	  
        --update card set curr_trans_sum =  3766.52 where card_id = _card_id;         	          
    END LOOP;
END$$;