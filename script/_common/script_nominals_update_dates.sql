
with a as 
(
		SELECT card_id, card_nominal_id, unit_type, date_beg, date_end
		, count(*) over(partition by card_id, unit_type) as cnt		
		, max(card_nominal_id) over(partition by card_id, unit_type) as max_card_nominal_id
		from discount.card_nominal 
		where 1=1
		--and card_id = 1043396761097340711
		and date_end is null 
		)
update discount.card_nominal b
set date_end = b.date_beg
from a
where b.card_nominal_id = a.card_nominal_id
and a.cnt > 1 and b.card_nominal_id <> a.max_card_nominal_id;

/*
-- old version

DO $$
DECLARE
    _row_nominal RECORD;
BEGIN	
	-- ���� �� ���������
	FOR _row_nominal IN 
		SELECT card_id, unit_type, max(card_nominal_id) AS card_nominal_id 
		from discount.card_nominal 
		where date_end is null 
		group by card_id, unit_type 
		having count(*) > 1
	LOOP 
		update discount.card_nominal set date_end = date_beg 
		where card_id = _row_nominal.card_id and unit_type = _row_nominal.unit_type
		and date_end is null and card_nominal_id <> _row_nominal.card_nominal_id;			
	END LOOP; -- ���� �� ���������			
END
$$;
*/