﻿----------------------------------------
DROP VIEW esn.vw_exchange_user;
DROP VIEW cabinet.vw_license;
DROP VIEW cabinet.vw_license2;
DROP VIEW cabinet.vw_service_cva_data;
DROP VIEW cabinet.vw_workplace;
DROP VIEW cabinet.vw_client_report1;
DROP VIEW cabinet.allclients;
DROP VIEW esn.vw_defect_dbf;
DROP VIEW esn.vw_defect;
DROP VIEW cabinet.vw_cab_user;
DROP VIEW discount.vw_programm;
DROP VIEW discount.vw_card_transaction;
DROP VIEW discount.vw_card_type;
DROP VIEW discount.vw_report_card_info;
DROP VIEW discount.vw_report_check_count;
DROP VIEW discount.vw_report_unused_cards;
DROP VIEW discount.vw_card;
DROP VIEW discount.vw_business_org_user;
DROP VIEW esn.vw_log_esn_exchange_partner;
DROP VIEW esn.vw_ved_source_region;
DROP VIEW esn.vw_ved_reestr_item;
DROP VIEW esn.vw_ved_reestr_region_item;
DROP VIEW esn.vw_ved_reestr_item_out;
DROP VIEW esn.vw_ved_reestr_region_item_out;
DROP VIEW esn.vw_exchange_monitor;
DROP VIEW cabinet.vw_client_service_summary;
DROP VIEW und.vw_stock_log;
DROP VIEW cabinet.vw_client_service;
DROP VIEW cabinet.vw_workplace_rereg;
DROP VIEW cabinet.vw_client_max_version;
DROP VIEW cabinet.vw_client_min_version;
DROP VIEW cabinet.vw_client_service_reg_an_unreg;
DROP VIEW cabinet.vw_client_service_registered;
DROP VIEW cabinet.vw_client_service_unregistered;
DROP VIEW esn.vw_exchange_client;
DROP VIEW cabinet.vw_cab_user2;
DROP VIEW cabinet.vw_crm_project;
DROP VIEW cabinet.vw_crm_priority;
DROP VIEW cabinet.vw_cab_grid_column_user_settings;
DROP VIEW discount.vw_card_type_business_org;
DROP VIEW discount.vw_business_summary;
DROP VIEW discount.vw_business_summary_graph;
DROP VIEW cabinet.vw_notify;
DROP VIEW discount.vw_programm_order;
DROP VIEW esn.vw_exchange_file_log;
DROP VIEW esn.vw_exchange_file_node;
DROP VIEW esn.vw_datasource_exchange_item;
DROP VIEW cabinet.vw_sales;
DROP VIEW cabinet.vw_client_user;
DROP VIEW cabinet.vw_service_cva_info;
DROP VIEW cabinet.vw_client_user_widget;
DROP VIEW cabinet.vw_client_info;
DROP VIEW discount.vw_business_summary_graph2;
DROP VIEW cabinet.vw_service;
DROP VIEW cabinet.vw_client_service_param;
DROP VIEW cabinet.vw_auth_user_section_perm;
DROP VIEW cabinet.vw_auth_user_perm;
DROP VIEW cabinet.vw_auth_user_role;
DROP VIEW cabinet.vw_auth_role_section_perm;
DROP VIEW cabinet.vw_auth_role_perm;
DROP VIEW cabinet.vw_auth_role;
DROP VIEW cabinet.vw_storage_file;
DROP VIEW cabinet.vw_log_storage;
DROP VIEW cabinet.vw_storage_folder_tree;
DROP VIEW cabinet.vw_storage_trash;
DROP VIEW cabinet.vw_storage_folder;
DROP VIEW cabinet.vw_service_kit_item;
DROP VIEW cabinet.vw_cab_help_tag;
DROP VIEW cabinet.vw_crm_notify_param_attr;
DROP VIEW cabinet.vw_crm_notify_param_content;
DROP VIEW cabinet.vw_crm_notify_param_user;
DROP VIEW cabinet.vw_client_compare_cabinet;
DROP VIEW cabinet.vw_client_compare_crm;
DROP VIEW discount.vw_campaign;
DROP VIEW discount.vw_campaign_business_org;
DROP VIEW cabinet.vw_update_batch;
DROP VIEW cabinet.vw_update_batch_item;
DROP VIEW cabinet.vw_prj_user_group_item;
DROP VIEW cabinet.vw_prj_report1;
DROP VIEW cabinet.vw_prj_work;
DROP VIEW cabinet.vw_prj_task;
DROP VIEW cabinet.vw_prj_claim;
DROP VIEW cabinet.vw_prj;
DROP VIEW cabinet.vw_prj_claim_simple;
DROP VIEW cabinet.vw_prj_task_simple;
DROP VIEW cabinet.vw_prj_work_simple;
DROP VIEW cabinet.vw_prj_mess;
DROP VIEW cabinet.vw_prj_log;
DROP VIEW cabinet.vw_prj_work_default;
DROP VIEW und.vw_doc_product_filter;
DROP VIEW discount.vw_report_card_fstsale;
DROP VIEW discount.vw_card_holder;
DROP VIEW und.vw_product;
DROP VIEW und.vw_partner_good_filter;
DROP VIEW und.vw_product_and_good;
DROP VIEW und.vw_partner_good;
DROP VIEW und.vw_doc_import_match_variant;
DROP VIEW und.vw_doc_import_match;
DROP VIEW und.vw_doc;
DROP VIEW und.vw_doc_import;
DROP VIEW cabinet.vw_prj_file;
DROP VIEW und.vw_stock_row;
DROP VIEW und.vw_stock;
DROP VIEW und.vw_stock_batch;
DROP VIEW und.vw_stock_sales_download;
DROP VIEW cabinet.vw_update_soft;
DROP VIEW und.vw_asna_client;
DROP VIEW und.vw_asna_user;
DROP VIEW und.vw_wa_task;
DROP VIEW und.vw_asna_user_log;
DROP VIEW und.vw_wa_task_lc;
DROP VIEW und.vw_asna_stock_row;
DROP VIEW und.vw_asna_stock_row_linked;
DROP VIEW und.vw_asna_stock_chain;
DROP VIEW und.vw_asna_stock_row_actual;
DROP VIEW und.vw_asna_order;
DROP VIEW und.vw_asna_order_row;
DROP VIEW und.vw_asna_order_row_state;
DROP VIEW und.vw_asna_order_state;
DROP VIEW und.vw_asna_price_row;
DROP VIEW und.vw_asna_price_row_linked;
DROP VIEW und.vw_asna_price_chain;
DROP VIEW und.vw_asna_price_row_actual;
DROP VIEW cabinet.vw_region;
DROP VIEW cabinet.vw_region_zone;
DROP VIEW cabinet.vw_region_price_limit;
DROP VIEW cabinet.vw_region_zone_price_limit;
DROP VIEW discount.vw_programm_step_param;
DROP VIEW discount.vw_programm_step_condition;
DROP VIEW discount.vw_programm_step_logic;
DROP VIEW cabinet.vw_prj_note;
DROP VIEW esn.vw_medical;
DROP VIEW cabinet.vw_prj_task_user_state;
DROP VIEW cabinet.vw_service_esn2;
DROP VIEW cabinet.vw_version_main;
DROP VIEW cabinet.vw_prj_project_version;
DROP VIEW cabinet.vw_prj_claim_filter_item;
----------------------------------------

-- DROP TABLE cabinet.vw_workplace;

CREATE TABLE cabinet.vw_workplace
(
  workplace_id integer NOT NULL,
  registration_key character varying(255),
  description text,
  sales_id integer,
  sales_name text,
  sales_address text,
  client_id integer,
  client_name character varying(255),
  user_name character varying(256),
  workplace_type integer,
  CONSTRAINT vw_workplace_pkey PRIMARY KEY (workplace_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_workplace OWNER TO pavlov;

----------------------------------------

-- Table: cabinet.vw_license

-- DROP TABLE cabinet.vw_license;

CREATE TABLE cabinet.vw_license
(
  client_id integer not null,
  client_full_name character varying(255),
  sales_id integer not null,
  sales_name text,
  workplace_id integer not null,
  workplace_reg_key character varying(255),
  workplace_descr text,
  order_id integer not null,
  order_status integer,
  order_is_confirmed smallint,
  order_active_status integer,
  version_id integer not null,
  version_name character varying(128),
  service_id integer not null,
  service_name character varying(128),
  order_item_id integer not null,
  order_item_quantity integer,
  service_registration_id integer not null,
  license_created_on timestamp without time zone not null,
  license_date_start timestamp without time zone,
  license_date_end timestamp without time zone,
  sales_address text,
  license_id integer NOT NULL,
  CONSTRAINT vw_license_pkey PRIMARY KEY (license_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_license OWNER TO pavlov;

----------------------------------------

-- Table: cabinet.vw_license

-- DROP TABLE cabinet.vw_license2;

CREATE TABLE cabinet.vw_license2
(
  id integer not null,
  client_id integer not null,
  license_id integer NOT NULL,
  dt_start timestamp without time zone,
  dt_end timestamp without time zone,
  service_registration_id integer,
  activation_key character varying,
  workplace_id integer,
  registration_key character varying,
  sales_id integer,
  order_item_id integer not null,
  service_id integer not null,
  version_id integer not null,
  quantity integer,
  order_id integer not null,
  is_conformed smallint,
  active_status integer,
  user_name character varying(256),
  workplace_type integer,
  CONSTRAINT vw_license2_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_license2 OWNER TO pavlov;

---------------------------------------- 

-- DROP TABLE cabinet.allclients;

CREATE TABLE cabinet.allclients
(
  id integer NOT NULL,
  client_name character varying(255),
  phone character varying(64),
  user_id integer,
  user_name character varying(256),
  city_id integer,
  city_name character varying(128),
  region_id integer,
  region_name character varying(128),
  reg_date timestamp without time zone not null,
  last_activity_date timestamp without time zone,
  created_by character varying(255),
  is_activated smallint,
  sales_count bigint,
  is_phis smallint,
  is_moderated smallint,
  contact_person character varying(128),
  business_id text,  
  inn character varying(64),
  legal_address character varying(255),
  absolute_address character varying(255),
  site character varying(255),
  kpp character varying(64),
  state_id integer,
  state_name character varying,
  sm_id integer,
  region_zone_id integer,
  tax_system_type_id integer,
  CONSTRAINT allclients_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.allclients OWNER TO pavlov;
  
----------------------------------------

-- DROP TABLE esn.vw_defect;

CREATE TABLE esn.vw_defect
(
  defect_id bigserial NOT NULL,
  comm_name character varying,
  pack_name character varying,
  full_name character varying,
  series character varying,
  producer character varying,
  defect_status_id integer,
  defect_type character varying,
  country_id bigint,
  document_id bigint,
  parent_defect_id bigint,
  defect_status_text character varying,
  scope character varying,
  is_drug smallint NOT NULL DEFAULT 1,
  old_id integer,
  created_on timestamp without time zone,
  created_by character varying,
  updated_on timestamp without time zone,
  updated_by character varying,
  defect_source_id integer,
  document_name character varying,
  document_num character varying,
  accept_date timestamp without time zone,
  create_date timestamp without time zone,
  link character varying,  
  comment character varying,
  document_type_id integer,
  body_size_kb numeric,
  doc_created_on timestamp without time zone,
  doc_created_by character varying,
  doc_updated_on timestamp without time zone,
  doc_updated_by character varying,
  old smallint,
  status_name character varying,
  source_name character varying,
  country_name character varying,
  CONSTRAINT vw_defect_pkey PRIMARY KEY (defect_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_defect  OWNER TO pavlov;

----------------------------------------

-- DROP TABLE esn.defect_dbf

CREATE TABLE esn.vw_defect_dbf
(  
  "ID" bigint NOT NULL,
  "MNFGRNX" bigint NOT NULL,
  "MNFNX" bigint NOT NULL,
  "MNFNMR" character varying(254) NOT NULL,
  "COUNTRYR" character varying(150) NOT NULL,
  "DRUGTXT" character varying(254) NOT NULL,
  "SERNM" character varying(50) NOT NULL,
  "LETTERSNR" character varying(50) NOT NULL,
  "LETTERSDT" date NOT NULL,
  "LABNMR" character varying(200) NOT NULL,
  "QUALNMR" character varying(254) NOT NULL,
  "TRADENMNX" bigint NOT NULL,
  "INNNX" bigint NOT NULL,
  "ISALLS" bigint NOT NULL,
  "DISTRIB" character varying(80) NOT NULL,
  "STATDEF" character varying(80) NOT NULL,
   CONSTRAINT vw_defect_dbf_pkey PRIMARY KEY ("ID")
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_defect_dbf OWNER TO pavlov;

----------------------------------------

--DROP TABLE discount.vw_card;

CREATE TABLE discount.vw_card
(
  card_id bigint NOT NULL,
  date_beg date,
  date_end date,
  curr_card_status_id bigint,  
  business_id bigint,
  curr_trans_count bigint,
  card_num bigint,
  card_num2 character varying,
  curr_trans_sum numeric,
  curr_holder_id bigint,
  curr_card_type_id bigint,
  curr_bonus_value numeric,
  curr_bonus_percent numeric,
  curr_discount_value numeric,
  curr_discount_percent numeric,
  curr_money_value numeric,
  curr_money_percent numeric,
  mess character varying,
  curr_card_type_name character varying,
  curr_card_status_name character varying,
  curr_holder_name character varying,
  card_id_str character varying,
  card_num_str character varying,
  card_type_group_id bigint,
  source_card_id bigint,
  curr_holder_first_name character varying,
  curr_holder_second_name character varying,
  curr_holder_date_birth date,
  crt_date timestamp without time zone,
  crt_user character varying,
  from_au boolean NOT NULL DEFAULT false,  
  curr_inactive_bonus_value numeric,
  curr_all_bonus_value numeric,
  curr_holder_full_name character varying,
  curr_holder_phone_num character varying,
  curr_holder_address character varying,
  curr_holder_email character varying,
  CONSTRAINT vw_card_pkey PRIMARY KEY (card_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_card OWNER TO pavlov;

--------------------

-- DROP TABLE vw_card_transaction;

CREATE TABLE discount.vw_card_transaction
(
  card_trans_id bigint NOT NULL,
  card_id bigint, -- FK card
  trans_num bigint, -- Номер транзакции
  date_beg timestamp without time zone, -- Дата транзакции
  trans_sum numeric, -- Сумма, на которую совершена транзакция
  trans_kind smallint NOT NULL DEFAULT 0, -- вид транзакции (расчет по карте, возврат по карте, ручной ввод, начальное сальдо)
  status integer, -- Статус транзакции
  canceled_trans_id bigint, -- Отмененная данной транзакция
  programm_result_id bigint, -- FK programm_result
  user_name character varying,
  date_check timestamp without time zone,
  business_id bigint,
  status_date timestamp without time zone,
  is_delayed smallint,
  curr_card_sum numeric,
  card_num bigint,
  card_num2 character varying,
  curr_card_type_name character varying,
  date_check_date_only date,
  curr_card_bonus_value numeric,
  curr_card_percent_value numeric,
  trans_card_bonus_value numeric,
  trans_card_percent_value numeric,
  trans_sum_discount numeric,
  trans_sum_with_discount numeric,
  trans_sum_bonus_for_card numeric,
  trans_sum_bonus_for_pay numeric,
  org_name character varying,
  CONSTRAINT vw_card_transaction_pkey PRIMARY KEY (card_trans_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_card_transaction  OWNER TO pavlov;

--------------------

-- DROP TABLE vw_card_type;

CREATE TABLE discount.vw_card_type
(
  card_type_id bigint NOT NULL,
  card_type_name character varying,
  business_id bigint,
  is_fin_spec smallint NOT NULL,
  card_type_group_id bigint,
  group_name character varying,
  programm_id bigint,
  programm_name character varying,
  descr_short character varying,
  descr_full character varying,
  update_nominal smallint NOT NULL DEFAULT 0,
  reset_interval_bonus_value smallint,
  reset_date timestamp without time zone,
  is_reset_without_sales smallint NOT NULL DEFAULT 0,
  CONSTRAINT vw_card_type_pkey PRIMARY KEY (card_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_card_type  OWNER TO pavlov;

--------------------

--DROP TABLE discount.vw_report_card_info;

CREATE TABLE discount.vw_report_card_info
(
  id bigint NOT NULL,
  business_id bigint,
  card_id bigint,
  card_num bigint,
  curr_discount_percent numeric,
  curr_trans_sum numeric,
  card_holder_name character varying,
  curr_trans_count bigint,
  CONSTRAINT vw_report_card_info_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_report_card_info OWNER TO pavlov;

--------------------

--DROP TABLE discount.vw_report_check_count;

CREATE TABLE discount.vw_report_check_count
(
  id bigint NOT NULL,
  business_id bigint,
  card_id bigint,
  card_num bigint,
  curr_discount_percent numeric,
  curr_trans_sum numeric,
  date_beg date,
  card_trans_id bigint,
  unit_value_with_discount numeric,
  unit_value_discount numeric,
  unit_value numeric,
  detail_id bigint,
  card_holder_name character varying,
  card_type_name character varying,
  curr_bonus_percent numeric,
  unit_value_bonus_for_card numeric,
  unit_value_bonus_for_pay numeric,
  curr_bonus_value numeric,
  CONSTRAINT vw_report_check_count_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_report_check_count OWNER TO pavlov;

--------------------

-- DROP TABLE vw_programm;

CREATE TABLE discount.vw_programm
(
  programm_id bigint NOT NULL,
  programm_name character varying,
  date_beg date,
  date_end date,
  descr_short character varying,
  business_id bigint,
  descr_full character varying,
  parent_template_id bigint,
  card_type_group_id bigint,
  group_name character varying,
  CONSTRAINT vw_programm_pkey PRIMARY KEY (programm_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_programm OWNER TO pavlov;

--------------------

-- DROP TABLE cabinet.vw_cab_user;

CREATE TABLE cabinet.vw_cab_user
(
  item_id bigint NOT NULL,
  user_name character varying,
  role_id bigint NOT NULL,
  use_defaults boolean NOT NULL DEFAULT true,
  role_name character varying,
  org_name character varying,
  business_name character varying,
  business_id bigint,
  user_descr character varying,
  CONSTRAINT vw_cab_user_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_cab_user OWNER TO pavlov;

--------------------

-- DROP TABLE discount.vw_business_org_user;

CREATE TABLE discount.vw_business_org_user
(
  org_user_id bigint NOT NULL,
  user_name character varying,  
  user_descr character varying,
  org_id bigint NOT NULL,
  org_name character varying,
  business_id bigint NOT NULL,
  add_to_timezone integer,
  card_scan_only integer,
  scanner_equals_reader smallint NOT NULL DEFAULT 1,
  allow_card_add boolean NOT NULL DEFAULT false,  
  CONSTRAINT vw_business_org_user_pkey PRIMARY KEY (org_user_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_business_org_user OWNER TO pavlov;

--------------------

-- DROP TABLE esn.vw_log_esn_exchange_partner

CREATE TABLE esn.vw_log_esn_exchange_partner
(
  client_id bigint,
  client_name character varying,
  user_id bigint,
  date_beg_minutes text,
  date_id bigint,
  log_id bigint NOT NULL,
  date_beg timestamp without time zone NOT NULL,
  mess character varying,
  user_name character varying,
  log_esn_type_id bigint,
  obj_id bigint,
  scope integer,
  date_end timestamp without time zone,
  mess2 character varying,  
  CONSTRAINT vw_log_esn_exchange_partner_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_log_esn_exchange_partner OWNER TO pavlov;

--------------------

-- DROP TABLE esn.vw_ved_source_region

CREATE TABLE esn.vw_ved_source_region
(
  item_id bigint NOT NULL,  
  region_id integer,
  url_site character varying,
  url_file_main character varying,
  url_file_res character varying,
  descr character varying,  
  arc_type integer,
  region_name character varying,
  url_file_a_content character varying,
  template_id bigint,
  out_template_id bigint,
  CONSTRAINT vw_ved_source_region_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_ved_source_region OWNER TO pavlov;

--------------------

-- DROP TABLE esn.vw_ved_reestr_item

CREATE TABLE esn.vw_ved_reestr_item
(
  reestr_id bigint NOT NULL,
  reestr_name character varying,
  date_beg date,
  reestr_crt_date timestamp without time zone,
  parent_id bigint,
  item_id bigint NOT NULL,  
  item_code bigint NOT NULL,
  mnn_id bigint,
  comm_name_id bigint,
  producer_id bigint,
  pack_name character varying,
  pack_count integer,
  limit_price numeric,
  price_for_init_pack boolean NOT NULL DEFAULT false,
  reg_num character varying,
  price_reg_date character varying,
  ean13 character varying,
  crt_date timestamp without time zone,  
  mnn character varying,
  producer character varying,
  comm_name character varying,  
  price_reg_ndoc character varying,  
  price_reg_date_full character varying,  
  CONSTRAINT vw_ved_reestr_item_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_ved_reestr_item OWNER TO pavlov;

--------------------

-- DROP TABLE esn.vw_ved_reestr_region_item

CREATE TABLE esn.vw_ved_reestr_region_item
(
  reestr_id bigint NOT NULL,
  reestr_name character varying,
  date_beg date,
  reestr_crt_date timestamp without time zone,
  parent_id bigint,
  item_id bigint NOT NULL,
  item_code bigint NOT NULL,
  pack_name character varying,
  pack_count integer,
  limit_price numeric,
  price_for_init_pack boolean NOT NULL DEFAULT false,
  reg_num character varying,
  price_reg_date character varying,
  ean13 character varying,
  limit_opt_incr numeric,
  limit_rozn_incr numeric,
  limit_rozn_price numeric,
  limit_rozn_price_with_nds numeric,  
  crt_date timestamp without time zone,  
  mnn character varying,
  producer character varying,
  comm_name character varying,  
  gos_reestr_item_id bigint,
  region_id bigint,
  price_reg_ndoc character varying,  
  price_reg_date_full character varying,  
  match_date timestamp without time zone,
  match_user character varying,  
  is_hand_match boolean NOT NULL DEFAULT false,  
  CONSTRAINT vw_ved_reestr_region_item_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_ved_reestr_region_item OWNER TO pavlov;

--------------------

-- DROP TABLE esn.vw_ved_reestr_item_out

CREATE TABLE esn.vw_ved_reestr_item_out
(
  reestr_id bigint NOT NULL,
  reestr_name character varying,
  date_beg date,
  reestr_crt_date timestamp without time zone,
  parent_id bigint,
  item_id bigint NOT NULL,  
  item_code bigint NOT NULL,
  mnn_id bigint,
  comm_name_id bigint,
  producer_id bigint,
  pack_name character varying,
  pack_count integer,
  limit_price numeric,
  price_for_init_pack boolean NOT NULL DEFAULT false,
  reg_num character varying,
  price_reg_date character varying,
  ean13 character varying,
  crt_date timestamp without time zone,  
  out_date date,
  out_reazon character varying,
  mnn character varying,
  producer character varying,
  comm_name character varying,  
  price_reg_ndoc character varying,  
  price_reg_date_full character varying, 
  CONSTRAINT vw_ved_reestr_item_out_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_ved_reestr_item_out OWNER TO pavlov;

--------------------

-- DROP TABLE esn.vw_ved_reestr_region_item_out


CREATE TABLE esn.vw_ved_reestr_region_item_out
(
  reestr_id bigint NOT NULL,
  reestr_name character varying,
  date_beg date,
  reestr_crt_date timestamp without time zone,
  parent_id bigint,
  item_id bigint NOT NULL,
  item_code bigint NOT NULL,
  pack_name character varying,
  pack_count integer,
  limit_price numeric,
  price_for_init_pack boolean NOT NULL DEFAULT false,
  reg_num character varying,
  price_reg_date character varying,
  ean13 character varying,
  limit_opt_incr numeric,
  limit_rozn_incr numeric,
  limit_rozn_price numeric,
  limit_rozn_price_with_nds numeric,  
  crt_date timestamp without time zone,  
  out_date date,
  out_reazon character varying,  
  mnn character varying,
  producer character varying,
  comm_name character varying,  
  gos_reestr_item_id bigint,
  region_id bigint,
  price_reg_ndoc character varying,  
  price_reg_date_full character varying,
  match_date timestamp without time zone,
  match_user character varying,  
  is_hand_match boolean NOT NULL DEFAULT false,
  CONSTRAINT vw_ved_reestr_region_item_out_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_ved_reestr_region_item_out OWNER TO pavlov;

--------------------

-- DROP TABLE esn.vw_exchange_monitor


CREATE TABLE esn.vw_exchange_monitor
(
  id bigint NOT NULL,
  user_id bigint NOT NULL,
  login character varying,
  workplace character varying,
  client_id bigint,
  cabinet_client_id bigint,
  cabinet_sales_id bigint,
  cabinet_workplace_id bigint,  
  client_name character varying,
  sales_name character varying,
  workplace_name character varying,
  reg_id bigint,
  ds_id bigint,
  is_prepared boolean NOT NULL DEFAULT true,
  upload_state integer NOT NULL DEFAULT 0,
  download_state integer NOT NULL DEFAULT 0,
  upload_err_mess character varying,
  download_err_mess character varying,
  upload_ok_date timestamp without time zone,
  download_ok_date timestamp without time zone,
  prepared_date timestamp without time zone,
  upload_last_date timestamp without time zone,
  download_last_date timestamp without time zone,
  upload_last_mess character varying,
  download_last_mess character varying,    
  download_ok_cnt integer,
  download_warn_cnt integer,
  download_err_cnt integer,
  upload_ok_cnt integer,
  upload_warn_cnt integer,
  upload_err_cnt integer,  
  ds_name character varying,
  user_reg_item_id bigint,
  CONSTRAINT vw_exchange_monitor_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_exchange_monitor OWNER TO pavlov;

--------------------

-- DROP TABLE cabinet.vw_client_service

CREATE TABLE cabinet.vw_client_service
(
  id bigint NOT NULL,
  client_id int NOT NULL,
  client_name character varying,
  sales_id int NOT NULL,
  sales_name character varying,
  sales_address character varying,
  workplace_id int NOT NULL,
  workplace_registration_key character varying,
  workplace_description character varying,
  service_id int,
  service_name character varying,
  service_description character varying,
  version_id int,
  version_name character varying,
  is_service boolean NOT NULL DEFAULT false,
  service_priority int,
  activation_key character varying(255),
  have_spec boolean,
  max_workplace_cnt integer,
  CONSTRAINT vw_client_service_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_service OWNER TO pavlov;

--------------------

-- DROP TABLE cabinet.vw_workplace_rereg

CREATE TABLE cabinet.vw_workplace_rereg
(
  rereg_id int NOT NULL,
  person character varying,
  reason character varying,
  manager_activation smallint,
  created_on timestamp without time zone,
  created_by character varying,
  updated_on timestamp without time zone,
  updated_by character varying,
  is_deleted smallint,
  is_used smallint,
  workplace_id integer,  
  registration_key character varying,
  description character varying,
  sales_id int,
  sales_name character varying,
  sales_address character varying,
  client_id int,
  client_name character varying, 
  CONSTRAINT vw_workplace_rereg_pkey PRIMARY KEY (rereg_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_workplace_rereg OWNER TO pavlov;

--------------------

CREATE TABLE cabinet.vw_client_service_registered
(
  id int NOT NULL,
  client_id int NOT NULL,
  sales_id int NOT NULL,
  workplace_id int NOT NULL,
  workplace_description character varying,
  activation_key character varying,
  license_id int,
  service_id int NOT NULL,
  version_id int,
  service_name character varying,
  version_name character varying,
  service_description text,
  have_spec boolean,
  max_workplace_cnt integer,
  CONSTRAINT vw_client_service_registered_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_service_registered OWNER TO pavlov;

--------------------

CREATE TABLE cabinet.vw_client_service_unregistered
(
  id int NOT NULL,
  client_id int NOT NULL,
  service_id int NOT NULL,
  version_id int,
  service_name character varying,
  version_name character varying,  
  service_description character varying,  
  license_id int,
  order_item_id int,
  order_id int,
  have_spec boolean,
  max_workplace_cnt integer,
  CONSTRAINT vw_client_service_unregistered_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_service_unregistered OWNER TO pavlov;

--------------------

CREATE TABLE esn.vw_exchange_user
(
  user_id bigint NOT NULL,
  login character varying,
  exchange_client_id bigint,
  client_id int,
  client_name character varying,
  sales_id int,
  sales_name character varying,
  workplace_id int,
  workplace_description character varying,
  workplace character varying, 
  task_cnt integer,
  task_cnt_completed integer,
  task_info text,
  CONSTRAINT vw_exchange_user_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_exchange_user OWNER TO pavlov;

--------------------

CREATE TABLE cabinet.vw_client_service_reg_an_unreg
(
  is_registered int NOT NULL,
  client_id int NOT NULL,
  workplace_id int,
  service_id int,
  version_id int,
  service_name character varying,
  version_name character varying,
  activation_key character varying,
  have_spec boolean,
  max_workplace_cnt integer,
  CONSTRAINT vw_client_service_reg_an_unreg_pkey PRIMARY KEY (client_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_service_reg_an_unreg OWNER TO pavlov;

--------------------

CREATE TABLE esn.vw_exchange_client
(
  client_id bigint NOT NULL, 
  client_name character varying, 
  cabinet_client_id integer,
  CONSTRAINT vw_exchange_client_pkey PRIMARY KEY (client_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_exchange_client OWNER TO pavlov;

--------------------
CREATE TABLE cabinet.vw_cab_user2
(
  user_id integer NOT NULL,
  user_name character varying,
  user_login character varying,  
  is_superadmin boolean NOT NULL,
  full_name character varying,
  email character varying,
  is_crm_user boolean NOT NULL,
  is_executer boolean NOT NULL,
  crm_user_role_id integer,
  icq character varying,
  is_active boolean NOT NULL DEFAULT false,
  crm_user_role_name character varying,
  auth_role_id integer,
  auth_role_name character varying,
  is_male boolean NOT NULL,
  is_notify_prj boolean NOT NULL,
  CONSTRAINT vw_cab_user2_pkey PRIMARY KEY (user_id)  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_cab_user2 OWNER TO pavlov;

--------------------
CREATE TABLE cabinet.vw_crm_project
(
  project_id integer NOT NULL,  
  project_name character varying,
  user_id integer,
  fore_color character varying,
  CONSTRAINT vw_crm_project_pkey PRIMARY KEY (project_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_crm_project OWNER TO pavlov;
--------------------
CREATE TABLE cabinet.vw_crm_priority
(
  priority_id integer NOT NULL,  
  priority_name character varying,
  user_id integer,
  fore_color character varying,
  ord integer NOT NULL,
  is_control boolean NOT NULL,
  is_boss boolean NOT NULL,
  rate numeric,
  CONSTRAINT vw_crm_priority_pkey PRIMARY KEY (priority_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_crm_priority OWNER TO pavlov;
--------------------
CREATE TABLE cabinet.vw_cab_grid_column_user_settings
(
  column_index integer NOT NULL,
  item_id serial NOT NULL,
  column_id integer NOT NULL,
  user_id integer NOT NULL DEFAULT 0,
  column_num integer,
  is_visible boolean NOT NULL DEFAULT true,
  width integer,
  is_filterable boolean NOT NULL DEFAULT true,
  is_sortable boolean NOT NULL DEFAULT true,
  grid_id integer NOT NULL,
  column_name character varying,
  column_name_rus character varying,
  format character varying,
  templ character varying,
  title character varying,
  CONSTRAINT vw_cab_grid_column_user_settings_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cabinet.vw_cab_grid_column_user_settings OWNER TO pavlov;
--------------------
-- DROP TABLE discount.vw_card_type_business_org;

CREATE TABLE discount.vw_card_type_business_org
(  
  card_type_id bigint NOT NULL,
  card_type_name character varying,  
  business_org_id bigint,
  org_name character varying,
  is_org_ok boolean NOT NULL,
  allow_discount boolean NOT NULL,
  allow_bonus_for_pay boolean NOT NULL,
  allow_bonus_for_card boolean NOT NULL,  
  CONSTRAINT vw_card_type_business_org_pkey PRIMARY KEY (card_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_card_type_business_org OWNER TO pavlov;
--------------------

-- DROP TABLE discount.vw_business_summary;

CREATE TABLE discount.vw_business_summary
(  
  business_id bigint NOT NULL,
  card_cnt integer,
  card_type_group_1_cnt integer,
  card_type_group_2_cnt integer,
  card_type_group_3_cnt integer,
  card_holder_cnt integer,
  card_trans_cnt integer,
  card_trans_today_cnt integer,
  CONSTRAINT vw_business_summary_pkey PRIMARY KEY (business_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_business_summary OWNER TO pavlov;

--------------------
-- DROP TABLE discount.vw_business_summary_graph;

CREATE TABLE discount.vw_business_summary_graph
(  
  id integer NOT NULL,
  business_id bigint NOT NULL,  
  date_beg_date date,
  trans_sum_total numeric,  
  CONSTRAINT vw_business_summary_graph_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_business_summary_graph OWNER TO pavlov;

--------------------
-- DROP TABLE cabinet.vw_notify;

CREATE TABLE cabinet.vw_notify
(  
  notify_id integer NOT NULL,
  scope integer NOT NULL DEFAULT 1,
  target_user_id integer,
  message character varying,
  message_sent boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  sent_date timestamp without time zone,
  transport_type integer NOT NULL DEFAULT 0,
  icq character varying,
  email character varying,
  is_active_icq boolean,
  is_active_email boolean,
  message_sent_email boolean NOT NULL DEFAULT false,
  CONSTRAINT vw_notify_pkey PRIMARY KEY (notify_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_notify OWNER TO pavlov;
--------------------

-- DROP TABLE discount.vw_programm_order;

CREATE TABLE discount.vw_programm_order
(
  order_id int NOT NULL,
  business_id bigint,
  mess character varying,
  descr character varying,
  crt_user character varying,
  crt_date timestamp without time zone,
  done_user character varying,
  done_date timestamp without time zone,
  programm_id bigint,
  card_type_group_id bigint,
  date_beg date,
  trans_sum_min_disc numeric,
  trans_sum_max_disc numeric,
  trans_sum_min_bonus_for_card numeric,
  trans_sum_max_bonus_for_card numeric,
  trans_sum_min_bonus_for_pay numeric,
  trans_sum_max_bonus_for_pay numeric,
  proc_const_disc numeric,
  proc_step_disc character varying,
  sum_step_card_disc boolean NOT NULL DEFAULT false,
  sum_step_trans_disc boolean NOT NULL DEFAULT false,
  proc_const_bonus_for_card numeric,
  proc_step_bonus_for_card character varying,
  sum_step_card_bonus_for_card boolean NOT NULL DEFAULT false,
  sum_step_trans_bonus_for_card boolean NOT NULL DEFAULT false,
  use_max_percent_disc boolean NOT NULL DEFAULT false,
  allow_zero_disc_bonus_for_card boolean NOT NULL DEFAULT false,
  allow_zero_disc_bonus_for_pay boolean NOT NULL DEFAULT false,
  same_for_zhv_disc boolean NOT NULL DEFAULT false,
  same_for_zhv_bonus boolean NOT NULL DEFAULT false,
  proc_const_zhv_disc numeric,
  proc_step_zhv_disc character varying,
  sum_step_card_zhv_disc boolean NOT NULL DEFAULT false,
  sum_step_trans_zhv_disc boolean NOT NULL DEFAULT false,
  proc_const_zhv_bonus_for_card numeric,
  proc_step_zhv_bonus_for_card character varying,
  sum_step_card_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  sum_step_trans_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  use_max_percent_zhv_disc boolean NOT NULL DEFAULT false,
  allow_zero_disc_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  allow_zero_disc_zhv_bonus_for_pay boolean NOT NULL DEFAULT false,
  allow_order_disc integer,
  allow_order_bonus_for_card integer,
  allow_order_bonus_for_pay integer,
  add_to_card_trans_sum boolean NOT NULL DEFAULT false,
  add_to_card_trans_sum_with_disc boolean NOT NULL DEFAULT false,
  add_to_card_trans_sum_with_bonus_for_pay boolean NOT NULL DEFAULT false,
  mess_contains_ext_info boolean NOT NULL DEFAULT false,
  card_type_id bigint,
  proc_const_from_card_disc boolean NOT NULL DEFAULT false,
  proc_const_from_card_zhv_disc boolean NOT NULL DEFAULT false,
  proc_const_from_card_bonus_for_card boolean NOT NULL DEFAULT false,
  proc_const_from_card_zhv_bonus_for_card boolean NOT NULL DEFAULT false,
  trans_sum_max_percent_bonus_for_pay numeric,
  trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay numeric,
  price_margin_percent_lte_forbid_disc numeric,
  price_margin_percent_lte_forbid_zhv_disc numeric,
  price_margin_percent_gte_forbid_disc numeric,
  price_margin_percent_gte_forbid_zhv_disc numeric,
  use_explicit_max_percent_disc numeric,
  use_explicit_max_percent_zhv_disc numeric,  
  order_state character varying,
  business_name character varying,
  card_type_group_name character varying,
  card_type_name character varying,
  programm_name character varying,
  CONSTRAINT vw_programm_order_pkey PRIMARY KEY (order_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_programm_order OWNER TO pavlov;

----------------------------------------


-- DROP TABLE esn.vw_exchange_file_node;

CREATE TABLE esn.vw_exchange_file_node
(  
  node_id int NOT NULL,
  partner_id bigint NOT NULL,
  client_id int NOT NULL,
  sales_id int NOT NULL,
  node_name character varying,
  client_name character varying,
  sales_address character varying,
  partner_name character varying,
  CONSTRAINT vw_exchange_file_node_pkey PRIMARY KEY (node_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_exchange_file_node OWNER TO pavlov;

----------------------------------------

-- DROP TABLE esn.vw_exchange_file_log;

CREATE TABLE esn.vw_exchange_file_log
(  
  id integer NOT NULL,
  node_id integer NOT NULL,
  partner_id bigint NOT NULL,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  partner_name character varying,
  client_name character varying,
  sales_address character varying,
  node_name character varying,
  exchange_item_id int NOT NULL,
  item_name character varying,
  filename_last character varying,
  period_last character varying,
  filename_prev character varying,
  period_prev character varying,  
  days_cnt integer,  
  CONSTRAINT vw_exchange_file_log_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_exchange_file_log OWNER TO pavlov;

----------------------------------------

-- DROP TABLE esn.vw_datasource_exchange_item;

CREATE TABLE esn.vw_datasource_exchange_item
(
  rel_id bigint NOT NULL,
  ds_id bigint NOT NULL,
  item_id integer NOT NULL,
  for_send boolean NOT NULL DEFAULT true,
  for_get boolean NOT NULL DEFAULT true,
  item_type_id integer,
  item_file_name character varying,
  sql_text character varying,
  param character varying,
  file_name_mask character varying,
  ds_name character varying,
  item_name character varying,
  CONSTRAINT vw_datasource_exchange_item_pkey PRIMARY KEY (rel_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_datasource_exchange_item OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_sales;

CREATE TABLE cabinet.vw_sales
(
  sales_id serial NOT NULL,  
  name text NOT NULL,
  client_id integer,
  city_id integer,
  cell character varying(32),
  contact_person character varying(128),
  adress text NOT NULL,
  is_deleted smallint,
  sm_id integer,
  email character varying(255),  
  user_id integer,
  client_name character varying,
  city_name character varying,
  region_id integer,
  region_name character varying,
  user_name character varying, 
  region_zone_id integer,
  tax_system_type_id integer,
  CONSTRAINT vw_sales_pkey PRIMARY KEY (sales_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_sales OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_client_user;

CREATE TABLE cabinet.vw_client_user
(
  user_id integer NOT NULL,
  client_id integer NOT NULL,
  sales_id integer,
  user_name character varying,
  user_name_full character varying,
  is_main_for_sales boolean NOT NULL DEFAULT false,
  need_change_pwd  boolean NOT NULL,
  client_name character varying,
  sales_address character varying,
  user_login character varying,
  user_email character varying,
  is_active smallint,
  role_id integer,
  role_name character varying,
  is_admin boolean NOT NULL,
  is_superadmin boolean NOT NULL,
  CONSTRAINT vw_client_user_pkey PRIMARY KEY (user_id)  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_user OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_service_cva_info;

CREATE TABLE cabinet.vw_service_cva_info
(  
  workplace_id integer NOT NULL,
  send_interval integer,
  last_sent_date timestamp without time zone,
  batch_num_for_send integer,
  batch_date_for_send timestamp without time zone,
  batch_sent_count integer,  
  CONSTRAINT vw_service_cva_info_pkey PRIMARY KEY (workplace_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_service_cva_info OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_service_cva_data;

CREATE TABLE cabinet.vw_service_cva_data
(
  client_id int NOT NULL,
  client_name character varying(255),
  sales_id int NOT NULL,
  sales_address text,
  workplace_description text,
  item_id int NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  batch_num integer,
  batch_date timestamp without time zone,
  batch_state integer NOT NULL DEFAULT 1,
  sending_date timestamp without time zone,
  sent_date timestamp without time zone,
  mess character varying,
  batch_size character varying,
  shortformat integer,
  CONSTRAINT vw_service_cva_data_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_service_cva_data OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_client_user_widget;

CREATE TABLE cabinet.vw_client_user_widget
(
  widget_id integer NOT NULL,
  widget_name character varying,
  widget_descr character varying,
  widget_element_id character varying,
  widget_header character varying,
  widget_content character varying,
  sort_num integer,
  is_active boolean NOT NULL DEFAULT true,
  service_id integer,
  user_id integer,
  is_installed boolean NOT NULL,
  client_id integer,
  sales_id integer,
  state integer, 
  param character varying,
  CONSTRAINT vw_client_user_widget_pkey PRIMARY KEY (widget_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_user_widget OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_client_info;

CREATE TABLE cabinet.vw_client_info
(
  client_id int NOT NULL,
  client_name character varying(255),
  client_address text,
  client_phone character varying(64),
  client_contact character varying(128),
  sales_count bigint,
  installed_service_cnt bigint,
  ordered_service_cnt bigint,
  CONSTRAINT vw_client_info_pkey PRIMARY KEY (client_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_info OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_client_service_summary;

CREATE TABLE cabinet.vw_client_service_summary
(
  client_id int NOT NULL,
  service_id int,
  sales_cnt bigint,
  CONSTRAINT vw_client_service_summary_pkey PRIMARY KEY (client_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_service_summary OWNER TO pavlov;

----------------------------------------

-- DROP TABLE discount.vw_business_summary_graph2;

CREATE TABLE discount.vw_business_summary_graph2
(  
  id integer NOT NULL,
  business_id bigint NOT NULL,  
  date_beg_date date,
  trans_sum_total numeric,
  trans_sum_discount numeric,
  trans_sum_with_discount numeric,
  trans_sum_bonus_for_card numeric,
  trans_sum_bonus_for_pay numeric,
  CONSTRAINT vw_business_summary_graph2_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_business_summary_graph2 OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_service;

CREATE TABLE cabinet.vw_service
(
  id int NOT NULL,
  guid character varying(40),
  name character varying(128),
  description text,
  parent_id integer,
  is_deleted smallint,
  exe_name character varying(128),
  is_site smallint,  
  price numeric(19,5),
  need_key smallint DEFAULT 1,
  "NeedCheckVersionLicense" smallint DEFAULT 1,
  is_service boolean NOT NULL DEFAULT false,
  priority integer,
  have_spec boolean NOT NULL DEFAULT false,
  max_workplace_cnt integer,
  group_id integer,
  group_name character varying,
  CONSTRAINT vw_service_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_service OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_client_service_param;

CREATE TABLE cabinet.vw_client_service_param
(
  param_id integer NOT NULL,
  client_id integer NOT NULL,
  service_id integer NOT NULL,
  max_workplace_cnt integer,
  service_name character varying,
  CONSTRAINT vw_client_service_param_pkey PRIMARY KEY (param_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_service_param OWNER TO pavlov;

----------------------------------------

--DROP TABLE discount.vw_report_unused_cards;

CREATE TABLE discount.vw_report_unused_cards
(
  id bigint NOT NULL,
  business_id bigint,
  card_id bigint,
  card_num bigint,
  curr_discount_percent numeric,
  curr_bonus_percent numeric,
  curr_bonus_value numeric,
  curr_trans_sum numeric,
  card_holder_name character varying,
  last_date_check timestamp without time zone,
  CONSTRAINT vw_report_unused_cards_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_report_unused_cards OWNER TO pavlov;

----------------------------------------

--DROP TABLE cabinet.vw_auth_user_perm;

CREATE TABLE cabinet.vw_auth_user_perm
(
  id int NOT NULL,
  section_id int NOT NULL,
  section_name character varying,
  part_id int,
  part_name character varying,
  user_id int NOT NULL,
  user_name character varying,
  role_id int,
  role_name character varying,
  role_perm boolean NOT NULL,
  user_perm boolean NOT NULL,  
  allow_view boolean NOT NULL,
  allow_edit boolean NOT NULL,
  role_group_id int,
  CONSTRAINT vw_auth_user_perm_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_auth_user_perm OWNER TO pavlov;

----------------------------------------
--DROP TABLE cabinet.vw_auth_user_role;

CREATE TABLE cabinet.vw_auth_user_role
(
  user_id int NOT NULL,
  user_name character varying,
  user_name_full character varying,
  client_id int NOT NULL,
  role_id int,
  role_name character varying,
  group_id int,
  user_login character varying,
  is_superadmin boolean,
  full_name character varying,
  email character varying,
  is_crm_user boolean,
  is_executer boolean,
  crm_user_role_id integer,
  icq character varying,
  is_active boolean,
  crm_user_role_name character varying,  
  is_notify_prj boolean,
  CONSTRAINT vw_auth_user_role_pkey PRIMARY KEY (user_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_auth_user_role OWNER TO pavlov;

----------------------------------------
--DROP TABLE cabinet.vw_auth_role_perm;

CREATE TABLE cabinet.vw_auth_role_perm
(
  id int NOT NULL,
  section_id int NOT NULL,
  section_name character varying,
  part_id int,
  part_name character varying,
  role_id int,
  role_name character varying,
  allow_view boolean NOT NULL,
  allow_edit boolean NOT NULL,
  role_group_id int,
  CONSTRAINT vw_auth_role_perm_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_auth_role_perm OWNER TO pavlov;
----------------------------------------
--DROP TABLE cabinet.vw_auth_role_section_perm;

CREATE TABLE cabinet.vw_auth_role_section_perm
(
  id int NOT NULL,
  section_id int NOT NULL,  
  role_id int,
  role_group_id int,
  section_name character varying,  
  allow_view_cnt integer,
  allow_edit_cnt integer,
  allow_view_section integer,
  allow_edit_section integer,
  CONSTRAINT vw_auth_role_section_perm_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_auth_role_section_perm OWNER TO pavlov;
----------------------------------------
--DROP TABLE cabinet.vw_auth_user_section_perm;

CREATE TABLE cabinet.vw_auth_user_section_perm
(
  id int NOT NULL,
  section_id int NOT NULL,  
  user_id int,
  role_group_id int,
  section_name character varying,  
  allow_view_cnt integer,
  allow_edit_cnt integer,
  allow_view_section integer,
  allow_edit_section integer,
  CONSTRAINT vw_auth_user_section_perm_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_auth_user_section_perm OWNER TO pavlov;

----------------------------------------

CREATE TABLE cabinet.vw_auth_role
(
  role_id int NOT NULL,
  role_name character varying,  
  is_group1 boolean NOT NULL,
  is_group2 boolean NOT NULL,
  CONSTRAINT vw_auth_role_pkey PRIMARY KEY (role_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_auth_role OWNER TO pavlov;

----------------------------------------

CREATE TABLE cabinet.vw_storage_file
(
  file_id integer NOT NULL,
  file_name character varying,
  file_nom integer NOT NULL,
  folder_id integer NOT NULL,
  ord integer NOT NULL,
  file_size character varying,
  physical_path character varying,
  physical_name character varying,
  download_link character varying,
  file_type_id integer NOT NULL,
  file_state_id integer NOT NULL,
  has_version boolean NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  state_date timestamp without time zone,
  state_user character varying,
  folder_name character varying,
  folder_level integer NOT NULL,
  file_type_name character varying,
  group_id integer,
  group_name character varying,
  file_state_name character varying,
  sprite_css_class character varying,
  file_name_full character varying,
  crt_date_str character varying,
  upd_date_str character varying,
  state_date_str character varying,  
  file_name_with_extention character varying,
  file_size_str character varying,
  v1_exists boolean NOT NULL,
  v1_file_version_id integer,
  v1_file_size character varying,
  v1_physical_path character varying,
  v1_physical_name character varying,
  v1_download_link character varying,
  v1_crt_date timestamp without time zone,
  v1_crt_user character varying,
  v1_file_name_full character varying,
  v1_crt_date_str character varying,
  v1_upd_date_str character varying,  
  v1_file_size_str character varying,
  v1_version_num_actual integer,
  v2_exists boolean NOT NULL,
  v2_file_version_id integer,
  v2_file_size character varying,
  v2_physical_path character varying,
  v2_physical_name character varying,
  v2_download_link character varying,
  v2_crt_date timestamp without time zone,
  v2_crt_user character varying,  
  v2_file_name_full character varying,
  v2_crt_date_str character varying,
  v2_upd_date_str character varying,  
  v2_file_size_str character varying,
  v2_version_num_actual integer,
  is_local_link boolean NOT NULL,
  file_date timestamp without time zone,
  file_date_str character varying,
  item_type integer,
  CONSTRAINT vw_storage_file_pkey PRIMARY KEY (file_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_storage_file OWNER TO pavlov;

----------------------------------------

CREATE TABLE cabinet.vw_log_storage
(
  log_id bigint NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  user_id integer,
  mess character varying,
  file_id integer,
  folder_id integer,
  file_name character varying,
  folder_name character varying,
  user_name character varying,
  full_name character varying,
  CONSTRAINT vw_log_storage_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_log_storage OWNER TO pavlov;

----------------------------------------

CREATE TABLE cabinet.vw_storage_folder_tree
(
  folder_id int NOT NULL,
  parent_id int,
  folder_name character varying,
  parent_name character varying,
  full_path character varying,
  CONSTRAINT vw_storage_folder_tree_pkey PRIMARY KEY (folder_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_storage_folder_tree OWNER TO pavlov;

----------------------------------------
CREATE TABLE cabinet.vw_storage_trash
(
  item_id integer NOT NULL,
  item_name character varying,
  file_nom integer NOT NULL,
  folder_id integer NOT NULL,
  ord integer NOT NULL,
  file_size character varying,
  physical_path character varying,
  physical_name character varying,
  download_link character varying,
  file_type_id integer NOT NULL,
  file_state_id integer NOT NULL,
  has_version boolean NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  state_date timestamp without time zone,
  state_user character varying,
  folder_name character varying,
  folder_level integer NOT NULL,
  file_type_name character varying,
  group_id integer,
  group_name character varying,
  file_state_name character varying,
  sprite_css_class character varying,
  file_name_full character varying,
  crt_date_str character varying,
  upd_date_str character varying,
  state_date_str character varying,  
  item_name_with_extention character varying,
  file_size_str character varying,
  del_date timestamp without time zone,
  del_date_str character varying,  
  del_user character varying,
  item_type integer,
  CONSTRAINT vw_storage_trash_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_storage_trash OWNER TO pavlov;
----------------------------------------
-- DROP TABLE cabinet.vw_storage_folder;

CREATE TABLE cabinet.vw_storage_folder
(
  folder_id int NOT NULL,
  folder_name character varying,
  parent_id integer,
  folder_level integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  file_cnt int NOT NULL,
  folder_cnt int NOT NULL,
  crt_date_str character varying,
  CONSTRAINT vw_storage_folder_pkey PRIMARY KEY (folder_id),
  CONSTRAINT vw_storage_folder_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES cabinet.vw_storage_folder (folder_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_storage_folder OWNER TO pavlov;
----------------------------------------
-- DROP TABLE cabinet.vw_service_kit_item;

CREATE TABLE cabinet.vw_service_kit_item
(
  item_id int NOT NULL,
  service_kit_id integer NOT NULL,
  service_id integer NOT NULL,
  version_id integer,
  service_kit_name character varying,
  service_name character varying,
  version_name character varying,
  CONSTRAINT vw_service_kit_item_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_service_kit_item OWNER TO pavlov;
----------------------------------------

-- DROP TABLE cabinet.vw_cab_help_tag;

CREATE TABLE cabinet.vw_cab_help_tag
(
  tag text NOT NULL,
  CONSTRAINT vw_cab_help_tag_pkey PRIMARY KEY (tag)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_cab_help_tag OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_crm_notify_param_attr;

CREATE TABLE cabinet.vw_crm_notify_param_attr
(
  adr int NOT NULL,
  notify_id int NOT NULL,
  user_id int NOT NULL,
  transport_type int,
  column_id int,
  column_name character varying,
  column_name_rus character varying,
  is_active boolean,
  CONSTRAINT vw_crm_notify_param_attr_pkey PRIMARY KEY (adr)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_crm_notify_param_attr OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_crm_notify_param_content;

CREATE TABLE cabinet.vw_crm_notify_param_content
(
  adr int NOT NULL,
  notify_id int NOT NULL,
  user_id int NOT NULL,
  transport_type int,
  column_id int,
  column_name character varying,
  column_name_rus character varying,
  is_active boolean,
  CONSTRAINT vw_crm_notify_param_content_pkey PRIMARY KEY (adr)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_crm_notify_param_content OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_crm_notify_param_user;

CREATE TABLE cabinet.vw_crm_notify_param_user
(
  adr int NOT NULL,
  notify_id int NOT NULL,
  user_id int NOT NULL,
  transport_type int,
  changer_user_id int,
  user_name character varying,
  is_executer boolean,
  is_owner boolean,
  is_active boolean,
  CONSTRAINT vw_crm_notify_param_user_pkey PRIMARY KEY (adr)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_crm_notify_param_user OWNER TO pavlov;

----------------------------------------
-- DROP TABLE cabinet.vw_client_compare_cabinet;

CREATE TABLE cabinet.vw_client_compare_cabinet
(
  id integer NOT NULL,
  city_id integer,
  full_name character varying(255) NOT NULL,
  inn character varying(64),
  kpp character varying(64),
  legal_address character varying(255),
  absolute_address character varying(255),
  bik character varying(64),
  phone character varying(64),
  email character varying(255),
  contact_person character varying(128),
  zip_code character varying(32),
  post character varying(128),
  sm_id integer,
  city_name character varying(128),
  region_name character varying(128),  
  state_id integer,
  state_name character varying,
  crm_name_company_full character varying(254),
  crm_name_region character varying(50),
  crm_name_city character varying(50),
  crm_address character varying(50),
  crm_phone character varying(50),
  crm_email character varying(50),
  crm_inn character varying(50),
  crm_kpp character varying(50),
  crm_face_fio character varying(50),
  crm_p_address character varying(254),      
  CONSTRAINT vw_client_compare_cabinet_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_compare_cabinet OWNER TO pavlov;

----------------------------------------

-- DROP TABLE cabinet.vw_client_compare_crm;

CREATE TABLE cabinet.vw_client_compare_crm
(
  id_company integer NOT NULL,  
  name_company_full character varying(254),
  name_region character varying(50),
  name_city character varying(50),
  address character varying(50),
  phone character varying(50),
  email character varying(50),
  inn character varying(50),
  kpp character varying(50),
  face_fio character varying(50),
  p_address character varying(254),
  id_client integer,
  id_state integer,
  name_state character varying(50),
  cabinet_client_id integer,
  CONSTRAINT vw_client_compare_crm_pkey PRIMARY KEY (id_company)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_compare_crm OWNER TO pavlov;

--------------------
-- DROP TABLE cabinet.vw_client_max_version;

CREATE TABLE cabinet.vw_client_max_version
(
  adr integer NOT NULL,
  id integer,
  name text,
  inn text,
  kpp text,
  sales_id integer,
  adress text,
  workplace_id integer,
  description text,
  registration_key text,
  service_name text,
  version_name text,
  CONSTRAINT vw_client_max_version_pkey PRIMARY KEY (adr)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_max_version OWNER TO pavlov;
--------------------
-- DROP TABLE discount.vw_campaign;

CREATE TABLE discount.vw_campaign
(
  campaign_id integer NOT NULL,
  business_id bigint NOT NULL,
  campaign_name character varying,
  campaign_descr character varying,
  campaign_check_text character varying,  
  date_beg date NOT NULL,
  date_end date NOT NULL,
  max_card_cnt integer,
  card_type_id bigint,
  auto_create_card boolean NOT NULL DEFAULT false,
  card_money_value numeric,
  state integer NOT NULL DEFAULT 0,
  mess character varying,
  issue_date_beg date NOT NULL,
  issue_date_end date NOT NULL,
  issue_rule integer NOT NULL DEFAULT 0,
  issue_condition_type integer NOT NULL DEFAULT 0,
  issue_condition_sale_sum numeric,
  issue_condition_pos_count integer,
  apply_condition_sale_sum numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  card_type_name character varying,
  card_count integer,
  active_card_count integer,
  CONSTRAINT vw_campaign_pkey PRIMARY KEY (campaign_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE discount.vw_campaign OWNER TO pavlov;
--------------------
-- DROP TABLE discount.vw_campaign_business_org;

CREATE TABLE discount.vw_campaign_business_org
(  
  campaign_id integer NOT NULL,
  business_id bigint NOT NULL,
  org_id bigint NOT NULL,
  org_name character varying,
  item_id integer,
  card_num_default character varying,
  is_in_campaign boolean NOT NULL DEFAULT false,
  CONSTRAINT vw_campaign_business_org_pkey PRIMARY KEY (org_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_campaign_business_org OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_update_batch;

CREATE TABLE cabinet.vw_update_batch
(
  batch_id integer NOT NULL,
  batch_name character varying,
  client_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  last_download_date timestamp without time zone,
  client_name character varying(255),
  CONSTRAINT vw_update_batch_pkey PRIMARY KEY (batch_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_update_batch OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_update_batch_item;

CREATE TABLE cabinet.vw_update_batch_item
(
  soft_id integer NOT NULL,
  soft_name character varying,
  batch_id integer,
  client_id integer,
  is_subscribed boolean NOT NULL DEFAULT false,
  ord integer,
  last_download_date timestamp without time zone,
  CONSTRAINT vw_update_batch_item_pkey PRIMARY KEY (soft_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_update_batch_item OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_work;

CREATE TABLE cabinet.vw_prj_work
(
  work_id integer NOT NULL,
  task_id integer NOT NULL,
  claim_id integer NOT NULL,
  work_num integer NOT NULL,
  work_type_id integer NOT NULL,
  exec_user_id integer NOT NULL,
  state_type_id integer NOT NULL,
  is_plan boolean NOT NULL DEFAULT false,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  is_accept boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  cost_plan numeric,
  cost_fact numeric,  
  mess character varying,
  old_task_id integer,
  old_subtask_id integer,  
  work_type_name character varying,
  exec_user_name character varying,
  state_type_name character varying,  
  state_type_templ character varying,
  task_num integer NOT NULL,
  task_text character varying,  
  task_state_type_id integer NOT NULL,
  task_date_plan date,
  task_date_fact date,
  task_state_type_name character varying,  
  task_state_type_templ character varying,  
  claim_num character varying,
  claim_name character varying,
  claim_text character varying,  
  claim_project_id integer NOT NULL,
  claim_module_id integer NOT NULL,
  claim_module_part_id integer NOT NULL,
  claim_project_version_id integer NOT NULL,
  claim_client_id integer NOT NULL,
  claim_priority_id integer NOT NULL,
  claim_resp_user_id integer NOT NULL,
  claim_date_plan date,
  claim_date_fact date,
  claim_project_repair_version_id integer,
  claim_is_archive boolean NOT NULL,
  claim_is_control boolean NOT NULL,
  claim_project_name character varying,
  claim_module_name character varying,
  claim_module_part_name character varying,
  claim_project_version_name character varying,
  claim_client_name character varying,
  claim_priority_name character varying,
  claim_resp_user_name character varying,
  claim_project_repair_version_name character varying,
  claim_state_type_id integer NOT NULL,
  claim_state_type_name character varying,
  claim_state_type_templ character varying,
  is_overdue boolean NOT NULL,
  date_time_beg timestamp without time zone,
  date_time_end timestamp without time zone,
  is_all_day boolean NOT NULL,
  claim_type_id integer NOT NULL,
  claim_stage_id integer NOT NULL,
  claim_type_name character varying,
  claim_stage_name character varying,  
  brief_state_type_id integer,
  brief_state_type_name character varying,  
  date_send timestamp without time zone,
  prj_id_list character varying,
  prj_name_list character varying,
  CONSTRAINT vw_prj_work_pkey PRIMARY KEY (work_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_work OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_task;

CREATE TABLE cabinet.vw_prj_task
(
  task_id integer NOT NULL,
  claim_id integer NOT NULL,
  task_num integer NOT NULL,
  task_text character varying,    
  date_plan date,
  date_fact date,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  old_task_id integer,
  old_subtask_id integer,
  claim_num character varying,
  claim_name character varying,
  claim_text character varying,  
  claim_project_id integer NOT NULL,
  claim_module_id integer NOT NULL,
  claim_module_part_id integer NOT NULL,
  claim_project_version_id integer NOT NULL,
  claim_client_id integer NOT NULL,
  claim_priority_id integer NOT NULL,
  claim_resp_user_id integer NOT NULL,
  claim_date_plan date,
  claim_date_fact date,
  claim_project_repair_version_id integer,
  claim_is_archive boolean NOT NULL,  
  claim_is_control boolean NOT NULL,
  claim_project_name character varying,
  claim_module_name character varying,
  claim_module_part_name character varying,
  claim_project_version_name character varying,
  claim_client_name character varying,
  claim_priority_name character varying,  
  claim_resp_user_name character varying,
  claim_project_repair_version_name character varying,
  claim_state_type_id integer NOT NULL,
  claim_state_type_name character varying,
  claim_state_type_templ character varying,
  task_state_type_id integer,
  work_cnt integer NOT NULL,
  active_work_cnt integer NOT NULL,
  done_work_cnt integer NOT NULL,
  canceled_work_cnt integer NOT NULL,
  task_state_type_name character varying,
  task_state_type_templ character varying,
  old_state_id integer,
  old_state_name character varying,
  active_work_type_id integer,
  active_work_type_name character varying,
  is_overdue boolean NOT NULL,
  claim_type_id integer NOT NULL,
  claim_stage_id integer NOT NULL,
  claim_type_name character varying,
  claim_stage_name character varying,
  task_mess_cnt integer,
  prj_id_list character varying,
  prj_name_list character varying,
  CONSTRAINT vw_prj_task_pkey PRIMARY KEY (task_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_task OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_claim;

CREATE TABLE cabinet.vw_prj_claim
(
  claim_id integer NOT NULL,
  claim_num character varying,
  claim_name character varying,
  claim_text character varying,  
  project_id integer NOT NULL,
  module_id integer NOT NULL,
  module_part_id integer NOT NULL,
  project_version_id integer NOT NULL,
  client_id integer NOT NULL,
  priority_id integer NOT NULL,
  resp_user_id integer NOT NULL,
  date_plan date,
  date_fact date,
  project_repair_version_id integer,
  is_archive boolean NOT NULL DEFAULT false,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  is_control boolean NOT NULL DEFAULT false,
  old_task_id integer,
  project_name character varying,
  module_name character varying,
  module_part_name character varying,
  project_version_name character varying,
  client_name character varying,
  priority_name character varying,
  resp_user_name character varying,
  project_repair_version_name character varying,
  task_cnt integer,
  new_task_cnt integer,
  active_task_cnt integer,
  done_task_cnt integer,
  canceled_task_cnt integer,  
  claim_state_type_id integer,
  claim_state_type_name character varying,
  claim_state_type_templ character varying,  
  old_group_id integer,
  old_group_name character varying,
  old_state_id integer,
  old_state_name character varying,
  active_work_type_id integer,
  active_work_type_name character varying,
  is_overdue boolean NOT NULL,
  brief_state_type_id integer,
  brief_state_type_name character varying,
  exec_user_name_list character varying,
  claim_type_id integer NOT NULL,
  claim_stage_id integer NOT NULL,
  claim_type_name character varying,
  claim_stage_name character varying,
  mess_cnt integer,
  priority_rate numeric,
  claim_type_rate numeric,
  summary_rate numeric,
  is_active_state boolean NOT NULL,
  prj_id_list character varying,
  prj_name_list character varying,
  version_id integer NOT NULL,
  is_version_fixed boolean NOT NULL,
  version_name character varying,
  version_value character varying,
  version_value_full character varying,
  version_type_id integer NOT NULL,
  version_type_name character varying,
  arc_date timestamp without time zone,
  arc_user character varying,
  CONSTRAINT vw_prj_claim_pkey PRIMARY KEY (claim_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_claim OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj;

CREATE TABLE cabinet.vw_prj
(
  prj_id integer NOT NULL,
  prj_name character varying,
  date_beg date,
  date_end date,
  curr_state_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  claim_cnt integer NOT NULL,
  new_claim_cnt integer NOT NULL,
  active_claim_cnt integer NOT NULL,
  done_claim_cnt integer NOT NULL,
  canceled_claim_cnt integer NOT NULL,  
  state_type_id integer,
  state_type_name character varying,
  state_type_name_templ character varying,
  claim_approved_cnt integer NOT NULL,
  claim_new_cnt integer NOT NULL,
  is_archive boolean NOT NULL,
  project_id integer,
  project_name character varying,
  CONSTRAINT vw_prj_pkey PRIMARY KEY (prj_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_work_simple;

CREATE TABLE cabinet.vw_prj_work_simple
(
  work_id integer NOT NULL,
  task_id integer NOT NULL,
  claim_id integer NOT NULL,
  state_type_id integer NOT NULL,
  done_or_canceled integer NOT NULL,
  work_type_id integer NOT NULL,
  work_num integer NOT NULL,
  exec_user_id integer NOT NULL,
  exec_user_name character varying,
  CONSTRAINT vw_prj_work_simple_pkey PRIMARY KEY (work_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_work_simple OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_task_simple;

CREATE TABLE cabinet.vw_prj_task_simple
(
  task_id integer NOT NULL,
  claim_id integer NOT NULL,
  task_state_type_id integer NOT NULL,
  work_cnt integer,
  active_work_cnt integer,
  done_work_cnt integer,
  canceled_work_cnt integer,
  active_work_type_id integer,
  exec_user_name_list character varying,
  CONSTRAINT vw_prj_task_simple_pkey PRIMARY KEY (task_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_task_simple  OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_claim_simple;

CREATE TABLE cabinet.vw_prj_claim_simple
(
  claim_id integer NOT NULL,  
  claim_state_type_id integer NOT NULL,
  task_cnt integer,
  new_task_cnt integer,
  active_task_cnt integer,
  done_task_cnt integer,
  canceled_task_cnt integer,  
  active_work_type_id integer,  
  exec_user_name_list character varying,
  CONSTRAINT vw_prj_claim_simple_pkey PRIMARY KEY (claim_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_claim_simple OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_mess;

CREATE TABLE cabinet.vw_prj_mess
(
  mess_id serial NOT NULL,
  mess character varying,
  mess_num character varying,
  user_id integer NOT NULL,
  prj_id integer,
  claim_id integer,
  task_id integer,
  work_id integer,
  crt_date timestamp without time zone,  
  user_name character varying,
  claim_num character varying,
  task_num integer,
  work_num integer,
  CONSTRAINT vw_prj_mess_pkey PRIMARY KEY (mess_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_mess OWNER TO pavlov;

--------------------
-- DROP TABLE cabinet.vw_prj_log;

CREATE TABLE cabinet.vw_prj_log
(
  log_id integer NOT NULL,
  mess character varying,
  prj_id integer,
  claim_id integer,
  task_id integer,
  work_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  claim_num character varying,
  task_num integer,
  work_num integer,
  claim_is_archive boolean,
  prj_is_archive boolean,
  CONSTRAINT vw_prj_log_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_log OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.prj_work_default;

CREATE TABLE cabinet.vw_prj_work_default
(  
  user_id integer NOT NULL,
  work_type_id integer NOT NULL,
  is_default boolean NOT NULL DEFAULT false,
  is_accept boolean NOT NULL DEFAULT false,
  exec_user_id integer NOT NULL DEFAULT 0,
  work_type_name character varying,
  exec_user_name character varying,
  CONSTRAINT vw_prj_work_default_pkey PRIMARY KEY (work_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_work_default OWNER TO pavlov;

--------------------
-- DROP TABLE und.vw_doc_product_filter;

CREATE TABLE und.vw_doc_product_filter
(  
  doc_id integer NOT NULL,
  doc_name character varying,
  product_cnt integer,
  CONSTRAINT vw_doc_product_filter_pkey PRIMARY KEY (doc_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_doc_product_filter OWNER TO pavlov;
--------------------
-- DROP TABLE discount.vw_report_card_fstsale;

CREATE TABLE discount.vw_report_card_fstsale
(  
  business_id bigint NOT NULL,
  card_id bigint NOT NULL,
  card_num bigint,
  card_num2 character varying,
  curr_holder_id bigint,
  curr_holder_name character varying,
  curr_holder_first_name character varying,
  curr_holder_second_name character varying,
  curr_holder_phone_num character varying,
  card_fstsale_org_name character varying,
  CONSTRAINT vw_report_card_fstsale_pkey PRIMARY KEY (card_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_report_card_fstsale OWNER TO pavlov;
--------------------
-- DROP TABLE discount.vw_card_holder;

CREATE TABLE discount.vw_card_holder
(
  card_holder_id bigint NOT NULL,
  card_holder_surname character varying,
  card_holder_name character varying,
  card_holder_fname character varying,
  date_birth date,
  phone_num character varying,
  address character varying,
  email character varying,
  comment character varying,
  business_id bigint NOT NULL,
  card_holder_full_name text,
  CONSTRAINT vw_card_holder_pkey PRIMARY KEY (card_holder_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.card_holder OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_partner_good;

CREATE TABLE und.vw_partner_good
(
  item_id integer NOT NULL,
  partner_id integer NOT NULL,
  good_id integer NOT NULL,
  partner_code character varying,
  partner_name character varying,
  source_partner_name character varying,
  product_id integer NOT NULL,
  mfr_id integer NOT NULL,
  barcode character varying,
  doc_id integer,
  doc_row_id integer,
  product_name character varying,
  mfr_name character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  apply_group_id integer,
  apply_group_name character varying,
  is_main boolean NOT NULL,
  CONSTRAINT vw_partner_good_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_partner_good OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_product;

CREATE TABLE und.vw_product
(
  product_id integer NOT NULL,
  product_form_id integer,
  pharm_market_id integer,
  inn_id integer,
  dosage_id integer,
  brand_id integer,
  space_id integer,
  metaname_id integer,
  product_name character varying,
  trade_name character varying,
  trade_name_lat character varying,
  trade_name_eng character varying,
  storage_period numeric,
  is_vital boolean NOT NULL DEFAULT false,
  is_powerful boolean NOT NULL DEFAULT false,
  is_poison boolean NOT NULL DEFAULT false,
  is_receipt boolean NOT NULL DEFAULT false,
  drug integer,
  size character varying,
  potion_amount numeric,
  pack_count integer,
  link character varying,
  mess character varying,
  doc_id integer,
  doc_row_id integer,
  is_deleted boolean NOT NULL DEFAULT false,
  good_cnt integer,
  apply_group_id integer,
  apply_group_name character varying,
  CONSTRAINT vw_product_pkey PRIMARY KEY (product_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_product OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_partner_good_filter;

CREATE TABLE und.vw_partner_good_filter
(  
  partner_id integer NOT NULL,
  partner_name character varying,
  good_cnt integer,
  CONSTRAINT vw_partner_good_filter_pkey PRIMARY KEY (partner_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_partner_good_filter OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_product_and_good;

CREATE TABLE und.vw_product_and_good
(
  item_id integer NOT NULL,
  product_id integer NOT NULL,
  product_name character varying,
  doc_id integer,
  doc_row_id integer,
  is_deleted boolean NOT NULL DEFAULT false,
  good_cnt integer,
  partner_id integer,
  good_id integer,
  partner_code character varying,
  partner_name character varying,
  source_partner_name character varying,
  mfr_id integer,
  barcode character varying,
  good_doc_id integer,
  good_doc_row_id integer,
  mfr_name character varying,
  good_is_deleted boolean NOT NULL DEFAULT false,  
  apply_group_id integer,
  apply_group_name character varying,
  is_main boolean,
  CONSTRAINT vw_product_and_good_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_product_and_good OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_doc;

CREATE TABLE und.vw_doc
(
  doc_id integer NOT NULL,
  doc_num character varying,
  doc_name character varying,
  doc_date date,
  doc_type_id integer,
  doc_state_id integer,
  partner_id integer,
  partner_target_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  doc_type_name character varying,
  doc_state_name character varying,
  partner_name character varying,
  row_cnt integer,
  unmatch_cnt integer,
  new_cnt integer,
  syn_cnt integer,
  CONSTRAINT vw_doc_pkey PRIMARY KEY (doc_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_doc OWNER TO pavlov;

--------------------
-- DROP TABLE und.vw_doc_import_match;

CREATE TABLE und.vw_doc_import_match
(
  row_id integer NOT NULL,
  import_id integer NOT NULL,
  row_num integer NOT NULL,
  source_name character varying,
  code character varying,
  mfr character varying,
  mfr_group character varying,
  barcode character varying,
  state integer NOT NULL DEFAULT 0,
  match_product_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  doc_id integer NOT NULL,
  doc_num character varying,
  doc_name character varying,
  doc_date date,
  doc_type_id integer,
  doc_state_id integer,
  partner_id integer,
  doc_type_name character varying,
  doc_state_name character varying,
  partner_name character varying,  
  match_product_name character varying,  
  match_good_id integer,
  match_barcode character varying,
  match_mfr_id integer,
  match_mfr_name character varying,
  is_approved boolean NOT NULL DEFAULT false,  
  percent character varying,
  is_auto_match boolean NOT NULL DEFAULT false,
  name_percent character varying,
  barcode_percent character varying,
  mfr_percent character varying,
  init_source_name character varying,
  apply_group_name character varying,
  CONSTRAINT vw_doc_import_match_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_doc_import_match OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_doc_import_match_variant;

CREATE TABLE und.vw_doc_import_match_variant
(
  variant_id integer NOT NULL,
  row_id integer NOT NULL,
  match_product_id integer NOT NULL,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  match_good_id integer,
  percent character varying,
  is_match_name boolean NOT NULL DEFAULT false,
  is_match_barcode boolean NOT NULL DEFAULT false,
  is_match_mfr boolean NOT NULL DEFAULT false,
  name_similarity numeric,
  barcode_similarity numeric,
  mfr_similarity numeric,
  avg_similarity numeric,
  doc_id integer NOT NULL,
  match_product_name character varying,
  match_barcode character varying,
  match_mfr_id integer,
  match_mfr_name character varying,
  barcode_percent character varying,
  name_percent character varying,
  mfr_percent character varying,
  CONSTRAINT vw_doc_import_match_variant_pkey PRIMARY KEY (variant_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_doc_import_match_variant OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_doc_import;

CREATE TABLE und.vw_doc_import
(
  import_id serial NOT NULL,
  config_type_id integer,
  file_path character varying,
  file_name character varying,
  doc_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  source_partner_id integer,
  row_cnt integer,
  state integer NOT NULL,
  row_done_cnt integer,
  doc_name character varying,
  CONSTRAINT vw_doc_import_pkey PRIMARY KEY (import_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_doc_import OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_file;

CREATE TABLE cabinet.vw_prj_file
(
  file_id integer NOT NULL,
  file_name character varying,
  file_link character varying,
  file_path character varying,
  prj_id integer,
  claim_id integer,
  task_id integer,
  work_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  file_name_url character varying,
  claim_num character varying,
  CONSTRAINT vw_prj_file_pkey PRIMARY KEY (file_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_file OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_stock_row;

CREATE TABLE und.vw_stock_row
(
  row_id serial NOT NULL,
  stock_id integer NOT NULL,
  batch_id integer,
  artikul integer NOT NULL,
  row_stock_id integer NOT NULL,
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  country_name character varying,
  unpacked_cnt integer,
  all_cnt numeric,
  price numeric,
  valid_date date,
  series character varying,
  is_vital boolean NOT NULL DEFAULT false,
  barcode character varying,
  price_firm numeric,
  price_gr numeric,
  percent_gross numeric,
  price_gross numeric,
  sum_gross numeric,
  percent_nds_gross numeric,
  price_nds_gross numeric,
  sum_nds_gross numeric,
  percent numeric,
  sum numeric,
  supplier_name character varying,
  supplier_doc_num character varying,
  gr_date date,
  farm_group_name character varying,
  supplier_doc_date date,
  profit numeric,
  mess character varying,
  state smallint NOT NULL DEFAULT 0,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  depart_id integer NOT NULL,
  depart_name character varying,
  pharmacy_name character varying,
  batch_num integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  batch_crt_date timestamp without time zone,
  client_name character varying,
  sales_name character varying,
  depart_address character varying,
  is_active boolean NOT NULL,
  batch_inactive_date timestamp without time zone,
  batch_is_confirmed boolean NOT NULL DEFAULT false,
  batch_confirm_date timestamp without time zone,  
  CONSTRAINT vw_stock_row_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_stock_row OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_stock;

CREATE TABLE und.vw_stock
(
  stock_id integer NOT NULL,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  depart_id integer NOT NULL,
  depart_name character varying,
  pharmacy_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  client_name character varying,
  sales_name character varying,
  depart_address character varying,
  CONSTRAINT vw_stock_pkey PRIMARY KEY (stock_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_stock OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_stock_batch;

CREATE TABLE und.vw_stock_batch
(
  batch_id serial NOT NULL,
  stock_id integer NOT NULL,
  batch_num integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  is_active boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  depart_id integer NOT NULL,
  depart_name character varying,
  pharmacy_name character varying,
  client_name character varying,
  sales_name character varying,  
  inactive_date timestamp without time zone,
  is_confirmed boolean NOT NULL DEFAULT false,
  confirm_date timestamp without time zone,  
  CONSTRAINT vw_stock_batch_pkey PRIMARY KEY (batch_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_stock_batch OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_stock_sales_download;

CREATE TABLE und.vw_stock_sales_download
(
  download_id serial NOT NULL,
  sales_id integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  is_active boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  download_batch_date_beg timestamp without time zone,
  download_batch_date_end timestamp without time zone,
  batch_row_cnt integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  skip_row_cnt integer,
  new_row_cnt integer,    
  force_all boolean NOT NULL DEFAULT false,
  inactive_date timestamp without time zone,
  is_confirmed boolean NOT NULL DEFAULT false,
  confirm_date timestamp without time zone,  
  CONSTRAINT vw_stock_sales_download_pkey PRIMARY KEY (download_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_stock_sales_download OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_update_soft;

CREATE TABLE cabinet.vw_update_soft
(
  soft_id integer NOT NULL,
  soft_name character varying,
  service_id integer,
  items_exists boolean NOT NULL,
  soft_guid character varying,
  group_id integer NOT NULL,
  group_name character varying,
  CONSTRAINT vw_update_soft_pkey PRIMARY KEY (soft_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_update_soft OWNER TO pavlov;

--------------------
-- DROP TABLE und.vw_asna_user;

CREATE TABLE und.vw_asna_user
(
  asna_user_id integer NOT NULL,
  workplace_id integer NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  asna_store_id character varying,
  asna_pwd character varying,
  asna_legal_id character varying,
  asna_rr_id integer,
  auth character varying,
  auth_date_beg timestamp without time zone,
  auth_date_end timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  is_active_str character varying,
  sch_full_schedule_id integer,
  sch_full_is_active boolean,
  sch_full_interval_type_id integer,
  sch_full_start_time_type_id integer,
  sch_full_end_time_type_id integer,
  sch_full_is_day1 boolean,
  sch_full_is_day2 boolean,
  sch_full_is_day3 boolean,
  sch_full_is_day4 boolean,
  sch_full_is_day5 boolean,
  sch_full_is_day6 boolean,
  sch_full_is_day7 boolean,
  sch_ch_schedule_id integer,
  sch_ch_is_active boolean,
  sch_ch_interval_type_id integer,
  sch_ch_start_time_type_id integer,
  sch_ch_end_time_type_id integer,
  sch_ch_is_day1 boolean,
  sch_ch_is_day2 boolean,
  sch_ch_is_day3 boolean,
  sch_ch_is_day4 boolean,
  sch_ch_is_day5 boolean,
  sch_ch_is_day6 boolean,
  sch_ch_is_day7 boolean,  
  sch_full_start_time_type_value integer,
  sch_full_end_time_type_value integer,
  sch_full_interval_type_value integer,
  sch_ch_start_time_type_value integer,
  sch_ch_end_time_type_value integer,
  sch_ch_interval_type_value integer,
  sales_tax_system_type_id integer,
  sales_region_zone_id integer,
  sales_region_id integer,
  client_tax_system_type_id integer,
  client_region_zone_id integer,
  client_region_id integer,
  user_id integer,
  user_name character varying,
  CONSTRAINT vw_asna_user_pkey PRIMARY KEY (asna_user_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_user OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_client;

CREATE TABLE und.vw_asna_client
(
  client_id integer NOT NULL,
  client_name character varying,
  CONSTRAINT vw_asna_client_pkey PRIMARY KEY (client_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_client OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_wa_task;

CREATE TABLE und.vw_wa_task
(
  task_id integer NOT NULL,
  request_type_id integer NOT NULL,
  task_state_id integer NOT NULL DEFAULT 1,
  client_id integer,
  sales_id integer,
  workplace_id integer,
  ord integer NOT NULL DEFAULT 0,
  ord_client integer NOT NULL DEFAULT 0,
  state_date timestamp without time zone,
  state_user character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,  
  is_auto_created boolean NOT NULL DEFAULT false,
  mess character varying,
  request_type_name character varying,
  request_type_group1_id integer,
  request_type_group2_id integer,
  request_type_group1_name character varying,
  request_type_group2_name character varying,
  task_state_name character varying,
  client_name character varying,
  sales_name character varying,
  workplace_name character varying,
  is_auto_created_str character varying,
  is_inner boolean NOT NULL DEFAULT false,
  is_outer boolean NOT NULL DEFAULT false,
  handling_server_cnt integer,
  handling_server_max_cnt integer NOT NULL,
  CONSTRAINT vw_wa_task_pkey PRIMARY KEY (task_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_wa_task OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_user_log;

CREATE TABLE und.vw_asna_user_log
(
  log_id integer NOT NULL,
  asna_user_id integer NOT NULL,
  mess character varying,
  request_id bigint,
  chain_id integer,
  in_batch_id integer,
  out_batch_id integer,
  info_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  workplace_id integer NOT NULL,
  is_active boolean NOT NULL,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  is_active_str character varying,
  CONSTRAINT vw_asna_user_log_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_user_log OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_wa_task_lc

CREATE TABLE und.vw_wa_task_lc
(
  task_lc_id integer NOT NULL,
  task_id integer NOT NULL,
  task_state_id integer NOT NULL DEFAULT 1,
  state_date timestamp without time zone,
  state_user character varying,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL,
  del_date timestamp without time zone,
  del_user character varying,
  request_type_id integer NOT NULL,
  client_id integer,
  sales_id integer,
  workplace_id integer,
  ord integer NOT NULL,
  ord_client integer NOT NULL,
  is_auto_created boolean NOT NULL,
  request_type_name character varying,
  request_type_group1_id integer,
  request_type_group2_id integer,
  request_type_group1_name character varying,
  request_type_group2_name character varying,
  task_state_name character varying,  
  is_auto_created_str character varying,
  is_inner boolean NOT NULL,
  is_outer boolean NOT NULL,  
  CONSTRAINT vw_wa_task_lc_pkey PRIMARY KEY (task_lc_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_wa_task_lc OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_stock_row;

CREATE TABLE und.vw_asna_stock_row
(
  row_id integer NOT NULL,
  asna_user_id integer NOT NULL,
  in_batch_id integer,
  out_batch_id integer,
  artikul integer NOT NULL,
  row_stock_id integer,
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  barcode character varying,
  asna_prt_id character varying,  
  asna_sku character varying,
  asna_qnt numeric,
  asna_doc character varying,
  asna_sup_inn character varying,
  asna_sup_id character varying,
  asna_sup_date timestamp without time zone,
  asna_nds integer,
  asna_gnvls boolean NOT NULL DEFAULT false,
  asna_dp boolean NOT NULL DEFAULT false,
  asna_man character varying,
  asna_series character varying,
  asna_exp timestamp without time zone,
  asna_prc_man numeric,
  asna_prc_opt numeric,
  asna_prc_opt_nds numeric,
  asna_prc_ret numeric,
  asna_prc_reestr numeric,
  asna_prc_gnvls_max numeric,
  asna_sum_opt numeric,
  asna_sum_opt_nds numeric,
  asna_mr_gnvls numeric,
  asna_tag integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  asna_store_id character varying,  
  asna_legal_id character varying,
  asna_rr_id integer,
  workplace_id integer NOT NULL,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  chain_id integer,
  asna_nnt integer,
  asna_name character varying,
  is_linked boolean NOT NULL DEFAULT false,
  is_unique boolean,
  CONSTRAINT vw_asna_stock_row_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_stock_row OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_stock_row_linked;

CREATE TABLE und.vw_asna_stock_row_linked
(
  row_id integer NOT NULL,
  asna_prt_id character varying,  
  asna_sku character varying,
  asna_qnt numeric,
  asna_doc character varying,
  asna_sup_inn character varying,
  asna_sup_id character varying,
  asna_sup_date timestamp without time zone,
  asna_nds integer,
  asna_gnvls boolean NOT NULL DEFAULT false,
  asna_dp boolean NOT NULL DEFAULT false,
  asna_man character varying,
  asna_series character varying,
  asna_exp timestamp without time zone,
  asna_prc_man numeric,
  asna_prc_opt numeric,
  asna_prc_opt_nds numeric,
  asna_prc_ret numeric,
  asna_prc_reestr numeric,
  asna_prc_gnvls_max numeric,
  asna_sum_opt numeric,
  asna_sum_opt_nds numeric,
  asna_mr_gnvls numeric,
  asna_tag integer,
  asna_nnt integer,
  chain_id integer,
  chain_stock_date timestamp without time zone,
  chain_is_full boolean,
  is_unique boolean NOT NULL,
  CONSTRAINT vw_asna_stock_row_linked_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_stock_row_linked OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_stock_chain;

CREATE TABLE und.vw_asna_stock_chain
(
  chain_id integer NOT NULL,
  asna_user_id integer NOT NULL,
  chain_num integer NOT NULL,
  stock_date timestamp without time zone,
  is_full boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  is_active boolean NOT NULL DEFAULT false,
  inactive_date timestamp without time zone,
  workplace_id integer NOT NULL,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  chain_name character varying,  
  CONSTRAINT vw_asna_stock_chain_pkey PRIMARY KEY (chain_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_stock_chain OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_stock_row_actual;

CREATE TABLE und.vw_asna_stock_row_actual
(
  row_id integer NOT NULL,
  asna_user_id integer NOT NULL,
  firm_name character varying,
  asna_prt_id character varying,
  asna_sku character varying,
  asna_qnt numeric,
  asna_doc character varying,
  asna_sup_inn character varying,
  asna_sup_id character varying,
  asna_sup_date timestamp without time zone,
  asna_nds integer,
  asna_gnvls boolean NOT NULL DEFAULT false,
  asna_dp boolean NOT NULL DEFAULT false,
  asna_man character varying,
  asna_series character varying,
  asna_exp timestamp without time zone,
  asna_prc_man numeric,
  asna_prc_opt numeric,
  asna_prc_opt_nds numeric,
  asna_prc_ret numeric,
  asna_prc_reestr numeric,
  asna_prc_gnvls_max numeric,
  asna_sum_opt numeric,
  asna_sum_opt_nds numeric,
  asna_mr_gnvls numeric,
  asna_tag integer,
  asna_stock_date timestamp without time zone,
  asna_load_date timestamp without time zone,
  asna_rv bytea,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  asna_store_id character varying,
  asna_legal_id character varying,
  asna_rr_id integer,
  workplace_id integer NOT NULL,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  asna_nnt integer,
  asna_name character varying,
  is_linked boolean NOT NULL DEFAULT false,  
  is_unique boolean,
  CONSTRAINT vw_asna_stock_row_actual_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_stock_row_actual OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_order;

CREATE TABLE und.vw_asna_order
(
  order_id integer NOT NULL,
  asna_user_id integer NOT NULL,
  issuer_asna_user_id integer NOT NULL,
  state_type_id integer,
  asna_order_id character varying,
  asna_src character varying,
  asna_num character varying,
  asna_date timestamp without time zone,
  asna_name character varying,
  asna_m_phone character varying,
  asna_pay_type character varying,
  asna_pay_type_id character varying,
  asna_d_card character varying,
  asna_ae smallint NOT NULL DEFAULT 0,
  asna_union_id character varying,
  asna_ts timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  workplace_id integer NOT NULL,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  issuer_workplace_id integer NOT NULL,
  issuer_workplace_name character varying,
  issuer_sales_id integer NOT NULL,
  issuer_sales_name character varying,
  issuer_client_id integer NOT NULL,
  issuer_client_name character varying,
  state_type_name character varying,  
  CONSTRAINT vw_asna_order_pkey PRIMARY KEY (order_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_order OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_order_row;

CREATE TABLE und.vw_asna_order_row
(
  row_id integer NOT NULL,
  order_id integer NOT NULL,
  state_type_id integer,
  asna_row_id character varying,
  asna_order_id character varying,
  asna_row_type smallint NOT NULL DEFAULT 0,
  asna_prt_id character varying,
  asna_nnt integer NOT NULL,
  asna_qnt numeric NOT NULL,
  asna_prc numeric NOT NULL,
  asna_prc_dsc numeric,
  asna_dsc_union character varying,
  asna_dtn numeric,
  asna_prc_loyal numeric,
  asna_prc_opt_nds numeric,
  asna_supp_inn character varying,
  asna_dlv_date timestamp without time zone,
  asna_qnt_unrsv numeric,
  asna_prc_fix numeric,
  asna_ts timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  state_type_name character varying,
  asna_sku character varying,
  asna_name character varying,
  CONSTRAINT vw_asna_order_row_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_order_row OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_order_row_state;

CREATE TABLE und.vw_asna_order_row_state
(
  row_state_id integer NOT NULL,
  row_id integer NOT NULL,
  state_type_id integer NOT NULL,
  asna_status_id character varying,
  asna_order_id character varying,
  asna_row_id character varying,
  asna_date timestamp without time zone,
  asna_rc_date timestamp without time zone,
  asna_cmnt character varying,
  asna_ts timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  state_type_name character varying,
  CONSTRAINT vw_asna_order_row_state_pkey PRIMARY KEY (row_state_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_order_row_state OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_order_state;

CREATE TABLE und.vw_asna_order_state
(
  order_state_id integer NOT NULL,
  order_id integer NOT NULL,
  state_type_id integer NOT NULL,
  asna_status_id character varying,
  asna_order_id character varying,
  asna_date timestamp without time zone,
  asna_rc_date timestamp without time zone,
  asna_cmnt character varying,
  asna_ts timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  state_type_name character varying,
  CONSTRAINT vw_asna_order_state_pkey PRIMARY KEY (order_state_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_order_state OWNER TO pavlov;
--------------------
--------------------
-- DROP TABLE und.vw_asna_price_row;

CREATE TABLE und.vw_asna_price_row
(
  row_id integer NOT NULL,
  asna_user_id integer NOT NULL,
  in_batch_id integer,
  out_batch_id integer,
  artikul integer NOT NULL,
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  barcode character varying,
  asna_sku character varying,
  asna_sup_inn character varying,
  asna_prc_opt_nds numeric,
  asna_prc_gnvls_max numeric,
  asna_status integer NOT NULL,
  asna_exp timestamp without time zone,
  asna_sup_date timestamp without time zone,
  asna_prcl_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  asna_store_id character varying,  
  asna_legal_id character varying,
  asna_rr_id integer,
  workplace_id integer NOT NULL,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  chain_id integer,
  asna_nnt integer,
  asna_name character varying,
  is_linked boolean NOT NULL DEFAULT false,
  is_unique boolean,
  CONSTRAINT vw_asna_price_row_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_price_row OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_price_chain;

CREATE TABLE und.vw_asna_price_chain
(
  chain_id integer NOT NULL,
  asna_user_id integer NOT NULL,
  chain_num integer NOT NULL,
  price_date timestamp without time zone,
  is_full boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  is_active boolean NOT NULL DEFAULT false,
  inactive_date timestamp without time zone,
  workplace_id integer NOT NULL,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  chain_name character varying,  
  CONSTRAINT vw_asna_price_chain_pkey PRIMARY KEY (chain_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_price_chain OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_price_row_actual;

CREATE TABLE und.vw_asna_price_row_actual
(
  row_id integer NOT NULL,
  asna_user_id integer NOT NULL,
  firm_name character varying,
  asna_sku character varying,
  asna_sup_inn character varying,
  asna_prc_opt_nds numeric,
  asna_prc_gnvls_max numeric,
  asna_status integer NOT NULL,
  asna_exp timestamp without time zone,
  asna_sup_date timestamp without time zone,
  asna_prcl_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  asna_store_id character varying,
  asna_legal_id character varying,
  asna_rr_id integer,
  workplace_id integer NOT NULL,
  workplace_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  client_id integer NOT NULL,
  client_name character varying,
  asna_nnt integer,
  asna_name character varying,
  is_linked boolean NOT NULL DEFAULT false,  
  is_unique boolean,
  CONSTRAINT vw_asna_price_row_actual_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_price_row_actual OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_asna_price_row_linked;

CREATE TABLE und.vw_asna_price_row_linked
(
  row_id integer NOT NULL,
  asna_sku character varying,
  asna_sup_inn character varying,
  asna_prc_opt_nds numeric,
  asna_prc_gnvls_max numeric,
  asna_status integer NOT NULL,
  asna_exp timestamp without time zone,
  asna_sup_date timestamp without time zone,
  asna_prcl_date timestamp without time zone,
  asna_nnt integer,
  chain_id integer,
  chain_price_date timestamp without time zone,
  chain_is_full boolean,
  is_unique boolean NOT NULL,
  CONSTRAINT vw_asna_price_row_linked_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_asna_price_row_linked OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_region;

CREATE TABLE cabinet.vw_region
(
  region_id integer NOT NULL,
  region_name character varying,
  is_deleted smallint,
  is_price_limit_exists boolean NOT NULL,
  CONSTRAINT vw_region_pkey PRIMARY KEY (region_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_region OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_region_zone;

CREATE TABLE cabinet.vw_region_zone
(
  zone_id integer NOT NULL,
  zone_name character varying,
  region_id integer NOT NULL,
  region_name character varying,
  is_deleted smallint,
  is_price_limit_exists boolean NOT NULL,
  CONSTRAINT vw_region_zone_pkey PRIMARY KEY (zone_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_region_zone OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_region_price_limit;

CREATE TABLE cabinet.vw_region_price_limit
(
  item_id integer NOT NULL,
  region_id integer NOT NULL,
  limit_type_id integer NOT NULL,
  limit_type_name character varying,
  percent_value numeric,
  is_price_limit_exists boolean NOT NULL,
  CONSTRAINT vw_region_price_limit_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_region_price_limit OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_region_zone_price_limit;

CREATE TABLE cabinet.vw_region_zone_price_limit
(
  item_id integer NOT NULL,
  zone_id integer NOT NULL,
  limit_type_id integer NOT NULL,
  limit_type_name character varying,
  percent_value numeric,
  is_price_limit_exists boolean NOT NULL,
  CONSTRAINT vw_region_zone_price_limit_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_region_zone_price_limit OWNER TO pavlov;
--------------------
-- DROP TABLE discount.vw_programm_step_param;

CREATE TABLE discount.vw_programm_step_param
(
  param_id bigint NOT NULL,
  programm_id bigint NOT NULL,
  step_id bigint NOT NULL,
  card_param_type integer,
  trans_param_type integer,
  programm_param_id bigint,
  prev_step_param_id bigint,
  param_num integer,
  param_name character varying,
  is_programm_output smallint NOT NULL DEFAULT 0,
  param_type_id integer,
  step_num integer NOT NULL,
  business_id bigint,
  param_type_name character varying,
  card_param_type_name character varying,
  trans_param_type_name character varying,
  programm_param_name character varying,
  prev_step_param_name character varying,
  is_programm_output_chb boolean NOT NULL,
  curr_param_name character varying,
  param_id_str character varying,
  programm_id_str character varying,
  step_id_str character varying,
  programm_param_id_str character varying,
  prev_step_param_id_str character varying,
  business_id_str character varying,  
  CONSTRAINT vw_programm_step_param_pkey PRIMARY KEY (param_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_step_param OWNER TO pavlov;
--------------------
-- DROP TABLE discount.vw_programm_step_condition;

CREATE TABLE discount.vw_programm_step_condition
(
  condition_id bigint NOT NULL,
  programm_id bigint NOT NULL,
  step_id bigint NOT NULL,
  condition_num integer,
  op_type integer,
  op1_param_id bigint,
  op2_param_id bigint,
  condition_name character varying,
  step_num bigint NOT NULL,
  business_id bigint,
  condition_type_id integer,
  condition_type_name character varying,
  op1_param_name character varying,
  op2_param_name character varying,
  condition_id_str character varying,
  programm_id_str character varying,
  step_id_str character varying,
  op1_param_id_str character varying,
  op2_param_id_str character varying,
  business_id_str character varying,
  CONSTRAINT vw_programm_step_condition_pkey PRIMARY KEY (condition_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_programm_step_condition OWNER TO pavlov;
--------------------
-- DROP TABLE discount.vw_programm_step_logic;

CREATE TABLE discount.vw_programm_step_logic
(
  logic_id bigint NOT NULL,
  step_id bigint NOT NULL,
  programm_id bigint NOT NULL,
  logic_num integer,
  op_type integer NOT NULL,
  op1_param_id bigint,
  op2_param_id bigint,
  op3_param_id bigint,
  op4_param_id bigint,
  condition_id bigint,
  step_num bigint NOT NULL,
  business_id bigint,
  logic_type_id integer NOT NULL,
  logic_type_name character varying,
  op1_param_name character varying,
  op2_param_name character varying,
  op3_param_name character varying,
  op4_param_name character varying,
  condition_name character varying,
  logic_id_str character varying,
  programm_id_str character varying,
  step_id_str character varying,
  condition_id_str character varying,
  op1_param_id_str character varying,
  op2_param_id_str character varying,
  op3_param_id_str character varying,
  op4_param_id_str character varying,
  business_id_str character varying,  
  CONSTRAINT vw_programm_step_logic_pkey PRIMARY KEY (logic_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.vw_programm_step_logic OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_user_group_item;

CREATE TABLE cabinet.vw_prj_user_group_item
(
  item_id integer NOT NULL,
  group_id integer NOT NULL,
  item_type_id integer NOT NULL,
  claim_id integer,
  event_id integer,
  note_id integer,
  ord integer NOT NULL,
  task_id integer,
  user_id integer NOT NULL,
  group_name character varying,  
  fixed_kind integer,
  item_type_name character varying,
  item_num character varying,
  item_name character varying,
  item_text character varying,
  item_state_type_id integer,
  item_state_type_name character varying,
  item_date_beg timestamp without time zone,
  item_date_end timestamp without time zone,
  ord_prev integer,
  group_id_prev integer,  
  CONSTRAINT vw_prj_user_group_item_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_user_group_item OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_note;

CREATE TABLE cabinet.vw_prj_note
(
  note_id integer NOT NULL,
  note_name character varying,
  note_text character varying,
  note_num character varying,
  CONSTRAINT vw_prj_note_pkey PRIMARY KEY (note_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_note OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_client_report1;

CREATE TABLE cabinet.vw_client_report1
(
  id integer NOT NULL,
  client_name character varying,
  sales_count character varying,
  region_name character varying,
  city_name character varying,
  version_min character varying,
  version_max character varying,
  date_reg_min date,
  date_reg_max date,
  sales_name character varying,
  kind integer NOT NULL,
  CONSTRAINT vw_client_report1_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_report1 OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_client_min_version;

CREATE TABLE cabinet.vw_client_min_version
(
  adr integer NOT NULL,
  id integer,
  name text,
  inn text,
  kpp text,
  sales_id integer,
  adress text,
  workplace_id integer,
  description text,
  registration_key text,
  service_name text,
  version_name text,
  CONSTRAINT vw_client_min_version_pkey PRIMARY KEY (adr)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_client_min_version OWNER TO pavlov;
--------------------
-- DROP TABLE esn.vw_medical;

CREATE TABLE esn.vw_medical
(
  medical_id bigint NOT NULL,
  document_id bigint,
  publish_date timestamp without time zone,
  medical_name character varying,
  reg_num character varying,
  reg_date timestamp without time zone,
  producer character varying,
  add_document_id bigint,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  document_name character varying,
  document_num character varying,
  document_link character varying,
  document_type_id integer,
  document_accept_date timestamp without time zone,
  document_create_date timestamp without time zone,
  document_type_name character varying,
  add_document_name character varying,
  add_document_num character varying,
  add_document_link character varying,
  add_document_type_id integer,
  add_document_accept_date timestamp without time zone,
  add_document_create_date timestamp without time zone,
  add_document_type_name character varying,
  CONSTRAINT vw_medical_pkey PRIMARY KEY (medical_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE esn.vw_medical OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_task_user_state;

CREATE TABLE cabinet.vw_prj_task_user_state
(
  item_id integer NOT NULL,
  user_id integer NOT NULL,
  task_id integer NOT NULL,
  state_type_id integer NOT NULL,
  from_user_id integer,
  is_current boolean NOT NULL DEFAULT false,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  user_name character varying,
  task_num integer NOT NULL,
  claim_id integer NOT NULL,
  state_type_name character varying,
  from_user_name character varying,
  CONSTRAINT vw_prj_task_user_state_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_task_user_state OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_service_esn2;

CREATE TABLE cabinet.vw_service_esn2
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  comp_name character varying,
  registration_key character varying,
  sales_id integer NOT NULL,
  client_id integer NOT NULL,
  sales_name character varying,
  client_name character varying,  
  workplace_type integer,
  CONSTRAINT vw_service_esn2_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_service_esn2 OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_version_main;

CREATE TABLE cabinet.vw_version_main
(
  version_main_id integer NOT NULL,
  version_main_name text, 
  project_id integer,
  version_type integer NOT NULL,
  CONSTRAINT vw_version_main_pkey PRIMARY KEY (version_main_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_version_main OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_project_version;

CREATE TABLE cabinet.vw_prj_project_version
(
  version_id integer NOT NULL,
  project_id integer NOT NULL,
  version_type_id integer NOT NULL,
  version_name character varying,
  version_value character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  project_name character varying,
  version_type_name character varying,
  CONSTRAINT vw_prj_project_version_pkey PRIMARY KEY (version_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_project_version OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_claim_filter_item;

CREATE TABLE cabinet.vw_prj_claim_filter_item
(
  field_id integer NOT NULL,
  field_name character varying,
  field_name_rus character varying,
  field_type_id integer NOT NULL,
  ord integer NOT NULL,
  cache_id integer,
  field_type_name character varying,
  field_type_name_rus character varying,
  is_field_in_filter boolean NOT NULL,
  filter_id integer,
  op_type_id integer,
  field_id_value character varying,
  field_name_value character varying,
  is_value_fixed boolean,
  filter_name character varying,      
  user_id integer,
  is_active boolean,
  user_name character varying,
  op_type_name character varying,
  op_type_name_rus character varying,
  op_type_symbol character varying,  
  cache_name character varying,
  cache_value_field character varying,
  cache_text_field character varying,
  CONSTRAINT vw_prj_claim_filter_item_pkey PRIMARY KEY (field_id)  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_claim_filter_item OWNER TO pavlov;
--------------------
-- DROP TABLE cabinet.vw_prj_report1;

CREATE TABLE cabinet.vw_prj_report1
(
  id integer NOT NULL,
  gr integer NOT NULL,
  gr_name text,
  task_num text,
  active_first_exec_user_name text,
  done_last_date_end timestamp without time zone,
  gr_cnt integer,
  done_cnt integer,
  cnt integer,
  CONSTRAINT vw_prj_report1_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.vw_prj_report1 OWNER TO pavlov;
--------------------
-- DROP TABLE und.vw_stock_log;

CREATE TABLE und.vw_stock_log
(
  client_id integer NOT NULL,
  client_name character varying,
  sales_id integer NOT NULL,
  sales_name character varying,
  last_upl_date timestamp without time zone,
  last_downl_date timestamp without time zone,
  last_del_date timestamp without time zone,
  CONSTRAINT vw_stock_log_pkey PRIMARY KEY (sales_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_stock_log OWNER TO pavlov;
--------------------