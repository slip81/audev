﻿
/*
delete from discount.card_nominal_uncommitted t1 using discount.card_transaction t2
where t1.card_trans_id = t2.card_trans_id
and t2.status = 1
and t2.date_beg <= '2016-10-26 00:01';
*/

select count(t1.*)
--select max(t2.date_beg)
from discount.card_nominal_uncommitted t1 
inner join discount.card_transaction t2 on t1.card_trans_id = t2.card_trans_id
where t2.status = 1
--and t2.date_beg <= current_timestamp - interval '19 hours';
and t2.date_beg <= '2016-10-25 00:01';
-- 143943

select count(t1.*)
from discount.programm_step_result t1
inner join discount.card_transaction t2 on t1.result_id = t2.programm_result_id
where t2.programm_result_id is not null
and t2.status = 1
and t2.date_beg <= '2016-10-25 00:01';

select count(t1.*)
from discount.programm_result_detail t1
inner join discount.card_transaction t2 on t1.result_id = t2.programm_result_id
where t2.programm_result_id is not null
and t2.status = 1
and t2.date_beg <= '2016-10-25 00:01';

select count(t1.*)
from discount.programm_result t1
inner join discount.card_transaction t2 on t1.result_id = t2.programm_result_id
where t2.programm_result_id is not null
and t2.status = 1
and t2.date_beg <= '2016-10-25 00:01';

/*
delete from discount.card_transaction_detail t1 using discount.card_transaction t2
where t1.card_trans_id = t2.card_trans_id
and t2.status = 1
and t2.date_beg <= '2016-10-26 00:01';
*/

select count(t1.*) 
from discount.card_transaction_detail t1
inner join discount.card_transaction t2 on t1.card_trans_id = t2.card_trans_id
where t2.status = 1
and t2.date_beg <= '2016-10-25 00:01';
-- 327586

/*
delete from discount.card_transaction_unit t1 using discount.card_transaction t2
where t1.card_trans_id = t2.card_trans_id
and t2.status = 1
and t2.date_beg <= '2016-10-26 00:01';
*/
	
select count(t1.*) 
from discount.card_transaction_unit t1
inner join discount.card_transaction t2 on t1.card_trans_id = t2.card_trans_id
where t2.status = 1
and t2.date_beg <= '2016-10-25 00:01';
-- 109052

/*
delete from discount.card_transaction
where card_trans_id in (
select card_trans_id
from discount.card_transaction
where status = 1
and date_beg >= '2016-10-25 00:01'
and date_beg <= '2016-10-26 00:01'
order by card_trans_id
limit 4000);
*/
	
select count(*)
from discount.card_transaction t1
where t1.status = 1
and date_beg >= '2016-10-25 00:01'
and date_beg <= '2016-10-26 00:01'
;
-- 3717

select min(t1.date_beg)
from discount.card_transaction t1
where t1.status = 1
--and t1.date_beg <= current_timestamp - interval '19 hours';
and t1.date_beg <= '2016-10-25 00:01';
-- "2016-08-31 23:07:39.762664"

