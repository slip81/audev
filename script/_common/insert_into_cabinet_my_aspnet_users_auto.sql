﻿/*
	insert into cabinet.my_aspnet_users auto_
*/

select * from cabinet.sales where coalesce(user_id, 0) <= 0 order by user_id;

select * from cabinet.my_aspnet_users where name like 'auto_%';

insert into cabinet.my_aspnet_users (id, "applicationId", name, "isAnonymous")
select (row_number() over (order by id) + (select max(id) from cabinet.my_aspnet_users)), 1, 'auto_' || id::text, 0
from cabinet.sales where coalesce(user_id, 0) <= 0;

update cabinet.sales set user_id = (select x1.id from cabinet.my_aspnet_users x1 where x1.name = ('auto_' || cabinet.sales.id::text) )
where coalesce(user_id, 0) <= 0;