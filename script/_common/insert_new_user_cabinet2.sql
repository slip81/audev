﻿
select * from cabinet.cab_user order by user_id desc
-- 24
-- 1

-- update cabinet.cab_user set crm_user_role_id = 1 where user_id = 24

select * from cabinet.cab_grid_user_settings where user_id in (1, 24)

/*
insert into cabinet.cab_grid_user_settings (grid_id, user_id, sort_options, filter_options, back_color_column_id, page_size, columns_options)
select grid_id, 24, sort_options, filter_options, back_color_column_id, page_size, columns_options
from cabinet.cab_grid_user_settings where user_id = 1
*/

select * from cabinet.cab_grid_column_user_settings where user_id in (1, 24)

/*
insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
select column_id, 24, column_num, is_visible, width, is_filterable, is_sortable
from cabinet.cab_grid_column_user_settings
where user_id = 1
*/

select * from cabinet.crm_priority_user_settings where user_id in (1, 24)

/*
insert into cabinet.crm_priority_user_settings (priority_id, user_id, fore_color)
select priority_id, 24, fore_color
from cabinet.crm_priority_user_settings 
where user_id = 1;
*/

select * from cabinet.crm_project_user_settings where user_id in (1, 24)

/*
insert into cabinet.crm_project_user_settings (project_id, user_id, fore_color)
select project_id, 24, fore_color
from cabinet.crm_project_user_settings 
where user_id = 1;
*/

select * from cabinet.crm_state_user_settings where user_id in (1, 24)

/*
insert into cabinet.crm_state_user_settings (state_id, user_id, fore_color)
select state_id, 24, fore_color
from cabinet.crm_state_user_settings 
where user_id = 1;
*/


select * from cabinet.crm_task_user_filter where user_id in (1, 24)

/*
insert into cabinet.crm_task_user_filter (user_id, grid_id, raw_filter, curr_group_id)
select 24, grid_id, raw_filter, curr_group_id
from cabinet.crm_task_user_filter
where user_id = 1;
*/

select * from cabinet.client_user where client_id = 1000 order by user_id desc

select * from cabinet.my_aspnet_users where id = 7176
select * from cabinet.my_aspnet_membership where "userId" = 7176

select * from cabinet.my_aspnet_users where id = 124
select * from cabinet.my_aspnet_membership where "userId" = 124


select * from cabinet.cab_user where user_id in (1, 24)

select * from cabinet.cab_user_role where user_name in ('pavlov')


insert into cabinet.my_aspnet_users (id, "applicationId", name, "isAnonymous", "lastActivityDate")
select 124, "applicationId", name, "isAnonymous", "lastActivityDate"
from cabinet.my_aspnet_users
where id = 24

insert into cabinet.my_aspnet_membership ("userId", "Password", "PasswordKey", "PasswordFormat", "IsApproved")
select 124, "Password", "PasswordKey", "PasswordFormat", "IsApproved"
from cabinet.my_aspnet_membership
where "userId" = 24

update cabinet.client_user set user_id = 124 where user_id = 24

delete from  cabinet.my_aspnet_users where id = 24
delete from  cabinet.my_aspnet_membership where "userId" = 24

delete from cabinet.client_user where user_id = 7176

update cabinet.my_aspnet_users set id = 24 where id = 7176
update cabinet.my_aspnet_membership set "userId" = 24 where "userId" = 7176

-- delete from cabinet.client_user where user_id = 24;

insert into cabinet.client_user (user_id, client_id, user_name, user_name_full, is_main_for_sales, need_change_pwd)
values (24, 1000, 'РАМ', 'Русяева Анна Михайловна', false, false)

select * from cabinet.client_user where client_id = 1000 order by user_id desc
