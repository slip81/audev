﻿
ALTER TABLE und.stock_batch ADD COLUMN is_confirmed boolean NOT NULL DEFAULT false;
ALTER TABLE und.stock_batch ADD COLUMN confirm_date timestamp without time zone;

UPDATE und.stock_batch SET is_confirmed = true, confirm_date = crt_date WHERE is_deleted != true;


ALTER TABLE und.stock_sales_download ADD COLUMN is_confirmed boolean NOT NULL DEFAULT false;
ALTER TABLE und.stock_sales_download ADD COLUMN confirm_date timestamp without time zone;

UPDATE und.stock_sales_download SET is_confirmed = true, confirm_date = crt_date WHERE is_deleted != true;

-- DROP VIEW und.vw_stock_batch;

CREATE OR REPLACE VIEW und.vw_stock_batch AS 
 SELECT t1.batch_id,
    t1.stock_id,
    t1.batch_num,
    t1.part_cnt,
    t1.part_num,
    t1.row_cnt,
    t1.is_active,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.client_id,
    t2.sales_id,
    t2.depart_id,
    t2.depart_name,
    t2.pharmacy_name,
    t3.name AS client_name,
    t4.adress AS sales_name,
    t1.inactive_date,
    t1.is_confirmed,
    t1.confirm_date
   FROM und.stock_batch t1
     JOIN und.stock t2 ON t1.stock_id = t2.stock_id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
     JOIN cabinet.sales t4 ON t2.sales_id = t4.id
  WHERE t1.is_deleted = false;

ALTER TABLE und.vw_stock_batch OWNER TO pavlov;

-- DROP VIEW und.vw_stock_row;

CREATE OR REPLACE VIEW und.vw_stock_row AS 
 SELECT t1.row_id,
    t1.stock_id,
    t1.batch_id,
    t1.artikul,
    t1.row_stock_id,
    t1.esn_id,
    t1.prep_id,
    t1.prep_name,
    t1.firm_id,
    t1.firm_name,
    t1.country_name,
    t1.unpacked_cnt,
    t1.all_cnt,
    t1.price,
    t1.valid_date,
    t1.series,
    t1.is_vital,
    t1.barcode,
    t1.price_firm,
    t1.price_gr,
    t1.percent_gross,
    t1.price_gross,
    t1.sum_gross,
    t1.percent_nds_gross,
    t1.price_nds_gross,
    t1.sum_nds_gross,
    t1.percent,
    t1.sum,
    t1.supplier_name,
    t1.supplier_doc_num,
    t1.gr_date,
    t1.farm_group_name,
    t1.supplier_doc_date,
    t1.profit,
    t1.mess,
    t1.state,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.client_id,
    t2.sales_id,
    t2.depart_id,
    t2.depart_name,
    t2.pharmacy_name,
    t3.batch_num,
    t3.part_cnt,
    t3.part_num,
    t3.row_cnt,
    t3.crt_date AS batch_crt_date,
    t4.name AS client_name,
    t5.adress AS sales_name,
    t2.depart_address,
    t3.is_active,
    t3.inactive_date AS batch_inactive_date,
    t3.is_confirmed AS batch_is_confirmed,
    t3.confirm_date AS batch_confirm_date
   FROM und.stock_row t1
     JOIN und.stock t2 ON t1.stock_id = t2.stock_id
     JOIN und.stock_batch t3 ON t1.batch_id = t3.batch_id
     JOIN cabinet.client t4 ON t2.client_id = t4.id
     JOIN cabinet.sales t5 ON t2.sales_id = t5.id;

ALTER TABLE und.vw_stock_row OWNER TO pavlov;

-- DROP VIEW und.vw_stock_sales_download;

CREATE OR REPLACE VIEW und.vw_stock_sales_download AS 
 SELECT t1.download_id,
    t1.sales_id,
    t1.part_cnt,
    t1.part_num,
    t1.row_cnt,
    t1.is_active,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.download_batch_date_beg,
    t1.download_batch_date_end,
    t1.batch_row_cnt,
    t2.adress AS sales_name,
    t3.id AS client_id,
    t3.name AS client_name,
    t1.skip_row_cnt,
    t1.new_row_cnt,
    t1.force_all,
    t1.inactive_date,
    t1.is_confirmed,
    t1.confirm_date    
   FROM und.stock_sales_download t1
     JOIN cabinet.sales t2 ON t1.sales_id = t2.id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
  WHERE t1.is_deleted = false;

ALTER TABLE und.vw_stock_sales_download OWNER TO pavlov;

