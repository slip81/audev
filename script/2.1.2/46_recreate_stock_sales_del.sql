﻿DROP FUNCTION und.get_stock_del_list(integer, timestamp without time zone);

DROP FUNCTION und.del_stock(integer, integer);

DROP TABLE und.stock_sales_del;

--------------------------------------------------

CREATE TABLE und.stock_sales_del
(
  del_id serial NOT NULL,
  source_sales_id integer NOT NULL,
  sales_id integer NOT NULL,
  row_cnt bigint,
  is_received boolean NOT NULL DEFAULT false,
  received_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT stock_sales_del_pkey PRIMARY KEY (del_id),
  CONSTRAINT stock_sales_del_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE und.stock_sales_del
  OWNER TO pavlov;

-- DROP TRIGGER und_stock_sales_del_set_stamp_trg ON und.stock_sales_del;

CREATE TRIGGER und_stock_sales_del_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_sales_del
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

--------------------------------------------------

CREATE OR REPLACE FUNCTION und.del_stock(
    IN _sales_id integer,
    IN _depart_id integer,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
 _client_id integer;
BEGIN

result:= 0;	

WITH stock_row_before_delete_cte AS (
select distinct batch_id
from und.stock_row t1
inner join und.stock t2 on t1.stock_id = t2.stock_id
where 1=1
and t1.state != 1
and t2.sales_id = _sales_id
and (((_depart_id is not null) and (t2.depart_id = _depart_id)) or (_depart_id is null))
)
update und.stock_batch t1
set is_confirmed = true, confirm_date = current_timestamp
from stock_row_before_delete_cte t2
where t1.batch_id = t2.batch_id;

WITH stock_row_delete_cte AS (
update und.stock_row t1
set state = 1, upd_date = current_timestamp, upd_user = 'del_stock'
from und.stock t2
where t1.stock_id = t2.stock_id
and t1.state != 1
and t2.sales_id = _sales_id
and (((_depart_id is not null) and (t2.depart_id = _depart_id)) or (_depart_id is null))
RETURNING 1
) SELECT count(*) FROM stock_row_delete_cte INTO result;

_client_id := (select client_id from und.stock where sales_id = _sales_id limit 1);

INSERT INTO und.stock_sales_del (source_sales_id, sales_id, row_cnt, is_received, crt_date, crt_user, upd_date, upd_user)
--VALUES (_sales_id, result, current_timestamp, 'delstock', current_timestamp, 'delstock')
SELECT DISTINCT _sales_id, t1.sales_id, result, false, current_timestamp, 'delstock', current_timestamp, 'delstock'
FROM und.stock t1
WHERE t1.client_id = _client_id AND t1.sales_id != _sales_id;

/*
WITH deleted AS (	
	delete from und.stock_row t1 using und.stock t2
	where t1.stock_id = t2.stock_id
	and t2.sales_id = _sales_id
	and (((_depart_id is not null) and (t2.depart_id = _depart_id)) or (_depart_id is null))
	RETURNING *
) SELECT count(*) FROM deleted INTO result;
*/

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.del_stock(integer, integer)  OWNER TO pavlov;
--------------------------------------------------

CREATE OR REPLACE FUNCTION und.get_stock_del_list(
    IN _sales_id integer,
    IN _date_cut timestamp without time zone)
  RETURNS SETOF integer AS
$BODY$
DECLARE
 _client_id integer;
BEGIN
	_client_id := (select client_id from und.stock where sales_id = _sales_id limit 1);

	/*
	RETURN QUERY
	select distinct t1.stock_id
	from und.stock t1
	inner join und.stock_sales_del t2 on t1.sales_id = t2.source_sales_id
	where t1.client_id = _client_id and t1.sales_id != _sales_id
	and t2.is_received = false
	and t2.crt_date > _download_batch_date_end
	;
	*/

	DROP TABLE IF EXISTS tmp_get_stock_del_list;

	CREATE TEMP TABLE tmp_get_stock_del_list(
	    sales_id integer NOT NULL,
	    stock_id integer NOT NULL
	);	

	INSERT INTO tmp_get_stock_del_list (sales_id, stock_id)
	select distinct t1.sales_id, t2.stock_id
	from und.stock_sales_del t1
	inner join und.stock t2 on t1.source_sales_id = t2.sales_id	
	where 1=1
	-- and t2.sales_id = _sales_id
	and t1.sales_id = _sales_id
	and t1.is_received = false
	-- and t1.crt_date > _date_cut
	;

	UPDATE und.stock_sales_del t1
	SET is_received = true, received_date = current_timestamp
	FROM tmp_get_stock_del_list t2
	WHERE t1.sales_id = t2.sales_id
	AND t1.is_received = false
	;
	
	RETURN QUERY
	select distinct t1.stock_id
	from tmp_get_stock_del_list t1
	order by t1.stock_id
	;

	DROP TABLE IF EXISTS tmp_get_stock_del_list;

	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION und.get_stock_del_list(integer, timestamp without time zone) OWNER TO pavlov;
