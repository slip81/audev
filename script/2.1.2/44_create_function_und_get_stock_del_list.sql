﻿/*
	CREATE OR REPLACE FUNCTION und.get_stock_del_list
*/

CREATE OR REPLACE FUNCTION und.get_stock_del_list(
    IN _sales_id integer,
    IN _download_batch_date_end timestamp without time zone
    )
  RETURNS TABLE(stock_id integer) AS
$BODY$
DECLARE
 _client_id integer;
BEGIN
	_client_id := (select client_id from und.stock where sales_id = _sales_id limit 1);

	RETURN QUERY
	select distinct t1.stock_id
	from und.stock t1
	inner join und.stock_sales_del t2 on t1.sales_id = t2.sales_id
	where t1.client_id = _client_id and t1.sales_id != _sales_id
	and t2.crt_date > _download_batch_date_end
	;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION und.get_stock_del_list(integer, timestamp without time zone) OWNER TO pavlov;
