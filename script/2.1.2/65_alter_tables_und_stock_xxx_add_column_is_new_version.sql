﻿/*
	ALTER TABLEs und.stock_xxx ADD COLUMN is_new_version
*/

ALTER TABLE und.stock_sales_download ADD COLUMN is_new_version boolean NOT NULL DEFAULT false;
ALTER TABLE und.stock_batch ADD COLUMN is_new_version boolean NOT NULL DEFAULT false;