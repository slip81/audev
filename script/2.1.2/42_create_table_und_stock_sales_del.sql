﻿/*
	CREATE TABLE und.stock_sales_del
*/

-- DROP TABLE und.stock_sales_del;

CREATE TABLE und.stock_sales_del
(
  del_id serial NOT NULL,
  sales_id integer NOT NULL,
  row_cnt bigint,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT stock_sales_del_pkey PRIMARY KEY (del_id),
  CONSTRAINT stock_sales_del_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_sales_del OWNER TO pavlov;

CREATE TRIGGER und_stock_sales_del_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_sales_del
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

