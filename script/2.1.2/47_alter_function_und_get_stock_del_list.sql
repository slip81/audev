﻿
DROP FUNCTION und.get_stock_del_list(integer, timestamp without time zone);

CREATE OR REPLACE FUNCTION und.get_stock_del_list(
    _sales_id integer,
    _date_cut timestamp without time zone,
    _do_update integer)
  RETURNS SETOF integer AS
$BODY$
DECLARE
 _client_id integer;
BEGIN
	_client_id := (select client_id from und.stock where sales_id = _sales_id limit 1);

	/*
	RETURN QUERY
	select distinct t1.stock_id
	from und.stock t1
	inner join und.stock_sales_del t2 on t1.sales_id = t2.source_sales_id
	where t1.client_id = _client_id and t1.sales_id != _sales_id
	and t2.is_received = false
	and t2.crt_date > _download_batch_date_end
	;
	*/

	DROP TABLE IF EXISTS tmp_get_stock_del_list;

	CREATE TEMP TABLE tmp_get_stock_del_list(
	    sales_id integer NOT NULL,
	    stock_id integer NOT NULL
	);	

	INSERT INTO tmp_get_stock_del_list (sales_id, stock_id)
	select distinct t1.sales_id, t2.stock_id
	from und.stock_sales_del t1
	inner join und.stock t2 on t1.source_sales_id = t2.sales_id	
	where 1=1
	-- and t2.sales_id = _sales_id
	and t1.sales_id = _sales_id
	and t1.is_received = false
	-- and t1.crt_date > _date_cut
	;

	IF (_do_update = 1) THEN
		UPDATE und.stock_sales_del t1
		SET is_received = true, received_date = current_timestamp
		FROM tmp_get_stock_del_list t2
		WHERE t1.sales_id = t2.sales_id
		AND t1.is_received = false
		;
	END IF;
	
	RETURN QUERY
	select distinct t1.stock_id
	from tmp_get_stock_del_list t1
	order by t1.stock_id
	;

	DROP TABLE IF EXISTS tmp_get_stock_del_list;

	RETURN;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION und.get_stock_del_list(integer, timestamp without time zone, integer) OWNER TO pavlov;
