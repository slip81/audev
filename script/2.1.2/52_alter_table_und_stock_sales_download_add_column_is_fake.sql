﻿/*
	ALTER TABLE und.stock_sales_download ADD COLUMN is_fake
*/

ALTER TABLE und.stock_sales_download ADD COLUMN is_fake boolean NOT NULL DEFAULT false;
