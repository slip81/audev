﻿
-- DROP FUNCTION und.set_stock_last_batch(integer, integer, timestamp without time zone);

CREATE OR REPLACE FUNCTION und.set_stock_last_batch(
    _client_id integer,
    _last_batch_id integer,
    _last_batch_date timestamp without time zone)
  RETURNS void AS
$BODY$
BEGIN	
	update und.stock
	set last_batch_id = _last_batch_id, last_batch_date = _last_batch_date
	where client_id = _client_id
	and ((last_batch_date is null) or (last_batch_date <= _last_batch_date));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION und.set_stock_last_batch(integer, integer, timestamp without time zone) OWNER TO pavlov;
