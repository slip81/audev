﻿
-- DROP FUNCTION und.del_stock(integer, integer);

CREATE OR REPLACE FUNCTION und.del_stock(
    IN _sales_id integer,
    IN _depart_id integer,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN

result:= 0;	

WITH stock_row_before_delete_cte AS (
select distinct batch_id
from und.stock_row t1
inner join und.stock t2 on t1.stock_id = t2.stock_id
where 1=1
and t1.state != 1
and t2.sales_id = _sales_id
and (((_depart_id is not null) and (t2.depart_id = _depart_id)) or (_depart_id is null))
)
update und.stock_batch t1
set is_confirmed = true, confirm_date = current_timestamp
from stock_row_before_delete_cte t2
where t1.batch_id = t2.batch_id;

WITH stock_row_delete_cte AS (
update und.stock_row t1
set state = 1, upd_date = current_timestamp, upd_user = 'del_stock'
from und.stock t2
where t1.stock_id = t2.stock_id
and t1.state != 1
and t2.sales_id = _sales_id
and (((_depart_id is not null) and (t2.depart_id = _depart_id)) or (_depart_id is null))
RETURNING 1
) SELECT count(*) FROM stock_row_delete_cte INTO result;

INSERT INTO und.stock_sales_del (sales_id, row_cnt, crt_date, crt_user, upd_date, upd_user)
VALUES (_sales_id, result, current_timestamp, 'delstock', current_timestamp, 'delstock');

/*
WITH deleted AS (	
	delete from und.stock_row t1 using und.stock t2
	where t1.stock_id = t2.stock_id
	and t2.sales_id = _sales_id
	and (((_depart_id is not null) and (t2.depart_id = _depart_id)) or (_depart_id is null))
	RETURNING *
) SELECT count(*) FROM deleted INTO result;
*/

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.del_stock(integer, integer) OWNER TO pavlov;
