﻿-- DROP TABLE und.vw_stock_row_tmp;

CREATE TABLE und.vw_stock_row_tmp
(
  row_id serial NOT NULL,
  stock_id integer NOT NULL,
  receiver_id integer NOT NULL,
  batch_id integer,
  artikul integer NOT NULL,
  row_stock_id integer NOT NULL,
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  country_name character varying,
  unpacked_cnt integer,
  all_cnt numeric,
  price numeric,
  valid_date date,
  series character varying,
  is_vital boolean NOT NULL DEFAULT false,
  barcode character varying,
  price_firm numeric,
  price_gr numeric,
  percent_gross numeric,
  price_gross numeric,
  sum_gross numeric,
  percent_nds_gross numeric,
  price_nds_gross numeric,
  sum_nds_gross numeric,
  percent numeric,
  sum numeric,
  supplier_name character varying,
  supplier_doc_num character varying,
  gr_date date,
  farm_group_name character varying,
  supplier_doc_date date,
  profit numeric,
  mess character varying,
  state smallint NOT NULL DEFAULT 0,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  depart_id integer NOT NULL,
  depart_name character varying,
  pharmacy_name character varying,
  batch_num integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  batch_crt_date timestamp without time zone,
  client_name character varying,
  sales_name character varying,
  depart_address character varying,
  is_active boolean NOT NULL,
  batch_inactive_date timestamp without time zone,
  batch_is_confirmed boolean NOT NULL DEFAULT false,
  batch_confirm_date timestamp without time zone,  
  CONSTRAINT vw_stock_row_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_stock_row_tmp OWNER TO pavlov;

CREATE INDEX und_vw_stock_row_tmp_receiver_id1_idx
  ON und.vw_stock_row_tmp
  USING btree
  (receiver_id);