﻿/*
	CREATE OR REPLACE FUNCTION und.del_stock
*/

DROP FUNCTION und.del_stock(integer);

CREATE OR REPLACE FUNCTION und.del_stock(
    IN _sales_id integer,
    IN _depart_id integer,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN	

WITH deleted AS (
	
	delete from und.stock_row t1 using und.stock t2
	where t1.stock_id = t2.stock_id
	and t2.sales_id = _sales_id
	and (((_depart_id is not null) and (t2.depart_id = _depart_id)) or (_depart_id is null))
	RETURNING *
) SELECT count(*) FROM deleted INTO result;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.del_stock(integer, integer) OWNER TO pavlov;
