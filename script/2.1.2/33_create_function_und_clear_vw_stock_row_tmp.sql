﻿
-- DROP FUNCTION und.clear_vw_stock_row_tmp(integer);

CREATE OR REPLACE FUNCTION und.clear_vw_stock_row_tmp(
    _receiver_id integer)
  RETURNS void AS
$BODY$
BEGIN	
	delete from und.vw_stock_row_tmp where receiver_id = _receiver_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.clear_vw_stock_row_tmp(integer) OWNER TO pavlov;
