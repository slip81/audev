﻿
-- DROP FUNCTION und.get_stock_checksum(integer,integer,integer);

CREATE OR REPLACE FUNCTION und.get_stock_checksum(
    IN _client_id integer,
    IN _sales_id integer,
    IN _depart_id integer,
    OUT result text)
  RETURNS text AS
$BODY$
BEGIN	

result:= (select md5(string_agg((t1.artikul::text || '_' || t1.all_cnt::text), ',' ORDER BY t1.artikul, t1.all_cnt))
from und.vw_stock_row t1
where 1=1
and (((t1.client_id = _client_id) and (coalesce(_client_id, 0) > 0)) or (coalesce(_client_id, 0) <= 0))
and (((t1.sales_id = _sales_id) and (coalesce(_sales_id, 0) > 0)) or (coalesce(_sales_id, 0) <= 0))
and (((t1.depart_id = _depart_id) and (coalesce(_depart_id, 0) > 0)) or (coalesce(_depart_id, 0) <= 0))
and t1.state != 1 
and t1.is_active = false
);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.get_stock_checksum(integer,integer,integer) OWNER TO pavlov;
