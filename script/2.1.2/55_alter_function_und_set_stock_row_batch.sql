﻿/*
	CREATE OR REPLACE FUNCTION und.set_stock_row_batch
*/

-- DROP FUNCTION und.set_stock_row_batch(integer);

CREATE OR REPLACE FUNCTION und.set_stock_row_batch(_new_batch_id integer)
  RETURNS void AS
$BODY$
BEGIN	
	update und.stock_row 
	set batch_id = new_batch_id, new_batch_id = null
	, all_cnt = new_all_cnt, new_all_cnt = null
	, state = new_state, new_state = 0
	where coalesce(new_batch_id, 0) = _new_batch_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.set_stock_row_batch(integer) OWNER TO pavlov;
