﻿/*
	ALTER TABLE und.stock ADD COLUMNs last_xxx
*/

ALTER TABLE und.stock ADD COLUMN last_batch_id integer;
ALTER TABLE und.stock ADD COLUMN last_batch_date timestamp without time zone;

ALTER TABLE und.stock ADD COLUMN last_download_id integer;
ALTER TABLE und.stock ADD COLUMN last_download_date timestamp without time zone;

ALTER TABLE und.stock ADD COLUMN last_del_date timestamp without time zone;