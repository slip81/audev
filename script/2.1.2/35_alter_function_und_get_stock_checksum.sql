﻿/*
	CREATE OR REPLACE FUNCTION und.get_stock_checksum
*/

-- DROP FUNCTION und.get_stock_checksum(integer, integer, integer);

CREATE OR REPLACE FUNCTION und.get_stock_checksum(
    IN _client_id integer,
    IN _sales_id integer,
    IN _depart_id integer,
    IN download_batch_date_beg timestamp without time zone,
    IN download_batch_date_end timestamp without time zone,
    OUT result text)
  RETURNS text AS
$BODY$
BEGIN	

result:= (select md5(string_agg(to_char(trunc(coalesce(t1.all_cnt,0),3), 'FM9999999999990.000'), ';' ORDER BY t1.stock_id,t1.row_stock_id))
from und.vw_stock_row t1
where 1=1
and (((t1.client_id = _client_id) and (coalesce(_client_id, 0) > 0)) or (coalesce(_client_id, 0) <= 0))
and (((t1.sales_id = _sales_id) and (coalesce(_sales_id, 0) > 0)) or (coalesce(_sales_id, 0) <= 0))
and (((t1.depart_id = _depart_id) and (coalesce(_depart_id, 0) > 0)) or (coalesce(_depart_id, 0) <= 0))
/*
and t1.batch_confirm_date > download_batch_date_beg
and t1.batch_confirm_date <= download_batch_date_end
*/
and t1.state != 1 
and t1.is_active = false
and t1.batch_is_confirmed = true
);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.get_stock_checksum(integer, integer, integer, timestamp without time zone, timestamp without time zone) OWNER TO pavlov;
