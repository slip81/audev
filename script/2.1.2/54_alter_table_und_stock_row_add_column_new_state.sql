﻿/*
	ALTER TABLE und.stock_row ADD COLUMN new_state
*/

ALTER TABLE und.stock_row ADD COLUMN new_state smallint NOT NULL DEFAULT 0;