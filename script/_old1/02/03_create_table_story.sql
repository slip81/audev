﻿/*
	CREATE TABLE story
*/

-- DROP TABLE story;

CREATE TABLE story
(
  story_id bigserial NOT NULL,
  story_text character varying,
  story_subject character varying,
  org_name character varying,
  org_address character varying,
  person_name character varying,
  person_phone character varying,
  person_email character varying,
  person_id bigint,
  person_ip character varying,
  init_state_id bigint NOT NULL,
  curr_state_id bigint NOT NULL,
  is_answered boolean NOT NULL DEFAULT false,
  resp_user character varying,
  resp_user_id bigint,
  crm_num integer,
  crm_id integer,
  crt_date timestamp without time zone,
  upd_date timestamp without time zone,
  crt_user character varying,
  upd_user character varying,
  CONSTRAINT story_pkey PRIMARY KEY (story_id),
  CONSTRAINT story_curr_state_id_fkey FOREIGN KEY (curr_state_id)
      REFERENCES story_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT story_init_state_id_fkey FOREIGN KEY (init_state_id)
      REFERENCES story_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE story OWNER TO pavlov;
