﻿/*
	CREATE TABLE story_state
*/

-- DROP TABLE story_state;

CREATE TABLE story_state
(
  state_id bigserial NOT NULL,
  state_name character varying,
  is_answered boolean NOT NULL DEFAULT false,
  CONSTRAINT story_state_pkey PRIMARY KEY (state_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE story_state OWNER TO pavlov;
