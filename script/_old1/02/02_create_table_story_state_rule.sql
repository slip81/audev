﻿/*
	CREATE TABLE story_state_rule
*/

-- DROP TABLE story_state_rule;

CREATE TABLE story_state_rule
(
  rule_id bigserial NOT NULL,
  from_state_id bigint,
  to_state_id bigint,
  CONSTRAINT story_state_rule_pkey PRIMARY KEY (rule_id),
  CONSTRAINT story_state_rule_from_state_id_fkey FOREIGN KEY (from_state_id)
      REFERENCES story_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT story_state_rule_to_state_id_fkey FOREIGN KEY (to_state_id)
      REFERENCES story_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE story_state_rule  OWNER TO pavlov;
