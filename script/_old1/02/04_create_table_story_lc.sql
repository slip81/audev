﻿/*
	CREATE TABLE story_lc
*/

-- DROP TABLE story_lc;

CREATE TABLE story_lc
(
  lc_id bigserial NOT NULL,
  story_id bigint NOT NULL,
  state_id bigint NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  crt_date timestamp without time zone,
  upd_date timestamp without time zone,
  crt_user character varying,
  upd_user character varying,
  CONSTRAINT story_lc_pkey PRIMARY KEY (lc_id),
  CONSTRAINT story_lc_story_id_fkey FOREIGN KEY (story_id)
      REFERENCES story (story_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT story_lc_state_id_fkey FOREIGN KEY (state_id)
      REFERENCES story_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE story_lc OWNER TO pavlov;
