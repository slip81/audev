﻿/*
	ALTER TABLE cab_action ADD COLUMN sys_action_name
*/

ALTER TABLE cab_action ADD COLUMN sys_action_name character varying;
