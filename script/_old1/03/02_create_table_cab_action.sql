﻿/*
	CREATE TABLE cab_action
*/

-- DROP TABLE cab_action;

CREATE TABLE cab_action
(
  action_id bigint NOT NULL,
  group_id bigint NOT NULL,
  action_name character varying,
  CONSTRAINT cab_action_pkey PRIMARY KEY (action_id),
  CONSTRAINT cab_action_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cab_action_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cab_action OWNER TO pavlov;
