﻿
insert into cab_action_group (group_id, group_name)
values (1, 'Дисконтные карты');

insert into cab_action_group (group_id, group_name)
values (2, 'Типы карт');

insert into cab_action_group (group_id, group_name)
values (3, 'Дисконтные алгоритмы');

insert into cab_action_group (group_id, group_name)
values (4, 'Владельцы карт');

insert into cab_action_group (group_id, group_name)
values (5, 'Продажи по картам');

insert into cab_action_group (group_id, group_name)
values (6, 'Отчеты');

-- group_id 1 'Дисконтные карты'
insert into cab_action (action_id, group_id, action_name)
values (1, 1, 'Просмотр карт');

insert into cab_action (action_id, group_id, action_name)
values (2, 1, 'Добавление карты');

insert into cab_action (action_id, group_id, action_name)
values (3, 1, 'Редактирование карты');

insert into cab_action (action_id, group_id, action_name)
values (4, 1, 'Удаление карты');

insert into cab_action (action_id, group_id, action_name)
values (5, 1, 'Добавление набора карт');

insert into cab_action (action_id, group_id, action_name)
values (6, 1, 'Пакетные операции');

-- group_id 2 'Типы карт'
insert into cab_action (action_id, group_id, action_name)
values (7, 2, 'Просмотр типов карт');

insert into cab_action (action_id, group_id, action_name)
values (8, 2, 'Добавление типов карт');

insert into cab_action (action_id, group_id, action_name)
values (9, 2, 'Редактирование типов карт');

insert into cab_action (action_id, group_id, action_name)
values (10, 2, 'Удаление типов карт');

-- group_id 3 'Дисконтные алгоритмы'
insert into cab_action (action_id, group_id, action_name)
values (11, 3, 'Просмотр алгоритмов');

insert into cab_action (action_id, group_id, action_name)
values (12, 3, 'Добавление алгоритмов');

insert into cab_action (action_id, group_id, action_name)
values (13, 3, 'Редактирование алгоритмов');

insert into cab_action (action_id, group_id, action_name)
values (14, 3, 'Удаление алгоритмов');

insert into cab_action (action_id, group_id, action_name)
values (15, 3, 'Редактирование параметров алгоритма');

-- group_id 4 'Владельцы карт'
insert into cab_action (action_id, group_id, action_name)
values (16, 4, 'Просмотр владельцев карт');

insert into cab_action (action_id, group_id, action_name)
values (17, 4, 'Добавление владельцев карт');

insert into cab_action (action_id, group_id, action_name)
values (18, 4, 'Редактирование владельцев карт');

insert into cab_action (action_id, group_id, action_name)
values (19, 4, 'Удаление владельцев карт');

-- group_id 5 'Продажи по картам'
insert into cab_action (action_id, group_id, action_name)
values (20, 5, 'Просмотр продаж по картам');

insert into cab_action (action_id, group_id, action_name)
values (21, 5, 'Просмотр детализации продаж');

-- group_id 6 'Отчеты'
insert into cab_action (action_id, group_id, action_name)
values (22, 6, 'Общий отчет по картам');

insert into cab_action (action_id, group_id, action_name)
values (23, 6, 'Количество чеков за период');

-- select * from cab_action_group
-- select * from cab_action