﻿/*
	CREATE TABLE cab_action_group
*/

-- DROP TABLE cab_action_group;

CREATE TABLE cab_action_group
(
  group_id bigint NOT NULL,
  group_name character varying,
  CONSTRAINT cab_action_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cab_action_group OWNER TO pavlov;
