﻿/*
	CREATE TABLE cab_role
*/

-- DROP TABLE cab_role;

CREATE TABLE cab_role
(
  role_id bigint NOT NULL,
  role_name character varying,
  CONSTRAINT cab_role_pkey PRIMARY KEY (role_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cab_role OWNER TO pavlov;
