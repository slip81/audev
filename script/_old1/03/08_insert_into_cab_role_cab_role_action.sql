﻿
insert into cab_role (role_id, role_name)
values (1, 'Руководитель');

insert into cab_role (role_id, role_name)
values (2, 'Маркетолог');

insert into cab_role (role_id, role_name)
values (3, 'Аналитик');

insert into cab_role (role_id, role_name)
values (4, 'Фармацевт');

insert into cab_role (role_id, role_name)
values (5, 'Менеджер');

-- role_id = 1 'Руководитель'
insert into cab_role_action (role_id, group_id, action_id)
values (1, 1, null);

insert into cab_role_action (role_id, group_id, action_id)
values (1, 2, null);

insert into cab_role_action (role_id, group_id, action_id)
values (1, 3, null);

insert into cab_role_action (role_id, group_id, action_id)
values (1, 4, null);

insert into cab_role_action (role_id, group_id, action_id)
values (1, 5, null);

insert into cab_role_action (role_id, group_id, action_id)
values (1, 6, null);

-- role_id = 2 'Маркетолог'
insert into cab_role_action (role_id, group_id, action_id)
values (2, 1, null);

insert into cab_role_action (role_id, group_id, action_id)
values (2, 2, null);

insert into cab_role_action (role_id, group_id, action_id)
values (2, 3, null);

insert into cab_role_action (role_id, group_id, action_id)
values (2, 4, null);

insert into cab_role_action (role_id, group_id, action_id)
values (2, 5, null);

insert into cab_role_action (role_id, group_id, action_id)
values (2, 6, null);

-- role_id = 3 'Аналитик'
insert into cab_role_action (role_id, group_id, action_id)
values (3, 1, null);

insert into cab_role_action (role_id, group_id, action_id)
values (3, 2, null);

insert into cab_role_action (role_id, group_id, action_id)
values (3, 3, null);

insert into cab_role_action (role_id, group_id, action_id)
values (3, 4, null);

insert into cab_role_action (role_id, group_id, action_id)
values (3, 5, null);

insert into cab_role_action (role_id, group_id, action_id)
values (3, 6, null);

-- role_id = 4 'Фармацевт'
insert into cab_role_action (role_id, group_id, action_id)
values (4, 1, null);

insert into cab_role_action (role_id, group_id, action_id)
values (4, 2, null);

insert into cab_role_action (role_id, group_id, action_id)
values (4, 3, null);

insert into cab_role_action (role_id, group_id, action_id)
values (4, 4, null);

insert into cab_role_action (role_id, group_id, action_id)
values (4, 5, null);

insert into cab_role_action (role_id, group_id, action_id)
values (4, 6, null);

-- role_id = 5 'Менеджер'
insert into cab_role_action (role_id, group_id, action_id)
values (5, 1, null);

insert into cab_role_action (role_id, group_id, action_id)
values (5, 2, null);

insert into cab_role_action (role_id, group_id, action_id)
values (5, 3, null);

insert into cab_role_action (role_id, group_id, action_id)
values (5, 4, null);

insert into cab_role_action (role_id, group_id, action_id)
values (5, 5, null);

insert into cab_role_action (role_id, group_id, action_id)
values (5, 6, null);

-- select * from cab_action_group
-- select * from cab_action
-- select * from cab_role
-- select * from cab_role_action

