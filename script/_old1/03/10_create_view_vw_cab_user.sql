﻿/*
	CREATE VIEW vw_cab_user
*/

-- DROP VIEW vw_cab_user;

CREATE OR REPLACE VIEW vw_cab_user AS 
 SELECT t1.item_id, t1.user_name, t1.role_id, t1.use_defaults,
	t2.role_name,
	t4.org_name, 
	t5.business_name, t5.business_id
   FROM cab_user_role t1
     INNER JOIN cab_role t2 ON t1.role_id = t2.role_id
     LEFT JOIN business_org_user t3 ON trim(COALESCE(t1.user_name, ''::character varying)) = trim(t3.user_name)
     LEFT JOIN business_org t4 ON t3.org_id = t4.org_id
     LEFT JOIN business t5 ON t4.business_id = t5.business_id
     ;

ALTER TABLE vw_cab_user OWNER TO pavlov;
