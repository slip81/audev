﻿
insert into cab_user_role (user_name, role_id, use_defaults)
select distinct user_name, 5, true
from business_org_user;

insert into cab_user_role_action (user_name, role_id, group_id, action_id)
select t1.user_name, t2.role_id, t2.group_id, t2.action_id
from cab_user_role t1
inner join cab_role_action t2 on t1.role_id = t2.role_id;


-- select * from cab_role;
-- select * from cab_role_action;
-- select * from cab_user_role;
-- select * from cab_user_role_action;