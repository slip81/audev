﻿--select * from cab_action order by group_id, action_id

update cab_action 
set sys_action_name = 'Card_Read'
where action_id = 1;

update cab_action 
set sys_action_name = 'Card_Add'
where action_id = 2;

update cab_action 
set sys_action_name = 'Card_Edit'
where action_id = 3;

update cab_action 
set sys_action_name = 'Card_Del'
where action_id = 4;

update cab_action 
set sys_action_name = 'Card_BatchAdd'
where action_id = 5;

update cab_action 
set sys_action_name = 'Card_BatchOps'
where action_id = 6;


update cab_action 
set sys_action_name = 'CardType_Read'
where action_id = 7;

update cab_action 
set sys_action_name = 'CardType_Add'
where action_id = 8;

update cab_action 
set sys_action_name = 'CardType_Edit'
where action_id = 9;

update cab_action 
set sys_action_name = 'CardType_Del'
where action_id = 10;

update cab_action 
set sys_action_name = 'Programm_Read'
where action_id = 11;

update cab_action 
set sys_action_name = 'Programm_Add'
where action_id = 12;

update cab_action 
set sys_action_name = 'Programm_Edit'
where action_id = 13;

update cab_action 
set sys_action_name = 'Programm_Del'
where action_id = 14;

update cab_action 
set sys_action_name = 'Programm_EditParams'
where action_id = 15;

update cab_action 
set sys_action_name = 'Client_Read'
where action_id = 16;

update cab_action 
set sys_action_name = 'Client_Add'
where action_id = 17;

update cab_action 
set sys_action_name = 'Client_Edit'
where action_id = 18;

update cab_action 
set sys_action_name = 'Client_Del'
where action_id = 19;

update cab_action 
set sys_action_name = 'Sale_Read'
where action_id = 20;

update cab_action 
set sys_action_name = 'Sale_ReadDetails'
where action_id = 21;

update cab_action 
set sys_action_name = 'Report_CardInfo'
where action_id = 22;

update cab_action 
set sys_action_name = 'Report_CheckCount'
where action_id = 23;