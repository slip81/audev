﻿/*
	CREATE TABLE cab_role_action
*/

-- DROP TABLE cab_role_action;

CREATE TABLE cab_role_action
(
  item_id bigserial NOT NULL,
  role_id bigint NOT NULL,
  group_id bigint,
  action_id bigint,
  CONSTRAINT cab_role_action_pkey PRIMARY KEY (item_id),
  CONSTRAINT cab_role_action_action_id_fkey FOREIGN KEY (action_id)
      REFERENCES cab_action (action_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cab_role_action_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cab_action_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT cab_role_action_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cab_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cab_role_action OWNER TO pavlov;
