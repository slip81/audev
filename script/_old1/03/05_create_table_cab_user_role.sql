﻿/*
	CREATE TABLE cab_user_role	
*/

-- DROP TABLE cab_user_role;

CREATE TABLE cab_user_role
(
  item_id bigserial NOT NULL,
  user_name character varying,
  role_id bigint NOT NULL,
  use_defaults boolean NOT NULL DEFAULT true,
  CONSTRAINT cab_user_role_pkey PRIMARY KEY (item_id),
  CONSTRAINT cab_user_role_role_id_fkey FOREIGN KEY (role_id)
      REFERENCES cab_role (role_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cab_user_role OWNER TO pavlov;
