/*
	CREATE VIEW vw_workplace
*/

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`%` 
    SQL SECURITY DEFINER
VIEW `vw_workplace` AS
    SELECT 
        `t1`.`id` AS `workplace_id`,
        `t1`.`registration_key` AS `registration_key`,
        `t1`.`description` AS `description`,
        `t1`.`sales_id` AS `sales_id`,
        `t2`.`name` AS `sales_name`,
        `t2`.`adress` AS `sales_address`,
        `t2`.`client_id` AS `client_id`,
        `t3`.`client_name` AS `client_name`,
        `t4`.`name` AS `user_name`
    FROM
        (((`workplace` `t1`
        JOIN `sales` `t2` ON ((`t1`.`sales_id` = `t2`.`id`)))
        JOIN `allclients` `t3` ON ((`t2`.`client_id` = `t3`.`id`)))
        LEFT JOIN `my_aspnet_Users` `t4` ON ((`t2`.`user_id` = `t4`.`id`)))