/*
	ALTER view vw_license
*/


ALTER view vw_license as

select t5.id as client_id, t5.full_name as client_full_name
, t4.id as sales_id, t4.name as sales_name
, t3.id as workplace_id, t3.registration_key as workplace_reg_key, t3.description as workplace_descr
, t7.id as order_id, t7.order_status, t7.is_conformed as order_is_confirmed, t7.active_status as order_active_status
, t8.id as version_id, t8.name as version_name
, t9.id as service_id, t9.name as service_name
, t6.id as order_item_id, t6.quantity as order_item_quantity
, t2.id as service_registration_id
, t1.id as license_id, t1.created_on as license_created_on
, t1.dt_start as license_date_start, t1.dt_end as license_date_end
, t4.adress as sales_address
from clients.license t1
inner join clients.service_registration t2 on t1.service_registration_id = t2.id
inner join clients.workplace t3 on t2.workplace_id = t3.id
inner join clients.sales t4 on t3.sales_id = t4.id
inner join clients.client t5 on t4.client_id = t5.id
inner join clients.order_item t6 on t1.order_item_id = t6.id
-- and t6.service_id = 34
inner join clients.order t7 on t6.order_id = t7.id
inner join clients.version t8 on t6.version_id = t8.id
inner join clients.service t9 on t8.service_id = t9.id
-- where t5.id = 4827
order by t5.id, t4.id, t3.id, t7.id, t8.id, t9.id, t6.id, t2.id, t1.id
;
