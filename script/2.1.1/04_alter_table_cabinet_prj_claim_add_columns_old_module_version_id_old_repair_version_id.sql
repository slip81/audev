﻿/*
	ALTER TABLE cabinet.prj_claim ADD COLUMNs old_module_version_id, old_repair_version_id
*/

ALTER TABLE cabinet.prj_claim ADD COLUMN old_module_version_id integer;
ALTER TABLE cabinet.prj_claim ADD COLUMN old_repair_version_id integer;

UPDATE cabinet.prj_claim
SET old_module_version_id = module_version_id;

UPDATE cabinet.prj_claim
SET old_repair_version_id = repair_version_id;