﻿/*
	CREATE OR REPLACE FUNCTION cabinet.prj_start_new_release
*/

-- DROP FUNCTION cabinet.prj_start_new_release(integer, text);

CREATE OR REPLACE FUNCTION cabinet.prj_start_new_release(
    _project_id integer,
    _new_version_num text)
  RETURNS integer AS
$BODY$
DECLARE
 _result integer;
BEGIN
  _result := 0;

  -- select * from cabinet.prj_version_type order by version_type_id

  -- переносим из Текущей в Выпущенную
  UPDATE cabinet.prj_claim
  SET version_id = (SELECT max(x1.version_id) FROM cabinet.prj_project_version x1 WHERE x1.project_id = _project_id AND x1.version_type_id = 2)
  WHERE is_archive = false AND project_id = _project_id /*AND is_version_fixed = false*/
  AND version_id = (SELECT max(x1.version_id) FROM cabinet.prj_project_version x1 WHERE x1.project_id = _project_id AND x1.version_type_id = 3);  

  -- переносим из Следующей в Текущую
  UPDATE cabinet.prj_claim
  SET version_id = (SELECT max(x1.version_id) FROM cabinet.prj_project_version x1 WHERE x1.project_id = _project_id AND x1.version_type_id = 3)
  WHERE is_archive = false AND project_id = _project_id /*AND is_version_fixed = false*/
  AND version_id = (SELECT max(x1.version_id) FROM cabinet.prj_project_version x1 WHERE x1.project_id = _project_id AND x1.version_type_id = 4);

  UPDATE cabinet.prj_project_version
  SET version_value = (SELECT max(x1.version_value) FROM cabinet.prj_project_version x1 WHERE x1.project_id = _project_id AND x1.version_type_id = 3)
  WHERE project_id = _project_id
  AND version_type_id = 2;

  UPDATE cabinet.prj_project_version
  SET version_value = (SELECT max(x1.version_value) FROM cabinet.prj_project_version x1 WHERE x1.project_id = _project_id AND x1.version_type_id = 4)
  WHERE project_id = _project_id
  AND version_type_id = 3;  

  UPDATE cabinet.prj_project_version
  SET version_value = _new_version_num
  WHERE project_id = _project_id
  AND version_type_id = 4;  
  
  return _result;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.prj_start_new_release(integer, text) OWNER TO pavlov;
