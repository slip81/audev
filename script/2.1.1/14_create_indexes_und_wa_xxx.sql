﻿/*
	CREATE INDEXes und_wa_xxx
*/

CREATE INDEX und_wa_worker_task_worker_id1_idx
  ON und.wa_worker_task
  USING btree
  (worker_id);

CREATE INDEX und_wa_worker_task_task_id1_idx
  ON und.wa_worker_task
  USING btree
  (task_id);
  
CREATE INDEX und_wa_task_param_task_id1_idx
  ON und.wa_task_param
  USING btree
  (task_id);

 CREATE INDEX und_wa_task_lc_task_id1_idx
  ON und.wa_task_lc
  USING btree
  (task_id); 