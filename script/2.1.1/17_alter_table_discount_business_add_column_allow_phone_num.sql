﻿/*
	ALTER TABLE discount.business ADD COLUMN allow_phone_num
*/

ALTER TABLE discount.business ADD COLUMN allow_phone_num boolean NOT NULL DEFAULT false;
