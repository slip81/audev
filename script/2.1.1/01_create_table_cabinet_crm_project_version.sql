﻿/*
	CREATE TABLE cabinet.crm_project_version
*/

-- DROP TABLE cabinet.crm_project_version;

CREATE TABLE cabinet.crm_project_version
(
  project_version_id serial NOT NULL,
  project_version_name character varying,
  project_id integer NOT NULL,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope10(),
  CONSTRAINT crm_project_version_pkey PRIMARY KEY (project_version_id),
  CONSTRAINT crm_project_version_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_project_version OWNER TO pavlov;

-- DROP TRIGGER cabinet_crm_project_version_set_stamp_trg ON cabinet.crm_project_version;

CREATE TRIGGER cabinet_crm_project_version_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.crm_project_version
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

