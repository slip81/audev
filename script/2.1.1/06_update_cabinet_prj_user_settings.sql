﻿/*
	update cabinet.prj_user_settings
*/

-- select * from cabinet.prj_user_settings where user_id = 1

update cabinet.prj_user_settings
set settings = replace(settings, 'repair_version_name', 'project_repair_version_name');

update cabinet.prj_user_settings
set settings = replace(settings, 'module_version_name', 'project_version_name');