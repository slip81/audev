﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim_xxx
*/

DROP VIEW cabinet.vw_prj_report1;
DROP VIEW cabinet.vw_prj_user_group_item;
DROP VIEW cabinet.vw_prj_work;
DROP VIEW cabinet.vw_prj_task;
DROP VIEW cabinet.vw_prj_claim;

ALTER TABLE cabinet.prj_claim DROP COLUMN module_version_id;
ALTER TABLE cabinet.prj_claim DROP COLUMN repair_version_id;

-----------------------------------------------

CREATE OR REPLACE VIEW cabinet.vw_prj_claim AS 
 SELECT t1.claim_id,
    t1.claim_num,
    t1.claim_name,
    t1.claim_text,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.project_version_id,
    t1.client_id,
    t1.priority_id,
    t1.resp_user_id,
    t1.date_plan,
    t1.date_fact,
    t1.project_repair_version_id,
    t1.is_archive,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.is_control,
    t1.old_task_id,
    t2.project_name,
    t3.module_name,
    t4.module_part_name,
    t5.project_version_name,
    t6.client_name,
    t7.priority_name,
    t8.user_name AS resp_user_name,
    t11.project_version_name AS project_repair_version_name,
    COALESCE(t12.task_cnt, 0::bigint) AS task_cnt,
    COALESCE(t12.new_task_cnt, 0::bigint) AS new_task_cnt,
    COALESCE(t12.active_task_cnt, 0::bigint) AS active_task_cnt,
    COALESCE(t12.done_task_cnt, 0::bigint) AS done_task_cnt,
    COALESCE(t12.canceled_task_cnt, 0::bigint) AS canceled_task_cnt,
    t12.claim_state_type_id,
    t13.state_type_name AS claim_state_type_name,
    t13.templ AS claim_state_type_templ,
    t1.old_group_id,
    t1.old_state_id,
    t14.group_name AS old_group_name,
    t15.state_name AS old_state_name,
    t12.active_work_type_id,
    t16.work_type_name AS active_work_type_name,
        CASE
            WHEN t17.claim_id IS NULL THEN false
            ELSE true
        END AS is_overdue,
    t18.state_type_id AS brief_state_type_id,
    t18.state_type_name AS brief_state_type_name,
    t12.exec_user_name_list,
    t1.claim_type_id,
    t1.claim_stage_id,
    t50.claim_type_name,
    t51.claim_stage_name,
    t19.mess_cnt,
    t7.rate AS priority_rate,
    t50.rate AS claim_type_rate,
    COALESCE(t7.rate, 0::numeric) * COALESCE(t50.rate, 0::numeric) AS summary_rate,
        CASE t12.claim_state_type_id
            WHEN 1 THEN true
            WHEN 2 THEN true
            ELSE false
        END AS is_active_state,
    t20.prj_id_list,
    t20.prj_name_list,
    t1.version_id,
    t1.is_version_fixed,
    t52.version_name,
        CASE t1.is_version_fixed
            WHEN true THEN t1.version_value
            ELSE t52.version_value
        END AS version_value,
    (t52.version_name::text || ' '::text) ||
        CASE t1.is_version_fixed
            WHEN true THEN t1.version_value
            ELSE t52.version_value
        END::text AS version_value_full,
    t53.version_type_id,
    t53.version_type_name,
    t1.arc_date,
    t1.arc_user
   FROM cabinet.prj_claim t1
     JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_project_version t5 ON t1.project_version_id = t5.project_version_id
     JOIN cabinet.crm_client t6 ON t1.client_id = t6.client_id
     JOIN cabinet.crm_priority t7 ON t1.priority_id = t7.priority_id
     JOIN cabinet.cab_user t8 ON t1.resp_user_id = t8.user_id
     JOIN cabinet.prj_claim_type t50 ON t1.claim_type_id = t50.claim_type_id
     JOIN cabinet.prj_claim_stage t51 ON t1.claim_stage_id = t51.claim_stage_id
     JOIN cabinet.prj_project_version t52 ON t1.version_id = t52.version_id
     JOIN cabinet.prj_version_type t53 ON t52.version_type_id = t53.version_type_id    
     LEFT JOIN cabinet.crm_project_version t11 ON t1.project_repair_version_id = t11.project_version_id
     LEFT JOIN cabinet.vw_prj_claim_simple t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_claim_state_type t13 ON t12.claim_state_type_id = t13.state_type_id
     LEFT JOIN cabinet.crm_group t14 ON t1.old_group_id = t14.group_id
     LEFT JOIN cabinet.crm_state t15 ON t1.old_state_id = t15.state_id
     LEFT JOIN cabinet.prj_work_type t16 ON t12.active_work_type_id = t16.work_type_id
     LEFT JOIN LATERAL ( SELECT x2.claim_id,
            min(x1.date_plan) AS min_date_plan
           FROM cabinet.prj_task x1
             JOIN cabinet.prj_work x2 ON x1.task_id = x2.task_id
             JOIN cabinet.prj_work_state_type x3 ON x2.state_type_id = x3.state_type_id
          WHERE x3.done_or_canceled = 0
          GROUP BY x2.claim_id) t17 ON t1.claim_id = t17.claim_id AND GREATEST(COALESCE(t1.date_plan, '2000-01-01'::date), COALESCE(t17.min_date_plan, '2000-01-01'::date)) < 'now'::text::date
     LEFT JOIN cabinet.prj_brief_state_type t18 ON t13.brief_state_type_id = t18.state_type_id
     LEFT JOIN ( SELECT x1.claim_id,
            count(x1.mess_id) AS mess_cnt
           FROM cabinet.prj_mess x1
          GROUP BY x1.claim_id) t19 ON t19.claim_id = t1.claim_id
     LEFT JOIN ( SELECT x1.claim_id,
            array_to_string(array_agg(x1.prj_id), ','::text) AS prj_id_list,
            array_to_string(array_agg(x2.prj_name), ','::text) AS prj_name_list
           FROM cabinet.prj_claim_prj x1
             JOIN cabinet.prj x2 ON x1.prj_id = x2.prj_id
          GROUP BY x1.claim_id) t20 ON t1.claim_id = t20.claim_id;

ALTER TABLE cabinet.vw_prj_claim OWNER TO pavlov;

-----------------------------------------------

-- DROP VIEW cabinet.vw_prj_task;

CREATE OR REPLACE VIEW cabinet.vw_prj_task AS 
 SELECT t1.task_id,
    t1.claim_id,
    t1.task_num,
    t1.task_text,
    t1.date_plan,
    t1.date_fact,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.claim_num,
    t2.claim_name,
    t2.claim_text,
    t2.project_id AS claim_project_id,
    t2.module_id AS claim_module_id,
    t2.module_part_id AS claim_module_part_id,
    t2.project_version_id AS claim_project_version_id,
    t2.client_id AS claim_client_id,
    t2.priority_id AS claim_priority_id,
    t2.resp_user_id AS claim_resp_user_id,
    t2.date_plan AS claim_date_plan,
    t2.date_fact AS claim_date_fact,
    t2.project_repair_version_id AS claim_project_repair_version_id,
    t2.is_archive AS claim_is_archive,
    t2.is_control AS claim_is_control,
    t2.project_name AS claim_project_name,
    t2.module_name AS claim_module_name,
    t2.module_part_name AS claim_module_part_name,
    t2.project_version_name AS claim_project_version_name,
    t2.client_name AS claim_client_name,
    t2.priority_name AS claim_priority_name,
    t2.resp_user_name AS claim_resp_user_name,
    t2.project_repair_version_name AS claim_project_repair_version_name,
    t2.claim_state_type_id,
    t2.claim_state_type_name,
    t2.claim_state_type_templ,
    t11.task_state_type_id,
    COALESCE(t11.work_cnt, 0::bigint) AS work_cnt,
    COALESCE(t11.active_work_cnt, 0::bigint) AS active_work_cnt,
    COALESCE(t11.done_work_cnt, 0::bigint) AS done_work_cnt,
    COALESCE(t11.canceled_work_cnt, 0::bigint) AS canceled_work_cnt,
    t12.state_type_name AS task_state_type_name,
    t12.templ AS task_state_type_templ,
    t1.old_state_id,
    t13.state_name AS old_state_name,
    t11.active_work_type_id,
    t14.work_type_name AS active_work_type_name,
    false AS is_overdue,
    t2.claim_type_id,
    t2.claim_stage_id,
    t2.claim_type_name,
    t2.claim_stage_name,
    t15.task_mess_cnt,
    t2.prj_id_list,
    t2.prj_name_list
   FROM cabinet.prj_task t1
     JOIN cabinet.vw_prj_claim t2 ON t1.claim_id = t2.claim_id
     LEFT JOIN cabinet.vw_prj_task_simple t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.prj_task_state_type t12 ON t11.task_state_type_id = t12.state_type_id
     LEFT JOIN cabinet.crm_state t13 ON t1.old_state_id = t13.state_id
     LEFT JOIN cabinet.prj_work_type t14 ON t11.active_work_type_id = t14.work_type_id
     LEFT JOIN ( SELECT x1.task_id,
            count(x1.mess_id) AS task_mess_cnt
           FROM cabinet.prj_mess x1
          GROUP BY x1.task_id) t15 ON t15.task_id = t1.task_id;

ALTER TABLE cabinet.vw_prj_task OWNER TO pavlov;
-----------------------------------------------

-- DROP VIEW cabinet.vw_prj_work;

CREATE OR REPLACE VIEW cabinet.vw_prj_work AS 
 SELECT t1.work_id,
    t1.task_id,
    t1.claim_id,
    t1.work_num,
    t1.work_type_id,
    t1.exec_user_id,
    t1.state_type_id,
    t1.is_plan,
    t1.date_beg,
    t1.date_end,
    t1.is_accept,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.cost_plan,
    t1.cost_fact,
    t1.mess,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.work_type_name,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    t4.templ AS state_type_templ,
    t5.task_num,
    t5.task_text,
    t5.task_state_type_id,
    t5.date_plan AS task_date_plan,
    t5.date_fact AS task_date_fact,
    t5.task_state_type_name,
    t5.task_state_type_templ,
    t5.claim_num,
    t5.claim_name,
    t5.claim_text,
    t5.claim_project_id,
    t5.claim_module_id,
    t5.claim_module_part_id,
    t5.claim_project_version_id,
    t5.claim_client_id,
    t5.claim_priority_id,
    t5.claim_resp_user_id,
    t5.claim_date_plan,
    t5.claim_date_fact,
    t5.claim_project_repair_version_id,
    t5.claim_is_archive,
    t5.claim_is_control,
    t5.claim_project_name,
    t5.claim_module_name,
    t5.claim_module_part_name,
    t5.claim_project_version_name,
    t5.claim_client_name,
    t5.claim_priority_name,
    t5.claim_resp_user_name,
    t5.claim_project_repair_version_name,
    t5.claim_state_type_id,
    t5.claim_state_type_name,
    t5.claim_state_type_templ,
        CASE
            WHEN t4.done_or_canceled = 0 AND GREATEST(COALESCE(t5.claim_date_plan, '2000-01-01'::date), COALESCE(t5.date_plan, '2000-01-01'::date)) < 'now'::text::date THEN true
            ELSE false
        END AS is_overdue,
    t1.date_time_beg,
    t1.date_time_end,
    t1.is_all_day,
    t5.claim_type_id,
    t5.claim_stage_id,
    t5.claim_type_name,
    t5.claim_stage_name,
    t11.state_type_id AS brief_state_type_id,
    t11.state_type_name AS brief_state_type_name,
    t1.date_send,
    t5.prj_id_list,
    t5.prj_name_list
   FROM cabinet.prj_work t1
     JOIN cabinet.prj_work_type t2 ON t1.work_type_id = t2.work_type_id
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id
     JOIN cabinet.vw_prj_task t5 ON t1.task_id = t5.task_id
     LEFT JOIN cabinet.prj_brief_state_type t11 ON t4.brief_state_type_id = t11.state_type_id;

ALTER TABLE cabinet.vw_prj_work OWNER TO pavlov;

-----------------------------------------------

-- DROP VIEW cabinet.vw_prj_user_group_item;

CREATE OR REPLACE VIEW cabinet.vw_prj_user_group_item AS 
 SELECT t1.item_id,
    t1.group_id,
    t1.item_type_id,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_id
            WHEN 4 THEN t14.claim_id
            ELSE NULL::integer
        END AS claim_id,
    t1.event_id,
    t1.note_id,
    t1.ord,
    t1.task_id,
    t2.user_id,
    t2.group_name,
    t2.fixed_kind,
    t3.item_type_name,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_num
            WHEN 3 THEN t13.note_num::character varying
            WHEN 4 THEN ((t14.claim_num::text || '/'::text) || t14.task_num::character varying::text)::character varying
            ELSE NULL::character varying
        END AS item_num,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_name
            WHEN 3 THEN t13.note_name
            WHEN 4 THEN t14.task_text
            ELSE NULL::character varying
        END AS item_name,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_text
            WHEN 3 THEN t13.note_text
            WHEN 4 THEN t14.claim_text
            ELSE NULL::character varying
        END AS item_text,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_state_type_id
            WHEN 4 THEN t14.task_state_type_id
            ELSE NULL::integer
        END AS item_state_type_id,
        CASE t1.item_type_id
            WHEN 1 THEN t11.claim_state_type_name
            WHEN 4 THEN t14.task_state_type_name
            ELSE NULL::character varying
        END AS item_state_type_name,
    NULL::timestamp without time zone AS item_date_beg,
    NULL::timestamp without time zone AS item_date_end,
    t1.ord_prev,
    t1.group_id_prev
   FROM cabinet.prj_user_group_item t1
     JOIN cabinet.prj_user_group t2 ON t1.group_id = t2.group_id
     JOIN cabinet.prj_user_group_item_type t3 ON t1.item_type_id = t3.item_type_id
     LEFT JOIN cabinet.vw_prj_claim t11 ON t1.claim_id = t11.claim_id
     LEFT JOIN cabinet.vw_prj_note t13 ON t1.note_id = t13.note_id
     LEFT JOIN cabinet.vw_prj_task t14 ON t1.task_id = t14.task_id;

ALTER TABLE cabinet.vw_prj_user_group_item OWNER TO pavlov;
-----------------------------------------------
-- DROP VIEW cabinet.vw_prj_report1;

CREATE OR REPLACE VIEW cabinet.vw_prj_report1 AS 
 SELECT row_number() OVER (ORDER BY x1.gr, x1.task_num) AS id,
    x1.gr,
    x1.gr_name,
    x1.task_num,
    x1.active_first_exec_user_name,
    x1.done_last_date_end,
    count(x1.*) OVER (PARTITION BY x1.gr) AS gr_cnt,
    sum(
        CASE
            WHEN x1.gr = 4 THEN 1
            ELSE 0
        END) OVER (PARTITION BY NULL::text) AS done_cnt,
    count(x1.*) OVER (PARTITION BY NULL::text) AS cnt
   FROM ( WITH cte1 AS (
                 SELECT t1.claim_id,
                    t1.claim_num,
                    t1.task_num,
                    t1.work_num,
                    t1.work_type_id,
                    t1.work_type_name,
                    t1.state_type_id,
                    t1.state_type_name,
                    t1.exec_user_id,
                    t1.exec_user_name,
                    first_value(t1.exec_user_name) OVER (PARTITION BY t1.claim_id, t1.task_num ORDER BY
                        CASE
                            WHEN t1.state_type_id = ANY (ARRAY[1, 4]) THEN 0
                            ELSE 1
                        END) AS active_first_exec_user_name,
                    t1.date_end,
                    max(t1.date_end) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_last_date_end,
                    count(*) OVER (PARTITION BY t1.claim_id, t1.task_num) AS work_cnt,
                    sum(
                        CASE
                            WHEN t1.state_type_id = ANY (ARRAY[1, 4]) THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS active_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[1, 4])) AND t1.work_type_id = 3 THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS active_progr_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[1, 4])) AND t1.work_type_id = 4 THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS active_test_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[1, 4])) AND (t1.work_type_id <> ALL (ARRAY[3, 4])) THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS active_other_work_cnt,
                    sum(
                        CASE
                            WHEN t1.state_type_id = ANY (ARRAY[2, 3]) THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[2, 3])) AND t1.work_type_id = 3 THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_progr_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[2, 3])) AND t1.work_type_id = 4 THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_test_work_cnt,
                    sum(
                        CASE
                            WHEN (t1.state_type_id = ANY (ARRAY[2, 3])) AND (t1.work_type_id <> ALL (ARRAY[3, 4])) THEN 1
                            ELSE 0
                        END) OVER (PARTITION BY t1.claim_id, t1.task_num) AS done_other_work_cnt
                   FROM cabinet.vw_prj_work t1
                     JOIN cabinet.vw_prj_claim t2 ON t1.claim_id = t2.claim_id AND t2.project_id = 1 AND t2.version_type_id = 3
                )
         SELECT DISTINCT 1 AS gr,
            'на тестировании'::text AS gr_name,
            (cte1.claim_num::text || '/'::text) || cte1.task_num AS task_num,
            cte1.active_first_exec_user_name,
            NULL::timestamp without time zone AS done_last_date_end
           FROM cte1
          WHERE cte1.active_test_work_cnt > 0
        UNION ALL
         SELECT DISTINCT 2 AS gr,
            'передано на доработку'::text AS gr_name,
            (cte1.claim_num::text || '/'::text) || cte1.task_num AS task_num,
            cte1.active_first_exec_user_name,
            NULL::timestamp without time zone AS done_last_date_end
           FROM cte1
          WHERE cte1.active_test_work_cnt <= 0 AND cte1.done_test_work_cnt > 0 AND cte1.active_progr_work_cnt > 0
        UNION ALL
         SELECT DISTINCT 3 AS gr,
            'в разработке'::text AS gr_name,
            (cte1.claim_num::text || '/'::text) || cte1.task_num AS task_num,
            cte1.active_first_exec_user_name,
            NULL::timestamp without time zone AS done_last_date_end
           FROM cte1
          WHERE cte1.done_test_work_cnt <= 0 AND cte1.active_progr_work_cnt > 0
        UNION ALL
         SELECT DISTINCT 4 AS gr,
            'выполнено'::text AS gr_name,
            (cte1.claim_num::text || '/'::text) || cte1.task_num AS task_num,
            NULL::text AS active_first_exec_user_name,
            cte1.done_last_date_end
           FROM cte1
          WHERE cte1.active_work_cnt <= 0) x1;

ALTER TABLE cabinet.vw_prj_report1 OWNER TO pavlov;
-----------------------------------------------