﻿/*
	insert into cabinet.crm_project_version
*/

-- delete from cabinet.crm_project_version;

insert into cabinet.crm_project_version (project_version_name, project_id)
select distinct module_version_name, 0 from cabinet.crm_module_version where module_id in (select module_id from cabinet.crm_module where project_id = 0) order by 1;

insert into cabinet.crm_project_version (project_version_name, project_id)
select distinct module_version_name, 1 from cabinet.crm_module_version where module_id in (select module_id from cabinet.crm_module where project_id = 1) order by 1;

insert into cabinet.crm_project_version (project_version_name, project_id)
select distinct module_version_name, 2 from cabinet.crm_module_version where module_id in (select module_id from cabinet.crm_module where project_id = 2) order by 1;

insert into cabinet.crm_project_version (project_version_name, project_id)
select distinct module_version_name, 3 from cabinet.crm_module_version where module_id in (select module_id from cabinet.crm_module where project_id = 3) order by 1;

insert into cabinet.crm_project_version (project_version_name, project_id)
select distinct module_version_name, 4 from cabinet.crm_module_version where module_id in (select module_id from cabinet.crm_module where project_id = 4) order by 1;

insert into cabinet.crm_project_version (project_version_name, project_id)
select distinct module_version_name, 5 from cabinet.crm_module_version where module_id in (select module_id from cabinet.crm_module where project_id = 5) order by 1;
