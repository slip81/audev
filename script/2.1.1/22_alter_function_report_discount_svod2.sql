﻿-- DROP FUNCTION discount.report_discount_svod2(bigint, bigint, date, date);

CREATE OR REPLACE FUNCTION discount.report_discount_svod2(
    IN _business_id bigint,
    IN _card_type_group_id bigint,
    IN _date_beg date,
    IN _date_end date)
  RETURNS TABLE(num bigint, date_check_date_only date, sum_unit_value_discount numeric, pos_count bigint, check_count bigint, sum_unit_value numeric, sum_unit_value_with_discount numeric, sum_tax_value numeric) AS
$BODY$
BEGIN
	RETURN QUERY
		select row_number() over(order by date(t1.date_check)) as num, date(t1.date_check) as date_check_date_only
		, sum(t2.unit_value_discount) as sum_unit_value_discount, count(t2.detail_id) as pos_count, count(DISTINCT t1.card_trans_id) as check_count
		, sum(t2.unit_value) as sum_unit_value, sum(t2.unit_value_with_discount) as sum_unit_value_with_discount, sum(t2.tax_summa) as sum_tax_value
		from discount.card_transaction t1
		inner join discount.card_transaction_detail t2 on t1.card_trans_id = t2.card_trans_id
		inner join discount.card t3 on t1.card_id = t3.card_id
		inner join discount.card_type t4 on t3.curr_card_type_id = t4.card_type_id
		-- исключаем сертификаты (см. AU-2410)
		and t4.card_type_group_id != 4
		where 1=1
		and t1.status = 0
		and t1.trans_kind = 1
		and t3.business_id = _business_id		
		and (((t1.date_check::date >= _date_beg) and (_date_beg is not null)) or (_date_beg is null))
		and (((t1.date_check::date <= _date_end) and (_date_end is not null)) or (_date_end is null))
		--and t4.card_type_group_id = _card_type_group_id
		group by date(t1.date_check)
		having sum(t2.unit_value_discount) > 0
		;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION discount.report_discount_svod2(bigint, bigint, date, date) OWNER TO pavlov;
		