﻿/*
	ALTER TABLE cabinet.prj_claim ADD COLUMNs project_version_id, project_repair_version_id
*/

ALTER TABLE cabinet.prj_claim ADD COLUMN project_version_id integer;

UPDATE cabinet.prj_claim SET project_version_id = (
SELECT max(x1.project_version_id)
FROM cabinet.crm_project_version x1
INNER JOIN cabinet.crm_module_version x2 ON x1.project_version_name = x2.module_version_name
--AND x2.module_id = cabinet.prj_claim.module_id
AND x2.module_version_id = cabinet.prj_claim.module_version_id
WHERE cabinet.prj_claim.project_id = x1.project_id
);

ALTER TABLE cabinet.prj_claim ALTER COLUMN project_version_id SET NOT NULL;

ALTER TABLE cabinet.prj_claim ADD COLUMN project_repair_version_id integer;

UPDATE cabinet.prj_claim SET project_repair_version_id = (
SELECT max(x1.project_version_id)
FROM cabinet.crm_project_version x1
INNER JOIN cabinet.crm_module_version x2 ON x1.project_version_name = x2.module_version_name
--AND x2.module_id = cabinet.prj_claim.module_id
AND x2.module_version_id = cabinet.prj_claim.repair_version_id
WHERE cabinet.prj_claim.project_id = x1.project_id
);


ALTER TABLE cabinet.prj_claim
  ADD CONSTRAINT prj_claim_project_version_id_fkey FOREIGN KEY (project_version_id)
      REFERENCES cabinet.crm_project_version (project_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cabinet.prj_claim
  ADD CONSTRAINT prj_claim_project_repair_version_id_fkey FOREIGN KEY (project_repair_version_id)
      REFERENCES cabinet.crm_project_version (project_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      