﻿/*
	CREATE OR REPLACE FUNCTION und.del_stock
*/

-- DROP FUNCTION und.del_stock(integer);

CREATE OR REPLACE FUNCTION und.del_stock(_sales_id integer, OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN	

WITH deleted AS (
	delete from und.stock_row t1 using und.stock t2
	where t1.stock_id = t2.stock_id
	and t2.sales_id = _sales_id
	RETURNING *
) SELECT count(*) FROM deleted INTO result;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.del_stock(integer) OWNER TO pavlov;
