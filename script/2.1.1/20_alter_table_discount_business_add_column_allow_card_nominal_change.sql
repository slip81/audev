﻿/*
	ALTER TABLE discount.business ADD COLUMN allow_card_nominal_change
*/

ALTER TABLE discount.business ADD COLUMN allow_card_nominal_change boolean NOT NULL DEFAULT false;