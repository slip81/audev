﻿/*
	CREATE INDEX und_stock_row_stock_id1_idx
*/

CREATE INDEX und_stock_row_stock_id1_idx
  ON und.stock_row
  USING btree
  (stock_id);