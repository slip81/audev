﻿/*
select * from und.wa

select * from und.wa_request_type order by request_type_id

select * from und.wa_request_type_group1

select * from und.wa_request_type_group2
*/

insert into und.wa_request_type_group1 (request_type_group1_id, request_type_group1_name, crt_date, crt_user, upd_date, upd_user, is_deleted)
values (3, 'Сводное наличие', current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

insert into und.wa_request_type (request_type_id, request_type_name, request_type_group1_id, crt_date, crt_user, upd_date, upd_user, is_deleted, is_inner, is_outer, handling_server_max_cnt)
values (10, 'Удаление сводного наличия', 3, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false, false, true, 1);