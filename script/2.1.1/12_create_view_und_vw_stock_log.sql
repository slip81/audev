﻿/*
	CREATE OR REPLACE VIEW und.vw_stock_log
*/

-- DROP VIEW und.vw_stock_log;

CREATE OR REPLACE VIEW und.vw_stock_log AS 

SELECT t1.client_id, t1.client_name, t1.sales_id, t1.sales_name, t11.crt_date as last_upl_date, t12.crt_date as last_downl_date
, t13.crt_date as last_del_date
FROM cabinet.vw_client_service t1
LEFT JOIN (
  SELECT t2.sales_id, max(t1.crt_date) as crt_date
  FROM und.stock_batch AS t1
  JOIN und.stock t2 ON t1.stock_id = t2.stock_id
  GROUP BY t2.sales_id
) t11 on t1.sales_id = t11.sales_id
LEFT JOIN (
  SELECT downl.sales_id, max(downl.crt_date) as crt_date
  FROM und.stock_sales_download AS downl
  GROUP BY downl.sales_id
) t12 on t1.sales_id = t12.sales_id
LEFT JOIN (
select t1.obj_id as sales_id, max(t1.date_beg) as crt_date
from esn.log_esn t1 where log_esn_type_id = 31
GROUP BY t1.obj_id
) t13 on t1.sales_id = t12.sales_id
WHERE t1.service_id = 65;

ALTER TABLE und.vw_stock_log OWNER TO pavlov;
