﻿/*
	CREATE OR REPLACE FUNCTION und.get_stock_chain_count
*/

CREATE OR REPLACE FUNCTION und.get_stock_chain_count(
    IN _for_upload integer,
    IN _for_download integer,
    OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN		
	/*
		этот запрос возвращает кол-во текущих активных цепочек либо для отправки клиенту (если параметр _for_download = 1),
		либо для получения от клиента (если параметр _for_upload = 1);
		в сервисе СН сейчас ведутся только цепочки для получения от клиента (SendStock);
	*/
	result := (select count(chain_id) -- кол-во цепочек
	from und.stock_chain 
	where is_active = true -- проверка, что цепочка активна
	and is_deleted = false -- проверка, что цепочка не удалена
	and (((_for_upload = 1) and (is_upload = true)) or (_for_upload != 1)) -- фильтр - только для SendStock
	and (((_for_download = 1) and (is_upload != true)) or (_for_download != 1)) -- фильтр - только для GetStock
	and EXTRACT(EPOCH FROM (current_timestamp::timestamp without time zone) - crt_date)/60 < 17 /* 17 min */ -- условие - цепочка была создана раньше, чем 17 минут назад
	limit 1);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.get_stock_chain_count(integer, integer) OWNER TO pavlov;
