﻿
DROP FUNCTION und.clear_vw_stock_row_tmp(integer);

CREATE OR REPLACE FUNCTION und.clear_vw_stock_row_tmp(
	IN _receiver_id integer,
	OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN
	result:= 0;
	delete from und.vw_stock_row_tmp where receiver_id = _receiver_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.clear_vw_stock_row_tmp(integer) OWNER TO pavlov;
