﻿/*
	CREATE OR REPLACE VIEW discount.vw_card_type
*/

-- DROP VIEW discount.vw_card_type;

CREATE OR REPLACE VIEW discount.vw_card_type AS 
 SELECT t1.card_type_id,
    t1.card_type_name,
    t1.business_id,
    t1.is_fin_spec,
    t1.card_type_group_id,
    t2.group_name,
    t4.programm_id,
    t4.programm_name,
    t4.descr_short,
    t4.descr_full,
    t1.update_nominal,
    t1.reset_interval_bonus_value,
    t1.reset_date,
    t1.is_reset_without_sales
   FROM discount.card_type t1
     LEFT JOIN discount.card_type_group t2 ON COALESCE(t1.card_type_group_id, 0::bigint) = t2.card_type_group_id
     LEFT JOIN discount.card_type_programm_rel t3 ON t1.card_type_id = t3.card_type_id AND t3.date_beg <= 'now'::text::date AND (t3.date_end IS NULL OR t3.date_end > 'now'::text::date)
     LEFT JOIN discount.programm t4 ON t3.programm_id = t4.programm_id;

ALTER TABLE discount.vw_card_type OWNER TO pavlov;
