﻿/*
	CREATE OR REPLACE FUNCTION und.get_stock_chain_count
*/

-- DROP FUNCTION und.get_stock_chain_count(integer, integer);

CREATE OR REPLACE FUNCTION und.get_stock_chain_count(
    IN _for_upload integer,
    IN _for_download integer,
    OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN	
	-- result := (select count(chain_id) from und.stock_chain where is_active = true and is_deleted = false and crt_date > current_date - 1 limit 1);
	result := (select count(chain_id) 
	from und.stock_chain 
	where is_active = true 
	and is_deleted = false 
	and (((_for_upload = 1) and (is_upload = true)) or (_for_upload != 1))
	and (((_for_download = 1) and (is_upload != true)) or (_for_download != 1))
	/*and EXTRACT(EPOCH FROM current_timestamp - crt_date)/3600 < 3 */
	and EXTRACT(EPOCH FROM (current_timestamp::timestamp without time zone) - crt_date)/60 < 17 /* 17 min */
	limit 1);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.get_stock_chain_count(integer, integer) OWNER TO pavlov;
