﻿/*
	CREATE OR REPLACE FUNCTION und.get_stock_chain_count
*/

-- DROP FUNCTION und.get_stock_chain_count();

CREATE OR REPLACE FUNCTION und.get_stock_chain_count(OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN	
	-- result := (select count(chain_id) from und.stock_chain where is_active = true and is_deleted = false and crt_date > current_date - 1 limit 1);
	result := (select count(chain_id) from und.stock_chain where is_active = true and is_deleted = false and EXTRACT(EPOCH FROM current_timestamp - crt_date)/3600 < 3 limit 1);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.get_stock_chain_count() OWNER TO pavlov;
