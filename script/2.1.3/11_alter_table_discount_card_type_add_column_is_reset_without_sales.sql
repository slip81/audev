﻿/*
	ALTER TABLE discount.card_type ADD COLUMN is_reset_without_sales
*/

ALTER TABLE discount.card_type ADD COLUMN is_reset_without_sales smallint NOT NULL DEFAULT 0;
