﻿/*
	CREATE OR REPLACE FUNCTION und.prepare_vw_stock_row_tmp
*/

-- DROP FUNCTION und.prepare_vw_stock_row_tmp(integer, integer, timestamp without time zone, timestamp without time zone, integer);

CREATE OR REPLACE FUNCTION und.prepare_vw_stock_row_tmp(
    IN _receiver_id integer,
    IN _client_id integer,
    IN _download_batch_date_beg timestamp without time zone,
    IN _download_batch_date_end timestamp without time zone,
    IN _force_all integer)
  RETURNS TABLE(checksum text, cnt integer) AS
$BODY$
BEGIN	
	-- !!! опасность блокировок
	-- LOCK TABLE und.stock_row IN ACCESS EXCLUSIVE MODE;
	
	-- часть 1 из 2: подготовка пакетов сводного надличия для отправки клиенту
	
	delete from und.vw_stock_row_tmp where receiver_id = _receiver_id;
	
	insert into und.vw_stock_row_tmp (
	  stock_id,
	  receiver_id,
	  batch_id,
	  artikul,
	  row_stock_id,
	  esn_id,
	  prep_id,
	  prep_name,
	  firm_id,
	  firm_name,
	  country_name,
	  unpacked_cnt,
	  all_cnt,
	  price,
	  valid_date,
	  series,
	  is_vital,
	  barcode,
	  price_firm,
	  price_gr,
	  percent_gross,
	  price_gross,
	  sum_gross,
	  percent_nds_gross,
	  price_nds_gross,
	  sum_nds_gross,
	  percent,
	  sum,
	  supplier_name,
	  supplier_doc_num,
	  gr_date,
	  farm_group_name,
	  supplier_doc_date,
	  profit,
	  mess,
	  state,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user,
	  client_id,
	  sales_id,
	  depart_id,
	  depart_name,
	  pharmacy_name,
	  batch_num,
	  part_cnt,
	  part_num,
	  row_cnt,
	  batch_crt_date,
	  client_name,
	  sales_name,
	  depart_address,
	  is_active,
	  batch_inactive_date,
	  batch_is_confirmed,
	  batch_confirm_date
	)
	select
	  stock_id,
	  _receiver_id,
	  batch_id,
	  artikul,
	  row_stock_id,
	  esn_id,
	  prep_id,
	  prep_name,
	  firm_id,
	  firm_name,
	  country_name,
	  unpacked_cnt,
	  all_cnt,
	  price,
	  valid_date,
	  series,
	  is_vital,
	  barcode,
	  price_firm,
	  price_gr,
	  percent_gross,
	  price_gross,
	  sum_gross,
	  percent_nds_gross,
	  price_nds_gross,
	  sum_nds_gross,
	  percent,
	  sum,
	  supplier_name,
	  supplier_doc_num,
	  gr_date,
	  farm_group_name,
	  supplier_doc_date,
	  profit,
	  mess,
	  state,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user,
	  client_id,
	  sales_id,
	  depart_id,
	  depart_name,
	  pharmacy_name,
	  batch_num,
	  part_cnt,
	  part_num,
	  row_cnt,
	  batch_crt_date,
	  client_name,
	  sales_name,
	  depart_address,
	  is_active,
	  batch_inactive_date,
	  batch_is_confirmed,
	  batch_confirm_date	
	from und.vw_stock_row t1
	where t1.client_id = _client_id
	and t1.batch_confirm_date > _download_batch_date_beg
	and t1.batch_confirm_date <= _download_batch_date_end
	/*and (((t1.state != 1) and (_force_all = 1)) or (_force_all != 1))*/
	/*and t1.state != 1*/
	and t1.is_active = false
	and t1.batch_is_confirmed = true;

	-- часть 2 из 2: расчет контрольной суммы по текущему полному сводному наличию клиента

	RETURN QUERY
	WITH cte1 AS (
	select t1.row_stock_id, t1.stock_id, t1.all_cnt
	from und.vw_stock_row t1
	where 1=1	
	and t1.client_id = _client_id
	-- and t1.batch_confirm_date > _download_batch_date_beg
	and t1.batch_confirm_date <= _download_batch_date_end
	and t1.state != 1 	
	and t1.is_active = false	
	and t1.batch_is_confirmed = true
	)
	select md5(string_agg(to_char(trunc(coalesce(t1.all_cnt,0),3), 'FM9999999999990.000'), ';' ORDER BY t1.stock_id,t1.row_stock_id)) as checksum
	, count(t1.*)::integer as cnt
	from cte1 t1
	;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

ALTER FUNCTION und.prepare_vw_stock_row_tmp(integer, integer, timestamp without time zone, timestamp without time zone, integer) OWNER TO pavlov;
