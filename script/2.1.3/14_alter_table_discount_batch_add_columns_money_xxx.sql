﻿/*
	ALTER TABLE discount.batch ADD COLUMNs money_xxx
*/

ALTER TABLE discount.batch ADD COLUMN money_add boolean NOT NULL DEFAULT false;
ALTER TABLE discount.batch ADD COLUMN money_add_value numeric;
ALTER TABLE discount.batch ADD COLUMN money_add_mode integer NOT NULL DEFAULT 0;
