﻿/*
	CREATE TABLE und.stock_chain
*/

-- DROP TABLE und.stock_chain;

CREATE TABLE und.stock_chain
(
  chain_id serial NOT NULL,
  stock_id integer NOT NULL,
  is_upload boolean NOT NULL DEFAULT false,
  is_active boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT stock_chain_pkey PRIMARY KEY (chain_id),
  CONSTRAINT stock_chain_stock_id_fkey FOREIGN KEY (stock_id)
      REFERENCES und.stock (stock_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_chain OWNER TO pavlov;

CREATE TRIGGER und_stock_chain_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_chain
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
