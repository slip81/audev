﻿
DROP FUNCTION und.set_stock_row_batch(integer);

CREATE OR REPLACE FUNCTION und.set_stock_row_batch(
	IN _new_batch_id integer,
	OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN
	result:= 0;	
	
	update und.stock_row 
	set batch_id = new_batch_id, new_batch_id = null
	, all_cnt = new_all_cnt, new_all_cnt = null
	, state = new_state, new_state = 0
	where coalesce(new_batch_id, 0) = _new_batch_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.set_stock_row_batch(integer) OWNER TO pavlov;
