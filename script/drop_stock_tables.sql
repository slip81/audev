﻿
DROP VIEW und.vw_stock_row;
DROP VIEW und.vw_stock_sales_download;
DROP VIEW und.vw_stock_log;
DROP VIEW und.vw_stock_batch;
DROP VIEW und.vw_stock;

DROP TABLE und.vw_stock_row_tmp;
DROP TABLE und.stock_chain;
DROP TABLE und.stock_row;
DROP TABLE und.stock_sales_del;
DROP TABLE und.stock_sales_download;
DROP TABLE und.stock_batch;
DROP TABLE und.stock;

--------------------------------------------

CREATE TABLE und.stock
(
  stock_id serial NOT NULL,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  depart_id integer NOT NULL,
  depart_name character varying,
  pharmacy_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  depart_address character varying,
  last_batch_id integer,
  last_batch_date timestamp without time zone,
  last_download_id integer,
  last_download_date timestamp without time zone,
  last_del_date timestamp without time zone,
  CONSTRAINT stock_pkey PRIMARY KEY (stock_id),
  CONSTRAINT stock_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT stock_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock OWNER TO pavlov;

CREATE TRIGGER und_stock_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

--------------------------------------------

CREATE TABLE und.stock_batch
(
  batch_id serial NOT NULL,
  stock_id integer NOT NULL,
  batch_num integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  is_active boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  inactive_date timestamp without time zone,
  is_confirmed boolean NOT NULL DEFAULT false,
  confirm_date timestamp without time zone,
  is_new_version boolean NOT NULL DEFAULT false,
  CONSTRAINT stock_batch_pkey PRIMARY KEY (batch_id),
  CONSTRAINT stock_batch_stock_id_fkey FOREIGN KEY (stock_id)
      REFERENCES und.stock (stock_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_batch OWNER TO pavlov;

CREATE TRIGGER und_stock_batch_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_batch
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

--------------------------------------------

CREATE TABLE und.stock_sales_download
(
  download_id serial NOT NULL,
  sales_id integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  is_active boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  download_batch_date_beg timestamp without time zone,
  download_batch_date_end timestamp without time zone,
  batch_row_cnt integer NOT NULL DEFAULT 100,
  skip_row_cnt integer,
  new_row_cnt integer,
  force_all boolean NOT NULL DEFAULT false,
  inactive_date timestamp without time zone,
  is_confirmed boolean NOT NULL DEFAULT false,
  confirm_date timestamp without time zone,
  checksum character varying,
  last_batch_id integer,
  is_fake boolean NOT NULL DEFAULT false,
  checksum_cnt integer,
  is_new_version boolean NOT NULL DEFAULT false,
  CONSTRAINT stock_download_pkey PRIMARY KEY (download_id),
  CONSTRAINT stock_sales_download_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_sales_download  OWNER TO pavlov;

CREATE TRIGGER und_stock_sales_download_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_sales_download
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

--------------------------------------------

CREATE TABLE und.stock_sales_del
(
  del_id serial NOT NULL,
  source_sales_id integer NOT NULL,
  sales_id integer NOT NULL,
  row_cnt bigint,
  is_received boolean NOT NULL DEFAULT false,
  received_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT stock_sales_del_pkey PRIMARY KEY (del_id),
  CONSTRAINT stock_sales_del_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_sales_del  OWNER TO pavlov;

CREATE TRIGGER und_stock_sales_del_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_sales_del
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

--------------------------------------------

CREATE TABLE und.stock_row
(
  row_id serial NOT NULL,
  stock_id integer NOT NULL,
  batch_id integer,
  artikul integer NOT NULL,
  row_stock_id integer NOT NULL,
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  country_name character varying,
  unpacked_cnt integer,
  all_cnt numeric,
  price numeric,
  valid_date date,
  series character varying,
  is_vital boolean NOT NULL DEFAULT false,
  barcode character varying,
  price_firm numeric,
  price_gr numeric,
  percent_gross numeric,
  price_gross numeric,
  sum_gross numeric,
  percent_nds_gross numeric,
  price_nds_gross numeric,
  sum_nds_gross numeric,
  percent numeric,
  sum numeric,
  supplier_name character varying,
  supplier_doc_num character varying,
  gr_date date,
  farm_group_name character varying,
  supplier_doc_date date,
  profit numeric,
  mess character varying,
  state smallint NOT NULL DEFAULT 0,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  new_batch_id integer,
  new_all_cnt numeric,
  new_state smallint NOT NULL DEFAULT 0,
  CONSTRAINT stock_row_pkey PRIMARY KEY (row_id),
  CONSTRAINT stock_row_batch_id_fkey FOREIGN KEY (batch_id)
      REFERENCES und.stock_batch (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT stock_row_stock_id_fkey FOREIGN KEY (stock_id)
      REFERENCES und.stock (stock_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_row  OWNER TO pavlov;

CREATE INDEX und_stock_row_new_batch_id1_idx
  ON und.stock_row
  USING btree
  (new_batch_id);

CREATE INDEX und_stock_row_state1_idx
  ON und.stock_row
  USING btree
  (state)
  WHERE state <> 1;

CREATE INDEX und_stock_row_stock_id_is_deleted1_idx
  ON und.stock_row
  USING btree
  (stock_id, is_deleted);

CREATE TRIGGER und_stock_row_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_row
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
  
--------------------------------------------
  
CREATE TABLE und.stock_chain
(
  chain_id serial NOT NULL,
  stock_id integer NOT NULL,
  is_upload boolean NOT NULL DEFAULT false,
  is_active boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT stock_chain_pkey PRIMARY KEY (chain_id),
  CONSTRAINT stock_chain_stock_id_fkey FOREIGN KEY (stock_id)
      REFERENCES und.stock (stock_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_chain OWNER TO pavlov;

CREATE TRIGGER und_stock_chain_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_chain
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();  

--------------------------------------------

CREATE TABLE und.vw_stock_row_tmp
(
  row_id serial NOT NULL,
  stock_id integer NOT NULL,
  receiver_id integer NOT NULL,
  batch_id integer,
  artikul integer NOT NULL,
  row_stock_id integer NOT NULL,
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  country_name character varying,
  unpacked_cnt integer,
  all_cnt numeric,
  price numeric,
  valid_date date,
  series character varying,
  is_vital boolean NOT NULL DEFAULT false,
  barcode character varying,
  price_firm numeric,
  price_gr numeric,
  percent_gross numeric,
  price_gross numeric,
  sum_gross numeric,
  percent_nds_gross numeric,
  price_nds_gross numeric,
  sum_nds_gross numeric,
  percent numeric,
  sum numeric,
  supplier_name character varying,
  supplier_doc_num character varying,
  gr_date date,
  farm_group_name character varying,
  supplier_doc_date date,
  profit numeric,
  mess character varying,
  state smallint NOT NULL DEFAULT 0,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  depart_id integer NOT NULL,
  depart_name character varying,
  pharmacy_name character varying,
  batch_num integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  batch_crt_date timestamp without time zone,
  client_name character varying,
  sales_name character varying,
  depart_address character varying,
  is_active boolean NOT NULL,
  batch_inactive_date timestamp without time zone,
  batch_is_confirmed boolean NOT NULL DEFAULT false,
  batch_confirm_date timestamp without time zone,
  CONSTRAINT vw_stock_row_tmp_pkey PRIMARY KEY (row_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vw_stock_row_tmp OWNER TO pavlov;

CREATE INDEX und_vw_stock_row_tmp_receiver_id1_idx
  ON und.vw_stock_row_tmp
  USING btree
  (receiver_id);

--------------------------------------------

CREATE OR REPLACE VIEW und.vw_stock AS 
 SELECT t1.stock_id,
    t1.client_id,
    t1.sales_id,
    t1.depart_id,
    t1.depart_name,
    t1.pharmacy_name,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.name AS client_name,
    t3.adress AS sales_name,
    t1.depart_address
   FROM und.stock t1
     JOIN cabinet.client t2 ON t1.client_id = t2.id
     JOIN cabinet.sales t3 ON t1.sales_id = t3.id;

ALTER TABLE und.vw_stock OWNER TO pavlov;

--------------------------------------------

CREATE OR REPLACE VIEW und.vw_stock_batch AS 
 SELECT t1.batch_id,
    t1.stock_id,
    t1.batch_num,
    t1.part_cnt,
    t1.part_num,
    t1.row_cnt,
    t1.is_active,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.client_id,
    t2.sales_id,
    t2.depart_id,
    t2.depart_name,
    t2.pharmacy_name,
    t3.name AS client_name,
    t4.adress AS sales_name,
    t1.inactive_date,
    t1.is_confirmed,
    t1.confirm_date
   FROM und.stock_batch t1
     JOIN und.stock t2 ON t1.stock_id = t2.stock_id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
     JOIN cabinet.sales t4 ON t2.sales_id = t4.id
  WHERE t1.is_deleted = false;

ALTER TABLE und.vw_stock_batch OWNER TO pavlov;

--------------------------------------------


CREATE OR REPLACE VIEW und.vw_stock_log AS 
 SELECT t1.client_id,
    t1.client_name,
    t1.sales_id,
    t1.sales_name,
    t11.crt_date AS last_upl_date,
    t12.crt_date AS last_downl_date,
    t13.crt_date AS last_del_date
   FROM cabinet.vw_client_service t1
     LEFT JOIN ( SELECT t2.sales_id,
            max(t1_1.crt_date) AS crt_date
           FROM und.stock_batch t1_1
             JOIN und.stock t2 ON t1_1.stock_id = t2.stock_id
          GROUP BY t2.sales_id) t11 ON t1.sales_id = t11.sales_id
     LEFT JOIN ( SELECT downl.sales_id,
            max(downl.crt_date) AS crt_date
           FROM und.stock_sales_download downl
          GROUP BY downl.sales_id) t12 ON t1.sales_id = t12.sales_id
     LEFT JOIN ( SELECT t1_1.obj_id AS sales_id,
            max(t1_1.date_beg) AS crt_date
           FROM esn.log_esn t1_1
          WHERE t1_1.log_esn_type_id = 31
          GROUP BY t1_1.obj_id) t13 ON t1.sales_id = t12.sales_id
  WHERE t1.service_id = 65;

ALTER TABLE und.vw_stock_log OWNER TO pavlov;

--------------------------------------------

CREATE OR REPLACE VIEW und.vw_stock_sales_download AS 
 SELECT t1.download_id,
    t1.sales_id,
    t1.part_cnt,
    t1.part_num,
    t1.row_cnt,
    t1.is_active,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.download_batch_date_beg,
    t1.download_batch_date_end,
    t1.batch_row_cnt,
    t2.adress AS sales_name,
    t3.id AS client_id,
    t3.name AS client_name,
    t1.skip_row_cnt,
    t1.new_row_cnt,
    t1.force_all,
    t1.inactive_date,
    t1.is_confirmed,
    t1.confirm_date
   FROM und.stock_sales_download t1
     JOIN cabinet.sales t2 ON t1.sales_id = t2.id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
  WHERE t1.is_deleted = false;

ALTER TABLE und.vw_stock_sales_download OWNER TO pavlov;

--------------------------------------------

CREATE OR REPLACE VIEW und.vw_stock_row AS 
 SELECT t1.row_id,
    t1.stock_id,
    t1.batch_id,
    t1.artikul,
    t1.row_stock_id,
    t1.esn_id,
    t1.prep_id,
    t1.prep_name,
    t1.firm_id,
    t1.firm_name,
    t1.country_name,
    t1.unpacked_cnt,
    t1.all_cnt,
    t1.price,
    t1.valid_date,
    t1.series,
    t1.is_vital,
    t1.barcode,
    t1.price_firm,
    t1.price_gr,
    t1.percent_gross,
    t1.price_gross,
    t1.sum_gross,
    t1.percent_nds_gross,
    t1.price_nds_gross,
    t1.sum_nds_gross,
    t1.percent,
    t1.sum,
    t1.supplier_name,
    t1.supplier_doc_num,
    t1.gr_date,
    t1.farm_group_name,
    t1.supplier_doc_date,
    t1.profit,
    t1.mess,
    t1.state,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.client_id,
    t2.sales_id,
    t2.depart_id,
    t2.depart_name,
    t2.pharmacy_name,
    t3.batch_num,
    t3.part_cnt,
    t3.part_num,
    t3.row_cnt,
    t3.crt_date AS batch_crt_date,
    t4.name AS client_name,
    t5.adress AS sales_name,
    t2.depart_address,
    t3.is_active,
    t3.inactive_date AS batch_inactive_date,
    t3.is_confirmed AS batch_is_confirmed,
    t3.confirm_date AS batch_confirm_date
   FROM und.stock_row t1
     JOIN und.stock t2 ON t1.stock_id = t2.stock_id
     JOIN und.stock_batch t3 ON t1.batch_id = t3.batch_id
     JOIN cabinet.client t4 ON t2.client_id = t4.id
     JOIN cabinet.sales t5 ON t2.sales_id = t5.id
  WHERE t1.is_deleted <> true;

ALTER TABLE und.vw_stock_row OWNER TO pavlov;

--------------------------------------------

