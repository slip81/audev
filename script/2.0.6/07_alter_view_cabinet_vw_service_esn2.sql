﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_service_esn2
*/

-- DROP VIEW cabinet.vw_service_esn2;

CREATE OR REPLACE VIEW cabinet.vw_service_esn2 AS 
 SELECT t1.item_id,
    t1.workplace_id,
    t1.service_id,
    t1.comp_name,
    t2.registration_key,
    t3.id AS sales_id,
    t3.client_id,
    t3.adress AS sales_name,
    t4.name AS client_name,
    t2.workplace_type
   FROM cabinet.service_esn2 t1
     JOIN cabinet.workplace t2 ON t1.workplace_id = t2.id AND t2.is_deleted <> 1
     JOIN cabinet.sales t3 ON t2.sales_id = t3.id AND t3.is_deleted <> 1
     JOIN cabinet.client t4 ON t3.client_id = t4.id AND t4.is_deleted <> 1;

ALTER TABLE cabinet.vw_service_esn2 OWNER TO pavlov;
