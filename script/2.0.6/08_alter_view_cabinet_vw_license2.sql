﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_license2
*/

-- DROP VIEW cabinet.vw_license2;

CREATE OR REPLACE VIEW cabinet.vw_license2 AS 
 SELECT row_number() OVER (ORDER BY t1.id) AS id,
    t3.client_id,
    t1.id AS license_id,
    t1.dt_start,
    t1.dt_end,
    t1.service_registration_id,
    t4.activation_key,
    t5.id AS workplace_id,
    t5.registration_key,
    t5.sales_id,
    t2.id AS order_item_id,
    t2.service_id,
    t2.version_id,
    t2.quantity,
    t3.id AS order_id,
    t3.is_conformed,
    t3.active_status,
    t11.name AS user_name,
    t5.workplace_type
   FROM cabinet.license t1
     JOIN cabinet.order_item t2 ON t1.order_item_id = t2.id
     JOIN cabinet."order" t3 ON t2.order_id = t3.id
     JOIN cabinet.service_registration t4 ON t1.service_registration_id = t4.id
     JOIN cabinet.workplace t5 ON t4.workplace_id = t5.id
     JOIN cabinet.sales t6 ON t5.sales_id = t6.id
     LEFT JOIN cabinet.my_aspnet_users t11 ON t6.user_id = t11.id
  WHERE t1.is_deleted <> 1;

ALTER TABLE cabinet.vw_license2 OWNER TO pavlov;
