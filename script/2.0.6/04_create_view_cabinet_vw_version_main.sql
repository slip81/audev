﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_version_main
*/

-- DROP VIEW cabinet.vw_version_main;

CREATE OR REPLACE VIEW cabinet.vw_version_main AS 
SELECT row_number() over (ORDER BY version_main_name) as version_main_id,
  x1.version_main_name
  FROM
  (
   SELECT DISTINCT substr(t1.name, 1, 3) as version_main_name
   FROM cabinet.version t1
   WHERE t1.service_id = 1
  ) x1;

ALTER TABLE cabinet.vw_version_main OWNER TO pavlov;
