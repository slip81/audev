﻿/*
	CREATE OR REPLACE FUNCTION discount.update_card_discount_percent_synergia
*/

-- DROP FUNCTION discount.update_card_discount_percent_synergia();

CREATE OR REPLACE FUNCTION discount.update_card_discount_percent_synergia(
    OUT result integer)
  RETURNS integer AS
$BODY$
DECLARE
  _card_id bigint;
BEGIN	
	/*
	Срок действия накопительной скидки – 12 мес., т.е. каждые 12 мес. должно происходить переподтверждение размера % дисконта по установленным порогам. 
	Пример: если человек имеет скидку по карте - 4%, но на 01 января текущего года осуществил покупок в предыдущем году на 10000 руб., 
	то должен произойти переход размера дисконта на меньший %, если на 12001, то останется 4%.	
	*/

	result := 0;
	
	FOR _card_id IN 
		select distinct t1.card_id
		from discount.vw_card t1
		inner join discount.card_transaction t2 on t1.card_id = t2.card_id
		and t2.trans_kind = 1 and t2.status = 0
		and t2.date_check >= '2017-01-01 00:00:00'
		and t2.date_check <= '2018-01-01 00:00:00'
		where t1.business_id = 1180339545837668336
		and t1.curr_discount_percent = 4
		group by t1.card_id
		having sum(t2.trans_sum) <= 12000
	LOOP
		UPDATE discount.card_nominal
		SET date_end = current_date
		WHERE card_id = _card_id
		AND unit_type = 2
		AND unit_value = 4
		AND date_end IS NULL;

		INSERT INTO discount.card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
		VALUES (_card_id, current_date, 2, 3, current_timestamp, 'pavlov');		
		
	END LOOP;

	FOR _card_id IN 
		select distinct t1.card_id
		from discount.vw_card t1
		inner join discount.card_transaction t2 on t1.card_id = t2.card_id
		and t2.trans_kind = 1 and t2.status = 0
		and t2.date_check >= '2017-01-01 00:00:00'
		and t2.date_check <= '2018-01-01 00:00:00'
		where t1.business_id = 1180339545837668336
		and t1.curr_discount_percent = 5
		group by t1.card_id
		having sum(t2.trans_sum) <= 18000
	LOOP
		UPDATE discount.card_nominal
		SET date_end = current_date
		WHERE card_id = _card_id
		AND unit_type = 2
		AND unit_value = 5
		AND date_end IS NULL;

		INSERT INTO discount.card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
		VALUES (_card_id, current_date, 2, 4, current_timestamp, 'pavlov');		
		
	END LOOP;	

	result:= 1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.update_card_discount_percent_synergia() OWNER TO pavlov;
