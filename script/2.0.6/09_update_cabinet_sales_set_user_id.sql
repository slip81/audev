﻿/*
	update cabinet.sales set user_id
*/

update cabinet.sales
set user_id = (select max(x1.user_id) from cabinet.vw_client_user x1 where x1.sales_id = cabinet.sales.id and x1.is_main_for_sales = true)
where not exists (select x1.user_id from cabinet.vw_client_user x1 where x1.sales_id = cabinet.sales.id and x1.user_id = cabinet.sales.user_id and x1.is_main_for_sales = true)