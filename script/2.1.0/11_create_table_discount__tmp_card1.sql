﻿/*
	CREATE TABLE discount._tmp_card1
*/

-- DROP TABLE discount._tmp_card1;

CREATE TABLE discount._tmp_card1
(
  card_id bigint NOT NULL,
  CONSTRAINT _tmp_card1_pkey PRIMARY KEY (card_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount._tmp_card1 OWNER TO pavlov;
