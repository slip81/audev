﻿/*
Дисконтный сервис. Каменская аптека. Вернуть старые проценты скидки старым клиентам. Проставить старые значения процентов скидок на картах, которые были до январских изменений. Только тем картам, по которым были продажи до 1 января 2018 года. Старые значения скидок: 2, 3, 5, 6, 7 процентов (новые значения: 1, 2, 3, 5, 7 процентов)

Карты, у которых нет продаж до 9.01 - новые
Все остальные карты - старые

1) Выделить старые карты
2) Присвоить старым картам тип карты = 158
3) Пересчитать у старых карт процент скидки:
0 - 10 000:		2
10 000 - 30 000:	3
30 000 - 60 000:	5
60 000 - 100 000:	6
> 100 000:		7
*/

select * from discount.business;
-- 1408627091141821514

select * from discount.card_type where business_id = 1408627091141821514 order by card_type_id
-- "Дисконтная накопительная (до 09.01.2018)"
-- 158

-- "Дисконтная накопительная (после 09.01.2018)"
-- 79

delete from discount._tmp_card1;

-- 1) Выделить старые карты
insert into discount._tmp_card1 (card_id)
select t2.card_id
from discount.card t2
where t2.business_id = 1408627091141821514
and t2.curr_card_type_id = 79
and t2.card_id not in (
-- Карты, у которых нет продаж до 9.01
select t1.card_id
from discount.card t1
where t1.business_id = 1408627091141821514
and t1.curr_card_type_id = 79
and not exists(select x1.card_trans_id from discount.card_transaction x1 
where x1.card_id = t1.card_id
and coalesce(x1.status, 0) = 0 and x1.trans_kind = 1
and x1.date_check <= '2018-01-09'
)
);

-- select count(*) from discount._tmp_card1;
-- 11404

-- 2) Присвоить старым картам тип карты = 158
update discount.card_card_type_rel
set date_end = '2018-03-12'
where card_type_id = 79
and card_id in (select card_id from discount._tmp_card1);

insert into discount.card_card_type_rel (card_id, card_type_id, date_beg, date_end)
select t1.card_id, 158, '2018-03-12', null
from discount._tmp_card1 t1;

update discount.card
set curr_card_type_id = 158
where card_id in (select card_id from discount._tmp_card1);

-- 3) Пересчитать у старых карт процент скидки
update discount.card_nominal
set date_end = '2018-03-12'
where unit_type = 2
and card_id in (
select t1.card_id 
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 0 and t2.curr_trans_sum < 10000
)
;

insert into discount.card_nominal (card_id, date_beg, date_end, unit_type, unit_value, crt_date, crt_user)
select t1.card_id, '2018-03-12', null, 2, 2, current_timestamp, 'pavlov'
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 0 and t2.curr_trans_sum < 10000;

update discount.card_nominal
set date_end = '2018-03-12'
where unit_type = 2
and card_id in (
select t1.card_id 
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 10000 and t2.curr_trans_sum < 30000
)
;

insert into discount.card_nominal (card_id, date_beg, date_end, unit_type, unit_value, crt_date, crt_user)
select t1.card_id, '2018-03-12', null, 2, 3, current_timestamp, 'pavlov'
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 10000 and t2.curr_trans_sum < 30000;

update discount.card_nominal
set date_end = '2018-03-12'
where unit_type = 2
and card_id in (
select t1.card_id 
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 30000 and t2.curr_trans_sum < 60000
)
;

insert into discount.card_nominal (card_id, date_beg, date_end, unit_type, unit_value, crt_date, crt_user)
select t1.card_id, '2018-03-12', null, 2, 5, current_timestamp, 'pavlov'
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 30000 and t2.curr_trans_sum < 60000;

update discount.card_nominal
set date_end = '2018-03-12'
where unit_type = 2
and card_id in (
select t1.card_id 
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 60000 and t2.curr_trans_sum < 100000
)
;

insert into discount.card_nominal (card_id, date_beg, date_end, unit_type, unit_value, crt_date, crt_user)
select t1.card_id, '2018-03-12', null, 2, 6, current_timestamp, 'pavlov'
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 60000 and t2.curr_trans_sum < 100000;

update discount.card_nominal
set date_end = '2018-03-12'
where unit_type = 2
and card_id in (
select t1.card_id 
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 100000
)
;

insert into discount.card_nominal (card_id, date_beg, date_end, unit_type, unit_value, crt_date, crt_user)
select t1.card_id, '2018-03-12', null, 2, 7, current_timestamp, 'pavlov'
from discount._tmp_card1 t1
inner join discount.card t2 on t1.card_id = t2.card_id
and t2.curr_trans_sum >= 100000;