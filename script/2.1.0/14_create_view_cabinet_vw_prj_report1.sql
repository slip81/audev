﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_report1 AS
*/

-- DROP VIEW cabinet.vw_prj_report1;

CREATE OR REPLACE VIEW cabinet.vw_prj_report1 AS 

select row_number() OVER (ORDER BY x1.gr, x1.task_num) AS id,
x1.*,
count(x1.*) over(partition by x1.gr) as gr_cnt,
sum(case when x1.gr = 4 then 1 else 0 end) over(partition by null) as done_cnt,
count(x1.*) over(partition by null) as cnt
from (
with cte1 as (
select t1.claim_id, t1.claim_num, t1.task_num, t1.work_num, t1.work_type_id, t1.work_type_name, t1.state_type_id, t1.state_type_name, t1.exec_user_id, t1.exec_user_name,
first_value(t1.exec_user_name) over(partition by t1.claim_id, t1.task_num order by case when t1.state_type_id in (1,4) then 0 else 1 end) as active_first_exec_user_name,
t1.date_end,
max(t1.date_end) over(partition by t1.claim_id, t1.task_num) as done_last_date_end,
count(*) over(partition by t1.claim_id, t1.task_num) as work_cnt,
sum(case when t1.state_type_id in (1,4) then 1 else 0 end) over(partition by t1.claim_id, t1.task_num) as active_work_cnt,
sum(case when t1.state_type_id in (1,4) and t1.work_type_id = 3 then 1 else 0 end) over(partition by t1.claim_id, t1.task_num) as active_progr_work_cnt,
sum(case when t1.state_type_id in (1,4) and t1.work_type_id = 4 then 1 else 0 end) over(partition by t1.claim_id, t1.task_num) as active_test_work_cnt,
sum(case when t1.state_type_id in (1,4) and t1.work_type_id not in (3,4) then 1 else 0 end) over(partition by t1.claim_id, t1.task_num) as active_other_work_cnt,
sum(case when t1.state_type_id in (2,3) then 1 else 0 end) over(partition by t1.claim_id, t1.task_num) as done_work_cnt,
sum(case when t1.state_type_id in (2,3) and t1.work_type_id = 3 then 1 else 0 end) over(partition by t1.claim_id, t1.task_num) as done_progr_work_cnt,
sum(case when t1.state_type_id in (2,3) and t1.work_type_id = 4 then 1 else 0 end) over(partition by t1.claim_id, t1.task_num) as done_test_work_cnt,
sum(case when t1.state_type_id in (2,3) and t1.work_type_id not in (3,4) then 1 else 0 end) over(partition by t1.claim_id, t1.task_num) as done_other_work_cnt
from cabinet.vw_prj_work t1 
inner join cabinet.vw_prj_claim t2 on t1.claim_id = t2.claim_id and t2.project_id = 1 and t2.version_type_id = 3
)
-- задания на тестировании (есть активная работа с типом "Тестирование")
select distinct 1::int as gr, 'на тестировании' as gr_name, claim_num || '/' || task_num as task_num, active_first_exec_user_name, null::timestamp without time zone as done_last_date_end
from cte1
where active_test_work_cnt > 0
union all
-- задания на доработке (есть активная работа с типом "Программирование" и выполненная работа с типом "Тестирование")
select distinct 2::int as gr, 'передано на доработку' as gr_name, claim_num || '/' || task_num as task_num, active_first_exec_user_name, null::timestamp without time zone as done_last_date_end
from cte1
where active_test_work_cnt <= 0 and done_test_work_cnt > 0 and active_progr_work_cnt > 0
union all
-- задания в разработке (есть активная работа с типом "Программирование" и нет выполненных работ с типом "Тестирование")
select distinct 3::int as gr, 'в разработке' as gr_name, claim_num || '/' || task_num as task_num, active_first_exec_user_name, null::timestamp without time zone as done_last_date_end
from cte1
where done_test_work_cnt <= 0 and active_progr_work_cnt > 0
-- выполненные задания
union all
select distinct 4::int as gr, 'выполнено' as gr_name, claim_num || '/' || task_num as task_num, null::text as active_first_exec_user_name, done_last_date_end
from cte1
where active_work_cnt <= 0
) x1
;
;

ALTER TABLE cabinet.vw_prj_report1 OWNER TO pavlov;
