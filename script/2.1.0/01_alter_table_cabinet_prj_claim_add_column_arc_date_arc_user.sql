﻿/*
	ALTER TABLE cabinet.prj_claim ADD COLUMN arc_date, arc_user
*/

ALTER TABLE cabinet.prj_claim ADD COLUMN arc_date timestamp without time zone;
ALTER TABLE cabinet.prj_claim ADD COLUMN arc_user character varying;

UPDATE cabinet.prj_claim SET arc_date = upd_date, arc_user = upd_user WHERE is_archive = true;