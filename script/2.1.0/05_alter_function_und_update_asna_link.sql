﻿/*
	CREATE OR REPLACE FUNCTION und.update_asna_link
*/

-- DROP FUNCTION und.update_asna_link(integer);

CREATE OR REPLACE FUNCTION und.update_asna_link(_link_id integer)
  RETURNS void AS
$BODY$
DECLARE
 _client_id integer;
BEGIN	
	UPDATE und.asna_link_row t1
	SET is_unique = true
	WHERE t1.link_id = _link_id
	AND NOT EXISTS (
	SELECT x1.row_id
	FROM und.asna_link_row x1
	WHERE x1.link_id = t1.link_id
	AND x1.row_id != t1.row_id
	AND x1.asna_nnt = t1.asna_nnt
	--AND x1.asna_sku = t1.asna_sku
	)
	;

	-- !!!
	_client_id := (SELECT t2.client_id FROM und.asna_link t1
			INNER JOIN und.vw_asna_user t2 ON t1.asna_user_id = t2.asna_user_id
			WHERE t1.link_id = _link_id
			LIMIT 1);
	IF (_client_id = 457) THEN
		PERFORM discount.update_programm_param_from_asna(_client_id, 1725243837632218851, 15, 2);
		PERFORM discount.update_programm_param_from_asna(_client_id, 1725243837632218851, 16, 6);
		PERFORM discount.update_programm_param_from_asna(_client_id, 1725243837632218851, 17, 7);

		PERFORM discount.update_programm_param_from_asna(_client_id, 1725462584024368218, 9, 2);

		PERFORM discount.update_programm_param_from_asna(_client_id, 1725459918049249232, 10, 4);
		PERFORM discount.update_programm_param_from_asna(_client_id, 1725459918049249232, 11, 5);

		PERFORM discount.update_programm_param_from_asna(_client_id, 1725463591789790338, 12, 4);
		PERFORM discount.update_programm_param_from_asna(_client_id, 1725463591789790338, 13, 5);
		PERFORM discount.update_programm_param_from_asna(_client_id, 1725463591789790338, 14, 10);
	END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.update_asna_link(integer) OWNER TO pavlov;
