﻿/*
	CREATE OR REPLACE FUNCTION cabinet.fill_prj_user_group_fixed
*/

DROP FUNCTION cabinet.fill_prj_user_group_fixed(integer);

CREATE OR REPLACE FUNCTION cabinet.fill_prj_user_group_fixed()
  RETURNS void AS
$BODY$
DECLARE
 _user_id integer;
BEGIN
	-- добавляем все возможные значения unit_type 
	FOR _user_id IN SELECT t1.user_id FROM cabinet.cab_user t1 WHERE t1.is_active = true AND t1.is_crm_user = true
	LOOP

		DELETE FROM cabinet.prj_user_group_item WHERE group_id IN
		(SELECT group_id FROM cabinet.prj_user_group WHERE fixed_kind in (1,2,3,4) AND user_id = _user_id);

		INSERT INTO cabinet.prj_user_group_item (
		  group_id,
		  item_type_id,
		  claim_id,
		  ord,
		  task_id
		)
		SELECT
		  t1.group_id,
		  4,
		  t3.claim_id,
		  row_number() over (order by t3.task_id),
		  t3.task_id
		FROM cabinet.prj_user_group t1
		INNER JOIN cabinet.prj_task_user_state t2 ON t1.user_id = t2.user_id
		AND t2.is_current = true AND t2.state_type_id = 2
		INNER JOIN cabinet.prj_task t3 ON t2.task_id = t3.task_id
		WHERE t1.fixed_kind = 1 -- Принято в работу
		AND t1.user_id = _user_id;

		INSERT INTO cabinet.prj_user_group_item (
		  group_id,
		  item_type_id,
		  claim_id,
		  ord,
		  task_id
		)
		SELECT
		  t1.group_id,
		  4,
		  t3.claim_id,
		  row_number() over (order by t3.task_id),
		  t3.task_id
		FROM cabinet.prj_user_group t1
		INNER JOIN cabinet.prj_task_user_state t2 ON t1.user_id = t2.user_id
		AND t2.is_current = true AND t2.state_type_id = 1
		INNER JOIN cabinet.prj_task t3 ON t2.task_id = t3.task_id
		WHERE t1.fixed_kind = 2 -- Передано в работу
		AND t1.user_id = _user_id;

		INSERT INTO cabinet.prj_user_group_item (
		  group_id,
		  item_type_id,
		  claim_id,
		  ord,
		  task_id
		)
		SELECT
		  t1.group_id,
		  4,
		  t3.claim_id,
		  row_number() over (order by t3.task_id),
		  t3.task_id
		FROM cabinet.prj_user_group t1
		INNER JOIN cabinet.prj_task_user_state t2 ON t1.user_id = t2.user_id
		AND t2.is_current = true AND t2.state_type_id = 3
		AND t2.crt_date >= (current_date - integer '7')::timestamp without time zone -- за последние 7 дней максимум
		INNER JOIN cabinet.prj_task t3 ON t2.task_id = t3.task_id
		WHERE t1.fixed_kind = 3 -- Выполнено
		AND t1.user_id = _user_id;	

		INSERT INTO cabinet.prj_user_group_item (
		  group_id,
		  item_type_id,
		  claim_id,
		  ord,
		  task_id
		)
		SELECT
		  t1.group_id,
		  4,
		  t3.claim_id,
		  row_number() over (order by t3.task_id),
		  t3.task_id
		FROM cabinet.prj_user_group t1
		INNER JOIN cabinet.prj_task_user_state t2 ON t1.user_id = t2.user_id
		AND t2.is_current = true AND t2.state_type_id = 4
		AND t2.crt_date >= (current_date - integer '7')::timestamp without time zone -- за последние 7 дней максимум
		INNER JOIN cabinet.prj_task t3 ON t2.task_id = t3.task_id
		WHERE t1.fixed_kind = 4 -- Отменено
		AND t1.user_id = _user_id;	

	END LOOP;	
 
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.fill_prj_user_group_fixed() OWNER TO pavlov;
