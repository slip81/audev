﻿/*
	CREATE OR REPLACE FUNCTION discount.update_programm_param_from_asna
*/

-- DROP FUNCTION discount.update_programm_param_from_asna(integer, bigint, integer, integer);

CREATE OR REPLACE FUNCTION discount.update_programm_param_from_asna(
    IN _client_id integer,
    IN _programm_id bigint,
    IN _source_param_num integer,
    IN _target_param_num integer
    )
  RETURNS void AS
$BODY$
BEGIN			
	update discount.programm_param
	set string_value = coalesce((
	select array_to_string(array_agg(distinct t4.prep_name), '{|}'::text)
	from und.vw_asna_user t1
	inner join discount.programm_param t11 on t11.programm_id = _programm_id and t11.param_num = _source_param_num
	inner join und.asna_link t2 on t1.asna_user_id = t2.asna_user_id and t2.is_deleted = false
	inner join und.asna_link_row t3 on t2.link_id = t3.link_id 
	and t3.asna_nnt = ANY(string_to_array(t11.string_value, ',')::int[])
	and t3.is_deleted = false
	inner join und.asna_stock_row t4 on t1.asna_user_id = t4.asna_user_id and t3.asna_sku::text = t4.asna_sku::text
	and t4.is_deleted = false
	where t1.client_id = _client_id 
	and t1.is_deleted = false
	),'-')
	where programm_id = _programm_id
	and param_num = _target_param_num;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.update_programm_param_from_asna(integer, bigint, integer, integer) OWNER TO pavlov;
