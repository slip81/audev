﻿/*
	update cabinet.prj_user_settings where page_type_id = 9
*/

update cabinet.prj_user_settings
set settings = replace(settings, '{"id":"exec_user"', '{"id":"prj_user_group_fixed_kind1","val":"True"},{"id":"prj_user_group_fixed_kind2","val":"True"},{"id":"prj_user_group_fixed_kind3","val":null},{"id":"prj_user_group_fixed_kind4","val":null},{"id":"exec_user"')
where page_type_id = 9;
