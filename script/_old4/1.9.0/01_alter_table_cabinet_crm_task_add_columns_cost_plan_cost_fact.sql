﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMNs cost_plan, cost_fact
*/

ALTER TABLE cabinet.crm_task ADD COLUMN cost_plan numeric;
ALTER TABLE cabinet.crm_task ADD COLUMN cost_fact numeric;