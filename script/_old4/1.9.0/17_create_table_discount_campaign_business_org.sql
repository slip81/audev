﻿/*
	CREATE TABLE discount.campaign_business_org
*/

-- DROP TABLE discount.campaign_business_org;

DELETE FROM discount.campaign;

CREATE TABLE discount.campaign_business_org
(
  item_id serial NOT NULL,
  campaign_id int NOT NULL,
  org_id bigint NOT NULL,
  card_num_default character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope6(),
  CONSTRAINT campaign_business_org_pkey PRIMARY KEY (item_id),
  CONSTRAINT campaign_campaign_id_fkey FOREIGN KEY (campaign_id)
      REFERENCES discount.campaign (campaign_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT campaign_org_id_fkey FOREIGN KEY (org_id)
      REFERENCES discount.business_org (org_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.campaign_business_org OWNER TO pavlov;

-- DROP TRIGGER discount_campaign_business_org_set_stamp_trg ON discount.campaign_business_org;

CREATE TRIGGER discount_campaign_business_org_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON discount.campaign_business_org
  FOR EACH ROW
  EXECUTE PROCEDURE discount.set_stamp();
