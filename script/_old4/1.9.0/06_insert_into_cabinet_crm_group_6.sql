﻿/*
	insert into cabinet.crm_group 6
*/

-- select * from cabinet.crm_group order by group_id

insert into cabinet.crm_group (group_id, group_name)
values (6, 'Проекты');

-- select * from cabinet.crm_group_operation order by group_id, operation_id

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select 6, operation_id, operation_name
from cabinet.crm_group_operation
where group_id = 4;