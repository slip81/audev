﻿/*
	CREATE OR REPLACE VIEW discount.vw_campaign_business_org
*/

-- DROP VIEW discount.vw_campaign_business_org;

CREATE OR REPLACE VIEW discount.vw_campaign_business_org AS
select t1.campaign_id, t1.business_id, t2.org_id, t2.org_name, t3.item_id, t3.card_num_default, case when coalesce(t3.item_id, 0) <= 0 then false else true end as is_in_campaign
from discount.campaign t1
left join discount.business_org t2 on t1.business_id = t2.business_id
left join discount.campaign_business_org t3 on t1.campaign_id = t3.campaign_id and t2.org_id = t3.org_id;

ALTER TABLE discount.vw_campaign_business_org OWNER TO pavlov;
