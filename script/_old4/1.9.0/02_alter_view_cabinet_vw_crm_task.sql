﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_task
*/

-- DROP VIEW cabinet.vw_crm_task;

CREATE OR REPLACE VIEW cabinet.vw_crm_task AS 
 SELECT t1.task_id,
    t1.task_name,
    t1.task_text,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.module_version_id,
    t1.client_id,
    t1.priority_id,
    t1.state_id,
    t1.owner_user_id,
    t1.exec_user_id,
    ((t1.task_num::text ||
        CASE
            WHEN t1.rel_child_cnt > 0 THEN (' [M-'::text || t1.rel_child_cnt::character varying::text) || ']'::text
            ELSE ''::text
        END) ||
        CASE
            WHEN t1.rel_parent_cnt > 0 THEN (' [S-'::text || t1.rel_parent_cnt::character varying::text) || ']'::text
            ELSE ''::text
        END)::character varying AS task_num,
    t1.crt_date,
    t1.required_date,
    t1.repair_date,
    t1.repair_version_id,
    t2.client_name,
    t3.module_name,
    t4.module_part_name,
    t5.module_version_name,
    t6.priority_name,
    t7.project_name,
    t8.state_name,
    t9.user_name AS owner_user_name,
    t10.user_name AS exec_user_name,
    t21.module_version_name AS repair_version_name,
    replace(t1.task_num::text, 'AU-'::text, ''::text)::integer AS task_num_int,
    t1.group_id,
    t1.prepared_user_id,
    t1.fixed_user_id,
    t1.approved_user_id,
    t11.user_name AS prepared_user_name,
    t12.user_name AS fixed_user_name,
    t13.user_name AS approved_user_name,
    t1.assigned_user_id,
    t14.user_name AS assigned_user_name,
    t15.group_name,
    t1.rel_child_cnt,
    t1.rel_parent_cnt,
    t1.is_fin,
        CASE
            WHEN t1.is_fin THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_fin_name,
    t1.fin_cost,
        CASE
            WHEN t30.event_id IS NULL THEN false
            ELSE true
        END AS is_event,
        CASE
            WHEN t30.event_id IS NULL THEN ''::text
            ELSE 'К'::text
        END AS is_event_name,
    t31.exec_user_id AS event_user_id,
    t1.upd_date,
    t1.upd_num,
    t1.upd_user,
    t22.user_id AS view_user_id,
    t23.fore_color AS state_fore_color,
    t24.fore_color AS project_fore_color,
    t25.fore_color AS priority_fore_color,
    t1.moderator_user_id,
    t16.user_name AS moderator_user_name,
    t1.storage_folder_id,
    t26.folder_name AS storage_folder_name,
        CASE
            WHEN COALESCE(t1.storage_folder_id, 0) > 0 THEN true
            ELSE false
        END AS is_in_storage,
        CASE
            WHEN COALESCE(t1.storage_folder_id, 0) > 0 THEN 'Хр'::text
            ELSE ''::text
        END AS is_in_storage_name,
        CASE
            WHEN char_length(t1.task_text::text) <= 100 THEN "substring"(t1.task_text::text, 1, 100)
            ELSE "substring"(t1.task_text::text, 1, 97) || '...'::text
        END AS task_text_substring,
    t27.subtasks_text,
    t27.subtasks_task_id,
    t27.subtasks_count,
    t28.notes_text,
    t28.notes_task_id,
    t28.notes_count,
    t1.batch_id,
    t29.batch_name,
    t6.is_boss AS priority_is_boss,
    t8.for_event AS state_for_event,
    t31.date_beg AS event_date_beg,
    t31.date_end AS event_date_end,
    t1.is_control,
    t32.required_date AS required_date_orig,
    t32.repair_date AS repair_date_orig,
    t32.event_date_beg AS event_date_beg_orig,
    t32.event_date_end AS event_date_end_orig,
    t32.crt_date AS control_crt_date_orig,
    t32.crt_user AS control_crt_user_orig,
    t33.required_date AS required_date_curr,
    t33.repair_date AS repair_date_curr,
    t33.event_date_beg AS event_date_beg_curr,
    t33.event_date_end AS event_date_end_curr,
    t33.crt_date AS control_crt_date_curr,
    t33.crt_user AS control_crt_user_curr,
    t1.cost_plan,
    t1.cost_fact
   FROM cabinet.crm_task t1
     JOIN cabinet.crm_client t2 ON t1.client_id = t2.client_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id
     JOIN cabinet.crm_priority t6 ON t1.priority_id = t6.priority_id
     JOIN cabinet.crm_project t7 ON t1.project_id = t7.project_id
     JOIN cabinet.crm_state t8 ON t1.state_id = t8.state_id
     JOIN cabinet.cab_user t9 ON t1.owner_user_id = t9.user_id
     JOIN cabinet.cab_user t10 ON t1.exec_user_id = t10.user_id
     JOIN cabinet.cab_user t11 ON t1.prepared_user_id = t11.user_id
     JOIN cabinet.cab_user t12 ON t1.fixed_user_id = t12.user_id
     JOIN cabinet.cab_user t13 ON t1.approved_user_id = t13.user_id
     JOIN cabinet.cab_user t14 ON t1.assigned_user_id = t14.user_id
     JOIN cabinet.crm_group t15 ON t1.group_id = t15.group_id
     JOIN cabinet.cab_user t16 ON t1.moderator_user_id = t16.user_id
     LEFT JOIN cabinet.crm_module_version t21 ON t1.repair_version_id = t21.module_version_id
     LEFT JOIN cabinet.cab_user t22 ON 1 = 1
     LEFT JOIN cabinet.crm_state_user_settings t23 ON t1.state_id = t23.state_id AND t22.user_id = COALESCE(t23.user_id, t22.user_id)
     LEFT JOIN cabinet.crm_project_user_settings t24 ON t1.project_id = t24.project_id AND t22.user_id = COALESCE(t24.user_id, t22.user_id)
     LEFT JOIN cabinet.crm_priority_user_settings t25 ON t1.priority_id = t25.priority_id AND t22.user_id = COALESCE(t25.user_id, t22.user_id)
     LEFT JOIN cabinet.storage_folder t26 ON t1.storage_folder_id = t26.folder_id AND t26.is_deleted = false
     LEFT JOIN ( SELECT string_agg((('П'::text || x1.task_num::text) || ': '::text) || x1.subtask_text::text, '
'::text ORDER BY x1.task_num) AS subtasks_text,
            x1.task_id AS subtasks_task_id,
            count(x1.subtask_id) AS subtasks_count
           FROM cabinet.crm_subtask x1
          GROUP BY x1.task_id) t27 ON t1.task_id = t27.subtasks_task_id
     LEFT JOIN ( SELECT string_agg((((('К'::text || x1.note_num::text) || ' ['::text) || x1.user_name::text) || ']: '::text) || x1.note_text::text, '
'::text ORDER BY x1.note_num DESC) AS notes_text,
            x1.task_id AS notes_task_id,
            count(x1.note_id) AS notes_count
           FROM cabinet.vw_crm_task_note x1
          GROUP BY x1.task_id) t28 ON t1.task_id = t28.notes_task_id
     LEFT JOIN cabinet.crm_batch t29 ON t1.batch_id = t29.batch_id
     LEFT JOIN cabinet.planner_task_event t30 ON t1.task_id = t30.task_id AND t30.is_actual = true
     LEFT JOIN cabinet.planner_event t31 ON t30.event_id = t31.event_id
     LEFT JOIN cabinet.crm_task_control t32 ON t1.task_id = t32.task_id AND t1.is_control = true AND t32.is_orig = true
     LEFT JOIN cabinet.crm_task_control t33 ON t1.task_id = t33.task_id AND t1.is_control = true AND t33.is_orig = false
  WHERE t1.is_archive = false;

ALTER TABLE cabinet.vw_crm_task OWNER TO pavlov;
