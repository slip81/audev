﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_max_version
*/

-- DROP VIEW cabinet.vw_client_max_version;

CREATE OR REPLACE VIEW cabinet.vw_client_max_version AS 
 SELECT row_number() OVER (ORDER BY ( SELECT 0)) AS adr,
    t1.id,
    t1.name,
    t1.inn,
    t1.kpp,
    t2.id AS sales_id,
    t2.adress,
    0::int AS workplace_id,
    ''::text as description,
    ''::character varying(255) as registration_key,
    t4.service_name,
    t4.version_name
   FROM cabinet.client t1
     LEFT JOIN cabinet.sales t2 ON t1.id = t2.client_id AND t2.is_deleted = 0     
     LEFT JOIN ( SELECT max(rpad(replace(t1_1.version_name::text, '.'::text, ''::text), 6, '0'::text)::integer) AS version_name_int_max,
            max(t1_1.version_name::text) AS version_name,
            max(t1_1.service_name::text) AS service_name,
            t1_1.client_id,
            t1_1.sales_id,            
            t1_1.service_id
           FROM cabinet.vw_client_service_registered t1_1
          WHERE t1_1.version_id <> 86
          GROUP BY t1_1.client_id, t1_1.sales_id, t1_1.service_id) t4 ON t2.id = t4.sales_id
  WHERE t1.is_deleted = 0 AND t1.id <> 1000 AND t1.id <> 4990 AND t1.id <> 936;

ALTER TABLE cabinet.vw_client_max_version OWNER TO pavlov;
