﻿/*
	insert into cabinet.cab_grid_column 45, 46
*/


insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus)
values (45, 1, 'cost_plan', 'ТЗ план');

insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
select distinct 45, user_id, coalesce((select max(x1.column_num) from cabinet.cab_grid_column_user_settings x1 where x1.user_id = cabinet.cab_grid_column_user_settings.user_id), 0) + 1, false, 100, true, true
from cabinet.cab_grid_column_user_settings;

insert into cabinet.cab_grid_column (column_id, grid_id, column_name, column_name_rus)
values (46, 1, 'cost_fact', 'ТЗ факт');

insert into cabinet.cab_grid_column_user_settings (column_id, user_id, column_num, is_visible, width, is_filterable, is_sortable)
select distinct 46, user_id, coalesce((select max(x1.column_num) from cabinet.cab_grid_column_user_settings x1 where x1.user_id = cabinet.cab_grid_column_user_settings.user_id), 0) + 1, false, 100, true, true
from cabinet.cab_grid_column_user_settings;

-- select * from cabinet.cab_grid_column order by grid_id, column_id

-- select * from cabinet.cab_grid_column where grid_id = 1 order by column_id
