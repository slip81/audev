﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_task_filter
*/

-- DROP VIEW cabinet.vw_crm_task_filter;

CREATE OR REPLACE VIEW cabinet.vw_crm_task_filter AS 

select row_number() over(order by (select 0)) as adr, t2.*
from
(

select 1 as filter_group_id, 'Статусы' as filter_group_name, t1.state_id as filter_value_id, t1.state_name as filter_value_name, t1.cnt
from
(
select x1.state_id, x2.state_name, count(x1.task_id) as cnt
from cabinet.crm_task x1
inner join cabinet.crm_state x2 on x1.state_id = x2.state_id
where x1.is_archive = false
group by x1.state_id, x2.state_name
) t1

union all

select 2 as filter_group_id, 'Программы' as filter_group_name, t1.project_id as filter_value_id, t1.project_name as filter_value_name, t1.cnt
from
(
select x1.project_id, x2.project_name, count(x1.task_id) as cnt
from cabinet.crm_task x1
inner join cabinet.crm_project x2 on x1.project_id = x2.project_id
where x1.is_archive = false
group by x1.project_id, x2.project_name
) t1

union all

select 3 as filter_group_id, 'Проекты' as filter_group_name, t1.batch_id as filter_value_id, t1.batch_name as filter_value_name, t1.cnt
from
(
select x1.batch_id, x2.batch_name, count(x1.task_id) as cnt
from cabinet.crm_task x1
inner join cabinet.crm_batch x2 on x1.batch_id = x2.batch_id
where x1.is_archive = false
group by x1.batch_id, x2.batch_name
) t1

/*
union all

select 4 as filter_group_id, 'Клиенты' as filter_group_name, t1.client_id as filter_value_id, t1.client_name as filter_value_name, t1.cnt
from
(
select x1.client_id, x2.client_name, count(x1.task_id) as cnt
from cabinet.crm_task x1
inner join cabinet.crm_client x2 on x1.client_id = x2.client_id
where x1.is_archive = false
group by x1.client_id, x2.client_name
) t1
*/

union all

select 5 as filter_group_id, 'Приоритеты' as filter_group_name, t1.priority_id as filter_value_id, t1.priority_name as filter_value_name, t1.cnt
from
(
select x1.priority_id, x2.priority_name, count(x1.task_id) as cnt
from cabinet.crm_task x1
inner join cabinet.crm_priority x2 on x1.priority_id = x2.priority_id
where x1.is_archive = false
group by x1.priority_id, x2.priority_name
) t1

union all

select 6 as filter_group_id, 'Ответственные' as filter_group_name, t1.assigned_user_id as filter_value_id, t1.assigned_user_name as filter_value_name, t1.cnt
from
(
select x1.assigned_user_id, x2.user_name as assigned_user_name, count(x1.task_id) as cnt
from cabinet.crm_task x1
inner join cabinet.cab_user x2 on x1.assigned_user_id = x2.user_id
where x1.is_archive = false
group by x1.assigned_user_id, x2.user_name
) t1

union all

select 7 as filter_group_id, 'Исполнители' as filter_group_name, t1.exec_user_id as filter_value_id, t1.exec_user_name as filter_value_name, t1.cnt
from
(
select x1.exec_user_id, x2.user_name as exec_user_name, count(x1.task_id) as cnt
from cabinet.crm_task x1
inner join cabinet.cab_user x2 on x1.exec_user_id = x2.user_id
where x1.is_archive = false
group by x1.exec_user_id, x2.user_name
) t1

union all

select 8 as filter_group_id, 'Группы' as filter_group_name, t1.group_id as filter_value_id, t1.group_name as filter_value_name, t1.cnt
from
(
select x1.group_id, x2.group_name, count(x1.task_id) as cnt
from cabinet.crm_task x1
inner join cabinet.crm_group x2 on x1.group_id = x2.group_id
where x1.is_archive = false
group by x1.group_id, x2.group_name
) t1

) t2;

ALTER TABLE cabinet.vw_crm_task_filter OWNER TO pavlov;
