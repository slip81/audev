﻿/*
	insert into cabinet.service
*/

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, price, need_key, "NeedCheckVersionLicense", is_service, priority, have_spec, group_id)
values (62, '{163a935d-b8e9-4b59-801a-268c05e5e018}', 'Приказ 883', 'Еженедельная и ежемесячная отчетность в Росздравнадзор', 12, 0, 'opmon2', 0, 0, 0, 0, true, 31, false, 3);

insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('{fb6501b4-4c57-4458-8a8d-2d1678f317fc}', '1.0.0.0', '1.0.0.0', 62, 0, 0);

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, price, need_key, "NeedCheckVersionLicense", is_service, priority, have_spec, group_id)
values (63, '{163a935d-b8e9-4b59-801a-268c05e5e018}', 'Еженедельный мониторинг', 'Еженедельная и ежемесячная отчетность в Росздравнадзор', 12, 0, 'opmon3', 0, 0, 0, 0, true, 32, false, 3);

insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('{fb6501b4-4c57-4458-8a8d-2d1678f317fc}', '1.0.0.0', '1.0.0.0', 63, 0, 0);

-- select * from cabinet.service order by id desc
-- select * from cabinet.version order by service_id desc, id desc