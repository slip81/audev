﻿/*
	CREATE OR REPLACE FUNCTION discount.sysuidgen_scope6
*/

-- DROP FUNCTION discount.sysuidgen_scope6();

CREATE OR REPLACE FUNCTION discount.sysuidgen_scope6(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (6 << 10) | (nextval('discount.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.sysuidgen_scope6()  OWNER TO pavlov;
