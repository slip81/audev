﻿/*
	CREATE TABLE discount.campaign
*/

DROP TABLE discount.campaign;

CREATE TABLE discount.campaign
(
  campaign_id serial NOT NULL,
  business_id bigint NOT NULL,
  campaign_name character varying,
  campaign_descr character varying,
  campaign_check_text character varying,
  card_num_default character varying,
  date_beg date NOT NULL,
  date_end date NOT NULL,
  max_card_cnt int,
  card_type_id bigint,  
  auto_create_card boolean NOT NULL DEFAULT false,
  card_money_value numeric,
  state integer NOT NULL DEFAULT 0,
  mess character varying,
  issue_date_beg date NOT NULL,
  issue_date_end date NOT NULL,    
  issue_rule integer NOT NULL DEFAULT 0,
  issue_condition_type integer NOT NULL DEFAULT 0,
  issue_condition_sale_sum numeric,
  issue_condition_pos_count integer,
  apply_condition_sale_sum numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope6(),
  CONSTRAINT campaign_pkey PRIMARY KEY (campaign_id),
  CONSTRAINT campaign_business_id_fkey FOREIGN KEY (business_id)
      REFERENCES discount.business (business_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT campaign_card_type_id_fkey FOREIGN KEY (card_type_id)
      REFERENCES discount.card_type (card_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.campaign OWNER TO pavlov;

-- DROP TRIGGER discount_campaign_set_stamp_trg ON discount.campaign;

CREATE TRIGGER discount_campaign_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON discount.campaign
  FOR EACH ROW
  EXECUTE PROCEDURE discount.set_stamp();
