﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_max_version
*/

-- DROP VIEW cabinet.vw_client_max_version;

CREATE OR REPLACE VIEW cabinet.vw_client_max_version AS 
select row_number() over(order by (select 0)) as adr, t1.id, t1.name, t1.inn, t1.kpp
, t2.id as sales_id, t2.adress
, t3.id as workplace_id, t3.description, t3.registration_key
, t4.service_name, t4.version_name
from cabinet.client t1
left join cabinet.sales t2 on t1.id = t2.client_id and t2.is_deleted = 0
left join cabinet.workplace t3 on t2.id = t3.sales_id and t3.is_deleted = 0
left join
(
select max(rpad(replace(t1.version_name, '.', ''), 6, '0')::int) as version_name_int_max, max(t1.version_name) as version_name, max(t1.service_name) as service_name, 
t1.client_id, t1.sales_id, t1.workplace_id, t1.service_id
from cabinet.vw_client_service_registered t1
where t1.version_id != 86
group by t1.client_id, t1.sales_id, t1.workplace_id, t1.service_id
) t4 on t3.id = t4.workplace_id
where t1.is_deleted = 0
and t1.id != 1000
and t1.id != 4990
and t1.id != 936
--order by trim(t1.name), trim(t2.adress), t3.description, t3.registration_key, t4.service_name, t4.version_name
;

ALTER TABLE cabinet.vw_client_max_version OWNER TO pavlov;
