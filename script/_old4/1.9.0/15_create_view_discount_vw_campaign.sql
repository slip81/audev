﻿/*
	CREATE OR REPLACE VIEW discount.vw_campaign
*/

-- DROP VIEW discount.vw_campaign;

CREATE OR REPLACE VIEW discount.vw_campaign AS 
	SELECT 
		t1.campaign_id,  
		t1.business_id,
		t1.campaign_name,
		t1.campaign_descr,
		t1.campaign_check_text,
		t1.card_num_default,
		t1.date_beg,
		t1.date_end,
		t1.max_card_cnt,
		t1.card_type_id,
		t1.auto_create_card,
		t1.card_money_value,
		t1.state,
		t1.mess,
		t1.issue_date_beg,
		t1.issue_date_end,
		t1.issue_rule,
		t1.issue_condition_type,
		t1.issue_condition_sale_sum,
		t1.issue_condition_pos_count,
		t1.apply_condition_sale_sum,
		t1.crt_date,
		t1.crt_user,
		t1.upd_date,
		t1.upd_user,	
		t11.card_type_name,
		t12.card_count,
		t12.active_card_count
	FROM discount.campaign t1
	LEFT JOIN discount.card_type t11 ON t1.card_type_id = t11.card_type_id
	LEFT JOIN
	(
		select count(x1.card_id) as card_count, sum(case x1.curr_card_status_id when 2 then 1 else 0 end) as active_card_count, x1.curr_card_type_id  from discount.card x1 group by x1.curr_card_type_id
	) t12 ON t11.card_type_id = t12.curr_card_type_id
	;


ALTER TABLE discount.vw_campaign OWNER TO pavlov;
