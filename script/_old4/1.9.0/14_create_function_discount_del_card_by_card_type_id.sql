﻿/*
	CREATE OR REPLACE FUNCTION discount.del_card_by_card_type_id
*/

-- DROP FUNCTION discount.del_card_by_card_type_id(bigint);

CREATE OR REPLACE FUNCTION discount.del_card_by_card_type_id(
    IN _card_type_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
    _card_id bigint;
BEGIN
	result:= 0;
	FOR _card_id IN SELECT card_id FROM discount.card WHERE curr_card_type_id = _card_type_id
	LOOP
		--select discount.del_card(_card_id);
		PERFORM discount.del_card(_card_id);
	END LOOP;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.del_card_by_card_type_id(bigint) OWNER TO pavlov;
