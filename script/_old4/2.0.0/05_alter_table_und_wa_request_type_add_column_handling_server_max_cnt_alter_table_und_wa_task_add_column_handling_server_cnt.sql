﻿/*
	ALTER TABLE und.wa_request_type ADD COLUMN handling_server_max_cnt, ALTER TABLE und.wa_task ADD COLUMN handling_server_cnt
*/

ALTER TABLE und.wa_request_type ADD COLUMN handling_server_max_cnt integer NOT NULL DEFAULT 1;

ALTER TABLE und.wa_task ADD COLUMN handling_server_cnt integer;

