﻿/*
	CREATE OR REPLACE FUNCTION und.update_asna_link
*/

-- DROP FUNCTION und.update_asna_link(integer);

CREATE OR REPLACE FUNCTION und.update_asna_link(_link_id integer)
  RETURNS void AS
$BODY$
BEGIN	
	UPDATE und.asna_link_row t1
	SET is_unique = true
	WHERE t1.link_id = _link_id
	AND NOT EXISTS (
	SELECT 
	FROM und.asna_link_row x1
	WHERE x1.link_id = t1.link_id
	AND x1.row_id != t1.row_id
	AND x1.asna_nnt = t1.asna_nnt
	AND x1.asna_sku = t1.asna_sku
	)
	;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.update_asna_link(integer) OWNER TO pavlov;
