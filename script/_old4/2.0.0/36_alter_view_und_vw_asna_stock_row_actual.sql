﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_stock_row_actual
*/

-- DROP VIEW und.vw_asna_stock_row_actual;

CREATE OR REPLACE VIEW und.vw_asna_stock_row_actual AS 
 SELECT t1.row_id,
    t1.asna_user_id,
    t1.firm_name,
    t1.asna_prt_id,
    t1.asna_sku,
    t1.asna_qnt,
    t1.asna_doc,
    t1.asna_sup_inn,
    t1.asna_sup_id,
    t1.asna_sup_date,
    t1.asna_nds,
    t1.asna_gnvls,
    t1.asna_dp,
    t1.asna_man,
    t1.asna_series,
    t1.asna_exp,
    t1.asna_prc_man,
    t1.asna_prc_opt,
    t1.asna_prc_opt_nds,
    t1.asna_prc_ret,
    t1.asna_prc_reestr,
    t1.asna_prc_gnvls_max,
    t1.asna_sum_opt,
    t1.asna_sum_opt_nds,
    t1.asna_mr_gnvls,
    t1.asna_tag,
    t1.asna_stock_date,
    t1.asna_load_date,
    t1.asna_rv,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.asna_store_id,
    t2.asna_legal_id,
    t2.asna_rr_id,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    t10.asna_nnt,
    t10.asna_name,
        CASE
            WHEN t10.asna_nnt IS NOT NULL THEN true
            ELSE false
        END AS is_linked,
    t10.is_unique
   FROM und.asna_stock_row_actual t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id
     LEFT JOIN und.asna_link t9 ON t1.asna_user_id = t9.asna_user_id
     LEFT JOIN und.asna_link_row t10 ON t9.link_id = t10.link_id AND t1.asna_sku::text = t10.asna_sku::text;

ALTER TABLE und.vw_asna_stock_row_actual OWNER TO pavlov;
