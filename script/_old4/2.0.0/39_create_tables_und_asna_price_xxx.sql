﻿/*
	CREATE TABLEs und.asna_price_xxx
*/

----------------------------------------------------------------

-- DROP TABLE und.asna_price_chain;

CREATE TABLE und.asna_price_chain
(
  chain_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  chain_num integer NOT NULL,
  price_date timestamp without time zone,
  is_full boolean NOT NULL DEFAULT false,
  is_active boolean NOT NULL DEFAULT false,
  inactive_date timestamp without time zone,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_price_chain_pkey PRIMARY KEY (chain_id),
  CONSTRAINT asna_price_chain_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_price_chain OWNER TO pavlov;

-- DROP TRIGGER und_asna_price_chain_set_stamp_trg ON und.asna_price_chain;

CREATE TRIGGER und_asna_price_chain_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_price_chain
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

----------------------------------------------------------------

-- DROP TABLE und.asna_price_batch_in;

CREATE TABLE und.asna_price_batch_in
(
  batch_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  chain_id integer NOT NULL,
  batch_num integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  is_active boolean NOT NULL DEFAULT false,
  inactive_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_price_batch_in_pkey PRIMARY KEY (batch_id),
  CONSTRAINT asna_price_batch_in_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_price_batch_in_chain_id_fkey FOREIGN KEY (chain_id)
      REFERENCES und.asna_price_chain (chain_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_price_batch_in OWNER TO pavlov;

-- DROP TRIGGER und_asna_price_batch_in_set_stamp_trg ON und.asna_price_batch_in;

CREATE TRIGGER und_asna_price_batch_in_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_price_batch_in
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

----------------------------------------------------------------

-- DROP TABLE und.asna_price_batch_out;

CREATE TABLE und.asna_price_batch_out
(
  batch_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  batch_num integer NOT NULL,
  row_cnt integer,
  price_date timestamp without time zone,
  is_full boolean NOT NULL DEFAULT false,
  is_active boolean NOT NULL DEFAULT false,
  inactive_date timestamp without time zone,
  mess character varying,
  in_chain_id integer,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_price_batch_out_pkey PRIMARY KEY (batch_id),
  CONSTRAINT asna_price_batch_out_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_price_batch_out_in_chain_id_fkey FOREIGN KEY (in_chain_id)
      REFERENCES und.asna_price_chain (chain_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_price_batch_out OWNER TO pavlov;

-- DROP TRIGGER und_asna_price_batch_out_set_stamp_trg ON und.asna_price_batch_out;

CREATE TRIGGER und_asna_price_batch_out_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_price_batch_out
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

----------------------------------------------------------------

-- DROP TABLE und.asna_price_row;

CREATE TABLE und.asna_price_row
(
  row_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  in_batch_id integer,
  out_batch_id integer,  
  artikul integer NOT NULL,  
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  barcode character varying,
  asna_sku character varying,
  asna_sup_inn character varying,
  asna_prc_opt_nds numeric,
  asna_prc_gnvls_max numeric,
  asna_status integer NOT NULL,
  asna_exp timestamp without time zone,
  asna_sup_date timestamp without time zone,
  asna_prcl_date timestamp without time zone,    
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_price_row_pkey PRIMARY KEY (row_id),
  CONSTRAINT asna_price_row_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_price_row_in_batch_id_fkey FOREIGN KEY (in_batch_id)
      REFERENCES und.asna_price_batch_in (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_price_row_out_batch_id_fkey FOREIGN KEY (out_batch_id)
      REFERENCES und.asna_price_batch_out (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_price_row OWNER TO pavlov;

-- DROP TRIGGER und_asna_price_row_set_stamp_trg ON und.asna_price_row;

CREATE TRIGGER und_asna_price_row_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_price_row
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

----------------------------------------------------------------

-- DROP TABLE und.asna_price_row_actual;

CREATE TABLE und.asna_price_row_actual
(
  row_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  firm_name character varying,
  asna_sku character varying,
  asna_sup_inn character varying,
  asna_prc_opt_nds numeric,
  asna_prc_gnvls_max numeric,
  asna_status integer NOT NULL,
  asna_exp timestamp without time zone,
  asna_sup_date timestamp without time zone,
  asna_prcl_date timestamp without time zone, 
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_price_row_actual_pkey PRIMARY KEY (row_id),
  CONSTRAINT asna_price_row_actual_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_price_row_actual OWNER TO pavlov;

-- DROP TRIGGER und_asna_price_row_actual_set_stamp_trg ON und.asna_price_row_actual;

CREATE TRIGGER und_asna_price_row_actual_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_price_row_actual
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

----------------------------------------------------------------