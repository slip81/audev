﻿/*
	CREATE TABLE und.asna_order_row
*/

-- DROP TABLE und.asna_order_row;

CREATE TABLE und.asna_order_row
(
  row_id serial NOT NULL,
  order_id integer NOT NULL,
  state_type_id integer,
  asna_row_id character varying,
  asna_order_id character varying,
  asna_row_type smallint NOT NULL DEFAULT 0,
  asna_prt_id character varying,
  asna_nnt integer NOT NULL,
  asna_qnt numeric NOT NULL,
  asna_prc numeric NOT NULL,
  asna_prc_dsc numeric,
  asna_dsc_union character varying,
  asna_dtn numeric,
  asna_prc_loyal numeric,
  asna_prc_opt_nds numeric,
  asna_supp_inn character varying,
  asna_div_date timestamp without time zone,
  asna_qnt_unrsv numeric,
  asna_prc_fix numeric,
  asna_ts timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_order_row_pkey PRIMARY KEY (row_id),
  CONSTRAINT asna_order_row_order_id_fkey FOREIGN KEY (order_id)
      REFERENCES und.asna_order (order_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_order_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES und.asna_order_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_order_row OWNER TO pavlov;

-- DROP TRIGGER und_asna_order_row_set_stamp_trg ON und.asna_order_row;

CREATE TRIGGER und_asna_order_row_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_order_row
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

