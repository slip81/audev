﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_order
*/

-- DROP VIEW und.vw_asna_order;

CREATE OR REPLACE VIEW und.vw_asna_order AS 
 SELECT t1.order_id,
  t1.asna_user_id,
  t1.issuer_asna_user_id,
  t1.state_type_id,
  t1.asna_order_id,
  t1.asna_src,
  t1.asna_num,
  t1.asna_date,
  t1.asna_name,
  t1.asna_m_phone,
  t1.asna_pay_type,
  t1.asna_pay_type_id,
  t1.asna_d_card,
  t1.asna_ae,
  t1.asna_union_id,
  t1.asna_ts,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,  
  t2.workplace_id,
  t3.registration_key AS workplace_name,
  t3.sales_id,
  t4.adress AS sales_name,
  t4.client_id,
  t5.name AS client_name,
  t6.workplace_id AS issuer_workplace_id,
  t7.registration_key AS issuer_workplace_name,
  t7.sales_id as issuer_sales_id,
  t8.adress AS issuer_sales_name,
  t8.client_id AS issuer_client_id,
  t9.name AS issuer_client_name,  
  t11.state_type_name
   FROM und.asna_order t1
   INNER JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
   INNER JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
   INNER JOIN cabinet.sales t4 ON t3.sales_id = t4.id
   INNER JOIN cabinet.client t5 ON t4.client_id = t5.id
   INNER JOIN und.asna_user t6 ON t1.issuer_asna_user_id = t6.asna_user_id
   INNER JOIN cabinet.workplace t7 ON t6.workplace_id = t7.id
   INNER JOIN cabinet.sales t8 ON t7.sales_id = t8.id
   INNER JOIN cabinet.client t9 ON t8.client_id = t9.id
   LEFT JOIN und.asna_order_state_type t11 ON t1.state_type_id = t11.state_type_id;

ALTER TABLE und.vw_asna_order OWNER TO pavlov;
