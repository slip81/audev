﻿/*
	CREATE TABLE und.asna_order_state
*/

-- DROP TABLE und.asna_order_state;

CREATE TABLE und.asna_order_state
(
  order_state_id serial NOT NULL, 
  order_id integer NOT NULL,
  state_type_id integer NOT NULL,
  asna_status_id character varying,
  asna_order_id character varying,
  asna_date timestamp without time zone,
  asna_rc_date timestamp without time zone,
  asna_cmnt character varying,
  asna_ts timestamp without time zone,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_order_state_pkey PRIMARY KEY (order_state_id),
  CONSTRAINT asna_order_state_order_id_fkey FOREIGN KEY (order_id)
      REFERENCES und.asna_order (order_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_order_state_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES und.asna_order_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_order_state OWNER TO pavlov;

-- DROP TRIGGER und_asna_order_state_set_stamp_trg ON und.asna_order_state;

CREATE TRIGGER und_asna_order_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_order_state
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

