﻿/*
	INSERT INTO und.wa_request_type 8,9
*/

INSERT INTO und.wa_request_type (
	request_type_id, 
	request_type_name, 
	request_type_group1_id, 
	request_type_group2_id,
	is_inner,
	is_outer,
	handling_server_max_cnt
)
VALUES (
	8,
	'АСНА: передача полного сводного прайс-листа',
	2,
	1,
	false,
	true,
	1	
);

INSERT INTO und.wa_request_type (
	request_type_id, 
	request_type_name, 
	request_type_group1_id, 
	request_type_group2_id,
	is_inner,
	is_outer,
	handling_server_max_cnt
)
VALUES (
	9,
	'АСНА: передача изменений в сводном прайс-листе',
	2,
	1,
	false,
	true,
	1	
);

-- select * from und.wa_request_type order by request_type_id
