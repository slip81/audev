﻿/*
	ALTER TABLE und.asna_stock_batch_out ADD COLUMN in_chain_id
*/

ALTER TABLE und.asna_stock_batch_out ADD COLUMN in_chain_id integer;

ALTER TABLE und.asna_stock_batch_out
  ADD CONSTRAINT asna_stock_batch_out_in_chain_id_fkey FOREIGN KEY (in_chain_id)
      REFERENCES und.asna_stock_chain (chain_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;