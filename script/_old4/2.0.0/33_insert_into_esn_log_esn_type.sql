﻿/*
	insert into esn.log_esn_type
*/

insert into esn.log_esn_type (type_id, type_name)
values (22, 'АСНА: отправка заказов');

insert into esn.log_esn_type (type_id, type_name)
values (23, 'АСНА: получение заказов');

insert into esn.log_esn_type (type_id, type_name)
values (24, 'АСНА: отправка сводных прайс-листов [полная]');

insert into esn.log_esn_type (type_id, type_name)
values (25, 'АСНА: отправка сводных прайс-листов [изменения]');

insert into esn.log_esn_type (type_id, type_name)
values (26, 'АСНА: получение сводных прайс-листов');

-- select * from esn.log_esn_type order by type_id