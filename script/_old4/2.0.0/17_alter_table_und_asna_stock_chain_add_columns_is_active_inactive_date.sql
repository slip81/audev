﻿/*
	ALTER TABLE und.asna_stock_chain ADD COLUMNs is_active, inactive_date
*/

ALTER TABLE und.asna_stock_chain ADD COLUMN is_active boolean NOT NULL DEFAULT false;
ALTER TABLE und.asna_stock_chain ADD COLUMN inactive_date timestamp without time zone;