﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_stock_chain
*/

-- DROP VIEW und.vw_asna_stock_chain;

CREATE OR REPLACE VIEW und.vw_asna_stock_chain AS 
 SELECT t1.chain_id,
    t1.asna_user_id,
    t1.chain_num,
    t1.stock_date,
    t1.is_full,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.is_active,
    t1.inactive_date,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    ('№ ' || t1.chain_id::text || ' [' || (case t1.is_full when true then 'полные' else 'изменения' end) || '] ' || to_char(t1.stock_date, 'DD.MM.YYYY HH24:MI:SS'))::text as chain_name
   FROM und.asna_stock_chain t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id;

ALTER TABLE und.vw_asna_stock_chain OWNER TO pavlov;
