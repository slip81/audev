﻿/*
	BKP wa_request_xxx
*/

/*
----------------------------------------------------------------------  

-- DROP TABLE und.wa_request_state;

CREATE TABLE und.wa_request_state
(
  request_state_id integer NOT NULL,
  request_state_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_request_state_pkey PRIMARY KEY (request_state_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE und.wa_request_state OWNER TO pavlov;

-- DROP TRIGGER und_wa_request_state_set_stamp_trg ON und.wa_request_state;

CREATE TRIGGER und_wa_request_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_state
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();


INSERT INTO und.wa_request_state (
  request_state_id,
  request_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
) VALUES (
  1,
  'В очереди',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false
);


INSERT INTO und.wa_request_state (
  request_state_id,
  request_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
) VALUES (
  2,
  'Обрабатывается',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false
);

INSERT INTO und.wa_request_state (
  request_state_id,
  request_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
) VALUES (
  3,
  'Выполнен успешно',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false
);

INSERT INTO und.wa_request_state (
  request_state_id,
  request_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
) VALUES (
  4,
  'Выполнен с ошибками',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false
);  

----------------------------------------------------------------------

-- DROP TABLE und.wa_request;

CREATE TABLE und.wa_request
(
  request_id bigint NOT NULL DEFAULT und.id_generator(),
  wa_id integer NOT NULL,
  request_type_id integer NOT NULL,
  request_state_id integer NOT NULL DEFAULT 1,
  client_id integer,
  sales_id integer,
  workplace_id integer,
  ord integer NOT NULL DEFAULT 0,
  ord_client integer NOT NULL DEFAULT 0,
  state_date timestamp without time zone,
  state_user character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  is_async boolean NOT NULL DEFAULT false,
  task_id integer,
  CONSTRAINT wa_request_pkey PRIMARY KEY (request_id),
  CONSTRAINT wa_request_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_request_state_id_fkey FOREIGN KEY (request_state_id)
      REFERENCES und.wa_request_state (request_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_request_type_id_fkey FOREIGN KEY (request_type_id)
      REFERENCES und.wa_request_type (request_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES und.wa_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_wa_id_fkey FOREIGN KEY (wa_id)
      REFERENCES und.wa (wa_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request OWNER TO pavlov;

-- DROP TRIGGER und_wa_request_set_stamp_trg ON und.wa_request;

CREATE TRIGGER und_wa_request_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

----------------------------------------------------------------------

-- DROP TABLE und.wa_request_data;

CREATE TABLE und.wa_request_data
(
  request_id bigint NOT NULL,
  data_text text,
  data_binary bytea,
  CONSTRAINT wa_request_data_pkey PRIMARY KEY (request_id),
  CONSTRAINT wa_request_data_request_id_fkey FOREIGN KEY (request_id)
      REFERENCES und.wa_request (request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_data OWNER TO pavlov;

----------------------------------------------------------------------

-- DROP TABLE und.wa_request_lc;

CREATE TABLE und.wa_request_lc
(
  request_lc_id serial NOT NULL,
  request_id bigint NOT NULL,
  request_state_id integer NOT NULL DEFAULT 1,
  is_current boolean NOT NULL DEFAULT true,
  state_date timestamp without time zone,
  state_user character varying,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_request_lc_pkey PRIMARY KEY (request_lc_id),
  CONSTRAINT wa_request_lc_request_id_fkey FOREIGN KEY (request_id)
      REFERENCES und.wa_request (request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_lc_request_state_id_fkey FOREIGN KEY (request_state_id)
      REFERENCES und.wa_request_state (request_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_lc OWNER TO pavlov;

-- DROP TRIGGER und_wa_request_lc_set_stamp_trg ON und.wa_request_lc;

CREATE TRIGGER und_wa_request_lc_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_lc
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

----------------------------------------------------------------------

-- DROP TABLE und.wa_request_log;

CREATE TABLE und.wa_request_log
(
  log_id serial NOT NULL,
  request_id bigint NOT NULL,
  log_num integer NOT NULL DEFAULT 1,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  is_error boolean NOT NULL DEFAULT false,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  CONSTRAINT wa_request_log_pkey PRIMARY KEY (log_id),
  CONSTRAINT wa_request_log_request_id_fkey FOREIGN KEY (request_id)
      REFERENCES und.wa_request (request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_log OWNER TO pavlov;

-- DROP TRIGGER und_wa_request_log_set_stamp_trg ON und.wa_request_log;

CREATE TRIGGER und_wa_request_log_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_log    
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

----------------------------------------------------------------------  

-- DROP TABLE und.wa_worker_request;

CREATE TABLE und.wa_worker_request
(
  item_id serial NOT NULL,
  worker_id integer NOT NULL,
  request_id bigint NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  inactive_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT wa_worker_request_pkey PRIMARY KEY (item_id),
  CONSTRAINT wa_worker_request_request_id_fkey FOREIGN KEY (request_id)
      REFERENCES und.wa_request (request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_worker_request_worker_id_fkey FOREIGN KEY (worker_id)
      REFERENCES und.wa_worker (worker_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_worker_request OWNER TO pavlov;

----------------------------------------------------------------------  
*/