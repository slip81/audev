﻿/*
	CREATE TABLE und.asna_order_state_type
*/

-- DROP TABLE und.asna_order_state_type;

CREATE TABLE und.asna_order_state_type
(
  state_type_id integer NOT NULL,
  state_type_name character varying,
  is_for_order boolean NOT NULL DEFAULT false,
  is_for_row boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_order_state_type_pkey PRIMARY KEY (state_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_order_state_type OWNER TO pavlov;

-- DROP TRIGGER und_asna_order_state_type_type_set_stamp_trg ON und.asna_order_state_type;

CREATE TRIGGER und_asna_order_state_type_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_order_state_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();


INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (100, 'Новый заказ', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (102, 'Отмена строки на сайте', false, true, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (108, 'Заказ отредактирован покупателем на сайте', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (111, 'Отмена заказа на сайте', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (200, 'Заказ принят аптекой', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (201, 'Заказ частично принят аптекой', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (202, 'Отказ со стороны аптеки', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (203, 'Ожидание предзаказа', true, true, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (204, 'Изменение времени бронирования', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (205, 'Резерв сброшен', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (206, 'Превышение ожидаемого срока поставки', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (207, 'Предзаказ выполнен', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (208, 'Редактирование строки запрещено', false, true, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (209, 'Резерв выкуплен, ожидает предзаказ', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (210, 'Заказ выкуплен', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (211, 'Отмена заказа подтверждена', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.asna_order_state_type (state_type_id, state_type_name, is_for_order, is_for_row, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (212, 'Заказ отменен аптекой', true, false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

-- select * from und.asna_order_state_type order by state_type_id;
-- delete from und.asna_order_state_type;
