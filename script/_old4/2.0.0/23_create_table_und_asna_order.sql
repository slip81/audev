﻿/*
	CREATE TABLE und.asna_order
*/

-- DROP TABLE und.asna_order;

CREATE TABLE und.asna_order
(
  order_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  issuer_asna_user_id integer NOT NULL,
  state_type_id integer,
  asna_order_id character varying,
  asna_src character varying,
  asna_num character varying,
  asna_date timestamp without time zone,
  asna_name character varying,
  asna_m_phone character varying,
  asna_pay_type character varying,
  asna_pay_type_id character varying,
  asna_d_card character varying,
  asna_ae smallint NOT NULL DEFAULT 0,
  asna_union_id character varying,
  asna_ts timestamp without time zone,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT asna_order_pkey PRIMARY KEY (order_id),
  CONSTRAINT asna_order_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_order_issuer_asna_user_id_fkey FOREIGN KEY (issuer_asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_order_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES und.asna_order_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION       
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_order OWNER TO pavlov;

-- DROP TRIGGER und_asna_order_set_stamp_trg ON und.asna_order;

CREATE TRIGGER und_asna_order_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_order
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

