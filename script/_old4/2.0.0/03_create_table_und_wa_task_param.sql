﻿/*
	CREATE TABLE und.wa_task_param
*/

-- DROP TABLE und.wa_task_param;

CREATE TABLE und.wa_task_param
(
  task_id integer NOT NULL,
  task_param text,  
  CONSTRAINT wa_task_param_pkey PRIMARY KEY (task_id),
  CONSTRAINT wa_task_param_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES und.wa_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_task_param OWNER TO pavlov;