﻿/*
	CREATE OR REPLACE FUNCTION und.send_remains_to_asna_finalize
*/

-- DROP FUNCTION und.send_remains_to_asna_finalize(integer, integer);

CREATE OR REPLACE FUNCTION und.send_remains_to_asna_finalize(_in_chain_id integer, _out_batch_id integer)
  RETURNS void AS
$BODY$
BEGIN	
	update und.asna_stock_row t1
	set out_batch_id = _out_batch_id
	from und.asna_stock_batch_in t2
	where t1.in_batch_id = t2.batch_id
	and t2.chain_id = _in_chain_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.send_remains_to_asna_finalize(integer, integer)  OWNER TO pavlov;
