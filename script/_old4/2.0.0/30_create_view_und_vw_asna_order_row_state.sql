﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_order_row_state
*/

-- DROP VIEW und.vw_asna_order_row_state;

CREATE OR REPLACE VIEW und.vw_asna_order_row_state AS 
 SELECT t1.row_state_id,
  t1.row_id,
  t1.state_type_id,
  t1.asna_status_id,
  t1.asna_order_id,
  t1.asna_row_id,
  t1.asna_date,
  t1.asna_rc_date,
  t1.asna_cmnt,
  t1.asna_ts,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t11.state_type_name
   FROM und.asna_order_row_state t1
     LEFT JOIN und.asna_order_state_type t11 ON t1.state_type_id = t11.state_type_id;

ALTER TABLE und.vw_asna_order_row_state OWNER TO pavlov;
