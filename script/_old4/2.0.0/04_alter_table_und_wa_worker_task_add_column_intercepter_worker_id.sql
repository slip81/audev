﻿/*
	ALTER TABLE und.wa_worker_task ADD COLUMN intercepter_worker_id
*/

ALTER TABLE und.wa_worker_task ADD COLUMN intercepter_worker_id integer;

ALTER TABLE und.wa_worker_task
  ADD CONSTRAINT wa_worker_task_intercepter_worker_id_fkey FOREIGN KEY (intercepter_worker_id)
      REFERENCES und.wa_worker (worker_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
