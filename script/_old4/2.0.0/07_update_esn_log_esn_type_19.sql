﻿/*
	update esn.log_esn_type where type_id = 19
*/

delete from esn.log_esn where log_esn_type_id = 18;
delete from esn.log_esn_type where type_id = 18;

update esn.log_esn_type
set type_name = 'Worker: выполнение заданий'
where type_id = 19;

-- select * from esn.log_esn_type order by type_id