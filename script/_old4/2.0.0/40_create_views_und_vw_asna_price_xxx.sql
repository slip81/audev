﻿/*
	CREATE VIEWs und.vw_asna_price_xxx
*/

--------------------------------------------------------

-- DROP VIEW und.vw_asna_price_chain;

CREATE OR REPLACE VIEW und.vw_asna_price_chain AS 
 SELECT t1.chain_id,
    t1.asna_user_id,
    t1.chain_num,
    t1.price_date,
    t1.is_full,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.is_active,
    t1.inactive_date,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    (((('№ '::text || t1.chain_id::text) || ' ['::text) ||
        CASE t1.is_full
            WHEN true THEN 'полные'::text
            ELSE 'изменения'::text
        END) || '] '::text) || to_char(t1.price_date, 'DD.MM.YYYY HH24:MI:SS'::text) AS chain_name
   FROM und.asna_price_chain t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id;

ALTER TABLE und.vw_asna_price_chain OWNER TO pavlov;

--------------------------------------------------------
-- DROP VIEW und.vw_asna_price_row;

CREATE OR REPLACE VIEW und.vw_asna_price_row AS 
 SELECT t1.row_id,
    t1.asna_user_id,
    t1.in_batch_id,
    t1.out_batch_id,
    t1.artikul,    
    t1.esn_id,
    t1.prep_id,
    t1.prep_name,
    t1.firm_id,
    t1.firm_name,
    t1.barcode,
    t1.asna_sku,
    t1.asna_sup_inn,
    t1.asna_prc_opt_nds,
    t1.asna_prc_gnvls_max,
    t1.asna_status,
    t1.asna_exp,
    t1.asna_sup_date,
    t1.asna_prcl_date,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.asna_store_id,
    t2.asna_legal_id,
    t2.asna_rr_id,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    t7.chain_id,
    t10.asna_nnt,
    t10.asna_name,
        CASE
            WHEN t10.asna_nnt IS NOT NULL THEN true
            ELSE false
        END AS is_linked,
    t10.is_unique
   FROM und.asna_price_row t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id
     LEFT JOIN und.asna_price_batch_in t6 ON t1.in_batch_id = t6.batch_id
     LEFT JOIN und.asna_price_chain t7 ON t6.chain_id = t7.chain_id
     LEFT JOIN und.asna_price_batch_out t8 ON t1.out_batch_id = t8.batch_id
     LEFT JOIN und.asna_link t9 ON t1.asna_user_id = t9.asna_user_id
     LEFT JOIN und.asna_link_row t10 ON t9.link_id = t10.link_id AND t1.asna_sku::text = t10.asna_sku::text;

ALTER TABLE und.vw_asna_price_row OWNER TO pavlov;
--------------------------------------------------------
-- DROP VIEW und.vw_asna_price_row_actual;

CREATE OR REPLACE VIEW und.vw_asna_price_row_actual AS 
 SELECT t1.row_id,
    t1.asna_user_id,
    t1.firm_name,
    t1.asna_sku,
    t1.asna_sup_inn,
    t1.asna_prc_opt_nds,
    t1.asna_prc_gnvls_max,
    t1.asna_status,
    t1.asna_exp,
    t1.asna_sup_date,
    t1.asna_prcl_date,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.asna_store_id,
    t2.asna_legal_id,
    t2.asna_rr_id,
    t2.workplace_id,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
    t10.asna_nnt,
    t10.asna_name,
        CASE
            WHEN t10.asna_nnt IS NOT NULL THEN true
            ELSE false
        END AS is_linked,
    t10.is_unique
   FROM und.asna_price_row_actual t1
     JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id
     LEFT JOIN und.asna_link t9 ON t1.asna_user_id = t9.asna_user_id
     LEFT JOIN und.asna_link_row t10 ON t9.link_id = t10.link_id AND t1.asna_sku::text = t10.asna_sku::text;

ALTER TABLE und.vw_asna_price_row_actual OWNER TO pavlov;
--------------------------------------------------------
-- DROP VIEW und.vw_asna_price_row_linked;

CREATE OR REPLACE VIEW und.vw_asna_price_row_linked AS 
 SELECT t1.row_id,
    t1.asna_sku,
    t1.asna_sup_inn,
    t1.asna_prc_opt_nds,
    t1.asna_prc_gnvls_max,
    t1.asna_status,
    t1.asna_exp,
    t1.asna_sup_date,
    t1.asna_prcl_date,
    t3.asna_nnt,
    t12.chain_id,
    t12.price_date AS chain_price_date,
    t12.is_full AS chain_is_full,
    t3.is_unique
   FROM und.asna_price_row t1
     JOIN und.asna_link t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN und.asna_link_row t3 ON t2.link_id = t3.link_id AND t1.asna_sku::text = t3.asna_sku::text
     LEFT JOIN und.asna_price_batch_in t11 ON t1.in_batch_id = t11.batch_id
     LEFT JOIN und.asna_price_chain t12 ON t11.chain_id = t12.chain_id;

ALTER TABLE und.vw_asna_price_row_linked OWNER TO pavlov;
--------------------------------------------------------