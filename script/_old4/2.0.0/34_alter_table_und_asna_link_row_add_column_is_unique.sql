﻿/*
	ALTER TABLE und.asna_link_row ADD COLUMN is_unique
*/

ALTER TABLE und.asna_link_row ADD COLUMN is_unique boolean NOT NULL DEFAULT false;

UPDATE und.asna_link_row t1
SET is_unique = true
WHERE NOT EXISTS (
SELECT x1.link_id
FROM und.asna_link_row x1
WHERE x1.link_id = t1.link_id
AND x1.row_id != t1.row_id
AND x1.asna_nnt = t1.asna_nnt
AND x1.asna_sku = t1.asna_sku
)
;
