﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_order_row
*/

-- DROP VIEW und.vw_asna_order_row;

CREATE OR REPLACE VIEW und.vw_asna_order_row AS 
 SELECT t1.row_id,
  t1.order_id,
  t1.state_type_id,
  t1.asna_row_id,
  t1.asna_order_id,
  t1.asna_row_type,
  t1.asna_prt_id,
  t1.asna_nnt,
  t1.asna_qnt,
  t1.asna_prc,
  t1.asna_prc_dsc,
  t1.asna_dsc_union,
  t1.asna_dtn,
  t1.asna_prc_loyal,
  t1.asna_prc_opt_nds,
  t1.asna_supp_inn,
  t1.asna_div_date,
  t1.asna_qnt_unrsv,
  t1.asna_prc_fix,
  t1.asna_ts,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t11.state_type_name
   FROM und.asna_order_row t1
     LEFT JOIN und.asna_order_state_type t11 ON t1.state_type_id = t11.state_type_id;

ALTER TABLE und.vw_asna_order_row OWNER TO pavlov;
