﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_stock_row_linked
*/

-- DROP VIEW und.vw_asna_stock_row_linked;

CREATE OR REPLACE VIEW und.vw_asna_stock_row_linked AS 
 SELECT t1.row_id,
    t1.asna_prt_id,    
    t1.asna_sku,
    t1.asna_qnt,
    t1.asna_doc,
    t1.asna_sup_inn,
    t1.asna_sup_id,
    t1.asna_sup_date,
    t1.asna_nds,
    t1.asna_gnvls,
    t1.asna_dp,
    t1.asna_man,
    t1.asna_series,
    t1.asna_exp,
    t1.asna_prc_man,
    t1.asna_prc_opt,
    t1.asna_prc_opt_nds,
    t1.asna_prc_ret,
    t1.asna_prc_reestr,
    t1.asna_prc_gnvls_max,
    t1.asna_sum_opt,
    t1.asna_sum_opt_nds,
    t1.asna_mr_gnvls,
    t1.asna_tag,
    t3.asna_nnt,
    t12.chain_id,
    t12.stock_date as chain_stock_date,
    t12.is_full as chain_is_full
   FROM und.asna_stock_row t1
     INNER JOIN und.asna_link t2 ON t1.asna_user_id = t2.asna_user_id
     INNER JOIN und.asna_link_row t3 ON t2.link_id = t3.link_id AND t1.asna_sku = t3.asna_sku
     LEFT JOIN und.asna_stock_batch_in t11 ON t1.in_batch_id = t11.batch_id
     LEFT JOIN und.asna_stock_chain t12 ON t11.chain_id = t12.chain_id
     ;

ALTER TABLE und.vw_asna_stock_row_linked OWNER TO pavlov;
