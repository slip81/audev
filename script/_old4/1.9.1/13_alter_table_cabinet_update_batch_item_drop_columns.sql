﻿/*
	ALTER TABLE cabinet.update_batch_item DROP COLUMNs
*/

 ALTER TABLE cabinet.update_batch_item DROP COLUMN service_version_id;
 ALTER TABLE cabinet.update_batch_item DROP COLUMN soft_version_id;
 ALTER TABLE cabinet.update_batch_item DROP COLUMN update_xml;