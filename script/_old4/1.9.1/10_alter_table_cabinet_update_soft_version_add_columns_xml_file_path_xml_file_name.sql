﻿/*
	ALTER TABLE cabinet.update_soft_version ADD COLUMN xml_file_path, xml_file_name
*/

ALTER TABLE cabinet.update_soft_version ADD COLUMN xml_file_path character varying;
ALTER TABLE cabinet.update_soft_version ADD COLUMN xml_file_name character varying;