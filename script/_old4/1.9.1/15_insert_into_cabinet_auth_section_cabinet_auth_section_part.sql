﻿/*
	insert into cabinet.auth_section, cabinet.auth_section_part
*/

insert into cabinet.auth_section (section_id, section_name, group_id)
values (14, 'Обновления', 1);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (36, 'Софт', 14, 1);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (37, 'Пакеты', 14, 1);


-- select * from cabinet.auth_section order by section_id;
-- select * from cabinet.auth_section_part order by section_id, part_id;
-- select * from cabinet.auth_section_part order by  part_id;