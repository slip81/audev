﻿/*
	CREATE TABLE cabinet.update_soft
*/

-- DROP TABLE cabinet.update_soft;

CREATE TABLE cabinet.update_soft
(
  soft_id integer NOT NULL,
  soft_name character varying,
  service_id int,
  items_exists boolean NOT NULL DEFAULT false,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT update_soft_pkey PRIMARY KEY (soft_id),
  CONSTRAINT update_soft_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.update_soft OWNER TO pavlov;

-- DROP TRIGGER cabinet_update_soft_set_stamp_trg ON cabinet.update_soft;

CREATE TRIGGER cabinet_update_soft_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.update_soft
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();