﻿/*
	CREATE TABLE cabinet.client_update_soft
*/

-- DROP TABLE cabinet.client_update_soft;

CREATE TABLE cabinet.client_update_soft
(
  item_id serial NOT NULL,
  client_id integer NOT NULL,
  sales_id integer,
  workplace_id integer,
  soft_id integer NOT NULL,
  version_id integer,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT client_update_soft_pkey PRIMARY KEY (item_id),
  CONSTRAINT client_update_soft_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT client_update_soft_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT client_update_soft_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,            
  CONSTRAINT client_update_soft_soft_id_fkey FOREIGN KEY (soft_id)
      REFERENCES cabinet.update_soft (soft_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT client_update_soft_version_id_fkey FOREIGN KEY (version_id)
      REFERENCES cabinet.update_soft_version (version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.client_update_soft OWNER TO pavlov;

-- DROP TRIGGER cabinet_client_update_soft_set_stamp_trg ON cabinet.client_update_soft;

CREATE TRIGGER cabinet_client_update_soft_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.client_update_soft
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();