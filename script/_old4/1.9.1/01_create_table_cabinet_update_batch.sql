﻿/*
	CREATE TABLE cabinet.update_batch
*/

-- DROP TABLE cabinet.update_batch;

CREATE TABLE cabinet.update_batch
(
  batch_id serial NOT NULL,
  batch_name character varying,
  client_id int NOT NULL,  
  crt_date timestamp without time zone,
  crt_user character varying,
  last_download_date timestamp without time zone,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT update_batch_pkey PRIMARY KEY (batch_id),
  CONSTRAINT update_batch_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.update_batch OWNER TO pavlov;

-- DROP TRIGGER cabinet_update_batch_set_stamp_trg ON cabinet.update_batch;

CREATE TRIGGER cabinet_update_batch_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.update_batch
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();