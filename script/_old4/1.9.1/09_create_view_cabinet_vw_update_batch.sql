﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_update_batch
*/

-- DROP VIEW cabinet.vw_update_batch;

CREATE OR REPLACE VIEW cabinet.vw_update_batch AS 
 SELECT t1.batch_id,
	t1.batch_name,
	t1.client_id,
	t1.crt_date,
	t1.crt_user,
	t1.last_download_date,
	t10.name as client_name
   FROM cabinet.update_batch t1     
     LEFT JOIN cabinet.client t10 ON t1.client_id = t10.id;

ALTER TABLE cabinet.vw_update_batch OWNER TO pavlov;
