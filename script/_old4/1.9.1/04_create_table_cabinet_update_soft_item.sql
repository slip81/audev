﻿/*
	CREATE TABLE cabinet.update_soft_item
*/

-- DROP TABLE cabinet.update_soft_item;

CREATE TABLE cabinet.update_soft_item
(
  item_id serial NOT NULL,
  soft_id int NOT NULL,
  item_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT update_soft_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT update_soft_item_soft_id_fkey FOREIGN KEY (soft_id)
      REFERENCES cabinet.update_soft (soft_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.update_soft_item OWNER TO pavlov;

-- DROP TRIGGER cabinet_update_soft_item_set_stamp_trg ON cabinet.update_soft_item;

CREATE TRIGGER cabinet_update_soft_item_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.update_soft_item
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();