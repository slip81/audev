﻿/*
	CREATE TABLE cabinet.update_batch_log
*/

-- DROP TABLE cabinet.update_batch_log;

CREATE TABLE cabinet.update_batch_log
(
  log_id serial NOT NULL,
  batch_id int NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  login character varying,
  workplace character varying,
  batch_item_id int,
  mess character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT update_batch_log_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.update_batch_log OWNER TO pavlov;

-- DROP TRIGGER cabinet_update_batch_log_set_stamp_trg ON cabinet.update_batch_log;

CREATE TRIGGER cabinet_update_batch_log_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.update_batch_log
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();