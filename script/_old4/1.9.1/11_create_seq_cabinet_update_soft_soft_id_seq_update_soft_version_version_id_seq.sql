﻿/*
	CREATE SEQUENCE cabinet.update_soft_soft_id_seq, cabinet.update_soft_version_version_id_seq
*/

CREATE SEQUENCE cabinet.update_soft_soft_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE cabinet.update_soft_soft_id_seq OWNER TO pavlov;

ALTER TABLE cabinet.update_soft ALTER COLUMN soft_id SET DEFAULT nextval('cabinet.update_soft_soft_id_seq'::regclass);

SELECT setval('cabinet.update_soft_soft_id_seq', (coalesce((select max(soft_id) from cabinet.update_soft), 0) + 1), true);

CREATE SEQUENCE cabinet.update_soft_version_version_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE cabinet.update_soft_version_version_id_seq OWNER TO pavlov;

ALTER TABLE cabinet.update_soft_version ALTER COLUMN version_id SET DEFAULT nextval('cabinet.update_soft_version_version_id_seq'::regclass);

SELECT setval('cabinet.update_soft_version_version_id_seq', (coalesce((select max(version_id) from cabinet.update_soft_version), 0) + 1), true);

-- select * from cabinet.update_soft_version





