﻿/*
	CREATE TABLE cabinet.update_batch_item
*/

-- DROP TABLE cabinet.update_batch_item;

CREATE TABLE cabinet.update_batch_item
(
  item_id serial NOT NULL,
  batch_id int NOT NULL,
  soft_id int NOT NULL,
  service_version_id int,
  soft_version_id int,
  update_xml character varying,
  ord int,
  last_download_date timestamp without time zone,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT update_batch_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT update_batch_item_batch_id_fkey FOREIGN KEY (batch_id)
      REFERENCES cabinet.update_batch (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT update_batch_item_soft_id_fkey FOREIGN KEY (soft_id)
      REFERENCES cabinet.update_soft (soft_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT update_batch_item_service_version_id_fkey FOREIGN KEY (service_version_id)
      REFERENCES cabinet.version (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT update_batch_item_soft_version_id_fkey FOREIGN KEY (soft_version_id)
      REFERENCES cabinet.update_soft_version (version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.update_batch_item OWNER TO pavlov;

-- DROP TRIGGER cabinet_update_batch_item_set_stamp_trg ON cabinet.update_batch_item;

CREATE TRIGGER cabinet_update_batch_item_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.update_batch_item
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();