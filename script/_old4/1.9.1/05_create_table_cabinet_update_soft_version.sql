﻿/*
	CREATE TABLE cabinet.update_soft_version
*/

-- DROP TABLE cabinet.update_soft_version;

CREATE TABLE cabinet.update_soft_version
(
  version_id int NOT NULL,
  soft_id int NOT NULL,
  version_name character varying,
  version_build character varying,
  version_date date,
  file_path character varying,
  file_name character varying,
  upload_date timestamp without time zone,
  upload_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT update_soft_version_pkey PRIMARY KEY (version_id),
  CONSTRAINT update_soft_version_soft_id_fkey FOREIGN KEY (soft_id)
      REFERENCES cabinet.update_soft (soft_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.update_soft_version OWNER TO pavlov;

-- DROP TRIGGER cabinet_update_soft_version_set_stamp_trg ON cabinet.update_soft_version;

CREATE TRIGGER cabinet_update_soft_version_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.update_soft_version
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();