﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_update_batch_item
*/

DROP VIEW cabinet.vw_update_batch_item;

CREATE OR REPLACE VIEW cabinet.vw_update_batch_item AS 
 SELECT t1.soft_id,
    t1.soft_name,
    t2.batch_id,
    t2.client_id,
        CASE
            WHEN COALESCE(t3.soft_id, 0) > 0 THEN true
            ELSE false
        END AS is_subscribed,
    t3.ord,
    t3.last_download_date
   FROM cabinet.update_soft t1
     LEFT JOIN cabinet.update_batch t2 ON 1 = 1
     LEFT JOIN cabinet.update_batch_item t3 ON t2.batch_id = t3.batch_id AND t1.soft_id = t3.soft_id
UNION ALL
 SELECT t1.soft_id,
    t1.soft_name,
    0 AS batch_id,
    NULL::integer AS client_id,
    false AS is_subscribed,
    NULL::integer AS ord,
    NULL::timestamp without time zone AS last_download_date
   FROM cabinet.update_soft t1;

ALTER TABLE cabinet.vw_update_batch_item OWNER TO pavlov;
