﻿/*
	CREATE OR REPLACE VIEW und.vw_product_and_good
*/

-- DROP VIEW und.vw_product_and_good;

CREATE OR REPLACE VIEW und.vw_product_and_good AS 
 SELECT row_number() over (order by null) as item_id,
    t1.product_id,
    t1.product_name,
    t1.doc_id,
    t1.doc_row_id,
    t1.is_deleted,
    count(*) over (partition by t1.product_id) as good_cnt,
    t11.partner_id,
    t11.good_id,
    t11.partner_code,
    t11.partner_name,
    t11.source_partner_name,    
    t11.mfr_id,
    t11.barcode,
    t11.doc_id as good_doc_id,
    t11.doc_row_id as good_doc_row_id,    
    t11.mfr_name,
    t11.is_deleted as good_is_deleted
   FROM und.product t1
    LEFT JOIN und.vw_partner_good t11 ON t1.product_id = t11.product_id
    ;

ALTER TABLE und.vw_product_and_good OWNER TO pavlov;
