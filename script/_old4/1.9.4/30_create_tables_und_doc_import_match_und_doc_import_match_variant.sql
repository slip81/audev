﻿/*
	CREATE TABLEs und.doc_import_match, und.doc_import_match_variant
*/

-- DROP TABLE und.doc_import_match;

CREATE TABLE und.doc_import_match
(
  row_id serial NOT NULL,
  import_id integer NOT NULL,
  row_num integer NOT NULL,
  source_name character varying,  
  code character varying,
  mfr character varying,
  mfr_group character varying,
  barcode character varying,
  state integer NOT NULL DEFAULT 0,
  match_product_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT doc_import_match_pkey PRIMARY KEY (row_id),
  CONSTRAINT doc_import_match_import_id_fkey FOREIGN KEY (import_id) 
      REFERENCES und.doc_import (import_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT doc_import_match_match_product_id_fkey FOREIGN KEY (match_product_id)
      REFERENCES und.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_match OWNER TO pavlov;


-- DROP TRIGGER und_doc_import_match_set_stamp_trg ON und.doc_import_match;

CREATE TRIGGER und_doc_import_match_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_import_match
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();


-- DROP TABLE und.doc_import_match_variant;

CREATE TABLE und.doc_import_match_variant
(
  variant_id serial NOT NULL,
  row_id integer NOT NULL,
  match_product_id integer NOT NULL,
  match_level integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT doc_import_match_variant_pkey PRIMARY KEY (variant_id),
  CONSTRAINT doc_import_match_variant_row_id_fkey FOREIGN KEY (row_id) 
      REFERENCES und.doc_import_match (row_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT doc_import_match_variant_match_product_id_fkey FOREIGN KEY (match_product_id)
      REFERENCES und.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_match_variant OWNER TO pavlov;


-- DROP TRIGGER und_doc_import_match_variant_set_stamp_trg ON und.doc_import_match_variant;

CREATE TRIGGER und_doc_import_match_variant_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_import_match_variant
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

