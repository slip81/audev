﻿/*
	ALTER TABLE cabinet.prj_task ADD COLUMN is_verified
*/

ALTER TABLE cabinet.prj_task ADD COLUMN is_verified boolean NOT NULL DEFAULT false;