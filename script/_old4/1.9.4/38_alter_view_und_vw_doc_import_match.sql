﻿/*
	CREATE OR REPLACE VIEW und.vw_doc_import_match
*/

-- DROP VIEW und.vw_doc_import_match;

CREATE OR REPLACE VIEW und.vw_doc_import_match AS 
 SELECT t1.row_id,
    t1.import_id,
    t1.row_num,
    t1.source_name,
    t1.code,
    t1.mfr,
    t1.mfr_group,
    t1.barcode,
    t1.state,
    t1.match_product_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t3.doc_id,
    t3.doc_num,
    t3.doc_name,
    t3.doc_date,
    t3.doc_type_id,
    t3.doc_state_id,
    t3.partner_id,
    t3.doc_type_name,
    t3.doc_state_name,
    t3.partner_name,
    t11.product_name AS match_product_name,
    t1.match_good_id,
    t12.barcode AS match_barcode,
    t12.mfr_id AS match_mfr_id,
    t13.mfr_name AS match_mfr_name,
    t1.is_approved
   FROM und.doc_import_match t1
     JOIN und.doc_import t2 ON t1.import_id = t2.import_id
     JOIN und.vw_doc t3 ON t2.doc_id = t3.doc_id
     LEFT JOIN und.product t11 ON t1.match_product_id = t11.product_id
     LEFT JOIN und.good t12 ON t1.match_good_id = t12.good_id
     LEFT JOIN und.manufacturer t13 ON t12.mfr_id = t13.mfr_id;

ALTER TABLE und.vw_doc_import_match OWNER TO pavlov;
