﻿/*
	insert into discount.report_discount
*/

insert into discount.report_discount (report_id, report_name, report_descr)
values (6, 'Первые продажи по картам', 'Отчет формирует список карт и адреса отделений, где была сделана первая покупка по карте, с указанем ФИО и телефона владельца карты.')

-- select * from discount.report_discount order by report_id;
-- select * from discount.business;
-- select * from discount.vw_report_card_fstsale where business_id =957862396381103979;