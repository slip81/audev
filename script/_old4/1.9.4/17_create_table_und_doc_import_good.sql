﻿/*
	CREATE TABLE und.doc_import_good
*/

-- DROP TABLE und.doc_import_good;

CREATE TABLE und.doc_import_good
(
  item_id serial NOT NULL,
  doc_id integer NOT NULL,
  product_id integer NOT NULL,
  mfr_id integer NOT NULL,
  barcode character varying,  
  partner_id integer NOT NULL,
  partner_code character varying,
  partner_name character varying, 
  CONSTRAINT doc_import_good_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_good OWNER TO pavlov;
