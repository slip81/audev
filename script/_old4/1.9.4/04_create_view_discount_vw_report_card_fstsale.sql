﻿/*
	CREATE OR REPLACE VIEW discount.vw_report_card_fstsale
*/

-- DROP VIEW discount.vw_report_card_fstsale;

CREATE OR REPLACE VIEW discount.vw_report_card_fstsale AS 
 SELECT t1.business_id,
    t1.card_id,
    t1.card_num,
    t1.card_num2,
    t1.curr_holder_id,
    t9.card_holder_surname AS curr_holder_name,
    t9.card_holder_name AS curr_holder_first_name,
    t9.card_holder_fname AS curr_holder_second_name,
    t9.phone_num AS curr_holder_phone_num,
    t5.org_name AS card_fstsale_org_name
   FROM discount.card t1
     LEFT JOIN discount.card_holder t9 ON t1.curr_holder_id = t9.card_holder_id
     LEFT JOIN LATERAL ( SELECT t2.card_trans_id,
            t2.card_id,
            y2.org_name
           FROM discount.card_transaction t2
             INNER JOIN discount.business_org_user y1 ON btrim(lower(COALESCE(y1.user_name, ''::character varying)::text)) = btrim(lower(COALESCE(t2.user_name, ''::character varying)::text))
             INNER JOIN discount.business_org y2 ON y1.org_id = y2.org_id
          WHERE t1.card_id = t2.card_id AND t2.trans_kind = 1 AND COALESCE(t2.status, 0) = 0
          ORDER BY t2.card_trans_id
         LIMIT 1) t5 ON t5.card_id = t1.card_id;


ALTER TABLE discount.vw_report_card_fstsale OWNER TO pavlov;
