﻿/*
	CREATE OR REPLACE FUNCTION und.approve_doc_import_match
*/

-- DROP FUNCTION und.approve_doc_import_match(integer, integer, character varying);

CREATE OR REPLACE FUNCTION und.approve_doc_import_match(
    IN _log_id integer,
    IN _state integer,
    IN _user_name character varying,
    OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN
	result:= 0;

	UPDATE und.doc_import_match t1
	SET state = _state, match_good_id = t2.good_id, match_product_id = t2.product_id, is_approved = true, is_auto_match = false, upd_date = clock_timestamp(), upd_user = _user_name
	FROM und.good t2
	WHERE t1.row_id = t2.tmp_row_id AND t2.log_id = _log_id;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.approve_doc_import_match(integer, integer, character varying) OWNER TO pavlov;
