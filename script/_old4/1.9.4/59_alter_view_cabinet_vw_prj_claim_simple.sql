﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple
*/

-- DROP VIEW cabinet.vw_prj_claim_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple AS 
 SELECT t1.claim_id,
    t1.prj_id,
        CASE
            WHEN COALESCE(t11.task_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.done_task_cnt THEN 3
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.canceled_task_cnt THEN 4
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.verified_task_cnt THEN 5
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND t11.task_cnt <> t11.canceled_task_cnt AND t11.task_cnt <> t11.done_task_cnt AND t11.task_cnt <> t11.verified_task_cnt THEN 3
            ELSE 2
        END AS claim_state_type_id,
    t11.task_cnt,
    t11.new_task_cnt,
    t11.active_task_cnt,
    t11.done_task_cnt,
    t11.canceled_task_cnt,
    t11.verified_task_cnt
   FROM cabinet.prj_claim t1
     LEFT JOIN ( SELECT x1.claim_id,
            count(x1.task_id) AS task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 1 THEN 1
                    ELSE 0
                END) AS new_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 2 THEN 1
                    ELSE 0
                END) AS active_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 3 THEN 1
                    ELSE 0
                END) AS done_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 4 THEN 1
                    ELSE 0
                END) AS canceled_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 5 THEN 1
                    ELSE 0
                END) AS verified_task_cnt
           FROM cabinet.vw_prj_task_simple x1
          GROUP BY x1.claim_id) t11 ON t1.claim_id = t11.claim_id;

ALTER TABLE cabinet.vw_prj_claim_simple OWNER TO pavlov;
