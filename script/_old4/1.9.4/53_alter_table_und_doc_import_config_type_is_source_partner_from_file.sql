﻿/*
	ALTER TABLE und.doc_import_config_type ADD COLUMN is_source_partner_from_file
*/

ALTER TABLE und.doc_import_config_type ADD COLUMN is_source_partner_from_file boolean NOT NULL DEFAULT false;
ALTER TABLE und.doc_import_config_type ALTER source_partner_id DROP NOT NULL;