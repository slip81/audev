﻿/*
	CREATE INDEXEs und.xxx
*/

-- DROP INDEX und.und_good_product_id1_idx;

CREATE INDEX und_good_product_id1_idx
  ON und.good
  USING btree
  (product_id);

-- DROP INDEX und.und_good_mfr_id1_idx;

CREATE INDEX und_good_mfr_id1_idx
  ON und.good
  USING btree
  (mfr_id);


-- DROP INDEX und.und_partner_good_good_id1_idx;

CREATE INDEX und_partner_good_good_id1_idx
  ON und.partner_good
  USING btree
  (good_id);  

-- DROP INDEX und.und_partner_good_partner_id1_idx;

CREATE INDEX und_partner_good_partner_id1_idx
  ON und.partner_good
  USING btree
  (partner_id);    

-- DROP INDEX und.und_product_alias_product_id1_idx;

CREATE INDEX und_product_alias_product_id1_idx
  ON und.product_alias
  USING btree
  (product_id);


-- DROP INDEX und.und_doc_lc_doc_id1_idx;

CREATE INDEX und_doc_lc_doc_id1_idx
  ON und.doc_lc
  USING btree
  (doc_id);  

-- DROP INDEX und.und_doc_import_match_variant_match_product_id1_idx;

CREATE INDEX und_doc_import_match_variant_match_product_id1_idx
  ON und.doc_import_match_variant
  USING btree
  (match_product_id);    

-- DROP INDEX und.und_doc_import_match_variant_row_id1_idx;

CREATE INDEX und_doc_import_match_variant_row_id1_idx
  ON und.doc_import_match_variant
  USING btree
  (row_id);      


ALTER TABLE und.doc_import_match_variant
  ADD CONSTRAINT doc_import_match_variant_match_good_id_fkey FOREIGN KEY (match_good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

-- DROP INDEX und.und_doc_import_match_variant_match_product_id1_idx;

CREATE INDEX und_doc_import_match_variant_match_good_id1_idx
  ON und.doc_import_match_variant
  USING btree
  (match_good_id);  


-- DROP INDEX und.und_doc_import_match_import_id1_idx;

CREATE INDEX und_doc_import_match_import_id1_idx
  ON und.doc_import_match
  USING btree
  (import_id);  

-- DROP INDEX und.und_doc_import_match_product_id1_idx;

CREATE INDEX und_doc_import_match_product_id1_idx
  ON und.doc_import_match
  USING btree
  (match_product_id); 

-- DROP INDEX und.und_doc_import_match_good_id1_idx;

CREATE INDEX und_doc_import_match_good_id1_idx
  ON und.doc_import_match
  USING btree
  (match_good_id);   

    