﻿/*
	CREATE OR REPLACE VIEW und.vw_doc_import_match_variant
*/

DROP VIEW und.vw_doc_import_match_variant;

CREATE OR REPLACE VIEW und.vw_doc_import_match_variant AS 
 SELECT t1.variant_id,
    t1.row_id,
    t1.match_product_id,    
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.match_good_id,
    t1.percent,
    t1.is_match_name,
    t1.is_match_barcode,
    t1.is_match_mfr,
    t1.name_similarity,
    t1.barcode_similarity,
    t1.mfr_similarity,
    t1.avg_similarity,
    t2.doc_id,
    t11.product_name AS match_product_name,
    t12.barcode AS match_barcode,
    t12.mfr_id AS match_mfr_id,
    t13.mfr_name AS match_mfr_name
   FROM und.doc_import_match_variant t1
     JOIN und.vw_doc_import_match t2 ON t1.row_id = t2.row_id
     LEFT JOIN und.product t11 ON t1.match_product_id = t11.product_id
     LEFT JOIN und.good t12 ON t1.match_good_id = t12.good_id
     LEFT JOIN und.manufacturer t13 ON t12.mfr_id = t13.mfr_id;

ALTER TABLE und.vw_doc_import_match_variant OWNER TO pavlov;
