﻿/*
	CREATE OR REPLACE VIEW und.vw_doc_import_match
*/

-- DROP VIEW und.vw_doc_import_match;

CREATE OR REPLACE VIEW und.vw_doc_import_match AS 
 SELECT
  t1.row_id,
  t1.import_id,
  t1.row_num,
  t1.source_name,
  t1.code,
  t1.mfr,
  t1.mfr_group,
  t1.barcode,
  t1.state,
  t1.match_product_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,  
  t3.doc_id,
  t3.doc_num,
  t3.doc_name,
  t3.doc_date,
  t3.doc_type_id,
  t3.doc_state_id,
  t3.partner_id,
  t3.doc_type_name,
  t3.doc_state_name,
  t3.partner_name,
  t11.product_name
 FROM und.doc_import_match t1
 INNER JOIN und.doc_import t2 ON t1.import_id = t2.import_id
 INNER JOIN und.vw_doc t3 ON t2.doc_id = t3.doc_id
 LEFT JOIN und.product t11 ON t1.match_product_id = t11.product_id
 ;

ALTER TABLE und.vw_doc_import_match OWNER TO pavlov;
