﻿/*
	insert into cabinet.service 64
*/

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, price, need_key, "NeedCheckVersionLicense", is_service, priority, have_spec, group_id)
values (64, '102B723D-DA32-4C06-B921-08C0561A676A', 'ЕСН 2.0', 'ЕСН 2.0', 12, 0, 'esn2', 0, 0, 0, 0, true, 33, false, 3);

insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('C84B2A2D-AC86-4D91-99EF-10F603F7BECC', '3.6.0.0', '3.6.0.0', 64, 0, 0);

SELECT setval('cabinet.service_id_seq', (SELECT COALESCE(max(id), 0) + 1 FROM cabinet.service), false);
SELECT setval('cabinet.version_id_seq', (SELECT COALESCE(max(id), 0) + 1 FROM cabinet.version), false);


-- select * from cabinet.service order by id desc
-- select * from cabinet.version order by id desc