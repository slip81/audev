﻿/*
	CREATE INDEXES pg_trgm
*/

-- DROP INDEX und.und_doc_import_match_source_name1_idx;

CREATE INDEX und_doc_import_match_source_name1_idx
  ON und.doc_import_match 
  USING GIST 
  --USING GIN
  (source_name und.gist_trgm_ops);
  --(source_name und.gin_trgm_ops);

-- DROP INDEX und.und_partner_good_partner_name1_idx;

CREATE INDEX und_partner_good_partner_name1_idx
  ON und.partner_good 
  USING GIST 
  --USING GIN
  (partner_name und.gist_trgm_ops);
  --(partner_name und.gin_trgm_ops);