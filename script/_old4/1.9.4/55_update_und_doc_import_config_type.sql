﻿/*
	update und.doc_import_config_type
*/

update und.doc_import_config_type set config_type_name = 'XML-файл', source_partner_id = null, is_source_partner_from_file = true where config_type_id = 3;
update und.doc_import_config_type_xml set source_partner_name_element = 'Name' where config_type_id = 3;

delete from und.doc_import_config_type_xml where config_type_id in (4, 5);
delete from und.doc_import_config_type where config_type_id in (4, 5);

--update und.doc_import_config_type set config_type_name = 'XML-файл Агроресурсы Тюмень', source_partner_id = 3, is_source_partner_from_file = false where config_type_id = 3;
--update und.doc_import_config_type_xml set source_partner_name_element = null where config_type_id = 3;

-- select * from und.doc_import_config_type order by config_type_id
-- select * from und.doc_import_config_type_xml order by config_type_id
-- select * from und.partner