﻿/*
	CREATE OR REPLACE FUNCTION und.move_import_to_match
*/

-- DROP FUNCTION und.move_import_to_match(integer, character varying);

CREATE OR REPLACE FUNCTION und.move_import_to_match(
    IN _import_id integer,
    IN _user_name character varying,
    OUT result integer)
  RETURNS integer AS
$BODY$
DECLARE
    _row_id integer;
    _doc_id integer;
    _doc_lc_id integer;
    _doc_num integer;
BEGIN
	result:= 0;

	_doc_num := (select coalesce(count(doc_id), 0) + 1 from und.doc);

	insert into und.doc (
	  doc_num,
	  doc_name,
	  doc_date,
	  doc_type_id,
	  doc_state_id,
	  partner_id,
	  partner_target_id,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user
	)
	select 
	  _doc_num,
	  t1.file_name,
	  current_date,
	  3,
	  3,
	  t1.source_partner_id,
	  null,
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name
	from und.doc_import t1	
	where t1.import_id = _import_id
	returning doc_id into _doc_id;

	insert into und.doc_lc (
	  doc_id,
	  doc_state_id,
	  lc_date,	  
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user	
	) values (
	  _doc_id,
	  3,
	  clock_timestamp(),
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name	  
	)
	returning doc_lc_id into _doc_lc_id;

	update und.doc_import
	set doc_id = _doc_id
	where import_id = _import_id;

	with match_cte1 as (
	select distinct
	  t1.row_num,	  
	  trim(coalesce(t1.source_name, '')) as source_name,
	  trim(coalesce(t1.code, '')) as code,	  
	  trim(coalesce(t1.mfr, '')) as mfr,
	  trim(coalesce(t1.mfr_group, '')) as mfr_group,
	  trim(coalesce(t1.barcode, '')) as barcode
	from und.doc_import_row t1
	where t1.import_id = _import_id
	and trim(coalesce(t1.source_name, '')) != ''
	)
	insert into und.doc_import_match (
	  import_id,
	  row_num,
	  source_name,
	  code,
	  mfr,
	  mfr_group,
	  barcode,
	  state,
	  match_product_id,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user,
	  is_deleted
	)
	select
	  _import_id,
	  t1.row_num,
	  t1.source_name,
	  t1.code,
	  t1.mfr,
	  t1.mfr_group,
	  t1.barcode,
	  0,
	  null,
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name,
	  false
	from match_cte1 t1;	

	-- !!!
	-- todo: insert into und.doc_import_match_variant

	insert into und.doc_lc (
	  doc_id,
	  doc_state_id,
	  lc_date,	  
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user	
	) values (
	  _doc_id,
	  4,
	  clock_timestamp(),
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name	  
	)
	returning doc_lc_id into _doc_lc_id;	

	result := (select count(row_id) from und.doc_import_match where import_id = _import_id);

	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.move_import_to_match(integer, character varying) OWNER TO pavlov;
