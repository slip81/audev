﻿/*
	CREATE OR REPLACE VIEW und.vw_doc
*/

-- DROP VIEW und.vw_doc;

CREATE OR REPLACE VIEW und.vw_doc AS 
 SELECT
   t1.doc_id,
   t1.doc_num,
   t1.doc_name,
   t1.doc_date,
   t1.doc_type_id,
   t1.doc_state_id,
   t1.partner_id,
   t1.partner_target_id,
   t1.crt_date,
   t1.crt_user,
   t1.upd_date,
   t1.upd_user,
   t11.doc_type_name,
   t12.doc_state_name,
   t13.partner_name
 FROM und.doc t1
 LEFT JOIN und.doc_type t11 on t1.doc_type_id = t11.doc_type_id
 LEFT JOIN und.doc_state t12 on t1.doc_state_id = t12.doc_state_id
 LEFT JOIN und.partner t13 on t1.partner_id = t13.partner_id
 ;

ALTER TABLE und.vw_doc OWNER TO pavlov;
