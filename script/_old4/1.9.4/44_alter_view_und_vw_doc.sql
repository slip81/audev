﻿/*
	CREATE OR REPLACE VIEW und.vw_doc
*/

-- DROP VIEW und.vw_doc;

CREATE OR REPLACE VIEW und.vw_doc AS 
 SELECT t1.doc_id,
    t1.doc_num,
    t1.doc_name,
    t1.doc_date,
    t1.doc_type_id,
    t1.doc_state_id,
    t1.partner_id,
    t1.partner_target_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t11.doc_type_name,
    t12.doc_state_name,
    t13.partner_name,
    t15.row_cnt,
    t15.unmatch_cnt,
    t15.new_cnt,
    t15.syn_cnt
   FROM und.doc t1
     LEFT JOIN und.doc_type t11 ON t1.doc_type_id = t11.doc_type_id
     LEFT JOIN und.doc_state t12 ON t1.doc_state_id = t12.doc_state_id
     LEFT JOIN und.partner t13 ON t1.partner_id = t13.partner_id
     LEFT JOIN und.doc_import t14 ON t1.doc_id = t14.doc_id
     LEFT JOIN LATERAL (
     SELECT count(x1.row_id) as row_cnt,
       sum(case when x1.state = 0 then 1 else 0 end) as unmatch_cnt,
       sum(case when x1.state = 1 then 1 else 0 end) as new_cnt,
       sum(case when x1.state = 2 then 1 else 0 end) as syn_cnt,
       x1.import_id
     FROM und.doc_import_match x1
     WHERE x1.import_id = t14.import_id
     GROUP BY x1.import_id
     ) t15 ON t14.import_id = t15.import_id
     ;

ALTER TABLE und.vw_doc OWNER TO pavlov;
