﻿/*
	CREATE OR REPLACE FUNCTION und.del_data
*/

-- DROP FUNCTION und.del_data(boolean, boolean, boolean);

CREATE OR REPLACE FUNCTION und.del_data(
    _del_import_data boolean,
    _del_good_data boolean,
    _del_product_data boolean)
  RETURNS void AS
$BODY$
BEGIN	
	if (_del_good_data) then
		delete from und.partner_good;
		delete from und.good;
	end if;
	if (_del_product_data) then
		delete from und.product_alias;
		delete from und.product;
		delete from und.manufacturer;
		delete from und.doc_lc;
		delete from und.doc;	
	end if;
	if (_del_import_data = true) then
		delete from und.doc_import_match_variant;
		delete from und.doc_import_match;
		delete from und.doc_import_good;
		delete from und.doc_import_row;
		delete from und.doc_import;
	end if;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.del_data(boolean, boolean, boolean) OWNER TO pavlov;
