﻿/*
	ALTER TABLE und.doc_import_match ADD COLUMNs match_good_id, is_approved
*/

ALTER TABLE und.doc_import_match ADD COLUMN match_good_id integer;
ALTER TABLE und.doc_import_match ADD COLUMN is_approved boolean NOT NULL DEFAULT false;


ALTER TABLE und.doc_import_match
  ADD CONSTRAINT doc_import_match_match_good_id_fkey FOREIGN KEY (match_good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;