﻿/*
	ALTER TABLE und.good ADD COLUMNs doc_id, doc_row_id
*/

ALTER TABLE und.good ADD COLUMN doc_id integer;
ALTER TABLE und.good ADD COLUMN doc_row_id integer;
