﻿/*
	CREATE TABLE und.doc_import_log
*/

-- DROP TABLE und.doc_import_log;

CREATE TABLE und.doc_import_log
(
  log_id serial NOT NULL,
  log_type_id integer NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  is_active boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope5(),
  CONSTRAINT doc_import_log_pkey PRIMARY KEY (log_id),
  CONSTRAINT doc_import_log_log_type_id_fkey FOREIGN KEY (log_type_id)
      REFERENCES und.doc_import_log_type (log_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_log OWNER TO pavlov;


-- DROP TRIGGER und_doc_import_log_set_stamp_trg ON und.doc_import_log;

CREATE TRIGGER und_doc_import_log_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_import_log
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
