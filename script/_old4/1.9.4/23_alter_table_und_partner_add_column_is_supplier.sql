﻿/*
	ALTER TABLE und.partner ADD COLUMN is_supplier
*/

ALTER TABLE und.partner ADD COLUMN is_supplier boolean NOT NULL DEFAULT false;

UPDATE und.partner
SET is_supplier = true
WHERE partner_id > 1;

-- select * from und.partner 