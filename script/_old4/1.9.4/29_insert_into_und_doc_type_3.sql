﻿/*
	insert into und.doc_type 3
*/

insert into und.doc_type (doc_type_id, doc_type_name, crt_date, crt_user, upd_date, upd_user, is_deleted)
values (3, 'Справчоник поставщика для разбора', current_timestamp, 'ПМС', current_timestamp, 'ПМС', false);

-- select * from und.doc_type order by doc_type_id