﻿/*
	CREATE OR REPLACE FUNCTION und.auto_match_full
*/

-- DROP FUNCTION und.auto_match_full(integer, integer, integer, integer, integer, character varying);

CREATE OR REPLACE FUNCTION und.auto_match_full(
    IN _doc_id integer,
    IN _is_match_by_barcode integer,
    IN _is_match_by_name integer,
    IN _is_match_by_mfr integer,
    IN _is_match_approved integer,
    IN _user_name character varying,
    OUT result integer)
  RETURNS integer AS
$BODY$
DECLARE
    _row_id integer;
BEGIN
	result:= 0;

	with cte1 as (
	select t1.row_id, t3.product_id, t3.good_id
	from und.doc_import_match t1
	inner join und.doc_import t2 on t1.import_id = t2.import_id
	inner join und.vw_partner_good t3 
	on (((lower(t1.source_name) = lower(t3.partner_name)) and (_is_match_by_name = 1)) or (_is_match_by_name != 1))
	and (((lower(t1.mfr) = lower(t3.mfr_name)) and (_is_match_by_mfr = 1)) or (_is_match_by_mfr != 1))
	and (((lower(t1.barcode) = lower(t3.barcode)) and (_is_match_by_barcode = 1)) or (_is_match_by_barcode != 1))
	where t2.doc_id = _doc_id
	and (((_is_match_approved != 1) and (t1.is_approved = false)) or (_is_match_approved = 1))
	)
	update und.doc_import_match
	set state = 2, match_product_id = product_id, match_good_id = good_id, is_approved = false, percent = '100.00', is_auto_match = true,
	upd_date = clock_timestamp(), upd_user = _user_name
	from cte1
	where und.doc_import_match.row_id = cte1.row_id
	;	

	--result := (select count(row_id) from und.doc_import_match where import_id = _import_id);	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.auto_match_full(integer, integer, integer, integer, integer, character varying) OWNER TO pavlov;
