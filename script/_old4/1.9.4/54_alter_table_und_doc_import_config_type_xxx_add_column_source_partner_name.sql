﻿/*
	ALTER TABLE und.doc_import_config_type_xxx ADD COLUMN source_partner_name
*/

ALTER TABLE und.doc_import_config_type_excel ADD COLUMN source_partner_name_col integer;
ALTER TABLE und.doc_import_config_type_xml ADD COLUMN source_partner_name_element character varying;
