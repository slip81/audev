﻿/*
	CREATE OR REPLACE VIEW und.vw_partner_good AS
*/

DROP VIEW und.vw_partner_good;

CREATE OR REPLACE VIEW und.vw_partner_good AS 
 SELECT
   t1.item_id,
   t1.partner_id,
   t1.good_id,
   t1.partner_code,
   t1.partner_name,
   t2.partner_name as source_partner_name,
   t3.product_id,
   t3.mfr_id,
   t3.barcode,
   t3.doc_id,
   t3.doc_row_id,
   t4.product_name,
   t5.mfr_name,
   t3.is_deleted
 FROM und.partner_good t1
 INNER JOIN und.partner t2 ON t1.partner_id = t2.partner_id
 INNER JOIN und.good t3 ON t1.good_id = t3.good_id
 INNER JOIN und.product t4 ON t3.product_id = t4.product_id
 INNER JOIN und.manufacturer t5 ON t3.mfr_id = t5.mfr_id
 ;

ALTER TABLE und.vw_partner_good OWNER TO pavlov;
