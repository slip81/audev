﻿/*
	insert into und.doc_import_config_type_xml 3, 4, 5
*/

insert into und.doc_import_config_type (config_type_name, source_partner_id, file_type)
values ('XML-файл Агроресурсы Тюмень', 3, 2);

insert into und.doc_import_config_type (config_type_name, source_partner_id, file_type)
values ('XML-файл Градиент Косметика', 4, 2);

insert into und.doc_import_config_type (config_type_name, source_partner_id, file_type)
values ('XML-файл Пульс', 5, 2);

insert into und.doc_import_config_type_xml (
  config_type_id,
  good_element,
  source_name_element,
  etalon_name_element,
  code_element,
  mfr_element,
  mfr_group_element,
  barcode_element
)
select
  config_type_id,
  'Tovar',
  'Name',
  null,
  'Code',
  'Izg',
  null,
  'Ean'
from und.doc_import_config_type
where source_partner_id in (3, 4, 5);

-- select * from und.doc_import_config_type order by config_type_id
-- select * from und.doc_import_config_type_xml order by config_type_id