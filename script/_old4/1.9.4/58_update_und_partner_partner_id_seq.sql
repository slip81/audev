﻿/*
	update und.partner_partner_id_seq
*/

SELECT setval('und.partner_partner_id_seq', (SELECT COALESCE(max(partner_id), 0) + 1 FROM und.partner), false);
