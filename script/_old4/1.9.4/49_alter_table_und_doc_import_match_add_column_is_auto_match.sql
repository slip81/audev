﻿/*
	ALTER TABLE und.doc_import_match ADD COLUMN is_auto_match
*/

ALTER TABLE und.doc_import_match ADD COLUMN is_auto_match boolean NOT NULL DEFAULT false;