﻿/*
	update cabinet.prj_work_exec
*/

update cabinet.prj_work_exec
set cost_fact = t2.cost_fact, cost_plan = t2.cost_plan, mess = t2.mess, state_type_id = t2.state_type_id
from cabinet.prj_work t2
where cabinet.prj_work_exec.work_id = t2.work_id;