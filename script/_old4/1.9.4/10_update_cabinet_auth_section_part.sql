﻿/*
	update cabinet.auth_section_part
*/

update cabinet.auth_section_part
set part_name = 'Задачи'
where part_id = 17;

update cabinet.auth_section_part
set part_name = 'План'
where part_id = 19;

update cabinet.auth_section_part
set part_name = 'Справочники'
where part_id = 21;

delete from cabinet.auth_role_perm 
where part_id in (18, 20, 22);

delete from cabinet.auth_user_perm 
where part_id in (18, 20, 22);

delete from cabinet.auth_section_part
where part_id in (18, 20, 22);

-- select * from cabinet.auth_section order by section_id
-- select * from cabinet.auth_section_part order by section_id, part_id;

/*
17;"Все задачи"
18;"Мои задачи"
19;"Календарь задач"
20;"Архив задач"
21;"Справочники задач"
22;"История задач"
*/