﻿/*
	CREATE TABLEs und.doc_import_config_type_xxx
*/

CREATE TABLE und.doc_import_config_type_excel
(
  config_type_id integer NOT NULL,
  list_num integer NOT NULL,
  row_num integer NOT NULL,
  source_name_col integer,
  etalon_name_col integer,
  code_col integer,
  mfr_col integer,
  mfr_group_col integer,
  barcode_col integer,
  CONSTRAINT doc_import_config_type_excel_pkey PRIMARY KEY (config_type_id),
  CONSTRAINT doc_import_config_type_excel_config_type_id_fkey FOREIGN KEY (config_type_id)
      REFERENCES und.doc_import_config_type (config_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_config_type_excel OWNER TO pavlov;


INSERT INTO und.doc_import_config_type_excel (
  config_type_id,
  list_num,
  row_num,
  source_name_col,
  etalon_name_col,
  code_col,
  mfr_col,
  mfr_group_col,
  barcode_col
)
SELECT
  config_type_id,
  list_num,
  row_num,
  source_name_col,
  etalon_name_col,
  code_col,
  mfr_col,
  mfr_group_col,
  barcode_col
 FROM und.doc_import_config_type; 


ALTER TABLE und.doc_import_config_type ADD COLUMN file_type integer NOT NULL DEFAULT 1; 
-- 1 excel, 2 xml


ALTER TABLE und.doc_import_config_type DROP COLUMN list_num;
ALTER TABLE und.doc_import_config_type DROP COLUMN row_num;
ALTER TABLE und.doc_import_config_type DROP COLUMN source_name_col;
ALTER TABLE und.doc_import_config_type DROP COLUMN etalon_name_col;
ALTER TABLE und.doc_import_config_type DROP COLUMN code_col;
ALTER TABLE und.doc_import_config_type DROP COLUMN mfr_col;
ALTER TABLE und.doc_import_config_type DROP COLUMN mfr_group_col;
ALTER TABLE und.doc_import_config_type DROP COLUMN barcode_col;

CREATE TABLE und.doc_import_config_type_xml
(
  config_type_id integer NOT NULL,
  good_element character varying,
  source_name_element character varying,
  etalon_name_element character varying,
  code_element character varying,
  mfr_element character varying,
  mfr_group_element character varying,
  barcode_element character varying,
  CONSTRAINT doc_import_config_type_xml_pkey PRIMARY KEY (config_type_id),
  CONSTRAINT doc_import_config_type_xml_config_type_id_fkey FOREIGN KEY (config_type_id)
      REFERENCES und.doc_import_config_type (config_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_config_type_xml OWNER TO pavlov;