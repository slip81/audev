﻿/*
	CREATE OR REPLACE FUNCTION und.move_import_to_etalon
*/

-- DROP FUNCTION und.move_import_to_etalon(integer, character varying);

CREATE OR REPLACE FUNCTION und.move_import_to_etalon(
    IN _import_id integer,
    IN _user_name character varying,
    OUT result integer)
  RETURNS int AS
$BODY$
DECLARE
    _row_id integer;
    _doc_id integer;
    _doc_lc_id integer;
    _doc_num integer;
BEGIN
	result:= 0;

	/*
	delete from und.product;
	delete from und.manufacturer;
	delete from und.doc_lc;
	delete from und.doc;
	*/

	_doc_num := (select coalesce(count(doc_id), 0) + 1 from und.doc);

	insert into und.doc (
	  doc_num,
	  doc_name,
	  doc_date,
	  doc_type_id,
	  doc_state_id,
	  partner_id,
	  partner_target_id,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user
	)
	select 
	  _doc_num,
	  t1.file_name,
	  current_date,
	  1,
	  3,
	  t2.source_partner_id,
	  null,
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name
	from und.doc_import t1
	inner join und.doc_import_config_type t2 on t1.config_type_id = t2.config_type_id
	where t1.import_id = _import_id
	returning doc_id into _doc_id;

	insert into und.doc_lc (
	  doc_id,
	  doc_state_id,
	  lc_date,	  
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user	
	) values (
	  _doc_id,
	  3,
	  clock_timestamp(),
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name	  
	)
	returning doc_lc_id into _doc_lc_id;

	update und.doc_import
	set doc_id = _doc_id
	where import_id = _import_id;

	with mfr_cte1 as (
	select distinct
	  trim(coalesce(t1.mfr, '')) as mfr
	from und.doc_import_row t1
	where t1.import_id = _import_id
	and trim(coalesce(t1.mfr, '')) != ''
	and not exists(select x1.mfr_id from und.manufacturer x1 where trim(lower(coalesce(x1.mfr_name, ''))) = trim(lower(coalesce(t1.mfr, ''))))	  
	)
	insert into und.manufacturer (
	  mfr_name,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user
	  )
	select
	  t1.mfr,
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name
	from mfr_cte1 t1;

	with product_cte1 as (
	select distinct	  
	  trim(coalesce(t1.source_name, '')) as source_name
	from und.doc_import_row t1
	where t1.import_id = _import_id
	and trim(coalesce(t1.source_name, '')) != ''
	and not exists(select x1.product_id from und.product x1 where trim(lower(coalesce(x1.product_name, ''))) = trim(lower(coalesce(t1.source_name, ''))))
	)
	insert into und.product (
	  product_name,
	  doc_id,
	  doc_row_id,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user
	  )	
	select
	  t1.source_name,
	  _doc_id,
	  null,
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name
	from product_cte1 t1;	  

	insert into und.doc_lc (
	  doc_id,
	  doc_state_id,
	  lc_date,	  
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user	
	) values (
	  _doc_id,
	  4,
	  clock_timestamp(),
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name	  
	)
	returning doc_lc_id into _doc_lc_id;	

	result := (select count(product_id) from und.product where doc_id = _doc_id);

	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION und.move_import_to_etalon(integer, character varying) OWNER TO pavlov;
