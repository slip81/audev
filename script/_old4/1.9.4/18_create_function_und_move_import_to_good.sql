﻿/*
	CREATE OR REPLACE FUNCTION und.move_import_to_good
*/

-- DROP FUNCTION und.move_import_to_good(integer, character varying);

CREATE OR REPLACE FUNCTION und.move_import_to_good(
    IN _import_id integer,
    IN _user_name character varying,
    OUT result integer)
  RETURNS integer AS
$BODY$
DECLARE
    _row_id integer;
    _doc_id integer;
    _doc_lc_id integer;
    _doc_num integer;
    _source_partner_id integer;
BEGIN
	result:= 0;

	_doc_num := (select coalesce(count(doc_id), 0) + 1 from und.doc);

	_source_partner_id := 
	(select t2.source_partner_id 
	from und.doc_import t1
	inner join und.doc_import_config_type t2 on t1.config_type_id = t2.config_type_id
	where t1.import_id = _import_id
	);

	insert into und.doc (
	  doc_num,
	  doc_name,
	  doc_date,
	  doc_type_id,
	  doc_state_id,
	  partner_id,
	  partner_target_id,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user
	)
	select 
	  _doc_num,
	  t1.file_name,
	  current_date,
	  1,
	  3,
	  _source_partner_id,
	  null,
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name
	from und.doc_import t1
	inner join und.doc_import_config_type t2 on t1.config_type_id = t2.config_type_id
	where t1.import_id = _import_id
	returning doc_id into _doc_id;

	insert into und.doc_lc (
	  doc_id,
	  doc_state_id,
	  lc_date,	  
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user	
	) values (
	  _doc_id,
	  3,
	  clock_timestamp(),
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name	  
	)
	returning doc_lc_id into _doc_lc_id;

	update und.doc_import
	set doc_id = _doc_id
	where import_id = _import_id;

	with good_cte1 as (
	select distinct	  
	  t2.product_id,
	  t3.mfr_id,
	  trim(coalesce(t1.barcode, '')) as barcode,
	  trim(coalesce(t1.code, '')) as code,
	  trim(coalesce(t1.source_name, '')) as source_name
	from und.doc_import_row t1
	--inner join und.product t2 on trim(coalesce(t1.source_name, '')) = trim(coalesce(t2.product_name, ''))
	inner join und.product t2 on trim(coalesce(t1.etalon_name, '')) = trim(coalesce(t2.product_name, ''))
	inner join und.manufacturer t3 on trim(coalesce(t1.mfr, '')) = trim(coalesce(t3.mfr_name, ''))
	where t1.import_id = _import_id
	and trim(coalesce(t1.source_name, '')) != ''
	and not exists (
	select x1.good_id
	from und.vw_partner_good x1
	where x1.partner_id = _source_partner_id
	--and trim(coalesce(t1.source_name, '')) = trim(coalesce(x1.partner_name, ''))
	and trim(coalesce(t1.etalon_name, '')) = trim(coalesce(x1.product_name, ''))
	and trim(coalesce(t1.mfr, '')) = trim(coalesce(x1.mfr_name, ''))
	and trim(coalesce(t1.barcode, '')) = trim(coalesce(x1.barcode, ''))
	)
	)
	insert into und.doc_import_good (
	  doc_id,
	  product_id,
	  mfr_id,
	  barcode,
	  partner_id,
	  partner_code,
	  partner_name
	)
	select distinct
	  _doc_id,
	  t1.product_id,
	  t1.mfr_id,
	  t1.barcode,
	 _source_partner_id,  
	 t1.code,
	 t1.source_name
	from good_cte1 t1;	

	insert into und.good (
	  product_id,
	  mfr_id,
	  barcode,  
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user,
	  doc_id
	)
	select distinct
	  t1.product_id,
	  t1.mfr_id,
	  t1.barcode,
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name,
	  _doc_id
	from und.doc_import_good t1
	where t1.doc_id = _doc_id;

	insert into und.partner_good (
	  partner_id,
	  good_id,
	  partner_code,
	  partner_name,
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user	
	)	
	select distinct
	  _source_partner_id,
	  t2.good_id,	  
	  t1.partner_code,
	  t1.partner_name,
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name	  
	from und.doc_import_good t1
	inner join und.good t2 on t1.doc_id = t2.doc_id and t1.product_id = t2.product_id and t1.mfr_id = t2.mfr_id and trim(coalesce(t1.barcode, '')) = trim(coalesce(t2.barcode, ''))
	where t1.doc_id = _doc_id;	


	insert into und.doc_lc (
	  doc_id,
	  doc_state_id,
	  lc_date,	  
	  crt_date,
	  crt_user,
	  upd_date,
	  upd_user	
	) values (
	  _doc_id,
	  4,
	  clock_timestamp(),
	  clock_timestamp(),
	  _user_name,
	  clock_timestamp(),
	  _user_name	  
	)
	returning doc_lc_id into _doc_lc_id;		

	result := (select count(good_id) from und.good where doc_id = _doc_id);

	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.move_import_to_good(integer, character varying) OWNER TO pavlov;
