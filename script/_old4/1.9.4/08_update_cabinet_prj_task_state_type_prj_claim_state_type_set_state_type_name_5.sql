﻿/*
	UPDATE cabinet.prj_task_state_type, cabinet.prj_claim_state_type SET state_type_name
*/

UPDATE cabinet.prj_task_state_type
SET state_type_name = 'Принято', templ = '<span class="text-info">Принято</span>'
WHERE state_type_id = 5;

UPDATE cabinet.prj_claim_state_type
SET state_type_name = 'Принято', templ = '<span class="text-info">Принято</span>'
WHERE state_type_id = 5;

-- select * from cabinet.prj_claim_state_type order by state_type_id;
-- select * from cabinet.prj_task_state_type order by state_type_id;