﻿/*
	ALTER TABLE und.good ADD COLUMNs log_id, tmp_row_id
*/

ALTER TABLE und.good ADD COLUMN log_id integer;
ALTER TABLE und.good ADD COLUMN tmp_row_id integer;