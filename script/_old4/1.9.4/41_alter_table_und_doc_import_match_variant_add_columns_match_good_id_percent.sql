﻿/*
	ALTER TABLE und.doc_import_match_variant ADD COLUMNs match_good_id, percent
*/

ALTER TABLE und.doc_import_match_variant ADD COLUMN match_good_id integer;
ALTER TABLE und.doc_import_match_variant ADD COLUMN percent character varying;