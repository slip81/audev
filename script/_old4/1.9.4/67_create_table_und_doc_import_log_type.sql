﻿/*
	CREATE TABLE und.doc_import_log_type
*/

-- DROP TABLE und.doc_import_log_type;

CREATE TABLE und.doc_import_log_type
(
  log_type_id serial NOT NULL,
  log_type_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope5(),
  CONSTRAINT doc_import_log_type_pkey PRIMARY KEY (log_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_log_type OWNER TO pavlov;

-- Trigger: und_doc_state_set_stamp_trg on und.doc_state

-- DROP TRIGGER und_doc_import_log_type_set_stamp_trg ON und.doc_import_log_type;

CREATE TRIGGER und_doc_import_log_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_import_log_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

INSERT INTO und.doc_import_log_type (log_type_id, log_type_name, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (1, 'Добавление в эталонные', current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.doc_import_log_type (log_type_id, log_type_name, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (2, 'Добавление в синонимы', current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

INSERT INTO und.doc_import_log_type (log_type_id, log_type_name, crt_date, crt_user, upd_date, upd_user, is_deleted)
VALUES (3, 'Удаление сопоставления', current_timestamp, 'pavlov', current_timestamp, 'pavlov', false);

-- select * from und.doc_import_log_type order by log_type_id