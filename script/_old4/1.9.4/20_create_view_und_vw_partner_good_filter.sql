﻿/*
	CREATE OR REPLACE VIEW und.vw_partner_good_filter
*/

-- DROP VIEW und.vw_partner_good_filter;

CREATE OR REPLACE VIEW und.vw_partner_good_filter AS 
 SELECT t1.partner_id,
    (((t1.source_partner_name::text || ' ('::text) || count(t1.good_id)::character varying::text) || ')'::text)::character varying AS partner_name,
    count(t1.good_id) AS good_cnt
   FROM und.vw_partner_good t1     
  GROUP BY t1.partner_id, t1.source_partner_name;
  
ALTER TABLE und.vw_partner_good_filter OWNER TO pavlov;
