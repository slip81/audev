﻿/*
	CREATE main TABLEs und.***
*/

------------------------------------------------------
/* 0 -----------------------------------------------*/
------------------------------------------------------

DROP SCHEMA und CASCADE;

CREATE SCHEMA und AUTHORIZATION pavlov;


-- DROP SEQUENCE und.sys_uid_sequence;

CREATE SEQUENCE und.sys_uid_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
ALTER TABLE und.sys_uid_sequence OWNER TO pavlov;

-- DROP FUNCTION und.sysuidgen_scope1();

CREATE OR REPLACE FUNCTION und.sysuidgen_scope1(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (1 << 10) | (nextval('und.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.sysuidgen_scope1() OWNER TO pavlov;


-- DROP FUNCTION und.sysuidgen_scope2();

CREATE OR REPLACE FUNCTION und.sysuidgen_scope2(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (2 << 10) | (nextval('und.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.sysuidgen_scope2() OWNER TO pavlov;


-- DROP FUNCTION und.sysuidgen_scope3();

CREATE OR REPLACE FUNCTION und.sysuidgen_scope3(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (3 << 10) | (nextval('und.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.sysuidgen_scope3() OWNER TO pavlov;

-- DROP FUNCTION und.sysuidgen_scope4();

CREATE OR REPLACE FUNCTION und.sysuidgen_scope4(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (4 << 10) | (nextval('und.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.sysuidgen_scope4() OWNER TO pavlov;

-- DROP FUNCTION und.sysuidgen_scope5();

CREATE OR REPLACE FUNCTION und.sysuidgen_scope5(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (5 << 10) | (nextval('und.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.sysuidgen_scope5() OWNER TO pavlov;


-- DROP FUNCTION und.set_stamp();

CREATE OR REPLACE FUNCTION und.set_stamp()
  RETURNS trigger AS
$BODY$
BEGIN
    NEW.sysrowstamp := (extract(epoch from clock_timestamp()) * 1000)::bigint;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION und.set_stamp() OWNER TO postgres;

------------------------------------------------------
/* 1 -----------------------------------------------*/
------------------------------------------------------

-- DROP TABLE und.doc_type;

CREATE TABLE und.doc_type
(
  doc_type_id serial NOT NULL,
  doc_type_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT doc_type_pkey PRIMARY KEY (doc_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_type OWNER TO pavlov;

-- DROP TRIGGER und_doc_type_set_stamp_trg ON und.doc_type;

CREATE TRIGGER und_doc_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.doc_state;

CREATE TABLE und.doc_state
(
  doc_state_id serial NOT NULL,
  doc_state_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT doc_state_pkey PRIMARY KEY (doc_state_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_type OWNER TO pavlov;

-- DROP TRIGGER und_doc_state_set_stamp_trg ON und.doc_state;

CREATE TRIGGER und_doc_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_state
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.partner;

CREATE TABLE und.partner
(
  partner_id serial NOT NULL,
  partner_name character varying,
  parent_id integer,
  is_source boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT partner_pkey PRIMARY KEY (partner_id),
  CONSTRAINT partner_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.partner OWNER TO pavlov;

-- DROP TRIGGER und_partner_set_stamp_trg ON und.partner;

CREATE TRIGGER und_partner_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.partner
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.partner_wh;

CREATE TABLE und.partner_wh
(
  wh_id serial NOT NULL,  
  wh_name character varying,
  partner_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT partner_wh_pkey PRIMARY KEY (wh_id),
  CONSTRAINT partner_wh_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.partner_wh OWNER TO pavlov;

-- DROP TRIGGER und_partner_wh_set_stamp_trg ON und.partner_wh;

CREATE TRIGGER und_partner_wh_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.partner_wh
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.func;

CREATE TABLE und.func
(
  func_id serial NOT NULL,  
  func_name character varying,
  func_descr character varying,  
  doc_type_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT func_pkey PRIMARY KEY (func_id),
  CONSTRAINT func_doc_type_id_fkey FOREIGN KEY (doc_type_id)
      REFERENCES und.doc_type (doc_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.func OWNER TO pavlov;

-- DROP TRIGGER und_func_set_stamp_trg ON und.func;

CREATE TRIGGER und_func_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.func
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.partner_func;

CREATE TABLE und.partner_func
(
  item_id serial NOT NULL,  
  partner_id integer NOT NULL,
  func_id integer NOT NULL,
  date_expire date,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT partner_func_pkey PRIMARY KEY (item_id),
  CONSTRAINT partner_func_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT partner_func_func_id_fkey FOREIGN KEY (func_id)
      REFERENCES und.func (func_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.partner_func OWNER TO pavlov;

-- DROP TRIGGER und_partner_func_set_stamp_trg ON und.partner_func;

CREATE TRIGGER und_partner_func_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.partner_func
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
  
------------------------------------------------------

-- DROP TABLE und.partner_doc_type;

CREATE TABLE und.partner_doc_type
(
  item_id serial NOT NULL,  
  partner_id integer NOT NULL,
  doc_type_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT partner_doc_type_pkey PRIMARY KEY (item_id),
  CONSTRAINT partner_doc_type_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT partner_doc_type_doc_type_id_fkey FOREIGN KEY (doc_type_id)
      REFERENCES und.doc_type (doc_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.partner_doc_type OWNER TO pavlov;

-- DROP TRIGGER und_partner_func_set_stamp_trg ON und.partner_doc_type;

CREATE TRIGGER und_partner_doc_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.partner_doc_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
  
------------------------------------------------------

-- DROP TABLE und.doc;

CREATE TABLE und.doc
(
  doc_id serial NOT NULL,  
  doc_num character varying,
  doc_name character varying,
  doc_date date,
  doc_type_id integer,
  doc_state_id integer,
  partner_id integer,
  partner_target_id integer,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT doc_pkey PRIMARY KEY (doc_id),
  CONSTRAINT doc_doc_type_id_fkey FOREIGN KEY (doc_type_id)
      REFERENCES und.doc_type (doc_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT doc_doc_state_id_fkey FOREIGN KEY (doc_state_id)
      REFERENCES und.doc_state (doc_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT partner_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT partner_partner_target_id_fkey FOREIGN KEY (partner_target_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc OWNER TO pavlov;

-- DROP TRIGGER und_doc_set_stamp_trg ON und.doc;

CREATE TRIGGER und_doc_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.doc_data;

CREATE TABLE und.doc_data
(
  doc_id integer NOT NULL,  
  data_binary bytea,
  CONSTRAINT doc_data_pkey PRIMARY KEY (doc_id),
  CONSTRAINT doc_data_doc_id_fkey FOREIGN KEY (doc_id)
      REFERENCES und.doc (doc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_data OWNER TO pavlov;

------------------------------------------------------

-- DROP TABLE und.doc_lc;

CREATE TABLE und.doc_lc
(
  doc_lc_id serial NOT NULL,  
  doc_id integer NOT NULL,
  doc_state_id integer,
  lc_date timestamp without time zone,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope1(),  
  CONSTRAINT doc_lc_pkey PRIMARY KEY (doc_lc_id),
  CONSTRAINT doc_lc_doc_id_fkey FOREIGN KEY (doc_id)
      REFERENCES und.doc (doc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT doc_lc_doc_state_id_fkey FOREIGN KEY (doc_state_id)
      REFERENCES und.doc_state (doc_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_lc OWNER TO pavlov;

-- DROP TRIGGER und_doc_lc_set_stamp_trg ON und.doc_lc;

CREATE TRIGGER und_doc_lc_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_lc
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------
/* 2 -----------------------------------------------*/
------------------------------------------------------ 

-- DROP TABLE und.area;

CREATE TABLE und.area
(
  area_id serial NOT NULL,  
  area_name character varying,
  area_short_name character varying,
  alpha2 character varying,
  alpha3 character varying,    
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT area_pkey PRIMARY KEY (area_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.area OWNER TO pavlov;

-- DROP TRIGGER und_area_set_stamp_trg ON und.area;

CREATE TRIGGER und_area_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.area
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------ 

-- DROP TABLE und.manufacturer;

CREATE TABLE und.manufacturer
(
  mfr_id serial NOT NULL,  
  mfr_name character varying,
  parent_id integer,
  area_id integer,
  first_packer character varying,
  second_packer character varying,
  control character varying,    
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT manufacturer_pkey PRIMARY KEY (mfr_id),
  CONSTRAINT manufacturer_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES und.manufacturer (mfr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT manufacturer_area_id_fkey FOREIGN KEY (area_id)
      REFERENCES und.area (area_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.manufacturer OWNER TO pavlov;

-- DROP TRIGGER und_manufacturer_set_stamp_trg ON und.manufacturer;

CREATE TRIGGER und_manufacturer_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.manufacturer
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp(); 

------------------------------------------------------ 

-- DROP TABLE und.manufacturer_alias;

CREATE TABLE und.manufacturer_alias
(
  alias_id serial NOT NULL,
  mfr_id integer NOT NULL,
  alias_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT manufacturer_alias_pkey PRIMARY KEY (alias_id),
  CONSTRAINT manufacturer_alias_mfr_id_fkey FOREIGN KEY (mfr_id)
      REFERENCES und.manufacturer (mfr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.manufacturer_alias OWNER TO pavlov;

-- DROP TRIGGER und_manufacturer_alias_set_stamp_trg ON und.manufacturer_alias;

CREATE TRIGGER und_manufacturer_alias_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.manufacturer_alias
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp(); 

------------------------------------------------------ 

-- DROP TABLE und.category;

CREATE TABLE und.category
(
  category_id serial NOT NULL,  
  category_name character varying,
  parent_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT category_pkey PRIMARY KEY (category_id),
  CONSTRAINT category_parent_id_fkey FOREIGN KEY (parent_id)
      REFERENCES und.category (category_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.category OWNER TO pavlov;

-- DROP TRIGGER und_category_set_stamp_trg ON und.category;

CREATE TRIGGER und_category_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.category
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp(); 

------------------------------------------------------ 

-- DROP TABLE und.brand;

CREATE TABLE und.brand
(
  brand_id serial NOT NULL,  
  brand_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT brand_pkey PRIMARY KEY (brand_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.brand OWNER TO pavlov;

-- DROP TRIGGER und_brand_set_stamp_trg ON und.brand;

CREATE TRIGGER und_brand_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.brand
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();   

------------------------------------------------------ 

-- DROP TABLE und.pharm_group;

CREATE TABLE und.pharm_group
(
  pharm_group_id serial NOT NULL,  
  pharm_group_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT pharm_group_pkey PRIMARY KEY (pharm_group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.pharm_group OWNER TO pavlov;

-- DROP TRIGGER und_pharm_group_set_stamp_trg ON und.pharm_group;

CREATE TRIGGER und_pharm_group_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.pharm_group
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();    

------------------------------------------------------ 

-- DROP TABLE und.atc;

CREATE TABLE und.atc
(
  atc_id serial NOT NULL,  
  atc_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT atc_pkey PRIMARY KEY (atc_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.atc OWNER TO pavlov;

-- DROP TRIGGER und_atc_set_stamp_trg ON und.atc;

CREATE TRIGGER und_atc_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.atc
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();    

------------------------------------------------------

-- DROP TABLE und.inn;

CREATE TABLE und.inn
(
  inn_id serial NOT NULL,  
  inn_name character varying,
  inn_name_eng character varying,
  inn_name_lat character varying,
  atc_id integer,
  pharm_action bytea,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT inn_pkey PRIMARY KEY (inn_id),
  CONSTRAINT inn_atc_id_fkey FOREIGN KEY (atc_id)
      REFERENCES und.atc (atc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.inn OWNER TO pavlov;

-- DROP TRIGGER und_inn_set_stamp_trg ON und.inn;

CREATE TRIGGER und_inn_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.inn
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();    

------------------------------------------------------

-- DROP TABLE und.pharm_market;

CREATE TABLE und.pharm_market
(
  pharm_market_id serial NOT NULL,  
  pharm_market_name character varying, 
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT pharm_market_pkey PRIMARY KEY (pharm_market_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.pharm_market OWNER TO pavlov;

-- DROP TRIGGER und_pharm_market_set_stamp_trg ON und.pharm_market;

CREATE TRIGGER und_pharm_market_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.pharm_market
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();    

------------------------------------------------------

-- DROP TABLE und.package_kind;

CREATE TABLE und.package_kind
(
  package_kind_id serial NOT NULL,  
  package_kind_name character varying, 
  package_kind_name_full character varying, 
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT package_kind_pkey PRIMARY KEY (package_kind_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.package_kind OWNER TO pavlov;

-- DROP TRIGGER und_package_kind_set_stamp_trg ON und.package_kind;

CREATE TRIGGER und_package_kind_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.package_kind
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();    

------------------------------------------------------

-- DROP TABLE und.misprint;

CREATE TABLE und.misprint
(
  misprint_id serial NOT NULL,  
  wrong_name character varying, 
  correct_full character varying, 
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT misprint_pkey PRIMARY KEY (misprint_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.misprint OWNER TO pavlov;

-- DROP TRIGGER und_misprint_set_stamp_trg ON und.misprint;

CREATE TRIGGER und_misprint_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.misprint
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();    

------------------------------------------------------

-- DROP TABLE und.measure_unit;

CREATE TABLE und.measure_unit
(
  unit_id serial NOT NULL,  
  unit_name character varying, 
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope2(),  
  CONSTRAINT measure_unit_pkey PRIMARY KEY (unit_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.measure_unit OWNER TO pavlov;

-- DROP TRIGGER und_measure_unit_set_stamp_trg ON und.measure_unit;

CREATE TRIGGER und_measure_unit_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.measure_unit
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------
/* 3 -----------------------------------------------*/
------------------------------------------------------

-- DROP TABLE und.space;

CREATE TABLE und.space
(
  space_id serial NOT NULL,  
  space_value numeric,
  unit_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT space_pkey PRIMARY KEY (space_id),
  CONSTRAINT space_unit_id_fkey FOREIGN KEY (unit_id)
      REFERENCES und.measure_unit (unit_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION 
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.space OWNER TO pavlov;

-- DROP TRIGGER und_space_set_stamp_trg ON und.space;

CREATE TRIGGER und_space_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.space
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.dosage;

CREATE TABLE und.dosage
(
  dosage_id serial NOT NULL,  
  weight_value numeric,
  weight_unit_id integer,
  concentration_value numeric,
  concentration_unit_id integer,
  effect_value numeric,
  effect_unit_id integer,      
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT dosage_pkey PRIMARY KEY (dosage_id),
  CONSTRAINT dosage_weight_unit_id_fkey FOREIGN KEY (weight_unit_id)
      REFERENCES und.measure_unit (unit_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT dosage_concentration_unit_id_fkey FOREIGN KEY (concentration_unit_id)
      REFERENCES und.measure_unit (unit_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT dosage_effect_unit_id_fkey FOREIGN KEY (effect_unit_id)
      REFERENCES und.measure_unit (unit_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.dosage OWNER TO pavlov;

-- DROP TRIGGER und_dosage_set_stamp_trg ON und.dosage;

CREATE TRIGGER und_dosage_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.dosage
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();  

------------------------------------------------------

-- DROP TABLE und.metaname;

CREATE TABLE und.metaname
(
  metaname_id serial NOT NULL,  
  metaname_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT metaname_pkey PRIMARY KEY (metaname_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.metaname OWNER TO pavlov;

-- DROP TRIGGER und_metaname_set_stamp_trg ON und.metaname;

CREATE TRIGGER und_metaname_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.metaname
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();  

------------------------------------------------------

-- DROP TABLE und.product_form;

CREATE TABLE und.product_form
(
  product_form_id serial NOT NULL,  
  product_form_name character varying,
  product_form_name_full character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT product_form_pkey PRIMARY KEY (product_form_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.product_form OWNER TO pavlov;

-- DROP TRIGGER und_product_form_set_stamp_trg ON und.product_form;

CREATE TRIGGER und_product_form_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.product_form
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.okdp;

CREATE TABLE und.okdp
(
  okdp_id serial NOT NULL,  
  okdp_name character varying,
  okdp_text character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT okdp_pkey PRIMARY KEY (okdp_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.okdp OWNER TO pavlov;

-- DROP TRIGGER und_okdp_set_stamp_trg ON und.okdp;

CREATE TRIGGER und_okdp_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.okdp
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.product;

CREATE TABLE und.product
(
  product_id serial NOT NULL,  
  product_form_id integer,
  pharm_market_id integer,
  inn_id integer,
  dosage_id integer,
  brand_id integer,
  space_id integer,
  metaname_id integer,  
  product_name character varying,
  trade_name character varying,
  trade_name_lat character varying,
  trade_name_eng character varying,
  storage_period numeric,
  is_vital boolean NOT NULL DEFAULT false,
  is_powerful boolean NOT NULL DEFAULT false,
  is_poison boolean NOT NULL DEFAULT false,
  is_receipt boolean NOT NULL DEFAULT false,
  drug integer,
  size character varying,
  potion_amount numeric,
  pack_count integer,
  link character varying,
  mess character varying, 
  doc_id integer,
  doc_row_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT product_pkey PRIMARY KEY (product_id),
  CONSTRAINT product_product_form_id_fkey FOREIGN KEY (product_form_id)  
      REFERENCES und.product_form (product_form_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_pharm_market_id_fkey FOREIGN KEY (pharm_market_id)
      REFERENCES und.pharm_market (pharm_market_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,     
  CONSTRAINT product_inn_id_fkey FOREIGN KEY (inn_id)
      REFERENCES und.inn (inn_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,       
  CONSTRAINT product_dosage_id_fkey FOREIGN KEY (dosage_id)
      REFERENCES und.dosage (dosage_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT product_brand_id_fkey FOREIGN KEY (brand_id)
      REFERENCES und.brand (brand_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_space_id_fkey FOREIGN KEY (space_id)
      REFERENCES und.space (space_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT product_metaname_id_fkey FOREIGN KEY (metaname_id)
      REFERENCES und.metaname (metaname_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.product OWNER TO pavlov;

-- DROP TRIGGER und_product_set_stamp_trg ON und.product;

CREATE TRIGGER und_product_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.product
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.product_category;

CREATE TABLE und.product_category
(
  item_id serial NOT NULL,
  product_id integer NOT NULL,
  category_id integer NOT NULL,   
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT product_category_pkey PRIMARY KEY (item_id),
  CONSTRAINT product_category_product_id_fkey FOREIGN KEY (product_id)  
      REFERENCES und.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_category_category_id_fkey FOREIGN KEY (category_id)  
      REFERENCES und.category (category_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.product_category OWNER TO pavlov;

-- DROP TRIGGER und_product_category_set_stamp_trg ON und.product_category;

CREATE TRIGGER und_product_category_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.product_category
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.product_alias;

CREATE TABLE und.product_alias
(
  alias_id serial NOT NULL,
  product_id integer NOT NULL,
  alias_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT product_alias_pkey PRIMARY KEY (alias_id),
  CONSTRAINT product_alias_product_id_fkey FOREIGN KEY (product_id)  
      REFERENCES und.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION   
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.product_alias OWNER TO pavlov;

-- DROP TRIGGER und_product_alias_set_stamp_trg ON und.product_alias;

CREATE TRIGGER und_product_alias_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.product_alias
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.product_pharm_group;

CREATE TABLE und.product_pharm_group
(
  item_id serial NOT NULL,
  product_id integer NOT NULL,
  pharm_group_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT product_pharm_group_pkey PRIMARY KEY (item_id),
  CONSTRAINT product_pharm_group_product_id_fkey FOREIGN KEY (product_id)  
      REFERENCES und.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_pharm_group_pharm_group_id_fkey FOREIGN KEY (pharm_group_id)  
      REFERENCES und.pharm_group (pharm_group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION        
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.product_pharm_group OWNER TO pavlov;

-- DROP TRIGGER und_product_pharm_group_set_stamp_trg ON und.product_pharm_group;

CREATE TRIGGER und_product_pharm_group_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.product_pharm_group
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.product_package_kind;

CREATE TABLE und.product_package_kind
(
  item_id serial NOT NULL,
  product_id integer NOT NULL,
  package_kind_id integer NOT NULL,
  embedded_pack integer,
  kft numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT product_package_kind_pkey PRIMARY KEY (item_id),
  CONSTRAINT product_package_kind_product_id_fkey FOREIGN KEY (product_id)  
      REFERENCES und.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_package_kind_package_kind_id_fkey FOREIGN KEY (package_kind_id)  
      REFERENCES und.package_kind (package_kind_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION        
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.product_package_kind OWNER TO pavlov;

-- DROP TRIGGER und_product_package_kind_set_stamp_trg ON und.product_package_kind;

CREATE TRIGGER und_product_package_kind_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.product_package_kind
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.product_okdp;

CREATE TABLE und.product_okdp
(
  item_id serial NOT NULL,
  product_id integer NOT NULL,
  okdp_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope3(),  
  CONSTRAINT product_okdp_pkey PRIMARY KEY (item_id),
  CONSTRAINT product_okdp_product_id_fkey FOREIGN KEY (product_id)  
      REFERENCES und.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT product_okdp_okdp_id_fkey FOREIGN KEY (okdp_id)  
      REFERENCES und.okdp (okdp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION        
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.product_okdp OWNER TO pavlov;

-- DROP TRIGGER und_product_okdp_set_stamp_trg ON und.product_okdp;

CREATE TRIGGER und_product_okdp_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.product_okdp
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------
/* 4 -----------------------------------------------*/
------------------------------------------------------

-- DROP TABLE und.good;

CREATE TABLE und.good
(
  good_id serial NOT NULL,
  product_id integer NOT NULL,
  mfr_id integer NOT NULL,
  barcode character varying,
  is_monitor boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT good_pkey PRIMARY KEY (good_id),
  CONSTRAINT good_product_id_fkey FOREIGN KEY (product_id)
      REFERENCES und.product (product_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT good_mfr_id_fkey FOREIGN KEY (mfr_id)
      REFERENCES und.manufacturer (mfr_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.good OWNER TO pavlov;

-- DROP TRIGGER und_good_set_stamp_trg ON und.good;

CREATE TRIGGER und_good_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.good
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.doc_row;

CREATE TABLE und.doc_row
(
  row_id serial NOT NULL,
  doc_id integer NOT NULL,
  good_id integer,
  row_text character varying,
  quantity numeric,
  price_orig numeric,
  price_income numeric,
  price_retail numeric,
  vat numeric,
  vat_income numeric,
  vat_retail numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT doc_row_pkey PRIMARY KEY (row_id),
  CONSTRAINT doc_row_doc_id_fkey FOREIGN KEY (doc_id)
      REFERENCES und.doc (doc_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT doc_row_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_row OWNER TO pavlov;

-- DROP TRIGGER und_doc_row_set_stamp_trg ON und.doc_row;

CREATE TRIGGER und_doc_row_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_row
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.doc_row_field;

CREATE TABLE und.doc_row_field
(
  field_id serial NOT NULL,
  row_id integer NOT NULL,
  field_name character varying,
  field_value character varying,
  field_value_binary bytea,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT doc_row_field_pkey PRIMARY KEY (field_id),
  CONSTRAINT doc_row_field_row_id_fkey FOREIGN KEY (row_id)
      REFERENCES und.doc_row (row_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_row_field OWNER TO pavlov;

-- DROP TRIGGER und_doc_row_field_set_stamp_trg ON und.doc_row_field;

CREATE TRIGGER und_doc_row_field_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_row_field
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.vital;

CREATE TABLE und.vital
(
  vital_id serial NOT NULL,
  good_id integer,  
  trade_name character varying,
  inn character varying,
  drug_form character varying,
  vendor character varying,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT vital_pkey PRIMARY KEY (vital_id),
  CONSTRAINT vital_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.vital OWNER TO pavlov;

-- DROP TRIGGER und_vital_set_stamp_trg ON und.vital;

CREATE TRIGGER und_vital_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.vital
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.partner_good;

CREATE TABLE und.partner_good
(
  item_id serial NOT NULL,
  partner_id integer NOT NULL,
  good_id integer NOT NULL,
  partner_code character varying,
  partner_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT partner_good_pkey PRIMARY KEY (item_id),
  CONSTRAINT partner_good_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT partner_good_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.partner_good OWNER TO pavlov;

-- DROP TRIGGER und_partner_good_set_stamp_trg ON und.partner_good;

CREATE TRIGGER und_partner_good_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.partner_good
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.remain;

CREATE TABLE und.remain
(
  remain_id serial NOT NULL,  
  partner_id integer NOT NULL,
  good_id integer NOT NULL,
  wh_id integer,
  quantity numeric,
  price numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT remain_pkey PRIMARY KEY (remain_id),
  CONSTRAINT remain_partner_id_fkey FOREIGN KEY (partner_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT remain_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT remain_wh_id_fkey FOREIGN KEY (wh_id)
      REFERENCES und.partner_wh (wh_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.remain OWNER TO pavlov;

-- DROP TRIGGER und_remain_set_stamp_trg ON und.remain;

CREATE TRIGGER und_remain_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.remain
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.doc_import_config_type;

CREATE TABLE und.doc_import_config_type
(
  config_type_id serial NOT NULL,
  config_type_name character varying,  
  source_partner_id integer NOT NULL,
  list_num integer NOT NULL,
  row_num integer NOT NULL,
  source_name_col integer,
  etalon_name_col integer,
  code_col integer,
  mfr_col integer,    
  mfr_group_col integer,    
  barcode_col integer,    
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT doc_import_config_type_pkey PRIMARY KEY (config_type_id),
  CONSTRAINT doc_import_config_type_source_partner_id_fkey FOREIGN KEY (source_partner_id)
      REFERENCES und.partner (partner_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_config_type OWNER TO pavlov;

-- DROP TRIGGER und_doc_import_config_type_set_stamp_trg ON und.doc_import_config_type;

CREATE TRIGGER und_doc_import_config_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_import_config_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.doc_import;

CREATE TABLE und.doc_import
(
  import_id serial NOT NULL,  
  config_type_id integer,
  file_path character varying,
  file_name character varying,  
  doc_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT doc_import_pkey PRIMARY KEY (import_id),
  CONSTRAINT doc_import_config_type_id_fkey FOREIGN KEY (config_type_id)
      REFERENCES und.doc_import_config_type (config_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import OWNER TO pavlov;

-- DROP TRIGGER und_doc_import_set_stamp_trg ON und.doc_import;

CREATE TRIGGER und_doc_import_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_import
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP TABLE und.doc_import_row;

CREATE TABLE und.doc_import_row
(
  row_id serial NOT NULL,    
  import_id integer NOT NULL,
  row_num integer NOT NULL,
  source_name character varying,
  etalon_name character varying,
  code character varying,
  mfr character varying,
  mfr_group character varying,
  barcode character varying, 
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope4(),
  CONSTRAINT doc_import_row_pkey PRIMARY KEY (row_id),
  CONSTRAINT doc_import_row_import_id_fkey FOREIGN KEY (import_id)
      REFERENCES und.doc_import (import_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.doc_import_row OWNER TO pavlov;

-- DROP TRIGGER und_doc_import_row_set_stamp_trg ON und.doc_import_row;

CREATE TRIGGER und_doc_import_row_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.doc_import_row
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

------------------------------------------------------

-- DROP VIEW und.vw_doc_product_filter;

CREATE OR REPLACE VIEW und.vw_doc_product_filter AS 
 SELECT t2.doc_id, (doc_name || ' (' || count(t1.product_id)::character varying || ')')::character varying as doc_name, count(t1.product_id) as product_cnt
 FROM und.product t1
 INNER JOIN und.doc t2 ON t1.doc_id = t2.doc_id
 GROUP BY t2.doc_id, t2.doc_name
 UNION ALL
 SELECT 0::int as doc_id, ('Ручной ввод (' || count(t1.product_id)::character varying || ')')::character varying as doc_name, count(t1.product_id) as product_cnt
 FROM und.product t1
 WHERE t1.doc_id is null;

ALTER TABLE und.vw_doc_product_filter OWNER TO pavlov;
------------------------------------------------------