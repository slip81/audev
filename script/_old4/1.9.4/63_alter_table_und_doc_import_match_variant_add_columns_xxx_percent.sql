﻿/*
	ALTER TABLE und.doc_import_match_variant ADD COLUMNs xxx_percent
*/

ALTER TABLE und.doc_import_match_variant ADD COLUMN name_percent character varying;
ALTER TABLE und.doc_import_match_variant ADD COLUMN barcode_percent character varying;
ALTER TABLE und.doc_import_match_variant ADD COLUMN mfr_percent character varying;
