﻿/*
	ALTER TABLE und.doc_import_match_variant ADD COLUMNs
*/


ALTER TABLE und.doc_import_match_variant ADD COLUMN is_match_name boolean NOT NULL DEFAULT false;
ALTER TABLE und.doc_import_match_variant ADD COLUMN is_match_barcode boolean NOT NULL DEFAULT false;
ALTER TABLE und.doc_import_match_variant ADD COLUMN is_match_mfr boolean NOT NULL DEFAULT false;

ALTER TABLE und.doc_import_match_variant ADD COLUMN name_similarity numeric;
ALTER TABLE und.doc_import_match_variant ADD COLUMN barcode_similarity numeric;
ALTER TABLE und.doc_import_match_variant ADD COLUMN mfr_similarity numeric;
ALTER TABLE und.doc_import_match_variant ADD COLUMN avg_similarity numeric;
