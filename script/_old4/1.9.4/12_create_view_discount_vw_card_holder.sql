﻿/*
	CREATE OR REPLACE VIEW discount.vw_card_holder
*/

-- DROP VIEW discount.vw_card_holder;

CREATE OR REPLACE VIEW discount.vw_card_holder AS 
 SELECT t1.card_holder_id,
  t1.card_holder_surname,
  t1.card_holder_name,
  t1.card_holder_fname,
  t1.date_birth,
  t1.phone_num,
  t1.address,
  t1.email,
  t1.comment,
  t1.business_id,
  trim((t1.card_holder_surname || ' ' || t1.card_holder_name || ' ' || t1.card_holder_fname))::text as card_holder_full_name
   FROM discount.card_holder t1;

ALTER TABLE discount.vw_card_holder OWNER TO pavlov;
