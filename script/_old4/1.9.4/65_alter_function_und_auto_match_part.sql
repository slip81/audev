﻿/*
	CREATE OR REPLACE FUNCTION und.auto_match_part
*/

-- DROP FUNCTION und.auto_match_part(integer, integer, integer, integer, integer, integer, numeric, numeric, numeric, numeric, character varying);

CREATE OR REPLACE FUNCTION und.auto_match_part(
    IN _doc_id integer,
    IN _is_match_by_barcode integer,
    IN _is_match_by_name integer,
    IN _is_match_by_mfr integer,
    IN _is_match_approved integer,
    IN _is_sum_percent integer,
    IN _min_percent numeric,
    IN _min_percent_barcode numeric,
    IN _min_percent_name numeric,
    IN _min_percent_mfr numeric,
    IN _user_name character varying,
    OUT result integer)
  RETURNS integer AS
$BODY$
DECLARE
    _row_id integer;
    _match_cnt integer;
BEGIN
	result:= 0;

	_match_cnt := (select (case when _is_match_by_barcode = 1 then 1 else 0 end) + (case when _is_match_by_name = 1 then 1 else 0 end) + (case when _is_match_by_mfr = 1 then 1 else 0 end));

	perform und.set_limit(0.3);

	delete from und.doc_import_match_variant t1 using und.doc_import_match t2, und.doc_import t3
	where t1.row_id = t2.row_id and t2.import_id = t3.import_id and t3.doc_id = _doc_id;

	update und.doc_import_match t1
	set state = 0, match_product_id = null, match_good_id = null, is_approved = false, percent = null, is_auto_match = false,
	upd_date = clock_timestamp(), upd_user = _user_name
	from und.doc_import t2
	where t1.import_id = t2.import_id
	and t2.doc_id = _doc_id
	and (((_is_match_approved != 1) and (t1.is_approved = false)) or (_is_match_approved = 1));

	insert into und.doc_import_match_variant (
	row_id,
	match_product_id,
	crt_date,
	crt_user,
	upd_date,
	upd_user,
	is_deleted,
	match_good_id,	
	is_match_name,
	is_match_barcode,
	is_match_mfr,
	name_similarity,
	barcode_similarity,
	mfr_similarity,
	avg_similarity
	)
	select t1.row_id, t3.product_id, clock_timestamp(), _user_name, clock_timestamp(), _user_name, false, t3.good_id,
	_is_match_by_name = 1, _is_match_by_barcode = 1, _is_match_by_mfr = 1,
	case when _is_match_by_name = 1 then und.similarity(t1.source_name, t3.partner_name) * 100 else null end,
	case when _is_match_by_barcode = 1 then und.similarity(t1.barcode, t3.barcode) * 100 else null end,
	case when _is_match_by_mfr = 1 then und.similarity(t1.mfr, t3.mfr_name) * 100 else null end,
	null
	from und.doc_import_match t1
	inner join und.doc_import t2 on t1.import_id = t2.import_id
	inner join und.vw_product_and_good t3 on 1=1
	and (((t1.source_name OPERATOR(und.%) t3.partner_name) and (_is_match_by_name = 1)) or (_is_match_by_name != 1))
	and (((t1.mfr OPERATOR(und.%) t3.mfr_name) and (_is_match_by_mfr = 1)) or (_is_match_by_mfr != 1))
	and (((t1.barcode OPERATOR(und.%) t3.barcode) and (_is_match_by_barcode = 1)) or (_is_match_by_barcode != 1))	
	where t2.doc_id = _doc_id
	and (((_is_match_approved != 1) and (t1.is_approved = false)) or (_is_match_approved = 1))
	;

	with cte1 as (
	select t1.variant_id, 
	coalesce(t1.name_similarity, 0) as name_similarity,
	coalesce(t1.barcode_similarity, 0) as barcode_similarity,
	coalesce(t1.mfr_similarity, 0) as mfr_similarity,
	((coalesce(t1.name_similarity, 0) + coalesce(t1.barcode_similarity, 0) + coalesce(t1.mfr_similarity, 0)) / _match_cnt) as avg_similarity
	from und.doc_import_match_variant t1
	inner join und.doc_import_match t2 on t1.row_id = t2.row_id
	inner join und.doc_import t3 on t2.import_id = t3.import_id
	where t3.doc_id = _doc_id
	)	
	-- подсчет среднего взависимости от _is_match_by_barcode integer, _is_match_by_name, _is_match_by_mfr integer
	update und.doc_import_match_variant t1
	set avg_similarity = t2.avg_similarity
	, percent = to_char(t2.avg_similarity, 'FM999999999.00')
	, barcode_percent = to_char(t2.barcode_similarity, 'FM999999999.00')
	, name_percent = to_char(t2.name_similarity, 'FM999999999.00')
	, mfr_percent = to_char(t2.mfr_similarity, 'FM999999999.00')
	from cte1 t2
	where t1.variant_id = t2.variant_id;

	with cte2 as (
	select t1.row_id, t1.match_product_id, t1.match_good_id, max(t1.avg_similarity) as avg_similarity
	, max(t1.name_similarity) as name_similarity, max(t1.barcode_similarity) as barcode_similarity, max(t1.mfr_similarity) as mfr_similarity
	from und.doc_import_match_variant t1
	inner join und.doc_import_match t2 on t1.row_id = t2.row_id
	inner join und.doc_import t3 on t2.import_id = t3.import_id
	where t3.doc_id = _doc_id
	-- !!!
	and (
	((coalesce(t1.avg_similarity, 0) >= _min_percent) and (_is_sum_percent = 1)) 
	or
	(
	(((coalesce(t1.name_similarity, 0) >= _min_percent_name) and (_is_match_by_name = 1)) or (_is_match_by_name != 1)) 
	and (((coalesce(t1.barcode_similarity, 0) >= _min_percent_barcode) and (_is_match_by_barcode = 1)) or (_is_match_by_barcode != 1)) 
	and (((coalesce(t1.mfr_similarity, 0) >= _min_percent_mfr) and (_is_match_by_mfr = 1)) or (_is_match_by_mfr != 1)) 
	and (_is_sum_percent != 1)
	)
	)
	group by t1.row_id, t1.match_product_id, t1.match_good_id
	)
	update und.doc_import_match t1
	set state = 2, match_product_id = t2.match_product_id, match_good_id = t2.match_good_id, is_approved = false, 
	percent = to_char(t2.avg_similarity, 'FM999999999.00'), 
	barcode_percent = to_char(t2.barcode_similarity, 'FM999999999.00'), 
	name_percent = to_char(t2.name_similarity, 'FM999999999.00'), 
	mfr_percent = to_char(t2.mfr_similarity, 'FM999999999.00'), 
	is_auto_match = true,
	upd_date = clock_timestamp(), upd_user = _user_name
	from cte2 t2
	where t1.row_id = t2.row_id;


	delete from und.doc_import_match_variant t1 using und.doc_import_match t2, und.doc_import t3
	where t1.row_id = t2.row_id and t2.import_id = t3.import_id and t3.doc_id = _doc_id
	and t1.match_product_id = t2.match_product_id and t1.match_good_id = t2.match_good_id
	;

	
	--result := (select count(row_id) from und.doc_import_match where import_id = _import_id);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION und.auto_match_part(integer, integer, integer, integer, integer, integer, numeric, numeric, numeric, numeric, character varying) OWNER TO pavlov;