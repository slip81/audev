﻿/*
	INSERT INTO und.xxx
*/

INSERT INTO und.partner (partner_name, parent_id, is_source, crt_date, crt_user, upd_date, upd_user)
VALUES ('Аурит', null, true, current_timestamp, 'ПМС', current_timestamp, 'ПМС');

INSERT INTO und.partner (partner_name, parent_id, is_source, crt_date, crt_user, upd_date, upd_user)
VALUES ('Катрэн', null, true, current_timestamp, 'ПМС', current_timestamp, 'ПМС');

INSERT INTO und.partner (partner_name, parent_id, is_source, crt_date, crt_user, upd_date, upd_user)
VALUES ('Сводный Заказ', null, true, current_timestamp, 'ПМС', current_timestamp, 'ПМС');

-- select * from und.partner order by partner_id


INSERT INTO und.doc_type (doc_type_name, crt_date, crt_user, upd_date, upd_user)
VALUES ('Справочник', current_timestamp, 'ПМС', current_timestamp, 'ПМС');

INSERT INTO und.doc_type (doc_type_name, crt_date, crt_user, upd_date, upd_user)
VALUES ('Накладная', current_timestamp, 'ПМС', current_timestamp, 'ПМС');


-- select * from und.doc_type order by doc_type_id

INSERT INTO und.doc_state (doc_state_name, crt_date, crt_user, upd_date, upd_user)
VALUES ('Загружен', current_timestamp, 'ПМС', current_timestamp, 'ПМС');

INSERT INTO und.doc_state (doc_state_name, crt_date, crt_user, upd_date, upd_user)
VALUES ('В очереди', current_timestamp, 'ПМС', current_timestamp, 'ПМС');

INSERT INTO und.doc_state (doc_state_name, crt_date, crt_user, upd_date, upd_user)
VALUES ('Обрабатывается', current_timestamp, 'ПМС', current_timestamp, 'ПМС');

INSERT INTO und.doc_state (doc_state_name, crt_date, crt_user, upd_date, upd_user)
VALUES ('Обработан', current_timestamp, 'ПМС', current_timestamp, 'ПМС');

-- select * from und.doc_state order by doc_state_id