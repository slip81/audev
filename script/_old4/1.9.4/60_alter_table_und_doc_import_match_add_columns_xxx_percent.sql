﻿/*
	ALTER TABLE und.doc_import_match ADD COLUMNs xxx_percent
*/

ALTER TABLE und.doc_import_match ADD COLUMN name_percent character varying;
ALTER TABLE und.doc_import_match ADD COLUMN barcode_percent character varying;
ALTER TABLE und.doc_import_match ADD COLUMN mfr_percent character varying;
