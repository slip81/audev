﻿/*
	insert into und.partner 3, 4, 5
*/

delete from und.partner where partner_id = 3;

insert into und.partner (partner_id, partner_name, crt_date, crt_user, upd_date, upd_user, is_supplier)
values (3, 'Агроресурсы Тюмень', current_timestamp, 'ПМС', current_timestamp, 'ПМС', true);

insert into und.partner (partner_id, partner_name, crt_date, crt_user, upd_date, upd_user, is_supplier)
values (4, 'Градиент Косметика', current_timestamp, 'ПМС', current_timestamp, 'ПМС', true);

insert into und.partner (partner_id, partner_name, crt_date, crt_user, upd_date, upd_user, is_supplier)
values (5, 'Пульс', current_timestamp, 'ПМС', current_timestamp, 'ПМС', true);

-- select * from und.partner