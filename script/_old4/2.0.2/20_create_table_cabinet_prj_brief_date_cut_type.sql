﻿/*
	CREATE TABLE cabinet.prj_brief_date_cut_type
*/

-- DROP TABLE cabinet.prj_brief_date_cut_type;

CREATE TABLE cabinet.prj_brief_date_cut_type
(
  date_cut_type_id integer NOT NULL,
  date_cut_type_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_brief_date_cut_type_pkey PRIMARY KEY (date_cut_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_brief_date_cut_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_brief_date_cut_type_set_stamp_trg ON cabinet.prj_brief_date_cut_type;

CREATE TRIGGER cabinet_prj_brief_date_cut_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_brief_date_cut_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_brief_date_cut_type (date_cut_type_id, date_cut_type_name)
VALUES (1, 'по дате изменения');

INSERT INTO cabinet.prj_brief_date_cut_type (date_cut_type_id, date_cut_type_name)
VALUES (2, 'по дате создания');

-- select * from cabinet.prj_brief_date_cut_type order by date_cut_type_id;