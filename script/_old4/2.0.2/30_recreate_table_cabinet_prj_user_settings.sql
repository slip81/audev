﻿/*
	RECREATE TABLE cabinet.prj_user_settings
*/

CREATE TABLE cabinet.prj_user_settings_tmp
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  page_type_id integer NOT NULL,
  settings character varying,
  setting_num integer NOT NULL DEFAULT 1,
  setting_name character varying,
  is_selected boolean NOT NULL DEFAULT false, 
  CONSTRAINT prj_user_settings_tmp_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_user_settings_tmp OWNER TO pavlov;

INSERT INTO cabinet.prj_user_settings_tmp (  
  user_id,
  page_type_id,
  settings,
  setting_num,
  setting_name,
  is_selected
)
SELECT
  user_id,
  1,
  settings,
  setting_num,
  setting_name,
  true
FROM cabinet.prj_user_settings;

DROP TABLE cabinet.prj_user_settings;

CREATE TABLE cabinet.prj_user_settings
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  page_type_id integer NOT NULL,
  settings character varying,
  setting_num integer NOT NULL DEFAULT 1,
  setting_name character varying,
  is_selected boolean NOT NULL DEFAULT false,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),  
  CONSTRAINT prj_user_settings_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_user_settings_page_type_id_fkey FOREIGN KEY (page_type_id)
      REFERENCES cabinet.prj_page_type (page_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_user_settings_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_user_settings OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_user_settings_set_stamp_trg ON cabinet.prj_user_settings;

CREATE TRIGGER cabinet_prj_user_settings_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_user_settings
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_user_settings (  
  user_id,
  page_type_id,
  settings,
  setting_num,
  setting_name,
  is_selected
)
SELECT
  user_id,
  page_type_id,
  settings,
  setting_num,
  setting_name,
  is_selected
FROM cabinet.prj_user_settings_tmp;

INSERT INTO cabinet.prj_user_settings (
  user_id,
  settings,  
  setting_num,
  setting_name,
  is_selected,
  page_type_id
)
SELECT
  t1.user_id,
  t1.settings,  
  t1.setting_num,
  t1.setting_name,
  t1.is_selected,
  t2.page_type_id
FROM cabinet.prj_user_settings t1
INNER JOIN cabinet.prj_page_type t2 ON t1.page_type_id != t2.page_type_id;

DROP TABLE cabinet.prj_user_settings_tmp;

-- select * FROM cabinet.prj_user_settings order by user_id, page_type_id;