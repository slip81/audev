﻿/*
	CREATE TABLE cabinet.prj_brief_interval_type
*/

-- DROP TABLE cabinet.prj_brief_interval_type;

CREATE TABLE cabinet.prj_brief_interval_type
(
  interval_type_id integer NOT NULL,
  interval_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_brief_interval_type_pkey PRIMARY KEY (interval_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_brief_interval_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_brief_interval_type_set_stamp_trg ON cabinet.prj_brief_interval_type;

CREATE TRIGGER cabinet_prj_brief_interval_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_brief_interval_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_brief_interval_type (interval_type_id, interval_type_name)
VALUES (1, 'за 2 месяца');

INSERT INTO cabinet.prj_brief_interval_type (interval_type_id, interval_type_name)
VALUES (2, 'за 1 месяц');

INSERT INTO cabinet.prj_brief_interval_type (interval_type_id, interval_type_name)
VALUES (3, 'за 1 неделю');

INSERT INTO cabinet.prj_brief_interval_type (interval_type_id, interval_type_name)
VALUES (4, 'за 2 дня');

-- select * from cabinet.prj_brief_interval_type order by interval_type_id;