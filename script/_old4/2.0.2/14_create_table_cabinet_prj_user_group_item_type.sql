﻿/*
	CREATE TABLE cabinet.prj_user_group_item_type
*/

-- DROP TABLE cabinet.prj_user_group_item_type;

CREATE TABLE cabinet.prj_user_group_item_type
(
  item_type_id integer NOT NULL,  
  item_type_name character varying,    
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_user_group_item_type_pkey PRIMARY KEY (item_type_id)
)
WITH (
  OIDS=FALSE
);


ALTER TABLE cabinet.prj_user_group_item_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_user_group_item_type_set_stamp_trg ON cabinet.prj_user_group_item_type;

CREATE TRIGGER cabinet_prj_user_group_item_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_user_group_item_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_user_group_item_type (item_type_id, item_type_name)
VALUES (1, 'Задача');

INSERT INTO cabinet.prj_user_group_item_type (item_type_id, item_type_name)
VALUES (2, 'Событие');

INSERT INTO cabinet.prj_user_group_item_type (item_type_id, item_type_name)
VALUES (3, 'Запись');

-- select * from cabinet.prj_user_group_item_type order by item_type_id;