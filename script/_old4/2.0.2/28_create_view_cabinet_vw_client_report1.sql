﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_client_report1
*/

-- DROP VIEW cabinet.vw_client_report1;

CREATE OR REPLACE VIEW cabinet.vw_client_report1 AS 
 SELECT DISTINCT t1.id,
    t1.client_name,
    t1.sales_count,
    t1.region_name,
    t1.city_name,
    t2.version_name::character varying AS version_min,
    t3.version_name::character varying AS version_max,
    t4.date_reg_min::date AS date_reg_min,
    t4.date_reg_max::date AS date_reg_max,
    ''::character varying AS sales_name,
    1::integer as kind
   FROM cabinet.allclients t1
   LEFT JOIN (
     select id, min(version_name) as version_name from cabinet.vw_client_min_version 
     /*where service_name = 'Склад'*/
     group by id
   ) t2 ON t1.id = t2.id
   LEFT JOIN (
     select id, max(version_name) as version_name from cabinet.vw_client_max_version 
     /*where service_name = 'Склад'*/
     group by id
   ) t3 ON t1.id = t3.id
   LEFT JOIN (
     select t2.client_id, min(t2.updated_on) as date_reg_min, max(t2.updated_on) as date_reg_max
     from cabinet.order_item t1
     inner join cabinet.order t2 on t1.order_id = t2.id and t2.active_status != 1 and t2.is_deleted != 1
     where t1.is_deleted != 1
     /*and t1.service_id = 11*/ -- склад     
     group by t2.client_id
   ) t4 ON t1.id = t4.client_id
   WHERE ((t2.version_name is not null) OR (t3.version_name is not null) OR (t4.date_reg_min is not null) OR (t4.date_reg_max is not null)) 
   UNION ALL
 SELECT DISTINCT t1.sales_id as id,
    t1.client_name,
    1 as sales_count,
    t1.region_name,
    t1.city_name,
    t2.version_name::character varying AS version_min,
    t3.version_name::character varying AS version_max,
    t4.date_reg_min::date AS date_reg_min,
    t4.date_reg_max::date AS date_reg_max,
    t1.adress::character varying AS sales_name,
    2::integer as kind
   FROM cabinet.vw_sales t1
   LEFT JOIN (
     select sales_id, min(version_name) as version_name from cabinet.vw_client_min_version 
     /*where service_name = 'Склад'*/
     group by sales_id
   ) t2 ON t1.sales_id = t2.sales_id
   LEFT JOIN (
     select sales_id, max(version_name) as version_name from cabinet.vw_client_max_version 
     /*where service_name = 'Склад'*/
     group by sales_id
   ) t3 ON t1.sales_id= t3.sales_id
   LEFT JOIN (
     select t2.client_id, min(t2.updated_on) as date_reg_min, max(t2.updated_on) as date_reg_max
     from cabinet.order_item t1
     inner join cabinet.order t2 on t1.order_id = t2.id and t2.active_status != 1 and t2.is_deleted != 1
     where t1.is_deleted != 1
     /*and t1.service_id = 11*/ -- склад     
     group by t2.client_id
   ) t4 ON t1.client_id = t4.client_id
   WHERE ((t2.version_name is not null) OR (t3.version_name is not null) OR (t4.date_reg_min is not null) OR (t4.date_reg_max is not null))    
;

ALTER TABLE cabinet.vw_client_report1 OWNER TO pavlov;