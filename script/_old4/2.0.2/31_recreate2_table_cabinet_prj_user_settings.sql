﻿/*
	RECREATE TABLE cabinet.prj_user_settings 2
*/

CREATE TABLE cabinet.prj_user_settings_tmp
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  page_type_id integer NOT NULL,
  settings character varying,
  setting_num integer NOT NULL DEFAULT 1,
  setting_name character varying,
  is_selected boolean NOT NULL DEFAULT false, 
  CONSTRAINT prj_user_settings_tmp_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_user_settings_tmp OWNER TO pavlov;

INSERT INTO cabinet.prj_user_settings_tmp (  
  user_id,
  page_type_id,
  settings,
  setting_num,
  setting_name,
  is_selected
)
SELECT
  user_id,
  1,
  settings,
  setting_num,
  setting_name,
  true
FROM cabinet.prj_user_settings
WHERE page_type_id = 1;

DROP TABLE cabinet.prj_user_settings;

delete from cabinet.prj_page_type;

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (1, 'prj');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (2, 'claim');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (3, 'task');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (4, 'work');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (5, 'plan');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (6, 'news');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (7, 'log');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (8, 'brief');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (9, 'notebook');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (10, 'sprav');

insert into cabinet.prj_page_type (page_type_id, page_type_name)
values (11, 'settings');


CREATE TABLE cabinet.prj_user_settings
(
  setting_id serial NOT NULL,
  setting_name character varying,  
  user_id integer NOT NULL,
  page_type_id integer NOT NULL,
  settings character varying,
  settings_grid character varying,
  is_selected boolean NOT NULL DEFAULT false,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),  
  CONSTRAINT prj_user_settings_pkey PRIMARY KEY (setting_id),
  CONSTRAINT prj_user_settings_page_type_id_fkey FOREIGN KEY (page_type_id)
      REFERENCES cabinet.prj_page_type (page_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_user_settings_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_user_settings OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_user_settings_set_stamp_trg ON cabinet.prj_user_settings;

CREATE TRIGGER cabinet_prj_user_settings_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_user_settings
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_user_settings (  
  setting_name,
  user_id,
  page_type_id,
  settings,   
  settings_grid,
  is_selected,
  crt_date,
  crt_user,
  upd_date,
  upd_user
)
SELECT
  setting_name,
  user_id,
  page_type_id,
  settings,
  null,
  is_selected,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov'  
FROM cabinet.prj_user_settings_tmp;

INSERT INTO cabinet.prj_user_settings (
  setting_name,
  user_id,
  page_type_id,
  settings,   
  settings_grid,
  is_selected,
  crt_date,
  crt_user,
  upd_date,
  upd_user
)
SELECT
  t1.setting_name,
  t1.user_id,
  t2.page_type_id,
  t1.settings,  
  t1.settings_grid,
  t1.is_selected,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov'  
FROM cabinet.prj_user_settings t1
INNER JOIN cabinet.prj_page_type t2 ON t1.page_type_id != t2.page_type_id;

DROP TABLE cabinet.prj_user_settings_tmp;

UPDATE cabinet.prj_user_settings SET setting_name = 'Стандарт';

UPDATE cabinet.prj_user_settings
SET settings_grid = (
SELECT x1.grid_options FROM cabinet.cab_grid_user_settings x1 
WHERE cabinet.prj_user_settings.user_id = x1.user_id
AND x1.grid_id = 3
)
WHERE page_type_id = 1;

UPDATE cabinet.prj_user_settings
SET settings_grid = (
SELECT x1.grid_options FROM cabinet.cab_grid_user_settings x1 
WHERE cabinet.prj_user_settings.user_id = x1.user_id
AND x1.grid_id = 4
)
WHERE page_type_id = 2;

UPDATE cabinet.prj_user_settings
SET settings_grid = (
SELECT x1.grid_options FROM cabinet.cab_grid_user_settings x1 
WHERE cabinet.prj_user_settings.user_id = x1.user_id
AND x1.grid_id = 6
)
WHERE page_type_id = 4;

UPDATE cabinet.prj_user_settings
SET settings_grid = (
SELECT x1.grid_options FROM cabinet.cab_grid_user_settings x1 
WHERE cabinet.prj_user_settings.user_id = x1.user_id
AND x1.grid_id = 7
)
WHERE page_type_id = 8;

-- select * FROM cabinet.prj_user_settings order by user_id, page_type_id;

-- select * from cabinet.prj_page_type order by page_type_id;

-- select * from cabinet.cab_grid_user_settings order by user_id, grid_id;
