﻿/*
	CREATE TABLE cabinet.prj_brief_type
*/

-- DROP TABLE cabinet.prj_brief_type;

CREATE TABLE cabinet.prj_brief_type
(
  brief_type_id integer NOT NULL,
  brief_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_brief_type_pkey PRIMARY KEY (brief_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_brief_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_brief_type_set_stamp_trg ON cabinet.prj_brief_type;

CREATE TRIGGER cabinet_prj_brief_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_brief_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_brief_type (brief_type_id, brief_type_name)
VALUES (1, 'Мониторинг задач по разработке');

-- select * from cabinet.prj_brief_type order by brief_type_id;