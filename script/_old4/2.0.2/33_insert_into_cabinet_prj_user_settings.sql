﻿/*
	insert into cabinet.prj_user_settings
*/

UPDATE cabinet.prj_user_settings
SET settings_grid = null
WHERE page_type_id = 1;


insert into cabinet.prj_user_settings (
  setting_name,
  user_id,
  page_type_id,
  settings,
  settings_grid,
  is_selected,
  crt_date,
  crt_user,
  upd_date,
  upd_user
)
select
 'Стандарт',
 t1.user_id,
 t2.page_type_id,
 t2.settings,
 t2.settings_grid,
 t2.is_selected, 
 current_timestamp,
 'pavlov',
  current_timestamp,
 'pavlov'
from cabinet.cab_user t1
inner join cabinet.prj_user_settings t2 on t2.user_id = 37
where t1.is_active and t1.is_crm_user and t1.user_id > 0
and not exists (select x1.user_id from cabinet.prj_user_settings x1 where t1.user_id = x1.user_id and x1.page_type_id = t2.page_type_id);

-- select * from cabinet.prj_page_type
-- select * from cabinet.prj_user_settings order by user_id, page_type_id;