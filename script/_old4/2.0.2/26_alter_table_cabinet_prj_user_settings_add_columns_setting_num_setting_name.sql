﻿/*
	ALTER TABLE cabinet.prj_user_settings ADD COLUMNs setting_num, setting_name
*/

ALTER TABLE cabinet.prj_user_settings ADD COLUMN setting_num integer NOT NULL DEFAULT 1;
ALTER TABLE cabinet.prj_user_settings ADD COLUMN setting_name character varying;


UPDATE cabinet.prj_user_settings SET setting_name = '[по умолчанию]';