﻿/*
	delete from cabinet.crm_priority
*/


update cabinet.prj_claim
set priority_id = 0 where priority_id not in (0, 1, 3, 4);

update cabinet.prj_claim
set priority_id = 4 where priority_id = 3;

update cabinet.crm_task
set priority_id = 0;

delete from cabinet.crm_priority_user_settings;
delete from cabinet.crm_priority where priority_id not in (0, 1, 4);

insert into cabinet.crm_priority (priority_id, priority_name, ord)
values (2, 'Критическая ошибка', 2);


-- select * from cabinet.crm_priority order by priority_id;