﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_user_group_item
*/

-- DROP VIEW cabinet.vw_prj_user_group_item;

CREATE OR REPLACE VIEW cabinet.vw_prj_user_group_item AS 
 SELECT t1.item_id,
  t1.group_id,
  t1.item_type_id,
  t1.claim_id,
  t1.event_id,
  t1.note_id,
  t1.ord,
  t2.user_id,
  t2.group_name,
  t2.sys_group_type_id, 
  t3.item_type_name,
  case t1.item_type_id when 1 then t11.claim_num when 2 then t12.claim_num when 3 then t13.note_num else null end as item_num,
  case t1.item_type_id when 1 then t11.claim_name when 2 then t12.claim_name when 3 then t13.note_name else null end as item_name,
  case t1.item_type_id when 1 then t11.claim_text when 2 then t12.task_text when 3 then t13.note_text else null end as item_text,
  case t1.item_type_id when 1 then t11.claim_state_type_id when 2 then t12.state_type_id else null end as item_state_type_id,
  case t1.item_type_id when 1 then t11.claim_state_type_name when 2 then t12.state_type_name else null end as item_state_type_name,
  case t1.item_type_id when 2 then t12.date_beg else null end as item_date_beg,
  case t1.item_type_id when 2 then t12.date_end else null end as item_date_end  
   FROM cabinet.prj_user_group_item t1
   INNER JOIN cabinet.prj_user_group t2 ON t1.group_id = t2.group_id
   INNER JOIN cabinet.prj_user_group_item_type t3 ON t1.item_type_id = t3.item_type_id
   LEFT JOIN cabinet.vw_prj_claim t11 ON t1.claim_id = t11.claim_id
   LEFT JOIN cabinet.vw_prj_plan t12 ON t1.event_id = t12.plan_id AND t12.work_id <= 0
   LEFT JOIN cabinet.vw_prj_note t13 ON t1.note_id = t13.note_id
   ;

ALTER TABLE cabinet.vw_prj_user_group_item OWNER TO pavlov;
