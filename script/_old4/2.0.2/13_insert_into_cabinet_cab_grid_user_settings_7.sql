﻿/*
	insert into cabinet.cab_grid_user_settings 7
*/

insert into cabinet.cab_grid_user_settings (grid_id, user_id, grid_options)
select 7, t1.user_id, (select x2.grid_options from cabinet.cab_grid_user_settings x2 where x2.user_id = 1 and x2.grid_id = 7)
from cabinet.cab_user t1
where t1.is_active and t1.is_crm_user and t1.user_id > 0
and not exists (select x1.item_id from cabinet.cab_grid_user_settings x1 where t1.user_id = x1.user_id and x1.grid_id = 7)
--order by t1.user_id
;

-- select * from cabinet.cab_grid_user_settings order by grid_id, user_id;