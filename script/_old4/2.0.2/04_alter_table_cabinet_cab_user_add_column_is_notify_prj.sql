﻿/*
	ALTER TABLE cabinet.cab_user ADD COLUMN is_notify_prj
*/

-- ALTER TABLE cabinet.cab_user DROP COLUMN is_notify_prj;

ALTER TABLE cabinet.cab_user ADD COLUMN is_notify_prj boolean NOT NULL DEFAULT false;
