﻿/*
	CREATE TABLE cabinet.prj_note
*/

-- DROP TABLE cabinet.prj_note;

CREATE TABLE cabinet.prj_note
(
  note_id serial NOT NULL,
  note_name character varying,
  note_text character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_note_pkey PRIMARY KEY (note_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_note OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_note_set_stamp_trg ON cabinet.prj_note;

CREATE TRIGGER cabinet_prj_note_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_note
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();
