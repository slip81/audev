﻿/*
	CREATE TABLE cabinet.prj_page_type
*/

-- DROP TABLE cabinet.prj_page_type;

CREATE TABLE cabinet.prj_page_type
(
  page_type_id integer NOT NULL,
  page_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_page_type_pkey PRIMARY KEY (page_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_page_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_page_type_set_stamp_trg ON cabinet.prj_page_type;

CREATE TRIGGER cabinet_prj_page_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_page_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (1, 'prj');

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (2, 'claim');

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (3, 'work');

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (4, 'plan');

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (5, 'notebook');

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (6, 'brief');

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (7, 'sprav');

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (8, 'log');

INSERT INTO cabinet.prj_page_type (page_type_id, page_type_name)
VALUES (9, 'settings');

-- select * from cabinet.prj_page_type order by page_type_id;