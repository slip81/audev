﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_note
*/

-- DROP VIEW cabinet.vw_prj_note;

CREATE OR REPLACE VIEW cabinet.vw_prj_note AS 
 SELECT t1.note_id,
  t1.note_name,
  t1.note_text,
  'N-'::text || t1.note_id::text AS note_num
   FROM cabinet.prj_note t1;

ALTER TABLE cabinet.vw_prj_note OWNER TO pavlov;
