﻿/*
	CREATE TABLE cabinet.prj_user_group_item
*/

-- DROP TABLE cabinet.prj_user_group_item;

CREATE TABLE cabinet.prj_user_group_item
(
  item_id serial NOT NULL,
  group_id integer NOT NULL,
  item_type_id integer NOT NULL,
  claim_id integer,
  event_id integer,
  note_id integer,
  ord integer,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_user_group_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_user_group_item_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.prj_user_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_user_group_item_item_type_id_fkey FOREIGN KEY (item_type_id)
      REFERENCES cabinet.prj_user_group_item_type (item_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_user_group_item_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_user_group_item_event_id_fkey FOREIGN KEY (event_id)
      REFERENCES cabinet.prj_event (event_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,    
  CONSTRAINT prj_user_group_item_note_id_fkey FOREIGN KEY (note_id)
      REFERENCES cabinet.prj_note (note_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_user_group_item OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_user_group_item_set_stamp_trg ON cabinet.prj_user_group_item;

CREATE TRIGGER cabinet_prj_user_group_item_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_user_group_item
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

