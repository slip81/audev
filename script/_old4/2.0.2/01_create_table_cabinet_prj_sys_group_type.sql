﻿/*
	CREATE TABLE cabinet.prj_sys_group_type
*/

-- DROP TABLE cabinet.prj_sys_group_type;

CREATE TABLE cabinet.prj_sys_group_type
(
  group_type_id integer NOT NULL,
  group_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_sys_group_type_pkey PRIMARY KEY (group_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_sys_group_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_sys_group_type_set_stamp_trg ON cabinet.prj_sys_group_type;

CREATE TRIGGER cabinet_prj_sys_group_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_sys_group_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.prj_sys_group_type (group_type_id, group_type_name)
VALUES (1, 'Задачи на сегодня');  

INSERT INTO cabinet.prj_sys_group_type (group_type_id, group_type_name)
VALUES (2, 'Избранные задачи');

-- select * from cabinet.prj_sys_group_type order by group_type_id;