﻿/*
	ALTER TABLE cabinet.prj_user_settings ALTER COLUMNs settings, settings_grid
*/

ALTER TABLE cabinet.prj_user_settings ALTER COLUMN settings TYPE text;
ALTER TABLE cabinet.prj_user_settings ALTER COLUMN settings_grid TYPE text;