﻿/*
	CREATE TABLE cabinet.prj_user_group
*/

-- DROP TABLE cabinet.prj_user_group;

CREATE TABLE cabinet.prj_user_group
(
  group_id serial NOT NULL,
  user_id integer NOT NULL,
  group_name character varying,
  sys_group_type_id integer,
  ord integer,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_user_group_pkey PRIMARY KEY (group_id),
  CONSTRAINT prj_user_group_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_user_group_sys_group_type_id_fkey FOREIGN KEY (sys_group_type_id)
      REFERENCES cabinet.prj_sys_group_type (group_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_user_group OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_user_group_set_stamp_trg ON cabinet.prj_user_group;

CREATE TRIGGER cabinet_prj_user_group_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_user_group
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

