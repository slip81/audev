﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_cab_user2
*/

-- DROP VIEW cabinet.vw_cab_user2;

CREATE OR REPLACE VIEW cabinet.vw_cab_user2 AS 
 SELECT t1.user_id,
    t1.user_name,
    t1.user_login,
    t1.is_superadmin,
    t1.full_name,
    t1.email,
    t1.is_crm_user,
    t1.is_executer,
    t1.crm_user_role_id,
    t1.icq,
    t1.is_active,
    t11.role_name AS crm_user_role_name,
    t13.role_id AS auth_role_id,
    t13.role_name AS auth_role_name,
    t1.is_male,
    t1.is_notify_prj
   FROM cabinet.cab_user t1
     LEFT JOIN cabinet.crm_user_role t11 ON t1.crm_user_role_id = t11.role_id
     LEFT JOIN cabinet.auth_user_role t12 ON t1.user_id = t12.user_id
     LEFT JOIN cabinet.auth_role t13 ON t12.role_id = t13.role_id;

ALTER TABLE cabinet.vw_cab_user2 OWNER TO pavlov;
