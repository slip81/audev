﻿/*
	ALTER TABLE und.wa_request_type ADD COLUMNs is_inner, is_outer
*/

ALTER TABLE und.wa_request_type ADD COLUMN is_inner boolean NOT NULL DEFAULT false;
ALTER TABLE und.wa_request_type ADD COLUMN is_outer boolean NOT NULL DEFAULT false;

UPDATE und.wa_request_type 
SET is_inner = true
WHERE request_type_id IN (2, 3, 4, 7);

UPDATE und.wa_request_type 
SET is_outer = true
WHERE request_type_id IN (1, 5, 6);


-- select * from und.wa_request_type order by request_type_id