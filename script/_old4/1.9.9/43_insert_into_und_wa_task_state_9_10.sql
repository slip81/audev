﻿/*
	INSERT INTO und.wa_task_state 9, 10
*/


INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  9,
  'Отменено клиентом',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  10,
  'Отменено сервером',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);


-- select * from und.wa_task_state order by task_state_id;