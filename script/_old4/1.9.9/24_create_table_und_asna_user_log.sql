﻿/*
	CREATE TABLE und.asna_user_log
*/

DROP TABLE und.asna_request_log;

-- DROP TABLE und.asna_user_log;

CREATE TABLE und.asna_user_log
(
  log_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  mess character varying,
  request_id bigint,
  chain_id integer,
  in_batch_id integer,
  out_batch_id integer,
  info_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT asna_user_log_pkey PRIMARY KEY (log_id),
  CONSTRAINT asna_user_log_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_user_log OWNER TO pavlov;

