﻿/*
	insert into cabinet.auth_section 16
*/

insert into cabinet.auth_section (section_id, section_name, group_id)
values (16, 'АСНА', null);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (40, 'Пользователи', 16, null);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (41, 'Справочник связок', 16, null);

-- select * from cabinet.auth_section order by section_id
-- select * from cabinet.auth_section_part order by section_id, part_id