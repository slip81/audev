﻿/*
	CREATE TABLE und.wa_worker
*/

-- DROP TABLE und.wa_worker;

CREATE TABLE und.wa_worker
(
  worker_id serial NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  inactive_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT wa_worker_pkey PRIMARY KEY (worker_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_worker OWNER TO pavlov;

-- DROP TRIGGER und_wa_worker_set_stamp_trg ON und.wa_worker;

CREATE TRIGGER und_wa_worker_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_worker
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

