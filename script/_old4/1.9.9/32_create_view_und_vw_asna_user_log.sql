﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_user_log
*/

-- DROP VIEW und.vw_asna_user_log;

CREATE OR REPLACE VIEW und.vw_asna_user_log AS 
 SELECT t1.log_id,
    t1.asna_user_id,
    t1.mess,
    t1.request_id,
    t1.chain_id,
    t1.in_batch_id,
    t1.out_batch_id,
    t1.info_id,
    t1.crt_date,
    t1.crt_user, 
    t2.workplace_id,
    t2.is_active,
    t3.registration_key AS workplace_name,
    t3.sales_id,
    t4.adress AS sales_name,
    t4.client_id,
    t5.name AS client_name,
        CASE t2.is_active
            WHEN true THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_active_str
    FROM und.asna_user_log t1
     INNER JOIN und.asna_user t2 ON t1.asna_user_id = t2.asna_user_id
     JOIN cabinet.workplace t3 ON t2.workplace_id = t3.id
     JOIN cabinet.sales t4 ON t3.sales_id = t4.id
     JOIN cabinet.client t5 ON t4.client_id = t5.id;

ALTER TABLE und.vw_asna_user_log OWNER TO postgres;
