﻿/*
	CREATE TABLE und.wa_task_lc
*/

-- DROP TABLE und.wa_task_lc;

CREATE TABLE und.wa_task_lc
(
  task_lc_id serial NOT NULL,
  task_id int NOT NULL,
  task_state_id integer NOT NULL DEFAULT 1,  
  state_date timestamp without time zone,
  state_user character varying,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope8(),
  CONSTRAINT wa_task_lc_pkey PRIMARY KEY (task_lc_id),
  CONSTRAINT wa_task_lc_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES und.wa_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_task_lc_task_state_id_fkey FOREIGN KEY (task_state_id)
      REFERENCES und.wa_task_state (task_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_task_lc OWNER TO pavlov;

-- DROP TRIGGER und_wa_task_lc_set_stamp_trg ON und.wa_task_lc;

CREATE TRIGGER und_wa_task_lc_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_task_lc
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
