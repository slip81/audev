﻿/*
	CREATE TABLE und.asna_stock_row
*/

-- DROP TABLE und.asna_stock_row;

CREATE TABLE und.asna_stock_row
(
  row_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  in_batch_id integer,
  out_batch_id integer,  
  artikul integer NOT NULL,
  row_stock_id integer,
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  barcode character varying,
  asna_prt_id character varying,
  asna_nnt integer,
  asna_sku character varying,
  asna_qnt numeric,
  asna_doc character varying,
  asna_sup_inn character varying,
  asna_sup_id character varying,
  asna_sup_date timestamp without time zone,
  asna_nds integer,
  asna_gnvls boolean NOT NULL DEFAULT false,
  asna_dp boolean NOT NULL DEFAULT false,
  asna_man character varying,
  asna_series character varying,    
  asna_exp timestamp without time zone,
  asna_prc_man numeric,
  asna_prc_opt numeric,
  asna_prc_opt_nds numeric,
  asna_prc_ret numeric,
  asna_prc_reestr numeric,
  asna_prc_gnvls_max numeric,
  asna_sum_opt numeric,
  asna_sum_opt_nds numeric,
  asna_mr_gnvls numeric,
  asna_tag integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT asna_stock_row_pkey PRIMARY KEY (row_id),
  CONSTRAINT asna_stock_row_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_stock_row_in_batch_id_fkey FOREIGN KEY (in_batch_id)
      REFERENCES und.asna_stock_batch_in (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_stock_row_out_batch_id_fkey FOREIGN KEY (out_batch_id)
      REFERENCES und.asna_stock_batch_out (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_stock_row OWNER TO pavlov;

-- DROP TRIGGER und_asna_stock_row_set_stamp_trg ON und.asna_stock_row;

CREATE TRIGGER und_asna_stock_row_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_stock_row
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

