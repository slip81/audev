﻿/*
	CREATE TABLEs und.asna_store_info, und.asna_store_info_schedule
*/

-- DROP TABLE und.asna_store_info;

CREATE TABLE und.asna_store_info
(
  info_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  asna_name character varying,
  asna_full_name character varying,
  asna_inn character varying,
  asna_rt_terminal integer,
  asna_rt_mobile integer,
  asna_rt_site integer,
  asna_edit_check boolean NOT NULL DEFAULT false,
  asna_cancel_order boolean NOT NULL DEFAULT false,
  asna_full_time boolean NOT NULL DEFAULT false,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT asna_store_info_pkey PRIMARY KEY (info_id),
  CONSTRAINT asna_store_info_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_store_info OWNER TO pavlov;

-- DROP TRIGGER und_asna_store_info_set_stamp_trg ON und.asna_store_info;

CREATE TRIGGER und_asna_store_info_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_store_info
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();


-- DROP TABLE und.asna_store_info_schedule;

CREATE TABLE und.asna_store_info_schedule
(
  schedule_id serial NOT NULL,
  info_id integer NOT NULL,
  asna_week_day_id integer NOT NULL,
  asna_start_time character varying,
  asna_end_time character varying,
  asna_work boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT asna_store_info_schedule_pkey PRIMARY KEY (schedule_id),
  CONSTRAINT asna_store_info_schedule_info_id_fkey FOREIGN KEY (info_id)
      REFERENCES und.asna_store_info (info_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_store_info_schedule OWNER TO pavlov;

-- DROP TRIGGER und_asna_store_info_schedule_set_stamp_trg ON und.asna_store_info_schedule;

CREATE TRIGGER und_asna_store_info_schedule_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_store_info_schedule
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();