﻿/*
	CREATE TABLE und.wa_worker_request
*/

-- DROP TABLE und.wa_worker_request;

CREATE TABLE und.wa_worker_request
(
  item_id serial NOT NULL,
  worker_id integer NOT NULL,
  request_id bigint NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  inactive_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT wa_worker_request_pkey PRIMARY KEY (item_id),
  CONSTRAINT wa_worker_request_request_id_fkey FOREIGN KEY (request_id)
      REFERENCES und.wa_request (request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_worker_request_worker_id_fkey FOREIGN KEY (worker_id)
      REFERENCES und.wa_worker (worker_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_worker_request OWNER TO pavlov;
