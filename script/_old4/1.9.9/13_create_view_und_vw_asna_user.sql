﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_user 
*/

-- DROP VIEW und.vw_asna_user;

CREATE OR REPLACE VIEW und.vw_asna_user AS 
 SELECT t1.asna_user_id,
  t1.workplace_id,
  t1.is_active,
  t1.asna_store_id,
  t1.asna_pwd,
  t1.asna_legal_id,
  t1.asna_rr_id,
  t1.auth,
  t1.auth_date_beg,
  t1.auth_date_end,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.is_deleted,
  t1.del_date,
  t1.del_user,
  t2.registration_key as workplace_name,
  t2.sales_id,
  t3.adress as sales_name,
  t3.client_id,
  t4.name as client_name,
  case t1.is_active when true then 'Да' else 'Нет' end as is_active_str
   FROM und.asna_user t1
     INNER JOIN cabinet.workplace t2 ON t1.workplace_id = t2.id
     INNER JOIN cabinet.sales t3 ON t2.sales_id = t3.id
     INNER JOIN cabinet.client t4 ON t3.client_id = t4.id
     ;
     

ALTER TABLE und.vw_stock OWNER TO pavlov;
