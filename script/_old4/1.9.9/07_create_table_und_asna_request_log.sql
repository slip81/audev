﻿/*
	CREATE TABLE und.asna_request_log
*/

-- DROP TABLE und.asna_request_log;

CREATE TABLE und.asna_request_log
(
  log_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  request_id bigint,
  chain_id integer,
  in_batch_id integer,
  out_batch_id integer,
  info_id integer,  
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT asna_request_log_pkey PRIMARY KEY (log_id),
  CONSTRAINT asna_request_log_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_request_log OWNER TO pavlov;

-- DROP TRIGGER und_asna_request_log_set_stamp_trg ON und.asna_request_log;

CREATE TRIGGER und_asna_request_log_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_request_log
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

