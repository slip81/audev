﻿/*
	insert into und.asna_schedule_xxx
*/

insert into und.asna_schedule_type (
  schedule_type_id,
  schedule_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  1,
  'Отправка полных остатков',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_type (
  schedule_type_id,
  schedule_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  2,
  'Отправка изменений остатков',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

-- select * from und.asna_schedule_type order by schedule_type_id;

----------------------------------------

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '15 минут',
  15,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '30 минут',
  30,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '1 час',
  60,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '2 часа',
  120,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '3 часа',
  180,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '4 часа',
  240,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '5 часов',
  300,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '6 часов',
  360,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '7 часов',
  420,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '8 часов',
  480,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '9 часов',
  540,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);
insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '10 часов',
  600,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '11 часов',
  660,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '12 часов',
  720,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_interval_type (  
  interval_type_name,
  interval_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  'не повторять',
  null,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

-- select * from und.asna_schedule_interval_type order by interval_type_id;

----------------------------------------

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '00:00',
  0,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '00:30',
  30,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '01:00',
  60,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '01:30',
  90,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '02:00',
  120,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '02:30',
  150,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '03:00',
  180,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '03:30',
  210,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '04:00',
  240,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '04:30',
  270,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '05:00',
  300,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '05:30',
  330,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '06:00',
  360,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '06:30',
  390,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '07:00',
  420,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '07:30',
  450,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '08:00',
  480,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '08:30',
  510,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '09:00',
  540,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '09:30',
  570,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '10:00',
  600,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '10:30',
  630,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '11:00',
  660,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '11:30',
  690,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '12:00',
  720,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '12:30',
  750,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '13:00',
  780,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '13:30',
  810,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '14:00',
  840,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '14:30',
  870,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '15:00',
  900,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '15:30',
  930,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '16:00',
  960,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '16:30',
  990,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '17:00',
  1020,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '17:30',
  1050,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '18:00',
  1080,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '18:30',
  1110,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '19:00',
  1140,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '19:30',
  1170,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '20:00',
  1200,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '20:30',
  1230,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '21:00',
  1260,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '21:30',
  1290,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '22:00',
  1320,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '22:30',
  1350,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '23:00',
  1380,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.asna_schedule_time_type (  
  time_type_name,
  time_type_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
values (
  '23:30',
  1410,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

-- select * from und.asna_schedule_time_type order by time_type_id;

----------------------------------------