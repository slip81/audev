﻿/*
	CREATE TABLE und.asna_schedule
*/

-- DROP TABLE und.asna_schedule;

CREATE TABLE und.asna_schedule
(
  schedule_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  schedule_type_id integer NOT NULL,
  is_active boolean NOT NULL DEFAULT false,
  interval_type_id integer,
  start_time_type_id integer,
  end_time_type_id integer,
  is_day1 boolean NOT NULL DEFAULT false,
  is_day2 boolean NOT NULL DEFAULT false,
  is_day3 boolean NOT NULL DEFAULT false,
  is_day4 boolean NOT NULL DEFAULT false,
  is_day5 boolean NOT NULL DEFAULT false,
  is_day6 boolean NOT NULL DEFAULT false,
  is_day7 boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT asna_schedule_pkey PRIMARY KEY (schedule_id),
  CONSTRAINT asna_schedule_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_schedule_schedule_type_id_fkey FOREIGN KEY (schedule_type_id)
      REFERENCES und.asna_schedule_type (schedule_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_schedule_interval_type_id_fkey FOREIGN KEY (interval_type_id)
      REFERENCES und.asna_schedule_interval_type (interval_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_schedule_start_time_type_id_fkey FOREIGN KEY (start_time_type_id)
      REFERENCES und.asna_schedule_time_type (time_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT asna_schedule_end_time_type_id_fkey FOREIGN KEY (end_time_type_id)
      REFERENCES und.asna_schedule_time_type (time_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_schedule OWNER TO pavlov;

-- DROP TRIGGER und_asna_schedule_set_stamp_trg ON und.asna_schedule;

CREATE TRIGGER und_asna_schedule_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_schedule
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
