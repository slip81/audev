﻿/*
	INSERT INTO und.wa_task_state
*/

DELETE FROM und.wa_task_state;

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  1,
  'Выдано',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  2,
  'Выполняется на клиенте',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  3,
  'Выполняется на сервере',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  4,
  'Выполнено успешно',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  5,
  'Выполнено с ошибками',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

-- select * from und.wa_task_state order by task_state_id;

