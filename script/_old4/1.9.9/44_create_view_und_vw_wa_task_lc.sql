﻿/*
	CREATE OR REPLACE VIEW und.vw_wa_task_lc
*/

-- DROP VIEW und.vw_wa_task_lc;

CREATE OR REPLACE VIEW und.vw_wa_task_lc AS 
 SELECT t1.task_lc_id,
    t1.task_id,
    t1.task_state_id,
    t1.state_date,
    t1.state_user,
    t1.mess,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,    
    t2.request_type_id,    
    t2.client_id,
    t2.sales_id,
    t2.workplace_id,
    t2.ord,
    t2.ord_client,
    t2.is_auto_created,        
    t3.request_type_name,
    t3.request_type_group1_id,
    t3.request_type_group2_id,
    t11.request_type_group1_name,
    t12.request_type_group2_name,
    t4.task_state_name,
        CASE t2.is_auto_created
            WHEN true THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_auto_created_str,
    t3.is_inner,
    t3.is_outer
   FROM und.wa_task_lc t1
     JOIN und.wa_task t2 ON t1.task_id = t2.task_id
     JOIN und.wa_request_type t3 ON t2.request_type_id = t3.request_type_id
     JOIN und.wa_task_state t4 ON t1.task_state_id = t4.task_state_id
     LEFT JOIN und.wa_request_type_group1 t11 ON t3.request_type_group1_id = t11.request_type_group1_id
     LEFT JOIN und.wa_request_type_group2 t12 ON t3.request_type_group2_id = t12.request_type_group2_id;

ALTER TABLE und.vw_wa_task_lc OWNER TO pavlov;
