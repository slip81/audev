﻿/*
	CREATE TABLE und.asna_link
*/

-- DROP TABLE und.asna_link;

CREATE TABLE und.asna_link
(
  link_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  asna_nnt integer,
  asna_sku character varying,
  asna_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT asna_link_pkey PRIMARY KEY (link_id),
  CONSTRAINT asna_link_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_link OWNER TO pavlov;

-- DROP TRIGGER und_asna_link_set_stamp_trg ON und.asna_link;

CREATE TRIGGER und_asna_link_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_link
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

