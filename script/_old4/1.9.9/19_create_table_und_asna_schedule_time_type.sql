﻿/*
	CREATE TABLE und.asna_schedule_time_type
*/

-- DROP TABLE und.asna_schedule_time_type;

CREATE TABLE und.asna_schedule_time_type
(
  time_type_id serial NOT NULL,
  time_type_name character varying,
  time_type_value integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT asna_schedule_time_type_pkey PRIMARY KEY (time_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_schedule_time_type OWNER TO pavlov;

-- DROP TRIGGER und_asna_schedule_time_type_set_stamp_trg ON und.asna_schedule_time_type;

CREATE TRIGGER und_asna_schedule_time_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_schedule_time_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
