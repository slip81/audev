﻿/*
	CREATE OR REPLACE VIEW und.vw_wa_task
*/

-- DROP VIEW und.vw_wa_task;

CREATE OR REPLACE VIEW und.vw_wa_task AS 
 SELECT t1.task_id,
    t1.request_type_id,
    t1.task_state_id,
    t1.client_id,
    t1.sales_id,
    t1.workplace_id,
    t1.ord,
    t1.ord_client,
    t1.state_date,
    t1.state_user,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t1.is_auto_created,
    t1.mess,
    t2.request_type_name,
    t2.request_type_group1_id,
    t2.request_type_group2_id,
    t14.request_type_group1_name,
    t15.request_type_group2_name,
    t3.task_state_name,
    t11.name AS client_name,
    t12.adress AS sales_name,
    t13.registration_key AS workplace_name,
        CASE t1.is_auto_created
            WHEN true THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_auto_created_str,
    t2.is_inner,
    t2.is_outer
   FROM und.wa_task t1
     JOIN und.wa_request_type t2 ON t1.request_type_id = t2.request_type_id
     JOIN und.wa_task_state t3 ON t1.task_state_id = t3.task_state_id
     LEFT JOIN cabinet.client t11 ON t1.client_id = t11.id
     LEFT JOIN cabinet.sales t12 ON t1.sales_id = t12.id
     LEFT JOIN cabinet.workplace t13 ON t1.client_id = t13.id
     LEFT JOIN und.wa_request_type_group1 t14 ON t2.request_type_group1_id = t14.request_type_group1_id
     LEFT JOIN und.wa_request_type_group2 t15 ON t2.request_type_group2_id = t15.request_type_group2_id;

ALTER TABLE und.vw_wa_task OWNER TO postgres;
