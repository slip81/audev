﻿/*
	ALTER TABLE und.wa_task ADD COLUMNs is_auto_created, mess
*/

ALTER TABLE und.wa_task ADD COLUMN is_auto_created boolean NOT NULL DEFAULT false;
ALTER TABLE und.wa_task ADD COLUMN mess character varying;