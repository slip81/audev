﻿/*
	CREATE TABLE und.wa_worker_task
*/

-- DROP TABLE und.wa_worker_task;

CREATE TABLE und.wa_worker_task
(
  item_id serial NOT NULL,
  worker_id integer NOT NULL,
  task_id integer NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  inactive_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  CONSTRAINT wa_worker_task_pkey PRIMARY KEY (item_id),
  CONSTRAINT wa_worker_task_worker_id_fkey FOREIGN KEY (worker_id)
      REFERENCES und.wa_worker (worker_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_worker_task_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES und.wa_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_worker_task OWNER TO pavlov;


