﻿/*
	INSERT INTO und.wa_task_state
*/

DELETE FROM und.wa_request;
DELETE FROM und.wa_task;
DELETE FROM und.wa_task_state;

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  1,
  'Выдано клиенту',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  2,
  'Выдано серверу',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  3,
  'Выполняется на клиенте',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  4,
  'Выполняется на сервере',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  5,
  'Выполнено клиентом успешно',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  6,
  'Выполнено клиентом с ошибками',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  7,
  'Выполнено сервером успешно',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  8,
  'Выполнено сервером с ошибками',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);


-- select * from und.wa_task_state;