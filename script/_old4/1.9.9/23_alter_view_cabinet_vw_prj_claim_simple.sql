﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple
*/

-- DROP VIEW cabinet.vw_prj_claim_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple AS 
 SELECT t1.claim_id,
    t1.prj_id,
        CASE
            WHEN COALESCE(t11.task_cnt, 0::bigint) <= 0 AND t1.is_verified = false THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.new_task_cnt THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) > 0 AND t1.is_verified = false THEN 2
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.done_task_cnt AND t1.is_verified = false THEN 3
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t11.new_task_cnt, 0::bigint) <= 0 AND t11.task_cnt <> t11.canceled_task_cnt AND t11.task_cnt <> t11.done_task_cnt AND t1.is_verified = false THEN 3
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t11.new_task_cnt, 0::bigint) > 0 AND t11.task_cnt <> t11.canceled_task_cnt AND t11.task_cnt <> t11.done_task_cnt AND t1.is_verified = false THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.canceled_task_cnt AND t1.is_verified = false THEN 4
            WHEN t1.is_verified = true THEN 5
            ELSE 2
        END AS claim_state_type_id,
    t11.task_cnt,
    t11.new_task_cnt,
    t11.active_task_cnt,
    t11.done_task_cnt,
    t11.canceled_task_cnt,
    t11.active_work_type_id
   FROM cabinet.prj_claim t1
     LEFT JOIN ( SELECT x1.claim_id,
            count(x1.task_id) AS task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 1 THEN 1
                    ELSE 0
                END) AS new_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 2 THEN 1
                    ELSE 0
                END) AS active_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 3 THEN 1
                    ELSE 0
                END) AS done_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 4 THEN 1
                    ELSE 0
                END) AS canceled_task_cnt,
            min(x1.active_work_type_id) AS active_work_type_id
           FROM cabinet.vw_prj_task_simple x1
          GROUP BY x1.claim_id) t11 ON t1.claim_id = t11.claim_id;

ALTER TABLE cabinet.vw_prj_claim_simple OWNER TO pavlov;
