﻿/*
	CREATE TABLE und.asna_stock_chain
*/

-- DROP TABLE und.asna_stock_chain;

CREATE TABLE und.asna_stock_chain
(
  chain_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  chain_num integer NOT NULL,
  stock_date timestamp without time zone,
  is_full boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),  
  CONSTRAINT asna_stock_chain_pkey PRIMARY KEY (chain_id),
  CONSTRAINT asna_stock_chain_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_stock_chain OWNER TO pavlov;


-- DROP TRIGGER und_asna_stock_chain_set_stamp_trg ON und.asna_stock_chain;

CREATE TRIGGER und_asna_stock_chain_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_stock_chain
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
