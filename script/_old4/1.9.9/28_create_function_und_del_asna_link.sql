﻿/*
	CREATE OR REPLACE FUNCTION und.del_asna_link
*/

-- DROP FUNCTION und.del_asna_link(integer);

CREATE OR REPLACE FUNCTION und.del_asna_link(
    _asna_user_id integer
    )
  RETURNS void AS
$BODY$
BEGIN	
	delete from und.asna_link where asna_user_id = _asna_user_id;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.del_asna_link(integer) OWNER TO pavlov;
