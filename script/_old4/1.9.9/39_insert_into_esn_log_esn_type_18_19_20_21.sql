﻿/*
	insert into esn.log_esn_type 18, 19, 20, 21
*/

insert into esn.log_esn_type (type_id, type_name)
values (18, 'Worker: создание плановых запросов');

insert into esn.log_esn_type (type_id, type_name)
values (19, 'Worker: выполнение плановых запросов');



delete from esn.log_esn_type where type_id = 21;

insert into esn.log_esn_type (type_id, type_name)
values (20, 'Worker: начало обработки');

insert into esn.log_esn_type (type_id, type_name)
values (21, 'Worker: окончание обработки');

-- select * from esn.log_esn_type order by type_id;