﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_user
*/

-- DROP VIEW und.vw_asna_user;

CREATE OR REPLACE VIEW und.vw_asna_user AS 
 SELECT t1.asna_user_id,
    t1.workplace_id,
    t1.is_active,
    t1.asna_store_id,
    t1.asna_pwd,
    t1.asna_legal_id,
    t1.asna_rr_id,
    t1.auth,
    t1.auth_date_beg,
    t1.auth_date_end,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.is_deleted,
    t1.del_date,
    t1.del_user,
    t2.registration_key AS workplace_name,
    t2.sales_id,
    t3.adress AS sales_name,
    t3.client_id,
    t4.name AS client_name,
        CASE t1.is_active
            WHEN true THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_active_str,
    t11.schedule_id AS sch_full_schedule_id,
    t11.is_active AS sch_full_is_active,
    t11.interval_type_id AS sch_full_interval_type_id,
    t11.start_time_type_id AS sch_full_start_time_type_id,
    t11.end_time_type_id AS sch_full_end_time_type_id,
    t11.is_day1 AS sch_full_is_day1,
    t11.is_day2 AS sch_full_is_day2,
    t11.is_day3 AS sch_full_is_day3,
    t11.is_day4 AS sch_full_is_day4,
    t11.is_day5 AS sch_full_is_day5,
    t11.is_day6 AS sch_full_is_day6,
    t11.is_day7 AS sch_full_is_day7,
    t12.schedule_id AS sch_ch_schedule_id,
    t12.is_active AS sch_ch_is_active,
    t12.interval_type_id AS sch_ch_interval_type_id,
    t12.start_time_type_id AS sch_ch_start_time_type_id,
    t12.end_time_type_id AS sch_ch_end_time_type_id,
    t12.is_day1 AS sch_ch_is_day1,
    t12.is_day2 AS sch_ch_is_day2,
    t12.is_day3 AS sch_ch_is_day3,
    t12.is_day4 AS sch_ch_is_day4,
    t12.is_day5 AS sch_ch_is_day5,
    t12.is_day6 AS sch_ch_is_day6,
    t12.is_day7 AS sch_ch_is_day7,
    t13.time_type_value as sch_full_start_time_type_value,
    t14.time_type_value as sch_full_end_time_type_value,
    t15.interval_type_value as sch_full_interval_type_value,
    t16.time_type_value as sch_ch_start_time_type_value,
    t17.time_type_value as sch_ch_end_time_type_value,
    t18.interval_type_value as sch_ch_interval_type_value    
   FROM und.asna_user t1
     JOIN cabinet.workplace t2 ON t1.workplace_id = t2.id
     JOIN cabinet.sales t3 ON t2.sales_id = t3.id
     JOIN cabinet.client t4 ON t3.client_id = t4.id
     LEFT JOIN und.asna_schedule t11 ON t1.asna_user_id = t11.asna_user_id AND t11.schedule_type_id = 1 AND t11.is_deleted = false
     LEFT JOIN und.asna_schedule t12 ON t1.asna_user_id = t12.asna_user_id AND t12.schedule_type_id = 2 AND t11.is_deleted = false
     LEFT JOIN und.asna_schedule_time_type t13 ON t11.start_time_type_id = t13.time_type_id
     LEFT JOIN und.asna_schedule_time_type t14 ON t11.end_time_type_id = t14.time_type_id
     LEFT JOIN und.asna_schedule_interval_type t15 ON t11.interval_type_id = t15.interval_type_id
     LEFT JOIN und.asna_schedule_time_type t16 ON t12.start_time_type_id = t16.time_type_id
     LEFT JOIN und.asna_schedule_time_type t17 ON t12.end_time_type_id = t17.time_type_id
     LEFT JOIN und.asna_schedule_interval_type t18 ON t12.interval_type_id = t18.interval_type_id     
     ;

ALTER TABLE und.vw_asna_user OWNER TO postgres;
