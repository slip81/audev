﻿/*
	CREATE TABLE und.asna_stock_batch_in
*/

-- DROP TABLE und.asna_stock_batch_in;

CREATE TABLE und.asna_stock_batch_in
(
  batch_id serial NOT NULL,
  asna_user_id integer NOT NULL,
  chain_id integer NOT NULL,  
  batch_num integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  is_active boolean NOT NULL DEFAULT false,
  inactive_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),  
  CONSTRAINT asna_stock_batch_in_pkey PRIMARY KEY (batch_id),
  CONSTRAINT asna_stock_batch_in_asna_user_id_fkey FOREIGN KEY (asna_user_id)
      REFERENCES und.asna_user (asna_user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT asna_stock_batch_in_chain_id_fkey FOREIGN KEY (chain_id)
      REFERENCES und.asna_stock_chain (chain_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_stock_batch_in OWNER TO pavlov;


-- DROP TRIGGER und_asna_stock_batch_in_set_stamp_trg ON und.asna_stock_batch_in;

CREATE TRIGGER und_asna_stock_batch_in_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_stock_batch_in
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

