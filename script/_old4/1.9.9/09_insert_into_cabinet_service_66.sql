﻿/*
	insert into cabinet.service 66
*/

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, price, need_key, "NeedCheckVersionLicense", is_service, priority, have_spec, group_id)
values (66, 'c53121f0-bcfd-4b02-8f16-ef66da195b2d', 'АСНА 2.0', 'АСНА 2.0', 12, 0, 'asna2', 0, 0, 0, 0, true, 35, true, 3);

insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('c6365724-3ead-48c5-a5c7-9812877b55f5', '2.0.0.0', '2.0.0.0', 66, 0, 0);

-- select * from cabinet.service order by id desc
-- select * from cabinet.version order by service_id desc, id desc