﻿/*
	CREATE TABLE und.asna_schedule_type
*/

-- DROP TABLE und.asna_schedule_type;

CREATE TABLE und.asna_schedule_type
(
  schedule_type_id integer NOT NULL,
  schedule_type_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT asna_schedule_type_pkey PRIMARY KEY (schedule_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_schedule_type OWNER TO pavlov;

-- DROP TRIGGER und_asna_schedule_type_set_stamp_trg ON und.asna_schedule_type;

CREATE TRIGGER und_asna_schedule_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_schedule_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

