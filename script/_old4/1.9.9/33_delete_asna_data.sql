﻿/*
	delete asna data
*/

delete from und.asna_link;
delete from und.asna_store_info_schedule;
delete from und.asna_store_info;
delete from und.asna_stock_row;
delete from und.asna_stock_batch_in;
delete from und.asna_stock_batch_out;
delete from und.asna_stock_chain;
delete from und.asna_schedule;
delete from und.asna_link;
delete from und.asna_user_log;
delete from und.asna_user;

delete from und.wa_worker_task;
delete from und.wa_worker;
delete from und.wa_task;
delete from und.wa_request_log;
delete from und.wa_request_lc;
delete from und.wa_request_data;
delete from und.wa_request;