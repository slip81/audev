﻿/*
	INSERT INTO esn.log_esn_type 14, 15
*/

INSERT INTO esn.log_esn_type (
  type_id,
  type_name
)
VALUES (
  14,
  'АСНА: отправка остатков [полная]'
);

INSERT INTO esn.log_esn_type (
  type_id,
  type_name
)
VALUES (
  15,
  'АСНА: отправка остатков [изменения]'
);

-- select * from esn.log_esn_type order by type_id