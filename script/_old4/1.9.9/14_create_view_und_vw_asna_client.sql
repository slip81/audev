﻿/*
	CREATE OR REPLACE VIEW und.vw_asna_client
*/

-- DROP VIEW und.vw_asna_client;

CREATE OR REPLACE VIEW und.vw_asna_client AS 
 SELECT DISTINCT t1.client_id,
   t1.client_name
   FROM und.vw_asna_user t1;

ALTER TABLE und.vw_asna_user OWNER TO pavlov;
