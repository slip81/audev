﻿/*
	CREATE TABLE und.asna_user
*/

-- DROP TABLE und.asna_user;

CREATE TABLE und.asna_user
(
  asna_user_id serial NOT NULL,
  workplace_id integer NOT NULL,
  is_active boolean NOT NULL DEFAULT true,
  asna_store_id character varying,  
  asna_pwd character varying,  
  asna_legal_id character varying,
  asna_rr_id integer,
  auth character varying,
  auth_date_beg timestamp without time zone,
  auth_date_end timestamp without time zone,      
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT asna_user_pkey PRIMARY KEY (asna_user_id),
  CONSTRAINT asna_user_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.asna_user OWNER TO pavlov;

-- DROP TRIGGER und_asna_user_set_stamp_trg ON und.asna_user;

CREATE TRIGGER und_asna_user_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.asna_user
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

