﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_planner_event
*/

-- DROP VIEW cabinet.vw_planner_event;

CREATE OR REPLACE VIEW cabinet.vw_planner_event AS 
 SELECT t1.event_id,
    t1.description,
    t1.date_end,
    t1.end_timezone,
    t1.is_all_day,
    t1.recurrence_exception,
    t1.recurrence_rule,
    t1.recurrence_id,
    t1.date_beg,
    t1.start_timezone,
    t1.title,
    t1.exec_user_id,
    t1.is_public,
    t1.is_public_global,
    t1.crm_event_id,
    t1.event_priority_id,
    t1.rate,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
        CASE
            WHEN t1.is_all_day THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_all_day_str,
        CASE
            WHEN t1.is_public THEN 'Да'::text
            ELSE 'Нет'::text
        END AS is_public_str,
    t2.priority_name AS event_priority_name,
    t11.user_name AS exec_user_name,
    t1.task_id,
    t1.date_beg::date AS date_beg_date_only,
    t1.date_end::date AS date_end_date_only,
        CASE
            WHEN t1.task_id IS NULL THEN 'С-'::text || t1.event_id::text
            ELSE NULL::text
        END AS event_num,
    t12.task_num,
        CASE
            WHEN t1.task_id IS NOT NULL THEN t12.state_id
            ELSE t1.event_state_id
        END AS event_state_id,
    t12.group_id,
        CASE
            WHEN t1.task_id IS NULL THEN t3.state_name
            ELSE t13.state_name
        END AS state_name,
    t14.group_name,
    t1.progress,
    t1.result,
    t1.event_group_id,
    t4.group_name AS event_group_name,
        CASE
            WHEN
            CASE
                WHEN COALESCE(t11.crm_user_role_id, 0) > 0 AND COALESCE(t1.task_id, 0) > 0 THEN
                CASE
                    WHEN COALESCE(t16.state_id, (-1)) = COALESCE(t12.state_id, 0) THEN true
                    WHEN COALESCE(t16.state_id, (-1)) <> COALESCE(t12.state_id, 0) AND t1.date_end < 'now'::text::date THEN false
                    ELSE true
                END
                ELSE true
            END THEN false
            ELSE true
        END AS is_overdue,
    t17.priority_id,
    t17.is_boss AS priority_is_boss,
        CASE
            WHEN COALESCE(t1.task_id, 0) > 0 THEN
            CASE
                WHEN COALESCE(t11.crm_user_role_id, 0) > 0 AND COALESCE(t16.state_id, (-1)) = COALESCE(t12.state_id, 0) THEN true
                ELSE false
            END
            ELSE
            CASE
                WHEN COALESCE(t11.crm_user_role_id, 0) > 0 AND COALESCE(t16.state_id, (-1)) = COALESCE(t1.event_state_id, 0) THEN true
                ELSE false
            END
        END AS is_done,
    t12.is_control,
    t18.event_date_beg AS event_date_beg_orig,
    t18.event_date_end AS event_date_end_orig,
    t19.event_date_beg AS event_date_beg_curr,
    t19.event_date_end AS event_date_end_curr,
    t12.exec_user_id as task_exec_user_id,
    t20.user_name as task_exec_user_name
   FROM cabinet.planner_event t1
     JOIN cabinet.planner_event_priority t2 ON t1.event_priority_id = t2.priority_id
     LEFT JOIN cabinet.crm_state t3 ON t1.event_state_id = t3.state_id
     JOIN cabinet.crm_event_group t4 ON t1.event_group_id = t4.group_id
     LEFT JOIN cabinet.cab_user t11 ON t1.exec_user_id = t11.user_id
     LEFT JOIN cabinet.crm_task t12 ON t1.task_id = t12.task_id
     LEFT JOIN cabinet.crm_state t13 ON t12.state_id = t13.state_id
     LEFT JOIN cabinet.crm_group t14 ON t12.group_id = t14.group_id
     LEFT JOIN cabinet.crm_user_role t15 ON t11.crm_user_role_id = t15.role_id
     LEFT JOIN cabinet.crm_user_role_state_done t16 ON t15.role_id = t16.role_id AND t1.task_id IS NOT NULL AND t12.state_id = t16.state_id
     LEFT JOIN cabinet.crm_priority t17 ON t12.priority_id = t17.priority_id
     LEFT JOIN cabinet.crm_task_control t18 ON t12.task_id = t18.task_id AND t12.is_control = true AND t18.is_orig = true
     LEFT JOIN cabinet.crm_task_control t19 ON t12.task_id = t19.task_id AND t12.is_control = true AND t19.is_orig = false
     LEFT JOIN cabinet.cab_user t20 ON t12.exec_user_id = t20.user_id;

ALTER TABLE cabinet.vw_planner_event OWNER TO pavlov;
