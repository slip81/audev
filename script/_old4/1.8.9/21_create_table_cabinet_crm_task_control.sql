﻿/*
	CREATE TABLE cabinet.crm_task_control
*/

-- DROP TABLE cabinet.crm_task_control;

CREATE TABLE cabinet.crm_task_control
(
  item_id serial NOT NULL,
  task_id integer NOT NULL,
  required_date timestamp without time zone,
  repair_date timestamp without time zone,
  event_date_beg timestamp without time zone,
  event_date_end timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  is_orig boolean NOT NULL DEFAULT false,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT crm_task_control_pkey PRIMARY KEY (item_id),
  CONSTRAINT crm_task_control_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_task_control OWNER TO pavlov;

-- Trigger: cabinet_crm_task_control_set_stamp_trg on cabinet.crm_task_control

-- DROP TRIGGER cabinet_crm_task_control_set_stamp_trg ON cabinet.crm_task_control;

CREATE TRIGGER cabinet_crm_task_control_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.crm_task_control
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();
