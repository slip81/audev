﻿/*
	update cabinet.crm_task set user_id = 0
*/

update cabinet.crm_task
set exec_user_id = 0
where task_id in
(
select task_id from cabinet.crm_task
where exec_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
);

update cabinet.crm_task
set owner_user_id = 0
where task_id in
(
select task_id from cabinet.crm_task
where owner_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
);

update cabinet.crm_task
set prepared_user_id = 0
where task_id in
(
select task_id from cabinet.crm_task
where prepared_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
);

update cabinet.crm_task
set fixed_user_id = 0
where task_id in
(
select task_id from cabinet.crm_task
where fixed_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
);

update cabinet.crm_task
set approved_user_id = 0
where task_id in
(
select task_id from cabinet.crm_task
where approved_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
);

update cabinet.crm_task
set assigned_user_id = 0
where task_id in
(
select task_id from cabinet.crm_task
where assigned_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
);

update cabinet.crm_task
set moderator_user_id = 0
where task_id in
(
select task_id from cabinet.crm_task
where moderator_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
);

update cabinet.crm_task
set event_user_id = 0
where task_id in
(
select task_id from cabinet.crm_task
where event_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
);

update cabinet.cab_user
set is_crm_user = false, is_executer = false
where user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
);

-- select * from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')

/*

select exec_user_id, * from cabinet.crm_task
where exec_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
order by 1

select owner_user_id, * from cabinet.crm_task
where owner_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
order by 1

select prepared_user_id, * from cabinet.crm_task
where prepared_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
order by 1

select fixed_user_id, * from cabinet.crm_task
where fixed_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
order by 1

select approved_user_id, * from cabinet.crm_task
where approved_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
order by 1

select assigned_user_id, * from cabinet.crm_task
where assigned_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
order by 1

select moderator_user_id, * from cabinet.crm_task
where moderator_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
order by 1

select event_user_id, * from cabinet.crm_task
where event_user_id in
(
select user_id from cabinet.cab_user where user_name in ('ЗПВ', 'ГОЛ', 'КЯС', 'МИН', 'ПАВ', 'РАМ', 'СА', 'СИА', 'СОВ', 'ХЮВ', 'ЯВВ')
)
order by 1
*/