﻿/*
	ALTER TABLE cabinet.crm_task DROP COLUMNs
*/

ALTER TABLE cabinet.crm_task DROP COLUMN event_user_id;
ALTER TABLE cabinet.crm_task DROP COLUMN is_event;
ALTER TABLE cabinet.crm_task DROP COLUMN point;