﻿/*
	CREATE OR REPLACE VIEW discount.vw_report_check_count
*/

-- DROP VIEW discount.vw_report_check_count;

CREATE OR REPLACE VIEW discount.vw_report_check_count AS 
 SELECT row_number() OVER (ORDER BY t1.business_id) AS id,
    t1.business_id,
    t1.card_id,
    t1.card_num,
    t1.curr_discount_percent,
    t1.curr_trans_sum,
    t2.date_check::date AS date_beg,
    t2.card_trans_id,
    t3.unit_value_with_discount,
    t3.unit_value_discount,
    t3.unit_value,
    t3.detail_id,
    t5.card_holder_surname AS card_holder_name,
    t6.card_type_name,
    t1.curr_bonus_percent,
    t3.unit_value_bonus_for_card,
    t3.unit_value_bonus_for_pay,
    t1.curr_bonus_value
   FROM discount.vw_card t1
     JOIN discount.card_transaction t2 ON t1.card_id = t2.card_id AND t2.trans_kind = 1 AND COALESCE(t2.status, 0) = 0
     JOIN discount.card_transaction_detail t3 ON t2.card_trans_id = t3.card_trans_id
     LEFT JOIN discount.card_holder t5 ON t1.curr_holder_id = t5.card_holder_id
     LEFT JOIN discount.card_type t6 ON t1.curr_card_type_id = t6.card_type_id
  WHERE 1 = 1
  ORDER BY t1.business_id, t1.card_id, t1.card_num, t1.curr_discount_percent, t1.curr_trans_sum, t2.date_check::date;

ALTER TABLE discount.vw_report_check_count OWNER TO pavlov;
