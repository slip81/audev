﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN is_control
*/

ALTER TABLE cabinet.crm_task ADD COLUMN is_control boolean NOT NULL DEFAULT false;
