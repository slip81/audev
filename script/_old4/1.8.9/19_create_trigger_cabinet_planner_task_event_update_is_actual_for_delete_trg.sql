﻿/*
	CREATE TRIGGER cabinet_planner_task_event_update_is_actual_for_delete_trg
*/

-- DROP TRIGGER cabinet_planner_task_event_update_is_actual_for_delete_trg ON cabinet.planner_task_event;

CREATE TRIGGER cabinet_planner_task_event_update_is_actual_for_delete_trg
  AFTER DELETE
  ON cabinet.planner_task_event
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.planner_task_event_update_is_actual_for_delete();
