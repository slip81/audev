﻿/*
	CREATE TABLE cabinet.planner_task_event
*/

-- DROP TABLE cabinet.planner_task_event;

CREATE TABLE cabinet.planner_task_event
(
  item_id serial NOT NULL,
  task_id integer NOT NULL,
  event_id integer NOT NULL,
  is_actual boolean NOT NULL DEFAULT false,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope7(),
  CONSTRAINT planner_task_event_pkey PRIMARY KEY (item_id),
  CONSTRAINT planner_task_event_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT planner_task_event_event_id_fkey FOREIGN KEY (event_id)
      REFERENCES cabinet.planner_event (event_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.planner_task_event OWNER TO pavlov;

-- DROP TRIGGER cabinet_planner_task_event_set_stamp_trg ON cabinet.planner_task_event;

CREATE TRIGGER cabinet_planner_task_event_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.planner_task_event
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();
