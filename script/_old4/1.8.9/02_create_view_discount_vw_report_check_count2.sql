﻿/*
	CREATE VIEW discount.vw_report_check_count2
*/

--DROP VIEW discount.vw_report_check_count2;

CREATE OR REPLACE VIEW discount.vw_report_check_count2 AS 
select row_number() over(order by (select 0)) as id, t2.business_id, t1.date_check::date as date_check_date_only, t2.card_num, t3.card_holder_surname, t3.card_holder_name, t3.card_holder_fname,
sum(t1.trans_sum) as trans_sum, count(t1.card_trans_id) as trans_count
from discount.card_transaction t1
inner join discount.card t2 on t1.card_id = t2.card_id
left join discount.card_holder t3 on t2.curr_holder_id = t3.card_holder_id
where t1.trans_kind = 1 and t1.status = 0
--and t2.business_id = 957862396381103979
and t1.date_beg >= '2017-02-10' and t1.date_beg <= '2017-02-15'
group by t2.business_id, t1.date_check::date, t2.card_num, t3.card_holder_name, t3.card_holder_surname, t3.card_holder_fname
having count(t1.card_trans_id) > 1
order by t2.business_id, date_check_date_only, trans_count desc, card_num
;

ALTER TABLE discount.vw_report_check_count2 OWNER TO pavlov;
