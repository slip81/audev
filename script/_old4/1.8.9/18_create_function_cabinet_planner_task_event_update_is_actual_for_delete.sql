﻿/*
	CREATE OR REPLACE FUNCTION cabinet.planner_task_event_update_is_actual_for_delete
*/

-- DROP FUNCTION cabinet.planner_task_event_update_is_actual_for_delete();

CREATE OR REPLACE FUNCTION cabinet.planner_task_event_update_is_actual_for_delete()
  RETURNS trigger AS
$BODY$
BEGIN   
    update cabinet.planner_task_event
    set is_actual = false
    where task_id = OLD.task_id;
    
    update cabinet.planner_task_event
    set is_actual = true
    where task_id = OLD.task_id
    and event_id = (select x1.event_id from cabinet.planner_event x1 where cabinet.planner_task_event.task_id = x1.task_id and x1.event_id != OLD.event_id order by x1.date_end desc, x1.date_beg desc limit 1)
    ;
    
    RETURN OLD;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.planner_task_event_update_is_actual_for_delete() OWNER TO postgres;
