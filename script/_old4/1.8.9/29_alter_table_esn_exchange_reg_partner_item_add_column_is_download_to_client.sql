﻿/*
	ALTER TABLE esn.exchange_reg_partner_item ADD COLUMN is_download_to_client
*/

-- ALTER TABLE esn.exchange_reg_partner_item DROP COLUMN is_download_to_client;

ALTER TABLE esn.exchange_reg_partner_item ADD COLUMN is_download_to_client boolean NOT NULL DEFAULT true;
