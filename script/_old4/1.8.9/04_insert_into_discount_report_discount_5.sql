﻿/*
	insert into discount.report_discount 5
*/

insert into discount.report_discount (report_id, 
report_name, 
report_descr
)
values (5, 
'Количество чеков за день больше одного', 
'Отчет формирует список карт с указанем количества и суммы покупок, совершенных по картам за указанный период, если в день было совершено больше одной покупки по данной карте. Выводятся номер карты, ФИО владельца.'
);


-- select * from discount.report_discount order by report_id
