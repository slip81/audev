﻿/*
	CREATE FUNCTION cabinet.planner_task_event_update_is_actual_for_insert
*/

-- DROP FUNCTION cabinet.planner_task_event_update_is_actual_for_insert();

CREATE OR REPLACE FUNCTION cabinet.planner_task_event_update_is_actual_for_insert()
  RETURNS trigger AS
$BODY$
BEGIN   
    update cabinet.planner_task_event
    set is_actual = false
    where task_id = NEW.task_id;
    
    update cabinet.planner_task_event
    set is_actual = true
    where task_id = NEW.task_id
    and event_id = (select x1.event_id from cabinet.planner_event x1 where cabinet.planner_task_event.task_id = x1.task_id order by x1.date_end desc, x1.date_beg desc limit 1);
    
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.planner_task_event_update_is_actual_for_insert()  OWNER TO postgres;
