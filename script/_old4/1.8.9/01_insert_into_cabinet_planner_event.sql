﻿/*
	insert into cabinet.planner_event
*/

--select * from cabinet.planner_event order by event_id desc limit 100;

-- delete from cabinet.planner_note;
delete from cabinet.planner_event where crm_event_id is not null;

insert into cabinet.planner_event (
  description,
  date_end,
  end_timezone,
  is_all_day,
  recurrence_exception,
  recurrence_rule,
  recurrence_id,
  date_beg,
  start_timezone,
  title,
  exec_user_id,
  is_public,
  crm_event_id,
  event_priority_id,
  rate,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  task_id,
  event_state_id,
  progress,
  result,
  event_group_id,
  is_public_global
)
select
  event_text,
  date_end,
  null,
  true,
  null,
  null,
  null,
  date_beg,
  null,
  event_name,
  exec_user_id,
  true,
  event_id,
  0,
  null,
  crt_date,
  crt_user,
  current_timestamp,
  'ПМС',
  task_id,
  case when is_canceled then 4 when is_done then 5 else 1 end,
  event_progress,
  event_result,
  event_group_id,
  false
from cabinet.crm_event;

/*
select * from cabinet.planner_event_priority;
select * from cabinet.crm_state order by state_id;
select * from cabinet.crm_event_group;
*/