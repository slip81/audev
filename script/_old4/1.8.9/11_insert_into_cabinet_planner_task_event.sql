﻿/*
	insert into cabinet.planner_task_event
*/

delete from cabinet.planner_task_event;

insert into cabinet.planner_task_event (task_id, event_id, is_actual)
select task_id, event_id, false
from cabinet.planner_event
where task_id is not null;

-- update cabinet.planner_task_event set is_actual = false;

update cabinet.planner_task_event
set is_actual = true
where event_id = (select x1.event_id from cabinet.planner_event x1 where cabinet.planner_task_event.task_id = x1.task_id order by x1.date_end desc, x1.date_beg desc limit 1);


-- select * from cabinet.planner_task_event order by task_id desc;
