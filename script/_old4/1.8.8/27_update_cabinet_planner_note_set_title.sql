﻿/*
	update cabinet.planner_note set title
*/

update cabinet.planner_note set title = description where coalesce(title,'') = '' and coalesce(task_id,0) = 0;

update cabinet.planner_note set title = 
(select task_name from cabinet.crm_task where task_id = cabinet.planner_note.task_id)
where coalesce(title,'') = '' and coalesce(task_id,0) != 0;