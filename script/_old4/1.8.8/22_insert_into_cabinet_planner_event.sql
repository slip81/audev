﻿/*
	insert into cabinet.planner_event
*/

insert into cabinet.planner_event (
  description,
  date_end,
  is_all_day,
  date_beg,  
  title,
  exec_user_id,
  is_public,
  crm_event_id,
  event_priority_id,
  rate,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  task_id,
  event_state_id,
  progress,
  result,
  event_group_id
)
select
  event_text,
  date_end,
  true,
  date_beg,  
  event_name,
  exec_user_id,
  true,
  event_id,
  0,
  0,
  crt_date,
  crt_user,
  current_timestamp,
  'ПМС',
  task_id,
  case when cabinet.crm_event.is_canceled then 2 else case when cabinet.crm_event.is_done then 1 else 0 end end,
  event_progress,
  event_result,
  coalesce(event_group_id, 1)
from cabinet.crm_event
where not exists (select x1.event_id from cabinet.planner_event x1 where x1.crm_event_id = cabinet.crm_event.event_id);
