﻿/*
	ALTER TABLE cabinet.crm_state ADD COLUMN for_event
*/

ALTER TABLE cabinet.crm_state ADD COLUMN for_event boolean NOT NULL DEFAULT false;
UPDATE cabinet.crm_state SET for_event = true WHERE state_id in (1, 4, 5);

-- select * from cabinet.crm_state order by state_id
