﻿/*
	CREATE TABLE cabinet.planner_event_state
*/

-- DROP TABLE cabinet.planner_event_state;

CREATE TABLE cabinet.planner_event_state
(
  state_id integer NOT NULL,
  state_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT planner_event_state_pkey PRIMARY KEY (state_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cabinet.planner_event_state
  OWNER TO pavlov;

-- DROP TRIGGER cabinet_planner_event_state_set_stamp_trg ON cabinet.planner_event_state;

CREATE TRIGGER cabinet_planner_event_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.planner_event_state
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();


insert into cabinet.planner_event_state (state_id, state_name)
values (0, 'Активно');

insert into cabinet.planner_event_state (state_id, state_name)
values (1, 'Выполнено');

insert into cabinet.planner_event_state (state_id, state_name)
values (2, 'Отменено');