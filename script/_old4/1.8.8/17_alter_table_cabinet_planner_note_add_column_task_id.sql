﻿/*
	ALTER TABLE cabinet.planner_note ADD COLUMN task_id integer
*/

ALTER TABLE cabinet.planner_note ADD COLUMN task_id integer;

ALTER TABLE cabinet.planner_note
  ADD CONSTRAINT planner_note_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
