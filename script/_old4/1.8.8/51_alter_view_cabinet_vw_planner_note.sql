﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_planner_note
*/

-- DROP VIEW cabinet.vw_planner_note;

CREATE OR REPLACE VIEW cabinet.vw_planner_note AS 
 SELECT t1.note_id,
    t1.description,
    t1.exec_user_id,
    t1.is_public,
    t1.is_public_global,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.sort_num,
    t1.trigger_flag,
    t1.task_id,
    t1.title,
    t1.progress,
    t1.result,
    t11.task_num,
    t1.event_state_id,
    t11.state_id,
    t12.state_name
   FROM cabinet.planner_note t1
     LEFT JOIN cabinet.crm_task t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.crm_state t12 ON t11.state_id = t12.state_id
     ;

ALTER TABLE cabinet.vw_planner_note OWNER TO pavlov;
