﻿/*
	ALTER TABLE cabinet.planner_event DROP CONSTRAINT planner_event_event_state_id_fkey
*/

delete from cabinet.planner_event where task_id is null;      

ALTER TABLE cabinet.planner_event DROP CONSTRAINT planner_event_event_state_id_fkey;

ALTER TABLE cabinet.planner_event
  ADD CONSTRAINT planner_event_event_state_id_fkey FOREIGN KEY (event_state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;