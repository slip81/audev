﻿/*
	update cabinet.client set inn
*/

update cabinet.company set name_company = '' where name_company = '_';
update cabinet.company set name_company_full = '' where name_company_full = '_';
update cabinet.company set name_region = '' where name_region = '_';
update cabinet.company set name_city = '' where name_city = '_';
update cabinet.company set address = '' where address = '_';
update cabinet.company set phone = '' where phone = '_';
update cabinet.company set email = '' where email = '_';
update cabinet.company set inn = '' where inn = '_';
update cabinet.company set kpp = '' where kpp = '_';
update cabinet.company set face_fio = '' where face_fio = '_';
update cabinet.company set p_address = '' where p_address = '_';
update cabinet.company set name_state = '' where name_state = '_';

-- select * from cabinet.company where id_company = 2969

update cabinet.client
set inn = (select x1.inn from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
where trim(coalesce(inn,'')) = ''
and sm_id is not null;	

