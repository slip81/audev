﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_planner_event
*/

-- DROP VIEW cabinet.vw_planner_event;

CREATE OR REPLACE VIEW cabinet.vw_planner_event AS
 SELECT t1.event_id,
  t1.description,
  t1.date_end,
  t1.end_timezone,
  t1.is_all_day,
  t1.recurrence_exception,
  t1.recurrence_rule,
  t1.recurrence_id,
  t1.date_beg,
  t1.start_timezone,
  t1.title,
  t1.exec_user_id,
  t1.is_public,
  t1.crm_event_id,
  t1.event_priority_id,
  t1.rate,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  case when t1.is_all_day then 'Да' else 'Нет' end as is_all_day_str,
  case when t1.is_public then 'Да' else 'Нет' end as is_public_str,
  t2.priority_name as event_priority_name,
  t11.user_name as exec_user_name
   FROM cabinet.planner_event t1
     JOIN cabinet.planner_event_priority t2 ON t1.event_priority_id = t2.priority_id
     LEFT JOIN cabinet.cab_user t11 ON t1.exec_user_id = t11.user_id;

ALTER TABLE cabinet.vw_planner_event OWNER TO pavlov;

