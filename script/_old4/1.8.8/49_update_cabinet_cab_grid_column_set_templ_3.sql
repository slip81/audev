﻿/*
	update cabinet.cab_grid_column set templ 3
*/

update cabinet.cab_grid_column 
set templ = '<a href="javascript:void" onclick="if (typeof taskList != ''undefined'') { taskList.onTaskNumCellClick(#=task_id#); } else { planner.onTaskNumCellClick(#=task_id#); }; return false;">#=task_num#</a>' 
where column_id = 3;

-- select * from cabinet.cab_grid_column
-- "<a href="javascript:void" onclick="taskList.onTaskNumCellClick(#=task_id#)">#=task_num#</a>"