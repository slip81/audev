﻿/*
	CREATE OR REPLACE FUNCTION cabinet.planner_note_insert_sort()
*/

-- DROP FUNCTION cabinet.planner_note_insert_sort();

CREATE OR REPLACE FUNCTION cabinet.planner_note_insert_sort()
  RETURNS trigger AS
$BODY$
BEGIN   
    NEW.sort_num := coalesce((select max(sort_num) + 1 from cabinet.planner_note where exec_user_id = NEW.exec_user_id), 1);
    --NEW.sort_num := 1;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.planner_note_insert_sort() OWNER TO postgres;