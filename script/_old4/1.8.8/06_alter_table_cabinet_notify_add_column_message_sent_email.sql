﻿/*
	ALTER TABLE cabinet.notify ADD COLUMN message_sent_email
*/

ALTER TABLE cabinet.notify ADD COLUMN message_sent_email boolean NOT NULL DEFAULT false;

UPDATE cabinet.notify SET message_sent_email = message_sent;