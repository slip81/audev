﻿/*
	CREATE TABLE cabinet.planner_event
*/

DROP TABLE cabinet.planner_event;

CREATE TABLE cabinet.planner_event
(
  event_id serial NOT NULL,
  description text,
  date_end timestamp without time zone NOT NULL,
  end_timezone text,
  is_all_day boolean NOT NULL DEFAULT false,
  recurrence_exception text,
  recurrence_rule text,
  recurrence_id integer,
  date_beg timestamp without time zone NOT NULL,
  start_timezone text,
  title text,
  exec_user_id integer,
  is_public boolean NOT NULL DEFAULT false,
  crm_event_id integer,
  event_priority_id integer NOT NULL DEFAULT 0,
  rate integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT planner_event_pkey PRIMARY KEY (event_id),
  CONSTRAINT planner_event_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT planner_event_crm_event_id_fkey FOREIGN KEY (crm_event_id)
      REFERENCES cabinet.crm_event (event_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT planner_event_event_priority_id_fkey FOREIGN KEY (event_priority_id)
      REFERENCES cabinet.planner_event_priority (priority_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cabinet.planner_event
  OWNER TO pavlov;


-- DROP TRIGGER cabinet_planner_event_set_stamp_trg ON cabinet.planner_event;

CREATE TRIGGER cabinet_planner_event_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.planner_event
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();
