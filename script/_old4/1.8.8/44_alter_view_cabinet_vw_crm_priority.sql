﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_priority
*/

-- DROP VIEW cabinet.vw_crm_priority;

CREATE OR REPLACE VIEW cabinet.vw_crm_priority AS 
 SELECT t1.priority_id,
    t1.priority_name,
    t11.user_id,
        CASE
            WHEN t12.fore_color IS NULL THEN 'black'::character varying
            ELSE t12.fore_color
        END AS fore_color,
    t1.ord,
    t1.is_control,
    t1.is_boss
   FROM cabinet.crm_priority t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1
     LEFT JOIN cabinet.crm_priority_user_settings t12 ON t1.priority_id = t12.priority_id AND t11.user_id = COALESCE(t12.user_id, t11.user_id);

ALTER TABLE cabinet.vw_crm_priority OWNER TO pavlov;
