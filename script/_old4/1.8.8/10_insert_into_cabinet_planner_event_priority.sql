﻿/*
	insert into cabinet.planner_event_priority
*/

insert into cabinet.planner_event_priority (priority_id, priority_name)
values (0, 'Обычный');

insert into cabinet.planner_event_priority (priority_id, priority_name)
values (1, 'Повышенный');

insert into cabinet.planner_event_priority (priority_id, priority_name)
values (2, 'Высокий');

insert into cabinet.planner_event_priority (priority_id, priority_name)
values (3, 'Максимальный');


-- select * from cabinet.planner_event_priority