﻿/*
	CREATE OR REPLACE FUNCTION discount.update_card_discount_percent_vip
*/

-- DROP FUNCTION discount.update_card_discount_percent_vip(bigint, bigint, text);

CREATE OR REPLACE FUNCTION discount.update_card_discount_percent_vip(
    IN _business_id bigint,
    IN _card_type_id bigint,
    IN _user_name text,
    OUT result integer)
  RETURNS integer AS
$BODY$
DECLARE
  _trans_sum_for_vip numeric;
  _discount_percent_for_vip numeric;
  _days_count_legit integer;
  _card_id bigint;
  _date_of_first_sale date;
  _days_count_from_first_sale integer;
BEGIN	
	/*
		Процедура запускается еженочно (в 00:05) и выставляет всем картам заданной организации и заданного типа процент скидки = _discount_percent_for_vip,
		если сумма накоплений на карте в течении _days_count_legit календарных дней с момента первой продажи больше либо равна _trans_sum_for_vip
	*/
	
	_trans_sum_for_vip := 30000;
	_discount_percent_for_vip := 20;
	_days_count_legit := 30;

	result := 0;
	
	FOR _card_id IN 
		SELECT DISTINCT card_id 
		FROM discount.vw_card
		WHERE business_id = _business_id AND curr_card_type_id = _card_type_id
		AND coalesce(curr_trans_sum, 0) >= _trans_sum_for_vip
		AND coalesce(curr_discount_percent, 0) < _discount_percent_for_vip
	LOOP
		_date_of_first_sale := (SELECT min(date_check)::date
		FROM discount.card_transaction
		WHERE card_id = _card_id
		AND status = 0
		AND trans_kind = 1);

		CONTINUE WHEN _date_of_first_sale IS NULL;

		_days_count_from_first_sale := (SELECT DATE_PART('day', current_date::timestamp - _date_of_first_sale::timestamp)::integer);

		CONTINUE WHEN _days_count_from_first_sale > _days_count_legit;
		
		UPDATE discount.card_nominal
		SET date_end = current_date
		WHERE card_id = _card_id
		AND unit_type = 2
		AND date_end IS NULL;

		INSERT INTO discount.card_nominal (card_id, date_beg, unit_type, unit_value, crt_date, crt_user)
		VALUES (_card_id, current_date, 2, _discount_percent_for_vip, current_timestamp, _user_name);		

		result:= result + 1;
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.update_card_discount_percent_vip(bigint, bigint, text)  OWNER TO pavlov;
