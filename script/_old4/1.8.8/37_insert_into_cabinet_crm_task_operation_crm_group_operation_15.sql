﻿/*
	insert into cabinet.crm_task_operation, cabinet.crm_group_operation
*/

insert into cabinet.crm_task_operation (operation_id, operation_name)
values (15, 'В проект');

insert into cabinet.crm_group_operation (group_id, operation_id, operation_name)
select group_id, 15, 'В проект'
from cabinet.crm_group;


/*
select * from cabinet.crm_task_operation order by operation_id
select * from cabinet.crm_group order by group_id;
select * from cabinet.crm_group_operation order by group_id, operation_id
*/