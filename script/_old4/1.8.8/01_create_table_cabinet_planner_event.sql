﻿/*
	CREATE TABLE cabinet.planner_event
*/

-- DROP TABLE cabinet.planner_event;

CREATE TABLE cabinet.planner_event
(
  event_id serial NOT NULL,
  description text,
  date_end timestamp without time zone NOT NULL,
  end_timezone text,
  is_all_day boolean NOT NULL DEFAULT False,       
  recurrence_exception text,
  recurrence_rule text,
  date_beg timestamp without time zone NOT NULL,
  start_tmezone text,
  title text,  
  exec_user_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT planner_event_pkey PRIMARY KEY (event_id),
  CONSTRAINT planner_event_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.planner_event OWNER TO pavlov;

-- DROP TRIGGER cabinet_planner_event_set_stamp_trg ON cabinet.planner_event;

CREATE TRIGGER cabinet_planner_event_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.planner_event
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

