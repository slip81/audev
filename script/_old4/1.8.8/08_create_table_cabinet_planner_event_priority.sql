﻿/*
	CREATE TABLE cabinet.planner_event_priority
*/

-- DROP TABLE cabinet.planner_event_priority;

CREATE TABLE cabinet.planner_event_priority
(
  priority_id integer NOT NULL,
  priority_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT planner_event_priority_pkey PRIMARY KEY (priority_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.planner_event_priority OWNER TO pavlov;


CREATE TRIGGER cabinet_planner_event_priority_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.planner_event_priority
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();
