﻿/*
	insert into cabinet.auth_section 13, cabinet.auth_section_part 35
*/

insert into cabinet.auth_section (section_id, section_name, group_id)
values (13, 'Планировщик', 1);


insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (35, 'События', 13, 1);


insert into cabinet.auth_role_perm (role_id, part_id, allow_view, allow_edit)
select role_id, 35, true, true
from cabinet.auth_role
where role_id in (1, 2, 3, 4, 5, 6, 18);


-- select * from cabinet.auth_section order by section_id;
-- select * from cabinet.auth_section_part order by part_id;
-- select * from cabinet.auth_role_perm order by role_id, part_id
-- select * from cabinet.auth_role order by role_id