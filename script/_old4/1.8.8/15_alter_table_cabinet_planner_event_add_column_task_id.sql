﻿/*
	ALTER TABLE cabinet.planner_event ADD COLUMN task_id integer
*/

ALTER TABLE cabinet.planner_event ADD COLUMN task_id integer;

ALTER TABLE cabinet.planner_event
  ADD CONSTRAINT planner_event_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.crm_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
