﻿/*
	ALTER TABLE cabinet.planner_note ADD COLUMNs
*/

ALTER TABLE cabinet.planner_note ADD COLUMN title text;
ALTER TABLE cabinet.planner_note ADD COLUMN progress text;
ALTER TABLE cabinet.planner_note ADD COLUMN result text;