﻿/*
	CREATE TABLE cabinet.planner_note
*/

-- DROP TABLE cabinet.planner_note;

CREATE TABLE cabinet.planner_note
(
  note_id serial NOT NULL,
  description text,
  exec_user_id integer,
  is_public boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  sort_num integer,
  trigger_flag boolean NOT NULL DEFAULT false,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT planner_note_pkey PRIMARY KEY (note_id),
  CONSTRAINT planner_note_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.planner_note OWNER TO pavlov;

-- DROP TRIGGER cabinet_planner_note_set_stamp_trg ON cabinet.planner_note;

CREATE TRIGGER cabinet_planner_note_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.planner_note
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

-- DROP TRIGGER cabinet_planner_note_update_sort_trg ON cabinet.planner_note;

CREATE TRIGGER cabinet_planner_note_update_sort_trg
  AFTER INSERT OR UPDATE OF trigger_flag
  ON cabinet.planner_note
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.planner_note_update_sort();

-- DROP TRIGGER cabinet_planner_note_insert_sort_trg ON cabinet.planner_note;

CREATE TRIGGER cabinet_planner_note_insert_sort_trg
  BEFORE INSERT
  ON cabinet.planner_note
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.planner_note_insert_sort();  