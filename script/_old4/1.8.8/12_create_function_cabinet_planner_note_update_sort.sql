﻿/*
	CREATE OR REPLACE FUNCTION cabinet.planner_note_update_sort()
*/

-- DROP FUNCTION cabinet.planner_note_update_sort();

CREATE OR REPLACE FUNCTION cabinet.planner_note_update_sort()
  RETURNS trigger AS
$BODY$
BEGIN   
    with cte2 as (
    select row_number() over (order by sort_num) as sort_num, note_id 
    from cabinet.planner_note 
    where exec_user_id = NEW.exec_user_id 
    and note_id != NEW.note_id
    and sort_num <= NEW.sort_num
    )
    update cabinet.planner_note
    set sort_num = cte2.sort_num
    from cte2
    where cabinet.planner_note.note_id = cte2.note_id and cabinet.planner_note.sort_num <= NEW.sort_num;  
    
    with cte1 as (
    select row_number() over (order by sort_num) as sort_num, note_id 
    from cabinet.planner_note 
    where exec_user_id = NEW.exec_user_id 
    and note_id != NEW.note_id
    and sort_num >= NEW.sort_num
    )
    update cabinet.planner_note
    set sort_num = NEW.sort_num + cte1.sort_num
    from cte1
    where cabinet.planner_note.note_id = cte1.note_id and cabinet.planner_note.sort_num >= NEW.sort_num;    
    
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.planner_note_update_sort() OWNER TO postgres;
