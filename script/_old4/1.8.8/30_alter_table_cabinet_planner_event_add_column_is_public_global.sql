﻿/*
	ALTER TABLE cabinet.planner_event ADD COLUMN is_public_global
*/

ALTER TABLE cabinet.planner_event ADD COLUMN is_public_global boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.planner_note ADD COLUMN is_public_global boolean NOT NULL DEFAULT false;

