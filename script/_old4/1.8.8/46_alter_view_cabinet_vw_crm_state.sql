﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_crm_state
*/

-- DROP VIEW cabinet.vw_crm_state;

CREATE OR REPLACE VIEW cabinet.vw_crm_state AS 
 SELECT t1.state_id,
    t1.state_name,
    t11.user_id,
        CASE
            WHEN t12.fore_color IS NULL THEN 'black'::character varying
            ELSE t12.fore_color
        END AS fore_color,
    t1.ord,
    t1.for_event
   FROM cabinet.crm_state t1
     LEFT JOIN cabinet.cab_user t11 ON 1 = 1
     LEFT JOIN cabinet.crm_state_user_settings t12 ON t1.state_id = t12.state_id AND t11.user_id = COALESCE(t12.user_id, t11.user_id);

ALTER TABLE cabinet.vw_crm_state OWNER TO pavlov;
