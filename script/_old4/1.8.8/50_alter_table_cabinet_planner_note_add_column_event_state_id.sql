﻿/*
	ALTER TABLE cabinet.planner_note ADD COLUMN event_state_id
*/

ALTER TABLE cabinet.planner_note ADD COLUMN event_state_id integer NOT NULL DEFAULT 0;

ALTER TABLE cabinet.planner_note
  ADD CONSTRAINT planner_note_event_state_id_fkey FOREIGN KEY (event_state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
