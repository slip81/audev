﻿/*
	ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN batch_filter
*/

ALTER TABLE cabinet.crm_task_user_filter ADD COLUMN batch_filter character varying;