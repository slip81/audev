﻿/*
	ALTER TABLE cabinet.crm_priority ADD COLUMN is_boss
*/

ALTER TABLE cabinet.crm_priority ADD COLUMN is_boss boolean NOT NULL DEFAULT false;

UPDATE cabinet.crm_priority SET is_boss = true WHERE priority_id = 4;