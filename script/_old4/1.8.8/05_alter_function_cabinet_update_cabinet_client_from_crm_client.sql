﻿/*
	CREATE OR REPLACE FUNCTION cabinet.update_cabinet_client_from_crm_client
*/

-- DROP FUNCTION cabinet.update_cabinet_client_from_crm_client(integer, integer);

CREATE OR REPLACE FUNCTION cabinet.update_cabinet_client_from_crm_client(
    IN _cabinet_client_id integer,
    IN _crm_client_id integer,
    OUT result integer)
  RETURNS integer AS
$BODY$
BEGIN
	update cabinet.client
	set sm_id = _crm_client_id
	where id = _cabinet_client_id;

	update cabinet.client
	set state_id = (select x1.id_state from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
	where id = _cabinet_client_id
	and state_id is null
	and _crm_client_id is not null;

	update cabinet.client
	set inn = (select x1.inn from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
	where id = _cabinet_client_id
	and trim(coalesce(inn,'')) = ''
	and _crm_client_id is not null;	

	update cabinet.client
	set kpp = (select x1.kpp from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
	where id = _cabinet_client_id
	and trim(coalesce(kpp,'')) = ''
	and _crm_client_id is not null;

	update cabinet.client
	set contact_person = (select x1.face_fio from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
	where id = _cabinet_client_id
	and trim(coalesce(contact_person,'')) = ''
	and _crm_client_id is not null;

	update cabinet.client
	set cell = (select x1.phone from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
	where id = _cabinet_client_id
	and trim(coalesce(cell,'')) = ''
	and _crm_client_id is not null;

	update cabinet.client
	set site = (select x1.email from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
	where id = _cabinet_client_id
	and trim(coalesce(site,'')) = ''
	and _crm_client_id is not null;

	update cabinet.client
	set legal_address = (select x1.address from cabinet.company x1 where x1.id_company = cabinet.client.sm_id and trim(coalesce(x1.p_address,'')) = '')
	where id = _cabinet_client_id
	and trim(coalesce(legal_address,'')) = ''
	and _crm_client_id is not null;

	update cabinet.client
	set legal_address = (select x1.p_address from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
	where id = _cabinet_client_id
	and trim(coalesce(legal_address,'')) = ''
	and _crm_client_id is not null;

	update cabinet.client
	set absolute_address = (select x1.address from cabinet.company x1 where x1.id_company = cabinet.client.sm_id and trim(coalesce(x1.p_address,'')) = '')
	where id = _cabinet_client_id
	and trim(coalesce(absolute_address,'')) = ''
	and _crm_client_id is not null;

	update cabinet.client
	set absolute_address = (select x1.p_address from cabinet.company x1 where x1.id_company = cabinet.client.sm_id)
	where id = _cabinet_client_id
	and trim(coalesce(absolute_address,'')) = ''
	and _crm_client_id is not null;

	result:= 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.update_cabinet_client_from_crm_client(integer, integer) OWNER TO pavlov;
