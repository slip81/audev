﻿/*
	ALTER TABLE cabinet.crm_task ADD COLUMN batch_id
*/

ALTER TABLE cabinet.crm_task ADD COLUMN batch_id integer NOT NULL DEFAULT 0;

ALTER TABLE cabinet.crm_task
  ADD CONSTRAINT crm_task_batch_id_fkey FOREIGN KEY (batch_id)
      REFERENCES cabinet.crm_batch (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
