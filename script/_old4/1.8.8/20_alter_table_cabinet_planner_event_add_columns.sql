﻿/*
	ALTER TABLE cabinet.planner_event ADD COLUMNs
*/

ALTER TABLE cabinet.planner_event ADD COLUMN event_state_id integer NOT NULL DEFAULT 0;
ALTER TABLE cabinet.planner_event ADD COLUMN progress text;
ALTER TABLE cabinet.planner_event ADD COLUMN result text;
ALTER TABLE cabinet.planner_event ADD COLUMN event_group_id integer NOT NULL DEFAULT 1;

ALTER TABLE cabinet.planner_event
  ADD CONSTRAINT planner_event_event_state_id_fkey FOREIGN KEY (event_state_id)
      REFERENCES cabinet.planner_event_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cabinet.planner_event
  ADD CONSTRAINT planner_event_event_group_id_fkey FOREIGN KEY (event_group_id)
      REFERENCES cabinet.crm_event_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      