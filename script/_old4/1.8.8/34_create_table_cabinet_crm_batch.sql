﻿/*
	CREATE TABLE cabinet.crm_batch
*/

-- DROP TABLE cabinet.crm_batch;

CREATE TABLE cabinet.crm_batch
(
  batch_id integer NOT NULL,
  batch_name character varying,
  date_beg date,
  date_end date,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope6(),
  CONSTRAINT crm_batch_pkey PRIMARY KEY (batch_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.crm_batch OWNER TO pavlov;

-- DROP TRIGGER cabinet_crm_batch_set_stamp_trg ON cabinet.crm_batch;

CREATE TRIGGER cabinet_crm_batch_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.crm_batch
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();


INSERT INTO cabinet.crm_batch (batch_id, batch_name)
VALUES (0, '[не указан]');
