﻿/*
	CREATE TABLE cabinet.service_stock
*/

-- DROP TABLE cabinet.service_stock;

CREATE TABLE cabinet.service_stock
(
  item_id serial NOT NULL,
  workplace_id integer NOT NULL,
  service_id integer NOT NULL,
  batch_row_cnt integer NOT NULL DEFAULT 100,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT service_stock_pkey PRIMARY KEY (item_id),
  CONSTRAINT service_stock_service_id_fkey FOREIGN KEY (service_id)
      REFERENCES cabinet.service (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT service_stock_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.service_stock OWNER TO pavlov;

-- DROP TRIGGER cabinet_service_stock_set_stamp_trg ON cabinet.service_stock;

CREATE TRIGGER cabinet_service_stock_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.service_stock
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

