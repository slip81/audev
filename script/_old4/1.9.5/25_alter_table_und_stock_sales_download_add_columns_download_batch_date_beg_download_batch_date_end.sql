﻿/*
	ALTER TABLE und.stock_sales_download ADD COLUMNs download_batch_date_beg, download_batch_date_end
*/
 
ALTER TABLE und.stock_sales_download ADD COLUMN download_batch_date_beg timestamp without time zone;
ALTER TABLE und.stock_sales_download ADD COLUMN download_batch_date_end timestamp without time zone;