﻿/*
	INSERT INTO cabinet.prj_file
*/

INSERT INTO cabinet.prj_file (  
  file_name,
  file_link,
  file_path,
  claim_id,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
SELECT
  t1.file_name,
  t1.file_path,
  t1.physical_path,
  t1.task_id,
  t1.crt_date,
  t11.user_name,
  null,
  null,  
  1
FROM cabinet.crm_task_file t1
LEFT JOIN cabinet.cab_user t11 on t1.owner_user_id = t11.user_id;

-- select * from cabinet.crm_task_file order by file_id limit 100
-- select * from cabinet.prj_file order by file_id desc limit 100
