﻿/*
	CREATE TABLE cabinet.prj_file
*/

-- DROP TABLE cabinet.prj_file;

CREATE TABLE cabinet.prj_file
(
  file_id serial NOT NULL,
  file_name character varying,
  file_link character varying,
  file_path character varying,  
  prj_id integer,
  claim_id integer,
  task_id integer,
  work_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_file_pkey PRIMARY KEY (file_id),
  CONSTRAINT prj_file_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_file_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_file_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.prj_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_file_work_id_fkey FOREIGN KEY (work_id)
      REFERENCES cabinet.prj_work (work_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_file OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_file_set_stamp_trg ON cabinet.prj_file;

CREATE TRIGGER cabinet_prj_file_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_file
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

