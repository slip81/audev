﻿/*
	insert into cabinet.auth_section 15
*/

insert into cabinet.auth_section (section_id, section_name, group_id)
values (15, 'Сводное наличие', null);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (38, 'Просмотр наличия', 15, null);

insert into cabinet.auth_section_part (part_id, part_name, section_id, group_id)
values (39, 'Просмотр журнала', 15, null);

-- select * from cabinet.auth_section order by section_id
-- select * from cabinet.auth_section_part order by section_id, part_id