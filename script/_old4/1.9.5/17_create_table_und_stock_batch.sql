﻿/*
	CREATE TABLE und.stock_batch
*/

-- DROP TABLE und.stock_batch;

CREATE TABLE und.stock_batch
(
  batch_id serial NOT NULL,
  stock_id integer NOT NULL,
  batch_num integer NOT NULL,
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  is_active boolean NOT NULL DEFAULT false,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  CONSTRAINT stock_batch_pkey PRIMARY KEY (batch_id),
  CONSTRAINT stock_batch_stock_id_fkey FOREIGN KEY (stock_id)
      REFERENCES und.stock (stock_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_batch OWNER TO pavlov;

-- DROP TRIGGER und_stock_batch_set_stamp_trg ON und.stock_batch;

CREATE TRIGGER und_stock_batch_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_batch
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

