﻿/*
	ALTER TABLE cabinet.prj_claim ADD COLUMNs is_verified, verified_date
*/

ALTER TABLE cabinet.prj_claim ADD COLUMN is_verified boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.prj_claim ADD COLUMN verified_date timestamp without time zone;
