﻿/*
	CREATE OR REPLACE FUNCTION und.sysuidgen_scope6
*/

-- DROP FUNCTION und.sysuidgen_scope6();

CREATE OR REPLACE FUNCTION und.sysuidgen_scope6(OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
    result := (SELECT ((FLOOR(EXTRACT(EPOCH FROM clock_timestamp()) * 1000)  - 1314220021721)::bigint << 23) | (6 << 10) | (nextval('und.sys_uid_sequence') % 1024));
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION und.sysuidgen_scope6() OWNER TO pavlov;
