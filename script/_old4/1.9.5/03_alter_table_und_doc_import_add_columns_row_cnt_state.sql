﻿/*
	ALTER TABLE und.doc_import ADD COLUMNs row_cnt, state
*/

ALTER TABLE und.doc_import ADD COLUMN row_cnt integer;
ALTER TABLE und.doc_import ADD COLUMN state integer NOT NULL DEFAULT 0;
 