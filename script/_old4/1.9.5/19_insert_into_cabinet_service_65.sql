﻿/*
	insert into cabinet.service
*/

insert into cabinet.service (id, guid, name, description, parent_id, is_deleted, exe_name, is_site, price, need_key, "NeedCheckVersionLicense", is_service, priority, have_spec, group_id)
values (65, 'b4ed8ef1-b4f7-4e46-a5d3-4d1f9c0adddc', 'Сводное наличие', 'Сводное наличие', 12, 0, 'svnal', 0, 0, 0, 0, true, 34, true, 3);

insert into cabinet.version (guid, name, description, service_id, is_deleted, file_size)
values ('d4bc4635-278c-491f-840a-0e3d43f7bca8', '1.0.0.0', '1.0.0.0', 65, 0, 0);

-- select * from cabinet.service order by id desc
-- select * from cabinet.version order by service_id desc, id desc