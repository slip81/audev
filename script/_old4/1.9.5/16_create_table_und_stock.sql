﻿/*
	CREATE TABLE und.stock
*/

-- DROP TABLE und.stock;

CREATE TABLE und.stock
(
  stock_id serial NOT NULL,
  client_id integer NOT NULL,
  sales_id integer NOT NULL,
  depart_id integer NOT NULL,
  depart_name character varying,
  pharmacy_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  CONSTRAINT stock_pkey PRIMARY KEY (stock_id),
  CONSTRAINT stock_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT stock_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock OWNER TO pavlov;

-- DROP TRIGGER und_stock_set_stamp_trg ON und.stock;

CREATE TRIGGER und_stock_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

