﻿/*
	CREATE TABLE und.stock_row
*/

-- DROP TABLE und.stock_row;

CREATE TABLE und.stock_row
(
  row_id serial NOT NULL,  
  stock_id integer NOT NULL,
  batch_id integer,
  artikul integer NOT NULL,
  row_stock_id integer NOT NULL,
  esn_id integer,
  prep_id integer,
  prep_name character varying,
  firm_id integer,
  firm_name character varying,
  country_name character varying,
  unpacked_cnt integer,
  all_cnt integer,
  price numeric,
  valid_date date,
  series character varying,
  is_vital boolean NOT NULL DEFAULT false,
  barcode character varying,
  price_firm numeric,
  price_gr numeric,
  percent_gross numeric,
  price_gross numeric,
  sum_gross numeric,
  percent_nds_gross numeric,
  price_nds_gross numeric,
  sum_nds_gross numeric,
  percent numeric,
  sum numeric,
  supplier_name character varying,
  supplier_doc_num character varying,
  gr_date date,
  farm_group_name character varying,
  supplier_doc_date date,
  profit numeric,
  mess character varying,  
  state smallint NOT NULL DEFAULT 0,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  CONSTRAINT stock_row_pkey PRIMARY KEY (row_id),
  CONSTRAINT stock_row_stock_id_fkey FOREIGN KEY (stock_id)
      REFERENCES und.stock (stock_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT stock_row_batch_id_fkey FOREIGN KEY (batch_id)
      REFERENCES und.stock_batch (batch_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_row OWNER TO pavlov;

-- DROP TRIGGER und_stock_row_set_stamp_trg ON und.stock_row;

CREATE TRIGGER und_stock_row_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_row
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

