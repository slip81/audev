﻿/*
	CREATE OR REPLACE VIEW und.vw_doc_import
*/

-- DROP VIEW und.vw_doc_import;

CREATE OR REPLACE VIEW und.vw_doc_import AS 
 SELECT t1.import_id,
  t1.config_type_id,
  t1.file_path,
  t1.file_name,
  t1.doc_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.source_partner_id,
  t1.row_cnt,
  t1.state,
  coalesce(t11.row_done_cnt, 0) as row_done_cnt,
  t12.doc_name
 FROM und.doc_import t1
 LEFT JOIN LATERAL (
	SELECT x1.import_id, count(x1.row_id) as row_done_cnt
	FROM und.doc_import_row x1	
	WHERE x1.import_id = t1.import_id
	GROUP BY x1.import_id
 ) t11 ON t1.import_id = t11.import_id
 LEFT JOIN und.doc t12 ON t1.doc_id = t12.doc_id
 ;

ALTER TABLE und.vw_doc_import OWNER TO pavlov;
