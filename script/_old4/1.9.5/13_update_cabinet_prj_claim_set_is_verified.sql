﻿/*
	UPDATE cabinet.prj_claim SET is_verified
*/

delete from cabinet.prj_task_state_type where state_type_id = 5;

UPDATE cabinet.prj_claim SET is_verified = true
WHERE EXISTS (SELECT x1.claim_id FROM cabinet.prj_task x1 WHERE cabinet.prj_claim.claim_id = x1.claim_id)
AND NOT EXISTS (SELECT x1.claim_id FROM cabinet.prj_task x1 WHERE cabinet.prj_claim.claim_id = x1.claim_id and x1.is_verified = false)
;

UPDATE cabinet.prj_claim SET verified_date = current_timestamp
WHERE is_verified = true AND verified_date IS NULL
;

ALTER TABLE cabinet.prj_task DROP COLUMN is_verified;

--select * from cabinet.prj_task_state_type;