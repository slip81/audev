﻿/*
	RECREATE VIEWs cabinet.vw_prj_xxx
*/

---------------------------------------------

DROP VIEW cabinet.vw_prj_plan;
DROP VIEW cabinet.vw_prj_work;
DROP VIEW cabinet.vw_prj_task;
DROP VIEW cabinet.vw_prj_claim;
DROP VIEW cabinet.vw_prj;
DROP VIEW cabinet.vw_prj_claim_simple;
DROP VIEW cabinet.vw_prj_task_simple;

---------------------------------------------
-- DROP VIEW cabinet.vw_prj_task_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_task_simple AS 
 SELECT t1.task_id,
    t1.claim_id,
        CASE
            WHEN COALESCE(t11.work_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND COALESCE(t11.active_work_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND t11.work_cnt = t11.done_work_cnt THEN 3
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND COALESCE(t11.active_work_cnt, 0::bigint) <= 0 AND COALESCE(t11.done_work_cnt, 0::bigint) > 0 AND COALESCE(t11.canceled_work_cnt, 0::bigint) > 0 THEN 3
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND t11.work_cnt = t11.canceled_work_cnt THEN 4            
            ELSE 2
        END AS task_state_type_id,
    t11.work_cnt,
    t11.active_work_cnt,
    t11.done_work_cnt,
    t11.canceled_work_cnt
   FROM cabinet.prj_task t1
     LEFT JOIN ( SELECT count(x1.work_id) AS work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 0 THEN 1
                    ELSE 0
                END) AS active_work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 1 THEN 1
                    ELSE 0
                END) AS done_work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 2 THEN 1
                    ELSE 0
                END) AS canceled_work_cnt,
            x1.task_id
           FROM cabinet.vw_prj_work_simple x1
          GROUP BY x1.task_id) t11 ON t1.task_id = t11.task_id;

ALTER TABLE cabinet.vw_prj_task_simple OWNER TO pavlov;
---------------------------------------------

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple AS 
 SELECT t1.claim_id,
    t1.prj_id,
        CASE
            WHEN COALESCE(t11.task_cnt, 0::bigint) <= 0 AND t1.is_verified = false THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) > 0 AND t1.is_verified = false THEN 2
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.done_task_cnt AND t1.is_verified = false THEN 3
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND t11.task_cnt <> t11.canceled_task_cnt AND t11.task_cnt <> t11.done_task_cnt AND t1.is_verified = false THEN 3
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.canceled_task_cnt AND t1.is_verified = false THEN 4
            WHEN t1.is_verified = true THEN 5            
            ELSE 2
        END AS claim_state_type_id,
    t11.task_cnt,
    t11.new_task_cnt,
    t11.active_task_cnt,
    t11.done_task_cnt,
    t11.canceled_task_cnt
   FROM cabinet.prj_claim t1
     LEFT JOIN ( SELECT x1.claim_id,
            count(x1.task_id) AS task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 1 THEN 1
                    ELSE 0
                END) AS new_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 2 THEN 1
                    ELSE 0
                END) AS active_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 3 THEN 1
                    ELSE 0
                END) AS done_task_cnt,
            sum(
                CASE
                    WHEN x1.task_state_type_id = 4 THEN 1
                    ELSE 0
                END) AS canceled_task_cnt
           FROM cabinet.vw_prj_task_simple x1
          GROUP BY x1.claim_id) t11 ON t1.claim_id = t11.claim_id;

ALTER TABLE cabinet.vw_prj_claim_simple OWNER TO pavlov;

---------------------------------------------

CREATE OR REPLACE VIEW cabinet.vw_prj AS 
 SELECT t1.prj_id,
    t1.prj_name,
    t1.date_beg,
    t1.date_end,
    t1.curr_state_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    COALESCE(t11.claim_cnt, 0::bigint) AS claim_cnt,
    COALESCE(t11.new_claim_cnt, 0::bigint) AS new_claim_cnt,
    COALESCE(t11.active_claim_cnt, 0::bigint) AS active_claim_cnt,
    COALESCE(t11.done_claim_cnt, 0::bigint) AS done_claim_cnt,
    COALESCE(t11.canceled_claim_cnt, 0::bigint) AS canceled_claim_cnt,
    COALESCE(t11.verified_claim_cnt, 0::bigint) AS verified_claim_cnt,
    t12.state_type_id,
    t13.state_type_name,
    t13.templ AS state_type_name_templ,
    COALESCE(t14.claim_approved_cnt, 0::bigint) AS claim_approved_cnt,
    COALESCE(t15.claim_new_cnt, 0::bigint) AS claim_new_cnt
   FROM cabinet.prj t1
     LEFT JOIN ( SELECT x1.prj_id,
            count(x1.claim_id) AS claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 1 THEN 1
                    ELSE 0
                END) AS new_claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 2 THEN 1
                    ELSE 0
                END) AS active_claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 3 THEN 1
                    ELSE 0
                END) AS done_claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 4 THEN 1
                    ELSE 0
                END) AS canceled_claim_cnt,
            sum(
                CASE
                    WHEN x1.claim_state_type_id = 5 THEN 1
                    ELSE 0
                END) AS verified_claim_cnt
           FROM cabinet.vw_prj_claim_simple x1
          GROUP BY x1.prj_id) t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_state t12 ON t1.curr_state_id = t12.state_id
     LEFT JOIN cabinet.prj_state_type t13 ON t12.state_type_id = t13.state_type_id
     LEFT JOIN ( SELECT count(x1.claim_id) AS claim_approved_cnt,
            x1.prj_id
           FROM cabinet.prj_set_approved x1
          GROUP BY x1.prj_id) t14 ON t1.prj_id = t14.prj_id
     LEFT JOIN ( SELECT count(x1.claim_id) AS claim_new_cnt,
            x1.prj_id
           FROM cabinet.prj_set_new x1
          GROUP BY x1.prj_id) t15 ON t1.prj_id = t15.prj_id;

ALTER TABLE cabinet.vw_prj OWNER TO pavlov;
---------------------------------------------

CREATE OR REPLACE VIEW cabinet.vw_prj_claim AS 
 SELECT t1.claim_id,
    t1.claim_num,
    t1.claim_name,
    t1.claim_text,
    t1.prj_id,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.module_version_id,
    t1.client_id,
    t1.priority_id,
    t1.resp_user_id,
    t1.date_plan,
    t1.date_fact,
    t1.repair_version_id,
    t1.is_archive,
    t1.rate,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.is_control,
    t1.old_task_id,
    t2.project_name,
    t3.module_name,
    t4.module_part_name,
    t5.module_version_name,
    t6.client_name,
    t7.priority_name,
    t8.user_name AS resp_user_name,
    t11.module_version_name AS repair_version_name,
    COALESCE(t12.task_cnt, 0::bigint) AS task_cnt,
    COALESCE(t12.new_task_cnt, 0::bigint) AS new_task_cnt,
    COALESCE(t12.active_task_cnt, 0::bigint) AS active_task_cnt,
    COALESCE(t12.done_task_cnt, 0::bigint) AS done_task_cnt,
    COALESCE(t12.canceled_task_cnt, 0::bigint) AS canceled_task_cnt,    
    t12.claim_state_type_id,
    t13.state_type_name AS claim_state_type_name,
    t13.templ AS claim_state_type_templ,
    t9.prj_name,
    t9.date_beg AS prj_date_beg,
    t9.date_end AS prj_date_end,
    t9.curr_state_id AS prj_curr_state_id,
    t9.state_type_id AS prj_state_type_id,
    t9.state_type_name AS prj_state_type_name,
    t1.old_group_id,
    t1.old_state_id,
    t14.group_name AS old_group_name,
    t15.state_name AS old_state_name,
    t1.is_verified,
    t1.verified_date
   FROM cabinet.prj_claim t1
     JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id
     JOIN cabinet.crm_client t6 ON t1.client_id = t6.client_id
     JOIN cabinet.crm_priority t7 ON t1.priority_id = t7.priority_id
     JOIN cabinet.cab_user t8 ON t1.resp_user_id = t8.user_id
     JOIN cabinet.vw_prj t9 ON t1.prj_id = t9.prj_id
     LEFT JOIN cabinet.crm_module_version t11 ON t1.repair_version_id = t11.module_version_id
     LEFT JOIN cabinet.vw_prj_claim_simple t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_claim_state_type t13 ON t12.claim_state_type_id = t13.state_type_id
     LEFT JOIN cabinet.crm_group t14 ON t1.old_group_id = t14.group_id
     LEFT JOIN cabinet.crm_state t15 ON t1.old_state_id = t15.state_id;

ALTER TABLE cabinet.vw_prj_claim OWNER TO pavlov;

---------------------------------------------

CREATE OR REPLACE VIEW cabinet.vw_prj_task AS 
 SELECT t1.task_id,
    t1.claim_id,
    t1.task_num,
    t1.task_text,
    t1.date_plan,
    t1.date_fact,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.claim_num,
    t2.claim_name,
    t2.claim_text,
    t2.prj_id AS claim_prj_id,
    t2.project_id AS claim_project_id,
    t2.module_id AS claim_module_id,
    t2.module_part_id AS claim_module_part_id,
    t2.module_version_id AS claim_module_version_id,
    t2.client_id AS claim_client_id,
    t2.priority_id AS claim_priority_id,
    t2.resp_user_id AS claim_resp_user_id,
    t2.date_plan AS claim_date_plan,
    t2.date_fact AS claim_date_fact,
    t2.repair_version_id AS claim_repair_version_id,
    t2.is_archive AS claim_is_archive,
    t2.rate AS claim_rate,
    t2.is_control AS claim_is_control,
    t2.project_name AS claim_project_name,
    t2.module_name AS claim_module_name,
    t2.module_part_name AS claim_module_part_name,
    t2.module_version_name AS claim_module_version_name,
    t2.client_name AS claim_client_name,
    t2.priority_name AS claim_priority_name,
    t2.resp_user_name AS claim_resp_user_name,
    t2.repair_version_name AS claim_repair_version_name,
    t2.claim_state_type_id,
    t2.claim_state_type_name,
    t2.claim_state_type_templ,
    t2.prj_name,
    t2.prj_date_beg,
    t2.prj_date_end,
    t2.prj_curr_state_id,
    t2.prj_state_type_id,
    t2.prj_state_type_name,
    t11.task_state_type_id,
    COALESCE(t11.work_cnt, 0::bigint) AS work_cnt,
    COALESCE(t11.active_work_cnt, 0::bigint) AS active_work_cnt,
    COALESCE(t11.done_work_cnt, 0::bigint) AS done_work_cnt,
    COALESCE(t11.canceled_work_cnt, 0::bigint) AS canceled_work_cnt,
    t12.state_type_name AS task_state_type_name,
    t12.templ AS task_state_type_templ,
    t1.old_state_id,
    t13.state_name AS old_state_name
   FROM cabinet.prj_task t1
     JOIN cabinet.vw_prj_claim t2 ON t1.claim_id = t2.claim_id
     LEFT JOIN cabinet.vw_prj_task_simple t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.prj_task_state_type t12 ON t11.task_state_type_id = t12.state_type_id
     LEFT JOIN cabinet.crm_state t13 ON t1.old_state_id = t13.state_id;

ALTER TABLE cabinet.vw_prj_task OWNER TO pavlov;

---------------------------------------------

CREATE OR REPLACE VIEW cabinet.vw_prj_work AS 
 SELECT t1.work_id,
    t1.task_id,
    t1.claim_id,
    t1.work_num,
    t1.work_type_id,
    t1.exec_user_id,
    t1.state_type_id,
    t1.is_plan,
    t1.date_beg,
    t1.date_end,
    t1.is_accept,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.cost_plan,
    t1.cost_fact,
    t1.mess,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.work_type_name,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    t4.templ AS state_type_templ,
    t5.task_num,
    t5.task_text,
    t5.task_state_type_id,
    t5.date_plan AS task_date_plan,
    t5.date_fact AS task_date_fact,
    t5.task_state_type_name,
    t5.task_state_type_templ,
    t5.claim_num,
    t5.claim_name,
    t5.claim_text,
    t5.claim_prj_id,
    t5.claim_project_id,
    t5.claim_module_id,
    t5.claim_module_part_id,
    t5.claim_module_version_id,
    t5.claim_client_id,
    t5.claim_priority_id,
    t5.claim_resp_user_id,
    t5.claim_date_plan,
    t5.claim_date_fact,
    t5.claim_repair_version_id,
    t5.claim_is_archive,
    t5.claim_rate,
    t5.claim_is_control,
    t5.claim_project_name,
    t5.claim_module_name,
    t5.claim_module_part_name,
    t5.claim_module_version_name,
    t5.claim_client_name,
    t5.claim_priority_name,
    t5.claim_resp_user_name,
    t5.claim_repair_version_name,
    t5.claim_state_type_id,
    t5.claim_state_type_name,
    t5.claim_state_type_templ,
    t5.prj_name,
    t5.prj_date_beg,
    t5.prj_date_end,
    t5.prj_curr_state_id,
    t5.prj_state_type_id,
    t5.prj_state_type_name
   FROM cabinet.prj_work t1
     JOIN cabinet.prj_work_type t2 ON t1.work_type_id = t2.work_type_id
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id
     JOIN cabinet.vw_prj_task t5 ON t1.task_id = t5.task_id;

ALTER TABLE cabinet.vw_prj_work OWNER TO pavlov;

---------------------------------------------

CREATE OR REPLACE VIEW cabinet.vw_prj_plan AS 
 SELECT t1.item_id AS plan_id,
    t1.work_id,
    t1.exec_user_id,
    t1.state_type_id,
    t1.is_plan,
    t1.date_beg,
    t1.date_end,
    t1.date_time_beg,
    t1.date_time_end,
    t1.is_all_day,
    t1.cost_plan,
    t1.cost_fact,
    t1.mess,
    t1.is_current,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t2.claim_prj_id AS prj_id,
    t2.prj_name,
    t2.task_id,
    t2.task_num,
    t2.task_text,
    t2.claim_id,
    t2.claim_num,
    t2.claim_name,
    t2.work_num,
    t2.work_type_id,
    t2.work_type_name,
    t2.is_accept,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    NULL::text AS progress,
    NULL::text AS result,
    true AS is_public,
    (((t2.claim_num::text || '-'::text) || t2.task_num::text) || '-'::text) || "left"(t2.work_type_name::text, 1) AS plan_num
   FROM cabinet.prj_work_exec t1
     JOIN cabinet.vw_prj_work t2 ON t1.work_id = t2.work_id
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id
UNION ALL
 SELECT t1.event_id AS plan_id,
    0 AS work_id,
    t1.exec_user_id,
    t1.state_type_id,
    true AS is_plan,
    t1.date_beg,
    t1.date_end,
    t1.date_beg AS date_time_beg,
    t1.date_end AS date_time_end,
    t1.is_all_day,
    NULL::numeric AS cost_plan,
    NULL::numeric AS cost_fact,
    NULL::character varying AS mess,
    true AS is_current,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    (-1) AS prj_id,
    NULL::character varying AS prj_name,
    0 AS task_id,
    0 AS task_num,
    t1.title AS task_text,
    0 AS claim_id,
    'С-'::text || t1.event_id::text AS claim_num,
    t1.description AS claim_name,
    0 AS work_num,
    0 AS work_type_id,
    NULL::character varying AS work_type_name,
    true AS is_accept,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    t1.progress,
    t1.result,
    t1.is_public,
    'С-'::text || t1.event_id::text AS plan_num
   FROM cabinet.prj_event t1
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id;

ALTER TABLE cabinet.vw_prj_plan OWNER TO pavlov;