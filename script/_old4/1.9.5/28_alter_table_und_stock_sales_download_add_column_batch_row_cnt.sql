﻿/*
	ALTER TABLE und.stock_sales_download ADD COLUMN batch_row_cnt
*/

ALTER TABLE und.stock_sales_download ADD COLUMN batch_row_cnt integer NOT NULL DEFAULT 100;
