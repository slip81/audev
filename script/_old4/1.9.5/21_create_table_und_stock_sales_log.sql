﻿/*
	CREATE TABLE und.stock_sales_log
*/

-- DROP TABLE und.stock_sales_log;

CREATE TABLE und.stock_sales_log
(
  log_id serial NOT NULL,
  stock_id integer,
  sales_id integer,
  log_type integer NOT NULL DEFAULT 0,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  CONSTRAINT stock_sales_log_pkey PRIMARY KEY (log_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_sales_log OWNER TO pavlov;

-- DROP TRIGGER und_stock_sales_log_set_stamp_trg ON und.stock_sales_log;

CREATE TRIGGER und_stock_sales_log_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_sales_log
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
