﻿/*
	CREATE TABLE und.stock_sales_download
*/

-- DROP TABLE und.stock_sales_download;

CREATE TABLE und.stock_sales_download
(
  download_id serial NOT NULL,
  sales_id integer NOT NULL,  
  part_cnt integer,
  part_num integer,
  row_cnt integer,
  is_active boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  CONSTRAINT stock_download_pkey PRIMARY KEY (download_id),
  CONSTRAINT stock_sales_download_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.stock_sales_download OWNER TO pavlov;

-- DROP TRIGGER und_stock_sales_download_set_stamp_trg ON und.stock_sales_download;

CREATE TRIGGER und_stock_sales_download_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.stock_sales_download
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

