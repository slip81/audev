﻿/*
	UPDATE cabinet.cab_grid_user_settings SET grid_options
*/

UPDATE cabinet.cab_grid_user_settings
SET grid_options = replace(grid_options, ', принято: #:verified_task_cnt#', '');

UPDATE cabinet.cab_grid_user_settings
SET grid_options = replace(grid_options, ' <span class=''text-info''>#:verified_task_cnt#</span>', '');


--, принято: #:verified_task_cnt#
-- <span class='text-info'>#:verified_task_cnt#</span>
-- select * from cabinet.cab_grid_user_settings where grid_options like '%verif%'
