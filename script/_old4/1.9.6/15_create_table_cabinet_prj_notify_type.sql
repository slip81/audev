﻿/*
	CREATE TABLE cabinet.prj_notify_type
*/

-- DROP TABLE cabinet.prj_notify_type;

CREATE TABLE cabinet.prj_notify_type
(
  notify_type_id integer NOT NULL,
  notify_type_name character varying,
  minute_count integer,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_notify_type_pkey PRIMARY KEY (notify_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_notify_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_notify_type_set_stamp_trg ON cabinet.prj_notify_type;

CREATE TRIGGER cabinet_prj_notify_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_notify_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

-----------------------

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (0, 'не уведомлять', null);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (1, '0 минут', 0);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (2, '5 минут', 5);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (3, '15 минут', 15);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (4, '30 минут', 30);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (5, '1 час', 60);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (6, '2 часа', 120);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (7, '12 часов', 720);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (8, '1 день', 1440);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (9, '2 дня', 2880);

INSERT INTO cabinet.prj_notify_type (notify_type_id, notify_type_name, minute_count)
VALUES (10, '1 неделя', 10080);

-----------------------

ALTER TABLE cabinet.prj_event ADD COLUMN notify_type_id integer NOT NULL DEFAULT 0;

ALTER TABLE cabinet.prj_event
  ADD CONSTRAINT prj_event_notify_type_id_fkey FOREIGN KEY (notify_type_id)
      REFERENCES cabinet.prj_notify_type (notify_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

-----------------------

ALTER TABLE cabinet.prj_event ADD COLUMN notify_sent boolean NOT NULL DEFAULT false;

-----------------------