﻿/*
	CREATE OR REPLACE VIEW und.vw_stock
*/

-- DROP VIEW und.vw_stock;

CREATE OR REPLACE VIEW und.vw_stock AS 
 SELECT t1.stock_id,
    t1.client_id,
    t1.sales_id,
    t1.depart_id,
    t1.depart_name,
    t1.pharmacy_name,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.name AS client_name,
    t3.adress AS sales_name,
    t1.depart_address
   FROM und.stock t1
     JOIN cabinet.client t2 ON t1.client_id = t2.id
     JOIN cabinet.sales t3 ON t1.sales_id = t3.id;

ALTER TABLE und.vw_stock OWNER TO pavlov;
