﻿/*
	CREATE OR REPLACE VIEW und.vw_product
*/

-- DROP VIEW und.vw_product;

CREATE OR REPLACE VIEW und.vw_product AS 
 SELECT t1.product_id,
    t1.product_form_id,
    t1.pharm_market_id,
    t1.inn_id,
    t1.dosage_id,
    t1.brand_id,
    t1.space_id,
    t1.metaname_id,
    t1.product_name,
    t1.trade_name,
    t1.trade_name_lat,
    t1.trade_name_eng,
    t1.storage_period,
    t1.is_vital,
    t1.is_powerful,
    t1.is_poison,
    t1.is_receipt,
    t1.drug,
    t1.size,
    t1.potion_amount,
    t1.pack_count,
    t1.link,
    t1.mess,
    t1.doc_id,
    t1.doc_row_id,
    t1.is_deleted,
    COALESCE(t11.good_cnt, 0::bigint) AS good_cnt,
    t1.apply_group_id,
    t12.apply_group_name
   FROM und.product t1
     LEFT JOIN ( SELECT x1.product_id,
            count(x1.item_id) AS good_cnt
           FROM und.vw_partner_good x1
          GROUP BY x1.product_id) t11 ON t1.product_id = t11.product_id
     LEFT JOIN und.apply_group t12 ON t1.apply_group_id = t12.apply_group_id;

ALTER TABLE und.vw_product OWNER TO pavlov;
