﻿/*
	ALTER TABLEs und.doc_import_config_type_xxx ADD COLUMN apply_group_col
*/

ALTER TABLE und.doc_import_config_type_excel ADD COLUMN apply_group_col integer;
UPDATE und.doc_import_config_type_excel SET apply_group_col = -1;

-- !!!
UPDATE und.doc_import_config_type_excel SET apply_group_col = 5 where config_type_id = 6;

ALTER TABLE und.doc_import_config_type_xml ADD COLUMN apply_group_element character varying;

-- select * from und.doc_import_config_type_excel
-- select * from und.doc_import_config_type_xml