﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_event_notify
*/

-- DROP VIEW cabinet.vw_prj_event_notify;

CREATE OR REPLACE VIEW cabinet.vw_prj_event_notify AS 
 SELECT row_number() over(order by null) as item_id,
   t1.event_id,
   t1.title,
   t1.description,
   t1.date_beg,
   t1.date_end,
   t1.is_all_day,
   t1.exec_user_id,
   t1.notify_type_id,
   t1.notify_sent,
   t2.notify_type_name,
   t2.minute_count,
   t11.user_id as party_user_id   
   FROM cabinet.prj_event t1
   INNER JOIN cabinet.prj_notify_type t2 ON t1.notify_type_id = t2.notify_type_id
   LEFT JOIN cabinet.prj_event_party t11 ON t1.event_id = t11.event_id
   WHERE t1.notify_type_id > 0
   ;

ALTER TABLE cabinet.vw_prj_event_notify OWNER TO pavlov;
