﻿/*
	CREATE OR REPLACE VIEW und.vw_product_and_good
*/

-- DROP VIEW und.vw_product_and_good;

CREATE OR REPLACE VIEW und.vw_product_and_good AS 
 SELECT row_number() OVER (ORDER BY NULL::text) AS item_id,
    t1.product_id,
    t1.product_name,
    t1.doc_id,
    t1.doc_row_id,
    t1.is_deleted,
    count(t11.*) OVER (PARTITION BY t1.product_id) AS good_cnt,
    t11.partner_id,
    t11.good_id,
    t11.partner_code,
    t11.partner_name,
    t11.source_partner_name,
    t11.mfr_id,
    t11.barcode,
    t11.doc_id AS good_doc_id,
    t11.doc_row_id AS good_doc_row_id,
    t11.mfr_name,
    t11.is_deleted AS good_is_deleted,
    t1.apply_group_id,
    t12.apply_group_name
   FROM und.product t1
     LEFT JOIN und.vw_partner_good t11 ON t1.product_id = t11.product_id
     LEFT JOIN und.apply_group t12 ON t1.apply_group_id = t12.apply_group_id;

ALTER TABLE und.vw_product_and_good OWNER TO pavlov;
