﻿/*
	update cabinet.cab_grid_user_settings active_work_type_name
*/

-- !!!

/*
update cabinet.cab_grid_user_settings
set grid_options = grid_options
where grid_id = 4;

{"encoded":true,"title":"Статус","width":"120px","template":"#=claim_state_type_templ#","field":"claim_state_type_name","filterable":{"messages":{"info":"Строки со значениями","isTrue":"истина","isFalse":"ложь","filter":"фильтровать","clear":"очистить фильтр","and":"и","or":"или","selectValue":"-выберите-","operator":"Оператор","value":"Значение","cancel":"Отмена"},"operators":{"string":{"eq":"равно","neq":"не равно","startswith":"начинающимися на","endswith":"оканчивается на","contains":"содержащими","doesnotcontain":"не содержит"},"number":{"eq":"равно","neq":"не равно","gte":"больше или равно","gt":"больше","lte":"меньше или равно","lt":"меньше"},"date":{"eq":"равна","neq":"не равна","gte":"после или равна","gt":"после","lte":"до или равна","lt":"до"},"enums":{"eq":"равно","neq":"не равно"}}},"headerAttributes":{"id":"d2b409f9-3b8e-4fa1-bb26-6f1073a211b2"}},
{"encoded":true,"title":"Стадия","width":142,"field":"active_work_type_name","filterable":{"messages":{"info":"Строки со значениями","isTrue":"истина","isFalse":"ложь","filter":"фильтровать","clear":"очистить фильтр","and":"и","or":"или","selectValue":"-выберите-","operator":"Оператор","value":"Значение","cancel":"Отмена"},"operators":{"string":{"eq":"равно","neq":"не равно","startswith":"начинающимися на","endswith":"оканчивается на","contains":"содержащими","doesnotcontain":"не содержит"},"number":{"eq":"равно","neq":"не равно","gte":"больше или равно","gt":"больше","lte":"меньше или равно","lt":"меньше"},"date":{"eq":"равна","neq":"не равна","gte":"после или равна","gt":"после","lte":"до или равна","lt":"до"},"enums":{"eq":"равно","neq":"не равно"}}},"headerAttributes":{"id":"047e0325-d875-48bf-99e2-d913cb143f1a"},"locked":false},
*/

-- select * from cabinet.cab_grid_user_settings order by grid_id

-- delete from cabinet.cab_grid_user_settings where grid_id = 4;
