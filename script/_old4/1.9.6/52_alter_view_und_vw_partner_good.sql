﻿/*
	CREATE OR REPLACE VIEW und.vw_partner_good
*/

-- DROP VIEW und.vw_partner_good;

CREATE OR REPLACE VIEW und.vw_partner_good AS 
 SELECT t1.item_id,
    t1.partner_id,
    t1.good_id,
    t1.partner_code,
    t1.partner_name,
    t2.partner_name AS source_partner_name,
    t3.product_id,
    t3.mfr_id,
    t3.barcode,
    t3.doc_id,
    t3.doc_row_id,
    t4.product_name,
    t5.mfr_name,
    t3.is_deleted,
    t4.apply_group_id,
    t11.apply_group_name,
    t3.is_main
   FROM und.good t3
   JOIN und.manufacturer t5 ON t3.mfr_id = t5.mfr_id
   JOIN und.partner_good t1 ON t1.good_id = t3.good_id
   JOIN und.partner t2 ON t1.partner_id = t2.partner_id
   JOIN und.product t4 ON t3.product_id = t4.product_id
   LEFT JOIN und.apply_group t11 ON t4.apply_group_id = t11.apply_group_id;

ALTER TABLE und.vw_partner_good OWNER TO pavlov;
