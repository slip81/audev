﻿/*
	CREATE TABLE cabinet.prj_notify
*/

-- DROP TABLE cabinet.prj_notify;

CREATE TABLE cabinet.prj_notify
(
  notify_id serial NOT NULL,
  target_user_id integer,
  event_id integer,
  claim_id integer,  
  title character varying,
  message character varying,  
  need_send boolean NOT NULL DEFAULT true,
  send_date timestamp without time zone,
  sent_cnt integer NOT NULL DEFAULT 0,  
  sent_date timestamp without time zone,
  crt_date timestamp without time zone,
  crt_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_notify_pkey PRIMARY KEY (notify_id),
  CONSTRAINT prj_notify_target_user_id_fkey FOREIGN KEY (target_user_id)  
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_notify_event_id_fkey FOREIGN KEY (event_id)
      REFERENCES cabinet.prj_event (event_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_notify_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_notify OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_notify_set_stamp_trg ON cabinet.prj_notify;

CREATE TRIGGER cabinet_prj_notify_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_notify
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

