﻿/*
	update cabinet.prj_event set title
*/

update cabinet.prj_event 
set title = 'Событие ' || event_id::text
where title is null;

-- select title, * from cabinet.prj_event where event_id = 1793
-- select title, * from cabinet.prj_event where title is null

