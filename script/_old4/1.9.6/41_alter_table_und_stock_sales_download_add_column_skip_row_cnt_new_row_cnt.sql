﻿/*
	ALTER TABLE und.stock_sales_download ADD COLUMNs skip_row_cnt, new_row_cnt
*/

ALTER TABLE und.stock_sales_download ADD COLUMN skip_row_cnt integer;
ALTER TABLE und.stock_sales_download ADD COLUMN new_row_cnt integer;