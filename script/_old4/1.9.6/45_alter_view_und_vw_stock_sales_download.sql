﻿/*
	CREATE OR REPLACE VIEW und.vw_stock_sales_download
*/

-- DROP VIEW und.vw_stock_sales_download;

CREATE OR REPLACE VIEW und.vw_stock_sales_download AS 
 SELECT t1.download_id,
    t1.sales_id,
    t1.part_cnt,
    t1.part_num,
    t1.row_cnt,
    t1.is_active,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.download_batch_date_beg,
    t1.download_batch_date_end,
    t1.batch_row_cnt,
    t2.adress AS sales_name,
    t3.id AS client_id,
    t3.name AS client_name,
    t1.skip_row_cnt,
    t1.new_row_cnt,
    t1.force_all
   FROM und.stock_sales_download t1
     JOIN cabinet.sales t2 ON t1.sales_id = t2.id
     JOIN cabinet.client t3 ON t2.client_id = t3.id;

ALTER TABLE und.vw_stock_sales_download OWNER TO pavlov;
