﻿/*
	insert into und.partner 0
*/

-- select * from und.partner order by partner_id;

insert into und.partner (partner_id, partner_name, is_source, crt_date, crt_user, upd_date, upd_user, is_deleted, is_supplier)
values (0, '[не определен]', false, current_timestamp, 'pavlov', current_timestamp, 'pavlov', false, true);

update und.partner
set is_supplier = true
where partner_name = 'Аурит';

-- delete from und.partner where partner_id = 0;