﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_update_soft
*/

-- DROP VIEW cabinet.vw_update_soft;

CREATE OR REPLACE VIEW cabinet.vw_update_soft AS 
 SELECT t1.soft_id,
  t1.soft_name,
  t1.service_id,
  t1.items_exists,
  t1.soft_guid,
  t1.group_id,
  t2.group_name
   FROM cabinet.update_soft t1
     INNER JOIN cabinet.update_soft_group t2 ON t1.group_id = t2.group_id;

ALTER TABLE cabinet.vw_update_soft OWNER TO pavlov;
