﻿/*
	insert into und.doc_import_config_type synonim
*/

select * from und.doc_import_config_type

select * from und.doc_import_config_type_excel
--delete from und.doc_import_config_type_excel where config_type_id = 8

insert into und.doc_import_config_type (
  config_type_name,
  source_partner_id,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted,
  file_type,
  is_source_partner_from_file
)
values (
  'Excel-файл синонимы',
  1,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false,
  1,
  false  
);

insert into und.doc_import_config_type_excel (
  config_type_id,
  list_num,
  row_num,
  source_name_col,
  etalon_name_col,
  code_col,
  mfr_col,
  mfr_group_col,
  barcode_col
)
values (
  8,
  0,
  1,
  1,
  1,
  0,
  -1,
  -1,
  -1
);