﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_work_simple
*/

-- DROP VIEW cabinet.vw_prj_work_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_work_simple AS 
 SELECT t1.work_id,
    t1.task_id,
    t1.claim_id,
    t1.state_type_id,
    t2.done_or_canceled,
    t1.work_type_id,
    t1.work_num
   FROM cabinet.prj_work t1
     JOIN cabinet.prj_work_state_type t2 ON t1.state_type_id = t2.state_type_id;

ALTER TABLE cabinet.vw_prj_work_simple OWNER TO pavlov;
