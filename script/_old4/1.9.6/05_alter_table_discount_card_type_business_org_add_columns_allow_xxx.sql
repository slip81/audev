﻿/*
	ALTER TABLE discount.card_type_business_org ADD COLUMNs allow_xxx
*/

ALTER TABLE discount.card_type_business_org ADD COLUMN allow_discount boolean NOT NULL DEFAULT true;
ALTER TABLE discount.card_type_business_org ADD COLUMN allow_bonus_for_pay boolean NOT NULL DEFAULT true;
ALTER TABLE discount.card_type_business_org ADD COLUMN allow_bonus_for_card boolean NOT NULL DEFAULT true;