﻿/*
	update und.good set good_id
*/

ALTER TABLE und.partner_good ADD COLUMN old_good_id integer;

ALTER TABLE und.partner_good DROP CONSTRAINT partner_good_good_id_fkey;

update und.partner_good set old_good_id = good_id;

with partner_good_cte1 as
(
select row_number() over(order by partner_name) as new_good_id, good_id
from und.partner_good
)
update und.partner_good t1
set good_id = t2.new_good_id
from partner_good_cte1 t2
where t1.good_id = t2.good_id;

-- select * from und.good where good_id = 22823
-- select * from und.partner_good where good_id = 22823
-- select * from und.partner_good where old_good_id = 22823

ALTER TABLE und.doc_row DROP CONSTRAINT doc_row_good_id_fkey;

ALTER TABLE und.vital DROP CONSTRAINT vital_good_id_fkey;

ALTER TABLE und.remain DROP CONSTRAINT remain_good_id_fkey;

ALTER TABLE und.doc_import_match DROP CONSTRAINT doc_import_match_match_good_id_fkey;

ALTER TABLE und.doc_import_match_variant DROP CONSTRAINT doc_import_match_variant_match_good_id_fkey;

ALTER TABLE und.good DROP CONSTRAINT good_pkey;

update und.good t1
set good_id = t2.good_id
from und.partner_good t2
where t1.good_id = t2.old_good_id;

ALTER TABLE und.good ADD CONSTRAINT good_pkey PRIMARY KEY(good_id);

ALTER TABLE und.doc_row
  ADD CONSTRAINT doc_row_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE und.vital
  ADD CONSTRAINT vital_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      

ALTER TABLE und.remain
  ADD CONSTRAINT remain_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      

ALTER TABLE und.doc_import_match
  ADD CONSTRAINT doc_import_match_match_good_id_fkey FOREIGN KEY (match_good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;     

ALTER TABLE und.doc_import_match_variant
  ADD CONSTRAINT doc_import_match_variant_match_good_id_fkey FOREIGN KEY (match_good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
  
ALTER TABLE und.partner_good
  ADD CONSTRAINT partner_good_good_id_fkey FOREIGN KEY (good_id)
      REFERENCES und.good (good_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

update und.partner_good set partner_code = good_id::character varying;

-- select * from und.good order by good_id limit 100;
-- select * from und.partner_good order by good_id desc limit 100;