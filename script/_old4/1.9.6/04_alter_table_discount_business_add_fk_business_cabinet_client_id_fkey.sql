﻿/*
	ALTER TABLE discount.business ADD CONSTRAINT business_cabinet_client_id_fkey
*/

ALTER TABLE discount.business
  ADD CONSTRAINT business_cabinet_client_id_fkey FOREIGN KEY (cabinet_client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

      