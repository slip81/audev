﻿/*
	update und.xxx set xxx_name to_lower
*/

update und.product set product_name = upper(left(product_name, 1)) || lower(substring(product_name from 2));
update und.partner_good set partner_name = upper(left(partner_name, 1)) || lower(substring(partner_name from 2));