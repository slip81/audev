﻿/*
	CREATE TABLE und.apply_group
*/

-- DROP TABLE und.apply_group;

CREATE TABLE und.apply_group
(
  apply_group_id integer NOT NULL,
  apply_group_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope6(),
  CONSTRAINT apply_group_pkey PRIMARY KEY (apply_group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.apply_group OWNER TO pavlov;

-- DROP TRIGGER und_apply_group_set_stamp_trg ON und.apply_group;

CREATE TRIGGER und_apply_group_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.apply_group
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

