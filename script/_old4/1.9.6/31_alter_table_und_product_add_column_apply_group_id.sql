﻿/*
	ALTER TABLE und.product ADD COLUMN apply_group_id
*/

ALTER TABLE und.product ADD COLUMN apply_group_id integer;

/*
ALTER TABLE und.product
  ADD CONSTRAINT product_apply_group_id_fkey FOREIGN KEY (apply_group_id)
      REFERENCES und.apply_group (apply_group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
*/

-- ALTER TABLE und.product DROP CONSTRAINT product_apply_group_id_fkey;