﻿/*
	CREATE TABLE cabinet.update_soft_group
*/

-- DROP TABLE cabinet.update_soft_group;

CREATE TABLE cabinet.update_soft_group
(
  group_id serial NOT NULL,
  group_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),  
  CONSTRAINT update_soft_group_pkey PRIMARY KEY (group_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.update_soft_group OWNER TO pavlov;

-- DROP TRIGGER cabinet_update_soft_group_set_stamp_trg ON cabinet.update_soft_group;

CREATE TRIGGER cabinet_update_soft_group_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.update_soft_group
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.update_soft_group (group_id, group_name)
VALUES (1, 'Модули АУ');

INSERT INTO cabinet.update_soft_group (group_id, group_name)
VALUES (2, 'Модули НАП');

ALTER TABLE cabinet.update_soft ADD COLUMN group_id integer NOT NULL DEFAULT 1;

ALTER TABLE cabinet.update_soft
  ADD CONSTRAINT update_soft_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES cabinet.update_soft_group (group_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;