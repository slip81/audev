﻿/*
	manual insert syn
*/

-- select * from und.doc_import_config_type_excel

select * from und.doc_import_row order by code limit 100;

select * from und.doc_import_match order by code limit 100;

select match_good_id, match_product_id, * from und.doc_import_match order by code limit 100;

select * from und.good order by product_id desc limit 100;

select count(*) from und.doc_import_match where code not in
(select partner_code from und.partner_good)
-- 294

select count(*) from und.doc_import_match where match_good_id is null;
-- 294

select count(*) from und.doc_import_match;
-- 55552

select count(*) from und.doc_import_row;
-- 55655

select count(*) from und.product;
-- 40516

select count(*) from und.good;
-- 40516
-- 95774

select count(*) from und.partner_good;
-- 40516
-- 95774

select count(*) from und.vw_partner_good;
-- 95774

select count(*) from und.manufacturer
-- 4812

select count(*) from und.manufacturer_alias
-- 6107

-- шаг 1
select und.del_data(true, false, false);

-- шаг 2
-- загузить из файла изготовителей (с конфигурацией "Excel файл синонимы")

-- шаг 3
insert into und.manufacturer (
  mfr_id,
  mfr_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
select
  t1.code::integer,
  t1.source_name,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false    
from und.doc_import_match t1;

SELECT setval('und.manufacturer_mfr_id_seq', (SELECT COALESCE(max(mfr_id), 0) + 1 FROM und.manufacturer), false);

-- шаг 4
select und.del_data(true, false, false);

-- шаг 5
-- загрузить из файла (с конфигурацией "Excel файл эталон") и перенести в эталонные основные наименования

-- шаг 6
select und.del_data(true, false, false);

-- шаг 7
-- загузить из файла синонимы

-- шаг 8
update und.doc_import_match t1
set match_good_id = t2.good_id, match_product_id = t2.product_id
from und.vw_partner_good t2
where t1.code = t2.partner_code;

-- шаг 9
insert into und.good (
  product_id,
  mfr_id,
  barcode,
  doc_row_id,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
select
  t1.match_product_id,
  t2.mfr_id,
  t1.barcode,
  t1.row_id,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false    
from und.doc_import_match t1
inner join und.good t2 on t1.match_good_id = t2.good_id
where t1.match_good_id is not null
order by t1.match_product_id;
-- 55258

-- шаг 10
insert into und.partner_good (
  good_id,
  partner_id,
  partner_code,
  partner_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
select
  t1.good_id,
  1,
  t2.code,
  t2.source_name,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false    
from und.good t1
inner join und.doc_import_match t2 on t1.doc_row_id = t2.row_id;
-- 55258

-- шаг 11
with partner_good_cte as
(
select (row_number() over(partition by product_id order by product_name, partner_name))::character varying as adr, item_id
from und.vw_partner_good
)
update und.partner_good t1
set partner_code = t2.adr
from partner_good_cte t2
where t1.item_id = t2.item_id;

-- шаг 12
select und.del_data(true, false, false);

-- шаг 13
-- загузить из файла синонимы изготовителей (с конфигурацией "Excel файл синонимы")

-- шаг 14
insert into und.manufacturer_alias (
  mfr_id,
  alias_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
select
  t2.mfr_id,
  t1.source_name,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false    
from und.doc_import_match t1
inner join und.manufacturer t2 on t1.code::integer = t2.mfr_id;

-- шаг 15
select und.del_data(true, false, false);

-- шаг 16
-- загузить из файла группы по применению (с конфигурацией "Excel файл синонимы")

-- шаг 17
update und.apply_group t1
set apply_group_name = t2.source_name
from und.doc_import_match t2
where t1.apply_group_id = t2.code::int

-- шаг 18
update und.partner_good
set partner_code = good_id::character varying;

-- шаг 19
select und.del_data(true, false, false);

-- select * from und.partner_good where good_id = 1
-- select * from und.vw_partner_good where product_id = 1
-- select * from und.partner

-- delete from und.partner_good where crt_user = 'pavlov';

/*
select und.del_data(true, false, false);

delete from und.doc_import_log;
delete from und.doc_import_match_variant;
delete from und.doc_import_match;
delete from und.doc_import_row;
delete from und.doc_import_good;
delete from und.doc_import;
delete from und.doc_lc;
delete from und.doc;
*/