﻿/*
	ALTER TABLE und.stock_sales_download ADD COLUMN force_all
*/

ALTER TABLE und.stock_sales_download ADD COLUMN force_all boolean NOT NULL DEFAULT false;