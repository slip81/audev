﻿/*
	ALTER TABLEs und.doc_import_xxx ADD COLUMN apply_group_name
*/

ALTER TABLE und.doc_import_row ADD COLUMN apply_group_name character varying;
ALTER TABLE und.doc_import_match ADD COLUMN apply_group_name character varying;
ALTER TABLE und.doc_import_good ADD COLUMN apply_group_name character varying;
