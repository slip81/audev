﻿/*
	ALTER TABLE und.good ADD COLUMN is_main
*/

ALTER TABLE und.good ADD COLUMN is_main boolean NOT NULL DEFAULT false;

UPDATE und.good t1
SET is_main = true
FROM und.product t2
WHERE t1.product_id = t2.product_id
AND t1.doc_id IS NOT NULL;
-- 40516

-- select count(*) from und.product;
-- 40516