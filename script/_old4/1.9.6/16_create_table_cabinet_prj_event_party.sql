﻿/*
	CREATE TABLE cabinet.prj_event_party
*/

-- DROP TABLE cabinet.prj_event_party;

CREATE TABLE cabinet.prj_event_party
(
  party_id serial NOT NULL,
  event_id integer NOT NULL,
  user_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT prj_event_party_pkey PRIMARY KEY (party_id),
  CONSTRAINT prj_event_party_event_id_fkey FOREIGN KEY (event_id)
      REFERENCES cabinet.prj_event (event_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_event_party_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_event_party OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_event_party_set_stamp_trg ON cabinet.prj_event_party;

CREATE TRIGGER cabinet_prj_event_party_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_event_party
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

