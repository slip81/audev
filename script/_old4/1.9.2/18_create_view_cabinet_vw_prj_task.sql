﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_task
*/

-- DROP VIEW cabinet.vw_prj_task;

CREATE OR REPLACE VIEW cabinet.vw_prj_task AS 
SELECT
  t1.task_id,
  t1.claim_id,
  t1.task_num,
  t1.task_text,
  t1.state_id,
  t1.date_plan,
  t1.date_fact,
  t1.cost_plan,
  t1.cost_fact,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num,
  t2.state_name,
  t2.done_or_canceled
FROM cabinet.prj_task t1
INNER JOIN cabinet.crm_state t2 ON t1.state_id = t2.state_id;

ALTER TABLE cabinet.vw_prj_task OWNER TO pavlov;