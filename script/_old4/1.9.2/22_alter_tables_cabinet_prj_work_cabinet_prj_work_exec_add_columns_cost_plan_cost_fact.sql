﻿/*
	ALTER TABLEs cabinet.prj_work, cabinet.prj_work_exec ADD COLUMNs cost_plan, cost_fact
*/

ALTER TABLE cabinet.prj_work ADD COLUMN cost_plan numeric;
ALTER TABLE cabinet.prj_work ADD COLUMN cost_fact numeric;

ALTER TABLE cabinet.prj_work_exec ADD COLUMN cost_plan numeric;
ALTER TABLE cabinet.prj_work_exec ADD COLUMN cost_fact numeric;