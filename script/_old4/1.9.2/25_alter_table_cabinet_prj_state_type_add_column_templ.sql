﻿/*
	ALTER TABLE cabinet.prj_state_type ADD COLUMN templ
*/

ALTER TABLE cabinet.prj_state_type ADD COLUMN templ character varying;

UPDATE cabinet.prj_state_type
SET templ = '<span class="text-primary">Подготовка</span>'
where state_type_id = 1;

UPDATE cabinet.prj_state_type
SET templ = '<span class="text-success">Активен</span>'
where state_type_id = 2;

UPDATE cabinet.prj_state_type
SET templ = '<span><em>Завершен</em></span>'
where state_type_id = 3;

UPDATE cabinet.prj_state_type
SET templ = '<span><em>В архиве</em></span>'
where state_type_id = 4;

-- select * from cabinet.prj_state_type
