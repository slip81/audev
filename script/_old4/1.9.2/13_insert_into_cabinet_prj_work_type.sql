﻿/*
	insert into cabinet.prj_work_type
*/

insert into cabinet.prj_work_type (work_type_id, work_type_name)
values (1, 'Проектирование');

insert into cabinet.prj_work_type (work_type_id, work_type_name)
values (2, 'Программирование');

insert into cabinet.prj_work_type (work_type_id, work_type_name)
values (3, 'Тестирование');

insert into cabinet.prj_work_type (work_type_id, work_type_name)
values (4, 'Документирование');


-- select * from cabinet.prj_work_type;