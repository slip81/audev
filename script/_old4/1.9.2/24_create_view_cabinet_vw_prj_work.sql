﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_work
*/

-- DROP VIEW cabinet.vw_prj_work;

CREATE OR REPLACE VIEW cabinet.vw_prj_work AS 
SELECT 
  t1.work_id,
  t1.task_id,
  t1.claim_id,
  t1.work_num,
  t1.work_type_id,
  t1.exec_user_id,
  t1.state_id,
  t1.date_beg,
  t1.date_end,
  t1.is_accept,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num,
  t1.cost_plan,
  t1.cost_fact,
  t2.work_type_name,
  t3.user_name as exec_user_name,
  t4.state_name
   FROM cabinet.prj_work t1
     INNER JOIN cabinet.prj_work_type t2 ON t1.work_type_id = t2.work_type_id
     INNER JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     INNER JOIN cabinet.crm_state t4 ON t1.state_id = t4.state_id;

ALTER TABLE cabinet.vw_prj_work OWNER TO pavlov;
