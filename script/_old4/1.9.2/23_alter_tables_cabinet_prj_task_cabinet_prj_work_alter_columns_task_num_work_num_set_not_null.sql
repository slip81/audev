﻿/*
	ALTER TABLEs cabinet.prj_task, cabinet.prj_work ALTER COLUMNs task_num, work_num SET NOT NULL;
*/

ALTER TABLE cabinet.prj_task ALTER COLUMN task_num SET NOT NULL;
ALTER TABLE cabinet.prj_work ALTER COLUMN work_num SET NOT NULL;