﻿/*
	insert into cabinet.prj, cabinet.prj_state
*/

delete from cabinet.prj_state;
delete from cabinet.prj;

select setval('cabinet.prj_prj_id_seq', 1, false);
select setval('cabinet.prj_state_state_id_seq', 1, false);

insert into cabinet.prj (prj_id, prj_name, date_beg, date_end, curr_state_id, crt_date, crt_user)
values (0, '[не указан]', '2010-01-01', '2020-01-01', 2, current_timestamp, 'ПМС');

insert into cabinet.prj (prj_id, prj_name, date_beg, date_end, curr_state_id, crt_date, crt_user)
select batch_id, batch_name, date_beg, date_end, 2, current_timestamp, 'ПМС'
from cabinet.crm_batch
where batch_id > 0;

update cabinet.prj
set date_beg = (select min(x1.crt_date)::date from cabinet.crm_task x1 where x1.batch_id = cabinet.prj.prj_id)
where date_beg is null;

update cabinet.prj
set date_end = date_beg + '1 year'::interval
where date_end is null;

insert into cabinet.prj_state (prj_id, state_type_id, date_beg, date_end, crt_date, crt_user)
select prj_id, 2, date_beg, null, current_timestamp, 'ПМС'
from cabinet.prj;

update cabinet.prj
set curr_state_id = (select max(x1.state_id) from cabinet.prj_state x1 where cabinet.prj.prj_id = x1.prj_id);


select setval('cabinet.prj_prj_id_seq', (select coalesce(max(prj_id), 0) + 1 from cabinet.prj), false);
select setval('cabinet.prj_state_state_id_seq', (select coalesce(max(state_id), 0) + 1 from cabinet.prj_state), false);

-- select * from cabinet.crm_batch;
-- select * from cabinet.prj;
-- select * from cabinet.prj_state;
-- select * from cabinet.prj_state_type;
