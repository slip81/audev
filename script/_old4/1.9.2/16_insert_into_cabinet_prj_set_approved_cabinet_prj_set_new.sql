﻿/*
	insert into cabinet.prj_set_approved, cabinet.prj_set_new
*/

delete from cabinet.prj_set_new;
delete from cabinet.prj_set_approved;

select setval('cabinet.prj_set_new_item_id_seq', 1, false);
select setval('cabinet.prj_set_approved_item_id_seq', 1, false);

insert into cabinet.prj_set_approved (
  prj_id,
  claim_id,
  crt_date,
  crt_user
)
select
  prj_id,
  claim_id,
  crt_date,
  crt_user
from cabinet.prj_claim
where prj_id > 0;

insert into cabinet.prj_set_new (
  prj_id,
  claim_id,
  crt_date,
  crt_user
)
select
  prj_id,
  claim_id,
  crt_date,
  crt_user
from cabinet.prj_claim
where prj_id <= 0;

select setval('cabinet.prj_set_new_item_id_seq', (select coalesce(max(item_id), 0) + 1 from cabinet.prj_set_new), false); 
select setval('cabinet.prj_set_approved_item_id_seq', (select coalesce(max(item_id), 0) + 1 from cabinet.prj_set_approved), false); 

/*
select * from cabinet.prj_set_new
select * from cabinet.prj_set_approved
*/