﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple
*/

-- DROP VIEW cabinet.vw_prj_claim_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple AS 
 SELECT t1.claim_id,
    t1.prj_id,
        CASE
            WHEN COALESCE(t12.task_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t12.task_cnt, 0::bigint) > 0 AND COALESCE(t12.active_task_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t12.task_cnt, 0::bigint) > 0 AND t12.task_cnt = t12.done_task_cnt THEN 3
            WHEN COALESCE(t12.task_cnt, 0::bigint) > 0 AND t12.task_cnt = t12.canceled_task_cnt THEN 4
            WHEN COALESCE(t12.task_cnt, 0::bigint) > 0 AND COALESCE(t12.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t12.done_task_cnt, 0::bigint) > 0 AND COALESCE(t12.canceled_task_cnt, 0::bigint) > 0 THEN 5
            ELSE 1
        END AS claim_state_type_id,
        t12.task_cnt,
        t12.active_task_cnt,
        t12.done_task_cnt,
        t12.canceled_task_cnt
   FROM cabinet.prj_claim t1
     LEFT JOIN ( SELECT count(x1.task_id) AS task_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 0 THEN 1
                    ELSE 0
                END) AS active_task_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 1 THEN 1
                    ELSE 0
                END) AS done_task_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 2 THEN 1
                    ELSE 0
                END) AS canceled_task_cnt,
            x1.claim_id
           FROM cabinet.vw_prj_task_simple x1
          GROUP BY x1.claim_id) t12 ON t1.claim_id = t12.claim_id;

ALTER TABLE cabinet.vw_prj_claim_simple OWNER TO pavlov;
