﻿/*
	ALTER TABLE cabinet.prj_claim_state_type ADD COLUMN templ
*/

ALTER TABLE cabinet.prj_claim_state_type ADD COLUMN templ character varying;

UPDATE cabinet.prj_claim_state_type
SET templ = '<span class="text-danger">Неразобрано</span>'
where state_type_id = 1;

UPDATE cabinet.prj_claim_state_type
SET templ = '<span class="text-success">Активно</span>'
where state_type_id = 2;

UPDATE cabinet.prj_claim_state_type
SET templ = '<span class="text-primary">Выполнено</span>'
where state_type_id = 3;

UPDATE cabinet.prj_claim_state_type
SET templ = '<span class="text-warning">Отменено</span>'
where state_type_id = 4;

UPDATE cabinet.prj_claim_state_type
SET templ = '<span class="text-info">Частично выполнено</span>'
where state_type_id = 5;

-- select * from cabinet.prj_claim_state_type order by state_type_id;