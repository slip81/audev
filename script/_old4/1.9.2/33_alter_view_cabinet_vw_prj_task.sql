﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_task
*/

-- DROP VIEW cabinet.vw_prj_task;

CREATE OR REPLACE VIEW cabinet.vw_prj_task AS 
 SELECT t1.task_id,
    t1.claim_id,
    t1.task_num,
    t1.task_text,
    t1.state_id,
    t1.date_plan,
    t1.date_fact,
    t1.cost_plan,
    t1.cost_fact,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t2.state_name,
    t2.done_or_canceled,
    t3.claim_num,
    t3.claim_name,
    t3.claim_text,
    t3.prj_id as claim_prj_id,
    t3.project_id as claim_project_id,
    t3.module_id as claim_module_id,
    t3.module_part_id as claim_module_part_id,
    t3.module_version_id as claim_module_version_id,
    t3.client_id as claim_client_id,
    t3.priority_id as claim_priority_id,
    t3.resp_user_id as claim_resp_user_id,
    t3.date_plan as claim_date_plan,
    t3.date_fact as claim_date_fact,
    t3.repair_version_id as claim_repair_version_id,
    t3.is_archive as claim_is_archive,
    t3.rate as claim_rate,
    t3.is_control as claim_is_control,
    t3.cost_plan as claim_cost_plan,
    t3.cost_fact as claim_cost_fact,
    t3.project_name as claim_project_name,
    t3.module_name as claim_module_name,
    t3.module_part_name as claim_module_part_name,
    t3.module_version_name as claim_module_version_name,
    t3.client_name as claim_client_name,
    t3.priority_name as claim_priority_name,
    t3.resp_user_name AS claim_resp_user_name,
    t3.repair_version_name AS claim_repair_version_name,
    t3.claim_state_type_id,    
    t3.prj_name,
    t3.prj_date_beg,
    t3.prj_date_end,
    t3.prj_curr_state_id,
    t3.prj_state_type_id,
    t3.prj_state_type_name       
   FROM cabinet.prj_task t1
     JOIN cabinet.crm_state t2 ON t1.state_id = t2.state_id
     JOIN cabinet.vw_prj_claim t3 ON t1.claim_id = t3.claim_id;

ALTER TABLE cabinet.vw_prj_task OWNER TO pavlov;
