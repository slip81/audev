﻿/*
	CREATE TABLE cabinet.prj_claim
*/

-- DROP TABLE cabinet.prj_claim;

CREATE TABLE cabinet.prj_claim
(
  claim_id serial NOT NULL,
  claim_num character varying,
  claim_name character varying,
  claim_text character varying,
  prj_id integer NOT NULL,
  project_id integer NOT NULL,
  module_id integer NOT NULL,
  module_part_id integer NOT NULL,
  module_version_id integer NOT NULL,
  client_id integer NOT NULL,
  priority_id integer NOT NULL,
  resp_user_id integer NOT NULL,
  date_plan date,
  date_fact date,
  repair_version_id integer,  
  is_archive boolean NOT NULL DEFAULT false,
  rate numeric,
  crt_date timestamp without time zone,  
  crt_user character varying,  
  upd_date timestamp without time zone,  
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  is_control boolean NOT NULL DEFAULT false,
  cost_plan numeric,
  cost_fact numeric,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_claim_pkey PRIMARY KEY (claim_id),
  CONSTRAINT prj_claim_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.crm_client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT prj_claim_module_id_fkey FOREIGN KEY (module_id)
      REFERENCES cabinet.crm_module (module_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT prj_claim_module_part_id_fkey FOREIGN KEY (module_part_id)
      REFERENCES cabinet.crm_module_part (module_part_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_module_version_id_fkey FOREIGN KEY (module_version_id)
      REFERENCES cabinet.crm_module_version (module_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_resp_user_id_fkey FOREIGN KEY (resp_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_priority_id_fkey FOREIGN KEY (priority_id)
      REFERENCES cabinet.crm_priority (priority_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT prj_claim_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT prj_claim_repair_version_id_fkey FOREIGN KEY (repair_version_id)
      REFERENCES cabinet.crm_module_version (module_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION           
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_claim_set_stamp_trg ON cabinet.prj_claim;

CREATE TRIGGER cabinet_prj_claim_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_claim
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();