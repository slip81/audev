﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_task_simple
*/

-- DROP VIEW cabinet.vw_prj_task_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_task_simple AS 
 SELECT t1.task_id,
    t1.claim_id,
    t1.state_id,
    t2.done_or_canceled
   FROM cabinet.prj_task t1
     JOIN cabinet.crm_state t2 ON t1.state_id = t2.state_id;

ALTER TABLE cabinet.vw_prj_task_simple OWNER TO pavlov;
