﻿/*
	insert into cabinet.prj_state_type
*/

insert into cabinet.prj_state_type (state_type_id, state_type_name)
values (1, 'Подготовка');

insert into cabinet.prj_state_type (state_type_id, state_type_name)
values (2, 'Активен');

insert into cabinet.prj_state_type (state_type_id, state_type_name)
values (3, 'Завершен');

insert into cabinet.prj_state_type (state_type_id, state_type_name)
values (4, 'В архиве');

-- select * from cabinet.prj_state_type;