﻿/*
	insert into cabinet.prj_claim, cabinet.prj_task, cabinet.prj_work, cabinet.prj_work_exec
*/

delete from cabinet.prj_work_exec;
delete from cabinet.prj_work;
delete from cabinet.prj_task;
delete from cabinet.prj_claim;

select setval('cabinet.prj_claim_claim_id_seq', 1, false);
select setval('cabinet.prj_task_task_id_seq', 1, false);
select setval('cabinet.prj_work_work_id_seq', 1, false);
select setval('cabinet.prj_work_exec_item_id_seq', 1, false);

insert into cabinet.prj_claim (
  claim_id,
  claim_num,
  claim_name,
  claim_text,
  prj_id,
  project_id,
  module_id,
  module_part_id,
  module_version_id,
  client_id,
  priority_id,
  resp_user_id,
  date_plan,
  date_fact,
  repair_version_id,
  is_archive,
  rate,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num,
  is_control,
  cost_plan,
  cost_fact
)
select
  task_id,
  task_num,
  task_name,
  task_text,
  coalesce(batch_id, 0),
  project_id,
  module_id,
  module_part_id,
  module_version_id,
  client_id,
  priority_id,
  assigned_user_id,
  required_date,
  repair_date,
  repair_version_id,
  is_archive,
  fin_cost,
  crt_date,
  t2.user_name,
  upd_date,
  upd_user,
  upd_num,
  is_control,
  cost_plan,
  cost_fact
from cabinet.crm_task t1
left join cabinet.cab_user t2 on t1.owner_user_id = t2.user_id;

-- select * from cabinet.cab_user 

insert into cabinet.prj_task (  
  claim_id,
  task_num,
  task_text,
  state_id,
  date_plan,
  date_fact,
  cost_plan,
  cost_fact,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
select
  t1.task_id,
  t1.task_num,
  t1.subtask_text,
  t1.state_id,
  t2.required_date,
  t2.repair_date,
  t2.cost_plan,
  t2.cost_fact,
  t1.crt_date,
  t3.user_name,
  t1.upd_date,
  t2.upd_user,
  t2.upd_num  
from cabinet.crm_subtask t1
inner join cabinet.crm_task t2 on t1.task_id = t2.task_id
left join cabinet.cab_user t3 on t2.owner_user_id = t3.user_id;

insert into cabinet.prj_task (  
  claim_id,
  task_num,
  task_text,
  state_id,
  date_plan,
  date_fact,
  cost_plan,
  cost_fact,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
select
  t1.task_id,
  1,
  t1.task_text,
  t1.state_id,
  t1.required_date,
  t1.repair_date,
  t1.cost_plan,
  t1.cost_fact,
  t1.crt_date,
  t2.user_name,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num 
from cabinet.crm_task t1
left join cabinet.cab_user t2 on t1.owner_user_id = t2.user_id
where not exists(select x1.task_id from cabinet.crm_subtask x1 where t1.task_id = x1.task_id);

insert into cabinet.prj_work (    
  task_id,
  claim_id,
  work_num,
  work_type_id,
  exec_user_id,
  state_id,
  date_beg,
  date_end,
  is_accept,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
select
  t3.task_id,
  t3.claim_id,
  1,
  2,
  t2.exec_user_id,
  t1.state_id,
  t4.date_beg,
  t4.date_end,
  true,
  t3.crt_date,
  t3.crt_user,
  t3.upd_date,
  t3.upd_user,
  t3.upd_num  
from cabinet.crm_subtask t1
inner join cabinet.crm_task t2 on t1.task_id = t2.task_id
inner join cabinet.prj_task t3 on t2.task_id = t3.claim_id and t1.task_num = t3.task_num
left join
(
select max(x1.event_id) as event_id, max(x1.date_beg) as date_beg, max(x1.date_end) as date_end, x1.task_id, x1.exec_user_id
from cabinet.planner_event x1
group by x1.task_id, x1.exec_user_id
) t4 on t2.task_id = t4.task_id and t2.exec_user_id = t4.exec_user_id;


insert into cabinet.prj_work (  
  task_id,
  claim_id,
  work_num,
  work_type_id,
  exec_user_id,
  state_id,
  date_beg,
  date_end,
  is_accept,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
select
  t3.task_id,
  t3.claim_id,
  1,
  2,
  t1.exec_user_id,
  t1.state_id,
  t4.date_beg,
  t4.date_end,
  true,
  t3.crt_date,
  t3.crt_user,
  t3.upd_date,
  t3.upd_user,
  t3.upd_num
from cabinet.crm_task t1
inner join cabinet.prj_task t3 on t1.task_id = t3.claim_id and t3.task_num = 1
left join
(
select max(x1.event_id) as event_id, max(x1.date_beg) as date_beg, max(x1.date_end) as date_end, x1.task_id, x1.exec_user_id
from cabinet.planner_event x1
group by x1.task_id, x1.exec_user_id
) t4 on t1.task_id = t4.task_id and t1.exec_user_id = t4.exec_user_id
where not exists(select x1.task_id from cabinet.crm_subtask x1 where t1.task_id = x1.task_id);

insert into cabinet.prj_work_exec (    
  work_id,
  exec_user_id,
  state_id,
  date_beg,
  date_end,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
select
  t1.work_id,
  t1.exec_user_id,
  t1.state_id,
  t1.date_beg,
  t1.date_end,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num 
from cabinet.prj_work t1;

select setval('cabinet.prj_claim_claim_id_seq', (select coalesce(max(claim_id), 0) + 1 from cabinet.prj_claim), false);
select setval('cabinet.prj_task_task_id_seq', (select coalesce(max(task_id), 0) + 1 from cabinet.prj_task), false);
select setval('cabinet.prj_work_work_id_seq', (select coalesce(max(work_id), 0) + 1 from cabinet.prj_work), false);
select setval('cabinet.prj_work_exec_item_id_seq', (select coalesce(max(item_id), 0) + 1 from cabinet.prj_work_exec), false);

-- select * from cabinet.prj_work_type
-- select * from cabinet.cab_grid_column

/*
select * from cabinet.prj_work_exec;
select * from cabinet.prj_work;
select * from cabinet.prj_task;
select * from cabinet.prj_claim;
*/