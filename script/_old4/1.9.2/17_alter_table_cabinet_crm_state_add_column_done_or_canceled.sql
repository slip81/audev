﻿/*
	ALTER TABLE cabinet.crm_state ADD COLUMN done_or_canceled
*/

ALTER TABLE cabinet.crm_state ADD COLUMN done_or_canceled integer NOT NULL DEFAULT 0;

UPDATE cabinet.crm_state SET done_or_canceled = 1 WHERE state_id IN (5, 10);
UPDATE cabinet.crm_state SET done_or_canceled = 2 WHERE state_id IN (4);

-- select * from cabinet.crm_state order by state_id
