﻿/*
	CREATE TABLE cabinet.prj_state
*/

-- DROP TABLE cabinet.prj_state;

CREATE TABLE cabinet.prj_state
(
  state_id serial NOT NULL,
  prj_id integer NOT NULL,
  state_type_id integer NOT NULL,
  date_beg date,
  date_end date,  
  crt_date timestamp without time zone,
  crt_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_state_pkey PRIMARY KEY (state_id),
  CONSTRAINT prj_state_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_state_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES cabinet.prj_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_state OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_state_set_stamp_trg ON cabinet.prj_state;

CREATE TRIGGER cabinet_prj_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_state
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();
