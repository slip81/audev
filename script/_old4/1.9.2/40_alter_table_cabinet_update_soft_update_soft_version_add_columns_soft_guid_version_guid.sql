﻿/*
	ALTER TABLE cabinet.update_soft ADD COLUMN soft_guid
*/

ALTER TABLE cabinet.update_soft ADD COLUMN soft_guid character varying;
ALTER TABLE cabinet.update_soft_version ADD COLUMN version_guid character varying;

UPDATE cabinet.update_soft
SET soft_guid = uuid_generate_v4()::text;

UPDATE cabinet.update_soft_version
SET version_guid = uuid_generate_v4()::text;


-- SELECT uuid_generate_v4()::text;
-- select * from cabinet.update_soft order by soft_id;