﻿/*
	CREATE TABLE cabinet.prj_work_exec
*/

-- DROP TABLE cabinet.prj_work_exec;

CREATE TABLE cabinet.prj_work_exec
(
  item_id serial NOT NULL,
  work_id integer NOT NULL,
  exec_user_id integer NOT NULL,
  state_id integer NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  crt_date timestamp without time zone,  
  crt_user character varying,  
  upd_date timestamp without time zone,  
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_exec_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_work_exec_work_id_fkey FOREIGN KEY (work_id)
      REFERENCES cabinet.prj_work (work_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT prj_work_exec_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,          
  CONSTRAINT prj_work_exec_state_id_fkey FOREIGN KEY (state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work_exec OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_exec_set_stamp_trg ON cabinet.prj_work_exec;

CREATE TRIGGER cabinet_prj_work_exec_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work_exec
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();