﻿/*
	CREATE TABLE cabinet.prj
*/

-- DROP TABLE cabinet.prj;

CREATE TABLE cabinet.prj
(
  prj_id serial NOT NULL,
  prj_name character varying,
  date_beg date,
  date_end date,
  curr_state_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_pkey PRIMARY KEY (prj_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_set_stamp_trg ON cabinet.prj;

CREATE TRIGGER cabinet_prj_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();