﻿/*
	CREATE TABLE cabinet.prj_work
*/

-- DROP TABLE cabinet.prj_work;

CREATE TABLE cabinet.prj_work
(
  work_id serial NOT NULL,
  task_id integer NOT NULL,
  claim_id integer NOT NULL,
  work_num integer,
  work_type_id integer NOT NULL,
  exec_user_id integer NOT NULL,
  state_id integer NOT NULL,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  is_accept boolean NOT NULL DEFAULT false,
  crt_date timestamp without time zone,  
  crt_user character varying,  
  upd_date timestamp without time zone,  
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_pkey PRIMARY KEY (work_id),
  CONSTRAINT prj_work_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.prj_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT prj_work_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_work_work_type_id_fkey FOREIGN KEY (work_type_id)
      REFERENCES cabinet.prj_work_type (work_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT prj_work_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,          
  CONSTRAINT prj_task_state_id_fkey FOREIGN KEY (state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_set_stamp_trg ON cabinet.prj_work;

CREATE TRIGGER cabinet_prj_work_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();