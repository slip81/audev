﻿/*
	CREATE TABLE cabinet.prj_set_new
*/

-- DROP TABLE cabinet.prj_set_new;

CREATE TABLE cabinet.prj_set_new
(
  item_id serial NOT NULL,
  prj_id integer NOT NULL,
  claim_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_set_new_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_set_new_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_set_new_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION 
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_set_new OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_set_new_set_stamp_trg ON cabinet.prj_set_new;

CREATE TRIGGER cabinet_prj_set_new_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_set_new
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();