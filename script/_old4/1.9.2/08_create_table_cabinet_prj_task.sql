﻿/*
	CREATE TABLE cabinet.prj_task
*/

-- DROP TABLE cabinet.prj_task;

CREATE TABLE cabinet.prj_task
(
  task_id serial NOT NULL,
  claim_id integer NOT NULL,  
  task_num integer,
  task_text character varying,
  state_id integer NOT NULL,
  date_plan date,
  date_fact date,  
  cost_plan numeric,
  cost_fact numeric,
  crt_date timestamp without time zone,  
  crt_user character varying,  
  upd_date timestamp without time zone,  
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_task_pkey PRIMARY KEY (task_id),
  CONSTRAINT prj_task_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_task_state_id_fkey FOREIGN KEY (state_id)
      REFERENCES cabinet.crm_state (state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_task OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_task_set_stamp_trg ON cabinet.prj_task;

CREATE TRIGGER cabinet_prj_task_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_task
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();