﻿/*
	CREATE TABLE cabinet.prj_work_type_set
*/

-- DROP TABLE cabinet.prj_work_type_set;

CREATE TABLE cabinet.prj_work_type_set
(
  work_type_set_id integer NOT NULL,
  work_type_set_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_type_set_pkey PRIMARY KEY (work_type_set_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work_type_set OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_type_set_stamp_trg ON cabinet.prj_work_type_set;

CREATE TRIGGER cabinet_prj_work_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work_type_set
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();


-- DROP TABLE cabinet.prj_work_type_set_item;

CREATE TABLE cabinet.prj_work_type_set_item
(
  item_id serial NOT NULL,
  work_type_set_id integer NOT NULL,
  work_type_id integer NOT NULL,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_type_set_item_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_work_type_set_item_work_type_set_id_fkey FOREIGN KEY (work_type_set_id)
      REFERENCES cabinet.prj_work_type_set (work_type_set_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_work_type_set_item_work_type_id_fkey FOREIGN KEY (work_type_id)
      REFERENCES cabinet.prj_work_type (work_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work_type_set_item OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_type_set_item_stamp_trg ON cabinet.prj_work_type_set_item;

CREATE TRIGGER cabinet_prj_work_type_set_item_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work_type_set_item
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();


INSERT INTO cabinet.prj_work_type_set (
  work_type_set_id,
  work_type_set_name
)
VALUES (
  1,
  'Прогр. + Тестир.'
);

INSERT INTO cabinet.prj_work_type_set (
  work_type_set_id,
  work_type_set_name
)
VALUES (
  2,
  'Проект. + Прогр. + Тестир.'
);

INSERT INTO cabinet.prj_work_type_set (
  work_type_set_id,
  work_type_set_name
)
VALUES (
  3,
  'Проект. + Прогр. + Тестир. + Документир.'
);

INSERT INTO cabinet.prj_work_type_set (
  work_type_set_id,
  work_type_set_name
)
VALUES (
  4,
  'Проектирование'
);

INSERT INTO cabinet.prj_work_type_set (
  work_type_set_id,
  work_type_set_name
)
VALUES (
  5,
  'Программирование'
);

INSERT INTO cabinet.prj_work_type_set (
  work_type_set_id,
  work_type_set_name
)
VALUES (
  6,
  'Тестирование'
);

INSERT INTO cabinet.prj_work_type_set (
  work_type_set_id,
  work_type_set_name
)
VALUES (
  7,
  'Документирование'
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  1,
  2
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  1,
  3
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  2,
  1
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  2,
  3
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  2,
  3
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  3,
  1
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  3,
  2
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  3,
  3
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  3,
  4
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  4,
  1
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  5,
  2
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  6,
  3
);

INSERT INTO cabinet.prj_work_type_set_item (
  work_type_set_id,
  work_type_id
)
VALUES (
  7,
  4
);

-- select * from cabinet.prj_work_type order by work_type_id