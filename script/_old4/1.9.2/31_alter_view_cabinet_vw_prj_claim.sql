﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim
*/

-- DROP VIEW cabinet.vw_prj_claim;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim AS 
 SELECT t1.claim_id,
    t1.claim_num,
    t1.claim_name,
    t1.claim_text,
    t1.prj_id,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.module_version_id,
    t1.client_id,
    t1.priority_id,
    t1.resp_user_id,
    t1.date_plan,
    t1.date_fact,
    t1.repair_version_id,
    t1.is_archive,
    t1.rate,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.is_control,
    t1.cost_plan,
    t1.cost_fact,
    t2.project_name,
    t3.module_name,
    t4.module_part_name,
    t5.module_version_name,
    t6.client_name,
    t7.priority_name,
    t8.user_name AS resp_user_name,
    t11.module_version_name AS repair_version_name,
    t12.task_cnt,
    t12.active_task_cnt,
    t12.done_task_cnt,
    t12.canceled_task_cnt,
        CASE
            WHEN COALESCE(t12.task_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t12.task_cnt, 0::bigint) > 0 AND COALESCE(t12.active_task_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t12.task_cnt, 0::bigint) > 0 AND t12.task_cnt = t12.done_task_cnt THEN 3
            WHEN COALESCE(t12.task_cnt, 0::bigint) > 0 AND t12.task_cnt = t12.canceled_task_cnt THEN 4
            WHEN COALESCE(t12.task_cnt, 0::bigint) > 0 AND COALESCE(t12.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t12.done_task_cnt, 0::bigint) > 0 AND COALESCE(t12.canceled_task_cnt, 0::bigint) > 0 THEN 5
            ELSE 1
        END AS claim_state_type_id,
    t9.prj_name,    
    t9.date_beg as prj_date_beg,
    t9.date_end as prj_date_end,
    t9.curr_state_id as prj_curr_state_id,
    t9.state_type_id as prj_state_type_id,
    t9.state_type_name as prj_state_type_name
   FROM cabinet.prj_claim t1
     JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id
     JOIN cabinet.crm_client t6 ON t1.client_id = t6.client_id
     JOIN cabinet.crm_priority t7 ON t1.priority_id = t7.priority_id
     JOIN cabinet.cab_user t8 ON t1.resp_user_id = t8.user_id
     JOIN cabinet.vw_prj t9 ON t1.prj_id = t9.prj_id
     LEFT JOIN cabinet.crm_module_version t11 ON t1.repair_version_id = t11.module_version_id
     LEFT JOIN ( SELECT count(x1.task_id) AS task_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 0 THEN 1
                    ELSE 0
                END) AS active_task_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 1 THEN 1
                    ELSE 0
                END) AS done_task_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 2 THEN 1
                    ELSE 0
                END) AS canceled_task_cnt,
            x1.claim_id
           FROM cabinet.vw_prj_task_simple x1
          GROUP BY x1.claim_id) t12 ON t1.claim_id = t12.claim_id;

ALTER TABLE cabinet.vw_prj_claim OWNER TO pavlov;
