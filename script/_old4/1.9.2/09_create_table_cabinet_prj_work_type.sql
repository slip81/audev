﻿/*
	CREATE TABLE cabinet.prj_work_type
*/

-- DROP TABLE cabinet.prj_work_type;

CREATE TABLE cabinet.prj_work_type
(
  work_type_id integer NOT NULL,
  work_type_name character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_type_pkey PRIMARY KEY (work_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_type_set_stamp_trg ON cabinet.prj_work_type;

CREATE TRIGGER cabinet_prj_work_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();