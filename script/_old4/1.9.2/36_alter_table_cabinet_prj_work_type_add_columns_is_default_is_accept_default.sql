﻿/*
	ALTER TABLE cabinet.prj_work_type ADD COLUMNs is_default, is_accept_default
*/

ALTER TABLE cabinet.prj_work_type ADD COLUMN is_default boolean NOT NULL DEFAULT false;
ALTER TABLE cabinet.prj_work_type ADD COLUMN is_accept_default boolean NOT NULL DEFAULT false;

UPDATE cabinet.prj_work_type
SET is_default = true
WHERE work_type_id in (2, 3);

UPDATE cabinet.prj_work_type
SET is_accept_default = true
WHERE work_type_id in (2);

-- select * from cabinet.prj_work_type order by work_type_id