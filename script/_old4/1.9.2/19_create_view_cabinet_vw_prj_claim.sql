﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_claim
*/

-- DROP VIEW cabinet.vw_prj_claim;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim AS 

SELECT
  t1.claim_id,
  t1.claim_num,
  t1.claim_name,
  t1.claim_text,
  t1.prj_id,
  t1.project_id,
  t1.module_id,
  t1.module_part_id,
  t1.module_version_id,
  t1.client_id,
  t1.priority_id,
  t1.resp_user_id,
  t1.date_plan,
  t1.date_fact,
  t1.repair_version_id,
  t1.is_archive,
  t1.rate,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num,
  t1.is_control,
  t1.cost_plan,
  t1.cost_fact,
  t2.project_name,
  t3.module_name,
  t4.module_part_name,
  t5.module_version_name,
  t6.client_name,
  t7.priority_name,
  t8.user_name as resp_user_name,
  t11.module_version_name as repair_version_name,
  t12.task_cnt,
  t12.active_task_cnt,
  t12.done_task_cnt,
  t12.canceled_task_cnt,
  case when coalesce(t12.task_cnt, 0) <= 0 then 1 
  when (coalesce(t12.task_cnt, 0) > 0) and (coalesce(t12.active_task_cnt, 0) > 0) then 2
  when (coalesce(t12.task_cnt, 0) > 0) and (t12.task_cnt = t12.done_task_cnt) then 3
  when (coalesce(t12.task_cnt, 0) > 0) and (t12.task_cnt = t12.canceled_task_cnt) then 4
  when (coalesce(t12.task_cnt, 0) > 0) and (coalesce(t12.active_task_cnt, 0) <= 0) and (coalesce(t12.done_task_cnt, 0) > 0) and (coalesce(t12.canceled_task_cnt, 0) > 0) then 5
  else 1 end as claim_state_type_id  
FROM cabinet.prj_claim t1
INNER JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
INNER JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
INNER JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
INNER JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id
INNER JOIN cabinet.crm_client t6 ON t1.client_id = t6.client_id
INNER JOIN cabinet.crm_priority t7 ON t1.priority_id = t7.priority_id
INNER JOIN cabinet.cab_user t8 ON t1.resp_user_id = t8.user_id
LEFT JOIN cabinet.crm_module_version t11 ON t1.repair_version_id = t11.module_version_id
LEFT JOIN (
	SELECT count(x1.task_id) as task_cnt,
	  sum(case when x1.done_or_canceled = 0 then 1 else 0 end) as active_task_cnt,
	  sum(case when x1.done_or_canceled = 1 then 1 else 0 end) as done_task_cnt,
	  sum(case when x1.done_or_canceled = 2 then 1 else 0 end) as canceled_task_cnt,
	  x1.claim_id
	FROM cabinet.vw_prj_task x1
	GROUP BY x1.claim_id
) t12 ON t1.claim_id = t12.claim_id
;

ALTER TABLE cabinet.vw_prj_claim OWNER TO pavlov;