﻿/*
	CREATE TABLE und.wa_request_lc
*/

-- DROP TABLE und.wa_request_lc;

CREATE TABLE und.wa_request_lc
(
  request_lc_id serial NOT NULL,
  request_id bigint NOT NULL,
  request_state_id integer NOT NULL DEFAULT 1,
  is_current boolean NOT NULL DEFAULT true,  
  state_date timestamp without time zone,
  state_user character varying,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_request_lc_pkey PRIMARY KEY (request_lc_id),
  CONSTRAINT wa_request_lc_request_state_id_fkey FOREIGN KEY (request_state_id)
      REFERENCES und.wa_request_state (request_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_lc_request_id_fkey FOREIGN KEY (request_id)
      REFERENCES und.wa_request (request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
  )
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_lc OWNER TO pavlov;

-- DROP TRIGGER und_wa_request_lc_set_stamp_trg ON und.wa_request_lc;

CREATE TRIGGER und_wa_request_lc_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_lc
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
