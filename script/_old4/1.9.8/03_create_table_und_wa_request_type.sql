﻿/*
	CREATE TABLE und.wa_request_type
*/

-- DROP TABLE und.wa_request_type;

CREATE TABLE und.wa_request_type
(
  request_type_id integer NOT NULL,
  request_type_name character varying,  
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_request_type_pkey PRIMARY KEY (request_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_type OWNER TO pavlov;

-- DROP TRIGGER und_wa_request_type_set_stamp_trg ON und.wa_request_type;

CREATE TRIGGER und_wa_request_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

INSERT INTO und.wa_request_type (
  request_type_id,
  request_type_name, 
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
) VALUES (
  1,
  'Получение эталонного справочника',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false   
);

