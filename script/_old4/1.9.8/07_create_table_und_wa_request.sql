﻿/*
	CREATE TABLE und.wa_request
*/

-- DROP TABLE und.wa_request;

CREATE TABLE und.wa_request
(
  request_id bigint NOT NULL DEFAULT und.id_generator(),
  wa_id integer NOT NULL,
  request_type_id integer NOT NULL,
  request_state_id integer NOT NULL DEFAULT 1,
  client_id integer,
  sales_id integer,
  workplace_id integer,
  ord integer NOT NULL DEFAULT 0,
  ord_client integer NOT NULL DEFAULT 0,
  state_date timestamp without time zone,
  state_user character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_request_pkey PRIMARY KEY (request_id),
  CONSTRAINT wa_request_wa_id_fkey FOREIGN KEY (wa_id)
      REFERENCES und.wa (wa_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_request_type_id_fkey FOREIGN KEY (request_type_id)
      REFERENCES und.wa_request_type (request_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_request_state_id_fkey FOREIGN KEY (request_state_id)
      REFERENCES und.wa_request_state (request_state_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.client (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_sales_id_fkey FOREIGN KEY (sales_id)
      REFERENCES cabinet.sales (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_workplace_id_fkey FOREIGN KEY (workplace_id)
      REFERENCES cabinet.workplace (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request OWNER TO pavlov;

-- DROP TRIGGER und_wa_request_set_stamp_trg ON und.wa_request;

CREATE TRIGGER und_wa_request_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

