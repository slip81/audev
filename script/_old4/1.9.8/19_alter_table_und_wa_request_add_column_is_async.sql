﻿/*
	ALTER TABLE und.wa_request ADD COLUMN is_async
*/

ALTER TABLE und.wa_request ADD COLUMN is_async boolean NOT NULL DEFAULT false;