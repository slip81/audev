﻿/*
	CREATE TABLEs und.wa_request_type_group1, und.wa_request_type_group2
*/

ALTER TABLE und.wa_request DROP CONSTRAINT wa_request_request_type_id_fkey;

DROP TABLE und.wa_request_type;

CREATE TABLE und.wa_request_type_group1
(
  request_type_group1_id integer NOT NULL,
  request_type_group1_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_request_type_group1_pkey PRIMARY KEY (request_type_group1_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_type_group1 OWNER TO pavlov;

CREATE TRIGGER und_wa_request_type_group1_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_type_group1
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
  

CREATE TABLE und.wa_request_type_group2
(
  request_type_group2_id integer NOT NULL,
  request_type_group2_name character varying,
  request_type_group1_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_request_type_group2_pkey PRIMARY KEY (request_type_group2_id),
  CONSTRAINT wa_request_type_group2_request_type_group1_id_fkey FOREIGN KEY (request_type_group1_id)  
      REFERENCES und.wa_request_type_group1 (request_type_group1_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_type_group2 OWNER TO pavlov;

CREATE TRIGGER und_wa_request_type_group2_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_type_group2
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();


CREATE TABLE und.wa_request_type
(
  request_type_id integer NOT NULL,
  request_type_name character varying,
  request_type_group1_id integer,
  request_type_group2_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_request_type_pkey PRIMARY KEY (request_type_id),
  CONSTRAINT wa_request_type_request_type_group1_id_fkey FOREIGN KEY (request_type_group1_id)  
      REFERENCES und.wa_request_type_group1 (request_type_group1_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT wa_request_type_request_type_group2_id_fkey FOREIGN KEY (request_type_group2_id)
      REFERENCES und.wa_request_type_group2 (request_type_group2_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_type OWNER TO pavlov;

CREATE TRIGGER und_wa_request_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_type
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();
  

INSERT INTO und.wa_request_type_group1 (
  request_type_group1_id, 
  request_type_group1_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  1,
  'ЕСН',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_request_type_group1 (
  request_type_group1_id, 
  request_type_group1_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  2,
  'АСНА',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);


INSERT INTO und.wa_request_type_group2 (
  request_type_group2_id, 
  request_type_group2_name,
  request_type_group1_id, 
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  1,
  'Отправка в АСНА',
  2,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_request_type_group2 (
  request_type_group2_id, 
  request_type_group2_name,
  request_type_group1_id, 
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  2,
  'Получение из АСНА',
  2,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  request_type_group1_id,
  request_type_group2_id,  
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  1,
  'Получение эталонного справочника',
  1,
  NULL,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  request_type_group1_id,
  request_type_group2_id,  
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  2,
  'АСНА: получение информации об аптеке',
  2,
  2,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  request_type_group1_id,
  request_type_group2_id,    
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  3,
  'АСНА: получение справочника связок номенклатуры',
  2,
  2,  
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  request_type_group1_id,
  request_type_group2_id,    
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  4,
  'АСНА: получение информации об остатках аптеки',
  2,
  2,  
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  request_type_group1_id,
  request_type_group2_id,    
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  5,
  'АСНА: передача полных остатков аптеки',
  2,
  1,  
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  request_type_group1_id,
  request_type_group2_id,    
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  6,
  'АСНА: передача изменений по остаткам аптеки',
  2,
  1,    
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  request_type_group1_id,
  request_type_group2_id,    
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  7,
  'АСНА: удаление информации по остаткам аптеки',
  2,
  1,    
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);


ALTER TABLE und.wa_request
  ADD CONSTRAINT wa_request_request_type_id_fkey FOREIGN KEY (request_type_id)
      REFERENCES und.wa_request_type (request_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;