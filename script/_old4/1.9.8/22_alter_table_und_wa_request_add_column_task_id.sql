﻿/*
	ALTER TABLE und.wa_request ADD COLUMN task_id
*/

ALTER TABLE und.wa_request ADD COLUMN task_id integer;

ALTER TABLE und.wa_request
  ADD CONSTRAINT wa_request_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES und.wa_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
