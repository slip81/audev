﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_work
*/

-- DROP VIEW cabinet.vw_prj_work;

CREATE OR REPLACE VIEW cabinet.vw_prj_work AS 
 SELECT t1.work_id,
    t1.task_id,
    t1.claim_id,
    t1.work_num,
    t1.work_type_id,
    t1.exec_user_id,
    t1.state_type_id,
    t1.is_plan,
    t1.date_beg,
    t1.date_end,
    t1.is_accept,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.cost_plan,
    t1.cost_fact,
    t1.mess,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.work_type_name,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    t4.templ AS state_type_templ,
    t5.task_num,
    t5.task_text,
    t5.task_state_type_id,
    t5.date_plan AS task_date_plan,
    t5.date_fact AS task_date_fact,
    t5.task_state_type_name,
    t5.task_state_type_templ,
    t5.claim_num,
    t5.claim_name,
    t5.claim_text,
    t5.claim_prj_id,
    t5.claim_project_id,
    t5.claim_module_id,
    t5.claim_module_part_id,
    t5.claim_module_version_id,
    t5.claim_client_id,
    t5.claim_priority_id,
    t5.claim_resp_user_id,
    t5.claim_date_plan,
    t5.claim_date_fact,
    t5.claim_repair_version_id,
    t5.claim_is_archive,
    t5.claim_rate,
    t5.claim_is_control,
    t5.claim_project_name,
    t5.claim_module_name,
    t5.claim_module_part_name,
    t5.claim_module_version_name,
    t5.claim_client_name,
    t5.claim_priority_name,
    t5.claim_resp_user_name,
    t5.claim_repair_version_name,
    t5.claim_state_type_id,
    t5.claim_state_type_name,
    t5.claim_state_type_templ,
    t5.prj_name,
    t5.prj_date_beg,
    t5.prj_date_end,
    t5.prj_curr_state_id,
    t5.prj_state_type_id,
    t5.prj_state_type_name,
        CASE
            WHEN t4.done_or_canceled = 0 AND GREATEST(COALESCE(t5.claim_date_plan, '2000-01-01'::date), COALESCE(t5.date_plan, '2000-01-01'::date)) < 'now'::text::date THEN true
            ELSE false
        END AS is_overdue,
    t1.date_time_beg,
    t1.date_time_end,
    t1.is_all_day,
    t5.claim_view_user_id,
    t5.claim_is_favor
   FROM cabinet.prj_work t1
     JOIN cabinet.prj_work_type t2 ON t1.work_type_id = t2.work_type_id
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id
     JOIN cabinet.vw_prj_task t5 ON t1.task_id = t5.task_id;

ALTER TABLE cabinet.vw_prj_work OWNER TO pavlov;
