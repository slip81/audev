﻿/*
	CREATE TABLE und.wa_request_data
*/

-- DROP TABLE und.wa_request_data;

CREATE TABLE und.wa_request_data
(
  request_id bigint NOT NULL,
  data_text text,
  data_binary bytea,
  CONSTRAINT wa_request_data_pkey PRIMARY KEY (request_id),
  CONSTRAINT wa_request_data_request_id_fkey FOREIGN KEY (request_id)
      REFERENCES und.wa_request (request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_data OWNER TO pavlov;

