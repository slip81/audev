﻿/*
	CREATE TABLE und.wa_task_state
*/

-- DROP TABLE und.wa_task_state;

CREATE TABLE und.wa_task_state
(
  task_state_id integer NOT NULL,
  task_state_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_task_state_pkey PRIMARY KEY (task_state_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_task_state OWNER TO pavlov;

-- DROP TRIGGER und_wa_task_state_set_stamp_trg ON und.wa_task_state;

CREATE TRIGGER und_wa_task_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_task_state
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();


INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  1,
  'Выдано',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  2,
  'Получено',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  3,
  'Выполнено успешно',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO und.wa_task_state (
  task_state_id,
  task_state_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  4,
  'Выполнено с ошибками',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);