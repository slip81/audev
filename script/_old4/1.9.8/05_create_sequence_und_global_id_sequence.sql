﻿/*
	CREATE SEQUENCE und.global_id_sequence
*/

-- DROP SEQUENCE und.global_id_sequence;

CREATE SEQUENCE und.global_id_sequence
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE und.global_id_sequence OWNER TO pavlov;
