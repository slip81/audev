﻿/*
	CREATE TABLE cabinet.prj_claim_favor
*/

-- DROP TABLE cabinet.prj_claim_favor;

CREATE TABLE cabinet.prj_claim_favor
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  claim_id integer NOT NULL,
  CONSTRAINT prj_claim_favor_pkey PRIMARY KEY (item_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_favor OWNER TO pavlov;
