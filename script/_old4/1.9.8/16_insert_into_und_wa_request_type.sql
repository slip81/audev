﻿/*
	insert into und.wa_request_type
*/

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  2,
  'АСНА: получение информации об аптеке',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  3,
  'АСНА: получение справочника связок номенклатуры',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  4,
  'АСНА: получение информации об остатках аптеки',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  5,
  'АСНА: передача полных остатков аптеки',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  6,
  'АСНА: передача изменений по остаткам аптеки',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

insert into und.wa_request_type (
  request_type_id, 
  request_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted 
)
values (
  7,
  'АСНА: удаление информации по остаткам аптеки',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

-- select * from und.wa_request_type order by request_type_id
