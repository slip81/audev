﻿/*
	CREATE TABLE und.wa_request_log
*/

-- DROP TABLE und.wa_request_log;

CREATE TABLE und.wa_request_log
(
  log_id serial NOT NULL,
  request_id bigint NOT NULL,
  log_num integer NOT NULL DEFAULT 1,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  is_error boolean NOT NULL DEFAULT false,
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  CONSTRAINT wa_request_log_pkey PRIMARY KEY (log_id),
  CONSTRAINT wa_request_log_request_id_fkey FOREIGN KEY (request_id)
      REFERENCES und.wa_request (request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa_request_log OWNER TO pavlov;

-- DROP TRIGGER und_wa_request_log_set_stamp_trg ON und.wa_request_log;

CREATE TRIGGER und_wa_request_log_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa_request_log
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

