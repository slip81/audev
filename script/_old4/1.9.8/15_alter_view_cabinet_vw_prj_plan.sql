﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_plan
*/

-- DROP VIEW cabinet.vw_prj_plan;

CREATE OR REPLACE VIEW cabinet.vw_prj_plan AS 
 SELECT t1.item_id AS plan_id,
    t1.work_id,
    t1.exec_user_id,
    t1.state_type_id,
    t1.is_plan,
    t1.date_beg,
    t1.date_end,
    t1.date_time_beg,
    t1.date_time_end,
    t1.is_all_day,
    t1.cost_plan,
    t1.cost_fact,
    t1.mess,
    t1.is_current,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t2.claim_prj_id AS prj_id,
    t2.prj_name,
    t2.task_id,
    t2.task_num,
    t2.task_text,
    t2.claim_id,
    t2.claim_num,
    t2.claim_name,
    t2.work_num,
    t2.work_type_id,
    t2.work_type_name,
    t2.is_accept,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    NULL::text AS progress,
    NULL::text AS result,
    true AS is_public,
    (((t2.claim_num::text || '-'::text) || t2.task_num::text) || '-'::text) || "left"(t2.work_type_name::text, 1) AS plan_num,
    0 AS notify_type_id,
    true AS notify_sent,
        CASE
            WHEN t4.done_or_canceled = 0 AND t1.date_end < 'now'::text::date AND t2.is_overdue THEN true
            ELSE false
        END AS is_overdue,
    NULL::text AS party_user_id_list,
    t2.claim_view_user_id,
    t2.claim_is_favor
   FROM cabinet.prj_work_exec t1
     JOIN cabinet.vw_prj_work t2 ON t1.work_id = t2.work_id
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id
UNION ALL
 SELECT t1.event_id AS plan_id,
    0 AS work_id,
    t1.exec_user_id,
    t1.state_type_id,
    true AS is_plan,
    t1.date_beg,
    t1.date_end,
    t1.date_beg AS date_time_beg,
    t1.date_end AS date_time_end,
    t1.is_all_day,
    NULL::numeric AS cost_plan,
    NULL::numeric AS cost_fact,
    NULL::character varying AS mess,
    true AS is_current,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    (-1) AS prj_id,
    NULL::character varying AS prj_name,
    0 AS task_id,
    0 AS task_num,
    t1.title AS task_text,
    0 AS claim_id,
    'С-'::text || t1.event_id::text AS claim_num,
    t1.description AS claim_name,
    0 AS work_num,
    0 AS work_type_id,
    NULL::character varying AS work_type_name,
    true AS is_accept,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    t1.progress,
    t1.result,
    t1.is_public,
    'С-'::text || t1.event_id::text AS plan_num,
    t1.notify_type_id,
    t1.notify_sent,
        CASE
            WHEN t4.done_or_canceled = 0 AND t1.date_end < 'now'::text::date THEN true
            ELSE false
        END AS is_overdue,
    t11.party_user_id_list,
    NULL::integer as claim_view_user_id,
    false as claim_is_favor
   FROM cabinet.prj_event t1
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id
     LEFT JOIN ( SELECT x1.event_id,
            array_to_string(array_agg(x1.user_id), ','::text) AS party_user_id_list
           FROM cabinet.prj_event_party x1
          GROUP BY x1.event_id) t11 ON t1.event_id = t11.event_id;

ALTER TABLE cabinet.vw_prj_plan OWNER TO pavlov;
