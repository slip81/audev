﻿/*
	CREATE TABLE und.wa
*/

-- DROP TABLE und.wa;

CREATE TABLE und.wa
(
  wa_id integer NOT NULL,
  wa_name character varying,
  wa_descr character varying,
  address_main character varying,
  address_test character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT und.sysuidgen_scope7(),
  CONSTRAINT wa_pkey PRIMARY KEY (wa_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE und.wa OWNER TO pavlov;

-- DROP TRIGGER und_wa_set_stamp_trg ON und.wa;

CREATE TRIGGER und_wa_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON und.wa
  FOR EACH ROW
  EXECUTE PROCEDURE und.set_stamp();

INSERT INTO und.wa (
  wa_id,
  wa_name,
  wa_descr,
  address_main,
  address_test,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  1,
  'auesnapi',
  'ЕСН 2.0',
  'https://service.aptekaural.ru/auesnapi',
  'http://192.168.0.101/auesnapi',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

