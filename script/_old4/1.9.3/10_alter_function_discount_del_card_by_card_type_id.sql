﻿/*
	CREATE OR REPLACE FUNCTION discount.del_card_by_card_type_id
*/

-- DROP FUNCTION discount.del_card_by_card_type_id(bigint);

CREATE OR REPLACE FUNCTION discount.del_card_by_card_type_id(
    IN _card_type_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
DECLARE
    _card_id bigint;
BEGIN
	result:= 0;
	
	delete from discount.card_nominal_uncommitted t1 using discount.card t2
	where t1.card_id = t2.card_id
	and t2.curr_card_type_id = _card_type_id;
	
	delete from discount.card_nominal t1 using discount.card t2
	where t1.card_id = t2.card_id
	and t2.curr_card_type_id = _card_type_id;	

	delete from discount.card_transaction_unit t1 using discount.card_transaction t2, discount.card t3
	where t1.card_trans_id = t2.card_trans_id
	and t2.card_id = t3.card_id
	and t3.curr_card_type_id = _card_type_id;
	
	delete from discount.card_transaction_detail t1 using discount.card_transaction t2, discount.card t3
	where t1.card_trans_id = t2.card_trans_id
	and t2.card_id = t3.card_id
	and t3.curr_card_type_id = _card_type_id;	
	
	delete from discount.card_transaction t1 using discount.card t2
	where t1.card_id = t2.card_id
	and t2.curr_card_type_id = _card_type_id;	
	
	delete from discount.card_card_status_rel t1 using discount.card t2
	where t1.card_id = t2.card_id
	and t2.curr_card_type_id = _card_type_id;
	
	delete from discount.card_card_type_rel t1 using discount.card t2
	where t1.card_id = t2.card_id
	and t2.curr_card_type_id = _card_type_id;
	
	delete from discount.card_holder_card_rel t1 using discount.card t2
	where t1.card_id = t2.card_id
	and t2.curr_card_type_id = _card_type_id;	

	delete from discount.card where curr_card_type_id = _card_type_id;	
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION discount.del_card_by_card_type_id(bigint)
  OWNER TO pavlov;
