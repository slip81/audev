﻿/*
	UPDATE cabinet.xxx SET xxx_name = '[н/о]'
*/

UPDATE cabinet.cab_user SET user_name = '[н/о]' WHERE user_id = 0;
UPDATE cabinet.crm_client SET client_name = '[н/о]' WHERE client_name like '%не опред%' OR client_name like '%не указа%';
UPDATE cabinet.crm_module SET module_name = '[н/о]' WHERE module_name like '%не опред%' OR module_name like '%не указа%';
UPDATE cabinet.crm_module_part SET module_part_name = '[н/о]' WHERE module_part_name like '%не опред%' OR module_part_name like '%не указа%';
UPDATE cabinet.crm_module_version SET module_version_name = '[н/о]' WHERE module_version_name like '%не опред%' OR module_version_name like '%не указа%';
UPDATE cabinet.crm_priority SET priority_name = '[н/о]' WHERE priority_name like '%не опред%' OR priority_name like '%не указа%';
UPDATE cabinet.crm_project SET project_name = '[н/о]' WHERE project_name like '%не опред%' OR project_name like '%не указа%';
UPDATE cabinet.crm_state SET state_name = '[н/о]' WHERE state_name like '%не опред%' OR state_name like '%не указа%';


--select * from cabinet.cab_user order by user_id