﻿/*
	ALTER TABLE discount.card_type_check_info ADD COLUMNs print_xxx
*/

ALTER TABLE discount.card_type_check_info ADD COLUMN print_used_bonus_value_trans boolean NOT NULL DEFAULT false;
ALTER TABLE discount.card_type_check_info ADD COLUMN print_programm_descr boolean NOT NULL DEFAULT false;
ALTER TABLE discount.card_type_check_info ADD COLUMN print_card_num boolean NOT NULL DEFAULT false;

ALTER TABLE discount.card_type_check_info ADD COLUMN print_cert_sum_card boolean NOT NULL DEFAULT false;
ALTER TABLE discount.card_type_check_info ADD COLUMN print_cert_sum_trans boolean NOT NULL DEFAULT false;
ALTER TABLE discount.card_type_check_info ADD COLUMN print_cert_sum_left boolean NOT NULL DEFAULT false;
