﻿/*
	ALTER TABLE cabinet.story ADD COLUMNs claim_id, claim_num
*/

ALTER TABLE cabinet.story ADD COLUMN claim_id integer;
ALTER TABLE cabinet.story ADD COLUMN claim_num character varying;

UPDATE cabinet.story
SET claim_id = (SELECT x1.claim_id FROM cabinet.prj_claim x1 WHERE x1.old_task_id = cabinet.story.task_id)
WHERE task_id is not null;

UPDATE cabinet.story
SET claim_num = (SELECT x1.claim_num FROM cabinet.prj_claim x1 WHERE x1.old_task_id = cabinet.story.task_id)
WHERE task_id is not null;

-- ALTER TABLE cabinet.story DROP COLUMN task_id;
-- ALTER TABLE cabinet.story DROP COLUMN task_num;