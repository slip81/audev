﻿/*
	insert into cabinet.cab_grid 3,4,5,6
*/


insert into cabinet.cab_grid (grid_id, grid_name)
values (3, 'prjGrid');

insert into cabinet.cab_grid (grid_id, grid_name)
values (4, 'prjClaimGrid');

insert into cabinet.cab_grid (grid_id, grid_name)
values (5, 'prjTaskGrid');

insert into cabinet.cab_grid (grid_id, grid_name)
values (6, 'prjWorkGrid');

-- select * from cabinet.cab_grid