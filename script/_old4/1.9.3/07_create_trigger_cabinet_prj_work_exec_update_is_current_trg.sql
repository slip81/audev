﻿/*
	CREATE OR REPLACE FUNCTION cabinet.prj_work_exec_update_is_current()
*/

-- DROP FUNCTION cabinet.prj_work_exec_update_is_current();

CREATE OR REPLACE FUNCTION cabinet.prj_work_exec_update_is_current()
  RETURNS trigger AS
$BODY$
BEGIN   
    IF (TG_OP = 'DELETE') THEN
      IF (OLD.is_current = true) THEN
        UPDATE cabinet.prj_work_exec SET is_current = true WHERE work_id = OLD.work_id AND item_id = (SELECT MAX(item_id) FROM cabinet.prj_work_exec WHERE work_id = OLD.work_id);
      END IF;
      RETURN OLD;
    ELSIF (TG_OP = 'INSERT') THEN
      UPDATE cabinet.prj_work_exec SET is_current = false WHERE work_id = NEW.work_id AND item_id != NEW.item_id;
      RETURN NEW;
    END IF;
    RETURN NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION cabinet.prj_work_exec_update_is_current() OWNER TO postgres;


-- DROP TRIGGER cabinet_prj_work_exec_update_is_current_trg ON cabinet.prj_work_exec;

CREATE TRIGGER cabinet_prj_work_exec_update_is_current_trg
  AFTER INSERT OR DELETE
  ON cabinet.prj_work_exec
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.prj_work_exec_update_is_current();
