﻿/*
	CREATE INDEXs discount_xxx
*/


-- DROP INDEX discount.discount_card_nominal_card_trans_id1_idx;

CREATE INDEX discount_card_nominal_card_trans_id1_idx
  ON discount.card_nominal
  USING btree
  (card_trans_id);

-- DROP INDEX discount.discount_card_nominal_uncommitted_card_trans_id1_idx;

CREATE INDEX discount_card_nominal_uncommitted_card_trans_id1_idx
  ON discount.card_nominal_uncommitted
  USING btree
  (card_trans_id); 

-- DROP INDEX discount.discount_card_transaction_detail_card_trans_id1_idx;

CREATE INDEX discount_card_transaction_detail_card_trans_id1_idx
  ON discount.card_transaction_detail
  USING btree
  (card_trans_id);   

-- DROP INDEX discount.discount_card_transaction_unit_card_trans_id1_idx;

CREATE INDEX discount_card_transaction_unit_card_trans_id1_idx
  ON discount.card_transaction_unit
  USING btree
  (card_trans_id);   


--------------------------------------

-- DROP INDEX discount.discount_card_transaction_unit_card_id1_idx;

CREATE INDEX discount_card_transaction_unit_card_id1_idx
  ON discount.card_additional_num
  USING btree
  (card_id);

-- DROP INDEX discount.discount_card_card_status_rel_card_id1_idx;

CREATE INDEX discount_card_card_status_rel_card_id1_idx
  ON discount.card_card_status_rel
  USING btree
  (card_id);  

-- DROP INDEX discount.discount_card_card_type_rel_card_id1_idx;

CREATE INDEX discount_card_card_type_rel_card_id1_idx
  ON discount.card_card_type_rel
  USING btree
  (card_id);    

-- DROP INDEX discount.discount_card_holder_card_rel_card_id1_idx;

CREATE INDEX discount_card_holder_card_rel_card_id1_idx
  ON discount.card_holder_card_rel
  USING btree
  (card_id);   

-- DROP INDEX discount.discount_card_nominal_card_id1_idx;

CREATE INDEX discount_card_nominal_card_id1_idx
  ON discount.card_nominal
  USING btree
  (card_id);    

-- DROP INDEX discount.discount_card_nominal_inactive_card_id1_idx;

CREATE INDEX discount_card_nominal_inactive_card_id1_idx
  ON discount.card_nominal_inactive
  USING btree
  (card_id);    

-- DROP INDEX discount.discount_card_transaction_card_id1_idx;

CREATE INDEX discount_card_transaction_card_id1_idx
  ON discount.card_transaction
  USING btree
  (card_id);    