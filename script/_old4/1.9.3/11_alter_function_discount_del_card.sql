﻿/*
	CREATE OR REPLACE FUNCTION discount.del_card
*/

-- DROP FUNCTION discount.del_card(bigint);

CREATE OR REPLACE FUNCTION discount.del_card(
    IN _card_id bigint,
    OUT result bigint)
  RETURNS bigint AS
$BODY$
BEGIN
	delete from discount.card_nominal_uncommitted where card_id = _card_id;

	delete from discount.card_nominal where card_id = _card_id;

	delete from discount.card_transaction_unit t1 using discount.card_transaction t2
	where t1.card_trans_id = t2.card_trans_id
	and t2.card_id = _card_id;	
		
	delete from discount.card_transaction_detail t1 using discount.card_transaction t2
	where t1.card_trans_id = t2.card_trans_id
	and t2.card_id = _card_id;	

	delete from discount.card_transaction where card_id = _card_id;

	delete from discount.card_card_status_rel where card_id = _card_id;

	delete from discount.card_card_type_rel where card_id = _card_id;

	delete from discount.card_holder_card_rel where card_id = _card_id;

	delete from discount.card where card_id = _card_id;

	result:= 0;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION discount.del_card(bigint) OWNER TO pavlov;
