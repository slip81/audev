﻿/*
	CREATE TABLEs and VIEWs cabinet.prj_xxx
*/


	/*
	new fields:
	prj.upd_num
	prj_claim.old_task_id
	prj_task.old_task_id
	prj_task.old_subtask_id
	prj_work.old_task_id
	prj_work.old_subtask_id
	prj_work.state_type_id
	prj_work.mess
	prj_work.is_plan

	del fields:
	prj_claim.cost_plan
	prj_claim.cost_fact
	prj_task.state_id
	prj_task.cost_plan
	prj_task.cost_fact
	prj_work.state_id

	new tables:
	prj_task_state_type
	prj_work_state_type
	prj_mess
	prj_log

	del tables:
	prj_work_type_set_item
	prj_work_type_set
	*/

	-- select * from cabinet.prj_state_type
	/*
	1;"Подготовка"
	2;"Активен"
	3;"Завершен"
	4;"В архиве"
	*/
	

	-- select * from cabinet.prj_claim_state_type
	/*
	1;"Неразобрано"
	2;"Активно"
	3;"Выполнено"
	4;"Отменено"
	5;"Частично выполнено"
	*/
	
	-- select * from cabinet.prj_task_state_type
	/*
	1;"Неразобрано"
	2;"Активно"
	3;"Выполнено"
	4;"Отменено"
	5;"Частично выполнено"
	*/
	
	-- select * from cabinet.prj_work_state_type
	/*
	1;"Активно"
	2;"Выполнено"
	3;"Отменено"
	4;"Возвращено"
	*/
	
	-- select * from cabinet.prj_work_type order by work_type_id
	/*
	1;"Рассмотрение"
	2;"Проектирование"
	3;"Программирование"
	4;"Тестирование"
	5;"Документирование"
	*/

----------------------------------------


DROP VIEW cabinet.vw_prj_log;
DROP VIEW cabinet.vw_prj_mess;
DROP TABLE cabinet.prj_log;
DROP TABLE cabinet.prj_mess;


DROP VIEW cabinet.vw_prj_plan;
DROP VIEW cabinet.vw_prj_work;
DROP VIEW cabinet.vw_prj_task;
DROP VIEW cabinet.vw_prj_claim;
DROP VIEW cabinet.vw_prj;
DROP VIEW cabinet.vw_prj_claim_simple;
DROP VIEW cabinet.vw_prj_task_simple;
DROP VIEW cabinet.vw_prj_work_simple;
DROP VIEW cabinet.vw_prj_work_default;

DROP TABLE cabinet.prj_default;
DROP TABLE cabinet.prj_claim_default;
DROP TABLE cabinet.prj_work_default;
DROP TABLE cabinet.prj_event;
DROP TABLE cabinet.prj_work_exec;
DROP TABLE cabinet.prj_work;
DROP TABLE cabinet.prj_task;
DROP TABLE cabinet.prj_set_approved;
DROP TABLE cabinet.prj_set_new;
DROP TABLE cabinet.prj_claim;
DROP TABLE cabinet.prj_state;
DROP TABLE cabinet.prj;
--DROP TABLE cabinet.prj_work_type_set_item;
--DROP TABLE cabinet.prj_work_type_set;
DROP TABLE cabinet.prj_work_type;
DROP TABLE cabinet.prj_state_type;
DROP TABLE cabinet.prj_claim_state_type;
DROP TABLE cabinet.prj_user_settings;


DROP TABLE cabinet.prj_task_state_type;
DROP TABLE cabinet.prj_work_state_type;


----------------------------------------

-- DROP TABLE cabinet.prj_state_type;

CREATE TABLE cabinet.prj_state_type
(
  state_type_id integer NOT NULL,
  state_type_name character varying,
  templ character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),  
  CONSTRAINT prj_state_type_pkey PRIMARY KEY (state_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_state_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_state_type_set_stamp_trg ON cabinet.prj_state_type;

CREATE TRIGGER cabinet_prj_state_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_state_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj;

CREATE TABLE cabinet.prj
(
  prj_id serial NOT NULL,
  prj_name character varying,
  date_beg date,
  date_end date,
  curr_state_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_pkey PRIMARY KEY (prj_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_set_stamp_trg ON cabinet.prj;

CREATE TRIGGER cabinet_prj_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_state;

CREATE TABLE cabinet.prj_state
(
  state_id serial NOT NULL,
  prj_id integer NOT NULL,
  state_type_id integer NOT NULL,
  date_beg date,
  date_end date,
  crt_date timestamp without time zone,
  crt_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_state_pkey PRIMARY KEY (state_id),
  CONSTRAINT prj_state_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_state_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES cabinet.prj_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_state OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_state_set_stamp_trg ON cabinet.prj_state;

CREATE TRIGGER cabinet_prj_state_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_state
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_claim;

CREATE TABLE cabinet.prj_claim
(
  claim_id serial NOT NULL,
  claim_num character varying,
  claim_name character varying,
  claim_text character varying,
  prj_id integer NOT NULL,
  project_id integer NOT NULL,
  module_id integer NOT NULL,
  module_part_id integer NOT NULL,
  module_version_id integer NOT NULL,
  client_id integer NOT NULL,
  priority_id integer NOT NULL,
  resp_user_id integer NOT NULL,
  date_plan date,
  date_fact date,
  repair_version_id integer,
  is_archive boolean NOT NULL DEFAULT false,
  rate numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  is_control boolean NOT NULL DEFAULT false,
  old_task_id integer,
  old_group_id integer,
  old_state_id integer,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_claim_pkey PRIMARY KEY (claim_id),
  CONSTRAINT prj_claim_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.crm_client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_module_id_fkey FOREIGN KEY (module_id)
      REFERENCES cabinet.crm_module (module_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_module_part_id_fkey FOREIGN KEY (module_part_id)
      REFERENCES cabinet.crm_module_part (module_part_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_module_version_id_fkey FOREIGN KEY (module_version_id)
      REFERENCES cabinet.crm_module_version (module_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_priority_id_fkey FOREIGN KEY (priority_id)
      REFERENCES cabinet.crm_priority (priority_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_repair_version_id_fkey FOREIGN KEY (repair_version_id)
      REFERENCES cabinet.crm_module_version (module_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_resp_user_id_fkey FOREIGN KEY (resp_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_claim_set_stamp_trg ON cabinet.prj_claim;

CREATE TRIGGER cabinet_prj_claim_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_claim
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_set_approved;

CREATE TABLE cabinet.prj_set_approved
(
  item_id serial NOT NULL,
  prj_id integer NOT NULL,
  claim_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_set_approved_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_set_approved_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_set_approved_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_set_approved OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_set_approved_set_stamp_trg ON cabinet.prj_set_approved;

CREATE TRIGGER cabinet_prj_set_approved_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_set_approved
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_set_new;

CREATE TABLE cabinet.prj_set_new
(
  item_id serial NOT NULL,
  prj_id integer NOT NULL,
  claim_id integer NOT NULL,
  crt_date timestamp without time zone,
  crt_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_set_new_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_set_new_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_set_new_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_set_new OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_set_new_set_stamp_trg ON cabinet.prj_set_new;

CREATE TRIGGER cabinet_prj_set_new_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_set_new
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_claim_state_type;

CREATE TABLE cabinet.prj_claim_state_type
(
  state_type_id integer NOT NULL,
  state_type_name character varying,
  templ character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),  
  CONSTRAINT prj_claim_state_type_pkey PRIMARY KEY (state_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_state_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_claim_state_type_set_stamp_trg ON cabinet.prj_claim_state_type;

CREATE TRIGGER cabinet_prj_claim_state_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_claim_state_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_task;

CREATE TABLE cabinet.prj_task
(
  task_id serial NOT NULL,
  claim_id integer NOT NULL,
  task_num integer NOT NULL,
  task_text character varying,  
  date_plan date,
  date_fact date,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  old_task_id integer,
  old_subtask_id integer,
  old_state_id integer,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_task_pkey PRIMARY KEY (task_id),
  CONSTRAINT prj_task_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_task OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_task_set_stamp_trg ON cabinet.prj_task;

CREATE TRIGGER cabinet_prj_task_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_task
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_task_state_type;

CREATE TABLE cabinet.prj_task_state_type
(
  state_type_id integer NOT NULL,
  state_type_name character varying,
  templ character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),  
  CONSTRAINT prj_task_state_type_pkey PRIMARY KEY (state_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_task_state_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_task_state_type_set_stamp_trg ON cabinet.prj_task_state_type;

CREATE TRIGGER cabinet_prj_task_state_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_task_state_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_work_type;

CREATE TABLE cabinet.prj_work_type
(
  work_type_id integer NOT NULL,
  work_type_name character varying,
  is_default boolean NOT NULL DEFAULT false,
  is_accept_default boolean NOT NULL DEFAULT false,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_type_pkey PRIMARY KEY (work_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_type_set_stamp_trg ON cabinet.prj_work_type;

CREATE TRIGGER cabinet_prj_work_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_work_state_type;

CREATE TABLE cabinet.prj_work_state_type
(
  state_type_id integer NOT NULL,
  state_type_name character varying,
  templ character varying,
  done_or_canceled integer,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),  
  CONSTRAINT prj_work_state_type_pkey PRIMARY KEY (state_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work_state_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_state_type_set_stamp_trg ON cabinet.prj_work_state_type;

CREATE TRIGGER cabinet_prj_work_state_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work_state_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_work;

CREATE TABLE cabinet.prj_work
(
  work_id serial NOT NULL,
  task_id integer NOT NULL,
  claim_id integer NOT NULL,
  work_num integer NOT NULL,
  work_type_id integer NOT NULL,
  exec_user_id integer NOT NULL,
  state_type_id integer NOT NULL,
  is_plan boolean NOT NULL DEFAULT false,
  date_beg timestamp without time zone,
  date_end timestamp without time zone,
  is_accept boolean NOT NULL DEFAULT false,
  cost_plan numeric,
  cost_fact numeric,  
  mess character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,  
  old_task_id integer,
  old_subtask_id integer,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_pkey PRIMARY KEY (work_id),
  CONSTRAINT prj_work_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES cabinet.prj_work_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_work_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_work_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_work_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.prj_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_work_work_type_id_fkey FOREIGN KEY (work_type_id)
      REFERENCES cabinet.prj_work_type (work_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_set_stamp_trg ON cabinet.prj_work;

CREATE TRIGGER cabinet_prj_work_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_work_exec;

CREATE TABLE cabinet.prj_work_exec
(
  item_id serial NOT NULL,
  work_id integer NOT NULL,
  exec_user_id integer NOT NULL,
  state_type_id integer NOT NULL,
  is_plan boolean NOT NULL DEFAULT false,
  date_beg timestamp without time zone NOT NULL,
  date_end timestamp without time zone NOT NULL,
  date_time_beg timestamp without time zone NOT NULL,
  date_time_end timestamp without time zone NOT NULL,
  is_all_day boolean NOT NULL DEFAULT false,
  cost_plan numeric,
  cost_fact numeric,  
  mess character varying,
  is_current boolean NOT NULL DEFAULT true,
  crt_date timestamp without time zone,
  crt_user character varying,  
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_exec_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_work_exec_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_work_exec_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES cabinet.prj_work_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_work_exec_work_id_fkey FOREIGN KEY (work_id)
      REFERENCES cabinet.prj_work (work_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work_exec OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_exec_set_stamp_trg ON cabinet.prj_work_exec;

CREATE TRIGGER cabinet_prj_work_exec_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work_exec
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_mess;

CREATE TABLE cabinet.prj_mess
(
  mess_id serial NOT NULL,  
  mess character varying,
  mess_num character varying,  
  user_id integer NOT NULL,
  prj_id integer,
  claim_id integer,
  task_id integer,  
  work_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,  
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_mess_pkey PRIMARY KEY (mess_id),
  CONSTRAINT prj_mess_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT prj_mess_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_mess_claim_id_fkey FOREIGN KEY (claim_id)
      REFERENCES cabinet.prj_claim (claim_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,      
  CONSTRAINT prj_mess_task_id_fkey FOREIGN KEY (task_id)
      REFERENCES cabinet.prj_task (task_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,            
  CONSTRAINT prj_mess_work_id_fkey FOREIGN KEY (work_id)
      REFERENCES cabinet.prj_work (work_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_mess OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_mess_set_stamp_trg ON cabinet.prj_mess;

CREATE TRIGGER cabinet_prj_mess_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_mess
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------
-- DROP TABLE cabinet.prj_log;

CREATE TABLE cabinet.prj_log
(
  log_id serial NOT NULL,  
  mess character varying,
  prj_id integer,
  claim_id integer,
  task_id integer,  
  work_id integer,  
  crt_date timestamp without time zone,
  crt_user character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_log_pkey PRIMARY KEY (log_id)      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_log OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_log_set_stamp_trg ON cabinet.prj_log;

CREATE TRIGGER cabinet_prj_log_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_log
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------
-- DROP TABLE cabinet.prj_user_settings;

CREATE TABLE cabinet.prj_user_settings
(
  user_id integer NOT NULL,  
  settings character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_user_settings_pkey PRIMARY KEY (user_id),
  CONSTRAINT prj_user_settings_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_user_settings OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_user_settings_set_stamp_trg ON cabinet.prj_user_settings;

CREATE TRIGGER cabinet_prj_user_settings_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_user_settings
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_default;

CREATE TABLE cabinet.prj_default
(
  user_id integer NOT NULL,  
  start_page character varying,   
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_default_pkey PRIMARY KEY (user_id)  
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_default OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_default_set_stamp_trg ON cabinet.prj_default;

CREATE TRIGGER cabinet_prj_default_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_default
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------  

-- DROP TABLE cabinet.prj_claim_default;

CREATE TABLE cabinet.prj_claim_default
(
  user_id integer NOT NULL,  
  prj_id integer NOT NULL DEFAULT 0,
  project_id integer NOT NULL DEFAULT 0,
  module_id integer NOT NULL DEFAULT 0,
  module_part_id integer NOT NULL DEFAULT 0,
  module_version_id integer NOT NULL DEFAULT 0,
  client_id integer NOT NULL DEFAULT 0,
  priority_id integer NOT NULL DEFAULT 0,
  resp_user_id integer NOT NULL DEFAULT 0,
  repair_version_id integer,
  rate numeric,    
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_claim_default_pkey PRIMARY KEY (user_id),
  CONSTRAINT prj_claim_default_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_client_id_fkey FOREIGN KEY (client_id)
      REFERENCES cabinet.crm_client (client_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_module_id_fkey FOREIGN KEY (module_id)
      REFERENCES cabinet.crm_module (module_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_module_part_id_fkey FOREIGN KEY (module_part_id)
      REFERENCES cabinet.crm_module_part (module_part_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_module_version_id_fkey FOREIGN KEY (module_version_id)
      REFERENCES cabinet.crm_module_version (module_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_priority_id_fkey FOREIGN KEY (priority_id)
      REFERENCES cabinet.crm_priority (priority_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_prj_id_fkey FOREIGN KEY (prj_id)
      REFERENCES cabinet.prj (prj_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_project_id_fkey FOREIGN KEY (project_id)
      REFERENCES cabinet.crm_project (project_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_repair_version_id_fkey FOREIGN KEY (repair_version_id)
      REFERENCES cabinet.crm_module_version (module_version_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT prj_claim_default_resp_user_id_fkey FOREIGN KEY (resp_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_claim_default OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_claim_default_set_stamp_trg ON cabinet.prj_claim_default;

CREATE TRIGGER cabinet_prj_claim_default_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_claim_default
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_work_default;

CREATE TABLE cabinet.prj_work_default
(
  item_id serial NOT NULL,
  user_id integer NOT NULL,
  work_type_id integer NOT NULL,
  is_accept boolean NOT NULL DEFAULT false,
  exec_user_id integer NOT NULL DEFAULT 0, 
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_work_default_pkey PRIMARY KEY (item_id),
  CONSTRAINT prj_work_default_pkey_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION, 
  CONSTRAINT prj_work_default_pkey_work_type_id_fkey FOREIGN KEY (work_type_id)
      REFERENCES cabinet.prj_work_type (work_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,       
  CONSTRAINT prj_work_default_pkey_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_work_default OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_work_default_set_stamp_trg ON cabinet.prj_work_default;

CREATE TRIGGER cabinet_prj_work_default_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_work_default
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

-- DROP TABLE cabinet.prj_event;

CREATE TABLE cabinet.prj_event
(
  event_id integer NOT NULL DEFAULT nextval('cabinet.prj_work_exec_item_id_seq'::regclass),
  date_beg timestamp without time zone NOT NULL,  
  date_end timestamp without time zone NOT NULL,
  is_all_day boolean NOT NULL DEFAULT false,  
  title text,
  description text,
  progress text,
  result text,
  exec_user_id integer NOT NULL DEFAULT 0,
  state_type_id integer NOT NULL DEFAULT 0,
  is_public boolean NOT NULL DEFAULT false,      
  old_event_id integer,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  upd_num integer NOT NULL DEFAULT 0,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope8(),
  CONSTRAINT prj_event_pkey PRIMARY KEY (event_id),
  CONSTRAINT prj_event_exec_user_id_fkey FOREIGN KEY (exec_user_id)
      REFERENCES cabinet.cab_user (user_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,  
  CONSTRAINT prj_event_state_type_id_fkey FOREIGN KEY (state_type_id)
      REFERENCES cabinet.prj_work_state_type (state_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.prj_event OWNER TO pavlov;

-- DROP TRIGGER cabinet_prj_event_set_stamp_trg ON cabinet.prj_event;

CREATE TRIGGER cabinet_prj_event_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.prj_event
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

----------------------------------------

INSERT INTO cabinet.prj_state_type (state_type_id, state_type_name, templ)
VALUES (1, 'Подготовка', '<span class="text-primary">Подготовка</span>');

INSERT INTO cabinet.prj_state_type (state_type_id, state_type_name, templ)
VALUES (2, 'Активен', '<span class="text-success">Активен</span>');

INSERT INTO cabinet.prj_state_type (state_type_id, state_type_name, templ)
VALUES (3, 'Завершен', '<span><em>Завершен</em></span>');
----------------------------------------

INSERT INTO cabinet.prj_claim_state_type (state_type_id, state_type_name, templ)
VALUES (1, 'Неразобрано', '<span class="text-danger">Неразобрано</span>');

INSERT INTO cabinet.prj_claim_state_type (state_type_id, state_type_name, templ)
VALUES (2, 'Активно', '<span class="text-success">Активно</span>');

INSERT INTO cabinet.prj_claim_state_type (state_type_id, state_type_name, templ)
VALUES (3, 'Выполнено', '<span class="text-primary">Выполнено</span>');

INSERT INTO cabinet.prj_claim_state_type (state_type_id, state_type_name, templ)
VALUES (4, 'Отменено', '<span class="text-warning">Отменено</span>');

INSERT INTO cabinet.prj_claim_state_type (state_type_id, state_type_name, templ)
VALUES (5, 'Частично выполнено', '<span class="text-info">Частично выполнено</span>');

----------------------------------------

INSERT INTO cabinet.prj_task_state_type (state_type_id, state_type_name, templ)
VALUES (1, 'Неразобрано', '<span class="text-danger">Неразобрано</span>');

INSERT INTO cabinet.prj_task_state_type (state_type_id, state_type_name, templ)
VALUES (2, 'Активно', '<span class="text-success">Активно</span>');

INSERT INTO cabinet.prj_task_state_type (state_type_id, state_type_name, templ)
VALUES (3, 'Выполнено', '<span class="text-primary">Выполнено</span>');

INSERT INTO cabinet.prj_task_state_type (state_type_id, state_type_name, templ)
VALUES (4, 'Отменено', '<span class="text-warning">Отменено</span>');

INSERT INTO cabinet.prj_task_state_type (state_type_id, state_type_name, templ)
VALUES (5, 'Частично выполнено', '<span class="text-info">Частично выполнено</span>');

----------------------------------------

INSERT INTO cabinet.prj_work_type (work_type_id, work_type_name, is_default, is_accept_default)
VALUES (1, 'Рассмотрение', true, true);

INSERT INTO cabinet.prj_work_type (work_type_id, work_type_name, is_default, is_accept_default)
VALUES (2, 'Проектирование', false, false);

INSERT INTO cabinet.prj_work_type (work_type_id, work_type_name, is_default, is_accept_default)
VALUES (3, 'Программирование', true, false);

INSERT INTO cabinet.prj_work_type (work_type_id, work_type_name, is_default, is_accept_default)
VALUES (4, 'Тестирование', true, false);

INSERT INTO cabinet.prj_work_type (work_type_id, work_type_name, is_default, is_accept_default)
VALUES (5, 'Документирование', false, false);

INSERT INTO cabinet.prj_work_type (work_type_id, work_type_name, is_default, is_accept_default)
VALUES (6, 'Консультирование', false, false);

----------------------------------------

INSERT INTO cabinet.prj_work_state_type (state_type_id, state_type_name, templ, done_or_canceled)
VALUES (1, 'Активно', '<span class="text-success">Активно</span>', 0);

INSERT INTO cabinet.prj_work_state_type (state_type_id, state_type_name, templ, done_or_canceled)
VALUES (2, 'Выполнено', '<span class="text-primary">Выполнено</span>', 1);

INSERT INTO cabinet.prj_work_state_type (state_type_id, state_type_name, templ, done_or_canceled)
VALUES (3, 'Отменено', '<span class="text-warning">Отменено</span>', 2);

INSERT INTO cabinet.prj_work_state_type (state_type_id, state_type_name, templ, done_or_canceled)
VALUES (4, 'Возвращено', '<span class="text-danger">Возвращено</span>', 0);

----------------------------------------

-- DROP VIEW cabinet.vw_prj_work_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_work_simple AS 
 SELECT t1.work_id,
    t1.task_id,
    t1.claim_id,
    t1.state_type_id,
    t2.done_or_canceled
   FROM cabinet.prj_work t1
     JOIN cabinet.prj_work_state_type t2 ON t1.state_type_id = t2.state_type_id;

ALTER TABLE cabinet.vw_prj_work_simple OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj_task_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_task_simple AS 
 SELECT t1.task_id,
    t1.claim_id,
        CASE
            WHEN COALESCE(t11.work_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND COALESCE(t11.active_work_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND t11.work_cnt = t11.done_work_cnt THEN 3
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND t11.work_cnt = t11.canceled_work_cnt THEN 4
            WHEN COALESCE(t11.work_cnt, 0::bigint) > 0 AND COALESCE(t11.active_work_cnt, 0::bigint) <= 0 AND COALESCE(t11.done_work_cnt, 0::bigint) > 0 AND COALESCE(t11.canceled_work_cnt, 0::bigint) > 0 THEN 5
            ELSE 1
        END AS task_state_type_id,    
    t11.work_cnt,
    t11.active_work_cnt,
    t11.done_work_cnt,
    t11.canceled_work_cnt
   FROM cabinet.prj_task t1
     LEFT JOIN ( SELECT count(x1.work_id) AS work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 0 THEN 1
                    ELSE 0
                END) AS active_work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 1 THEN 1
                    ELSE 0
                END) AS done_work_cnt,
            sum(
                CASE
                    WHEN x1.done_or_canceled = 2 THEN 1
                    ELSE 0
                END) AS canceled_work_cnt,
            x1.task_id
           FROM cabinet.vw_prj_work_simple x1
          GROUP BY x1.task_id) t11 ON t1.task_id = t11.task_id;

ALTER TABLE cabinet.vw_prj_task_simple OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj_claim_simple;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim_simple AS 
 SELECT t1.claim_id,
    t1.prj_id,
        CASE
            WHEN COALESCE(t11.task_cnt, 0::bigint) <= 0 THEN 1
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) > 0 THEN 2
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.done_task_cnt THEN 3
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.canceled_task_cnt THEN 4
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND t11.task_cnt = t11.part_task_cnt THEN 5
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t11.done_task_cnt, 0::bigint) > 0 AND COALESCE(t11.canceled_task_cnt, 0::bigint) > 0 THEN 5
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t11.done_task_cnt, 0::bigint) > 0 AND COALESCE(t11.part_task_cnt, 0::bigint) > 0 THEN 5
            WHEN COALESCE(t11.task_cnt, 0::bigint) > 0 AND COALESCE(t11.active_task_cnt, 0::bigint) <= 0 AND COALESCE(t11.part_task_cnt, 0::bigint) > 0 AND COALESCE(t11.canceled_task_cnt, 0::bigint) > 0 THEN 5
            ELSE 1
        END AS claim_state_type_id,
    t11.task_cnt,
    t11.new_task_cnt,
    t11.active_task_cnt,
    t11.done_task_cnt,
    t11.canceled_task_cnt,
    t11.part_task_cnt
   FROM cabinet.prj_claim t1
     LEFT JOIN ( SELECT x1.claim_id, COUNT(x1.task_id) AS task_cnt,
       SUM(CASE WHEN x1.task_state_type_id = 1 THEN 1 ELSE 0 END) AS new_task_cnt,
       SUM(CASE WHEN x1.task_state_type_id = 2 THEN 1 ELSE 0 END) AS active_task_cnt,
       SUM(CASE WHEN x1.task_state_type_id = 3 THEN 1 ELSE 0 END) AS done_task_cnt,
       SUM(CASE WHEN x1.task_state_type_id = 4 THEN 1 ELSE 0 END) AS canceled_task_cnt,
       SUM(CASE WHEN x1.task_state_type_id = 5 THEN 1 ELSE 0 END) AS part_task_cnt
     FROM cabinet.vw_prj_task_simple x1
     GROUP BY x1.claim_id) t11 ON t1.claim_id = t11.claim_id;

ALTER TABLE cabinet.vw_prj_claim_simple OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj;

CREATE OR REPLACE VIEW cabinet.vw_prj AS 
 SELECT t1.prj_id,
    t1.prj_name,
    t1.date_beg,
    t1.date_end,
    t1.curr_state_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    COALESCE(t11.claim_cnt, 0::bigint) AS claim_cnt,
    COALESCE(t11.new_claim_cnt, 0::bigint) AS new_claim_cnt,
    COALESCE(t11.active_claim_cnt, 0::bigint) AS active_claim_cnt,
    COALESCE(t11.done_claim_cnt, 0::bigint) AS done_claim_cnt,
    COALESCE(t11.canceled_claim_cnt, 0::bigint) AS canceled_claim_cnt,
    COALESCE(t11.part_claim_cnt, 0::bigint) AS part_claim_cnt,
    t12.state_type_id,
    t13.state_type_name,
    t13.templ AS state_type_name_templ,
    COALESCE(t14.claim_approved_cnt, 0::bigint) AS claim_approved_cnt,
    COALESCE(t15.claim_new_cnt, 0::bigint) AS claim_new_cnt
   FROM cabinet.prj t1
     LEFT JOIN ( SELECT x1.prj_id, COUNT(x1.claim_id) AS claim_cnt,
       SUM(CASE WHEN x1.claim_state_type_id = 1 THEN 1 ELSE 0 END) AS new_claim_cnt,
       SUM(CASE WHEN x1.claim_state_type_id = 2 THEN 1 ELSE 0 END) AS active_claim_cnt,
       SUM(CASE WHEN x1.claim_state_type_id = 3 THEN 1 ELSE 0 END) AS done_claim_cnt,
       SUM(CASE WHEN x1.claim_state_type_id = 4 THEN 1 ELSE 0 END) AS canceled_claim_cnt,
       SUM(CASE WHEN x1.claim_state_type_id = 5 THEN 1 ELSE 0 END) AS part_claim_cnt
     FROM cabinet.vw_prj_claim_simple x1
     GROUP BY x1.prj_id) t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_state t12 ON t1.curr_state_id = t12.state_id
     LEFT JOIN cabinet.prj_state_type t13 ON t12.state_type_id = t13.state_type_id
     LEFT JOIN ( SELECT count(x1.claim_id) AS claim_approved_cnt,
            x1.prj_id
           FROM cabinet.prj_set_approved x1
          GROUP BY x1.prj_id) t14 ON t1.prj_id = t14.prj_id
     LEFT JOIN ( SELECT count(x1.claim_id) AS claim_new_cnt,
            x1.prj_id
           FROM cabinet.prj_set_new x1
          GROUP BY x1.prj_id) t15 ON t1.prj_id = t15.prj_id;

ALTER TABLE cabinet.vw_prj OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj_claim;

CREATE OR REPLACE VIEW cabinet.vw_prj_claim AS 
 SELECT t1.claim_id,
    t1.claim_num,
    t1.claim_name,
    t1.claim_text,
    t1.prj_id,
    t1.project_id,
    t1.module_id,
    t1.module_part_id,
    t1.module_version_id,
    t1.client_id,
    t1.priority_id,
    t1.resp_user_id,
    t1.date_plan,
    t1.date_fact,
    t1.repair_version_id,
    t1.is_archive,
    t1.rate,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.is_control,
    t1.old_task_id,
    t2.project_name,
    t3.module_name,
    t4.module_part_name,
    t5.module_version_name,
    t6.client_name,
    t7.priority_name,
    t8.user_name AS resp_user_name,
    t11.module_version_name AS repair_version_name,
    COALESCE(t12.task_cnt, 0::bigint) AS task_cnt,
    COALESCE(t12.new_task_cnt, 0::bigint) AS new_task_cnt,
    COALESCE(t12.active_task_cnt, 0::bigint) AS active_task_cnt,
    COALESCE(t12.done_task_cnt, 0::bigint) AS done_task_cnt,
    COALESCE(t12.canceled_task_cnt, 0::bigint) AS canceled_task_cnt,    
    COALESCE(t12.part_task_cnt, 0::bigint) AS part_task_cnt,    
    t12.claim_state_type_id,  
    t13.state_type_name AS claim_state_type_name,
    t13.templ AS claim_state_type_templ,    
    t9.prj_name,
    t9.date_beg AS prj_date_beg,
    t9.date_end AS prj_date_end,
    t9.curr_state_id AS prj_curr_state_id,
    t9.state_type_id AS prj_state_type_id,
    t9.state_type_name AS prj_state_type_name,
    t1.old_group_id,
    t1.old_state_id,
    t14.group_name as old_group_name,
    t15.state_name as old_state_name
   FROM cabinet.prj_claim t1
     JOIN cabinet.crm_project t2 ON t1.project_id = t2.project_id
     JOIN cabinet.crm_module t3 ON t1.module_id = t3.module_id
     JOIN cabinet.crm_module_part t4 ON t1.module_part_id = t4.module_part_id
     JOIN cabinet.crm_module_version t5 ON t1.module_version_id = t5.module_version_id
     JOIN cabinet.crm_client t6 ON t1.client_id = t6.client_id
     JOIN cabinet.crm_priority t7 ON t1.priority_id = t7.priority_id
     JOIN cabinet.cab_user t8 ON t1.resp_user_id = t8.user_id
     JOIN cabinet.vw_prj t9 ON t1.prj_id = t9.prj_id
     LEFT JOIN cabinet.crm_module_version t11 ON t1.repair_version_id = t11.module_version_id
     LEFT JOIN cabinet.vw_prj_claim_simple t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_claim_state_type t13 ON t12.claim_state_type_id = t13.state_type_id
     LEFT JOIN cabinet.crm_group t14 ON t1.old_group_id = t14.group_id
     LEFT JOIN cabinet.crm_state t15 ON t1.old_state_id = t15.state_id
     ;

ALTER TABLE cabinet.vw_prj_claim OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj_task;

CREATE OR REPLACE VIEW cabinet.vw_prj_task AS 
 SELECT t1.task_id,
    t1.claim_id,
    t1.task_num,
    t1.task_text,    
    t1.date_plan,
    t1.date_fact,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.claim_num,
    t2.claim_name,
    t2.claim_text,
    t2.prj_id AS claim_prj_id,
    t2.project_id AS claim_project_id,
    t2.module_id AS claim_module_id,
    t2.module_part_id AS claim_module_part_id,
    t2.module_version_id AS claim_module_version_id,
    t2.client_id AS claim_client_id,
    t2.priority_id AS claim_priority_id,
    t2.resp_user_id AS claim_resp_user_id,
    t2.date_plan AS claim_date_plan,
    t2.date_fact AS claim_date_fact,
    t2.repair_version_id AS claim_repair_version_id,
    t2.is_archive AS claim_is_archive,
    t2.rate AS claim_rate,
    t2.is_control AS claim_is_control,
    t2.project_name AS claim_project_name,
    t2.module_name AS claim_module_name,
    t2.module_part_name AS claim_module_part_name,
    t2.module_version_name AS claim_module_version_name,
    t2.client_name AS claim_client_name,
    t2.priority_name AS claim_priority_name,
    t2.resp_user_name AS claim_resp_user_name,
    t2.repair_version_name AS claim_repair_version_name,
    t2.claim_state_type_id,
    t2.claim_state_type_name,
    t2.claim_state_type_templ,
    t2.prj_name,
    t2.prj_date_beg,
    t2.prj_date_end,
    t2.prj_curr_state_id,
    t2.prj_state_type_id,
    t2.prj_state_type_name,
    t11.task_state_type_id,    
    COALESCE(t11.work_cnt, 0) as work_cnt,
    COALESCE(t11.active_work_cnt, 0) as active_work_cnt,
    COALESCE(t11.done_work_cnt, 0) as done_work_cnt,
    COALESCE(t11.canceled_work_cnt, 0) as canceled_work_cnt,
    t12.state_type_name as task_state_type_name,
    t12.templ as task_state_type_templ,
    t1.old_state_id,
    t13.state_name as old_state_name
   FROM cabinet.prj_task t1     
     JOIN cabinet.vw_prj_claim t2 ON t1.claim_id = t2.claim_id
     LEFT JOIN cabinet.vw_prj_task_simple t11 ON t1.task_id = t11.task_id
     LEFT JOIN cabinet.prj_task_state_type t12 ON t11.task_state_type_id = t12.state_type_id
     LEFT JOIN cabinet.crm_state t13 ON t1.old_state_id = t13.state_id;

ALTER TABLE cabinet.vw_prj_task OWNER TO pavlov;


----------------------------------------

-- DROP VIEW cabinet.vw_prj_work;

CREATE OR REPLACE VIEW cabinet.vw_prj_work AS 
 SELECT t1.work_id,
    t1.task_id,
    t1.claim_id,
    t1.work_num,
    t1.work_type_id,
    t1.exec_user_id,
    t1.state_type_id,
    t1.is_plan,
    t1.date_beg,
    t1.date_end,
    t1.is_accept,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    t1.cost_plan,
    t1.cost_fact,
    t1.mess,
    t1.old_task_id,
    t1.old_subtask_id,
    t2.work_type_name,
    t3.user_name AS exec_user_name,
    t4.state_type_name,
    t4.templ as state_type_templ,
    t5.task_num,
    t5.task_text,
    t5.task_state_type_id,
    t5.date_plan AS task_date_plan,
    t5.date_fact AS task_date_fact,
    t5.task_state_type_name,
    t5.task_state_type_templ,
    t5.claim_num,
    t5.claim_name,
    t5.claim_text,
    t5.claim_prj_id,
    t5.claim_project_id,
    t5.claim_module_id,
    t5.claim_module_part_id,
    t5.claim_module_version_id,
    t5.claim_client_id,
    t5.claim_priority_id,
    t5.claim_resp_user_id,
    t5.claim_date_plan,
    t5.claim_date_fact,
    t5.claim_repair_version_id,
    t5.claim_is_archive,
    t5.claim_rate,
    t5.claim_is_control,
    t5.claim_project_name,
    t5.claim_module_name,
    t5.claim_module_part_name,
    t5.claim_module_version_name,
    t5.claim_client_name,
    t5.claim_priority_name,
    t5.claim_resp_user_name,
    t5.claim_repair_version_name,
    t5.claim_state_type_id,
    t5.claim_state_type_name,
    t5.claim_state_type_templ,
    t5.prj_name,
    t5.prj_date_beg,
    t5.prj_date_end,
    t5.prj_curr_state_id,
    t5.prj_state_type_id,
    t5.prj_state_type_name
   FROM cabinet.prj_work t1
     JOIN cabinet.prj_work_type t2 ON t1.work_type_id = t2.work_type_id
     JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
     JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id
     JOIN cabinet.vw_prj_task t5 ON t1.task_id = t5.task_id;

ALTER TABLE cabinet.vw_prj_work OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj_mess;

CREATE OR REPLACE VIEW cabinet.vw_prj_mess AS 
 SELECT t1.mess_id,
    t1.mess,
    t1.mess_num,
    t1.user_id,
    t1.prj_id,
    t1.claim_id,
    t1.task_id,
    t1.work_id,
    t1.crt_date,
    t2.user_name,
    t12.claim_num,
    t13.task_num,
    t14.work_num,
    t12.prj_id as claim_prj_id
   FROM cabinet.prj_mess t1
     JOIN cabinet.cab_user t2 ON t1.user_id = t2.user_id
     LEFT JOIN cabinet.prj t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_claim t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_task t13 ON t1.task_id = t13.task_id
     LEFT JOIN cabinet.prj_work t14 ON t1.work_id = t14.work_id;

ALTER TABLE cabinet.vw_prj_mess OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj_plan;

-- !!!

CREATE OR REPLACE VIEW cabinet.vw_prj_plan AS 
 SELECT t1.item_id as plan_id,
  t1.work_id,
  t1.exec_user_id,
  t1.state_type_id,
  t1.is_plan,
  t1.date_beg,
  t1.date_end,
  t1.date_time_beg,
  t1.date_time_end,
  t1.is_all_day,
  t1.cost_plan,
  t1.cost_fact,
  t1.mess,
  t1.is_current,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num,
  t2.claim_prj_id as prj_id,
  t2.prj_name,
  t2.task_id,
  t2.task_num,
  t2.task_text,
  t2.claim_id,
  t2.claim_num,
  t2.claim_name,
  t2.work_num,
  t2.work_type_id,
  t2.work_type_name,
  t2.is_accept,
  t3.user_name as exec_user_name,
  t4.state_type_name,
  null::text as progress,
  null::text as result,
  true::boolean as is_public  
   FROM cabinet.prj_work_exec t1
   INNER JOIN cabinet.vw_prj_work t2 ON t1.work_id = t2.work_id
   INNER JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
   INNER JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id


   UNION ALL

 SELECT t1.event_id as plan_id,
  0 as work_id,
  t1.exec_user_id,
  t1.state_type_id,
  true as is_plan,
  t1.date_beg,
  t1.date_end,
  t1.date_beg as date_time_beg,
  t1.date_end as date_time_end,
  t1.is_all_day,
  null as cost_plan,
  null as cost_fact,
  null as mess,
  true as is_current,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num,
  -1 as prj_id,
  null as prj_name,
  0 as task_id,
  0 as task_num,
  t1.description as task_text,
  0 as claim_id,
  ('С-' || t1.event_id::text) as claim_num,
  t1.title as claim_name,
  0 as work_num,
  0 as work_type_id,
  null as work_type_name,
  true as is_accept,
  t3.user_name as exec_user_name,
  t4.state_type_name,
  t1.progress,
  t1.result,
  t1.is_public
   FROM cabinet.prj_event t1   
   INNER JOIN cabinet.cab_user t3 ON t1.exec_user_id = t3.user_id
   INNER JOIN cabinet.prj_work_state_type t4 ON t1.state_type_id = t4.state_type_id    
   ;

ALTER TABLE cabinet.vw_prj_plan OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj_log;

CREATE OR REPLACE VIEW cabinet.vw_prj_log AS 
 SELECT 
    t1.log_id,
    t1.mess,
    t1.prj_id,
    t1.claim_id,
    t1.task_id,
    t1.work_id,
    t1.crt_date,
    t1.crt_user,
    t12.claim_num,
    t13.task_num,
    t14.work_num,
    t12.prj_id as claim_prj_id    
   FROM cabinet.prj_log t1     
     LEFT JOIN cabinet.prj t11 ON t1.prj_id = t11.prj_id
     LEFT JOIN cabinet.prj_claim t12 ON t1.claim_id = t12.claim_id
     LEFT JOIN cabinet.prj_task t13 ON t1.task_id = t13.task_id
     LEFT JOIN cabinet.prj_work t14 ON t1.work_id = t14.work_id;

ALTER TABLE cabinet.vw_prj_log OWNER TO pavlov;

----------------------------------------

-- DROP VIEW cabinet.vw_prj_work_default;

CREATE OR REPLACE VIEW cabinet.vw_prj_work_default AS 
 SELECT    
   t2.user_id,
   t1.work_type_id,
   case when t3.item_id is not null then true else false end as is_default,
   case when t3.item_id is not null then t3.is_accept else false end as is_accept,
   case when t3.item_id is not null then t3.exec_user_id else 0 end as exec_user_id,
   t4.work_type_name,
   t5.user_name as exec_user_name
   FROM cabinet.prj_work_type t1
   INNER JOIN cabinet.cab_user t2 ON 1 = 1
   LEFT JOIN cabinet.prj_work_default t3 ON t1.work_type_id = t3.work_type_id AND t2.user_id = t3.user_id
   LEFT JOIN cabinet.prj_work_type t4 ON t1.work_type_id = t4.work_type_id
   LEFT JOIN cabinet.cab_user t5 ON coalesce(t3.exec_user_id, 0) = t5.user_id
   ;

ALTER TABLE cabinet.vw_prj_work_default OWNER TO pavlov;

----------------------------------------