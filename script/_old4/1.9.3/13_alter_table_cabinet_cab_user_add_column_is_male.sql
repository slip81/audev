﻿/*
	ALTER TABLE cabinet.cab_user ADD COLUMN is_male boolean
*/

ALTER TABLE cabinet.cab_user ADD COLUMN is_male boolean NOT NULL DEFAULT true;

UPDATE cabinet.cab_user SET is_male = false WHERE user_id in (4, 5, 10, 12, 13, 17, 20, 22, 24, 28, 32, 34);

-- select * from cabinet.cab_user order by user_id