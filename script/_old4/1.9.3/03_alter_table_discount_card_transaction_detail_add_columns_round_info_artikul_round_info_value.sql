﻿/*
	ALTER TABLE discount.card_transaction_detail ADD COLUMNs round_info_artikul, round_info_value
*/

ALTER TABLE discount.card_transaction_detail ADD COLUMN round_info_artikul integer;
ALTER TABLE discount.card_transaction_detail ADD COLUMN round_info_value numeric;
