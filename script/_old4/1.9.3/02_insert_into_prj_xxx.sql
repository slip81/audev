﻿/*
	insert into prj_xxx
*/


INSERT INTO cabinet.prj (prj_id, prj_name, date_beg, date_end, curr_state_id, crt_date, crt_user)
VALUES (0, '[не указан]', '2010-01-01', '2020-01-01', null, current_timestamp, 'ПМС');

INSERT INTO cabinet.prj (prj_id, prj_name, date_beg, date_end, curr_state_id, crt_date, crt_user)
SELECT batch_id, batch_name, date_beg, date_end, null, current_timestamp, 'ПМС'
FROM cabinet.crm_batch
WHERE batch_id > 0;

UPDATE cabinet.prj
SET date_beg = '2017-01-01'
WHERE date_beg IS NULL;

INSERT INTO cabinet.prj_state (prj_id, state_type_id, date_beg, date_end, crt_date, crt_user)
select prj_id, 2, date_beg, null, current_timestamp, 'ПМС'
from cabinet.prj;

UPDATE cabinet.prj
SET curr_state_id = (SELECT max(x1.state_id) FROM cabinet.prj_state x1 WHERE cabinet.prj.prj_id = x1.prj_id);


-- select * from cabinet.crm_batch;
-- select * from cabinet.prj;
-- select * from cabinet.prj_state;
-- select * from cabinet.prj_state_type;


INSERT INTO cabinet.prj_claim (
  claim_id,
  claim_num,
  claim_name,
  claim_text,
  prj_id,
  project_id,
  module_id,
  module_part_id,
  module_version_id,
  client_id,
  priority_id,
  resp_user_id,
  date_plan,
  date_fact,
  repair_version_id,
  is_archive,
  rate,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num,
  is_control,
  old_task_id,
  old_group_id,
  old_state_id
)
SELECT
  task_id,
  task_num,
  task_name,
  task_text,
  coalesce(batch_id, 0),
  project_id,
  module_id,
  module_part_id,
  module_version_id,
  client_id,
  priority_id,
  assigned_user_id,
  required_date,
  repair_date,
  repair_version_id,
  is_archive,
  fin_cost,
  crt_date,
  t2.user_name,
  upd_date,
  upd_user,
  upd_num,
  is_control,
  task_id,
  group_id,
  state_id
FROM cabinet.crm_task t1
LEFT JOIN cabinet.cab_user t2 on t1.owner_user_id = t2.user_id;


INSERT INTO cabinet.prj_set_approved (  
  prj_id,
  claim_id,
  crt_date,
  crt_user
)
SELECT
  t1.prj_id,
  t1.claim_id,
  current_timestamp,
  'ПМС'
FROM cabinet.prj_claim t1
WHERE t1.prj_id > 0;

INSERT INTO cabinet.prj_set_new (  
  prj_id,
  claim_id,
  crt_date,
  crt_user
)
SELECT
  t1.prj_id,
  t1.claim_id,
  current_timestamp,
  'ПМС'
FROM cabinet.prj_claim t1
WHERE t1.prj_id = 0;


INSERT INTO cabinet.prj_task (    
  claim_id,
  task_num,
  task_text,
  date_plan,
  date_fact,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num,
  old_task_id,
  old_subtask_id,
  old_state_id
)
SELECT  
  t1.task_id,
  t1.task_num,
  t1.subtask_text,
  t2.required_date,
  t2.repair_date,
  t1.crt_date,
  t3.user_name,
  t1.upd_date,
  t2.upd_user,
  t2.upd_num,
  t1.task_id,
  t1.subtask_id,
  t1.state_id
FROM cabinet.crm_subtask t1
INNER JOIN cabinet.crm_task t2 on t1.task_id = t2.task_id
LEFT JOIN cabinet.cab_user t3 on t2.owner_user_id = t3.user_id;

INSERT INTO cabinet.prj_task ( 
  claim_id,
  task_num,
  task_text,
  date_plan,
  date_fact,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num,
  old_task_id,
  old_subtask_id
)
SELECT
  t1.task_id,
  1,
  t1.task_text,  
  t1.required_date,
  t1.repair_date,
  t1.crt_date,
  t2.user_name,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num,
  t1.task_id,
  null
FROM cabinet.crm_task t1
LEFT JOIN cabinet.cab_user t2 on t1.owner_user_id = t2.user_id
WHERE NOT EXISTS(SELECT x1.task_id FROM cabinet.crm_subtask x1 WHERE t1.task_id = x1.task_id);

-- select * from cabinet.prj_work_type
-- select * from cabinet.prj_work_state_type
-- select * from cabinet.crm_state

INSERT INTO cabinet.prj_work (    
  task_id,
  claim_id,
  work_num,
  work_type_id,
  exec_user_id,
  state_type_id,
  is_plan,
  date_beg,
  date_end,
  is_accept,
  cost_plan,
  cost_fact,
  mess,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num,
  old_task_id,
  old_subtask_id   
)
SELECT
  t3.task_id,
  t3.claim_id,
  1,
  3,
  t2.exec_user_id,
  case when t4.done_or_canceled = 0 then 1 when t4.done_or_canceled = 1 then 2 when t4.done_or_canceled = 2 then 3 else 1 end,
  case when coalesce(t11.event_id, 0) > 0 then true else false end,
  t11.date_beg,
  t11.date_end,
  true,
  null,
  null,
  null,
  t3.crt_date,
  t3.crt_user,
  t3.upd_date,
  t3.upd_user,
  t3.upd_num,
  t1.task_id,
  t1.subtask_id
FROM cabinet.crm_subtask t1
INNER JOIN cabinet.crm_task t2 on t1.task_id = t2.task_id
INNER JOIN cabinet.prj_task t3 on t2.task_id = t3.claim_id and t1.task_num = t3.task_num
INNER JOIN cabinet.crm_state t4 on t1.state_id = t4.state_id
LEFT JOIN
(
SELECT max(x1.event_id) as event_id, max(x1.date_beg) as date_beg, max(x1.date_end) as date_end, x1.task_id, x1.exec_user_id
FROM cabinet.planner_event x1
GROUP BY x1.task_id, x1.exec_user_id
) t11 on t2.task_id = t11.task_id and t2.exec_user_id = t11.exec_user_id;

INSERT INTO cabinet.prj_work (  
  task_id,
  claim_id,
  work_num,
  work_type_id,
  exec_user_id,
  state_type_id,
  is_plan,
  date_beg,
  date_end,
  is_accept,
  cost_plan,
  cost_fact,
  mess,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num,
  old_task_id,
  old_subtask_id  
)
SELECT
  t3.task_id,
  t3.claim_id,
  1,
  3,
  t1.exec_user_id,  
  case when t4.done_or_canceled = 0 then 1 when t4.done_or_canceled = 1 then 2 when t4.done_or_canceled = 2 then 3 else 1 end,
  case when coalesce(t11.event_id, 0) > 0 then true else false end,
  t11.date_beg,
  t11.date_end,
  true,
  null,
  null,
  null,
  t3.crt_date,
  t3.crt_user,
  t3.upd_date,
  t3.upd_user,
  t3.upd_num,
  t1.task_id,
  null
FROM cabinet.crm_task t1
INNER JOIN cabinet.prj_task t3 on t1.task_id = t3.claim_id and t3.task_num = 1
INNER JOIN cabinet.crm_state t4 on t1.state_id = t4.state_id
LEFT JOIN
(
SELECT max(x1.event_id) as event_id, max(x1.date_beg) as date_beg, max(x1.date_end) as date_end, x1.task_id, x1.exec_user_id
FROM cabinet.planner_event x1
GROUP BY x1.task_id, x1.exec_user_id
) t11 on t1.task_id = t11.task_id and t1.exec_user_id = t11.exec_user_id
WHERE NOT EXISTS(SELECT x1.task_id FROM cabinet.crm_subtask x1 WHERE t1.task_id = x1.task_id);


INSERT INTO cabinet.prj_work_exec (
  work_id,
  exec_user_id,
  state_type_id,
  is_plan,
  date_beg,
  date_end,
  date_time_beg,
  date_time_end,  
  is_all_day,
  cost_plan,
  cost_fact,
  mess,
  is_current,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
SELECT
  t1.work_id,
  t1.exec_user_id,
  t1.state_type_id,
  t1.is_plan,
  t1.date_beg,  
  t1.date_end,
  t1.date_beg,
  t1.date_end,  
  case when ((t1.date_beg != t1.date_beg::date) or (t1.date_end != t1.date_end::date)) then false else true end,
  t1.cost_plan,
  t1.cost_fact,
  t1.mess,
  true,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  t1.upd_num 
FROM cabinet.prj_work t1
WHERE t1.date_beg IS NOT NULL and t1.date_end IS NOT NULL
;

UPDATE cabinet.prj_work
SET date_beg = date_beg::date, date_end = date_end::date;

UPDATE cabinet.prj_work_exec
SET date_beg = date_beg::date, date_end = date_end::date;

-- select * from cabinet.prj_work order by work_id desc

-- select * from cabinet.cab_user


SELECT setval('cabinet.prj_work_exec_item_id_seq', (SELECT COALESCE(max(item_id), 0) + 1 FROM cabinet.prj_work_exec), false);

INSERT INTO cabinet.prj_event (
  date_beg,
  date_end,
  is_all_day,
  title,
  description,
  progress,
  result,
  exec_user_id,
  state_type_id,
  is_public,
  old_event_id,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
SELECT
  t1.date_beg,
  t1.date_end,
  t1.is_all_day,
  t1.title,
  t1.description,
  t1.progress,
  t1.result,
  t1.exec_user_id,
  case when t4.done_or_canceled = 0 then 1 when t4.done_or_canceled = 1 then 2 when t4.done_or_canceled = 2 then 3 else 1 end,
  true,
  t1.event_id,
  t1.crt_date,
  t1.crt_user,
  t1.upd_date,
  t1.upd_user,
  0
FROM cabinet.planner_event t1
INNER JOIN cabinet.crm_state t4 on t1.event_state_id = t4.state_id
WHERE t1.task_id is null;


INSERT INTO cabinet.prj_mess (
  mess,
  mess_num,
  user_id,
  prj_id,
  claim_id,
  task_id,
  work_id,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  upd_num
)
SELECT
  t1.note_text,
  t1.note_id,
  t1.owner_user_id,
  null,
  t1.task_id,
  null,
  null,
  t1.crt_date,
  t11.user_name,
  null,
  null,
  0
FROM cabinet.crm_task_note t1
LEFT JOIN cabinet.cab_user t11 on t1.owner_user_id = t11.user_id;


INSERT INTO cabinet.prj_log (
  mess,
  prj_id,
  claim_id,
  task_id,
  work_id,
  crt_date,
  crt_user
)
SELECT
  t1.mess,
  null,
  t1.task_id,
  null,
  null,
  t1.date_beg,
  t11.user_name
FROM cabinet.log_crm t1
LEFT JOIN cabinet.cab_user t11 on t1.user_id = t11.user_id;


DELETE FROM cabinet.prj_state
WHERE NOT EXISTS(SELECT x1.claim_id FROM cabinet.prj_claim x1 WHERE x1.prj_id = cabinet.prj_state.prj_id);

DELETE FROM cabinet.prj
WHERE NOT EXISTS(SELECT x1.claim_id FROM cabinet.prj_claim x1 WHERE x1.prj_id = cabinet.prj.prj_id);

DELETE FROM cabinet.cab_grid_user_settings WHERE grid_id > 2;

SELECT setval('cabinet.prj_prj_id_seq', (SELECT COALESCE(max(prj_id), 0) + 1 FROM cabinet.prj), false);
SELECT setval('cabinet.prj_state_state_id_seq', (SELECT COALESCE(max(state_id), 0) + 1 FROM cabinet.prj_state), false);
SELECT setval('cabinet.prj_claim_claim_id_seq', (SELECT COALESCE(max(claim_id), 0) + 1 FROM cabinet.prj_claim), false);
SELECT setval('cabinet.prj_task_task_id_seq', (SELECT COALESCE(max(task_id), 0) + 1 FROM cabinet.prj_task), false);
SELECT setval('cabinet.prj_work_work_id_seq', (SELECT COALESCE(max(work_id), 0) + 1 FROM cabinet.prj_work), false);
SELECT setval('cabinet.prj_work_exec_item_id_seq', (SELECT COALESCE(max(plan_id), 0) + 1 FROM cabinet.vw_prj_plan), false);
SELECT setval('cabinet.prj_mess_mess_id_seq', (SELECT COALESCE(max(mess_id), 0) + 1 FROM cabinet.prj_mess), false);
SELECT setval('cabinet.prj_log_log_id_seq', (SELECT COALESCE(max(log_id), 0) + 1 FROM cabinet.prj_log), false);
