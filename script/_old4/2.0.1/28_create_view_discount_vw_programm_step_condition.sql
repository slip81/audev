﻿/*
	CREATE OR REPLACE VIEW discount.vw_programm_step_condition
*/

-- DROP VIEW discount.vw_programm_step_condition;

CREATE OR REPLACE VIEW discount.vw_programm_step_condition AS 
 SELECT t1.condition_id,
  t1.programm_id,
  t1.step_id,
  t1.condition_num,
  t1.op_type,
  t1.op1_param_id,
  t1.op2_param_id,
  t1.condition_name,
  t2.step_num,
  t3.business_id,
  t11.condition_type_id,
  t11.condition_type_name,
  t12.param_name as op1_param_name,
  t13.param_name as op2_param_name,
  t1.condition_id::character varying as condition_id_str,
  t1.programm_id::character varying as programm_id_str,
  t1.step_id::character varying as step_id_str,
  t1.op1_param_id::character varying as op1_param_id_str,
  t1.op2_param_id::character varying as op2_param_id_str,
  t3.business_id::character varying as business_id_str
   FROM discount.programm_step_condition t1
   INNER JOIN discount.programm_step t2 ON t1.step_id = t2.step_id
   INNER JOIN discount.programm t3 ON t2.programm_id = t3.programm_id
   LEFT JOIN discount.programm_step_condition_type t11 ON t1.op_type = t11.condition_type_id
   LEFT JOIN discount.programm_step_param t12 ON t1.op1_param_id = t12.param_id AND t1.step_id = t12.step_id
   LEFT JOIN discount.programm_step_param t13 ON t1.op2_param_id = t13.param_id AND t1.step_id = t13.step_id
   ;

ALTER TABLE discount.vw_programm_step_condition OWNER TO pavlov;
