﻿/*
	CREATE TABLE cabinet.region_zone
*/

-- DROP TABLE cabinet.region_zone;

CREATE TABLE cabinet.region_zone
(
  zone_id serial NOT NULL,
  region_id integer NOT NULL,  
  zone_name character varying,
  is_deleted smallint,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT region_zone_pkey PRIMARY KEY (zone_id),
  CONSTRAINT region_zone_region_id_fkey FOREIGN KEY (region_id)
      REFERENCES cabinet.region (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.region_zone OWNER TO pavlov;

-- DROP TRIGGER cabinet_region_zone_set_stamp_trg ON cabinet.region_zone;

CREATE TRIGGER cabinet_region_zone_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.region_zone
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

