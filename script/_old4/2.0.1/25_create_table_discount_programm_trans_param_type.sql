﻿/*
	CREATE TABLE discount.programm_trans_param_type
*/

-- DROP TABLE discount.programm_trans_param_type;

CREATE TABLE discount.programm_trans_param_type
(
  trans_param_type integer NOT NULL,
  trans_param_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope6(),
  CONSTRAINT programm_trans_param_type_pkey PRIMARY KEY (trans_param_type)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_trans_param_type OWNER TO pavlov;

-- DROP TRIGGER discount_programm_trans_param_type_set_stamp_trg ON discount.programm_trans_param_type;

CREATE TRIGGER discount_programm_trans_param_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON discount.programm_trans_param_type
  FOR EACH ROW
  EXECUTE PROCEDURE discount.set_stamp();

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  1,
  'Номер позиции в продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  2,
  'Сумма позиции в продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  3,
  'Сумма продажи'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  4,
  'Дата-время продажи'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  5,
  'Наименование позиции продажи'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  6,
  'Макс. процент скидки по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  7,
  'Стоимость позиции со скидкой'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  8,
  'Сумма скидки по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  9,
  'Процент скидки по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  10,
  'Сумма бонусов к зачислению по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  11,
  'Процент бонусов к зачислению по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  17,
  'Сумма бонусов к оплате по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  18,
  'Процент бонусов к оплате по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  12,
  'Сумма продажи со скидкой'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  13,
  'Сумма скидки по продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  14,
  'Процент скидки по продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  15,
  'Сумма бонусов к зачислению по продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  16,
  'Процент бонусов к зачислению по продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  19,
  'Сумма бонусов к оплате по продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  20,
  'Процент бонусов к оплате по продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  21,
  'Макс. процент бонусов к зачислению по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  22,
  'Макс. процент бонусов к оплате по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  23,
  'Процент розничной наценки по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  24,
  'Процент стоимости позиции от стоимости продажи'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  25,
  'Количество позиций в продаже'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  26,
  'Количество единиц товара в позиции продажи'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  27,
  'Позиция является ЖНВЛП'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  28,
  'Позиция участвует в промо-акции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  29,
  'Процент НДС по позиции'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  30,
  'Карта выбрана сканером'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  31,
  'Карта выбрана карт-ридером'
);

INSERT INTO discount.programm_trans_param_type (
  trans_param_type,
  trans_param_type_name
)
VALUES (
  32,
  'Маржа по позиции'
);


-- select * from discount.programm_trans_param_type order by trans_param_type;