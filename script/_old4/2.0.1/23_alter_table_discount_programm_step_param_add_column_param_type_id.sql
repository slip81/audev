﻿/*
	ALTER TABLE discount.programm_step_param ADD COLUMN param_type_id
*/

ALTER TABLE discount.programm_step_param ADD COLUMN param_type_id integer;

ALTER TABLE discount.programm_step_param
  ADD CONSTRAINT programm_step_param_param_type_id_fkey FOREIGN KEY (param_type_id)
      REFERENCES discount.programm_step_param_type (param_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE discount.programm_step_param
SET param_type_id = 4
WHERE prev_step_param_id is not null;

UPDATE discount.programm_step_param
SET param_type_id = 3
WHERE programm_param_id is not null;

UPDATE discount.programm_step_param
SET param_type_id = 2
WHERE trans_param_type is not null;      

UPDATE discount.programm_step_param
SET param_type_id = 1
WHERE card_param_type is not null;