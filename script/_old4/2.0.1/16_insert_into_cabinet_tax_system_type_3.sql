﻿/*
	INSERT INTO cabinet.tax_system_type 3
*/

INSERT INTO cabinet.tax_system_type (
  system_type_id,
  system_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  3, 
  'УСН',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false
);

-- select * from cabinet.tax_system_type order by system_type_id;