﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_region
*/

-- DROP VIEW cabinet.vw_region;

CREATE OR REPLACE VIEW cabinet.vw_region AS 
 WITH region_price_limit_cte AS
 (
   SELECT ROW_NUMBER() OVER (PARTITION BY region_id ORDER BY limit_type_id) AS num, region_id FROM cabinet.region_price_limit
 )
 SELECT t1.id as region_id,  
  t1.name as region_name,
  t1.is_deleted,
  case when t2.region_id IS NOT NULL then true else false end as is_price_limit_exists
   FROM cabinet.region t1
   LEFT JOIN region_price_limit_cte t2 ON t1.id = t2.region_id AND t2.num = 1
   ;

ALTER TABLE cabinet.vw_region OWNER TO pavlov;
