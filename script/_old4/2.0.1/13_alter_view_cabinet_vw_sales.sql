﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_sales
*/

-- DROP VIEW cabinet.vw_sales;

CREATE OR REPLACE VIEW cabinet.vw_sales AS 
 SELECT t1.id AS sales_id,
    t1.name,
    t1.client_id,
    t1.city_id,
    t1.cell,
    t1.contact_person,
    t1.adress,
    t1.is_deleted,
    t1.sm_id,
    t1.email,
    t13.user_id,
    t2.name AS client_name,
    t11.name AS city_name,
    t11.region_id,
    t12.name AS region_name,
    t15.name AS user_name,
    t1.region_zone_id,
    t1.tax_system_type_id
   FROM cabinet.sales t1
     JOIN cabinet.client t2 ON t1.client_id = t2.id
     LEFT JOIN cabinet.city t11 ON t1.city_id = t11.id
     LEFT JOIN cabinet.region t12 ON t11.region_id = t12.id
     LEFT JOIN cabinet.client_user t13 ON t1.id = t13.sales_id AND t13.is_main_for_sales = true
     LEFT JOIN cabinet.my_aspnet_users t15 ON t13.user_id = t15.id
  WHERE t1.is_deleted = 0;

ALTER TABLE cabinet.vw_sales OWNER TO pavlov;
