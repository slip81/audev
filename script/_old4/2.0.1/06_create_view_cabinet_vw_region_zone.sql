﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_region_zone
*/

-- DROP VIEW cabinet.vw_region_zone;

CREATE OR REPLACE VIEW cabinet.vw_region_zone AS 
 WITH region_zone_price_limit_cte AS
 (
   SELECT ROW_NUMBER() OVER (PARTITION BY zone_id ORDER BY limit_type_id) AS num, zone_id FROM cabinet.region_zone_price_limit
 )
 SELECT t1.zone_id,  
  t1.zone_name,
  t1.region_id,
  t2.name as region_name,
  t1.is_deleted,
  case when t11.zone_id IS NOT NULL then true else false end as is_price_limit_exists
   FROM cabinet.region_zone t1
   INNER JOIN cabinet.region t2 ON t1.region_id = t2.id
   LEFT JOIN region_zone_price_limit_cte t11 ON t1.zone_id = t11.zone_id AND t11.num = 1
   ;

ALTER TABLE cabinet.vw_region_zone OWNER TO pavlov;