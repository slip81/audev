﻿/*
	ALTER TABLE cabinet.client ADD COLUMNs region_zone_id, tax_system_type_id
*/

ALTER TABLE cabinet.client ADD COLUMN region_zone_id integer;
ALTER TABLE cabinet.client ADD COLUMN tax_system_type_id integer;

ALTER TABLE cabinet.client
  ADD CONSTRAINT client_region_zone_id_fkey FOREIGN KEY (region_zone_id)
      REFERENCES cabinet.region_zone (zone_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE cabinet.client
  ADD CONSTRAINT client_tax_system_type_id_fkey FOREIGN KEY (tax_system_type_id)
      REFERENCES cabinet.tax_system_type (system_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;      