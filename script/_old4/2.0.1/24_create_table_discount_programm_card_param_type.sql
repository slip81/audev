﻿/*
	CREATE TABLE discount.programm_card_param_type
*/

-- DROP TABLE discount.programm_card_param_type;

CREATE TABLE discount.programm_card_param_type
(
  card_param_type integer NOT NULL,
  card_param_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope6(),
  CONSTRAINT programm_card_param_type_pkey PRIMARY KEY (card_param_type)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_card_param_type OWNER TO pavlov;

-- DROP TRIGGER discount_programm_card_param_type_set_stamp_trg ON discount.programm_card_param_type;

CREATE TRIGGER discount_programm_card_param_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON discount.programm_card_param_type
  FOR EACH ROW
  EXECUTE PROCEDURE discount.set_stamp();

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  1,
  'Сумма бонусов'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  2,
  'Процент скидки'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  3,
  'Сумма на сертификате'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  4,
  'Бонусный процент'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  5,
  'Сумма скидки'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  10,
  'Накопленная сумма на карте'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  11,
  'Количество продаж по карте'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  12,
  'Дата сгорания накопленной суммы'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  13,
  'Статус карты'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  14,
  'Лимит по карте (в деньгах)'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  15,
  'Использованный лимит по карте (в деньгах)'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  16,
  'Лимит по карте (в бонусах)'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  17,
  'Использованный лимит по карте (в бонусах)'
);

INSERT INTO discount.programm_card_param_type (
  card_param_type,
  card_param_type_name
)
VALUES (
  18,
  'Сумма неактивных бонусов'
);



-- select * from discount.programm_card_param_type order by card_param_type;