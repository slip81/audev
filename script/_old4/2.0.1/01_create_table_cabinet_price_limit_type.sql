﻿/*
	CREATE TABLE cabinet.price_limit_type
*/

-- DROP TABLE cabinet.price_limit_type;

CREATE TABLE cabinet.price_limit_type
(
  limit_type_id integer NOT NULL,
  limit_type_name character varying,
  limit_value numeric,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT price_limit_type_pkey PRIMARY KEY (limit_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.price_limit_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_price_limit_type_set_stamp_trg ON cabinet.price_limit_type;

CREATE TRIGGER cabinet_price_limit_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.price_limit_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.price_limit_type (
  limit_type_id,
  limit_type_name,
  limit_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  1,
  'Цена изготвителя без НДС до 50 руб. включительно',
  50,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO cabinet.price_limit_type (
  limit_type_id,
  limit_type_name,
  limit_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  2,
  'Цена изготвителя без НДС от 50 руб. до 500 руб. включительно',
  500,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);

INSERT INTO cabinet.price_limit_type (
  limit_type_id,
  limit_type_name,
  limit_value,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  3,
  'Цена изготвителя без НДС свыше 500 руб.',
  10000000,
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false  
);