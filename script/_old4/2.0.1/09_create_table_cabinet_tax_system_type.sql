﻿/*
	CREATE TABLE cabinet.tax_system_type
*/

-- DROP TABLE cabinet.tax_system_type;

CREATE TABLE cabinet.tax_system_type
(
  system_type_id integer NOT NULL,
  system_type_name character varying,
  crt_date timestamp without time zone,
  crt_user character varying,
  upd_date timestamp without time zone,
  upd_user character varying,
  is_deleted boolean NOT NULL DEFAULT false,
  del_date timestamp without time zone,
  del_user character varying,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT tax_system_type_pkey PRIMARY KEY (system_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.tax_system_type OWNER TO pavlov;

-- DROP TRIGGER cabinet_tax_system_type_set_stamp_trg ON cabinet.tax_system_type;

CREATE TRIGGER cabinet_tax_system_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.tax_system_type
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

INSERT INTO cabinet.tax_system_type (
  system_type_id,
  system_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  1, 
  'ОСНО',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false
);

INSERT INTO cabinet.tax_system_type (
  system_type_id,
  system_type_name,
  crt_date,
  crt_user,
  upd_date,
  upd_user,
  is_deleted
)
VALUES (
  2, 
  'ЕНВД',
  current_timestamp,
  'pavlov',
  current_timestamp,
  'pavlov',
  false
);