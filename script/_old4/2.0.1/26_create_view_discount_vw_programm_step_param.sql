﻿/*
	CREATE OR REPLACE VIEW discount.vw_programm_step_param
*/

-- DROP VIEW discount.vw_programm_step_param;

CREATE OR REPLACE VIEW discount.vw_programm_step_param AS
 SELECT t1.param_id,
  t1.programm_id,
  t1.step_id,
  t1.card_param_type,
  t1.trans_param_type,
  t1.programm_param_id,
  t1.prev_step_param_id,
  t1.param_num,
  t1.param_name,
  t1.is_programm_output,
  t1.param_type_id,
  t2.step_num,  
  t3.business_id,
  t11.param_type_name,
  t12.card_param_type_name,
  t13.trans_param_type_name,
  t14.param_name as programm_param_name,
  t15.param_name as prev_step_param_name,
  case t1.is_programm_output when 1 then true else false end as is_programm_output_chb,
  case t1.param_type_id when 1 then t12.card_param_type_name when 2 then t13.trans_param_type_name when 3 then t14.param_name when 4 then t15.param_name else '' end as curr_param_name,
  t1.param_id::character varying as param_id_str,
  t1.programm_id::character varying as programm_id_str,
  t1.step_id::character varying as step_id_str,
  t1.programm_param_id::character varying as programm_param_id_str,
  t1.prev_step_param_id::character varying as prev_step_param_id_str,
  t3.business_id::character varying as business_id_str
   FROM discount.programm_step_param t1
   INNER JOIN discount.programm_step t2 ON t1.step_id = t2.step_id
   INNER JOIN discount.programm t3 ON t2.programm_id = t3.programm_id
   LEFT JOIN discount.programm_step_param_type t11 ON t1.param_type_id = t11.param_type_id
   LEFT JOIN discount.programm_card_param_type t12 ON t1.card_param_type = t12.card_param_type
   LEFT JOIN discount.programm_trans_param_type t13 ON t1.trans_param_type = t13.trans_param_type
   LEFT JOIN discount.programm_param t14 ON t1.programm_param_id = t14.param_id
   LEFT JOIN discount.programm_step_param t15 ON t1.prev_step_param_id = t15.param_id
   ;

ALTER TABLE discount.vw_programm_step_param OWNER TO pavlov;
