﻿/*
	CREATE TABLE discount.programm_step_logic_type
*/

-- DROP TABLE discount.programm_step_logic_type;

CREATE TABLE discount.programm_step_logic_type
(
  logic_type_id integer NOT NULL,
  logic_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope6(),
  CONSTRAINT programm_step_logic_type_pkey PRIMARY KEY (logic_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_step_logic_type OWNER TO pavlov;

-- DROP TRIGGER discount_programm_step_logic_type_set_stamp_trg ON discount.programm_step_logic_type;

CREATE TRIGGER discount_programm_step_logic_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON discount.programm_step_logic_type
  FOR EACH ROW
  EXECUTE PROCEDURE discount.set_stamp();

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  1,
  'Сложить'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  2,
  'Вычесть'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  3,
  'Взять процент'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  4,
  'Взять сумму за вычетом процента'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  5,
  'Умножить'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  6,
  'Разделить'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  7,
  'Взять первый параметр'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  8,
  'Взять дату'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  9,
  'Взять время'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  10,
  'Взять год'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  11,
  'Взять месяц'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  12,
  'Взять день месяца'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  13,
  'Взять день недели'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  14,
  'Взять час'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  15,
  'Взять минуту'
);

INSERT INTO discount.programm_step_logic_type (
  logic_type_id,
  logic_type_name
)
VALUES (
  16,
  'Взять процент от суммы'
);


-- select * from discount.programm_step_logic_type order by logic_type_id;