﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_region_price_limit
*/

-- DROP VIEW cabinet.vw_region_price_limit;

CREATE OR REPLACE VIEW cabinet.vw_region_price_limit AS 
 SELECT ROW_NUMBER() OVER (ORDER BY t1.id) as item_id,
     t1.id as region_id,
     t11.limit_type_id,
     t11.limit_type_name,
     t12.percent_value,
     case when t12.region_id is not null then true else false end as is_price_limit_exists
   FROM cabinet.region t1
     LEFT JOIN cabinet.price_limit_type t11 ON 1=1
     LEFT JOIN cabinet.region_price_limit t12 ON t1.id = t12.region_id AND t11.limit_type_id = t12.limit_type_id;

ALTER TABLE cabinet.vw_region_price_limit OWNER TO pavlov;
