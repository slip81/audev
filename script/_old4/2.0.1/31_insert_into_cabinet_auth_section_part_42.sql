﻿/*
	insert into cabinet.auth_section_part 42
*/

insert into cabinet.auth_section_part (
  part_id,
  part_name,
  section_id,
  group_id
)
values (
  42,
  'Конструктор',
  1,
  1
);

-- select * from cabinet.auth_section order by section_id
-- select * from cabinet.auth_section_part order by part_id