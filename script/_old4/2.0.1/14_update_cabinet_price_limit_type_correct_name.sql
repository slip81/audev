﻿/*
	update cabinet.price_limit_type
*/

update cabinet.price_limit_type
set limit_type_name = 'Цена изготовителя без НДС до 50 руб. включительно'
where limit_type_id = 1;

update cabinet.price_limit_type
set limit_type_name = 'Цена изготовителя без НДС от 50 руб. до 500 руб. включительно'
where limit_type_id = 2;

update cabinet.price_limit_type
set limit_type_name = 'Цена изготовителя без НДС свыше 500 руб.'
where limit_type_id = 3;

-- select * from cabinet.price_limit_type order by limit_type_id