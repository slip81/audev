﻿/*
	CREATE TABLE discount.programm_step_condition_type
*/

-- DROP TABLE discount.programm_step_condition_type;

CREATE TABLE discount.programm_step_condition_type
(
  condition_type_id integer NOT NULL,
  condition_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope6(),
  CONSTRAINT programm_step_condition_type_pkey PRIMARY KEY (condition_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_step_condition_type OWNER TO pavlov;

-- DROP TRIGGER discount_programm_step_condition_type_set_stamp_trg ON discount.programm_step_condition_type;

CREATE TRIGGER discount_programm_step_condition_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON discount.programm_step_condition_type
  FOR EACH ROW
  EXECUTE PROCEDURE discount.set_stamp();

INSERT INTO discount.programm_step_condition_type (
  condition_type_id,
  condition_type_name
)
VALUES (
  1,
  'Больше чем'
);

INSERT INTO discount.programm_step_condition_type (
  condition_type_id,
  condition_type_name
)
VALUES (
  2,
  'Равно'
);

INSERT INTO discount.programm_step_condition_type (
  condition_type_id,
  condition_type_name
)
VALUES (
  3,
  'Не равно'
);

INSERT INTO discount.programm_step_condition_type (
  condition_type_id,
  condition_type_name
)
VALUES (
  4,
  'Больше или равно'
);

-- select * from discount.programm_step_condition_type order by condition_type_id;