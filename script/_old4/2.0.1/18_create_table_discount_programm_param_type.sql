﻿/*
	CREATE TABLE discount.programm_param_type
*/

-- DROP TABLE discount.programm_param_type;

CREATE TABLE discount.programm_param_type
(
  param_type_id integer NOT NULL,
  param_type_name character varying,  
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT discount.sysuidgen_scope6(),
  CONSTRAINT programm_param_type_pkey PRIMARY KEY (param_type_id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE discount.programm_param_type OWNER TO pavlov;

-- DROP TRIGGER discount_programm_param_type_set_stamp_trg ON discount.programm_param_type;

CREATE TRIGGER discount_programm_param_type_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON discount.programm_param_type
  FOR EACH ROW
  EXECUTE PROCEDURE discount.set_stamp();

INSERT INTO discount.programm_param_type (
  param_type_id,
  param_type_name
)
VALUES (
  1,
  'Строка'
);

INSERT INTO discount.programm_param_type (
  param_type_id,
  param_type_name
)
VALUES (
  2,
  'Целое число'
);

INSERT INTO discount.programm_param_type (
  param_type_id,
  param_type_name
)
VALUES (
  3,
  'Дробное число'
);

INSERT INTO discount.programm_param_type (
  param_type_id,
  param_type_name
)
VALUES (
  4,
  'Дата-время'
);

INSERT INTO discount.programm_param_type (
  param_type_id,
  param_type_name
)
VALUES (
  5,
  'Дата'
);

INSERT INTO discount.programm_param_type (
  param_type_id,
  param_type_name
)
VALUES (
  6,
  'Время'
);

-- select * from discount.programm_param_type order by param_type_id;