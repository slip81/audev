﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_region_zone_price_limit
*/

-- DROP VIEW cabinet.vw_region_zone_price_limit;

CREATE OR REPLACE VIEW cabinet.vw_region_zone_price_limit AS 
 SELECT ROW_NUMBER() OVER (ORDER BY t1.zone_id) as item_id,
     t1.zone_id,
     t11.limit_type_id,
     t11.limit_type_name,
     t12.percent_value,
     case when t12.zone_id is not null then true else false end as is_price_limit_exists
   FROM cabinet.region_zone t1
     LEFT JOIN cabinet.price_limit_type t11 ON 1=1
     LEFT JOIN cabinet.region_zone_price_limit t12 ON t1.zone_id = t12.zone_id AND t11.limit_type_id = t12.limit_type_id;

ALTER TABLE cabinet.vw_region_zone_price_limit OWNER TO pavlov;
