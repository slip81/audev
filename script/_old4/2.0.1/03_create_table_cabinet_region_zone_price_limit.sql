﻿/*
	CREATE TABLE cabinet.region_zone_price_limit
*/

-- DROP TABLE cabinet.region_zone_price_limit;

CREATE TABLE cabinet.region_zone_price_limit
(
  item_id serial NOT NULL,
  zone_id integer NOT NULL,
  limit_type_id integer NOT NULL,
  percent_value numeric,
  sysrowstamp bigint,
  sysrowuid bigint NOT NULL DEFAULT cabinet.sysuidgen_scope9(),
  CONSTRAINT region_zone_price_limit_pkey PRIMARY KEY (item_id),
  CONSTRAINT region_zone_price_limit_zone_id_fkey FOREIGN KEY (zone_id)
      REFERENCES cabinet.region_zone (zone_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT region_zone_price_limit_limit_type_id_fkey FOREIGN KEY (limit_type_id)
      REFERENCES cabinet.price_limit_type (limit_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION      
)
WITH (
  OIDS=FALSE
);

ALTER TABLE cabinet.region_zone_price_limit OWNER TO pavlov;

-- DROP TRIGGER cabinet_region_zone_price_limit_set_stamp_trg ON cabinet.region_zone_price_limit;

CREATE TRIGGER cabinet_region_zone_price_limit_set_stamp_trg
  BEFORE INSERT OR UPDATE
  ON cabinet.region_zone_price_limit
  FOR EACH ROW
  EXECUTE PROCEDURE cabinet.set_stamp();

