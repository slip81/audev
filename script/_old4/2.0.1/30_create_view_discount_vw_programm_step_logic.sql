﻿/*
	CREATE OR REPLACE VIEW discount.vw_programm_step_logic
*/

-- DROP VIEW discount.vw_programm_step_logic;

CREATE OR REPLACE VIEW discount.vw_programm_step_logic AS 
 SELECT t1.logic_id,
    t1.programm_id,
    t1.step_id,
    t1.logic_num,
    t1.op_type,
    t1.op1_param_id,
    t1.op2_param_id,
    t1.op3_param_id,
    t1.op4_param_id,    
    t1.condition_id,    
    t2.step_num,
    t3.business_id,
    t4.logic_type_id,
    t4.logic_type_name,
    t12.param_name AS op1_param_name,
    t13.param_name AS op2_param_name,
    t14.param_name AS op3_param_name,
    t15.param_name AS op4_param_name,
    t16.condition_name,
    t1.logic_id::character varying AS logic_id_str,    
    t1.programm_id::character varying AS programm_id_str,
    t1.step_id::character varying AS step_id_str,
    t1.condition_id::character varying AS condition_id_str,
    t1.op1_param_id::character varying AS op1_param_id_str,
    t1.op2_param_id::character varying AS op2_param_id_str,
    t1.op3_param_id::character varying AS op3_param_id_str,
    t1.op4_param_id::character varying AS op4_param_id_str,
    t3.business_id::character varying AS business_id_str
   FROM discount.programm_step_logic t1
     INNER JOIN discount.programm_step t2 ON t1.step_id = t2.step_id
     INNER JOIN discount.programm t3 ON t2.programm_id = t3.programm_id
     INNER JOIN discount.programm_step_logic_type t4 ON t1.op_type = t4.logic_type_id
     LEFT JOIN discount.programm_step_param t12 ON t1.op1_param_id = t12.param_id AND t1.step_id = t12.step_id
     LEFT JOIN discount.programm_step_param t13 ON t1.op2_param_id = t13.param_id AND t1.step_id = t13.step_id
     LEFT JOIN discount.programm_step_param t14 ON t1.op3_param_id = t14.param_id AND t1.step_id = t14.step_id
     LEFT JOIN discount.programm_step_param t15 ON t1.op4_param_id = t15.param_id AND t1.step_id = t15.step_id
     LEFT JOIN discount.programm_step_condition t16 ON t1.condition_id = t16.condition_id
     ;

ALTER TABLE discount.vw_programm_step_logic OWNER TO pavlov;
