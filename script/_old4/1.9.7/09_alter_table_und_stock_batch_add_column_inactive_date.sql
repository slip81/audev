﻿/*
	ALTER TABLE und.stock_batch ADD COLUMN inactive_date
*/

ALTER TABLE und.stock_batch ADD COLUMN inactive_date timestamp without time zone;

UPDATE und.stock_batch SET inactive_date = crt_date;