﻿/*
	ALTER TABLE und.stock_sales_download ADD COLUMN inactive_date
*/

ALTER TABLE und.stock_sales_download ADD COLUMN inactive_date timestamp without time zone;