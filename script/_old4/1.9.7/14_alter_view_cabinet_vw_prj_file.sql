﻿/*
	CREATE OR REPLACE VIEW cabinet.vw_prj_file
*/

-- DROP VIEW cabinet.vw_prj_file;

CREATE OR REPLACE VIEW cabinet.vw_prj_file AS 
 SELECT t1.file_id,
    t1.file_name,
    t1.file_link,
    t1.file_path,
    t1.prj_id,
    t1.claim_id,
    t1.task_id,
    t1.work_id,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t1.upd_num,
    ((('<a href="'::text || t1.file_link::text) || '" target="_blank" title="' || t1.file_name || '">'::text) || t1.file_name::text) || '</a>'::text AS file_name_url,
    t11.claim_num
   FROM cabinet.prj_file t1
     LEFT JOIN cabinet.prj_claim t11 ON t1.claim_id = t11.claim_id;

ALTER TABLE cabinet.vw_prj_file OWNER TO pavlov;
