﻿/*
	ALTER TABLE cabinet.prj_work ADD COLUMNs date_time_beg, date_time_end, is_all_day
*/

ALTER TABLE cabinet.prj_work ADD COLUMN date_time_beg timestamp without time zone;
ALTER TABLE cabinet.prj_work ADD COLUMN date_time_end timestamp without time zone;
ALTER TABLE cabinet.prj_work ADD COLUMN is_all_day boolean NOT NULL DEFAULT true;

UPDATE cabinet.prj_work
SET date_time_beg = date_beg, date_time_end = date_end;

UPDATE cabinet.prj_work t1
SET is_all_day = t2.is_all_day, date_time_beg = t2.date_time_beg, date_time_end = t2.date_time_end
FROM cabinet.prj_work_exec t2
WHERE t1.work_id = t2.work_id
AND t2.is_current = true;