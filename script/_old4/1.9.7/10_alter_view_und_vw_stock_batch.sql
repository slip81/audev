﻿/*
	CREATE OR REPLACE VIEW und.vw_stock_batch
*/

-- DROP VIEW und.vw_stock_batch;

CREATE OR REPLACE VIEW und.vw_stock_batch AS 
 SELECT t1.batch_id,
    t1.stock_id,
    t1.batch_num,
    t1.part_cnt,
    t1.part_num,
    t1.row_cnt,
    t1.is_active,
    t1.crt_date,
    t1.crt_user,
    t1.upd_date,
    t1.upd_user,
    t2.client_id,
    t2.sales_id,
    t2.depart_id,
    t2.depart_name,
    t2.pharmacy_name,
    t3.name AS client_name,
    t4.adress AS sales_name,
    t1.inactive_date
   FROM und.stock_batch t1
     JOIN und.stock t2 ON t1.stock_id = t2.stock_id
     JOIN cabinet.client t3 ON t2.client_id = t3.id
     JOIN cabinet.sales t4 ON t2.sales_id = t4.id
  WHERE t1.is_deleted = false;

ALTER TABLE und.vw_stock_batch OWNER TO pavlov;
