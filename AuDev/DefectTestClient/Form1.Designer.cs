﻿namespace DefectTestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radButton12 = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radButton8 = new Telerik.WinControls.UI.RadButton();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox10 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox9 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox8 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox7 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox6 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox5 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.radDateTimePicker3 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker4 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridView2 = new Telerik.WinControls.UI.RadGridView();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.splitPanel6 = new Telerik.WinControls.UI.SplitPanel();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel7 = new Telerik.WinControls.UI.SplitPanel();
            this.radLabel13 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox17 = new Telerik.WinControls.UI.RadTextBox();
            this.radDateTimePicker6 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker5 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radButton11 = new Telerik.WinControls.UI.RadButton();
            this.radButton9 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox13 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox12 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox11 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.splitPanel8 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridView3 = new Telerik.WinControls.UI.RadGridView();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.splitPanel9 = new Telerik.WinControls.UI.SplitPanel();
            this.radPageViewPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radSplitContainer4 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel10 = new Telerik.WinControls.UI.SplitPanel();
            this.radLabel14 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel15 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel16 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox14 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox15 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox16 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton10 = new Telerik.WinControls.UI.RadButton();
            this.splitPanel11 = new Telerik.WinControls.UI.SplitPanel();
            this.radGridView4 = new Telerik.WinControls.UI.RadGridView();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.splitPanel12 = new Telerik.WinControls.UI.SplitPanel();
            this.radButton13 = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            this.radPageViewPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            this.splitPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).BeginInit();
            this.radPageViewPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel7)).BeginInit();
            this.splitPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel8)).BeginInit();
            this.splitPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel9)).BeginInit();
            this.radPageViewPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer4)).BeginInit();
            this.radSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel10)).BeginInit();
            this.splitPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel11)).BeginInit();
            this.splitPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton13)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Controls.Add(this.splitPanel3);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(792, 521);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radButton13);
            this.splitPanel1.Controls.Add(this.radButton12);
            this.splitPanel1.Controls.Add(this.radCheckBox2);
            this.splitPanel1.Controls.Add(this.radCheckBox1);
            this.splitPanel1.Controls.Add(this.radLabel7);
            this.splitPanel1.Controls.Add(this.radLabel6);
            this.splitPanel1.Controls.Add(this.radLabel5);
            this.splitPanel1.Controls.Add(this.radLabel4);
            this.splitPanel1.Controls.Add(this.radLabel3);
            this.splitPanel1.Controls.Add(this.radLabel2);
            this.splitPanel1.Controls.Add(this.radLabel1);
            this.splitPanel1.Controls.Add(this.radButton8);
            this.splitPanel1.Controls.Add(this.radButton7);
            this.splitPanel1.Controls.Add(this.radTextBox10);
            this.splitPanel1.Controls.Add(this.radTextBox9);
            this.splitPanel1.Controls.Add(this.radTextBox8);
            this.splitPanel1.Controls.Add(this.radTextBox7);
            this.splitPanel1.Controls.Add(this.radTextBox6);
            this.splitPanel1.Controls.Add(this.radTextBox5);
            this.splitPanel1.Controls.Add(this.radTextBox1);
            this.splitPanel1.Controls.Add(this.radButton2);
            this.splitPanel1.Controls.Add(this.radTextBox2);
            this.splitPanel1.Controls.Add(this.radDateTimePicker2);
            this.splitPanel1.Controls.Add(this.radDateTimePicker1);
            this.splitPanel1.Controls.Add(this.radButton3);
            this.splitPanel1.Controls.Add(this.radButton1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(792, 294);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2397661F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 142);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radButton12
            // 
            this.radButton12.Location = new System.Drawing.Point(598, 59);
            this.radButton12.Name = "radButton12";
            this.radButton12.Size = new System.Drawing.Size(174, 27);
            this.radButton12.TabIndex = 28;
            this.radButton12.Text = "Скачать ГР";
            this.radButton12.Click += new System.EventHandler(this.radButton12_Click);
            // 
            // radCheckBox2
            // 
            this.radCheckBox2.Location = new System.Drawing.Point(138, 159);
            this.radCheckBox2.Name = "radCheckBox2";
            this.radCheckBox2.Size = new System.Drawing.Size(65, 18);
            this.radCheckBox2.TabIndex = 27;
            this.radCheckBox2.Text = "в архиве";
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.Location = new System.Drawing.Point(212, 41);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(113, 18);
            this.radCheckBox1.TabIndex = 26;
            this.radCheckBox1.Text = "скачивать письма";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(511, 111);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(72, 18);
            this.radLabel7.TabIndex = 25;
            this.radLabel7.Text = "код запроса:";
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(360, 111);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(76, 18);
            this.radLabel6.TabIndex = 24;
            this.radLabel6.Text = "дата запроса:";
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(209, 111);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(82, 18);
            this.radLabel5.TabIndex = 23;
            this.radLabel5.Text = "всего записей:";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(511, 59);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(45, 18);
            this.radLabel4.TabIndex = 22;
            this.radLabel4.Text = "версия:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(360, 59);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(86, 18);
            this.radLabel3.TabIndex = 21;
            this.radLabel3.Text = "рабочее место:";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(209, 59);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(39, 18);
            this.radLabel2.TabIndex = 20;
            this.radLabel2.Text = "логин:";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(24, 42);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(46, 18);
            this.radLabel1.TabIndex = 19;
            this.radLabel1.Text = "пароль:";
            // 
            // radButton8
            // 
            this.radButton8.Location = new System.Drawing.Point(598, 132);
            this.radButton8.Name = "radButton8";
            this.radButton8.Size = new System.Drawing.Size(174, 27);
            this.radButton8.TabIndex = 18;
            this.radButton8.Text = "Подтвердить запрос";
            this.radButton8.Click += new System.EventHandler(this.radButton8_Click);
            // 
            // radButton7
            // 
            this.radButton7.Location = new System.Drawing.Point(24, 132);
            this.radButton7.Name = "radButton7";
            this.radButton7.Size = new System.Drawing.Size(179, 27);
            this.radButton7.TabIndex = 17;
            this.radButton7.Text = "Получить XML (все)";
            this.radButton7.Click += new System.EventHandler(this.radButton7_Click);
            // 
            // radTextBox10
            // 
            this.radTextBox10.Location = new System.Drawing.Point(511, 135);
            this.radTextBox10.Name = "radTextBox10";
            this.radTextBox10.Size = new System.Drawing.Size(64, 20);
            this.radTextBox10.TabIndex = 16;
            // 
            // radTextBox9
            // 
            this.radTextBox9.Enabled = false;
            this.radTextBox9.Location = new System.Drawing.Point(360, 135);
            this.radTextBox9.Name = "radTextBox9";
            this.radTextBox9.Size = new System.Drawing.Size(131, 20);
            this.radTextBox9.TabIndex = 15;
            // 
            // radTextBox8
            // 
            this.radTextBox8.Enabled = false;
            this.radTextBox8.Location = new System.Drawing.Point(209, 135);
            this.radTextBox8.Name = "radTextBox8";
            this.radTextBox8.Size = new System.Drawing.Size(131, 20);
            this.radTextBox8.TabIndex = 14;
            // 
            // radTextBox7
            // 
            this.radTextBox7.Location = new System.Drawing.Point(511, 83);
            this.radTextBox7.Name = "radTextBox7";
            this.radTextBox7.Size = new System.Drawing.Size(64, 20);
            this.radTextBox7.TabIndex = 13;
            this.radTextBox7.Text = "3.2.0.0";
            // 
            // radTextBox6
            // 
            this.radTextBox6.Location = new System.Drawing.Point(360, 83);
            this.radTextBox6.Name = "radTextBox6";
            this.radTextBox6.Size = new System.Drawing.Size(131, 20);
            this.radTextBox6.TabIndex = 12;
            this.radTextBox6.Text = "0A86-A76B-6978-5D4B";
            // 
            // radTextBox5
            // 
            this.radTextBox5.Location = new System.Drawing.Point(209, 83);
            this.radTextBox5.Name = "radTextBox5";
            this.radTextBox5.Size = new System.Drawing.Size(131, 20);
            this.radTextBox5.TabIndex = 11;
            this.radTextBox5.Text = "aurit_803";
            // 
            // radTextBox1
            // 
            this.radTextBox1.AutoSize = false;
            this.radTextBox1.Location = new System.Drawing.Point(24, 219);
            this.radTextBox1.Multiline = true;
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.ReadOnly = true;
            this.radTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.radTextBox1.Size = new System.Drawing.Size(748, 61);
            this.radTextBox1.TabIndex = 8;
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(24, 76);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(179, 27);
            this.radButton2.TabIndex = 7;
            this.radButton2.Text = "Получить XML (только новое)";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(73, 41);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.PasswordChar = '*';
            this.radTextBox2.Size = new System.Drawing.Size(130, 20);
            this.radTextBox2.TabIndex = 6;
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.Location = new System.Drawing.Point(360, 12);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            this.radDateTimePicker2.Size = new System.Drawing.Size(131, 20);
            this.radDateTimePicker2.TabIndex = 5;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.Text = "13 сентября 2015 г.";
            this.radDateTimePicker2.Value = new System.DateTime(2015, 9, 13, 15, 34, 52, 85);
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.Location = new System.Drawing.Point(209, 12);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.Size = new System.Drawing.Size(131, 20);
            this.radDateTimePicker1.TabIndex = 4;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Text = "13 сентября 2015 г.";
            this.radDateTimePicker1.Value = new System.DateTime(2015, 9, 13, 15, 34, 52, 85);
            // 
            // radButton3
            // 
            this.radButton3.Location = new System.Drawing.Point(503, 12);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(212, 27);
            this.radButton3.TabIndex = 3;
            this.radButton3.Text = "Посмотреть список брака за период";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(24, 12);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(179, 27);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "Скачать брак с сайта";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radGridView1);
            this.splitPanel2.Controls.Add(this.radPanel1);
            this.splitPanel2.Location = new System.Drawing.Point(0, 298);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(792, 195);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.04678361F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -28);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radGridView1
            // 
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Location = new System.Drawing.Point(0, 17);
            // 
            // 
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterTemplate.AllowEditRow = false;
            this.radGridView1.MasterTemplate.EnableFiltering = true;
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.ReadOnly = true;
            this.radGridView1.Size = new System.Drawing.Size(792, 178);
            this.radGridView1.TabIndex = 1;
            this.radGridView1.Text = "radGridView1";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.MintCream;
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(792, 17);
            this.radPanel1.TabIndex = 0;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Location = new System.Drawing.Point(0, 497);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(792, 24);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2865497F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -114);
            this.splitPanel3.TabIndex = 2;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Controls.Add(this.radPageViewPage4);
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(1101, 529);
            this.radPageView1.TabIndex = 1;
            this.radPageView1.Text = "Брак: позиции";
            this.radPageView1.ViewMode = Telerik.WinControls.UI.PageViewMode.Backstage;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.radSplitContainer1);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(150F, 45F);
            this.radPageViewPage1.Location = new System.Drawing.Point(305, 4);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(792, 521);
            this.radPageViewPage1.Text = "Брак: позиции";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.Controls.Add(this.radSplitContainer2);
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(150F, 45F);
            this.radPageViewPage2.Location = new System.Drawing.Point(305, 4);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(792, 521);
            this.radPageViewPage2.Text = "Брак: письма";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Controls.Add(this.splitPanel5);
            this.radSplitContainer2.Controls.Add(this.splitPanel6);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            this.radSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer2.Size = new System.Drawing.Size(792, 521);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            this.radSplitContainer2.Text = "radSplitContainer2";
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.radTextBox3);
            this.splitPanel4.Controls.Add(this.radButton5);
            this.splitPanel4.Controls.Add(this.radDateTimePicker3);
            this.splitPanel4.Controls.Add(this.radDateTimePicker4);
            this.splitPanel4.Controls.Add(this.radButton4);
            this.splitPanel4.Location = new System.Drawing.Point(0, 0);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel4.Size = new System.Drawing.Size(792, 106);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.127451F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -61);
            this.splitPanel4.TabIndex = 0;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // radTextBox3
            // 
            this.radTextBox3.Location = new System.Drawing.Point(250, 60);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(293, 20);
            this.radTextBox3.TabIndex = 10;
            // 
            // radButton5
            // 
            this.radButton5.Location = new System.Drawing.Point(17, 53);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(210, 27);
            this.radButton5.TabIndex = 9;
            this.radButton5.Text = "Открыть письмо по ссылке";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click);
            // 
            // radDateTimePicker3
            // 
            this.radDateTimePicker3.Location = new System.Drawing.Point(250, 11);
            this.radDateTimePicker3.Name = "radDateTimePicker3";
            this.radDateTimePicker3.Size = new System.Drawing.Size(131, 20);
            this.radDateTimePicker3.TabIndex = 8;
            this.radDateTimePicker3.TabStop = false;
            this.radDateTimePicker3.Text = "13 сентября 2015 г.";
            this.radDateTimePicker3.Value = new System.DateTime(2015, 9, 13, 15, 34, 52, 85);
            // 
            // radDateTimePicker4
            // 
            this.radDateTimePicker4.Location = new System.Drawing.Point(400, 11);
            this.radDateTimePicker4.Name = "radDateTimePicker4";
            this.radDateTimePicker4.Size = new System.Drawing.Size(131, 20);
            this.radDateTimePicker4.TabIndex = 7;
            this.radDateTimePicker4.TabStop = false;
            this.radDateTimePicker4.Text = "13 сентября 2015 г.";
            this.radDateTimePicker4.Value = new System.DateTime(2015, 9, 13, 15, 34, 52, 85);
            // 
            // radButton4
            // 
            this.radButton4.Location = new System.Drawing.Point(17, 8);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(210, 27);
            this.radButton4.TabIndex = 6;
            this.radButton4.Text = "Посмотреть список писем за период";
            this.radButton4.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // splitPanel5
            // 
            this.splitPanel5.Controls.Add(this.radGridView2);
            this.splitPanel5.Controls.Add(this.radPanel2);
            this.splitPanel5.Location = new System.Drawing.Point(0, 110);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel5.Size = new System.Drawing.Size(792, 363);
            this.splitPanel5.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.3746499F);
            this.splitPanel5.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 178);
            this.splitPanel5.TabIndex = 1;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // radGridView2
            // 
            this.radGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView2.Location = new System.Drawing.Point(0, 35);
            // 
            // 
            // 
            this.radGridView2.MasterTemplate.AllowAddNewRow = false;
            this.radGridView2.MasterTemplate.AllowDeleteRow = false;
            this.radGridView2.MasterTemplate.AllowEditRow = false;
            this.radGridView2.MasterTemplate.EnableFiltering = true;
            this.radGridView2.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridView2.Name = "radGridView2";
            this.radGridView2.ReadOnly = true;
            this.radGridView2.Size = new System.Drawing.Size(792, 328);
            this.radGridView2.TabIndex = 2;
            this.radGridView2.Text = "radGridView2";
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.MintCream;
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel2.Location = new System.Drawing.Point(0, 0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(792, 35);
            this.radPanel2.TabIndex = 1;
            // 
            // splitPanel6
            // 
            this.splitPanel6.Location = new System.Drawing.Point(0, 477);
            this.splitPanel6.Name = "splitPanel6";
            // 
            // 
            // 
            this.splitPanel6.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel6.Size = new System.Drawing.Size(792, 44);
            this.splitPanel6.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2471989F);
            this.splitPanel6.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -117);
            this.splitPanel6.TabIndex = 2;
            this.splitPanel6.TabStop = false;
            this.splitPanel6.Text = "splitPanel6";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.Controls.Add(this.radSplitContainer3);
            this.radPageViewPage3.ItemSize = new System.Drawing.SizeF(150F, 45F);
            this.radPageViewPage3.Location = new System.Drawing.Point(305, 4);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(792, 521);
            this.radPageViewPage3.Text = "Брак: лицензии";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel7);
            this.radSplitContainer3.Controls.Add(this.splitPanel8);
            this.radSplitContainer3.Controls.Add(this.splitPanel9);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            this.radSplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer3.Size = new System.Drawing.Size(792, 521);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            this.radSplitContainer3.Text = "radSplitContainer3";
            // 
            // splitPanel7
            // 
            this.splitPanel7.Controls.Add(this.radLabel13);
            this.splitPanel7.Controls.Add(this.radLabel12);
            this.splitPanel7.Controls.Add(this.radLabel11);
            this.splitPanel7.Controls.Add(this.radLabel8);
            this.splitPanel7.Controls.Add(this.radLabel9);
            this.splitPanel7.Controls.Add(this.radLabel10);
            this.splitPanel7.Controls.Add(this.radTextBox17);
            this.splitPanel7.Controls.Add(this.radDateTimePicker6);
            this.splitPanel7.Controls.Add(this.radDateTimePicker5);
            this.splitPanel7.Controls.Add(this.radButton11);
            this.splitPanel7.Controls.Add(this.radButton9);
            this.splitPanel7.Controls.Add(this.radTextBox13);
            this.splitPanel7.Controls.Add(this.radTextBox12);
            this.splitPanel7.Controls.Add(this.radTextBox11);
            this.splitPanel7.Controls.Add(this.radTextBox4);
            this.splitPanel7.Controls.Add(this.radButton6);
            this.splitPanel7.Location = new System.Drawing.Point(0, 0);
            this.splitPanel7.Name = "splitPanel7";
            // 
            // 
            // 
            this.splitPanel7.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel7.Size = new System.Drawing.Size(792, 158);
            this.splitPanel7.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.02534114F);
            this.splitPanel7.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -13);
            this.splitPanel7.TabIndex = 0;
            this.splitPanel7.TabStop = false;
            this.splitPanel7.Text = "splitPanel7";
            // 
            // radLabel13
            // 
            this.radLabel13.Location = new System.Drawing.Point(655, 107);
            this.radLabel13.Name = "radLabel13";
            this.radLabel13.Size = new System.Drawing.Size(46, 18);
            this.radLabel13.TabIndex = 28;
            this.radLabel13.Text = "пароль:";
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(537, 107);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(76, 18);
            this.radLabel12.TabIndex = 27;
            this.radLabel12.Text = "действует по:";
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(414, 107);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(68, 18);
            this.radLabel11.TabIndex = 26;
            this.radLabel11.Text = "действует с:";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(479, 10);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(45, 18);
            this.radLabel8.TabIndex = 25;
            this.radLabel8.Text = "версия:";
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(270, 10);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(86, 18);
            this.radLabel9.TabIndex = 24;
            this.radLabel9.Text = "рабочее место:";
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(163, 10);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(39, 18);
            this.radLabel10.TabIndex = 23;
            this.radLabel10.Text = "логин:";
            // 
            // radTextBox17
            // 
            this.radTextBox17.Location = new System.Drawing.Point(655, 131);
            this.radTextBox17.Name = "radTextBox17";
            this.radTextBox17.PasswordChar = '*';
            this.radTextBox17.Size = new System.Drawing.Size(129, 20);
            this.radTextBox17.TabIndex = 20;
            // 
            // radDateTimePicker6
            // 
            this.radDateTimePicker6.Location = new System.Drawing.Point(537, 131);
            this.radDateTimePicker6.Name = "radDateTimePicker6";
            this.radDateTimePicker6.Size = new System.Drawing.Size(117, 20);
            this.radDateTimePicker6.TabIndex = 19;
            this.radDateTimePicker6.TabStop = false;
            this.radDateTimePicker6.Text = "22 сентября 2015 г.";
            this.radDateTimePicker6.Value = new System.DateTime(2015, 9, 22, 20, 8, 44, 405);
            // 
            // radDateTimePicker5
            // 
            this.radDateTimePicker5.Location = new System.Drawing.Point(414, 131);
            this.radDateTimePicker5.Name = "radDateTimePicker5";
            this.radDateTimePicker5.Size = new System.Drawing.Size(117, 20);
            this.radDateTimePicker5.TabIndex = 18;
            this.radDateTimePicker5.TabStop = false;
            this.radDateTimePicker5.Text = "22 сентября 2015 г.";
            this.radDateTimePicker5.Value = new System.DateTime(2015, 9, 22, 20, 8, 44, 405);
            // 
            // radButton11
            // 
            this.radButton11.Location = new System.Drawing.Point(296, 128);
            this.radButton11.Name = "radButton11";
            this.radButton11.Size = new System.Drawing.Size(112, 27);
            this.radButton11.TabIndex = 17;
            this.radButton11.Text = "Поменять даты";
            this.radButton11.Click += new System.EventHandler(this.radButton11_Click);
            // 
            // radButton9
            // 
            this.radButton9.Location = new System.Drawing.Point(14, 128);
            this.radButton9.Name = "radButton9";
            this.radButton9.Size = new System.Drawing.Size(189, 27);
            this.radButton9.TabIndex = 16;
            this.radButton9.Text = "Посмотреть список лицензий";
            this.radButton9.Click += new System.EventHandler(this.radButton9_Click);
            // 
            // radTextBox13
            // 
            this.radTextBox13.Location = new System.Drawing.Point(479, 34);
            this.radTextBox13.Name = "radTextBox13";
            this.radTextBox13.Size = new System.Drawing.Size(88, 20);
            this.radTextBox13.TabIndex = 15;
            this.radTextBox13.Text = "3.2.0.0";
            // 
            // radTextBox12
            // 
            this.radTextBox12.Location = new System.Drawing.Point(270, 34);
            this.radTextBox12.Name = "radTextBox12";
            this.radTextBox12.Size = new System.Drawing.Size(203, 20);
            this.radTextBox12.TabIndex = 14;
            this.radTextBox12.Text = "D095-AAA9-78C7-7BA9";
            // 
            // radTextBox11
            // 
            this.radTextBox11.Location = new System.Drawing.Point(165, 34);
            this.radTextBox11.Name = "radTextBox11";
            this.radTextBox11.Size = new System.Drawing.Size(99, 20);
            this.radTextBox11.TabIndex = 13;
            this.radTextBox11.Text = "apteka_ul2";
            // 
            // radTextBox4
            // 
            this.radTextBox4.AutoSize = false;
            this.radTextBox4.Location = new System.Drawing.Point(13, 60);
            this.radTextBox4.Multiline = true;
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.Size = new System.Drawing.Size(748, 41);
            this.radTextBox4.TabIndex = 12;
            // 
            // radButton6
            // 
            this.radButton6.Location = new System.Drawing.Point(14, 27);
            this.radButton6.Name = "radButton6";
            this.radButton6.Size = new System.Drawing.Size(145, 27);
            this.radButton6.TabIndex = 11;
            this.radButton6.Text = "Посмотреть лицензию";
            this.radButton6.Click += new System.EventHandler(this.radButton6_Click);
            // 
            // splitPanel8
            // 
            this.splitPanel8.Controls.Add(this.radGridView3);
            this.splitPanel8.Controls.Add(this.radPanel3);
            this.splitPanel8.Location = new System.Drawing.Point(0, 162);
            this.splitPanel8.Name = "splitPanel8";
            // 
            // 
            // 
            this.splitPanel8.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel8.Size = new System.Drawing.Size(792, 331);
            this.splitPanel8.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.3118908F);
            this.splitPanel8.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 160);
            this.splitPanel8.TabIndex = 1;
            this.splitPanel8.TabStop = false;
            this.splitPanel8.Text = "splitPanel8";
            // 
            // radGridView3
            // 
            this.radGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView3.Location = new System.Drawing.Point(0, 35);
            // 
            // 
            // 
            this.radGridView3.MasterTemplate.AllowAddNewRow = false;
            this.radGridView3.MasterTemplate.AllowDeleteRow = false;
            this.radGridView3.MasterTemplate.AllowEditRow = false;
            this.radGridView3.MasterTemplate.EnableFiltering = true;
            this.radGridView3.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.radGridView3.Name = "radGridView3";
            this.radGridView3.ReadOnly = true;
            this.radGridView3.Size = new System.Drawing.Size(792, 296);
            this.radGridView3.TabIndex = 3;
            this.radGridView3.Text = "radGridView3";
            this.radGridView3.SelectionChanged += new System.EventHandler(this.radGridView3_SelectionChanged);
            // 
            // radPanel3
            // 
            this.radPanel3.BackColor = System.Drawing.Color.MintCream;
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel3.Location = new System.Drawing.Point(0, 0);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(792, 35);
            this.radPanel3.TabIndex = 2;
            // 
            // splitPanel9
            // 
            this.splitPanel9.Location = new System.Drawing.Point(0, 497);
            this.splitPanel9.Name = "splitPanel9";
            // 
            // 
            // 
            this.splitPanel9.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel9.Size = new System.Drawing.Size(792, 24);
            this.splitPanel9.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2865497F);
            this.splitPanel9.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -147);
            this.splitPanel9.TabIndex = 2;
            this.splitPanel9.TabStop = false;
            this.splitPanel9.Text = "splitPanel9";
            // 
            // radPageViewPage4
            // 
            this.radPageViewPage4.Controls.Add(this.radSplitContainer4);
            this.radPageViewPage4.ItemSize = new System.Drawing.SizeF(150F, 45F);
            this.radPageViewPage4.Location = new System.Drawing.Point(305, 4);
            this.radPageViewPage4.Name = "radPageViewPage4";
            this.radPageViewPage4.Size = new System.Drawing.Size(792, 521);
            this.radPageViewPage4.Text = "Брак: история запросов";
            // 
            // radSplitContainer4
            // 
            this.radSplitContainer4.Controls.Add(this.splitPanel10);
            this.radSplitContainer4.Controls.Add(this.splitPanel11);
            this.radSplitContainer4.Controls.Add(this.splitPanel12);
            this.radSplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer4.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer4.Name = "radSplitContainer4";
            this.radSplitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer4.Size = new System.Drawing.Size(792, 521);
            this.radSplitContainer4.TabIndex = 0;
            this.radSplitContainer4.TabStop = false;
            this.radSplitContainer4.Text = "radSplitContainer4";
            // 
            // splitPanel10
            // 
            this.splitPanel10.Controls.Add(this.radLabel14);
            this.splitPanel10.Controls.Add(this.radLabel15);
            this.splitPanel10.Controls.Add(this.radLabel16);
            this.splitPanel10.Controls.Add(this.radTextBox14);
            this.splitPanel10.Controls.Add(this.radTextBox15);
            this.splitPanel10.Controls.Add(this.radTextBox16);
            this.splitPanel10.Controls.Add(this.radButton10);
            this.splitPanel10.Location = new System.Drawing.Point(0, 0);
            this.splitPanel10.Name = "splitPanel10";
            // 
            // 
            // 
            this.splitPanel10.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel10.Size = new System.Drawing.Size(792, 73);
            this.splitPanel10.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.1910332F);
            this.splitPanel10.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -98);
            this.splitPanel10.TabIndex = 0;
            this.splitPanel10.TabStop = false;
            this.splitPanel10.Text = "splitPanel10";
            // 
            // radLabel14
            // 
            this.radLabel14.Location = new System.Drawing.Point(553, 8);
            this.radLabel14.Name = "radLabel14";
            this.radLabel14.Size = new System.Drawing.Size(45, 18);
            this.radLabel14.TabIndex = 28;
            this.radLabel14.Text = "версия:";
            // 
            // radLabel15
            // 
            this.radLabel15.Location = new System.Drawing.Point(346, 8);
            this.radLabel15.Name = "radLabel15";
            this.radLabel15.Size = new System.Drawing.Size(86, 18);
            this.radLabel15.TabIndex = 27;
            this.radLabel15.Text = "рабочее место:";
            // 
            // radLabel16
            // 
            this.radLabel16.Location = new System.Drawing.Point(239, 8);
            this.radLabel16.Name = "radLabel16";
            this.radLabel16.Size = new System.Drawing.Size(39, 18);
            this.radLabel16.TabIndex = 26;
            this.radLabel16.Text = "логин:";
            // 
            // radTextBox14
            // 
            this.radTextBox14.Location = new System.Drawing.Point(553, 29);
            this.radTextBox14.Name = "radTextBox14";
            this.radTextBox14.Size = new System.Drawing.Size(88, 20);
            this.radTextBox14.TabIndex = 19;
            this.radTextBox14.Text = "3.2.0.0";
            // 
            // radTextBox15
            // 
            this.radTextBox15.Location = new System.Drawing.Point(344, 29);
            this.radTextBox15.Name = "radTextBox15";
            this.radTextBox15.Size = new System.Drawing.Size(203, 20);
            this.radTextBox15.TabIndex = 18;
            this.radTextBox15.Text = "D095-AAA9-78C7-7BA9";
            // 
            // radTextBox16
            // 
            this.radTextBox16.Location = new System.Drawing.Point(239, 29);
            this.radTextBox16.Name = "radTextBox16";
            this.radTextBox16.Size = new System.Drawing.Size(99, 20);
            this.radTextBox16.TabIndex = 17;
            this.radTextBox16.Text = "apteka_ul2";
            // 
            // radButton10
            // 
            this.radButton10.Location = new System.Drawing.Point(16, 22);
            this.radButton10.Name = "radButton10";
            this.radButton10.Size = new System.Drawing.Size(206, 27);
            this.radButton10.TabIndex = 16;
            this.radButton10.Text = "Посмотреть историю запросов";
            this.radButton10.Click += new System.EventHandler(this.radButton10_Click);
            // 
            // splitPanel11
            // 
            this.splitPanel11.Controls.Add(this.radGridView4);
            this.splitPanel11.Controls.Add(this.radPanel4);
            this.splitPanel11.Location = new System.Drawing.Point(0, 77);
            this.splitPanel11.Name = "splitPanel11";
            // 
            // 
            // 
            this.splitPanel11.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel11.Size = new System.Drawing.Size(792, 375);
            this.splitPanel11.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.3976608F);
            this.splitPanel11.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 204);
            this.splitPanel11.TabIndex = 1;
            this.splitPanel11.TabStop = false;
            this.splitPanel11.Text = "splitPanel11";
            // 
            // radGridView4
            // 
            this.radGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView4.Location = new System.Drawing.Point(0, 35);
            // 
            // 
            // 
            this.radGridView4.MasterTemplate.AllowAddNewRow = false;
            this.radGridView4.MasterTemplate.AllowDeleteRow = false;
            this.radGridView4.MasterTemplate.AllowEditRow = false;
            this.radGridView4.MasterTemplate.EnableFiltering = true;
            this.radGridView4.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.radGridView4.Name = "radGridView4";
            this.radGridView4.ReadOnly = true;
            this.radGridView4.Size = new System.Drawing.Size(792, 340);
            this.radGridView4.TabIndex = 4;
            this.radGridView4.Text = "radGridView4";
            // 
            // radPanel4
            // 
            this.radPanel4.BackColor = System.Drawing.Color.MintCream;
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel4.Location = new System.Drawing.Point(0, 0);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(792, 35);
            this.radPanel4.TabIndex = 3;
            // 
            // splitPanel12
            // 
            this.splitPanel12.Location = new System.Drawing.Point(0, 456);
            this.splitPanel12.Name = "splitPanel12";
            // 
            // 
            // 
            this.splitPanel12.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel12.Size = new System.Drawing.Size(792, 65);
            this.splitPanel12.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2066277F);
            this.splitPanel12.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -106);
            this.splitPanel12.TabIndex = 2;
            this.splitPanel12.TabStop = false;
            this.splitPanel12.Text = "splitPanel12";
            // 
            // radButton13
            // 
            this.radButton13.Location = new System.Drawing.Point(24, 183);
            this.radButton13.Name = "radButton13";
            this.radButton13.Size = new System.Drawing.Size(179, 27);
            this.radButton13.TabIndex = 29;
            this.radButton13.Text = "Скачать мед. изделия с сайта";
            this.radButton13.Click += new System.EventHandler(this.radButton13_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1101, 529);
            this.Controls.Add(this.radPageView1);
            this.Name = "Form1";
            this.Text = "Брак";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            this.splitPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            this.radPageViewPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            this.splitPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            this.splitPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).EndInit();
            this.radPageViewPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel7)).EndInit();
            this.splitPanel7.ResumeLayout(false);
            this.splitPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel8)).EndInit();
            this.splitPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel9)).EndInit();
            this.radPageViewPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer4)).EndInit();
            this.radSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel10)).EndInit();
            this.splitPanel10.ResumeLayout(false);
            this.splitPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel11)).EndInit();
            this.splitPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton13)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker3;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker4;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.RadGridView radGridView2;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.SplitPanel splitPanel6;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel7;
        private Telerik.WinControls.UI.RadTextBox radTextBox4;
        private Telerik.WinControls.UI.RadButton radButton6;
        private Telerik.WinControls.UI.SplitPanel splitPanel8;
        private Telerik.WinControls.UI.SplitPanel splitPanel9;
        private Telerik.WinControls.UI.RadTextBox radTextBox7;
        private Telerik.WinControls.UI.RadTextBox radTextBox6;
        private Telerik.WinControls.UI.RadTextBox radTextBox5;
        private Telerik.WinControls.UI.RadTextBox radTextBox10;
        private Telerik.WinControls.UI.RadTextBox radTextBox9;
        private Telerik.WinControls.UI.RadTextBox radTextBox8;
        private Telerik.WinControls.UI.RadButton radButton7;
        private Telerik.WinControls.UI.RadButton radButton8;
        private Telerik.WinControls.UI.RadTextBox radTextBox11;
        private Telerik.WinControls.UI.RadTextBox radTextBox12;
        private Telerik.WinControls.UI.RadTextBox radTextBox13;
        private Telerik.WinControls.UI.RadButton radButton9;
        private Telerik.WinControls.UI.RadGridView radGridView3;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage4;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer4;
        private Telerik.WinControls.UI.SplitPanel splitPanel10;
        private Telerik.WinControls.UI.SplitPanel splitPanel11;
        private Telerik.WinControls.UI.SplitPanel splitPanel12;
        private Telerik.WinControls.UI.RadTextBox radTextBox14;
        private Telerik.WinControls.UI.RadTextBox radTextBox15;
        private Telerik.WinControls.UI.RadTextBox radTextBox16;
        private Telerik.WinControls.UI.RadButton radButton10;
        private Telerik.WinControls.UI.RadGridView radGridView4;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker6;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker5;
        private Telerik.WinControls.UI.RadButton radButton11;
        private Telerik.WinControls.UI.RadTextBox radTextBox17;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel16;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox2;
        private Telerik.WinControls.UI.RadButton radButton12;
        private Telerik.WinControls.UI.RadButton radButton13;
    }
}

