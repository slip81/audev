﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.IO.Compression;
using AuDev.Common.Util;

namespace DefectTestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //
            System.Net.ServicePointManager.Expect100Continue = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            radDateTimePicker1.Value = DateTime.Today;
            radDateTimePicker2.Value = DateTime.Today;
            radDateTimePicker3.Value = DateTime.Today;
            radDateTimePicker4.Value = DateTime.Today;
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            radGridView1.DataSource = null;
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.DownloadDefectList(radTextBox2.Text, radDateTimePicker1.Value, radDateTimePicker2.Value, radCheckBox1.Checked);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка скачивания списка брака");
                }
                else
                {
                    radGridView1.DataSource = res.Defect_list;
                    radGridView1.BestFitColumns();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton5_Click(object sender, EventArgs e)
        {
            OpenLink(radTextBox3.Text);
        }



        public void OpenLink(string sUrl)
        {

            try
            {

                System.Diagnostics.Process.Start(sUrl);

            }

            catch (Exception exc1)
            {

                // System.ComponentModel.Win32Exception is a known exception that occurs when Firefox is default browser.  

                // It actually opens the browser but STILL throws this exception so we can just ignore it.  If not this exception,

                // then attempt to open the URL in IE instead.

                if (exc1.GetType().ToString() != "System.ComponentModel.Win32Exception")
                {

                    // sometimes throws exception so we have to just ignore

                    // this is a common .NET bug that no one online really has a great reason for so now we just need to try to open

                    // the URL using IE if we can.

                    try
                    {

                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo("IExplore.exe", sUrl);

                        System.Diagnostics.Process.Start(startInfo);

                        startInfo = null;

                    }

                    catch (Exception exc2)
                    {

                        // still nothing we can do so just show the error to the user here.

                    }

                }

            }

        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            radGridView1.DataSource = null;
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetDefectList(radTextBox2.Text, radDateTimePicker1.Value, radDateTimePicker2.Value);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка загрузки списка брака");
                }
                else
                {
                    radGridView1.DataSource = res.Defect_list;
                    radGridView1.BestFitColumns();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton4_Click(object sender, EventArgs e)
        {
            radGridView2.DataSource = null;
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetDocumentList(radTextBox2.Text, radDateTimePicker3.Value, radDateTimePicker4.Value);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка загрузки списка документов");
                }
                else
                {
                    radGridView2.DataSource = res.Document_list;
                    radGridView2.BestFitColumns();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            radTextBox8.Text = "";
            radTextBox9.Text = "";
            radTextBox10.Text = "";
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetDefectXml(radTextBox5.Text, radTextBox6.Text, radTextBox7.Text);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка загрузки XML");
                }
                else
                {
                    radTextBox1.Text = res.Data;
                    radTextBox8.Text = res.RecordCount.ToString();
                    radTextBox9.Text = res.RequestDate.ToString();
                    radTextBox10.Text = res.RequestId.ToString();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton6_Click(object sender, EventArgs e)
        {
            /*
            radTextBox4.Text = "";
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetLicense(radTextBox11.Text, radTextBox12.Text, radTextBox13.Text);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка получения лицензии");
                }
                else
                {
                    radTextBox4.Text += res.SalesName.Trim();
                    radTextBox4.Text += "; ";
                    radTextBox4.Text += res.Workplace.Trim();
                    radTextBox4.Text += "; ";
                    radTextBox4.Text += res.VersionNum.Trim();
                    radTextBox4.Text += "; ";
                }                
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            */
        }

        private void radButton7_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            radTextBox8.Text = "";
            radTextBox9.Text = "";
            radTextBox10.Text = "";
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            DefectServ.DefectXml res = null;
            Cursor = Cursors.WaitCursor;
            try
            {
                if (!radCheckBox2.Checked)
                {
                    res = serv.GetDefectXmlInit(radTextBox5.Text, radTextBox6.Text, radTextBox7.Text);
                }
                else
                {
                    res = serv.GetDefectXmlInit_Arc(radTextBox5.Text, radTextBox6.Text, radTextBox7.Text);
                }
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка загрузки XML");
                }
                else
                {
                    if (!radCheckBox2.Checked)
                    {
                        radTextBox1.Text = res.Data;
                    }
                    else
                    {
                        //var res_str = GlobalUtil.DecompressString(res.Data);
                        //Encoding enc = Encoding.GetEncoding("windows-1251");
                        //File.WriteAllText(@"C:\remove.zip", res_str, enc);
                        //radTextBox1.Text = "done !";
                        var res_binary = res.DataBinary;
                        File.WriteAllBytes(@"C:\remove.zip", res_binary);
                        radTextBox1.Text = "done !";
                    }
                    radTextBox8.Text = res.RecordCount.ToString();
                    radTextBox9.Text = res.RequestDate.ToString();
                    radTextBox10.Text = res.RequestId.ToString();
                    
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton8_Click(object sender, EventArgs e)
        {
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.CommitRequest(radTextBox5.Text, radTextBox6.Text, radTextBox7.Text, Convert.ToInt32(radTextBox10.Text));
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка загрузки XML");
                }
                else
                {
                    MessageBox.Show("ok");
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton9_Click(object sender, EventArgs e)
        {
            /*
            radGridView3.DataSource = null;
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetLicenseList();
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка загрузки списка лицензий");
                }
                else
                {
                    radGridView3.DataSource = res.License_list;
                    radGridView3.BestFitColumns();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            */
        }

        private void radButton10_Click(object sender, EventArgs e)
        {
            radGridView4.DataSource = null;
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetRequestList(radTextBox16.Text, radTextBox15.Text, radTextBox14.Text);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка загрузки истории запросов");
                }
                else
                {
                    radGridView4.DataSource = res.Request_list;
                    radGridView4.BestFitColumns();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton11_Click(object sender, EventArgs e)
        {
            if (radGridView3.SelectedRows.Count <= 0)
                return;
            /*
            long id = Convert.ToInt64(radGridView3.SelectedRows[0].Cells["LicenseId"].Value);

            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.UpdateLicense(radTextBox17.Text, id, radDateTimePicker5.Value, radDateTimePicker6.Value);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка обновления лицензии");
                }
                else
                {

                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            */
        }

        private void radGridView3_SelectionChanged(object sender, EventArgs e)
        {
            if (radGridView3.SelectedRows.Count <= 0)
                return;
            radDateTimePicker5.Value = Convert.ToDateTime(radGridView3.SelectedRows[0].Cells["DateStart"].Value);
            radDateTimePicker6.Value = Convert.ToDateTime(radGridView3.SelectedRows[0].Cells["DateEnd"].Value);
        }

        private void radButton12_Click(object sender, EventArgs e)
        {            
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GR_DownloadList(radTextBox2.Text);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка скачивания ГР");
                }
                else
                {
                    MessageBox.Show("Файл скачан", "");
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton13_Click(object sender, EventArgs e)
        {
            radGridView1.DataSource = null;
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.DownloadMedicalList(radTextBox2.Text, radDateTimePicker1.Value, radDateTimePicker2.Value, radCheckBox1.Checked);
                if (res.error != null)
                {
                    MessageBox.Show(res.error.Message, "Ошибка скачивания списка мед. изделий");
                }
                else
                {
                    /*
                    radGridView1.DataSource = res.Medical_list;
                    radGridView1.BestFitColumns();
                    */
                    MessageBox.Show("Скачано " + res.Medical_list.Count.ToString() + " строк");
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        /*
        private string DecompressString(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }
        */

    }
}
