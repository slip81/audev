﻿function BusinessList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#businessListGrid', 0);

        $('#btnBusinessAdd').children('span').first().addClass('k-icon k-i-plus');
    }
}

var businessList = null;

$(function () {
    businessList = new BusinessList();
    businessList.init();
});
