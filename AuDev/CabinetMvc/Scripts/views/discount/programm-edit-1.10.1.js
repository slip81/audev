﻿function ProgrammEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $('.attr-change').bind('input propertychange', _this.onAttrChanged)

        $(document).keypress(function (e) {
            if (e.which == 13 && e.target.tagName != 'TEXTAREA') {
                return false;
            }
        });

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo2 = $("#cardTypeGroupList").data("kendoDropDownList");
            $("#card_type_group_id").val(combo2.dataItem()["card_type_group_id"]);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('btn-primary');
                                $('#btnSubmit').addClass('btn-default');
                                $('#btnSubmit').removeClass('active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-warning');
                                $('#btnRevert').addClass('hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();

            $("#programm_name").val(model_programm_name);
            $("#date_beg").getKendoDatePicker().value(model_date_beg);
            $("#date_end").getKendoDatePicker().value(model_date_end);
            $("#cardTypeGroupList").getKendoDropDownList().value(model_card_type_group_id);

            $('#btnSubmit').removeClass('btn-primary');
            $('#btnSubmit').addClass('btn-default');
            $('#btnSubmit').removeClass('active');
            $('#btnSubmit').attr('disabled', true);
            $('#btnRevert').removeClass('btn-warning');
            $('#btnRevert').addClass('hidden');
        });
    };

    this.onAttrChanged = function() {
        $('#btnSubmit').addClass('btn-primary');
        $('#btnSubmit').removeClass('btn-default');
        $('#btnSubmit').removeAttr('disabled');
        $('#btnSubmit').addClass('active');
        $('#btnRevert').removeClass('hidden');
        $('#btnRevert').addClass('btn-warning');
    };

}

var programmEdit = null;

$(function () {
    programmEdit = new ProgrammEdit();
    programmEdit.init();
});
