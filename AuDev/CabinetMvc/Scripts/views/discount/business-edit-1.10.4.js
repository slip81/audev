﻿function BusinessEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $('.attr-change').bind('input propertychange', _this.onAttrChanged);

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('btn-primary');
                                $('#btnSubmit').addClass('btn-default');
                                $('#btnSubmit').removeClass('active');
                                $('#btnSubmit').attr('disabled', true);                            
                                $('#btnRevert').removeClass('btn-warning');
                                $('#btnRevert').addClass('hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#IsCardScanOnly").click(function (e) {
            _this.onAttrChanged();
        });

        $("#IsScannerEqualsReader").click(function (e) {
            _this.onAttrChanged();
        });

        $("#allow_card_add").click(function (e) {
            _this.onAttrChanged();
        });

        $("#allow_phone_num").click(function (e) {
            _this.onAttrChanged();
        });

        $("#allow_card_nominal_change").click(function (e) {
            _this.onAttrChanged();
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();

            $("#business_name").val(model_business_name);
            $("#business_name_alt").val(model_business_name_alt);
            $("#add_to_timezone").getKendoNumericTextBox().value(model_add_to_timezone);
            if ('True' === model_IsCardScanOnly) {
                $("#IsCardScanOnly").prop({ checked: model_IsCardScanOnly });
            } else {
                $("#IsCardScanOnly").removeProp('checked');
            };
            if ('True' === model_IsScannerEqualsReader) {
                $("#IsScannerEqualsReader").prop({ checked: model_IsScannerEqualsReader });
            } else {
                $("#IsScannerEqualsReader").removeProp('checked');
            };
            if ('True' === model_allow_card_add) {
                $("#allow_card_add").prop({ checked: model_allow_card_add });
            } else {
                $("#allow_card_add").removeProp('checked');
            };
            if ('True' === model_allow_phone_num) {
                $("#allow_phone_num").prop({ checked: model_allow_phone_num });
            } else {
                $("#allow_phone_num").removeProp('checked');
            };
            //allow_card_nominal_change
            if ('True' === model_allow_card_nominal_change) {
                $("#allow_card_nominal_change").prop({ checked: model_allow_card_nominal_change });
            } else {
                $("#allow_card_nominal_change").removeProp('checked');
            };

            $('#btnSubmit').removeClass('btn-primary');
            $('#btnSubmit').addClass('btn-default');
            $('#btnSubmit').removeClass('active');
            $('#btnSubmit').attr('disabled', true);        
            $('#btnRevert').removeClass('btn-warning');
            $('#btnRevert').addClass('hidden');
        });
    }

    this.onRequestEnd = function (e) {
        if ((e.type == "create") || (e.type == "update")) {
            $("#businessOrgListGrid").data("kendoGrid").dataSource.read();
        }
    };

    this.onNumericSpin = function (e) {
        _this.onAttrChanged();
    };

    this.onAttrChanged = function () {
        $('#btnSubmit').addClass('btn-primary');
        $('#btnSubmit').removeClass('btn-default');
        $('#btnSubmit').removeAttr('disabled');
        $('#btnSubmit').addClass('active');
        $('#btnRevert').removeClass('hidden');        
        $('#btnRevert').addClass('btn-warning');
    };

    this.onUserEdit = function (e) {
        if (e.model.isNew() == false) {
            var ddl = $("#RoleId").getKendoDropDownList();
            if (ddl) {
                ddl.readonly();
            };
        };
    };

}

var businessEdit = null;

$(function () {
    businessEdit = new BusinessEdit();
    businessEdit.init();
});
