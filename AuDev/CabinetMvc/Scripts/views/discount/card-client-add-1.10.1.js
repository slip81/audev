﻿function CardClientAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            $('#card_num_rel').val($('#acCardNum').getKendoAutoComplete().value());

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
    };

    this.onAdditionalData = function(e) {
        var ctrl = $(e);
        var card_num_part = ctrl[0].filter.filters[0].value;
        return {
            text: card_num_part
        };
    };

}

var cardClientAdd = null;

$(function () {
    cardClientAdd = new CardClientAdd();
    cardClientAdd.init();
});
