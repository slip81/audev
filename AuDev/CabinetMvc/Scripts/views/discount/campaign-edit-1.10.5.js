﻿function CampaignEdit() {
    _this = this;

    this.init = function () {
        //$("form").kendoValidator();
        $('.section-edit-mode-only').addClass(class_for_section_edit_mode_only);
    };

    this.onBtnOrgForwardClick = function (e) {
        if (!validateStep(1))
            return;
        stepFromTo(1, 2);
    };

    this.onBtnDescrBackClick = function (e) {
        stepFromTo(2, 1);
    };

    this.onBtnDescrForwardClick = function (e) {
        if (!validateStep(2))
            return;
        stepFromTo(2, 3);
    };

    this.onBtnIssueRuleBackClick = function (e) {
        stepFromTo(3, 2);
    };

    this.onBtnIssueRuleForwardClick = function (e) {
        if (!validateStep(3))
            return;
        stepFromTo(3, 4);
    };

    this.onBtnCardParamBackClick = function (e) {
        stepFromTo(4, 3);
    };

    this.onBtnCardParamForwardClick = function (e) {
        if (!validateStep(4))
            return;
        stepFromTo(4, 5);
    };

    this.onBtnIssueConditionBackClick = function (e) {
        stepFromTo(5, 4);
    };

    this.onBtnIssueConditionForwardClick = function (e) {
        if (!validateStep(5))
            return;
        stepFromTo(5, 6);
    };

    this.onBtnApplyConditionBackClick = function (e) {
        stepFromTo(6, 5);
    };

    this.onBtnApplyConditionForwardClick = function (e) {
        if (!validateStep(6))
            return;
        stepFromTo(6, 7);
    };

    this.onBtnSaveDataBackClick = function (e) {
        stepFromTo(7, 6);
    };

    this.onBtnSaveDataClick = function (e) {
        e.preventDefault();
        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Сохранить изменения ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "80px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#save-changes-confirmation").html())
            .center().open();

        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        kendoWindow.data("kendoWindow").close();
                        $('#btnSubmit').removeClass('btn-primary');
                        $('#btnSubmit').addClass('btn-default');
                        $('#btnSubmit').removeClass('active');
                        $('#btnSubmit').attr('disabled', true);
                        $("#btnSubmitProceed").trigger('click');
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end();
    };

    this.checkAll = function (chb) {
        var state = $(chb).is(':checked');
        var grid = $("#orgListGrid").data("kendoGrid");
        var view = grid.dataSource.view();

        for (var i = 0; i < view.length; i++) {
            var dataRow = view[i];
            var elementRow = grid.table.find(".checkbox")[i];
            if (elementRow != null) {
                var checked = elementRow.checked,
                    row = $(elementRow).closest("tr"),
                    dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));

                if (state) {
                    elementRow.checked = true;
                } else {
                    elementRow.checked = false;
                }
            }
        };
    };

    this.onCampaignTabStripActivate = function (e) {
        $('#campaignTabStrip>ul>li.k-item').removeClass('k-state-default');
    };

    /******* private *********/

    function stepFromTo(from, to) {
        var prevIndex = from - 1;
        var nextIndex = to - 1;
        var tabStrip = $('#campaignTabStrip').data('kendoTabStrip');
        var prevTab = tabStrip.select();
        tabStrip.select(nextIndex);
        tabStrip.disable(prevTab);
        var nextTab = tabStrip.select();
        tabStrip.enable(nextTab, true);        
    };

    function validateStep(num) {
        $('#step' + num + '-wait').removeClass('hidden');
        switch (num) {
            case 1:                
                var grid = $("#orgListGrid").data("kendoGrid");
                var view = grid.dataSource.view();
                checkedIds = [];

                for (var i = 0; i < view.length; i++) {
                    var dataRow = view[i];
                    var elementRow = grid.table.find(".checkbox")[i];
                    var dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));

                    if ((elementRow != null) && (elementRow.checked)) {
                        checkedIds.push(dataItem.org_id_str);
                    }
                };
                if (checkedIds.length <= 0) {
                    $('#org_checked_ids').val('');
                    $('#orgListGrid').addClass('input-validation-error');
                    $('#step' + num + '-wait').addClass('hidden');
                    return false;
                } else {
                    $('#org_checked_ids').val(checkedIds.join());
                    $('#orgListGrid').removeClass('input-validation-error');
                    $('#step' + num + '-wait').addClass('hidden');
                    return true;
                }
            case 2:
                var d1 = $('#date_beg').data('kendoDatePicker').value();
                if (!d1)
                    $('#date_beg').addClass('input-validation-error');
                else
                    $('#date_beg').removeClass('input-validation-error');

                var d2 = $('#date_end').data('kendoDatePicker').value();
                if ((!d2) || (d2 < d1))
                    $('#date_end').addClass('input-validation-error');
                else
                    $('#date_end').removeClass('input-validation-error');

                if ((d1) && (d2) && (d2 >= d1)) {
                    var d3 = $('#issue_date_beg').data('kendoDatePicker');
                    var d3_value = d3.value();
                    if ((d3_value) && (d3_value < d1))
                        d3.value(d1);
                    d3.min(d1);
                    if ((d3_value) && (d3_value > d2))
                        d3.value(d2);
                    d3.max(d2);

                    var d4 = $('#issue_date_end').data('kendoDatePicker');
                    var d4_value = d4.value();
                    if ((d4_value) && (d4_value < d1))
                        d4.value(d1);
                    d4.min(d1);
                    if ((d4_value) && (d4_value > d2))
                        d4.value(d2);
                    d4.max(d2);
                }

                var t1 = $('#campaign_name').val();
                if (!t1)
                    $('#campaign_name').addClass('input-validation-error');
                else
                    $('#campaign_name').removeClass('input-validation-error');

                var t2 = $('#campaign_descr').val();
                if (!t2)
                    $('#campaign_descr').addClass('input-validation-error');
                else
                    $('#campaign_descr').removeClass('input-validation-error');

                var res = d1 && d2 && (d2 >= d1) && t1 && t2;

                var ok = true;
                if (res) {
                    $.ajax({
                        url: 'CampaignCheckDates',
                        type: "POST",
                        async: false,
                        contentType: 'application/json; charset=windows-1251',
                        data: JSON.stringify({ campaign_id: $('#campaign_id').val(), date_beg: d1, date_end: d2 }),
                        success: function (response) {                            
                            if ((response == null) || (response.err)) {
                                ok = false;
                                $('#date_beg').addClass('input-validation-error');
                                $('#date_end').addClass('input-validation-error');
                                //alert(response == null ? 'Ошибка при попытке проверить даты' : response.mess, "error");
                                $('#step2-validation-error').children('p').html(response == null ? 'Ошибка при попытке проверить даты' : response.mess);
                                $('#step2-validation-error').removeClass('hidden');
                            } else {
                                ok = true;
                                $('#step2-validation-error').children('p').html('');
                                $('#step2-validation-error').addClass('hidden');
                            };
                        },
                        error: function (response) {
                            ok = false;
                            $('#date_beg').addClass('input-validation-error');
                            $('#date_end').addClass('input-validation-error');
                            //alert('Ошибка при попытке проверить даты', "error");
                            $('#step2-validation-error').children('p').html('Ошибка при попытке проверить даты');
                            $('#step2-validation-error').removeClass('hidden');
                        }
                    });
                }                               

                $('#step' + num + '-wait').addClass('hidden');
                return res && ok;
            case 3:                
                $('#issue_rule').val($('input.k-radio[name="rb-group2"]:checked').val());

                if ($('#issue_rule').val() == issue_rule_InSaleForThisSale)
                    $('.section-auto-create').addClass('hidden');
                else
                    $('.section-auto-create').removeClass('hidden');
                
                $('#step' + num + '-wait').addClass('hidden');
                return true;
            case 4:
                var card_type_id_ddl = $('#cardTypeList').data('kendoDropDownList');
                var card_type_id_ind = card_type_id_ddl.select();
                $('#card_type_id').val(card_type_id_ind <= 0 ? null : card_type_id_ddl.value());

                var auto_create_card_is_checked = $('#auto_create_card').is(':checked');
                var max_card_cnt_value = $('#max_card_cnt').val();
                if ((auto_create_card_is_checked) && (max_card_cnt_value <= 0))
                    $('#max_card_cnt').parents('.k-numerictextbox').addClass('input-validation-error');
                else
                    $('#max_card_cnt').parents('.k-numerictextbox').removeClass('input-validation-error');

                $('#step' + num + '-wait').addClass('hidden');
                return (!auto_create_card_is_checked) || ((auto_create_card_is_checked) && (max_card_cnt_value > 0));
            case 5:
                var d1 = $('#issue_date_beg').data('kendoDatePicker').value();
                if (!d1)
                    $('#issue_date_beg').addClass('input-validation-error');
                else
                    $('#issue_date_beg').removeClass('input-validation-error');

                var d2 = $('#issue_date_end').data('kendoDatePicker').value();
                if ((!d2) || (d2 < d1))
                    $('#issue_date_end').addClass('input-validation-error');
                else
                    $('#issue_date_end').removeClass('input-validation-error');

                $('#issue_condition_type').val($('input.k-radio[name="rb-group1"]:checked').val());

                $('#step' + num + '-wait').addClass('hidden');
                return d1 && d2 && (d2 >= d1);
            case 6:
                $('#step' + num + '-wait').addClass('hidden');
                return true;
            default:
                $('#step' + num + '-wait').addClass('hidden');
                return true;
        }
    }
}

var campaignEdit = null;
var checkedIds = [];

$(function () {
    campaignEdit = new CampaignEdit();
    campaignEdit.init();
});
