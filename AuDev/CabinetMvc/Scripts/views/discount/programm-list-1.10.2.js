﻿function ProgrammList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#programmListGrid', 200);
        //
        $('#btnProgrammAdd').children('span').first().addClass('k-icon k-i-plus');
        //
        $('#btnProgrammShowAll').click(function (e) {
            $('#btnProgrammShowAll').addClass('btn-primary');
            $('#btnProgrammShowActive').removeClass('btn-primary');
            $('#btnProgrammShowNotActive').removeClass('btn-primary');

            var grid = $("#programmListGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            grid.dataSource.filter(_fltStatus);
        });
        //
        $('#btnProgrammShowActive').click(function (e) {
            $('#btnProgrammShowAll').removeClass('btn-primary');
            $('#btnProgrammShowActive').addClass('btn-primary');
            $('#btnProgrammShowNotActive').removeClass('btn-primary');

            var grid = $("#programmListGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            _fltStatus.filters.push({ field: "date_end", operator: "eq", value: "null" });            
            grid.dataSource.filter(_fltStatus);
        });
        //
        $('#btnProgrammShowNotActive').click(function (e) {
            $('#btnProgrammShowAll').removeClass('btn-primary');
            $('#btnProgrammShowActive').removeClass('btn-primary');
            $('#btnProgrammShowNotActive').addClass('btn-primary');

            var grid = $("#programmListGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            _fltStatus.filters.push({ field: "date_end", operator: "neq", value: "null" });
            //_fltStatus.filters.push({ field: "is_active", operator: "eq", value: false });
            grid.dataSource.filter(_fltStatus);
        });
        //
        $('#btnProgrammOrderShowAll').click(function (e) {
            $('#btnProgrammOrderShowAll').addClass('btn-primary');
            $('#btnProgrammOrderShowActive').removeClass('btn-primary');
            $('#btnProgrammOrderShowNotActive').removeClass('btn-primary');

            var grid = $("#programmOrderListGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            grid.dataSource.filter(_fltStatus);
        });
        //
        $('#btnProgrammOrderShowActive').click(function (e) {
            $('#btnProgrammOrderShowAll').removeClass('btn-primary');
            $('#btnProgrammOrderShowActive').addClass('btn-primary');
            $('#btnProgrammOrderShowNotActive').removeClass('btn-primary');

            var grid = $("#programmOrderListGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            _fltStatus.filters.push({ field: "done_date", operator: "eq", value: "null" });            
            grid.dataSource.filter(_fltStatus);
        });
        //
        $('#btnProgrammOrderShowNotActive').click(function (e) {
            $('#btnProgrammOrderShowAll').removeClass('btn-primary');
            $('#btnProgrammOrderShowActive').removeClass('btn-primary');
            $('#btnProgrammOrderShowNotActive').addClass('btn-primary');

            var grid = $("#programmOrderListGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            _fltStatus.filters.push({ field: "done_date", operator: "neq", value: "null" });            
            grid.dataSource.filter(_fltStatus);
        });
    };

    this.btnCopyProgrammClick = function(e) {
        e.preventDefault();

        var row = $(e.target).closest("tr");
        var grid = $("#programmListGrid").data("kendoGrid");
        var dataItem = grid.dataItem(row);

        var kendoWindow = $("#winCopyProgramm");

        kendoWindow.data("kendoWindow")
            .center().open();

        kendoWindow
            .find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        kendoWindow.data("kendoWindow").close();
                        var curr_business_id_str = null;
                        var ddl = $('#ddlBusiness').getKendoDropDownList();
                        if (ddl) {
                            curr_business_id_str = ddl.value();
                        };
                        var sendData = { programm_id: dataItem.programm_id_str, business_id_str: curr_business_id_str };
                        $.ajax({
                            url: 'Card/ProgrammCopy',
                            data: JSON.stringify(sendData),
                            type: "POST",
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                grid.dataSource.read();
                            },
                            error: function (response) {
                                alert('Ошибка при попытке скопировать алгоритм');
                            }
                        });
                    }
                    kendoWindow.data("kendoWindow").close();
                })
             .end()
    };

    this.onProgrammListGridDataBinding = function (e) {        
        if (programmListGridDataBound_first) {
            programmListGridDataBound_first = false;
            $('#btnProgrammShowActive').trigger('click');
            return;
        };
    };

    this.onProgrammListGridDataBound = function (e) {
        drawEmptyRow("#programmListGrid");
        //
        $('.btn-copy-programm').each(function (i) {
            $(this).children('span').first().addClass('k-icon k-i-restore');
        });
    };

    this.onProgrammOrderListGridDataBinding = function (e) {        
        if (programmOrderListGridDataBound_first) {
            programmOrderListGridDataBound_first = false;
            $('#btnProgrammOrderShowActive').trigger('click');
            return;
        };
    };

    this.onProgrammOrderListGridDataBound = function(e) {
        drawEmptyRow("#programmOrderListGrid");
    };

}

var programmList = null;
var programmListGridDataBound_first = true;
var programmOrderListGridDataBound_first = true;

$(function () {
    programmList = new ProgrammList();
    programmList.init();
});
