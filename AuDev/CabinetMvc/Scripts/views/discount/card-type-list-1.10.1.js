﻿function CardTypeList() {
    _this = this;

    this.init = function () {
        $('#btnCardTypeAdd').children('span').first().addClass('k-icon k-i-plus');
    };

}

var cardTypeList = null;

$(function () {
    cardTypeList = new CardTypeList();
    cardTypeList.init();
});
