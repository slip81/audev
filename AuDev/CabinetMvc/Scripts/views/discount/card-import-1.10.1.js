﻿function CardImport() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Начать импорт ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#cardTypeList").data("kendoDropDownList");
            if (combo1.dataItem()) {
                $("#card_type_id").val(combo1.dataItem()["card_type_id"]);
            };
            var combo2 = $("#cardStatusList").data("kendoDropDownList");
            if (combo2.dataItem()) {
                $("#card_status_id").val(combo2.dataItem()["card_status_id"]);
            };
            var mode1 = ($('#rbTransSum0_:checked').length > 0) ? 0 : (($('#rbTransSum1_:checked').length > 0) ? 1 : 2);
            var mode2 = ($('#rbDiscPerc0_:checked').length > 0) ? 0 : (($('#rbDiscPerc1_:checked').length > 0) ? 1 : 2);
            var mode3 = ($('#rbBonusSum0_:checked').length > 0) ? 0 : (($('#rbBonusSum1_:checked').length > 0) ? 1 : 2);
            $("#action_for_existing_summ").val(mode1);
            $("#action_for_existing_disc_percent").val(mode2);
            $("#action_for_existing_bonus").val(mode3);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".save-changes-confirm,.save-changes-cancel")
                        .click(function () {
                            if ($(this).hasClass("save-changes-confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('k-state-active');
                                $('#btnSubmit').attr('disabled', true);
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });
    };
}

var cardImport = null;

$(function () {
    cardImport = new CardImport();
    cardImport.init();
});
