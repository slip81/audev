﻿function CardBrief() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#programmGrid', 200);
        setGridMaxHeight('#businessOrgGrid', 200);
    };

    this.getGridData = function (e) {
        var curr_business_id_str = null;
        var curr_business_id_dataItem = $('#businessDdl').data('kendoDropDownList').dataItem();
        if ((curr_business_id_dataItem) && (curr_business_id_dataItem.business_id_str)) {
            curr_business_id_str = curr_business_id_dataItem.business_id_str;
        }
        return { business_id_str: curr_business_id_str };
    };

    this.onBusinessDdlChange = function (e) {
        var ind = e.sender.select();
        if (ind <= 0) {
            $("#programmGrid").data("kendoGrid").showColumn("business_name");            
        } else {
            $("#programmGrid").data("kendoGrid").hideColumn("business_name");            
        };                
        refreshGrid('#programmGrid');
        refreshGrid('#businessOrgGrid');
    };

    this.onProgrammGridDataBound = function (e) {
        drawEmptyRow('#programmGrid');
    };

    this.onBusinessOrgGridDataBound = function (e) {
        drawEmptyRow('#businessOrgGrid');
    };
}

var cardBrief = null;

$(function () {
    cardBrief = new CardBrief();
    cardBrief.init();
});
