﻿function Login() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();
 
        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var validator = $("form").getKendoValidator();
            if (validator.validate()) {
                $('#ReturnUrl').val(return_url);
                //
                $('#btnSubmit').removeClass('active');
                $("#btnSubmit").attr('disabled', true);
                $("#btnRegister").attr('disabled', true);
                $("form").submit();
                return true;
            } else {
                return false;
            };
        });

        $("#btnRegister").click(function (e) {
            $("#btnSubmit").attr('disabled', true);
            $("#btnRegister").attr('disabled', true);
            window.location = '/Register';
        });

        $('#btnForgot').click(function (e) {
            $('#email-div').removeClass('hidden');
            $('#email-sent-ok').addClass('hidden');
            $('#email-text').focus();
        });

        $('#btnSend').click(function (e) {
            $('#op-wait').removeClass('hidden');
            $('#btnSend').addClass('disabled');
            $('#email-sent-ok').text('');
            $('#email-sent-ok').addClass('hidden');
            $('#email-sent-err').text('');
            $('#email-sent-err').addClass('hidden');

            $.ajax({
                url: url_RestorePwd,
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ user_email: $('#email-text').val() }),
                success: function (response) {
                    $('#op-wait').addClass('hidden');
                    $('#btnSend').removeClass('disabled');
                    if ((response == null) || (response.err)) {
                        $('#email-sent-err').text(response.mess ? response.mess : 'Адрес не найден');
                        $('#email-sent-err').removeClass('hidden');
                    } else {
                        $('#email-div').addClass('hidden');
                        $('#email-sent-ok').text('Пароль выслан !');
                        $('#email-sent-ok').removeClass('hidden');
                    };
                },
                error: function (response) {
                    $('#email-sent-err').text('Ошибка связи');
                    $('#email-sent-err').removeClass('hidden');
                }
            });        

        });
    };
}

var login = null;

$(function () {
    login = new Login();
    login.init();
});
