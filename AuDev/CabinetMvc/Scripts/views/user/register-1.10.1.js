﻿function Register() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
    };
}

var register = null;

$(function () {
    register = new Register();
    register.init();
});
