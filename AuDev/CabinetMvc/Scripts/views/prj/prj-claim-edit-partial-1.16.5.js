﻿function PrjClaimEditPartial() {
    _this = this;

    this.init = function () {        
        //
        $(document).on('click', '#btnPrjClaimEditSubmit', function () {
            savePrjClaimEdit(true);
        });
        //
        $(document).on('click', '#btnPrjClaimEditSave', function () {
            savePrjClaimEdit(false);
        });
        //
        $(document).on('click', '#btnPrjClaimEditCancel', function () {
            showConfirmWindow('#confirm-templ', 'Выйти из задачи без сохранения ?', function () {
                closePrjClaimEdit();
            });
        });
        //
        $(document).on('click', '#btnPrjClaimEditReset', function () {
            showConfirmWindow('#confirm-templ', 'Отменить несохраненные изменения ?', function () {
                reloadClaim($('#claim_id').val());
            });
        });
        //
        $(document).on('click', '#btn-prj-claim-edit-back', function () {
            closePrjClaimEdit();
        });
        //
        $(document).on('click', '#btnPrjClaimEditTaskEditSubmit', function () {
            if (validateData_task()) {
                var grid = $("#prjClaimEditTaskGrid").data("kendoGrid");
                if (prj_claim_task_add_mode) {
                    var dataSource = grid.dataSource;                    
                    dataSource.add(curr_prj_claim_task);
                } else {
                    var select = grid.select();
                    var dataItem = grid.dataItem(select);
                    dataItem.task_text = curr_prj_claim_task.task_text;
                    dataItem.dirty = true;
                    kendoFastRedrawRow(grid, select);
                };
                //
                $('.prj-claim-edit-task-edit').hide();
                $('.prj-claim-edit-task-add').hide();                
                $('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-add", true);
                $('#prjClaimEditTaskGrid').show();
                $('#prjClaimEditWorkAddToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-work-add", true);
                $('#prjClaimEditWorkGrid').show();
                $('.prj-claim-edit-task-mess').show();
                showPrjClaimEditButtons(true);
            };
        });
        //
        $(document).on('click', '#btnPrjClaimEditTaskEditCancel', function () {
            $('.prj-claim-edit-task-edit').hide();
            $('.prj-claim-edit-task-add').hide();            
            $('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-add", true);
            $('#prjClaimEditTaskGrid').show();
            $('#prjClaimEditWorkAddToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-work-add", true);
            $('#prjClaimEditWorkGrid').show();
            $('.prj-claim-edit-task-mess').show();
            showPrjClaimEditButtons(true);
        });
        //
        $(document).on('dblclick', '#prjClaimEditTaskGrid tbody>tr', function () {
            var dataItem = $("#prjClaimEditTaskGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {
                curr_prj_claim_task = dataItem;
                openPrjClaimTaskEdit();
            };
        });
        //
        $(document).on('click', '#btnPrjClaimEditWorkAddSubmit', function () {
            if (validateData_work()) {
                var today = new Date();
                var grid = $("#prjClaimEditWorkGrid").data("kendoGrid");
                var task_grid = $('#prjClaimEditTaskGrid').data('kendoGrid');
                var curr_task = task_grid.dataItem(task_grid.select());
                if (prj_claim_work_add_mode) {
                    if (curr_prj_claim_work_new) {                        
                        grid.dataSource.add(curr_prj_claim_work_new);
                        //
                        if (curr_task) {
                            curr_task_user_state_list.push({                                
                                task_num: curr_task.task_num,
                                user_state_type_id: 1,
                                user_state_user_id: curr_prj_claim_work_new.exec_user_id,
                                user_state_crt_date: new Date()
                            });
                        }
                    };                    
                    switch (currTaskChildActionId) {
                        case 4:
                        case 5:
                            if (curr_prj_claim_work) {                                
                                curr_prj_claim_work.set('state_type_id', 2);
                                curr_prj_claim_work.set('state_type_name', 'Выполнено');
                                curr_prj_claim_work.set('state_type_templ', '<span class="text-primary">Выполнено</span>');
                                curr_prj_claim_work.set('is_plan', true);
                                curr_prj_claim_work.set('date_end', today);
                                curr_prj_claim_work.set('date_time_end', today);
                                curr_prj_claim_work.set('upd_date', today);
                                curr_prj_claim_work.set('upd_user', currUser_CabUserName);
                                if (!curr_prj_claim_work.is_accept)
                                    curr_prj_claim_work.set('is_accept', true);
                                if (!curr_prj_claim_work.date_beg) {
                                    curr_prj_claim_work.set('date_beg', today);
                                    curr_prj_claim_work.set('date_time_beg', today);
                                }
                                curr_prj_claim_work.dirty = true;
                                //
                                if (curr_task) {
                                    curr_task_user_state_list.push({
                                        task_num: curr_task.task_num,
                                        user_state_type_id: 3,
                                        user_state_user_id: curr_prj_claim_work.exec_user_id,
                                        user_state_crt_date: new Date()
                                    });
                                }
                            }
                            break;
                        default:
                            break;
                    }                    
                } else {
                    switch (currTaskChildActionId) {
                        case 6:
                        case 7:                            
                            $("#claim_work_type_id_single").data("kendoDropDownList").enable(true);
                            if (curr_prj_claim_work) {
                                curr_prj_claim_work.set('state_type_id', (currTaskChildActionId == 6 ? 2 : 3));
                                curr_prj_claim_work.set('state_type_name', (currTaskChildActionId == 6 ? 'Выполнено' : 'Отменено'));
                                curr_prj_claim_work.set('state_type_templ', (currTaskChildActionId == 6 ? '<span class="text-primary">Выполнено</span>' : '<span class="text-warning">Отменено</span>'));
                                curr_prj_claim_work.set('is_plan', true);
                                curr_prj_claim_work.set('date_end', today);
                                curr_prj_claim_work.set('date_time_end', today);
                                curr_prj_claim_work.set('upd_date', today);
                                curr_prj_claim_work.set('upd_user', currUser_CabUserName);
                                if (currTaskChildActionId == 6) {
                                    if (!curr_prj_claim_work.is_accept)
                                        curr_prj_claim_work.set('is_accept', true);
                                    if (!curr_prj_claim_work.date_beg) {
                                        curr_prj_claim_work.set('date_beg', today);
                                        curr_prj_claim_work.set('date_time_beg', today);
                                    }
                                }
                                curr_prj_claim_work.dirty = true;
                                //
                                if (curr_task) {
                                    curr_task_user_state_list.push({
                                        task_num: curr_task.task_num,
                                        user_state_type_id: currTaskChildActionId == 6 ? 3 : 4,
                                        user_state_user_id: curr_prj_claim_work.exec_user_id,
                                        user_state_crt_date: new Date()
                                    });
                                };
                            };
                            break;
                        default:
                            break;
                    }
                };
                //                
                $('#claim_work_mess').removeClass('input-validation-error');
                $('.prj-claim-edit-work-type-edit').hide();
                $('.prj-claim-edit-work-user-edit').hide();
                $('.prj-claim-edit-work-mess-edit').hide();
                $('.prj-claim-edit-work-add').hide();                
                $('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-add", true);
                $('#prjClaimEditTaskGrid').show();
                $('#prjClaimEditWorkAddToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-work-add", true);
                $('#prjClaimEditWorkGrid').show();
                $('.prj-claim-edit-task-mess').show();
                showPrjClaimEditButtons(true);
            };
        });
        //
        $(document).on('click', '#btnPrjClaimEditWorkEditCancel, #btnPrjClaimEditWorkAddCancel', function () {
            $('#claim_work_exec_user_id').siblings('span.k-dropdown-wrap').removeClass('input-validation-error');
            $('#claim_work_state_type_id').siblings('span.k-dropdown-wrap').removeClass('input-validation-error');
            $('#claim_work_mess').removeClass('input-validation-error');
            $('.prj-claim-edit-work-type-edit').hide();
            $('.prj-claim-edit-work-user-edit').hide();
            $('.prj-claim-edit-work-mess-edit').hide();
            $('.prj-claim-edit-work-add').hide();            
            $('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-add", true);
            $('#prjClaimEditTaskGrid').show();
            $('#prjClaimEditWorkAddToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-work-add", true);
            $('#prjClaimEditWorkGrid').show();
            $('.prj-claim-edit-task-mess').show();
            showPrjClaimEditButtons(true);
        });
        //
        $(document).on('click', '#btnPrjClaimEditMessEditSubmit', function () {
            if (validateData_mess()) {
                var grid = $("#prjClaimEditMessGrid").data("kendoGrid");
                var dataSource = grid.dataSource;
                dataSource.add(curr_prj_claim_mess);
                //
                $('.prj-claim-edit-mess').show();
                $('.prj-claim-edit-mess-add').hide();
                //
                showPrjClaimEditButtons(true);
            };
            //
        });
        //
        $(document).on('click', '#btnPrjClaimEditMessEditCancel', function () {
            $('.prj-claim-edit-mess').show();
            $('.prj-claim-edit-mess-add').hide();
            //            
            showPrjClaimEditButtons(true);
        });
        //
        $(document).on('click', '#btnPrjClaimEditTaskMessEditCancel', function () {
            $('.prj-claim-edit-task-mess').show();
            $('.prj-claim-edit-task-mess-add').hide();
            //
            showPrjClaimEditButtons(true);
        });
        //
        $(document).on('click', '#btnPrjClaimEditTaskMessEditSubmit', function () {
            if (validateData_task_mess()) {
                $("#prjClaimEditTaskMessGrid").data("kendoGrid").dataSource.add(curr_prj_claim_mess);
                $("#prjClaimEditMessGrid").data("kendoGrid").dataSource.add(curr_prj_claim_mess);
                //
                $('.prj-claim-edit-task-mess').show();
                $('.prj-claim-edit-task-mess-add').hide();
                //
                showPrjClaimEditButtons(true);
            };
        });
        //
        $(document).on('click', '#btnPrjClaimEditRestore', function () {
            showConfirmWindow('#confirm-templ', 'Восстановить задачу из корзины ?', function () {
                var curr_claim_id = $('#claim_id').val();
                loadingDialog('#prj-loading', true);
                $.ajax({
                    url: '/PrjClaimRestore',
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify({ claim_id: curr_claim_id }),
                    success: function (res) {
                        if ((!res) || (res.err)) {
                            loadingDialog('#prj-loading', false);
                            alert(res && res.mess ? res.mess : 'Ошибка при восстановлении задачи');
                        } else {
                            firstDataBound_ClaimEditTaskGrid = true;
                            firstDataBound_ClaimEditWorkGrid = true;
                            firstDataBound_ClaimEditTaskMessGrid = true;
                            curr_task_user_state_list = [];
                            setTimeout(function () {
                                rightPaneContentType = 2;
                                prjList.openPrjClaimEditPartial(curr_claim_id);
                            }, waitTimeout);
                        }
                    },
                    error: function (res) {
                        loadingDialog('#prj-loading', false);
                        alert('Ошибка восстановления задачи');
                    }
                });
            });
        });
        //
        $(document).on('input', '#claim_name, #claim_text, #claim_text_solution, #claim_text_reprod, #claim_text_client', function () {
            onAttrChanged();
        });
    };

    this.loadData = function () {        
        //
    };

    this.onPrjClaimEditTaskGridDataBound = function (e) {
        drawEmptyRow('#prjClaimEditTaskGrid');
        if (firstDataBound_ClaimEditTaskGrid) {
            firstDataBound_ClaimEditTaskGrid = false;
        };               
        var task_cnt = $('#prjClaimEditTaskGrid').data('kendoGrid').dataSource.view().length;
        if (task_cnt > 0) {
            $('#prj-claim-edit-task-cnt').text('(' + task_cnt + ')');
            $('#prj-claim-edit-task-cnt').show();
        } else {
            $('#prj-claim-edit-task-cnt').hide();
            $('#prj-claim-edit-task-cnt').text('');
        };     
    };

    this.onBtnPrjClaimEditTaskAddClick = function (e) {
        curr_prj_claim_task = null;
        //
        openPrjClaimTaskEdit();
    };

    this.onBtnPrjClaimEditWorkAddClick = function (e) {        
        var task_grid = $('#prjClaimEditTaskGrid').data('kendoGrid');
        var curr_task = task_grid.dataItem(task_grid.select());
        if (!curr_task) {
            alert('Не выбрано задание');
            return false;
        }
        openPrjClaimWorkAdd();
    };

    this.onPrjClaimEditWorkGridDataBound = function (e) {
        if (lockDataBound_ClaimEditWorkGrid)
            return;        
        if (firstDataBound_ClaimEditWorkGrid) {
            firstDataBound_ClaimEditWorkGrid = false;
        };
        drawEmptyRow('#prjClaimEditWorkGrid');                
        var work_cnt = $('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.view().length;
        if (work_cnt > 0) {
            $('#prj-claim-edit-work-cnt').text('(' + work_cnt + ')');
            $('#prj-claim-edit-work-cnt').show();
        } else {
            $('#prj-claim-edit-work-cnt').hide();
            $('#prj-claim-edit-work-cnt').text('');
        }; 
    };

    this.onPrjClaimEditTaskMessGridDataBound = function (e) {
        if (firstDataBound_ClaimEditTaskMessGrid) {
            firstDataBound_ClaimEditTaskMessGrid = false;
            //
            setTimeout(function () {                
                $("#prjClaimEditTaskGrid").data("kendoGrid").select($("#prjClaimEditTaskGrid").data("kendoGrid").tbody.find('tr:first'));                
                showPrjClaimEditButtons(false);
                loadingDialog('#prj-loading', false);
                var scroll_task_id = curr_prj_claim_task_id;
                if (scroll_task_id) {
                    var scroll_dataItem = $("#prjClaimEditTaskGrid").data("kendoGrid").dataSource.get(scroll_task_id);
                    if (scroll_dataItem) {
                        var scroll_row = $("#prjClaimEditTaskGrid").data("kendoGrid").tbody.find("tr[data-uid='" + scroll_dataItem.uid + "']");
                        if (scroll_row) {                            
                            document.getElementById("prjClaimEditTaskGrid").scrollIntoView();                            
                            $("#prjClaimEditTaskGrid").data("kendoGrid").select(scroll_row);
                            $("#prjClaimEditTaskGrid div.k-grid-content").animate({
                                scrollTop: $(scroll_row).offset().top
                            }, 400);
                        };
                    };
                };
            }, 500);           
        }
        drawEmptyRow('#prjClaimEditTaskMessGrid');
        //
        var mess_cnt = $('#prjClaimEditTaskMessGrid').data('kendoGrid').dataSource.view().length;
        if (mess_cnt > 0) {
            $('#prj-claim-edit-task-mess-cnt').text('(' + mess_cnt + ')');
            $('#prj-claim-edit-task-mess-cnt').show();
        } else {
            $('#prj-claim-edit-task-mess-cnt').hide();
            $('#prj-claim-edit-task-mess-cnt').text('');
        };
    };

    this.onPrjClaimEditTaskGridChange = function (e) {
        var curr_task_num = null;
        var parentGrid = $('#prjClaimEditTaskGrid').data('kendoGrid');
        var dataItem = parentGrid.dataItem(parentGrid.select());
        if (dataItem) {
            curr_task_num = dataItem.task_num;
            $('#btn-prj-claim-edit-task-out').show();
        } else {
            $('#btn-prj-claim-edit-task-out').hide();
        }
        //
        var grid1 = $('#prjClaimEditWorkGrid').data('kendoGrid');
        var ds1 = grid1.dataSource;
        if (ds1.data().length > 0) {
            ds1.filter({ field: "task_num", operator: "eq", value: curr_task_num });
        }
        //
        var grid2 = $('#prjClaimEditTaskMessGrid').data('kendoGrid');
        var ds2 = grid2.dataSource;
        if (ds2.data().length > 0) {
            ds2.filter({ field: "task_num", operator: "eq", value: curr_task_num });
        }
    };

    this.onPrjClaimEditWorkListForAddGridDataBound = function (e) {        
        $("#prjClaimEditWorkListForAddGrid .templateCell").each(function () {
            eval($(this).children("script").last().html());
        });
    };

    this.onPrjClaimEditMessGridDataBound = function (e) {
        drawEmptyRow('#prjClaimEditMessGrid');
        //
        var mess_cnt = $('#prjClaimEditMessGrid').data('kendoGrid').dataSource.view().length;
        if (mess_cnt > 0) {
            $('#prj-claim-edit-mess-cnt').text('(' + mess_cnt + ')');
            $('#prj-claim-edit-mess-cnt').show();
        } else {
            $('#prj-claim-edit-mess-cnt').hide();
            $('#prj-claim-edit-mess-cnt').text('');
        };
    };

    this.onBtnPrjClaimEditMessAddClick = function (e) {
        curr_prj_claim_mess = null;
        openPrjClaimMessEdit();
    };

    this.onBtnPrjClaimEditTaskMessAddClick = function (e) {
        var task_grid = $('#prjClaimEditTaskGrid').data('kendoGrid');
        var curr_task = task_grid.dataItem(task_grid.select());
        if (!curr_task) {
            alert('Не выбрано задание');
            return false;
        };
        curr_prj_claim_mess = null;
        openPrjClaimTaskMessEdit();
    };

    this.onUplPrjFileUpload = function (e) {
        var ok = true;        
        var files = e.files;
        $.each(files, function () {
            if (this.size > maxFileSize) {
                alert("Файл превышает допустимый размер (" + maxFileSize + ")");                
                e.preventDefault();
                ok = false;
                return;
            };
            var ind = allowedExtentions.indexOf(this.extension.toLowerCase());
            if (ind < 0) {
                alert("Тип файла не поддерживается (" + this.extension + ")");                
                e.preventDefault();
                ok = false;
                return;
            };            
        });

        if (!ok) return;
        e.data = { claim_id: $('#claim_id').val() };
    };

    this.onUplPrjFileError = function (e) {
        var err = e.XMLHttpRequest.responseText;
        alert(err);
    };

    this.onUplPrjFileRemove = function (e) {
        e.data = { claim_id: $('#claim_id').val() };
    };

    this.onUplPrjFileSuccess = function (e) {
        onAttrChanged();
    };

    this.onBtnPrjClaimEditTaskListClick = function (e) {
        if ($('.prj-claim-edit-work-list').hasClass('hidden')) {
            $('#btn-prj-claim-edit-task-list_overflow>a').attr('title', 'Расширить список');
            $('#btn-prj-claim-edit-task-list_overflow>a>span').addClass('k-i-arrow-e');
            $('#btn-prj-claim-edit-task-list_overflow>a>span').removeClass('k-i-arrow-w');
            $('.prj-claim-edit-task-list').addClass('col-lg-5 col-md-5 col-sm-5');
            $('.prj-claim-edit-task-list').removeClass('col-lg-10 col-md-10 col-sm-10');
            $('.prj-claim-edit-work-list').removeClass('hidden');
        } else {
            $('#btn-prj-claim-edit-task-list_overflow>a').attr('title', 'Сузить список');
            $('#btn-prj-claim-edit-task-list_overflow>a>span').addClass('k-i-arrow-w');
            $('#btn-prj-claim-edit-task-list_overflow>a>span').removeClass('k-i-arrow-e');
            $('.prj-claim-edit-task-list').addClass('col-lg-10 col-md-10 col-sm-10');
            $('.prj-claim-edit-task-list').removeClass('col-lg-5 col-md-5 col-sm-5');
            $('.prj-claim-edit-work-list').addClass('hidden');
        }
    };

    this.onBtnPrjClaimEditTaskOutClick = function (e) {        
        var curr_task = $('#prjClaimEditTaskGrid').data('kendoGrid').dataItem($('#prjClaimEditTaskGrid').data('kendoGrid').select());
        if (!curr_task) {
            alert('Не выбрано задание');
            return false;
        }
        showEditWindow("#edit2-templ", "Новая задача", "Название", $('#claim_name').val(), "Формулировка", curr_task.task_text, function () {            
            create_claim_from_task = curr_task.task_num;
            create_claim_name = $('#win-edit-value').val();
            create_claim_text = $('#win-edit2-value').val();
            savePrjClaimEdit(false)            
        });        
    };

    this.onBtnPrjClaimEditWorkDelClick = function (e) {
        var curr_work = $('#prjClaimEditWorkGrid').data('kendoGrid').dataItem($('#prjClaimEditWorkGrid').data('kendoGrid').select());
        if (!curr_work) {
            alert('Не выбрана работа');
            return false;
        }
        showConfirmWindow('#confirm-templ', 'Удалить работу ' + curr_work.work_type_name + ' ?', function () {
            if (curr_work.work_id > 0) {
                curr_prj_claim_work_del_list.push(curr_work.work_id);
            }
            $('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.remove(curr_work);
        });
    };

    this.sortableNoHint = function () {
        //
    };

    this.sortablePlaceholder = function (el) {
        return el.clone().addClass("k-state-hover").css("opacity", 0.65);
    };

    this.onSortableChange = function (e) {
        var grid = $("#prjClaimEditWorkGrid").data("kendoGrid"),
            skip = grid.dataSource.skip(),
            oldIndex = e.oldIndex + skip,
            newIndex = e.newIndex + skip,
            dataItem = grid.dataSource.getByUid(e.item.data("uid"));

        lockDataBound_ClaimEditWorkGrid = true;

        grid.dataSource.remove(dataItem);        
        grid.dataSource.insert(newIndex, dataItem);

        var dataItems_data = grid.dataSource.data();
        var dataItems_view = grid.dataSource.view();
        var i = 1;
        //
        $.each(dataItems_data, function (index, value) {
            if ($.inArray(value, dataItems_view) !== -1) {
                value.set('work_num', i);
                value.set('is_new', true);
                i = i + 1;
            };
        });
        //
        lockDataBound_ClaimEditWorkGrid = false;
    };

    this.onBtnPrjClaimEditLogClick = function (e) {
        closePrjClaimEdit();
        $('#tsPrj').data('kendoTabStrip').select(6);
    };

    this.onBtnPrjClaimEditTaskActionsClick = function (e) {
        currTaskChildActionId = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (currTaskChildActionId) {
            case 1:
                curr_prj_claim_task = null;                
                openPrjClaimTaskEdit();
                break;
            case 2:
                openPrjClaimWorkAdd();
                break;
            case 3:
            case 4:
            case 5:                
            case 6:
            case 7:
                proceedTaskAction();
                break;
            default:
                break;
        };
    };

    this.onBtnPrjClaimEditWorkActionsClick = function (e) {
        curr_prj_claim_work = null;
        currWorkChildActionId = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (currWorkChildActionId) {
            case 1:
                var task_grid = $('#prjClaimEditTaskGrid').data('kendoGrid');
                var curr_task = task_grid.dataItem(task_grid.select());
                if (!curr_task) {
                    alert('Не выбрано задание');
                    return false;
                }
                //
                currTaskChildActionId = 2;
                openPrjClaimWorkAdd();
                break;
            case 2:
                var work_grid = $('#prjClaimEditWorkGrid').data('kendoGrid');
                curr_prj_claim_work = work_grid.dataItem(work_grid.select());
                if (!curr_prj_claim_work) {
                    alert('Не выбрана работа');
                    return false;
                }
                //                
                openPrjClaimWorkEdit();
                break;
            case 3:
                // !!!
                var work_grid = $('#prjClaimEditWorkGrid').data('kendoGrid');
                curr_prj_claim_work = work_grid.dataItem(work_grid.select());
                if (!curr_prj_claim_work) {
                    alert('Не выбрана работа');
                    return false;
                }
                //                
                openPrjClaimWorkUserEdit();
                break;
            default:
                break;
        };
    };

    this.onDdlClaimWorkExecUserChange = function(e) {
        var dataItem = e.sender.dataItem();
        var curr_role_id = dataItem ? dataItem.crm_user_role_id : 0;
        if (curr_role_id > 0) {
            switch (curr_role_id) {
                case 1:
                    $('#claim_work_type_id_single').data('kendoDropDownList').value(3);
                    break;
                case 2:
                    $('#claim_work_type_id_single').data('kendoDropDownList').value(4);
                    break;
                case 3:
                case 4:
                    $('#claim_work_type_id_single').data('kendoDropDownList').value(1);
                    break;
                default:
                    break;
            }        
        };
    };

    this.onAttrChanged = function(e) {        
        showPrjClaimEditButtons(true);
    };

    this.onBtnPrjClaimEditToBufferClick = function(e) {
                var text1 = $('#claim_num').val();
                var text2 = $('#claim_name').val();
                var text3 = $('#claim_text').val();

                showClaimToBufferDialog($('#claim_num').val() + ' ' + $('#claim_name').val() + '\n' + $('#claim_text').val());
    };

    this.onBtnPrjClaimEditTaskToBufferClick = function(e) {
                var curr_task = $('#prjClaimEditTaskGrid').data('kendoGrid').dataItem($('#prjClaimEditTaskGrid').data('kendoGrid').select());
                if (!curr_task) {
                    alert('Не выбрано задание');
                    return false;
                };

                showClaimToBufferDialog($('#claim_num').val() + '/' + curr_task.task_num + ' ' + $('#claim_name').val() + '\n' + curr_task.task_text);
    };

    // private

    function savePrjClaimEdit(close) {
        if (validateData()) {
            loadingDialog('#prj-loading', true);
            //
            $.ajax({
                url: '/PrjClaimEditPartial',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ claim: curr_prj_claim, mess_list: curr_prj_claim_mess_list, task_list: curr_prj_claim_task_list, work_list: curr_prj_claim_work_list, work_del_list: curr_prj_claim_work_del_list, task_user_state_list: curr_task_user_state_list }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response == null ? 'Ошибка при попытке сохранения' : response.mess);
                    } else {
                        if (close) {
                            closePrjClaimEdit();
                            if (prj_claim_add_mode) {
                                switch (leftPaneContentType) {
                                    case 1:
                                        refreshGrid('#prjClaimChildGrid');
                                        break;
                                    case 2:
                                        refreshGrid('#prjClaimGrid');
                                        break;
                                    case 3:
                                        refreshGrid('#prjTaskGrid');
                                        break;
                                    case 4:
                                        refreshGrid('#prjWorkGrid');
                                        break;
                                    case 5:
                                        refreshScheduler('#prjPlanScheduler');
                                        refreshGrid('#prjPlanWorkGrid');
                                        break;
                                    case 6:
                                        refreshGrid('#prjNewsClaimGrid');
                                        refreshGrid('#prjNewsTaskGrid');
                                        refreshGrid('#prjNewsMessGrid');
                                        refreshGrid('#prjNewsLogGrid');
                                        break;
                                    default:
                                        break;
                                }
                            } else {
                                var grid = null;
                                var gridsToRefresh = [];
                                switch (leftPaneContentType) {
                                    case 1:
                                        gridsToRefresh.push('#prjClaimChildGrid');
                                        gridsToRefresh.push('#prjClaimChildTaskChildGrid');
                                        break;
                                    case 2:
                                        gridsToRefresh.push('#prjClaimGrid');
                                        gridsToRefresh.push('#prjTaskChildGrid');
                                        break;
                                    case 3:
                                        gridsToRefresh.push('#prjTaskGrid');
                                        break;
                                    case 4:
                                        gridsToRefresh.push('#prjWorkGrid');
                                        break;
                                    case 5:
                                        gridsToRefresh.push('#prjPlanWorkGrid');
                                        refreshScheduler('#prjPlanScheduler');
                                        break;
                                    case 6:
                                        gridsToRefresh.push('#prjNewsClaimGrid');
                                        gridsToRefresh.push('#prjNewsTaskGrid');
                                        gridsToRefresh.push('#prjNewsMessGrid');
                                        gridsToRefresh.push('#prjNewsLogGrid');
                                        break;
                                    case 8:
                                        gridsToRefresh.push('#prjBriefGrid');
                                        gridsToRefresh.push('#prjBriefWorkGrid');
                                        break;
                                    default:
                                        break;
                                }
                                if (grid) {
                                    var select = grid.select();
                                    if (select) {
                                        var data = grid.dataItem(select);
                                        data.claim_name = response.claim.claim_name;
                                        data.claim_state_type_id = response.claim.claim_state_type_id;
                                        data.claim_state_type_name = response.claim.claim_state_type_name;
                                        data.task_cnt = response.claim.task_cnt;
                                        data.active_task_cnt = response.claim.active_task_cnt;
                                        data.done_task_cnt = response.claim.done_task_cnt;
                                        data.canceled_task_cnt = response.claim.canceled_task_cnt;
                                        data.resp_user_name = response.claim.resp_user_name;
                                        data.date_plan = kendo.toString(kendo.parseDate(response.claim.date_plan), 'dd.MM.yyyy');                                        
                                        data.date_fact = response.claim.date_fact;
                                        data.project_name = response.claim.project_name;
                                        data.module_name = response.claim.module_name;
                                        data.module_part_name = response.claim.module_part_name;
                                        data.module_version_name = response.claim.module_version_name;
                                        //data.repair_version_name = response.claim.repair_version_name;
                                        data.version_id = response.claim.version_id;
                                        data.version_name = response.claim.version_name;
                                        data.client_name = response.claim.client_name;
                                        data.priority_name = response.claim.priority_name;
                                        data.prj_name = response.claim.prj_name;                                                                                
                                        data.active_work_type_id = response.claim.active_work_type_id;
                                        data.active_work_type_name = response.claim.active_work_type_name;
                                        data.claim_type_name = response.claim.claim_type_name;
                                        data.claim_stage_name = response.claim.claim_stage_name;
                                        kendoFastRedrawRow(grid, select);
                                    }
                                }
                                //
                                if (gridsToRefresh.length > 0) {
                                    for (var i = 0; i < gridsToRefresh.length; i++) {
                                        refreshGrid(gridsToRefresh[i]);
                                    };
                                };
                            }
                        } else {                            
                            reloadClaim(response.claim.claim_id);
                        };
                    };
                },
                error: function (response) {
                    alert('Ошибка при сохранении');
                },
                complete: function (response) {
                    if (close) {
                        loadingDialog('#prj-loading', false);
                    };
                }
            });
        }
    };

    function validateData() {
        curr_prj_claim = null;
        curr_prj_claim_mess_list = [];
        curr_prj_claim_task_list = [];
        curr_prj_claim_work_list = [];        
        var ok = true;

        var curr_claim_id = $('#claim_id').val() ? $('#claim_id').val() : 0;
        var curr_claim_num = $('#claim_num').val();
        var curr_claim_name = $('#claim_name').val();
        var curr_claim_text = $('#claim_text').val();

        var curr_claim_text_reprod = $('#claim_text_reprod').val();
        var curr_claim_text_solution = $('#claim_text_solution').val();
        var curr_claim_text_client = $('#claim_text_client').val();
        
        var curr_prj_id_list_dataItems = $('#prjMultiselect').data('kendoMultiSelect').dataItems();
        var curr_prj_id_list = curr_prj_id_list_dataItems ? curr_prj_id_list_dataItems.map(function(el){ return el.prj_id; }).join(','): null;

        var curr_project_dataItem = $('#projectDdl').data('kendoDropDownList').dataItem();
        var curr_project_id = curr_project_dataItem ? curr_project_dataItem["project_id"] : null;

        var curr_module_dataItem = $('#moduleDdl').data('kendoDropDownList').dataItem();
        var curr_module_id = curr_module_dataItem ? curr_module_dataItem["module_id"] : null;

        var curr_module_part_dataItem = $('#modulePartDdl').data('kendoDropDownList').dataItem();
        var curr_module_part_id = curr_module_part_dataItem ? curr_module_part_dataItem["module_part_id"] : null;

        var curr_project_version_dataItem = $('#projectVersionDdl').data('kendoDropDownList').dataItem();
        var curr_project_version_id = curr_project_version_dataItem ? curr_project_version_dataItem["project_version_id"] : null;

        var curr_client_dataItem = $('#clientDdl').data('kendoDropDownList').dataItem();
        var curr_client_id = curr_client_dataItem ? curr_client_dataItem["client_id"] : null;

        var curr_priority_dataItem = $('#priorityDdl').data('kendoDropDownList').dataItem();
        var curr_priority_id = curr_priority_dataItem ? curr_priority_dataItem["priority_id"] : null;

        var curr_resp_user_dataItem = $('#respUserDdl').data('kendoDropDownList').dataItem();
        var curr_resp_user_id = curr_resp_user_dataItem ? curr_resp_user_dataItem["user_id"] : null;

        var curr_date_plan = $('#date_plan').val();
        var curr_date_fact = $('#date_fact').val();

        var curr_version_id = 0;
        var curr_version_ddl = $('#versionDdl').data('kendoDropDownList');
        if (curr_version_ddl) {
                var curr_version_dataItem = $('#versionDdl').data('kendoDropDownList').dataItem();
                curr_version_id = curr_version_dataItem ? curr_version_dataItem["version_id"] : 0;
        };

        var curr_claim_type_dataItem = $('#claimTypeDdl').data('kendoDropDownList').dataItem();
        var curr_claim_type_id = curr_claim_type_dataItem ? curr_claim_type_dataItem["claim_type_id"] : null;

        var curr_claim_stage_dataItem = $('#claimStageDdl').data('kendoDropDownList').dataItem();
        var curr_claim_stage_id = curr_claim_stage_dataItem ? curr_claim_stage_dataItem["claim_stage_id"] : null;
        
        prj_claim_add_mode = !(curr_claim_id > 0);

        $("#prj-main-pane-right .input-validation-error").removeClass("input-validation-error");

        if (!curr_project_dataItem) {
            $('#projectDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };
        if ((!curr_module_dataItem) || (curr_module_dataItem.module_id <= 0)) {
            $('#moduleDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };
        if ((!curr_module_part_dataItem) || (curr_module_part_dataItem.module_part_id <= 0)) {
            $('#modulePartDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };
        if (!curr_project_version_dataItem) {
            $('#projectVersionDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };
        if ((!curr_client_dataItem) || (curr_client_dataItem.client_id <= 0)) {
            $('#clientDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };
        if ((!curr_priority_dataItem) || (curr_priority_dataItem.priority_id <= 0)) {
            $('#priorityDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };
        if (!curr_resp_user_dataItem) {
            $('#respUserDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };        if ((!curr_claim_type_dataItem) || (curr_claim_type_dataItem.claim_type_id <= 0)) {
            $('#claimTypeDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };
        if (!curr_claim_stage_dataItem) {
            $('#claimStageDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };
        if (
            (curr_client_dataItem) && (curr_client_dataItem.client_name == 'Клиент') 
            && ((!curr_project_version_dataItem) || (curr_project_version_dataItem.project_version_id <= 0))
           ) {
            $('#projectVersionDdl').siblings('span.k-dropdown-wrap').addClass('input-validation-error');
            ok = false;
        };

        if (!curr_claim_name) {
            $('#claim_name').addClass('input-validation-error');
            ok = false;
        }
        if (!curr_claim_text) {
            $('#claim_text').addClass('input-validation-error');
            ok = false;
        }

        if (ok) {            
            curr_prj_claim = {
                claim_id: curr_claim_id,
                claim_num: curr_claim_num,
                claim_name: curr_claim_name,
                claim_text: curr_claim_text,
                claim_text_reprod: curr_claim_text_reprod,
                claim_text_solution: curr_claim_text_solution,
                claim_text_client: curr_claim_text_client,
                prj_id_list: curr_prj_id_list,
                project_id: curr_project_id,
                module_id: curr_module_id,
                module_part_id: curr_module_part_id,
                project_version_id: curr_project_version_id,
                client_id: curr_client_id,
                priority_id: curr_priority_id,
                resp_user_id: curr_resp_user_id,
                date_plan: curr_date_plan,
                date_fact: curr_date_fact,
                version_id: curr_version_id,
                claim_type_id: curr_claim_type_id,
                claim_stage_id: curr_claim_stage_id,
                create_claim_from_task: create_claim_from_task,
                create_claim_name: create_claim_name,
                create_claim_text: create_claim_text
            };
            //
            curr_prj_claim_mess_list = $.grep($('#prjClaimEditMessGrid').data('kendoGrid').dataSource.data(), function (item) {
                return ((item.dirty) || (item.is_new));
            });
            //
            curr_prj_claim_task_list = $.grep($('#prjClaimEditTaskGrid').data('kendoGrid').dataSource.data(), function (item) {
                return ((item.dirty) || (item.is_new));
            });
            //
            curr_prj_claim_work_list = $.grep($('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.data(), function (item) {
                return ((item.dirty) || (item.is_new));
            });
        };

        return ok;
    };

    function validateData_mess() {
        var ok = true;

        var curr_claim_id = $('#claim_id').val() ? $('#claim_id').val() : 0;
        var curr_claim_mess_id = 0;
        var curr_claim_mess = $('#claim_mess').val();

        $("#prj-main-pane-right .input-validation-error").removeClass("input-validation-error");
        if (!curr_claim_mess) {
            $('#claim_mess').addClass('input-validation-error');
            ok = false;
        };

        if (ok) {
            curr_prj_claim_mess = {
                mess_id: curr_claim_mess_id,
                claim_id: curr_claim_id,
                task_id: null,
                work_id: null,
                prj_id: null,
                mess_num: null,
                mess: curr_claim_mess,
                user_name: currUser_CabUserName,
                user_id: currUser_CabUserId,
                crt_date: new Date(),
                is_new: true
            };
            $('#claim_mess').val(null);
        };

        return ok;
    }

    function validateData_task() {        
        var ok = true;

        var curr_claim_id = $('#claim_id').val() ? $('#claim_id').val() : 0;
        var curr_claim_task_id = curr_prj_claim_task ? curr_prj_claim_task.task_id : 0;
        var curr_claim_task_text = $('#claim_task_text').val();
        var curr_claim_task_date_plan = $('#claim_task_date_plan').data('kendoDatePicker').value();        

        prj_claim_task_add_mode = !(curr_prj_claim_task);

        $("#prj-main-pane-right .input-validation-error").removeClass("input-validation-error");

        if (!curr_claim_task_text) {
            $('#claim_task_text').addClass('input-validation-error');
            ok = false;
        };

        var task_num_new = 1;
        var task_list = $('#prjClaimEditTaskGrid').data('kendoGrid').dataSource.data();
        if ((task_list) && (task_list.length > 0)) {
            var task_array = $(task_list).toArray();
            task_num_new = Math.max.apply(Math, task_array.map(function (o) { return o.task_num; }))
            task_num_new = task_num_new + 1;
        };        

        if (ok) {
            if (prj_claim_task_add_mode) {
                curr_prj_claim_task = {
                    task_id: curr_claim_task_id,
                    claim_id: curr_claim_id,
                    task_num: task_num_new,
                    task_text: curr_claim_task_text,                    
                    task_state_type_name: 'Неразобрано',
                    task_state_type_templ: '<span class="text-danger">Неразобрано</span>',
                    date_plan: curr_claim_task_date_plan,
                    upd_date: new Date(),
                    upd_user: currUser_CabUserName,
                    is_new: true
                };
            } else {
                curr_prj_claim_task.task_text = curr_claim_task_text;                
                curr_prj_claim_task.date_plan = curr_claim_task_date_plan;
                curr_prj_claim_task.upd_date = new Date();
                curr_prj_claim_task.upd_user = currUser_CabUserName;
                curr_prj_claim_task.dirty = true;
            };
        };

        return ok;
    };

    function validateData_work() {
        return prj_claim_work_add_mode ? validateData_work_add() : validateData_work_edit();
    };

    function validateData_work_add() {
        //
        var selected_task = $('#prjClaimEditTaskGrid').data('kendoGrid').dataItem($('#prjClaimEditTaskGrid').data('kendoGrid').select());
        if (selected_task == null) {
            alert('Не выбрано задание');
            return false;
        }
        var curr_exec_user_dataItem = $('#claim_work_exec_user_id_single').data('kendoDropDownList').dataItem();
        var curr_work_type_dataItem = $('#claim_work_type_id_single').data('kendoDropDownList').dataItem();        
        //
        var curr_works = $.grep($('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.view(), function (item) {
            return ((item.exec_user_id == curr_work_type_dataItem.user_id) && (item.work_type_id == curr_work_type_dataItem.work_type_id) && (item.state_type_id == 1));
        });
        if ((curr_works) && (curr_works.length > 0)) {
            alert('Уже есть активная работа для ' + curr_exec_user_dataItem.user_name + ' с типом ' + curr_work_type_dataItem.work_type_name);
            return false;
        }
        //
        var work_num_new = 1;
        var work_list_for_task = $('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.view();
        if ((work_list_for_task) && (work_list_for_task.length > 0)) {
            var work_array = $(work_list_for_task).toArray();
            work_num_new = Math.max.apply(Math, work_array.map(function (o) { return o.work_num; }))
            work_num_new = work_num_new + 1;
        };        
        selected_task.dirty = true;
        var today = new Date();
        curr_prj_claim_work_new = {
            work_id: 0,
            task_id: selected_task.task_id,
            claim_id: selected_task.claim_id,
            work_num: work_num_new,
            work_type_id: curr_work_type_dataItem.work_type_id,
            exec_user_id: curr_exec_user_dataItem.user_id,
            is_all_day: true,
            is_accept: false,
            task_num: selected_task.task_num,
            work_type_name: curr_work_type_dataItem.work_type_name,
            exec_user_name: curr_exec_user_dataItem.user_name,
            state_type_id: currTaskChildActionId == 5 ? 4 : 1,
            state_type_name: currTaskChildActionId == 5 ? 'Возвращено' : 'Активно',
            state_type_templ: currTaskChildActionId == 5 ? '<span class="text-danger">Возвращено</span>' : '<span class="text-success">Активно</span>',
            mess: null,
            date_send: today,
            upd_date: today,
            upd_user: currUser_CabUserName,
            is_new: true
        };
        //
        var curr_mess = $('#claim_task_mess_single').val();
        if (curr_mess) {
            curr_prj_claim_mess = {
                mess_id: 0,
                claim_id: selected_task.claim_id,
                task_id: selected_task.task_id,
                work_id: null,
                prj_id: null,
                task_num: selected_task.task_num,
                work_num: null, 
                mess_num: null,
                mess: curr_mess,
                user_name: currUser_CabUserName,
                user_id: currUser_CabUserId,
                crt_date: new Date(),
                is_new: true
            };
            $('#claim_task_mess_single').val(null);
            //            
            $("#prjClaimEditTaskMessGrid").data("kendoGrid").dataSource.add(curr_prj_claim_mess);
            $("#prjClaimEditMessGrid").data("kendoGrid").dataSource.add(curr_prj_claim_mess);
        }
        //
        return true;
    };

    function validateData_work_edit() {
        var ok = true;        

        var selected_task = $('#prjClaimEditTaskGrid').data('kendoGrid').dataItem($('#prjClaimEditTaskGrid').data('kendoGrid').select());
        if (selected_task == null) {
            alert('Не выбрано задание');
            return false;
        };
        
        if (curr_prj_claim_work) {
            var curr_work_type_dataItem = $('#claim_work_type_id_single').data('kendoDropDownList').dataItem();
            if ($('#claim_work_type_id_single').closest('.k-dropdown').is(':visible') && (curr_work_type_dataItem) && (curr_prj_claim_work.work_type_id != curr_work_type_dataItem.work_type_id)) {
                curr_prj_claim_work.set('work_type_id', curr_work_type_dataItem.work_type_id);
                curr_prj_claim_work.set('work_type_name', curr_work_type_dataItem.work_type_name);
                curr_prj_claim_work.dirty = true;
            };
            var curr_exec_user_dataItem = $('#claim_work_exec_user_id_single').data('kendoDropDownList').dataItem();
            if ($('#claim_work_exec_user_id_single').closest('.k-dropdown').is(':visible') && (curr_exec_user_dataItem) && (curr_prj_claim_work.exec_user_id != curr_exec_user_dataItem.user_id)) {
                curr_prj_claim_work.set('exec_user_id', curr_exec_user_dataItem.user_id);
                curr_prj_claim_work.set('exec_user_name', curr_exec_user_dataItem.user_name);
                curr_prj_claim_work.dirty = true;
            };
        };

        //
        selected_task.dirty = true;
        var curr_mess = $('#claim_task_mess_single').val();
        if (curr_mess) {
            curr_prj_claim_mess = {
                mess_id: 0,
                claim_id: selected_task.claim_id,
                task_id: selected_task.task_id,
                work_id: null,
                prj_id: null,
                task_num: selected_task.task_num,
                work_num: null, 
                mess_num: null,
                mess: curr_mess,
                user_name: currUser_CabUserName,
                user_id: currUser_CabUserId,
                crt_date: new Date(),
                is_new: true
            };
            $('#claim_task_mess_single').val(null);
            //            
            $("#prjClaimEditTaskMessGrid").data("kendoGrid").dataSource.add(curr_prj_claim_mess);
            $("#prjClaimEditMessGrid").data("kendoGrid").dataSource.add(curr_prj_claim_mess);
        }
                
        // todo ?

        return ok;
    };

    function validateData_task_mess() {
        var ok = true;
        
        var selected_task = $('#prjClaimEditTaskGrid').data('kendoGrid').dataItem($('#prjClaimEditTaskGrid').data('kendoGrid').select());
        if (selected_task == null) {
            alert('Не выбрано задание');
            return false;
        }

        var curr_claim_id = $('#claim_id').val() ? $('#claim_id').val() : 0;
        var curr_claim_task_id = selected_task.task_id;
        var curr_claim_task_num = selected_task.task_num;
        var curr_claim_mess_id = 0;
        var curr_claim_mess = $('#claim_task_mess').val();

        $("#prj-main-pane-right .input-validation-error").removeClass("input-validation-error");
        if (!curr_claim_mess) {
            $('#claim_task_mess').addClass('input-validation-error');
            ok = false;
        };

        if (ok) {
            selected_task.dirty = true;
            curr_prj_claim_mess = {
                mess_id: curr_claim_mess_id,
                claim_id: curr_claim_id,
                task_id: curr_claim_task_id,
                work_id: null,
                prj_id: null,
                task_num: curr_claim_task_num,
                work_num: null, 
                mess_num: null,
                mess: curr_claim_mess,
                user_name: currUser_CabUserName,
                user_id: currUser_CabUserId,
                crt_date: new Date(),
                is_new: true
            };
            $('#claim_task_mess').val(null);
        };

        return ok;
    };

    function openPrjClaimTaskEdit() {
        $('#claim_task_text').val('');        
        $("#claim_task_date_plan").data("kendoDatePicker").value(null);
        $("#claim_task_add_work").prop('checked', true);                
        if (curr_prj_claim_task) {
            $('#claim_task_text').val(curr_prj_claim_task.task_text);
            $("#claim_task_date_plan").data("kendoDatePicker").value(curr_prj_claim_task.date_plan);
            $("#claim_task_add_work").prop('checked', false);            
            $('.prj-claim-edit-task-edit').show();
        } else {            
            $('.prj-claim-edit-task-add').show();            
        };        
        //$('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-list-templ", false);
        $('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-add", false);
        $('#prjClaimEditTaskGrid').hide();
        $('#prjClaimEditWorkAddToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-work-add", false);
        $('#prjClaimEditWorkGrid').hide();
        $('.prj-claim-edit-task-mess').hide();
        $('#claim_task_text').focus();
        showPrjClaimEditButtons(false);
    };

    function openPrjClaimWorkAdd() {
        prj_claim_work_add_mode = true;        
        $('.prj-claim-edit-work-user-edit').hide();
        $('.prj-claim-edit-work-state-edit').hide();
        $('.prj-claim-edit-work-mess-edit').hide();        
        $('.prj-claim-edit-work-add').show();
        $('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-add", false);
        $('#prjClaimEditTaskGrid').hide();
        $('#prjClaimEditWorkAddToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-work-add", false);
        $('#prjClaimEditWorkGrid').hide();
        $('.prj-claim-edit-task-mess').hide();
        $('#claim_task_mess_single').focus();
        showPrjClaimEditButtons(false);
    };

    function openPrjClaimWorkEdit() {
        prj_claim_work_add_mode = false;                        
        $('.prj-claim-edit-work-add').hide();          
        $('.prj-claim-edit-work-user-edit').hide();
        $('.prj-claim-edit-work-type-edit').show();        
        $('.prj-claim-edit-work-mess-edit').show();
        $('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-add", false);
        $('#prjClaimEditTaskGrid').hide();
        $('#prjClaimEditWorkAddToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-work-add", false);
        $('#prjClaimEditWorkGrid').hide();
        $('.prj-claim-edit-task-mess').hide();
        $('#claim_task_mess_single').focus();
        $('#claim_work_type_id_single').data('kendoDropDownList').value(curr_prj_claim_work.work_type_id);
        showPrjClaimEditButtons(false);
    };

    function openPrjClaimWorkUserEdit() {
        prj_claim_work_add_mode = false;
        $('.prj-claim-edit-work-add').hide();
        $('.prj-claim-edit-work-type-edit').hide();
        $('.prj-claim-edit-work-user-edit').show();
        $('.prj-claim-edit-work-mess-edit').show();
        $('#prjClaimEditTaskToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-task-add", false);
        $('#prjClaimEditTaskGrid').hide();
        $('#prjClaimEditWorkAddToolbar').data('kendoToolBar').enable("#btn-prj-claim-edit-work-add", false);
        $('#prjClaimEditWorkGrid').hide();
        $('.prj-claim-edit-task-mess').hide();
        $('#claim_task_mess_single').focus();
        $('#claim_work_exec_user_id_single').data('kendoDropDownList').value(curr_prj_claim_work.exec_user_id);
        showPrjClaimEditButtons(false);
    }

    function openPrjClaimMessEdit() {        
        $('.prj-claim-edit-mess').hide();
        $('.prj-claim-edit-mess-add').show();
        $('#claim_mess').focus();
        //        
        showPrjClaimEditButtons(false);
    };

    function openPrjClaimTaskMessEdit() {
        $('.prj-claim-edit-task-mess').hide();
        $('.prj-claim-edit-task-mess-add').show();
        $('#claim_task_mess').focus();
        //
        showPrjClaimEditButtons(false);
    };

    function closePrjClaimEdit() {
        firstDataBound_ClaimEditTaskGrid = true;
        firstDataBound_ClaimEditWorkGrid = true;
        firstDataBound_ClaimEditTaskMessGrid = true;
        curr_task_user_state_list = [];
        create_claim_from_task = null;
        currTaskChildActionId = null;
        currWorkChildActionId = null;
        toggleSplitter("#prjMainSplitter", "#prj-main-pane-left", "#prj-main-pane-right");
        //
        switch (leftPaneContentType) {
            case 1:
                refreshGrid('#prjClaimChildGrid');
                break;
            case 2:
                refreshGrid('#prjTaskChildGrid');
                break;
            case 4:
                refreshGrid('#prjWorkRestChildGrid');                
                break;
            default:
                break;
        }
    };

    function proceedTaskAction() {
        var today = new Date();
        var task_grid = $('#prjClaimEditTaskGrid').data('kendoGrid');
        var selected_task = task_grid.dataItem(task_grid.select());
        if (selected_task == null) {
            alert('Не выбрано задание');
            return false;
        };
        var curr_works = [];
        switch (currTaskChildActionId) {
            case 3: // принять в работу
                curr_works = $.grep($('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.view(), function (item) {
                    return ((item.exec_user_id == currUser_CabUserId) && ((item.state_type_id == 1) || (item.state_type_id == 4)));
                });
                if ((curr_works) && (curr_works.length > 0)) {
                    curr_prj_claim_work = curr_works[0];
                    if (!curr_prj_claim_work.is_accept) {
                        curr_prj_claim_work.set('is_accept',true);
                        curr_prj_claim_work.set('is_plan', true);        
                        curr_prj_claim_work.set('date_beg', today);
                        curr_prj_claim_work.set('date_end', today);
                        curr_prj_claim_work.set('date_time_beg', today);
                        curr_prj_claim_work.set('date_time_end', today);
                        curr_prj_claim_work.set('upd_date', today);
                        curr_prj_claim_work.set('upd_user', currUser_CabUserName);
                        curr_prj_claim_work.dirty = true;
                        //
                        if (selected_task) {
                            curr_task_user_state_list.push({
                                task_num: selected_task.task_num,
                                user_state_type_id: 2,
                                user_state_user_id: curr_prj_claim_work.exec_user_id,
                                user_state_crt_date: new Date()
                            });
                        }
                    }
                } else {                    
                    var work_num_new = 1;
                    var work_list_for_task = $('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.view();
                    if ((work_list_for_task) && (work_list_for_task.length > 0)) {
                        var work_array = $(work_list_for_task).toArray();
                        work_num_new = Math.max.apply(Math, work_array.map(function (o) { return o.work_num; }))
                        work_num_new = work_num_new + 1;
                    };        
                    //
                    var curr_work_type_id = 3;
                    var curr_work_type_name = 'Программирование';
                    switch (currUser_CabUserCrmRoleId) {
                        case 1:
                            curr_work_type_id = 3;
                            curr_work_type_name = 'Программирование';
                            break;
                        case 2:
                            curr_work_type_id = 4;
                            curr_work_type_name = 'Тестирование';
                            break;
                        case 3:
                        case 4:
                            curr_work_type_id = 1;
                            curr_work_type_name = 'Рассмотрение';
                            break;
                        default:
                            break;
                    }
                    //
                    selected_task.dirty = true;
                    var today = new Date();
                    curr_prj_claim_work_new = {
                        work_id: 0,
                        task_id: selected_task.task_id,
                        claim_id: selected_task.claim_id,
                        work_num: work_num_new,                        
                        work_type_id: curr_work_type_id,
                        exec_user_id: currUser_CabUserId,
                        is_all_day: true,
                        is_accept: true,
                        is_plan: true,
                        task_num: selected_task.task_num,                        
                        work_type_name: curr_work_type_name,
                        exec_user_name: currUser_CabUserName,
                        state_type_id: 1,
                        state_type_name: 'Активно',
                        state_type_templ: '<span class="text-success">Активно</span>',
                        mess: null,
                        date_send: today,
                        date_beg: today,
                        date_end: today,
                        date_time_beg: today,
                        date_time_end: today,
                        upd_date: today,
                        upd_user: currUser_CabUserName,
                        is_new: true
                    };
                    //
                    $('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.add(curr_prj_claim_work_new);
                    //
                    curr_task_user_state_list.push({                                
                        task_num: selected_task.task_num,
                        user_state_type_id: 2,
                        user_state_user_id: curr_prj_claim_work_new.exec_user_id,
                        user_state_crt_date: new Date()
                    });
                };
                showPrjClaimEditButtons(true);
                break;
            case 4: // выполнить и передать дальше
            case 5: // выполнить и вернуть назад
                //
                curr_works = $.grep($('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.view(), function (item) {
                    return ((item.exec_user_id == currUser_CabUserId) && ((item.state_type_id == 1) || (item.state_type_id == 4)));
                });
                if ((curr_works) && (curr_works.length > 0)) {
                    curr_prj_claim_work = curr_works[0];
                } else {
                    alert('Не найдено активной работы для ' + currUser_CabUserName);
                    return false;
                };
                openPrjClaimWorkAdd();
                //
                if (currTaskChildActionId == 5) {                    
                    var last_work = null;
                    var curr_works = $('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.view();                    
                    for (var i = 0; i < curr_works.length; i++) {
	                    var curr_work_dataItem = curr_works.at(i);
	                    if ((curr_work_dataItem) && (curr_work_dataItem.state_type_id == 2)) {
		                    last_work = curr_work_dataItem;
	                    };
                    };
                    if (last_work) {
	                    $("#claim_work_exec_user_id_single").data("kendoDropDownList").value(last_work.exec_user_id);                        
                        $("#claim_work_type_id_single").data("kendoDropDownList").value(last_work.work_type_id);
                    };
                };
                break;
            case 6: // выполнить и завершить
            case 7: // отменить
                curr_works = $.grep($('#prjClaimEditWorkGrid').data('kendoGrid').dataSource.view(), function (item) {
                    return ((item.exec_user_id == currUser_CabUserId) && ((item.state_type_id == 1) || (item.state_type_id == 4)));
                });
                if ((curr_works) && (curr_works.length > 0)) {
                    curr_prj_claim_work = curr_works[0];
                    openPrjClaimWorkEdit();
                    $("#claim_work_type_id_single").data("kendoDropDownList").value(curr_prj_claim_work.work_type_id);
                    $("#claim_work_type_id_single").data("kendoDropDownList").enable(false);
                } else {
                    alert('Не найдено активной работы для ' + currUser_CabUserName);
                    return false;
                };
                break;
            default:
                break;
        }
    };

    function getTemplForUserStateType(id) {
        switch (id) {
            case 1:
                return '<span class="k-sprite cab-png cab-png-send-task" title="Передано в работу"></span>';
            case 2:
                return '<span class="k-sprite cab-png cab-png-receive-task" title="Принято в работу"></span>';
            case 3:
                return '<span class="k-sprite cab-png cab-png-done-task" title="Выполнено"></span>';
            case 4:
                return '<span class="k-sprite cab-png cab-png-cancel-task" title="Отменено"></span>';
            default:
                return "";
        };
    }

    function showPrjClaimEditButtons(show) {
        if (show) {
            $('#btnPrjClaimEditSubmit').show();
            $('#btnPrjClaimEditSave').show();
            $('#btnPrjClaimEditCancel').show();
            $('#btnPrjClaimEditReset').show();
		    $('.prj-claim-edit-separator').show();
        } else {
            $('#btnPrjClaimEditSubmit').hide();
            $('#btnPrjClaimEditSave').hide();
            $('#btnPrjClaimEditCancel').hide();
            $('#btnPrjClaimEditReset').hide();
		    $('.prj-claim-edit-separator').hide();
        }
    };

    function onAttrChanged() {
        showPrjClaimEditButtons(true);
    };

    function reloadClaim(claim_id) {
        firstDataBound_ClaimEditTaskGrid = true;
        firstDataBound_ClaimEditWorkGrid = true;
        firstDataBound_ClaimEditTaskMessGrid = true;
        curr_task_user_state_list = [];
        create_claim_from_task = null;
        currTaskChildActionId = null;
        currWorkChildActionId = null;
        loadingDialog('#prj-loading', true);
        setTimeout(function () {
            rightPaneContentType = 2;
            leftPaneContentType = 2;
            leftPaneContentType_main = 3;
            leftPaneContentType_child = 4;
            prjList.openPrjClaimEditPartial(claim_id);
        }, waitTimeout);
    };

    function initClaimToBufferDialog() {    
        $("#copy-claim-dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 800,
            height: 700,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            },
            open: function (event, ui) {
                $('#copy-claim-textarea').select();                
            }
        });
    };

    function showClaimToBufferDialog(text) {
                initClaimToBufferDialog();
                $('#copy-claim-textarea').val(text);
                $('#copy-claim-dialog').dialog('open');
    };

}

var prjClaimEditPartial = null;
var curr_prj_claim = null;
var curr_prj_claim_mess_list = [];
var curr_prj_claim_task_list = [];
var curr_prj_claim_work_list = [];
var curr_prj_claim_work_del_list = [];
var curr_task_user_state_list = [];
var curr_prj_claim_mess = null;
var curr_prj_claim_task = null;
var curr_prj_claim_work = null;
var curr_prj_claim_work_new = null;
var prj_claim_add_mode = false;
var prj_claim_task_add_mode = false;
var prj_claim_work_add_mode = false;
var firstDataBound_ClaimEditTaskGrid = true;
var firstDataBound_ClaimEditWorkGrid = true;
var firstDataBound_ClaimEditTaskMessGrid = true;
var prj_claim_task_edit_change_dates = false;
var prj_claim_task_edit_change_exec_user = false;
var prj_claim_task_edit_finish_work = false;
var create_claim_from_task = null;
var create_claim_name = null;
var create_claim_text = null;
var lockDataBound_ClaimEditWorkGrid = false;
var curr_prj_claim_task_id = null;

$(function () {
    prjClaimEditPartial = new PrjClaimEditPartial();
    prjClaimEditPartial.init();
});

