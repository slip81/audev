﻿function PrjWorkListPartial() {
    _this = this;

    this.init = function () {
        //
    };

    this.init_after = function () {
        if (prjWorkList_inited) {
            return;
        }
        //
        setControlHeight('#prjWorkSplitter', 0);
        // !!!
        // разобраться
        $("#prjWorkSplitter").data("kendoSplitter").size("#work-pane-top", "60%");
        //
        $("#prjWorkGrid").delegate("tbody>tr", "dblclick", function () {
            var dataItem = $("#prjWorkGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {                
                loadingDialog('#prj-loading', true);
                setTimeout(function () {
                    rightPaneContentType = 2;
                    leftPaneContentType = 4;
                    leftPaneContentType_main = 4;
                    leftPaneContentType_child = null;
                    prjList.openPrjClaimEditPartial(dataItem.claim_id);
                }, waitTimeout);
            };
        });
        //
        $("#prjWorkRestChildGrid").delegate("tbody>tr", "dblclick", function () {
            var dataItem = $("#prjWorkRestChildGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {                
                loadingDialog('#prj-loading', true);
                setTimeout(function () {
                    rightPaneContentType = 2;
                    leftPaneContentType = 4;
                    leftPaneContentType_main = 4;
                    leftPaneContentType_child = null;
                    prjList.openPrjClaimEditPartial(dataItem.claim_id);
                }, waitTimeout);
            };
        });
        //
        $(document).on('click', '#btn-prj-work-apply-filter', function () {
            var grid = $("#prjWorkGrid").data("kendoGrid")
            var ds = grid.dataSource;
            var currFilters = [];
            var currFilter = [];

            currFilter = getMultiSelectFilters('#prjWorkRespFilter', 'claim_resp_user_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            currFilter = getMultiSelectFilters('#prjWorkProjectFilter', 'claim_project_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            currFilter = getMultiSelectFilters('#prjWorkClientFilter', 'claim_client_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            currFilter = getMultiSelectFilters('#prjWorkPriorityFilter', 'claim_priority_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            currFilter = getMultiSelectFilters('#prjWorkTypeFilter', 'work_type_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            var datePlanBegFilter = $('#prjWorkDatePlan1').data('kendoDatePicker').value();
            if (datePlanBegFilter) {
                currFilters.push({ field: "task_date_plan", operator: "gte", value: datePlanBegFilter });
            };

            var datePlanEndFilter = $('#prjWorkDatePlan2').data('kendoDatePicker').value();
            if (datePlanEndFilter) {
                currFilters.push({ field: "task_date_plan", operator: "lte", value: datePlanEndFilter });
            };

            var dateBegFilter = $('#prjWorkDateBegFilter').data('kendoDatePicker').value();
            if (dateBegFilter) {
                currFilters.push({ field: "date_beg", operator: "lte", value: dateBegFilter });
                currFilters.push(
                    {
                        logic: "or",
                        filters: [
                            {
                                field: "date_end",
                                operator: "gte",
                                value: dateBegFilter
                            },
                            {
                                field: "date_end",
                                operator: "eq",
                                value: "null"
                            }
                        ]
                    });
            };

            var textFilter = $('#prjWorkTextFilter').val();
            if (textFilter) {
                currFilters.push({ field: "task_text", operator: "contains", value: textFilter });
            }

            ds.filter({ logic: "and", filters: currFilters });
            togglePrjWorkFilter();
        });
        //
        $(document).on('click', '#btn-prj-work-reset-filter', function () {
            clearPrjWorkFilter(false);
            togglePrjWorkFilter();
        });
        //
        $(document).on('click', '.k-checkbox-label[for="chbWorkIsAccept"]', function () {
            setTimeout(function () {
                refreshGrid('#prjWorkGrid');
            }, 500);            
        });
        //
        prjWorkList_inited = true;
    };

    this.reload_after = function () {
        prjWorkGridDataBound_first = true;
        prjWorkGridDataBinding_first = true;
        prjWorkRestChildGridDataBound_first = true;
        prjWorkRestChildMessChildGridDataBound_first = true;
        prjWorkList_inited = false;
    };

    this.onPrjWorkGridDataBinding = function (e) {
        if (prjWorkGridDataBinding_first) {
            prjWorkGridDataBinding_first = false;
            gridConfigApply(curr_user_settings_grid, '#prjWorkGrid');            
        };
        prjWork_selectedWorkId = saveSelectedRow('#prjWorkGrid', 'work_id');
    };

    this.onPrjWorkGridDataBound = function (e) {
        drawEmptyRow('#prjWorkGrid');
        if (prjWorkGridDataBound_first) {
            prjWorkGridDataBound_first = false;
            resizePrjWorkGrid();            
        };
        restoreSelectedRow('#prjWorkGrid', prjWork_selectedWorkId);
        //
        refreshPrjWorkGridFooter();
    };

    this.onPrjWorkGridChange = function (e) {
        refreshGrid('#prjWorkRestChildGrid');
    };

    this.onBtnPrjClaimAddClick = function (e) {        
        loadingDialog('#prj-loading', true);
        setTimeout(function () {
            rightPaneContentType = 2;
            leftPaneContentType = 4;
            leftPaneContentType_main = 4;
            leftPaneContentType_child = null;
            prjList.openPrjClaimEditPartial(null, null);
        }, waitTimeout);
    };

    this.onPrjWorkSplitterResize = function (e) {
        resizePrjWorkGrid();
        resizePrjWorkRestChildGrid();
        resizePrjWorkRestChildMessChildGrid();
    };

    this.getPrjWorkRestChildGridData = function (e) {
        var selected_work_id = null;
        var grid = $("#prjWorkGrid").data("kendoGrid");
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem) {
            selected_work_id = selectedItem.work_id;
        }
        return { work_id: selected_work_id };
    };

    this.onPrjWorkRestChildGridDataBinding = function (e) {
        prjWork_selectedWorkRestId = saveSelectedRow('#prjWorkRestChildGrid', 'work_id');
    };

    this.onPrjWorkRestChildGridDataBound = function (e) {
        drawEmptyRow('#prjWorkRestChildGrid');
        if (prjWorkRestChildGridDataBound_first) {
            prjWorkRestChildGridDataBound_first = false;
            resizePrjWorkRestChildGrid();            
        };
        restoreSelectedRow('#prjWorkRestChildGrid', prjWork_selectedWorkRestId);
    };

    this.onPrjWorkRestChildGridChange = function (e) {
        refreshGrid('#prjWorkRestChildMessChildGrid');
    };

    this.getPrjWorkRestChildMessChildGridData = function (e) {
        var selected_work_id = null;
        var grid = $("#prjWorkRestChildGrid").data("kendoGrid");
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem) {
            selected_work_id = selectedItem.work_id;
        }
        return { work_id: selected_work_id };
    };

    this.onPrjWorkRestChildMessChildGridDataBound = function (e) {
        drawEmptyRow('#prjWorkRestChildMessChildGrid');
        if (prjWorkRestChildMessChildGridDataBound_first) {
            prjWorkRestChildMessChildGridDataBound_first = false;
            resizePrjWorkRestChildMessChildGrid();
        };
    };


    this.onPrjWorkSearchKeyup = function (e) {
        if (e.keyCode == 13) {
            refreshGrid('#prjWorkGrid');
        };
    };

    this.onPrjWorkPrjFilterChange = function (e) {
        refreshGrid('#prjWorkGrid');
    };

    this.onPrjWorkExecUserFilterChange = function (e) {
        refreshGrid('#prjWorkGrid');
    };

    this.onPrjWorkToolbarToggle = function (e) {
        refreshGrid('#prjWorkGrid');
    };

    this.getPrjWorkGridData = function (e) {        
        if (prjWorkGridDataBinding_first) {
            return {
                prj_id_list: null,
                exec_user_id_list: -1,
                work_state_type_id_list: null,
                is_accept: false,
                search: null
            };
        };
        var curr_prj_id = null;
        var curr_prj_dataItem = $('#prjWorkPrjFilter').data('kendoDropDownList').dataItem();
        if (curr_prj_dataItem) {
            curr_prj_id = curr_prj_dataItem.prj_id;
        };
        //
        var curr_exec_user_id = null;
        var curr_exec_user_dataItem = $('#prjWorkExecUserFilter').data('kendoDropDownList').dataItem();
        if (curr_exec_user_dataItem) {
            curr_exec_user_id = curr_exec_user_dataItem.user_id;
        };
        //
        var curr_work_state_type_id = null;
        var curr_work_state_type_arr = [];
        $('#prjWorkToolbar>.k-button-group>.k-toggle-button.k-state-active').each(function (ind) {
            curr_work_state_type_arr.push($(this).attr('data-state-type-id'))
        })
        if (curr_work_state_type_arr.length > 0) {
            curr_work_state_type_id = curr_work_state_type_arr.join();
        }
        //
        var curr_is_accept = $('#chbWorkIsAccept').is(':checked');
        //var curr_is_accept = !($('#chbWorkIsAccept').is(':checked'));
        //
        return {
            prj_id_list: curr_prj_id,
            exec_user_id_list: curr_exec_user_id,
            work_state_type_id_list: curr_work_state_type_id,
            is_accept: curr_is_accept,
            search: $('#prj-work-search').val()
        };
    };

    this.onBtnPrjWorkFilterClick = function (e) {
        var config_action_type = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (config_action_type) {
            case 1:
                togglePrjWorkFilter();
                break;
            case 2:                
                clearPrjWorkFilter(true);
                break;
            case 3:                
                clearPrjWorkSort();
                break;
            default:
                break;
        };	
    };


    this.onBtnPrjFaqClick = function (e) {
        window.open('/prj_faq.html', '_blank');
    };

    this.onBtnPrjWorkExcelClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Выгрузить список в Excel ?', function () {
            $('#prjWorkGrid').data('kendoGrid').saveAsExcel();
        });        
    };

    this.getSettings = function () {
        var settings = [];
        settings.push({ id: 'prj', val: $("#prjWorkPrjFilter").data("kendoDropDownList").value() });
        settings.push({ id: 'exec_user', val: $("#prjWorkExecUserFilter").data("kendoDropDownList").value() });
        //
        $('#prjWorkToolbar>.k-button-group>.k-toggle-button').each(function (ind) {
            settings.push({ id: 'work_state_type_' + $(this).attr('data-state-type-id'), val: $(this).hasClass('k-state-active') ? 'true' : null });
        })
        //
        settings.push({ id: 'is_accept', val: $("#chbWorkIsAccept").is(":checked") });
        //
        settings.push({ id: 'work_resp_user', val: JSON.stringify($("#prjWorkRespFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'work_project', val: JSON.stringify($("#prjWorkProjectFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'work_client', val: JSON.stringify($("#prjWorkClientFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'work_priority', val: JSON.stringify($("#prjWorkPriorityFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'work_date_plan1', val: $("#prjWorkDatePlan1").data("kendoDatePicker").value() });
        settings.push({ id: 'work_date_plan2', val: $("#prjWorkDatePlan2").data("kendoDatePicker").value() });
        settings.push({ id: 'work_text', val: $("#prjWorkTextFilter").val() });
        settings.push({ id: 'work_type', val: JSON.stringify($("#prjWorkTypeFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'work_date_beg', val: $("#prjWorkDateBegFilter").data("kendoDatePicker").value() });
        //
        settings.push({ id: 'work_splitter', val: $("#prjWorkSplitter").data("kendoSplitter").size("#work-pane-top") });
        settings.push({ id: 'work_splitter_child', val: $("#prjWorkSplitterBottom").data("kendoSplitter").size("#work-pane-bottom-left") });
        //
        return settings;
    };

    // private

    function resizePrjWorkGrid() {
        setGridMaxHeight_relative('#prjWorkGrid', '#work-pane-top', $('#prjWorkToolbar').height() + 10);        
    };

    function resizePrjWorkRestChildGrid() {
        setGridMaxHeight_relative('#prjWorkRestChildGrid', '#work-pane-bottom', $('#prjWorkRestChildToolbar').height() + 10);
    };

    function resizePrjWorkRestChildMessChildGrid() {
        setGridMaxHeight_relative('#prjWorkRestChildMessChildGrid', '#work-pane-bottom', $('#prjWorkRestChildMessChildToolbar').height() + 10);
    };

    function togglePrjWorkFilter() {
        var filter = $('#prjWorkFilter').data('kendoPanelBar');
        var is_expanded = $('#prj-work-filter-panel').hasClass('k-state-active');
        if (is_expanded) {
            filter.collapse($('#prj-work-filter-panel'));
        } else {
            filter.expand($('#prj-work-filter-panel'));
        };
    };

    function clearPrjWorkFilter(all) {
        var grid = $("#prjWorkGrid").data("kendoGrid")
        var ds = grid.dataSource;
        var currFilters = [];

        $('#prjWorkRespFilter').data('kendoMultiSelect').value([]);
        $('#prjWorkProjectFilter').data('kendoMultiSelect').value([]);
        $('#prjWorkClientFilter').data('kendoMultiSelect').value([]);
        $('#prjWorkPriorityFilter').data('kendoMultiSelect').value([]);
        $('#prjWorkTypeFilter').data('kendoMultiSelect').value([]);
        $('#prjWorkDatePlan1').data('kendoDatePicker').value(null);
        $('#prjWorkDatePlan2').data('kendoDatePicker').value(null);
        $('#prjWorkDateBegFilter').data('kendoDatePicker').value(null);
        $('#prjWorkTextFilter').val('');
        $('#prj-work-search').val('');        

        if (all) {
            $('#prjWorkPrjFilter').data('kendoDropDownList').select(0);
            $('#prjWorkExecUserFilter').data('kendoDropDownList').select(0);
            $('#chbWorkIsAccept').removeAttr('checked');
        };

        ds.filter({ logic: "and", filters: currFilters });
    };

    function clearPrjWorkSort() {
        clearSortGrid("#prjWorkGrid");        
    };

    function refreshPrjWorkGridFooter() {        
        var active_cnt = 0;
        var done_cnt = 0;        
        var canceled_cnt = 0;
        var returned_cnt = 0;
        var all_cnt = 0;
        $.each($('#prjWorkGrid').data('kendoGrid').dataSource.view(), function (key, item) {
            all_cnt++;
            switch (item.state_type_id) {
                case 1:
                    active_cnt++;
                    break;
                case 2:
                    done_cnt++;
                    break;
                case 3:
                    canceled_cnt++;
                    break;
                case 4:
                    returned_cnt++;
                    break;
                default:
                    break;
            }
        });

        var first = true;
        var footer_html = 'Всего:' + all_cnt + ' [';
        if (active_cnt) {
            if (!first) {
                footer_html += ', ';
            }
            footer_html += 'активно: ' + active_cnt;
            first = false;
        };
        if (done_cnt) {
            if (!first) {
                footer_html += ', ';
            }
            footer_html += 'выполнено: ' + done_cnt;
            first = false;
        };
        if (canceled_cnt) {
            if (!first) {
                footer_html += ', ';
            }
            footer_html += 'отменено: ' + canceled_cnt;
            first = false;
        };
        if (returned_cnt) {
            if (!first) {
                footer_html += ', ';
            }
            footer_html += 'возвращено: ' + returned_cnt;
            first = false;
        };
        footer_html += ']';

        var footer = $('#prjWorkGrid>.k-grid-pager').find('span.prj-grid-footer');
        if (footer.length <= 0) {
            $('#prjWorkGrid span.k-pager-sizes').append('<span class="prj-grid-footer"></span>')
        }
        $('#prjWorkGrid>.k-grid-pager span.prj-grid-footer').html(footer_html);
    };
}

var prjWorkListPartial = null;
var prjWorkList_inited = false;
var prjWorkGridDataBound_first = true;
var prjWorkGridDataBinding_first = true;
var prjWorkRestChildGridDataBound_first = true;
var prjWorkRestChildMessChildGridDataBound_first = true;
var prjWork_selectedWorkId = null;
var prjWork_selectedWorkRestId = null;

$(function () {
    prjWorkListPartial = new PrjWorkListPartial();
    prjWorkListPartial.init();
});
