﻿function PrjClaimListPartial() {
    _this = this;

    this.init = function () {
        //
    };

    this.init_after = function () {
        if (prjClaimList_inited) {
            return;
        }
        //
        setControlHeight('#prjClaimSplitter', 0);
        setControlHeight('#prjClaimSplitter', 0);
        // разобраться
        $("#prjClaimSplitter").data("kendoSplitter").size("#clame-pane-top", "60%");
        //
        $("#prjClaimGrid").delegate("tbody>tr", "dblclick", function () {
            var dataItem = $("#prjClaimGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {
                dblclickFired = true;
                loadingDialog('#prj-loading', true);
                setTimeout(function () {
                    rightPaneContentType = 2;
                    leftPaneContentType = 2;
                    leftPaneContentType_main = 3;
                    leftPaneContentType_child = 4;
                    prjList.openPrjClaimEditPartial(dataItem.claim_id);
                    dblclickFired = false;
                }, waitTimeout);
            };
        });
        //
        $("#prjTaskChildGrid").delegate("tbody>tr", "dblclick", function () {
            var dataItem = $("#prjTaskChildGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {
                loadingDialog('#prj-loading', true);
                setTimeout(function () {
                    rightPaneContentType = 2;
                    leftPaneContentType = 2;
                    leftPaneContentType_main = 3;
                    leftPaneContentType_child = 4;
                    prjList.openPrjClaimEditPartial(dataItem.claim_id, null, null, dataItem.task_id);
                }, waitTimeout);
            };
        });
        //
        $("#prjTaskChildWorkChildGrid").delegate("tbody>tr", "dblclick", function () {
            var dataItem = $("#prjTaskChildWorkChildGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {
                loadingDialog('#prj-loading', true);
                setTimeout(function () {
                    rightPaneContentType = 2;
                    leftPaneContentType = 2;
                    leftPaneContentType_main = 3;
                    leftPaneContentType_child = 4;
                    prjList.openPrjClaimEditPartial(dataItem.claim_id, null, null, dataItem.task_id);
                }, waitTimeout);
            };
        });
        //
        $(document).on('click', '#btn-prj-claim-apply-filter', function () {
            var grid = $("#prjClaimGrid").data("kendoGrid")
            var ds = grid.dataSource;
            var currFilters = [];
            var currFilter = [];

            currFilter = getMultiSelectFilters('#prjClaimRespFilter', 'resp_user_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            currFilter = getMultiSelectFilters('#prjClaimClientFilter', 'client_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            currFilter = getMultiSelectFilters('#prjClaimPriorityFilter', 'priority_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            currFilter = getMultiSelectFilters('#prjClaimClaimTypeFilter', 'claim_type_id', 'eq');
            if (currFilter.length > 0) {
                currFilters.push(
                    {
                        logic: "or",
                        filters: currFilter
                    });
            };

            var datePlanBegFilter = $('#prjClaimCrtDate1').data('kendoDatePicker').value();
            if (datePlanBegFilter) {
                currFilters.push({ field: "crt_date", operator: "gte", value: datePlanBegFilter });
            };

            var datePlanEndFilter = $('#prjClaimCrtDate2').data('kendoDatePicker').value();
            if (datePlanEndFilter) {
                currFilters.push({ field: "crt_date", operator: "lte", value: datePlanEndFilter });
            };

            var dsFilter = ds.filter();
            if (!dsFilter) {
                dsFilter = {};
                dsFilter.filters = [];
            };
            var newFilter = [];
            newFilter.push({ logic: "and", filters: currFilters });
            if (dsFilter.filters.length > 0) {
                newFilter.push({ logic: "and", filters: dsFilter.filters });
            };
            //ds.filter({ logic: "and", filters: currFilters });
            ds.filter(newFilter);
            togglePrjClaimFilter();
        });
        //
        $(document).on('click', '#btn-prj-claim-reset-filter', function () {
            clearPrjClaimFilter(false);
            togglePrjClaimFilter();
        });
        //
        $(document).on('click', '.claim-list-favor-cell', function () {
            var row = $(this).closest('tr');
            var dataItem = $('#prjClaimGrid').data('kendoGrid').dataItem(row);
            if (!dataItem)
                return;
            if ($(this).hasClass('cab-png-favor-not')) {
                $.ajax({
                    url: '/PrjClaimFavorAdd',
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify({ claim_id: dataItem.claim_id })
                });
                $(this).removeClass('cab-png-favor-not');
                $(this).addClass('cab-png-favor');
            } else {
                $.ajax({
                    url: '/PrjClaimFavorDel',
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify({ claim_id: dataItem.claim_id })
                });
                $(this).removeClass('cab-png-favor');
                $(this).addClass('cab-png-favor-not');
            };
        });
        //
        $('#btnPrjUserSelectOk').on('click', function () {
            $('#winPrjUserSelect').data('kendoWindow').close();
            proceedTaskChildAction();
        });
        //
        $('#btnPrjUserSelectCancel').on('click', function () {
            $('#winPrjUserSelect').data('kendoWindow').close();
        });
        //
        $('#btnPrjTaskAddOk').on('click', function () {
            $('#winPrjTaskAdd').data('kendoWindow').close();
            addTask();
        });
        //
        $('#btnPrjTaskAddCancel').on('click', function () {
            $('#winPrjTaskAdd').data('kendoWindow').close();
        });
        //
        $('#btnPrjWorkTypeChangeOk').on('click', function () {
            $('#winPrjWorkTypeChange').data('kendoWindow').close();
            changeWorkType();
        });
        //
        $('#btnPrjWorkTypeChangeCancel').on('click', function () {
            $('#winPrjWorkTypeChange').data('kendoWindow').close();
        });
        //
        $('#btnPrjWorkUserChangeOk').on('click', function () {
            $('#winPrjWorkUserChange').data('kendoWindow').close();
            changeWorkUser();
        });
        //
        $('#btnPrjWorkUserChangeCancel').on('click', function () {
            $('#winPrjWorkUserChange').data('kendoWindow').close();
        });
        //
        $(document).on('dblclick', '.prj-claim-filter-ddl', function () {
            //alert('k-dropdown-wrap dblclick!');
            var curr_filter_id = $('#prjClaimFilter').data('kendoDropDownList').value();
            if (curr_filter_id == -1) {
                prjClaimFilterEdit();
            } else if (curr_filter_id) {
                prjClaimFilterEdit(curr_filter_id);
            };
        });
        //
        $(document).on('click', '#prjClaimGrid a.k-pager-refresh', function () {
            //alert('manual grid refresh !');           
            var curr_filter_id_dataItem = $('#prjClaimFilter').data('kendoDropDownList').dataItem();
            prjClaim_setFilterParams = curr_filter_id_dataItem && curr_filter_id_dataItem.filter_id > 0 && !curr_filter_id_dataItem.is_fixed;
        });
        //
        prjClaimList_inited = true;
    };

    this.reload_after = function () {
        prjClaimGridDataBound_first = true;
        prjClaimGridDataBinding_first = true;
        prjTaskChildGridDataBound_first = true;
        prjTaskChildWorkChildGridDataBound_first = true;
        prjClaimList_inited = false;
    };

    this.onPrjClaimGridDataBinding = function (e) {
        if (prjClaimGridDataBinding_first) {
            prjClaimGridDataBinding_first = false;
            gridConfigApply(curr_user_settings_grid, '#prjClaimGrid');
        };
        // !!!
        if (prjClaim_setFilterParams) {
            e.preventDefault();
            prjClaim_setFilterParams = false;            
            var curr_filter_id = $('#prjClaimFilter').data('kendoDropDownList').value();
            $('#winPrjClaimFilterParams').data('kendoWindow')
            .refresh({
                url: "/PrjClaimFilterParams",
                data: { filter_id: curr_filter_id }
            }).center().open();            
        }
        prjClaim_selectedClaimId = saveSelectedRow('#prjClaimGrid', 'claim_id');
    };

    this.onPrjClaimGridDataBound = function (e) {
        drawEmptyRow('#prjClaimGrid');
        if (prjClaimGridDataBound_first) {
            prjClaimGridDataBound_first = false;
            resizePrjClaimGrid();
        };
        restoreSelectedRow('#prjClaimGrid', prjClaim_selectedClaimId);
        refreshPrjClaimGridFooter();
        //
        var dsFilter = $('#prjClaimGrid').data('kendoGrid').dataSource.filter();
        if ((dsFilter) && (dsFilter.filters.length > 0)) {
            $('#btn-prj-claim-filter').css('background-color', '#ebebeb');
        } else {
            $('#btn-prj-claim-filter').css('background-color', '');
        };
    };

    this.onPrjClaimGridChange = function (e) {
        setTimeout(function () {
            if (!dblclickFired) {
                refreshGrid('#prjTaskChildGrid');
            };
        }, waitTimeout / 2);
    };

    this.onPrjClaimSplitterResize = function (e) {
        resizePrjClaimGrid();
        resizePrjTaskChildGrid();
        resizePrjTaskChildWorkChildGrid();
    };

    this.getPrjTaskChildGridData = function (e) {
        var selected_claim_id = null;
        var grid = $("#prjClaimGrid").data("kendoGrid");
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem) {
            selected_claim_id = selectedItem.claim_id;
        }
        //
        var curr_task_user_state_type = null;
        //
        var curr_exec_user_id = null;
        var curr_exec_user_dataItem = $('#prjClaimExecUserFilter').data('kendoDropDownList').dataItem();
        if (curr_exec_user_dataItem) {
            curr_exec_user_id = curr_exec_user_dataItem.user_id;
        };
        //
        return { claim_id: selected_claim_id, exec_user_id: curr_exec_user_id, task_user_state_type: curr_task_user_state_type };
    };

    this.onPrjTaskChildGridDataBinding = function (e) {
        prjClaim_selectedTaskId = saveSelectedRow('#prjTaskChildGrid', 'task_id');
    };

    this.onPrjTaskChildGridDataBound = function (e) {
        drawEmptyRow('#prjTaskChildGrid');
        if (prjTaskChildGridDataBound_first) {
            prjTaskChildGridDataBound_first = false;
            resizePrjTaskChildGrid();
        };
        restoreSelectedRow('#prjTaskChildGrid', prjClaim_selectedTaskId);
    };

    this.onPrjTaskChildGridChange = function (e) {
        refreshGrid('#prjTaskChildWorkChildGrid');
    };

    this.getPrjTaskChildWorkChildGridData = function (e) {
        var selected_task_id = null;
        var grid = $("#prjTaskChildGrid").data("kendoGrid");
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem) {
            selected_task_id = selectedItem.task_id;
        }
        return { task_id: selected_task_id };
    };

    this.onPrjTaskChildWorkChildGridDataBound = function (e) {
        drawEmptyRow('#prjTaskChildWorkChildGrid');
        if (prjTaskChildWorkChildGridDataBound_first) {
            prjTaskChildWorkChildGridDataBound_first = false;
            resizePrjTaskChildWorkChildGrid();
        };
    }

    this.onBtnPrjClaimFavorClick = function (e) {
        var grid = $("#prjClaimGrid").data("kendoGrid")
        var ds = grid.dataSource;
        var currFilters = [];
        var btn = $(e.target).find('span').first();
        if (btn.hasClass('cab-png-favor-not')) {
            btn.removeClass('cab-png-favor-not');
            btn.addClass('cab-png-favor');
            //
            currFilters.push({ field: "is_favor", operator: "equals", value: 'true' });
        } else {
            btn.removeClass('cab-png-favor');
            btn.addClass('cab-png-favor-not');
        };
        ds.filter({ logic: "and", filters: currFilters });
    };

    this.onBtnPrjClaimFilterClick = function (e) {
        var config_action_type = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (config_action_type) {
            case 1:
                togglePrjClaimFilter();
                break;
            case 2:
                clearPrjClaimFilter(true);
                break;
            case 3:
                clearPrjClaimSort();
                break;
            case 4:
                $("#prjClaimGrid").data("kendoGrid").dataSource.sort([{ field: "is_active_state", dir: "desc" }, { field: "summary_rate", dir: "desc" }, { field: "crt_date", dir: "desc" }]);
                break;
            case 5:
                // !!!
                /*
                var d = new Date();
                var month = d.getMonth() + 1;
                var day = d.getDate();
                var date_str = (day < 10 ? '0' : '') + day
                    + '.'
                    + (month < 10 ? '0' : '') + month
                    + '.'
                    + d.getFullYear();
                showPrjReportDialog('Задания по текущей версии Аптека-Урал на ' + date_str);
                */
                $.ajax({
                    url: '/PrjClaimReport1',
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: null,
                    success: function (res) {
                        if ((!res) || (res.err)) {
                            alert(res && res.mess ? res.mess : 'Ошибка при формировании отчета');
                        } else {
                            showPrjReportDialog(res.text);
                        };
                    },
                    error: function (res) {
                        alert('Ошибка формирования отчета');
                    }
                });
                break;
            default:
                break;
        };
    };

    this.onPrjClaimSearchKeyup = function (e) {
        if (e.keyCode == 13) {
            refreshGrid('#prjClaimGrid');
        };
    };

    this.getPrjClaimGridData = function (e) {
        if (prjClaimGridDataBinding_first) {
            return {
                project_id_list: null,
                version_main_id_list: null,
                claim_stage_id_list: null,
                exec_user_id_list: -1,
                claim_is_active_list: null,
                filter_id: null,
                prj_id_list: null,                
                search: null             
            };
        };
        var curr_project_id = null;
        var curr_project_dataItem = $('#prjClaimProjectFilter').data('kendoDropDownList').dataItem();
        if (curr_project_dataItem) {
            curr_project_id = curr_project_dataItem.project_id;
        }
        //
        var curr_version_main_name = null;
        var curr_version_main_dataItem = $('#prjClaimVersionFilter').data('kendoDropDownList').dataItem();
        if ((curr_version_main_dataItem) && (curr_version_main_dataItem.version_main_id > 0)) {
            curr_version_main_name = curr_version_main_dataItem.version_main_name;
        }
        //
        var curr_claim_stage_id = null;
        var curr_claim_stage_dataItem = $('#prjClaimStageFilter').data('kendoDropDownList').dataItem();
        if (curr_claim_stage_dataItem) {
            curr_claim_stage_id = curr_claim_stage_dataItem.claim_stage_id;
        }
        //
        var curr_exec_user_id = null;
        var curr_exec_user_dataItem = $('#prjClaimExecUserFilter').data('kendoDropDownList').dataItem();
        if (curr_exec_user_dataItem) {
            curr_exec_user_id = curr_exec_user_dataItem.user_id;
        };
        //
        var curr_claim_is_active_list = null;
        var curr_claim_is_active_arr = [];
        $('#prjClaimToolbar>.k-button-group>.k-toggle-button.k-state-active').each(function (ind) {
            curr_claim_is_active_arr.push($(this).attr('data-state-type-id'))
        })
        if (curr_claim_is_active_arr.length > 0) {
            curr_claim_is_active_list = curr_claim_is_active_arr.join();
        }
        //
        var curr_filter_id = null;
        var curr_filter_id_dataItem = $('#prjClaimFilter').data('kendoDropDownList').dataItem();
        if (!curr_filter_id_dataItem) {
            curr_filter_id = curr_user_settings_filter;
        } else if ((curr_filter_id_dataItem) && (curr_filter_id_dataItem.filter_id != -1)) {
            curr_filter_id = curr_filter_id_dataItem.filter_id;
        };
        //
        var curr_prj_id = null;
        var curr_prj_id_arr = $('#prjClaimPrjFilter').data('kendoMultiSelect').value();
        if (curr_prj_id_arr.length > 0) {
            curr_prj_id = curr_prj_id_arr.join();
        }        
        //
        return {                        
            project_id_list: curr_project_id,
            version_main_name_list: curr_version_main_name,
            claim_stage_id_list: curr_claim_stage_id,
            exec_user_id_list: curr_exec_user_id,            
            claim_is_active_list: curr_claim_is_active_list,
            filter_id: curr_filter_id,
            prj_id_list: curr_prj_id,
            search: $('#prj-claim-search').val(),
        };
    };

    this.onPrjClaimToolbarToggle = function (e) {
        refreshGrid('#prjClaimGrid');
    };

    this.onPrjClaimVersionFilterDataBound = function (e) {
        if (prjClaimVersionFilterDataBound_first) {
            prjClaimVersionFilterDataBound_first = false;
            var ds2 = $('#prjClaimVersionFilter').data('kendoDropDownList').dataSource;
            var dsFilters = [{ field: "project_id", operator: "eq", value: -1 }];
            ds2.filter(
                {
                    logic: 'or',
                    filters: dsFilters
                });
        }
    };

    this.onPrjClaimProjectFilterChange = function (e) {
        //
        refreshPrjAndVersion();
        //
        setTimeout(function () {
            refreshGrid('#prjClaimGrid');
        }, 500);
    };

    this.onPrjClaimExecUserFilterChange = function (e) {
        refreshGrid('#prjClaimGrid');
    };

    this.onPrjClaimVersionFilterChange = function (e) {
        refreshGrid('#prjClaimGrid');
    };

    this.onPrjClaimStageFilterChange = function (e) {
        refreshGrid('#prjClaimGrid');
    };

    this.onBtnPrjFaqClick = function (e) {
        window.open('/prj_faq.html', '_blank');
    };

    this.onBtnPrjClaimExcelClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Выгрузить список в Excel ?', function () {
            $('#prjClaimGrid').data('kendoGrid').saveAsExcel();
        });
    };

    this.getSettings = function () {
        var settings = [];
        //settings.push({ id: 'project', val: $("#prjClaimProjectFilter").data("kendoDropDownList").value() });
        settings.push({ id: 'prj', val: JSON.stringify($("#prjClaimPrjFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'version', val: $("#prjClaimVersionFilter").data("kendoDropDownList").value() });
        settings.push({ id: 'claim_stage', val: $("#prjClaimStageFilter").data("kendoDropDownList").value() });
        settings.push({ id: 'exec_user', val: $("#prjClaimExecUserFilter").data("kendoDropDownList").value() });                
        settings.push({ id: 'filter', val: $("#prjClaimFilter").data("kendoDropDownList").value() });                
        //
        settings.push({ id: 'claim_resp_user', val: JSON.stringify($("#prjClaimRespFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'claim_client', val: JSON.stringify($("#prjClaimClientFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'claim_priority', val: JSON.stringify($("#prjClaimPriorityFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'claim_crt_date1', val: $("#prjClaimCrtDate1").data("kendoDatePicker").value() });
        settings.push({ id: 'claim_crt_date2', val: $("#prjClaimCrtDate2").data("kendoDatePicker").value() });
        settings.push({ id: 'claim_claim_type', val: JSON.stringify($("#prjClaimClaimTypeFilter").data("kendoMultiSelect").value()) });
        // 
        $('#prjClaimToolbar>.k-button-group>.k-toggle-button').each(function (ind) {
            settings.push({ id: $(this).attr('data-setting-name'), val: $(this).hasClass('k-state-active') ? true : null });
        })
        //
        settings.push({ id: 'claim_splitter', val: $("#prjClaimSplitter").data("kendoSplitter").size("#clame-pane-top") });
        settings.push({ id: 'claim_splitter_child', val: $("#prjClaimSplitterBottom").data("kendoSplitter").size("#claim-pane-bottom-left") });
        //
        return settings;
    };

    this.onBtnPrjClaimActionsClick = function (e) {
        var config_action_type = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (config_action_type) {
            case 1:
                prjClaimAdd();
                break;
            case 2:
                var grid = $("#prjClaimGrid").data("kendoGrid");
                var selectedRow = grid.select();
                var selectedItem = grid.dataItem(selectedRow);
                if (selectedItem) {
                    showConfirmWindow('#confirm-templ', 'Переместить задачу ' + selectedItem.claim_num + ' в корзину ?', function () {
                        prjClaimDel(selectedRow, selectedItem.claim_id);
                    });
                }
                break;
            default:
                break;
        };
    };

    this.onBtnPrjTaskChildActionsClick = function (e) {
        $('#div-prj-user-select-comment').hide();
        $('#div-prj-user-select').hide();
        $('#div-prj-user-select-work-type').hide();
        $('#txtPrjUserSelectComment').val('');
        currTaskChildActionId = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (currTaskChildActionId) {
            case 1:
                $('#winPrjTaskAdd').data('kendoWindow').center().open();
                break;
            case 2:
                $('#div-prj-user-select').show();
                $('#div-prj-user-select-work-type').show();
                $('#winPrjUserSelect').data('kendoWindow').center().open();
                break;
            case 3:
                proceedTaskChildAction();
                break;
            case 4:
            case 5:
                $('#div-prj-user-select').show();
                $('#div-prj-user-select-work-type').show();
                $('#div-prj-user-select-comment').show();
                $('#winPrjUserSelect').data('kendoWindow').center().open();
                break;
            case 6:
            case 7:
                $('#div-prj-user-select-comment').show();
                $('#winPrjUserSelect').data('kendoWindow').center().open();
                break;
            default:
                break;
        }
    };

    this.onBtnPrjTaskChildWorkChildActionsClick = function (e) {
        $('#div-prj-user-select-comment').hide();
        $('#div-prj-user-select').hide();
        $('#div-prj-user-select-work-type').hide();
        $('#txtPrjUserSelectComment').val('');
        currWorkChildActionId = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (currWorkChildActionId) {
            case 1:
                currTaskChildActionId = 2;
                $('#div-prj-user-select').show();
                $('#div-prj-user-select-work-type').show();
                $('#winPrjUserSelect').data('kendoWindow').center().open();
                break;
            case 2:
                $('#winPrjWorkTypeChange').data('kendoWindow').center().open();
                break;
            case 3:
                // !!!
                $('#winPrjWorkUserChange').data('kendoWindow').center().open();
                break;
            default:
                break;
        }
    };

    this.onDdlPrjUserSelectSelect = function (e) {
        var curr_work_type_id = 1;
        var curr_role_id = e.sender.dataItem(e.item).crm_user_role_id;
        switch (curr_role_id) {
            case 1:
                curr_work_type_id = 3;
                break;
            case 2:
            case 3:
                curr_work_type_id = 4;
                break;
            default:
                curr_work_type_id = 1;
                break;
        };
        $('#ddlPrjUserSelectWorkType').data('kendoDropDownList').value(curr_work_type_id);
    };

    this.prjClaimGridMessCntClick = function (id) {
        prjClaim_clickedClaimId = id;
        prjClaim_clickedTaskId = null;
        $("#winPrjClaimMess").data("kendoWindow").center().open();
    };

    this.prjClaimGridTaskMessCntClick = function (id) {
        prjClaim_clickedClaimId = null;
        prjClaim_clickedTaskId = id;
        $("#winPrjClaimMess").data("kendoWindow").center().open();
    };

    this.getPrjClaimMessGridData = function (e) {
        return { claim_id: prjClaim_clickedClaimId, task_id: prjClaim_clickedTaskId };
    };

    this.onWinPrjClaimMessActivate = function (e) {
        refreshGrid('#prjClaimMessGrid');
    };

    this.onWinPrjUserSelectActivate = function (e) {
        if (currTaskChildActionId == 5) {
            var last_work = null;
            var curr_work_ds = $('#prjTaskChildWorkChildGrid').data('kendoGrid').dataSource;
            var curr_works = curr_work_ds.view();
            for (var i = 0; i < curr_works.length; i++) {
                var curr_work_dataItem = curr_work_ds.at(i);
                if ((curr_work_dataItem) && (curr_work_dataItem.state_type_id == 2)) {
                    last_work = curr_work_dataItem;
                };
            };
            if (last_work) {
                $("#ddlPrjUserSelect").data("kendoDropDownList").value(last_work.exec_user_id);
                $("#ddlPrjUserSelectWorkType").data("kendoDropDownList").value(last_work.work_type_id);
            };
        };
    };

    this.onPrjClaimFilterChange = function (e) {
        //alert('filter_id = ' + e.sender.value());
        var curr_dataItem = e.sender.dataItem();
        var curr_filter_id = curr_dataItem ? curr_dataItem.filter_id : 0;
        var curr_is_fixed = curr_dataItem && curr_filter_id ? curr_dataItem.is_fixed : true;
        if (curr_filter_id == -1) {
            prjClaimFilterEdit();
        } else  {
            // !!!
            if (curr_is_fixed) {
                refreshGrid('#prjClaimGrid');
            } else {
                var wnd = $('#winPrjClaimFilterParams').data('kendoWindow');
                wnd.refresh({
                    url: "/PrjClaimFilterParams",
                    data: { filter_id: curr_filter_id }
                }).center().open();
            };            
        };
    };

    this.onBtnPrjClaimFilterParamsSaveClick = function(e) {
        //$('#winPrjClaimFilterParams').data('kendoWindow').close();
                var curr_filter_id = $('#prjClaimFilter').data('kendoDropDownList').value();
                var curr_filter_items = [];
                var curr_filter_values = [];
                var curr_field_id = 0;
                var curr_field_name = null;
                var curr_field_id_value = null;
                var curr_field_text_value = null;
                var curr_data_control_type = 0;    
                $('.prj-claim-filter-items-layout div.prj-claim-filter-items').each(function(key, val) {
                    curr_field_id = parseInt($(val).attr('data-field-id'), 10);                    
                    curr_filter_values = [];
                    $(val).find('.prj-claim-filter-item').each(function(ind, item) {
                        curr_data_control_type = parseInt($(item).attr('data-control-type'), 10);
                        //curr_field_name = $(item).attr('id');
                        curr_field_name = $(item).attr('data-field-type');
                        switch (curr_data_control_type) {
                            case 1:                                
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).is(':checked');
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value});
                                break;
                            case 2:                                
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).data("kendoMultiSelect").value().join(';');
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            case 3:                                
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).val();
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            case 4:
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).data("kendoDatePicker").value();
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            case 5:
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).data("kendoNumericTextBox").value();                                
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            case 6:
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).data("kendoDropDownList").value();
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            default:
                                break;
                        }                        
                    });
                    curr_filter_items.push({field_id: curr_field_id, filter_values: curr_filter_values});
                });
                    
                $.ajax({
                    url: '/PrjClaimFilterItemsEdit',
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify({
                        filter_id: curr_filter_id,
                        filter_items: curr_filter_items
                    }),
                    success: function (res) {
                        if ((!res) || (res.err)) {                            
                            alert(res && res.mess ? res.mess : 'Ошибка при сохранении фильтра');
                        } else {
                            $('#winPrjClaimFilterParams').data('kendoWindow').close();
                            refreshGrid('#prjClaimGrid');
                        };
                    },
                    error: function (res) {                        
                        alert('Ошибка сохранения фильтра');
                    }
                });
    };

    this.onBtnPrjClaimFilterParamsCancelClick = function(e) {
        $('#winPrjClaimFilterParams').data('kendoWindow').close();
    };

    this.onWinPrjClaimFilterParamsDeactivate = function (e) {
        //this.destroy();
    };

    // private

    function resizePrjClaimGrid() {
        setGridMaxHeight_relative('#prjClaimGrid', '#clame-pane-top', $('#prjClaimToolbar').height() + 10);        
    };

    function resizePrjTaskChildGrid() {
        setGridMaxHeight_relative('#prjTaskChildGrid', '#clame-pane-bottom', $('#prjTaskChildToolbar').height() + 10);
    };

    function resizePrjTaskChildWorkChildGrid() {
        setGridMaxHeight_relative('#prjTaskChildWorkChildGrid', '#clame-pane-bottom', $('#prjTaskChildWorkChildToolbar').height() + 10);
    };

    function togglePrjClaimFilter() {
        var filter = $('#prjClaimFilterPanel').data('kendoPanelBar');
        var is_expanded = $('#prj-claim-filter-panel').hasClass('k-state-active');
        if (is_expanded) {
            filter.collapse($('#prj-claim-filter-panel'));
        } else {
            filter.expand($('#prj-claim-filter-panel'));
        };
    };

    function clearPrjClaimFilter(all) {
        var grid = $("#prjClaimGrid").data("kendoGrid")
        var ds = grid.dataSource;
        var currFilters = [];

        $('#prjClaimRespFilter').data('kendoMultiSelect').value([]);        
        $('#prjClaimClientFilter').data('kendoMultiSelect').value([]);
        $('#prjClaimPriorityFilter').data('kendoMultiSelect').value([]);
        $('#prjClaimClaimTypeFilter').data('kendoMultiSelect').value([]);        
        $('#prjClaimCrtDate1').data('kendoDatePicker').value(null);
        $('#prjClaimCrtDate2').data('kendoDatePicker').value(null);        
        $('#prj-claim-search').val('');
        $('#prjClaimPrjFilter').data('kendoMultiSelect').value([]);

        if (all) {            
            $('#prjClaimProjectFilter').data('kendoDropDownList').select(0);            
            $('#prjClaimVersionFilter').data('kendoDropDownList').select(0);
            $('#prjClaimStageFilter').data('kendoDropDownList').select(0);
        };

        ds.filter({ logic: "and", filters: currFilters });
    };

    function clearPrjClaimSort() {
        clearSortGrid("#prjClaimGrid");
    };

    function refreshPrjClaimGridFooter() {        
        /*
        var footer_html = 'Всего: ' + $('#prjClaimGrid').data('kendoGrid').dataSource.view().length;        
        */
        var footer = $('#prjClaimGrid>.k-grid-pager').find('span.prj-grid-footer');
        if (footer.length <= 0) {
            $('#prjClaimGrid span.k-pager-sizes').append('<span class="prj-grid-footer"></span>')
        };                
        var filterStr = "";
        var filterArray = [];
        var filter = $('#prjClaimGrid').data('kendoGrid').dataSource.filter();
        if (filter) {                        
            $.each(filter.filters, function(ind, val){
                if (val.filters) {
                    $.each(val.filters, function(ind1, val1){                
                        if (val1.filters) {
                            $.each(val1.filters, function(ind2, val2){
                                if (val2.filters) {
                                    $.each(val2.filters, function(ind3, val3){
                                        if (val3.field)
                                            filterArray.push(val3.field);
                                    });
                                } else {
                                    if (val2.field)
                                        filterArray.push(val2.field);
                                };
                            });
                        } else {
                            if (val1.field)
                                filterArray.push(val1.field);
                        };
                    });
                } else {
                    if (val.field)
                        filterArray.push(val.field);
                };
            });
            //
            var filterArrayUnique = [...new Set(filterArray.map(a => a))];
            var first = true;
            var columns = $('#prjClaimGrid').data('kendoGrid').columns;
            $.each(filterArrayUnique, function(ind, val) {
                if (!first) {
                    filterStr = filterStr + ", ";
                };
                var column = $.grep(columns, function(e){ return e.field == val; });
                if (column.length <= 0) {
                    column = $.grep(extFilterArray, function(e){ return e.field == val; });
                }
                if (column.length > 0) {
                    filterStr = filterStr + column[0].title;
                    first = false;
                }
            });
            if (filterStr)
                filterStr = "фильтры: " + filterStr;
        };
        $('#prjClaimGrid>.k-grid-pager span.prj-grid-footer').html(filterStr);
    };

    function prjClaimAdd() {
        var curr_project_id = null;
        var curr_project_dataItem = $('#prjClaimProjectFilter').data('kendoDropDownList').dataItem();
        if ((curr_project_dataItem) && (curr_project_dataItem.project_id > 0)) {
            curr_project_id = curr_project_dataItem.project_id;
        };
        if (!curr_project_id) {
            alert('Не выбран проект !');
            return false;
        };
        loadingDialog('#prj-loading', true);
        setTimeout(function () {
            rightPaneContentType = 2;
            leftPaneContentType = 2;
            leftPaneContentType_main = 3;
            leftPaneContentType_child = 4;
            prjList.openPrjClaimEditPartial(null, null, curr_project_id);
        }, waitTimeout);
    };

    function prjClaimDel(row, id) {
        $("#prjClaimGrid").data("kendoGrid").removeRow(row);
        $.ajax({
            url: '/PrjClaimDel',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ claim_id: id })
        });
    };

    function proceedTaskChildAction() {        
        var grid = $("#prjTaskChildGrid").data("kendoGrid");
        var curr_task_dataItem = grid.dataItem(grid.select());
        var curr_task_id = curr_task_dataItem ? curr_task_dataItem.task_id : null;
        if (!curr_task_id) {
            alert('Не выбрано задание');
            return;
        }
        var curr_user_dataItem = $('#ddlPrjUserSelect').data('kendoDropDownList').dataItem();
        var curr_user_id = curr_user_dataItem ? curr_user_dataItem.user_id : null;
        var curr_work_type_dataItem = $('#ddlPrjUserSelectWorkType').data('kendoDropDownList').dataItem();
        var curr_work_type_id = curr_work_type_dataItem ? curr_work_type_dataItem.work_type_id : null;
        var curr_comment = $('#txtPrjUserSelectComment').val();        
        //        
        switch (currTaskChildActionId) {
            case 2: // передать в работу                
                if (curr_user_id <= 0) {
                    alert('Не выбран пользователь');
                    return;
                };                
                break;
            case 4: // выполнить и передать далее
            case 5: // выполнить и вернуть
                if (curr_user_id <= 0) {
                    alert('Не выбран пользователь');
                    return;
                };
                break;
            default:
                break;
        }
        //
        loadingDialog('#prj-loading', true);
        //
        $.ajax({
            url: '/PrjTaskUserAction',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ task_id: curr_task_id, action_id: currTaskChildActionId, user_id: curr_user_id, work_type_id: curr_work_type_id, comment: curr_comment }),
            success: function (response) {
                if ((!response) || (response.err)) {
                    alert((response && response.err) ? response.mess : 'Ошибка выполнения действия');
                } else {
                    //
                };
            },
            error: function (response) {
                alert('Ошибка выполнения действия');
            },
            complete: function (response) {
                loadingDialog('#prj-loading', false);
                refreshGrid('#prjTaskChildGrid');
            }
        });
    };

    function addTask() {
        var grid = $("#prjClaimGrid").data("kendoGrid");
        var curr_claim_dataItem = grid.dataItem(grid.select());
        var curr_claim_id = curr_claim_dataItem ? curr_claim_dataItem.claim_id : null;
        if (!curr_claim_id) {
            alert('Не выбрана задача');
            return;
        }
        var curr_task_text = $('#txtTaskText').val();
        if (!curr_task_text) {
            alert('Не задан текст задания');
            return;
        };
        //
        loadingDialog('#prj-loading', true);
        //
        $.ajax({
            url: '/PrjTaskSimpleAdd',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ claim_id: curr_claim_id, task_text: curr_task_text }),
            success: function (response) {
                if ((!response) || (response.err)) {
                    alert((response && response.err) ? response.mess : 'Ошибка создания задания');
                } else {
                    //
                };
            },
            error: function (response) {
                alert('Ошибка создания задания');
            },
            complete: function (response) {
                loadingDialog('#prj-loading', false);
                refreshGrid('#prjTaskChildGrid');
            }
        });
    };

    function changeWorkType() {
        var grid = $("#prjTaskChildWorkChildGrid").data("kendoGrid");
        var curr_work_dataItem = grid.dataItem(grid.select());
        var curr_work_id = curr_work_dataItem ? curr_work_dataItem.work_id : null;
        if (!curr_work_id) {
            alert('Не выбрана работа');
            return;
        }
        var curr_work_type_id = $('#ddlPrjWorkTypeChange').data('kendoDropDownList').dataItem().work_type_id;
        if (!curr_work_type_id) {
            alert('Не задан тип работы');
            return;
        };
        //
        loadingDialog('#prj-loading', true);
        //
        $.ajax({
            url: '/PrjWorkTypeEdit',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ work_id: curr_work_id, work_type_id: curr_work_type_id }),
            success: function (response) {
                if ((!response) || (response.err)) {
                    alert((response && response.err) ? response.mess : 'Ошибка изменения работы');
                } else {
                    //
                };
            },
            error: function (response) {
                alert('Ошибка изменения работы');
            },
            complete: function (response) {
                loadingDialog('#prj-loading', false);
                refreshGrid('#prjTaskChildWorkChildGrid');
            }
        });
    };

    function changeWorkUser() {
        var grid = $("#prjTaskChildWorkChildGrid").data("kendoGrid");
        var curr_work_dataItem = grid.dataItem(grid.select());
        var curr_work_id = curr_work_dataItem ? curr_work_dataItem.work_id : null;
        if (!curr_work_id) {
            alert('Не выбрана работа');
            return;
        }
        var curr_work_exec_user_id = $('#ddlPrjWorkUserChange').data('kendoDropDownList').dataItem().user_id;
        if (!curr_work_exec_user_id) {
            alert('Не задан исполнитель работы');
            return;
        };
        //
        loadingDialog('#prj-loading', true);
        //
        $.ajax({
            url: '/PrjWorkUserEdit',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ work_id: curr_work_id, exec_user_id: curr_work_exec_user_id }),
            success: function (response) {
                if ((!response) || (response.err)) {
                    alert((response && response.err) ? response.mess : 'Ошибка изменения работы');
                } else {
                    //
                };
            },
            error: function (response) {
                alert('Ошибка изменения работы');
            },
            complete: function (response) {
                loadingDialog('#prj-loading', false);
                refreshGrid('#prjTaskChildWorkChildGrid');
            }
        });
    };

    function refreshPrjAndVersion() {
        var curr_project_id = null;
        var curr_project_dataItem = $('#prjClaimProjectFilter').data('kendoDropDownList').dataItem();
        if (curr_project_dataItem) {
            curr_project_id = curr_project_dataItem.project_id;
        }
        var filterVal = ((curr_project_id) && (curr_project_id > 0)) ? parseInt(curr_project_id) : null;
        //
        var ds1 = $('#prjClaimPrjFilter').data('kendoMultiSelect').dataSource;
        var ds2 = $('#prjClaimVersionFilter').data('kendoDropDownList').dataSource;
        var dsFilters = [{field: "project_id", operator: "eq", value: filterVal}, {field: "project_id", operator: "eq", value: null}];
        ds1.filter(
        {
            logic: 'or',
            filters: dsFilters
        });
        ds2.filter(
        {
            logic: 'or',
            filters: dsFilters
        });
    };

    function prjClaimFilterEdit(filter_id) {        
        loadingDialog('#prj-loading', true);
        setTimeout(function () {
            rightPaneContentType = 13;
            leftPaneContentType = 2;
            leftPaneContentType_main = 3;
            leftPaneContentType_child = 4;
            prjList.openPrjClaimFilterEditPartial(filter_id);
        }, waitTimeout);
    };

    function initPrjReportDialog() {
        $("#prj-report-dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 800,
            height: 500,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    };

    function showPrjReportDialog(text) {
        initPrjReportDialog();        
        //$('#prj-report-textarea').val(text);
        $('#prj-report-span').html(text);
        $('#prj-report-dialog').dialog('open');
    };

}

var prjClaimListPartial = null;
var prjClaimList_inited = false;
var prjClaimGridDataBound_first = true;
var prjClaimGridDataBinding_first = true;
var prjTaskChildGridDataBound_first = true;
var prjTaskChildWorkChildGridDataBound_first = true;
var prjClaimVersionFilterDataBound_first = true;
var prjClaim_selectedClaimId = null;
var prjClaim_selectedTaskId = null;
var prjClaim_clickedClaimId = null;
var prjClaim_clickedTaskId = null;
var prjClaim_setFilterParams = false;
var curr_user_settings_filter = null;
var extFilterArray = [{field:'resp_user_id',title:'Ответственный'}
                      , {field:'client_id',title:'Источник'}
                      , {field:'priority_id',title:'Приоритет'}
                      , {field:'claim_type_id',title:'Тип'}
                      ];
$(function () {
    prjClaimListPartial = new PrjClaimListPartial();
    prjClaimListPartial.init();
});
