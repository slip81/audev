﻿function PrjClaimFilterEditPartial() {
    _this = this;

    this.init = function () {
        //
    };

    this.loadData = function () {
        setTimeout(function () {
            loadingDialog('#prj-loading', false);
        }, 500);
    };

    this.onBtnPrjClaimFilterBackClick = function (e) {
        closePrjClaimFilterEdit();
    };

    this.onBtnPrjClaimFilterSaveClick = function (e) {
        savePrjClaimFilterEdit();
    };

    this.onBtnPrjClaimFilterCancelClick = function (e) {
        closePrjClaimFilterEdit();
    };

    this.onBtnPrjClaimFilterDeleteClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Удалить фильтр ?', function () {
            var curr_filter_id = $('#filter_id').val();
            loadingDialog('#prj-loading', true);
            $.ajax({
                url: '/PrjClaimFilterDel',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ filter_id: curr_filter_id }),
                success: function (res) {
                    if ((!res) || (res.err)) {
                        loadingDialog('#prj-loading', false);
                        alert(res && res.mess ? res.mess : 'Ошибка при удалении фильтра');
                    } else {                        
                        closePrjClaimFilterEdit();
                        //
                        var ds = $('#prjClaimFilter').data('kendoDropDownList').dataSource;
                        var filter_dataItems = $.grep(ds.data(), function (item) {
                            return item.filter_id == curr_filter_id;
                        });
                        var filter_dataItem = filter_dataItems.length > 0 ? filter_dataItems[0] : null;
		                if (filter_dataItem) {
			                ds.remove(filter_dataItem);                
		                };
                        //
                        loadingDialog('#prj-loading', false);
                    };
                },
                error: function (res) {
                    loadingDialog('#prj-loading', false);
                    alert('Ошибка удаления фильтра');
                }
            });
        });
    };

    this.onOpTypeForDatePickerChange = function (id1, id2) {        
        return function (e) {            
            var curr_value = e.sender.value();
            switch (curr_value) {
                case "7":
                case "8":                    
                    $('#' + id2).data('kendoDatePicker').enable(true);
                    break;
                default:                    
                    $('#' + id2).data('kendoDatePicker').enable(false);
                    break;
            }
        };
    };

    this.onOpTypeForNumericChange = function (id1, id2) {        
        return function (e) {            
            var curr_value = e.sender.value();
            switch (curr_value) {
                case "7":
                case "8":
                    $('#' + id2).data('kendoNumericTextBox').enable(true);
                    break;
                default:
                    $('#' + id2).data('kendoNumericTextBox').enable(false);
                    break;
            }
        };
    };

    // private

    function closePrjClaimFilterEdit() {
        toggleSplitter("#prjMainSplitter", "#prj-main-pane-left", "#prj-main-pane-right");
    };

    function savePrjClaimFilterEdit() {
                var curr_filter_id = $('#filter_id').val();
                var curr_filter_items = [];
                var curr_filter_values = [];
                var curr_field_id = 0;
                var curr_field_name = null;
                var curr_field_id_value = null;
                var curr_field_text_value = null;
                var curr_data_control_type = 0;    
                $('.prj-claim-filter-layout div.prj-claim-filter-items').each(function(key, val) {
                    curr_field_id = parseInt($(val).attr('data-field-id'), 10);                    
                    curr_filter_values = [];
                    $(val).find('.prj-claim-filter-item').each(function(ind, item) {
                        curr_data_control_type = parseInt($(item).attr('data-control-type'), 10);
                        //curr_field_name = $(item).attr('id');
                        curr_field_name = $(item).attr('data-field-type');
                        switch (curr_data_control_type) {
                            case 1:                                
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).is(':checked');
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value});
                                break;
                            case 2:                                
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).data("kendoMultiSelect").value().join(';');
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            case 3:                                
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).val();
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            case 4:
                                if ($(item).is(':disabled'))
                                    break;
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).data("kendoDatePicker").value();
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            case 5:
                                if ($(item).is(':disabled'))
                                    break;
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).data("kendoNumericTextBox").value();                                
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            case 6:
                                curr_field_id_value = null;
                                curr_field_text_value = $(item).data("kendoDropDownList").value();
                                curr_filter_values.push({ field_name: curr_field_name, field_id_value: curr_field_id_value, field_text_value: curr_field_text_value });
                                break;
                            default:
                                break;
                        }                        
                    });
                    curr_filter_items.push({field_id: curr_field_id, filter_values: curr_filter_values});
                });
    
                loadingDialog('#prj-loading', true);
                $.ajax({
                    url: '/PrjClaimFilterEdit',
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify({
                        filter_id: curr_filter_id,
                        filter_name: $('#filter_name').val(),
                        is_active: $('#is_active').is(':checked'),
                        is_global: $('#is_global').is(':checked'),
                        filter_items: curr_filter_items
                    }),
                    complete: function(res) {                        
                        setTimeout(function () {
                            loadingDialog('#prj-loading', false);
                        }, 500);
                    },
                    success: function (res) {
                        if ((!res) || (res.err)) {                            
                            alert(res && res.mess ? res.mess : 'Ошибка при сохранении фильтра');
                        } else {
                            updatePrjClaimToolbar(res.filter_id, res.filter_name, res.user_id, res.is_active, res.is_fixed);
                            toggleSplitter("#prjMainSplitter", "#prj-main-pane-left", "#prj-main-pane-right");
                        };
                    },
                    error: function (res) {                        
                        alert('Ошибка сохранения фильтра');
                    }
                });
    };

    function updatePrjClaimToolbar(new_filter_id, new_filter_name, new_user_id, new_is_active, new_is_fixed) {
        var ds = $('#prjClaimFilter').data('kendoDropDownList').dataSource;
        var filter_dataItems = $.grep(ds.data(), function (item) {
            return item.filter_id == new_filter_id;
        });
        var filter_dataItem = filter_dataItems.length > 0 ? filter_dataItems[0] : null;
        if (new_is_active) {
            if (!filter_dataItem) {
                ds.insert(ds.data().length - 1, { filter_id: new_filter_id, filter_name: new_filter_name, user_id: new_user_id, is_active: new_is_active, is_fixed: new_is_fixed });
            } else {
                filter_dataItem.set('filter_name', new_filter_name);
                filter_dataItem.set('user_id', new_user_id);
                filter_dataItem.set('is_fixed', new_is_fixed);
            };
        } else {            
            if (filter_dataItem) {
                ds.remove(filter_dataItem);                
            };
        };
    };
}

var prjClaimFilterEditPartial = null;

$(function () {
    prjClaimFilterEditPartial = new PrjClaimFilterEditPartial();
    prjClaimFilterEditPartial.init();
});

