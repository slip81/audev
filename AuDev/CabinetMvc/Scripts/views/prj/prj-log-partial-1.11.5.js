﻿function PrjLogPartial() {
    _this = this;

    this.init = function () {
        //
    }

    this.init_after = function () {
        if (prjLog_inited) {
            return;
        }
        //
        prjLog_inited = true;
        //
        $("#prjLogGrid").delegate("tbody>tr", "dblclick", function () {          
            var dataItem = $("#prjLogGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {
                //if (dataItem.claim_is_archive) {
                if (false) {
                    showConfirmWindow('#confirm-templ', 'Задача в корзине. Восстановить ?', function () {
                        loadingDialog('#prj-loading', true);
                        $.ajax({
                            url: '/PrjClaimRestore',
                            type: "POST",
                            contentType: 'application/json; charset=windows-1251',
                            data: JSON.stringify({ claim_id: dataItem.claim_id }),
                            success: function (res) {
                                if ((!res) || (res.err)) {
                                    loadingDialog('#prj-loading', false);
                                    alert(res && res.mess ? res.mess : 'Ошибка при восстановлении задачи');
                                } else {
                                    setTimeout(function () {
                                        rightPaneContentType = 2;
                                        leftPaneContentType = 7;
                                        leftPaneContentType_main = null;
                                        leftPaneContentType_child = null;
                                        prjList.openPrjClaimEditPartial(dataItem.claim_id);
                                    }, waitTimeout);
                                }
                            },
                            error: function (res) {
                                loadingDialog('#prj-loading', false);
                                alert('Ошибка восстановления задачи');
                            }
                        });
                    });
                //} else if (dataItem.prj_is_archive) {
                } else if (false) {
                    showConfirmWindow('#confirm-templ', 'Группа в корзине. Восстановить ?', function () {
                        loadingDialog('#prj-loading', true);
                        $.ajax({
                            url: '/PrjRestore',
                            type: "POST",
                            contentType: 'application/json; charset=windows-1251',
                            data: JSON.stringify({ prj_id: dataItem.prj_id }),
                            success: function (res) {
                                if ((!res) || (res.err)) {
                                    loadingDialog('#prj-loading', false);
                                    alert(res && res.mess ? res.mess : 'Ошибка при восстановлении группы');
                                } else {
                                    setTimeout(function () {
                                        rightPaneContentType = 1;
                                        leftPaneContentType = 7;
                                        leftPaneContentType_main = null;
                                        leftPaneContentType_child = null;
                                        prjList.openPrjEditPartial(dataItem.prj_id);
                                    }, waitTimeout);
                                }
                            },
                            error: function (res) {
                                loadingDialog('#prj-loading', false);
                                alert('Ошибка восстановления группы');
                            }
                        });
                    });
                } else {
                    if (dataItem.claim_id) {
                        loadingDialog('#prj-loading', true);
                        setTimeout(function () {
                            rightPaneContentType = 2;
                            leftPaneContentType = 7;
                            leftPaneContentType_main = null;
                            leftPaneContentType_child = null;
                            prjList.openPrjClaimEditPartial(dataItem.claim_id);
                        }, waitTimeout);
                    } else if (dataItem.prj_id) {
                        loadingDialog('#prj-loading', true);
                        setTimeout(function () {
                            rightPaneContentType = 1;
                            leftPaneContentType = 7;
                            leftPaneContentType_main = null;
                            leftPaneContentType_child = null;
                            prjList.openPrjEditPartial(dataItem.prj_id);
                        }, waitTimeout);
                    }
                };
            };            
        });
    };

    this.onPrjLogGridDataBound = function (e) {
        drawEmptyRow('#prjLogGrid');
        if (prjLogGridDataBound_first) {
            prjLogGridDataBound_first = false;
            setGridMaxHeight('#prjLogGrid', 45);
        };
    };

    this.getPrjLogGridData = function (e) {
        return { show_deleted: prjLog_show_deleted, search: $('#prj-log-search').val(), };
    };

    this.onPrjLogSearchKeyup = function (e) {
        if (e.keyCode == 13) {
            refreshGrid('#prjLogGrid');
        };
    };
}

var prjLogPartial = null;
var prjLog_inited = false;
var prjLogGridDataBound_first = true;
var prjLog_show_deleted = false;

$(function () {
    prjLogPartial = new PrjLogPartial();
    prjLogPartial.init();
});
