﻿function AsnaStockList() {
    _this = this;

    this.init = function () {        
        setGridMaxHeight('#asnaStockGrid', 50);
    };

    this.onAsnaUserClientDdlChange = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserClientDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserSalesDdlChange = function (e) {
        var asnaStockChainDate = $('#asnaStockChainDate').data('kendoDatePicker');
        refreshAsnaStockChainDatePicker(!asnaStockChainDate);
        //
        $('#asnaStockChainDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserSalesDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').select(0);
        //
        var asnaStockChainDate = $('#asnaStockChainDate').data('kendoDatePicker');
        refreshAsnaStockChainDatePicker(!asnaStockChainDate);
        //
        $('#asnaStockChainDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaStockChainDateChange = function (e) {
        //alert('onAsnaStockChainDateChange !');
        $('#asnaStockChainDdl').data('kendoDropDownList').dataSource.read();
    };

    this.getAsnaUserSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#asnaUserClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    };

    this.getAsnaStockChainDdlData = function (e) {
        var curr_sales_id = null;
        var curr_chain_date = null;
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }        
        curr_chain_date = kendo.toString($('#asnaStockChainDate').data('kendoDatePicker').value(), "dd.MM.yyyy")
        return { sales_id: curr_sales_id, chain_date: curr_chain_date};
    };

    this.onAsnaStockChainDdlChange = function (e) {        
        refreshGrid('#asnaStockGrid');
    };

    this.onAsnaStockChainDdlDataBound = function (e) {
        $('#asnaStockChainDdl').data('kendoDropDownList').select(0);
        refreshGrid('#asnaStockGrid');
    };

    this.getAsnaStockGridData = function (e) {
        var curr_chain_id = null;
        var dataItem = $('#asnaStockChainDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.chain_id)) {
            curr_chain_id = dataItem.chain_id;
        }
        return { chain_id: curr_chain_id };
    };

    this.onAsnaStockGridDataBound = function (e) {
        drawEmptyRow('#asnaStockGrid');
        //
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if ((dataItem) && (!dataItem.is_linked)) {
                row.addClass("k-error-colored");
            };
        };
    };

    this.isNotAllowedDate = function (d) {
        //return false;
        //var dateActual = new Date(d);
        var dateActual = d;
        //alert('dateActual = ' + dateActual + ', allowedDates[0] = ' + allowedDates[0]);
        if ($.inArray(dateActual, allowedDates) != -1)
            return false;
        else
            return true;
    };

    this.onBtnAsnaStockGridExcelClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Выгрузить список в Excel ?', function () {
            $('#asnaStockGrid').data('kendoGrid').saveAsExcel();
        });
    };

    // private

    function getAllowedDates() {
        var curr_sales_id = null;
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        $.ajax({
            url: '/GetAsnaStockChainDates',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            async: false,
            data: JSON.stringify({ sales_id: curr_sales_id }),
            success: function (response) {
                allowedDates = [];
                if (!response.dates) {
                    //alert('Ошибка получения дат');
                    return false;
                };                
                $.each(response.dates, function (index, value) {
                    var valueD = new Date(parseInt(value.replace("/Date(", "").replace(")/", ""), 10));
                    allowedDates.push(valueD.getTime());                    
                });
                return true;
            },
            error: function (response) {
                alert('Ошибка получения дат');
                return false;
            }
        });
    }

    function refreshAsnaStockChainDatePicker(create) {
        if (!create) {
            $("#asnaStockChainDate").data('kendoDatePicker').enable(false);
        };
        //
        getAllowedDates();
        //
        if (create) {
            $("#asnaStockChainDate").kendoDatePicker({
                value: new Date(),
                month: {
                    content: '# if (asnaStockList.isNotAllowedDate(+data.date)) { #' +
                         '<span class="text-muted">#= data.value #</span>' +
                         '# } else { #' +
                         '<span class="text-success"><strong>#= data.value #</strong></span>' +
                         '# } #'
                },
                change: _this.onAsnaStockChainDateChange
            });
        } else {
            $("#asnaStockChainDate").data('kendoDatePicker').enable(true);
        }
    }
}

var asnaStockList = null;
$(function () {
    asnaStockList = new AsnaStockList();
    asnaStockList.init();
});
