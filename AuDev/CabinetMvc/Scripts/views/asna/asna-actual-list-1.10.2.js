﻿function AsnaActualList() {
    _this = this;

    this.init = function () {        
        setGridMaxHeight('#asnaActualGrid', 50);
    };

    this.onAsnaUserClientDdlChange = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserClientDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserSalesDdlChange = function (e) {
        refreshGrid('#asnaActualGrid');
    };

    this.onAsnaUserSalesDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').select(0);
        //
        refreshGrid('#asnaActualGrid');
    };

    this.getAsnaUserSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#asnaUserClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    };

    this.getAsnaActualGridData = function (e) {
        var curr_sales_id = null;
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        return { sales_id: curr_sales_id };
    };

    this.onAsnaActualGridDataBound = function (e) {
        drawEmptyRow('#asnaActualGrid');
        //
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if ((dataItem) && (!dataItem.is_linked)) {
                row.addClass("k-error-colored");
            };
        };
    };

    this.onBtnAsnaActualGridExcelClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Выгрузить список в Excel ?', function () {
            $('#asnaActualGrid').data('kendoGrid').saveAsExcel();
        });
    };
}

var asnaActualList = null;
$(function () {
    asnaActualList = new AsnaActualList();
    asnaActualList.init();
});
