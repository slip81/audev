﻿function AsnaUserList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#asnaUserGrid', 50);
    };

    this.onAsnaUserClientDdlChange = function (e) {
        refreshGrid('#asnaUserGrid');
    };

    this.onAsnaUserClientDdlDataBound = function (e) {
        refreshGrid('#asnaUserGrid');
    };

    this.onBtnAsnaClientAddClick = function (e) {
        location.href = '/AsnaUserEdit?asna_user_id=0';
    };

    this.getAsnaUserGridData = function (e) {
        var curr_client_id = null;        
        var dataItem = $('#asnaUserClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    };

    this.onAsnaUserGridDataBound = function (e) {
        drawEmptyRow('#asnaUserGrid');
    };

    this.onAsnaUserGridError = function (e) {
        if (e.errors) {
            var message = "Ошибка:\n";
            $.each(e.errors, function (key, value) {
                if ('errors' in value) {
                    $.each(value.errors, function () {
                        message += this + "\n";
                    });
                }
            });
            alert(message);
            $("#asnaUserGrid").data('kendoGrid').cancelChanges();            
        }
    };
}

var asnaUserList = null;
$(function () {
    asnaUserList = new AsnaUserList();
    asnaUserList.init();
});
