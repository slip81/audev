﻿function StockList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#stockGrid', 50);
    };

    this.getStockGridData = function (e) {
        var curr_client_id = null;
        var curr_sales_id = null;
        var dataItem = null;
        dataItem = $('#stockClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        dataItem = $('#stockSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        return { client_id: curr_client_id, sales_id: curr_sales_id, search: $('#search-stock').val() };
    };

    this.onStockGridDataBound = function (e) {
        drawEmptyRow('#stockGrid')
    };

    this.onStockClientDdlDataBound = function (e) {
        $('#stockSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onStockClientDdlChange = function (e) {
        $('#stockSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onStockSalesDdlChange = function (e) {
        refreshGrid('#stockGrid');
    };

    this.onStockSalesDdlDataBound = function (e) {
        refreshGrid('#stockGrid');
    };

    this.getStockSalesDdlData = function (e) {
        var curr_client_id = null;        
        var dataItem = $('#stockClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    };

    this.onStockGridExcelClick = function (e) {
        $('#stockGrid').data('kendoGrid').saveAsExcel();
    };

    this.onBtnStockClientAddClick = function (e) {
        location.href = '/ClientList';
    };

    this.onSearchStockKeyup = function (e) {
        if (e.keyCode == 13) {
            refreshGrid('#stockGrid');
        };
    };
}

var stockList = null;

$(function () {
    stockList = new StockList();
    stockList.init();
});
