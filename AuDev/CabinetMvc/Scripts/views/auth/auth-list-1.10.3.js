﻿function AuthList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#authGrid', 0);
        //
        $('#btnAddPers').children('span').first().addClass('k-icon k-i-plus');
        $('#btnAddRole').children('span').first().addClass('k-icon k-i-plus');
        //
        $('#btnShowAll').click(function (e) {
            $('#btnShowAll').addClass('btn-primary');
            $('#btnShowActive').removeClass('btn-primary');
            $('#btnShowNotActive').removeClass('btn-primary');

            var grid = $("#authGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            grid.dataSource.filter(_fltStatus);
        });

        $('#btnShowActive').click(function (e) {
            $('#btnShowAll').removeClass('btn-primary');
            $('#btnShowActive').addClass('btn-primary');
            $('#btnShowNotActive').removeClass('btn-primary');

            var grid = $("#authGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            _fltStatus.filters.push({ field: "is_active", operator: "eq", value: true });
            grid.dataSource.filter(_fltStatus);
        });

        $('#btnShowNotActive').click(function (e) {
            $('#btnShowAll').removeClass('btn-primary');
            $('#btnShowActive').removeClass('btn-primary');
            $('#btnShowNotActive').addClass('btn-primary');

            var grid = $("#authGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            _fltStatus.filters.push({ field: "is_active", operator: "eq", value: false });
            grid.dataSource.filter(_fltStatus);
        });
    };

    this.onAuthGridDataBound = function(e) {
        drawEmptyRow('#authGrid');
        //
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if (dataItem.get("is_active") != true) {
                row.addClass("k-error-colored");
            };
        };
        //
        $(".templateCell").each(function () {
            eval($(this).children("script").last().html());
        });
        //
        $('.templateCell span.k-link').children('span.k-icon').removeClass('k-i-arrow-s');
        $('.templateCell span.k-link').children('span.k-icon').addClass('k-i-custom');
    };
}

var authList = null;

$(function () {
    authList = new AuthList();
    authList.init();
});
