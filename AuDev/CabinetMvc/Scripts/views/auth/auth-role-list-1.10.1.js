﻿function AuthRoleList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#authRoleGrid', 0);
    };
    
    this.onAuthRoleGridDataBound = function(e) {
        drawEmptyRow('#authRoleGrid');
        if (firstDataBound) {
            firstDataBound = false;
            var isAdd = request_add;
            if (isAdd == '1') {                
                $('.k-grid-add').trigger('click');
            }
        }
    };

    this.onAuthRoleGridRequestEnd = function(e) {
        if ((e.type == "create") || (e.type == "update")) {
            refreshGrid("#authRoleGrid");
        };
    };
}

var authRoleList = null;

var firstDataBound = true;

$(function () {
    authRoleList = new AuthRoleList();
    authRoleList.init();
});
