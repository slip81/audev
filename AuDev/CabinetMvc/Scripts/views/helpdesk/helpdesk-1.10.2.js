﻿function HelpDesk() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#storyListGrid', 0);
    };

    this.btnAcceptStoryClick = function(e) {
        e.preventDefault();

        var row = $(e.target).closest("tr");
        var grid = $("#storyListGrid").data("kendoGrid");
        var dataItem = grid.dataItem(row);

        var acceptStoryWindow = $("<div />").kendoWindow({
            actions: ['Close'],
            title: "Введите номер задачи",
            draggable: true,
            resizable: true,
            modal: true,
            iframe: false,
            activate: function () {
                $('#txtCrmNum').select();
            },
            deactivate: function () {
                this.destroy();
            }
        });

        if (dataItem.curr_state_id === 1) {

            acceptStoryWindow.data("kendoWindow").content($("#accept-story-confirmation").html());
            acceptStoryWindow.data("kendoWindow").center().open();
            acceptStoryWindow.find(".confirm,.cancel")
                    .click(function () {
                        if ($(this).hasClass("confirm")) {
                            var crm_num_val = acceptStoryWindow.find("#txtCrmNum").val();
                            var to_task_chb = acceptStoryWindow.find("#chb-story-to-task");
                            var to_task_val = $(to_task_chb).is(':checked');
                            if (crm_num_val) {
                                acceptStoryWindow.data("kendoWindow").close();
                                var sendData = { story_id: dataItem.story_id, crm_num: crm_num_val, toTask: to_task_val };
                                $.ajax({
                                    url: 'HelpDesk/StoryEdit',
                                    data: JSON.stringify(sendData),
                                    type: "POST",
                                    contentType: 'application/json; charset=windows-1251',
                                    success: function (response) {
                                        refreshGrid('#storyListGrid');
                                    },
                                    error: function (response) {
                                        alert('Ошибка при попытке принять вопрос');
                                    }
                                });

                                acceptStoryWindow.data("kendoWindow").close();
                            }
                        }
                        else {
                            acceptStoryWindow.data("kendoWindow").close();
                        }
                    })
                 .end();
        }
    };

    this.onStoryListGridDataBound = function() {
        $("#storyListGrid tbody tr .k-grid-accept-story-btn").each(function () {
            var currentDataItem = $("#storyListGrid").data("kendoGrid").dataItem($(this).closest("tr"));

            if (currentDataItem.is_answered == true) {
                $(this).remove();
            }
        });
        drawEmptyRow('#storyListGrid');
    };

    this.btnShowAllClick = function(e) {
        $('#btnShowAll').addClass('k-primary');
        $('#btnShowNotAnswered').removeClass('k-primary');
        $('#btnShowAnswered').removeClass('k-primary');

        var grid = $("#storyListGrid").data("kendoGrid"),
            _fltStatus = { logic: "and", filters: [] };

        grid.dataSource.filter(_fltStatus);
    };

    this.btnShowNotAnsweredClick = function(e) {
        $('#btnShowAll').removeClass('k-primary');
        $('#btnShowNotAnswered').addClass('k-primary');
        $('#btnShowAnswered').removeClass('k-primary');

        var grid = $("#storyListGrid").data("kendoGrid"),
            _fltStatus = { logic: "and", filters: [] };

        _fltStatus.filters.push({ field: "is_answered", operator: "eq", value: "false" });
        grid.dataSource.filter(_fltStatus);
    };

    this.btnShowAnsweredClick = function(e) {
        $('#btnShowAll').removeClass('k-primary');
        $('#btnShowNotAnswered').removeClass('k-primary');
        $('#btnShowAnswered').addClass('k-primary');

        var grid = $("#storyListGrid").data("kendoGrid"),
            _fltStatus = { logic: "and", filters: [] };

        _fltStatus.filters.push({ field: "is_answered", operator: "eq", value: "true" });
        grid.dataSource.filter(_fltStatus);
    };

    this.btnRefreshClick = function(e) {
        refreshDate();
        _this.btnShowNotAnsweredClick(e);
    };

    function addZeroIfNeeded(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    };

    function refreshDate() {
        var today = new Date();
        var y = today.getFullYear();
        var mo = today.getMonth() + 1;
        var d = today.getDate();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        mo = addZeroIfNeeded(mo);
        m = addZeroIfNeeded(m);
        s = addZeroIfNeeded(s);
        document.getElementById('lblRefresh').innerHTML = d + "." + mo + "." + y + " " + h + ":" + m + ":" + s;
    };
}

var helpDesk = null;

$(function () {
    helpDesk = new HelpDesk();
    helpDesk.init();
});
