﻿function Planner() {
    _this = this;

    this.init = function () {
        $(document).kendoTooltip({
            filter: ".k-event:not(.k-event-drag-hint) > div, .k-task",
            position: "top",
            width: 250,
            content: kendo.template($('#planner-event-tooltip-template').html())
        });
        //
        $(document).on('click', '#chbShowPublic', function () {
            refreshScheduler();
        });
        //
        $(document).on('click', '.btn-del-note', function () {
            //alert('btn-del-note clicked !');
            if (!(confirm('Удалить запись ?'))) {
                return;
            };
            var grid = $('#plannerNoteGrid').data('kendoGrid');
            var dataItem = grid.dataItem($(this).closest('tr'))
            if (dataItem) {
                grid.dataSource.remove(dataItem);
                $.ajax({
                    url: '/PlannerNoteDel',
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ note_id: dataItem.note_id }),
                    success: function (res) {
                        if ((!res) || (res.err)) {                            
                            alert(((res) && (res.mess)) ? res.mess : 'Ошибка при удалении записи ! Пожалуйста обновите страницу');
                        } else {
                            //
                        };
                    },
                    error: function (res) {
                        alert(((res) && (res.mess)) ? res.mess : 'Ошибка при удалении записи ! Пожалуйста обновите страницу');
                    }
                });
            }
        });
        //        
        $(document).on('click', '.btn-cancel-note-change', function (e) {            
            var grid = $('#plannerNoteGrid').data('kendoGrid');            
            grid.closeCell(true);
        });
        //
        $(document).on('click', '#btnTaskListWindowOk', function (e) {
            var id_list = '';
            $.each(checkedIds, function (key, value) {
                if (value) {
                    id_list = id_list + key + ',';
                }
            });
            //
            $("#winTaskList").data("kendoWindow").close();
            //
            $.ajax({
                url: '/PlannerNoteAddTasks',
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ task_id_list: id_list, curr_user_id: currUser_CabUserId }),
                success: function (res) {
                    checkedIds = {};
                    if ((!res) || (res.err)) {
                        alert(((res) && (res.mess)) ? res.mess : 'Ошибка при добавлении задач ! Пожалуйста обновите страницу');
                    } else {
                        refreshNotesGrid();
                    };
                },
                error: function (res) {
                    checkedIds = {};
                    alert(((res) && (res.mess)) ? res.mess : 'Ошибка при добавлении задач ! Пожалуйста обновите страницу');
                }
            });
        });
        //
        $(document).on('click', '#btnTaskListWindowCancel', function (e) {
            $("#winTaskList").data("kendoWindow").close();
        });
    };

    // task-edit-planner.js

    this.init_popup = function () {

        $('.attr-change').bind('input propertychange', _this.onAttrChanged);

        $('.subtask-addPlanner').click(function (e) {
            onSubtaskAddClick(e);
        });

        $('.task-note-addPlanner').click(function (e) {
            onTaskNoteAddClick(e);
        });

        $('.subtask-excelPlanner').click(function (e) {
            onSubtaskExcelClick(e);
        });

        $('.subtask-pdfPlanner').click(function (e) {
            onSubtaskPdfClick(e);
        });

        $('#btnTaskOkPlanner').click(function (e) {
            onBtnTaskOkClick(e);
        });

        $('#btnTaskSavePlanner').click(function (e) {
            onBtnTaskSaveClick(e);
        });

        $('#btnTaskCancelPlanner').click(function (e) {
            onBtnTaskCancelClick(e);
        });

        $('.attr-change').click(function (e) {
            _this.onAttrChanged();            
        });

        $('div#div-subtaskPlanner ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
            $('#div-commentPlanner').toggleClass('hidden');
            $('#div-subtaskPlanner').toggleClass('col-sm-6');
            $('#div-subtaskPlanner').toggleClass('col-sm-12');
        });

        $('div#div-commentPlanner ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
            $('#div-subtaskPlanner').toggleClass('hidden');
            $('#div-commentPlanner').toggleClass('col-sm-6');
            $('#div-commentPlanner').toggleClass('col-sm-12');
            if ($('#div-subtaskPlanner').hasClass('hidden')) {
                $('#div-commentPlanner').css('margin-left', '10px');
            } else {
                $('#div-commentPlanner').css('margin-left', '-10px');
            };
        });
    };

    //planner.js

    this.getCurrUserId = function (e) {
        return { curr_user_id: currUser_CabUserId };
    };

    this.onUserChange = function (e) {        
        currUser_CabUserId = this.value();        
        refreshScheduler();
        refreshNotesGrid();
        var rightPanelIndex = $('#tabPlanerPanel').data('kendoTabStrip').select().index();        
        if (rightPanelIndex == 0) {
            refreshUpcomingEventGrid();
        } else {            
            refreshOverdueEventGrid();
        };
    };

    this.getEventFilter = function (e) {
        var scheduler = $('#scheduler').getKendoScheduler();
        return { date_beg: scheduler.view().startDate(), date_end: scheduler.view().endDate(), exec_user_id: currUser_CabUserId, showPublic: $('#chbShowPublic').is(':checked') };
    };

    this.onBtnAddNoteClick = function (e) {        
        $('#plannerNoteGrid .k-grid-add').trigger('click');
    };

    this.onBtnAddTaskClick = function (e) {
        //alert('onBtnAddTaskClick !');
        $("#winTaskList").data("kendoWindow").open().center();
    };

    this.onSchedulerEdit = function (e) {        
        curr_event = e.event;
        curr_task_id = e.event.task_id;
        
        if (curr_task_id) {
            e.preventDefault();
            openPlannerTask();
            return false;
        };

        var sliderLabels = [];
        sliderLabels[0] = 'О';
        sliderLabels[1] = 'П';
        sliderLabels[2] = 'В';
        sliderLabels[3] = 'М';
        var sliderTitles = [];
        sliderTitles[0] = 'Обычный';
        sliderTitles[1] = 'Повышенный';
        sliderTitles[2] = 'Высокий';
        sliderTitles[3] = 'Максимальный';

        //var slider = $('#event_priority_id').data("kendoSlider");
        //var sliderItems = slider.siblings(".k-slider-items");
        var sliderItems = $('#event_priority_id').siblings(".k-slider-items");
        $.each(sliderLabels, function (index, value) {
            var item = sliderItems.find("li:eq(" + (index) + ")");
            //item.attr("title", value);
            item.attr("title", sliderTitles[index]);
            item.find("span").text(value);
        });
    };

    this.onSchedulerSave = function (e) {
        var note_id = e.event.SourceNoteId;
        var upcoming_event_id = e.event.SourceUpcomingEventId;
        if (note_id) {
            var grid = $("#plannerNoteGrid").data("kendoGrid");
            var dataItem = grid.dataSource.get(note_id);
            if (dataItem) {
                grid.dataSource.remove(dataItem);
            };
        } else if (upcoming_event_id) {
            var grid = $("#plannerUpcomingEventGrid").data("kendoGrid");
            var dataItem = grid.dataSource.get(upcoming_event_id);
            if (dataItem) {
                grid.dataSource.remove(dataItem);
            };
        };
        var rightPanelIndex = $('#tabPlanerPanel').data('kendoTabStrip').select().index();
        setTimeout(function () {
            if (rightPanelIndex == 0) {
                refreshUpcomingEventGrid();
            } else {
                refreshOverdueEventGrid();
            };
        }, 1500);
    };

    this.getEventClass = function (data) {
        var priority = data.event_priority_id;
        return 'event-priority-' + priority;
    };

    this.onPlannerToolBarToggle = function (e) {
        refreshScheduler();
    };

    this.getUpcomingEventFilter = function (e) {
        var days = 1;
        var filter_id = $('#tbUpcomingPlanner .k-button-group>.k-toggle-button.k-state-active').attr('id');
        if (filter_id == 'btn-upcoming-event-filter-tomorrow') {
            days = 1;
        } else if (filter_id == 'btn-upcoming-event-filter-3days') {
            days = 3;
        } else if (filter_id == 'btn-upcoming-event-filter-7days') {
            days = 7;
        };
        return { days_forward: days, exec_user_id: currUser_CabUserId };
    };

    this.getOverdueEventFilter = function (e) {
        return { exec_user_id: currUser_CabUserId };
    }

    this.getPlanerNoteFilter = function (e) {
        return { exec_user_id: currUser_CabUserId };
    };

    this.onPlannerUpcomingEventGridDataBound = function (e) {
        if (firstDataBound_upcoming) {
            firstDataBound_upcoming = false;
            createPlannerUpcomingDraggable();
            createUpcomingDropArea(this);
        }        
        drawEmptyRow('#plannerUpcomingEventGrid');
    };

    this.onPlannerNoteGridDataBound = function (e) {        
        //
        if (firstDataBound_note) {
            firstDataBound_note = false;            
            createPlannerNoteDraggable();
            createNotesDropArea(this);
        }    
        drawEmptyRow('#plannerNoteGrid');
        //        
        var rowColorClasses = ['note-danger-color', 'note-warning-color', 'note-info-color', 'note-success-color'];
        var curr_step = 1;
        var grid = $("#plannerNoteGrid").data("kendoGrid");
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            if (curr_step <= 3) {
                row.addClass(rowColorClasses[0])
            } else if (curr_step <= 6) {
                row.addClass(rowColorClasses[1])
            } else if (curr_step <= 9) {
                row.addClass(rowColorClasses[2])
            } else if (curr_step <= 12) {
                row.addClass(rowColorClasses[3])
            };
            curr_step = curr_step + 1;
        };
    };

    this.onPlannerOverdueEventGridDataBound = function (e) {
        drawEmptyRow('#plannerOverdueEventGrid');
    };

    this.onPlannerNoteGridSave = function (e) {        
        if (e.values.description !== "") {
            setTimeout(function () {
                $('#plannerNoteGrid').data('kendoGrid').dataSource.sync()
            });
        } else {            
            e.preventDefault();
        }
    };

    this.onBtnNotesRefreshClick = function (e) {        
        refreshNotesGrid();
    }

    this.placeholder = function (element) {        
         return element.clone().addClass("k-state-hover").css("opacity", 0.65);
    };

    this.onPlannerNoteGridSortChange = function (e) {
        var grid = $("#plannerNoteGrid").data("kendoGrid"),
                    skip = grid.dataSource.skip(),
                    oldIndex = e.oldIndex + skip,
                    newIndex = e.newIndex + skip,
                    data = grid.dataSource.data(),
                    dataItem = grid.dataSource.getByUid(e.item.data("uid"));

        grid.dataSource.remove(dataItem);
        grid.dataSource.insert(newIndex, dataItem);

        dataItem.dirty = false;
        
        $.ajax({
            url: '/PlannerNoteReorder',
            type: "POST",            
            data: {
                note_id: dataItem.note_id,
                new_index: newIndex
            }
        });
    };

    this.onSchedulerDataBound = function (e) {
        if (firstDataBound_scheduler) {
            firstDataBound_scheduler = false;            
        };
        createSchedulerDropArea(this);        
        createSchedulerDraggable();        
    };

    this.onSchedulerRemove = function (e) {
        var rightPanelIndex = $('#tabPlanerPanel').data('kendoTabStrip').select().index();
        setTimeout(function () {
            if (rightPanelIndex == 0) {
                refreshUpcomingEventGrid();
            } else {
                refreshOverdueEventGrid();
            };
        }, 1500);
    };

    this.sortableHintHandler = function (element) {
        return element.clone().addClass("hint");
    };

    this.onPlannerNoteGridCancel = function (e) {
        if (e.model.isNew() == true) {
            $("#plannerNoteGrid").data("kendoGrid").cancelRow();
        };
    };

    this.onBtnNotesSearchClick = function (e) {
        //
        $('#plannerNoteGrid .k-filter-row').toggle();
    };

    this.onTbUpcomingPlannerToggle = function (e) {
        refreshUpcomingEventGrid();
    };

    this.onWinTaskListActivate = function (e) {
        $('#taskListWinGrid').data('kendoGrid').dataSource.read();
    };

    this.onWinTaskListDeactivate = function (e) {
        //this.destroy();
    };

    this.onSchedulerError = function (e) {
        if (e.errors) {
            var message = "Ошибка:\n";
            $.each(e.errors, function (key, value) {
                if ('errors' in value) {
                    $.each(value.errors, function () {
                        message += this + "\n";
                    });
                }
            });
            alert(message);
            $('#scheduler').getKendoScheduler().cancelEvent();            
        }
    };

    this.onWinTaskEditPlannerOpen = function (e) {
        //
    };

    this.onAttrChanged = function () {
        $('#btnTaskOkPlanner').removeClass('btn-default');
        $('#btnTaskOkPlanner').addClass('btn-success');
        $('#btnTaskOkPlanner').removeAttr('disabled');
        //
        $('#btnTaskSavePlanner').removeClass('btn-default');
        $('#btnTaskSavePlanner').addClass('btn-info');
        $('#btnTaskSavePlanner').removeAttr('disabled');
    };

    this.subtaskStateListChanged = function (e) {
        var item = $('#subtaskGridPlanner').data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        item.set('state_id', ddl_val);
        item.set('state_id_ReadOnly', ddl_val);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.subtaskFileListChanged = function (e) {
        var item = $('#subtaskGridPlanner').data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        var ddl_text = this.text();
        item.set('file_id', ddl_val);
        item.set('file_id_ReadOnly', ddl_val);
        item.set('file_name', ddl_text);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.onSubtaskGridPlannerDataBound = function (e) {
        $(".stateListCell").each(function () {
            eval($(this).children("script").last().html());
        });
        $(".fileListCell").each(function () {
            eval($(this).children("script").last().html());
        });
        var grid = $('#subtaskGridPlanner').getKendoGrid();
        var ds = grid.dataSource;
        var task_num_max = ds.aggregates().task_num.max;
        localStorage.subtask_num = task_num_max;
    };

    this.onSubtaskGridPlannerEdit = function (e) {
        e.model.IsNew = true;
        _this.onAttrChanged();
    };

    this.onSubtaskGridPlannerRemove = function (e) {
        setTimeout(function () { $('#subtaskGridPlanner').data('kendoGrid').dataSource.sync() })
    };

    this.onSubtaskGridPlannerExcelExport = function (e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsExcel();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onSubtaskGridPlannerPdfExport = function (e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsPDF();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onUplFilePlannerSuccess = function (e) {
        if (e.operation == 'upload') {
            var grid = $("#taskFilesGridPlanner").data("kendoGrid");
            grid.dataSource.read();
            _this.onAttrChanged();
        };

        return true;
    };

    this.onUplFilePlannerError = function (e) {
        var err = e.XMLHttpRequest.responseText;
        if (err.length <= 200) {
            alert(err);
        };
    };

    this.onTabPlanerPanelShow = function (e) {
        if ($(e.item).text() === 'Просроченные') {
            refreshOverdueEventGrid();
        };
    };

    this.onSchedulerRequestEnd = function (e) {
        if ((e.type == "create") && (needUpdate_scheduler)) {
            needUpdate_scheduler = false;
            refreshScheduler();
        }
    };

    // private

    function refreshScheduler() {
        $('#scheduler').data('kendoScheduler').dataSource.read();
    };

    function refreshUpcomingEventGrid() {
        $('#plannerUpcomingEventGrid').data('kendoGrid').dataSource.read();
    };

    function refreshNotesGrid() {
        $('#plannerNoteGrid').data('kendoGrid').dataSource.read();
    };

    function refreshOverdueEventGrid() {
        $('#plannerOverdueEventGrid').data('kendoGrid').dataSource.read();
    };

    function createSchedulerDropArea(scheduler) {        
        //scheduler.view().content.kendoDropTargetArea({
        $('#scheduler>.k-scheduler-layout').kendoDropTargetArea({
            filter: ".k-scheduler-table td, .k-event",
            drop: function (e) {

                var gridPlanner = $("#plannerNoteGrid").data("kendoGrid");
                var gridUpcoming = $("#plannerUpcomingEventGrid").data("kendoGrid");
                var scheduler = $('#scheduler').data('kendoScheduler');
                var offset = $(e.dropTarget).offset();
                var slot = scheduler.slotByPosition(offset.left, offset.top);
                //var dataItem = grid.dataItem(grid.select());
                var row = e.draggable.currentTarget;
                var dataItem_fromNotes = true;
                var dataItem_fromUpcoming = false;
                var dataItem_fromScheduler = false;
                var dataItem = gridPlanner.dataSource.getByUid(row.data("uid"));
                if ((!dataItem) || (dataItem.length <= 0) || (!dataItem.note_id)) {
                    dataItem = gridUpcoming.dataSource.getByUid(row.data("uid"));
                    dataItem_fromNotes = false;
                    dataItem_fromUpcoming = true;
                };
                if ((!dataItem) || (dataItem.length <= 0)) {                    
                    dataItem_fromUpcoming = false;
                    dataItem_fromScheduler = true;

                    // !!!
                    var container = $(row).parents('.k-event').first();
                    var uid = scheduler.wrapper.find(container).data("uid");
                    dataItem = scheduler.occurrenceByUid(uid);
                    if ((!dataItem) || (dataItem.length <= 0)) {                        
                        dataItem_fromScheduler = false;
                    };
                };

                var new_title = null;
                var new_description = null;
                var new_task_id = null;                
                var new_event_priority_id = null;                
                var new_is_public = false;
                var new_rate = null;
                var new_event_state_id = 0;
                var new_progress = null;
                var new_result = null;
                var new_event_group_id = 1;
                if (dataItem_fromNotes) {
                    new_title = dataItem.description;
                    new_description = dataItem.description;
                    new_task_id = dataItem.task_id;
                    new_event_priority_id = 0;
                    new_is_public = dataItem.is_public;
                    new_rate = 0;
                    new_event_state_id = 0;
                    new_progress = null;
                    new_result = null;
                    new_event_group_id = 1;
                } else if (dataItem_fromUpcoming) {
                    new_title = dataItem.Title;
                    new_description = dataItem.Description;
                    new_task_id = dataItem.task_id;
                    new_event_priority_id = dataItem.event_priority_id;
                    new_is_public = dataItem.is_public;
                    new_rate = dataItem.rate;
                    new_event_state_id = dataItem.event_state_id;
                    new_progress = dataItem.progress;
                    new_result = dataItem.result;
                    new_event_group_id = dataItem.event_group_id;
                };
                

                if ((dataItem && slot) && ((dataItem_fromNotes) || (dataItem_fromUpcoming))) {
                    var offsetMiliseconds = new Date().getTimezoneOffset() * 60000;
                    var newEvent = {
                        title: new_title,
                        end: slot.isDaySlot ? slot.startDate : slot.endDate,
                        start: slot.startDate,
                        isAllDay: slot.isDaySlot,
                        description: new_description,
                        exec_user_id: currUser_CabUserId,                        
                        event_priority_id: new_event_priority_id,
                        task_id: new_task_id,
                        is_public: new_is_public,
                        rate: new_rate,
                        event_state_id: new_event_state_id,
                        progress: new_progress,
                        result: new_result,
                        event_group_id: new_event_group_id,
                        SourceNoteId: dataItem.note_id,
                        SourceUpcomingEventId: dataItem.event_id
                    };

                    //scheduler.addEvent(newEvent);
                    if (dataItem_fromNotes) {
                        gridPlanner.dataSource.remove(dataItem);
                    } else if (dataItem_fromUpcoming) {
                        //gridUpcoming.dataSource.remove(dataItem);
                        gridUpcoming.removeRow(row);
                    }

                    /*
                    var existing_event = scheduler.dataSource.get(dataItem.event_id);
                    if (existing_event) {
                        scheduler.dataSource.remove(existing_event);
                    };
                    */
                    scheduler.dataSource.add(newEvent);
                    needUpdate_scheduler = true;
                    setTimeout(function () { scheduler.dataSource.sync(); });
                } else if ((dataItem && slot) && (dataItem_fromScheduler)) {
                    // !!!
                    var existing_event = scheduler.dataSource.get(dataItem.event_id);
                    existing_event.set('end', slot.isDaySlot ? slot.startDate : slot.endDate);
                    existing_event.set('start', slot.startDate);
                    existing_event.set('isAllDay', slot.isDaySlot);
                    setTimeout(function () { scheduler.dataSource.sync(); });
                }
            }
        });
    };

    function createNotesDropArea(notesGrid) {        
        notesGrid.content.kendoDropTargetArea({
            filter: "tbody > tr",
            drop: function (e) {
                var gridNotes = $("#plannerNoteGrid").data("kendoGrid");
                var gridUpcoming = $("#plannerUpcomingEventGrid").data("kendoGrid");
                var scheduler = $('#scheduler').data('kendoScheduler');
                var row = e.draggable.currentTarget;
                var uid = null;
                var dataItem_fromNotes = true;
                var dataItem_fromUpcoming = false;
                var dataItem_fromScheduler = false;
                var dataItem = gridNotes.dataSource.getByUid(row.data("uid"));
                if (!dataItem) {
                    dataItem_fromNotes = false;
                    dataItem_fromUpcoming = true;
                    dataItem = gridUpcoming.dataSource.getByUid(row.data("uid"));
                    if (!dataItem) {
                        dataItem_fromUpcoming = false;
                        dataItem_fromScheduler = true;                        
                        var container = $(row).parents('.k-event').first();
                        uid = scheduler.wrapper.find(container).data("uid");
                        dataItem = scheduler.occurrenceByUid(uid);
                        if (!dataItem) {
                            dataItem_fromScheduler = false;
                        };
                    }
                };

                var dataRows = gridNotes.items();
                var newIndex = dataRows.index(e.dropTarget);

                if (dataItem_fromNotes) {

                    gridNotes.dataSource.remove(dataItem);
                    gridNotes.dataSource.insert(newIndex, dataItem);

                    dataItem.dirty = false;

                    $.ajax({
                        url: '/PlannerNoteReorder',
                        type: "POST",
                        data: {
                            note_id: dataItem.note_id,
                            new_index: newIndex
                        }
                    });
                } else if (dataItem_fromUpcoming) {
                    gridNotes.dataSource.insert(newIndex, {
                        description: dataItem.Title,
                        exec_user_id: currUser_CabUserId,
                        is_public: dataItem.is_public,
                        SourceUpcomingEventId: dataItem.event_id
                    });

                    // !!!
                    var event = scheduler.dataSource.get(dataItem.event_id);
                    var options = scheduler.options;
                    var options_orig = $.extend(true, {}, options);
                    options.editable.confirmation = false;
                    scheduler.options = options;
                    scheduler.removeEvent(event);
                    scheduler.options = options_orig;

                    gridUpcoming.removeRow(row);
                    gridNotes.dataSource.sync();
                } else if (dataItem_fromScheduler) {
                    gridNotes.dataSource.insert(newIndex, {
                        description: dataItem.title,
                        exec_user_id: currUser_CabUserId,
                        is_public: dataItem.is_public                        
                    });
                    
                    var options = scheduler.options;
                    var options_orig = $.extend(true, {}, options);
                    options.editable.confirmation = false;
                    scheduler.options = options;
                    scheduler.removeEvent(uid);
                    scheduler.options = options_orig;                    

                    gridNotes.dataSource.sync();
                };
            }
        });
    };

    function createUpcomingDropArea(upcomingGrid) {
        upcomingGrid.content.kendoDropTargetArea({
            filter: "tbody > tr",
            drop: function (e) {
                var gridNotes = $("#plannerNoteGrid").data("kendoGrid");
                var gridUpcoming = $("#plannerUpcomingEventGrid").data("kendoGrid");
                var row = e.draggable.currentTarget;
                var dataItem_fromNotes = true;
                var dataItem_fromScheduler = false;
                var dataItem = gridNotes.dataSource.getByUid(row.data("uid"));
                if (!dataItem) {
                    var dataItem_fromNotes = false;                    
                    dataItem_fromScheduler = true;
                    var scheduler = $('#scheduler').data('kendoScheduler');
                    var container = $(row).parents('.k-event').first();
                    var uid = scheduler.wrapper.find(container).data("uid");
                    var dataItem = scheduler.occurrenceByUid(uid);
                    if (!dataItem) {
                        dataItem_fromScheduler = false;
                    };
                };

                var dataItem_target = gridUpcoming.dataItem(e.dropTarget);

                if ((dataItem_fromNotes) || (dataItem_fromScheduler)) {

                    var curr_upcoming_event_date = new Date();
                    var curr_days_add = 1;

                    if (dataItem_target) {
                        curr_upcoming_event_date = dataItem_target.Start;
                        curr_days_add = null;
                    } else {
                        curr_upcoming_event_date = null;
                        var filter_id = $('#tbUpcomingPlanner .k-button-group>.k-toggle-button.k-state-active').attr('id');
                        if (filter_id == 'btn-upcoming-event-filter-tomorrow') {
                            curr_days_add = 1;
                        } else if (filter_id == 'btn-upcoming-event-filter-3days') {
                            curr_days_add = 3;
                        } else if (filter_id == 'btn-upcoming-event-filter-7days') {
                            curr_days_add = 7;
                        };
                    }

                    $.ajax({
                        url: '/UpcomingEventAdd',
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({ note_id: dataItem.note_id, event_id: dataItem.event_id, fromNotes: dataItem_fromNotes, fromScheduler: dataItem_fromScheduler, upcoming_event_date: curr_upcoming_event_date, days_add: curr_days_add }),
                        success: function (res) {
                            if ((!res) || (res.err)) {
                                alert(((res) && (res.mess)) ? res.mess : 'Ошибка при добавлении события ! Пожалуйста обновите страницу');
                            } else {
                                refreshUpcomingEventGrid();
                                refreshScheduler();
                            };
                        },
                        error: function (res) {
                            alert(((res) && (res.mess)) ? res.mess : 'Ошибка при добавлении события ! Пожалуйста обновите страницу');
                        }
                    });

                    if (dataItem_fromNotes) {
                        gridNotes.dataSource.remove(dataItem);
                        gridNotes.dataSource.sync();
                    };
                };
            }
        });
    };

    function createPlannerNoteDraggable() {
        $("#plannerNoteGrid").kendoDraggable({
            filter: "tbody > tr",            
            hint: function (element) {
                var grid = $("#plannerNoteGrid").data("kendoGrid");
                var dataItem = grid.dataItem(element);
                var tooltipHtml = "<div class='k-event' id='dragTooltip'><div class='k-event-template' style='background:#b8f7fb;'>" + dataItem.description + "</div></div>";
                return $(tooltipHtml).css("width", 300);
            }            
        });
    };

    function createPlannerUpcomingDraggable() {
        $("#plannerUpcomingEventGrid").kendoDraggable({
            filter: "tbody > tr",
            hint: function (element) {
                var grid = $("#plannerUpcomingEventGrid").data("kendoGrid");
                var dataItem = grid.dataItem(element);
                var tooltipHtml = "<div class='k-event' id='dragTooltip'><div class='k-event-template' style='background:#b8f7fb;'>" + dataItem.Title + "</div></div>";
                return $(tooltipHtml).css("width", 300);
            }
        });
    };
    
    function createSchedulerDraggable() {
        $("#scheduler").kendoDraggable({
            //filter: ".k-event:not(.k-resize-handle)",
            //filter: ".planner-event > p",
            filter: ".planner-event",
            hint: function (element) {
                var scheduler = $("#scheduler").data("kendoScheduler");
                var container = $(element).parents('.k-event').first();                
                var uid = scheduler.wrapper.find(container).data("uid");
                var event = scheduler.occurrenceByUid(uid);
                return container.clone().css({
                    "opacity": 0.6
                });
            }
        });
    };


    //****************
    // task-planner-edit
    //****************

    function openPlannerTask() {
        var win = $('#winTaskEditPlanner').data('kendoWindow');
        $('#winTaskEditPlanner').html("");
        win.refresh({
            url: 'TaskEditPlanner',            
            data: { task_id: curr_event.task_id, exec_user_id: curr_event.exec_user_id, date_plan: curr_event.end }
        });

        win.center().open();
        $('#winTaskEditPlanner').closest(".k-window").css({
            top: $(document).scrollTop()
        });
    };

    function onSubtaskAddClick(e) {
        var text = $('#txtSubtaskAddPlanner').val();
        if (!text) return false;
        
        var combo = $("#assignedUserListPlanner").getKendoDropDownList();
        var curr_exec_user_id = combo.dataItem()["user_id"];
        var curr_subtask_num = generateSubtaskNum();
        var new_subtask = {
            subtask_id: 0, task_id: $('#task_id').val(), subtask_text: text,
            state_id: 1, owner_user_id: currUser_CabUserId, crt_date: new Date(),
            repair_date: null, task_num: curr_subtask_num, owner_user_name: currUser_CabUserName,
            file_id: null,
            IsChecked: false, IsNew: true
        };

        var grid = $("#subtaskGridPlanner").data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_subtask);
        $('#txtSubtaskAddPlanner').val('');

        _this.onAttrChanged();

        return true;
    };

    function generateSubtaskNum() {
        AutoID = 1;
        var newAutoID = localStorage.subtask_num;
        if (!isNaN(newAutoID)) {
            AutoID = parseInt(newAutoID) + 1;
        }
        localStorage.subtask_num = AutoID;
        return AutoID;
    };

    function onTaskNoteAddClick(e) {
        var text = $('#txtTaskNoteAddPlanner').val();
        if (!text) return false;

        var new_note = { note_id: 0, note_text: text, crt_date: new Date(), owner_user_id: currUser_CabUserId, user_name: currUser_CabUserName, file_name: '' };
        var grid = $("#taskNoteGridPlanner").data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_note);
        $('#txtTaskNoteAddPlanner').val('');

        _this.onAttrChanged();

        return true;
    };

    function onSubtaskExcelClick(e) {
        var grid = $('#subtaskGridPlanner').getKendoGrid();
        grid.saveAsExcel();
    };

    function onSubtaskPdfClick(e) {
        var grid = $('#subtaskGridPlanner').getKendoGrid();
        grid.saveAsPDF();
    };

    function onBtnTaskOkClick(e) {
        if (checkSaveFlag) {
            checkTaskSave();
        }
        else {
            taskSave(true);
        };
    };

    function checkTaskSave() {
        var ctrl = $('#spanTaskErrMessPlanner');

        $.ajax({
            type: "POST",
            url: '/Crm/TaskSaveBefore?task_id=' + curr_task_id + '&task_upd_num=' + $('#upd_num').val(),
            data: '',
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response != null) {
                    if (!response.is_ok) {
                        ctrl.html(response.mess);
                        ctrl.removeClass('hidden');
                        checkSaveFlag = false;
                        return false;
                    } else {
                        taskSave(true);
                        return true;
                    };
                } else {
                    taskSave(true);
                    return true;
                };
            },
            error: function (task_res) {
                taskSave(true);
                return true;
            }
        });
    };

    function taskSave(winclose) {
        var win = $('#winTaskEditPlanner').data('kendoWindow');

        var project_name_combo = $("#projectListPlanner").getKendoDropDownList();
        var module_name_combo = $("#moduleListPlanner").getKendoDropDownList();
        var module_version_name_combo = $("#moduleVersionListPlanner").getKendoDropDownList();
        var repair_version_name_combo = $("#repairVersionListPlanner").getKendoDropDownList();
        var client_name_combo = $("#clientListPlanner").getKendoDropDownList();
        var priority_name_combo = $("#priorityListPlanner").getKendoDropDownList();
        var state_name_combo = $("#stateListPlanner").getKendoDropDownList();
        var assigned_user_name_combo = $("#assignedUserListPlanner").getKendoDropDownList();
        var exec_user_name_combo = $("#execUserListPlanner").getKendoDropDownList();
        var event_user_name_combo = $("#eventUserListPlanner").getKendoDropDownList();
        var group_name_combo = $('#groupListPlanner').getKendoDropDownList();

        var task_id_col = curr_task_id;
        var task_name_col = $('#task_namePlanner').val();
        var task_text_col = $('#task_textPlanner').val();
        var task_num_col = 'AU-0000';
        var crt_date_col = new Date();
        var client_name_col = client_name_combo.dataItem()["client_name"];
        var client_id_col = client_name_combo.dataItem()["client_id"];
        var owner_user_name_col = currUser_CabUserName;
        var owner_user_id_col = currUser_CabUserId;

        var required_date_col = null;
        var repair_date_col = null;
        var repair_version_name_col = '[не указан]';
        var repair_version_id_col = 0;
        var project_name_col = '[не указан]';
        var project_id_col = 0;
        var module_name_col = '[не указан]';
        var module_id_col = 0;
        var module_version_name_col = '[не указан]';
        var module_version_id_col = 0;
        var module_part_name_col = '[не указан]';
        var module_part_id_col = 0;
        var priority_name_col = '[не указан]';
        var priority_id_col = 0;
        var state_name_col = '[не указан]';
        var state_id_col = 0;
        var assigned_user_name_col = '[не указан]';
        var assigned_user_id_col = 0;
        var exec_user_name_col = '[не указан]';
        var exec_user_id_col = 0;
        var is_moderated_col = false;
        var event_user_name_col = '[не указан]';
        var event_user_id_col = 1;
        var point_col = 0;
        var fin_cost_col = 0;
        var is_event_name_col = '';
        var is_event_col = false;
        var group_id_col = 0;
        var group_name_col = 'Неразобранные';
        var task_notes = null;
        var task_subtasks = null;

        point_col = $('#pointPlanner').val();
        fin_cost_col = $('#fin_costPlanner').val();
        task_notes = $("#taskNoteGridPlanner").data("kendoGrid").dataSource.view();
        task_subtasks = $("#subtaskGridPlanner").data("kendoGrid").dataSource.view();

        repair_date_col = $('#repair_datePlanner').val();
        state_name_col = state_name_combo.dataItem()["state_name"];
        state_id_col = state_name_combo.dataItem()["state_id"];

        assigned_user_name_col = assigned_user_name_combo.dataItem()["user_name"];
        assigned_user_id_col = assigned_user_name_combo.dataItem()["user_id"];
        exec_user_name_col = exec_user_name_combo.dataItem()["user_name"];
        exec_user_id_col = exec_user_name_combo.dataItem()["user_id"];
        is_moderated_col = $('#IsModeratedPlanner').is(':checked');
        if (event_user_name_combo) {
            event_user_name_col = event_user_name_combo.dataItem()["Text"];
            event_user_id_col = event_user_name_combo.dataItem()["Value"];
        };
        required_date_col = $('#required_datePlanner').val();
        project_name_col = project_name_combo.dataItem()["project_name"];
        project_id_col = project_name_combo.dataItem()["project_id"];
        module_name_col = module_name_combo.dataItem()["module_name"];
        module_id_col = module_name_combo.dataItem()["module_id"];
        module_version_name_col = module_version_name_combo.dataItem()["module_version_name"];
        module_version_id_col = module_version_name_combo.dataItem()["module_version_id"];
        repair_version_name_col = repair_version_name_combo.dataItem()["module_version_name"];
        repair_version_id_col = repair_version_name_combo.dataItem()["module_version_id"];
        priority_name_col = priority_name_combo.dataItem()["priority_name"];
        priority_id_col = priority_name_combo.dataItem()["priority_id"];

        is_event_col = $('#is_eventPlanner').is(':checked');
        if (is_event_col) { is_event_col_name = 'К'; } else { is_event_col_name = ''; };

        group_id_col = group_name_combo.dataItem()["group_id"];
        group_name_col = group_name_combo.dataItem()["group_name"];

        var task = {
            task_id: task_id_col, crt_date: crt_date_col, task_num: task_num_col,
            task_name: task_name_col, task_text: task_text_col,
            state_name: state_name_col, state_id: state_id_col,
            client_name: client_name_col, client_id: client_id_col,
            exec_user_name: exec_user_name_col, exec_user_id: exec_user_id_col,
            is_event_for_exec: event_user_id_col,
            assigned_user_name: assigned_user_name_col, assigned_user_id: assigned_user_id_col,
            module_name: module_name_col, module_id: module_id_col,
            module_version_name: module_version_name_col, module_version_id: module_version_id_col,
            repair_version_name: repair_version_name_col, repair_version_id: repair_version_id_col,
            owner_user_name: owner_user_name_col, owner_user_id: owner_user_id_col,
            priority_name: priority_name_col, priority_id: priority_id_col,
            project_name: project_name_col, project_id: project_id_col,
            required_date: required_date_col,
            repair_date: repair_date_col,
            group_id: group_id_col, group_name: group_name_col,
            point: point_col, fin_cost: fin_cost_col, is_event: is_event_col, is_event_name: is_event_name_col,
            IsModerated: is_moderated_col
        };
        $.ajax({
            type: "POST",
            url: "Crm/TaskAdd",
            data: JSON.stringify({ task: task, notes: task_notes, subtasks: task_subtasks }),
            contentType: 'application/json; charset=windows-1251',
            success: function (task_res) {
                refreshScheduler();                
            },
            error: function (task_res) {
                alert('Ошибка при редактировании задачи');
            }
        });

        win.close();
    };

    function onBtnTaskCancelClick(e) {
        var win = $('#winTaskEditPlanner').data('kendoWindow');
        win.close();
    };
}

var planner = null;
var firstDataBound_scheduler = true;
var firstDataBound_note = true;
var firstDataBound_upcoming = true;
var checkedIds = {};
var exportFlag = false;
var checkSaveFlag = true;
var curr_task_id = null;
var curr_event = null;
var needUpdate_scheduler = false;

$(function () {
    planner = new Planner();
    planner.init();
});
