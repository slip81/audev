﻿function Planner() {
    _this = this;

    this.init = function () {
        //setGridMaxHeight('#plannerTaskGrid', 250);
        //
        setControlHeight('#plannerMainSplitter', 50);
        //        
        confirmBackspaceNavigations();
        //
        curr_page_group_id = userFilter_curr_group_id;
        if (curr_page_group_id == null) curr_page_group_id = 0;

        $('.visible-group-0').addClass('hidden');
        if (curr_page_group_id == -1) {
            $('#menu-task-all').addClass('k-state-selected');
        } else if (curr_page_group_id == 0) {
            $('#menu-task-bucket').addClass('k-state-selected');
            $('.visible-group-0').removeClass('hidden');
        } else if (curr_page_group_id == 1) {
            $('#menu-task-prepared').addClass('k-state-selected');
        } else if (curr_page_group_id == 2) {
            $('#menu-task-fixed').addClass('k-state-selected');
        } else if (curr_page_group_id == 3) {
            $('#menu-task-approved').addClass('k-state-selected');
        } else if (curr_page_group_id == 5) {
            $('#menu-task-version').addClass('k-state-selected');
        } else if (curr_page_group_id == 4) {
            $('#menu-task-closed').addClass('k-state-selected');
        } else if (curr_page_group_id == 6) {
            $('#menu-task-project').addClass('k-state-selected');
        };

        confirmBackspaceNavigations();
        //
        $(document).kendoTooltip({
            filter: ".k-event:not(.k-event-drag-hint) > div, .k-task",
            position: "top",
            width: 250,
            content: kendo.template($('#planner-event-tooltip-template').html())
        });
        //
        $(document).kendoTooltip({
            filter: "#plannerOverdueEventGrid tr, #plannerStrategEventGrid tr",
            position: "top",
            width: 250,
            content: kendo.template($('#planner-event-grid-tooltip-template').html())
        });
        //
        $(document).on('click', '#chbShowPublic', function () {
            refreshScheduler();
        });
        //
        $(document).on('click', '.btn-del-note', function () {
            //alert('btn-del-note clicked !');
            if (!(confirm('Удалить запись ?'))) {
                return;
            };
            var grid = $('#plannerNoteGrid').data('kendoGrid');
            var dataItem = grid.dataItem($(this).closest('tr'))
            if (dataItem) {
                grid.dataSource.remove(dataItem);
                $.ajax({
                    url: '/PlannerNoteDel',
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    data: JSON.stringify({ note_id: dataItem.note_id }),
                    success: function (res) {
                        if ((!res) || (res.err)) {                            
                            alert(((res) && (res.mess)) ? res.mess : 'Ошибка при удалении записи ! Пожалуйста обновите страницу');
                        } else {
                            //
                        };
                    },
                    error: function (res) {
                        alert(((res) && (res.mess)) ? res.mess : 'Ошибка при удалении записи ! Пожалуйста обновите страницу');
                    }
                });
            }
        });
        //        
        $(document).on('click', '.btn-cancel-note-change', function (e) {            
            var grid = $('#plannerNoteGrid').data('kendoGrid');            
            grid.closeCell(true);
        });        
        //
        $("#plannerNoteGrid").delegate("tbody>tr", "dblclick", function () {
            //alert("Double Click!");
            var dataItem = $("#plannerNoteGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {                
                if (dataItem.task_id) {
                    openPlannerTask_byTaskId(dataItem.task_id, dataItem.task_num);
                } else {
                    openPlannerNote(dataItem.note_id);
                }
            };
        });
        //
        $(document).on('click', '#winPlannerNoteEdit .btn-save', function (e) {
            var noteId = $('#winPlannerNoteEdit #note_id').val();
            var noteTitle = $('#winPlannerNoteEdit #title').val();
            var noteDescrition = $('#winPlannerNoteEdit #description').val();
            var noteProgress = $('#winPlannerNoteEdit #progress').val();
            var noteResult = $('#winPlannerNoteEdit #result').val();
            var noteEventStateId = $('#event_state_id').data('kendoDropDownList').value();
            var noteIsPublic = !($('#IsPrivate').is(':checked'));
            $("#winPlannerNoteEdit").data("kendoWindow").close();
            
            var plannerNoteGrid = $("#plannerNoteGrid").data("kendoGrid");
            var dataSource = plannerNoteGrid.dataSource;
            $.ajax({
                url: '/PlannerNoteEditWin',
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({ note_id: noteId, title: noteTitle, description: noteDescrition, progress: noteProgress, result: noteResult, exec_user_id: currUser_CabUserId, event_state_id: noteEventStateId, is_public: noteIsPublic }),
                success: function (res) {
                    if ((!res) || (res.err)) {
                        alert(((res) && (res.mess)) ? res.mess : 'Ошибка при добавлении записи ! Пожалуйста обновите страницу');
                    } else {
                        if (noteId > 0) {
                            var dataItem_old = dataSource.get(noteId);
                            dataItem_old.set('title', res.planner_note.title);
                            dataItem_old.set('description', res.planner_note.description);
                            dataItem_old.set('progress', res.planner_note.progress);
                            dataItem_old.set('result', res.planner_note.result);                            
                        } else {
                            dataSource.insert(0, res.planner_note);
                        }
                    };
                },
                error: function (res) {
                    alert(((res) && (res.mess)) ? res.mess : 'Ошибка при добавлении записи ! Пожалуйста обновите страницу');
                }
            });
        });
        //
        $(document).on('click', '#winPlannerNoteEdit .btn-cancel', function (e) {
            $("#winPlannerNoteEdit").data("kendoWindow").close();
        });
        //
        $("#plannerOverdueEventGrid").delegate("tbody>tr", "dblclick", function () {
            //alert("Double Click!");
            var dataItem = $("#plannerOverdueEventGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {                
                if (dataItem.task_id) {
                    openPlannerTask_byTaskId(dataItem.task_id, dataItem.task_num);
                } else {
                    //
                }
            };
        });
        //
        $("#plannerStrategEventGrid").delegate("tbody>tr", "dblclick", function () {
            //alert("Double Click!");
            var dataItem = $("#plannerStrategEventGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {
                if (dataItem.task_id) {
                    openPlannerTask_byTaskId(dataItem.task_id, dataItem.task_num);
                } else {
                    //
                }
            };
        });
        //
        $("#plannerTaskGrid").delegate("tbody>tr", "dblclick", function () {            
            var dataItem = $("#plannerTaskGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {
                dblclickFired = true;                
                openPlannerTask_byTaskId(dataItem.task_id, dataItem.task_num);
                setTimeout(function () { dblclickFired = false; }, 800);
            };
        });
        //
        $("#btnTaskListFilter").click(function (e) {
            e.preventDefault();
            $("#taskFilterPanel").data("kendoPanelBar").collapse($("li.k-state-active"));
            taskUserFilterSave();
        });
        //
        $('#btnWinTaskBatchOk').click(function (e) {
            e.preventDefault();            
            var curr_batch_id = $("#winTaskBatch #ddlTaskBatch").data("kendoDropDownList").value();
            $.ajax({
                type: "POST",
                url: "Crm/TaskOperationExecute",
                data: JSON.stringify({ task_id_list: id_list, operation_id: taskAction, batch_id: curr_batch_id }),
                contentType: 'application/json; charset=windows-1251',
                success: function (task_res) {
                    $("#winTaskBatch").data("kendoWindow").close();
                    checkedIds = {};
                    $("#taskGridCombo").data("kendoDropDownList").select(0);
                    refreshTaskGrid();
                },
                error: function (task_res) {
                    $("#winTaskBatch").data("kendoWindow").close();
                    $("#taskGridCombo").data("kendoDropDownList").select(0);
                    alert('Ошибка при выполнении операции со списком задач');
                }
            });
        });
        //
        $('#btnWinTaskBatchCancel').click(function (e) {
            e.preventDefault();
            $("#winTaskBatch").data("kendoWindow").close();
            return false;
        });
        //
        $('#taskGridPhrase').keypress(function (e) {
            if (e.keyCode == 13) {
                refreshTaskGrid();
            };
        });
        //
        $("#btnTaskGridConfigOk").click(function (e) {
            e.preventDefault();
            var win = $("#winTaskGridConfig").data("kendoWindow");
            var settingsData = getSettingsData();
            $.ajax({
                type: "POST",
                url: "Sys/CabGridColumnUserSettings",
                data: kendo.stringify(settingsData),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if (response != null && response.success) {
                        win.close();
                        window.location = url_Planner;
                    } else {
                        win.close();
                        alert(response);
                    }
                },
                error: function (response) {
                    win.close();
                    alert('error');
                }
            });

            return false;
        });
        //
        $("#btnTaskGridConfigCancel").click(function (e) {
            e.preventDefault();
            var win = $("#winTaskGridConfig").data("kendoWindow");
            win.close();
            return false;
        });
        //
        $(document).on('click', '#tabPlanerTask>.k-tabstrip-items>li.k-item', function () {
            var splitter = $("#plannerEventSplitter").data("kendoSplitter");            
            var leftBottomPane = splitter.options.panes[1];
            var leftTopPane_height = $("#left-top-pane").height();
            var leftBottomPane_height = $("#left-bottom-pane").height();

            if ($(this).hasClass('planner-planner')) {
                splitter.size("#left-bottom-pane", leftBottomPane_minSize);
            } else {
                if (leftBottomPane.size == leftBottomPane_minSize) {
                    splitter.size("#left-bottom-pane", ((leftTopPane_height + leftBottomPane_height) / verticalKft()) + "px");
                };
            };            
        });
        //
        $(document).on('click', '#chbStrategActive', function () {            
            refreshStrategEventGrid();
        });
        //
        $(document).on('click', '#btn-planner-export-excel', function () {
            var sch = $('#scheduler').data('kendoScheduler');
            var date_beg = new Date(sch.view().startDate()).toISOString();
            var date_end = new Date(sch.view().endDate()).toISOString();
            var user_id = -1;
            var user_name = 'все';
            var ddl1 = $('#userList').getKendoDropDownList();
            if (ddl1) {
                var item1 = ddl1.dataItem();
                if (item1) {
                    user_id = item1.user_id;
                    user_name = item1.user_name;
                };
            };
            var overdue_only = false;
            var priority_control = false;

            window.location = '/PlannerExcelExport?date_beg=' + date_beg + '&date_end=' + date_end + '&user_id=' + user_id + '&user_name=' + user_name;
        });
        //
        $(document).on('click', '#btn-planner-export-pdf', function () {
            $('#scheduler .k-pdf').first().trigger('click');
        });
        //
    };

    // task-edit-planner.js

    this.init_popup = function () {

        $('.attr-change').bind('input propertychange', _this.onAttrChanged);

        $('.subtask-addPlanner').click(function (e) {
            onSubtaskAddClick(e);
        });

        $('.task-note-addPlanner').click(function (e) {
            onTaskNoteAddClick(e);
        });

        $('.subtask-excelPlanner').click(function (e) {
            onSubtaskExcelClick(e);
        });

        $('.subtask-pdfPlanner').click(function (e) {
            onSubtaskPdfClick(e);
        });

        $('#btnTaskOkPlanner').click(function (e) {
            //onBtnTaskOkClick(e);
            onBtnTaskCancelClick(e);
        });

        $('#btnTaskSavePlanner').click(function (e) {
            //onBtnTaskSaveClick(e);
            onBtnTaskCancelClick(e);
        });

        $('#btnTaskCancelPlanner').click(function (e) {
            onBtnTaskCancelClick(e);
        });

        $('.attr-change').click(function (e) {
            _this.onAttrChanged();            
        });

        $('div#div-subtaskPlanner ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
        //$('div#div-subtaskPlanner ul.k-tabstrip-items>li.k-item>a.k-link').click(function (e) {
            $('#div-commentPlanner').toggleClass('hidden');
            $('#div-subtaskPlanner').toggleClass('col-sm-6');
            $('#div-subtaskPlanner').toggleClass('col-sm-12');
        });

        $('div#div-commentPlanner ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
        //$('div#div-commentPlanner ul.k-tabstrip-items>li.k-item>a.k-link').click(function (e) {
            var index = $(this).index();
            var selectedIndex = $('#taskNoteTabStripPlanner').data('kendoTabStrip').select().index();
            $('#div-subtaskPlanner').toggleClass('hidden');
            $('#div-commentPlanner').toggleClass('col-sm-6');
            $('#div-commentPlanner').toggleClass('col-sm-12');
            if ($('#div-subtaskPlanner').hasClass('hidden')) {
                $('#div-commentPlanner').css('margin-left', '10px');
            } else {
                $('#div-commentPlanner').css('margin-left', '-10px');
            };
        });
    };

    //planner.js

    this.getCurrUserId = function (e) {
        return { curr_user_id: currUser_CabUserId };
    };

    this.onUserChange = function (e) {        
        //
        currUser_CabUserId = this.value();                
        refreshSchedulerResource();
        //refreshNotesGrid();        
        var index = $("#tabPlanerTask").data("kendoTabStrip").select().index();
        refreshTabPlanerTask(index);
        //
        setTimeout(function () { scrollToToday(); });
    };

    this.getEventFilter = function (e) {
        return _getEventFilter();
    };

    this.onBtnAddNoteClick = function (e) {        
        //$('#plannerNoteGrid .k-grid-add').trigger('click');
        openPlannerNote(0);
    };

    this.onSchedulerEdit = function (e) {
        /*
        var scheduler = $('#scheduler').data('kendoScheduler');
        var offset = $(e.currentTarget).offset();
        var slot = scheduler.slotByPosition(offset.left, offset.top);
        */
        
        // alert('onSchedulerEdit !');

        curr_event = e.event;
        curr_task_id = e.event.task_id;
        
        if (curr_task_id) {
            e.preventDefault();
            openPlannerTask();
            return false;
        };

        var title = e.container.prev('.k-window-titlebar').find('.k-window-title').first();
        if (title)
            title.html(e.event.event_id > 0 ? ('Событие ' + e.event.event_num) : 'Новое событие');

        if ((e.event.event_id <= 0) && (!$('#scheduler').children('.k-scheduler-layout').first().hasClass('k-scheduler-dayview'))) {
            e.event.set("isAllDay", true);
        };

        var sliderLabels = [];
        sliderLabels[0] = 'О';
        sliderLabels[1] = 'П';
        sliderLabels[2] = 'В';
        sliderLabels[3] = 'М';
        var sliderTitles = [];
        sliderTitles[0] = 'Обычный';
        sliderTitles[1] = 'Повышенный';
        sliderTitles[2] = 'Высокий';
        sliderTitles[3] = 'Максимальный';

        //var slider = $('#event_priority_id').data("kendoSlider");
        //var sliderItems = slider.siblings(".k-slider-items");
        var sliderItems = $('#event_priority_id').siblings(".k-slider-items");
        $.each(sliderLabels, function (index, value) {
            var item = sliderItems.find("li:eq(" + (index) + ")");
            //item.attr("title", value);
            item.attr("title", sliderTitles[index]);
            item.find("span").text(value);
        });
        
        hideShowUser();
    };

    this.onSchedulerAdd = function (e) {        
        e.event.end.setHours(e.event.start.getHours() + 1, 0, 0, 0);
    };

    this.onSchedulerSave = function (e) {
        var ddl1 = $('#exec_user_id').data('kendoDropDownList');
        var ddl2 = $('#event_state_id').data('kendoDropDownList');
        if (ddl1) {
            e.event.exec_user_id = ddl1.value();
        };
        if (ddl2) {
            e.event.event_state_id = ddl2.value();
        };
        
        if (resized) {
            resized = false;

            var isAllDay = false;
            if (!$('#scheduler').children('.k-scheduler-layout').first().hasClass('k-scheduler-dayview')) {
                isAllDay = true;
            };

            if (isAllDay) {
                e.event.isAllDay = true;
                if ((offset_left_after_resize < offset_left_before_resize) && (event_start_before_resize > slot_start_after_resize)) {
                    //двигaем левую границу налево
                    e.event.start = slot_start_after_resize;
                    e.event.end = event_end_before_resize;
                } else if ((offset_left_after_resize > offset_left_before_resize + 10) && (event_end_before_resize < slot_start_after_resize)) {
                    //двигaем правую границу направо
                    e.event.start = event_start_before_resize;
                    e.event.end = slot_start_after_resize;
                //} else if ((offset_left_after_resize + 5 >= offset_left_before_resize) && (event_end_before_resize >= slot_end_after_resize) && (event_start_before_resize >= slot_start_after_resize)) {
                } else if ((offset_left_after_resize + 5 >= offset_left_before_resize) && (event_end_before_resize >= slot_end_after_resize) && (event_start_before_resize <= slot_start_after_resize)) {
                    //двигaем правую границу налево
                    e.event.start = event_start_before_resize;
                    e.event.end = slot_start_after_resize;
                } else if ((offset_left_after_resize > offset_left_before_resize + 10) && (event_start_before_resize < slot_start_after_resize)) {
                    //двигaем левую границу направо
                    e.event.start = slot_start_after_resize;
                    e.event.end = event_end_before_resize;
                };

                offset_left_after_resize = null;
                offset_left_before_resize = null;
                slot_start_after_resize = null;
                event_start_before_resize = null;
                event_end_before_resize = null;
            }
        }

        setTimeout(function () {            
            var index = $("#tabPlanerTask").data("kendoTabStrip").select().index();
            refreshTabPlanerTask(index);
        }, 1500);
    };

    this.getEventClass = function (data) {
        var priority = data.event_priority_id;
        return 'event-priority-' + priority;
    };

    this.onPlannerToolBarToggle = function (e) {
        // !!!
        e.target.removeClass('k-state-active');
        var span = e.target.children('span').first();        
        var id = $(e.target).attr('id');
        switch (id) {
            case 'btn-panel-split-vert':                                
                if (!isVerticalHalf) {                    
                    span.removeClass('cab-png-split-vert');
                    span.addClass('cab-png-split-vert-active');
                } else {                    
                    span.removeClass('cab-png-split-vert-active');
                    span.addClass('cab-png-split-vert');
                };
                isVerticalHalf = !isVerticalHalf;
                var index = $("#tabPlanerTask").data("kendoTabStrip").select().index();
                if (index > 0) {
                    // !!!
                    var splitter = $("#plannerEventSplitter").data("kendoSplitter");
                    var leftTopPane_height = $("#left-top-pane").height();
                    var leftBottomPane_height = $("#left-bottom-pane").height();                    
                    splitter.size("#left-bottom-pane", ((leftTopPane_height + leftBottomPane_height) / verticalKft()) + "px");
                }
                break;
            case 'btn-panel-split-hor':                
                if (!isHorizontalHalf) {                    
                    span.removeClass('cab-png-split-hor');
                    span.addClass('cab-png-split-hor-active');
                } else {                    
                    span.removeClass('cab-png-split-hor-active');
                    span.addClass('cab-png-split-hor');
                };
                isHorizontalHalf = !isHorizontalHalf;
                var splitterMain = $("#plannerMainSplitter").data("kendoSplitter");
                if (isHorizontalHalf) {
                    refreshNotesGrid();
                    splitterMain.size('#left-pane', '70%');                    
                } else {                    
                    splitterMain.size('#left-pane', '100%');
                };
                break;
            default:
                break;
        }
    };

    this.getUpcomingEventFilter = function (e) {
        var days = 1;
        var filter_id = $('#tbUpcomingPlanner .k-button-group>.k-toggle-button.k-state-active').attr('id');
        if (filter_id == 'btn-upcoming-event-filter-tomorrow') {
            days = 1;
        } else if (filter_id == 'btn-upcoming-event-filter-3days') {
            days = 3;
        } else if (filter_id == 'btn-upcoming-event-filter-7days') {
            days = 7;
        };
        return { days_forward: days, exec_user_id: currUser_CabUserId };
    };

    this.getOverdueEventFilter = function (e) {
        return { exec_user_id: currUser_CabUserId };
    }

    this.getStrategEventFilter = function (e) {
        var is_active = $("#chbStrategActive").is(':checked');
        return { exec_user_id: currUser_CabUserId, is_active: is_active };
    }

    this.getPlanerNoteFilter = function (e) {
        return { exec_user_id: currUser_CabUserId };
    };

    this.onPlannerUpcomingEventGridDataBound = function (e) {
        if (firstDataBound_upcoming) {
            firstDataBound_upcoming = false;
            createPlannerUpcomingDraggable();
            createUpcomingDropArea(this);            
        }        
        drawEmptyRow('#plannerUpcomingEventGrid');
        //
        var cnt = e.sender.dataSource.total();
        var tab = $('#tabPlanerTask>.k-tabstrip-items>.k-item.planner-soon>.k-link');
        tab.html("<span class='k-sprite cab-png cab-png-soon'></span>Скоро (" + cnt + ")");
    };

    this.onPlannerNoteGridDataBound = function (e) {        
        //
        if (firstDataBound_note) {
            firstDataBound_note = false;            
            createPlannerNoteDraggable();
            createNotesDropArea(this);            
        }    
        drawEmptyRow('#plannerNoteGrid');
        //        
        var rowColorClasses = ['note-category1-color', 'note-category2-color', 'note-category3-color', 'note-category4-color'];
        var curr_step = 1;
        var grid = $("#plannerNoteGrid").data("kendoGrid");
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            if (curr_step <= 3) {
                row.addClass(rowColorClasses[0])
            } else if (curr_step <= 6) {
                row.addClass(rowColorClasses[1])
            } else if (curr_step <= 9) {
                row.addClass(rowColorClasses[2])
            } else if (curr_step <= 12) {
                row.addClass(rowColorClasses[3])
            };
            curr_step = curr_step + 1;
        };
        //
        var cnt = e.sender.dataSource.total();
        var tab = $('#tabPlanerNotes>.k-tabstrip-items>.k-item.planner-note>.k-link');
        tab.html("<span class='k-sprite cab-png cab-png-note'></span>Записи (" + cnt + ")");
    };

    this.onPlannerOverdueEventGridDataBound = function (e) {        
        if (firstDataBound_overdue) {
            firstDataBound_overdue = false;
            createPlannerOverdueDraggable();
        }
        drawEmptyRow('#plannerOverdueEventGrid');
        //
        var cnt = e.sender.dataSource.total();
        var tab = $('#tabPlanerTask>.k-tabstrip-items>.k-item.planner-overdue>.k-link');
        tab.html("<span class='k-sprite cab-png cab-png-error'></span>Просроченные (" + cnt + ")");
    };

    this.onPlannerStrategEventGridDataBound = function (e) {
        if (firstDataBound_strateg) {
            firstDataBound_strateg = false;
            createPlannerStrategDraggable();
        }
        drawEmptyRow('#plannerStrategEventGrid');
        //
        var cnt = e.sender.dataSource.total();
        var tab = $('#tabPlanerTask>.k-tabstrip-items>.k-item.planner-strateg>.k-link');
        tab.html("<span class='k-sprite cab-png cab-png-monitor-yellow'></span>Стратегические (" + cnt + ")");
    };

    this.onPlannerNoteGridSave = function (e) {        
        if (e.values.description !== "") {
            setTimeout(function () {
                $('#plannerNoteGrid').data('kendoGrid').dataSource.sync()
            });
        } else {            
            e.preventDefault();
        }
    };

    this.onBtnNotesRefreshClick = function (e) {
        refreshNotesGrid();
    };

    this.placeholder = function (element) {        
         return element.clone().addClass("k-state-hover").css("opacity", 0.65);
    };

    this.onPlannerNoteGridSortChange = function (e) {
        var grid = $("#plannerNoteGrid").data("kendoGrid"),
                    skip = grid.dataSource.skip(),
                    oldIndex = e.oldIndex + skip,
                    newIndex = e.newIndex + skip,
                    data = grid.dataSource.data(),
                    dataItem = grid.dataSource.getByUid(e.item.data("uid"));

        grid.dataSource.remove(dataItem);
        grid.dataSource.insert(newIndex, dataItem);

        dataItem.dirty = false;
        
        $.ajax({
            url: '/PlannerNoteReorder',
            type: "POST",            
            data: {
                note_id: dataItem.note_id,
                new_index: newIndex
            }
        });
    };

    this.onSchedulerDataBound = function (e) {
        if (firstDataBound_scheduler) {
            firstDataBound_scheduler = false;            
            scrollToToday();
        };
        createSchedulerDropArea(this);        
        createSchedulerDraggable();

        hideShowUser();

        var view = this.view();        
        view.datesHeader.find("tr:last").prev().hide();
        view.timesHeader.find("tr:last").prev().hide();        

        var events = this.dataSource.view();
        var eventElement;
        var event;
        for (var idx = 0, length = events.length; idx < length; idx++) {
            event = events[idx];
            if (event.priority_is_boss) {
                eventElement = view.element.find("[data-uid=" + event.uid + "]");
                eventElement.css("border-color", "#fa4c25");
            }
        };
        //
        var cnt = e.sender.dataSource.total();
        var tab = $('#tabPlanerTask>.k-tabstrip-items>.k-item.planner-planner>.k-link');
        tab.html("<span class='k-sprite cab-png cab-png-planner'></span>Календарь (" + cnt + ")");
    };

    this.onSchedulerRemove = function (e) {        
        setTimeout(function () {            
            var index = $("#tabPlanerTask").data("kendoTabStrip").select().index();
            refreshTabPlanerTask(index);
        }, 1500);
    };

    this.onSchedulerResizeStart = function (e) {
        var element = $(".k-event[data-uid=" + e.event.uid + "]").first();
        var offset = $(element).offset();
        offset_top_before_resize = Math.round(offset.top);
        offset_left_before_resize = Math.round(offset.left);
        event_start_before_resize = e.event.start;
        event_end_before_resize = e.event.end;
    };

    this.onSchedulerResizeEnd = function (e) {
        var offset = $(e.slot.element).offset();
        offset_top_after_resize = Math.round(offset.top);
        offset_left_after_resize = Math.round(offset.left);
        slot_start_after_resize = e.slot.start;
        slot_end_after_resize = e.slot.end;
        resized = true;
    };

    this.onSchedulerNavigate = function (e) {        
        setTimeout(function () { scrollToToday(); });
    }

    this.sortableHintHandler = function (element) {
        return element.clone().addClass("planner-hint");
    };

    this.onPlannerNoteGridCancel = function (e) {
        if (e.model.isNew() == true) {
            $("#plannerNoteGrid").data("kendoGrid").cancelRow();
        };
    };

    this.onBtnNotesSearchClick = function (e) {
        //
        $('#plannerNoteGrid .k-filter-row').toggle();
    };

    this.onTbUpcomingPlannerToggle = function (e) {
        refreshUpcomingEventGrid();
    };

    this.onSchedulerError = function (e) {
        if (e.errors) {
            var message = "Ошибка:\n";
            $.each(e.errors, function (key, value) {
                if ('errors' in value) {
                    $.each(value.errors, function () {
                        message += this + "\n";
                    });
                }
            });
            alert(message);
            $('#scheduler').getKendoScheduler().cancelEvent();            
        }
    };

    this.onWinTaskEditPlannerRefresh = function (e) {
        var winCrmTask_taskTextHeight = localStorage.winCrmTask_taskTextHeight;
        if (winCrmTask_taskTextHeight) {            
            $('#task_textPlanner').css("height", winCrmTask_taskTextHeight);
        };
        var winCrmTask_taskTextWidth = localStorage.winCrmTask_taskTextWidth;
        if (winCrmTask_taskTextWidth) {
            $('#task_textPlanner').css("width", winCrmTask_taskTextWidth);
        };
    };

    this.onAttrChanged = function () {
        $('#btnTaskOkPlanner').removeClass('btn-default');
        $('#btnTaskOkPlanner').addClass('btn-success');
        $('#btnTaskOkPlanner').removeAttr('disabled');
        //
        $('#btnTaskSavePlanner').removeClass('btn-default');
        $('#btnTaskSavePlanner').addClass('btn-info');
        $('#btnTaskSavePlanner').removeAttr('disabled');
    };

    this.subtaskStateListChanged = function (e) {
        var item = $('#subtaskGridPlanner').data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        item.set('state_id', ddl_val);
        item.set('state_id_ReadOnly', ddl_val);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.subtaskFileListChanged = function (e) {
        var item = $('#subtaskGridPlanner').data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        var ddl_text = this.text();
        item.set('file_id', ddl_val);
        item.set('file_id_ReadOnly', ddl_val);
        item.set('file_name', ddl_text);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.onSubtaskGridPlannerDataBound = function (e) {
        $(".stateListCell").each(function () {
            eval($(this).children("script").last().html());
        });
        $(".fileListCell").each(function () {
            eval($(this).children("script").last().html());
        });
        var grid = $('#subtaskGridPlanner').getKendoGrid();
        var ds = grid.dataSource;
        var task_num_max = ds.aggregates().task_num.max;
        localStorage.subtask_num = task_num_max;
    };

    this.onSubtaskGridPlannerEdit = function (e) {
        e.model.IsNew = true;
        _this.onAttrChanged();
    };

    this.onSubtaskGridPlannerRemove = function (e) {
        setTimeout(function () { $('#subtaskGridPlanner').data('kendoGrid').dataSource.sync() })
    };

    this.onSubtaskGridPlannerExcelExport = function (e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsExcel();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onSubtaskGridPlannerPdfExport = function (e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsPDF();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onUplFilePlannerSuccess = function (e) {
        if (e.operation == 'upload') {
            var grid = $("#taskFilesGridPlanner").data("kendoGrid");
            grid.dataSource.read();
            _this.onAttrChanged();
        };

        return true;
    };

    this.onUplFilePlannerError = function (e) {
        var err = e.XMLHttpRequest.responseText;
        if (err.length <= 200) {
            alert(err);
        };
    };

    this.onSchedulerRequestEnd = function (e) {
        if ((e.type == "create") && (needUpdate_scheduler)) {
            needUpdate_scheduler = false;
            refreshScheduler();
            var index = $("#tabPlanerTask").data("kendoTabStrip").select().index();
            refreshTabPlanerTask(index);            
        };
        if ((e.type == "create") && (needUpdate_notes)) {
            needUpdate_notes = false;
            refreshNotesGrid();
        };
    };

    this.getTaskListData = function (e) {        
        var phrase_val = $('#taskGridPhrase').val();
        return { group_id: curr_page_group_id, phrase: phrase_val };
    };

    this.onPlannerTaskGridDataBound = function (e) {
        var grid = $("#plannerTaskGrid").data("kendoGrid");
        //
        if (firstDataBound_task) {
            firstDataBound_task = false;
            createTaskListDraggable();            
            //
            var gridSortData = $.parseJSON(html_taskGridSortOptions);
            var gridFilterData = $.parseJSON(html_taskGridFilterOptions);
            if ((gridSortData) || (gridFilterData)) {
                grid.dataSource.filter(gridFilterData);
                grid.dataSource.sort(gridSortData);
            };
            var pageSizeDropDownList = grid.wrapper.children(".k-grid-pager").find("select").data("kendoDropDownList");
            pageSizeDropDownList.bind("change", function (e) {
                saveUserConfig(null, null);
            });
            if (curr_page_group_id != -1) {
                grid.hideColumn(1);
            } else {
                grid.showColumn(1);
            };
            setFilterHeader();
        }
        drawEmptyRow('#plannerTaskGrid');
        //        
        var row = null;
        var cnt = 0;
        var data = grid.dataSource.data();
        for (var x = 0; x < data.length; x++) {
            var dataItem = data[x];
            var tr = $("#plannerTaskGrid").find("[data-uid='" + dataItem.uid + "']");
            // state_name
            var col_ind = settings_state_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.state_fore_color) {

                    var is_row_colored = taskGridColorColumn_state_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.state_fore_color);
                        if ((dataItem.state_fore_color == 'black') || (dataItem.state_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.state_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.state_fore_color);
                }
            }
            //
            cnt = cnt + 1;
        };
        //
        var cnt = e.sender.dataSource.total();
        var tab = $('#tabPlanerTask>.k-tabstrip-items>.k-item.planner-task>.k-link');
        tab.html("<span class='k-sprite cab-png cab-png-task'></span>Задачи (" + cnt + ")");
        //
        fillTaskGridCombo(curr_page_group_id);
    };

    this.onPlannerEventSplitterResize = function (e) {        
        var gridId = '#plannerTaskGrid';
        var grid_max_height = $('#left-bottom-pane').height() - $(gridId + '>.k-grid-toolbar').height() - $(gridId + '>.k-grid-header').height() - $(gridId + '>.k-grid-pager').height() - 150;
        $(gridId + '>.k-grid-content').css('max-height', grid_max_height + 'px');
    };

    this.onPlannerNoteGridError = function (e) {
        if (e.errors) {
            var message = "Ошибка:\n";
            $.each(e.errors, function (key, value) {
                if ('errors' in value) {
                    $.each(value.errors, function () {
                        message += this + "\n";
                    });
                }
            });
            alert(message);
            $("#plannerNoteGrid").getKendoGrid().dataSource.read();            
        }
    };

    this.onTabPlanerTaskSelect = function (e) {
        var index = $(e.item).index();
        refreshTabPlanerTask(index);
    };

    //****************
    // task-planner-edit (public)
    //****************

    this.onTaskNumCellClick = function (task_id) {
        setTimeout(function () {
            if (!dblclickFired) {
                saveUserConfig(null, function () { window.location = '/TaskEdit?task_id=' + task_id; });
            }
        }, 300);
    };

    this.onTaskListMenuSelect = function (e) {
        e.preventDefault();
        var grid = $("#plannerTaskGrid").data("kendoGrid");
        var menu_item = e.item;
        if (menu_item) {
            var menu_item_id = $(menu_item).attr('id');
            if (menu_item_id == 'menu-task-all') {
                curr_page_group_id = -1;
            } else if (menu_item_id == 'menu-task-bucket') {
                curr_page_group_id = 0;
            } else if (menu_item_id == 'menu-task-prepared') {
                curr_page_group_id = 1;
            } else if (menu_item_id == 'menu-task-fixed') {
                curr_page_group_id = 2;
            } else if (menu_item_id == 'menu-task-approved') {
                curr_page_group_id = 3;
            } else if (menu_item_id == 'menu-task-closed') {
                curr_page_group_id = 4;
            } else if (menu_item_id == 'menu-task-version') {
                curr_page_group_id = 5;
            } else if (menu_item_id == 'menu-task-project') {
                curr_page_group_id = 6;
            } else {
                return false;
            };
            //
            taskUserFilterSave();
            //
            if (curr_page_group_id != -1) {
                grid.hideColumn(1);
            } else {
                grid.showColumn(1);
            };
            checkedIds = {};
            toggleVisibility();
            grid.dataSource.read();
            fillTaskGridCombo(curr_page_group_id);
            this.element.find(".k-state-selected").removeClass('k-state-selected');
            $(e.item).addClass('k-state-selected');
            return true;
            //
        } else {
            return false;
        };
    };

    this.onTaskGridComboChange = function (e) {
        var ddl = $("#taskGridCombo").data("kendoDropDownList");
        if (!ddl)
            return false;

        taskAction = null;
        var dataItem = ddl.dataItem();
        if (dataItem) {
            taskAction = dataItem['operation_id'];
            if (taskAction == null) {
                ddl.select(0);
                return false;
            }

            id_list = '';
            $.each(checkedIds, function (key, value) {
                if (value) {
                    id_list = id_list + key + ',';
                }
            });
            if (!id_list) {
                ddl.select(0);
                alert('Нет отмеченных задач !');
                return false;
            };

            if (taskAction != 15) {
                $.ajax({
                    type: "POST",
                    url: "Crm/TaskOperationExecute",
                    data: JSON.stringify({ task_id_list: id_list, operation_id: taskAction }),
                    contentType: 'application/json; charset=windows-1251',
                    success: function (task_res) {
                        checkedIds = {};
                        ddl.select(0);
                        refreshTaskGrid();
                    },
                    error: function (task_res) {
                        ddl.select(0);
                        alert('Ошибка при выполнении операции со списком задач');
                    }
                });
            } else {                
                $("#winTaskBatch").data("kendoWindow").center().open();
            }
        }
    };

    //
    this.onCabGridCheckboxHeaderClick = function (e) {
        var state = $(e).is(':checked');
        var grid = $("#plannerTaskGrid").data("kendoGrid");
        var view = grid.dataSource.view();

        for (var i = 0; i < view.length; i++) {
            var dataRow = view[i];
            var elementRow = grid.table.find(".cab-grid-checkbox")[i];
            if (elementRow != null) {
                var checked = elementRow.checked,
                    row = $(elementRow).closest("tr"),
                    dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));

                checkedIds[dataItem.task_id] = state;
                if (state) {
                    elementRow.checked = true;
                    row.addClass("k-info-colored");
                } else {
                    elementRow.checked = false;
                    row.removeClass("k-info-colored");
                }
            }
        };
    };
    //
    this.onCabGridCheckboxClick = function (e) {
        var checked = $(e).is(':checked'),
            row = $(e).closest("tr"),
            grid = $("#plannerTaskGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);

        checkedIds[dataItem.task_id] = checked;
        if (checked) {
            row.addClass("k-info-colored");
        } else {
            row.removeClass("k-info-colored");
        }
    };
    //
    this.onAddTaskBtnClick = function () {
        selected_data_item = null;
        selected_row = null;        
        curr_task_id = 0;
        var win = $('#winTaskEditPlanner').data('kendoWindow');
        $('#winTaskEditPlanner').html("");
        win.title('Новая задача');
        win.refresh({
            url: 'Crm/TaskEditPlanner',
            data: { task_id: null }
        });

        win.center().open();
        $('#winTaskEditPlanner').closest(".k-window").css({
            top: $(document).scrollTop()
        });

        return false;
    };
    //
    this.onAddTaskNewBtnClick = function (btn) {
        window.location = '/TaskEdit?task_id=0';
    };
    //
    this.onGridPhraseSearchBtnClick = function (btn) {
        refreshTaskGrid();
        return false;
    }
    //
    this.onGridConfigBtnClick = function (btn) {
        var win = $("#winTaskGridConfig").data("kendoWindow");
        win.center().open();
        return false;
    };
    //
    this.onSortChange = function (e) {
        var grid = $("#taskGridColumnSettingsGrid").data("kendoGrid"),
                    skip = grid.dataSource.skip(),
                    oldIndex = e.oldIndex + skip,
                    newIndex = e.newIndex + skip,
                    data = grid.dataSource.data(),
                    dataItem = grid.dataSource.getByUid(e.item.data("uid"));

        grid.dataSource.remove(dataItem);
        grid.dataSource.insert(newIndex, dataItem);
    };

    this.onTaskNoteTabStripPlannerSelect = function (e) {
        // !!!
    };

    //****************
    // private
    //****************

    function refreshScheduler() {
        $('#scheduler').data('kendoScheduler').dataSource.read();
    };

    function refreshSchedulerResource() {
        var ddl = $('#userList').data('kendoDropDownList');
        var usr = ddl.value();
        var usr_int = -1;
        if (usr)
            usr_int = parseInt(usr);
        var scheduler = $("#scheduler").data("kendoScheduler");        
        var res = scheduler.resources[0];

        var dataSourceConfig = {
            data: ddl.dataSource.data()
        };
        var ds = new kendo.data.DataSource(dataSourceConfig);

        if (usr_int >= 0) {
            ds.filter(
                    {
                        operator: "eq",
                        field: "user_id",
                        value: usr_int
                    });
        } else {            
            $.ajax({
                url: '/GetPlannerEventExecUserList',
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(_getEventFilter()),
                async: false,
                success: function (res) {
                    var currFilter = { logic: "or", filters: [] };
                    $.each(res.ids, function (key, value) {
                        currFilter.filters.push(
                            {
                                operator: "eq",
                                field: "user_id",
                                value: value,
                            });
                    });
                    ds.filter(currFilter);
                },
                error: function (res) {
                    alert(((res) && (res.mess)) ? res.mess : 'Ошибка при получении списка исполнителей ! Пожалуйста обновите страницу');
                }
            });
        };

        res.dataSource = ds;
        scheduler.view(scheduler.view().name);
    };

    function refreshUpcomingEventGrid() {
        $('#plannerUpcomingEventGrid').data('kendoGrid').dataSource.read();
    };

    function refreshNotesGrid() {
        $('#plannerNoteGrid').data('kendoGrid').dataSource.read();
    };

    function refreshOverdueEventGrid() {
        $('#plannerOverdueEventGrid').data('kendoGrid').dataSource.read();
    };

    function refreshStrategEventGrid() {
        $('#plannerStrategEventGrid').data('kendoGrid').dataSource.read();
    };

    function refreshTaskGrid() {
        $('#plannerTaskGrid').data('kendoGrid').dataSource.read();
    };

    function createSchedulerDropArea(scheduler) {
        // !!!
        return;
        $('#scheduler>.k-scheduler-layout').kendoDropTargetArea({
            //filter: ".k-scheduler-table td, .k-event",
            filter: ".k-scheduler-table td",
            drop: function (e) {
                var gridPlanner = $("#plannerNoteGrid").data("kendoGrid");
                var gridUpcoming = $("#plannerUpcomingEventGrid").data("kendoGrid");
                var gridTask = $("#plannerTaskGrid").data("kendoGrid");
                var gridOverdue = $("#plannerOverdueEventGrid").data("kendoGrid");
                var gridStrateg = $("#plannerStrategEventGrid").data("kendoGrid");
                var scheduler = $('#scheduler').data('kendoScheduler');
                var offset = $(e.dropTarget).offset();
                var slot = scheduler.slotByPosition(offset.left, offset.top);                
                //var dataItem = grid.dataItem(grid.select());
                var row = e.draggable.currentTarget;
                var dataItem_fromNotes = true;
                var dataItem_fromUpcoming = false;
                var dataItem_fromTask = false;
                var dataItem_fromOverdue = false;
                var dataItem_fromStrateg = false;
                var dataItem_fromScheduler = false;
                //
                var dataItem = gridPlanner.dataSource.getByUid(row.data("uid"));
                if ((!dataItem) || (dataItem.length <= 0) || (!dataItem.note_id)) {
                    dataItem = gridUpcoming.dataSource.getByUid(row.data("uid"));
                    dataItem_fromNotes = false;
                    dataItem_fromUpcoming = true;
                };
                if ((!dataItem) || (dataItem.length <= 0)) {                    
                    dataItem_fromUpcoming = false;
                    dataItem_fromScheduler = true;
                    
                    var container = $(row).parents('.k-event').first();
                    var uid = scheduler.wrapper.find(container).data("uid");
                    dataItem = scheduler.occurrenceByUid(uid);
                    if ((!dataItem) || (dataItem.length <= 0)) {
                        dataItem = gridTask.dataSource.getByUid(row.data("uid"));
                        dataItem_fromScheduler = false;
                        dataItem_fromTask = true;
                        if ((!dataItem) || (dataItem.length <= 0)) {
                            dataItem_fromTask = false;
                            dataItem_fromOverdue = true;
                            dataItem = gridOverdue.dataSource.getByUid(row.data("uid"));
                            if ((!dataItem) || (dataItem.length <= 0)) {
                                dataItem_fromOverdue = false;
                                dataItem_fromStrateg = true;
                                dataItem = gridStrateg.dataSource.getByUid(row.data("uid"));
                                if ((!dataItem) || (dataItem.length <= 0)) {
                                    dataItem_fromStrateg = false;
                                }
                            };
                        };
                    };
                };

                var isAllDay = slot ? slot.isDaySlot : true;
                if (!$('#scheduler').children('.k-scheduler-layout').first().hasClass('k-scheduler-dayview')) {
                    isAllDay = true;
                };

                var new_title = null;
                var new_description = null;
                var new_task_id = null;                
                var new_event_priority_id = null;                
                var new_is_public = false;
                var new_is_public_global = false;
                var new_rate = null;
                var new_event_state_id = 0;
                var new_progress = null;
                var new_result = null;
                var new_event_group_id = 1;
                if (dataItem_fromNotes) {
                    new_title = dataItem.title;
                    new_description = dataItem.description;
                    new_task_id = dataItem.task_id;
                    new_event_priority_id = 0;
                    new_is_public = dataItem.is_public;
                    new_is_public_global = dataItem.is_public_global;
                    new_rate = 0;
                    new_event_state_id = dataItem.event_state_id;
                    new_progress = dataItem.progress;
                    new_result = dataItem.result;
                    new_event_group_id = 1;
                    needUpdate_notes = true;
                } else if (dataItem_fromUpcoming) {
                    new_title = dataItem.Title;
                    new_description = dataItem.Description;
                    new_task_id = dataItem.task_id;
                    new_event_priority_id = dataItem.event_priority_id;
                    new_is_public = dataItem.is_public;
                    new_is_public_global = dataItem.is_public_global;
                    new_rate = dataItem.rate;
                    new_event_state_id = dataItem.event_state_id;
                    new_progress = dataItem.progress;
                    new_result = dataItem.result;
                    new_event_group_id = dataItem.event_group_id;
                } else if (dataItem_fromTask) {
                    new_title = dataItem.task_name;
                    new_description = dataItem.task_text;
                    new_task_id = dataItem.task_id;
                    new_event_priority_id = 0;
                    new_is_public = true;
                    new_is_public_global = false;
                    new_rate = null;
                    new_event_state_id = 0;
                    new_progress = null;
                    new_result = null;
                    new_event_group_id = 1;
                } else if (dataItem_fromOverdue) {
                    new_title = dataItem.Title;
                    new_description = dataItem.Description;
                    new_task_id = dataItem.task_id;
                    new_event_priority_id = dataItem.event_priority_id;
                    new_is_public = dataItem.is_public;
                    new_is_public_global = dataItem.is_public_global;
                    new_rate = dataItem.rate;
                    new_event_state_id = dataItem.event_state_id;
                    new_progress = dataItem.progress;
                    new_result = dataItem.result;
                    new_event_group_id = dataItem.event_group_id;
                } else if (dataItem_fromStrateg) {
                    new_title = dataItem.Title;
                    new_description = dataItem.Description;
                    new_task_id = dataItem.task_id;
                    new_event_priority_id = dataItem.event_priority_id;
                    new_is_public = dataItem.is_public;
                    new_is_public_global = dataItem.is_public_global;
                    new_rate = dataItem.rate;
                    new_event_state_id = dataItem.event_state_id;
                    new_progress = dataItem.progress;
                    new_result = dataItem.result;
                    new_event_group_id = dataItem.event_group_id;
                };

               

                if ((dataItem && slot) && ((dataItem_fromNotes) || (dataItem_fromUpcoming) || (dataItem_fromTask) || (dataItem_fromOverdue) || (dataItem_fromStrateg))) {
                    var offsetMiliseconds = new Date().getTimezoneOffset() * 60000;
                    var newEvent = {
                        title: new_title,
                        end: isAllDay ? slot.startDate : slot.endDate,                        
                        start: slot.startDate,
                        isAllDay: isAllDay,
                        description: new_description,
                        exec_user_id: currUser_CabUserId,                        
                        event_priority_id: new_event_priority_id,
                        task_id: new_task_id,
                        is_public: new_is_public,
                        is_public_global: new_is_public_global,
                        rate: new_rate,
                        event_state_id: new_event_state_id,
                        progress: new_progress,
                        result: new_result,
                        event_group_id: new_event_group_id,
                        SourceNoteId: dataItem.note_id,                        
                        SourceUpcomingEventId: dataItem_fromUpcoming ? dataItem.event_id : null,
                        SourceOverdueEventId: dataItem_fromOverdue ? dataItem.event_id : null,
                        SourceStrategEventId: dataItem_fromStrateg ? dataItem.event_id : null
                    };
                    
                    scheduler.dataSource.add(newEvent);
                    needUpdate_scheduler = true;
                    setTimeout(function () { scheduler.dataSource.sync(); });

                } else if ((dataItem && slot) && (dataItem_fromScheduler)) {                    
                    var existing_event = scheduler.dataSource.get(dataItem.event_id);
                    //var new_end = slot.isDaySlot ? slot.startDate : slot.endDate;
                    //var eventDays = diffDays(existing_event.start, existing_event.end) - 1;
                    var eventDays = diffDays(existing_event.start, existing_event.end);
                    var new_end = addDays((isAllDay ? slot.startDate : slot.endDate), eventDays);
                    if (isAllDay) {
                        existing_event.set('end', new_end);
                        existing_event.set('start', slot.startDate);
                    } else {
                        existing_event.set('end', slot.endDate);
                        existing_event.set('start', slot.startDate);
                    }
                    existing_event.set('isAllDay', isAllDay);
                    setTimeout(function () { scheduler.dataSource.sync(); });
                }
            }
        });
    };

    function createNotesDropArea(notesGrid) {
        // !!!
        return;
        notesGrid.content.kendoDropTargetArea({
            filter: "tbody > tr",
            drop: function (e) {
                onPlannerNoteGridDrop(e);
            }
        });
        //$('#right-pane').kendoDropTargetArea({
        $('#plannerMainSplitter').kendoDropTargetArea({            
            //filter: "#right-bottom-pane",
            filter: "#right-pane",
            drop: function (e) {
                onPlannerNoteGridDrop(e);
            }
        });
    };

    function onPlannerNoteGridDrop(e) {
        var gridNotes = $("#plannerNoteGrid").data("kendoGrid");
        var gridUpcoming = $("#plannerUpcomingEventGrid").data("kendoGrid");
        var gridTask = $("#plannerTaskGrid").data("kendoGrid");
        var gridOverdue = $("#plannerOverdueEventGrid").data("kendoGrid");
        var gridStrateg = $("#plannerStrategEventGrid").data("kendoGrid");
        var scheduler = $('#scheduler').data('kendoScheduler');
        var row = e.draggable.currentTarget;
        var uid = null;
        var dataItem_fromNotes = true;
        var dataItem_fromUpcoming = false;
        var dataItem_fromTask = false;
        var dataItem_fromOverdue = false;
        var dataItem_fromStrateg = false;
        var dataItem_fromScheduler = false;
        var dataItem = gridNotes.dataSource.getByUid(row.data("uid"));
        if (!dataItem) {
            dataItem_fromNotes = false;
            dataItem_fromUpcoming = true;
            dataItem = gridUpcoming.dataSource.getByUid(row.data("uid"));
            if (!dataItem) {
                dataItem_fromUpcoming = false;
                dataItem_fromScheduler = true;
                var container = $(row).parents('.k-event').first();
                uid = scheduler.wrapper.find(container).data("uid");
                dataItem = scheduler.occurrenceByUid(uid);
                if (!dataItem) {
                    dataItem_fromScheduler = false;
                    dataItem_fromTask = true;
                    dataItem = gridTask.dataSource.getByUid(row.data("uid"));
                    if (!dataItem) {
                        dataItem_fromTask = false;
                        dataItem_fromOverdue = true;
                        dataItem = gridOverdue.dataSource.getByUid(row.data("uid"));
                        if ((!dataItem) || (dataItem.length <= 0)) {
                            dataItem_fromOverdue = false;
                            dataItem_fromStrateg = true;
                            dataItem = gridStrateg.dataSource.getByUid(row.data("uid"));
                            if ((!dataItem) || (dataItem.length <= 0)) {
                                dataItem_fromStrateg = false;
                            }
                        };
                    }
                };
            }
        };

        var dataRows = gridNotes.items();
        var newIndex = dataRows.index(e.dropTarget);
        if (!newIndex)
            newIndex = 0;

        if (dataItem_fromNotes) {

            gridNotes.dataSource.remove(dataItem);
            gridNotes.dataSource.insert(newIndex, dataItem);

            dataItem.dirty = false;

            $.ajax({
                url: '/PlannerNoteReorder',
                type: "POST",
                data: {
                    note_id: dataItem.note_id,
                    new_index: newIndex
                }
            });
        } else if (dataItem_fromUpcoming) {
            gridNotes.dataSource.insert(0, {
                description: dataItem.Description,
                title: dataItem.Title,
                progress: dataItem.progress,
                result: dataItem.result,
                task_id: dataItem.task_id,
                task_num: dataItem.task_num,
                exec_user_id: currUser_CabUserId,
                is_public: dataItem.is_public,
                is_public_global: dataItem.is_public_global,
                event_state_id: dataItem.event_state_id,
                state_id: dataItem.event_state_id,
                state_name: dataItem.state_name,
                SourceUpcomingEventId: dataItem.event_id,
            });

            var event = scheduler.dataSource.get(dataItem.event_id);
            var options = scheduler.options;
            var options_orig = $.extend(true, {}, options);
            options.editable.confirmation = false;
            scheduler.options = options;
            scheduler.removeEvent(event);
            scheduler.options = options_orig;

            gridUpcoming.removeRow(row);
            gridNotes.dataSource.sync();
        } else if (dataItem_fromScheduler) {
            gridNotes.dataSource.insert(0, {
                description: dataItem.description,
                title: dataItem.title,
                progress: dataItem.progress,
                result: dataItem.result,
                task_id: dataItem.task_id,
                task_num: dataItem.task_num,
                exec_user_id: currUser_CabUserId,
                is_public: dataItem.is_public,
                is_public_global: dataItem.is_public_global,
                event_state_id: dataItem.event_state_id,
                state_id: dataItem.event_state_id,
                state_name: dataItem.state_name
            });

            var options = scheduler.options;
            var options_orig = $.extend(true, {}, options);
            options.editable.confirmation = false;
            scheduler.options = options;
            scheduler.removeEvent(uid);
            scheduler.options = options_orig;

            gridNotes.dataSource.sync();
        } else if (dataItem_fromTask) {
            gridNotes.dataSource.insert(0, {
                description: dataItem.task_text,
                title: dataItem.task_name,
                progress: null,
                result: null,
                task_id: dataItem.task_id,
                task_num: dataItem.task_num,
                exec_user_id: currUser_CabUserId,
                is_public: true,
                is_public_global: false,
                event_state_id: dataItem.state_id,
                state_id: dataItem.event_state_id,
                state_name: dataItem.state_name,
                SourceUpcomingEventId: null
            });

            gridNotes.dataSource.sync();
        } else if (dataItem_fromOverdue) {
            gridNotes.dataSource.insert(0, {
                description: dataItem.Description,
                title: dataItem.Title,
                progress: dataItem.progress,
                result: dataItem.result,
                task_id: dataItem.task_id,
                task_num: dataItem.task_num,
                exec_user_id: currUser_CabUserId,
                is_public: dataItem.is_public,
                is_public_global: dataItem.is_public_global,
                event_state_id: dataItem.event_state_id,
                state_id: dataItem.event_state_id,
                state_name: dataItem.state_name,
                SourceUpcomingEventId: dataItem.event_id,
            });
            gridOverdue.removeRow(row);
            gridNotes.dataSource.sync();
        } else if (dataItem_fromStrateg) {
            gridNotes.dataSource.insert(0, {
                description: dataItem.Description,
                title: dataItem.Title,
                progress: dataItem.progress,
                result: dataItem.result,
                task_id: dataItem.task_id,
                task_num: dataItem.task_num,
                exec_user_id: currUser_CabUserId,
                is_public: dataItem.is_public,
                is_public_global: dataItem.is_public_global,
                event_state_id: dataItem.event_state_id,
                state_id: dataItem.event_state_id,
                state_name: dataItem.state_name,
                SourceUpcomingEventId: dataItem.event_id,
            });
            gridStrateg.removeRow(row);
            gridNotes.dataSource.sync();
        };
    };

    function createUpcomingDropArea(upcomingGrid) {
        // !!!
        return;
        upcomingGrid.content.kendoDropTargetArea({
            filter: "tbody > tr",
            drop: function (e) {
                onPlannerUpcomingEventGridDrop(e);
            }
        });
        $('#right-pane').kendoDropTargetArea({
            filter: "#right-top-pane",
            drop: function (e) {
                onPlannerUpcomingEventGridDrop(e);
            }
        });
    };

    function onPlannerUpcomingEventGridDrop(e) {
        var gridNotes = $("#plannerNoteGrid").data("kendoGrid");
        var gridUpcoming = $("#plannerUpcomingEventGrid").data("kendoGrid");
        var gridTask = $("#plannerTaskGrid").data("kendoGrid");
        var row = e.draggable.currentTarget;
        var dataItem_fromNotes = true;
        var dataItem_fromScheduler = false;
        var dataItem_fromTask = false;
        var dataItem = gridNotes.dataSource.getByUid(row.data("uid"));
        if (!dataItem) {
            var dataItem_fromNotes = false;
            dataItem_fromScheduler = true;
            var scheduler = $('#scheduler').data('kendoScheduler');
            var container = $(row).parents('.k-event').first();
            var uid = scheduler.wrapper.find(container).data("uid");
            var dataItem = scheduler.occurrenceByUid(uid);
            if (!dataItem) {
                dataItem_fromScheduler = false;
                dataItem_fromTask = true;
                dataItem = gridTask.dataSource.getByUid(row.data("uid"));
                if (!dataItem) {
                    dataItem_fromTask = false;
                }
            };
        };

        var dataItem_target = gridUpcoming.dataItem(e.dropTarget);

        if ((dataItem_fromNotes) || (dataItem_fromScheduler) || (dataItem_fromTask)) {

            var curr_upcoming_event_date = new Date();
            var curr_days_add = 1;

            if (dataItem_target) {
                curr_upcoming_event_date = dataItem_target.Start;
                curr_days_add = null;
            } else {
                curr_upcoming_event_date = null;
                var filter_id = $('#tbUpcomingPlanner .k-button-group>.k-toggle-button.k-state-active').attr('id');
                if (filter_id == 'btn-upcoming-event-filter-tomorrow') {
                    curr_days_add = 1;
                } else if (filter_id == 'btn-upcoming-event-filter-3days') {
                    curr_days_add = 3;
                } else if (filter_id == 'btn-upcoming-event-filter-7days') {
                    curr_days_add = 7;
                };
            }

            $.ajax({
                url: '/UpcomingEventAdd',
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify({
                    note_id: dataItem.note_id, event_id: dataItem.event_id, task_id: dataItem.task_id,
                    fromNotes: dataItem_fromNotes, fromScheduler: dataItem_fromScheduler, fromTask: dataItem_fromTask,
                    upcoming_event_date: curr_upcoming_event_date, days_add: curr_days_add, exec_user_id: currUser_CabUserId
                }),
                success: function (res) {
                    if ((!res) || (res.err)) {
                        alert(((res) && (res.mess)) ? res.mess : 'Ошибка при добавлении события ! Пожалуйста обновите страницу');
                    } else {
                        refreshUpcomingEventGrid();
                        refreshScheduler();
                    };
                },
                error: function (res) {
                    alert(((res) && (res.mess)) ? res.mess : 'Ошибка при добавлении события ! Пожалуйста обновите страницу');
                }
            });

            if (dataItem_fromNotes) {
                gridNotes.dataSource.remove(dataItem);
                gridNotes.dataSource.sync();
            };
        };
    }
    
    function createPlannerNoteDraggable() {
        // !!!
        return;
        $("#plannerNoteGrid").kendoDraggable({
            filter: "tbody > tr",            
            hint: function (element) {
                var grid = $("#plannerNoteGrid").data("kendoGrid");
                var dataItem = grid.dataItem(element);
                var tooltipHtml = "<div class='k-event' id='dragTooltip'><div class='k-event-template' style='background:#b8f7fb;'>" + dataItem.description + "</div></div>";
                return $(tooltipHtml).css("width", 300);
            }            
        });
    };

    function createPlannerUpcomingDraggable() {
        // !!!
        return;
        $("#plannerUpcomingEventGrid").kendoDraggable({
            filter: "tbody > tr",
            hint: function (element) {
                var grid = $("#plannerUpcomingEventGrid").data("kendoGrid");
                var dataItem = grid.dataItem(element);
                var tooltipHtml = "<div class='k-event' id='dragTooltip'><div class='k-event-template' style='background:#b8f7fb;'>" + dataItem.Title + "</div></div>";
                return $(tooltipHtml).css("width", 300);
            }
        });
    };
    
    function createSchedulerDraggable() {
        // !!!
        return;
        $("#scheduler").kendoDraggable({
            //filter: ".k-event:not(.k-resize-handle)",
            //filter: ".planner-event > p",
            filter: ".planner-event",
            hint: function (element) {
                var scheduler = $("#scheduler").data("kendoScheduler");
                var container = $(element).parents('.k-event').first();                
                var uid = scheduler.wrapper.find(container).data("uid");
                var event = scheduler.occurrenceByUid(uid);
                return container.clone().css({
                    "opacity": 0.6
                });
            }
        });
    };

    function createTaskListDraggable() {
        // !!!
        return;
        var gridRowOffset = $("#plannerTaskGrid").data("kendoGrid").tbody.find("tr:first").offset();
        $("#plannerTaskGrid").kendoDraggable({
            filter: "tbody > tr",
            dragstart: function (e) {
                //add margin to position correctly the tooltip under the pointer
                $("#dragTooltip").css("margin-left", e.clientX - gridRowOffset.left - 50);
            },
            hint: function (element) {
                var grid = $("#plannerTaskGrid").data("kendoGrid");
                var dataItem = grid.dataItem(element);
                //var tooltipHtml = "<div class='k-event' id='dragTooltip'><div class='k-event-template' style='background:#b8f7fb;'>" + dataItem.task_num + dataItem.task_name + "</div></div>";
                var tooltipHtml = "<div class='k-event' id='dragTooltip'><div class='k-event-template k-scheduler-dayview planner-event'>" + dataItem.task_num + dataItem.task_name + "</div></div>";
                return $(tooltipHtml).css("width", 300);
            }
        });
    };

    function createPlannerOverdueDraggable() {
        // !!!
        return;
        var gridRowOffset = $("#plannerOverdueEventGrid").data("kendoGrid").tbody.find("tr:first").offset();
        $("#plannerOverdueEventGrid").kendoDraggable({
            filter: "tbody > tr",
            dragstart: function (e) {                
                $("#dragTooltip").css("margin-left", e.clientX - gridRowOffset.left - 50);
            },
            hint: function (element) {
                var grid = $("#plannerOverdueEventGrid").data("kendoGrid");
                var dataItem = grid.dataItem(element);                
                var tooltipHtml = "<div class='k-event' id='dragTooltip'><div class='k-event-template k-scheduler-dayview planner-event'>" + dataItem.Title + "</div></div>";
                return $(tooltipHtml).css("width", 300);
            }
        });
    };
    
    function createPlannerStrategDraggable() {
        // !!!
        return;
        var gridRowOffset = $("#plannerStrategEventGrid").data("kendoGrid").tbody.find("tr:first").offset();
        $("#plannerStrategEventGrid").kendoDraggable({
            filter: "tbody > tr",
            dragstart: function (e) {
                $("#dragTooltip").css("margin-left", e.clientX - gridRowOffset.left - 50);
            },
            hint: function (element) {
                var grid = $("#plannerStrategEventGrid").data("kendoGrid");
                var dataItem = grid.dataItem(element);
                var tooltipHtml = "<div class='k-event' id='dragTooltip'><div class='k-event-template k-scheduler-dayview planner-event'>" + dataItem.Title + "</div></div>";
                return $(tooltipHtml).css("width", 300);
            }
        });
    };

    function openPlannerNote(curr_note_id) {
        var win = $('#winPlannerNoteEdit').data('kendoWindow');
        $('#winPlannerNoteEdit').html("");
        win.refresh({
            url: 'PlannerNoteEditWin',
            data: { note_id: curr_note_id }
        });

        win.center().open();
        
        $('#winPlannerNoteEdit').closest(".k-window").css({
            top: $(document).scrollTop()
        });
        
    };

    function hideShowUser() {
        if (currUser_CabUserId > 0) {
            $('.exec-user').addClass('hidden');
        } else {
            $('.exec-user').removeClass('hidden');
        };
    };

    function _getEventFilter() {
        var scheduler = $('#scheduler').getKendoScheduler();
        var public_filter_mode = 0;
        var filter_id = $('#plannerToolBar .k-button-group>.k-toggle-button.k-state-active').attr('id');
        if (filter_id == 'btn-planner-event-my') {
            public_filter_mode = 1;
        };
        return {
            date_beg: scheduler.view().startDate(),
            date_end: scheduler.view().endDate(),
            exec_user_id: currUser_CabUserId,
            public_filter_mode: public_filter_mode
        };
    };

    function taskUserFilterSave() {
        var filter_project_values = $("#filterProjectList").getKendoMultiSelect().value();
        var filter_client_values = $("#filterClientList").getKendoMultiSelect().value();
        var filter_priority_values = $("#filterPriorityList").getKendoMultiSelect().value();
        var filter_state_values = $("#filterStateList").getKendoMultiSelect().value();
        var filter_exec_user_values = $("#filterExecUserList").getKendoMultiSelect().value();
        var filter_assigned_user_values = $("#filterAssignedUserList").getKendoMultiSelect().value();
        var filter_required_date1_value = $("#filterRequiredDate1").getKendoDatePicker().value();
        var filter_required_date2_value = $("#filterRequiredDate2").getKendoDatePicker().value();
        var filter_repair_date1_value = $("#filterRepairDate1").getKendoDatePicker().value();
        var filter_repair_date2_value = $("#filterRepairDate2").getKendoDatePicker().value();
        var filter_fin_cost_value = null;
        var filter_is_moderated_value = $("#filterIsModerated").is(':checked');
        var filter_batch_values = $("#filterBatchList").getKendoMultiSelect().value();

        var filter_project_ids = '';
        var filter_client_ids = '';
        var filter_priority_ids = '';
        var filter_state_ids = '';
        var filter_exec_user_ids = '';
        var filter_assigned_user_ids = '';
        var filter_batch_ids = '';


        var grid = $("#plannerTaskGrid").data("kendoGrid");
        var gridFilter = null;
        if (grid.dataSource.filter() != null) {
            gridFilter = { logic: "and", filters: [] };
        } else {
            gridFilter = { logic: "and", filters: [] };
        };

        var currFilter = null;
        var ok1 = false;
        var ok2 = false;

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_project_values) {
            $.each(filter_project_values, function (i, v) {
                currFilter.filters.push({ field: "project_id", operator: "eq", value: v });
                filter_project_ids = filter_project_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_client_values) {
            $.each(filter_client_values, function (i, v) {
                currFilter.filters.push({ field: "client_id", operator: "eq", value: v });
                filter_client_ids = filter_client_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_priority_values) {
            $.each(filter_priority_values, function (i, v) {
                currFilter.filters.push({ field: "priority_id", operator: "eq", value: v });
                filter_priority_ids = filter_priority_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_state_values) {
            $.each(filter_state_values, function (i, v) {
                currFilter.filters.push({ field: "state_id", operator: "eq", value: v });
                filter_state_ids = filter_state_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_assigned_user_values) {
            $.each(filter_assigned_user_values, function (i, v) {
                currFilter.filters.push({ field: "assigned_user_id", operator: "eq", value: v });
                filter_assigned_user_ids = filter_assigned_user_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_exec_user_values) {
            $.each(filter_exec_user_values, function (i, v) {
                currFilter.filters.push({ field: "exec_user_id", operator: "eq", value: v });
                filter_exec_user_ids = filter_exec_user_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_required_date1_value) {
            currFilter.filters.push({ field: "required_date", operator: "gte", value: filter_required_date1_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_required_date2_value) {
            currFilter.filters.push({ field: "required_date", operator: "lte", value: filter_required_date2_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_repair_date1_value) {
            currFilter.filters.push({ field: "repair_date", operator: "gte", value: filter_repair_date1_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_repair_date2_value) {
            currFilter.filters.push({ field: "repair_date", operator: "lte", value: filter_repair_date2_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_fin_cost_value) {
            currFilter.filters.push({ field: "fin_cost", operator: "gte", value: filter_fin_cost_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_is_moderated_value) {
            currFilter.filters.push({ field: "moderator_user_id", operator: "gt", value: 0 });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_batch_values) {
            $.each(filter_batch_values, function (i, v) {
                currFilter.filters.push({ field: "batch_id", operator: "eq", value: v });
                filter_batch_ids = filter_batch_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        grid.dataSource.filter(gridFilter);
        saveUserConfig(null, null);

        $.ajax({
            type: "POST",
            url: "Crm/TaskUserFilterSave",
            data: JSON.stringify({
                grid_id: 1
                , raw_filter: JSON.stringify(gridFilter)
                , project_filter: filter_project_ids
                , client_filter: filter_client_ids
                , priority_filter: filter_priority_ids
                , state_filter: filter_state_ids
                , exec_user_filter: filter_exec_user_ids
                , assigned_user_filter: filter_assigned_user_ids
                , required_date1_filter: filter_required_date1_value
                , required_date2_filter: filter_required_date2_value
                , repair_date1_filter: filter_repair_date1_value
                , repair_date2_filter: filter_repair_date2_value
                , fin_cost_filter: filter_fin_cost_value
                , curr_group_id: curr_page_group_id
                , is_moderated_filter: filter_is_moderated_value
                , batch_filter: filter_batch_ids
            }),
            contentType: 'application/json; charset=windows-1251',
            success: function (task_res) {
                setFilterHeader();
            },
            error: function (task_res) {
                $('#filter-panel-header').text('Фильтры: ошибка');
            }
        });

        return false;
    };

    function setFilterHeader() {
        var filter_project_name_list = $("#filterProjectList").getKendoMultiSelect().dataItems();
        var filter_client_name_list = $("#filterClientList").getKendoMultiSelect().dataItems();
        var filter_priority_name_list = $("#filterPriorityList").getKendoMultiSelect().dataItems();
        var filter_state_name_list = $("#filterStateList").getKendoMultiSelect().dataItems();
        var filter_exec_user_name_list = $("#filterExecUserList").getKendoMultiSelect().dataItems();
        var filter_assigned_user_name_list = $("#filterAssignedUserList").getKendoMultiSelect().dataItems();
        var filter_is_moderated_value = $("#filterIsModerated").is(':checked');
        var filter_batch_name_list = $("#filterBatchList").getKendoMultiSelect().dataItems();

        var project_names = "";
        if (filter_project_name_list.length != 0) {
            project_names = " [";
            $.each(filter_project_name_list, function (i, v) {
                project_names = project_names + v.project_name + '; ';
            });
            project_names = project_names + "]";
        };

        var client_names = "";
        if (filter_client_name_list.length != 0) {
            client_names = " [";
            $.each(filter_client_name_list, function (i, v) {
                client_names = client_names + v.client_name + '; ';
            });
            client_names = client_names + "]";
        };

        var priority_names = "";
        if (filter_priority_name_list.length != 0) {
            priority_names = " [";
            $.each(filter_priority_name_list, function (i, v) {
                priority_names = priority_names + v.priority_name + '; ';
            });
            priority_names = priority_names + "]";
        };

        var state_names = "";
        if (filter_state_name_list.length != 0) {
            state_names = " [";
            $.each(filter_state_name_list, function (i, v) {
                state_names = state_names + v.state_name + '; ';
            });
            state_names = state_names + "]";
        };

        var exec_user_names = "";
        if (filter_exec_user_name_list.length != 0) {
            exec_user_names = " [";
            $.each(filter_exec_user_name_list, function (i, v) {
                exec_user_names = exec_user_names + v.user_name + '; ';
            });
            exec_user_names = exec_user_names + "]";
        };

        var assigned_user_names = "";
        if (filter_assigned_user_name_list.length != 0) {
            assigned_user_names = " [";
            $.each(filter_assigned_user_name_list, function (i, v) {
                assigned_user_names = assigned_user_names + v.user_name + '; ';
            });
            assigned_user_names = assigned_user_names + "]";
        };

        var is_moderated_value = "";
        if (filter_is_moderated_value) {
            is_moderated_value = " [Модерация]";
        }

        var batch_names = "";
        if (filter_batch_name_list.length != 0) {
            batch_names = " [";
            $.each(filter_batch_name_list, function (i, v) {
                batch_names = batch_names + v.batch_name + '; ';
            });
            batch_names = batch_names + "]";
        };

        $('#filter-panel-header').text("Фильтры" + project_names + client_names + priority_names + state_names + exec_user_names + assigned_user_names + is_moderated_value + batch_names);
    };

    function toggleVisibility() {
        if (curr_page_group_id == 0) {
            $('.hidden-group-0').addClass('hidden');
            //
            $('.visible-group-0').removeClass('hidden');
        } else if (curr_page_group_id == 1) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').removeClass('hidden');
        } else if (curr_page_group_id == 2) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').removeClass('hidden');
        } else if (curr_page_group_id == 3) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').removeClass('hidden');
            $('.hidden-group-3').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').addClass('hidden');
            $('.visible-group-3').removeClass('hidden');
        } else if (curr_page_group_id == 4) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').removeClass('hidden');
            $('.hidden-group-3').removeClass('hidden');
            $('.hidden-group-4').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').addClass('hidden');
            $('.visible-group-3').addClass('hidden');
            $('.visible-group-4').removeClass('hidden');
        } else if (curr_page_group_id == 5) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').removeClass('hidden');
            $('.hidden-group-3').removeClass('hidden');
            $('.hidden-group-4').removeClass('hidden');
            $('.hidden-group-5').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').addClass('hidden');
            $('.visible-group-3').addClass('hidden');
            $('.visible-group-4').addClass('hidden');
            $('.visible-group-5').removeClass('hidden');
        } else {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').removeClass('hidden');
            $('.hidden-group-3').removeClass('hidden');
            $('.hidden-group-4').removeClass('hidden');
            $('.hidden-group-5').removeClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').addClass('hidden');
            $('.visible-group-3').addClass('hidden');
            $('.visible-group-4').addClass('hidden');
            $('.visible-group-5').addClass('hidden');
        };
    };

    function fillTaskGridCombo(for_group_id) {
        var ddl = $("#taskGridCombo").data("kendoDropDownList");
        if (!ddl)
            return false;

        ddl.dataSource.filter({
            field: "group_id",
            operator: "eq",
            value: parseInt(for_group_id)
        });
        ddl.select(0);
    };

    function getSettingsData() {
        var grid = $("#taskGridColumnSettingsGrid").data("kendoGrid");
        var data = grid.dataSource.view();
        var color_column_id = 5;
        return { settings: data, color_column_id: color_column_id };
    };

    function refreshTabPlanerTask(index) {
        switch (index) {
            case 1:
                refreshTaskGrid();
                break;
            case 2:
                refreshOverdueEventGrid();
                break;
            case 3:
                refreshStrategEventGrid();
                break;
            case 4:
                refreshUpcomingEventGrid();
                break;
            //case 5:
                //refreshNotesGrid();
                //break;
            default:
                break;
        }
    };

    function scrollToToday() {
        var sch = $('#scheduler').getKendoScheduler();        
        var contentDiv = $('#scheduler').find('div.k-scheduler-content');
        var cellToday = $('td.k-today', 'table.k-scheduler-table', '#scheduler')[0];
        if (cellToday) {
            var scrollTarget = $(cellToday).offset().left + contentDiv.scrollLeft() - contentDiv.position().left - 800;
            contentDiv.scrollLeft(scrollTarget);
        };
    };

    function verticalKft() {
        return isVerticalHalf ? 1.7 : 1;
    };

    //****************
    // task-planner-edit (private)
    //****************

    function openPlannerTask() {
        var win = $('#winTaskEditPlanner').data('kendoWindow');
        $('#winTaskEditPlanner').html("");

        win.title('Задача ' + curr_event.task_num);

        var winOptions = {};
        var winCrmTask_height = localStorage.winCrmTask_height;
        if (winCrmTask_height) {
            winOptions.height = winCrmTask_height;
        };
        var winCrmTask_width = localStorage.winCrmTask_width;
        if (winCrmTask_width) {
            winOptions.width = winCrmTask_width;
        };
        var winCrmTask_positionTop = localStorage.winCrmTask_positionTop;
        var winCrmTask_positionLeft = localStorage.winCrmTask_positionLeft;
        if ((winCrmTask_positionTop) && (winCrmTask_positionLeft)) {
            winOptions.position = {};
            winOptions.position.top = winCrmTask_positionTop;
            winOptions.position.left = winCrmTask_positionLeft;
        };
        if (winOptions) {
            win.setOptions(winOptions);
        };

        win.refresh({
            url: 'TaskEditPlanner',            
            data: { task_id: curr_event.task_id, exec_user_id: curr_event.task_id ? null : curr_event.exec_user_id, date_plan: curr_event.end }
        });

        if ((!winCrmTask_positionTop) || (!winCrmTask_positionLeft)) {
            win.center().open();
        } else {
            win.open();
        };

        $('#winTaskEditPlanner').closest(".k-window").css({
            top: $(document).scrollTop()
        });
    };

    function openPlannerTask_byTaskId(task_id, task_num) {
        curr_task_id = task_id;
        var win = $('#winTaskEditPlanner').data('kendoWindow');
        $('#winTaskEditPlanner').html("");
        win.title('Задача ' + task_num);

        var winOptions = {};
        var winCrmTask_height = localStorage.winCrmTask_height;
        if (winCrmTask_height) {
            winOptions.height = winCrmTask_height;
        };
        var winCrmTask_width = localStorage.winCrmTask_width;
        if (winCrmTask_width) {
            winOptions.width = winCrmTask_width;
        };
        var winCrmTask_positionTop = localStorage.winCrmTask_positionTop;
        var winCrmTask_positionLeft = localStorage.winCrmTask_positionLeft;
        if ((winCrmTask_positionTop) && (winCrmTask_positionLeft)) {
            winOptions.position = {};
            winOptions.position.top = winCrmTask_positionTop;
            winOptions.position.left = winCrmTask_positionLeft;
        };
        if (winOptions) {
            win.setOptions(winOptions);
        };

        win.refresh({
            url: 'TaskEditPlanner',
            data: { task_id: task_id }
        });

        if ((!winCrmTask_positionTop) || (!winCrmTask_positionLeft)) {
            win.center().open();
        } else {
            win.open();
        };

        $('#winTaskEditPlanner').closest(".k-window").css({
            top: $(document).scrollTop()
        });
    };

    function onSubtaskAddClick(e) {
        var text = $('#txtSubtaskAddPlanner').val();
        if (!text) return false;
        
        var combo = $("#assignedUserListPlanner").getKendoDropDownList();
        var curr_exec_user_id = combo.dataItem()["user_id"];
        var curr_subtask_num = generateSubtaskNum();
        var new_subtask = {
            subtask_id: 0, task_id: $('#task_id').val(), subtask_text: text,
            state_id: 1, owner_user_id: currUser_CabUserId, crt_date: new Date(), upd_date: null,
            repair_date: null, task_num: curr_subtask_num, owner_user_name: currUser_CabUserName,
            file_id: null,
            IsChecked: false, IsNew: true
        };

        var grid = $("#subtaskGridPlanner").data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_subtask);
        $('#txtSubtaskAddPlanner').val('');

        _this.onAttrChanged();

        return true;
    };

    function generateSubtaskNum() {
        AutoID = 1;
        var newAutoID = localStorage.subtask_num;
        if (!isNaN(newAutoID)) {
            AutoID = parseInt(newAutoID) + 1;
        }
        localStorage.subtask_num = AutoID;
        return AutoID;
    };

    function onTaskNoteAddClick(e) {
        var text = $('#txtTaskNoteAddPlanner').val();
        if (!text) return false;

        var new_note = { note_id: 0, note_text: text, crt_date: new Date(), owner_user_id: currUser_CabUserId, user_name: currUser_CabUserName, file_name: '' };
        var grid = $("#taskNoteGridPlanner").data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_note);
        $('#txtTaskNoteAddPlanner').val('');

        _this.onAttrChanged();

        return true;
    };

    function onSubtaskExcelClick(e) {
        var grid = $('#subtaskGridPlanner').getKendoGrid();
        grid.saveAsExcel();
    };

    function onSubtaskPdfClick(e) {
        var grid = $('#subtaskGridPlanner').getKendoGrid();
        grid.saveAsPDF();
    };

    function onBtnTaskOkClick(e) {
        if (checkSaveFlag) {
            checkTaskSave();
        }
        else {
            taskSave(true);
        };
    };

    function checkTaskSave() {
        var ctrl = $('#spanTaskErrMessPlanner');

        $.ajax({
            type: "POST",
            url: '/Crm/TaskSaveBefore?task_id=' + curr_task_id + '&task_upd_num=' + $('#upd_num').val(),
            data: '',
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response != null) {
                    if (!response.is_ok) {
                        ctrl.html(response.mess);
                        ctrl.removeClass('hidden');
                        checkSaveFlag = false;
                        return false;
                    } else {
                        taskSave(true);
                        return true;
                    };
                } else {
                    taskSave(true);
                    return true;
                };
            },
            error: function (task_res) {
                taskSave(true);
                return true;
            }
        });
    };

    function taskSave(winclose) {
        var win = $('#winTaskEditPlanner').data('kendoWindow');

        var project_name_combo = $("#projectListPlanner").getKendoDropDownList();
        var module_name_combo = $("#moduleListPlanner").getKendoDropDownList();
        var module_version_name_combo = $("#moduleVersionListPlanner").getKendoDropDownList();
        var repair_version_name_combo = $("#repairVersionListPlanner").getKendoDropDownList();
        var client_name_combo = $("#clientListPlanner").getKendoDropDownList();
        var priority_name_combo = $("#priorityListPlanner").getKendoDropDownList();
        var state_name_combo = $("#stateListPlanner").getKendoDropDownList();
        var assigned_user_name_combo = $("#assignedUserListPlanner").getKendoDropDownList();
        var exec_user_name_combo = $("#execUserListPlanner").getKendoDropDownList();
        var event_user_name_combo = $("#eventUserListPlanner").getKendoDropDownList();
        var group_name_combo = $('#groupListPlanner').getKendoDropDownList();
        var batch_name_combo = $("#batchListPlanner").getKendoDropDownList();

        var task_id_col = curr_task_id;
        var task_name_col = $('#task_namePlanner').val();
        var task_text_col = $('#task_textPlanner').val();
        var task_num_col = 'AU-0000';
        var crt_date_col = new Date();
        var client_name_col = client_name_combo.dataItem()["client_name"];
        var client_id_col = client_name_combo.dataItem()["client_id"];
        var owner_user_name_col = currUser_CabUserName;
        var owner_user_id_col = currUser_CabUserId;

        var required_date_col = null;
        var repair_date_col = null;
        var repair_version_name_col = '[не указан]';
        var repair_version_id_col = 0;
        var project_name_col = '[не указан]';
        var project_id_col = 0;
        var module_name_col = '[не указан]';
        var module_id_col = 0;
        var module_version_name_col = '[не указан]';
        var module_version_id_col = 0;
        var module_part_name_col = '[не указан]';
        var module_part_id_col = 0;
        var priority_name_col = '[не указан]';
        var priority_id_col = 0;
        var state_name_col = '[не указан]';
        var state_id_col = 0;
        var assigned_user_name_col = '[не указан]';
        var assigned_user_id_col = 0;
        var exec_user_name_col = '[не указан]';
        var exec_user_id_col = 0;
        var is_moderated_col = false;
        var event_user_name_col = '[не указан]';
        var event_user_id_col = 1;        
        var fin_cost_col = 0;
        var is_event_name_col = '';
        var is_event_col = false;
        var group_id_col = 0;
        var group_name_col = 'Неразобранные';
        var batch_name_col = '[не указан]';
        var batch_id_col = 0;
        var is_control_col = false;
        var event_date_beg = null;
        var event_date_end = null;
        var cost_plan_col = 0;
        var cost_fact_col = 0;
        var task_notes = null;
        var task_subtasks = null;
        
        fin_cost_col = $('#fin_costPlanner').val();
        task_notes = $("#taskNoteGridPlanner").data("kendoGrid").dataSource.view();
        task_subtasks = $("#subtaskGridPlanner").data("kendoGrid").dataSource.view();

        repair_date_col = $('#repair_datePlanner').val();
        state_name_col = state_name_combo.dataItem()["state_name"];
        state_id_col = state_name_combo.dataItem()["state_id"];

        assigned_user_name_col = assigned_user_name_combo.dataItem()["user_name"];
        assigned_user_id_col = assigned_user_name_combo.dataItem()["user_id"];
        exec_user_name_col = exec_user_name_combo.dataItem()["user_name"];
        exec_user_id_col = exec_user_name_combo.dataItem()["user_id"];
        is_moderated_col = $('#IsModeratedPlanner').is(':checked');
        if (event_user_name_combo) {
            event_user_name_col = event_user_name_combo.dataItem()["Text"];
            event_user_id_col = event_user_name_combo.dataItem()["Value"];
        };
        required_date_col = $('#required_datePlanner').val();
        project_name_col = project_name_combo.dataItem()["project_name"];
        project_id_col = project_name_combo.dataItem()["project_id"];
        module_name_col = module_name_combo.dataItem()["module_name"];
        module_id_col = module_name_combo.dataItem()["module_id"];
        module_version_name_col = module_version_name_combo.dataItem()["module_version_name"];
        module_version_id_col = module_version_name_combo.dataItem()["module_version_id"];
        repair_version_name_col = repair_version_name_combo.dataItem()["module_version_name"];
        repair_version_id_col = repair_version_name_combo.dataItem()["module_version_id"];
        priority_name_col = priority_name_combo.dataItem()["priority_name"];
        priority_id_col = priority_name_combo.dataItem()["priority_id"];
        batch_name_col = batch_name_combo.dataItem()["batch_name"];
        batch_id_col = batch_name_combo.dataItem()["batch_id"];
        event_date_beg_col = $('#event_date_begPlanner').val();
        event_date_end_col = $('#event_date_endPlanner').val();
        cost_plan_col = $('#cost_planPlanner').val();
        cost_fact_col = $('#cost_factPlanner').val();

        is_event_col = $('#is_eventPlanner').is(':checked');
        if (is_event_col) { is_event_col_name = 'К'; } else { is_event_col_name = ''; };

        is_control_col = $('#is_controlPlanner') ? $('#is_controlPlanner').is(':checked') : false;

        group_id_col = group_name_combo.dataItem()["group_id"];
        group_name_col = group_name_combo.dataItem()["group_name"];

        var task = {
            task_id: task_id_col, crt_date: crt_date_col, task_num: task_num_col,
            task_name: task_name_col, task_text: task_text_col,
            state_name: state_name_col, state_id: state_id_col,
            client_name: client_name_col, client_id: client_id_col,
            exec_user_name: exec_user_name_col, exec_user_id: exec_user_id_col,
            is_event_for_exec: event_user_id_col,
            assigned_user_name: assigned_user_name_col, assigned_user_id: assigned_user_id_col,
            module_name: module_name_col, module_id: module_id_col,
            module_version_name: module_version_name_col, module_version_id: module_version_id_col,
            repair_version_name: repair_version_name_col, repair_version_id: repair_version_id_col,
            owner_user_name: owner_user_name_col, owner_user_id: owner_user_id_col,
            priority_name: priority_name_col, priority_id: priority_id_col,
            project_name: project_name_col, project_id: project_id_col,
            required_date: required_date_col,
            repair_date: repair_date_col,
            group_id: group_id_col, group_name: group_name_col,
            batch_name: batch_name_col, batch_id: batch_id_col,
            fin_cost: fin_cost_col, is_event: is_event_col, is_event_name: is_event_name_col,
            event_date_beg: event_date_beg_col, event_date_end: event_date_end_col,
            cost_plan: cost_plan_col, cost_fact: cost_fact_col,
            is_control: is_control_col,
            IsModerated: is_moderated_col
        };
        $.ajax({
            type: "POST",
            url: "Crm/TaskAdd",
            data: JSON.stringify({ task: task, notes: task_notes, subtasks: task_subtasks }),
            contentType: 'application/json; charset=windows-1251',
            success: function (task_res) {
                refreshScheduler();
                refreshTaskGrid();                
                var index = $("#tabPlanerTask").data("kendoTabStrip").select().index();
                refreshTabPlanerTask(index);
            },
            error: function (task_res) {
                alert('Ошибка при редактировании задачи');
            }
        });

        localStorage.winCrmTask_height = win.options.height;
        localStorage.winCrmTask_width = win.options.width;
        localStorage.winCrmTask_positionTop = win.options.position.top;
        localStorage.winCrmTask_positionLeft = win.options.position.left;
        localStorage.winCrmTask_taskTextHeight = $('#task_textPlanner').height();
        localStorage.winCrmTask_taskTextWidth = $('#task_textPlanner').width();
        win.close();
    };

    function onBtnTaskCancelClick(e) {
        var win = $('#winTaskEditPlanner').data('kendoWindow');
        win.close();
    };

    function saveUserConfig(sender, callback) {
        if (sender) {
            sender.addClass('hidden');
        };
        var grid = $("#plannerTaskGrid").getKendoGrid();
        var ds = grid.dataSource;

        $.ajax({
            type: "POST",
            url: "Sys/CabGridUserSettings",
            data: JSON.stringify({ grid_id: 1, sort_options: kendo.stringify(ds.sort()), filter_options: kendo.stringify(ds.filter()), page_size: ds.pageSize(), columns_options: kendo.stringify(grid.columns) }),
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (sender) {
                    sender.removeClass('hidden');
                };
                if (response != null) {
                    if (response.error) {
                        alert(response.mess);
                    } else {
                        if (callback)
                            callback();
                    };
                } else {
                    alert('Ошибка при попытке сохранения настроек');
                };
            },
            error: function (response) {
                if (sender) {
                    sender.removeClass('hidden');
                };
                alert('Ошибка при сохранении настроек');
            }
        });
    };
}

var planner = null;
var firstDataBound_scheduler = true;
var firstDataBound_task = true;
var firstDataBound_upcoming = true;
var firstDataBound_overdue = true;
var firstDataBound_strateg = true;
var firstDataBound_note = true;
var checkedIds = {};
var exportFlag = false;
var checkSaveFlag = true;
var curr_task_id = null;
var curr_event = null;
var needUpdate_scheduler = false;
var needUpdate_notes = false;
var dblclickFired = false;
var curr_page_group_id = -1;
var taskAction = null;
var id_list = '';
var selected_data_item = null;
var selected_row = null;
var noHint = $.noop;
var offset_top_before_resize = null;
var offset_top_after_resize = null;
var offset_left_before_resize = null;
var offset_left_after_resize = null;
var event_start_before_resize = null;
var event_end_before_resize = null;
var slot_start_after_resize = null;
var slot_end_after_resize = null;
var resized = false;
var isHorizontalHalf = false;
var isVerticalHalf = false;

$(function () {
    planner = new Planner();
    planner.init();
});
