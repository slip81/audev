﻿function DeliveryTemplateList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#deliveryTemplateGrid', 0);
    };

    this.onDeliveryTemplateGridDataBound = function(e) {
        drawEmptyRow('#deliveryTemplateGrid');
    };
}

var deliveryTemplateList = null;

$(function () {
    deliveryTemplateList = new DeliveryTemplateList();
    deliveryTemplateList.init();
});
