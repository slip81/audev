﻿function OrderAdd() {
    _this = this;

    this.init = function () {
        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            proceedSubmit(false);
        });

        $("#btnSubmit2").click(function (e) {
            e.preventDefault();
            proceedSubmit(true);
        });
    };

    this.initVersionList = function(e) {
        $(".versionListCell").each(function () {
            eval($(this).children("script").last().html());
        });
    };

    this.versionListChanged = function () {
        var grid = $("#orderItemListGrid").getKendoGrid();        
        var row = $(this.element).closest('tr');
        var item = grid.dataItem(row);
        var ddl_val = this.value();
        item.set('OrderedVersionId_ReadOnly', ddl_val);
        item.set('OrderedVersionId', ddl_val);
        //        
        editFromDdlChange = true;
        var cell = grid.tbody.find("tr[data-uid='" + item.uid + "']" + " td.orderedLicenseCell");
        grid.editCell(cell);        
    };

    this.versionListOpen = function(e) {        
        var grid = $('#orderItemListGrid').getKendoGrid();
        var row = $(e.sender.element).closest("tr");
        var dataItem = grid.dataItem(row);
        //
        var ddl = $(e.sender.element).getKendoDropDownList();
        var ds = ddl.dataSource;
        var ddlFilter = { logic: "and", filters: [] };
        ddlFilter.filters.push({ field: "service_id", operator: "eq", value: dataItem.id });
        ds.filter(ddlFilter);
        var ddlSort = [{ field: "id", dir: "desc" }];
        ds.sort(ddlSort);
    };

    this.onOrderItemListGridEdit = function (e) {
        //alert('onOrderItemListGridEdit !');
        if (editFromDdlChange) {
            var model = e.model;
            var curr_OrderedLicenseCount = model.get("OrderedLicenseCount");
            if (editFromDdlChange_reset) {
                editFromDdlChange_reset = false;
                model.set("OrderedLicenseCount", null);
            } else {
                if (curr_OrderedLicenseCount == null)
                    model.set("OrderedLicenseCount", 1);
            };
        };
        editFromDdlChange = false;
    };

    this.onBtnServiceKitAddClick = function (e) {
        window.location = url_ServiceKitAdd;
    };

    this.onBtnServiceKitEditClick = function (e) {
        var ddl = $('#ddlServiceKit').getKendoDropDownList();
        if (ddl.select() <= 0) {
            //alert('Не выбран набор');
            getPopup().show('Не выбран набор', "error");
            return false;
        };
        var dataItem = ddl.dataItem();
        if (!dataItem) {
            getPopup().show('Не выбран набор', "error");
            return false;
        };
        window.location = url_ServiceKitEdit + '?service_kit_id=' + dataItem.service_kit_id + '&client_id=' + model_client_id;
    };

    this.onBtnServiceKitDelClick = function (e) {
        var ddl = $('#ddlServiceKit').getKendoDropDownList();
        if (ddl.select() <= 0) {
            getPopup().show('Не выбран набор', "error");
            return false;
        };
        var dataItem = ddl.dataItem();
        if (!dataItem) {
            getPopup().show('Не выбран набор', "error");
            return false;
        };
        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Удалить набор ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "80px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#save-changes-confirmation").html())
            .center().open();

        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        $('#ddl-wait').removeClass('hidden');
                        kendoWindow.data("kendoWindow").close();
                        var postData = JSON.stringify({ service_kit_id: dataItem.service_kit_id });
                        $.ajax({
                            type: "POST",
                            url: "ServiceKitDel",
                            data: postData,
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                $('#ddl-wait').addClass('hidden');
                                if (response.err) {
                                    if (response.mess) {
                                        getPopup().show(response.mess, "error");
                                    } else {
                                        getPopup().show('Ошибка при удалении набора', "error");
                                    };
                                } else {
                                    var oldData = ddl.dataSource.data();
                                    var dataLength = oldData.length;
                                    for (var i = 0; i < dataLength; i++) {
                                        var item = oldData[i];
                                        if (item.service_kit_id == dataItem.service_kit_id) {
                                            ddl.dataSource.remove(item);
                                            break;
                                        }
                                    };
                                    ddl.select(0);
                                };
                                return true;
                            },
                            error: function (response) {
                                $('#ddl-wait').addClass('hidden');
                                getPopup().show('Ошибка при попытке удаления набора', "error");
                                return true;
                            }
                        });
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end();

    };

    this.onDdlServiceKitChange = function (e) {
        if (this.select() <= 0) {
            return;
        };
        $('#ddl-wait').removeClass('hidden');
        var value = this.value();
        var postData = JSON.stringify({ service_kit_id: value });
        $.ajax({
            type: "POST",
            url: "GetServiceKitItemList",
            data: postData,
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {                
                if (response.err) {
                    if (response.mess) {
                        getPopup().show(response.mess, "error");
                    } else {
                        getPopup().show('Ошибка при чтении набора', "error");
                    };
                } else {
                    var serviceKitItems = $.parseJSON(response.serviceKitItemList);
                    $(".versionListCell").each(function () {
                        var grid = $("#orderItemListGrid").getKendoGrid();
                        var row = $(this).closest('tr');
                        var item = grid.dataItem(row);
                        //
                        editFromDdlChange = true;
                        editFromDdlChange_reset = true;
                        $.each(serviceKitItems, function (i, el) {
                            if (el.service_id == item.id) {
                                editFromDdlChange_reset = false;
                                return false;
                            };
                        });                        
                        var cell = grid.tbody.find("tr[data-uid='" + item.uid + "']" + " td.orderedLicenseCell");
                        grid.editCell(cell);
                    });
                    $('#ddl-wait').addClass('hidden');
                };
                return true;
            },
            error: function (response) {
                $('#ddl-wait').addClass('hidden');
                getPopup().show('Ошибка при чтении набора', "error");
                return true;
            }
        });
    }

    // private

    function getPopup() {
        if (!popupNotification)
            popupNotification = $("#popupNotification").data("kendoNotification");
        return popupNotification;
    };

    function proceedSubmit(need_activate) {
        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Оформить заявку ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "80px"
        });
        kendoWindow.data("kendoWindow")
            .content($("#save-changes-confirmation").html())
            .center().open();

        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        kendoWindow.data("kendoWindow").close();
                        $('#btnSubmit').attr('disabled', true);
                        $('#btnSubmit2').attr('disabled', true);

                        var gridView = $("#orderItemListGrid").data("kendoGrid").dataSource.view();
                        var data1 = gridView[0].items;
                        var data2 = gridView[1].items;
                        var data3 = gridView[2].items;
                        var dataAll = data1.concat(data2).concat(data3);

                        var serv_data = kendo.stringify({ serv_data: dataAll, client_id: model_client_id, comment: $('#commentary').val(), comment_manager: $('#commentary_manager').val(), activate: need_activate });
                        $.ajax({
                            type: "POST",
                            url: "OrderAdd",
                            data: serv_data,
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                if (response.err) {
                                    if (response.mess) {
                                        alert(response.mess);
                                    } else {
                                        alert('Ошибка при добавлении заявки');
                                    };
                                } else {
                                    window.location = "/ClientEdit?client_id=" + model_client_id + "&active_page=1";
                                };
                                kendoWindow.data("kendoWindow").close();
                            },
                            error: function (response) {
                                alert('Ошибка при попытке добавлении заявки');
                                kendoWindow.data("kendoWindow").close();
                            }
                        });
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end();
    };

}

var orderAdd = null;
var popupNotification = null;
var editFromDdlChange = false;
var editFromDdlChange_reset = false;

$(function () {
    orderAdd = new OrderAdd();
    orderAdd.init();
});
