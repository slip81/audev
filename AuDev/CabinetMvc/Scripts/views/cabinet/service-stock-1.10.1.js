﻿function ServiceStock() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('k-state-active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-revert-visible');
                                $('#btnRevert').removeClass('k-state-focused');
                                $('#btnRevert').addClass('btn-revert-hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });
    };
}

var serviceStock = null;

$(function () {
    serviceStock = new ServiceStock();
    serviceStock.init();
});
