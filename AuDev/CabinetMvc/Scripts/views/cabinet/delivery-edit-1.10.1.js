﻿function DeliveryEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $('#btn-select-all').click(function (e) {
            e.preventDefault();
            var sourceGrid = $("#clientGrid").getKendoGrid();
            sourceGrid.select("#clientGrid tr:gt(1)");
            return false;
        });

        $('#btn-select-all2').click(function (e) {
            e.preventDefault();
            var sourceGrid = $("#clientGrid2").getKendoGrid();
            sourceGrid.select("#clientGrid2 tr:gt(1)");
            return false;
        });

        $('#btn-move-right').click(function (e) {
            e.preventDefault();
            var sourceGrid = $("#clientGrid").getKendoGrid();
            var targetGrid = $("#clientGrid2").getKendoGrid();
            var rows = sourceGrid.select();
            rows.each(function (index, row) {
                var selectedItem = sourceGrid.dataItem(row);
                if (selectedItem != null) {
                    var id = selectedItem.id;
                    if (targetGrid.dataSource.get(id) == null) {
                        targetGrid.dataSource.add(selectedItem);
                    };
                }
            });
            return false;
        });

        $('#btn-remove').click(function (e) {
            e.preventDefault();
            var sourceGrid = $("#clientGrid2").getKendoGrid();
            var rows = sourceGrid.select();
            rows.each(function (index, row) {
                var selectedItem = sourceGrid.dataItem(row);
                if (selectedItem != null) {
                    sourceGrid.dataSource.remove(selectedItem);
                };
            });
            return false;
        });

        $('#btn-remove-all').click(function (e) {
            e.preventDefault();
            var sourceGrid = $("#clientGrid2").getKendoGrid();
            sourceGrid.dataSource.data([]);
            return false;
        });

        $("#btnSubmit").click(function (e) {
            e.preventDefault();

            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var ddl1 = $("#templateList").getKendoDropDownList();
            $('#template_id').val(ddl1.value());

            var details = $('#clientGrid2').getKendoGrid().dataSource.view();

            if (validator.validate()) {
                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').attr('disabled', true);                                

                                var postData = JSON.stringify({ delivery_id: $('#delivery_id').val(), descr: $('#descr').val(), template_id: $('#template_id').val(), detailList: details });
                                $.ajax({
                                    type: "POST",
                                    url: "DeliveryEdit",
                                    data: postData,
                                    contentType: 'application/json; charset=windows-1251',
                                    success: function (response) {
                                        if (response.err) {
                                            $('#btnSubmit').attr('disabled', false);                                            
                                            if (response.mess) {
                                                alert(response.mess);
                                            } else {
                                                alert('Ошибка при редактировании рассылки');
                                            };
                                        } else {
                                            window.location = "/DeliveryList";
                                        };
                                        kendoWindow.data("kendoWindow").close();
                                    },
                                    error: function (response) {
                                        $('#btnSubmit').attr('disabled', false);                                        
                                        alert('Ошибка при попытке редактирования рассылки');
                                        kendoWindow.data("kendoWindow").close();
                                    }
                                });

                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        setGridMaxHeight('#clientGrid', 200);
        setGridMaxHeight('#clientGrid2', 200);
    };

    this.onClientGrid2Edit = function (e) {
        var fieldName = e.container.find("input").attr("name");

        if (fieldName != 'site') {
            this.closeCell(); // prevent editing
        } else {
            $('#clientGrid2').getKendoGrid().clearSelection();
        };

    };
}

var deliveryEdit = null;

$(function () {
    deliveryEdit = new DeliveryEdit();
    deliveryEdit.init();
});
