﻿function ClientUserEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $('#span-for-sales').trigger('click');

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            $('#is_active').val($('#user-active').is(':checked') ? 1 : 0);
            var is_for_sales_checked = $('#is_for_sales').is(':checked');
            if (is_for_sales_checked) {
                var ddl = $('#salesList').getKendoDropDownList();
                if (!ddl.value()) {
                    $('#span-for-sales-valid').removeClass('hidden');
                    return false;
                } else {
                    $('#sales_id').val(ddl.value());
                }
            } else {
                $('#sales_id').val(null);
            }

            var ddl1 = $('#ddlClientUserRole').getKendoDropDownList();
            $('#role_id').val(parseInt(ddl1.value()));

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('k-state-active');
                                $('#btnSubmit').attr('disabled', true);
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $('#span-for-sales').click(function (e) {
            var ddl = $('#salesList').getKendoDropDownList();
            var is_checked = $('#is_for_sales').is(':checked');
            ddl.enable(is_checked);
            if (is_checked) {
                $('#is_main_for_sales').removeProp('disabled');
            } else {
                $('#is_main_for_sales').prop('disabled', 'disabled');
            };
        });
    };

    this.onSalesListChange = function(e) {
        $('#span-for-sales-valid').addClass('hidden');
    };
}

var clientUserEdit = null;

$(function () {
    clientUserEdit = new ClientUserEdit();
    clientUserEdit.init();
});
