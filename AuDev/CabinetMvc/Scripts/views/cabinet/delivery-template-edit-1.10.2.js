﻿function DeliveryTemplateEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            
            uploadError = false;

            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#templateGroupList").getKendoComboBox();
            var prev_group_name = '';
            var curr_group_name = '';
            if ($('#template_id').val() > 0) {
                prev_group_name = $("#Group_Selected").val();
                curr_group_name = combo1.text();
                if (prev_group_name != curr_group_name) {
                    $("#Group_Selected").val(curr_group_name);
                    $("#Group_Changed").val(true);
                } else {
                    $("#Group_Changed").val(false);
                };
            } else {
                curr_group_name = combo1.text();
                $("#Group_Selected").val(curr_group_name);
                $("#Group_Changed").val(true);
            };

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').attr('disabled', true);
                                //$("#btnSubmitProceed").trigger('click');
                                
                                var postData = JSON.stringify({ template_id: $('#template_id').val(), group_id: $('#group_id').val(), template_name: $('#template_name').val(), subject: $('#subject').val(), body: $('#body').val(), Group_Selected: $('#Group_Selected').val(), Group_Changed: $('#Group_Changed').val() });
                                $.ajax({
                                    type: "POST",
                                    url: "DeliveryTemplateEdit",
                                    data: postData,
                                    contentType: 'application/json; charset=windows-1251',
                                    success: function (response) {
                                        if (response.err) {
                                            $('#btnSubmit').attr('disabled', false);
                                            //$('#btnSubmitAndStart').attr('disabled', false);
                                            if (response.mess) {
                                                alert(response.mess);
                                            } else {
                                                alert('Ошибка при редактировании шаблона рассылки');
                                            };
                                        } else {
                                            $('#template_id').val(response.id);
                                            var upload = $("#uplDeliveryTemplateFile").data("kendoUpload"),
                                                len = upload.wrapper.find(".k-file:not(.k-file-success, .k-file-error)").length;
                                                
                                            if ((len) && (len > 0)) {
                                                $('.k-button.k-upload-selected').trigger('click');                                                
                                            } else {
                                                window.location = "/DeliveryTemplateList";
                                            };
                                        };
                                        kendoWindow.data("kendoWindow").close();
                                    },
                                    error: function (response) {
                                        $('#btnSubmit').attr('disabled', false);
                                        //$('#btnSubmitAndStart').attr('disabled', false);
                                        alert('Ошибка при попытке редактирования шаблона рассылки');
                                        kendoWindow.data("kendoWindow").close();
                                    }
                                });

                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
           
        });
    };

    this.onUplDeliveryTemplateFileComplete = function (e) {
        if (!uploadError) {
            window.location = "/DeliveryTemplateList";
        } else {
            $('#btnSubmit').attr('disabled', false);
        };
    };

    this.onUplDeliveryTemplateFileUpload = function (e) {
        e.data = { template_id: $('#template_id').val() };
    };

    this.onUplDeliveryTemplateFileError = function (e) {
        uploadError = true;
        var err = e.XMLHttpRequest.responseText;
        alert(err);
    };

    this.onUplDeliveryTemplateFileRemove = function (e) {
        e.data = { template_id: $('#template_id').val() };
    };    

}

var deliveryTemplateEdit = null;
var uploadError = false;

$(function () {
    deliveryTemplateEdit = new DeliveryTemplateEdit();
    deliveryTemplateEdit.init();
});
