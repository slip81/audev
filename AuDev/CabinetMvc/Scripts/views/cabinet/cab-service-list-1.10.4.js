﻿function CabServiceList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#cabServiceGrid', 0);
    };
   
    this.onRequestEnd1 = function(gridName) {
        return function (e) {
            var grid = $("#" + gridName).data("kendoGrid");
            if ((e.type == "create") || (e.type == "update")) {
                grid.dataSource.read();
            }
        }
    };

    this.onBtnVersionBatchAddClick = function(e) {
        var verNum = $('#edVersionBatchAdd').val();
        if ((!verNum) || (verNum.length <= 0)) {
            return;
        };

        var groupId = $('#ddlVersionBatchAdd').getKendoDropDownList().value();

        $('#btnVersionBatchAdd').addClass("k-state-disabled")
        var postData = JSON.stringify({ version_num: verNum, group_id: groupId });
        $.ajax({
            type: "POST",
            url: "VersionBatchAdd",
            data: postData,
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {                
                if (response.err) {
                    $('#btnVersionBatchAdd').removeClass("k-state-disabled")
                    if (response.mess) {
                        alert(response.mess);
                    } else {
                        alert('Ошибка при добавлении версии');
                    };
                } else {
                    location.href = location.href;
                };
                kendoWindow.data("kendoWindow").close();
            },
            error: function (response) {
                $('#btnVersionBatchAdd').removeClass("k-state-disabled")
                alert('Ошибка при попытке добавлении версии');
                kendoWindow.data("kendoWindow").close();
            }
        });
    };

    this.onBtnVersionBatchDelClick = function (e) {
        var verNum = $('#edVersionBatchDel').val();
        if ((!verNum) || (verNum.length <= 0)) {
            return;
        };

        var groupId = $('#ddlVersionBatchDel').getKendoDropDownList().value();

        $('#btnVersionBatchDel').addClass("k-state-disabled")
        var postData = JSON.stringify({ version_num: verNum, group_id: groupId });
        $.ajax({
            type: "POST",
            url: "VersionBatchDel",
            data: postData,
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response.err) {
                    $('#btnVersionBatchDel').removeClass("k-state-disabled")
                    if (response.mess) {
                        alert(response.mess);
                    } else {
                        alert('Ошибка при удалении версии');
                    };
                } else {
                    location.href = location.href;
                };
                kendoWindow.data("kendoWindow").close();
            },
            error: function (response) {
                $('#btnVersionBatchDel').removeClass("k-state-disabled")
                alert('Ошибка при попытке удалить версии');
                kendoWindow.data("kendoWindow").close();
            }
        });
    };
}

var cabServiceList = null;

$(function () {
    cabServiceList = new CabServiceList();
    cabServiceList.init();
});
