﻿function CabHelpPart() {
    _this = this;

    this.init = function () {
        setControlHeight('#cabHelpMainSplitter', 0);
    };
}

var cabHelpPart = null;

$(function () {
    cabHelpPart = new CabHelpPart();
    cabHelpPart.init();
});
