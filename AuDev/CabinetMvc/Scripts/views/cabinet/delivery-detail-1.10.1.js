﻿function DeliveryDetail() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#deliveryDetailGrid', 0);
    };
}

var deliveryDetail = null;

$(function () {
    deliveryDetail = new DeliveryDetail();
    deliveryDetail.init();
});
