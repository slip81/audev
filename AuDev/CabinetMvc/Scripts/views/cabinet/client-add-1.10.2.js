﻿function ClientAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            var combo1 = $("#regionList").getKendoComboBox();
            var curr_region_name = combo1.text();
            $("#RegionName_Selected").val(curr_region_name);
            $("#RegionName_Changed").val(true);

            var combo2 = $("#cityList").getKendoComboBox();
            var curr_city_name = combo2.text();
            $("#CityName_Selected").val(curr_city_name);
            $("#CityName_Changed").val(true);
            
            var combo3 = $("#regionZoneList").data('kendoDropDownList');
            var curr_region_zone_dataItem = combo3.dataItem();
            var curr_region_zone_id = null;
            if ((curr_region_zone_dataItem) && (curr_region_zone_dataItem.zone_id)) {
                curr_region_zone_id = curr_region_zone_dataItem.zone_id;
            }
            $("#region_zone_id").val(curr_region_zone_id);

            var combo4 = $("#taxSystemList").data('kendoDropDownList');
            var curr_tax_system_type_dataItem = combo4.dataItem();
            var curr_tax_system_type_id = null;
            if ((curr_tax_system_type_dataItem) && (curr_tax_system_type_dataItem.system_type_id)) {
                curr_tax_system_type_id = curr_tax_system_type_dataItem.system_type_id;
            }
            $("#tax_system_type_id").val(curr_tax_system_type_id);

            var is_ph_eq_jur = $('#chbPhEqJur').is(':checked');
            if (is_ph_eq_jur) {
                $("#absolute_address").val($("#legal_address").val());
            };

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };

        });

        $('#chbPhEqJur').click(function (e) {
            var checked = $(this).is(':checked');
            if (checked) {
                $('#absolute_address').val($('#legal_address').val());
                $('#absolute_address').attr('readonly', 'readonly');
            } else {
                $('#absolute_address').removeAttr('readonly');
            };
        });
    };

    this.filterRegion = function() {
        return {
            region_id: $("#regionList").val()
        };
    };

    this.onRegionListChange = function(e) {        
        $("#cityList").getKendoComboBox().enable(true);
    };
}

var clientAdd = null;

$(function () {
    clientAdd = new ClientAdd();
    clientAdd.init();
});
