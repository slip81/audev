﻿function ClientReport() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#clientReportGrid', 30);
        //
        $('#chbIsClient').on('click', function (e) {
            refreshGrid('#clientReportGrid');
        });
    };

    this.onClientReportGridDataBound = function (e) {
        drawEmptyRow('#clientReportGrid');
        //
        if ($('#chbIsClient').is(':checked')) {
            $('#clientReportGrid').data('kendoGrid').hideColumn('sales_name');
        } else {
            $('#clientReportGrid').data('kendoGrid').showColumn('sales_name');
        };
    };

    this.getClientReportGridData = function () {
        var curr_service_name = null;
        var dataItem = $('#serviceDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.id)) {
            curr_service_name = dataItem.name;
        };
        return { is_client: ($('#chbIsClient').is(':checked') ? 1 : 0), service_name: curr_service_name };
    };

    this.onBtnExcelClick = function (e) {
        $('#clientReportGrid').data('kendoGrid').saveAsExcel();
    };
    
    this.onServiceDdlChange = function (e) {
        refreshGrid('#clientReportGrid');
    };

    this.onServiceDdlDataBound = function (e) {        
        setTimeout(function () {
            refreshGrid('#clientReportGrid');
        }, 1000);
    };
}

var clientReport = null;
$(function () {
    clientReport = new ClientReport();
    clientReport.init();
});
