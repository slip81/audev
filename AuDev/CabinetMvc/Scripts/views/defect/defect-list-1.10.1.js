﻿function DefectList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#defectGrid', 0);
    };
}

var defectList = null;

$(function () {
    defectList = new DefectList();
    defectList.init();
});
