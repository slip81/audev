﻿function StorageFolderTree() {
    _this = this;

    this.init = function () {
        //
    };

    this.onStorageFolderExpandClick = function (e) {
        $("#folderTree").getKendoTreeView().expand('.k-item');
    };

    this.onStorageFolderCollapseClick = function (e) {
        $("#folderTree").getKendoTreeView().collapse('.k-item');
    };

    this.onStorageFolderSearchKeyup = function (search) {
        //
    };

    this.onFolderTreeSelect = function (e) {
        //
    };
    
    this.onFolderTreeChange = function (e) {
        //
    };

    this.onStorageFolderSelectClick = function (e) {
        //alert('onStorageFolderSelectClick !');
        var folderTree = $("#folderTree").getKendoTreeView();
        var currentFolder = folderTree.select();
        if ((!currentFolder) || (currentFolder.length <= 0)) {
            return;
        }
        var dataItem = folderTree.dataItem(currentFolder);
        if ((!dataItem) || (dataItem.length <= 0)) {
            return;
        }

        $('#storage_folder_id').val(dataItem.id);
        //var currentFolderText = dataItem.folder_name;
        var parents = currentFolder.add(currentFolder.parentsUntil('.k-treeview', '.k-item'));
        var nodePath = $.map(parents, function (parent) {
            return $(parent).find('>div span.k-in span.storage-tree-view-simple').text();
        });

        //var currentFolderText = currentFolder.text();
        //$('#storage-folder-selected-span').html("<span class='storage storage-folder'></span>" + currentFolderText);
        $('#storage-folder-selected-span').text(nodePath.join("\\"));
        if (!$('#storage-folder-tree-div').hasClass('hidden')) {
            $('#storage-folder-tree-div').addClass('hidden');
        };
        if ($('#storage-folder-selected-div').hasClass('hidden')) {
            $('#storage-folder-selected-div').removeClass('hidden')
        };
        $('#is_in_storage').prop('checked', true);
    };
}

var storageFolderTree = null;

$(function () {
    storageFolderTree = new StorageFolderTree();
    storageFolderTree.init();
});
