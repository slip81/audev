﻿function StorageLog() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#storageLogGrid', 0);
    };

    this.onStorageLogGridDataBound = function (e) {
        drawEmptyRow('#storageLogGrid');
    };
}

var storageLog = null;

$(function () {
    storageLog = new StorageLog();
    storageLog.init();
});
