﻿function Main() {
    _this = this;

    this.init = function () {
        _this.setMainContentText();
    };

    this.placeholder = function(element) {
        return element.clone().addClass("placeholder");
    };

    this.hint = function(element) {
        return element.clone().addClass("hint")
                    .height(element.height())
                    .width(element.width());
    };

    this.onSidebarChange = function(e) {
        //alert('onSidebarChange !');
    };

    this.onMainContentChange = function(e) {
        var container_id = e.sender.element.attr("id"),
                id = e.item.attr("id"),
                newIndex = e.newIndex,
                oldIndex = e.oldIndex;        
        _this.setWidget(id, oldIndex, newIndex);
        //
        _this.setMainContentText();
    };

    this.setWidget = function(widget_id, old_index, new_index) {
        var postData = JSON.stringify({ id: widget_id, oldIndex: old_index, newIndex: new_index });
        $.ajax({
            type: "POST",
            url: "SetWidget",
            data: postData,
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response.err) {
                    alert(response.err)
                } else {
                    //
                };
            },
            error: function (response) {
                if (response.err) {
                    alert(response.err)
                } else {
                    alert('Ошибка при попытке установки виджета')
                };
            }
        });
    };

    this.updateWidget = function(widget_id, widget_param) {
        var postData = JSON.stringify({ id: widget_id, param: widget_param });
        $.ajax({
            type: "POST",
            url: "UpdateWidget",
            data: postData,
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response.err) {
                    alert(response.err)
                } else {
                    //
                };
            },
            error: function (response) {
                if (response.err) {
                    alert(response.err)
                } else {
                    alert('Ошибка при попытке обновления параметров виджета')
                };
            }
        });
    };

    this.btnOrderService = function(curr_widget_id, curr_service_id) {        
        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Заказать услугу ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "80px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#service-order-confirmation").html())
            .center().open();

        kendoWindow.find(".save-changes-confirm,.save-changes-cancel")
                .click(function () {
                    if ($(this).hasClass("save-changes-confirm")) {
                        kendoWindow.data("kendoWindow").close();
                        $.ajax({
                            type: "POST",
                            url: "ServiceOrder",
                            data: JSON.stringify({ service_id: curr_service_id }),
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                if (response.err) {
                                    alert(response.err)
                                } else {
                                    var header_span = $('#' + curr_widget_id).find('span.widget-sidebar-header');                                        
                                    $(header_span).html("<span title='Услуга заказана'><img class='widget-icon-svg widget-sidebar-only' src='" + url_medium_priority_svg + "' /></span>");
                                    var content_div = $('#' + curr_widget_id).find('div.widget-content');
                                    $(content_div).html("услуга заказана");
                                };
                            },
                            error: function (response) {
                                if (response.err) {
                                    alert(response.err)
                                } else {
                                    alert('Ошибка при попытке установки виджета')
                                };
                            }
                        });
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end();
            
    };

    this.setMainContentText = function() {
        var obj = $('#widget-main').find('div').first();
        if (obj.length == 0) {
            $('#main-content-default').removeClass('hidden');
        } else {
            $('#main-content-default').addClass('hidden');
        };
    }

}

var main = null;

$(function () {
    main = new Main();
    main.init();
});
