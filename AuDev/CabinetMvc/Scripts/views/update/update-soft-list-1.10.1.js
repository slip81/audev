﻿function UpdateSoftList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#updateSoftGrid', 0);
    };

    this.onUpdateSoftVersionGridSave = function (e) {
        //alert('onUpdateSoftVersionGridSave !');        
        if (!e.model.dirty) {
            e.model.dirty = true;
            //e.sender.dataSource.sync();
        };
    };

    this.onUpdateSoftGridDataBound = function (e) {
        drawEmptyRow('#updateSoftGrid')
    };
}

var updateSoftList = null;

$(function () {
    updateSoftList = new UpdateSoftList();
    updateSoftList.init();
});
