﻿function UpdateBatchList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#updateBatchGrid', 0);
    };

    this.onUpdateBatchGridDataBound = function (e) {
        drawEmptyRow('#updateBatchGrid')
    };
}

var updateBatchList = null;

$(function () {
    updateBatchList = new UpdateBatchList();
    updateBatchList.init();
});
