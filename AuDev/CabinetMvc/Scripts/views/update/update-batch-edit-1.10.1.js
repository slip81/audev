﻿function UpdateBatchEdit() {
    _this = this;

    this.init = function () {
        //
        $("form").kendoValidator();
        //
        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            var curr_client_id = null;
            if (!model_batch_id) {
                var grid = $('#clientGrid').getKendoGrid();
                var selectedItem = grid.dataItem(grid.select());
                if (selectedItem)
                    curr_client_id = selectedItem.id;
                $('#client_id').val(curr_client_id);
            }

            $('#update_batch_item_list').val(JSON.stringify($('#updateBatchItemGrid').getKendoGrid().dataSource.view()));

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
    };

    this.getBatchId = function (e) {
        return { batch_id: model_batch_id };
    };

    this.onUpdateBatchItemGridDataBound = function (e) {
        drawEmptyRow('#updateBatchItemGrid');
    };

    this.checkAll = function (ele) {
        var state = $(ele).is(':checked');
        var grid = $("#updateBatchItemGrid").data("kendoGrid");
        var view = grid.dataSource.view();

        for (var i = 0; i < view.length; i++) {
            var dataRow = view[i];
            var elementRow = grid.table.find(".checkbox")[i];
            if (elementRow != null) {
                var checked = elementRow.checked,
                    row = $(elementRow).closest("tr"),
                    dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));
                
                if (state) {
                    elementRow.checked = true;
                    dataItem.is_subscribed = true;
                } else {
                    elementRow.checked = false;
                    dataItem.is_subscribed = false;
                }
            }
        };
    };

    this.checkEl = function (ele) {
        var state = $(ele).is(':checked');
        var grid = $("#updateBatchItemGrid").data("kendoGrid");
        var view = grid.dataSource.view();
        var row = $(ele).closest("tr");
        var dataItem = grid.dataItem(row);
        dataItem.is_subscribed = state;
    };

}

var updateBatchEdit = null;

$(function () {
    updateBatchEdit = new UpdateBatchEdit();
    updateBatchEdit.init();
});
