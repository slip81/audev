﻿function ExchangeRegPartnerEdit() {
    _this = this;

    this.init = function () {        
        onUploadEqualsDownloadClick();

        $("#btnSubmit").click(function (e) {
            var combo1 = $("#exchangePartnerDropDownList").data("kendoDropDownList");
            $("#ds_id").val(combo1.dataItem()["ds_id"]);
            var combo2 = $("#exchangeFormatTypeDropDownList").data("kendoDropDownList");
            $("#format_type").val(combo2.dataItem()["obj_id"]);
            var combo3 = $("#exchangeTransportTypeDropDownList").data("kendoDropDownList");
            $("#transport_type").val(combo3.dataItem()["obj_id"]);
            var combo4 = $("#exchangeUploadArcTypeDropDownList").data("kendoDropDownList");
            $("#upload_arc_type").val(combo4.dataItem()["obj_id"]);
            var combo5 = $("#exchangeDownloadArcTypeDropDownList").data("kendoDropDownList");
            $("#download_arc_type").val(combo5.dataItem()["obj_id"]);
            //
            $("#btnSubmit").attr('disabled', true);
            $("#btnSubmitProceed").trigger('click');
        });

        $("#upload_equals_download_email").click(function (e) {
            onUploadEqualsDownloadClick(e);
        });
    };
    
    this.onTransportTypeDataBound = function (e) {
        if (firstDataBoundTransport) {
            firstDataBoundTransport = false;
            _this.onTransportTypeChange();
        };
    };

    this.onTransportTypeChange = function(e) {
        var curr_transport_type = $("#exchangeTransportTypeDropDownList").data("kendoDropDownList").dataItem()["obj_id"];
        if (curr_transport_type == 1) {
            $("#ftp-group").show();
            $("#email-group").hide();
        } else if (curr_transport_type == 3) {
            $("#ftp-group").hide();
            $("#email-group").show();
        } else {
            $("#ftp-group").hide();
            $("#email-group").hide();
        };
    };

    function onUploadEqualsDownloadClick(e) {
        var curr_upload_equals_download = ($('#upload_equals_download_email:checked').length > 0) ? true : false;
        if (curr_upload_equals_download) {
            $("#email-upload-group").hide();
        } else {
            $("#email-upload-group").show();
        }
    };
}

var exchangeRegPartnerEdit = null;

var firstDataBoundTransport = true;

$(function () {
    exchangeRegPartnerEdit = new ExchangeRegPartnerEdit();
    exchangeRegPartnerEdit.init();
});
