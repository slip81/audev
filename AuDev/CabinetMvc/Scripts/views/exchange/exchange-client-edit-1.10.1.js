﻿function ExchangeClientAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $('.attr-change').bind('input propertychange', onAttrChanged)

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".save-changes-confirm,.save-changes-cancel")
                        .click(function () {
                            if ($(this).hasClass("save-changes-confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('k-state-active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-revert-visible');
                                $('#btnRevert').removeClass('k-state-focused');
                                $('#btnRevert').addClass('btn-revert-hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();

            $("#client_name").val(htmlDecode(model_client_name));

            $('#btnSubmit').removeClass('k-state-active');
            $('#btnSubmit').attr('disabled', true);
            $('#btnRevert').removeClass('btn-revert-visible');
            $('#btnRevert').removeClass('k-state-focused');
            $('#btnRevert').addClass('btn-revert-hidden');
        });
    };

    function onAttrChanged() {
        $('#btnSubmit').removeAttr('disabled');
        $('#btnSubmit').addClass('k-state-active');
        $('#btnRevert').removeClass('btn-revert-hidden');
        $('#btnRevert').addClass('btn-revert-visible');
        $('#btnRevert').addClass('k-state-focused');
    };
}

var exchangeClientAdd = null;

$(function () {
    exchangeClientAdd = new ExchangeClientAdd();
    exchangeClientAdd.init();
});
