﻿function ExchangeClientAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            var selected_client_id = null;
            var selected_client_name = null;
            var grid = $("#clientGrid").getKendoGrid();
            var selectedItem = grid.dataItem(grid.select());
            if (selectedItem) {
                selected_client_id = selectedItem['id'];
                selected_client_name = selectedItem['client_name'];
            };

            $("#cabinet_client_id").val(selected_client_id);
            $("#client_name").val(selected_client_name);

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
    };
}

var exchangeClientAdd = null;

$(function () {
    exchangeClientAdd = new ExchangeClientAdd();
    exchangeClientAdd.init();
});
