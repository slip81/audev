﻿function ExchangeFileNodeAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            var curr_client_id = null;
            var curr_sales_id = null;
            var curr_partner_id = null;

            var grid = $('#clientGrid').getKendoGrid();
            var selectedItem = grid.dataItem(grid.select());
            if (selectedItem)
                curr_client_id = selectedItem.id;
            $('#client_id').val(curr_client_id);

            curr_sales_id = $('#salesList').getKendoDropDownList().value();
            $('#sales_id').val(curr_sales_id);

            curr_partner_id = $('#dsList').getKendoDropDownList().value();
            $('#partner_id').val(curr_partner_id);

            $('#node_item_send_list').val(JSON.stringify($('#nodeItemSendGrid').getKendoGrid().dataSource.view()));
            $('#node_item_get_list').val(JSON.stringify($('#nodeItemGetGrid').getKendoGrid().dataSource.view()));

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
    };

    this.getDsId = function(e) {
        //
        var curr_ds_id = -1;
        var ddl = $('#dsList').getKendoDropDownList();
        if (ddl) {
            curr_ds_id = ddl.value();
        };
        return { ds_id: curr_ds_id };
    };

    this.onNodeItemSendGridDataBound = function (e) {
        drawEmptyRow('#nodeItemSendGrid');
    };

    this.onNodeItemGetGridDataBound = function (e) {
        drawEmptyRow('#nodeItemGetGrid');
    };

    this.onDsListChange = function(e) {
        $('#nodeItemSendGrid').getKendoGrid().dataSource.read();
        $('#nodeItemGetGrid').getKendoGrid().dataSource.read();
    };

    this.onClientGridChange = function(e) {
        $('#salesList').getKendoDropDownList().dataSource.read();
    };

    this.getClientId = function(e) {
        var curr_client_id = -1;
        var grid = $('#clientGrid').getKendoGrid();
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem) {
            curr_client_id = selectedItem.id;
        };
        return { client_id: curr_client_id };
    };
}

var exchangeFileNodeAdd = null;

$(function () {
    exchangeFileNodeAdd = new ExchangeFileNodeAdd();
    exchangeFileNodeAdd.init();
});
