﻿function ExchangeMonitor() {
    _this = this;

    this.init = function () {
        $(document).on('click', '#chbErrorOnly', function () {
            _this.exchangeMonitorGridRefresh();
        });

        setInterval(function () {
            var checked = $('#chbAutoRefresh').is(':checked');
            if (checked) {
                if (refreshFirstAttempt) {
                    refreshFirstAttempt = false;
                } else {
                    _this.exchangeMonitorGridRefresh();
                }
            }
        }, refreshInterval);
    };

    this.exchangeMonitorGridRefresh = function (e) {
        var grid = $('#exchangeMonitorGrid').getKendoGrid();
        grid.dataSource.read();
    };

    this.onPartnerDataBound = function(e) {
        var ddl = $('#partnerDropDownList').getKendoDropDownList()
        ddl.select(0);
        _this.onPartnerChange();
    };

    this.onClientChange = function (e) {        
        var ddl = $('#partnerDropDownList').getKendoDropDownList()
        ddl.dataSource.read();
    };

    this.onPartnerChange = function(e) {
        _this.exchangeMonitorGridRefresh();
    };

    this.setClient = function(e) {
        var id = $('#clientDropDownList').getKendoDropDownList().dataItem()["client_id"];
        return {
            client_id: id
        };
    };

    this.setMonitorData = function(e) {
        var ddl1 = $('#clientDropDownList').getKendoDropDownList();
        var ddl2 = $('#partnerDropDownList').getKendoDropDownList();
        var id1 = -1;
        var id2 = -1;
        var id3 = false;

        if (ddl1) {
            var item1 = ddl1.dataItem();
            if (item1) {
                id1 = item1["client_id"];
            }
        };
        if (ddl2) {
            var item2 = ddl2.dataItem();
            if (item2) {
                id2 = item2["ds_id"];
            };
        };
        id3 = $('#chbErrorOnly').is(':checked');

        return {
            client_id: id1,
            ds_id: id2,
            errors_only: id3,
        };
    };

    this.setMonitorLogData = function(num) {
        var ddl2 = $('#partnerDropDownList').getKendoDropDownList();
        var id2 = -1;
        if (ddl2) {
            var item2 = ddl2.dataItem();
            if (item2) {
                id2 = item2["ds_id"];
            };
        };
        return {
            ds_id: id2,
            exchange_direction: num
        };
    };

    this.onImageClick = function(e, num, err_level) {
        if (num == -1) {
            return false;
        };

        var grid = $("#exchangeMonitorGrid").data("kendoGrid");
        var row = $(e).closest("tr");
        var dataItem = grid.dataItem(row);
        var id = dataItem['id'];
        var detailGrid1Name = '#grid1_' + id;
        var detailGrid2Name = '#grid2_' + id;
        var detailGrid1 = $(detailGrid1Name).data("kendoGrid");
        var detailGrid2 = $(detailGrid2Name).data("kendoGrid");
        var detailGridFilter = { logic: "and", filters: [] };
        var detailTabName = '#tabDetail_' + id;
        var tab_strip = $(detailTabName).getKendoTabStrip();

        // детализация не раскрыта - раскрываем ее с нужным фильтром
        if (!(expandedTabs[id])) {
            expandedTabs[id] = 1;
            if (num == 0) {
                expandedGrids1[id] = 1;
                expandedGrids2[id] = 0;
            } else {
                expandedGrids1[id] = 0;
                expandedGrids2[id] = 1;
            };
            grid.expandRow(row);
            tab_strip = $(detailTabName).getKendoTabStrip();
            tab_strip.select(num);

            if (err_level != -1) {
                detailGridFilter.filters.push({ field: "error_level", operator: "equals", value: err_level });
                if (num == 0) {
                    detailGrid1 = $(detailGrid1Name).data("kendoGrid");
                    detailGrid1.dataSource.filter(detailGridFilter);
                    detailGrid1.dataSource.read();
                } else {
                    detailGrid2 = $(detailGrid2Name).data("kendoGrid");
                    detailGrid2.dataSource.filter(detailGridFilter);
                    detailGrid2.dataSource.read();
                };
            }

            //alert('added to expandedTabs: ' + id);
            // детализация раскрыта - скрываем ее, либо если задан фильтр - не скрывваем а фильтруем
        } else {
            if (num == 0) {
                if (!(expandedGrids1[id])) {
                    expandedGrids1[id] = 1;
                    expandedGrids2[id] = 0;
                    tab_strip = $(detailTabName).getKendoTabStrip();
                    tab_strip.select(num);
                } else {
                    if (err_level == -1) {
                        expandedTabs[id] = 0;
                        expandedGrids1[id] = 0;
                        expandedGrids2[id] = 0;
                        grid.collapseRow(row);
                    }
                    //alert('removed from expandedTabs: ' + id);
                };
            } else {
                if (!(expandedGrids2[id])) {
                    expandedGrids1[id] = 0;
                    expandedGrids2[id] = 1;
                    tab_strip = $(detailTabName).getKendoTabStrip();
                    tab_strip.select(num);
                } else {
                    if (err_level == -1) {
                        expandedTabs[id] = 0;
                        expandedGrids1[id] = 0;
                        expandedGrids2[id] = 0;
                        grid.collapseRow(row);
                    }
                    //alert('removed from expandedTabs: ' + id);
                };
            };

            if (err_level != -1) {
                detailGridFilter.filters.push({ field: "error_level", operator: "equals", value: err_level });
                if (num == 0) {
                    detailGrid1 = $(detailGrid1Name).data("kendoGrid");
                    detailGrid1.dataSource.filter(detailGridFilter);
                    detailGrid1.dataSource.read();
                } else {
                    detailGrid2 = $(detailGrid2Name).data("kendoGrid");
                    detailGrid2.dataSource.filter(detailGridFilter);
                    detailGrid2.dataSource.read();
                };
            }
        }
    };


    this.onDataBound = function(e) {
        this.tbody.find("tr.k-master-row>.k-hierarchy-cell>a").hide();
        //
        var grid = $("#exchangeMonitorGrid").data("kendoGrid");
        $.each(expandedTabs, function (key, value) {
            if (value) {
                var dataItem = grid.dataSource.get(key);
                if (dataItem) {
                    var row = grid.tbody.find("tr[data-uid='" + dataItem.uid + "']");
                    var num = 0;
                    if (expandedGrids2[key]) {
                        num = 1;
                    };
                    grid.expandRow(row);
                    tab_strip = $('#tabDetail_' + key).getKendoTabStrip();
                    tab_strip.select(num);
                }
            }
        });
        //        
        var today = new Date();
        var data = grid.dataSource.data();
        for (var x = 0; x < data.length; x++) {
            var dataItem = data[x];
            var tr = $("#exchangeMonitorGrid").find("[data-uid='" + dataItem.uid + "']");
            var last_ok_date = dataItem.download_ok_date;
            var diff = daydiff(parseDate(last_ok_date), parseDate(today));
            if (diff >= 5) {
                tr.css('background-color', 'rgb(255, 204, 204)');
            } else if (diff >= 2) {
                tr.css('background-color', 'rgb(255, 255, 179)');
            } else {
                last_ok_date = dataItem.upload_ok_date;
                diff = daydiff(parseDate(last_ok_date), parseDate(today));
                if (diff >= 5) {
                    tr.css('background-color', 'rgb(255, 204, 204)');
                } else if (diff >= 2) {
                    tr.css('background-color', 'rgb(255, 255, 179)');
                };
            };
        };
    };

    this.onDataBound2 = function(grid_name) {
        return function (e) {
            var rows = e.sender.tbody.children();

            for (var j = 0; j < rows.length; j++) {
                var row = $(rows[j]);
                var dataItem = e.sender.dataItem(row);
                if (dataItem.get("error_level") == '2') {
                    row.addClass("k-error-colored");
                } else if (dataItem.get("error_level") == '1') {
                    row.addClass("k-info-colored");
                };
            };
            drawEmptyRow('#' + grid_name);
        };
    };

    this.onTabShow = function(e) {
        var grid = $('#exchangeMonitorGrid').getKendoGrid();
        var row = e.sender.element.closest("tr").prev();
        var dataItem = grid.dataItem(row);
        var id = dataItem['id'];
        var tab_index = $(e.item).index();
        //
        if (tab_index == 0) {
            expandedGrids1[id] = 1;
            expandedGrids2[id] = 0;
        } else {
            expandedGrids1[id] = 0;
            expandedGrids2[id] = 1;
        };
    };


    // private

    function parseDate(str) {
        return kendo.parseDate(str);
    };

    function daydiff(first, second) {
        return Math.round((second - first) / (1000 * 60 * 60 * 24));
    };
}

var exchangeMonitor = null;

var refreshInterval = 60000; // 60 sec
var refreshFirstAttempt = true;
var expandedTabs = {};
var expandedGrids1 = {};
var expandedGrids2 = {};

$(function () {
    exchangeMonitor = new ExchangeMonitor();
    exchangeMonitor.init();
});
