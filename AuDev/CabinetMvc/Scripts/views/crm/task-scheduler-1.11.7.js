﻿function TaskScheduler() {
    _this = this;

    this.init = function () {        
        $("#crmTaskOverdueListGrid").on("dblclick", "tr", function () {
            var grid = $("#crmTaskOverdueListGrid").data("kendoGrid");
            curr_task_id = 0;
            var selected_row = $(this);
            var selected_data_item = grid.dataItem($(this));
            var curr_task_group_id = 0;
            var curr_win_task_name = 'Scheduler';
            if (selected_data_item != null) {
                curr_task_id = selected_data_item.task_id;
                curr_task_group_id = selected_data_item.group_id;
                if (curr_task_id > 0) {
                    var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');
                    $('#winTaskEdit' + curr_win_task_name).html("");
                    win.refresh({
                        url: 'Crm/TaskEdit' + curr_win_task_name,
                        data: { task_id: curr_task_id }
                    });
                    win.open();
                } else {                    
                    var sch = $('#scheduler').getKendoScheduler();

                    $("#windowcontainer").append("<div id='window'></div>");
                    var kendoWindow = $("#window").kendoWindow({
                        actions: ['close'],
                        title: "Событие",
                        resizable: true,
                        modal: true,
                        width: "450px",
                        deactivate: function () {
                            this.destroy();
                        }
                    });

                    var wnd = kendoWindow.data("kendoWindow");
                    wnd.content($("#crm-scheduler-edit-template2").html());
                    wnd.title('Событие ' + selected_data_item.task_num + ' от ' + kendo.format('{0:d}', selected_data_item.crt_date) + ' ' + selected_data_item.crt_user);

                    $("#date_beg2").kendoDatePicker();
                    $("#date_end2").kendoDatePicker();
                    $("#date_plan2").kendoDatePicker();
                    $("#date_fact2").kendoDatePicker();

                    $("#date_beg2").getKendoDatePicker().value(selected_data_item.Start);
                    $("#date_end2").getKendoDatePicker().value(selected_data_item.End);
                    $("#description2").val(selected_data_item.Description);
                    $("#event_progress2").val(selected_data_item.event_progress);
                    $("#event_result2").val(selected_data_item.event_result);
                    $("#date_plan2").getKendoDatePicker().value(selected_data_item.date_plan);
                    $("#date_fact2").getKendoDatePicker().value(selected_data_item.date_fact);
                    $("#scheduler_link2").attr("href", "/TaskScheduler?event_id=" + selected_data_item.event_id);

                    wnd.center().open();

                    kendoWindow.find(".action-confirm,.action-cancel")
                            .click(function () {
                                if ($(this).hasClass("action-confirm")) {
                                    kendoWindow.data("kendoWindow").close();
                                    return false;
                                } else {
                                    kendoWindow.data("kendoWindow").close();
                                    return false;
                                };
                            })
                         .end();
                    return false;
                };
            };
        });

        $(document).kendoTooltip({
            filter: ".k-event:not(.k-event-drag-hint) > div, .k-task",
            position: "top",
            width: 250,
            content: kendo.template($('#crm-scheduler-event-tooltip-template').html())
        });

        $('#btn-refresh').click(function (e) {
            refreshScheduler();
            refreshOverdueList();
        });

        $('#btn-export-pdf').click(function (e) {
            $('.k-button, .k-pdf').trigger('click');
        });

        $('#btn-export-excel').click(function (e) {
            var sch = $('#scheduler').getKendoScheduler();
            var date_beg = new Date(sch.view().startDate()).toISOString();
            var date_end = new Date(sch.view().endDate()).toISOString();
            var user_id = -1;
            var user_name = 'все';
            var ddl1 = $('#userList').getKendoDropDownList();
            if (ddl1) {
                var item1 = ddl1.dataItem();
                if (item1) {
                    user_id = item1.user_id;
                    user_name = item1.user_name;
                };
            };
            var overdue_only = overdueViewSelected;
            var priority_control = $('#chbPriorityControl').is(':checked');

            window.location = '/TaskSchedulerExport?date_beg=' + date_beg + '&date_end=' + date_end + '&user_id=' + user_id + '&user_name=' + user_name + '&overdue_only=' + overdue_only + '&priority_control=' + priority_control;
        });

        $('#btn-report1').click(function (e) {
            var kendoWindow = $("#winReport");
            kendoWindow.data("kendoWindow").center().open();
            kendoWindow
                .find(".confirm,.cancel")
                    .click(function () {
                        if ($(this).hasClass("confirm")) {
                            kendoWindow.data("kendoWindow").close();
                            var curr_report_user_id = 0;
                            var ddl = $('#ddlReportExecUser').getKendoDropDownList();
                            if (ddl) {
                                curr_report_user_id = ddl.value();
                            };
                            var dtBeg = $('#dtReportDateBeg').getKendoDatePicker().value();
                            var dtEnd = $('#dtReportDateEnd').getKendoDatePicker().value();
                            var sendData = { user_id: curr_report_user_id, date_beg: dtBeg, date_end: dtEnd };
                            $.ajax({
                                url: "GetReport1",
                                data: JSON.stringify(sendData),
                                type: "POST",
                                contentType: 'application/json; charset=windows-1251',
                                success: function (res) {
                                    if ((!res) || (res.err)) {
                                        alert('Ошибка при формировании отчета');
                                    } else {
                                        window.location = '/GetReport1?file_name=' + res.file_name;
                                    }
                                },
                                error: function (res) {
                                    alert('Ошибка формирования отчета');
                                }
                            });
                        }
                        kendoWindow.data("kendoWindow").close();
                    })
                 .end()
        });

        $('#chbPriorityControl').click(function (e) {
            refreshScheduler();
        });

        $('#btn-period-apply').click(function () {
            var scheduler = $("#scheduler").getKendoScheduler();
            scheduler.view("CustomDaysView");
        });

        $('input[type=radio][name=overdueMode]').change(function () {
            // !!!
            //alert(this.value);
            $('#crmTaskOverdueListGrid').getKendoGrid().dataSource.read();
        });
    };

    // task-edit-scheduler.js

    this.init_popup = function () {        

        $('.attr-change').bind('input propertychange', _this.onAttrChanged);

        $('.subtask-addScheduler').click(function (e) {
            onSubtaskAddClick(e);
        });

        $('.task-note-addScheduler').click(function (e) {
            onTaskNoteAddClick(e);
        });

        $('.subtask-excelScheduler').click(function (e) {
            onSubtaskExcelClick(e);
        });

        $('.subtask-pdfScheduler').click(function (e) {
            onSubtaskPdfClick(e);
        });

        $('#btnTaskOkScheduler').click(function (e) {
            onBtnTaskOkClick(e);
        });

        $('#btnTaskSaveScheduler').click(function (e) {
            onBtnTaskSaveClick(e);
        });

        $('#btnTaskCancelScheduler').click(function (e) {
            onBtnTaskCancelClick(e);
        });

        $('.attr-change').click(function (e) {
            _this.onAttrChanged();
        });

        $('div#div-subtaskScheduler ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
            $('#div-commentScheduler').toggleClass('hidden');
            $('#div-subtaskScheduler').toggleClass('col-sm-6');
            $('#div-subtaskScheduler').toggleClass('col-sm-12');
        });

        $('div#div-commentScheduler ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
            $('#div-subtaskScheduler').toggleClass('hidden');
            $('#div-commentScheduler').toggleClass('col-sm-6');
            $('#div-commentScheduler').toggleClass('col-sm-12');
            if ($('#div-subtaskScheduler').hasClass('hidden')) {
                $('#div-commentScheduler').css('margin-left', '10px');
            } else {
                $('#div-commentScheduler').css('margin-left', '-10px');
            };
        });
    };

    // public

    this.getTaskOverdueListData = function() {
        var par3 = -1;
        var ddl1 = $('#userList').getKendoDropDownList();

        if (ddl1) {
            var item1 = ddl1.dataItem();
            if (item1) {
                par3 = item1["user_id"];
            }
        };

        var overdue = ($('#rbOverdue_:checked').length > 0) ? true : false;
        var canceled = ($('#rbCanceled_:checked').length > 0) ? true : false;

        return { show_done: null, show_canceled: null, user_id: par3, overdue_only: overdue, date_beg: null, date_end: null, canceled_only: canceled };
    }

    this.onSchedulerEdit = function (e) {
        dblclickFired = true;
        curr_task_id = 0;
        curr_event = e.event;
        curr_event.IsDateChanged = false;
        var btn1 = $('.k-scheduler-edit-form').find('#btn-crm-scheduler-simple');
        var btnActive = $('.k-scheduler-edit-form').find('#btn-crm-scheduler-simple-active');
        var btnDone = $('.k-scheduler-edit-form').find('#btn-crm-scheduler-simple-done');
        var btnCanceled = $('.k-scheduler-edit-form').find('#btn-crm-scheduler-simple-canceled');
        var row1 = $('.k-scheduler-edit-form').find('#div-crm-scheduler-row1');
        var row3 = $('.k-scheduler-edit-form').find('#div-crm-scheduler-row3');
        var ev_progress = $('.k-scheduler-edit-form').find('#div-crm-scheduler-simple-progress');
        var ev_result = $('.k-scheduler-edit-form').find('#div-crm-scheduler-simple-result');
        var hdr = $('.k-window-titlebar').find('.k-window-title');
        var date_fact_div = $('.k-scheduler-edit-form').find('#div-date-fact');
        if (curr_event.event_id > 0) {
            setTimeout(function () { dblclickFired = false; }, 800);
            if (curr_event.task_id) {
                e.preventDefault();
                curr_task_id = curr_event.task_id;
                openSchedulerTask();                
                return false;
            } else {
                //
                checkAuthorization(url_IsAuthorized, url_Login);
                //
                btn1.trigger('click');
                row1.addClass('hidden');
                ev_progress.removeClass('hidden');
                ev_result.removeClass('hidden');
                row3.removeClass('hidden');
                //
                date_fact_div.addClass('hidden');
                if (curr_event.is_done) {
                    btnDone.trigger('click');
                    date_fact_div.removeClass('hidden');
                } else if (curr_event.is_canceled) {
                    btnCanceled.trigger('click');
                };
                //                
                hdr.text('Событие ' + curr_event.task_num + ' от ' + kendo.format('{0:d}', curr_event.crt_date) + ' ' + curr_event.crt_user);
            };
        } else if (curr_event.event_id == -1) {
            e.preventDefault();
            curr_task_id = 0;
            openSchedulerTask();
            return false;
        } else {
            setTimeout(function () { dblclickFired = false; }, 800);
            //
            checkAuthorization(url_IsAuthorized, url_Login);
            //
            if (curr_event.task_id != -1) {
                //
            } else {
                btn1.trigger('click');
                row1.addClass('hidden');
                hdr.text('Новое событие');
            };
            //
            var wnd = $(e.container).data("kendoWindow");
            wnd.setOptions({
                position: {
                    top: 100, // or "100px"
                    left: "20%"
                },
                resizable: true,
                activate: function () {
                    $('#crmSchedulerTaskListGrid').getKendoGrid().dataSource.read();
                }
            });
        };
    };

    this.onBtnTaskClick = function(e) {        
        $('#btn-crm-scheduler-task').removeClass('btn-default');
        $('#btn-crm-scheduler-task').addClass('btn-primary');

        $('#btn-crm-scheduler-simple').removeClass('btn-primary');
        $('#btn-crm-scheduler-simple').addClass('btn-default');

        $('#div-crm-scheduler-simple').addClass('hidden');
        $('#div-crm-scheduler-task').removeClass('hidden');
    };

    this.onBtnEventClick = function (e) {
        $('#btn-crm-scheduler-simple').removeClass('btn-default');
        $('#btn-crm-scheduler-simple').addClass('btn-primary');

        $('#btn-crm-scheduler-task').removeClass('btn-primary');
        $('#btn-crm-scheduler-task').addClass('btn-default');

        $('#div-crm-scheduler-task').addClass('hidden');
        $('#div-crm-scheduler-simple').removeClass('hidden');
    };

    this.onBtnEventActiveClick = function(e) {
        $('#btn-crm-scheduler-simple-active').removeClass('btn-default');
        $('#btn-crm-scheduler-simple-active').addClass('btn-primary');
        //
        $('#btn-crm-scheduler-simple-done').removeClass('btn-success');
        $('#btn-crm-scheduler-simple-done').addClass('btn-default');
        //
        $('#btn-crm-scheduler-simple-canceled').removeClass('btn-warning');
        $('#btn-crm-scheduler-simple-canceled').addClass('btn-default');
        //
        $('#div-date-fact').addClass('hidden');
        //
        curr_event.is_done = false;
        curr_event.is_canceled = false;        
        curr_event.dirty = true;
    };

    this.onBtnEventDoneClick = function(e) {
        $('#btn-crm-scheduler-simple-active').removeClass('btn-primary');
        $('#btn-crm-scheduler-simple-active').addClass('btn-default');
        //
        $('#btn-crm-scheduler-simple-done').removeClass('btn-default');
        $('#btn-crm-scheduler-simple-done').addClass('btn-success');
        //
        $('#btn-crm-scheduler-simple-canceled').removeClass('btn-warning');
        $('#btn-crm-scheduler-simple-canceled').addClass('btn-default');
        //
        $('#div-date-fact').removeClass('hidden');
        //
        curr_event.is_done = true;
        curr_event.is_canceled = false;        
        curr_event.dirty = true;
    };

    this.onBtnEventCanceledClick = function(e) {
        $('#btn-crm-scheduler-simple-active').removeClass('btn-primary');
        $('#btn-crm-scheduler-simple-active').addClass('btn-default');
        //
        $('#btn-crm-scheduler-simple-done').removeClass('btn-success');
        $('#btn-crm-scheduler-simple-done').addClass('btn-default');
        //
        $('#btn-crm-scheduler-simple-canceled').removeClass('btn-default');
        $('#btn-crm-scheduler-simple-canceled').addClass('btn-warning');
        //
        $('#div-date-fact').addClass('hidden');
        //
        curr_event.is_done = false;
        curr_event.is_canceled = true;        
        curr_event.dirty = true;
    };

    this.getTaskListData = function() {
        return { group_id: -1 };
    }

    this.onSchedulerSave = function(e) {        
        var task_num_val = $('.k-scheduler-edit-form').find('#task_num').val();
        e.event.task_num = task_num_val;
    };

    this.onAttrChanged = function() {
        $('#btnTaskOkScheduler').removeClass('btn-default');
        $('#btnTaskOkScheduler').addClass('btn-success');
        $('#btnTaskOkScheduler').removeAttr('disabled');
        //
        $('#btnTaskSaveScheduler').removeClass('btn-default');
        $('#btnTaskSaveScheduler').addClass('btn-info');
        $('#btnTaskSaveScheduler').removeAttr('disabled');
    };

    this.onSubtaskGridSchedulerDataBound = function(e) {
        $(".stateListCell").each(function () {
            eval($(this).children("script").last().html());
        });
        $(".fileListCell").each(function () {
            eval($(this).children("script").last().html());
        });
        var grid = $('#subtaskGridScheduler').getKendoGrid();
        var ds = grid.dataSource;
        var task_num_max = ds.aggregates().task_num.max;
        localStorage.subtask_num = task_num_max;
    };

    this.onSubtaskGridSchedulerEdit = function(e) {
        e.model.IsNew = true;
        //
        _this.onAttrChanged();
    };

    this.subtaskStateListChanged = function(e) {        
        var item = $('#subtaskGridScheduler').data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        item.set('state_id', ddl_val);
        item.set('state_id_ReadOnly', ddl_val);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.subtaskFileListChanged = function (e) {
        var item = $('#subtaskGrid' + curr_win_task_name).data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        var ddl_text = this.text();
        item.set('file_id', ddl_val);
        item.set('file_id_ReadOnly', ddl_val);
        item.set('file_name', ddl_text);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.onCrmSchedulerTaskListGridDataBound = function(e) {
        drawEmptyRow("#crmSchedulerTaskListGrid");
        //
        var grid = $("#crmSchedulerTaskListGrid").data("kendoGrid");
        var row = null;
        var cnt = 0;

        var data = grid.dataSource.data();
        for (var x = 0; x < data.length; x++) {
            var dataItem = data[x];
            var tr = $("#crmSchedulerTaskListGrid").find("[data-uid='" + dataItem.uid + "']");

            // state_name
            var col_ind = settings_state_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.state_fore_color) {

                    var is_row_colored = taskGridColorColumn_state_name;
                    if (is_row_colored)
                        tr.css('background-color', dataItem.state_fore_color);

                    if (dataItem.state_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.state_fore_color);
                }
            }
            // project_name
            col_ind = settings_project_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.project_fore_color) {

                    var is_row_colored = taskGridColorColumn_project_name;
                    if (is_row_colored)
                        tr.css('background-color', dataItem.project_fore_color);

                    if (dataItem.project_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.project_fore_color);
                }
            }
            // priority_name
            col_ind = settings_priority_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.priority_fore_color) {

                    var is_row_colored = taskGridColorColumn_priority_name;
                    if (is_row_colored)
                        tr.css('background-color', dataItem.priority_fore_color);

                    if (dataItem.priority_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.priority_fore_color);
                }
            }
            // exec_user_name
            col_ind = settings_exec_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.exec_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_exec_user_name;
                    if (is_row_colored)
                        tr.css('background-color', dataItem.exec_user_fore_color);

                    if (dataItem.exec_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.exec_user_fore_color);
                }
            }
            // owner_user_name
            col_ind = settings_owner_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.owner_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_owner_user_name;
                    if (is_row_colored)
                        tr.css('background-color', dataItem.owner_user_fore_color);

                    if (dataItem.owner_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.owner_user_fore_color);
                }
            }
            // assigned_user_name
            col_ind = settings_assigned_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.assigned_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_assigned_user_name;
                    if (is_row_colored)
                        tr.css('background-color', dataItem.assigned_user_fore_color);

                    if (dataItem.assigned_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.assigned_user_fore_color);
                }
            }
            //
            cnt = cnt + 1;
        };
    };

    this.onCrmSchedulerTaskListGridChange = function (e) {        
        var task_num_ctrl = $('.k-scheduler-edit-form').find('#task_num');
        var task_text_ctrl = $('.k-scheduler-edit-form').find('#task_text');
        var selectedRows = this.select();
        if (selectedRows) {
            var dataItem = this.dataItem(selectedRows[0]);
            task_num_ctrl.val(dataItem.task_num);
            task_text_ctrl.val(dataItem.task_text);
        } else {            
            task_num_ctrl.val('');
            task_text_ctrl.val('');
        };
    };

    this.getEventFilter = function(e) {
        var scheduler = $('#scheduler').getKendoScheduler();
        var ddl1 = $('#userList').getKendoDropDownList();
        var par1 = true;
        var par2 = true;
        var par3 = -1;
        var par4 = overdueViewSelected;
        var par5 = false;        
        var par6 = scheduler.view().startDate();
        var par7 = scheduler.view().endDate();

        if (ddl1) {
            var item1 = ddl1.dataItem();
            if (item1) {
                par3 = item1["user_id"];
            }
        };

        par5 = $('#chbPriorityControl').is(':checked');

        var overdue = false;
        var canceled = false;
        if (par4) {
            overdue = ($('#rbOverdue_:checked').length > 0) ? true : false;
            canceled = ($('#rbCanceled_:checked').length > 0) ? true : false;
        };

        return { show_done: par1, show_canceled: par2, user_id: par3, overdue_only: overdue, priority_control: par5, date_beg: par6, date_end: par7, canceled_only: canceled };
    };

    this.onSchedulerDataBound = function (e) {        
        if (firstDataBoundScheduler) {
            firstDataBoundScheduler = false;
            $('ul.k-scheduler-tools').addClass('hidden');
            if (execUserId) {
                $('#userList').getKendoDropDownList().value(execUserId);
                //forceRefresh = true;
                userChange_forScheduler(false);
            }
        };
        //
        removeTimeRows();
        //
        $('div.k-scheduler-times table.k-scheduler-table tbody').each(function() {
            $('th', this).each(function () {
                var th_text = $(this).text();
                if (th_text == 'Важные') {
                    $(this).css("background-color", "#c8dfdd");
                } else if (th_text == 'Срочные') {
                    $(this).css("background-color", "#bce6eb");
                };
            });
        });
        //
        if (forceRefresh) {
            forceRefresh = false;
            refreshScheduler();
            refreshOverdueList();
        }
        //
        scrollToToday(true);
        //
        if (taskId) {
            $('div.k-event-row[data-task-id="' + taskId + '"]').parent('.k-event').addClass('glowing-border');
        } else if (eventId) {
            $('div.k-event-row[data-event-id="' + eventId + '"]').parent('.k-event').addClass('glowing-border');
        };
    };

    this.onSchedulerNavigate = function(e) {
        if (e.action == 'changeView') {
            if (e.view == 'FakeView') {
                e.preventDefault();
                $('.k-scheduler-views>li').removeClass('k-state-selected');
                $('.k-view-fakeview').addClass('k-state-selected');
                $('#row-grid').removeClass('hidden');
                $('.k-scheduler-layout').addClass('hidden');
                $('.fake-view-only').removeClass('hidden');
                //
                $('#crmTaskOverdueListGrid').getKendoGrid().dataSource.read();
                $('#row-params-period').addClass('hidden');
                return false;
            } else {
                $('#row-grid').addClass('hidden');
                $('.k-scheduler-layout').removeClass('hidden');
                $('.fake-view-only').addClass('hidden');

                userChange_forScheduler(true);

                //
                if (e.view == 'OverdueView') {
                    forceRefresh = !overdueViewSelected;
                    overdueViewSelected = true;
                } else {
                    forceRefresh = overdueViewSelected;
                    overdueViewSelected = false;
                };
                //
                if (e.view == 'CustomDaysView') {
                    $('#row-params-period').removeClass('hidden');
                    $('.k-nav-today, .k-nav-prev, .k-nav-next').addClass('hidden');
                } else {
                    $('#row-params-period').addClass('hidden');
                    $('.k-nav-today, .k-nav-prev, .k-nav-next').removeClass('hidden');
                };
            }
        }
    };

    this.onSubtaskGridSchedulerRemove = function(e) {        
        setTimeout(function(){$('#subtaskGridScheduler').data('kendoGrid').dataSource.sync()})
    };

    this.onUserChange = function(e) {
        if ($('.k-view-fakeview').hasClass('k-state-selected')) {            
            userChange_forGrid();
        } else {
            userChange_forScheduler(false);
        };
    };

    this.onSubtaskGridSchedulerExcelExport = function(e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsExcel();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onSubtaskGridSchedulerPdfExport = function (e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsPDF();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onWinTaskEditSchedulerOpen = function(e) {
        checkAuthorization(url_IsAuthorized, url_Login);
    };
    
    this.onSchedulerRequestStart = function(e) {        
        checkAuthorization(url_IsAuthorized, url_Login);
    };

    this.onCrmTaskOverdueListGridRequestStart = function(e) {        
        checkAuthorization(url_IsAuthorized, url_Login);
    };

    this.onUplFileSchedulerSuccess = function(e) {        
        if (e.operation == 'upload') {

            var grid = $("#taskFilesGridScheduler").data("kendoGrid");
            grid.dataSource.read();

            _this.onAttrChanged();
        };

        return true;
    };

    this.onUplFileSchedulerError = function(e) {
        var err = e.XMLHttpRequest.responseText;
        if (err.length <= 200) {
            alert(err);
        };
    };

    this.onTaskNumCellClick = function (task_id) {        
        setTimeout(function () {
            if (!dblclickFired) {
                window.location = '/TaskEdit?task_id=' + task_id;
            }
        }, 500);
    };

    this.onShedulerMenuSelect = function (e) {
        var scheduler = $('#scheduler').getKendoScheduler();
        var target = $(e.target);
        var slot = scheduler.slotByElement(target);
        var resource = scheduler.resourcesBySlot(slot);
        
        var ev = {
            event_id: 0,
            task_id: 0,
            start: slot.startDate,
            end: slot.endDate,            
            date_plan: slot.startDate,
            exec_user_id: resource.exec_user_id,
            event_group_id: resource.event_group_id
        };        
        if ($(e.item).hasClass('menu-task-select')) {
            //
        } else if ($(e.item).hasClass('menu-task-add')) {
            ev.event_id = -1;
        } else if ($(e.item).hasClass('menu-event-add')) {
            ev.task_id = -1;
        };
        scheduler.addEvent(ev);
    };

    // private

    function removeTimeRows() {
        var r1 = $('table.k-scheduler-layout div.k-scheduler-times table tr:eq(1)');
        r1.addClass('hidden');
        var r2 = $('table.k-scheduler-layout div.k-scheduler-header table tr:eq(1)');
        r2.addClass('hidden');
    };

    function refreshScheduler() {
        var sch = $('#scheduler').getKendoScheduler();
        sch.dataSource.read();
    };

    function refreshOverdueList() {
        $('#crmTaskOverdueListGrid').getKendoGrid().dataSource.read();
    };

    function openSchedulerTask() {
        var win = $('#winTaskEditScheduler').data('kendoWindow');
        $('#winTaskEditScheduler').html("");
        win.refresh({
            url: 'Crm/TaskEditScheduler',
            //data: { task_id: curr_event.task_id }
            data: { task_id: curr_event.task_id, exec_user_id: curr_event.exec_user_id, date_plan: curr_event.date_plan }
        });

        win.center().open();
        $('#winTaskEditScheduler').closest(".k-window").css({
            top: $(document).scrollTop()
        });
    };

    function onSubtaskAddClick(e) {
        var text = $('#txtSubtaskAddScheduler').val();
        if (!text) return false;

        var curr_exec_user_id = 7;
        var combo = $("#assignedUserListScheduler").getKendoDropDownList();
        curr_exec_user_id = combo.dataItem()["user_id"];
        var curr_subtask_num = generateSubtaskNum();
        var new_subtask = {
            subtask_id: 0, task_id: $('#task_id').val(), subtask_text: text,
            state_id: 1, owner_user_id: currUser_CabUserId, crt_date: new Date(),
            repair_date: null, task_num: curr_subtask_num, owner_user_name: currUser_CabUserName,
            file_id: null,
            IsChecked: false, IsNew: true
        };

        var grid = $("#subtaskGridScheduler").data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_subtask);
        $('#txtSubtaskAddScheduler').val('');

        _this.onAttrChanged();

        return true;
    };

    function onTaskNoteAddClick(e) {
        var text = $('#txtTaskNoteAddScheduler').val();
        if (!text) return false;

        var new_note = { note_id: 0, note_text: text, crt_date: new Date(), owner_user_id: currUser_CabUserId, user_name: currUser_CabUserName, file_name: '' };
        var grid = $("#taskNoteGridScheduler").data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_note);
        $('#txtTaskNoteAddScheduler').val('');

        _this.onAttrChanged();

        return true;
    };

    function taskSave(winclose) {
        var win = $('#winTaskEditScheduler').data('kendoWindow');

        var project_name_combo = $("#projectListScheduler").getKendoDropDownList();
        var module_name_combo = $("#moduleListScheduler").getKendoDropDownList();
        var module_version_name_combo = $("#moduleVersionListScheduler").getKendoDropDownList();
        var repair_version_name_combo = $("#repairVersionListScheduler").getKendoDropDownList();
        var client_name_combo = $("#clientListScheduler").getKendoDropDownList();
        var priority_name_combo = $("#priorityListScheduler").getKendoDropDownList();
        var state_name_combo = $("#stateListScheduler").getKendoDropDownList();
        var assigned_user_name_combo = $("#assignedUserListScheduler").getKendoDropDownList();
        var exec_user_name_combo = $("#execUserListScheduler").getKendoDropDownList();
        var event_user_name_combo = $("#eventUserListScheduler").getKendoDropDownList();
        var group_name_combo = $('#groupListScheduler').getKendoDropDownList();
        var batch_name_combo = $("#batchListScheduler").getKendoDropDownList();

        var task_id_col = curr_task_id;
        var task_name_col = $('#task_nameScheduler').val();
        var task_text_col = $('#task_textScheduler').val();
        var task_num_col = 'AU-0000';
        var crt_date_col = new Date();
        var client_name_col = client_name_combo.dataItem()["client_name"];
        var client_id_col = client_name_combo.dataItem()["client_id"];
        var owner_user_name_col = currUser_CabUserName;
        var owner_user_id_col = currUser_CabUserId;

        var required_date_col = null;
        var repair_date_col = null;
        var repair_version_name_col = '[не указан]';
        var repair_version_id_col = 0;
        var project_name_col = '[не указан]';
        var project_id_col = 0;
        var module_name_col = '[не указан]';
        var module_id_col = 0;
        var module_version_name_col = '[не указан]';
        var module_version_id_col = 0;
        var module_part_name_col = '[не указан]';
        var module_part_id_col = 0;
        var priority_name_col = '[не указан]';
        var priority_id_col = 0;
        var state_name_col = '[не указан]';
        var state_id_col = 0;
        var assigned_user_name_col = '[не указан]';
        var assigned_user_id_col = 0;
        var exec_user_name_col = '[не указан]';
        var exec_user_id_col = 0;
        var is_moderated_col = false;
        var event_user_name_col = '[не указан]';
        var event_user_id_col = 1;
        var point_col = 0;
        var fin_cost_col = 0;
        var is_event_name_col = '';
        var is_event_col = false;
        var group_id_col = 0;
        var group_name_col = 'Неразобранные';
        var batch_name_col = '[не указан]';
        var batch_id_col = 0;
        var task_notes = null;
        var task_subtasks = null;

        point_col = $('#pointScheduler').val();
        fin_cost_col = $('#fin_costScheduler').val();
        task_notes = $("#taskNoteGridScheduler").data("kendoGrid").dataSource.view();
        task_subtasks = $("#subtaskGridScheduler").data("kendoGrid").dataSource.view();

        repair_date_col = $('#repair_dateScheduler').val();
        state_name_col = state_name_combo.dataItem()["state_name"];
        state_id_col = state_name_combo.dataItem()["state_id"];

        assigned_user_name_col = assigned_user_name_combo.dataItem()["user_name"];
        assigned_user_id_col = assigned_user_name_combo.dataItem()["user_id"];
        exec_user_name_col = exec_user_name_combo.dataItem()["user_name"];
        exec_user_id_col = exec_user_name_combo.dataItem()["user_id"];
        is_moderated_col = $('#IsModerated' + curr_win_task_name).is(':checked');
        if (event_user_name_combo) {
            event_user_name_col = event_user_name_combo.dataItem()["Text"];
            event_user_id_col = event_user_name_combo.dataItem()["Value"];
        };
        required_date_col = $('#required_dateScheduler').val();
        project_name_col = project_name_combo.dataItem()["project_name"];
        project_id_col = project_name_combo.dataItem()["project_id"];
        module_name_col = module_name_combo.dataItem()["module_name"];
        module_id_col = module_name_combo.dataItem()["module_id"];
        module_version_name_col = module_version_name_combo.dataItem()["module_version_name"];
        module_version_id_col = module_version_name_combo.dataItem()["module_version_id"];
        repair_version_name_col = repair_version_name_combo.dataItem()["module_version_name"];
        repair_version_id_col = repair_version_name_combo.dataItem()["module_version_id"];
        priority_name_col = priority_name_combo.dataItem()["priority_name"];
        priority_id_col = priority_name_combo.dataItem()["priority_id"];
        batch_name_col = batch_name_combo.dataItem()["batch_name"];
        batch_id_col = batch_name_combo.dataItem()["batch_id"];

        is_event_col = $('#is_eventScheduler').is(':checked');
        if (is_event_col) { is_event_col_name = 'К'; } else { is_event_col_name = ''; };

        group_id_col = group_name_combo.dataItem()["group_id"];
        group_name_col = group_name_combo.dataItem()["group_name"];

        var task = {
            task_id: task_id_col, crt_date: crt_date_col, task_num: task_num_col,
            task_name: task_name_col, task_text: task_text_col,
            state_name: state_name_col, state_id: state_id_col,
            client_name: client_name_col, client_id: client_id_col,
            exec_user_name: exec_user_name_col, exec_user_id: exec_user_id_col,
            is_event_for_exec: event_user_id_col,
            assigned_user_name: assigned_user_name_col, assigned_user_id: assigned_user_id_col,
            module_name: module_name_col, module_id: module_id_col,
            module_version_name: module_version_name_col, module_version_id: module_version_id_col,
            repair_version_name: repair_version_name_col, repair_version_id: repair_version_id_col,
            owner_user_name: owner_user_name_col, owner_user_id: owner_user_id_col,
            priority_name: priority_name_col, priority_id: priority_id_col,
            project_name: project_name_col, project_id: project_id_col,
            required_date: required_date_col,
            repair_date: repair_date_col,
            group_id: group_id_col, group_name: group_name_col,
            batch_name: batch_name_col, batch_id: batch_id_col,
            point: point_col, fin_cost: fin_cost_col, is_event: is_event_col, is_event_name: is_event_name_col,
            IsModerated: is_moderated_col
        };
        $.ajax({
            type: "POST",
            url: "Crm/TaskAdd",
            data: JSON.stringify({ task: task, notes: task_notes, subtasks: task_subtasks }),
            contentType: 'application/json; charset=windows-1251',
            success: function (task_res) {
                refreshScheduler();
                refreshOverdueList();
            },
            error: function (task_res) {
                alert('Ошибка при редактировании задачи');
            }
        });

        win.close();
    };

    function onBtnTaskCancelClick(e) {
        var win = $('#winTaskEditScheduler').data('kendoWindow');
        win.close();
    };

    function generateSubtaskNum() {
        AutoID = 1; // Get the latest sequential ID for this sector.
        var newAutoID = localStorage.subtask_num;
        if (!isNaN(newAutoID)) {
            AutoID = parseInt(newAutoID) + 1; // Save the new ID
        }
        localStorage.subtask_num = AutoID;
        return AutoID;
    };

    function scrollToToday(is_init) {
        var sch = $('#scheduler').getKendoScheduler();
        var isMonthView = sch.view().name.indexOf("Month") > -1;
        var contentDiv = $('#scheduler').find('div.k-scheduler-content');
        var cellToday = $('td.k-today', 'table.k-scheduler-table', '#scheduler')[0];
        if (cellToday) {
            var scrollTarget = $(cellToday).offset().left + contentDiv.scrollLeft() - contentDiv.position().left - 800;
            contentDiv.scrollLeft(scrollTarget);
            //sch.date(new Date());
            if ((is_init) && (isMonthView))
                scroll_inited = true;
        };
    };

    function onSubtaskExcelClick(e) {
        var grid = $('#subtaskGridScheduler').getKendoGrid();
        grid.saveAsExcel();
    };

    function userChange_forScheduler(no_refresh) {
        var usr = $('#userList').getKendoDropDownList().value();
        var usr_int = -1;
        if (usr)
            usr_int = parseInt(usr);
        var scheduler = $("#scheduler").data("kendoScheduler");
        var res = scheduler.resources[0];
        var ds = res.dataSource;

        if (usr_int >= 0) {
            ds.filter(
                    {
                        operator: "eq",
                        field: "user_id",
                        value: usr_int
                    });
        } else {
            ds.filter(
                    {
                        operator: "gt",
                        field: "user_id",
                        value: usr_int
                    });
        };

        // refreshScheduler();

        if (!no_refresh) {
            scheduler.view(scheduler.view().name);
        };
    };

    function userChange_forGrid() {
        $('#crmTaskOverdueListGrid').getKendoGrid().dataSource.read();
    };

    function onSubtaskPdfClick(e) {
        var grid = $('#subtaskGridScheduler').getKendoGrid();
        grid.saveAsPDF();
    };

    function onBtnTaskOkClick(e) {
        if (checkSaveFlag) {
            checkTaskSave();
        }
        else {
            taskSave(true);
        };
    };

    function checkTaskSave() {
        var ctrl = $('#spanTaskErrMessScheduler');

        $.ajax({
            type: "POST",
            url: '/Crm/TaskSaveBefore?task_id=' + curr_task_id + '&task_upd_num=' + $('#upd_num').val(),
            data: '',
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response != null) {
                    if (!response.is_ok) {
                        ctrl.html(response.mess);
                        ctrl.removeClass('hidden');
                        checkSaveFlag = false;
                        return false;
                    } else {
                        taskSave(true);
                        return true;
                    };
                } else {
                    taskSave(true);
                    return true;
                };
            },
            error: function (task_res) {
                taskSave(true);
                return true;
            }
        });
    };
}

var taskScheduler = null;

var curr_task_id = 0;
var curr_event = null;
var scroll_inited = false;
var exportFlag = false;
var overdueViewSelected = false;
var forceRefresh = false;
var checkSaveFlag = true;
var curr_win_task_name = 'Scheduler';
var firstDataBoundScheduler = true;
var dblclickFired = false;

$(function () {
    taskScheduler = new TaskScheduler();
    taskScheduler.init();
});
