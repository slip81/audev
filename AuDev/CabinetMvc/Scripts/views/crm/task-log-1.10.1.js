﻿function TaskLog() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#crmLogGrid', 0);
    }

    this.onCrmLogGridDataBound = function(e) {
        drawEmptyRow('#crmLogGrid');
    };
}

var taskLog = null;

$(function () {
    taskLog = new TaskLog();
    taskLog.init();
});
