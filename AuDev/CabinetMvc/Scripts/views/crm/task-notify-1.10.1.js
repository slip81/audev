﻿function TaskNotify() {
    _this = this;

    this.init = function () {
        //
    }

    this.onParamTransportGridDataBound = function (e) {
        drawEmptyRow("#paramTransportGrid");
    };

    this.onParamUsersIcqGridDataBound = function (e) {
        drawEmptyRow("#paramUsersIcqGrid");
    };

    this.onParamAttrIcqGridDataBound = function (e) {
        drawEmptyRow("#paramAttrIcqGrid");
    };

    this.onParamContentIcqGridDataBound = function (e) {
        drawEmptyRow("#paramContentIcqGrid");
    };

    this.onParamTransportGridRequestEnd = function (e) {
        if ((e.type == "create") || (e.type == "update")) {
            refreshGrid("#paramTransportGrid");
        };
    };

    this.onParamUsersIcqGridRequestEnd = function (e) {
        if ((e.type == "create") || (e.type == "update")) {
            refreshGrid("#paramUsersIcqGrid");
        };
    };

    this.onParamAttrIcqGridRequestEnd = function (e) {
        if ((e.type == "create") || (e.type == "update")) {
            refreshGrid("#paramAttrIcqGrid");
        };
    };

    this.onParamContentIcqGridRequestEnd = function (e) {
        if ((e.type == "create") || (e.type == "update")) {
            refreshGrid("#paramContentIcqGrid");
        };
    };

    $(document).on('click', '.chb-is-transport-disabled', function (e) {
        var grid = $(e.target).parents('.k-grid').getKendoGrid();
        var isChecked = $(this).is(':checked');
        var row = $(this).closest("tr");
        var dataItem = grid.dataItem(row);
        dataItem.Disabled = !isChecked;
        dataItem.dirty = true;
    });

    $(document).on('click', '.chb-is-executer', function (e) {
        var grid = $(e.target).parents('.k-grid').getKendoGrid();
        var isChecked = $(this).is(':checked');
        var row = $(this).closest("tr");
        var dataItem = grid.dataItem(row);
        dataItem.is_executer = isChecked;
        dataItem.dirty = true;
    });

    $(document).on('click', '.chb-is-owner', function (e) {
        var grid = $(e.target).parents('.k-grid').getKendoGrid();
        var isChecked = $(this).is(':checked');
        var row = $(this).closest("tr");
        var dataItem = grid.dataItem(row);
        dataItem.is_owner = isChecked;
        dataItem.dirty = true;
    });

    $(document).on('click', '.chb-is-transport-disabled-all', function (e) {
        var grid_el = $(e.target).parents('.k-grid');
        var grid = grid_el.getKendoGrid();
        var isChecked = $(this).is(':checked');
        $.each(grid_el.find('tr'), function (i1, row) {
            $.each($(row).find('.chb-is-transport-disabled'), function (i2, chb) {
                var dataItem = grid.dataItem(row);
                dataItem.Disabled = !isChecked;
                dataItem.dirty = true;
                $(chb).prop('checked', !isChecked);
            })
        });
    });

    $(document).on('click', '.chb-is-executer-all', function (e) {
        var grid_el = $(e.target).parents('.k-grid');
        var grid = grid_el.getKendoGrid();
        var isChecked = $(this).is(':checked');
        $.each(grid_el.find('tr'), function (i1, row) {
            $.each($(row).find('.chb-is-executer'), function (i2, chb) {
                var dataItem = grid.dataItem(row);
                dataItem.is_executer = isChecked;
                dataItem.dirty = true;                
                $(chb).prop('checked', isChecked);                
            })
        });
    });

    $(document).on('click', '.chb-is-owner-all', function (e) {
        var grid_el = $(e.target).parents('.k-grid');
        var grid = grid_el.getKendoGrid();
        var isChecked = $(this).is(':checked');
        $.each(grid_el.find('tr'), function (i1, row) {
            $.each($(row).find('.chb-is-owner'), function (i2, chb) {
                var dataItem = grid.dataItem(row);
                dataItem.is_owner = isChecked;
                dataItem.dirty = true;                
                $(chb).prop('checked', isChecked);
            })
        });
    });

    $(document).on('click', '.chb-is-active', function (e) {
        var grid = $(e.target).parents('.k-grid').getKendoGrid();
        var isChecked = $(this).is(':checked');
        var row = $(this).closest("tr");
        var dataItem = grid.dataItem(row);
        dataItem.is_active = isChecked;
        dataItem.dirty = true;
    });

    $(document).on('click', '.chb-is-active-all', function (e) {
        var grid_el = $(e.target).parents('.k-grid');
        var grid = grid_el.getKendoGrid();
        var isChecked = $(this).is(':checked');
        $.each(grid_el.find('tr'), function (i1, row) {
            $.each($(row).find('.chb-is-active'), function (i2, chb) {
                var dataItem = grid.dataItem(row);
                dataItem.is_active = isChecked;
                dataItem.dirty = true;
                $(chb).prop('checked', isChecked);
            })
        });
    });
}

var taskNotify = null;

$(function () {
    taskNotify = new TaskNotify();
    taskNotify.init();
});
