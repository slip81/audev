﻿function TaskList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#taskListGrid', 50);
        //
        $("#taskListGrid").delegate("tr.k-master-row", "dblclick", taskGridRowDblClick);
        //$("#taskListGrid").delegate("tr.k-master-row:not(:has(a))", "dblclick", taskGridRowDblClick);
        //
        curr_page_group_id = userFilter_curr_group_id;
        if (curr_page_group_id == null) curr_page_group_id = 0;

        $('.visible-group-0').addClass('hidden');
        if (curr_page_group_id == -1) {
            $('#menu-task-all').addClass('k-state-selected');
        } else if (curr_page_group_id == 0) {
            $('#menu-task-bucket').addClass('k-state-selected');
            $('.visible-group-0').removeClass('hidden');
        } else if (curr_page_group_id == 1) {
            $('#menu-task-prepared').addClass('k-state-selected');
        } else if (curr_page_group_id == 2) {
            $('#menu-task-fixed').addClass('k-state-selected');
        } else if (curr_page_group_id == 3) {
            $('#menu-task-approved').addClass('k-state-selected');
        } else if (curr_page_group_id == 5) {
            $('#menu-task-version').addClass('k-state-selected');
        } else if (curr_page_group_id == 4) {
            $('#menu-task-closed').addClass('k-state-selected');
        } else if (curr_page_group_id == 6) {
            $('#menu-task-project').addClass('k-state-selected');
        };

        confirmBackspaceNavigations();

        $('#taskGridPhrase').keypress(function (e) {
            if (e.keyCode == 13) {
                refreshGrid('#taskListGrid');
            };
        });

        $("#btnTaskGridConfigOk").click(function (e) {
            e.preventDefault();
            var win = $("#winTaskGridConfig").data("kendoWindow");
            var settingsData = getSettingsData();
            $.ajax({
                type: "POST",
                url: "Sys/CabGridColumnUserSettings",
                data: kendo.stringify(settingsData),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if (response != null && response.success) {
                        win.close();
                        window.location = url_TaskList;
                    } else {
                        win.close();
                        alert(response);
                    }
                },
                error: function (response) {
                    win.close();
                    alert('error');
                }
            });

            return false;
        });

        $("#btnTaskGridConfigCancel").click(function (e) {
            e.preventDefault();
            var win = $("#winTaskGridConfig").data("kendoWindow");
            win.close();
            return false;
        });

        $("#btnTaskGridConfig").click(function (e) {
            e.preventDefault();
            var win = $("#winTaskGridConfig").data("kendoWindow");
            win.close();
            return false;
        });

        $("#btnTaskListFilter").click(function (e) {
            e.preventDefault();
            $("#taskFilterPanel").data("kendoPanelBar").collapse($("li.k-state-active"));
            taskUserFilterSave();
        });

        $('#btnWinTaskBatchOk').click(function (e) {
            e.preventDefault();
            // !!!
            var curr_batch_id = $("#winTaskBatch #ddlTaskBatch").data("kendoDropDownList").value();
            $.ajax({
                type: "POST",
                url: "Crm/TaskOperationExecute",
                data: JSON.stringify({ task_id_list: id_list, operation_id: taskAction, batch_id: curr_batch_id }),
                contentType: 'application/json; charset=windows-1251',
                success: function (task_res) {
                    $("#winTaskBatch").data("kendoWindow").close();
                    checkedIds = {};
                    $("#taskGridCombo").data("kendoDropDownList").select(0);
                    refreshGrid('#taskListGrid');
                },
                error: function (task_res) {
                    $("#winTaskBatch").data("kendoWindow").close();
                    $("#taskGridCombo").data("kendoDropDownList").select(0);
                    alert('Ошибка при выполнении операции со списком задач');
                }
            });
        });
        $('#btnWinTaskBatchCancel').click(function (e) {
            e.preventDefault();
            $("#winTaskBatch").data("kendoWindow").close();
            return false;
        });
    };

    // task-edit-full.js

    this.init_popup = function () {                
        $('.attr-change').bind('input propertychange', _this.onAttrChanged);

        $('.subtask-addFull').click(function (e) {
            onSubtaskAddClick(e);
        });

        $('.task-note-addFull').click(function (e) {
            onTaskNoteAddClick(e);
        });

        $('.subtask-excelFull').click(function (e) {
            onSubtaskExcelClick(e);
        });

        $('.subtask-pdfFull').click(function (e) {
            onSubtaskPdfClick(e);
        });

        $('#btnTaskOkFull').click(function (e) {
            onBtnTaskOkClick(e);
        });
        $('#btnTaskSaveFull').click(function (e) {
            onBtnTaskSaveClick(e);
        });
        $('#btnTaskCancelFull').click(function (e) {
            onBtnTaskCancelClick(e);
        });

        $('.attr-change').click(function (e) {
            _this.onAttrChange();
        });

        $('div#div-subtaskFull ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
            $('#div-commentFull').toggleClass('hidden');
            $('#div-subtaskFull').toggleClass('col-sm-6');
            $('#div-subtaskFull').toggleClass('col-sm-12');
        });

        $('div#div-commentFull ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
            $('#div-subtaskFull').toggleClass('hidden');
            $('#div-commentFull').toggleClass('col-sm-6');
            $('#div-commentFull').toggleClass('col-sm-12');
            if ($('#div-subtaskFull').hasClass('hidden')) {
                $('#div-commentFull').css('margin-left', '10px');
            } else {
                $('#div-commentFull').css('margin-left', '-10px');
            };
        });
    };

    // public

    this.onTaskGridExcelExport = function(e) {
        this.options.excel.allPages = $('#taskGridExportAll').is(':checked');        
    }

    this.onTaskGridPdfExport = function(e) {
        this.options.pdf.allPages = $('#taskGridExportAll').is(':checked');
    }

    this.placeholder = function(element) {
        return element.clone().addClass("k-state-hover").css("opacity", 0.65);
    };

    this.onSortChange = function(e) {
        var grid = $("#taskGridColumnSettingsGrid").data("kendoGrid"),
                    skip = grid.dataSource.skip(),
                    oldIndex = e.oldIndex + skip,
                    newIndex = e.newIndex + skip,
                    data = grid.dataSource.data(),
                    dataItem = grid.dataSource.getByUid(e.item.data("uid"));

        grid.dataSource.remove(dataItem);
        grid.dataSource.insert(newIndex, dataItem);
    };

    this.getTaskListData = function() {
        var phrase_val = $('#taskGridPhrase').val();
        return { group_id: curr_page_group_id, phrase: phrase_val };
    }

    this.onTaskGridDataBound = function(e) {            
        var grid = $("#taskListGrid").data("kendoGrid");

        if (firstDataBound) {
            firstDataBound = false;
            var gridSortData = $.parseJSON(html_taskGridSortOptions);
            var gridFilterData = $.parseJSON(html_taskGridFilterOptions);
            if ((gridSortData) || (gridFilterData)) {
                grid.dataSource.filter(gridFilterData);
                grid.dataSource.sort(gridSortData);
            };
            var pageSizeDropDownList = grid.wrapper.children(".k-grid-pager").find("select").data("kendoDropDownList");
            pageSizeDropDownList.bind("change", function (e) {
                saveUserConfig(null, null);
            });
            if (curr_page_group_id != -1) {
                grid.hideColumn(1);
            } else {
                grid.showColumn(1);
            };
            setFilterHeader();
        };

        drawEmptyRow("#taskListGrid");

        var row = null;
        var cnt = 0;

        var data = grid.dataSource.data();
        for (var x = 0; x < data.length; x++) {
            var dataItem = data[x];
            var tr = $("#taskListGrid").find("[data-uid='" + dataItem.uid + "']");

            // state_name
            var col_ind = settings_state_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.state_fore_color) {

                    var is_row_colored = taskGridColorColumn_state_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.state_fore_color);
                        if ((dataItem.state_fore_color == 'black') || (dataItem.state_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.state_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.state_fore_color);
                }
            }
            // project_name
            col_ind = settings_project_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.project_fore_color) {

                    var is_row_colored = taskGridColorColumn_project_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.project_fore_color);
                        if ((dataItem.project_fore_color == 'black') || (dataItem.project_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.project_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.project_fore_color);
                }
            }
            // priority_name
            col_ind = settings_priority_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.priority_fore_color) {

                    var is_row_colored = taskGridColorColumn_priority_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.priority_fore_color);
                        if ((dataItem.priority_fore_color == 'black') || (dataItem.priority_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.priority_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.priority_fore_color);
                }
            }
            // exec_user_name
            col_ind = settings_exec_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.exec_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_exec_user_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.exec_user_fore_color);
                        if ((dataItem.exec_user_fore_color == 'black') || (dataItem.exec_user_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.exec_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.exec_user_fore_color);
                }
            }
            // owner_user_name
            col_ind = settings_owner_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.owner_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_owner_user_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.owner_user_fore_color);
                        if ((dataItem.owner_user_fore_color == 'black') || (dataItem.owner_user_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.owner_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.owner_user_fore_color);
                }
            }
            // assigned_user_name
            col_ind = settings_assigned_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.assigned_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_assigned_user_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.assigned_user_fore_color);
                        if ((dataItem.assigned_user_fore_color == 'black') || (dataItem.assigned_user_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.assigned_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.assigned_user_fore_color);
                }
            }
            //
            cnt = cnt + 1;
        }
        //
        fillTaskGridCombo(curr_page_group_id);
    };

    this.onCabGridCheckboxHeaderClick = function(e) {
        var state = $(e).is(':checked');
        var grid = $("#taskListGrid").data("kendoGrid");
        var view = grid.dataSource.view();

        for (var i = 0; i < view.length; i++) {
            var dataRow = view[i];
            var elementRow = grid.table.find(".cab-grid-checkbox")[i];
            if (elementRow != null) {
                var checked = elementRow.checked,
                    row = $(elementRow).closest("tr"),
                    dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));

                checkedIds[dataItem.task_id] = state;
                if (state) {
                    elementRow.checked = true;
                    row.addClass("k-info-colored");
                } else {
                    elementRow.checked = false;                    
                    row.removeClass("k-info-colored");
                }
            }
        };
    };

    this.onCabGridCheckboxClick = function(e) {        
        var checked = $(e).is(':checked'),
            row = $(e).closest("tr"),
            grid = $("#taskListGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);

        checkedIds[dataItem.task_id] = checked;
        if (checked) {
            row.addClass("k-info-colored");
        } else {            
            row.removeClass("k-info-colored");
        }
    };

    this.onTaskGridComboChange = function (e) {
        var ddl = $("#taskGridCombo").data("kendoDropDownList");
        if (!ddl)
            return false;

        taskAction = null;
        var dataItem = ddl.dataItem();
        if (dataItem) {

            taskAction = dataItem['operation_id'];
            if (taskAction == null) {
                ddl.select(0);
                return false;
            }

            id_list = '';
            $.each(checkedIds, function (key, value) {
                if (value) {
                    id_list = id_list + key + ',';
                }
            });
            if (!id_list) {
                ddl.select(0);
                alert('Нет отмеченных задач !');
                return false;
            };

            if (taskAction != 15) {
                $.ajax({
                    type: "POST",
                    url: "Crm/TaskOperationExecute",
                    data: JSON.stringify({ task_id_list: id_list, operation_id: taskAction }),
                    contentType: 'application/json; charset=windows-1251',
                    success: function (task_res) {
                        checkedIds = {};
                        ddl.select(0);
                        refreshGrid('#taskListGrid');
                    },
                    error: function (task_res) {
                        ddl.select(0);
                        alert('Ошибка при выполнении операции со списком задач');
                    }
                });
            } else {
                $("#winTaskBatch").data("kendoWindow").center().open();
            }
        }
    };

    this.onTaskListMenuSelect = function(e) {
        e.preventDefault();
        var grid = $("#taskListGrid").data("kendoGrid");
        var menu_item = e.item;
        if (menu_item) {
            var menu_item_id = $(menu_item).attr('id');
            if (menu_item_id == 'menu-task-all') {
                curr_page_group_id = -1;
            } else if (menu_item_id == 'menu-task-bucket') {
                curr_page_group_id = 0;
            } else if (menu_item_id == 'menu-task-prepared') {
                curr_page_group_id = 1;
            } else if (menu_item_id == 'menu-task-fixed') {
                curr_page_group_id = 2;
            } else if (menu_item_id == 'menu-task-approved') {
                curr_page_group_id = 3;
            } else if (menu_item_id == 'menu-task-closed') {
                curr_page_group_id = 4;
            } else if (menu_item_id == 'menu-task-version') {
                curr_page_group_id = 5;
            } else if (menu_item_id == 'menu-task-project') {
                curr_page_group_id = 6;
            } else {
                return false;
            };
            //
            taskUserFilterSave();
            //
            if (curr_page_group_id != -1) {
                grid.hideColumn(1);
            } else {
                grid.showColumn(1);
            };
            checkedIds = {};
            toggleVisibility();
            grid.dataSource.read();
            fillTaskGridCombo(curr_page_group_id);
            this.element.find(".k-state-selected").removeClass('k-state-selected');
            $(e.item).addClass('k-state-selected');
            return true;
            //
        } else {
            return false;
        };
    };

    this.onTaskGridColumnSettingsGridClick = function(e) {
        var checked = $(e).is(':checked');
        var grid = $('#taskGridColumnSettingsGrid').getKendoGrid();
        var dataItem = grid.dataItem($(e).closest('tr'));
        dataItem.is_visible = checked;
    };

    this.onAdditionalData = function(e) {
        var ctrl = $(e);
        var task_num_part = ctrl[0].filter.filters[0].value;
        return {
            text: task_num_part
        };
    };

    this.addTaskRel = function(parent_task_id) {
        var ddl = $('#taskRelTypeCombo_' + parent_task_id).getKendoDropDownList();
        var rel_type_id = ddl.value();
        var ac = $('#taskRelAutoComplete_' + parent_task_id).getKendoAutoComplete();
        var child_task_num = ac.value();

        if (rel_type_id <= 0)
            return false;
        if (!child_task_num)
            return false;

        $.ajax({
            type: "POST",
            url: "Crm/TaskRelAdd",
            data: JSON.stringify({ parent_task_id: parent_task_id, child_task_num: child_task_num, rel_type_id: rel_type_id }),
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response.success) {
                    ac.value('');
                    refreshGrid('#grid_' + parent_task_id);                    
                } else {
                    alert(response.responseText);
                };
            },
            error: function (response) {
                alert('Ошибка при попытке создания связи');
            }
        });
    };

    this.delTaskRel = function(parent_task_id) {
        var child_grid = $('#grid_' + parent_task_id).data('kendoGrid');
        var child_item = child_grid.dataItem(child_grid.select());

        if (!child_item)
            return false;
        if (child_item.child_task_id <= 0)
            return false;

        var rel_type_id = child_item.rel_type_id;
        if (rel_type_id <= 0)
            return false;

        $.ajax({
            type: "POST",
            url: "Crm/TaskRelDel",
            data: JSON.stringify({ parent_task_id: parent_task_id, child_task_id: child_item.child_task_id, rel_type_id: rel_type_id }),
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response.success) {
                    refreshGrid('#grid_' + parent_task_id);
                } else {
                    alert(response.responseText);
                };
            },
            error: function (response) {
                alert('Ошибка при попытке удаления связи');
            }
        });
    };

    this.onTaskRelGridDataBound = function(parent_task_id) {
        $('#grid_' + parent_task_id).delegate("tbody>tr", "dblclick", taskRelGridRowDblClick);
    };

    this.onAttrChanged = function() {
        $('#btnTaskOkFull').removeClass('btn-default');
        $('#btnTaskOkFull').addClass('btn-success');
        $('#btnTaskOkFull').removeAttr('disabled');
        //
        $('#btnTaskSaveFull').removeClass('btn-default');
        $('#btnTaskSaveFull').addClass('btn-info');
        $('#btnTaskSaveFull').removeAttr('disabled');

    };

    this.onSubtaskGridFullDataBound = function(e) {
        $(".stateListCell").each(function () {            
            eval($(this).children("script").last().html());
        });

        $(".fileListCell").each(function () {
            eval($(this).children("script").last().html());
        });
        
        var grid = $('#subtaskGrid' + curr_win_task_name).getKendoGrid();
        var ds = grid.dataSource;
        var task_num_max = ds.aggregates().task_num.max;
        localStorage.subtask_num = task_num_max;
    };

    this.onSubtaskGridFullEdit = function(e) {        
        e.model.IsNew = true;
        //
        _this.onAttrChanged();
    };

    this.subtaskStateListChanged = function(e) {        
        var item = $('#subtaskGrid' + curr_win_task_name).data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        var ddl_text = this.text();
        item.set('state_id', ddl_val);
        item.set('state_id_ReadOnly', ddl_val);
        item.set('state_name', ddl_text);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.subtaskFileListChanged = function (e) {
        var item = $('#subtaskGrid' + curr_win_task_name).data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        var ddl_text = this.text();
        item.set('file_id', ddl_val);
        item.set('file_id_ReadOnly', ddl_val);
        item.set('file_name', ddl_text);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.onSubtaskGridFullRemove = function(e) {
        setTimeout(function(){$('#subtaskGrid' + curr_win_task_name).data('kendoGrid').dataSource.sync()})
    };

    this.onSubtaskGridFullExcelExport = function(e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsExcel();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onSubtaskGridFullPdfExport = function(e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsPDF();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onAddTaskBtnClick = function(btn) {
        return addTask(0);
    };

    this.onAddTaskNewBtnClick = function (btn) {
        window.location = '/TaskEdit?task_id=0';
    };

    this.onGridPhraseSearchBtnClick = function(btn) {
        refreshGrid('#taskListGrid');        
        return false;
    }

    this.onGridClearFiltersBtnClick = function(btn) {
        var sender = $(btn);
        var ds = $("#taskListGrid").getKendoGrid().dataSource;
        ds.filter([]);
        cleanFilterHeader();
        return false;
    };

    this.onGridSaveSettingsBtnClick = function(btn) {
        var sender = $(btn);
        saveUserConfig(sender, null);
        return false;
    };

    this.onGridConfigBtnClick = function(btn) {
        var win = $("#winTaskGridConfig").data("kendoWindow");
        win.center().open();
        return false;
    };

    this.onWinTaskEditFullRefresh = function (e) {
        var winCrmTask_taskTextHeight = localStorage.winCrmTask_taskTextHeight;
        if (winCrmTask_taskTextHeight) {
            $('#task_textFull').css("height", winCrmTask_taskTextHeight);
        };
        var winCrmTask_taskTextWidth = localStorage.winCrmTask_taskTextWidth;
        if (winCrmTask_taskTextWidth) {
            $('#task_textFull').css("width", winCrmTask_taskTextWidth);
        };
    };

    this.onUplFileFullSuccess = function(e) {        
        if (e.operation == 'upload') {

            var grid = $("#taskFilesGrid" + curr_win_task_name).data("kendoGrid");
            grid.dataSource.read();

            _this.onAttrChanged();
        };

        return true;
    };

    this.onUplFileFullError = function(e) {
        var err = e.XMLHttpRequest.responseText;
        if (err.length <= 200) {
            alert(err);
        };
    };

    this.onTaskNumCellClick = function (task_id) {        
        setTimeout(function () {
            if (!dblclickFired) {
                saveUserConfig(null, function () { window.location = '/TaskEdit?task_id=' + task_id; });
            }
        }, 300);       
    };

    this.onTaskEventCellClick = function (task_id) {        
        setTimeout(function () {
            if (!dblclickFired) {
                window.location = '/TaskScheduler?task_id=' + task_id;
            }
        }, 300);
    };

    this.onTaskStorageCellClick = function (folder_id) {
        setTimeout(function () {
            if (!dblclickFired) {
                window.location = '/StorageTree?folder_id=' + folder_id;
            }
        }, 300);
    };

    // private

    function addTask(parent_task_id) {
        selected_data_item = null;
        selected_row = null;
        curr_win_task_name = 'Full';
        curr_task_id = 0;
        var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');
        $('#winTaskEdit' + curr_win_task_name).html("");

        win.title('Новая задача');

        var winOptions = {};
        var winCrmTask_height = localStorage.winCrmTask_height;
        if (winCrmTask_height) {
            winOptions.height = winCrmTask_height;
        };
        var winCrmTask_width = localStorage.winCrmTask_width;
        if (winCrmTask_width) {
            winOptions.width = winCrmTask_width;
        };
        var winCrmTask_positionTop = localStorage.winCrmTask_positionTop;
        var winCrmTask_positionLeft = localStorage.winCrmTask_positionLeft;
        if ((winCrmTask_positionTop) && (winCrmTask_positionLeft)) {
            winOptions.position = {};
            winOptions.position.top = winCrmTask_positionTop;
            winOptions.position.left = winCrmTask_positionLeft;
        };
        if (winOptions) {
            win.setOptions(winOptions);
        };

        win.refresh({
            url: 'Crm/TaskEdit' + curr_win_task_name,
            data: { task_id: null }
        });

        if ((!winCrmTask_positionTop) || (!winCrmTask_positionLeft)) {
            win.center().open();
        } else {
            win.open();
        };

        $('#winTaskEdit' + curr_win_task_name).closest(".k-window").css({
            top: $(document).scrollTop()
        });

        return false;
    };

    function saveUserConfig(sender, callback) {
        if (sender) {
            sender.addClass('hidden');
        };
        var grid = $("#taskListGrid").getKendoGrid();
        var ds = grid.dataSource;

        $.ajax({
            type: "POST",
            url: "Sys/CabGridUserSettings",
            data: JSON.stringify({ grid_id: 1, sort_options: kendo.stringify(ds.sort()), filter_options: kendo.stringify(ds.filter()), page_size: ds.pageSize(), columns_options: kendo.stringify(grid.columns) }),
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (sender) {
                    sender.removeClass('hidden');
                };
                if (response != null) {
                    if (response.error) {
                        alert(response.mess);
                    } else {
                        if (callback)
                            callback();
                    };
                } else {
                    alert('Ошибка при попытке сохранения настроек');
                };
            },
            error: function (response) {
                if (sender) {
                    sender.removeClass('hidden');
                };
                alert('Ошибка при сохранении настроек');
            }
        });
    };

    function getSettingsData() {
        var grid = $("#taskGridColumnSettingsGrid").data("kendoGrid");
        var data = grid.dataSource.view();
        var ddl = $("#taskGridColumnColorList").getKendoDropDownList();
        var dataItem = ddl.dataItem();
        var color_column_id = null;
        if (dataItem) { color_column_id = dataItem.column_id; }
        return { settings: data, color_column_id: color_column_id };
    };

    function toggleVisibility() {
        if (curr_page_group_id == 0) {
            $('.hidden-group-0').addClass('hidden');
            //
            $('.visible-group-0').removeClass('hidden');
        } else if (curr_page_group_id == 1) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').removeClass('hidden');
        } else if (curr_page_group_id == 2) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').removeClass('hidden');
        } else if (curr_page_group_id == 3) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').removeClass('hidden');
            $('.hidden-group-3').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').addClass('hidden');
            $('.visible-group-3').removeClass('hidden');
        } else if (curr_page_group_id == 4) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').removeClass('hidden');
            $('.hidden-group-3').removeClass('hidden');
            $('.hidden-group-4').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').addClass('hidden');
            $('.visible-group-3').addClass('hidden');
            $('.visible-group-4').removeClass('hidden');
        } else if (curr_page_group_id == 5) {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').removeClass('hidden');
            $('.hidden-group-3').removeClass('hidden');
            $('.hidden-group-4').removeClass('hidden');
            $('.hidden-group-5').addClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').addClass('hidden');
            $('.visible-group-3').addClass('hidden');
            $('.visible-group-4').addClass('hidden');
            $('.visible-group-5').removeClass('hidden');
        } else {
            $('.hidden-group-0').removeClass('hidden');
            $('.hidden-group-1').removeClass('hidden');
            $('.hidden-group-2').removeClass('hidden');
            $('.hidden-group-3').removeClass('hidden');
            $('.hidden-group-4').removeClass('hidden');
            $('.hidden-group-5').removeClass('hidden');
            //
            $('.visible-group-0').addClass('hidden');
            $('.visible-group-1').addClass('hidden');
            $('.visible-group-2').addClass('hidden');
            $('.visible-group-3').addClass('hidden');
            $('.visible-group-4').addClass('hidden');
            $('.visible-group-5').addClass('hidden');
        };
    };

    function taskGridRowDblClick(e) {
        dblclickFired = true;
        var grid = $("#taskListGrid").data("kendoGrid");
        curr_task_id = 0;
        selected_row = $(this);
        selected_data_item = grid.dataItem($(this));
        curr_task_group_id = 0;
        curr_win_task_name = 'Simple';
        if (selected_data_item != null) {
            curr_task_id = selected_data_item.task_id;
            curr_task_group_id = selected_data_item.group_id;
            if (selected_data_item.group_id == 0) {
                curr_win_task_name = 'Full';
            } else {
                curr_win_task_name = 'Full';
            };
            if (curr_task_id > 0) {
                var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');
                $('#winTaskEdit' + curr_win_task_name).html("");

                win.title('Задача ' + selected_data_item.task_num);

                var winOptions = {};
                var winCrmTask_height = localStorage.winCrmTask_height;
                if (winCrmTask_height) {
                    winOptions.height = winCrmTask_height;
                };
                var winCrmTask_width = localStorage.winCrmTask_width;
                if (winCrmTask_width) {
                    winOptions.width = winCrmTask_width;
                };
                var winCrmTask_positionTop = localStorage.winCrmTask_positionTop;
                var winCrmTask_positionLeft = localStorage.winCrmTask_positionLeft;
                if ((winCrmTask_positionTop) && (winCrmTask_positionLeft)) {
                    winOptions.position = {};
                    winOptions.position.top = winCrmTask_positionTop;
                    winOptions.position.left = winCrmTask_positionLeft;
                };
                if (winOptions) {
                    win.setOptions(winOptions);
                };

                win.refresh({
                    url: 'Crm/TaskEdit' + curr_win_task_name,
                    data: { task_id: curr_task_id }
                });

                if ((!winCrmTask_positionTop) || (!winCrmTask_positionLeft)) {
                    win.center().open();
                } else {
                    win.open();
                };

                $('#winTaskEdit' + curr_win_task_name).closest(".k-window").css({
                    top: $(document).scrollTop()
                });
            };
        };
        setTimeout(function () { dblclickFired = false; }, 800);
    };

    function taskUserFilterSave() {
        var filter_project_values = $("#filterProjectList").getKendoMultiSelect().value();
        var filter_client_values = $("#filterClientList").getKendoMultiSelect().value();
        var filter_priority_values = $("#filterPriorityList").getKendoMultiSelect().value();
        var filter_state_values = $("#filterStateList").getKendoMultiSelect().value();
        var filter_exec_user_values = $("#filterExecUserList").getKendoMultiSelect().value();
        var filter_assigned_user_values = $("#filterAssignedUserList").getKendoMultiSelect().value();
        var filter_required_date1_value = $("#filterRequiredDate1").getKendoDatePicker().value();
        var filter_required_date2_value = $("#filterRequiredDate2").getKendoDatePicker().value();
        var filter_repair_date1_value = $("#filterRepairDate1").getKendoDatePicker().value();
        var filter_repair_date2_value = $("#filterRepairDate2").getKendoDatePicker().value();
        var filter_fin_cost_value = $("#filterFinCost").getKendoNumericTextBox().value();
        var filter_is_moderated_value = $("#filterIsModerated").is(':checked');
        var filter_batch_values = $("#filterBatchList").getKendoMultiSelect().value();

        var filter_project_ids = '';
        var filter_client_ids = '';
        var filter_priority_ids = '';
        var filter_state_ids = '';
        var filter_exec_user_ids = '';
        var filter_assigned_user_ids = '';
        var filter_batch_ids = '';


        var grid = $("#taskListGrid").data("kendoGrid");
        var gridFilter = null;
        if (grid.dataSource.filter() != null) {
            gridFilter = { logic: "and", filters: [] };
        } else {
            gridFilter = { logic: "and", filters: [] };
        };

        var currFilter = null;
        var ok1 = false;
        var ok2 = false;

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_project_values) {
            $.each(filter_project_values, function (i, v) {
                currFilter.filters.push({ field: "project_id", operator: "eq", value: v });
                filter_project_ids = filter_project_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_client_values) {
            $.each(filter_client_values, function (i, v) {
                currFilter.filters.push({ field: "client_id", operator: "eq", value: v });
                filter_client_ids = filter_client_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_priority_values) {
            $.each(filter_priority_values, function (i, v) {
                currFilter.filters.push({ field: "priority_id", operator: "eq", value: v });
                filter_priority_ids = filter_priority_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_state_values) {
            $.each(filter_state_values, function (i, v) {
                currFilter.filters.push({ field: "state_id", operator: "eq", value: v });
                filter_state_ids = filter_state_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_assigned_user_values) {
            $.each(filter_assigned_user_values, function (i, v) {
                currFilter.filters.push({ field: "assigned_user_id", operator: "eq", value: v });
                filter_assigned_user_ids = filter_assigned_user_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_exec_user_values) {
            $.each(filter_exec_user_values, function (i, v) {
                currFilter.filters.push({ field: "exec_user_id", operator: "eq", value: v });
                filter_exec_user_ids = filter_exec_user_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_required_date1_value) {
            currFilter.filters.push({ field: "required_date", operator: "gte", value: filter_required_date1_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_required_date2_value) {
            currFilter.filters.push({ field: "required_date", operator: "lte", value: filter_required_date2_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_repair_date1_value) {
            currFilter.filters.push({ field: "repair_date", operator: "gte", value: filter_repair_date1_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_repair_date2_value) {
            currFilter.filters.push({ field: "repair_date", operator: "lte", value: filter_repair_date2_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_fin_cost_value) {
            currFilter.filters.push({ field: "fin_cost", operator: "gte", value: filter_fin_cost_value });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_is_moderated_value) {
            currFilter.filters.push({ field: "moderator_user_id", operator: "gt", value: 0 });
            ok1 = true;
            gridFilter.filters.push(currFilter);
            ok2 = true;
        };

        ok1 = false;
        currFilter = { logic: "or", filters: [] };
        if (filter_batch_values) {
            $.each(filter_batch_values, function (i, v) {
                currFilter.filters.push({ field: "batch_id", operator: "eq", value: v });
                filter_batch_ids = filter_batch_ids + v + ',';
                ok1 = true;
            });
            if (ok1) {
                gridFilter.filters.push(currFilter);
                ok2 = true;
            };
        };
        
        grid.dataSource.filter(gridFilter);
        saveUserConfig(null, null);

        $.ajax({
            type: "POST",
            url: "Crm/TaskUserFilterSave",
            data: JSON.stringify({
                grid_id: 1
                , raw_filter: JSON.stringify(gridFilter)
                , project_filter: filter_project_ids
                , client_filter: filter_client_ids
                , priority_filter: filter_priority_ids
                , state_filter: filter_state_ids
                , exec_user_filter: filter_exec_user_ids
                , assigned_user_filter: filter_assigned_user_ids
                , required_date1_filter: filter_required_date1_value
                , required_date2_filter: filter_required_date2_value
                , repair_date1_filter: filter_repair_date1_value
                , repair_date2_filter: filter_repair_date2_value
                , fin_cost_filter: filter_fin_cost_value
                , curr_group_id: curr_page_group_id
                , is_moderated_filter: filter_is_moderated_value
                , batch_filter: filter_batch_ids
            }),
            contentType: 'application/json; charset=windows-1251',
            success: function (task_res) {
                setFilterHeader();
            },
            error: function (task_res) {
                $('#filter-panel-header').text('Фильтры: ошибка');
            }
        });

        return false;
    };

    function fillTaskGridCombo(for_group_id) {
        var ddl = $("#taskGridCombo").data("kendoDropDownList");
        if (!ddl)
            return false;

        ddl.dataSource.filter({
            field: "group_id",
            operator: "eq",
            value: parseInt(for_group_id)
        });
        ddl.select(0);
    };

    function onBtnTaskSaveClick(e) {
        taskSave(false);
    };

    function taskSave(winclose) {
        var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');

        var project_name_combo = $("#projectList" + curr_win_task_name).getKendoDropDownList();
        var module_name_combo = $("#moduleList" + curr_win_task_name).getKendoDropDownList();
        var module_version_name_combo = $("#moduleVersionList" + curr_win_task_name).getKendoDropDownList();
        var repair_version_name_combo = $("#repairVersionList" + curr_win_task_name).getKendoDropDownList();
        var client_name_combo = $("#clientList" + curr_win_task_name).getKendoDropDownList();
        var priority_name_combo = $("#priorityList" + curr_win_task_name).getKendoDropDownList();
        var state_name_combo = $("#stateList" + curr_win_task_name).getKendoDropDownList();
        var assigned_user_name_combo = $("#assignedUserList" + curr_win_task_name).getKendoDropDownList();
        var exec_user_name_combo = $("#execUserList" + curr_win_task_name).getKendoDropDownList();
        var event_user_name_combo = $("#eventUserList" + curr_win_task_name).getKendoDropDownList();
        var group_name_combo = $('#groupList' + curr_win_task_name).getKendoDropDownList();
        var batch_name_combo = $("#batchList" + curr_win_task_name).getKendoDropDownList();

        var task_id_col = curr_task_id;
        var task_name_col = $('#task_name' + curr_win_task_name).val();
        var task_text_col = $('#task_text' + curr_win_task_name).val();
        var task_num_col = 'AU-0000';
        var crt_date_col = new Date();
        var client_name_col = client_name_combo.dataItem()["client_name"];
        var client_id_col = client_name_combo.dataItem()["client_id"];
        var owner_user_name_col = currUser_CabUserName;
        var owner_user_id_col = currUser_CabUserId;

        var required_date_col = null;
        var repair_date_col = null;
        var repair_version_name_col = '[не указан]';
        var repair_version_id_col = 0;
        var project_name_col = '[не указан]';
        var project_id_col = 0;
        var module_name_col = '[не указан]';
        var module_id_col = 0;
        var module_version_name_col = '[не указан]';
        var module_version_id_col = 0;
        var module_part_name_col = '[не указан]';
        var module_part_id_col = 0;
        var priority_name_col = '[не указан]';
        var priority_id_col = 0;
        var state_name_col = '[не указан]';
        var state_id_col = 0;
        var assigned_user_name_col = '[не указан]';
        var assigned_user_id_col = 0;
        var exec_user_name_col = '[не указан]';
        var exec_user_id_col = 0;
        var is_moderated_col = false;
        var event_user_name_col = '[не указан]';
        var event_user_id_col = 1;                
        var fin_cost_col = 0;
        var is_event_name_col = '';
        var is_event_col = false;
        var group_id_col = 0;
        var group_name_col = 'Неразобранные';
        var batch_name_col = '[не указан]';
        var batch_id_col = 0;
        var is_control_col = false;
        var event_date_beg = null;
        var event_date_end = null;
        var cost_plan_col = 0;
        var cost_fact_col = 0;
        var task_notes = null;
        var task_subtasks = null;
        
        
        fin_cost_col = $('#fin_cost' + curr_win_task_name).val();
        task_notes = $("#taskNoteGrid" + curr_win_task_name).data("kendoGrid").dataSource.view();
        task_subtasks = $("#subtaskGrid" + curr_win_task_name).data("kendoGrid").dataSource.view();

        repair_date_col = $('#repair_date' + curr_win_task_name).val();
        state_name_col = state_name_combo.dataItem()["state_name"];
        state_id_col = state_name_combo.dataItem()["state_id"];

        assigned_user_name_col = assigned_user_name_combo.dataItem()["user_name"];
        assigned_user_id_col = assigned_user_name_combo.dataItem()["user_id"];
        exec_user_name_col = exec_user_name_combo.dataItem()["user_name"];
        exec_user_id_col = exec_user_name_combo.dataItem()["user_id"];
        is_moderated_col = $('#IsModerated' + curr_win_task_name).is(':checked');
        if (event_user_name_combo) {
            event_user_name_col = event_user_name_combo.dataItem()["Text"];
            event_user_id_col = event_user_name_combo.dataItem()["Value"];
        };
        required_date_col = $('#required_date' + curr_win_task_name).val();
        project_name_col = project_name_combo.dataItem()["project_name"];
        project_id_col = project_name_combo.dataItem()["project_id"];
        module_name_col = module_name_combo.dataItem()["module_name"];
        module_id_col = module_name_combo.dataItem()["module_id"];
        module_version_name_col = module_version_name_combo.dataItem()["module_version_name"];
        module_version_id_col = module_version_name_combo.dataItem()["module_version_id"];
        repair_version_name_col = repair_version_name_combo.dataItem()["module_version_name"];
        repair_version_id_col = repair_version_name_combo.dataItem()["module_version_id"];
        priority_name_col = priority_name_combo.dataItem()["priority_name"];
        priority_id_col = priority_name_combo.dataItem()["priority_id"];
        batch_name_col = batch_name_combo.dataItem()["batch_name"];
        batch_id_col = batch_name_combo.dataItem()["batch_id"];
        event_date_beg_col = $('#event_date_beg' + curr_win_task_name).val();
        event_date_end_col = $('#event_date_end' + curr_win_task_name).val();
        cost_plan_col = $('#cost_plan' + curr_win_task_name).val();
        cost_fact_col = $('#cost_fact' + curr_win_task_name).val();

        is_event_col = $('#is_event' + curr_win_task_name).is(':checked');
        if (is_event_col) { is_event_col_name = 'К'; } else { is_event_col_name = ''; };

        is_control_col = $('#is_control' + curr_win_task_name) ? $('#is_control' + curr_win_task_name).is(':checked') : false;

        group_id_col = group_name_combo.dataItem()["group_id"];
        group_name_col = group_name_combo.dataItem()["group_name"];

        var grid = $('#taskListGrid').data('kendoGrid');
        var dataSource = grid.dataSource;

        var task = {
            task_id: task_id_col, crt_date: crt_date_col, task_num: task_num_col,
            task_name: task_name_col, task_text: task_text_col,
            state_name: state_name_col, state_id: state_id_col,
            client_name: client_name_col, client_id: client_id_col,
            exec_user_name: exec_user_name_col, exec_user_id: exec_user_id_col,
            is_event_for_exec: event_user_id_col,
            assigned_user_name: assigned_user_name_col, assigned_user_id: assigned_user_id_col,
            module_name: module_name_col, module_id: module_id_col,
            module_version_name: module_version_name_col, module_version_id: module_version_id_col,
            repair_version_name: repair_version_name_col, repair_version_id: repair_version_id_col,
            owner_user_name: owner_user_name_col, owner_user_id: owner_user_id_col,
            priority_name: priority_name_col, priority_id: priority_id_col,
            project_name: project_name_col, project_id: project_id_col,
            required_date: required_date_col,
            repair_date: repair_date_col,
            group_id: group_id_col, group_name: group_name_col,
            batch_name: batch_name_col, batch_id: batch_id_col,
            fin_cost: fin_cost_col, is_event: is_event_col, is_event_name: is_event_name_col,
            event_date_beg: event_date_beg_col, event_date_end: event_date_end_col,
            cost_plan: cost_plan_col, cost_fact: cost_fact_col,
            is_control: is_control_col,
            IsModerated: is_moderated_col
        };
        $.ajax({
            type: "POST",
            url: "Crm/TaskAdd",
            data: JSON.stringify({ task: task, notes: task_notes, subtasks: task_subtasks }),
            contentType: 'application/json; charset=windows-1251',
            success: function (task_res) {
                if (task_res != null) {
                    if (task_id_col > 0) {
                        if (selected_data_item != null) {
                            if ((curr_page_group_id != -1) && (curr_task_group_id != task_res.group_id)) {
                                dataSource.remove(selected_data_item);
                            } else {
                                //
                                if (!winclose) {
                                    var grid = $("#subtaskGrid" + curr_win_task_name).data("kendoGrid");
                                    var view = grid.dataSource.view();
                                    for (var i = 0; i < view.length; i++) {
                                        var dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));
                                        if (dataItem) {
                                            dataItem.IsNew = false;
                                        };
                                    };
                                } else {
                                    refreshGrid('#taskListGrid');
                                };
                            }
                        };
                    } else {
                        refreshGrid('#taskListGrid');
                    }
                };
            },
            error: function (task_res) {
                alert('Ошибка при редактировании задачи');
            }
        });

        localStorage.winCrmTask_height = win.options.height;
        localStorage.winCrmTask_width = win.options.width;
        localStorage.winCrmTask_positionTop = win.options.position.top;
        localStorage.winCrmTask_positionLeft = win.options.position.left;
        localStorage.winCrmTask_taskTextHeight = $('#task_textFull').height();
        localStorage.winCrmTask_taskTextWidth = $('#task_textFull').width();

        if (winclose) {
            win.close();
        } else {
            $('#btnTaskOkFull').removeClass('btn-success');
            $('#btnTaskOkFull').addClass('btn-default');
            $('#btnTaskOkFull').attr('disabled', true);
            //
            $('#btnTaskSaveFull').removeClass('btn-info');
            $('#btnTaskSaveFull').addClass('btn-default');
            $('#btnTaskSaveFull').attr('disabled', true);
        }
    };

    function onBtnTaskCancelClick(e) {
        var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');
        win.close();
    };

    function onSubtaskAddClick(e) {
        var text = $('#txtSubtaskAdd' + curr_win_task_name).val();
        if (!text) return false;

        var curr_exec_user_id = 7;
        var combo = $("#assignedUserList" + curr_win_task_name).getKendoDropDownList();
        curr_exec_user_id = combo.dataItem()["user_id"];
        var curr_task_num = generateSubtaskNum();
        var new_subtask = {
            subtask_id: 0, task_id: $('#task_id').val(), subtask_text: text,
            state_id: 1, owner_user_id: currUser_CabUserId, crt_date: new Date(), upd_date: null,
            repair_date: null, task_num: curr_task_num, owner_user_name: currUser_CabUserName,
            file_id: null,
            IsChecked: false, IsNew: true
        };

        var grid = $("#subtaskGrid" + curr_win_task_name).data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_subtask);
        $('#txtSubtaskAdd' + curr_win_task_name).val('');

        _this.onAttrChanged();

        return true;
    };

    function onTaskNoteAddClick(e) {
        var text = $('#txtTaskNoteAdd' + curr_win_task_name).val();
        if (!text) return false;

        var new_note = { note_id: 0, note_text: text, crt_date: new Date(), owner_user_id: currUser_CabUserId, user_name: currUser_CabUserName, file_name: '' };
        var grid = $("#taskNoteGrid" + curr_win_task_name).data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_note);
        $('#txtTaskNoteAdd' + curr_win_task_name).val('');

        _this.onAttrChanged();

        return true;
    };

    function taskRelGridRowDblClick(e) {
        var grid = $(this).closest('.k-grid').data('kendoGrid');
        curr_task_id = 0;
        selected_row = $(this);
        selected_data_item = grid.dataItem($(this));
        curr_task_group_id = 0;
        curr_win_task_name = 'Simple';
        if (selected_data_item != null) {
            curr_task_id = selected_data_item.child_task_id;
            curr_task_group_id = selected_data_item.group_id;
            if (selected_data_item.group_id == 0) {
                curr_win_task_name = 'Full';
            } else {
                curr_win_task_name = 'Full';
            };
            if (curr_task_id > 0) {
                var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');
                $('#winTaskEdit' + curr_win_task_name).html("");
                win.refresh({
                    url: 'Crm/TaskEdit' + curr_win_task_name,
                    data: { task_id: curr_task_id }
                });
                win.open();
            };
        };
    };

    function setFilterHeader() {
        var filter_project_name_list = $("#filterProjectList").getKendoMultiSelect().dataItems();
        var filter_client_name_list = $("#filterClientList").getKendoMultiSelect().dataItems();
        var filter_priority_name_list = $("#filterPriorityList").getKendoMultiSelect().dataItems();
        var filter_state_name_list = $("#filterStateList").getKendoMultiSelect().dataItems();
        var filter_exec_user_name_list = $("#filterExecUserList").getKendoMultiSelect().dataItems();
        var filter_assigned_user_name_list = $("#filterAssignedUserList").getKendoMultiSelect().dataItems();
        var filter_is_moderated_value = $("#filterIsModerated").is(':checked');
        var filter_batch_name_list = $("#filterBatchList").getKendoMultiSelect().dataItems();

        var project_names = "";
        if (filter_project_name_list.length != 0) {
            project_names = " [";
            $.each(filter_project_name_list, function (i, v) {
                project_names = project_names + v.project_name + '; ';
            });
            project_names = project_names + "]";
        };

        var client_names = "";
        if (filter_client_name_list.length != 0) {
            client_names = " [";
            $.each(filter_client_name_list, function (i, v) {
                client_names = client_names + v.client_name + '; ';
            });
            client_names = client_names + "]";
        };

        var priority_names = "";
        if (filter_priority_name_list.length != 0) {
            priority_names = " [";
            $.each(filter_priority_name_list, function (i, v) {
                priority_names = priority_names + v.priority_name + '; ';
            });
            priority_names = priority_names + "]";
        };

        var state_names = "";
        if (filter_state_name_list.length != 0) {
            state_names = " [";
            $.each(filter_state_name_list, function (i, v) {
                state_names = state_names + v.state_name + '; ';
            });
            state_names = state_names + "]";
        };

        var exec_user_names = "";
        if (filter_exec_user_name_list.length != 0) {
            exec_user_names = " [";
            $.each(filter_exec_user_name_list, function (i, v) {
                exec_user_names = exec_user_names + v.user_name + '; ';
            });
            exec_user_names = exec_user_names + "]";
        };

        var assigned_user_names = "";
        if (filter_assigned_user_name_list.length != 0) {
            assigned_user_names = " [";
            $.each(filter_assigned_user_name_list, function (i, v) {
                assigned_user_names = assigned_user_names + v.user_name + '; ';
            });
            assigned_user_names = assigned_user_names + "]";
        };

        var is_moderated_value = "";
        if (filter_is_moderated_value) {
            is_moderated_value = " [Модерация]";
        }

        var batch_names = "";
        if (filter_batch_name_list.length != 0) {
            batch_names = " [";
            $.each(filter_batch_name_list, function (i, v) {
                batch_names = batch_names + v.batch_name + '; ';
            });
            batch_names = batch_names + "]";
        };

        $('#filter-panel-header').text("Фильтры" + project_names + client_names + priority_names + state_names + exec_user_names + assigned_user_names + is_moderated_value + batch_names);
    };

    function generateSubtaskNum() {
        AutoID = 1; // Get the latest sequential ID for this sector.
        var newAutoID = localStorage.subtask_num;
        if (!isNaN(newAutoID)) {
            AutoID = parseInt(newAutoID) + 1; // Save the new ID
        }
        localStorage.subtask_num = AutoID;
        return AutoID;
    };

    function onSubtaskExcelClick(e) {
        var grid = $('#subtaskGrid' + curr_win_task_name).getKendoGrid();
        grid.saveAsExcel();
    };

    function onSubtaskPdfClick(e) {
        var grid = $('#subtaskGrid' + curr_win_task_name).getKendoGrid();
        grid.saveAsPDF();
    };

    function onBtnTaskOkClick(e) {
        // !!!
        var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');
        win.close();
        /*
        if (checkSaveFlag) {
            checkTaskSave();
        }
        else {
            taskSave(true);
        };
        */
    };

    function checkTaskSave() {
        var ctrl = $('#spanTaskErrMessFull');

        $.ajax({
            type: "POST",
            url: '/Crm/TaskSaveBefore?task_id=' + curr_task_id + '&task_upd_num=' + $('#upd_num').val(),
            data: '',
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response != null) {
                    if (!response.is_ok) {
                        ctrl.html(response.mess);
                        ctrl.removeClass('hidden');
                        checkSaveFlag = false;
                        return false;
                    } else {
                        taskSave(true);
                        return true;
                    };
                } else {
                    taskSave(true);
                    return true;
                };
            },
            error: function (task_res) {
                taskSave(true);
                return true;
            }
        });
    };

    function cleanFilterHeader() {
        $('#filter-panel-header').text('Фильтры');
    };
}

var taskList = null;

var firstDataBound = true;
var firstSubtaskDataBound = true;
var noHint = $.noop;
var checkedIds = {};
var curr_page_group_id = 0;
var curr_task_group_id = 0;
var curr_task_id = 0;
var selected_row = null;
var selected_data_item = null;
var curr_win_task_name = 'Simple';
var exportFlag = false;
var checkSaveFlag = true;
var dblclickFired = false;
var taskAction = null;
var id_list = '';

$(function () {
    taskList = new TaskList();
    taskList.init();
});
