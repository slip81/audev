﻿function TaskEdit() {
    _this = this;

    this.init = function () {

        $('.floating-right-top').css('margin-right', ($(window).width() - $('#cab-content').width())/2);

        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();
            
            $("#client_id").val($("#clientList").getKendoDropDownList().dataItem()["client_id"]);
            $("#group_id").val($("#groupList").getKendoDropDownList().dataItem()["group_id"]);
            $("#priority_id").val($("#priorityList").getKendoDropDownList().dataItem()["priority_id"]);
            $("#project_id").val($("#projectList").getKendoDropDownList().dataItem()["project_id"]);
            $("#module_id").val($("#moduleList").getKendoDropDownList().dataItem()["module_id"]);
            $("#module_version_id").val($("#moduleVersionList").getKendoDropDownList().dataItem()["module_version_id"]);
            $("#assigned_user_id").val($("#assignedUserList").getKendoDropDownList().dataItem()["user_id"]);
            $("#exec_user_id").val($("#execUserList").getKendoDropDownList().dataItem()["user_id"]);
            $("#state_id").val($("#stateList").getKendoDropDownList().dataItem()["state_id"]);
            $("#repair_version_id").val($("#repairVersionList").getKendoDropDownList().dataItem()["module_version_id"]);
            $("#is_event_for_exec").val(true);
            $("#batch_id").val($("#batchList").getKendoDropDownList().dataItem()["batch_id"]);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').attr('disabled', true);
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $('#storage-folder-tree-span>label').click(function (e) {
            setTimeout(function () {
                var checked = $('#is_in_storage').is(':checked');                
                if (!checked) {
                    $('#storage-folder-tree-div').addClass('hidden');
                    $('#storage_folder_id').val(null);
                    $('#storage-folder-selected-span').text('');
                    $('#storage-folder-selected-div').addClass('hidden')
                } else {
                    $('#storage-folder-tree-div').removeClass('hidden');
                }
            }, 200);
        });
        
        $('#storageFolderMenu>li.k-item>span.k-link').append('<span class="k-icon k-i-custom"></span>');
    };

    this.onSubtaskGridDataBound = function (e) {
        drawEmptyRow('#subtaskGrid');
    };

    this.onSubtaskGridRequestEnd = function (e) {
        if ((e.type == "create") || (e.type == "update")) {
            $("#subtaskGrid").getKendoGrid().dataSource.read();
        }
    };

    this.onTaskNoteGridDataBound = function (e) {
        drawEmptyRow('#taskNoteGrid');
        this.table.find(".k-grid-edit").hide();
    };

    this.onTaskNoteGridRequestEnd = function (e) {
        if ((e.type == "create") || (e.type == "update")) {
            $("#taskNoteGrid").getKendoGrid().dataSource.read();
        }
    };

    this.onTaskFilesGridDataBound = function (e) {
        drawEmptyRow('#taskFilesGrid');
    };

    this.onUplFileSuccess = function (e) {
        if (e.operation == 'upload') {
            $("#taskFilesGrid").getKendoGrid().dataSource.read();
        };
        return true;
    };

    this.onUplFileError = function (e) {
        var err = e.XMLHttpRequest.responseText;
        if (err.length <= 200) {
            alert(err);
        };
    };

    this.onTaskLogGridDataBound = function (e) {
        drawEmptyRow('#taskLogGrid');
    };

    this.onStorageFolderMenuSelect = function (e) {
        var selectedText = $(e.item).children(".k-link").text();
        if (selectedText == 'Сменить') {
            if ($('#storage-folder-tree-div').hasClass('hidden')) {
                $('#storage-folder-tree-div').removeClass('hidden');
            }
        } else if (selectedText == 'Очистить') {
            $('#storage_folder_id').val(null);
            $('#is_in_storage').prop('checked', false);
            //$('#storage-folder-selected-span').html('');
            $('#storage-folder-selected-span').text('');
            if (!$('#storage-folder-tree-div').hasClass('hidden')) {
                $('#storage-folder-tree-div').addClass('hidden');
            };
            if (!$('#storage-folder-selected-div').hasClass('hidden')) {
                $('#storage-folder-selected-div').addClass('hidden')
            }; 
        }
    };
}

var taskEdit = null;

var exportFlag = false;
var curr_win_task_name = "";

$(function () {
    taskEdit = new TaskEdit();
    taskEdit.init();
});
