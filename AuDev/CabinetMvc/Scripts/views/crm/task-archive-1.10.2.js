﻿function TaskArchive() {
    _this = this;

    this.init = function () {
        $('#btnRemoveFromArchive').click(function (e) {
            var id_list = '';
            $.each(checkedIds, function (key, value) {
                if (value) {
                    id_list = id_list + key + ',';
                }
            });
            if (!id_list) {
                alert('Нет отмеченных задач !');
                return false;
            };

            $.ajax({
                type: "POST",
                url: "Crm/TaskOperationExecute",
                data: JSON.stringify({ task_id_list: id_list, operation_id: 14 }),
                contentType: 'application/json; charset=windows-1251',
                success: function (task_res) {
                    checkedIds = {};
                    refreshGrid('#taskArcListGrid');
                },
                error: function (task_res) {   
                    alert('Ошибка при выполнении операции со списком задач');
                }
            });
        });
    }    

    this.onTaskArcListGridDataBound = function(e) {
        drawEmptyRow("#taskArcListGrid");
    };

    this.onCabGridCheckboxClick = function(e) {
        var checked = $(e).is(':checked'),
            row = $(e).closest("tr"),
            grid = $("#taskArcListGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);

        checkedIds[dataItem.task_id] = checked;
        if (checked) {            
            row.addClass("k-info-colored");
        } else {            
            row.removeClass("k-info-colored");
        }
    };

    this.onCabGridCheckboxHeaderClick = function(e) {
        var state = $(e).is(':checked');
        var grid = $("#taskArcListGrid").data("kendoGrid");
        var view = grid.dataSource.view();

        for (var i = 0; i < view.length; i++) {
            var dataRow = view[i];
            var elementRow = grid.table.find(".cab-grid-checkbox")[i];
            if (elementRow != null) {
                var checked = elementRow.checked,
                    row = $(elementRow).closest("tr"),
                    dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));

                checkedIds[dataItem.task_id] = state;
                if (state) {
                    elementRow.checked = true;                    
                    row.addClass("k-info-colored");
                } else {
                    elementRow.checked = false;                    
                    row.removeClass("k-info-colored");
                }
            }
        };
    };

}

var taskArchive = null;

var checkedIds = {};

$(function () {
    taskArchive = new TaskArchive();
    taskArchive.init();
});
