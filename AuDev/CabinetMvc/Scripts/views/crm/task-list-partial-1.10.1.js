﻿function TaskListPartial() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#taskListGrid', 0);
        //
        //_this.loadTaskGridConfig();
    };

    this.getTaskListData = function (e) {
        return { group_id: -1, phrase: '' };
    };

    this.onCabGridCheckboxClick = function (e) {
        var checked = $(e).is(':checked'),
            row = $(e).closest("tr"),
            grid = $("#taskListGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);

        checkedIds[dataItem.task_id] = checked;
        if (checked) {
            row.addClass("k-info-colored");
        } else {
            row.removeClass("k-info-colored");
        }
    };

    this.onCabGridCheckboxHeaderClick = function (e) {
        var state = $(e).is(':checked');
        var grid = $("#taskListGrid").data("kendoGrid");
        var view = grid.dataSource.view();

        for (var i = 0; i < view.length; i++) {
            var dataRow = view[i];
            var elementRow = grid.table.find(".cab-grid-checkbox")[i];
            if (elementRow != null) {
                var checked = elementRow.checked,
                    row = $(elementRow).closest("tr"),
                    dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));

                checkedIds[dataItem.task_id] = state;
                if (state) {
                    elementRow.checked = true;
                    row.addClass("k-info-colored");
                } else {
                    elementRow.checked = false;
                    row.removeClass("k-info-colored");
                }
            }
        };
    };

    this.onTaskStorageCellClick = function (folder_id) {
        setTimeout(function () {
            if (!dblclickFired) {
                window.location = '/StorageTree?folder_id=' + folder_id;
            }
        }, 300);
    };

    this.onTaskGridDataBound = function (e) {
        drawEmptyRow("#taskListGrid");
        //        
        var grid = $("#taskListGrid").data("kendoGrid");
        var data = grid.dataSource.data();
        for (var x = 0; x < data.length; x++) {
            var dataItem = data[x];
            var tr = $("#taskListGrid").find("[data-uid='" + dataItem.uid + "']");

            // state_name
            if (dataItem.state_fore_color) {
                tr.css('background-color', dataItem.state_fore_color);
                if ((dataItem.state_fore_color == 'black') || (dataItem.state_fore_color == 'rgba(0, 0, 0, 1)')) {
                    tr.css('color', 'white');
                } else {
                    tr.css('color', 'black');
                };
                if (dataItem.state_fore_color != 'rgba(255, 255, 255, 1)')
                    tr.css('background-color', dataItem.state_fore_color);
            }
        }
        //
        if (firstDataBound_taskListGrid) {
            firstDataBound_taskListGrid = false;
        } else {
            _this.saveTaskGridConfig(e);
        };
    };

    this.saveTaskGridConfig = function (e) {
        var grid = $("#taskListGrid").data("kendoGrid");
        var dataSource = grid.dataSource;

        var state = {
            columns: grid.columns,
            //page: dataSource.page(),
            pageSize: dataSource.pageSize(),
            sort: dataSource.sort(),
            filter: dataSource.filter()
            //group: dataSource.group()
        };

        $.ajax({
            url: "TaskListGridConfigSave",
            type: "POST",
            //contentType: 'application/json; charset=windows-1251',
            //data: JSON.stringify({ data: state }),
            data: {
                data: JSON.stringify(state)
            }
        });
    };

    this.loadTaskGridConfig = function (e) {

        // !!!
        // alert('loadTaskGridConfig !');

        var grid = $("#taskListGrid").data("kendoGrid");
        var dataSource = grid.dataSource;

        $.ajax({
            url: "TaskListGridConfigLoad",
            success: function (state) {
                state = JSON.parse(state);
                if (state) {
                    var options = grid.options;

                    options.columns = state.columns;
                    //options.dataSource.page = state.page;
                    options.dataSource.pageSize = state.pageSize;
                    options.dataSource.sort = state.sort;
                    options.dataSource.filter = state.filter;
                    //options.dataSource.group = state.group;

                    grid.destroy();
                    $("#taskListGrid").empty().kendoGrid(options);
                    setGridMaxHeight('#taskListGrid', 0);
                };
                //needUpdate_ms = true;
            },
            error: function () {
                //needUpdate_ms = true;
            },
            complete: function () {
                msTaskListColumnFill();
                $("#taskListGrid").data("kendoGrid").dataSource.read();
            }
        });
    };

    this.onMsTaskListColumnSelect = function (e) {
        var selectedText = e.item.text();
        var grid = $("#taskListGrid").data("kendoGrid");
        var columns = grid.columns;
        if (columns.length > 0) {
            for (var i = 0; i < columns.length; i++) {
                var col = columns[i];
                if (col.title) {
                    if (col.title == selectedText) {
                        if (col.hidden) {
                            grid.showColumn(col);
                        } else {
                            grid.hideColumn(col);
                        }
                        break;
                    };
                };
            };
        };
    };

    // private

    function msTaskListColumnFill() {
        var dataSource = new kendo.data.DataSource();
        var selectedItems = [];
        var columns = $("#taskListGrid").data("kendoGrid").columns;
        if (columns.length > 0) {
            for (var i = 0; i < columns.length; i++) {
                var col = columns[i];
                if (col.title) {
                    dataSource.add({ column_name: col.title, column_index: i });
                    if (!col.hidden)
                        selectedItems.push({ column_name: col.title, column_index: i });
                }
            }
        }        
        var ms = $("#msTaskListColumn").data("kendoMultiSelect");
        ms.setDataSource(dataSource);
        ms.value(selectedItems);
    };
}

var taskListPartial = null;
var checkedIds = {};
var needUpdate_ms = false;
var firstDataBound_taskListGrid = true;

$(function () {
    taskListPartial = new TaskListPartial();
    taskListPartial.init();
});
