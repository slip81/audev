﻿function TaskSprav() {
    _this = this;

    this.init = function () {
        $(".add-client-btn").click(function (e) {
            e.preventDefault();
            $('#clientGrid').getKendoGrid().create();
        });
    }

    this.onProjectGridChange = function(e) {
        var parentGrid = $('#projectGrid').getKendoGrid();
        var selectedItem = parentGrid.dataItem(parentGrid.select());
        if (selectedItem) {
            var grid = $('#moduleGrid').getKendoGrid();
            var gridFilter = { logic: "and", filters: [] };
            gridFilter.filters.push({ field: "project_id", operator: "eq", value: selectedItem.project_id });
            grid.dataSource.filter(gridFilter);
            //
            $('#projectUserGrid').getKendoGrid().dataSource.read();
        }
    };

    this.getProjectId = function(e) {
        var curr_project_id = -1;
        var grid = $('#projectGrid').getKendoGrid();
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem) {
            curr_project_id = selectedItem.project_id;
        }
        return { parent_project_id: curr_project_id };
    };

    this.onModuleGridChange = function(e) {
        var parentGrid = $('#moduleGrid').getKendoGrid();
        var selectedItem = parentGrid.dataItem(parentGrid.select());
        if (selectedItem) {
            var grid = $('#moduleVersionGrid').getKendoGrid();
            var gridFilter = { logic: "and", filters: [] };
            gridFilter.filters.push({ field: "module_id", operator: "eq", value: selectedItem.module_id });
            grid.dataSource.filter(gridFilter);
        }
    };

    this.getModuleId = function(e) {
        var curr_module_id = -1;
        var grid = $('#moduleGrid').getKendoGrid();
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem) {
            curr_module_id = selectedItem.module_id;
        }
        return { parent_module_id: curr_module_id };
    };

    this.onGridEdit = function(e) {
        if (e.container[0].cellIndex == 2) {
            $(".k-selected-color").css("width", "100%");
            $("#ForeColor").data("kendoColorPicker").open();
        }
    };

    this.onRequestEnd = function(gridName) {
        return function (e) {
            var grid = $("#" + gridName).data("kendoGrid");
            if ((e.type == "create") || (e.type == "delete")) {
                grid.dataSource.read();
            }
        }
    };

    this.onForeColorSelect = function(e) {
        var grid = $(this.element).closest('.k-grid').getKendoGrid();
        var dataItem = grid.dataItem($(this.element).closest('tr'));
        var picker = $("#ForeColor").getKendoColorPicker();
        var color = picker.value();
        dataItem.fore_color = color;
    };
}

var taskSprav = null;

$(function () {
    taskSprav = new TaskSprav();
    taskSprav.init();
});
