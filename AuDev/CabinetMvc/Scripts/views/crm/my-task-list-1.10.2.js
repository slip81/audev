﻿function MyTaskList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#taskListGrid', 0);
        //
        $("#taskListGrid").delegate("tbody>tr", "dblclick", taskGridRowDblClick);
        //
        confirmBackspaceNavigations();

        $(".refresh-list-btn").click(function (e) {
            e.preventDefault();
            refreshGrid('#taskListGrid');
            return false;
        });

        $("#btnTaskGridConfigOk").click(function (e) {
            e.preventDefault();
            var win = $("#winTaskGridConfig").data("kendoWindow");
            var settingsData = getSettingsData();
            $.ajax({
                type: "POST",
                url: "Sys/CabGridColumnUserSettings",
                data: kendo.stringify(settingsData),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if (response != null && response.success) {
                        win.close();
                        window.location = url_TaskList;
                    } else {
                        win.close();
                        alert(response);
                    }
                },
                error: function (response) {
                    win.close();
                    alert('error');
                }
            });

            return false;
        });

        $("#btnTaskGridConfigCancel").click(function (e) {
            e.preventDefault();
            var win = $("#winTaskGridConfig").data("kendoWindow");
            win.close();
            return false;
        });

        $("#btnTaskGridConfig").click(function (e) {
            e.preventDefault();
            var win = $("#winTaskGridConfig").data("kendoWindow");
            win.close();
            return false;
        });
    };

    // task-edit-personal.js

    this.init_popup = function () {        

        $('.attr-change').bind('input propertychange', _this.onAttrChanged);

        $('.subtask-addPersonal').click(function (e) {
            onSubtaskAddClick(e);
        });

        $('.task-note-addPersonal').click(function (e) {
            onTaskNoteAddClick(e);
        });

        $('.subtask-excelPersonal').click(function (e) {
            onSubtaskExcelClick(e);
        });

        $('.subtask-pdfPersonal').click(function (e) {
            onSubtaskPdfClick(e);
        });

        $('#btnTaskOkPersonal').click(function (e) {
            onBtnTaskOkClick(e);
        });
        $('#btnTaskSavePersonal').click(function (e) {
            onBtnTaskSaveClick(e);
        });
        $('#btnTaskCancelPersonal').click(function (e) {
            onBtnTaskCancelClick(e);
        });

        $('.attr-change').click(function (e) {
            _this.onAttrChange();
        });

        $('div#div-subtaskPersonal ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
            $('#div-commentPersonal').toggleClass('hidden');
            $('#div-subtaskPersonal').toggleClass('col-sm-6');
            $('#div-subtaskPersonal').toggleClass('col-sm-12');
        });

        $('div#div-commentPersonal ul.k-tabstrip-items>li.k-item>a.k-link>span.k-icon').click(function (e) {
            $('#div-subtaskPersonal').toggleClass('hidden');
            $('#div-commentPersonal').toggleClass('col-sm-6');
            $('#div-commentPersonal').toggleClass('col-sm-12');
            if ($('#div-subtaskPersonal').hasClass('hidden')) {
                $('#div-commentPersonal').css('margin-left', '10px');
            } else {
                $('#div-commentPersonal').css('margin-left', '-10px');
            };
        });
    }

    // public

    this.placeholder = function(element) {
        return element.clone().addClass("k-state-hover").css("opacity", 0.65);
    };

    this.onSortChange = function(e) {
        var grid = $("#taskGridColumnSettingsGrid").data("kendoGrid"),
                    skip = grid.dataSource.skip(),
                    oldIndex = e.oldIndex + skip,
                    newIndex = e.newIndex + skip,
                    data = grid.dataSource.data(),
                    dataItem = grid.dataSource.getByUid(e.item.data("uid"));

        grid.dataSource.remove(dataItem);
        grid.dataSource.insert(newIndex, dataItem);
    };

    this.getMyTaskListData = function (e) {
        return { role_id: curr_role_id, phrase: '222' };
    };

    this.onTaskGridDataBound = function (e) {        
        var grid = $("#taskListGrid").data("kendoGrid");

        if (firstDataBound) {
            firstDataBound = false;
            var gridSortData = $.parseJSON(html_taskGridSortOptions);
            if ((gridSortData) || (gridFilterData)) {
                grid.dataSource.sort(gridSortData);
            };            
        }

        drawEmptyRow("#taskListGrid");
        //        
        var row = null;
        var cnt = 0;

        var data = grid.dataSource.data();
        for (var x = 0; x < data.length; x++) {
            var dataItem = data[x];
            var tr = $("#taskListGrid").find("[data-uid='" + dataItem.uid + "']");

            // state_name
            var col_ind = settings_state_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.state_fore_color) {

                    var is_row_colored = taskGridColorColumn_state_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.state_fore_color);
                        if ((dataItem.state_fore_color == 'black') || (dataItem.state_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.state_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.state_fore_color);
                }
            }
            // project_name
            col_ind = settings_project_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.project_fore_color) {

                    var is_row_colored = taskGridColorColumn_project_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.project_fore_color);
                        if ((dataItem.project_fore_color == 'black') || (dataItem.project_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.project_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.project_fore_color);
                }
            }
            // priority_name
            col_ind = settings_priority_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.priority_fore_color) {

                    var is_row_colored = taskGridColorColumn_priority_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.priority_fore_color);
                        if ((dataItem.priority_fore_color == 'black') || (dataItem.priority_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.priority_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.priority_fore_color);
                }
            }
            // exec_user_name
            col_ind = settings_exec_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.exec_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_exec_user_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.exec_user_fore_color);
                        if ((dataItem.exec_user_fore_color == 'black') || (dataItem.exec_user_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.exec_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.exec_user_fore_color);
                }
            }
            // owner_user_name
            col_ind = settings_owner_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.owner_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_owner_user_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.owner_user_fore_color);
                        if ((dataItem.owner_user_fore_color == 'black') || (dataItem.owner_user_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.owner_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.owner_user_fore_color);
                }
            }
            // assigned_user_name
            col_ind = settings_assigned_user_name;
            if (col_ind) {
                var cell = $("td:nth-child(" + col_ind + ")", tr);
                if (dataItem.assigned_user_fore_color) {

                    var is_row_colored = taskGridColorColumn_assigned_user_name;
                    if (is_row_colored) {
                        tr.css('background-color', dataItem.assigned_user_fore_color);
                        if ((dataItem.assigned_user_fore_color == 'black') || (dataItem.assigned_user_fore_color == 'rgba(0, 0, 0, 1)')) {
                            tr.css('color', 'white');
                        } else {
                            tr.css('color', 'black');
                        };
                    }

                    if (dataItem.assigned_user_fore_color != 'rgba(255, 255, 255, 1)')
                        cell.css('background-color', dataItem.assigned_user_fore_color);
                }
            }
            //
            cnt = cnt + 1;
        }
    };

    this.onTaskListMenuSelect = function(e) {
        e.preventDefault();
        var grid = $("#taskListGrid").data("kendoGrid");
        var menu_item = e.item;
        if (menu_item) {
            var menu_item_id = $(menu_item).attr('id');
            if (menu_item_id == 'menu-task-all') {
                curr_role_id = 0;
            } else if (menu_item_id == 'menu-task-exec') {
                curr_role_id = 1;
            } else if (menu_item_id == 'menu-task-assigned') {
                curr_role_id = 2;
            } else if (menu_item_id == 'menu-task-follow') {
                curr_role_id = 3;
            } else if (menu_item_id == 'menu-task-partner') {
                curr_role_id = 4;
            } else {
                return false;
            };
            //
            grid.dataSource.read();
            this.element.find(".k-state-selected").removeClass('k-state-selected');
            $(e.item).addClass('k-state-selected');
            return true;
        } else {
            return false;
        };
    };

    this.onTaskGridColumnSettingsGridClick = function(e) {
        var checked = $(e).is(':checked');
        var grid = $('#taskGridColumnSettingsGrid').getKendoGrid();
        var dataItem = grid.dataItem($(e).closest('tr'));
        dataItem.is_visible = checked;
    };

    this.onAttrChanged = function() {
        $('#btnTaskOkPersonal').removeClass('btn-default');
        $('#btnTaskOkPersonal').addClass('btn-success');
        $('#btnTaskOkPersonal').removeAttr('disabled');
        //
        $('#btnTaskSavePersonal').removeClass('btn-default');
        $('#btnTaskSavePersonal').addClass('btn-info');
        $('#btnTaskSavePersonal').removeAttr('disabled');
    };

    this.onSubtaskGridPersonalDataBound = function(e) {
        $(".stateListCell").each(function () {            
            eval($(this).children("script").last().html());
        });
        $(".fileListCell").each(function () {
            eval($(this).children("script").last().html());
        });
        var grid = $('#subtaskGrid' + curr_win_task_name).getKendoGrid();
        var ds = grid.dataSource;
        var task_num_max = ds.aggregates().task_num.max;
        localStorage.subtask_num = task_num_max;
    };

    this.onSubtaskGridPersonalEdit = function(e) {
        e.model.IsNew = true;
        _this.onAttrChanged();
    };

    this.subtaskStateListChanged = function(e) {
        var item = $('#subtaskGrid' + curr_win_task_name).data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        item.set('state_id', ddl_val);
        item.set('state_id_ReadOnly', ddl_val);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.subtaskFileListChanged = function (e) {
        var item = $('#subtaskGrid' + curr_win_task_name).data().kendoGrid.dataItem($(this.element).closest('tr'));
        var ddl_val = this.value();
        var ddl_text = this.text();
        item.set('file_id', ddl_val);
        item.set('file_id_ReadOnly', ddl_val);
        item.set('file_name', ddl_text);
        item.set('IsNew', true);
        //
        _this.onAttrChanged();
    };

    this.onSubtaskGridPersonalRemove = function(e) {        
        setTimeout(function () { $('#subtaskGrid' + curr_win_task_name).data('kendoGrid').dataSource.sync(); });
    };

    this.onSubtaskGridPersonalExcelExport = function(e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsExcel();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onSubtaskGridPersonalPdfExport = function(e) {
        if (!exportFlag) {
            e.sender.showColumn(1);
            e.sender.hideColumn(2);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
                e.sender.saveAsPDF();
            });
        } else {
            e.sender.hideColumn(1);
            e.sender.showColumn(2);
            exportFlag = false;
        }
    };

    this.onWinTaskEditPersonalOpen = function(e) {        
        $.ajax({
            type: "POST",
            url: url_IsAuthorized,
            data: '',
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response != null) {
                    if (!response.is_auth) {
                        window.location = url_Login;
                    };
                } else {
                    window.location = url_Login;
                };
            },
            error: function (task_res) {
                window.location = url_Login;
            }
        });
    };

    this.onUplFilePersonalSuccess = function(e) {
        if (e.operation == 'upload') {

            var grid = $("#taskFilesGridPersonal").data("kendoGrid");
            grid.dataSource.read();

            _this.onAttrChanged();
        };

        return true;
    };

    this.onUplFilePersonalError = function(e) {
        var err = e.XMLHttpRequest.responseText;
        if (err.length <= 200) {
            alert(err);
        };
    };


    // private

    function getSettingsData() {
        var grid = $("#taskGridColumnSettingsGrid").data("kendoGrid");
        var data = grid.dataSource.view();
        var ddl = $("#taskGridColumnColorList").getKendoDropDownList();
        var dataItem = ddl.dataItem();
        var color_column_id = null;
        if (dataItem) { color_column_id = dataItem.column_id; }
        return { settings: data, color_column_id: color_column_id };
    };

    function taskGridRowDblClick(e) {
        var grid = $("#taskListGrid").data("kendoGrid");
        curr_task_id = 0;
        selected_row = $(this);
        selected_data_item = grid.dataItem($(this));
        curr_task_group_id = 0;
        curr_win_task_name = 'Personal';
        if (selected_data_item != null) {
            curr_task_id = selected_data_item.task_id;
            curr_task_group_id = selected_data_item.group_id;
            if (curr_task_id > 0) {
                var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');
                $('#winTaskEdit' + curr_win_task_name).html("");                
                win.refresh({
                    url: 'Crm/TaskEdit' + curr_win_task_name,
                    data: { task_id: curr_task_id }
                });
                win.open();
            };
        };
    };

    function onBtnTaskSaveClick(e) {
        taskSave(false);
    };

    function taskSave(winclose) {
        var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');

        var project_name_combo = $("#projectList" + curr_win_task_name).getKendoDropDownList();
        var module_name_combo = $("#moduleList" + curr_win_task_name).getKendoDropDownList();
        var module_version_name_combo = $("#moduleVersionList" + curr_win_task_name).getKendoDropDownList();
        var repair_version_name_combo = $("#repairVersionList" + curr_win_task_name).getKendoDropDownList();
        var client_name_combo = $("#clientList" + curr_win_task_name).getKendoDropDownList();
        var priority_name_combo = $("#priorityList" + curr_win_task_name).getKendoDropDownList();
        var state_name_combo = $("#stateList" + curr_win_task_name).getKendoDropDownList();
        var assigned_user_name_combo = $("#assignedUserList" + curr_win_task_name).getKendoDropDownList();
        var exec_user_name_combo = $("#execUserList" + curr_win_task_name).getKendoDropDownList();
        var event_user_name_combo = $("#eventUserList" + curr_win_task_name).getKendoDropDownList();
        var group_name_combo = $('#groupList' + curr_win_task_name).getKendoDropDownList();

        var task_id_col = curr_task_id;
        var task_name_col = $('#task_name' + curr_win_task_name).val();
        var task_text_col = $('#task_text' + curr_win_task_name).val();
        var task_num_col = 'AU-0000';
        var crt_date_col = new Date();
        var client_name_col = client_name_combo.dataItem()["client_name"];
        var client_id_col = client_name_combo.dataItem()["client_id"];
        var owner_user_name_col = currUser_CabUserName;
        var owner_user_id_col = currUser_CabUserId;

        var required_date_col = null;
        var repair_date_col = null;
        var repair_version_name_col = '[не указан]';
        var repair_version_id_col = 0;
        var project_name_col = '[не указан]';
        var project_id_col = 0;
        var module_name_col = '[не указан]';
        var module_id_col = 0;
        var module_version_name_col = '[не указан]';
        var module_version_id_col = 0;
        var module_part_name_col = '[не указан]';
        var module_part_id_col = 0;
        var priority_name_col = '[не указан]';
        var priority_id_col = 0;
        var state_name_col = '[не указан]';
        var state_id_col = 0;
        var assigned_user_name_col = '[не указан]';
        var assigned_user_id_col = 0;
        var exec_user_name_col = '[не указан]';
        var exec_user_id_col = 0;
        var is_moderated_col = false;
        var event_user_name_col = '[не указан]';
        var event_user_id_col = 0;
        var point_col = 0;
        var fin_cost_col = 0;
        var is_event_name_col = '';
        var is_event_col = false;
        var group_id_col = 0;
        var group_name_col = 'Неразобранные';
        var task_notes = null;
        var task_subtasks = null;

        if (curr_task_group_id == 0) {
            //
        } else {
            point_col = $('#point' + curr_win_task_name).val();
            fin_cost_col = $('#fin_cost' + curr_win_task_name).val();
            task_notes = $("#taskNoteGrid" + curr_win_task_name).data("kendoGrid").dataSource.view();
            task_subtasks = $("#subtaskGrid" + curr_win_task_name).data("kendoGrid").dataSource.view();

            repair_date_col = $('#repair_date' + curr_win_task_name).val();
            state_name_col = state_name_combo.dataItem()["state_name"];
            state_id_col = state_name_combo.dataItem()["state_id"];

            assigned_user_name_col = assigned_user_name_combo.dataItem()["user_name"];
            assigned_user_id_col = assigned_user_name_combo.dataItem()["user_id"];
            exec_user_name_col = exec_user_name_combo.dataItem()["user_name"];
            exec_user_id_col = exec_user_name_combo.dataItem()["user_id"];
            is_moderated_col = $('#IsModerated' + curr_win_task_name).is(':checked');
            if (event_user_name_combo) {
                event_user_name_col = event_user_name_combo.dataItem()["Text"];
                event_user_id_col = event_user_name_combo.dataItem()["Value"];
            };
            required_date_col = $('#required_date' + curr_win_task_name).val();
            project_name_col = project_name_combo.dataItem()["project_name"];
            project_id_col = project_name_combo.dataItem()["project_id"];
            module_name_col = module_name_combo.dataItem()["module_name"];
            module_id_col = module_name_combo.dataItem()["module_id"];
            module_version_name_col = module_version_name_combo.dataItem()["module_version_name"];
            module_version_id_col = module_version_name_combo.dataItem()["module_version_id"];
            repair_version_name_col = repair_version_name_combo.dataItem()["module_version_name"];
            repair_version_id_col = repair_version_name_combo.dataItem()["module_version_id"];
            priority_name_col = priority_name_combo.dataItem()["priority_name"];
            priority_id_col = priority_name_combo.dataItem()["priority_id"];

            is_event_col = $('#is_event' + curr_win_task_name).is(':checked');
            if (is_event_col) { is_event_col_name = 'К'; } else { is_event_col_name = ''; };


            group_id_col = group_name_combo.dataItem()["group_id"];
            group_name_col = group_name_combo.dataItem()["group_name"];
        };

        var grid = $('#taskListGrid').data('kendoGrid');
        var dataSource = grid.dataSource;

        var task = {
            task_id: task_id_col, crt_date: crt_date_col, task_num: task_num_col,
            task_name: task_name_col, task_text: task_text_col,
            state_name: state_name_col, state_id: state_id_col,
            client_name: client_name_col, client_id: client_id_col,
            exec_user_name: exec_user_name_col, exec_user_id: exec_user_id_col,
            is_event_for_exec: event_user_id_col,
            assigned_user_name: assigned_user_name_col, assigned_user_id: assigned_user_id_col,
            module_name: module_name_col, module_id: module_id_col,
            //module_part_name: module_part_name_col, module_part_id: module_part_id_col,
            module_version_name: module_version_name_col, module_version_id: module_version_id_col,
            repair_version_name: repair_version_name_col, repair_version_id: repair_version_id_col,
            owner_user_name: owner_user_name_col, owner_user_id: owner_user_id_col,
            priority_name: priority_name_col, priority_id: priority_id_col,
            project_name: project_name_col, project_id: project_id_col,
            required_date: required_date_col,
            repair_date: repair_date_col,
            group_id: group_id_col, group_name: group_name_col,
            point: point_col, fin_cost: fin_cost_col, is_event: is_event_col, is_event_name: is_event_name_col,
            IsModerated: is_moderated_col
        };
        $.ajax({
            type: "POST",
            url: "Crm/TaskAdd",
            data: JSON.stringify({ task: task, notes: task_notes, subtasks: task_subtasks }),
            contentType: 'application/json; charset=windows-1251',
            success: function (task_res) {
                if (task_res != null) {
                    if (task_id_col > 0) {
                        if (selected_data_item != null) {
                            if ((curr_page_group_id != -1) && (curr_task_group_id != task_res.group_id)) {
                                dataSource.remove(selected_data_item);
                            } else {
                                selected_data_item.task_name = task_res.task_name;
                                selected_data_item.state_name = task_res.state_name;
                                selected_data_item.state_id = task_res.state_id;
                                selected_data_item.client_name = task_res.client_name;
                                selected_data_item.client_id = task_res.client_id;
                                selected_data_item.exec_user_name = task_res.exec_user_name;
                                selected_data_item.exec_user_id = task_res.exec_user_id;
                                selected_data_item.is_event_for_exec = task_res.is_event_for_exec;
                                selected_data_item.assigned_user_name = task_res.assigned_user_name;
                                selected_data_item.assigned_user_id = task_res.assigned_user_id;
                                selected_data_item.module_name = task_res.module_name;
                                selected_data_item.module_id = task_res.module_id;
                                selected_data_item.module_version_name = task_res.module_version_name;
                                selected_data_item.module_version_id = task_res.module_version_id;
                                selected_data_item.repair_version_name = task_res.repair_version_name;
                                selected_data_item.repair_version_id = task_res.repair_version_id;
                                selected_data_item.owner_user_name = task_res.owner_user_name;
                                selected_data_item.owner_user_id = task_res.owner_user_id;
                                selected_data_item.priority_name = task_res.priority_name;
                                selected_data_item.priority_id = task_res.priority_id;
                                selected_data_item.project_name = task_res.project_name;
                                selected_data_item.project_id = task_res.project_id;
                                selected_data_item.required_date = task_res.required_date;
                                selected_data_item.repair_date = task_res.repair_date;
                                selected_data_item.group_id = task_res.group_id;
                                selected_data_item.point = task_res.point;
                                selected_data_item.fin_cost = task_res.fin_cost;
                                selected_data_item.is_event = task_res.is_event;
                                selected_data_item.is_event_name = task_res.is_event_name;
                                selected_data_item.group_name = task_res.group_name;
                                //
                                if (!winclose) {                                    
                                    var grid = $("#subtaskGrid" + curr_win_task_name).data("kendoGrid");
                                    var view = grid.dataSource.view();
                                    for (var i = 0; i < view.length; i++) {
                                        var dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));
                                        if (dataItem) {
                                            dataItem.IsNew = false;
                                        };
                                    };
                                } else {                                    
                                    refreshGrid('#taskListGrid');
                                };
                            }
                        };
                    } else {
                        dataSource.insert(0, task_res);
                    }
                };
            },
            error: function (task_res) {
                alert('Ошибка при редактировании задачи');
            }
        });

        if (winclose) {
            win.close();
        } else {
            $('#btnTaskOkPersonal').removeClass('btn-success');
            $('#btnTaskOkPersonal').addClass('btn-default');
            $('#btnTaskOkPersonal').attr('disabled', true);
            //
            $('#btnTaskSavePersonal').removeClass('btn-info');
            $('#btnTaskSavePersonal').addClass('btn-default');
            $('#btnTaskSavePersonal').attr('disabled', true);
        }
    };

    function onBtnTaskCancelClick(e) {
        var win = $('#winTaskEdit' + curr_win_task_name).data('kendoWindow');
        win.close();
    };

    function onSubtaskAddClick(e) {
        var text = $('#txtSubtaskAdd' + curr_win_task_name).val();
        if (!text) return false;

        var curr_exec_user_id = 7;
        var combo = $("#assignedUserList" + curr_win_task_name).getKendoDropDownList();
        curr_exec_user_id = combo.dataItem()["user_id"];
        var curr_task_num = generateSubtaskNum();
        var new_subtask = {
            subtask_id: 0, task_id: $('#task_id').val(), subtask_text: text,
            state_id: 1, owner_user_id: currUser_CabUserId, crt_date: new Date(),            
            repair_date: null, task_num: curr_task_num, owner_user_name: currUser_CabUserName,
            file_id: null,
            IsChecked: false, IsNew: true
        };

        var grid = $("#subtaskGrid" + curr_win_task_name).data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_subtask);
        $('#txtSubtaskAdd' + curr_win_task_name).val('');

        _this.onAttrChanged();

        return true;
    };

    function onTaskNoteAddClick(e) {
        var text = $('#txtTaskNoteAdd' + curr_win_task_name).val();
        if (!text) return false;

        var new_note = { note_id: 0, note_text: text, crt_date: new Date(), owner_user_id: currUser_CabUserId, user_name: currUser_CabUserName, file_name: '' };
        var grid = $("#taskNoteGrid" + curr_win_task_name).data("kendoGrid");
        var dataSource = grid.dataSource;
        dataSource.insert(0, new_note);
        $('#txtTaskNoteAdd' + curr_win_task_name).val('');

        _this.onAttrChanged();

        return true;
    };

    function generateSubtaskNum() {
        AutoID = 1; // Get the latest sequential ID for this sector.
        var newAutoID = localStorage.subtask_num;
        if (!isNaN(newAutoID)) {
            AutoID = parseInt(newAutoID) + 1; // Save the new ID
        }
        localStorage.subtask_num = AutoID;
        return AutoID;
    };

    function onSubtaskExcelClick(e) {
        var grid = $('#subtaskGrid' + curr_win_task_name).getKendoGrid();
        grid.saveAsExcel();
    };

    function onSubtaskPdfClick(e) {
        var grid = $('#subtaskGrid' + curr_win_task_name).getKendoGrid();
        grid.saveAsPDF();
    };

    function onBtnTaskOkClick(e) {
        if (checkSaveFlag) {
            checkTaskSave();
        }
        else {
            taskSave(true);
        };
    };

    function checkTaskSave() {
        var ctrl = $('#spanTaskErrMessPersonal');

        $.ajax({
            type: "POST",
            url: '/Crm/TaskSaveBefore?task_id=' + curr_task_id + '&task_upd_num=' + $('#upd_num').val(),
            data: '',
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response != null) {
                    if (!response.is_ok) {
                        ctrl.html(response.mess);
                        ctrl.removeClass('hidden');
                        checkSaveFlag = false;
                        return false;
                    } else {
                        taskSave(true);
                        return true;
                    };
                } else {
                    taskSave(true);
                    return true;
                };
            },
            error: function (task_res) {
                taskSave(true);
                return true;
            }
        });
    };
}

var myTaskList = null;

var firstDataBound = true;
var noHint = $.noop;
var checkedIds = {};
var curr_page_group_id = 3;
var curr_task_group_id = 3;
var curr_role_id = 1;
var curr_task_id = 0;
var selected_row = null;
var selected_data_item = null;
var curr_win_task_name = 'Personal';
var exportFlag = false;
var checkSaveFlag = true;

$(function () {
    myTaskList = new MyTaskList();
    myTaskList.init();
});
