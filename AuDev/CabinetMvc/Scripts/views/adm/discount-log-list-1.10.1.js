﻿function DiscountLogList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#admEventGrid', 0);
    };
}

var discountLogList = null;

$(function () {
    discountLogList = new DiscountLogList();
    discountLogList.init();
});
