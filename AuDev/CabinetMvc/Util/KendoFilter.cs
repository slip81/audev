﻿using System;
using System.Net;
using System.Windows;
using System.Collections.Generic;
using Newtonsoft.Json;
using AuDev.Common.Util;

namespace CabinetMvc.Util
{
    public class KendoFilter
    {
        public KendoFilter()
        {
            //
        }

        public KendoFilterItem filter { get; set; }

        [JsonIgnore()]
        //public List<string> FilterStringParams { get; set; }
        public List<object> FilterStringParams { get; set; }
        [JsonIgnore()]
        public string FilterString 
        { 
            get
            {
                string res = "";
                int curr_param_num = 0;
                bool isFirst = true;
                
                bool isDecimal = false;
                decimal decimal_val = 0;

                bool isDate = false;
                DateTime date_val = DateTime.Today;
                DateTime date_val_local = DateTime.Today;

                string symb = "";
                
                FilterStringParams = new List<object>();

                if ((filter != null) && (filter.filters != null))
                {
                    foreach (var item in filter.filters)
                    {                        
                        switch (item.Operator)
                        {
                            case "contains":
                                if (!isFirst)
                                    res += " AND ";
                                res += item.Field + ".ToLower().Contains(@" + (curr_param_num).ToString() + ")";
                                //res += item.Field + ".Contains(@" + (curr_param_num).ToString() + ")";
                                FilterStringParams.Add(item.Value.ToLower());
                                isFirst = false;
                                curr_param_num++;
                                break;
                            case "eq":
                                if (!isFirst)
                                    res += " AND ";
                                //res += item.Field + ".ToLower() = @" + (curr_param_num).ToString();
                                isDecimal = decimal.TryParse(item.Value, out decimal_val);
                                isDate = DateTime.TryParse(item.Value, out date_val);
                                if (isDecimal)
                                {
                                    res += item.Field + " == @" + (curr_param_num).ToString();                                    
                                    FilterStringParams.Add(decimal_val); 
                                }
                                else if (isDate)
                                {

                                    date_val_local = CabinetUtil.KendoFilterDateToLocal(date_val);
                                    //date_val_local = new DateTime(date_val.Ticks, DateTimeKind.Utc);     
                                    //date_val_local = date_val.Date;
                                                                   
                                    res += item.Field + " == @" + (curr_param_num).ToString();
                                    FilterStringParams.Add(date_val_local); 
                                }
                                else
                                {
                                    res += item.Field + ".ToLower() = @" + (curr_param_num).ToString();                                    
                                    FilterStringParams.Add(item.Value.ToLower());
                                }                                                                
                                isFirst = false;
                                curr_param_num++;
                                break;
                            case "startswith":
                                if (!isFirst)
                                    res += " AND ";
                                res += item.Field + ".ToLower().StartsWith(@" + (curr_param_num).ToString() + ")";
                                //res += item.Field + ".StartsWith(@" + (curr_param_num).ToString() + ")";
                                FilterStringParams.Add(item.Value.ToLower());
                                isFirst = false;
                                curr_param_num++;
                                break;
                            case "lt":
                            case "gt":
                            case "lte":
                            case "gte":
                            case "neq":
                                if (!isFirst)
                                    res += " AND ";
                                
                                if (item.Operator == "lt")
                                {
                                    symb = "<";
                                }
                                else if (item.Operator == "gt")
                                {
                                    symb = ">";
                                }
                                else if (item.Operator == "lte")
                                {
                                    symb = "<=";
                                }
                                else if (item.Operator == "gte")
                                {
                                    symb = ">=";
                                }
                                else if (item.Operator == "neq")
                                {
                                    symb = "!=";
                                }
                                
                                isDecimal = decimal.TryParse(item.Value, out decimal_val);
                                isDate = DateTime.TryParse(item.Value, out date_val);
                                if (isDecimal)
                                {
                                    res += item.Field + " " + symb + " @" + (curr_param_num).ToString();                                    
                                    FilterStringParams.Add(decimal_val); 
                                }
                                else if (isDate)
                                {
                                    date_val_local = CabinetUtil.KendoFilterDateToLocal(date_val);
                                    res += item.Field + " " + symb + " @" + (curr_param_num).ToString();
                                    FilterStringParams.Add(date_val_local); 
                                }    
                                else
                                {
                                    if (item.Operator == "neq")
                                    {
                                        res += item.Field + ".ToLower() != @" + (curr_param_num).ToString();
                                        FilterStringParams.Add(item.Value.ToLower());
                                    }
                                }
                                isFirst = false;
                                curr_param_num++;
                                break;
                            default:
                                break;
                        }
                    }
                }
                return res;
            }        
        }
    }
}
