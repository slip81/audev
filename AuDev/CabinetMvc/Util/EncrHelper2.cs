﻿using System;
using System.Net;
using System.Windows;

namespace CabinetMvc.Util
{
    internal class EncrHelper2
    {
        private delegate char HashMapHandler(int i);
        private HashMapHandler HashMap = _HashMap;

        // Авторский алгоритм (с) Силин
        // Полезен спрятать запросик
        private char HashToChar(string Hash, int I)
        {
            int J; int V1; int V2;
            V1 = 0;
            for (int i = 0; i < 32; i++)
                if (HashMap(i + 1) == Hash[0]) { V1 = i; break; }
            V2 = 0;
            for (int i = 0; i < 32; i++)
                if (HashMap(i + 1) == Hash[1]) { V2 = i; break; }
            J = ((V1 & 0xF) | (V2 << 4)) - (I & 0xFF);
            if (J < 13) { J = 13; }
            return (char)(J & 0xFF);
        }
        private string CharToHash(char C, int I)
        {
            I = ((byte)C) + (I & 0xFF);
            string s = char.ToString(HashMap((I & 0xF) + 1));
            return s + char.ToString(HashMap(((I >> 4) & 0x1F) + 1));
        }

        public string _GetHashValue(string Value)
        {
            string S = Value.Substring(0, (Value.Length) & 0xFFFE);
            char[] Result = new char[((S.Length + 1) >> 1)];
            //char[] Result = new char[((S.Length + 1) >> 1) + 1];
            for (int i = 0; i < S.Length >> 1; i++)
            {
                Result[i] = HashToChar(
                     S.Substring(i << 1, 2),
                     i + 1);
            }
            return new string(Result);
        }
        public string _SetHashValue(string Value)
        {
            string Result = "";
            for (int i = 0; i < Value.Length; i++)
            {
                Result = Result + CharToHash(Value[i], i + 1);
            }
            return Result;
        }

        // Авторский алгоритм (с) АУРИТ
        // Полезен спрятать запросик
        private static char _HashMap(int i)
        {
            char[] m = new char[32 + 1];
            m[10] = 'I';
            m[11] = 'h';
            m[12] = 'Y';
            m[13] = 'j';
            m[14] = 'F';
            m[20] = 'b';
            m[21] = 'B';
            m[22] = 's';
            m[23] = 'J';
            m[24] = 'k';
            m[25] = 'W';
            m[26] = 'a';
            m[27] = 'R';
            m[28] = 'f';
            m[29] = 'Z';
            m[15] = 'y';
            m[16] = 'H';
            m[17] = 'A';
            m[18] = 'z';
            m[19] = 'K';
            m[1] = 'd';
            m[2] = 'D';
            m[3] = 'g';
            m[4] = 'G';
            m[5] = 'w';
            m[30] = 'm';
            m[31] = 'S';
            m[32] = 'c';
            m[6] = 'M';
            m[7] = 'i';
            m[8] = 'C';
            m[9] = 'r';
            return m[i];

        }

    }
}
