﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.Util
{
    public class CabinetRazorViewEngine : RazorViewEngine
    {
        public CabinetRazorViewEngine()
        {
            FileExtensions = new string[] { "cshtml" };

			ViewLocationFormats = new[]
			{
				"~/Views/{1}/{0}.cshtml",
				"~/Views/Shared/{0}.cshtml",
				"~/Views/Adm/{0}.cshtml",
				"~/Views/Cabinet/{0}.cshtml",                
				"~/Views/Defect/{0}.cshtml",
                "~/Views/Exchange/{0}.cshtml",
                "~/Views/HelpDesk/{0}.cshtml",
                "~/Views/Discount/{0}.cshtml",
				"~/Views/Scheduler/{0}.cshtml",
				"~/Views/User/{0}.cshtml",
                "~/Views/Cva/{0}.cshtml",
                "~/Views/Auth/{0}.cshtml",
                "~/Views/Storage/{0}.cshtml",
                "~/Views/Help/{0}.cshtml",                
                "~/Views/Update/{0}.cshtml",
                "~/Views/Prj/{0}.cshtml",
                "~/Views/Stock/{0}.cshtml",
                "~/Views/Asna/{0}.cshtml",
			};
			MasterLocationFormats = new[]
			{
				"~/Views/{1}/{0}.cshtml",
				"~/Views/Shared/{0}.cshtml",
				"~/Views/Adm/{0}.cshtml",
				"~/Views/Cabinet/{0}.cshtml",                
                "~/Views/Defect/{0}.cshtml",
                "~/Views/Exchange/{0}.cshtml",
                "~/Views/HelpDesk/{0}.cshtml",
				"~/Views/Discount/{0}.cshtml",
				"~/Views/Scheduler/{0}.cshtml",
				"~/Views/User/{0}.cshtml",
                "~/Views/Cva/{0}.cshtml",
                "~/Views/Auth/{0}.cshtml",
                "~/Views/Storage/{0}.cshtml",
                "~/Views/Help/{0}.cshtml",                
                "~/Views/Update/{0}.cshtml",
                "~/Views/Prj/{0}.cshtml",
                "~/Views/Stock/{0}.cshtml",
                "~/Views/Asna/{0}.cshtml",
			};
			PartialViewLocationFormats = new[]
			{
				"~/Views/{1}/{0}.cshtml",
				"~/Views/Shared/{0}.cshtml",
				"~/Views/Adm/{0}.cshtml",
				"~/Views/Cabinet/{0}.cshtml",                
                "~/Views/Defect/{0}.cshtml",
                "~/Views/Exchange/{0}.cshtml",
                "~/Views/HelpDesk/{0}.cshtml",
				"~/Views/Discount/{0}.cshtml",
				"~/Views/Scheduler/{0}.cshtml",
				"~/Views/User/{0}.cshtml",
                "~/Views/Cva/{0}.cshtml",
                "~/Views/Auth/{0}.cshtml",
                "~/Views/Storage/{0}.cshtml",
                "~/Views/Help/{0}.cshtml",                
                "~/Views/Update/{0}.cshtml",
                "~/Views/Prj/{0}.cshtml",
                "~/Views/Stock/{0}.cshtml",
                "~/Views/Asna/{0}.cshtml",
			};
        }
    }
}