﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Hosting;
using System.Reflection;
using System.Configuration;
using System.Data.EntityClient;
using System.IO;
using System.Text.RegularExpressions;
using System.ComponentModel.DataAnnotations;
using NodaTime;
using NodaTime.Text;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.Util
{
    public static class CabinetUtil
    {
        public static string GetAuMainDbConnectionString()
        {
            string entityConnectionString = ConfigurationManager.ConnectionStrings["AuMainDb"].ConnectionString;
            return new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;
        }

        public static DateTime KendoFilterDateToLocal(DateTime filter_date)
        {
            var pattern = InstantPattern.CreateWithInvariantCulture("ddMMyyHHmmss");
            var parseResult = pattern.Parse(filter_date.ToString("ddMMyyHHmmss"));
            if (!parseResult.Success)
                throw new Exception("...whatever...");
            var instant = parseResult.Value;
            var timeZone = DateTimeZoneProviders.Tzdb["Asia/Yekaterinburg"];
            var zonedDateTime = instant.InZone(timeZone);

            return zonedDateTime.ToDateTimeUnspecified().Date;   
        }

        public static List<string> GetErrorListFromModelState(ModelStateDictionary modelState)
        {
            var query = from state in modelState.Values
                        from error in state.Errors
                        select error.ErrorMessage;

            var errorList = query.ToList();
            return errorList;
        }

        public static string GetTimestampedUrl(string virtualPath)
        {
            var realPath = HostingEnvironment.MapPath(virtualPath);
            var file = new FileInfo(realPath);
            var res = VirtualPathUtility.ToAbsolute(virtualPath) + "?" + file.LastWriteTime.ToFileTime();
            return res;
        }

        public static string SiteMapNode_GetImagePath(Kendo.Mvc.SiteMapNode node)
        {                        
            var img_attr = node.Attributes.Where(ss => ss.Key == "img").FirstOrDefault();
            return img_attr.Value != null ? (string)img_attr.Value : "";
        }

        public static Tuple<bool, string, string> SiteMapNode_GetRouteValues(Kendo.Mvc.SiteMapNode node)
        {
            var route_values_attr = node.Attributes.Where(ss => ss.Key == "route_values").FirstOrDefault();
            if (route_values_attr.Value == null)
                return new Tuple<bool, string, string>(false, "", "");
            var route_attr_splitted = ((string)route_values_attr.Value).Split(new []{'='});
            return new Tuple<bool, string, string>(true, route_attr_splitted[0], route_attr_splitted[1]);
        }

        public static bool SiteMapNode_IsSeparator(Kendo.Mvc.SiteMapNode node)
        {
            var type_attr = node.Attributes.Where(ss => ss.Key == "type").FirstOrDefault();
            return type_attr.Value != null ? ((string)type_attr.Value == "separator") : false;
        }

        public static Tuple<bool, string> SiteMapNode_IsExternalLink(Kendo.Mvc.SiteMapNode node)
        {
            var type_attr = node.Attributes.Where(ss => ss.Key == "type").FirstOrDefault();
            var url_attr = node.Attributes.Where(ss => ss.Key == "url").FirstOrDefault();
            if ((type_attr.Value == null) || (url_attr.Value == null))
                return new Tuple<bool, string>(false, "");
            return new Tuple<bool, string>((string)type_attr.Value == "external_link", (string)url_attr.Value);            
        }

        public static bool SiteMapNode_IsActionFake(Kendo.Mvc.SiteMapNode node)
        {
            var action_fake_attr = node.Attributes.Where(ss => ss.Key == "action_fake").FirstOrDefault();
            return action_fake_attr.Value != null ? ((string)action_fake_attr.Value == "true") : false;
        }

        public static string GetKendoFolder()
        {
            return "2015.2.624";
        }

        public static string GetJsBundleName()
        {
            return "~/Scripts/kendo/" + GetKendoFolder() + "/js";
        }

        /*
        public static string GetDiscountBundleName()
        {
            return "~/Scripts/views/discount";
        }
        */

        public static string GetCssBundleName()
        {
            return "~/Content/kendo/" + GetKendoFolder() + "/css";
        }

        
        public static string GetScripts(string scriptFolder, string scriptFile)
        {
            var path = HttpRuntime.AppDomainAppPath;
            var files = Directory.GetFiles(path + scriptFolder).Select(x => Path.GetFileName(x)).ToList();
            string script = string.Empty;
            scriptFile = scriptFile.Replace(".", @"\.").Replace("{0}", "(\\d+\\.?)+");
            //if (1 == 1)
            if (!System.Diagnostics.Debugger.IsAttached)            
            {
                scriptFile = scriptFile.Replace(".js", ".min.js");
            }
            Regex r = new Regex(scriptFile, RegexOptions.IgnoreCase);

            foreach (var f in files)
            {
                Match m = r.Match(f);
                while (m.Success)
                {
                    script = m.Captures[0].ToString();

                    m = m.NextMatch();
                }
            }

            return scriptFolder + "/" + script;
        }

        public static List<SelectListItem> EnumToSelectList(Type enumType)
        {
            return Enum
              .GetValues(enumType)
              //.Cast<int>()
              .Cast<Enum>()
              .Select(i => new SelectListItem
              {
                  Value = Convert.ToInt32(i).ToString(),
                  Text = i.GetAttribute<DisplayAttribute>().Name,
              }
              )
              .ToList();
        }
        
    }
}