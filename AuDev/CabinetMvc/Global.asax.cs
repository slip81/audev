﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Net.Http.Headers;

namespace CabinetMvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            //logger.Info("Application_Start start");

            /*
            HttpConfiguration config = GlobalConfiguration.Configuration;
            foreach (var mediaType in config.Formatters.FormUrlEncodedFormatter.SupportedMediaTypes)
            {
                config.Formatters.JsonFormatter.SupportedMediaTypes.Add(mediaType);
            }
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/plain"));

            config.Formatters.Remove(config.Formatters.FormUrlEncodedFormatter);
            config.Formatters.Remove(config.Formatters.XmlFormatter);            
            */

            AreaRegistration.RegisterAllAreas();
            
            ViewEngines.Engines.Clear();
            var viewEngine = new Util.CabinetRazorViewEngine();
            ViewEngines.Engines.Add(viewEngine);
            
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //logger.Info("Application_Start end");

            /*
            if (System.Diagnostics.Debugger.IsAttached)
                EFlogger.EntityFramework6.EFloggerFor6.Initialize();
            */
        }

        protected void Session_Start()
        {
            /*
            if (!Util.UserManager.Init(User.Identity.Name))
            {
                FormsAuthentication.SignOut();
            }
            */
        }


        /*
        public void Init() { logger.Info("Application Init"); }
        public void Dispose() { logger.Info("Application Dispose"); }
        protected void Application_Error() { logger.Info("Application Error"); }
        protected void Application_End() { logger.Info("Application End"); }
        */
    }
}
