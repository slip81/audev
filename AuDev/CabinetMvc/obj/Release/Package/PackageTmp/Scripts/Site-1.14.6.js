﻿/*
 * 
 * 
 * 
 */
 
    function drawEmptyRow(gridName) {

        //Get the number of Columns in the grid        
        var colCount = $(gridName).find('.k-grid-header colgroup > col').length;
        var gridDataSource = $(gridName).getKendoGrid().dataSource;
        var maxRowCount = 5;

        //If There are no results place an indicator row
        if (gridDataSource._view.length == 0) {
            $(gridName).find('.k-grid-content tbody')
                .append('<tr class="kendo-data-row"><td colspan="' +
                    colCount +
                    '" style="text-align:left; background-color: #d9d9d9;"><b>Нет записей для отображения</b></td></tr>');
        }
    };
	
    function htmlDecode(value) {
        return $('<div/>').html(value).text();
    };

    function onMenuItemSelect(e) {
        var menu_item = e.item;
        if (menu_item) {
            if ($(menu_item).hasClass('action-fake-menu')) {
                e.preventDefault();
                return false;
            } else if ($(menu_item).hasClass('clear-cache-menu')) {
                //alert('clear-cache-menu !');
                clearCache();
                return false;
            } else {
                return true;
            };
        } else {
            return false;
        };
    };

    function onGridError(grid_name) {
        return function (e) {
            if (e.errors) {
                var message = "Ошибка:\n";
                $.each(e.errors, function (key, value) {
                    if ('errors' in value) {
                        $.each(value.errors, function () {
                            message += this + "\n";
                        });
                    }
                });
                alert(message);
                var grid = $('#' + grid_name).getKendoGrid();
                grid.cancelChanges();
            };
        };
    };

    function onGridRequestEnd(grid_name) {
        return function (e) {
            if ((e.type == "create") || (e.type == "update")) {
                $('#' + grid_name).data("kendoGrid").dataSource.read();
            }
        };
    };

    function refreshGrid(grid_name) {
        var grid = $(grid_name).data('kendoGrid');
        if (grid) {
            if (grid.dataSource.page() != 1) {
                grid.dataSource.page(1);
            };
            grid.dataSource.read();
        };
    };

    function refreshScheduler(sch_name, lock) {
        if (!lock) {
            $(sch_name).data('kendoScheduler').dataSource.read();
        };
    };

    function clearSortGrid(grid_name) {
        var grid = $(grid_name).data('kendoGrid');
        if (grid) {
            grid.dataSource.sort({});
        };
    };

    function kendoFastRedrawRow(grid, row) {
        var dataItem = grid.dataItem(row);

        var rowChildren = $(row).children('td[role="gridcell"]');

        for (var i = 0; i < grid.columns.length; i++) {

            var column = grid.columns[i];
            var template = column.template;
            var cell = rowChildren.eq(i);

            if (template !== undefined) {
                var kendoTemplate = kendo.template(template);

                // Render using template
                cell.html(kendoTemplate(dataItem));
            } else {
                var fieldValue = dataItem[column.field];

                var format = column.format;
                var values = column.values;

                if (values !== undefined && values != null) {
                    // use the text value mappings (for enums)
                    for (var j = 0; j < values.length; j++) {
                        var value = values[j];
                        if (value.value == fieldValue) {
                            cell.html(value.text);
                            break;
                        }
                    }
                } else if (format !== undefined) {
                    // use the format
                    cell.html(kendo.format(format, fieldValue));
                } else {
                    // Just dump the plain old value
                    cell.html(fieldValue);
                }
            }
        }
    };

    function setGridMaxHeight(gridId, add) {
        var grid_max_height = $(window).height() - $(gridId).offset().top - $(gridId + '>.k-grid-toolbar').height() - $(gridId + '>.k-grid-header').height() - $(gridId + '>.k-grid-pager').height() - $(gridId + '>.k-grid-footer').height() - 50 - add;
        $(gridId + '>.k-grid-content').css('max-height', grid_max_height + 'px');
    };

    function setGridMaxHeight_table(gridId, add) {
        var grid_max_height = $(window).height() - $(gridId).offset().top - $(gridId + ' .k-grid-header').height() - 50 - add;
        $(gridId + ' tbody[role="rowgroup"]').css('max-height', grid_max_height + 'px');
    };

    function setControlHeight(controlId, add) {
        var control_max_height = $(window).height() - $(controlId).offset().top - 50 - add;
        $(controlId).css('height', control_max_height + 'px');
    };

    function setGridMaxHeight_relative(gridId, relativeControlId, add) {
        var grid_max_height = $(relativeControlId).height() - $(gridId + '>.k-grid-toolbar').height() - $(gridId + '>.k-grid-header').height() - $(gridId + '>.k-grid-pager').height() - $(gridId + '>.k-grid-footer').height() - add;
        $(gridId + '>.k-grid-content').css('max-height', grid_max_height + 'px');
    };

    function isInt(value) {
        if (isNaN(value)) {
            return false;
        }
        var x = parseFloat(value);
        return (x | 0) === x;
    };

    function generateRandomString() {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";        

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    };

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    };

    function diffDays(dateBeg, dateEnd) {
        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        return Math.round(Math.abs((dateBeg.getTime() - dateEnd.getTime()) / (oneDay)));
    };

    function confirmBackspaceNavigations() {
        // http://stackoverflow.com/a/22949859/2407309
        var backspaceIsPressed = false
        $(document).keydown(function (event) {
            if (event.which == 8) {
                backspaceIsPressed = true
            }
        })
        $(document).keyup(function (event) {
            if (event.which == 8) {
                backspaceIsPressed = false
            }
        })
        $(window).on('beforeunload', function () {
            if (backspaceIsPressed) {
                backspaceIsPressed = false
                return "Вы уверены, что хотите покинуть эту страницу?"
            }
        })
    };

    function onTaskFileRemoveBtnClick(id) {        
        $.ajax({
            type: "POST",
            url: "TaskFileRemove",
            data: JSON.stringify({ file_id: id }),
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response != null) {
                    if (response.err) {
                        alert(response.mess);
                    } else {
                        $("#taskFilesGrid" + curr_win_task_name).getKendoGrid().dataSource.read();
                    };
                } else {
                    alert('Ошибка при попытке удаления файла');
                };
                return false;
            },
            error: function (response) {
                alert('Ошибка при удалении файла');
                return false;
            }
        });
        return false;
    };

    function showConfirmWindow(templId, confirmContent, confirmHandler) {
        var kendoWindow = $("<div />").kendoWindow({
            actions: ["Close"],
            title: "Подтверждение",
            draggable: true,
            resizable: true,
            modal: true,
            iframe: false,
            deactivate: function () {
                this.destroy();
            }
        });

        kendoWindow.data("kendoWindow").content($(templId).html());
        kendoWindow.find('.confirm-text').text(confirmContent);
        kendoWindow.data("kendoWindow").center().open();
        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        kendoWindow.data("kendoWindow").close();
                        if (confirmHandler)
                            confirmHandler();
                    } else {
                        kendoWindow.data("kendoWindow").close();
                    };
                })
             .end();
    };

    function showEditWindow (templId, windowTitle, editLabelContent, editValueInit, editLabel2Content, editValue2Init, confirmHandler) {
        var kendoWindow = $("<div />").kendoWindow({
            actions: ["Close"],
            title: windowTitle,
            draggable: true,
            resizable: true,
            modal: true,
            iframe: false,
            activate: function () {
                $('#win-edit-value').select();
            },
            deactivate: function () {
                this.destroy();
            }
        });

        kendoWindow.data("kendoWindow").content($(templId).html());
        kendoWindow.find('.edit-label').text(editLabelContent);
        $(kendoWindow.find('#win-edit-value')).attr("value", editValueInit);
        kendoWindow.find('.edit2-label').text(editLabel2Content);
        $(kendoWindow.find('#win-edit2-value')).attr("value", editValue2Init);
        kendoWindow.data("kendoWindow").center().open();
        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        kendoWindow.data("kendoWindow").close();
                        if (confirmHandler)
                            confirmHandler();
                    } else {
                        kendoWindow.data("kendoWindow").close();
                    };
                })
             .end();
    };

    function showInfoWindow(templId, windowTitle, activateHandler) {
        var kendoWindow = $("<div />").kendoWindow({
            actions: ["Close"],
            title: windowTitle,
            draggable: true,
            resizable: true,
            modal: true,
            iframe: false,
            activate: function () {
                if (activateHandler)
                    activateHandler();
            },
            deactivate: function () {
                this.destroy();
            }
        });

        kendoWindow.data("kendoWindow").content($(templId).html());        
        kendoWindow.data("kendoWindow").center().open();
        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    kendoWindow.data("kendoWindow").close();
                })
             .end();
    };

    function checkAuthorization(url_IsAuthorized, url_Login) {
        return null;
    };

    function imgCycle(el, img_src_list) {
        var curr_src = $(el).attr('src');        
        var max_index = img_src_list.length - 1;
        var curr_index = img_src_list.indexOf(curr_src);
        if (curr_index <= -1)
            return;
        if (curr_index >= max_index)
            curr_index = -1;
        var next_index = curr_index + 1;
        var next_src = img_src_list[next_index];
        if (next_src)
            $(el).attr('src', next_src);
    };

    function toggleSplitter(splitterId, paneToExpand, paneToCollapse) {
        var splitter = $(splitterId).data("kendoSplitter");
        splitter.expand(paneToExpand);
        splitter.collapse(paneToCollapse);        
    };

    function waitIndicator(target, start) {
        var element = $(target);
        kendo.ui.progress(element, start);
    };

    function loadingDialog(element, open, text, timeout) {
        var effects = ['blind', 'bounce', 'clip', 'drop', 'explode', 'fade', 'fold', 'puff', 'pulsate', 'scale', 'slide'];
        var effect = effects[Math.floor(Math.random() * effects.length)];
        var loadingHtml = text ? text : '<p class="text-center"><strong>Пожалуйста подождите...</strong></p>';
        if (open) {
            $(element).dialog('option', 'show', effect);
            $(element).dialog('open').html(loadingHtml);
            if (timeout) {
                setTimeout(function () {
                    $(element).dialog('option', 'hide', effect);
                    $(element).dialog('close');
                }, timeout);
            }
        } else {
            $(element).dialog('option', 'hide', effect);
            $(element).dialog('close');
        };
    };

    function saveSelectedRow(gridId, dataItemKey) {
        var grid = $(gridId).data('kendoGrid');
        var row = grid.select();
        var dataItem = grid.dataItem(row);
        return dataItem ? dataItem[dataItemKey] : null;
    };

    function restoreSelectedRow(gridId, dataItemId) {
        var grid = $(gridId).data('kendoGrid');
        var dataItem = null;
        var row = null;
        if (dataItemId) {
            var dataItem = grid.dataSource.get(dataItemId);
        };        
        if (dataItem) {
            row = grid.tbody.find('tr[data-uid="' + dataItem.uid + '"]');
        } else {
            row = grid.tbody.find('tr:first');
            grid.content.scrollTop(row.position().top);
        };
        grid.select(row);
    };

    function getMultiSelectFilters(filterId, fieldName, filterOperator) {
        var result = [];
        var filter = $(filterId).data('kendoMultiSelect').value();
        if ((filter) && (filter.length > 0)) {
            $.each(filter, function (key, val) {
                result.push(
                    {
                        filters: [
                            {
                                field: fieldName,
                                operator: filterOperator,
                                value: val
                            }
                        ]
                    });
            });
        };
        return result;
    };

    function gridConfigSave(gridId, cabGridId, successHandler, errorHandler, completeHandler) {
        var grid = $(gridId).data("kendoGrid");
        var dataSource = grid.dataSource;

        var state = {
            columns: grid.columns,
            pageSize: dataSource.pageSize(),
            sort: dataSource.sort(),
            filter: dataSource.filter(),
            group: dataSource.group()
        };

        $.ajax({
            url: "GridConfigSave",
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ grid_id: cabGridId, data: JSON.stringify(state) }),
            success: function () {
                if (successHandler) {
                    successHandler();
                };
            },
            error: function () {
                if (errorHandler) {
                    errorHandler();
                };
            },
            complete: function () {
                if (completeHandler) {
                    completeHandler();
                };
            }
        });
    };

    function gridConfigLoad(gridId, cabGridId, errorHandler, completeHandler, columnsHandler) {
        $.ajax({
            url: "GridConfigLoad",
            async: false,
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ grid_id: cabGridId }),
            success: function (data) {
                gridConfigApply(data, gridId, columnsHandler);
            },
            error: function () {
                if (errorHandler) {
                    errorHandler();
                };
            },
            complete: function () {
                if (completeHandler) {
                    completeHandler();
                };
            }
        });
    };

    function gridConfigApply(data, gridId, columnsHandler) {
        var grid = $(gridId).data("kendoGrid");
        //
        if ((!data) || ($.isEmptyObject(data))) {
            grid.dataSource.read();
            return;
        };
        //        
        var state = JSON.parse(data);
        if ((!state) || ($.isEmptyObject(state))) {
            grid.dataSource.read();
            return;
        };
        //
        var options = grid.options;
        options.columns = state.columns;
        if (columnsHandler) {
            columnsHandler(options.columns);
        };
        options.dataSource.pageSize = state.pageSize;
        options.dataSource.sort = state.sort;
        options.dataSource.filter = state.filter;
        options.dataSource.group = state.group;
        grid.destroy();
        $(gridId).empty().kendoGrid(options);
    };

    function gridConfig(gridId) {
        var grid = $(gridId).data("kendoGrid");
        var dataSource = grid.dataSource;
        var config = {
            columns: grid.columns,
            pageSize: dataSource.pageSize(),
            sort: dataSource.sort(),
            filter: dataSource.filter(),
            group: dataSource.group()
        };
        return config;
    };

    function sendNotification(title, options, clickHandler) {
        // Проверим, поддерживает ли браузер HTML5 Notifications
        if (!("Notification" in window)) {
            alert('Ваш браузер не поддерживает HTML Notifications, его необходимо обновить.');
        }

            // Проверим, есть ли права на отправку уведомлений
        else if (Notification.permission === "granted") {
            // Если права есть, отправим уведомление
            var notification = new Notification(title, options);

            //function clickHandler() { alert('Пользователь кликнул на уведомление'); }
            if (clickHandler)
                notification.onclick = clickHandler;
        }

        // Если прав нет, пытаемся их получить
        else if (Notification.permission !== 'denied') {
        // !!!
        //else if (1 == 1) {
            Notification.requestPermission(function (permission) {
                // Если права успешно получены, отправляем уведомление
                if (permission === "granted") {
                    var notification = new Notification(title, options);
                } else {
                    alert('Вы запретили показывать уведомления'); // Юзер отклонил наш запрос на показ уведомлений
                }
            });
        } else {
            // Пользователь ранее отклонил наш запрос на показ уведомлений
            // В этом месте мы можем, но не будем его беспокоить. Уважайте решения своих пользователей.
        }
    };

    function clearCache() {
        $.ajax({
            type: "POST",
            url: "CabCacheClear",                        
            success: function (response) {
                alert('Кэш очищен')
            },
            error: function (response) {
                alert('Ошибка при очистке кэша');                
            }
        });        
    };

    function formatTextToHTML(text) {
        var regexp1 = new RegExp('\n', 'g');
        var regexp2 = new RegExp(' ', 'g');
        return text.replace(regexp1, '<br>');/*.replace(regexp2, '&nbsp;');*/
    };