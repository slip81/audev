﻿function OrderList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#orderListGrid', 0);
    }
}

var orderList = null;

$(function () {
    orderList = new OrderList();
    orderList.init();
});
