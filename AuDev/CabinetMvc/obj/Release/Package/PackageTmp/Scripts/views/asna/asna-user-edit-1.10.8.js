﻿function AsnaUserEdit() {
    _this = this;

    this.init = function () {
        //
        $("form").kendoValidator();
        //
        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            var curr_workplace_id = null;
            var selectedItem = $('#workplaceDdl').data('kendoDropDownList').dataItem();
            if ((selectedItem) && (selectedItem.id)) {
                curr_workplace_id = selectedItem.id;
            }
            $('#workplace_id').val(curr_workplace_id);
            if (model_asna_user_id > 0) {
                $('#is_active').val($('#chbIsActive').is(':checked'));
            };

            $('#sch_full_start_time_type_id').val($('#fullStartTimeDdl').data('kendoDropDownList').value());
            $('#sch_full_end_time_type_id').val($('#fullEndTimeDdl').data('kendoDropDownList').value());
            $('#sch_full_interval_type_id').val($('#fullIntervalDdl').data('kendoDropDownList').value());
            $('#sch_ch_start_time_type_id').val($('#chStartTimeDdl').data('kendoDropDownList').value());
            $('#sch_ch_end_time_type_id').val($('#chEndTimeDdl').data('kendoDropDownList').value());
            $('#sch_ch_interval_type_id').val($('#chIntervalDdl').data('kendoDropDownList').value());

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
        //
        $('#btnTaskAdd').click(function (e) {
            curr_task_id = null;
            $('#winAsnaTask').data('kendoWindow').center().open();
        });
        //        
        $('#btnWinAsnaTaskOk').click(function (e) {
            var dataItem = $('#waRequestTypeDdl').data('kendoDropDownList').dataItem();
            $('#winAsnaTask').data('kendoWindow').close();
            if ((dataItem) && (dataItem.request_type_id)) {
                if (curr_task_id) {
                    taskEdit(curr_task_id, dataItem.request_type_id);
                } else {
                    taskAdd(dataItem.request_type_id);
                };
            } else {
                alert('Не выбрано значение');
            };
        });
        //        
        $('#btnWinAsnaTaskCancel').click(function (e) {
            $('#winAsnaTask').data('kendoWindow').close();
        });
        //
        $('#btnGetStoreInfo').click(function (e) {
            if (!confirm("Создать задание на запрос в АСНА ?")) {
                return;
            }
            taskAdd(2, function () {
                alert('Задание создано');
            })
        });
        //
        $('#btnGetGoodsLinks').click(function (e) {
            if (!confirm("Создать задание на запрос в АСНА ?")) {
                return;
            }            
            taskAdd(3, function () {
                alert('Задание создано');
            })
        });
        //
        $('#btnGetRemains').click(function (e) {
            if (!confirm("Создать задание на запрос в АСНА ?")) {
                return;
            }
            taskAdd(4, function () {
                alert('Задание создано');
            })
        });
        //
        $('#btnDelRemains').click(function (e) {
            if (!confirm("Создать задание на запрос в АСНА ?")) {
                return;
            }
            taskAdd(7, function () {
                alert('Задание создано');
            })
        });
    };

    this.onClientGridDataBound = function (e) {
        drawEmptyRow('#clientGrid');
    };

    this.onClientGridChange = function (e) {
        $('#salesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.getSalesDdlData = function (e) {
        var curr_client_id = null;
        if (model_asna_user_id) {
            curr_client_id = model_client_id;
        } else {
            var selectedRow = $('#clientGrid').data('kendoGrid').select();
            if (selectedRow) {
                var dataItem = $('#clientGrid').data('kendoGrid').dataItem(selectedRow);
                if (dataItem) {
                    curr_client_id = dataItem.id;
                }
            }
        }
        return { client_id: curr_client_id };
    };

    this.onSalesDdlChange = function (e) {
        if (model_asna_user_id) {
            return;
        }
        $('#workplaceDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onSalesDdlDataBound = function (e) {
        if (model_asna_user_id) {
            return;
        }
        $('#salesDdl').data('kendoDropDownList').select(0);
        $('#workplaceDdl').data('kendoDropDownList').dataSource.read();
    };

    this.getWorkplaceDdlData = function (e) {
        var curr_sales_id = null;
        if (model_asna_user_id) {
            curr_sales_id = model_sales_id;
        } else {
            var dataItem = $('#salesDdl').data('kendoDropDownList').dataItem();
            if ((dataItem) && (dataItem.sales_id)) {
                curr_sales_id = dataItem.sales_id;
            }
        }
        return { sales_id: curr_sales_id };
    };

    this.onWorkplaceDdlDataBound = function (e) {
        if (model_asna_user_id) {
            return;
        }
        $('#workplaceDdl').data('kendoDropDownList').select(0);
    };    

    this.onTaskGridDataBound = function (e) {
        drawEmptyRow('#taskGrid');
        $('.k-grid-btnTaskEdit').children('span').addClass('k-icon k-edit');
        $('.k-grid-btnTaskDel').children('span').addClass('k-icon k-delete');
    };

    this.getDataTaskGrid = function (e) {
        var curr_workplace_id = null;
        if (model_asna_user_id) {
            curr_workplace_id = model_workplace_id;
        } else {
            var dataItem = $('#workplaceDdl').data('kendoDropDownList').dataItem();
            if ((dataItem) && (dataItem.id)) {
                curr_workplace_id = dataItem.id;
            }
        }
        return { workplace_id: curr_workplace_id };
    };

    this.onBtnTaskEditClick = function (e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        if ((dataItem) && (dataItem.request_type_id)) {
            if (!dataItem.is_outer) {
                alert('Нельзя изменить это задание');
                return;
            };
            curr_task_id = dataItem.task_id;
            $('#winAsnaTask').data('kendoWindow').center().open();
            $('#waRequestTypeDdl').data('kendoDropDownList').value(dataItem.request_type_id);
        } else {
            alert('Не найдено задание');
        };
    };

    this.onBtnTaskDelClick = function (e) {
        e.preventDefault();
        if (!confirm("Отменить задание ?")) {
            return;
        }
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        if ((dataItem) && (dataItem.task_id)) {
            taskDel(dataItem.task_id);
        } else {
            alert('Не найдено задание');
        };
    };

    this.getDataLogGrid = function (e) {        
        return { asna_user_id: model_asna_user_id };
    };

    this.onLogGridDataBound = function (e) {
        drawEmptyRow('#logGrid');
    };

    this.onBtnCheckStateClick = function (task_id) {
        showInfoWindow('#info-templ', 'Статус', function () {
            $('.confirm-text').text('Проверка статуса...');
            $.ajax({
                url: 'AsnaTaskCheckState',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ user_id: model_asna_user_id, task_id: task_id }),
                success: function (response) {
                    if ((response == null) || (response.err)) {                        
                        $('.confirm-text').text(response != null && response.mess ? response.mess : 'Ошибка при проверке статуса задания');
                    } else {
                        $('.confirm-text').text(response.mess);
                    };
                },
                error: function (response) {
                    alert('Ошибка при попытке проверки статуса задания');
                }
            });
        });
    };

    // private
    function taskAdd(curr_request_type_id, successHandler) {
        $.ajax({
            url: 'AsnaTaskAdd',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ asna_user_id: model_asna_user_id, request_type_id: curr_request_type_id }),
            success: function (response) {                
                if ((response == null) || (response.err)) {
                    alert(response != null && response.err ? response.err : 'Ошибка при сохранении задания');
                } else {                    
                    if (successHandler) {
                        successHandler();
                    };
                    refreshGrid('#taskGrid');
                };
            },
            error: function (response) {
                alert('Ошибка при попытке сохранения задания');
            }
        });
    }

    function taskEdit(curr_task_id, curr_request_type_id) {
        $.ajax({
            url: 'AsnaTaskEdit',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ asna_user_id: model_asna_user_id, task_id: curr_task_id, request_type_id: curr_request_type_id }),
            success: function (response) {
                if ((response == null) || (response.err)) {
                    alert(response != null && response.err ? response.err : 'Ошибка при сохранении задания');
                } else {
                    refreshGrid('#taskGrid');
                };
            },
            error: function (response) {
                alert('Ошибка при попытке сохранения задания');
            }
        });
    }

    function taskDel(curr_task_id) {
        $.ajax({
            url: 'AsnaTaskDel',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ asna_user_id: model_asna_user_id, task_id: curr_task_id }),
            success: function (response) {
                if ((response == null) || (response.err)) {
                    alert(response != null && response.err ? response.err : 'Ошибка при отмене задания');
                } else {
                    refreshGrid('#taskGrid');
                };
            },
            error: function (response) {
                alert('Ошибка при попытке отмены задания');
            }
        });
    }
}

var asnaUserEdit = null;
var curr_task_id = null;
$(function () {
    asnaUserEdit = new AsnaUserEdit();
    asnaUserEdit.init();
});
