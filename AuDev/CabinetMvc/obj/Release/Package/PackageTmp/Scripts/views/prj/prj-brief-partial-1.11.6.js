﻿function PrjBriefPartial() {
    _this = this;

    this.init = function () {
        //
    }

    this.init_after = function () {
        if (prjBrief_inited) {
            return;
        }
        //
        $(curr_user_settings_grid_name).delegate("tbody>tr", "dblclick", function () {
            var dataItem = $(curr_user_settings_grid_name).data("kendoGrid").dataItem($(this));
            if (dataItem) {
                loadingDialog('#prj-loading', true);
                setTimeout(function () {
                    rightPaneContentType = 2;
                    leftPaneContentType = 8;
                    leftPaneContentType_main = null;
                    leftPaneContentType_child = null;
                    prjList.openPrjClaimEditPartial(dataItem.claim_id, null, null, dataItem.task_id);
                }, waitTimeout);
            };
        });
        //
        $(document).on('click', '#btn-prj-brief-apply-filter', function () {
            var grid = $(curr_user_settings_grid_name).data("kendoGrid")
            var ds = grid.dataSource;
            var currFilters = [];
            var currFilter = [];

            if (curr_user_settings_grid_name == '#prjBriefGrid') {
                currFilter = getMultiSelectFilters('#prjBriefPrjFilter', 'prj_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefProjectFilter', 'project_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefRespFilter', 'resp_user_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefExecFilter', 'exec_user_name_list', 'contains');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefPriorityFilter', 'priority_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefClaimTypeFilter', 'claim_type_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefClaimStageFilter', 'claim_stage_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };
            }
            else if (curr_user_settings_grid_name == '#prjBriefWorkGrid') {
                currFilter = getMultiSelectFilters('#prjBriefPrjFilter', 'claim_prj_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefProjectFilter', 'claim_project_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefRespFilter', 'claim_resp_user_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefExecFilter', 'exec_user_name', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefPriorityFilter', 'claim_priority_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefClaimTypeFilter', 'claim_type_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefClaimStageFilter', 'claim_stage_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };
            } else if (curr_user_settings_grid_name == '#prjBriefTaskGrid') {
                currFilter = getMultiSelectFilters('#prjBriefPrjFilter', 'claim_prj_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefProjectFilter', 'claim_project_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefRespFilter', 'claim_resp_user_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefExecFilter', 'user_state_user_name', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefPriorityFilter', 'claim_priority_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefClaimTypeFilter', 'claim_type_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };

                currFilter = getMultiSelectFilters('#prjBriefClaimStageFilter', 'claim_stage_id', 'eq');
                if (currFilter.length > 0) {
                    currFilters.push(
                        {
                            logic: "or",
                            filters: currFilter
                        });
                };
            }

            ds.filter({ logic: "and", filters: currFilters });
            togglePrjBriefFilter();
        });
        //
        $(document).on('click', '#btn-prj-brief-reset-filter', function () {
            clearPrjBriefFilter(false);
            togglePrjBriefFilter();
        });
        //
        prjBrief_inited = true;
    };

    this.reload_after = function () {
        prjBriefGridDataBinding_first = true;
        prjBriefGridDataBound_first = true;
        prjBrief_inited = false;
        //
        selected_grid_name = null;
    };

    this.onPrjBriefTypeFilterChange = function (e) {
        var dataItem = e.sender.dataItem();
        selected_grid_name = '#prjBriefGrid';
        if (dataItem && dataItem.brief_type_id) {
            if (dataItem.brief_type_id == 3) {
                selected_grid_name = '#prjBriefTaskGrid';
            } else if (dataItem.brief_type_id == 2) {
                selected_grid_name = '#prjBriefWorkGrid';
            }
        }        
        curr_user_settings_grid_name = selected_grid_name;
        //refreshGrid(curr_user_settings_grid_name);

        // !!!
        // _this.reload_after();
        prjBriefGridDataBinding_first = true;
        prjBriefGridDataBound_first = true;
        prjBrief_inited = false;        

        var item = $('#tsPrj li.k-state-active');
        var link = item.find('a.k-link').first();
        link.data('contentUrl', 'PrjBriefPartial?selected_brief_type_id=' + dataItem.brief_type_id);
        var tabStrip = $("#tsPrj").data('kendoTabStrip');        
        var content = $('#tsPrj .k-content.k-state-active').first();
        content.html('');        
        setTimeout(function () {            
            tabStrip.reload(item);
        }, 300);
    };

    this.getPrjBriefGridData = function (e) {
        // !!!
        /*
        if (prjBriefGridDataBinding_first) {
            return { brief_type_id: null, interval_type_id: null, date_cut_type_id: null };
        };
        */
        var curr_brief_type_id = null;
        var curr_brief_type_dataItem = $('#prjBriefTypeFilter').data('kendoDropDownList').dataItem();
        if ((curr_brief_type_dataItem) && (curr_brief_type_dataItem.brief_type_id)) {
            curr_brief_type_id = curr_brief_type_dataItem.brief_type_id;
        };
        // !!!
        /*
        var curr_interval_type_id = null;
        var curr_interval_type_dataItem = $('#prjBriefIntervalTypeFilter').data('kendoDropDownList').dataItem();
        if ((curr_interval_type_dataItem) && (curr_interval_type_dataItem.interval_type_id)) {
            curr_interval_type_id = curr_interval_type_dataItem.interval_type_id;
        };
        */
        var curr_date_beg = $('#prjBriefDateBegFilter').data('kendoDatePicker').value();
        var curr_date_end = $('#prjBriefDateEndFilter').data('kendoDatePicker').value();
        //
        var curr_date_cut_type_id = null;
        var curr_date_cut_type_dataItem = $('#prjBriefDateCutTypeFilter').data('kendoDropDownList').dataItem();
        if ((curr_date_cut_type_dataItem) && (curr_date_cut_type_dataItem.date_cut_type_id)) {
            curr_date_cut_type_id = curr_date_cut_type_dataItem.date_cut_type_id;
        };
        return { brief_type_id: curr_brief_type_id, date_beg: curr_date_beg, date_end: curr_date_end, date_cut_type_id: curr_date_cut_type_id };
    };

    this.onPrjBriefGridDataBinding = function (e) {
        if (prjBriefGridDataBinding_first) {            
            prjBriefGridDataBinding_first = false;            
            gridConfigApply(curr_user_settings_grid, curr_user_settings_grid_name);
        };        
    };

    this.onPrjBriefGridDataBound = function (e) {
        var grid = this;
        var kendoGrid = $(curr_user_settings_grid_name).data("kendoGrid");
        drawEmptyRow(curr_user_settings_grid_name);
        if (prjBriefGridDataBound_first) {
            prjBriefGridDataBound_first = false;
            //
            setGridMaxHeight(curr_user_settings_grid_name, 50);
            //
            setInterval(function () {                
                var checked = $('.prj-brief-auto-refresh').hasClass('k-state-active');
                if (checked) {
                    if (refreshFirstAttempt) {
                        refreshFirstAttempt = false;
                    } else {
                        refreshGrid(curr_user_settings_grid_name);
                    }
                }
            }, refreshInterval);
            //
            kendoGrid.table.on("click", ".k-grouping-row .k-i-collapse, .k-grouping-row .k-i-expand", function (e) {
                var row = $(this).closest("tr"),
                              groupKey = rowGroupKey(row, kendoGrid);

                if ($(this).hasClass("k-i-collapse")) {
                    prjBriefGroup_collapsed[groupKey] = false;
                }
                else {
                    prjBriefGroup_collapsed[groupKey] = true;
                }
            });
        };
        //        
        var groups = grid.dataSource.group();
        if (groups.length) {
            grid.tbody.children(".k-grouping-row").each(function () {
                var row = $(this),
                    groupKey = rowGroupKey(row, grid);
                if (prjBriefGroup_collapsed[groupKey]) {
                    grid.collapseRow(row);
                }
            });
        }        
        //
        refreshPrjBriefGridFooter();
    };    

    this.onBtnPrjBriefExcelClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Выгрузить список в Excel ?', function () {
            $(curr_user_settings_grid_name).data('kendoGrid').saveAsExcel();
        });
    };

    this.onBtnPrjBriefPrintClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Распечатать список ?', function () {
            printGrid();
        });        
    };

    this.onBtnPrjBriefExpandAllClick = function (e) {
        $(curr_user_settings_grid_name).find(".k-icon.k-i-expand").trigger("click");
    };

    this.onBtnPrjBriefCollapseAllClick = function (e) {
        $(curr_user_settings_grid_name).find(".k-icon.k-i-collapse").trigger("click");
    };

/*
    this.onPrjBriefIntervalTypeFilterChange = function (e) {
        refreshGrid(curr_user_settings_grid_name);
    };
*/

    this.onPrjBriefDateCutTypeFilterChange = function (e) {
        refreshGrid(curr_user_settings_grid_name);
    };    

    this.onPrjBriefDateBegFilterChange = function (e) {
        refreshGrid(curr_user_settings_grid_name);
    }; 

    this.onPrjBriefDateEndFilterChange = function (e) {
        refreshGrid(curr_user_settings_grid_name);
    }; 

    this.onPrjBriefToolbarToggle = function (e) {
        var str = e.checked ? '<span class="k-sprite k-icon k-i-clock"></span>Автообн. вкл' : '<span class="k-sprite k-icon k-warning"></span>Автообн. откл';
        e.target.html(str);
    };

    this.onPrjBriefGridExcelExport = function (e) {
        var today = new Date();        
        var rows = e.workbook.sheets[0].rows;
        rows.unshift({
            cells: [{ value: "СВОДКА от " + kendo.toString(today, "d"), textAlign: "center", colSpan: 7, bold: "true" }]
        });
    };

    this.onPrjBriefGridRequestEnd = function (e) {
        if (prjBriefGroups.length != this.group().length) {
            var dataSourceGroups = this.group(),
                length = prjBriefGroups.length;
            if (length > dataSourceGroups.length) {
                if (dataSourceGroups.length === 0) {
                    prjBriefGroup_collapsed = {};
                } else {
                    for (var key in prjBriefGroup_collapsed) {
                        if (key.indexOf(length - 1) === 0) {
                            prjBriefGroup_collapsed[key] = false;
                        }
                    }
                }
            }

            prjBriefGroups = this.group().slice(0);
        }
    };

    this.onBtnPrjBriefFilterClick = function (e) {        
        var config_action_type = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (config_action_type) {
            case 1:
                togglePrjBriefFilter();
                break;
            case 2:                
                clearPrjBriefFilter(true);
                break;
            case 3:                
                clearPrjBriefSort();
                break;
            default:
                break;
        };	
    };

    this.getSettings = function () {
        var settings = [];
        settings.push({ id: 'brief_type', val: $("#prjBriefTypeFilter").data("kendoDropDownList").value() });
        //settings.push({ id: 'brief_interval', val: $("#prjBriefIntervalTypeFilter").data("kendoDropDownList").value() });
        settings.push({ id: 'brief_date_beg', val: $('#prjBriefDateBegFilter').data('kendoDatePicker').value() });
        settings.push({ id: 'brief_date_end', val: $('#prjBriefDateEndFilter').data('kendoDatePicker').value() });
        settings.push({ id: 'brief_date_cut', val: $("#prjBriefDateCutTypeFilter").data("kendoDropDownList").value() });
        settings.push({ id: 'brief_auto_refresh', val: $('.prj-brief-auto-refresh').hasClass('k-state-active') ? 'true' : 'false' });        
        //
        settings.push({ id: 'brief_prj', val: JSON.stringify($("#prjBriefPrjFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'brief_project', val: JSON.stringify($("#prjBriefProjectFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'brief_resp_user', val: JSON.stringify($("#prjBriefRespFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'brief_exec_user', val: JSON.stringify($("#prjBriefExecFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'brief_priority', val: JSON.stringify($("#prjBriefPriorityFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'brief_claim_type', val: JSON.stringify($("#prjBriefClaimTypeFilter").data("kendoMultiSelect").value()) });
        settings.push({ id: 'brief_claim_stage', val: JSON.stringify($("#prjBriefClaimStageFilter").data("kendoMultiSelect").value()) });
        //
        settings.push({ id: 'brief_grid_name', val: curr_user_settings_grid_name });
        //
        return settings;
    };

    // private

    function printGrid() {
        var gridElement = $(curr_user_settings_grid_name),
            today = new Date(),
            printableContent = 'СВОДКА от ' + kendo.toString(today, 'd'),
            win = window.open('', '', 'width=800, height=500, resizable=1, scrollbars=1'),
            doc = win.document.open();

        var htmlStart =
                '<!DOCTYPE html>' +
                '<html>' +
                '<head>' +
                '<meta charset="utf-8" />' +
                '<title>Kendo UI Grid</title>' +
                //'<link href="http://kendo.cdn.telerik.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
                '<link href="/Content/kendo/2015.2.624/kendo.common.min.css" rel="stylesheet"/>' +
                '<style>' +
                'html { font: 11pt sans-serif; }' +
                '.k-grid { border-top-width: 0; }' +
                '.k-grid, .k-grid-content { height: auto !important; }' +
                '.k-grid-content { overflow: visible !important; }' +
                'div.k-grid table { table-layout: auto; width: 100% !important; }' +
                '.k-grid .k-grid-header th { border-top: 1px solid; }' +
                '.k-grouping-header, .k-grid-toolbar, .k-grid-pager > .k-link { display: none; }' +
                //'.k-grid-pager { display: none; }' + // optional: hide the whole pager
                '</style>' +
                '</head>' +
                '<body>';

        var htmlEnd =
                '</body>' +
                '</html>';

        printableContent += '<br/>';

        var gridHeader = gridElement.children('.k-grid-header');
        if (gridHeader[0]) {
            var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
            printableContent += gridElement
                .clone()
                    .children('.k-grid-header').remove()
                .end()
                    .children('.k-grid-content')
                        .find('table')
                            .first()
                                .children('tbody').before(thead)
                            .end()
                        .end()
                    .end()
                .end()[0].outerHTML;
        } else {
            printableContent += gridElement.clone()[0].outerHTML;
        }

        doc.write(htmlStart + printableContent + htmlEnd);
        doc.close();
        win.print();
    };

    function rowGroupKey(row, grid) {
        var next = row.nextUntil("[data-uid]").next(),
            item = grid.dataItem(next.length ? next : row.next()),
            groupIdx = row.children(".k-group-cell").length,
            groups = grid.dataSource.group(),
            field = grid.dataSource.group()[groupIdx].field,
            groupValue = item[field];
        return "" + groupIdx + groupValue;
    };

    function refreshPrjBriefGridFooter() {
        //var footer_html = 'Всего задач: ' + $(curr_user_settings_grid_name).data('kendoGrid').dataSource.aggregates().claim_name.count;
        var footer_html = 'Всего задач: ' + $(curr_user_settings_grid_name).data('kendoGrid').dataSource.total();
        var footer = $(curr_user_settings_grid_name + '>.k-grid-pager').find('span.prj-grid-footer');
        if (footer.length <= 0) {
            $(curr_user_settings_grid_name + ' span.k-pager-sizes').append('<span class="prj-grid-footer"></span>')
        }
        $(curr_user_settings_grid_name + '>.k-grid-pager span.prj-grid-footer').html(footer_html);
    };

    function togglePrjBriefFilter() {
        var filter = $('#prjBriefFilter').data('kendoPanelBar');
        var is_expanded = $('#prj-brief-filter-panel').hasClass('k-state-active');
        if (is_expanded) {
            filter.collapse($('#prj-brief-filter-panel'));
        } else {
            filter.expand($('#prj-brief-filter-panel'));
        };
    };

    function clearPrjBriefFilter(all) {
        var grid = $(curr_user_settings_grid_name).data("kendoGrid")
        var ds = grid.dataSource;
        var currFilters = [];

        $('#prjBriefPrjFilter').data('kendoMultiSelect').value([]);
        $('#prjBriefProjectFilter').data('kendoMultiSelect').value([]);
        $('#prjBriefRespFilter').data('kendoMultiSelect').value([]);
        $('#prjBriefExecFilter').data('kendoMultiSelect').value([]);        
        $('#prjBriefPriorityFilter').data('kendoMultiSelect').value([]);
        $('#prjBriefClaimTypeFilter').data('kendoMultiSelect').value([]);
        $('#prjBriefClaimStageFilter').data('kendoMultiSelect').value([]);

        if (all) {
            //
        };

        ds.filter({ logic: "and", filters: currFilters });
    };

    function clearPrjBriefSort() {
        clearSortGrid(curr_user_settings_grid_name);
        //
        $(curr_user_settings_grid_name).data('kendoGrid').dataSource.group([]);
    };
}

var prjBriefPartial = null;
var prjBrief_inited = false;
var prjBriefGridDataBinding_first = true;
var prjBriefGridDataBound_first = true;
var refreshFirstAttempt = true;
var refreshInterval = 300000; //5 min
//var refreshInterval = 10000; //10 sec
var prjBriefGroups = [];
var prjBriefGroup_collapsed = {};

$(function () {
    prjBriefPartial = new PrjBriefPartial();
    prjBriefPartial.init();
});
