﻿function AdminList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#userGrid', 0);

        $('#userGrid').on('click', '.chb-is_crm_user', function () {
            var checked = $(this).is(':checked');
            var grid = $('#userGrid').data().kendoGrid;
            var dataItem = grid.dataItem($(this).closest('tr'));
            dataItem.set('is_crm_user', checked);
            dataItem.dirty = true;
        });

        $('#userGrid').on('click', '.chb-is_executer', function () {
            var checked = $(this).is(':checked');
            var grid = $('#userGrid').data().kendoGrid;
            var dataItem = grid.dataItem($(this).closest('tr'));
            dataItem.set('is_executer', checked);
            dataItem.dirty = true;
        });

        $('#userGrid').on('click', '.chb-is_active', function () {
            var checked = $(this).is(':checked');
            var grid = $('#userGrid').data().kendoGrid;
            var dataItem = grid.dataItem($(this).closest('tr'));
            dataItem.set('is_active', checked);
            dataItem.dirty = true;
        });

        $('#btnShowAll').click(function (e) {
            $('#btnShowAll').addClass('btn-primary');
            $('#btnShowActive').removeClass('btn-primary');
            $('#btnShowNotActive').removeClass('btn-primary');

            var grid = $("#userGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            grid.dataSource.filter(_fltStatus);
        });

        $('#btnShowActive').click(function (e) {
            $('#btnShowAll').removeClass('btn-primary');
            $('#btnShowActive').addClass('btn-primary');
            $('#btnShowNotActive').removeClass('btn-primary');

            var grid = $("#userGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            _fltStatus.filters.push({ field: "is_active", operator: "eq", value: true });
            grid.dataSource.filter(_fltStatus);
        });

        $('#btnShowNotActive').click(function (e) {
            $('#btnShowAll').removeClass('btn-primary');
            $('#btnShowActive').removeClass('btn-primary');
            $('#btnShowNotActive').addClass('btn-primary');

            var grid = $("#userGrid").data("kendoGrid"),
                _fltStatus = { logic: "and", filters: [] };

            _fltStatus.filters.push({ field: "is_active", operator: "eq", value: false });
            grid.dataSource.filter(_fltStatus);
        });
    };    

    this.grid_error = function(e) {
        if (e.errors) {
            var message = "Ошибка:\n";
            $.each(e.errors, function (key, value) {
                if ('errors' in value) {
                    $.each(value.errors, function () {
                        message += this + "\n";
                    });
                }
            });
            alert(message);
            var grid = $("#userGrid").getKendoGrid();
            grid.cancelChanges();
        }
    };

    this.onUserGridDataBound = function (e) {        
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if (dataItem.get("is_active") != true) {
                row.addClass("k-error-colored");
            };
        };
        if (firstDataBound) {
            firstDataBound = false;
            var isAdd = request_add;
            if (isAdd == '1') {
                $('.k-grid-add').trigger('click');
            };
        };
    };
   
}

var adminList = null;

var firstDataBound = true;

$(function () {
    adminList = new AdminList();
    adminList.init();
});
