﻿function ServiceLogList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#serviceLogGrid', 50);

        $('#chb-error-only').on('change', function () {
            refreshGrid('#serviceLogGrid');
        });
    };

    this.onServiceLogToolbarToggle = function (e) {
        refreshGrid('#serviceLogGrid');
    };
   
    this.onServiceLogGridDataBound = function(e) {
        drawEmptyRow('#serviceLogGrid');
    };

    this.getScope = function (e) {
        var curr_scope = $('#serviceLogToolbar>.k-button-group>[data-group="service-log-scope-group"].k-toggle-button.k-state-active').attr('data-scope');
        var curr_error_only = $('#chb-error-only').is(':checked');
        return { scope: curr_scope, error_only: curr_error_only };
    };
}

var serviceLogList = null;

$(function () {
    serviceLogList = new ServiceLogList();
    serviceLogList.init();
});
