﻿function AsnaOrderStateType() {
    _this = this;

    this.init = function () {        
        setGridMaxHeight('#asnaOrderStateTypeGrid', 50);
    };

    this.onAsnaOrderStateTypeGridDataBound = function (e) {
        drawEmptyRow('#asnaOrderStateTypeGrid');        
    };

}

var asnaOrderStateType = null;
$(function () {
    asnaOrderStateType = new AsnaOrderStateType();
    asnaOrderStateType.init();
});
