﻿function PrjSpravPartial() {
    _this = this;

    this.init = function () {
        //
        $(document).on('click', '.btn-project-new-release', function () {
            //alert('project_name = ' + $(this).attr('data-project-name'));
            curr_project_name = $(this).attr('data-project-name');
            $('#txtNewReleaseNum').val('');
            $('#txtNewReleaseNum').removeClass('input-validation-error');
            $('#winProjectNewRelease').data('kendoWindow').center().open();
        });        
        //
        $(document).on('click', '#btnProjectNewReleaseOk', function () {            
            curr_new_release_num = $('#txtNewReleaseNum').val();
            if (!curr_new_release_num) {
                $('#txtNewReleaseNum').addClass('input-validation-error');
                return;
            }
            $('#txtNewReleaseNum').removeClass('input-validation-error');
            $('#winProjectNewRelease').data('kendoWindow').close();
            //
            showConfirmWindow('#confirm-templ', 'Если вы не уверены своих действиях - нажмите кнопку Отмена ! После нажатия на ОК операцию отменить будет невозможно', function () {
                startNewRelease();
            });            
        }); 
        //
        $(document).on('click', '#btnProjectNewReleaseCancel', function () {
            $('#winProjectNewRelease').data('kendoWindow').close();
            curr_project_name = null;
            curr_new_release_num = null;
        });
    };

    this.init_after = function () {
        if (prjSprav_inited) {
            return;
        }
        //
        prjSprav_inited = true;
    };

    this.onProjectVersionGridDataBound = function (e) {
        drawEmptyRow('#projectVersionGrid');
        if (projectVersionGridDataBound_first) {
            projectVersionGridDataBound_first = false;            
            resizeGrid('#projectVersionGrid', 60);
        };        
    };

    this.onPrjSpravGridDataBound = function (e) {
        drawEmptyRow('#prjSpravGrid');
        if (prjSpravGridDataBound_first) {
            prjSpravGridDataBound_first = false;
            resizeGrid('#prjSpravGrid', 60);
        };        
    };

    this.onPriorityGridDataBound = function (e) {
        drawEmptyRow('#priorityGrid');      
    };

    this.onClaimTypeGridGridDataBound = function (e) {
        drawEmptyRow('#claimTypeGrid');      
    };

    this.onClaimStageGridDataBound = function (e) {
        drawEmptyRow('#claimStageGrid');
    };

    this.onProjectSpravDdlDataBound = function (e) {
        setTimeout(function () { refreshGrid('#moduleSpravGrid'); refreshGrid('#crmProjectVersionSpravGrid'); }, 300);
    };

    this.onProjectSpravDdlChange = function (e) {
        refreshGrid('#moduleSpravGrid');
        refreshGrid('#crmProjectVersionSpravGrid');
    };

    this.onModuleSpravGridDataBound = function (e) {
        drawEmptyRow('#moduleSpravGrid');
        if (moduleSpravGridDataBound_first) {
            moduleSpravGridDataBound_first = false;
            resizeGrid('#moduleSpravGrid', 200);
        };
        //$("#moduleSpravGrid").data("kendoGrid").select("tr:eq(1)");
    };

    this.onModuleSpravGridChange = function (e) {
        refreshGrid('#modulePartSpravGrid');
        //refreshGrid('#moduleVersionSpravGrid');
    };

    this.onModulePartSpravGridDataBound = function (e) {
        drawEmptyRow('#modulePartSpravGrid');
        if (modulePartSpravGridDataBound_first) {
            modulePartSpravGridDataBound_first = false;
            resizeGrid('#modulePartSpravGrid', ($('#prj-main-pane-left').height() / 2) + 80);
        };
    };

    this.onCrmProjectVersionSpravGridDataBound = function (e) {
        drawEmptyRow('#crmProjectVersionSpravGrid');
        if (crmProjectVersionSpravGridDataBound_first) {
            crmProjectVersionSpravGridDataBound_first = false;
            resizeGrid('#crmProjectVersionSpravGrid', ($('#prj-main-pane-left').height() / 2) + 80);
        };
    };
    
    this.getProjectId = function (e) {        
        return { parent_project_id: $('#projectSpravDdl').data('kendoDropDownList').value() };
    };

    this.getModuleId = function (e) {        
        return { parent_module_id: $('#moduleSpravGrid').data('kendoGrid').dataItem($('#moduleSpravGrid').data('kendoGrid').select()).module_id };
    };


    // private

    function resizeGrid(grid_name, add) {
        setGridMaxHeight_relative(grid_name, '#prj-main-pane-left', add);
    };

    function startNewRelease() {
        //
        //alert('startNewRelease for ' + curr_project_name + ' release num ' + curr_new_release_num);
        //
                loadingDialog('#prj-loading', true);
                $.ajax({
                    url: '/PrjProjectVersionNewRelease',
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify({ project_name: curr_project_name, new_release_num: curr_new_release_num }),
                    complete: function(res) {
                        loadingDialog('#prj-loading', false);
                        curr_project_name = null;
                        curr_new_release_num = null;
                    },
                    success: function (res) {
                        if ((!res) || (res.err)) {                            
                            alert(res && res.mess ? res.mess : 'Ошибка при создании нового релиза');
                        } else {
                            refreshGrid('#projectVersionGrid');
                        };
                    },
                    error: function (res) {                        
                        alert('Ошибка создания нового релиза');
                    }
                });
    };

}

var prjSpravPartial = null;
var prjSprav_inited = false;
var projectVersionGridDataBound_first = true;
var prjSpravGridDataBound_first = true;
var moduleSpravGridDataBound_first = true;
var modulePartSpravGridDataBound_first = true;
var crmProjectVersionSpravGridDataBound_first = true;
var curr_project_name = null;
var curr_new_release_num = null;

$(function () {
    prjSpravPartial = new PrjSpravPartial();
    prjSpravPartial.init();
});
