﻿function ClientMaxVersion() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#clientMaxVersionGrid', 50);
    };
    
    this.onClientMaxVersionGridDataBound = function (e) {
        drawEmptyRow('#clientMaxVersionGrid');
    };
}

var clientMaxVersion = null;
$(function () {
    clientMaxVersion = new ClientMaxVersion();
    clientMaxVersion.init();
});
