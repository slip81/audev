﻿function CardList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#сardListGrid', 0);

        $('#btnCardAdd').children('span').first().addClass('k-icon k-i-plus');
        $('#btnCardBatchAdd').children('span').first().addClass('k-icon k-i-plus');
        $('#btnCardBatchOperations').children('span').first().addClass('k-icon k-i-clock');
        $('#btnCardBatchOperationsLog').children('span').first().addClass('k-icon k-i-hbars');
    };

    this.cardsReadData = function() {
        return {
            card_search_string: request_search,
            card_adv_search_string: request_advanced_search
        };
    };

    this.onCardListGridDataBound = function(e) {
        drawEmptyRow('#сardListGrid');
    };

    this.cardNumFilter = function(control) {
        $(control).kendoNumericTextBox({ "format": "#", "decimals": 0 });
    };
}

var cardList = null;

$(function () {
    cardList = new CardList();
    cardList.init();
});
