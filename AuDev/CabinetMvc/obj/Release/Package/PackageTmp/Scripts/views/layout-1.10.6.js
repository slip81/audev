﻿function Layout() {
    _this = this;

    this.init = function () {
        $(document).bind("keypress", function (e) {
            if (e.keyCode == 13) {
                var visibleWindow = $(".k-window:visible > .k-window-content");
                if (visibleWindow.length)
                    visibleWindow.find('.confirm').trigger('click');
            }
        });
        //
        setInterval(function () {
            if (!forbidCheckAuthorization) {
                isAuthorizedGlobal();
            }
        }, 40000); // 40 sec
        //
        client_id_before_open = currUser_CabClientId;
        client_id_after_close = currUser_CabClientId;
        //
        $('#btn-client-change').click(function (e) {
            e.preventDefault();
            $("#winChangeBusiness").data("kendoWindow").open().center();
        });
        //
        $('#cab-header-img').dblclick(function (e) {            
            imgCycle(this, img_src_list);
        });
    };

    this.onCBGridDataBound = function(e) {
        this.table.on("dblclick", " tbody > tr", gridRowDblClick);
    };

    this.onWindowDeactivate = function (e) {
        if (client_id_before_open != client_id_after_close) {
            location.href = '/ChangeBusiness?id=' + client_id_after_close;
        };
    };

    this.onWindowActivate = function(e) {
        $("#changeBusinessGrid").getKendoGrid().dataSource.read();
    };

    function isAuthorizedGlobal() {
        $.ajax({
            type: "POST",
            url: '/IsAuthorized',
            data: '',
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if (response != null) {
                    if (!response.is_auth) {
                        forbidCheckAuthorization = true;
                        showNotAuthorizedWindow();
                    } else {
                        // !!!
                        if ((response.notify) && (response.notify.length)) {
                            var time = 1000;
                            $.each(response.notify, function (key, value) {
                                setTimeout(function () {
                                    sendNotification(value.title, {
                                        body: value.message,
                                        icon: '/Images/au_logo.jpg',
                                        //icon: '/Images/au_logo32.png',
                                        dir: 'auto'
                                    }, null);
                                }, time);
                                time += 1000;
                            });
                        }
                    };
                };
            },
            error: function (response) {
                //
            }
        });
    };

    function gridRowDblClick(e) {
        var grid = $("#changeBusinessGrid").data("kendoGrid");
        client_id_after_close = grid.dataItem($(this))['id'];
        var win_data = $("#winChangeBusiness").data("kendoWindow")
        win_data.close();
    };

    function showNotAuthorizedWindow() {
        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: 'Внимание',
            draggable: false,
            resizable: false,
            modal: true,
            iframe: false,
            deactivate: function () {
                this.destroy();
            }
        });

        kendoWindow.data("kendoWindow").content($('#not-auth-templ').html());
        kendoWindow.data("kendoWindow").center().open();
        kendoWindow.find(".confirm")
                .click(function () {
                    kendoWindow.data("kendoWindow").close();
                    window.location = url_Login;
                })
             .end();
    };
};

var layout = null;

$(function () {
    layout = new Layout();
    layout.init();
});
