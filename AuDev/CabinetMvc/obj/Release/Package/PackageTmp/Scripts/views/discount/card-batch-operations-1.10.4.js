﻿function CardBatchOperations() {
    _this = this;

    this.init = function () {
        $(document).on('click', '#chbTransSum', function () {
            if ($('#chbTransSum:checked').length > 0) {
                $('#edTransSum').prop('disabled', false);
                $('#transSumAdd_').prop('disabled', false);
                $('#transSumReplace_').prop('disabled', false);
            }
            else {
                $('#edTransSum').prop('disabled', true);
                $('#transSumAdd_').prop('disabled', true);
                $('#transSumReplace_').prop('disabled', true);
            }
        });

        $(document).on('click', '#chbDiscPercent', function () {
            if ($('#chbDiscPercent:checked').length > 0) {
                $('#edDiscPercent').prop('disabled', false);
                $('#discPercentAdd_').prop('disabled', false);
                $('#discPercentReplace_').prop('disabled', false);
            }
            else {
                $('#edDiscPercent').prop('disabled', true);
                $('#discPercentAdd_').prop('disabled', true);
                $('#discPercentReplace_').prop('disabled', true);
            }
        });

        $(document).on('click', '#chbBonusPercent', function () {
            if ($('#chbBonusPercent:checked').length > 0) {
                $('#edBonusPercent').prop('disabled', false);
                $('#bonusPercentAdd_').prop('disabled', false);
                $('#bonusPercentReplace_').prop('disabled', false);
            }
            else {
                $('#edBonusPercent').prop('disabled', true);
                $('#bonusPercentAdd_').prop('disabled', true);
                $('#bonusPercentReplace_').prop('disabled', true);
            }
        });

        $(document).on('click', '#chbBonusSum', function () {
            if ($('#chbBonusSum:checked').length > 0) {
                $('#edBonusSum').prop('disabled', false);
                $('#bonusSumAdd_').prop('disabled', false);
                $('#bonusSumReplace_').prop('disabled', false);
            }
            else {
                $('#edBonusSum').prop('disabled', true);
                $('#bonusSumAdd_').prop('disabled', true);
                $('#bonusSumReplace_').prop('disabled', true);
            }
        });

        $(document).on('click', '#chbMoneySum', function () {
            if ($('#chbMoneySum:checked').length > 0) {
                $('#edMoneySum').prop('disabled', false);
                $('#moneySumAdd_').prop('disabled', false);
                $('#moneySumReplace_').prop('disabled', false);
            }
            else {
                $('#edMoneySum').prop('disabled', true);
                $('#moneySumAdd_').prop('disabled', true);
                $('#moneySumReplace_').prop('disabled', true);
            }
        });

        $(document).on('click', '#chbMess', function () {
            if ($('#chbMess:checked').length > 0) {
                $('#mess_add_value').prop('disabled', false);
                $('#messAdd_').prop('disabled', false);
                $('#messReplace_').prop('disabled', false);
            }
            else {
                $('#mess_add_value').prop('disabled', true);
                $('#messAdd_').prop('disabled', true);
                $('#messReplace_').prop('disabled', true);
            }
        });

        $("#btnSubmit").click(function (e) {
            e.preventDefault();

            var checkedIds_cnt = 0;
            $.each(checkedIds, function (key, value) {
                if (value) {
                    checkedIds_cnt++;
                }
            });
            if (checkedIds_cnt > 100) {
                alert('Максимально допустимое для выделения кол-во строк = 100 !');
                return false;
            };

            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Внимание ! Эту операцию невозможно будет отменить ! Подтверждаете действие ?",
                resizable: false,
                modal: true,
                width: "650px",
                height: "80px"
            });

            kendoWindow.data("kendoWindow")
                .content($("#batch-op-confirmation").html())
                .center().open();

            kendoWindow
                .find(".batch-op-confirm,.batch-op-cancel")
                    .click(function () {
                        if ($(this).hasClass("batch-op-confirm")) {
                            kendoWindow.data("kendoWindow").close();
                            $('#btnSubmit').attr('disabled', true);
                            var batch = getBatch();
                            $.post("Card/CardBatchOperations", batch, function (data) {
                                location.href = '/CardBatchOperationsLog';
                            });
                        }
                        kendoWindow.data("kendoWindow").close();
                    })
                 .end()
        });
    };    

    this.grid_error = function(e) {
        if (e.errors) {
            var message = "Ошибка:\n";            
            $.each(e.errors, function (key, value) {
                if ('errors' in value) {
                    $.each(value.errors, function () {
                        message += this + "\n";
                    });
                }
            });            
            alert(message);
            var grid = $("#сardListGrid").data("kendoGrid");
            grid.cancelChanges();
        }
    };

    this.onGridDataBound = function(e) {
        $("#masterCheckBox").prop('checked', false);

        var grid = $("#сardListGrid").data("kendoGrid");
        $(grid.tbody).on("click", ".checkbox", function (e) {

            var checked = this.checked,
                row = $(this).closest("tr"),
                grid = $("#сardListGrid").data("kendoGrid"),
                dataItem = grid.dataItem(row);

            checkedIds[dataItem.card_id_str] = checked;
            if (checked) {                
                row.addClass("k-state-selected");
            } else {
                row.removeClass("k-state-selected");
            }
        });

        var view = this.dataSource.view();
        for (var i = 0; i < view.length; i++) {
            if (checkedIds[view[i].card_id_str]) {
                this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                    .addClass("k-state-selected")
                    .find(".checkbox")
                    .attr("checked", "checked");
            }
        }
    };

    this.checkAll = function(ele) {
        var state = $(ele).is(':checked');
        var grid = $("#сardListGrid").data("kendoGrid");
        var view = grid.dataSource.view();

        for (var i = 0; i < view.length; i++) {
            var dataRow = view[i];
            var elementRow = grid.table.find(".checkbox")[i];
            if (elementRow != null) {
                var checked = elementRow.checked,
                    row = $(elementRow).closest("tr"),
                    dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));

                checkedIds[dataItem.card_id_str] = state;
                if (state) {                    
                    elementRow.checked = true;
                    row.addClass("k-state-selected");
                } else {
                    elementRow.checked = false;
                    row.removeClass("k-state-selected");
                }
            }
        };
    };

    function getBatch() {
        var use_sum1 = ($('#chbTransSum:checked').length > 0) ? 'true' : 'false';
        var sum1 = $("#edTransSum").val();
        var sum1_mode = ($('#transSumAdd_:checked').length > 0) ? 0 : 1;

        var use_sum2 = ($('#chbDiscPercent:checked').length > 0) ? 'true' : 'false';
        var sum2 = $("#edDiscPercent").val();
        var sum2_mode = ($('#discPercentAdd_:checked').length > 0) ? 0 : 1;

        var use_sum3 = ($('#chbBonusPercent:checked').length > 0) ? 'true' : 'false';
        var sum3 = $("#edBonusPercent").val();
        var sum3_mode = ($('#bonusPercentAdd_:checked').length > 0) ? 0 : 1;

        var use_sum4 = ($('#chbBonusSum:checked').length > 0) ? 'true' : 'false';
        var sum4 = $("#edBonusSum").val();
        var sum4_mode = ($('#bonusSumAdd_:checked').length > 0) ? 0 : 1;

        var use_sum5 = ($('#chbMoneySum:checked').length > 0) ? 'true' : 'false';
        var sum5 = $("#edMoneySum").val();
        var sum5_mode = ($('#moneySumAdd_:checked').length > 0) ? 0 : 1;

        var use_mess1 = ($('#chbMess:checked').length > 0) ? 'true' : 'false';
        var mess1 = $("#mess_add_value").val();
        var mess1_mode = ($('#messAdd_:checked').length > 0) ? 0 : 1;

        var change_status = ($('#chbCardStatus:checked').length > 0) ? 'true' : 'false';
        var combo_status = $("#cardStatusList").data("kendoDropDownList");
        var status_id = combo_status.dataItem()["card_status_id"];

        var change_card_type = ($('#chbCardType:checked').length > 0) ? 'true' : 'false';
        var combo_card_type = $("#cardTypeList").data("kendoDropDownList");
        var card_type_id = combo_card_type.dataItem()["card_type_id"];

        var del = ($('#chbDelCards:checked').length > 0) ? 'true' : 'false';

        var list_mode = ($('#cardListCurrentPage_:checked').length > 0) ? 0 : (($('#cardListAllPages_:checked').length > 0) ? 1 : (($('#cardListAllCards_:checked').length > 0) ? 2 : 3));

        var card_grid = $("#сardListGrid").getKendoGrid();

        var id_list = '';
        $.each(checkedIds, function (key, value) {
            if (value) {
                id_list = id_list + key + ',';
            }
        });

        return {
            trans_sum_add: use_sum1, trans_sum_add_value: sum1, trans_sum_add_mode: sum1_mode,
            disc_percent_add: use_sum2, disc_percent_add_value: sum2, disc_percent_add_mode: sum2_mode,
            bonus_percent_add: use_sum3, bonus_percent_add_value: sum3, bonus_percent_add_mode: sum3_mode,
            bonus_add: use_sum4, bonus_add_value: sum4, bonus_add_mode: sum4_mode,
            money_add: use_sum5, money_add_value: sum5, money_add_mode: sum5_mode,
            mess_add: use_mess1, mess_add_value: mess1, mess_add_mode: mess1_mode,
            change_card_status: change_status, card_status_id: status_id,
            change_card_type: change_card_type, card_type_id: card_type_id,
            del_cards: del,
            card_list_mode: list_mode,
            Message: '',
            GridFilterString: kendo.stringify({ filter: card_grid.dataSource.filter() }),
            GridSortString: kendo.stringify({ sort: card_grid.dataSource.sort() }),
            GridPageSize: card_grid.dataSource.pageSize(),
            GridPageNumber: card_grid.dataSource.page(),
            card_id_list: id_list
        }
    };

    function count(obj) {
        var count = 0;
        for (var prop in obj) {
            if ((obj.hasOwnProperty(prop)) && (prop == "true")) {
                ++count;
            }
        }
        return count;
    };
}

var cardBatchOperations = null;
var checkedIds = {};

$(function () {
    cardBatchOperations = new CardBatchOperations();
    cardBatchOperations.init();
});
