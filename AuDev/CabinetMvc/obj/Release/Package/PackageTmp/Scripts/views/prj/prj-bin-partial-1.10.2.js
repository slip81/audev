﻿function PrjBinPartial() {
    _this = this;

    this.init = function () {
        //
    };

    this.init_after = function () {
        if (prjBin_inited) {
            return;
        }
        //
        prjBin_inited = true;
        //
        $("#prjBinGrid").delegate("tbody>tr", "dblclick", function () {
            var dataItem = $("#prjBinGrid").data("kendoGrid").dataItem($(this));
            if (dataItem) {
                loadingDialog('#prj-loading', true);
                setTimeout(function () {
                    rightPaneContentType = 2;
                    leftPaneContentType = 12;
                    leftPaneContentType_main = null;
                    leftPaneContentType_child = null;
                    prjList.openPrjClaimEditPartial(dataItem.claim_id);
                }, waitTimeout);
            };
        });
    };

    this.onPrjBinSearchKeyup = function (e) {
        if (e.keyCode == 13) {
            refreshGrid('#prjBinGrid');
        };
    };

    this.getPrjBinGridData = function (e) {
        return { search: $('#prj-bin-search').val(), };
    };

    this.onPrjBinGridDataBound = function (e) {
        drawEmptyRow('#prjBinGrid');
        if (prjBinGridDataBound_first) {
            prjBinGridDataBound_first = false;
            setGridMaxHeight('#prjBinGrid', 45);
        };
    };
}

var prjBinPartial = null;
var prjBin_inited = false;
var prjBinGridDataBound_first = true;

$(function () {
    prjBinPartial = new PrjBinPartial();
    prjBinPartial.init();
});
