﻿function CardEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();
        //
        $('.attr-change').bind('input propertychange', _this.onAttrChanged)        

        if (no_manual_change === true) {
            $('#btnCardStatusChange').attr('disabled', true);
        };

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#cardTypeDropDownList").getKendoDropDownList();
            if (ddl_card_type_visible) {
                $("#curr_card_type_id").val(combo1.dataItem()["card_type_id"]);
            } else {
                $("#curr_card_type_id").val(model_curr_card_type_id);
            };
            var combo2 = $("#cardStatusDropDownList").getKendoDropDownList();
            if (ddl_card_status_visible) {
                $("#curr_card_status_id").val(combo2.dataItem()["card_status_id"]);
            } else {
                $("#curr_card_status_id").val(model_curr_card_status_id);
            };
            //var combo3 = $("#cardClientDropDownList").getKendoDropDownList();
            if (ddl_card_client_visible) {
                $("#curr_client_id").val(curr_card_holder_id);
                /*
                if (combo3.dataItem()) {
                    $("#curr_client_id").val(combo3.dataItem()["client_id_str"]);
                } else {
                    $("#curr_client_id").val(null);
                };
                */
            } else {
                $("#curr_client_id").val(model_curr_client_id_str);
            };

            $("#new_client").val(is_new_client);

            var prev_val = 0;
            var curr_val = 0;

            prev_val = model_curr_bonus_percent;
            curr_val = $("#curr_bonus_percent").getKendoNumericTextBox().value();
            if (curr_val === undefined || curr_val === null)
                curr_val = 0;
            if (prev_val != curr_val)
                $("#IsBonusPercentChanged").val(true);
            prev_val = model_curr_bonus_value;
            curr_val = $("#curr_bonus_value").getKendoNumericTextBox().value();
            if (curr_val === undefined || curr_val === null)
                curr_val = 0;
            if (prev_val != curr_val)
                $("#IsBonusValueChanged").val(true);
            prev_val = model_curr_discount_percent;
            curr_val = $("#curr_discount_percent").getKendoNumericTextBox().value();
            if (curr_val === undefined || curr_val === null)
                curr_val = 0;
            if (prev_val != curr_val)
                $("#IsDiscPercentChanged").val(true);
            prev_val = model_curr_money_value;
            curr_val = $("#curr_money_value").getKendoNumericTextBox().value();
            if (curr_val === undefined || curr_val === null)
                curr_val = 0;
            if (prev_val != curr_val)
                $("#IsMoneyValueChanged").val(true);
            prev_val = model_curr_trans_sum;
            curr_val = $("#curr_trans_sum").getKendoNumericTextBox().value();
            if (curr_val === undefined || curr_val === null)
                curr_val = 0;
            if (prev_val != curr_val)
                $("#IsTransSumChanged").val(true);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('btn-primary');
                                $('#btnSubmit').addClass('btn-default');
                                $('#btnSubmit').removeClass('active');
                                $('#btnSubmit').attr('disabled', true);                                
                                $('#btnRevert').removeClass('btn-warning');
                                $('#btnRevert').addClass('hidden');

                                // иначе пойдут нули
                                $("#curr_trans_sum").getKendoNumericTextBox().enable(true);
                                $("#curr_discount_percent").getKendoNumericTextBox().enable(true);
                                $("#curr_bonus_percent").getKendoNumericTextBox().enable(true);
                                $("#curr_bonus_value").getKendoNumericTextBox().enable(true);
                                $("#curr_money_value").getKendoNumericTextBox().enable(true);

                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();
            
            $("#card_num").val(model_card_num);
            $("#card_num2").val(model_card_num2);
            $("#date_beg").getKendoDatePicker().value(model_date_beg);
            $("#date_end").getKendoDatePicker().value(model_date_end);
            if (ddl_card_type_visible) { _this.btnCardTypeChangeClick(e); }
            if (ddl_card_status_visible) { _this.btnCardStatusChangeClick(e); }
            if (ddl_card_client_visible) { _this.btnCardClientChangeClick(e); }
            $("#mess").val(model_mess);

            if (is_new_client === true) {
                _this.btnCardClientAddClick(e);
            }

            curr_trans_sum_enabled = false;
            $("#curr_trans_sum").getKendoNumericTextBox().value(model_curr_trans_sum);
            $("#curr_trans_sum").getKendoNumericTextBox().enable(false);
            $("#btnCurrTransSumChange").removeClass("active");
            curr_disc_perc_enabled = false;
            $("#curr_discount_percent").getKendoNumericTextBox().value(model_curr_discount_percent);
            $("#curr_discount_percent").getKendoNumericTextBox().enable(false);
            $("#btnCurrDiscPercChange").removeClass("active");
            curr_bonus_perc_enabled = false;
            $("#curr_bonus_percent").getKendoNumericTextBox().value(model_curr_bonus_percent);
            $("#curr_bonus_percent").getKendoNumericTextBox().enable(false);
            $("#btnCurrBonusPercChange").removeClass("active");
            curr_bonus_sum_enabled = false;
            $("#curr_bonus_value").getKendoNumericTextBox().value(model_curr_bonus_value);
            $("#curr_bonus_value").getKendoNumericTextBox().enable(false);
            $("#btnCurrBonusSumChange").removeClass("active");
            curr_money_sum_enabled = false;
            $("#curr_money_value").getKendoNumericTextBox().value(model_curr_money_value);
            $("#curr_money_value").getKendoNumericTextBox().enable(false);
            $("#btnCurrMoneySumChange").removeClass("active");


            $('#btnSubmit').removeClass('btn-primary');
            $('#btnSubmit').addClass('btn-default');
            $('#btnSubmit').removeClass('active');
            $('#btnSubmit').attr('disabled', true);            
            $('#btnRevert').removeClass('btn-warning');
            $('#btnRevert').addClass('hidden');
        });
    };

    this.btnCardTypeChangeClick = function(e) {
        $("#curr_card_type_name").toggle();
        var ddl_card_type = $("#cardTypeDropDownList").getKendoDropDownList();
        if (ddl_card_type_visible) {
            //ddl_card_type.wrapper.hide();
            $('#card-type-ddl').addClass('hidden');
            ddl_card_type_visible = false;
            $("#btnCardTypeChange").removeClass("active");
            ddl_card_type.value(model_curr_card_type_id);
            setCardTypeSpecificAttrs();
        } else {
            //ddl_card_type.wrapper.show();
            $('#card-type-ddl').removeClass('hidden');
            ddl_card_type_visible = true;
            $("#btnCardTypeChange").addClass("active");
        }
    };

    this.btnCardStatusChangeClick = function(e) {
        $("#curr_card_status_name").toggle();
        var ddl_card_status = $("#cardStatusDropDownList").getKendoDropDownList();
        if (ddl_card_status_visible) {
            //ddl_card_status.wrapper.hide();
            $('#card-status-ddl').addClass('hidden');
            ddl_card_status_visible = false;
            $("#btnCardStatusChange").removeClass("active");
            ddl_card_status.value(model_curr_card_status_id);
        } else {
            //ddl_card_status.wrapper.show();
            $('#card-status-ddl').removeClass('hidden');
            ddl_card_status_visible = true;
            $("#btnCardStatusChange").addClass("active");
        }
    };

    this.btnCardClientChangeClick = function(e) {
        $("#curr_client_full_name").toggle();
        //var ddl_card_client = $("#cardClientDropDownList").getKendoDropDownList();
        var ac_card_client = $("#cardClientAutoComplete").data('kendoAutoComplete');
        if (ddl_card_client_visible) {
            //ddl_card_client.wrapper.hide();
            $('#card-client-ddl').addClass('hidden');
            ddl_card_client_visible = false;
            $("#btnCardClientChange").removeClass("active");
            $("#divCardClientAdd").addClass("hidden");
            $("#divCardClientAdd").removeClass("show");
        } else {
            //ddl_card_client.wrapper.show();
            $('#card-client-ddl').removeClass('hidden');
            ddl_card_client_visible = true;
            $("#btnCardClientChange").addClass("active");
            $("#divCardClientAdd").removeClass("hidden");
            $("#divCardClientAdd").addClass("show");
            _this.onAttrChanged();
        };
    };

    this.btnCardClientAddClick = function(e) {
        if (is_new_client === false) {
            $("#divCliendAdd").removeClass("hidden");
            $("#btnCardClient").addClass("active");
            $("#divCliendAdd").addClass("show");
            is_new_client = true;
        } else {
            $("#divCliendAdd").removeClass("show");
            $("#btnCardClient").removeClass("active");
            $("#divCliendAdd").addClass("hidden");
            is_new_client = false;
        };
    };

    this.onClientChange = function(e) {        
        var dataItem = this.dataItem(e.item.index());
        if (dataItem) {
            curr_card_holder_id = dataItem.client_id_str;
        } else {
            curr_card_holder_id = model_curr_client_id_str;
        }
        if (is_new_client === true) {
            _this.btnCardClientAddClick(e);
        };
        _this.onAttrChanged();
    };

    this.btnCurrTransSumChangeClick = function(e) {
        if (curr_trans_sum_enabled) {
            curr_trans_sum_enabled = false;
            $("#curr_trans_sum").getKendoNumericTextBox().enable(false);
            $("#btnCurrTransSumChange").removeClass("active");
        } else {
            curr_trans_sum_enabled = true;
            $("#curr_trans_sum").getKendoNumericTextBox().enable(true);
            $("#btnCurrTransSumChange").addClass("active");
        };
    };

    this.btnCurrDiscPercChangeClick = function(e) {
        if (curr_disc_perc_enabled) {
            curr_disc_perc_enabled = false;
            $("#curr_discount_percent").getKendoNumericTextBox().enable(false);
            $("#btnCurrDiscPercChange").removeClass("active");
        } else {
            curr_disc_perc_enabled = true;
            $("#curr_discount_percent").getKendoNumericTextBox().enable(true);
            $("#btnCurrDiscPercChange").addClass("active");
        };
    };

    this.btnCurrBonusPercChangeClick = function(e) {
        if (curr_bonus_perc_enabled) {
            curr_bonus_perc_enabled = false;
            $("#curr_bonus_percent").getKendoNumericTextBox().enable(false);
            $("#btnCurrBonusPercChange").removeClass("active");
        } else {
            curr_bonus_perc_enabled = true;
            $("#curr_bonus_percent").getKendoNumericTextBox().enable(true);
            $("#btnCurrBonusPercChange").addClass("active");
        };
    };

    this.btnCurrBonusSumChangeClick = function(e) {
        if (curr_bonus_sum_enabled) {
            curr_bonus_sum_enabled = false;
            $("#curr_bonus_value").getKendoNumericTextBox().enable(false);
            $("#btnCurrBonusSumChange").removeClass("active");
        } else {
            curr_bonus_sum_enabled = true;
            $("#curr_bonus_value").getKendoNumericTextBox().enable(true);
            $("#btnCurrBonusSumChange").addClass("active");
        };
    };

    this.btnCurrMoneySumChangeClick = function(e) {
        if (curr_money_sum_enabled) {
            curr_money_sum_enabled = false;
            $("#curr_money_value").getKendoNumericTextBox().enable(false);
            $("#btnCurrMoneySumChange").removeClass("active");
        } else {
            curr_money_sum_enabled = true;
            $("#curr_money_value").getKendoNumericTextBox().enable(true);
            $("#btnCurrMoneySumChange").addClass("active");
        };
    };

    this.valueMapper = function(options) {
        $.ajax({
            url: url_CardClient_ValueMapper,
            data: convertValues(options.value),
            success: function (data) {
                options.success(data);
            }
        });
    };

    this.onAttrChanged = function() {
        $('#btnSubmit').addClass('btn-primary');
        $('#btnSubmit').removeClass('btn-default');
        $('#btnSubmit').removeAttr('disabled');
        $('#btnSubmit').addClass('active');
        $('#btnRevert').removeClass('hidden');        
        $('#btnRevert').addClass('btn-warning');
    };

    this.onCardTypeDataBound = function (e) {
        setCardTypeSpecificAttrs();
    };

    this.onCardTypeChange = function(e) {
        setCardTypeSpecificAttrs();
        _this.onAttrChanged();
    };

    this.grid_error = function(e) {
        var grid = $("#cardAdditionalNumListGrid").data("kendoGrid");
        if (e.errors) {
            var message = "Ошибка:\n";
            // Create a message containing all errors.
            $.each(e.errors, function (key, value) {
                if ('errors' in value) {
                    $.each(value.errors, function () {
                        message += this + "\n";
                    });
                }
            });
            // Display the message
            alert(message);
            // Cancel the changes
            grid.cancelChanges();
        };
    };

    this.onRequestEnd = function(e) {
        var grid = $("#cardAdditionalNumListGrid").data("kendoGrid");
        if ((e.type == "create") || (e.type == "update")) {
            grid.dataSource.read();
        };
    };

    this.getCardClientAutoCompleteData = function () {
        return {
            text: $("#cardClientAutoComplete").val()
        };
    };

    /* private */

    function setCardTypeSpecificAttrs() {
        var selected_card_type = $("#cardTypeDropDownList").getKendoDropDownList().dataItem();
        if (selected_card_type) {
            var value = selected_card_type["card_type_group_id"];
            if (value == '1') {
                $('#divDiscPerc').show();
                $('#divBonusPerc').hide();
                $('#divBonusSum').hide();
                $('#divMoneySum').hide();
            }
            else if (value == '2') {
                $('#divDiscPerc').hide();
                $('#divBonusPerc').show();
                $('#divBonusSum').show();
                $('#divMoneySum').hide();
            }
            else if (value == '3') {
                $('#divDiscPerc').show();
                $('#divBonusPerc').show();
                $('#divBonusSum').show();
                $('#divMoneySum').hide();
            }
            else if (value == '4') {
                $('#divDiscPerc').hide();
                $('#divBonusPerc').hide();
                $('#divBonusSum').hide();
                $('#divMoneySum').show();
            }
            else {
                $('#divDiscPerc').hide();
                $('#divBonusPerc').hide();
                $('#divBonusSum').hide();
                $('#divMoneySum').hide();
            }
        };
    };

    function convertValues(value) {
        var data = {};

        value = $.isArray(value) ? value : [value];

        for (var idx = 0; idx < value.length; idx++) {
            data["values[" + idx + "]"] = value[idx];
        }

        return data;
    };

}

var cardEdit = null;

var ddl_card_type_visible = false;
var ddl_card_status_visible = false;
var ddl_card_client_visible = false;

var curr_trans_sum_enabled = false;
var curr_disc_perc_enabled = false;
var curr_bonus_perc_enabled = false;
var curr_bonus_sum_enabled = false;
var curr_money_sum_enabled = false;
var is_new_client = false;
var curr_card_holder_id = null;

$(function() {
    cardEdit = new CardEdit();
    cardEdit.init();    
});
