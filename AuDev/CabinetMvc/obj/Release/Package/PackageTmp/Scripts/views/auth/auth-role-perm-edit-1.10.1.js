﻿function AuthRolePermEdit() {
    _this = this;

    this.init = function () {        
        setGridMaxHeight('#authRoleSectionGrid', 0);

        $(document).on('click', '.chb-allow-view-section', function () {
            var grid = $("#authRoleSectionGrid").getKendoGrid();            
            var isChecked = $(this).is(':checked');
            var row = $(this).closest("tr");
            var dataItem = grid.dataItem(row);
            dataItem.allow_view = isChecked;
            var detailGridId = '#grid1_' + dataItem.section_id;            
            $(detailGridId).find('.chb-allow-view-part').prop('checked', isChecked);
            var detailGrid = $(detailGridId).getKendoGrid();
            if (detailGrid) {
                $.each($(detailGridId).find('tr'), function (index, value) {
                    var detailDataItem = detailGrid.dataItem(value);
                    if (detailDataItem) {
                        detailDataItem.allow_view = isChecked;
                    };
                });
            };
        });
        //
        $(document).on('click', '.chb-allow-edit-section', function () {
            var grid = $("#authRoleSectionGrid").getKendoGrid();
            var isChecked = $(this).is(':checked');
            var row = $(this).closest("tr");
            var dataItem = grid.dataItem(row);
            dataItem.allow_edit = isChecked;
            var detailGridId = '#grid1_' + dataItem.section_id;            
            $(detailGridId).find('.chb-allow-edit-part').prop('checked', isChecked);
            var detailGrid = $(detailGridId).getKendoGrid();
            if (detailGrid) {
                $.each($(detailGridId).find('tr'), function (index, value) {
                    var detailDataItem = detailGrid.dataItem(value);
                    if (detailDataItem) {
                        detailDataItem.allow_edit = isChecked;
                    };
                });
            };
        });
        //
        $(document).on('click', '.chb-allow-view-part', function (e) {
            var grid = $(e.target).parents('.k-grid').getKendoGrid();
            var isChecked = $(this).is(':checked');
            var row = $(this).closest("tr");
            var dataItem = grid.dataItem(row);
            dataItem.allow_view = isChecked;
        });
        //
        $(document).on('click', '.chb-allow-edit-part', function (e) {
            var grid = $(e.target).parents('.k-grid').getKendoGrid();
            var isChecked = $(this).is(':checked');
            var row = $(this).closest("tr");
            var dataItem = grid.dataItem(row);
            dataItem.allow_edit = isChecked;
        });

        $('#btnCancel').click(function (e) {
            refreshGrid('#authRoleSectionGrid')
        });

        $('#btnSave').click(function (e) {        
            var data2 = [];
            for (i = 1; i <= 20; i++)
            {
                var grid = $("#grid1_" + i).getKendoGrid();
                if (grid) {
                    data2.push(grid.dataSource.view());
                }
            
            }
            var postData = { permList: data2, role_id: model_role_id };
            $.ajax({
                type: "POST",
                url: "AuthRolePermEdit",
                data: JSON.stringify(postData),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if (response.err) {
                        if (response.mess) {
                            alert(response.mess);
                        } else {
                            alert('Ошибка при сохранении изменений');
                        };
                    } else {
                        //
                    };
                    refreshGrid('#authRoleSectionGrid');                
                },
                error: function (response) {
                    alert('Ошибка при попытке сохранения изменений');                
                }
            });        
        });
    };

    this.onAuthRoleSectionGridDataBound = function(e) {
        drawEmptyRow('#authRoleSectionGrid');
    };

    this.onAuthRolePartGridDataBound = function(e) {
        var grid_id = $(this.wrapper).attr('id');
        drawEmptyRow('#' + grid_id);
    };
}

var authRolePermEdit = null;

$(function () {
    authRolePermEdit = new AuthRolePermEdit();
    authRolePermEdit.init();
});
