﻿function CardBatchOperationsLog() {
    _this = this;

    this.init = function () {
        //
    };

    this.refreshGridRow = function(e) {

        var row = $(e.target).closest("tr");
        var grid = $("#cardBatchOperationsLogGrid").data("kendoGrid");
        var dataItem = grid.dataItem(row);

        $.ajax({
            url: url_RefreshBatchOperations,
            data: {
                id: dataItem.batch_id
            },
            dataType: "json",
            type: "POST",
            error: function () {
                alert("Ошибка при попытке обновить строку");
            },
            success: function (data) {
                dataItem.set("record_cnt", data.record_cnt);
                dataItem.set("state_str", data.state_str);
                dataItem.set("state_date", data.state_date);
                dataItem.set("processed_cnt", data.processed_cnt);
                dataItem.set("Message", data.Message);
                dataItem.set("batch_name", data.batch_name);
            }
        });

    };

    this.onCardBatchOperationsLogGridDataBound = function(e) {
        drawEmptyRow('#cardBatchOperationsLogGrid');
    };
}

var cardBatchOperationsLog = null;

$(function () {
    cardBatchOperationsLog = new CardBatchOperationsLog();
    cardBatchOperationsLog.init();
});
