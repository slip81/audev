﻿function RegionList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#regionGrid', 30);
        //
        $('#btnSave').click(function (e) {
            curr_price_limit_list = $.grep($('#regionPriceLimitGrid').data('kendoGrid').dataSource.data(), function (item) {
                return item.dirty;
            });
            $('#btnSave').addClass('disabled');
            $('#btnCancel').addClass('disabled');
            $('#btnDelete').addClass('disabled');
            //
            $.ajax({
                url: '/RegionPriceLimitSave',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ region_id: curr_region_id, price_limit_list: curr_price_limit_list }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response == null ? 'Ошибка при попытке сохранения' : response.mess);
                    } else {
                        var dataItem = $('#regionGrid').data('kendoGrid').dataSource.get(curr_region_id);
                        if (dataItem) {
                            var row = $('#regionGrid').find('tr[data-uid=' + dataItem.uid + ']').first();
                            if (row) {
                                var span1 = $(row).find('span.span-charge-param').first();
                                if (span1) {
                                    span1.removeClass('text-success');
                                    span1.addClass('text-primary');
                                }
                                var span2 = $(row).find('span.span-charge-param-icon').first();
                                if (span2) {
                                    span2.removeClass('k-i-plus');
                                    span2.addClass('k-edit');
                                }
                            }
                        }
                    };
                },
                error: function (response) {
                    alert('Ошибка при сохранении');
                },
                complete: function (response) {
                    $('#btnSave').removeClass('disabled');
                    $('#btnCancel').removeClass('disabled');
                    $('#btnDelete').removeClass('disabled');
                    $("#winRegionPriceLimit").data("kendoWindow").close();
                }
            });            
        });
        //
        $('#btnCancel').click(function (e) {
            $("#winRegionPriceLimit").data("kendoWindow").close();
        });
        //        
        $('#btnDelete').click(function (e) {
            if (!(confirm('Удалить наценки ?')))
                return;
            //
            $('#btnSave').addClass('disabled');
            $('#btnCancel').addClass('disabled');
            $('#btnDelete').addClass('disabled');
            //
            $.ajax({
                url: '/RegionPriceLimitDel',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ region_id: curr_region_id }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response == null ? 'Ошибка при попытке удаления' : response.mess);
                    } else {                        
                        var dataItem = $('#regionGrid').data('kendoGrid').dataSource.get(curr_region_id);
                        if (dataItem) {
                            var row = $('#regionGrid').find('tr[data-uid=' + dataItem.uid + ']').first();
                            if (row) {
                                var span1 = $(row).find('span.span-charge-param').first();
                                if (span1) {
                                    span1.removeClass('text-primary');
                                    span1.addClass('text-success');
                                }
                                var span2 = $(row).find('span.span-charge-param-icon').first();
                                if (span2) {
                                    span2.removeClass('k-edit');
                                    span2.addClass('k-i-plus');
                                }
                            }
                        }
                    };
                },
                error: function (response) {
                    alert('Ошибка при удалении');
                },
                complete: function (response) {
                    $('#btnSave').removeClass('disabled');
                    $('#btnCancel').removeClass('disabled');
                    $('#btnDelete').removeClass('disabled');
                    $("#winRegionPriceLimit").data("kendoWindow").close();
                }
            });            
        });
        //
        $('#btnZoneSave').click(function (e) {
            curr_price_limit_list = $.grep($('#regionZonePriceLimitGrid').data('kendoGrid').dataSource.data(), function (item) {
                return item.dirty;
            });
            $('#btnZoneSave').addClass('disabled');
            $('#btnZoneCancel').addClass('disabled');
            $('#btnZoneDelete').addClass('disabled');
            //
            $.ajax({
                url: '/RegionZonePriceLimitSave',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ zone_id: curr_zone_id, price_limit_list: curr_price_limit_list }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response == null ? 'Ошибка при попытке сохранения' : response.mess);
                    } else {
                        var grid_name = '#regionZoneGrid_' + curr_region_id;
                        var dataItem = $(grid_name).data('kendoGrid').dataSource.get(curr_zone_id);
                        if (dataItem) {
                            var row = $(grid_name).find('tr[data-uid=' + dataItem.uid + ']').first();
                            if (row) {
                                var span1 = $(row).find('span.span-zone-charge-param').first();
                                if (span1) {
                                    span1.removeClass('text-success');
                                    span1.addClass('text-primary');
                                }
                                var span2 = $(row).find('span.span-zone-charge-param-icon').first();
                                if (span2) {
                                    span2.removeClass('k-i-plus');
                                    span2.addClass('k-edit');
                                }
                            }
                        }
                    };
                },
                error: function (response) {
                    alert('Ошибка при сохранении');
                },
                complete: function (response) {
                    $('#btnZoneSave').removeClass('disabled');
                    $('#btnZoneCancel').removeClass('disabled');
                    $('#btnZoneDelete').removeClass('disabled');
                    $("#winRegionZonePriceLimit").data("kendoWindow").close();
                }
            });
        });
        //
        $('#btnZoneCancel').click(function (e) {
            $("#winRegionZonePriceLimit").data("kendoWindow").close();
        });
        //        
        $('#btnZoneDelete').click(function (e) {
            if (!(confirm('Удалить наценки ?')))
                return;
            //
            $('#btnZoneSave').addClass('disabled');
            $('#btnZoneCancel').addClass('disabled');
            $('#btnZoneDelete').addClass('disabled');
            //
            $.ajax({
                url: '/RegionZonePriceLimitDel',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ zone_id: curr_zone_id }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response == null ? 'Ошибка при попытке удаления' : response.mess);
                    } else {
                        var grid_name = '#regionZoneGrid_' + curr_region_id;
                        var dataItem = $(grid_name).data('kendoGrid').dataSource.get(curr_zone_id);
                        if (dataItem) {
                            var row = $(grid_name).find('tr[data-uid=' + dataItem.uid + ']').first();
                            if (row) {
                                var span1 = $(row).find('span.span-zone-charge-param').first();
                                if (span1) {
                                    span1.removeClass('text-primary');
                                    span1.addClass('text-success');
                                }
                                var span2 = $(row).find('span.span-zone-charge-param-icon').first();
                                if (span2) {
                                    span2.removeClass('k-edit');
                                    span2.addClass('k-i-plus');
                                }
                            }
                        }
                    };
                },
                error: function (response) {
                    alert('Ошибка при удалении');
                },
                complete: function (response) {
                    $('#btnZoneSave').removeClass('disabled');
                    $('#btnZoneCancel').removeClass('disabled');
                    $('#btnZoneDelete').removeClass('disabled');
                    $("#winRegionZonePriceLimit").data("kendoWindow").close();
                }
            });
        });
    };


    this.onRegionGridDataBound = function (e) {
        drawEmptyRow('#regionGrid');
    };

    this.onChargeParamBtnClick = function (region_id) {
        //alert('region_id = ' + region_id);
        curr_region_id = region_id;
        curr_price_limit_list = null;
        $("#winRegionPriceLimit").data("kendoWindow").open().center();
    };

    this.onZoneChargeParamBtnClick = function (region_id, zone_id) {
        //alert('zone_id = ' + zone_id); 
        curr_region_id = region_id;
        curr_zone_id = zone_id;
        curr_price_limit_list = null;
        $("#winRegionZonePriceLimit").data("kendoWindow").open().center();
    };

    this.onRegionZoneGridDataBound = function (e) {
        var grid_id = $(this.wrapper).attr('id');
        drawEmptyRow('#' + grid_id);
    };

    this.getRegionPriceLimitGridData = function (e) {
        return { region_id: curr_region_id };
    };

    this.onWinRegionPriceLimitActivate = function (e) {
        refreshGrid('#regionPriceLimitGrid');
    };

    this.onWinRegionZonePriceLimitActivate = function (e) {
        refreshGrid('#regionZonePriceLimitGrid');
    };

    this.getRegionZonePriceLimitGridData = function (e) {
        return { zone_id: curr_zone_id };
    };
}

var regionList = null;
var curr_region_id = null;
var curr_zone_id = null;
var curr_price_limit_list = null;
$(function () {
    regionList = new RegionList();
    regionList.init();
});
