﻿function AsnaOrderList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#asnaOrderGrid', 50);
    };

    this.onAsnaUserClientDdlChange = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserClientDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserSalesDdlChange = function (e) {
        refreshGrid('#asnaOrderGrid');
    };

    this.onAsnaUserSalesDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').select(0);
        refreshGrid('#asnaOrderGrid');
    };

    this.getAsnaUserSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#asnaUserClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    }

    this.getAsnaOrderGridData = function (e) {
        var curr_sales_id = null;        
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        return { sales_id: curr_sales_id };
    };

    this.onAsnaOrderGridDataBound = function (e) {
        drawEmptyRow('#asnaOrderGrid');
    };

    this.onBtnAsnaUserEditClick = function (e) {
        var curr_sales_id = null;
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        if (!curr_sales_id) {
            alert('Не выбрана торговая точка');
            return;
        };
        location.href = '/AsnaSalesEdit?sales_id=' + curr_sales_id;
    };

    this.onAsnaOrderRowGridDataBound = function(e) {
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if (dataItem.get("asna_qnt_unrsv")) {
                row.addClass("k-error-colored");
            };
        };
    };
}

var asnaOrderList = null;
$(function () {
    asnaOrderList = new AsnaOrderList();
    asnaOrderList.init();
});
