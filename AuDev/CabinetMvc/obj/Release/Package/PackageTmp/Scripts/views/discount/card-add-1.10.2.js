﻿function CardAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            var combo1 = $("#cardTypeDropDownList").getKendoDropDownList();
            $("#curr_card_type_id").val(combo1.dataItem()["card_type_id"]);
            var combo2 = $("#cardStatusDropDownList").getKendoDropDownList();
            $("#curr_card_status_id").val(combo2.dataItem()["card_status_id"]);
            //var combo3 = $("#cardClientDropDownList").getKendoDropDownList();
            //$("#curr_client_id").val(combo3.dataItem()["client_id_str"]);
            $("#curr_client_id").val(curr_card_holder_id);
            $("#new_client").val(is_new_client);

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
    };

    this.onCardStatusDataBound = function() {
        var curr_card_status = $("#curr_card_status_id").val();
        if (curr_card_status) {
            $("#cardStatusDropDownList").getKendoDropDownList().value(curr_card_status);
        };
    };

    this.onCardTypeDataBound = function() {
        var curr_card_type = $("#curr_card_type_id").val();
        if (curr_card_type) {            
            $("#cardTypeDropDownList").getKendoDropDownList().value(curr_card_type);
        };        
        _this.onCardTypeChange();
    };

    this.onCardTypeChange = function() {
        var selected_card_type = $("#cardTypeDropDownList").getKendoDropDownList().dataItem();
        if (selected_card_type) {
            var value = selected_card_type["card_type_group_id"];
            if (value == '1') {
                $('#divDiscPerc').show();
                $('#divBonusPerc').hide();
                $('#divBonusSum').hide();
                $('#divMoneySum').hide();
            }
            else if (value == '2') {
                $('#divDiscPerc').hide();
                $('#divBonusPerc').show();
                $('#divBonusSum').show();
                $('#divMoneySum').hide();
            }
            else if (value == '3') {
                $('#divDiscPerc').show();
                $('#divBonusPerc').show();
                $('#divBonusSum').show();
                $('#divMoneySum').hide();
            }
            else if (value == '4') {
                $('#divDiscPerc').hide();
                $('#divBonusPerc').hide();
                $('#divBonusSum').hide();
                $('#divMoneySum').show();
            }
            else {
                $('#divDiscPerc').hide();
                $('#divBonusPerc').hide();
                $('#divBonusSum').hide();
                $('#divMoneySum').hide();
            }
        };
    };

    this.valueMapper = function(options) {
        $.ajax({
            url: url_CardClient_ValueMapper,
            data: convertValues(options.value),
            success: function (data) {
                options.success(data);
            }
        });
    };

    this.btnCardClientAddClick = function(e) {
        if (is_new_client === false) {
            $("#divCliendAdd").removeClass("hidden");
            $("#btnCardClient").addClass("active");
            $("#divCliendAdd").addClass("show");            
            is_new_client = true;
        } else {
            $("#divCliendAdd").removeClass("show");
            $("#btnCardClient").removeClass("active");
            $("#divCliendAdd").addClass("hidden");            
            is_new_client = false;
        };
    };

    this.onClientChange = function (e) {
        var dataItem = this.dataItem(e.item.index());
        if (dataItem) {
            curr_card_holder_id = dataItem.client_id_str;
        } else {
            curr_card_holder_id = null;
        }
        if (is_new_client === true) {
            _this.btnCardClientAddClick(e);
        }
    };

    this.getCardClientAutoCompleteData = function () {
        return {
            text: $("#cardClientAutoComplete").val()
        };
    };

    /* private */

    function convertValues(value) {
        var data = {};

        value = $.isArray(value) ? value : [value];

        for (var idx = 0; idx < value.length; idx++) {
            data["values[" + idx + "]"] = value[idx];
        }

        return data;
    };

}

var cardAdd = null;
var is_new_client = false;
var curr_card_holder_id = null;

$(function () {
    cardAdd = new CardAdd();
    cardAdd.init();
});
