﻿function PrjList() {
    _this = this;

    this.init = function () {
        $('#side-menu-div').hide();
        $('#main-content-div').addClass('col-sm-12');
        $('#main-content-div').removeClass('col-sm-10 col-sm-10-bigger');
        setControlHeight('#prjMainSplitter', 0);
        //
        $("#prj-loading").dialog({
            modal: true,
            hide: 'drop',
            show: 'drop',
            autoOpen: false,
            height: 100,
            dialogClass: "noclose"
        });
        //
        $('#tsPrj>ul.k-tabstrip-items>li.k-item').on('click', function () {
            var txt = $(this).find('a.k-link').first().text();
            afterContentLoad(false, txt);
        });
        //        
        $('#btnPrjUserSettingsLoadOk').on('click', function () {
            $('#winPrjUserSettingsLoad').data('kendoWindow').close();
            //
            loadingDialog('#prj-loading', true);
            //
            var curr_new_setting_id = null;
            var curr_dataItem = $('#ddlPrjUserSettingsLoad').data('kendoDropDownList').dataItem();
            if ((curr_dataItem) && (curr_dataItem.setting_id)) {
                curr_new_setting_id = curr_dataItem.setting_id;
            }
            //
            $.ajax({
                url: '/PrjUserSettingsChange',
                type: "POST",                
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ old_setting_id: curr_user_settings_id, new_setting_id: curr_new_setting_id }),
                success: function (response) {
                    if ((!response) || (response.err)) {
                        alert((response && response.err) ? response.mess : 'Ошибка смены конфигурации');
                    } else {
                        var tabStrip = $("#tsPrj").data('kendoTabStrip');
                        var item = $('#tsPrj li.k-state-active');
                        var content = $('#tsPrj .k-content.k-state-active').first();
                        //
                        switch (leftPaneContentType) {
                            case 1:
                                prjListPartial.reload_after();
                                break;
                            case 2:
                                prjClaimListPartial.reload_after();
                                break;
                            case 4:
                                prjWorkListPartial.reload_after();
                                break;
                            case 5:
                                prjPlanPartial.reload_after();
                                break;
                            case 8:
                                prjBriefPartial.reload_after();                                
                                var link = item.find('a.k-link').first();
                                link.data('contentUrl', 'PrjBriefPartial');
                                break;
                            default:
                                break;
                        }
                        //
                        content.html('');
                        setTimeout(function () {
                            tabStrip.reload(item);
                        }, 300);
                    };
                },
                error: function (response) {
                    alert('Ошибка при обновлении страницы');
                },
                complete: function (response) {
                    loadingDialog('#prj-loading', false);
                }
            });
        });
        //        
        $('#btnPrjUserSettingsLoadCancel').on('click', function () {
            $('#winPrjUserSettingsLoad').data('kendoWindow').close();
        });
        //        
        $('#btnPrjUserSettingsSaveOk').on('click', function () {
            $('#winPrjUserSettingsSave').data('kendoWindow').close();
            //
            var curr_mode = 1;
            if ($('#rbNewSetting_').is(':checked')) {
                curr_mode = 1;
            } else if ($('#rbRewriteCurrent_').is(':checked')) {
                curr_mode = 2;
            } else if ($('#rbRewriteOther_').is(':checked')) {
                curr_mode = 3;
            };
            var curr_name = null;
            var curr_setting_id = null;
            switch (curr_mode) {
                case 1:
                    curr_name = $('#txtNewSetting').val();
                    break;
                case 2:
                    curr_name = $('#txtCurrentSetting').val();
                    curr_setting_id = curr_user_settings_id;
                    break;
                case 3:
                    var dataItem = $('#ddlPrjUserSettingsSave').data('kendoDropDownList').dataItem();
                    curr_setting_id = (dataItem && dataItem.setting_id) ? dataItem.setting_id : null;
                    break;
                default:
                    break;
            }
            //
            prjUserConfigSave(curr_mode, curr_setting_id, curr_name);
        });
        //        
        $('#btnPrjUserSettingsSaveCancel').on('click', function () {
            $('#winPrjUserSettingsSave').data('kendoWindow').close();
        });
    };

    this.onTsPrjContentLoad = function (e) {
        afterContentLoad(true, $(e.item).text());
    };

    this.onPrjMainSplitterContentLoad = function (e) {
        var pane_id = $(e.pane).attr('id');
        if (pane_id == 'prj-main-pane-right') {
            switch (rightPaneContentType) {
                case 1:
                    prjEditPartial.loadData();
                    loadingDialog('#prj-loading', false);
                    break;
                case 2:
                    prjClaimEditPartial.loadData();
                    break;
                case 13:
                    prjClaimFilterEditPartial.loadData();
                    break;
                default:
                    loadingDialog('#prj-loading', false);
                    break;                    
            }
        };                
    };

    this.openPrjEditPartial = function (curr_prj_id) {
        var splitter = $("#prjMainSplitter").data("kendoSplitter");
        splitter.ajaxRequest("#prj-main-pane-right", "/PrjEditPartial", { prj_id: curr_prj_id });
        toggleSplitter("#prjMainSplitter", "#prj-main-pane-right", "#prj-main-pane-left");
    };

    this.openPrjClaimEditPartial = function (curr_claim_id, curr_prj_id, curr_project_id, curr_task_id) {
        var splitter = $("#prjMainSplitter").data("kendoSplitter");
        splitter.ajaxRequest("#prj-main-pane-right", "/PrjClaimEditPartial", { claim_id: curr_claim_id, prj_id: curr_prj_id, project_id: curr_project_id, task_id: curr_task_id });
        toggleSplitter("#prjMainSplitter", "#prj-main-pane-right", "#prj-main-pane-left");
    };

    this.onBtnConfigClick = function (e) {
        var config_action_type = parseInt(e.target.attr('data-config-action-type'), 10);
        switch (config_action_type) {
            case 1:
                configSaveDefault();
                break;
            case 2:
                $('#winPrjUserSettingsSave').data('kendoWindow').center().open();
                break;
            case 3:
                $('#winPrjUserSettingsLoad').data('kendoWindow').center().open();
                break;
            default:
                break;
        };
    };

    this.onWinPrjUserSettingsLoadOpen = function (e) {
        var ddl = $('#ddlPrjUserSettingsLoad').data('kendoDropDownList');
        ddl.enable(false);
        ddl.dataSource.read();        
    };

    this.onDdlPrjUserSettingsLoadDataBound = function (e) {
        var ddl = $('#ddlPrjUserSettingsLoad').data('kendoDropDownList');
        if (curr_user_settings_id) {
            ddl.value(curr_user_settings_id);
        } else {
            ddl.select(0);
        };
        ddl.enable(true);
    };

    this.getCurrPageTypeId = function (e) {
        return { page_type_id: leftPaneContentType };
    };

    this.onWinPrjUserSettingsSaveOpen = function (e) {
        $('#txtNewSetting').val('Новая (' + kendo.toString(new Date(), "dd.MM.yyyy HH:mm:ss") + ')');
        $('#txtCurrentSetting').val(curr_user_settings_name);
        //
        var ddl = $('#ddlPrjUserSettingsSave').data('kendoDropDownList');
        ddl.enable(false);
        ddl.dataSource.read();
    };

    this.onDdlPrjUserSettingsSaveDataBound = function (e) {
        var ddl = $('#ddlPrjUserSettingsSave').data('kendoDropDownList');
        if (curr_user_settings_id) {
            ddl.value(curr_user_settings_id);
        } else {
            ddl.select(0);
        };
        ddl.enable(true);
    };

    this.openPrjClaimFilterEditPartial = function (curr_filter_id) {
        var splitter = $("#prjMainSplitter").data("kendoSplitter");
        splitter.ajaxRequest("#prj-main-pane-right", "/PrjClaimFilterEditPartial", { filter_id: curr_filter_id });
        toggleSplitter("#prjMainSplitter", "#prj-main-pane-right", "#prj-main-pane-left");
    };

    // private

    function afterContentLoad(init, txt) {
        if (txt == 'ЗАДАЧИ') {
            leftPaneContentType = 2;
            if (init) {
                prjClaimListPartial.init_after();
            };
        } else if (txt == 'БЛОКНОТ') {
            leftPaneContentType = 9;
            if (init) {
                prjNotebookPartial.init_after();
            };
        } else if (txt == 'СВОДКА') {
            leftPaneContentType = 8;
            if (init) {
                prjBriefPartial.init_after();
            };
        } else if (txt == 'СПИСКИ') {
            leftPaneContentType = 10;
            if (init) {
                prjSpravPartial.init_after();
            };
        } else if (txt == 'ЖУРНАЛ') {
            leftPaneContentType = 7;
            if (init) {
                prjLogPartial.init_after();
            };
        } else if (txt == 'СЕРВИС') {
            leftPaneContentType = 11;
            if (init) {
                prjSettingsPartial.init_after();
            };
        } else if (txt == 'КОРЗИНА') {
            leftPaneContentType = 12;
            if (init) {
                prjBinPartial.init_after();
            };
        };
    };

    function prjUserConfigSave(curr_mode, curr_setting_id, curr_name) {
        loadingDialog('#prj-loading', true);
        //
        var curr_settings = [];
        var curr_settings_grid = {};
        var curr_grid_name = null;
        var curr_tag= null;
        switch (leftPaneContentType) {
            case 1:
                curr_settings = prjListPartial.getSettings();                
                curr_grid_name = '#prjGrid';                
                curr_settings_grid = gridConfig(curr_grid_name);
                break;
            case 2:
                curr_settings = prjClaimListPartial.getSettings();                
                curr_grid_name = '#prjClaimGrid';                
                curr_settings_grid = gridConfig(curr_grid_name);
                break;
            case 4:
                curr_settings = prjWorkListPartial.getSettings();
                curr_grid_name = '#prjWorkGrid';                
                curr_settings_grid = gridConfig(curr_grid_name);
                break;
            case 5:
                curr_settings = prjPlanPartial.getSettings();
                curr_grid_name = '#prjPlanWorkGrid';
                curr_settings_grid = gridConfig(curr_grid_name);
                break;
            case 8:
                curr_settings = prjBriefPartial.getSettings();                
                //curr_grid_name = '#prjBriefGrid';
                curr_grid_name = curr_user_settings_grid_name;
                curr_settings_grid = gridConfig(curr_grid_name);
                if (curr_user_settings_grid_name == '#prjBriefWorkGrid') {
                    curr_tag = 'work';
                } else if (curr_user_settings_grid_name == '#prjBriefTaskGrid') {
                    curr_tag = 'task';
                } else {
                    curr_tag = null;
                }                
                break;
            case 9:
                curr_settings = prjNotebookPartial.getSettings();
                break;
            default:
                break;
        }
        //
        $.ajax({
            url: '/PrjUserSettingsSave',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ page_type_id: leftPaneContentType, mode: curr_mode, setting_id: curr_setting_id, setting_name: curr_name, settings: JSON.stringify(curr_settings), settings_grid: JSON.stringify(curr_settings_grid), tag: curr_tag }),
            success: function (response) {
                if ((!response) || (response.err)) {
                    alert((response && response.err) ? response.mess : 'Ошибка сохранения конфигурации');
                } else {
                    curr_user_settings_id = response.setting.setting_id;
                    curr_user_settings_name = response.setting.setting_name;
                };
            },
            error: function (response) {
                alert('Ошибка при сохранении конфигурации');
            },
            complete: function (response) {
                loadingDialog('#prj-loading', false);
            }
        });
    };

    function configSaveDefault() {
        var handler = function () {
            var curr_mode = curr_user_settings_id ? 2 : 1;
            var curr_setting_id = curr_user_settings_id;
            var curr_name = curr_user_settings_id ? curr_user_settings_name : 'Новая (' + kendo.toString(new Date(), "dd.MM.yyyy HH:mm:ss") + ')';
            prjUserConfigSave(curr_mode, curr_setting_id, curr_name);
        };
        if (!curr_user_settings_id) {
            handler();
        } else {
            showConfirmWindow('#confirm-templ', 'Перезаписать текущую конфигурацию ?', handler);
        };
    };

}

var prjList = null;

//null:не определено,1:группа,2:задача,3:задание,4:работа,5:календарь,6:обзор,7:журнал,8:сводка,9:блокнот,10:справочники,11:сервис,12:корзина,13:фильтр
var rightPaneContentType = 1;
var leftPaneContentType = 2;
var leftPaneContentType_main = 2;
var leftPaneContentType_child = 3;
var waitTimeout = 400;
var curr_user_settings_id = null;
var curr_user_settings_name = null;
var curr_user_settings_grid = null;
var curr_user_settings_grid_name = null;
var selected_grid_name = null;
var dblclickFired = false;
var currTaskChildActionId = null;
var currWorkChildActionId = null;
$(function () {
    prjList = new PrjList();
    prjList.init();
});
