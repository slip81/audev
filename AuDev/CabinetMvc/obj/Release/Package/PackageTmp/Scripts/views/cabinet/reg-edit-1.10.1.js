﻿function RegEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();
        
        genPwd();

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Активировать пользователя ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            $('#state').val(1);

            var validator = $("form").getKendoValidator();

            $('#pwd').val($('#input-pwd').val());

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnReject').attr('disabled', true);
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end();
            } else {
                return false;
            };
        });

        $("#btnReject").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Отклонить заявку ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            $('#state').val(2);

            kendoWindow.data("kendoWindow")
                .content($("#save-changes-confirmation").html())
                .center().open();

            kendoWindow.find(".confirm,.cancel")
                    .click(function () {
                        if ($(this).hasClass("confirm")) {
                            kendoWindow.data("kendoWindow").close();
                            $('#btnSubmit').attr('disabled', true);
                            $('#btnReject').attr('disabled', true);
                            $("#btnSubmitProceed").trigger('click');
                            return true;
                        } else {
                            kendoWindow.data("kendoWindow").close();
                            return false;
                        };
                    })
                 .end();
        });

        $('#btnGenPwd').click(function () {
            genPwd();
        });
    };

    function genPwd() {        
        var newPwd = generateRandomString();        
        $('#input-pwd').attr('value', newPwd);
    };
}

var regEdit = null;

$(function () {
    regEdit = new RegEdit();
    regEdit.init();
});
