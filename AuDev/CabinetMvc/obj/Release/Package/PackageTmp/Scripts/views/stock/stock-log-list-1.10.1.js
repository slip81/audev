﻿function StockLogList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#stockUploadLogGrid', 250);
        setGridMaxHeight('#stockDownloadLogGrid', 250);
    };

    this.getStockLogGridData = function (e) {
        var curr_client_id = null;
        var curr_sales_id = null;
        var dataItem = null;
        dataItem = $('#stockLogClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        dataItem = $('#stockLogSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        return { client_id: curr_client_id, sales_id: curr_sales_id };
    };

    this.onStockUploadLogGridDataBound = function (e) {
        drawEmptyRow('#stockUploadLogGrid')
    };

    this.onStockDownloadLogGridDataBound = function (e) {
        drawEmptyRow('#stockDownloadLogGrid')
    };

    this.onStockLogClientDdlDataBound = function (e) {
        $('#stockLogSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onStockLogClientDdlChange = function (e) {
        $('#stockLogSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onStockLogSalesDdlChange = function (e) {
        refreshGrid('#stockUploadLogGrid');
        refreshGrid('#stockDownloadLogGrid');
    };

    this.onStockLogSalesDdlDataBound = function (e) {        
        refreshGrid('#stockUploadLogGrid');
        refreshGrid('#stockDownloadLogGrid');
    };

    this.getStockLogSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#stockLogClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    };

}

var stockLogList = null;

$(function () {
    stockLogList = new StockLogList();
    stockLogList.init();
});
