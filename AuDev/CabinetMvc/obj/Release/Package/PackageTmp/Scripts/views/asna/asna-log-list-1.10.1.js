﻿function AsnaLogList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#asnaLogGrid', 50);
    };

    this.onAsnaUserClientDdlChange = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserClientDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserSalesDdlChange = function (e) {
        refreshGrid('#asnaLogGrid');
    };

    this.onAsnaUserSalesDdlDataBound = function (e) {
        refreshGrid('#asnaLogGrid');
    };

    this.getAsnaUserSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#asnaUserClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    }

    this.getAsnaLogGridData = function (e) {
        var curr_client_id = null;
        var curr_sales_id = null;
        var dataItem1 = $('#asnaUserClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem1) && (dataItem1.client_id)) {
            curr_client_id = dataItem1.client_id;
        }
        var dataItem2 = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem2) && (dataItem2.sales_id)) {
            curr_sales_id = dataItem2.sales_id;
        }
        return { client_id: curr_client_id, sales_id: curr_sales_id };
    };

    this.onAsnaLogGridDataBound = function (e) {
        drawEmptyRow('#asnaLogGrid');
    };
}

var asnaLogList = null;
$(function () {
    asnaLogList = new AsnaLogList();
    asnaLogList.init();
});
