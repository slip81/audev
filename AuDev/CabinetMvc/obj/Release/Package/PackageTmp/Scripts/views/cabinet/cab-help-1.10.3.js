﻿function CabHelp() {
    _this = this;

    this.init = function () {
        //setControlHeight('#cabHelpMainSplitter', 0);
    };

    this.onCabHelpSearchKeyup = function (e) {

    };

    this.onCabHelpTreeSelect = function (e) {
        var url = $(e.node).attr('help-url');
        var splitter = $('#cabHelpMainSplitter').getKendoSplitter();                
        splitter.ajaxRequest('#right-pane', url);
    };    
    
    this.onCabHelpTabContentLoad = function (e) {
        //alert('onCabHelpTabContentLoad !');        
        if (firstDataBound) {
            firstDataBound = false;
            $('#cabHelpTab ul.k-tabstrip-items').remove();
            setControlHeight('#cabHelpMainSplitter', 0);
            if (goto_cabHelpId) {
                gotoCabHelp();
            } else {
                $('#op-wait').addClass('hidden');
                /*
                var cabHelpTree = $("#cabHelpTree").getKendoTreeView();
                cabHelpTree.expand("li:first");
                cabHelpTree.expand("li:first li:first");
                cabHelpTree.select("li:first");
                var url = $("li:first").attr('help-url');
                var splitter = $('#cabHelpMainSplitter').getKendoSplitter();
                splitter.ajaxRequest('#right-pane', url);
                */
            }
        };
    };

    this.getSearchHelpData = function () {
        return {
            text: $("#search-help").val()
        };
    };

    this.onSearchHelpSelect = function (e) {
        //
    };

    this.onSearchHelpKeyup = function (e) {
        if (e.keyCode == 13) {
            //alert('enter pressed !');            
            helpList_fromSearch = null;
            $('div span.k-in.highlight').removeClass('highlight');
            var help_search_str = $('#search-help').val();
            if (help_search_str) {
                $('#search-wait').removeClass('hidden');
                $.ajax({
                    url: url_CabHelpSearch,
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify({ help_name: help_search_str }),
                    success: function (response) {
                        $('#search-wait').addClass('hidden');
                        if ((response == null) || (response.err)) {
                            $('div span.k-in.highlight').removeClass('highlight');
                            getPopup().show(response == null ? 'Ошибка при попытке найти раздел' : response.mess, "error");
                        } else {
                            helpList_fromSearch = $.parseJSON(response.helpList);
                            if ((helpList_fromSearch == null) || (helpList_fromSearch.length <= 0)) {
                                $('div span.k-in.highlight').removeClass('highlight');
                                getPopup().show('Разделы не найдены', "error");
                            } else {
                                $.each(helpList_fromSearch, function (i, item) {
                                    highlightHelp(item.help_id);                                    
                                });
                            }
                        };
                    },
                    error: function (response) {
                        $('div span.k-in.highlight').removeClass('highlight');
                        $('#search-wait').addClass('hidden');
                        getPopup().show('Ошибка поиска разделов', "error");
                    }
                });
            };
        };
    };

    function gotoCabHelp() {
        if (goto_cabHelpId) {
            var cabHelpTree = $("#cabHelpTree").getKendoTreeView();
            var dataItem = cabHelpTree.dataSource.get(goto_cabHelpId);
            goto_cabHelpId = null;
            if (dataItem) {
                var cabHelpNode = cabHelpTree.findByUid(dataItem.uid);
                if (cabHelpNode) {
                    cabHelpTree.expandTo(dataItem);
                    cabHelpTree.expand(cabHelpNode);
                    cabHelpTree.select(cabHelpNode);
                    $('#op-wait').addClass('hidden');
                    var url = cabHelpNode.attr('help-url');
                    var splitter = $('#cabHelpMainSplitter').getKendoSplitter();
                    splitter.ajaxRequest('#right-pane', url);
                    //cabHelpTree.trigger('select', { node: cabHelpNode });
                    //refreshFileTree();
                }
            };
        }
    };

    function highlightHelp(help_id) {
        var helptree = $("#cabHelpTree").getKendoTreeView();
        var dataItem_help = helptree.dataSource.get(help_id);
        if (!dataItem_help) {
            return;
        }
        var helpNode = helptree.findByUid(dataItem_help.uid);
        if (!helpNode) {
            return;
        }
        var helpNodeSpan = helpNode.find('>div span.k-in');
        if (!helpNodeSpan) {
            return;
        }
        helptree.expandTo(help_id);
        if (!helpNodeSpan.hasClass('highlight')) {
            helpNodeSpan.addClass('highlight');
        }
    };

    function getPopup() {
        if (!popupNotification)
            popupNotification = $("#popupNotification").data("kendoNotification");
        return popupNotification;
    };
}

var cabHelp = null;
var firstDataBound = true;
var helpList_fromSearch = null;
var popupNotification = null;

$(function () {
    cabHelp = new CabHelp();
    cabHelp.init();
});
