﻿function ServiceReg() {
    _this = this;

    this.init = function () {
    };

    this.onApplyServiceClick = function(workplace_id, curr_version_id) {

        var grid = $('#grid2_' + workplace_id).getKendoGrid();
        var dataItem = grid.dataSource.get(curr_version_id);

        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Установить услугу ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "150px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#activation-confirmation").html())
            .center().open();

        kendoWindow.find(".operation-confirm,.operation-cancel")
                .click(function () {
                    if ($(this).hasClass("operation-confirm")) {
                        var act_key_edit = kendoWindow.find('.act-key');
                        var curr_act_key = $(act_key_edit).val();

                        kendoWindow.data("kendoWindow").close();

                        var sendData = { client_id: model_id, workplace_id: dataItem.workplace_id, service_id: dataItem.service_id, version_id: dataItem.version_id, activation_key: curr_act_key };
                        $.ajax({
                            url: 'Cabinet/ServiceApply',
                            data: JSON.stringify(sendData),
                            type: "POST",
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                grid.dataSource.read();
                                return true;
                            },
                            error: function (response) {
                                alert('Ошибка при попытке установить услугу');
                                return false;
                            }
                        });
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end()

        return false;
    };

    this.onDelServiceClick = function(workplace_id, curr_version_id) {
        var grid = $('#grid2_' + workplace_id).getKendoGrid();        
        var dataItem = grid.dataSource.get(curr_version_id);

        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Убрать услугу ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "80px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#remove-confirmation").html())
            .center().open();

        kendoWindow.find(".operation-confirm,.operation-cancel")
                .click(function () {
                    if ($(this).hasClass("operation-confirm")) {
                        kendoWindow.data("kendoWindow").close();

                        var sendData = { client_id: model_id, workplace_id: dataItem.workplace_id, service_id: dataItem.service_id, version_id: dataItem.version_id };
                        $.ajax({
                            url: 'Cabinet/ServiceRemove',
                            data: JSON.stringify(sendData),
                            type: "POST",
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                grid.dataSource.read();
                                return true;
                            },
                            error: function (response) {
                                alert('Ошибка при попытке убрать услугу');
                                return false;
                            }
                        });
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end()

        return false;
    };

    this.onDetailExpand2 = function(e) {        
        var masterGrid = $(e.sender.element).getKendoGrid();
        var masterDataItem = masterGrid.dataItem(e.masterRow);
        var detailGrid = e.detailRow.find('#grid2_' + masterDataItem.id).data("kendoGrid");
        detailGrid.dataSource.read();
    };
}

var serviceReg = null;

$(function () {
    serviceReg = new ServiceReg();
    serviceReg.init();
});
