﻿function ReportList() {
    _this = this;

    this.init = function () {
        //
    };

    this.onReportDiscountGridBound = function(e) {
        drawEmptyRow('#reportDiscountGrid');
    };

    this.onStartReportBtnClick = function(id) {
        $("#windowcontainer").append("<div id='window'></div>");
        var kendoWindow = $("#window").kendoWindow({
            actions: ['close'],
            title: "Параметры отчета",
            resizable: true,
            modal: true,
            width: "450px",
            deactivate: function () {
                this.destroy();
            }
        });

        var wnd = kendoWindow.data("kendoWindow");
        wnd.content($("#action-confirmation").html());

        $("#edDateBeg").kendoDatePicker();
        $("#edDateEnd").kendoDatePicker();

        if ((id == 1) || (id == 6)) {
            $('.no-report-param').removeClass('hidden');            
        } else {
            $('.report-param').removeClass('hidden');
        };

        wnd.center().open();

        kendoWindow.find(".action-confirm,.action-cancel")
                .click(function () {
                    if ($(this).hasClass("action-confirm")) {
                        $('#report_id').val(id);
                        if ((id != 1) && (id != 6)) {
                            var dt1 = $('#edDateBeg').getKendoDatePicker().value();
                            var dt2 = $('#edDateEnd').getKendoDatePicker().value();
                            $('#date_beg').val(dt1 ? dt1.toUTCString() : null);
                            $('#date_end').val(dt2 ? dt2.toUTCString() : null);
                        };
                        kendoWindow.data("kendoWindow").close();                        
                        $('#btnSubmitProceed').trigger('click');
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end();

        return false;
    };
}

var reportList = null;

$(function () {
    reportList = new ReportList();
    reportList.init();
});
