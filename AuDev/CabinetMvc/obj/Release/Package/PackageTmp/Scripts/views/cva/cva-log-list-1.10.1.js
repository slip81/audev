﻿function CvaLogList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#cvaLogGrid', 0);

        $('#btnNodeAdd').click(function (e) {
            location = '/ClientList';
        });

        $('#btnRefresh').click(function (e) {
            refreshGrid('#cvaLogGrid');
        });
    };


    this.onClientListChange = function(e) {        
        var ddl1 = $('#salesList').getKendoDropDownList();
        ddl1.dataSource.read();
        ddl1.value();
    };

    this.onSalesListChange = function(e) {
        refreshGrid('#cvaLogGrid');
    };

    this.onSalesListDataBound = function(e) {
        refreshGrid('#cvaLogGrid');
    };

    this.getClientId = function(e) {
        var curr_client_id = -1;

        var ddl1 = $('#clientList').getKendoDropDownList();
        if (ddl1) {
            curr_client_id = ddl1.value();
        };

        return { client_id: curr_client_id };
    };

    this.getLogParams = function(e) {
        var curr_client_id = -1;
        var curr_sales_id = -1;

        var ddl1 = $('#clientList').getKendoDropDownList();
        if (ddl1) {
            curr_client_id = ddl1.value();
        };

        var ddl2 = $('#salesList').getKendoDropDownList();
        if (ddl2) {
            curr_sales_id = ddl2.value();
        };

        return { client_id: curr_client_id, sales_id: curr_sales_id };
    };

    this.onCvaLogGridDataBound = function(e) {
        drawEmptyRow("#cvaLogGrid");
    };
}

var cvaLogList = null;

$(function () {
    cvaLogList = new CvaLogList();
    cvaLogList.init();
});
