﻿function DeliveryList() {
    _this = this;

    this.init = function () {
        $('#btnDeliveryAdd>span').addClass('k-icon k-i-plus');
        $('#btnRefresh>span').addClass('k-icon k-i-refresh');
        setGridMaxHeight('#deliveryGrid', 0);

        $('#btnRefresh').click(function (e) {
            e.preventDefault();
            $('#deliveryGrid').getKendoGrid().dataSource.read();
            return false;
        });
    };

    this.onDeliveryGridDataBound = function(e) {
        drawEmptyRow('#deliveryGrid');
    };

    this.onStartDeliveryBtnClick = function(id) {
        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Запустить рассылку ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "80px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#action-confirmation").html())
            .center().open();

        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        kendoWindow.data("kendoWindow").close();
                        var postData = { delivery_id: id };
                        $.ajax({
                            type: "POST",
                            url: "StartDelivery",
                            data: JSON.stringify(postData),
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                if (response.err) {
                                    if (response.mess) {
                                        alert(response.mess);
                                    } else {
                                        alert('Ошибка при запуске рассылки');
                                    };
                                } else {
                                    $('#deliveryGrid').getKendoGrid().dataSource.read();
                                };
                                kendoWindow.data("kendoWindow").close();
                            },
                            error: function (response) {
                                alert('Ошибка при попытке запуска рассылки');
                                kendoWindow.data("kendoWindow").close();
                            }
                        });
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end();
    };

    this.onStopDeliveryBtnClick = function(id) {
        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Прервать рассылку ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "80px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#action-confirmation").html())
            .center().open();

        kendoWindow.find(".action-confirm,.action-cancel")
                .click(function () {
                    if ($(this).hasClass("action-confirm")) {
                        kendoWindow.data("kendoWindow").close();
                        var postData = { delivery_id: id };
                        $.ajax({
                            type: "POST",
                            url: "StopDelivery",
                            data: JSON.stringify(postData),
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                if (response.err) {
                                    if (response.mess) {
                                        alert(response.mess);
                                    } else {
                                        alert('Ошибка при остановке рассылки');
                                    };
                                } else {
                                    $('#deliveryGrid').getKendoGrid().dataSource.read();
                                };
                                kendoWindow.data("kendoWindow").close();
                            },
                            error: function (response) {
                                alert('Ошибка при попытке остановки рассылки');
                                kendoWindow.data("kendoWindow").close();
                            }
                        });
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end();
    };
}

var deliveryList = null;

$(function () {
    deliveryList = new DeliveryList();
    deliveryList.init();
});
