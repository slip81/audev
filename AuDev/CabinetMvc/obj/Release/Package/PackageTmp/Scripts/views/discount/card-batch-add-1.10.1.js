﻿function CardBatchAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $('.res-refresh').bind('input propertychange', _this.res_refresh);

        $(document).on('click', '#edUseCheckValue', _this.res_refresh);

        $("#btnSubmit").on("click", function (e) {
            e.preventDefault();

            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Создать карты с заданными параметрами ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#cardTypeList").data("kendoDropDownList");
            $("#card_type_id").val(combo1.dataItem()["card_type_id"]);
            var combo2 = $("#cardStatusList").data("kendoDropDownList");
            $("#card_status_id").val(combo2.dataItem()["card_status_id"]);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#batch-add-confirmation").html())
                    .center().open();

                kendoWindow.find(".batch-add-confirm,.batch-add-cancel")
                        .click(function () {
                            if ($(this).hasClass("batch-add-confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('active');
                                $('#btnSubmit').attr('disabled', true);
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });
    };

    this.res_refresh = function() {
        
        $("#edResultNumFirst").val('обновление...');
        $("#edResultNumLast").val('обновление...');

        var batchParams = {
            num_first: $("#edNumFirst").val(),
            num_count: $("#edNumCount").val(),
            next_num_first: $("#edNextNumFirst").val(),
            next_num_step: $("#edNextNumStep").val(),
            use_check_value: ($('#edUseCheckValue:checked').length > 0) ? 'true' : 'false',
            card_count: $("#edCardCount").val()
        };

        $.ajax({
            url: 'Card/BuildCardNum',            
            data: JSON.stringify(batchParams),
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                $("#edResultNumFirst").val(response.result_num_first);
                $("#edResultNumLast").val(response.result_num_last);                
            },
            error: function (response) {
                $("#edResultNumFirst").val(response.result_num_first);
                $("#edResultNumLast").val(response.result_num_last);                
            }
        });
    }

    this.onAttrChanged = function(e) {
        _this.res_refresh();
    };
}

var cardBatchAdd = null;

$(function () {
    cardBatchAdd = new CardBatchAdd();
    cardBatchAdd.init();
});
