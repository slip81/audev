﻿function ClientEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();        

        $("#copy-workplace-dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 800,
            height: 500,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            },
            open: function (event, ui) {
                $('#copy-workplace-textarea').select();                
            }
        });

        $('#btnOrderAdd').click(function (e) {
            location = 'OrderAdd?client_id=' + model_id;
        });

        $('#spanOrderActiveOnly').click(function (e) {            
            refreshGrid('#clientOrderListGrid');
        });

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#regionList").getKendoComboBox();
            var prev_region_name = $("#RegionName_Selected").val();
            var curr_region_name = combo1.text();
            if (prev_region_name != curr_region_name) {
                $("#RegionName_Selected").val(curr_region_name);
                $("#RegionName_Changed").val(true);
            } else {
                $("#RegionName_Changed").val(false);
            };

            var combo2 = $("#cityList").getKendoComboBox();
            var prev_city_name = $("#CityName_Selected").val();
            var curr_city_name = combo2.text();
            if (prev_city_name != curr_city_name) {
                $("#CityName_Selected").val(curr_city_name);
                $("#CityName_Changed").val(true);
            } else {
                $("#CityName_Changed").val(false);
            };

            var combo3 = $("#regionZoneList").data('kendoDropDownList');
            var curr_region_zone_dataItem = combo3.dataItem();
            var curr_region_zone_id = null;
            if ((curr_region_zone_dataItem) && (curr_region_zone_dataItem.zone_id)) {
                curr_region_zone_id = curr_region_zone_dataItem.zone_id;
            }
            $("#region_zone_id").val(curr_region_zone_id);

            var combo4 = $("#taxSystemList").data('kendoDropDownList');
            var curr_tax_system_type_dataItem = combo4.dataItem();
            var curr_tax_system_type_id = null;
            if ((curr_tax_system_type_dataItem) && (curr_tax_system_type_dataItem.system_type_id)) {
                curr_tax_system_type_id = curr_tax_system_type_dataItem.system_type_id;
            }
            $("#tax_system_type_id").val(curr_tax_system_type_id);

            var is_ph_eq_jur = $('#chbPhEqJur').is(':checked');
            if (is_ph_eq_jur) {
                $("#absolute_address").val($("#legal_address").val());
            };

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('k-state-active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-revert-visible');
                                $('#btnRevert').removeClass('k-state-focused');
                                $('#btnRevert').addClass('btn-revert-hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnServiceUnregDel").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Удалить отмеченные услуги ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            kendoWindow.data("kendoWindow")
                .content($("#del-confirmation").html())
                .center().open();

            kendoWindow.find(".confirm,.cancel")
                    .click(function () {
                        if ($(this).hasClass("confirm")) {
                            kendoWindow.data("kendoWindow").close();
                            var ds = $("#clientServiceUnregGrid").getKendoGrid().dataSource;
                            var post_data = kendo.stringify({ servListForDel: ds.view() });
                            $.ajax({
                                type: "POST",
                                url: "Cabinet/ClientServiceUnregDel",
                                data: post_data,
                                contentType: 'application/json; charset=windows-1251',
                                success: function (response) {
                                    kendoWindow.data("kendoWindow").close();
                                    if (response.error) {
                                        //alert(response.mess);
                                        getPopup().show(response.mess, "error");
                                        return false;
                                    } else {
                                        ds.read();
                                        return true;
                                    };
                                },
                                error: function (response) {
                                    kendoWindow.data("kendoWindow").close();                                    
                                    getPopup().show('Ошибка при отправке запроса на удаление', "error");
                                    return false;
                                }
                            });
                        } else {
                            kendoWindow.data("kendoWindow").close();
                            return false;
                        };
                    })
                 .end();
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();

            $('#btnSubmit').removeClass('k-state-active');
            $('#btnSubmit').attr('disabled', true);
            $('#btnRevert').removeClass('btn-revert-visible');
            $('#btnRevert').removeClass('k-state-focused');
            $('#btnRevert').addClass('btn-revert-hidden');
        });

        $('#chbPhEqJur').click(function (e) {
            var checked = $(this).is(':checked');
            if (checked) {
                $('#absolute_address').val($('#legal_address').val());
                $('#absolute_address').attr('readonly', 'readonly');
            } else {
                $('#absolute_address').removeAttr('readonly');
            };
        });
    };

    this.filterRegion = function () {
        return {
            region_id: $("#regionList").val()
        };
    };

    this.onRequestEnd1 = function(gridName) {
        return function (e) {
            var grid = $("#" + gridName).data("kendoGrid");
            if ((e.type == "create") || (e.type == "update")) {
                grid.dataSource.read();
            }            
        }
    };

    this.onServiceUnregGridDataBound = function (e) {
        if (firstDataBound_ClientServiceUnreg) {
            firstDataBound_ClientServiceUnreg = false;
            refreshGrid('#clientServiceParamGrid');
        };
        drawEmptyRow("#clientServiceUnregGrid");
    };

    this.onclientSalesListGridDataBound = function (e) {
        //alert('onclientSalesListGridDataBound !');
        if (needExpand_SalesList) {
            needExpand_SalesList = false;
            $('#clientSalesListGrid').getKendoGrid().expandRow($('#clientSalesListGrid tbody>tr:first'));
        };
        drawEmptyRow("#clientSalesListGrid");
    };

    this.onClientOrderListGridDataBound = function (e) {
        if (firstDataBound_ClientOrder) {
            firstDataBound_ClientOrder = false;
            refreshGrid('#clientServiceGrid');
        };
        drawEmptyRow("#clientOrderListGrid");
    };

    this.onClientServiceGridDataBound = function (e) {
        if (firstDataBound_ClientService) {
            firstDataBound_ClientService = false;
            refreshGrid('#clientServiceUnregGrid');
        };
        drawEmptyRow("#clientServiceGrid");
    };

    this.onClientUserListGridDataBound = function(e) {
        drawEmptyRow("#clientUserListGrid");
    };

    this.onServUnregCheckboxHeaderClick = function(chk) {
        var state = $(chk).is(':checked'),
            grid = $("#clientServiceUnregGrid").data("kendoGrid"),
            view = grid.dataSource.view();
        for (var i = 0; i < view.length; i++) {
            var dataRow = view[i];
            var elementRow = grid.table.find(".serv-unreg-checkbox")[i];
            if (elementRow != null) {
                var checked = elementRow.checked,
                    row = $(elementRow).closest("tr"),
                    dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));
                dataItem.ForDel = state;
                dataItem.dirty = true;
                if (state) {
                    elementRow.checked = true;
                    row.addClass("k-info-colored");
                } else {
                    elementRow.checked = false;
                    row.removeClass("k-info-colored");
                }
            }
        };
    };

    this.onServUnregCheckboxClick = function(chk) {
        var checked = $(chk).is(':checked'),
            row = $(chk).closest("tr"),
            grid = $("#clientServiceUnregGrid").data("kendoGrid"),
            dataItem = grid.dataItem(row);

        dataItem.ForDel = checked;
        dataItem.dirty = true;
        if (checked) {
            row.addClass("k-info-colored");
        } else {
            row.removeClass("k-info-colored");
        }
    };

    this.onApplyServiceClick = function(workplace_id, curr_version_id) {

        var grid = $('#grid11_' + workplace_id).getKendoGrid();
        var dataItem = grid.dataSource.get(curr_version_id);

        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Установить услугу ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "150px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#activation-confirmation").html())
            .center().open();

        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        
                        kendoWindow.data("kendoWindow").close();

                        var sendData = { client_id: model_id, workplace_id: dataItem.workplace_id, service_id: dataItem.service_id, version_id: dataItem.version_id};
                        $.ajax({
                            url: 'ServiceApply',
                            data: JSON.stringify(sendData),
                            type: "POST",
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                if (response.err) {
                                    getPopup().show(response.mess ? response.mess : 'Ошибка при установке услуги', "error");
                                    return true;
                                } else {
                                    grid.dataSource.read();
                                    return true;
                                };
                            },
                            error: function (response) {                                
                                getPopup().show('Ошибка при попытке установить услугу', "error");
                                return false;
                            }
                        });
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end()

        return false;
    };

    this.onDelServiceClick = function (workplace_id, curr_version_id) {
        var grid = $('#grid11_' + workplace_id).getKendoGrid();
        var dataItem = grid.dataSource.get(curr_version_id);

        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Убрать услугу ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "80px"
        });

        kendoWindow.data("kendoWindow")
            .content($("#remove-confirmation").html())
            .center().open();

        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {
                        kendoWindow.data("kendoWindow").close();

                        var sendData = { client_id: model_id, workplace_id: dataItem.workplace_id, service_id: dataItem.service_id, version_id: dataItem.version_id };
                        $.ajax({
                            url: 'Cabinet/ServiceRemove',
                            data: JSON.stringify(sendData),
                            type: "POST",
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                grid.dataSource.read();
                                return true;
                            },
                            error: function (response) {                                
                                getPopup().show('Ошибка при попытке убрать услугу', "error");
                                return false;
                            }
                        });
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end()

        return false;
    };

    this.onGrid1Expand = function(e) {        
        var masterGrid = $(e.sender.element).getKendoGrid();
        var masterDataItem = masterGrid.dataItem(e.masterRow);
        var detailGrid = e.detailRow.find('#grid11_' + masterDataItem.id).data("kendoGrid");
        detailGrid.dataSource.read();
    };

    this.onTabClientShow = function(e) {        
        if ($(e.item).text() === 'Услуги') {
            if (firstTabClientShow) {
                firstTabClientShow = false;
                refreshGrid('#clientOrderListGrid');
            }            
            /*
            refreshGrid('#clientServiceGrid');
            refreshGrid('#clientServiceUnregGrid');
            refreshGrid('#clientServiceParamGrid');
            */
        }
    };

    this.onRegionListChange = function(e) {        
        $("#cityList").getKendoComboBox().enable(true);
    };

    this.getOrderListParams = function(e) {
        return { is_active_only: $('#chbOrderActiveOnly').is(':checked') };
    };

    this.onGrid1DataBound = function (e) {
        if (needExpand_workplaceId) {
            var grid = $(e.sender.element).getKendoGrid();
            var ds = grid.dataSource;
            var dataItem = ds.get(needExpand_workplaceId);
            if (dataItem) {
                var row = grid.tbody.find("tr[data-uid='" + dataItem.uid + "']");
                if (row) {
                    grid.expandRow($(row));
                }
            }
        }
        $('.copy-workplace').each(function (i) {
            $(this).children('span').first().addClass('k-icon k-i-restore');
        });
    };

    this.onGrid1Edit = function (e) {
        $(e.container.find(".k-button.k-grid-update")).prop("title", "Сохранить");
        $(e.container.find(".k-button.k-grid-update")).attr("title", "Сохранить");
        $(e.container.find(".k-button.k-grid-cancel")).prop("title", "Отмена");
        $(e.container.find(".k-button.k-grid-cancel")).attr("title", "Отмена");
    };

    this.onGrid1Cancel = function (e) {
        setTimeout(function () {
            $('.copy-workplace').each(function (i) {
                var cell = $(this).children('span').first();
                if (!cell.hasClass('k-i-restore')) {
                    cell.addClass('k-icon k-i-restore');
                };
            });
        }, 300);
    };

    this.onBtnCopyWorkplaceClick = function(gridName) {
        return function (e) {
            workplaceText = '';
            var row = $(e.target).closest('tr');
            var grid = $('#' + gridName).getKendoGrid();
            var dataItem = grid.dataItem(row);            
            workplaceText = dataItem.description + '     ' + dataItem.registration_key + '\n';
            needShowCopyWindow = true;
            grid.collapseRow(row);
            grid.expandRow(row);
        };
    };

    this.onGrid11DataBound = function (gridName) {
        return function (e) {
            if (needShowCopyWindow) {
                needShowCopyWindow = false;

                var text2 = '';
                var grid = $('#' + gridName).getKendoGrid();
                var view = grid.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    var dataItem = grid.dataItem(grid.tbody.find("tr").eq(i));
                    if (dataItem.DelServiceLink) {
                        text2 = text2 + dataItem.service_name + '     ' + dataItem.version_name + '     ' + (dataItem.activation_key ? dataItem.activation_key : '') + '\n';
                    };
                };

                $('#copy-workplace-textarea').val(workplaceText + '\n' + text2);
                $('#copy-workplace-dialog').dialog('open');
            };
        };
    };

    this.onSalesSearchKeyup = function (e) {
        if (e.keyCode == 13) {
            _this.onSalesSearchBtnClick(e)
        };
    };

    this.onSalesSearchBtnClick = function (e) {
        var workplace = $('#sales-search-workplace').val();
        var key = $('#sales-search-key').val();
        var grid = $('#clientSalesListGrid').getKendoGrid();
        var gridFilter = { logic: "and", filters: [] };
        needExpand_SalesList = false;
        needExpand_workplaceId = null;
        if ((workplace) || (key)) {
            $('#file-wait').removeClass('hidden');
            $.ajax({
                url: url_SalesSearch,
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ client_id: model_id, workplace: workplace, key: key }),
                success: function (response) {
                    $('#file-wait').addClass('hidden');
                    if ((response == null) || (response.err)) {
                        getPopup().show(response == null ? 'Ошибка при попытке найти рабочее место' : response.mess, "error");
                    } else {
                        if (response.sales_id != null) {
                            needExpand_SalesList = true;
                            needExpand_workplaceId = response.workplace_id;
                            gridFilter.filters.push({ field: "sales_id", operator: "eq", value: response.sales_id });                            
                            grid.dataSource.filter(gridFilter);
                        } else {
                            getPopup().show('Рабочее место не найдено', "error");
                        };
                    };
                },
                error: function (response) {
                    $('#file-wait').addClass('hidden');
                    getPopup().show('Ошибка поиска рабочего места', "error");
                }
            });
        } else {
            grid.dataSource.filter(gridFilter);
        };
    };

    this.onChbApplyServiceAllClick = function (e) {
        //alert('onChbApplyServiceAllClick !');
        var is_checked = $(e).is(':checked');
        var serviceGrid = $(e).parents('.k-grid').first();
        serviceGrid.find('.chb-apply-service').prop('checked', is_checked);
    };

    this.onBtnApplyServiceAllClick = function (e) {
        var kendoWindow = $("<div />").kendoWindow({
            actions: [],
            title: "Установить отмеченные услуги ?",
            resizable: false,
            modal: true,
            width: "400px",
            height: "150px"
        });

        var serviceGrid = $(e).parents('.k-grid').first();
        var grid = serviceGrid.getKendoGrid();
        var serviceArray = [];
        var workplace_id = null;

        $.each(serviceGrid.find('.chb-apply-service'), function (i, el) {
            var is_checked = $(el).is(':checked');
            if (is_checked) {
                var row = $(el).closest('tr');
                var dataItem = grid.dataItem(row);
                var serv = {};
                serv.service_id = dataItem.service_id;
                serv.id = dataItem.version_id;                
                serviceArray.push(serv);
                workplace_id = dataItem.workplace_id;
            }
        });

        var serviceList = JSON.stringify(serviceArray);

        kendoWindow.data("kendoWindow")
            .content($("#activation-confirmation").html())
            .center().open();

        kendoWindow.find(".confirm,.cancel")
                .click(function () {
                    if ($(this).hasClass("confirm")) {

                        kendoWindow.data("kendoWindow").close();

                        var sendData = { client_id: model_id, workplace_id: workplace_id, serviceList: serviceList };
                        $.ajax({
                            url: 'ServiceListApply',
                            data: JSON.stringify(sendData),
                            type: "POST",
                            contentType: 'application/json; charset=windows-1251',
                            success: function (response) {
                                if (response.err) {
                                    getPopup().show(response.mess ? response.mess : 'Ошибка при установке услуги', "error");
                                    serviceGrid.find('.chb-apply-service-all').prop('checked', false);
                                    grid.dataSource.read();
                                    return true;
                                } else {
                                    serviceGrid.find('.chb-apply-service-all').prop('checked', false);
                                    grid.dataSource.read();
                                    return true;
                                };
                            },
                            error: function (response) {                                
                                getPopup().show('Ошибка при попытке установить услуги', "error");
                                serviceGrid.find('.chb-apply-service-all').prop('checked', false);
                                return false;
                            }
                        });
                        return true;
                    } else {
                        kendoWindow.data("kendoWindow").close();
                        return false;
                    };
                })
             .end()

        return false;
    };

    function getPopup() {
        if (!popupNotification)
            popupNotification = $("#popupNotification").data("kendoNotification");
        return popupNotification;
    };
}

var clientEdit = null;

var workplaceText = '';
var needShowCopyWindow = false;
var firstTabClientShow = true;
var firstDataBound_ClientOrder = true;
var firstDataBound_ClientServiceUnreg = true;
var firstDataBound_ClientService = true;

$(function () {
    clientEdit = new ClientEdit();
    clientEdit.init();
});
