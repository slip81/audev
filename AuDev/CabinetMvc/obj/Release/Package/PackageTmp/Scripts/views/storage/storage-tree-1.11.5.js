﻿function StorageTree() {
    _this = this;

    this.init = function () {        
        setControlHeight('#storageTreeMainSplitter', 0);
               
        $('.file-upload-header>span.k-link').click(function (e) {
            $('.k-widget .k-upload').toggle();
            $(this).toggleClass('k-i-arrow-n k-i-arrow-s');
            $(this).parent('div').toggleClass('noborder-bottom');
        });

        $(document).kendoTooltip({
            filter: "#fileTree span.k-in",
            position: "top",
            width: 250,
            showAfter: 600,
            content: kendo.template($('#file-tooltip-template').html())
        });
    };

    this.onStorageFileSearchKeyup = function (e) {
        if (e.keyCode == 13) {
            checkAuthorization(url_IsAuthorized, url_Login);
            fileList_fromSearch = null;
            $('div span.k-in.highlight').removeClass('highlight');
            var file_search_str = $('#search-file').val();
            if (file_search_str) {
                $('#file-wait').removeClass('hidden');
                $.ajax({
                    url: url_StorageFileSearch,
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify({ file_name: file_search_str }),
                    success: function (response) {
                        $('#file-wait').addClass('hidden');
                        if ((response == null) || (response.err)) {
                            $('div span.k-in.highlight').removeClass('highlight');
                            popupNotification.show(response == null ? 'Ошибка при попытке найти файлы' : response.mess, "error");
                        } else {
                            fileList_fromSearch = $.parseJSON(response.fileList);
                            if ((fileList_fromSearch == null) || (fileList_fromSearch.length <= 0)) {
                                $('div span.k-in.highlight').removeClass('highlight');
                                popupNotification.show('Файлы не найдены', "error");
                            } else {
                                $.each(fileList_fromSearch, function (i, item) {
                                    highlightFolder(item.folder_id);
                                    highlightFile(item.file_id);
                                });
                            }
                        };
                    },
                    error: function (response) {
                        $('div span.k-in.highlight').removeClass('highlight');
                        $('#file-wait').addClass('hidden');                        
                        popupNotification.show('Ошибка поиска файлов', "error");
                    }
                });
            };
        };
    };

    this.onFolderTreeDrop = function (e) {
        var treeView = this,
            sourceItem = treeView.dataItem(e.sourceNode),
            destinationItem = treeView.dataItem(e.destinationNode),
            parent = destinationItem;
        if (e.dropPosition != "over") {
            parent = parent.parentNode();
        };

        checkAuthorization(url_IsAuthorized, url_Login);

        if (sourceItem.id == destinationItem.id) {
            e.setValid(false);
            return false;
        };       
        
        $('#file-wait').removeClass('hidden');
        var ok = true;
        $.ajax({
            url: url_StorageFolderTreeMove,
            type: "POST",
            async: false,
            data: {
                id: sourceItem.id,
                parent_id: parent ? parent.id : null
            },
            success: function (response) {
                $('#file-wait').addClass('hidden');
                if ((response == null) || (response.err)) {
                    popupNotification.show(response == null ? 'Ошибка при попытке переместить папку' : response.mess, "error");
                    ok = false;
                    //e.setValid(false);
                    e.preventDefault();
                    return false;                    
                } else {                    
                    ok = true;
                    return true;
                };
            },
            error: function (response) {
                $('#file-wait').addClass('hidden');
                popupNotification.show('Ошибка перемещения папки', "error");
                ok = false;
                //e.setValid(false);
                e.preventDefault();
                return false;                
            }
        });
    };

    this.onFolderTreeSelect = function (e) {        
        var treeView = this,
            currentItem = treeView.dataItem(e.node);        
        
        refreshFile(null);

        if (!currentItem) {
            $('#storage-folder-info').text('');
            $('#storage-folder-link1').addClass('hidden');
            return;
        }
        var parents = $(e.node).add($(e.node).parentsUntil('.k-treeview', '.k-item'));
        var nodePath = $.map(parents, function (parent) {
            return $(parent).find('>div span.k-in span.storage-tree-view-simple').text();
        });

        var folder_link_in_storage = url_StorageTreeLink + '?folder_id=' + currentItem.id;

        $('#storage-folder-storage-link').attr("href", folder_link_in_storage);
        $('#storage-folder-storage-link').html(folder_link_in_storage);

        $('#storage-folder-info').text(nodePath.join("\\"));
        $('#storage-folder-link1').removeClass('hidden');
    };

    this.onFolderTreeChange = function (e) {
        if (!goto_fileId) {
            refreshFileTree();
        }
    };

    this.onFileTreeDrop = function (e) {
        var srcTreeView = this,
            sourceItem = srcTreeView.dataItem(e.sourceNode);        

        var destTreeView = $(e.destinationNode).closest("[data-role=treeview]").getKendoTreeView();
        if (!destTreeView) {
            e.preventDefault();
            return false;
        };
        var destinationItem = destTreeView.dataItem(e.destinationNode),
            parent = destinationItem;
        if (e.dropPosition != "over") {
            parent = parent.parentNode();
        };

        if ((destinationItem.folder_id) && (sourceItem.folder_id == destinationItem.folder_id)) {
            e.preventDefault();
            return false;
        };

        if (sourceItem.folder_id == destinationItem.id) {
            e.preventDefault();
            return false;
        }

        if (!parent) {
            e.preventDefault();
            return false;
        }

        checkAuthorization(url_IsAuthorized, url_Login);

        e.preventDefault();
        $('#file-wait').removeClass('hidden');
        $.ajax({
            url: url_StorageFileTreeMove,
            type: "POST",
            data: {
                id: sourceItem.id,
                parent_id: parent.id,
                item_type: sourceItem.item_type
            },
            success: function (response) {
                $('#file-wait').addClass('hidden');
                if ((response == null) || (response.err)) {                    
                    popupNotification.show(response == null ? 'Ошибка при попытке переместить файл' : response.mess, "error");
                } else {
                    srcTreeView.remove(e.sourceNode);
                };
            },
            error: function (response) {
                $('#file-wait').addClass('hidden');
                popupNotification.show('Ошибка перемещения файла', "error");
            }
        });
    };

    this.onFileTreeDataBound = function (e) {
        $('#file-wait').addClass('hidden');
        var foldertree = $("#folderTree").getKendoTreeView();
        var filetree = $("#fileTree").getKendoTreeView();
        if (currViewMode == 0) {
            $('.storage-tree-view-ext').addClass('hidden');
        } else {
            $('.storage-tree-view-ext').removeClass('hidden');
        };
        if (firstDataBound_fileTree) {
            firstDataBound_fileTree = false;
            foldertree.select(".k-first");
            foldertree.trigger("select", {
                //node: $("#folderTree .k-item:first")[0]
                node: $("#folderTree .k-first")[0]
            });
            popupNotification = $("#popupNotification").data("kendoNotification");
        };
        if (goto_folderId) {
            gotoFolder();
        } else if (goto_fileId) {
            var dataItem = filetree.dataSource.get(goto_fileId);
            goto_fileId = null;
            if (dataItem) {
                var fileNode = filetree.findByUid(dataItem.uid);
                if (fileNode) {
                    filetree.select(fileNode);
                }
            }
        } else if (fileList_fromSearch) {
            $.each(fileList_fromSearch, function (i, item) {
                highlightFile(item.file_id);
            });
        };
    };

    this.onStorageFolderRootAddClick = function (e) {
        checkAuthorization(url_IsAuthorized, url_Login);
        showEditWindow("#edit-templ", "Новая корневая папка", "", "", "", "", function () {
            var folder_name_new = $('#win-edit-value').val();
            if ((!folder_name_new) || (folder_name_new.length <= 0)) {                
                popupNotification.show('Не задано наименование папки', "error");
                return;
            };            
            addFolder(folder_name_new, null);
        });
    };

    this.onStorageFolderAddClick = function (e) {        
        var treeview = $("#folderTree").getKendoTreeView();
        var parentNode = treeview.select();
        if ((!parentNode) || (parentNode.length <= 0)) {            
            popupNotification.show('Не выбрана родителькая папка', "error");
            return;
        };

        checkAuthorization(url_IsAuthorized, url_Login);

        showEditWindow("#edit-templ", "Новая папка", "", "", "", "", function () {
            var folder_name_new = $('#win-edit-value').val();
            if ((!folder_name_new) || (folder_name_new.length <= 0)) {                
                popupNotification.show('Не задано наименование папки', "error");
                return;
            };            
            addFolder(folder_name_new, parentNode);
        });

    };

    this.onStorageFolderEditClick = function (e) {
        var treeview = $("#folderTree").getKendoTreeView();
        var currentNode = treeview.select();
        if ((!currentNode) || (currentNode.length <= 0)) {            
            popupNotification.show('Не выбрана папка', "error");
            return;
        };
        var dataItem = treeview.dataItem(currentNode);
        var folder_id_current = dataItem.id;
        if (!folder_id_current) {
            popupNotification.show('Не выбрана папка', "error");
            return;
        };

        checkAuthorization(url_IsAuthorized, url_Login);

        //var dataItemText = dataItemText;
        var dataItemText = currentNode.find('.storage-tree-view-simple').first().text();
        var extMode = !currentNode.hasClass('hidden');
        showEditWindow("#edit-templ", "Переименование папки", "", dataItemText, "", "", function () {
            var folder_name_new = $('#win-edit-value').val();
            if ((!folder_name_new) || (folder_name_new.length <= 0)) {                
                popupNotification.show('Не задано наименование папки', "error");
                return;
            };            
            $.ajax({
                type: "POST",
                url: url_StorageFolderTreeEdit,
                data: JSON.stringify({ folder_id: folder_id_current, folder_name: folder_name_new }),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if ((response == null) || (response.err)) {                        
                        popupNotification.show(response == null ? 'Ошибка при попытке переименовать папку' : response.mess, "error");
                    } else {
                        //treeview.text(currentNode, response.folder.folder_name);                        
                        //treeview.text(currentNode, getFolderText(response.folder, extMode));
                        renameFolder(currentNode, response.folder, false);
                    };
                },
                error: function (response) {                    
                    popupNotification.show('Ошибка переименовывания папки', "error");
                }
            });
        });
    };

    this.onStorageFolderLinkClick = function (e) {
        var treeview = $("#folderTree").getKendoTreeView();
        var currentNode = treeview.select();
        if ((!currentNode) || (currentNode.length <= 0)) {
            popupNotification.show('Не выбрана папка', "error");
            return;
        };
        var dataItem = treeview.dataItem(currentNode);
        var folder_id_current = dataItem.id;
        if (!folder_id_current) {
            popupNotification.show('Не выбрана папка', "error");
            return;
        };        

        var folder_link_in_storage = url_StorageTreeLink + '?folder_id=' + folder_id_current;
        showEditWindow("#edit-templ", "Ссылка на папку", "", folder_link_in_storage, "", "", null);
    };

    this.onStorageFolderDelClick = function (e) {
        var treeview = $("#folderTree").getKendoTreeView();
        var currentNode = treeview.select();
        if ((!currentNode) || (currentNode.length <= 0)) {            
            popupNotification.show('Не выбрана папка', "error");
            return;
        };
        var dataItem = treeview.dataItem(currentNode);
        var folder_id_current = dataItem.id;
        if (!folder_id_current) {
            popupNotification.show('Не выбрана папка', "error");
            return;
        };

        checkAuthorization(url_IsAuthorized, url_Login);
        
        $('#file-wait').removeClass('hidden');
        showConfirmWindow('#confirm-templ', 'Удалить папку и все вложенные папки ?', function () {
            $.ajax({
                type: "POST",
                url: url_StorageFolderTreeDel,
                data: JSON.stringify({ folder_id: folder_id_current }),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    $('#file-wait').addClass('hidden');
                    if ((response == null) || (response.err)) {                        
                        popupNotification.show(response == null ? 'Ошибка при попытке удалить папку' : response.mess, "error");
                    } else {
                        treeview.remove(currentNode);                                                
                        var tabIndex = $('#tabStorageInfo').getKendoTabStrip().select().index();
                        if (tabIndex == 1) {
                            refreshStorageTrashGrid();
                        };
                    };
                },
                error: function (response) {
                    $('#file-wait').addClass('hidden');
                    popupNotification.show('Ошибка удаления папки', "error");
                }
            });
        });
    };


    this.onStorageTrashGridDataBound = function (e) {
        drawEmptyRow('#storageTrashGrid');
        //
        if (firstDataBound_trash) {
            firstDataBound_trash = false;
            $("#storageTrashGrid").kendoDraggable({
                filter: "tr",
                hint: function (element) {
                    //return element.clone().css({
                    return element.clone().find('td:first-child').css({
                        "opacity": 0.6                        
                    });
                }
            });
            //$("#folderTree li").kendoDropTarget({
            $("#left-pane").kendoDropTarget({
                drop: onStorageTrashGridToLeftPaneDrop
            });
            $("#folderTree li").kendoDropTarget({
                drop: onStorageTrashGridToFolderTreeDrop
            });
            //$("#fileTree li").kendoDropTarget({
            //$("#right-pane:not(#storageFileToolBar, #upload-div)").kendoDropTarget({            
            $("#right-pane").kendoDropTarget({
                drop: onStorageTrashGridToFileTreeDrop
            });
        }
    };

    this.onStorageFullLogGridDataBound = function (e) {
        drawEmptyRow('#storageFullLogGrid');
    };

    this.onStorageFolderExpandClick = function (e) {
        $("#folderTree").getKendoTreeView().expand('.k-item');
    };

    this.onStorageFolderCollapseClick = function (e) {
        $("#folderTree").getKendoTreeView().collapse('.k-item');
    };

    this.onStorageFolderDownloadAllClick = function (e) {
        checkAuthorization(url_IsAuthorized, url_Login);
        $('#file-wait').removeClass('hidden');
        showConfirmWindow('#confirm-templ', 'Скачать все хранилище ?', function () {            
            $.ajax({
                url: url_StorageFolderTreeDownloadAll,
                type: "POST",
                success: function (response) {
                    $('#file-wait').addClass('hidden');
                    if ((response == null) || (response.err)) {                        
                        popupNotification.show(response == null ? 'Ошибка при попытке скачивания хранилища' : response.mess, "error");
                    } else {
                        location.href = response.storage_download_link;
                    };
                },
                error: function (response) {
                    $('#file-wait').addClass('hidden');
                    popupNotification.show('Ошибка скачивания хранилища', "error");                    
                }
            });
        });
    };

    this.onStorageFileAddClick = function(e) {        
        $('#uplFile').trigger('click');
    };

    this.onStorageLinkAddClick = function (e) {
        var treeview = $("#folderTree").getKendoTreeView();
        var parentNode = treeview.select();
        if ((!parentNode) || (parentNode.length <= 0)) {            
            popupNotification.show('Не выбрана родителькая папка', "error");
            return;
        };
        var folder_id = treeview.dataItem(parentNode).id;
        if (!folder_id) {
            popupNotification.show('Не выбрана родителькая папка', "error");
            return;
        };

        checkAuthorization(url_IsAuthorized, url_Login);

        showEditWindow("#edit2-templ", "Добавление ссылки", "Адрес ссылки", "", "Описание", "Новая ссылка", function () {
            var link_url = $('#win-edit-value').val();
            var link_descr = $('#win-edit2-value').val();
            if ((!link_url) || (link_url.length <= 0)) {                
                popupNotification.show('Не задан адрес ссылки', "error");
                return;
            };
            $.ajax({
                url: url_StorageLinkAdd,
                type: "POST",
                data: {
                    folder_id: folder_id,
                    link_url: link_url,
                    link_descr: link_descr
                },
                success: function (response) {
                    if ((response == null) || (response.err)) {                        
                        popupNotification.show(response == null ? 'Ошибка при попытке добавить ссылку' : response.mess, "error");
                    } else {
                        refreshFileTree();
                    };
                },
                error: function (response) {                    
                    popupNotification.show('Ошибка добавления ссылки', "error");
                }
            });
        });
    };

    this.onStorageFileEditClick = function (e) {        
        var treeview = $("#fileTree").getKendoTreeView();
        var currentNode = treeview.select();
        if ((!currentNode) || (currentNode.length <= 0)) {            
            popupNotification.show('Не выбран файл', "error");
            return;
        };
        var dataItem = treeview.dataItem(currentNode);
        var file_id_current = dataItem.id;
        if (!file_id_current) {            
            popupNotification.show('Не выбран файл', "error");
            return;
        };

        checkAuthorization(url_IsAuthorized, url_Login);

        showEditWindow("#edit-templ", "Переименование файла", "", dataItem.file_name, "", "", function () {
            var file_name_new = $('#win-edit-value').val();
            if ((!file_name_new) || (file_name_new.length <= 0)) {                
                popupNotification.show('Не задано наименование', "error");
                return;
            };            
            $.ajax({
                type: "POST",
                url: url_StorageFileTreeEdit,
                data: JSON.stringify({ file_id: file_id_current, file_name: file_name_new, item_type: dataItem.item_type }),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if ((response == null) || (response.err)) {                        
                        popupNotification.show(response == null ? 'Ошибка при попытке переименовать файл' : response.mess, "error");
                    } else {                        
                        //treeview.text(currentNode, response.file_name_full);
                        renameFile(currentNode, response.file, false)
                    };
                },
                error: function (response) {                    
                    popupNotification.show('Ошибка переименовывания файла', "error");
                }
            });
        });
    };

    this.onStorageFileLinkClick = function (e) {
        var treeview = $("#fileTree").getKendoTreeView();
        var currentNode = treeview.select();
        if ((!currentNode) || (currentNode.length <= 0)) {
            popupNotification.show('Не выбран файл', "error");
            return;
        };
        var dataItem = treeview.dataItem(currentNode);
        var file_id_current = dataItem.id;
        if (!file_id_current) {
            popupNotification.show('Не выбран файл', "error");
            return;
        };        

        var file_link_in_storage = url_StorageTreeLink + '?file_id=' + file_id_current;
        showEditWindow("#edit2-templ", "Ссылки на файл", "Ссылка на хранилище:", file_link_in_storage, "Ссылка на скачивание:", dataItem.download_link, null);
    };

    this.onStorageFileDelClick = function (e) {
        var treeview = $("#fileTree").getKendoTreeView();
        var currentNode = treeview.select();
        if ((!currentNode) || (currentNode.length <= 0)) {            
            popupNotification.show('Не выбран файл', "error");
            return;
        };
        var dataItem = treeview.dataItem(currentNode);
        var file_id_current = dataItem.id;
        if (!file_id_current) {
            popupNotification.show('Не выбран файл', "error");
            return;
        };

        checkAuthorization(url_IsAuthorized, url_Login);

        showConfirmWindow('#confirm-templ', 'Удалить файл ?', function () {
            $.ajax({
                type: "POST",
                url: url_StorageFileTreeDel,
                data: JSON.stringify({ file_id: file_id_current, item_type: dataItem.item_type }),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if ((response == null) || (response.err)) {                        
                        popupNotification.show(response == null ? 'Ошибка при попытке удалить файл' : response.mess, "error");
                    } else {
                        treeview.remove(currentNode);
                        var tabIndex = $('#tabStorageInfo').getKendoTabStrip().select().index();
                        if (tabIndex == 1) {
                            refreshStorageTrashGrid();
                        };
                    };
                },
                error: function (response) {                    
                    popupNotification.show('Ошибка удаления файла', "error");
                }
            });
        });
    };

    this.onFileTreeSelect = function (e) {
        var treeView = this,
            currentDataItem = treeView.dataItem(e.node);

        refreshFile(currentDataItem);
    };

    this.getFileRequestData = function (e) {
        var treeview = $("#folderTree").getKendoTreeView();
        var currentNode = treeview.select();
        if ((!currentNode) || (currentNode.length <= 0)) {
            return { folder_id: -1 };
        };
        var dataItem = treeview.dataItem(currentNode);
        if (!dataItem) {
            return { folder_id: -1 };
        };
        return { folder_id: dataItem.id };
    };

    this.onUplFileUpload = function (e) {
        var curr_folder_id = null;
        var treeview = $("#folderTree").getKendoTreeView();
        var currentNode = treeview.select();
        if ((!currentNode) || (currentNode.length <= 0)) {            
            popupNotification.show("Не выбрана папка", "error");
            e.preventDefault();
            return;
        };
        var dataItem = treeview.dataItem(currentNode);
        if (!dataItem) {            
            popupNotification.show("Не выбрана папка", "error");
            e.preventDefault();
            return;
        };

        checkAuthorization(url_IsAuthorized, url_Login);

        var ok = true;
        var fileDate = null;
        var files = e.files;                
        $.each(files, function () {
            if (this.size > maxFileSize) {
                popupNotification.show("Файл превышает допустимый размер (" + maxFileSize + ")", "error");
                e.preventDefault();
                ok = false;
                return;
            };
            var ind = allowedExtentions.indexOf(this.extension.toLowerCase());
            if (ind < 0) {
                //alert("Тип файла не поддерживается (" + this.extension + ")");
                popupNotification.show("Тип файла не поддерживается (" + this.extension + ")", "error");
                e.preventDefault();
                ok = false;
                return;
            };
            fileDate = this.rawFile.lastModifiedDate;
        });

        if (!ok) return;

        e.data = { folder_id: dataItem.id, overwrite: $('#chb-overwrite').is(':checked'), file_date: fileDate };
    };

    this.onUplFileSuccess = function (e) {
        if (e.operation == 'upload') {
            $("li.k-file-success>span[title='" + e.files[0].name + "']").next(".k-upload-status").find(".k-upload-err-ext").hide();
            refreshFileTree();
        };
        return true;
    };

    this.onUplFileError = function (e) {        
        var err = e.XMLHttpRequest.responseText;
        $("li.k-file-error>span[title='" + e.files[0].name + "']").next(".k-upload-status").html("<span class='k-upload-err-ext'><span class='k-icon k-warning'></span><span style='padding-left: 5px;'>" + err + "</span>" + "<button class='k-button k-button-bare k-upload-action' type='button'><span class='k-icon k-i-refresh k-retry' title='Повторить'></span></button></span>");

    };

    this.onUplFileComplete = function (e) {        
        $('strong.k-upload-status-total').append('<span class="k-link k-icon k-i-close" title="Очистить список файлов" onclick="_this.btnUploadClearClick()" style="margin-left: 10px"></span>');
    };

    this.onFolderTreeContextMenuSelect = function (e) {
        var treeview = $("#folderTree").getKendoTreeView();                
        var currentNode = $('#folderTree_tv_active');
        var dataItem = treeview.dataItem(currentNode);
        if (currentNode)
            treeview.select(currentNode);

        var menu_item_id = $(e.item).attr('id');
        if (menu_item_id == 'menu-folder-root-add') {
            return _this.onStorageFolderRootAddClick(e);
        } else if (menu_item_id == 'menu-folder-add') {
            return _this.onStorageFolderAddClick(e);
        } else if (menu_item_id == 'menu-folder-edit') {
            return _this.onStorageFolderEditClick(e);
        } else if (menu_item_id == 'menu-folder-del') {
            return _this.onStorageFolderDelClick(e);
        } else if (menu_item_id == 'menu-folder-link') {
            return _this.onStorageFolderLinkClick(e);
        } else if (menu_item_id == 'menu-folder-expand') {
            return _this.onStorageFolderExpandClick(e);
        } else if (menu_item_id == 'menu-folder-collapse') {
            return _this.onStorageFolderCollapseClick(e);
        } else if (menu_item_id == 'menu-folder-download-all') {
            return _this.onStorageFolderDownloadAllClick(e);
        } else {
            return false;
        };        
    };


    this.onFileTreeContextMenuSelect = function (e) {
        var treeview = $("#fileTree").getKendoTreeView();
        var currentNode = $('#fileTree_tv_active');
        var dataItem = treeview.dataItem(currentNode);
        if (currentNode)
            treeview.select(currentNode);        

        var menu_item_id = $(e.item).attr('id');
        if (menu_item_id == 'menu-file-add') {
            return _this.onStorageFileAddClick(e);
        } else if (menu_item_id == 'menu-file-link') {
            return _this.onStorageFileLinkClick(e);        
        } else if (menu_item_id == 'menu-link-add') {
            return _this.onStorageLinkAddClick(e);
        } else if (menu_item_id == 'menu-file-edit') {
            return _this.onStorageFileEditClick(e);
        } else if (menu_item_id == 'menu-file-del') {
            return _this.onStorageFileDelClick(e);
        } else if (menu_item_id == 'menu-file-log') {
            return _this.onStorageFileLogClick(e);
        } else {
            return false;
        };
    };

    this.onFileTreeContextMenuActivate = function (e) {
        var treeview = $("#fileTree").getKendoTreeView();
        var currentNode = $('#fileTree_tv_active');
        var dataItem = treeview.dataItem(currentNode);
        if (currentNode)
            treeview.select(currentNode);
        refreshFile(dataItem);
    };

    this.onStorageFileLogClick = function (e) {
        var treeview = $("#fileTree").getKendoTreeView();
        var currentNode = $('#fileTree_tv_active');
        if ((!currentNode) || (currentNode.length <= 0))
            return;        
        $("#winStorageLog").data("kendoWindow").open().center();
    };

    this.onWinStorageLogActivate = function (e) {
        checkAuthorization(url_IsAuthorized, url_Login);
        $('#storageLogGrid').getKendoGrid().dataSource.read();
    };

    this.getStorageLogGridData = function (e) {
        var treeview = $("#fileTree").getKendoTreeView();
        var currentNode = $('#fileTree_tv_active');
        var dataItem = treeview.dataItem(currentNode);
        if (dataItem)
            return { file_id: dataItem.file_id };
        else
            return { file_id: null };
    };

    this.btnUploadClearClick = function () {        
        $(".k-widget.k-upload").find("ul").remove();
        $('.k-dropzone>.k-upload-status').hide();
    };

    this.onBtnStorageTrashClearClick = function (e) {
        checkAuthorization(url_IsAuthorized, url_Login);
        $('#btnStorageTrashClear').addClass('k-state-disabled');
        $.ajax({
            url: url_StorageTrashClear,
            type: "POST",
            data: {},
            success: function (response) {
                $('#btnStorageTrashClear').removeClass('k-state-disabled');
                if ((response == null) || (response.err)) {
                    popupNotification.show(response.mess ? response.mess : 'Ошибка очистки корзины', "error");
                } else {
                    refreshStorageTrashGrid();                    
                };
            },
            error: function (response) {
                $('#btnStorageTrashClear').removeClass('k-state-disabled');
                popupNotification.show('Ошибка очистки корзины', "error");
            }
        });
    };

    this.onTabStorageInfoActivate = function (e) {
        var ind = $(e.item).index();
        if (ind == 1) {
            refreshStorageTrashGrid();
        } else if (ind == 2) {
            refreshStorageFullLogGrid();
        }
    };

    this.onStorageFolderToolBarToggle = function (e) {
        //var view = e.target.text().toLowerCase();
        //if (view == "простой вид") {
        var btnId = e.target.attr('id');
        if (btnId == 'btn-folder-view-simple') {
            currViewMode = 0;
            $('.storage-tree-view-ext').addClass('hidden');
        } else {
            currViewMode = 1;
            $('.storage-tree-view-ext').removeClass('hidden');
        };
    };

    // private

    function addFolder(folder_name, parent) {
        var treeview = $("#folderTree").getKendoTreeView();
        var parentNode = parent;
        var parent_id_new = null;
        if ((parentNode) && (parentNode.length > 0)) {
            parent_id_new = treeview.dataItem(parentNode).id;
        };
        $.ajax({
            type: "POST",
            url: url_StorageFolderTreeAdd,
            data: JSON.stringify({ folder_name: folder_name, parent_id: parent_id_new }),
            contentType: 'application/json; charset=windows-1251',
            success: function (response) {
                if ((response == null) || (response.err)) {                    
                    popupNotification.show(response == null ? 'Ошибка при попытке добавить папку' : response.mess, "error");
                } else {
                    treeview.append({                        
                        //text: response.folder.folder_name,
                        text: getFolderText(response.folder, false),
                        id: response.folder.id,
                        hasChildren: response.folder.hasChildren,
                        spriteCssClass: 'storage storage-folder',
                        encoded: false
                    }, parentNode);
                };
            },
            error: function (response) {                
                popupNotification.show('Ошибка добавления папки', "error");
            }
        });
    };

    function refreshFileTree() {
        checkAuthorization(url_IsAuthorized, url_Login);
        $('#file-wait').removeClass('hidden');
        $('#fileTree').getKendoTreeView().dataSource.read();
    };

    function refreshFile(currentDataItem) {        
        if (currentDataItem) {
            $("#storage-file-download, #menu-file-download>a.k-link").removeAttr("disabled");            
            $("#storage-file-download, #menu-file-download, #menu-file-download>a.k-link").removeClass("k-state-disabled");
            $("#storage-file-download, #menu-file-download>a.k-link").attr("href", currentDataItem.download_link);            
            $("#storage-file-download, #menu-file-download>a.k-link").attr("target", "_blank");
        } else {
            $("#storage-file-download, #menu-file-download>a.k-link").attr("disabled", "disabled");
            $("#storage-file-download, #menu-file-download>a.k-link").attr("href", "javascript:void");
            $("#storage-file-download, #menu-file-download, #menu-file-download>a.k-link").addClass("k-state-disabled");            
        };

        
        $('#storage-file-info').html(currentDataItem ? currentDataItem.file_name_with_extention : '');
        $('#storage-file-type').html(currentDataItem ? currentDataItem.group_name : '');
        $('#storage-file-size').html((currentDataItem && currentDataItem.file_size_str) ? currentDataItem.file_size_str : '-');
        $('#storage-file-date').html((currentDataItem && currentDataItem.file_date_str) ? currentDataItem.file_date_str : '-');
        $('#storage-file-author').html(currentDataItem ? (currentDataItem.crt_user + ' ' + currentDataItem.crt_date_str) : '');
        /*
        $('#storage-file-v1-date').html(currentDataItem && currentDataItem.v1_exists ? ("Версия 1 от " + currentDataItem.v1_crt_date_str) : '');
        $('#storage-file-v1-author').html(currentDataItem && currentDataItem.v1_exists ? ('(' +currentDataItem.v1_crt_user + ')') : '');
        $('#storage-file-v2-date').html(currentDataItem && currentDataItem.v2_exists ? ("Версия 2 от " + currentDataItem.v2_crt_date_str) : '');
        $('#storage-file-v2-author').html(currentDataItem && currentDataItem.v2_exists ? ('(' + currentDataItem.v2_crt_user + ')') : '');
        */
        $('#storage-file-v1').attr("href", currentDataItem && currentDataItem.v1_exists ? currentDataItem.v1_download_link : "javascript:void(0)");
        $('#storage-file-v1').html(currentDataItem && currentDataItem.v1_exists ? ("Версия " + currentDataItem.v1_version_num_actual + " от " + currentDataItem.v1_crt_date_str + " (" + currentDataItem.v1_crt_user + ")") : "");
        $('#storage-file-v2').attr("href", currentDataItem && currentDataItem.v2_exists ? currentDataItem.v2_download_link : "javascript:void(0)");
        $('#storage-file-v2').html(currentDataItem && currentDataItem.v2_exists ? ("Версия " + currentDataItem.v2_version_num_actual + " от " + currentDataItem.v2_crt_date_str + " (" + currentDataItem.v2_crt_user + ")") : "");

        $('#storage-file-storage-link').attr("href", currentDataItem ? (url_StorageTreeLink + '?file_id=' + currentDataItem.id) : "javascript:void(0)");
        $('#storage-file-storage-link').html(currentDataItem ? (url_StorageTreeLink + '?file_id=' + currentDataItem.id) : "");
        $('#storage-file-download-link').attr("href", currentDataItem ? currentDataItem.download_link : "javascript:void(0)");        
        $('#storage-file-download-link').html(currentDataItem ? currentDataItem.download_link : "");

        if (currentDataItem) {
            $('#storage-file-attr').removeClass('hidden');
            $('#storage-file-link1').removeClass('hidden');
            $('#storage-file-link2').removeClass('hidden');
            if ((currentDataItem.v1_exists) || (currentDataItem.v2_exists)) {
                $('#storage-file-versions-attr').removeClass('hidden');                
            } else {
                $('#storage-file-versions-attr').addClass('hidden');
            };
            if (currentDataItem.is_local_link) {
                $('#storage-file-local-link').removeClass('hidden');
            } else {
                $('#storage-file-local-link').addClass('hidden');
            }
        } else {
            $('#storage-file-attr').addClass('hidden');
            $('#storage-file-link1').addClass('hidden');
            $('#storage-file-link2').addClass('hidden');
            $('#storage-file-versions-attr').addClass('hidden');
            $('#storage-file-local-link').addClass('hidden');
        };

        if ((!currentDataItem) || ((!currentDataItem.v1_exists) && (!currentDataItem.v2_exists))) {
            $("#menu-file-version").addClass("k-state-disabled");
        }
        else {
            $("#menu-file-version").removeClass("k-state-disabled");
        };

        if ((currentDataItem) && (currentDataItem.v1_exists)) {
            $("#menu-file-version1>a.k-link").removeAttr("disabled");
            $("#menu-file-version1, #menu-file-version1>a.k-link").removeClass("k-state-disabled");
            $("#menu-file-version1>a.k-link").attr("href", currentDataItem.v1_download_link);            
            $("#menu-file-version1>a.k-link").attr("target", "_blank");
            $("#menu-file-version1>a.k-link").text('Версия ' + currentDataItem.v1_version_num_actual + ' от ' + currentDataItem.v1_crt_date_str + ' (' + currentDataItem.v1_crt_user + ')');
        } else {
            $("#menu-file-version1>a.k-link").attr("disabled", "disabled");
            $("#menu-file-version1>a.k-link").attr("href", "javascript:void");
            $("#menu-file-version1, #menu-file-version1>a.k-link").addClass("k-state-disabled");            
            $("#menu-file-version1>a.k-link").text('-');
        };

        if ((currentDataItem) && (currentDataItem.v2_exists)) {
            $("#menu-file-version2>a.k-link").removeAttr("disabled");
            $("#menu-file-version2, #menu-file-version2>a.k-link").removeClass("k-state-disabled");
            $("#menu-file-version2>a.k-link").attr("href", currentDataItem.v2_download_link);            
            $("#menu-file-version2>a.k-link").attr("target", "_blank");
            $("#menu-file-version2>a.k-link").text('Версия ' + currentDataItem.v2_version_num_actual + ' от ' + currentDataItem.v2_crt_date_str + ' (' + currentDataItem.v2_crt_user + ')');
        } else {
            $("#menu-file-version2>a.k-link").attr("disabled", "disabled");
            $("#menu-file-version2>a.k-link").attr("href", "javascript:void");
            $("#menu-file-version2, #menu-file-version2>a.k-link").addClass("k-state-disabled");
            $("#menu-file-version2>a.k-link").text('-');
        };
    };

    
    function gotoFolder() {
        if (goto_folderId) {
            var foldertree = $("#folderTree").getKendoTreeView();
            var dataItem_folder = foldertree.dataSource.get(goto_folderId);
            goto_folderId = null;
            if (dataItem_folder) {
                var folderNode = foldertree.findByUid(dataItem_folder.uid);
                if (folderNode) {
                    foldertree.expandTo(dataItem_folder);
                    foldertree.select(folderNode);
                    refreshFileTree();
                }
            };
        }
    };
    

    function highlightFolder(folder_id) {
        var foldertree = $("#folderTree").getKendoTreeView();
        var dataItem_folder = foldertree.dataSource.get(folder_id);
        if (!dataItem_folder) {
            return;
        }
        var folderNode = foldertree.findByUid(dataItem_folder.uid);
        if (!folderNode) {
            return;
        }        
        var folderNodeSpan = folderNode.find('>div span.k-in');
        if (!folderNodeSpan) {
            return;
        }
        foldertree.expandTo(folder_id);        
        if (!folderNodeSpan.hasClass('highlight')) {
            folderNodeSpan.addClass('highlight');
        }
    };

    function highlightFile(file_id) {
        var filetree = $("#fileTree").getKendoTreeView();
        var dataItem = filetree.dataSource.get(file_id);
        if (!dataItem) {
            return;
        }
        var fileNode = filetree.findByUid(dataItem.uid);
        if (!fileNode) {
            return;
        }
        //filetree.select(fileNode);
        var fileNodeSpan = fileNode.find('>div span.k-in');
        if (!fileNodeSpan) {
            return;
        }
        if (!fileNodeSpan.hasClass('highlight')) {
            fileNodeSpan.addClass('highlight');
        }
    };

    function onStorageTrashGridToFolderTreeDrop(e) {
        var treeView = $("#folderTree").getKendoTreeView();
        //var selectedNode = treeView.select();
        var storageGrid = $('#storageTrashGrid').getKendoGrid();
        var dataItem = storageGrid.dataItem(e.draggable.currentTarget);
        var treeDataItem = treeView.dataItem($(e.dropTarget).closest(".k-item"))
        var selectedNode = treeView.findByUid(treeDataItem.uid);

        if ((!dataItem) || (!dataItem.item_type)) {
            return;
        }

        checkAuthorization(url_IsAuthorized, url_Login);

        $.ajax({
            url: url_StorageTrashRestore,
            type: "POST",
            data: {
                item_id: dataItem.item_id,
                folder_id: treeDataItem.id
            },
            success: function (response) {
                if ((response == null) || (response.err)) {
                    popupNotification.show(response == null ? 'Ошибка при восстановлении из корзины' : response.mess, "error");
                } else {
                    switch (dataItem.item_type) {
                        case 3:
                            treeView.append({
                                text: dataItem.item_name,
                                //text: getFolderText(dataItem, false),
                                id: dataItem.item_id,
                                hasChildren: false,
                                spriteCssClass: dataItem.sprite_css_class,
                                encoded: false
                            }, selectedNode);
                            break;
                        case 2:
                        case 1:
                            treeView.select(selectedNode);
                            //refreshFileTree();
                            break;
                        default:
                            break;
                    }
                    refreshStorageTrashGrid();
                };
            },
            error: function (response) {
                popupNotification.show('Ошибка восстановления из корзины', "error");
            }
        });
    };

    function onStorageTrashGridToFileTreeDrop(e) {
        var treeView = $("#folderTree").getKendoTreeView();
        var selectedNode = treeView.select();
        var storageGrid = $('#storageTrashGrid').getKendoGrid();
        var dataItem = storageGrid.dataItem(e.draggable.currentTarget);
        var treeDataItem = treeView.dataItem(selectedNode);

        if ((!dataItem) || (!dataItem.item_type)) {
            return;
        };

        checkAuthorization(url_IsAuthorized, url_Login);

        switch (dataItem.item_type) {
            case 3:                
                break;
            case 2:
            case 1:
                $.ajax({
                    url: url_StorageTrashRestore,
                    type: "POST",
                    data: {
                        item_id: dataItem.item_id,
                        folder_id: treeDataItem.id
                    },
                    success: function (response) {                        
                        if ((response == null) || (response.err)) {
                            popupNotification.show(response == null ? 'Ошибка при восстановлении из корзины' : response.mess, "error");
                        } else {
                            refreshFileTree();
                            refreshStorageTrashGrid();
                        };
                    },
                    error: function (response) {                        
                        popupNotification.show('Ошибка восстановления из корзины', "error");
                    }
                });                
                break;
            default:
                break;
        }
    };

    function onStorageTrashGridToLeftPaneDrop(e) {
        var treeView = $("#folderTree").getKendoTreeView();
        var storageGrid = $('#storageTrashGrid').getKendoGrid();
        var dataItem = storageGrid.dataItem(e.draggable.currentTarget);

        if ((!dataItem) || (!dataItem.item_type)) {
            return;
        }

        checkAuthorization(url_IsAuthorized, url_Login);

        switch (dataItem.item_type) {
            case 3:
                $.ajax({
                    url: url_StorageTrashRestore,
                    type: "POST",
                    data: {
                        item_id: dataItem.item_id,
                        folder_id: null
                    },
                    success: function (response) {
                        if ((response == null) || (response.err)) {
                            popupNotification.show(response == null ? 'Ошибка при восстановлении из корзины' : response.mess, "error");
                        } else {
                            treeView.append({
                                text: dataItem.item_name,
                                //text: getFolderText(dataItem, false),
                                id: dataItem.item_id,
                                hasChildren: false,
                                spriteCssClass: dataItem.sprite_css_class,
                                encoded: false
                            }, null);
                            refreshStorageTrashGrid();
                        };
                    },
                    error: function (response) {
                        popupNotification.show('Ошибка восстановления из корзины', "error");
                    }
                });
                break;
            case 2:
            case 1:
                break;
            default:
                break;
        }
    };

    function refreshStorageTrashGrid() {
        checkAuthorization(url_IsAuthorized, url_Login);
        $('#storageTrashGrid').getKendoGrid().dataSource.read();
    };

    function refreshStorageFullLogGrid() {
        checkAuthorization(url_IsAuthorized, url_Login);
        $('#storageFullLogGrid').getKendoGrid().dataSource.read();
    };

    function getFolderText(folder, extMode) {
        var hiddenClass = 'hidden';
        if (extMode)
            hiddenClass = '';
        return "<span class='storage-tree-view-simple'>" + folder.folder_name + "</span>" + "<span class='storage-tree-view-ext " + hiddenClass + "'>&nbsp/&nbsp" + folder.folder_cnt + "п&nbsp" + folder.file_cnt + "ф</span>";
    };

    function getFileText(file, extMode) {
        var hiddenClass = 'hidden';
        if (extMode)
            hiddenClass = '';
        return "<span class='storage-tree-view-simple'>" + file.file_name_with_extention + "</span><span class='storage-tree-view-ext " + hiddenClass + "'>&nbsp/&nbsp" + file.file_size_str + "</span><span class='storage-tree-view-ext " + hiddenClass + "'>&nbsp/&nbsp" + file.crt_user + "</span><span class='storage-tree-view-ext " + hiddenClass + "'>&nbsp/&nbsp" + file.crt_date_str + "</span>";
    };

    function renameFolder(selectedNode, folder, extMode) {
        var hiddenClass = 'hidden';
        if (extMode)
            hiddenClass = '';
        var data = getFolderText(folder, extMode);
        refreshTreeNode(selectedNode, data);
    };

    function renameFile(selectedNode, file, extMode) {
        var hiddenClass = 'hidden';
        if (extMode)
            hiddenClass = '';
        var data = getFileText(file, extMode);
        refreshTreeNode(selectedNode, data);
    };

    function refreshTreeNode(selectedNode, data) {
        //var currentText = selectedNode.find('span.k-in').first().text();
        var node = selectedNode.find('span.k-in').first();
        var nodeSpan = selectedNode.find('span.k-in > span.k-sprite').first();
        var nodeSpan2 = selectedNode.find('span.k-in > a').first();
        node.text('');
        node.append(nodeSpan);
        node.append(nodeSpan2);
        node.append(data);
    };
}

var storageTree = null;
var fileList_fromSearch = null;
var firstDataBound_trash = true;
var currViewMode = 0;

$(function () {
    storageTree = new StorageTree();
    storageTree.init();    
});
