﻿function AdminEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();
        //
        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#ddlAuthRole").data("kendoDropDownList");
            $("#auth_role_id").val(combo1.dataItem().role_id);
            var combo2 = $("#ddlCrmUserRole").data("kendoDropDownList");
            $("#crm_user_role_id").val(combo2.dataItem().role_id);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();                                
                                $('#btnSubmit').addClass('k-state-disabled');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });
    };

};

var adminEdit = null;

$(function () {
    adminEdit = new AdminEdit();
    adminEdit.init();
});
