﻿function StockLastList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#stockLastGrid', 70);        
    };

    this.getStockLastGridData = function (e) {
        var curr_client_id = null;
        var curr_sales_id = null;
        var dataItem = null;
        dataItem = $('#stockClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        dataItem = $('#stockSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        //
        var curr_day_cnt = $('#stock-last-day-cnt').val();
        if (!(curr_day_cnt >= 0))
            curr_day_cnt = null;
        return { client_id: curr_client_id, sales_id: curr_sales_id, day_cnt: curr_day_cnt };
    };

    this.onStockLastGridDataBound = function (e) {
        drawEmptyRow('#stockLastGrid')
    };
    
    this.onStockClientDdlDataBound = function (e) {
        $('#stockSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onStockClientDdlChange = function (e) {
        $('#stockSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onStockSalesDdlChange = function (e) {
        refreshGrid('#stockLastGrid');        
    };

    this.onStockSalesDdlDataBound = function (e) {        
        refreshGrid('#stockLastGrid');
    };

    this.getStockSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#stockClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }        
        return { client_id: curr_client_id };
    };

    this.onStockLastDayCntKeyup = function (e) {
        if (e.keyCode == 13) {
            refreshGrid('#stockLastGrid');
        };
    };

}

var stockLastList = null;

$(function () {
    stockLastList = new StockLastList();
    stockLastList.init();
});
