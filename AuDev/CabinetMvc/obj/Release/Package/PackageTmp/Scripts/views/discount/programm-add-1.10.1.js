﻿function ProgrammAdd() {
    _this = this;

    this.init = function () {
        $(document).keypress(function (e) {
            if (e.which == 13 && e.target.tagName != 'TEXTAREA') {
                return false;
            }
        });

        setDisabled('#step2-cont', true, true);
        setDisabled('#step3-cont', true, true);

        $("#btnNext1").click(function (e) {
            setDisabled('#step1-cont', true, true);
            setDisabled('#step2-cont', false, true);
            $('#step2-cont').removeClass('hidden');
            //
            var curr_card_type_group_id = -1;
            var ddl = $('#cardTypeDropDownList').getKendoDropDownList();
            if (ddl) {
                curr_card_type_group_id = ddl.dataItem()['card_type_group_id'];
            };
            switch (curr_card_type_group_id) {
                case 1:
                    $('#step2-div-disc').removeClass('hidden');
                    $('#step2-div-bonus').addClass('hidden');
                    break;
                case 2:
                    $('#step2-div-disc').addClass('hidden');
                    $('#step2-div-bonus').removeClass('hidden');
                    break;
                case 3:
                    $('#step2-div-disc').removeClass('hidden');
                    $('#step2-div-bonus').removeClass('hidden');
                    break;
                case 4:
                    $('#step2-div-disc').addClass('hidden');
                    $('#step2-div-bonus').addClass('hidden');
                    break;
                default:
                    $('#step2-div-disc').addClass('hidden');
                    $('#step2-div-bonus').addClass('hidden');
                    break;
            }
            //
            setDescrText(1, ['#cardTypeDropDownList']);
            //
            goToByScroll('#step2-cont');
        });

        $("#btnNext2").click(function (e) {
            var el_arr = [];
            //
            setDisabled('#step2-cont', true, true);
            setDisabled('#step3-cont', false, true);
            $('#step3-cont').removeClass('hidden');
            //
            if (!$('#step2-div-disc').hasClass('hidden')) {
                if ($('#chb_unit_simple_equals_zhv').is(':checked')) {
                    el_arr.push('#trans_sum_min_disc', '#chb_add_to_card_trans_sum', '#rb_proc_const_disc_', '#chb_use_max_percent_disc', '#chb_price_margin_percent_lte_forbid_disc', '#chb_price_margin_percent_gte_forbid_disc', '#chb_unit_simple_equals_zhv');
                } else {
                    el_arr.push('#trans_sum_min_disc', '#chb_add_to_card_trans_sum', '#rb_proc_const_disc_', '#chb_use_max_percent_disc', '#chb_price_margin_percent_lte_forbid_disc', '#chb_price_margin_percent_gte_forbid_disc', '#chb_unit_simple_equals_zhv', '#rb_proc_const_zhv_disc_', '#chb_use_max_percent_zhv_disc', '#chb_price_margin_percent_lte_forbid_zhv_disc', '#chb_price_margin_percent_gte_forbid_zhv_disc');
                };
            };
            if (!$('#step2-div-bonus').hasClass('hidden')) {
                if ($('#chb_unit_simple_equals_zhv_bonus').is(':checked')) {
                    el_arr.push('#chb_allow_bonus_for_card', '#chb_allow_bonus_for_pay', '#trans_sum_min_bonus_for_card', '#trans_sum_min_bonus_for_pay', '#rb_trans_sum_max_bonus_for_pay_', '#rb_trans_sum_max_percent_bonus_for_pay_', '#chb_add_to_card_trans_sum_bonus', '#rb_proc_const_bonus_for_card_', '#chb_allow_zero_disc_bonus_for_card', '#chb_allow_zero_disc_bonus_for_pay', '#chb_unit_simple_equals_zhv_bonus');
                } else {
                    el_arr.push('#chb_allow_bonus_for_card', '#chb_allow_bonus_for_pay', '#trans_sum_min_bonus_for_card', '#trans_sum_min_bonus_for_pay', '#rb_trans_sum_max_bonus_for_pay_', '#rb_trans_sum_max_percent_bonus_for_pay_', '#chb_add_to_card_trans_sum_bonus', '#rb_proc_const_bonus_for_card_', '#chb_allow_zero_disc_bonus_for_card', '#chb_allow_zero_disc_bonus_for_pay', '#chb_unit_simple_equals_zhv_bonus', '#rb_proc_const_zhv_bonus_for_card_', '#chb_allow_zero_disc_zhv_bonus_for_card', '#chb_allow_zero_disc_zhv_bonus_for_pay');
                };
            };
            //
            setDescrText(2, el_arr);
            //        
            goToByScroll('#step3-cont');
        });

        $('#btnNext3').click(function (e) {
            setDisabled('#btnNext3', true, false);
            setDisabled('#btnPrev3', true, false);
            //                        
            $('#step3-div-wait').removeClass('hidden');
            $('#step3-span-result-ok').addClass('hidden');
            $('#step3-span-result-ok-done').addClass('hidden');
            //
            var orderData = getPostData();
            $.ajax({
                type: "POST",
                url: "ProgrammAdd",
                data: JSON.stringify(orderData),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if (response.error) {
                        $('#step3-span-result-error').text(response.error);
                    } else {
                        if (response.is_done) {
                            $('#step3-span-result-ok-done').removeClass('hidden');
                        } else {
                            $('#step3-span-result-ok').removeClass('hidden');
                        };
                    };
                    $('#step3-div-wait').addClass('hidden');
                    $('#step3-div-result').removeClass('hidden');
                },
                error: function (response) {
                    if (response.error) {
                        $('#step3-span-result-error').text(response.error);
                    } else {
                        $('#step3-span-result-error').text('Ошибка при отправке заявки');
                    }
                }
            });
            setTimeout(function () {                
                $('#step3-div-wait').addClass('hidden');
                $('#step3-div-result').removeClass('hidden');
            }, 1500);
        });

        $("#btnPrev2").click(function (e) {
            setDisabled('#step1-cont', false, true);
            setDisabled('#step2-cont', true, true);
            //
            goToByScroll('#step1-cont');
        });

        $("#btnPrev3").click(function (e) {
            setDisabled('#step2-cont', false, true);
            setDisabled('#step3-cont', true, true);
            //
            goToByScroll('#step2-cont');
        });

        $('#btnRestart').click(function (e) {
            location.reload();
        });

        $('#btnUnitSimple1').click(function (e) {
            $('#step2-div-disc-simple').removeClass('hidden');
            $('#step2-div-disc-zhv').addClass('hidden');
            //
            var is_disabled = $('#btnUnitZhv1:disabled').length > 0 ? true : false;
            if (!is_disabled) {
                $('#btnUnitZhv1').removeClass('btn-primary');
                $('#btnUnitZhv1').addClass('btn-info');
            }
            $('#btnUnitSimple1').addClass('btn-primary');
            $('#btnUnitSimple1').removeClass('btn-info');
        });

        $('#btnUnitZhv1').click(function (e) {
            var is_disabled = $('#btnUnitZhv1:disabled').length > 0 ? true : false;
            if (is_disabled) {
                return false;
            }
            //
            $('#step2-div-disc-simple').addClass('hidden');
            $('#step2-div-disc-zhv').removeClass('hidden');
            //
            $('#btnUnitZhv1').addClass('btn-primary');
            $('#btnUnitZhv1').removeClass('btn-info');
            $('#btnUnitSimple1').removeClass('btn-primary');
            $('#btnUnitSimple1').addClass('btn-info');
        });

        $('#btnUnitSimple2').click(function (e) {
            $('#step2-div-bonus-simple').removeClass('hidden');
            $('#step2-div-bonus-zhv').addClass('hidden');
            //
            var is_disabled = $('#btnUnitZhv2:disabled').length > 0 ? true : false;
            if (!is_disabled) {
                $('#btnUnitZhv2').removeClass('btn-primary');
                $('#btnUnitZhv2').addClass('btn-info');
            }
            $('#btnUnitSimple2').addClass('btn-primary');
            $('#btnUnitSimple2').removeClass('btn-info');
        });

        $('#btnUnitZhv2').click(function (e) {
            var is_disabled = $('#btnUnitZhv2:disabled').length > 0 ? true : false;
            if (is_disabled) {
                return false;
            }
            //
            $('#step2-div-bonus-simple').addClass('hidden');
            $('#step2-div-bonus-zhv').removeClass('hidden');
            //
            $('#btnUnitZhv2').addClass('btn-primary');
            $('#btnUnitZhv2').removeClass('btn-info');
            $('#btnUnitSimple2').removeClass('btn-primary');
            $('#btnUnitSimple2').addClass('btn-info');
        });

        $('#chb_unit_simple_equals_zhv').click(function (e) {
            var is_checked = $('#chb_unit_simple_equals_zhv:checked').length > 0 ? true : false;
            if (is_checked) {
                setDisabled('#btnUnitZhv1', true, false);
                $('#btnUnitZhv1').addClass('btn-default');
                $('#btnUnitZhv1').removeClass('btn-info');
                $('#btnUnitZhv1').removeClass('btn-primary');
                $('#btnUnitSimple1').trigger('click');
            } else {
                setDisabled('#btnUnitZhv1', false, false);
                $('#btnUnitZhv1').addClass('btn-info');
                $('#btnUnitZhv1').removeClass('btn-default');
            }
        });

        $('#chb_unit_simple_equals_zhv_bonus').click(function (e) {
            var is_checked = $('#chb_unit_simple_equals_zhv_bonus:checked').length > 0 ? true : false;
            if (is_checked) {
                setDisabled('#btnUnitZhv2', true, false);
                $('#btnUnitZhv2').addClass('btn-default');
                $('#btnUnitZhv2').removeClass('btn-info');
                $('#btnUnitZhv2').removeClass('btn-primary');
                $('#btnUnitSimple2').trigger('click');
            } else {
                setDisabled('#btnUnitZhv2', false, false);
                $('#btnUnitZhv2').addClass('btn-info');
                $('#btnUnitZhv2').removeClass('btn-default');
            }
        });
    };

    this.getBusinessId = function() {
        return {
            business_id_str: $("#businessDropDownList").getKendoDropDownList().value()
        };
    };

    function goToByScroll(id) {
        $('html,body').animate({
            scrollTop: $(id).offset().top - 100
        },
        'slow');
    };

    function setDisabled(ctrl, dis, chld) {
        if (dis) {
            $(ctrl).prop('disabled', 'disabled');
            $(ctrl).addClass('disabled');
            if (chld) {
                $.each($(ctrl).find('*'), function () {
                    var ddl = $(this).getKendoDropDownList();
                    if (ddl) {
                        ddl.enable(false);
                        return;
                    };
                    var dt = $(this).getKendoDatePicker();
                    if (dt) {
                        dt.enable(false);
                        return;
                    };
                    if ($(this).is('.no-auto-disable')) {
                        return;
                    };
                    $(this).prop('disabled', 'disabled');
                    $(this).addClass('disabled');
                    if ($(this).is('input[type="button"].btn-form-nav')) {
                        $(this).addClass('btn-default');
                        $(this).removeClass('btn-primary');
                    };
                });
            }
        } else {
            $(ctrl).removeProp('disabled');
            $(ctrl).removeClass('disabled');
            if (chld) {
                $.each($(ctrl).find('*'), function () {
                    var ddl = $(this).getKendoDropDownList();
                    if (ddl) {
                        ddl.enable(true);
                        return;
                    };
                    var dt = $(this).getKendoDatePicker();
                    if (dt) {
                        dt.enable(true);
                        return;
                    };
                    if ($(this).is('.no-auto-disable')) {
                        return;
                    };
                    $(this).removeProp('disabled');
                    $(this).removeClass('disabled');
                    if ($(this).is('input[type="button"].btn-form-nav')) {
                        $(this).addClass('btn-primary');
                        $(this).removeClass('btn-default');
                    };
                });
            }
        }
    };

    function getPostData() {
        var curr_business_id = -1;
        var curr_card_type_id = -1;
        var curr_card_type_group_id = -1;
        var curr_trans_sum_min_disc = null;
        var curr_trans_sum_max_disc = null;
        var curr_trans_sum_min_bonus_for_card = null;
        var curr_trans_sum_max_bonus_for_card = null;
        var curr_trans_sum_min_bonus_for_pay = null;
        var curr_trans_sum_max_bonus_for_pay = null;
        var curr_proc_const_disc = null;
        var curr_proc_step_disc = null;
        var curr_sum_step_card_disc = false;
        var curr_sum_step_trans_disc = false;
        var curr_proc_const_bonus_for_card = null;
        var curr_proc_step_bonus_for_card = null;
        var curr_sum_step_card_bonus_for_card = false;
        var curr_sum_step_trans_bonus_for_card = false;
        var curr_use_max_percent_disc = false;
        var curr_allow_zero_disc_bonus_for_card = false;
        var curr_allow_zero_disc_bonus_for_pay = false;
        var curr_same_for_zhv_disc = false;
        var curr_same_for_zhv_bonus = false;
        var curr_proc_const_zhv_disc = null;
        var curr_proc_step_zhv_disc = null;
        var curr_sum_step_card_zhv_disc = false;
        var curr_sum_step_trans_zhv_disc = false;
        var curr_proc_const_zhv_bonus_for_card = null;
        var curr_proc_step_zhv_bonus_for_card = null;
        var curr_sum_step_card_zhv_bonus_for_card = false;
        var curr_sum_step_trans_zhv_bonus_for_card = false;
        var curr_use_max_percent_zhv_disc = false;
        var curr_allow_zero_disc_zhv_bonus_for_card = false;
        var curr_allow_zero_disc_zhv_bonus_for_pay = false;
        var curr_allow_order_disc = null;
        var curr_allow_order_bonus_for_card = null;
        var curr_allow_order_bonus_for_pay = null;
        var curr_add_to_card_trans_sum = false;
        var curr_add_to_card_trans_sum_with_disc = false;
        var curr_add_to_card_trans_sum_with_bonus_for_pay = false;
        //
        var curr_proc_const_from_card_disc = false;
        var curr_proc_const_from_card_bonus_for_card = false;
        var curr_trans_sum_max_percent_bonus_for_pay = null;
        var curr_trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay = null;
        var curr_price_margin_percent_lte_forbid_disc = null;
        var curr_price_margin_percent_gte_forbid_disc = null;
        var curr_use_explicit_max_percent_disc = null;
        var curr_use_explicit_max_percent_zhv_disc = null;
        var curr_proc_const_from_card_zhv_disc = false;
        var curr_proc_const_from_card_zhv_bonus_for_card = false;
        var curr_price_margin_percent_lte_forbid_zhv_disc = null;
        var curr_price_margin_percent_gte_forbid_zhv_disc = null;

        var ddl1 = $('#businessDropDownList').getKendoDropDownList();
        if (ddl1) {
            curr_business_id = ddl1.dataItem().business_id_str;
        };

        var ddl2 = $('#cardTypeDropDownList').getKendoDropDownList();
        if (ddl2) {
            curr_card_type_id = ddl2.dataItem().card_type_id;
            curr_card_type_group_id = ddl2.dataItem().card_type_group_id;
        };

        function setDiscountParams() {
            curr_trans_sum_min_disc = $('#trans_sum_min_disc').getKendoNumericTextBox().value();
            if ($('#rb_proc_const_disc_').is(':checked')) {
                if ($('#rb_proc_const_from_card_disc_').is(':checked')) {
                    curr_proc_const_from_card_disc = true;
                } else {
                    curr_proc_const_disc = $('#proc_const_disc').getKendoNumericTextBox().value();
                };
            } else {
                curr_proc_step_disc = kendo.stringify($('#procStepDiscGrid').getKendoGrid().dataSource.view());
                if ($('#rb_sum_step_card_disc_').is(':checked')) {
                    curr_sum_step_card_disc = true;
                } else {
                    curr_sum_step_trans_disc = true;
                };
            };
            if ($('#chb_use_max_percent_disc').is(':checked')) {
                if ($('#rb_use_au_max_percent_disc_').is(':checked')) {
                    curr_use_max_percent_disc = true;
                } else {
                    curr_use_explicit_max_percent_disc = $('#use_explicit_max_percent_disc').getKendoNumericTextBox().value();
                }
            };
            if ($('#chb_price_margin_percent_lte_forbid_disc').is(':checked')) {
                curr_price_margin_percent_lte_forbid_disc = $('#price_margin_percent_lte_forbid_disc').getKendoNumericTextBox().value();
            };
            if ($('#chb_price_margin_percent_gte_forbid_disc').is(':checked')) {
                curr_price_margin_percent_gte_forbid_disc = $('#price_margin_percent_gte_forbid_disc').getKendoNumericTextBox().value();
            };
            if ($('#chb_unit_simple_equals_zhv').is(':checked')) {
                curr_same_for_zhv_disc = true;
            };
            if (!curr_same_for_zhv_disc) {
                if ($('#rb_proc_const_zhv_disc_').is(':checked')) {
                    if ($('#rb_proc_const_from_card_zhv_disc_').is(':checked')) {
                        curr_proc_const_from_card_zhv_disc = true;
                    } else {
                        curr_proc_const_zhv_disc = $('#proc_const_zhv_disc').getKendoNumericTextBox().value();
                    };
                } else {
                    curr_proc_step_zhv_disc = kendo.stringify($('#procStepZhvDiscGrid').getKendoGrid().dataSource.view());
                    if ($('#rb_sum_step_card_zhv_disc_').is(':checked')) {
                        curr_sum_step_card_zhv_disc = true;
                    } else {
                        curr_sum_step_trans_zhv_disc = true;
                    };
                };
                if ($('#chb_use_max_percent_zhv_disc').is(':checked')) {
                    if ($('#rb_use_au_max_percent_zhv_disc_').is(':checked')) {
                        curr_use_max_percent_zhv_disc = true;
                    } else {
                        curr_use_explicit_max_percent_zhv_disc = $('#use_explicit_max_percent_zhv_disc').getKendoNumericTextBox().value();
                    }
                };
                if ($('#chb_price_margin_percent_lte_forbid_zhv_disc').is(':checked')) {
                    curr_price_margin_percent_lte_forbid_zhv_disc = $('#price_margin_percent_lte_forbid_zhv_disc').getKendoNumericTextBox().value();
                };
                if ($('#chb_price_margin_percent_gte_forbid_zhv_disc').is(':checked')) {
                    curr_price_margin_percent_gte_forbid_zhv_disc = $('#price_margin_percent_gte_forbid_zhv_disc').getKendoNumericTextBox().value();
                };
            };

            if ($('#chb_add_to_card_trans_sum').is(':checked')) {
                if ($('#rb_add_to_card_trans_sum_with_disc_').is(':checked')) {
                    curr_add_to_card_trans_sum_with_disc = true;
                } else {
                    curr_add_to_card_trans_sum = true;
                };
            };
        };

        function setBonusParams() {
            curr_trans_sum_min_bonus_for_card = $('#trans_sum_min_bonus_for_card').getKendoNumericTextBox().value();
            curr_trans_sum_min_bonus_for_pay = $('#trans_sum_min_bonus_for_pay').getKendoNumericTextBox().value();

            if ($('#rb_trans_sum_max_bonus_for_pay_').is(':checked')) {
                curr_trans_sum_max_bonus_for_pay = $('#trans_sum_max_bonus_for_pay').getKendoNumericTextBox().value();
            } else if ($('#rb_trans_sum_max_percent_bonus_for_pay_').is(':checked')) {
                if ($('#chb_trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay').is(':checked')) {
                    curr_trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay = $('#trans_sum_max_percent_bonus_for_pay').getKendoNumericTextBox().value();
                } else {
                    curr_trans_sum_max_percent_bonus_for_pay = $('#trans_sum_max_percent_bonus_for_pay').getKendoNumericTextBox().value();
                };
            };

            if ($('#rb_proc_const_bonus_for_card_').is(':checked')) {
                if ($('#rb_proc_const_from_card_bonus_for_card_').is(':checked')) {
                    curr_proc_const_from_card_bonus_for_card = true;
                } else {
                    curr_proc_const_bonus_for_card = $('#proc_const_bonus_for_card').getKendoNumericTextBox().value();
                };
            } else {
                curr_proc_step_bonus_for_card = kendo.stringify($('#procStepBonusGrid').getKendoGrid().dataSource.view());
                if ($('#rb_sum_step_card_bonus_for_card_').is(':checked')) {
                    curr_sum_step_card_bonus_for_card = true;
                } else {
                    curr_sum_step_trans_bonus_for_card = true;
                };
            };

            curr_allow_zero_disc_bonus_for_card = $('#chb_allow_zero_disc_bonus_for_card').is(':checked');
            curr_allow_zero_disc_bonus_for_pay = $('#chb_allow_zero_disc_bonus_for_pay').is(':checked');

            curr_same_for_zhv_bonus = $('#chb_unit_simple_equals_zhv_bonus').is(':checked');
            if (!curr_same_for_zhv_bonus) {
                if ($('#rb_proc_const_zhv_bonus_for_card_').is(':checked')) {
                    if ($('#rb_proc_const_from_card_zhv_bonus_for_card_').is(':checked')) {
                        curr_proc_const_from_card_zhv_bonus_for_card = true;
                    } else {
                        curr_proc_const_zhv_bonus_for_card = $('#proc_const_zhv_bonus_for_card').getKendoNumericTextBox().value();
                    };
                } else {
                    curr_proc_step_zhv_bonus_for_card = kendo.stringify($('#procStepZhvBonusGrid').getKendoGrid().dataSource.view());
                    if ($('#rb_sum_step_card_zhv_bonus_for_card_').is(':checked')) {
                        curr_sum_step_card_zhv_bonus_for_card = true;
                    } else {
                        curr_sum_step_trans_zhv_bonus_for_card = true;
                    };
                };
                curr_allow_zero_disc_zhv_bonus_for_card = $('#chb_allow_zero_disc_zhv_bonus_for_card').is(':checked');
                curr_allow_zero_disc_zhv_bonus_for_pay = $('#chb_allow_zero_disc_zhv_bonus_for_pay').is(':checked');
            };

            if ($('#chb_add_to_card_trans_sum_bonus').is(':checked')) {
                if ($('#rb_add_to_card_trans_sum_with_bonus_for_pay_').is(':checked')) {
                    curr_add_to_card_trans_sum_with_bonus_for_pay = true;
                } else {
                    curr_add_to_card_trans_sum = true;
                };
            };
        };

        switch (curr_card_type_group_id) {
            // дисконтные карты
            case 1:
                setDiscountParams();
                curr_allow_order_disc = 1;
                break;
                // бонусные карты
            case 2:
                setBonusParams();
                curr_allow_order_bonus_for_pay = 1;
                curr_allow_order_bonus_for_card = 2;
                break;
                // дисконтно-бонусные карты
            case 3:
                setDiscountParams();
                setBonusParams();
                curr_allow_order_disc = 1;
                curr_allow_order_bonus_for_pay = 2;
                curr_allow_order_bonus_for_card = 3;
                break;
                // подарочные сертификаты
            case 4:
                break;
            default:
                break;
        }

        return {
            business_id: curr_business_id,
            card_type_id: curr_card_type_id,
            mess: $('#mess').val(),
            descr: $('#descr').val(),
            card_type_group_id: curr_card_type_group_id,
            date_beg: $('#date_beg').getKendoDatePicker().value(),
            trans_sum_min_disc: curr_trans_sum_min_disc,
            trans_sum_max_disc: curr_trans_sum_max_disc,
            trans_sum_min_bonus_for_card: curr_trans_sum_min_bonus_for_card,
            trans_sum_max_bonus_for_card: curr_trans_sum_max_bonus_for_card,
            trans_sum_min_bonus_for_pay: curr_trans_sum_min_bonus_for_pay,
            trans_sum_max_bonus_for_pay: curr_trans_sum_max_bonus_for_pay,
            proc_const_disc: curr_proc_const_disc,
            proc_step_disc: curr_proc_step_disc,
            sum_step_card_disc: curr_sum_step_card_disc,
            sum_step_trans_disc: curr_sum_step_trans_disc,
            proc_const_bonus_for_card: curr_proc_const_bonus_for_card,
            proc_step_bonus_for_card: curr_proc_step_bonus_for_card,
            sum_step_card_bonus_for_card: curr_sum_step_card_bonus_for_card,
            sum_step_trans_bonus_for_card: curr_sum_step_trans_bonus_for_card,
            use_max_percent_disc: curr_use_max_percent_disc,
            allow_zero_disc_bonus_for_card: curr_allow_zero_disc_bonus_for_card,
            allow_zero_disc_bonus_for_pay: curr_allow_zero_disc_bonus_for_pay,
            same_for_zhv_disc: curr_same_for_zhv_disc,
            same_for_zhv_bonus: curr_same_for_zhv_bonus,
            proc_const_zhv_disc: curr_proc_const_zhv_disc,
            proc_step_zhv_disc: curr_proc_step_zhv_disc,
            sum_step_card_zhv_disc: curr_sum_step_card_zhv_disc,
            sum_step_trans_zhv_disc: curr_sum_step_trans_zhv_disc,
            proc_const_zhv_bonus_for_card: curr_proc_const_zhv_bonus_for_card,
            proc_step_zhv_bonus_for_card: curr_proc_step_zhv_bonus_for_card,
            sum_step_card_zhv_bonus_for_card: curr_sum_step_card_zhv_bonus_for_card,
            sum_step_trans_zhv_bonus_for_card: curr_sum_step_trans_zhv_bonus_for_card,
            use_max_percent_zhv_disc: curr_use_max_percent_zhv_disc,
            allow_zero_disc_zhv_bonus_for_card: curr_allow_zero_disc_zhv_bonus_for_card,
            allow_zero_disc_zhv_bonus_for_pay: curr_allow_zero_disc_zhv_bonus_for_pay,
            allow_order_disc: curr_allow_order_disc,
            allow_order_bonus_for_card: curr_allow_order_bonus_for_card,
            allow_order_bonus_for_pay: curr_allow_order_bonus_for_pay,
            add_to_card_trans_sum: curr_add_to_card_trans_sum,
            add_to_card_trans_sum_with_disc: curr_add_to_card_trans_sum_with_disc,
            add_to_card_trans_sum_with_bonus_for_pay: curr_add_to_card_trans_sum_with_bonus_for_pay,
            //
            proc_const_from_card_disc: curr_proc_const_from_card_disc,
            proc_const_from_card_bonus_for_card: curr_proc_const_from_card_bonus_for_card,
            trans_sum_max_percent_bonus_for_pay: curr_trans_sum_max_percent_bonus_for_pay,
            trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay: curr_trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay,
            price_margin_percent_lte_forbid_disc: curr_price_margin_percent_lte_forbid_disc,
            price_margin_percent_gte_forbid_disc: curr_price_margin_percent_gte_forbid_disc,
            use_explicit_max_percent_disc: curr_use_explicit_max_percent_disc,
            use_explicit_max_percent_zhv_disc: curr_use_explicit_max_percent_zhv_disc,
            proc_const_from_card_zhv_disc: curr_proc_const_from_card_zhv_disc,
            proc_const_from_card_zhv_bonus_for_card: curr_proc_const_from_card_zhv_bonus_for_card,
            price_margin_percent_lte_forbid_zhv_disc: curr_price_margin_percent_lte_forbid_zhv_disc,
            price_margin_percent_gte_forbid_zhv_disc: curr_price_margin_percent_gte_forbid_zhv_disc,
            //
            mess_contains_ext_info: $('#mess_contains_ext_info').is(':checked')
        };
    };

    function setDescrText(step_num, el_arr) {
        //
        switch (step_num) {
            case 1:
                curr_step1_text = '';
                curr_step2_text = '';
                break;
            case 2:
                curr_step2_text = '';
                break;
            default:
                break;
        }
        //
        $.each(el_arr, function (index, item) {
            setDescrText_forElement(item);
        });
        //
        switch (step_num) {
            case 1:
                step1_text = curr_step1_text;
                step2_text = curr_step2_text;
                break;
            case 2:
                step2_text = curr_step2_text;
                break;
            default:
                break;
        };
        //
        $('#descr').text(step1_text + step2_text);
    };

    function setDescrText_forElement(el) {
        var curr_id = $(el).attr('id');
        //
        var id1 = null;
        var val1 = null;
        var val2 = null;
        var str1 = null;
        //
        switch (curr_id) {
            case 'cardTypeDropDownList':
                id1 = $(el).getKendoDropDownList().dataItem().card_type_group_id;
                switch (id1) {
                    case 1:
                        curr_step1_text += 'Дисконтная система.' + '\n';
                        break;
                    case 2:
                        curr_step1_text += 'Бонусная система.' + '\n';
                        break;
                    case 3:
                        curr_step1_text += 'Дисконтно-бонусная система.' + '\n';
                        break;
                    case 4:
                        curr_step1_text += 'Подарочный сертификат (НЕ ПОДДЕРЖИВАЕТСЯ).' + '\n';
                        break;
                    default:
                        break;
                };
                break;
                // disc
            case 'trans_sum_min_disc':
                val1 = $(el).getKendoNumericTextBox().value();
                if (val1) {
                    curr_step2_text += 'Мин. сумма покупки: ' + val1 + '.' + '\n';
                };
                break;
            case 'chb_add_to_card_trans_sum':
                val1 = $(el).is(':checked');
                if (val1) {
                    val2 = $('#rb_add_to_card_trans_sum_with_disc_').is(':checked');
                    if (val2) {
                        curr_step2_text += 'На карту накапливается сумма со скидкой.';
                    } else {
                        curr_step2_text += 'На карту накапливается сумма без скидки.';
                    };
                    curr_step2_text += '\n';
                }
                break;
            case 'rb_proc_const_disc_':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Процент скидки постоянный';
                    val2 = $('#rb_proc_const_from_card_disc_').is(':checked');
                    if (val2) {
                        curr_step2_text += ', берется с карты.';
                    } else {
                        curr_step2_text += ': ' + $('#proc_const_disc').getKendoNumericTextBox().value() + '.' + ' %';
                    };
                    curr_step2_text += '\n';
                } else {
                    str1 = ($('#rb_sum_step_card_disc_').is(':checked')) ? 'накоплений' : 'продажи';
                    curr_step2_text += 'Процент скидки зависит от суммы ' + str1 + ', порогов: ' + $('#procStepDiscGrid').getKendoGrid().dataSource.total() + '.';
                    curr_step2_text += '\n';
                };
                break;
            case 'chb_use_max_percent_disc':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Макс. скидка по позициям';
                    val2 = $('#rb_use_au_max_percent_disc_').is(':checked');
                    if (val2) {
                        curr_step2_text += ' берется из АУ.';
                    } else {
                        curr_step2_text += ': ' + $('#use_explicit_max_percent_disc').getKendoNumericTextBox().value() + ' %.';
                    };
                    curr_step2_text += '\n';
                };
                break;
            case 'chb_price_margin_percent_lte_forbid_disc':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Запрет скидки для розничной наценки <= ' + $('#price_margin_percent_lte_forbid_disc').getKendoNumericTextBox().value() + '.' + '\n';
                };
                break;
            case 'chb_price_margin_percent_gte_forbid_disc':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Запрет скидки для розничной наценки >= ' + $('#price_margin_percent_gte_forbid_disc').getKendoNumericTextBox().value() + '.' + '\n';
                };
                break;
            case 'chb_unit_simple_equals_zhv':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Настройки для ЖНВЛП совпадают.';
                } else {
                    curr_step2_text += 'Настройки для ЖНВЛП:';
                };
                curr_step2_text += '\n';
                break;
                // disc zhv
            case 'rb_proc_const_zhv_disc_':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Процент скидки постоянный';
                    val2 = $('#rb_proc_const_from_card_zhv_disc_').is(':checked');
                    if (val2) {
                        curr_step2_text += ', берется с карты.';
                    } else {
                        curr_step2_text += ': ' + $('#proc_const_zhv_disc').getKendoNumericTextBox().value() + ' %' + '.';
                    };
                    curr_step2_text += '\n';
                } else {
                    str1 = ($('#rb_sum_step_card_zhv_disc_').is(':checked')) ? 'накоплений' : 'продажи';
                    curr_step2_text += 'Процент скидки зависит от суммы ' + str1 + ', порогов: ' + $('#procStepZhvDiscGrid').getKendoGrid().dataSource.total() + '.';
                    curr_step2_text += '\n';
                };
                break;
            case 'chb_use_max_percent_zhv_disc':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Макс. скидка по позициям';
                    val2 = $('#rb_use_au_max_percent_zhv_disc_').is(':checked');
                    if (val2) {
                        curr_step2_text += ' берется из АУ.';
                    } else {
                        curr_step2_text += ': ' + $('#use_explicit_max_percent_zhv_disc').getKendoNumericTextBox().value() + ' %.';
                    };
                    curr_step2_text += '\n';
                };
                break;
            case 'chb_price_margin_percent_lte_forbid_zhv_disc':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Запрет скидки для розничной наценки <= ' + $('#price_margin_percent_lte_forbid_zhv_disc').getKendoNumericTextBox().value() + '.' + '\n';
                };
                break;
            case 'chb_price_margin_percent_gte_forbid_zhv_disc':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Запрет скидки для розничной наценки >= ' + $('#price_margin_percent_gte_forbid_zhv_disc').getKendoNumericTextBox().value() + '.' + '\n';
                };
                break;
                // bonus
            case 'chb_allow_bonus_for_card':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Разрешено накопление бонусов.' + '\n';
                };
                break;
            case 'chb_allow_bonus_for_pay':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Разрешена оплата бонусами.' + '\n';
                };
                break;
            case 'trans_sum_min_bonus_for_card':
                val1 = $(el).getKendoNumericTextBox().value();
                if (val1) {
                    curr_step2_text += 'Мин. сумма покупки для накопления бонусов: ' + val1 + '.' + '\n';
                };
                break;
            case 'trans_sum_min_bonus_for_pay':
                val1 = $(el).getKendoNumericTextBox().value();
                if (val1) {
                    curr_step2_text += 'Мин. сумма покупки для оплаты бонусами: ' + val1 + '.' + '\n';
                };
                break;
            case 'rb_trans_sum_max_bonus_for_pay_':
                val1 = $(el).is(':checked');
                if (val1) {
                    val2 = $('#trans_sum_max_bonus_for_pay').getKendoNumericTextBox().value();
                    curr_step2_text += 'Сумма покупки, которую можно оплатить бонусами: ' + val2 + '.' + '\n';
                };
                break;
            case 'rb_trans_sum_max_percent_bonus_for_pay_':
                val1 = $(el).is(':checked');
                if (val1) {
                    val2 = $('#trans_sum_max_percent_bonus_for_pay').getKendoNumericTextBox().value();
                    curr_step2_text += 'Процент покупки, который можно оплатить бонусами: ' + val2 + '.' + '\n';
                };
                break;
            case 'chb_add_to_card_trans_sum_bonus':
                val1 = $(el).is(':checked');
                if (val1) {
                    val2 = $('#rb_add_to_card_trans_sum_wo_bonus_for_pay_').is(':checked');
                    if (val2) {
                        curr_step2_text += 'На карту накапливается сумма до оплаты бонусами.';
                    } else {
                        curr_step2_text += 'На карту накапливается сумма после оплаты бонусами.';
                    };
                    curr_step2_text += '\n';
                }
                break;
            case 'rb_proc_const_bonus_for_card_':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Бонусный процент постоянный';
                    val2 = $('#rb_proc_const_from_card_bonus_for_card_').is(':checked');
                    if (val2) {
                        curr_step2_text += ', берется с карты.';
                    } else {
                        curr_step2_text += ': ' + $('#proc_const_bonus_for_card').getKendoNumericTextBox().value() + ' %.';
                    };
                    curr_step2_text += '\n';
                } else {
                    str1 = ($('#rb_sum_step_card_bonus_for_card_').is(':checked')) ? 'накоплений' : 'продажи';
                    curr_step2_text += 'Процент скидки зависит от суммы ' + str1 + ', порогов: ' + $('#procStepBonusGrid').getKendoGrid().dataSource.total() + '.';
                    curr_step2_text += '\n';
                };
                break;
            case 'chb_allow_zero_disc_bonus_for_card':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Накапливать бонусы на товары с макс. скидкой = 0.' + '\n';
                };
                break;
            case 'chb_allow_zero_disc_bonus_for_pay':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Списывать бонусы для товаров с макс. скидкой = 0.' + '\n';
                };
                break;
            case 'chb_unit_simple_equals_zhv_bonus':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Настройки для ЖНВЛП совпадают.';
                } else {
                    curr_step2_text += 'Настройки для ЖНВЛП:';
                };
                curr_step2_text += '\n';
                break;
                // bonus zhv
            case 'rb_proc_const_zhv_bonus_for_card_':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Бонусный процент постоянный';
                    val2 = $('#rb_proc_const_from_card_zhv_bonus_for_card_').is(':checked');
                    if (val2) {
                        curr_step2_text += ', берется с карты.';
                    } else {
                        curr_step2_text += ': ' + $('#proc_const_zhv_bonus_for_card').getKendoNumericTextBox().value() + ' %.';
                    };
                    curr_step2_text += '\n';
                } else {
                    str1 = ($('#rb_sum_step_card_zhv_bonus_for_card_').is(':checked')) ? 'накоплений' : 'продажи';
                    curr_step2_text += 'Процент скидки зависит от суммы ' + str1 + ', порогов: ' + $('#procStepZhvBonusGrid').getKendoGrid().dataSource.total() + '.';
                    curr_step2_text += '\n';
                };
                break;
            case 'chb_allow_zero_disc_zhv_bonus_for_card':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Накапливать бонусы на товары с макс. скидкой = 0.' + '\n';
                };
                break;
            case 'chb_allow_zero_disc_zhv_bonus_for_pay':
                val1 = $(el).is(':checked');
                if (val1) {
                    curr_step2_text += 'Списывать бонусы для товаров с макс. скидкой = 0.' + '\n';
                };
                break;
            default:
                break;
        };
    };
}

var programmAdd = null;
var step1_text = '';
var step2_text = '';
var curr_step1_text = '';
var curr_step2_text = '';

$(function () {
    programmAdd = new ProgrammAdd();
    programmAdd.init();
});
