﻿function ExchangeLogList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#exchangeLogGrid', 0);

        $('#btnNodeAdd').click(function (e) {
            location = 'ExchangeFileNodeAdd';
        });

        $('#chbErrorOnly').click(function (e) {
            $('#exchangeLogGrid').getKendoGrid().dataSource.read();
        });

        $('#btnRefresh').click(function (e) {
            $('#exchangeLogGrid').getKendoGrid().dataSource.read();
        });
    };


    this.onDsListChange = function(e) {
        $('#exchangeLogGrid').getKendoGrid().dataSource.read();
    };

    this.onClientListChange = function(e) {
        $('#exchangeLogGrid').getKendoGrid().dataSource.read();
    };

    this.getLogParams = function(e) {
        var curr_ds_id = -1;
        var curr_client_id = -1;
        var curr_error_only = false;

        var ddl1 = $('#dsList').getKendoDropDownList();
        if (ddl1) {
            curr_ds_id = ddl1.value();
        };

        var ddl2 = $('#clientList').getKendoDropDownList();
        if (ddl2) {
            curr_client_id = ddl2.value();
        };

        curr_error_only = $('#chbErrorOnly').is(':checked');

        return { ds_id: curr_ds_id, client_id: curr_client_id, error_only: curr_error_only };
    };

    this.onExchangeLogGridDataBound = function(e) {
        drawEmptyRow("#exchangeLogGrid");
        //
        var grid = $("#exchangeLogGrid").getKendoGrid();
        var row = null;
        var cnt = 0;

        var len0 = grid.dataSource.data().length;
        var len1 = 0;
        var len2 = 0;
        var len3 = 0;
        var data1 = null;
        var data2 = null;
        var data3 = null;

        for (var i0 = 0; i0 < len0; i0++) {
            data1 = grid.dataSource.data()[i0].items;
            len1 = data1.length;
            for (var i1 = 0; i1 < len1; i1++) {
                data2 = data1[i1].items;
                len2 = data2.length;
                for (var i2 = 0; i2 < len2; i2++) {
                    data3 = data2[i2].items;
                    len3 = data3.length;
                    for (var i3 = 0; i3 < len3; i3++) {
                        var dataItem = data3[i3];
                        var tr = $("#exchangeLogGrid").find("[data-uid='" + dataItem.uid + "']");

                        if (dataItem.days_cnt !== null) {
                            if (dataItem.days_cnt > 3) {
                                tr.css('background-color', '#ff6666');
                            } else if (dataItem.days_cnt > 1) {
                                tr.css('background-color', '#ffff66');
                            } else {
                                tr.css('background-color', '#80ff80');
                            };
                        } else {
                            tr.css('background-color', '#ff6666');
                        };
                    };
                };
            };
        };
    };
}

var exchangeLogList = null;

$(function () {
    exchangeLogList = new ExchangeLogList();
    exchangeLogList.init();
});
