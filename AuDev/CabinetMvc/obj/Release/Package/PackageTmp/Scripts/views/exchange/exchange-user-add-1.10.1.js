﻿function ExchangeUserAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            var combo1 = $("#salesList").getKendoDropDownList();
            $("#cabinet_sales_id").val(combo1.dataItem()["sales_id"]);

            var combo2 = $("#workplaceList").getKendoDropDownList();
            $("#cabinet_workplace_id").val(combo2.dataItem()["id"]);

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
    };
}

var exchangeUserAdd = null;

$(function () {
    exchangeUserAdd = new ExchangeUserAdd();
    exchangeUserAdd.init();
});
