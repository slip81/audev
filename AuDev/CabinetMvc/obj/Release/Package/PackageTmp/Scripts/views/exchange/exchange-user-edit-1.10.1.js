﻿function ExchangeUserEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();
        $('.attr-change').bind('input propertychange', _this.onAttrChanged);

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#salesList").getKendoDropDownList();
            $("#cabinet_sales_id").val(combo1.dataItem()["sales_id"]);

            var combo2 = $("#workplaceList").getKendoDropDownList();
            $("#cabinet_workplace_id").val(combo2.dataItem()["id"]);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".save-changes-confirm,.save-changes-cancel")
                        .click(function () {
                            if ($(this).hasClass("save-changes-confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('btn-primary');
                                $('#btnSubmit').addClass('btn-default');
                                $('#btnSubmit').removeClass('active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-warning');
                                $('#btnRevert').addClass('hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();

            $("#workplaceList").getKendoDropDownList().value(model_cabinet_workplace_id);
            $("#salesList").getKendoDropDownList().value(model_cabinet_sales_id);

            $('#btnSubmit').removeClass('btn-primary');
            $('#btnSubmit').addClass('btn-default');
            $('#btnSubmit').removeClass('active');
            $('#btnSubmit').attr('disabled', true);
            $('#btnRevert').removeClass('btn-warning');
            $('#btnRevert').addClass('hidden');
        });
    };

    this.onAttrChanged = function() {
        $('#btnSubmit').addClass('btn-primary');
        $('#btnSubmit').removeClass('btn-default');
        $('#btnSubmit').removeAttr('disabled');
        $('#btnSubmit').addClass('active');
        $('#btnRevert').removeClass('hidden');
        $('#btnRevert').addClass('btn-warning');
    };
}

var exchangeUserEdit = null;

$(function () {
    exchangeUserEdit = new ExchangeUserEdit();
    exchangeUserEdit.init();
});
