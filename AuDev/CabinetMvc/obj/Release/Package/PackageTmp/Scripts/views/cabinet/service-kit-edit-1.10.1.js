﻿function ServiceKitEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();
        //
        setGridMaxHeight('#serviceGrid1', 200);
        setGridMaxHeight('#serviceGrid2', 200);
        //
        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();
            if (validator.validate()) {
                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').addClass('k-state-disabled');
                                var postData = JSON.stringify({ service_kit_id: $('#service_kit_id').val(), service_kit_name: $('#service_kit_name').val(), serviceList: $('#serviceGrid2').getKendoGrid().dataSource.view() });
                                $.ajax({
                                    type: "POST",
                                    url: "ServiceKitEdit",
                                    data: postData,
                                    contentType: 'application/json; charset=windows-1251',
                                    success: function (response) {
                                        if (response.err) {
                                            $('#btnSubmit').removeClass('k-state-disabled');
                                            if (response.mess) {
                                                alert(response.mess);
                                            } else {
                                                alert('Ошибка при добавлении набора');
                                            };
                                        } else {
                                            if (backToClient) {
                                                window.location = "/OrderAdd?client_id=" + backToClient;
                                            } else {
                                                window.location = "/ClientList";
                                            };
                                        };
                                        return true;
                                    },
                                    error: function (response) {
                                        $('#btnSubmit').removeClass('k-state-disabled');
                                        alert('Ошибка при попытке добавления набора');
                                        return true;
                                    }
                                });
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else {
                return false;
            };
        });        
    };

    this.onServiceGrid1DataBound = function (e) {
        drawEmptyRow('#serviceGrid1');
        if (firstDataBound_grid1) {
            firstDataBound_grid1 = false;
        };
    };

    this.onServiceGrid2DataBound = function (e) {
        drawEmptyRow('#serviceGrid2');
        if (firstDataBound_grid2) {
            firstDataBound_grid2 = false;
            //
            var grid1 = $('#serviceGrid1').getKendoGrid();
            var dataSource1 = grid1.dataSource;
            var grid2 = $('#serviceGrid2').getKendoGrid();
            var dataSource2 = grid2.dataSource;
            //
            $(grid1.element).kendoDraggable({
                filter: "tr",
                hint: function (element) {
                    return element.clone().find('td:first-child').css({
                        "opacity": 0.8
                    });
                },
                group: "gridGroup1",
            });
            //            
            $(grid2.element).kendoDraggable({
                filter: "tr",
                hint: function (element) {
                    return element.clone().find('td:first-child').css({
                        "opacity": 0.8
                    });
                },
                group: "gridGroup2",
            });
            //
            grid1.table.kendoDropTarget({
                drop: function (e) {
                    var dataItem = dataSource2.getByUid(e.draggable.currentTarget.data("uid"));
                    dataSource2.remove(dataItem);
                    dataSource1.add(dataItem);
                },
                group: "gridGroup2",
            });
            //
            grid2.table.kendoDropTarget({
                drop: function (e) {
                    var dataSource2 = grid2.dataSource;
                    var dataItem = dataSource1.getByUid(e.draggable.currentTarget.data("uid"));
                    dataSource1.remove(dataItem);
                    dataSource2.add(dataItem);
                },
                group: "gridGroup1",
            });
        };
    };
}

var serviceKitEdit = null;
var firstDataBound_grid1 = true;
var firstDataBound_grid2 = true;

$(function () {
    serviceKitEdit = new ServiceKitEdit();
    serviceKitEdit.init();
});
