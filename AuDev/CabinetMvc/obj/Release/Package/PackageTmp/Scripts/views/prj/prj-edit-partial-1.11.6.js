﻿function PrjEditPartial() {
    _this = this;

    this.init = function () {        
        //
        $(document).on('click', '#btnPrjEditSubmit', function () {
            if (validateData()) {
                loadingDialog('#prj-loading', true);
                //
                $.ajax({
                    url: '/PrjEditPartial',
                    type: "POST",
                    contentType: 'application/json; charset=windows-1251',
                    data: JSON.stringify(curr_prj),
                    success: function (response) {                        
                        if ((response == null) || (response.err)) {
                            alert(response == null ? 'Ошибка при попытке сохранения' : response.mess);
                        } else {
                            toggleSplitter("#prjMainSplitter", "#prj-main-pane-left", "#prj-main-pane-right");
                            if (prj_add_mode) {
                                refreshGrid('#prjGrid');
                            } else {                                
                                var grid = $("#prjGrid").data("kendoGrid");
                                var select = grid.select();                                
                                var data = grid.dataItem(select);
                                data.prj_name = response.prj.prj_name;
                                data.date_beg = kendo.toString(kendo.parseDate(response.prj.date_beg), 'dd.MM.yyyy');
                                data.date_end = kendo.toString(kendo.parseDate(response.prj.date_end), 'dd.MM.yyyy');
                                data.state_type_id = response.prj.state_type_id;
                                data.state_type_name = response.prj.state_type_name;
                                data.state_type_name_templ = response.prj.state_type_name_templ;
                                data.claim_cnt = response.prj.claim_cnt;
                                data.new_claim_cnt = response.prj.new_claim_cnt;
                                data.active_claim_cnt = response.prj.active_claim_cnt;
                                data.done_claim_cnt = response.prj.done_claim_cnt;
                                data.canceled_claim_cnt = response.prj.canceled_claim_cnt;
                                data.verified_claim_cnt = response.prj.verified_claim_cnt;
                                kendoFastRedrawRow(grid, select);
                            }
                        };
                    },
                    error: function (response) {
                        alert('Ошибка при сохранении');
                    },
                    complete: function (response) {
                        loadingDialog('#prj-loading', false);
                    }
                });
            }            
        });
        //
        $(document).on('click', '#btnPrjEditCancel', function () {
            closePrjEdit();            
        });
        //
        $(document).on('click', '#btn-prj-edit-back', function () {
            $('#btnPrjEditCancel').trigger('click');
        });
        //
        $(document).on('click', '.btn-prj-state-type', function () {
            $('.btn-prj-state-type').removeClass('btn-success');
            $(this).addClass('btn-success');
            $('#state_type_id').val($(this).attr('data-curr-state-type-id'));
        });
    };

    this.loadData = function () {        
        $('.btn-prj-state-type').removeClass('btn-success');
        var curr_state_type_id = $('#state_type_id').val();        
        $('.btn-prj-state-type[data-curr-state-type-id=' + curr_state_type_id + ']').addClass('btn-success');
        //loadingDialog('#prj-loading', false);
    };

    // private

    function validateData() {
        curr_prj = null;
        var ok = true;
        var curr_prj_id = $('#prj_id').val();
        var curr_prj_name = $('#prj_name').val();
        var curr_date_beg = $('#date_beg').val();
        var curr_date_end = $('#date_end').val();
        var curr_state_type_id = $('#state_type_id').val();
        var curr_project_id = $('#project_id').data('kendoDropDownList').value();

        prj_add_mode = !(curr_prj_id >= 0);

        $("#prj-main-pane-right .input-validation-error").removeClass("input-validation-error");

        if (!curr_prj_name) {
            $('#prj_name').addClass('input-validation-error');
            ok = false;
        }
        if (!curr_date_beg) {
            $('#date_beg').addClass('input-validation-error');
            ok = false;
        }
        if (!curr_date_end) {
            $('#date_end').addClass('input-validation-error');
            ok = false;
        }

        if (ok) {
            curr_prj = { 
                prj_id: curr_prj_id, 
                prj_name: curr_prj_name, 
                date_beg: curr_date_beg, 
                date_end: curr_date_end, 
                state_type_id: curr_state_type_id, 
                project_id: curr_project_id 
              };
        }

        return ok;
    };

    function closePrjEdit() {        
        toggleSplitter("#prjMainSplitter", "#prj-main-pane-left", "#prj-main-pane-right");
        //
        switch (leftPaneContentType) {
            case 1:
                refreshGrid('#prjClaimChildGrid');
                break;
            default:
                break;
        }
    };
}

var prjEditPartial = null;
var curr_prj = null;
var prj_add_mode = false;

$(function () {
    prjEditPartial = new PrjEditPartial();
    prjEditPartial.init();
});
