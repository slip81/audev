﻿function ChangePassword() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator({
            rules: {
                checkIfEmpty: function (input) {
                    var ret = true;
                    if (input.is("[name=edPwdOld]")) {
                        ret = input.val();
                    }
                    return ret;
                },
                verifyPasswords: function (input) {
                    var ret = true;
                    if (input.is("[name=edPwdNewConfirm]")) {
                        ret = input.val() === $("#edPwdNew").val();
                    }
                    return ret;
                }
            },
            messages: {
                checkIfEmpty: "Не задан пароль !",
                verifyPasswords: "Пароли не совпадают !"
            }
        });
    };

    this.onBtnSubmitClick = function (e) {
        var validator = $("form").getKendoValidator();
        if (validator.validate()) {
            showConfirmWindow('#confirm-templ', 'Изменить пароль ?', function () {
                $('#btnSubmit').addClass('k-state-disabled');
                $('#op-wait').removeClass('hidden');
                var pwd1 = $('#edPwdOld').val();
                var pwd2 = $('#edPwdNew').val();
                var pwd3 = $('#edPwdNewConfirm').val();
                $.ajax({
                    type: "POST",
                    url: url_ChangePassword,
                    data: JSON.stringify({ pwd1: pwd1, pwd2: pwd2, pwd3: pwd3 }),
                    contentType: 'application/json; charset=windows-1251',
                    success: function (response) {
                        $('#btnSubmit').removeClass('k-state-disabled');
                        $('#op-wait').addClass('hidden');
                        if ((response == null) || (response.err)) {
                            getPopup().show(response == null ? 'Ошибка при попытке сменить пароль' : response.mess, "error");
                        } else {
                            getPopup().show("Пароль успешно изменен !", "success");
                            $('#edPwdOld').val('');
                            $('#edPwdNew').val('');
                            $('#edPwdNewConfirm').val('');
                        };
                    },
                    error: function (response) {
                        $('#btnSubmit').removeClass('k-state-disabled');
                        $('#op-wait').addClass('hidden');
                        getPopup().show('Ошибка смена пароля', "error");
                    }
                });
            });
        };
    }

    function getPopup() {
        if (!popupNotification)
            popupNotification = $("#popupNotification").data("kendoNotification");
        return popupNotification;
    }
}

var changePassword = null;
var popupNotification = null;

$(function () {
    changePassword = new ChangePassword();
    changePassword.init();
});
