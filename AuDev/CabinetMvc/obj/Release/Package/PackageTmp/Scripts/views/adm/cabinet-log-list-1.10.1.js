﻿function CabinetLogList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#admEventGrid', 0);
    };
}

var cabinetLogList = null;

$(function () {
    cabinetLogList = new CabinetLogList();
    cabinetLogList.init();
});
