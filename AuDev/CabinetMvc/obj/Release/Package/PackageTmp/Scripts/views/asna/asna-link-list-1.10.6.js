﻿function AsnaLinkList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#asnaLinkGrid', 50);
    };

    this.onAsnaUserClientDdlChange = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserClientDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserSalesDdlChange = function (e) {
        refreshGrid('#asnaLinkGrid');
    };

    this.onAsnaUserSalesDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').select(0);
        refreshGrid('#asnaLinkGrid');
    };

    this.getAsnaUserSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#asnaUserClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    }

    this.getAsnaLinkGridData = function (e) {
        var curr_sales_id = null;        
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        return { sales_id: curr_sales_id };
    };

    this.onAsnaLinkGridDataBound = function (e) {
        drawEmptyRow('#asnaLinkGrid');
        //
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if ((dataItem) && (!dataItem.is_unique)) {
                row.addClass("k-info-colored");
            };
        };
    };

    this.onBtnAsnaUserEditClick = function (e) {
        var curr_sales_id = null;
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        if (!curr_sales_id) {
            alert('Не выбрана торговая точка');
            return;
        };
        location.href = '/AsnaSalesEdit?sales_id=' + curr_sales_id;
    };

    this.onBtnAsnaLinkGridExcelClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Выгрузить список в Excel ?', function () {
            $('#asnaLinkGrid').data('kendoGrid').saveAsExcel();
        });
    };
}

var asnaLinkList = null;
$(function () {
    asnaLinkList = new AsnaLinkList();
    asnaLinkList.init();
});
