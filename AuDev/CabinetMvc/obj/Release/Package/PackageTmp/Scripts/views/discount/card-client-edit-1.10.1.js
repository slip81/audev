﻿function CardClientEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $('.attr-change').bind('input propertychange', _this.onAttrChanged)

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            $('#card_num_rel').val($('#acCardNum').getKendoAutoComplete().value());

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('btn-primary');
                                $('#btnSubmit').addClass('btn-default');
                                $('#btnSubmit').removeClass('active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-warning');
                                $('#btnRevert').addClass('hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();

            $("#client_surname").val(model_client_surname);
            $("#client_name").val(model_client_name);
            $("#client_fname").val(model_client_fname);
            $("#phone_num").val(model_phone_num);
            $("#email").val(model_email);
            $("#date_birth").getKendoDatePicker().value(model_DateBirth);
            $("#address").val(model_address);
            $("#comment").val(model_comment);
            $('#acCardNum').getKendoAutoComplete().value(model_card_num_rel);

            $('#btnSubmit').removeClass('btn-primary');
            $('#btnSubmit').addClass('btn-default');
            $('#btnSubmit').removeClass('active');
            $('#btnSubmit').attr('disabled', true);
            $('#btnRevert').removeClass('btn-warning');
            $('#btnRevert').addClass('hidden');
        });
    };

    this.onAttrChanged = function() {
        $('#btnSubmit').addClass('btn-primary');
        $('#btnSubmit').removeClass('btn-default');
        $('#btnSubmit').removeAttr('disabled');
        $('#btnSubmit').addClass('active');
        $('#btnRevert').removeClass('hidden');
        $('#btnRevert').addClass('btn-warning');
    };


    this.onAdditionalData = function(e) {
        var ctrl = $(e);
        var card_num_part = ctrl[0].filter.filters[0].value;
        return {
            text: card_num_part
        };
    };

}

var cardClientEdit = null;

$(function () {
    cardClientEdit = new CardClientEdit();
    cardClientEdit.init();
});
