﻿function UserProfile() {
    _this = this;

    this.init = function () {
        $('#start_page_title_text').text($('#start_page_title').val());

        $('#btnSubmit').click(function () {
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var details = $('#userMenuGrid').getKendoGrid().dataSource.view();

            kendoWindow.data("kendoWindow")
                .content($("#save-changes-confirmation").html())
                .center().open();

            kendoWindow.find(".save-changes-confirm,.save-changes-cancel")
                    .click(function () {
                        if ($(this).hasClass("save-changes-confirm")) {
                            kendoWindow.data("kendoWindow").close();
                            $('#btnSubmit').attr('disabled', true);
                            $('#btnMakeStart').attr('disabled', true);
                            $('#btnToMyPages').attr('disabled', true);

                            var postData = JSON.stringify({ profile_id: $('#profile_id').val(), user_name: $('#user_name').val(), start_page: $('#start_page').val(), userMenuList: details });
                            $.ajax({
                                type: "POST",
                                url: "UserProfile",
                                data: postData,
                                contentType: 'application/json; charset=windows-1251',
                                success: function (response) {
                                    $('#btnSubmit').attr('disabled', false);
                                    $('#btnMakeStart').attr('disabled', false);
                                    $('#btnToMyPages').attr('disabled', false);
                                    if (response.err) {
                                        if (response.mess) {
                                            alert(response.mess);
                                        } else {
                                            alert('Ошибка при сохранении профиля');
                                        };
                                    } else {
                                        window.location.href = window.location.href;
                                    };
                                    kendoWindow.data("kendoWindow").close();
                                },
                                error: function (response) {
                                    $('#btnSubmit').attr('disabled', false);
                                    $('#btnMakeStart').attr('disabled', false);
                                    $('#btnToMyPages').attr('disabled', false);
                                    alert('Ошибка при попытке сохранения профиля');
                                    kendoWindow.data("kendoWindow").close();
                                }
                            });

                            return true;
                        } else {
                            kendoWindow.data("kendoWindow").close();
                            return false;
                        };
                    })
                 .end();
        });

        $('#btnMakeStart').click(function () {
            var ddl = $('#startPageDdl').getKendoDropDownList();
            var selectedItem = ddl.dataItem();
            if (selectedItem) {
                $('#start_page_title_text').text(selectedItem.title);
                $('#start_page').val(selectedItem.action);
            };
        });

        $('#btnToMyPages').click(function () {
            var targetGrid = $("#userMenuGrid").getKendoGrid();
            var ddl = $('#startPageDdl').getKendoDropDownList();
            var selectedItem = ddl.dataItem();
            if (selectedItem) {
                var id = selectedItem.action;
                if (targetGrid.dataSource.get(id) == null) {
                    targetGrid.dataSource.add(selectedItem);
                };
            };
            return false;
        });

        $('#btn-remove').click(function () {
            var sourceGrid = $("#userMenuGrid").getKendoGrid();
            var rows = sourceGrid.select();
            rows.each(function (index, row) {
                var selectedItem = sourceGrid.dataItem(row);
                if (selectedItem != null) {
                    sourceGrid.dataSource.remove(selectedItem);
                };
            });
            return false;
        });

        $('#btn-remove-all').click(function () {
            var sourceGrid = $("#userMenuGrid").getKendoGrid();
            sourceGrid.dataSource.data([]);
            return false;
        });
    };

    this.placeholder = function(element) {
        return element.clone().addClass("k-state-hover").css("opacity", 0.65);
    }

    this.onChange = function (e) {
        var grid = $("#userMenuGrid").getKendoGrid(),
            oldIndex = e.oldIndex,
            newIndex = e.newIndex,
            data = grid.dataSource.data(),
            dataItem = grid.dataSource.getByUid(e.item.data("uid"));

        grid.dataSource.remove(dataItem);
        grid.dataSource.insert(newIndex, dataItem);
    };

    this.onStartPageDdlSelect = function(e) {        
        var dataItem = this.dataItem(e.item.index());
        if (dataItem["action"] == -1) {
            e.preventDefault();
            var oldVal = this.value();
            this.value(oldVal);
        };

    };

    this.onUserMenuGridDataBound = function(e) {
        drawEmptyRow('#userMenuGrid');
    };

}

var userProfile = null;

var noHint = $.noop;

$(function () {
    userProfile = new UserProfile();
    userProfile.init();
});
