﻿function AsnaTaskList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#asnaTaskGrid', 50);
    };

    this.onAsnaUserClientDdlChange = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserClientDdlDataBound = function (e) {
        $('#asnaUserSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onAsnaUserSalesDdlChange = function (e) {
        refreshGrid('#asnaTaskGrid');
    };

    this.onAsnaUserSalesDdlDataBound = function (e) {        
        $('#asnaUserSalesDdl').data('kendoDropDownList').select(0);
        refreshGrid('#asnaTaskGrid');
    };

    this.getAsnaUserSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#asnaUserClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    }

    this.getAsnaTaskGridData = function (e) {
        var curr_sales_id = null;        
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        };
        var curr_task_state_id_list = $('#asnaTaskToolbar>.k-button-group>[data-group="asna-task-filter-group"].k-toggle-button.k-state-active').attr('data-task-state-id');
        return { sales_id: curr_sales_id, task_state_id_list: curr_task_state_id_list };
    };

    this.onAsnaTaskGridDataBound = function (e) {
        drawEmptyRow('#asnaTaskGrid');
        //
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if (dataItem) {
                switch (dataItem.task_state_id) {
                    case 6:
                    case 8:
                        row.addClass("k-error-colored");
                        break;
                    case 9:
                    case 10:                        
                        row.addClass("k-info-colored");
                        break;
                    default:
                        break;
                };
            };
        };
    };

    this.onBtnAsnaUserEditClick = function (e) {
        var curr_sales_id = null;
        var dataItem = $('#asnaUserSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        }
        if (!curr_sales_id) {
            alert('Не выбрана торговая точка');
            return;
        };
        location.href = '/AsnaSalesEdit?sales_id=' + curr_sales_id;
    };

    this.onAsnaTaskToolbarToggle = function (e) {
        refreshGrid('#asnaTaskGrid');
    };
}

var asnaTaskList = null;
$(function () {
    asnaTaskList = new AsnaTaskList();
    asnaTaskList.init();
});
