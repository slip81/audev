﻿function AuthUserPermEdit() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#authUserRoleSectionGrid', 0);
        //
        $(document).on('click', '.chb-allow-view-section', function () {
            var grid = $("#authUserRoleSectionGrid").getKendoGrid();
            var isChecked = $(this).is(':checked');
            var row = $(this).closest("tr");
            var dataItem = grid.dataItem(row);
            dataItem.allow_view = isChecked;
            var detailGridId = '#grid1_' + dataItem.section_id;
            $(detailGridId).find('.chb-allow-view-part').prop('checked', isChecked);
            var detailGrid = $(detailGridId).getKendoGrid();
            if (detailGrid) {
                $.each($(detailGridId).find('tr'), function (index, value) {
                    var detailDataItem = detailGrid.dataItem(value);
                    if (detailDataItem) {
                        detailDataItem.allow_view = isChecked;
                    };
                });
            };
        });
        //
        $(document).on('click', '.chb-allow-edit-section', function () {
            var grid = $("#authUserRoleSectionGrid").getKendoGrid();
            var isChecked = $(this).is(':checked');
            var row = $(this).closest("tr");
            var dataItem = grid.dataItem(row);
            dataItem.allow_edit = isChecked;
            var detailGridId = '#grid1_' + dataItem.section_id;
            $(detailGridId).find('.chb-allow-edit-part').prop('checked', isChecked);
            var detailGrid = $(detailGridId).getKendoGrid();
            if (detailGrid) {
                $.each($(detailGridId).find('tr'), function (index, value) {
                    var detailDataItem = detailGrid.dataItem(value);
                    if (detailDataItem) {
                        detailDataItem.allow_edit = isChecked;
                    };
                });
            };
        });
        //
        $(document).on('click', '.chb-allow-view-part', function (e) {
            var grid = $(e.target).parents('.k-grid').getKendoGrid();
            var isChecked = $(this).is(':checked');
            var row = $(this).closest("tr");
            var dataItem = grid.dataItem(row);
            dataItem.allow_view = isChecked;
        });
        //
        $(document).on('click', '.chb-allow-edit-part', function (e) {
            var grid = $(e.target).parents('.k-grid').getKendoGrid();
            var isChecked = $(this).is(':checked');
            var row = $(this).closest("tr");
            var dataItem = grid.dataItem(row);
            dataItem.allow_edit = isChecked;
        });

        $('#btnSave').click(function (e) {        
            var data2 = [];
            for (i = 1; i <= 20; i++)
            {
                var grid = $("#grid1_" + i).getKendoGrid();
                if (grid) {
                    data2.push(grid.dataSource.view());
                }

            }
            var role_id =  $('#authRoleList').getKendoDropDownList().value();
            var postData = { permList: data2, role_id: role_id, user_id: model_user_id };
            $.ajax({
                type: "POST",
                url: "AuthUserPermEdit",
                data: JSON.stringify(postData),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if (response.err) {
                        if (response.mess) {
                            alert(response.mess);
                        } else {
                            alert('Ошибка при сохранении изменений');
                        };
                    } else {
                        //
                    };
                    refreshGrid('#authUserRoleSectionGrid');
                },
                error: function (response) {
                    alert('Ошибка при попытке сохранения изменений');
                }
            });
        });

        $('#btnCancel').click(function (e) {
            $('#authRoleList').getKendoDropDownList().value(init_role_id.toString());
            new_role_id = -1;
            refreshGrid('#authUserRoleSectionGrid');
        });
    };

    this.onAuthUserRoleSectionGridDataBound = function(e) {
        drawEmptyRow('#authUserRoleSectionGrid');
    };

    this.onAuthUserRolePartGridDataBound = function(e) {
        var grid_id = $(this.wrapper).attr('id');
        drawEmptyRow('#' + grid_id);
    };

    this.onAuthRoleListChange = function(e) {
        new_role_id = $('#authRoleList').getKendoDropDownList().value();
        refreshGrid('#authUserRoleSectionGrid');
    };

    this.getUserRoleSectionGridData = function() {
        return { user_id: model_user_id, role_id: new_role_id, group_id: 1 };
    };

    this.getUserRolePartGridData = function() {
        return { role_id: new_role_id, group_id: 1};
    };
}

var authUserPermEdit = null;

var new_role_id = -1;

$(function () {
    authUserPermEdit = new AuthUserPermEdit();
    authUserPermEdit.init();
});
