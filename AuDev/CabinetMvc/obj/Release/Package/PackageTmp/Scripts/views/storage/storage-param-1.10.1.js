﻿function StorageParam() {
    _this = this;

    this.init = function () {
        //
    };

    this.onFileTypeGridDataBound = function (e) {
        this.table.find(".k-grid-edit").hide();
    };

    this.onMaxFileSizeListChange = function (e) {
        $('#max_file_size').val($('#maxFileSizeList').getKendoDropDownList().value());
        $.ajax({
            url: url_StorageParamSave,
            type: "POST",
            data: {
                max_file_size: $('#max_file_size').val()
            },
            success: function (response) {                
                if ((response == null) || (response.err)) {
                    alert(response == null ? 'Ошибка при сохранении настроек' : response.mess);
                };
            },
            error: function (response) {                
                alert('Ошибка сохранения настроек');
            }
        });
    };
}

var storageParam = null;

$(function () {
    storageParam = new StorageParam();
    storageParam.init();
});
