﻿function CampaignList() {
    _this = this;

    this.init = function () {
        $('#btnCampaignAdd').children('span').first().addClass('k-icon k-i-plus');
    };

    this.onCampaignListGridDataBound = function (e) {
        drawEmptyRow('#campaignListGrid');
    };
}

var campaignList = null;

$(function () {
    campaignList = new CampaignList();
    campaignList.init();
});
