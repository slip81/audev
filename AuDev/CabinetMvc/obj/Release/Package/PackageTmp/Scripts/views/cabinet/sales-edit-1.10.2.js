﻿function SalesEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();        

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#regionList").getKendoComboBox();
            var prev_region_name = $("#RegionName_Selected").val();
            var curr_region_name = combo1.text();
            if (prev_region_name != curr_region_name) {
                $("#RegionName_Selected").val(curr_region_name);
                $("#RegionName_Changed").val(true);
            } else {
                $("#RegionName_Changed").val(false);
            };

            var combo2 = $("#cityList").getKendoComboBox();
            var prev_city_name = $("#CityName_Selected").val();
            var curr_city_name = combo2.text();
            if (prev_city_name != curr_city_name) {
                $("#CityName_Selected").val(curr_city_name);
                $("#CityName_Changed").val(true);
            } else {
                $("#CityName_Changed").val(false);
            };

            var combo3 = $("#regionZoneList").data('kendoDropDownList');
            var curr_region_zone_dataItem = combo3.dataItem();
            var curr_region_zone_id = null;
            if ((curr_region_zone_dataItem) && (curr_region_zone_dataItem.zone_id)) {
                curr_region_zone_id = curr_region_zone_dataItem.zone_id;
            }
            $("#region_zone_id").val(curr_region_zone_id);

            var combo4 = $("#taxSystemList").data('kendoDropDownList');
            var curr_tax_system_type_dataItem = combo4.dataItem();
            var curr_tax_system_type_id = null;
            if ((curr_tax_system_type_dataItem) && (curr_tax_system_type_dataItem.system_type_id)) {
                curr_tax_system_type_id = curr_tax_system_type_dataItem.system_type_id;
            }
            $("#tax_system_type_id").val(curr_tax_system_type_id);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('k-state-active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-revert-visible');
                                $('#btnRevert').removeClass('k-state-focused');
                                $('#btnRevert').addClass('btn-revert-hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();
            $('#btnSubmit').removeClass('k-state-active');
            $('#btnSubmit').attr('disabled', true);
            $('#btnRevert').removeClass('btn-revert-visible');
            $('#btnRevert').removeClass('k-state-focused');
            $('#btnRevert').addClass('btn-revert-hidden');
        });
    };


    this.filterRegion = function() {
        return {
            region_id: $("#regionList").val()
        };
    };

    this.onRegionListChange = function(e) {
        $("#cityList").getKendoComboBox().enable(true);
    };
}

var salesEdit = null;

$(function () {
    salesEdit = new SalesEdit();
    salesEdit.init();
});
