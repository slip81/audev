﻿function RegList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#regListGrid', 0);

        $('.btn-scope').click(function (e) {
            var curr_id = $(this).attr('id');
            $('.btn-scope[id!=' + curr_id + ']').removeClass('btn-primary');
            $(this).addClass('btn-primary');
            //
            if (curr_id == 'btn-scope-all') {
                curr_scope = -1;
            } else if (curr_id == 'btn-scope-active') {
                curr_scope = 0;
            } else if (curr_id == 'btn-scope-accepted') {
                curr_scope = 1;
            } else if (curr_id == 'btn-scope-rejected') {
                curr_scope = 2;
            } else {
                curr_scope = -1;
            };
            //
            $('#regListGrid').getKendoGrid().dataSource.read();
        });
    };

    this.onRegListGridDataBound = function(e) {
        drawEmptyRow('#regListGrid');
    };

    this.getScope = function(e) {
        return { scope: curr_scope };
    };
}

var regList = null;

var curr_scope = 0;

$(function () {
    regList = new RegList();
    regList.init();
});
