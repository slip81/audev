﻿function CardClientList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#cardClientListGrid', 0);

        $('#btnCardClientAdd').children('span').first().addClass('k-icon k-i-plus');
    };

    this.onRequestEnd = function(e) {
        if (e.response.Data && e.response.Data.length) {
            var data = e.response.Data;
            if (e.type == "read") {
                loopRecords(data);
            }
        }
    };

    function loopRecords(clients) {
        for (var i = 0; i < clients.length; i++) {
            var client = clients[i];
            offsetDateFields(client);
        }
    };

    function offsetDateFields(obj) {
        for (var name in obj) {
            var prop = obj[name];
            if (typeof (prop) === "string" && prop.indexOf("/Date(") == 0) {
                obj[name] = prop.replace(/\d+/, function (n) {
                    var offsetMiliseconds = new Date(parseInt(n)).getTimezoneOffset() * 60000;
                    return parseInt(n) + offsetMiliseconds
                });
            }
        }
    };
}

var cardClientList = null;

$(function () {
    cardClientList = new CardClientList();
    cardClientList.init();
});
