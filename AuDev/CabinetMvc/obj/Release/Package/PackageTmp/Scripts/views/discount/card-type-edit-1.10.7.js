﻿function CardTypeEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $('.attr-change').bind('input propertychange', _this.onAttrChanged)

        $('#cardTypeBusinessOrgListGrid').on('click', '.chkbx1', function () {
            var checked = $(this).is(':checked');
            var grid = $('#cardTypeBusinessOrgListGrid').data().kendoGrid;
            var dataItem = grid.dataItem($(this).closest('tr'));
            dataItem.set('is_org_ok', checked);
            dataItem.dirty = true;
        });

        $('#cardTypeBusinessOrgListGrid').on('click', '.chkbx2', function () {
            var checked = $(this).is(':checked');
            var grid = $('#cardTypeBusinessOrgListGrid').data().kendoGrid;
            var dataItem = grid.dataItem($(this).closest('tr'));
            dataItem.set('allow_discount', checked);
            dataItem.dirty = true;
        });

        $('#cardTypeBusinessOrgListGrid').on('click', '.chkbx3', function () {
            var checked = $(this).is(':checked');
            var grid = $('#cardTypeBusinessOrgListGrid').data().kendoGrid;
            var dataItem = grid.dataItem($(this).closest('tr'));
            dataItem.set('allow_bonus_for_pay', checked);
            dataItem.dirty = true;
        });

        $('#cardTypeBusinessOrgListGrid').on('click', '.chkbx4', function () {
            var checked = $(this).is(':checked');
            var grid = $('#cardTypeBusinessOrgListGrid').data().kendoGrid;
            var dataItem = grid.dataItem($(this).closest('tr'));
            dataItem.set('allow_bonus_for_card', checked);
            dataItem.dirty = true;
        });

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var combo1 = $("#cardTypeGroupList").data("kendoDropDownList");
            $("#card_type_group_id").val(combo1.dataItem()["card_type_group_id"]);
            var combo2 = $("#programmList").data("kendoDropDownList");
            $("#programm_id").val(combo2.dataItem()["programm_id_str"]);

            var chb1 = ($('#chb_reset_interval_bonus_value').is(':checked') && ((combo1.dataItem()["card_type_group_id"] == 2) || (combo1.dataItem()["card_type_group_id"] == 3)));
            $('#reset_interval_bonus_value').val(chb1 ? $('#reset_interval_bonus_value').val() : null);

            var chb2 = ($('#chb_is_reset_without_sales').is(':checked') && ((combo1.dataItem()["card_type_group_id"] == 2) || (combo1.dataItem()["card_type_group_id"] == 3)));
            $('#is_reset_without_sales').val(chb2 ? 1 : 0);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('btn-primary');
                                $('#btnSubmit').addClass('btn-default');
                                $('#btnSubmit').removeClass('active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-warning');
                                $('#btnRevert').addClass('hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnRevert").click(function (e) {
            $("form").getKendoValidator().hideMessages();

            $("#card_type_name").val(model_card_type_name);
            $("#cardTypeGroupList").getKendoDropDownList().value(model_card_type_group_id);
            $("#update_nominal_bool").prop('checked', model_update_nominal_bool);
            $("#programmList").getKendoDropDownList().value(model_programm_id);
            $("#edProgrammDescr").val($("#programmList").data("kendoDropDownList").dataItem()["programm_descr"]);

            $("#is_active").prop('checked', model_is_active);
            $("#print_bonus_percent_card").prop('checked', model_print_bonus_percent_card);
            $("#print_bonus_value_card").prop('checked', model_print_bonus_value_card);
            $("#print_bonus_value_trans").prop('checked', model_print_bonus_value_trans);
            $("#print_disc_percent_card").prop('checked', model_print_disc_percent_card);
            $("#print_sum_trans").prop('checked', model_print_sum_trans);
            $("#print_inactive_bonus_value_card").prop('checked', model_print_inactive_bonus_value_card);

            $("#print_used_bonus_value_trans").prop('checked', model_print_used_bonus_value_trans);
            $("#print_card_num").prop('checked', model_print_card_num);
            $("#print_programm_descr").prop('checked', model_print_programm_descr);
            $("#print_cert_sum_card").prop('checked', model_print_cert_sum_card);
            $("#print_cert_sum_trans").prop('checked', model_print_cert_sum_trans);
            $("#print_cert_sum_left").prop('checked', model_print_cert_sum_left);

            $('#btnSubmit').removeClass('btn-primary');
            $('#btnSubmit').addClass('btn-default');
            $('#btnSubmit').removeClass('active');
            $('#btnSubmit').attr('disabled', true);
            $('#btnRevert').removeClass('btn-warning');
            $('#btnRevert').addClass('hidden');
        });
    };

    this.onAttrChanged = function() {
        $('#btnSubmit').addClass('btn-primary');
        $('#btnSubmit').removeClass('btn-default');
        $('#btnSubmit').removeAttr('disabled');
        $('#btnSubmit').addClass('active');
        $('#btnRevert').removeClass('hidden');
        $('#btnRevert').addClass('btn-warning');
    };

    this.onProgrammChange = function(e) {
        var combo2 = $("#programmList").data("kendoDropDownList");
        $("#edProgrammDescr").val(combo2.dataItem()["programm_descr"]);
        _this.onAttrChanged();
    };

    this.onRequestEnd = function(e) {
        if (e.type == "create") {
            $("#cardTypeLimitListGrid").data("kendoGrid").dataSource.read();
        }
    };

    this.onCardTypeGroupListDataBound = function(e) {
        updateVisibility(e.sender.value());
    };

    this.onCardTypeGroupListChange = function(e) {
        updateVisibility(e.sender.value());
        _this.onAttrChanged();
    };

    // private

    function updateVisibility(card_type_group_id) {        
            if ((card_type_group_id == 2) || (card_type_group_id == 3)) {
                $('.bonus-card-only').show();
            } else {
                $('.bonus-card-only').hide();
            };
    }
}

var cardTypeEdit = null;

$(function () {
    cardTypeEdit = new CardTypeEdit();
    cardTypeEdit.init();
});
