﻿function PrjSettingsPartial() {
    _this = this;

    this.init = function () {
        //        
        $(document).on('click', '#btnPrjSettingsSubmit', function () {            
            var prjModel = {
                start_page: $('#prjSettingsStartPageToolbar>.k-button-group>[data-group="start-page-group"].k-toggle-button.k-state-active').attr('data-start-page')
            };
            //
            var claimModel = {
                prj_id: $('#prjClaimDefaultDdl').data('kendoDropDownList').value(),
                client_id: $('#clientClaimDefaultDdl').data('kendoDropDownList').value(),
                priority_id: $('#priorityClaimDefaultDdl').data('kendoDropDownList').value(),                
                resp_user_id: $('#respUserClaimDefaultDdl').data('kendoDropDownList').value(),
                project_id: $('#projectClaimDefaultDdl').data('kendoDropDownList').value(),
                module_id: $('#moduleClaimDefaultDdl').data('kendoDropDownList').value(),
                module_part_id: $('#modulePartClaimDefaultDdl').data('kendoDropDownList').value(),
                module_version_id: $('#moduleVersionClaimDefaultDdl').data('kendoDropDownList').value(),
                repair_version_id: $('#repairVersionClaimDefaultDdl').data('kendoDropDownList').value()
            };
            //
            var workListModel = [];
            var def_work_type = false;
            for (work_type_id = 1; work_type_id <= 6; work_type_id++) {
                def_work_type = $('#work_default_' + work_type_id).is(':checked');
                if (def_work_type) {
                    workListModel.push({
                        is_default: true,
                        work_type_id: work_type_id,
                        is_accept: $('#work_default_is_accept_' + work_type_id).is(':checked'),
                        exec_user_id: $('#work_default_exec_user_ddl_' + work_type_id).data('kendoDropDownList').value()
                    });
                }
            }
            //
            loadingDialog('#prj-loading', true);
            $.ajax({
                url: '/PrjSettingsPartial',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ prj: prjModel, claim: claimModel, workList: workListModel }),
                success: function (response) {                        
                    if ((response == null) || (response.err)) {
                        alert(response == null ? 'Ошибка при попытке сохранения' : response.mess);
                    } else {		
                        //
                    };
                },
                error: function (response) {
                    alert('Ошибка при сохранении');
                },
                complete: function (response) {
                    loadingDialog('#prj-loading', false);
                }
            });
        });
    }

    this.init_after = function () {
        if (prjSettings_inited) {
            return;
        }
        //

        //
        prjSettings_inited = true;
    };

    this.onPrjSettingsStartPageToolbarToggle = function (e) {
        //
    };

    this.onPrjWorkDefaultGridDataBound = function (e) {
        $("#prjWorkDefaultGrid .templateCell").each(function () {
            eval($(this).children("script").last().html());
        });
    };
}

var prjSettingsPartial = null;
var prjSettings_inited = false;

$(function () {
    prjSettingsPartial = new PrjSettingsPartial();
    prjSettingsPartial.init();
});
