﻿function ServiceEsn2() {
    _this = this;

    this.init = function () {
        //
    };

    this.onEsn2GridDataBound = function(e) {
        drawEmptyRow('#esn2Grid');
        e.sender.table.find(".k-grid-edit").hide();
    };
}

var serviceEsn2 = null;

$(function () {
    serviceEsn2 = new ServiceEsn2();
    serviceEsn2.init();
});
