﻿function CardTransactionList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#cardTransactionListGrid', 0);
    };

    this.cardTransactionsFilter = function() {
        var grid = $("#cardTransactionListGrid").data("kendoGrid"),
            ed1 = $("#edCardNum").data("kendoNumericTextBox"),
            ed2 = $("#edDateBeg").data("kendoDatePicker"),
            ed3 = $("#edDateEnd").data("kendoDatePicker")
        ;

        var card_num_val = ed1.value(),
            date_beg_val = ed2.value(),
            date_end_val = ed3.value()
        ;

        if (date_end_val) {
            date_end_val.setDate(date_end_val.getDate() + 1);
        }

        if ((card_num_val) || (date_beg_val) || (date_end_val)) {
            var _fltStatus = { logic: "and", filters: [] };
            if (card_num_val) {
                _fltStatus.filters.push({ field: "card_num", operator: "eq", value: card_num_val });
            }
            if (date_beg_val) {
                _fltStatus.filters.push({ field: "date_beg", operator: "gte", value: date_beg_val });
            }
            if (date_end_val) {
                _fltStatus.filters.push({ field: "date_beg", operator: "lte", value: date_end_val });
            }
            grid.dataSource.filter(_fltStatus);
        }
    };

    this.onCardTransactionListGridDataBound = function (e) {
        drawEmptyRow('#cardTransactionListGrid');
    };
}

var cardTransactionList = null;

$(function () {
    cardTransactionList = new CardTransactionList();
    cardTransactionList.init();
});
