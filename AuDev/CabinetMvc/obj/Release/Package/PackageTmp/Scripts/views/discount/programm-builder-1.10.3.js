﻿function ProgrammBuilder() {
    _this = this;

    this.init = function () {
        $('#btnProgrammSave').click(function (e) {
            showConfirmWindow('#confirm-templ', 'Сохранить алгоритм ?', function () {
                $.ajax({
                    type: 'POST',
                    url: '/ProgrammHeaderEdit',
                    data: JSON.stringify(
                        {
                            programm_id: $('#edProgrammId').val(),
                            //business_id: $('#businessDdl').data('kendoDropDownList').value(),
                            programm_name: $('#edProgrammName').val(),
                            date_beg: $('#edDateBeg').data('kendoDatePicker').value(),
                            date_end: $('#edDateEnd').data('kendoDatePicker').value(),
                            descr_short: $('#edDescrShort').val(),
                            descr_full: $('#edDescrFull').val(),
                            card_type_group_id: $('#edCardTypeGroup').data('kendoDropDownList').value()
                        }),
                    contentType: 'application/json; charset=windows-1251',
                    success: function (response) {                        
                        if ((response == null) || (response.err)) {
                            alert(response == null ? 'Ошибка при попытке сохранить алгоритм' : response.mess);
                        } else {
                            curr_programm_id = $('#edProgrammId').val();
                            $('#programmDdl').data('kendoDropDownList').dataSource.read();
                            //alert('Алгоритм сохранен !');
                        };
                    },
                    error: function (response) {                        
                        alert('Ошибка при сохранения алгоритма');
                    }
                });
            });
        });
    };

    this.onBusinessDdlChange = function (e) {
        $('#programmDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onBusinessDdlDataBound = function (e) {
        $('#programmDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onProgrammDdlChange = function (e) {        
        refreshProgramm();
    };

    this.onProgrammDdlDataBound = function (e) {
        if (curr_programm_id) {
            $('#programmDdl').data('kendoDropDownList').value(curr_programm_id);            
        } else {
            $('#programmDdl').data('kendoDropDownList').select(0);
        }
        curr_programm_id = null;
        refreshProgramm();
    };

    this.getProgrammDdlData = function (e) {
        var curr_business_id_str = null;
        var dataItem = $('#businessDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.business_id_str)) {
            curr_business_id_str = dataItem.business_id_str;
        }
        return { business_id_str: curr_business_id_str };
    };

    this.getProgrammParamGridData = function (e) {
        var curr_programm_id_str = null;
        var dataItem = $('#programmDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.programm_id_str)) {
            curr_programm_id_str = dataItem.programm_id_str;
        }
        return { programm_id_str: curr_programm_id_str };
    };

    this.onProgrammParamGridDataBound = function (e) {
        drawEmptyRow('#programmParamGrid');
    };

    this.onPbPanelExpand = function (e) {        
        var expandedId = $(e.item).attr('id');        
        switch (expandedId) {
            case 'panel-programm-param':
                refreshGrid('#programmParamGrid');
                break;
            case 'panel-programm-step':
                refreshGrid('#programmStepGrid');
                break;
            case 'panel-programm-step-param':
                refreshGrid('#programmStepParamGrid');
                break;
            case 'panel-programm-step-condition':
                refreshGrid('#programmStepConditionGrid');
                break;
            case 'panel-programm-step-logic':
                refreshGrid('#programmStepLogicGrid');
                break;
            default:
                break;
        }
    };

    this.onDdlProgrammParamTypeChange = function (e) {
        toggleProgrammParamType(e.sender.dataItem());
    };

    this.onProgrammParamGridEdit = function (e) {        
        toggleProgrammParamType(e.model);
    };

    this.onProgrammStepGridDataBound = function (e) {
        drawEmptyRow('#programmStepGrid');
        $('#programmStepGrid').data('kendoGrid').select('#programmStepGrid tr:eq(1)');
    };

    this.onProgrammStepGridChange = function (e) {
        if ($('#panel-programm-step-param').hasClass('k-state-active')) {
            refreshGrid('#programmStepParamGrid');
        };
        if ($('#panel-programm-step-condition').hasClass('k-state-active')) {
            refreshGrid('#programmStepConditionGrid');
        };
        if ($('#panel-programm-step-logic').hasClass('k-state-active')) {
            refreshGrid('#programmStepLogicGrid');
        };
    };

    this.getProgrammStepParamGridData = function (e) {
        var curr_programm_id_str = null;
        var dataItem = $('#programmDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.programm_id_str)) {
            curr_programm_id_str = dataItem.programm_id_str;
        }
        var curr_step_id_str = null;
        var row = $('#programmStepGrid').data('kendoGrid').select();
        if ((row) && (row.length > 0)) {
            var dataItem2 = $('#programmStepGrid').data('kendoGrid').dataItem(row);
            if ((dataItem2) && (dataItem2.step_id_str)) {
                curr_step_id_str = dataItem2.step_id_str;
            }
        }
        return { parent_programm_id_str: curr_programm_id_str, parent_step_id_str: curr_step_id_str };
    };

    this.onProgrammStepParamGridDataBound = function (e) {
        drawEmptyRow('#programmStepParamGrid');
    };

    this.onProgrammStepParamGridEdit = function (e) {
        toggleProgrammStepParamType(e.model);
    };

    this.onProgrammStepParamTypeDdlChange = function (e) {
        toggleProgrammStepParamType(e.sender.dataItem());
    };

    this.onProgrammStepParamGridSave = function (e) {
        //var isAddMode = !(e.model.id);
        var dataItem_stepParamType = $('.step-param-type-id>input').data('kendoDropDownList').dataItem();
        if ((dataItem_stepParamType) && (dataItem_stepParamType.param_type_id)) {
            e.model.set('param_type_id', dataItem_stepParamType.param_type_id);
        } else {
            e.preventDefault();
            alert('Не задан тип параметра');
        };
        //
        switch (dataItem_stepParamType.param_type_id) {
            case 1:
                var dataItem_cardParamType = $('.step-card-param-type-id>input').data('kendoDropDownList').dataItem();
                if ((dataItem_cardParamType) && (dataItem_cardParamType.card_param_type)) {
                    e.model.set('card_param_type', dataItem_cardParamType.card_param_type);
                } else {
                    e.preventDefault();
                    alert('Не задан параметр карты');
                };
                break;
            case 2:
                var dataItem_transParamType = $('.step-trans-param-type-id>input').data('kendoDropDownList').dataItem();
                if ((dataItem_transParamType) && (dataItem_transParamType.trans_param_type)) {
                    e.model.set('trans_param_type', dataItem_transParamType.trans_param_type);
                } else {
                    e.preventDefault();
                    alert('Не задан параметр продажи');
                };
                break;
            case 3:
                var dataItem_programmParamType = $('.step-programm-param-type-id>input').data('kendoDropDownList').dataItem();
                if ((dataItem_programmParamType) && (dataItem_programmParamType.param_id_str)) {
                    e.model.set('programm_param_id_str', dataItem_programmParamType.param_id_str);
                } else {
                    e.preventDefault();
                    alert('Не задан параметр алгоритма');
                };
                break;
            case 4:
                var dataItem_prevStepParamType = $('.step-prev-step-param-type-id>input').data('kendoDropDownList').dataItem();
                if ((dataItem_prevStepParamType) && (dataItem_prevStepParamType.param_id_str)) {
                    e.model.set('prev_step_param_id_str', dataItem_prevStepParamType.param_id_str);
                } else {
                    e.preventDefault();
                    alert('Не задан параметр другого шага');
                };
                break;
            default:
                break;
        }
    };

    this.onProgrammStepConditionGridDataBound = function (e) {
        drawEmptyRow('#programmStepConditionGrid');
    };

    this.onProgrammStepConditionGridSave = function (e) {
        var dataItem_stepConditionType = $('.step-condition-type-id>input').data('kendoDropDownList').dataItem();
        if ((dataItem_stepConditionType) && (dataItem_stepConditionType.condition_type_id)) {
            e.model.set('condition_type_id', dataItem_stepConditionType.condition_type_id);
        } else {
            e.preventDefault();
            alert('Не задан тип условия');
        };
    };

    this.onProgrammStepLogicGridDataBound = function (e) {
        drawEmptyRow('#programmStepLogicGrid');
    };

    this.onProgrammStepLogicGridSave = function (e) {
        //
        var dataItem_stepLogicType = $('.step-logic-type-id>input').data('kendoDropDownList').dataItem();
        if ((dataItem_stepLogicType) && (dataItem_stepLogicType.logic_type_id)) {
            e.model.set('logic_type_id', dataItem_stepLogicType.logic_type_id);
        } else {
            e.preventDefault();
            alert('Не задан тип логики');
        };
    };

    // private

    function refreshProgramm() {
        var dataItem = $('#programmDdl').data('kendoDropDownList').dataItem();
        if (!dataItem) {
            return false;
        };
        $('#edProgrammName').val(dataItem.programm_name);
        $('#edDateBeg').data('kendoDatePicker').value(dataItem.date_beg);
        $('#edDateEnd').data('kendoDatePicker').value(dataItem.date_end);
        $('#edDescrShort').val(dataItem.descr_short);
        $('#edDescrFull').val(dataItem.descr_full);
        $('#edCardTypeGroup').data('kendoDropDownList').value(dataItem.card_type_group_id);
        $('#edProgrammId').val(dataItem.programm_id_str);

        if ($('#panel-programm-param').hasClass('k-state-active')) {
            refreshGrid('#programmParamGrid');
        };
        if ($('#panel-programm-step').hasClass('k-state-active')) {
            refreshGrid('#programmStepGrid');
        };
        /*
        if ($('#panel-programm-step-param').hasClass('k-state-active')) {
            refreshGrid('#programmStepParamGrid');
        };
        */
    };

    function toggleProgrammParamType(dataItem) {
        $('.param-type').addClass('hidden');
        if ((dataItem) && (dataItem.param_type_id)) {
            switch (dataItem.param_type_id) {
                case 1:
                    $('.param-type-string').removeClass('hidden');
                    break;
                case 2:
                    $('.param-type-int').removeClass('hidden');
                    break;
                case 3:
                    $('.param-type-float').removeClass('hidden');
                    break;
                case 4:
                    $('.param-type-date').removeClass('hidden');
                    break;
                case 5:
                    $('.param-type-date-only').removeClass('hidden');
                    break;
                case 6:
                    $('.param-type-time-only').removeClass('hidden');
                    break;
                default:
                    break;
            }
        };
    };

    function toggleProgrammStepParamType(dataItem) {
        $('.step-param-type').addClass('hidden');
        if ((dataItem) && (dataItem.param_type_id)) {
            switch (dataItem.param_type_id) {
                case 1:
                    $('.step-param-type-card').removeClass('hidden');
                    break;
                case 2:
                    $('.step-param-type-trans').removeClass('hidden');
                    break;
                case 3:
                    $('.step-param-type-programm').removeClass('hidden');
                    break;
                case 4:
                    $('.step-param-type-prev-step').removeClass('hidden');
                    break;
                default:
                    break;
            }
        };
    };
}

var curr_programm_id = null;
var programmBuilder = null;
$(function () {
    programmBuilder = new ProgrammBuilder();
    programmBuilder.init();
});
