﻿function ClientList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#clientListGrid', 0);

        $(".ed-search").keypress(function (e) {
            if (e.which == 13) {
                clientSearch(e);
                return true;
            }
            else {
                return true;
            };
        });

        $("#btnFilter").click(function (e) {
            clientSearch(e);
        });

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
        });

        $('#btnClientAdd').click(function () {
            window.location = '/ClientAdd';
        });

        $('#btnGridConfig').click(function () {
            var win = $("#winClientGridConfig").data("kendoWindow");
            win.center().open();
            return false;
        });

        $("#btnClientGridConfigOk").click(function (e) {
            e.preventDefault();
            var win = $("#winClientGridConfig").data("kendoWindow");
            var settingsData = getSettingsData();
            $.ajax({
                type: "POST",
                url: "Sys/CabGridColumnUserSettings",
                data: kendo.stringify(settingsData),
                contentType: 'application/json; charset=windows-1251',
                success: function (response) {
                    if (response != null && response.success) {
                        win.close();
                        window.location = url_ClientList;
                    } else {
                        win.close();
                        alert(response);
                    }
                },
                error: function (response) {
                    win.close();
                    alert('error');
                }
            });

            return false;
        });

        $("#btnClientGridConfigCancel").click(function (e) {
            e.preventDefault();
            var win = $("#winClientGridConfig").data("kendoWindow");
            win.close();
            return false;
        });
    };

    this.placeholder = function(element) {
        return element.clone().addClass("k-state-hover").css("opacity", 0.65);
    };

    this.onSortChange = function(e) {
        var grid = $("#clientGridColumnSettingsGrid").data("kendoGrid"),
                    skip = grid.dataSource.skip(),
                    oldIndex = e.oldIndex + skip,
                    newIndex = e.newIndex + skip,
                    data = grid.dataSource.data(),
                    dataItem = grid.dataSource.getByUid(e.item.data("uid"));

        grid.dataSource.remove(dataItem);
        grid.dataSource.insert(newIndex, dataItem);
    };

    this.onGridDataBound = function(e) {
        var grid = $("#clientListGrid").data("kendoGrid");

        var rows = e.sender.tbody.children();

        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if (dataItem.get("is_activated") != true) {
                row.addClass("k-error-colored");
            };
        };
    };

    this.onClientDetailGridDataBound = function(e) {
        var grid_id = $(this.wrapper).attr('id');
        drawEmptyRow('#' + grid_id);
    };

    this.getClientListData = function(e) {
        var curr_service_id = null;
        var ddl = $('#serviceList').getKendoDropDownList();
        if (ddl) {
            curr_service_id = ddl.value();
        }
        return { client_name: $("#edClientName").val(), is_jur: $('#chbJur').is(':checked'), is_phis: $('#chbPh').is(':checked'), wp_key: $("#edWorkplaceKey").val(), reg_key: $("#edRegKey").val(), service_id: curr_service_id };
    };

    this.onRegListGridDataBound = function(e) {
        var ds = $('#regListGrid').getKendoGrid().dataSource;
        if (ds._view.length > 0) {
            $('#reg-list-div').removeClass('hidden');
        } else {
            $('#reg-list-div').addClass('hidden');
        };
    };

    this.onClientGridColumnSettingsGridClick = function(e) {
        var checked = $(e).is(':checked');
        var grid = $('#clientGridColumnSettingsGrid').getKendoGrid();
        var dataItem = grid.dataItem($(e).closest('tr'));
        dataItem.is_visible = checked;
    };

    function clientSearch(e) {
        $("#clientListGrid").getKendoGrid().dataSource.read();
    };

    function getSettingsData() {
        var grid = $("#clientGridColumnSettingsGrid").data("kendoGrid");
        var data = grid.dataSource.view();
        return { settings: data, color_column_id: null };
    };
}

var clientList = null;

var noHint = $.noop;

$(function () {
    clientList = new ClientList();
    clientList.init();
});
