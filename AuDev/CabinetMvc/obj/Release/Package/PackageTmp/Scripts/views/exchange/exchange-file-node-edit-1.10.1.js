﻿function ExchangeFileNodeEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();        

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var curr_partner_id = null;

            curr_partner_id = $('#dsList').getKendoDropDownList().value();
            $('#partner_id').val(curr_partner_id);

            $('#node_item_send_list').val(JSON.stringify($('#nodeItemSendGrid').getKendoGrid().dataSource.view()));
            $('#node_item_get_list').val(JSON.stringify($('#nodeItemGetGrid').getKendoGrid().dataSource.view()));

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".save-changes-confirm,.save-changes-cancel")
                        .click(function () {
                            if ($(this).hasClass("save-changes-confirm")) {
                                kendoWindow.data("kendoWindow").close();                            
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnDelete').attr('disabled', true);
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });

        $("#btnDelete").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Удалить узел ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            kendoWindow.data("kendoWindow")
                .content($("#save-changes-confirmation").html())
                .center().open();

            kendoWindow.find(".save-changes-confirm,.save-changes-cancel")
                    .click(function () {
                        if ($(this).hasClass("save-changes-confirm")) {
                            kendoWindow.data("kendoWindow").close();
                            $('#btnSubmit').attr('disabled', true);
                            $('#btnDelete').attr('disabled', true);

                            var postData = { node_id: model_node_id };
                            $.ajax({
                                type: "POST",
                                url: "ExchangeFileNodeDel",
                                data: JSON.stringify(postData),
                                contentType: 'application/json; charset=windows-1251',
                                success: function (response) {
                                    window.location = "/ExchangeLogList";
                                    kendoWindow.data("kendoWindow").close();
                                    return true;
                                },
                                error: function (response) {
                                    kendoWindow.data("kendoWindow").close();
                                    return true;
                                }
                            });                        
                        
                        } else {
                            kendoWindow.data("kendoWindow").close();
                            return false;
                        };
                    })
                 .end();
        });
    };



    this.getDsId = function(e) {
        //
        var curr_ds_id = -1;
        var ddl = $('#dsList').getKendoDropDownList();
        if (ddl) {
            curr_ds_id = ddl.value();
        };
        return { ds_id: curr_ds_id };
    };

    this.onNodeItemSendGridDataBound = function(e) {
        drawEmptyRow('#nodeItemSendGrid');
    };

    this.onNodeItemGetGridDataBound = function(e) {
        drawEmptyRow('#nodeItemGetGrid');
    };

    this.onDsListChange = function(e) {
        $('#nodeItemSendGrid').getKendoGrid().dataSource.read();
        $('#nodeItemGetGrid').getKendoGrid().dataSource.read();
    };
}

var exchangeFileNodeEdit = null;

$(function () {
    exchangeFileNodeEdit = new ExchangeFileNodeEdit();
    exchangeFileNodeEdit.init();
});
