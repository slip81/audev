﻿function StockTaskList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#stockTaskGrid', 50);
        //
        $('#btnStockTaskAddOk').on('click', function () {
            $('#winStockTaskAdd').data('kendoWindow').close();
            addTask();
        });
        //
        $('#btnStockTaskAddCancel').on('click', function () {
            $('#winStockTaskAdd').data('kendoWindow').close();
        });
    };

    this.onStockClientDdlChange = function (e) {
        $('#stockSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onStockClientDdlDataBound = function (e) {
        $('#stockSalesDdl').data('kendoDropDownList').dataSource.read();
    };

    this.onStockSalesDdlChange = function (e) {
        refreshGrid('#stockTaskGrid');
    };

    this.onStockSalesDdlDataBound = function (e) {        
        $('#stockSalesDdl').data('kendoDropDownList').select(0);
        refreshGrid('#stockTaskGrid');
    };

    this.getStockSalesDdlData = function (e) {
        var curr_client_id = null;
        var dataItem = $('#stockClientDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.client_id)) {
            curr_client_id = dataItem.client_id;
        }
        return { client_id: curr_client_id };
    }

    this.getStockTaskGridData = function (e) {
        var curr_sales_id = null;        
        var dataItem = $('#stockSalesDdl').data('kendoDropDownList').dataItem();
        if ((dataItem) && (dataItem.sales_id)) {
            curr_sales_id = dataItem.sales_id;
        };
        var curr_task_state_id_list = null;
        return { sales_id: curr_sales_id, task_state_id_list: curr_task_state_id_list };
    };

    this.onStockTaskGridDataBound = function (e) {
        drawEmptyRow('#stockTaskGrid');
        //
        var rows = e.sender.tbody.children();
        for (var j = 0; j < rows.length; j++) {
            var row = $(rows[j]);
            var dataItem = e.sender.dataItem(row);
            if (dataItem) {
                switch (dataItem.task_state_id) {
                    case 6:
                    case 8:
                        row.addClass("k-error-colored");
                        break;
                    case 9:
                    case 10:                        
                        row.addClass("k-info-colored");
                        break;
                    default:
                        break;
                };
            };
        };
    };

    this.onBtnStockTaskAddClick = function (e) {
        $('#winStockTaskAdd').data('kendoWindow').center().open();
    };

    this.onBtnTaskCancelClick = function (task_id) {
        if (!confirm('Отменить задание ?')) {
            return;
        }
        //
        $.ajax({
            url: '/StockTaskDel',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ task_id: task_id }),
            success: function (response) {
                if ((!response) || (response.err)) {
                    alert((response && response.err) ? response.mess : 'Ошибка отмены задания');
                } else {
                    //
                };
            },
            error: function (response) {
                alert('Ошибка отмены задания');
            },
            complete: function (response) {
                refreshGrid('#stockTaskGrid');
            }
        });
    }

    // private

    function addTask() {
        var curr_sales_id = null;
        var sales_dataItem = $('#stockSalesDdl').data('kendoDropDownList').dataItem();
        if ((sales_dataItem) && (sales_dataItem.sales_id)) {
            curr_sales_id = sales_dataItem.sales_id;
        };
        if (!curr_sales_id) {
            alert('Не выбрана торговая точка');
            return;
        }
        //
        var curr_request_type_id = null;
        var request_type_dataItem = $('#ddlStockTaskType').data('kendoDropDownList').dataItem();
        if ((request_type_dataItem) && (request_type_dataItem.request_type_id)) {
            curr_request_type_id = request_type_dataItem.request_type_id;
        };
        //
        $.ajax({
            url: '/StockTaskAdd',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ sales_id: curr_sales_id, request_type_id: curr_request_type_id }),
            success: function (response) {
                if ((!response) || (response.err)) {
                    alert((response && response.err) ? response.mess : 'Ошибка создания задания');
                } else {
                    //
                };
            },
            error: function (response) {
                alert('Ошибка создания задания');
            },
            complete: function (response) {                
                refreshGrid('#stockTaskGrid');
            }
        });
    };
}

var stockTaskList = null;
$(function () {
    stockTaskList = new StockTaskList();
    stockTaskList.init();
});
