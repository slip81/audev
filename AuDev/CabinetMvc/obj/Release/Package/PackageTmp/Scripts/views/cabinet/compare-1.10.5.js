﻿function Compare() {
    _this = this;

    var createDraggable = function () {
        $("#clientCrmGrid").kendoDraggable({
            filter: "tr",
            hint: function (element) {
                return element.clone().find('td:first-child').css({
                    "opacity": 0.6
                });
            },
            group: "gridGroup1"
        });
    };

    var createDropTarget = function () {
        $('#clientCabGrid tr').kendoDropTarget({
            drop: function (e) {
                var grid1 = $('#clientCabGrid').getKendoGrid();
                var grid2 = $('#clientCrmGrid').getKendoGrid();
                var dataItem1 = grid1.dataItem(e.dropTarget);
                var dataItem2 = grid2.dataSource.getByUid(e.draggable.currentTarget.data("uid"));
                dataItem1.set("sm_id", dataItem2.id_company);
                dataItem1.set("crm_name_company_full", dataItem2.name_company_full);
                dataItem1.set("row_class", "k-success-colored");
                if (!dataItem1.region_name)
                    dataItem1.set("region_name", dataItem2.name_region);
                if (!dataItem1.city_name)
                    dataItem1.set("city_name", dataItem2.name_city);
                if (!dataItem1.legal_address) {
                    if (dataItem2.p_address) {
                        dataItem1.set("legal_address", dataItem2.p_address);
                    } else {
                        dataItem1.set("legal_address", dataItem2.address);
                    }
                };
                if (!dataItem1.absolute_address) {
                    if (dataItem2.p_address) {
                        dataItem1.set("absolute_address", dataItem2.p_address);
                    } else {
                        dataItem1.set("absolute_address", dataItem2.address);
                    }
                };
                if (!dataItem1.inn)
                    dataItem1.set("inn", dataItem2.inn);
                if (!dataItem1.kpp)
                    dataItem1.set("kpp", dataItem2.kpp);
                if (!dataItem1.phone)
                    dataItem1.set("phone", dataItem2.phone);
                if (!dataItem1.email)
                    dataItem1.set("email", dataItem2.email);
                if (!dataItem1.contact_person)
                    dataItem1.set("contact_person", dataItem2.face_fio);
                if (!dataItem1.state_name)
                    dataItem1.set("state_name", dataItem2.name_state);
                dataItem1.dirty = true;
                var index1 = cabUidsRemoved.indexOf(dataItem1.uid);
                if (index1 >= 0)
                    cabUidsRemoved.splice(index1, 1);
                cabUids.push(dataItem1.uid);
                dataItem2.set("row_class", "k-success-colored");
                var index2 = crmUidsRemoved.indexOf(dataItem2.uid);
                if (index2 >= 0)
                    crmUidsRemoved.splice(index1, 1);
                crmUids.push(dataItem2.uid);
                createDraggable();
                createDropTarget();
                //$("#clientCabGrid tr[data-uid='" + dataItem1.uid + "']").addClass("k-success-colored");
                //$("#clientCrmGrid tr[data-uid='" + e.draggable.currentTarget.data('uid') + "']").addClass("k-success-colored");
                $.each(cabUidsRemoved, function (index, value) {
                    $("#clientCabGrid tr[data-uid='" + value + "']").removeClass("k-success-colored");
                });
                $.each(crmUidsRemoved, function (index, value) {
                    $("#clientCrmGrid tr[data-uid='" + value + "']").removeClass("k-success-colored");
                });
                $.each(cabUids, function (index, value) {
                    $("#clientCabGrid tr[data-uid='" + value + "']").addClass("k-success-colored");
                });
                $.each(crmUids, function (index, value) {
                    $("#clientCrmGrid tr[data-uid='" + value + "']").addClass("k-success-colored");
                });
            },
            group: "gridGroup1",
        });
    };

    this.init = function () {
        //setGridMaxHeight('#clientCabGrid', 250);
        //setGridMaxHeight('#clientCrmGrid', 250);
        //
        $('#btn-unmated').click(function () {
            curr_group = 1;
            $('#btn-unmated').addClass('btn-primary');
            $('#btn-mated').removeClass('btn-primary');
            $('#btn-all').removeClass('btn-primary');
            $('#clientCabGrid').data("kendoGrid").dataSource.read();
            $('#clientCrmGrid').data("kendoGrid").dataSource.read();
        });
        //
        $('#btn-mated').click(function () {
            curr_group = 2;
            $('#btn-unmated').removeClass('btn-primary');
            $('#btn-mated').addClass('btn-primary');
            $('#btn-all').removeClass('btn-primary');
            $('#clientCabGrid').data("kendoGrid").dataSource.read();
            $('#clientCrmGrid').data("kendoGrid").dataSource.read();
        });
        //
        $('#btn-all').click(function () {
            curr_group = 0;
            $('#btn-unmated').removeClass('btn-primary');
            $('#btn-mated').removeClass('btn-primary');
            $('#btn-all').addClass('btn-primary');
            $('#clientCabGrid').data("kendoGrid").dataSource.read();
            $('#clientCrmGrid').data("kendoGrid").dataSource.read();
        });
        //
        $('#btn-save').click(function () {
            if (!confirm("Сохранить изменения ?"))
                return;

            $('#btn-save').addClass('disabled');
            var data = $('#clientCabGrid').data("kendoGrid").dataSource.data();
            var changedRows = $.grep(data, function (item) {
                return item.dirty
            });            
            $.ajax({
                url: '/ClientCabListUpdate',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ items: changedRows }),
                success: function (response) {
                    $('#btn-save').removeClass('disabled');
                    if ((response == null) || (response.err)) {
                        alert((response.err && response.err.mess) ? response.err.mess : 'Ошибка при сохранении');
                    } else {
                        //location.href = location.href;
                    };
                },
                error: function (response) {
                    $('#btn-save').removeClass('disabled');
                    alert('Ошибка при сохранении');
                }
            });
        });
        //
        $(".ed-search").keypress(function (e) {
            if (e.which == 13) {
                clientSearch(e);
                return true;
            }
            else {
                return true;
            };
        });
        //
        $(".cab-client-create").click(function (e) {
            alert('cab-client-create !');
        });
    };

    this.onClientCabGridDataBound = function (e) {
        var grid1 = $('#clientCabGrid').getKendoGrid();
        var grid2 = $('#clientCrmGrid').getKendoGrid();
        //
        if (firstDataBound_Cab) {
            firstDataBound_Cab = false;                        
            //
            grid2.dataSource.read();
        }
        //
        createDropTarget();
        //
        drawEmptyRow('#clientCabGrid');
    };

    this.onClientCrmGridDataBound = function (e) {
        //
        if (firstDataBound_Crm) {
            firstDataBound_Crm = false;
        }
        //
        createDraggable();
        //
        drawEmptyRow('#clientCrmGrid');
    };

    this.getAdditionalData = function () {
        return { group_id: curr_group };
    };

    this.onUnmateBtnClick = function (client_id) {
        var dataItem = $("#clientCabGrid").data("kendoGrid").dataSource.get(client_id);
        var dataItem2 = null;
        if (dataItem.sm_id) {
            dataItem2 = $("#clientCrmGrid").data("kendoGrid").dataSource.get(dataItem.sm_id);            
            dataItem2.set("row_class", "");
            crmUidsRemoved.push(dataItem2.uid);
            var index1 = crmUids.indexOf(dataItem2.uid);
            if (index1 >= 0)
                crmUids.splice(index1, 1);
            //$("#clientCrmGrid tr[data-uid='" + dataItem2.uid + "']").removeClass("k-success-colored");
        };
        dataItem.set("sm_id", null);
        dataItem.set("crm_name_company_full", null);
        dataItem.set("row_class", "");        
        dataItem.dirty = true;
        cabUidsRemoved.push(dataItem.uid);
        var index2 = cabUids.indexOf(dataItem.uid);
        if (index2 >= 0)
            cabUids.splice(index2, 1);
        //$("#clientCabGrid tr[data-uid='" + dataItem.uid + "']").removeClass("k-success-colored");
        $.each(cabUids, function (index, value) {
            $("#clientCabGrid tr[data-uid='" + value + "']").addClass("k-success-colored");
        });
        $.each(crmUids, function (index, value) {
            $("#clientCrmGrid tr[data-uid='" + value + "']").addClass("k-success-colored");
        });
        $.each(cabUidsRemoved, function (index, value) {
            $("#clientCabGrid tr[data-uid='" + value + "']").removeClass("k-success-colored");
        });
        $.each(crmUidsRemoved, function (index, value) {
            $("#clientCrmGrid tr[data-uid='" + value + "']").removeClass("k-success-colored");
        });
    };

    this.onCreateBtnClick = function (id_company) {
        //alert('onCreateBtnClick: ' + id_company);
        var dataItem = $("#clientCrmGrid").data("kendoGrid").dataSource.get(id_company);
        if ((!dataItem) || (dataItem.row_class == "k-success-colored"))
            return;
        var grid1 = $("#clientCabGrid").data("kendoGrid");
        var dataItem_new = grid1.dataSource.insert(0, {
            row_class: "k-success-colored",
            UnmateBtn: "<button class='k-button k-button-icontext' title='Убрать стыковку' onclick='compare.onUnmateBtnClick(-1)'><span class='k-icon k-delete'></span></button>",
            full_name: dataItem.name_company_full,
            region_name: dataItem.name_region,
            city_name: dataItem.name_city,
            legal_address: dataItem.p_address ? dataItem.p_address : dataItem.address,
            absolute_address: dataItem.p_address ? dataItem.p_address : dataItem.address,
            inn: dataItem.inn,
            kpp: dataItem.kpp,
            phone: dataItem.phone,
            email: dataItem.email,
            contact_person: dataItem.face_fio,
            id: -1,
            sm_id: dataItem.id_company,
            crm_name_company_full: dataItem.name_company_full,
            state_name: dataItem.name_state,
            dirty: true,
            uid: kendo.guid()
        });

        var index1 = cabUidsRemoved.indexOf(dataItem_new.uid);
        if (index1 >= 0)
            cabUidsRemoved.splice(index1, 1);
        cabUids.push(dataItem_new.uid);
        var index2 = crmUidsRemoved.indexOf(dataItem.uid);
        if (index2 >= 0)
            crmUidsRemoved.splice(index1, 1);
        crmUids.push(dataItem.uid);

        dataItem.set("row_class", "k-success-colored");

        $.each(cabUidsRemoved, function (index, value) {
            $("#clientCabGrid tr[data-uid='" + value + "']").removeClass("k-success-colored");
        });
        $.each(crmUidsRemoved, function (index, value) {
            $("#clientCrmGrid tr[data-uid='" + value + "']").removeClass("k-success-colored");
        });
        $.each(cabUids, function (index, value) {
            $("#clientCabGrid tr[data-uid='" + value + "']").addClass("k-success-colored");
        });
        $.each(crmUids, function (index, value) {
            $("#clientCrmGrid tr[data-uid='" + value + "']").addClass("k-success-colored");
        });
    }

    this.onDdlStateChange = function (e) {
        clientSearch(e);
    };

    // private

    function clientSearch(e) {
        var grid1 = $("#clientCabGrid").data("kendoGrid");
        var grid2 = $("#clientCrmGrid").data("kendoGrid");

        var gridFilter1 = { logic: "and", filters: [] };
        var gridFilter2 = { logic: "and", filters: [] };

        var currOrFilter1 = { logic: "or", filters: [] };
        var currOrFilter2 = { logic: "or", filters: [] };

        var curr_val = "";
        
        curr_val = $('#txtName').val();
        if (curr_val) {
            gridFilter1.filters.push({ field: "full_name", operator: "contains", value: curr_val });
            gridFilter2.filters.push({ field: "name_company_full", operator: "contains", value: curr_val });
        };

        curr_val = $('#txtCity').val();
        if (curr_val) {
            gridFilter1.filters.push({ field: "city_name", operator: "contains", value: curr_val });
            gridFilter2.filters.push({ field: "name_city", operator: "contains", value: curr_val });
        };

        curr_val = $('#txtInn').val();
        if (curr_val) {
            gridFilter1.filters.push({ field: "inn", operator: "eq", value: curr_val });
            gridFilter2.filters.push({ field: "inn", operator: "eq", value: curr_val });
        };

        curr_val = $('#txtEmail').val();
        if (curr_val) {
            gridFilter1.filters.push({ field: "email", operator: "eq", value: curr_val });
            gridFilter2.filters.push({ field: "email", operator: "eq", value: curr_val });
        };

        curr_val = $('#ddlState').getKendoMultiSelect().value();
        if ((curr_val) && (curr_val.length > 0)) {
            currOrFilter1 = { logic: "or", filters: [] };
            currOrFilter2 = { logic: "or", filters: [] };
            $.each(curr_val, function (i, v) {
                currOrFilter1.filters.push({ field: "state_id", operator: "eq", value: v });
                currOrFilter2.filters.push({ field: "id_state", operator: "eq", value: v });
            });

            gridFilter1.filters.push(currOrFilter1);
            gridFilter2.filters.push(currOrFilter2);
        };

        grid1.dataSource.filter(gridFilter1);
        grid2.dataSource.filter(gridFilter2);
    };
}

var compare = null;
var firstDataBound_Cab = true;
var firstDataBound_Crm = true;
var curr_group = 1;
var cabUids = [];
var cabUidsRemoved = [];
var crmUids = [];
var crmUidsRemoved = [];

$(function () {
    compare = new Compare();
    compare.init();
});
