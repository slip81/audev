﻿function DefectRequestList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#defectRequestListGrid', 0);
    };
}

var defectRequestList = null;

$(function () {
    defectRequestList = new DefectRequestList();
    defectRequestList.init();
});
