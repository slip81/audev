﻿function PrjNotebookPartial() {
    _this = this;

    this.init = function () {
        $(document).on('click', '#btnWinPrjGroupAddOk', function () {
            $('#winPrjGroupAdd').data('kendoWindow').close();
            var curr_exec_user_id = null;
            var curr_exec_user_id_dataItem = $('#prjNotebookExecUserFilter').data('kendoDropDownList').dataItem();
            if (curr_exec_user_id_dataItem) {
                curr_exec_user_id = curr_exec_user_id_dataItem.user_id;
            };
            var curr_new_group_name = $('#txtNewGroup').val();            
            $.ajax({
                url: 'PrjUserGroupAdd',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ exec_user_id: curr_exec_user_id, new_group_name: curr_new_group_name }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response != null && response.mess ? response.mess : 'Ошибка при сохранении группы');
                    } else {                        
                        var curr_new_item_html = response.new_item_html;
                        if (curr_new_item_html) {
                            $('.prj-notebook-panel').append(curr_new_item_html);
                        };
                    };
                },
                error: function (response) {
                    alert('Ошибка при попытке сохранения группы');
                }
            });
        });
        //
        $(document).on('click', '#btnWinPrjGroupAddCancel', function () {
            $('#winPrjGroupAdd').data('kendoWindow').close();
        });
        //
        $(document).on('click', '#btnWinPrjUserGroupItemOk', function () {
            $('#winPrjUserGroupItem').data('kendoWindow').close();
            var curr_exec_user_id = null;
            var curr_exec_user_id_dataItem = $('#prjNotebookExecUserFilter').data('kendoDropDownList').dataItem();
            if (curr_exec_user_id_dataItem) {
                curr_exec_user_id = curr_exec_user_id_dataItem.user_id;
            };
            var curr_group_id = $('#winPrjUserGroupItem').attr('data-group-id');
            var curr_note_id = $('#winPrjUserGroupItem').attr('data-note-id');
            var curr_note_text = $('#txtPrjNoteText').val();
            $.ajax({
                url: 'PrjNoteEdit',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ exec_user_id: curr_exec_user_id, group_id: curr_group_id, note_id: curr_note_id, note_text: curr_note_text }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response != null && response.mess ? response.mess : 'Ошибка при сохранении записи');
                    } else {
                        $('#prjGroupPanelList_' + curr_group_id).data('kendoListView').dataSource.read();
                    };
                },
                error: function (response) {
                    alert('Ошибка при попытке сохранения записи');
                }
            });
        });
        //
        $(document).on('click', '#btnWinPrjUserGroupItemCancel', function () {
            $('#winPrjUserGroupItem').data('kendoWindow').close();
        });
        //
        $(document).on('click', '#btnPrjNotebookTaskActionOk', function () {
            $('#winPrjNotebookTaskAction').data('kendoWindow').close();
            proceedTaskChildAction();
        });
        //
        $(document).on('click', '#btnPrjNotebookTaskActionCancel', function () {
            $('#winPrjNotebookTaskAction').data('kendoWindow').close();
        });
    }

    this.init_after = function () {
        if (prjNotebook_inited) {
            return;
        }
        //
        $(".prj-group-content-container").delegate(".prj-group-content", "dblclick", function () {            
            var claim_id_html = $(this).find('.prj-group-content-claim-id').first();
            var claim_id = claim_id_html ? claim_id_html.text() : null;
            var task_id_html = $(this).find('.prj-group-content-task-id').first();
            var task_id = task_id_html ? task_id_html.text() : null;
            //
            if (claim_id) {
                loadingDialog('#prj-loading', true);
                setTimeout(function () {
                    rightPaneContentType = 2;
                    leftPaneContentType = 9;
                    leftPaneContentType_main = null;
                    leftPaneContentType_child = null;
                    prjList.openPrjClaimEditPartial(claim_id, null, null, task_id);
                }, waitTimeout);
            };
        });
        //
        prjNotebook_inited = true;
    };

    this.onBtnPrjNotebookGroupAddClick = function(e) {
        $('#winPrjGroupAdd').data('kendoWindow').center().open();
    };

    this.onBtnPrjNotebookGroupRefreshClick = function (e) {
        refreshGroups();
    };

    this.onBtnPrjGroupNoteClick = function (e) {
        var config_action_type = parseInt(e.target.attr('data-config-action-type'), 10);
        var curr_group_id = e.target.attr('data-group-id');
        switch (config_action_type) {
            case 1:
                prjNoteAdd(curr_group_id);
                break;
            case 2:
                var list = $('#prjGroupPanelList_' + curr_group_id).data('kendoListView');                
                var dataItem = list.dataItem(list.select());
                var curr_note_id = dataItem && dataItem.note_id ? dataItem.note_id : null;
                var curr_note_text = dataItem && dataItem.note_id ? dataItem.item_text : null;
                prjNoteEdit(curr_group_id, curr_note_id, curr_note_text);
                break;
            case 3:
                var list = $('#prjGroupPanelList_' + curr_group_id).data('kendoListView');
                var dataItem = list.dataItem(list.select());
                var curr_note_id = dataItem && dataItem.note_id ? dataItem.note_id : null;
                prjNoteDel(curr_group_id, curr_note_id);
                break;
            default:                
                break;
        }
    };

    this.onBtnPrjGroupPanelDelClick = function (e) {
        showConfirmWindow('#confirm-templ', 'Удалить группу ?', function () {
            var curr_exec_user_id = null;
            var curr_exec_user_id_dataItem = $('#prjNotebookExecUserFilter').data('kendoDropDownList').dataItem();
            if (curr_exec_user_id_dataItem) {
                curr_exec_user_id = curr_exec_user_id_dataItem.user_id;
            };
            var curr_group_id = $(e.target).attr('data-group-id');
            $.ajax({
                url: 'PrjUserGroupDel',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ exec_user_id: curr_exec_user_id, group_id: curr_group_id }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response != null && response.mess ? response.mess : 'Ошибка при удалении группы');
                    } else {
                        $('#prj-user-group-panel-' + curr_group_id).remove();
                    };
                },
                error: function (response) {
                    alert('Ошибка при попытке удаления группы');
                }
            });
        });
    };

    this.onPrjGroupPanelListDataBound = function (e) {        
        var canDrag = true;
        if (canDrag) {
            this.wrapper.kendoDraggable({
                filter: ".prj-group-content",
                hint: function (element) {
                    //return element.clone().css({
                    return element.clone().find('strong').first().css({
                        "opacity": 0.6
                    });
                }
            });
        };
        //
        $('.prj-group-content-container, .prj-group-content-container>.k-listview>.prj-group-content').kendoDropTarget({
            drop: onPrjGroupPanelListDrop
        });
    };

    this.onPrjNotebookExecUserFilterChange = function (e) {
        //alert('user changed !');

        var curr_exec_user_id = null;
        var curr_exec_user_id_dataItem = $('#prjNotebookExecUserFilter').data('kendoDropDownList').dataItem();
        if (curr_exec_user_id_dataItem) {
            curr_exec_user_id = curr_exec_user_id_dataItem.user_id;
        };
        if (!curr_exec_user_id)
            return;

        prjNotebook_inited = false;

        var item = $('#tsPrj li.k-state-active');
        var link = item.find('a.k-link').first();
        link.data('contentUrl', 'PrjNotebookPartial?user_id=' + curr_exec_user_id);
        var tabStrip = $("#tsPrj").data('kendoTabStrip');
        var content = $('#tsPrj .k-content.k-state-active').first();
        content.html('');
        setTimeout(function () {
            tabStrip.reload(item);
        }, 300);
    }

    this.getSettings = function () {
        var settings = [];        
        settings.push({ id: 'exec_user', val: $("#prjNotebookExecUserFilter").data("kendoDropDownList").value() });
        //
        var par1 = [];
        var par2 = [];
        $('.prj-notebook-panel>.prj-widget').each(function (ind) {
            par1.push({ id: $(this).attr('id'), val: $(this).height() });
            par2.push({ id: $(this).attr('id'), val: $(this).width() });
        })
        settings.push({ id: 'prj_widget_height', val: JSON.stringify(par1) });
        settings.push({ id: 'prj_widget_width', val: JSON.stringify(par2) });
        // !!!
        $('#prjNotebookToolbar>.k-button-group>.k-toggle-button').each(function (ind) {                        
            settings.push({ id: $(this).attr('data-setting-name'), val: $(this).hasClass('k-state-active') ? true : null });
        })
        return settings;
    };

    this.getPrjGroupPanelListData = function (group_id) {
        return function (e) {
            var curr_period_id = null;
            var ddl = $('#prjGroupPanelPeriodTypeDdl_' + group_id).data('kendoDropDownList');
            if (ddl) {
                curr_period_id = ddl.value();
            };
            return { period_id: curr_period_id};
        };        
    };

    this.onPrjGroupPanelPeriodDdlChange = function (group_id) {
        return function (e) {            
            $('#prjGroupPanelList_' + group_id).data('kendoListView').dataSource.read();
        };
    };

    this.onBtnPrjGroupPanelChildActionsClick = function (group_id) {
        return function (e) {
            prjNotebook_currGroupId = group_id;
            $('#div-prj-notebook-user-select-comment').hide();
            $('#div-prj-notebook-user-select').hide();
            $('#div-prj-notebook-user-select-work-type').hide();
            $('#txtPrjNotebookUserSelectComment').val('');
            currTaskChildActionId = parseInt(e.target.attr('data-config-action-type'), 10);
            switch (currTaskChildActionId) {
                case 2:
                    $('#div-prj-notebook-user-select').show();
                    $('#div-prj-notebook-user-select-work-type').show();
                    $('#winPrjNotebookTaskAction').data('kendoWindow').center().open();
                    break;
                case 3:
                    proceedTaskChildAction();
                    break;
                case 4:
                case 5:
                    $('#div-prj-notebook-user-select').show();
                    $('#div-prj-notebook-user-select-work-type').show();
                    $('#div-prj-notebook-user-select-comment').show();
                    $('#winPrjNotebookTaskAction').data('kendoWindow').center().open();
                    break;
                case 6:
                case 7:
                    $('#div-prj-notebook-user-select-comment').show();
                    $('#winPrjNotebookTaskAction').data('kendoWindow').center().open();
                    break;
                default:
                    break;
            }
        };
    };

    this.onWinPrjNotebookTaskActionActivate = function (e) {
        //
    };

    this.onDdlPrjNotebookUserSelectSelect = function (e) {
        var curr_work_type_id = 1;
        var curr_role_id = e.sender.dataItem(e.item).crm_user_role_id;
        switch (curr_role_id) {
            case 1:
                curr_work_type_id = 3;
                break;
            case 2:
            case 3:
                curr_work_type_id = 4;
                break;
            default:
                curr_work_type_id = 1;
                break;
        };
        $('#ddlPrjNotebookUserSelectWorkType').data('kendoDropDownList').value(curr_work_type_id);
    };

    this.onPrjNotebookToolbarToggle = function (e) {                
        var curr_fixed_kind_arr = [];
        $('#prjNotebookToolbar>.k-button-group>.k-toggle-button').each(function (ind) {            
            var is_visible = $(this).hasClass('k-state-active');
            var curr_widget = $('.prj-group-content-container[data-fixed-kind=' + $(this).attr('data-fixed-kind') + ']').closest('.prj-widget');
            if (is_visible) {
                curr_widget.show();
            } else {
                curr_widget.hide();
            };
        });
    };

    // private

    function prjNoteAdd(group_id) {
        $('#winPrjUserGroupItem').attr('data-group-id', group_id);
        $('#winPrjUserGroupItem').attr('data-note-id', 0);
        $('#txtPrjNoteText').val('');
        $('#winPrjUserGroupItem').data('kendoWindow').center().open();
    };

    function prjNoteEdit(group_id, note_id, note_text) {
        if (!note_id) {
            alert('Не выбрана запись');
            return;
        }
        $('#winPrjUserGroupItem').attr('data-group-id', group_id);
        $('#winPrjUserGroupItem').attr('data-note-id', note_id);
        $('#txtPrjNoteText').val(note_text);
        $('#winPrjUserGroupItem').data('kendoWindow').center().open();        
    };

    function prjNoteDel(group_id, note_id) {
        showConfirmWindow('#confirm-templ', 'Удалить запись ?', function () {
            var curr_exec_user_id = null;
            var curr_exec_user_id_dataItem = $('#prjNotebookExecUserFilter').data('kendoDropDownList').dataItem();
            if (curr_exec_user_id_dataItem) {
                curr_exec_user_id = curr_exec_user_id_dataItem.user_id;
            };
            $.ajax({
                url: 'PrjNoteDel',
                type: "POST",
                contentType: 'application/json; charset=windows-1251',
                data: JSON.stringify({ exec_user_id: curr_exec_user_id, group_id: group_id, note_id: note_id }),
                success: function (response) {
                    if ((response == null) || (response.err)) {
                        alert(response != null && response.mess ? response.mess : 'Ошибка при удалении записи');
                    } else {
                        $('#prjGroupPanelList_' + group_id).data('kendoListView').dataSource.read();
                    };
                },
                error: function (response) {
                    alert('Ошибка при попытке удаления записи');
                }
            });
        });
    };

    function onPrjGroupPanelListDrop(e) {
        try {
            prjGroupPanelListDrop(e);
        }
        catch (ex) {
            e.preventDefault();
            return;
        };                
    };

    function prjGroupPanelListDrop(e) {
        var is_target_content = e.dropTarget.is('.prj-group-content');

        var source_item_id = null;
        var target_item_id = null;
        var source_group_id = null;
        var target_group_id = null;
        var source_fixed_kind = null;
        var target_fixed_kind = null;

        var source_item_id_span = $(e.target).find('span.prj-group-content-id').first();
        source_item_id = source_item_id_span.text();
        var source_group_id_span = $(e.target).find('span.prj-group-content-group-id').first();
        source_group_id = source_group_id_span.text();
        var source_fixed_kind_span = $(e.target).find('span.prj-group-content-fixed-kind').first();
        source_fixed_kind = source_fixed_kind_span.text();

        if (is_target_content) {
            var target_div = e.dropTarget.closest('div').first();
            var target_group_id_span = $(target_div).find('span.prj-group-content-group-id').first();
            var target_item_id_span = $(target_div).find('span.prj-group-content-id').first();
            target_item_id = is_target_content ? target_item_id_span.text() : null;
            target_group_id = target_group_id_span.text();
            var target_fixed_kind_span = $(target_div).find('span.prj-group-content-fixed-kind').first();
            target_fixed_kind = target_fixed_kind_span.text();
        } else {
            target_group_id = e.dropTarget.attr('data-group-id');
            target_fixed_kind = e.dropTarget.attr('data-fixed-kind');
        };

        //alert('source_item_id = ' + source_item_id + ', source_group_id = ' + source_group_id + ', target_item_id = ' + target_item_id + ', target_group_id = ' + target_group_id);

        var source_ds = null;
        var target_ds = null;
        var source_item = null;
        var target_item = null;
        var source_item_index = null;
        var target_item_index = null;
        var ord = null;
        var ord_prev = null;

        if ((is_target_content) && (source_item_id == target_item_id) && (source_group_id == target_group_id)) {
            return;
        };

        if ((source_group_id != target_group_id) && (source_fixed_kind > 0)) {
            e.preventDefault();
            alert('Нельзя убирать записи из фиксированной группы');
            return;
        };

        if ((source_group_id != target_group_id) && (target_fixed_kind > 0)) {
            e.preventDefault();
            alert('Нельзя добавлять записи в фиксированную группу');
            return;
        };

        if ((is_target_content) && (source_item_id != target_item_id) && (source_group_id == target_group_id)) {
            source_ds = $('#prjGroupPanelList_' + source_group_id).data('kendoListView').dataSource;
            source_item = source_ds.get(source_item_id)
            source_item_index = source_ds.indexOf(source_item);
            target_item = source_ds.get(target_item_id)
            target_item_index = source_ds.indexOf(target_item);
            source_ds.remove(source_item);            
            ord_prev = source_item_index + 1;
            source_item.set('ord_prev', source_item.ord);
            source_item.set('ord', target_item_index + 1);
            source_ds.insert(target_item_index, source_item);
            ord = source_item.ord;
            //
            prjNoteReorder(source_item_id, source_group_id, target_group_id, ord, ord_prev);
            //
            return;
        };
        if ((is_target_content) && (source_item_id != target_item_id) && (source_group_id != target_group_id)) {
            source_ds = $('#prjGroupPanelList_' + source_group_id).data('kendoListView').dataSource;
            source_item = source_ds.get(source_item_id)
            source_item_index = source_ds.indexOf(source_item);
            target_ds = $('#prjGroupPanelList_' + target_group_id).data('kendoListView').dataSource;
            target_item = target_ds.get(target_item_id)
            target_item_index = target_ds.indexOf(target_item);
            source_ds.remove(source_item);            
            ord_prev = source_item_index + 1;
            source_item.set('group_id', target_group_id);
            source_item.set('ord_prev', source_item.ord);
            source_item.set('ord', target_item_index + 1);
            target_ds.insert(target_item_index, source_item);
            ord = source_item.ord;
            //
            prjNoteReorder(source_item_id, source_group_id, target_group_id, ord, ord_prev);
            //
            return;
        };
        if ((!is_target_content) && (source_group_id == target_group_id)) {
            source_ds = $('#prjGroupPanelList_' + source_group_id).data('kendoListView').dataSource;
            source_item = source_ds.get(source_item_id)
            source_item_index = source_ds.indexOf(source_item);
            target_item_index = source_ds.data().length;
            source_ds.remove(source_item);            
            ord_prev = source_item_index + 1;
            source_item.set('ord_prev', source_item.ord);
            source_item.set('ord', target_item_index + 1);
            source_ds.insert(target_item_index, source_item);
            ord = source_item.ord;
            //
            prjNoteReorder(source_item_id, source_group_id, target_group_id, ord, ord_prev);
            //
            return;
        };
        if ((!is_target_content) && (source_group_id != target_group_id)) {
            source_ds = $('#prjGroupPanelList_' + source_group_id).data('kendoListView').dataSource;
            source_item = source_ds.get(source_item_id)
            source_item_index = source_ds.indexOf(source_item);
            target_ds = $('#prjGroupPanelList_' + target_group_id).data('kendoListView').dataSource;
            target_item_index = target_ds.data().length;
            source_ds.remove(source_item);            
            ord_prev = source_item_index + 1;
            source_item.set('group_id', target_group_id);
            source_item.set('ord_prev', source_item.ord);
            source_item.set('ord', target_item_index + 1);
            target_ds.insert(target_item_index, source_item);
            ord = source_item.ord;
            //
            prjNoteReorder(source_item_id, source_group_id, target_group_id, ord, ord_prev);
            //
            return;
        };
    }

    function prjNoteReorder(source_item_id, source_group_id, target_group_id, ord, ord_prev) {
        $.ajax({
            url: 'PrjNoteReorder',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ source_item_id: source_item_id, source_group_id: source_group_id, target_group_id: target_group_id, ord: ord, ord_prev: ord_prev })
        });
    };

    function proceedTaskChildAction() {
        // !!!
        var list = $('#prjGroupPanelList_' + prjNotebook_currGroupId).data('kendoListView');
        var curr_task_dataItem = list.dataItem(list.select());
        var curr_task_id = curr_task_dataItem ? curr_task_dataItem.task_id : null;
        if (!curr_task_id) {
            alert('Не выбрано задание');
            return;
        }
        var curr_user_dataItem = $('#ddlPrjNotebookUserSelect').data('kendoDropDownList').dataItem();
        var curr_user_id = curr_user_dataItem ? curr_user_dataItem.user_id : null;
        var curr_work_type_dataItem = $('#ddlPrjNotebookUserSelectWorkType').data('kendoDropDownList').dataItem();
        var curr_work_type_id = curr_work_type_dataItem ? curr_work_type_dataItem.work_type_id : null;
        var curr_comment = $('#txtPrjNotebookUserSelectComment').val();
        //        
        switch (currTaskChildActionId) {
            case 2: // передать в работу                
                if (curr_user_id <= 0) {
                    alert('Не выбран пользователь');
                    return;
                };
                break;
            case 4: // выполнить и передать далее
            case 5: // выполнить и вернуть
                if (curr_user_id <= 0) {
                    alert('Не выбран пользователь');
                    return;
                };
                break;
            default:
                break;
        }
        //
        loadingDialog('#prj-loading', true);
        //
        $.ajax({
            url: '/PrjTaskUserAction',
            type: "POST",
            contentType: 'application/json; charset=windows-1251',
            data: JSON.stringify({ task_id: curr_task_id, action_id: currTaskChildActionId, user_id: curr_user_id, work_type_id: curr_work_type_id, comment: curr_comment }),
            success: function (response) {
                if ((!response) || (response.err)) {
                    alert((response && response.err) ? response.mess : 'Ошибка выполнения действия');
                } else {
                    //
                };
            },
            error: function (response) {
                alert('Ошибка выполнения действия');
            },
            complete: function (response) {
                loadingDialog('#prj-loading', false);
                refreshGroups();
            }
        });
    };

    function refreshGroups() {
        $('.prj-group-content-container').each(function () {
            $('#prjGroupPanelList_' + $(this).attr('data-group-id')).data('kendoListView').dataSource.read();
        });
    };
}

var prjNotebookPartial = null;
var prjNotebook_inited = false;
var prjNotebook_currGroupId = null;

$(function () {
    prjNotebookPartial = new PrjNotebookPartial();
    prjNotebookPartial.init();
});
