﻿function ServiceSender() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();
        //        
        if ($('#rb_interval_in_days_').is(':checked')) {
            $('#rb_interval_in_days_').trigger('click');
        };

        if ($('#rb_schedule_weekly_').is(':checked')) {
            var curr_weekly_values = $('#schedule_weekly').val().split(',');
            if (curr_weekly_values) {
                for (var i = 0; i <= 6; i++) {
                    if (curr_weekly_values[i] == 1) {
                        $('#chb_schedule_weekly_' + i).prop('checked', 'checked');
                    };
                };
            };
            $('#rb_schedule_weekly_').trigger('click');
        };

        if ($('#rb_schedule_monthly_').is(':checked')) {
            var curr_monthly_values = $('#schedule_monthly').val().split(',');
            if (curr_monthly_values) {
                for (var i = 0; i <= 30; i++) {
                    if (curr_monthly_values[i] == 1) {
                        $('#chb_schedule_monthly_' + i).prop('checked', 'checked');
                    };
                };
            };
            $('#rb_schedule_monthly_').trigger('click');
        };

        $('#rb_interval_in_days_').click(function (e) {
            $('#div-daily').find('*').removeClass('disabled k-state-disabled');
            $('#div-weekly').find('*').addClass('disabled k-state-disabled');
            $('#div-monthly').find('*').addClass('disabled k-state-disabled');
        });

        $('#rb_schedule_weekly_').click(function (e) {
            $('#div-weekly').find('*').removeClass('disabled k-state-disabled');
            $('#div-daily').find('*').addClass('disabled k-state-disabled');
            $('#div-monthly').find('*').addClass('disabled k-state-disabled');
        });

        $('#rb_schedule_monthly_').click(function (e) {
            $('#div-monthly').find('*').removeClass('disabled k-state-disabled');
            $('#div-daily').find('*').addClass('disabled k-state-disabled');
            $('#div-weekly').find('*').addClass('disabled k-state-disabled');
        });

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Сохранить изменения ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            var validator = $("form").getKendoValidator();

            var curr_schedule_type = 1;
            var curr_values = [];
            if ($('#rb_schedule_weekly_').is(':checked')) {
                for (var i = 0; i <= 6; i++) {
                    var curr_val = $('#chb_schedule_weekly_' + i).is(':checked') ? 1 : 0;
                    curr_values.push(curr_val);
                };
                $('#schedule_weekly').val(curr_values);
                curr_schedule_type = 2;
            } else if ($('#rb_schedule_monthly_').is(':checked')) {
                for (var i = 0; i <= 30; i++) {
                    var curr_val = $('#chb_schedule_monthly_' + i).is(':checked') ? 1 : 0;
                    curr_values.push(curr_val);
                };
                $('#schedule_monthly').val(curr_values);
                curr_schedule_type = 3;
            };

            $('#schedule_type').val(curr_schedule_type);

            if (validator.validate()) {

                kendoWindow.data("kendoWindow")
                    .content($("#save-changes-confirmation").html())
                    .center().open();

                kendoWindow.find(".confirm,.cancel")
                        .click(function () {
                            if ($(this).hasClass("confirm")) {
                                kendoWindow.data("kendoWindow").close();
                                $('#btnSubmit').removeClass('k-state-active');
                                $('#btnSubmit').attr('disabled', true);
                                $('#btnRevert').removeClass('btn-revert-visible');
                                $('#btnRevert').removeClass('k-state-focused');
                                $('#btnRevert').addClass('btn-revert-hidden');
                                $("#btnSubmitProceed").trigger('click');
                                return true;
                            } else {
                                kendoWindow.data("kendoWindow").close();
                                return false;
                            };
                        })
                     .end()
            } else { return false; };
        });
    };

    this.onWorkplaceServiceLogListDataBound = function(e) {
        drawEmptyRow("#workplaceServiceLogList");
    };
}

var serviceSender = null;

$(function () {
    serviceSender = new ServiceSender();
    serviceSender.init();
});
