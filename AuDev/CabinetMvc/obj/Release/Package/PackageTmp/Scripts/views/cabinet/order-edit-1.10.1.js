﻿function OrderEdit() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();
        
        var is_conformed = 1;
        var active_status = model_active_status;
        if ((is_conformed != 1) || (active_status != 1)) {
            $('#btnSubmit').attr('disabled', true);
        };

        $("#btnSubmit").click(function (e) {
            e.preventDefault();
            var kendoWindow = $("<div />").kendoWindow({
                actions: [],
                title: "Активировать заявку ?",
                resizable: false,
                modal: true,
                width: "400px",
                height: "80px"
            });

            kendoWindow.data("kendoWindow")
                .content($("#op-confirmation").html())
                .center().open();

            kendoWindow.find(".confirm,.cancel")
                    .click(function () {
                        if ($(this).hasClass("confirm")) {
                            kendoWindow.data("kendoWindow").close();
                            var id = { order_id: model_id };
                            var view = $('#orderItemListGrid').getKendoGrid().dataSource.view();
                            var order_data = kendo.stringify({ order_data: $("#orderItemListGrid").data("kendoGrid").dataSource.view(), commentary: $('#commentary').val(), commentary_manager: $('#commentary_manager').val() });
                            $.ajax({
                                type: "POST",
                                url: "Cabinet/OrderActivate",
                                data: order_data,
                                contentType: 'application/json; charset=windows-1251',
                                success: function (response) {
                                    location = '/ClientEdit?client_id=' + model_client_id + '&active_page=1';
                                },
                                error: function (response) {
                                    kendoWindow.data("kendoWindow").close();
                                }
                            });
                            return true;
                        } else {
                            kendoWindow.data("kendoWindow").close();
                            return false;
                        };
                    })
                 .end();
        });
    };
    
    this.getParentId = function(e) {
        var model = getCurrentEditedModel();
        return {
            max_cnt: model.quantity
        };
    };

    this.setSelectedValue = function(e) {
        var model = getCurrentEditedModel();
        var ddl = $("#ddlQuantity").getKendoDropDownList();
        var ddl_val = ddl.value();
        var ddl_text = ddl.text();
        model.set("QuantityForActivationVM.QuantityId", ddl_val);
        model.set("QuantityForActivationVM.QuantityName", ddl_text);      
    };

    function getCurrentEditedModel() {
        var grid = $("#orderItemListGrid").data("kendoGrid");
        var editRow = grid.tbody.find("tr:has(.k-edit-cell)");
        return grid.dataItem(editRow);
    };
}

var orderEdit = null;

$(function () {
    orderEdit = new OrderEdit();
    orderEdit.init();
});
