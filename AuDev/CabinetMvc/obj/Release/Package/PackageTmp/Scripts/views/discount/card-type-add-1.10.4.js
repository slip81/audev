﻿function CardTypeAdd() {
    _this = this;

    this.init = function () {
        $("form").kendoValidator();

        $("#btnSubmit").click(function (e) {
            var validator = $("form").getKendoValidator();

            var combo1 = $("#cardTypeGroupList").data("kendoDropDownList");
            $("#card_type_group_id").val(combo1.dataItem()["card_type_group_id"]);
            var combo2 = $("#programmList").data("kendoDropDownList");
            $("#programm_id").val(combo2.dataItem()["programm_id_str"]);

            var chb1 = ($('#chb_reset_interval_bonus_value').is(':checked') && ((combo1.dataItem()["card_type_group_id"] == 2) || (combo1.dataItem()["card_type_group_id"] == 3)));
            $('#reset_interval_bonus_value').val(chb1 ? $('#reset_interval_bonus_value').val() : null);

            var chb2 = ($('#chb_is_reset_without_sales').is(':checked') && ((combo1.dataItem()["card_type_group_id"] == 2) || (combo1.dataItem()["card_type_group_id"] == 3)));
            $('#is_reset_without_sales').val(chb2 ? 1 : 0);

            if (validator.validate()) {
                $("#btnSubmit").attr('disabled', true);
                $("#btnSubmitProceed").trigger('click');
            };
        });
    };

    this.onProgrammChange = function(e) {
        var combo2 = $("#programmList").data("kendoDropDownList");
        $("#edProgrammDescr").val(combo2.dataItem()["programm_descr"]);
    };

    this.onCardTypeGroupListDataBound = function(e) {
        updateVisibility(e.sender.value());
    };

    this.onCardTypeGroupListChange = function(e) {
        updateVisibility(e.sender.value());
    };

    // private

    function updateVisibility(card_type_group_id) {        
            if ((card_type_group_id == 2) || (card_type_group_id == 3)) {
                $('.bonus-card-only').show();
            } else {
                $('.bonus-card-only').hide();
            };
    }

}

var cardTypeAdd = null;

$(function () {
    cardTypeAdd = new CardTypeAdd();
    cardTypeAdd.init();
});
