﻿function ExchangeClientList() {
    _this = this;

    this.init = function () {
        setGridMaxHeight('#exchangeUserGrid', 0);
        
        $('.add-user-btn').bind('click', onBtnExchangeUserAddClick);
    };

    this.onClientDataBound = function(e) {
        var ddl = $('#clientList').getKendoDropDownList()
        ddl.select(0);        
    };

    this.onClientChange = function(e) {
        exchangeUserGridRefresh();
    };

    this.getClient = function(e) {
        var ddl1 = $('#clientList').getKendoDropDownList();
        var id1 = -1;

        if (ddl1) {
            var item1 = ddl1.dataItem();
            if (item1) {
                id1 = item1["client_id"];
            }
        };
        return {
            client_id: id1
        };
    };

    this.onBtnExchangeClientAddClick = function(e) {
        location.href = "/ExchangeClientAdd";
    };

    this.onRequestEnd1 = function(gridName) {
        return function (e) {
            var grid = $("#" + gridName).data("kendoGrid");
            if ((e.type == "create") || (e.type == "update")) {
                grid.dataSource.read();
            }
        }
    };

    function exchangeUserGridRefresh(e) {
        var grid = $('#exchangeUserGrid').getKendoGrid();
        grid.dataSource.read();
    };

    function onBtnExchangeUserAddClick(e) {
        e.preventDefault;

        var ddl1 = $('#clientList').getKendoDropDownList();
        var id1 = null;
        if (ddl1) {
            var item1 = ddl1.dataItem();
            if (item1) {
                id1 = item1["client_id"];
            }
        };

        if (!id1) {
            alert('Выберите участника обмена');
            return false;
        } else {
            location.href = "/ExchangeUserAdd?client_id=" + id1;
        };

        return false;
    };
}

var exchangeClientList = null;

$(function () {
    exchangeClientList = new ExchangeClientList();
    exchangeClientList.init();
});
