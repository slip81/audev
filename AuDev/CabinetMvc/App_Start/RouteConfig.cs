﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CabinetMvc
{
    public class RouteConfig
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static void RegisterRoutes(RouteCollection routes)
        {
            //logger.Info("RegisterRoutes start");

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region IsAuthorize

            routes.MapRoute(
                "IsAuthorizedRoute",
                "IsAuthorized",
                new { controller = "Base", action = "IsAuthorized" }
            );

            #endregion

            #region CabCacheClear

            routes.MapRoute(
                "CabCacheClearRoute",
                "CabCacheClear",
                new { controller = "Base", action = "CabCacheClear" }
            );

            #endregion

            #region HomeController

            #region Main

            routes.MapRoute(
                "GetSelectListFakeRoute",
                "GetSelectListFake",
                new { controller = "Home", action = "GetSelectListFake" }
            );

            routes.MapRoute(
                "MainRoute",
                "Main",
                new { controller = "Home", action = "Main" }
            );

            #endregion

            #region Widget

            routes.MapRoute(
                "SetWidgetRoute",
                "SetWidget",
                new { controller = "Home", action = "SetWidget" }
            );

            routes.MapRoute(
                "UpdateWidgetRoute",
                "UpdateWidget",
                new { controller = "Home", action = "UpdateWidget" }
            );
            
            #endregion

            #endregion

            #region UserController

            routes.MapRoute(
                "LoginRoute",
                "Login",
                new { controller = "User", action = "Login" }
            );

            routes.MapRoute(
                "LogoutRoute",
                "Logout",
                new { controller = "User", action = "Logout" }
            );

            routes.MapRoute(
                "RegisterRoute",
                "Register",
                new { controller = "User", action = "Register" }
            );

            routes.MapRoute(
                "RegisterSuccessRoute",
                "RegisterSuccess",
                new { controller = "User", action = "RegisterSuccess" }
            );

            routes.MapRoute(
                "ChangeRoleRoute",
                "ChangeRole",
                new { controller = "User", action = "ChangeRole" }
            );

            routes.MapRoute(
                "UserProfileRoute",
                "UserProfile",
                new { controller = "User", action = "UserProfile" }
            );

            routes.MapRoute(
                "ChangePasswordRoute",
                "ChangePassword",
                new { controller = "User", action = "ChangePassword" }
            );

            routes.MapRoute(
                "UserPasswordEditRoute",
                "UserPasswordEdit",
                new { controller = "User", action = "UserPasswordEdit" }
            );

            routes.MapRoute(
                "RestorePasswordRoute",
                "RestorePassword",
                new { controller = "User", action = "RestorePassword" }
            );            

            #endregion

            #region AdmController

            #region Log

            routes.MapRoute(
                "CabinetLogListRoute",
                "CabinetLogList",
                new { controller = "Adm", action = "CabinetLogList" }
            );

            routes.MapRoute(
                "GetCabinetLogListRoute",
                "GetCabinetLogList",
                new { controller = "Adm", action = "GetCabinetLogList" }
            );

            routes.MapRoute(
                "CabinetLogViewRoute",
                "CabinetLogView",
                new { controller = "Adm", action = "CabinetLogView" }
            );

            routes.MapRoute(
                "DiscountLogListRoute",
                "DiscountLogList",
                new { controller = "Adm", action = "DiscountLogList" }
            );

            routes.MapRoute(
                "GetDiscountLogListRoute",
                "GetDiscountLogList",
                new { controller = "Adm", action = "GetDiscountLogList" }
            );

            routes.MapRoute(
                "ServiceLogListRoute",
                "ServiceLogList",
                new { controller = "Adm", action = "ServiceLogList" }
            );

            routes.MapRoute(
                "GetServiceLogListRoute",
                "GetServiceLogList",
                new { controller = "Adm", action = "GetServiceLogList" }
            );

            routes.MapRoute(
                "ServiceLogViewRoute",
                "ServiceLogView",
                new { controller = "Adm", action = "ServiceLogView" }
            );

            routes.MapRoute(
                "GetLogDetailMessRoute",
                "GetLogDetailMess",
                new { controller = "Adm", action = "GetLogDetailMess" }
            );

            #endregion

            #region ChangeBusiness

            routes.MapRoute(
                "ChangeBusinessRoute",
                "ChangeBusiness",
                new { controller = "Adm", action = "ChangeBusiness" }
            );

            #endregion

            #region Role

            routes.MapRoute(
                "RoleListRoute",
                "RoleList",
                new { controller = "Adm", action = "RoleList" }
            );

            routes.MapRoute(
                "RoleAddRoute",
                "RoleAdd",
                new { controller = "Adm", action = "RoleAdd" }
            );

            routes.MapRoute(
                "RoleEditRoute",
                "RoleEdit",
                new { controller = "Adm", action = "RoleEdit" }
            );

            routes.MapRoute(
                "RoleActionEditRoute",
                "RoleActionEdit",
                new { controller = "Adm", action = "RoleActionEdit" }
            );
            
            routes.MapRoute(
                "RoleDelRoute",
                "RoleDel",
                new { controller = "Adm", action = "RoleDel" }
            );

            #endregion

            #region User

            routes.MapRoute(
                "UserListRoute",
                "UserList",
                new { controller = "Adm", action = "UserList" }
            );

            routes.MapRoute(
                "GetUserRoleListRoute",
                "GetUserRoleList",
                new { controller = "Adm", action = "GetUserRoleList" }
            );

            routes.MapRoute(
                "UserEditRoute",
                "UserEdit",
                new { controller = "Adm", action = "UserEdit" }
            );

            routes.MapRoute(
                "GetUserRoleActionListRoute",
                "GetUserRoleActionList",
                new { controller = "Adm", action = "GetUserRoleActionList" }
            );

            routes.MapRoute(
                "UserRoleActionEditRoute",
                "UserRoleActionEdit",
                new { controller = "Adm", action = "UserRoleActionEdit" }
            );

            routes.MapRoute(
                "UserDelRoute",
                "UserDel",
                new { controller = "Adm", action = "UserDel" }
            );

            #endregion

            #region Admin

            routes.MapRoute(
                "AdminListRoute",
                "AdminList",
                new { controller = "Adm", action = "AdminList" }
            );

            routes.MapRoute(
                "AdminAddRoute",
                "AdminAdd",
                new { controller = "Adm", action = "AdminAdd" }
            );

            routes.MapRoute(
                "AdminEditRoute",
                "AdminEdit",
                new { controller = "Adm", action = "AdminEdit" }
            );

            #endregion            

            #endregion

            #region CabinetController

            #region Client

            routes.MapRoute(
                "GetClientListRoute",
                "GetClientList",
                new { controller = "Cabinet", action = "GetClientList" }
            );

            routes.MapRoute(
                "GetClientList_SpravRoute",
                "GetClientList_Sprav",
                new { controller = "Cabinet", action = "GetClientList_Sprav" }
            );

            routes.MapRoute(
                "ClientListRoute",
                "ClientList",
                new { controller = "Cabinet", action = "ClientList" }
            );

            routes.MapRoute(
                "ClientEditRoute",
                "ClientEdit",
                new { controller = "Cabinet", action = "ClientEdit" }
            );

            routes.MapRoute(
                "ClientEdit2Route",
                "ClientEdit2",
                new { controller = "Cabinet", action = "ClientEdit2" }
            );            

            routes.MapRoute(
                "ClientActivateRoute",
                "ClientActivate",
                new { controller = "Cabinet", action = "ClientActivate" }
            );

            routes.MapRoute(
                "ClientAddRoute",
                "ClientAdd",
                new { controller = "Cabinet", action = "ClientAdd" }
            );

            routes.MapRoute(
                "GetClientList_StockRoute",
                "GetClientList_Stock",
                new { controller = "Cabinet", action = "GetClientList_Stock" }
            );

            routes.MapRoute(
                "ClientReportRoute",
                "ClientReport",
                new { controller = "Cabinet", action = "ClientReport" }
            );

            routes.MapRoute(
                "GetClientReportRoute",
                "GetClientReport",
                new { controller = "Cabinet", action = "GetClientReport" }
            );

            #endregion

            #region License

            routes.MapRoute(
                "GetLicenseListRoute",
                "GetLicenseList",
                new { controller = "Cabinet", action = "GetLicenseList" }
            );

            routes.MapRoute(
                "LicenseEditRoute",
                "LicenseEdit",
                new { controller = "Cabinet", action = "LicenseEdit" }
            );

            routes.MapRoute(
                "LicenseDelRoute",
                "LicenseDel",
                new { controller = "Cabinet", action = "LicenseDel" }
            );

            #endregion

            #region Workplace

            routes.MapRoute(
                "GetWorkplaceListRoute",
                "GetWorkplaceList",
                new { controller = "Cabinet", action = "GetWorkplaceList" }
            );

            routes.MapRoute(
                "WorkplaceDelRoute",
                "WorkplaceDel",
                new { controller = "Cabinet", action = "WorkplaceDel" }
            );

            routes.MapRoute(
                "WorkplaceAddRoute",
                "WorkplaceAdd",
                new { controller = "Cabinet", action = "WorkplaceAdd" }
            );

            routes.MapRoute(
                "WorkplaceEditRoute",
                "WorkplaceEdit",
                new { controller = "Cabinet", action = "WorkplaceEdit" }
            );

            routes.MapRoute(
                "GetWorkplaceReregListRoute",
                "GetWorkplaceReregList",
                new { controller = "Cabinet", action = "GetWorkplaceReregList" }
            );

            routes.MapRoute(
                "WorkplaceReregListRoute",
                "WorkplaceReregList",
                new { controller = "Cabinet", action = "WorkplaceReregList" }
            );  

            routes.MapRoute(
                "WorkplaceReregRoute",
                "WorkplaceRereg",
                new { controller = "Cabinet", action = "WorkplaceRereg" }
            );

            routes.MapRoute(
                "WorkplaceReregConfirmRoute",
                "WorkplaceReregConfirm",
                new { controller = "Cabinet", action = "WorkplaceReregConfirm" }
            );

            routes.MapRoute(
                "WorkplaceServiceRoute",
                "WorkplaceService",
                new { controller = "Cabinet", action = "WorkplaceService" }
            );
           
            routes.MapRoute(
                "ServiceCommonRoute",
                "ServiceCommon",
                new { controller = "Cabinet", action = "ServiceCommon" }
            );

            routes.MapRoute(
                "ServiceSenderRoute",
                "ServiceSender",
                new { controller = "Cabinet", action = "ServiceSender" }
            );

            routes.MapRoute(
                "GetWorkplaceServiceLogListRoute",
                "GetWorkplaceServiceLogList",
                new { controller = "Cabinet", action = "GetWorkplaceServiceLogList" }
            );

            routes.MapRoute(
                "ServiceCvaRoute",
                "ServiceCva",
                new { controller = "Cabinet", action = "ServiceCva" }
            );

            routes.MapRoute(
                "ServiceCvaDocRoute",
                "ServiceCvaDoc",
                new { controller = "Cabinet", action = "ServiceCvaDoc" }
            );

            routes.MapRoute(
                "ServiceDefectRoute",
                "ServiceDefect",
                new { controller = "Cabinet", action = "ServiceDefect" }
            );

            routes.MapRoute(
                "ServiceStockRoute",
                "ServiceStock",
                new { controller = "Cabinet", action = "ServiceStock" }
            );

            routes.MapRoute(
                "GetWorkplaceList_bySalesRoute",
                "GetWorkplaceList_bySales",
                new { controller = "Cabinet", action = "GetWorkplaceList_bySales" }
            );

            routes.MapRoute(
                "ServiceEsn2Route",
                "ServiceEsn2",
                new { controller = "Cabinet", action = "ServiceEsn2" }
            );

            routes.MapRoute(
                "GetServiceEsn2ListRoute",
                "GetServiceEsn2List",
                new { controller = "Cabinet", action = "GetServiceEsn2List" }
            );

            routes.MapRoute(
                "ServiceEsn2DelRoute",
                "ServiceEsn2Del",
                new { controller = "Cabinet", action = "ServiceEsn2Del" }
            );

            routes.MapRoute(
                "ServiceEsn2AddRoute",
                "ServiceEsn2Add",
                new { controller = "Cabinet", action = "ServiceEsn2Add" }
            );

            #endregion

            #region Order

            routes.MapRoute(
                "OrderListRoute",
                "OrderList",
                new { controller = "Cabinet", action = "OrderList" }
            );

            routes.MapRoute(
                "GetOrderListRoute",
                "GetOrderList",
                new { controller = "Cabinet", action = "GetOrderList" }
            );

            routes.MapRoute(
                "GetOrderActiveListRoute",
                "GetOrderActiveList",
                new { controller = "Cabinet", action = "GetOrderActiveList" }
            );

            routes.MapRoute(
                "GetOrderUnactiveListRoute",
                "GetOrderUnactiveList",
                new { controller = "Cabinet", action = "GetOrderUnactiveList" }
            );

            routes.MapRoute(
                "GetOrderListFullRoute",
                "GetOrderListFull",
                new { controller = "Cabinet", action = "GetOrderListFull" }
            );

            routes.MapRoute(
                "GetOrderItemListRoute",
                "GetOrderItemList",
                new { controller = "Cabinet", action = "GetOrderItemList" }
            );

            routes.MapRoute(
                "OrderEditRoute",
                "OrderEdit",
                new { controller = "Cabinet", action = "OrderEdit" }
            );

            routes.MapRoute(
                "OrderAddRoute",
                "OrderAdd",
                new { controller = "Cabinet", action = "OrderAdd" }
            );

            routes.MapRoute(
                "OrderActivateRoute",
                "OrderActivate",
                new { controller = "Cabinet", action = "OrderActivate" }
            );

            routes.MapRoute(
                "OrderConfirmRoute",
                "OrderConfirm",
                new { controller = "Cabinet", action = "OrderConfirm" }
            );

            routes.MapRoute(
                "ServiceOrderRoute",
                "ServiceOrder",
                new { controller = "Cabinet", action = "ServiceOrder" }
            );

            #endregion

            #region Sales

            routes.MapRoute(
                "SalesAddRoute",
                "SalesAdd",
                new { controller = "Cabinet", action = "SalesAdd" }
            );

            routes.MapRoute(
                "SalesEditRoute",
                "SalesEdit",
                new { controller = "Cabinet", action = "SalesEdit" }
            );

            routes.MapRoute(
                "SalesDelRoute",
                "SalesDel",
                new { controller = "Cabinet", action = "SalesDel" }
            );

            routes.MapRoute(
                "GetSalesList_SpravRoute",
                "GetSalesList_Sprav",
                new { controller = "Cabinet", action = "GetSalesList_Sprav" }
            );

            routes.MapRoute(
                "GetSalesList_CvaRoute",
                "GetSalesList_Cva",
                new { controller = "Cabinet", action = "GetSalesList_Cva" }
            );

            routes.MapRoute(
                "SalesSearchRoute",
                "SalesSearch",
                new { controller = "Cabinet", action = "SalesSearch" }
            );

            routes.MapRoute(
                "GetSalesList_StockRoute",
                "GetSalesList_Stock",
                new { controller = "Cabinet", action = "GetSalesList_Stock" }
            );

            #endregion

            #region Sprav

            routes.MapRoute(
                "GetSprav_RegionRoute",
                "GetSprav_Region",
                new { controller = "Cabinet", action = "GetSprav_Region" }
            );

            routes.MapRoute(
                "GetSprav_CityRoute",
                "GetSprav_City",
                new { controller = "Cabinet", action = "GetSprav_City" }
            );

            routes.MapRoute(
                "GetSprav_DistrictRoute",
                "GetSprav_District",
                new { controller = "Cabinet", action = "GetSprav_District" }
            );

            routes.MapRoute(
                "GetSprav_StreetRoute",
                "GetSprav_Street",
                new { controller = "Cabinet", action = "GetSprav_Street" }
            );
            
            routes.MapRoute(
                "RegionListRoute",
                "RegionList",
                new { controller = "Cabinet", action = "RegionList" }
            );

            routes.MapRoute(
                "GetRegionListRoute",
                "GetRegionList",
                new { controller = "Cabinet", action = "GetRegionList" }
            );

            routes.MapRoute(
                "RegionAddRoute",
                "RegionAdd",
                new { controller = "Cabinet", action = "RegionAdd" }
            );

            routes.MapRoute(
                "RegionEditRoute",
                "RegionEdit",
                new { controller = "Cabinet", action = "RegionEdit" }
            );

            routes.MapRoute(
                "RegionDelRoute",
                "RegionDel",
                new { controller = "Cabinet", action = "RegionDel" }
            );

            routes.MapRoute(
                "GetRegionZoneListRoute",
                "GetRegionZoneList",
                new { controller = "Cabinet", action = "GetRegionZoneList" }
            );

            routes.MapRoute(
                "GetSprav_RegionZoneRoute",
                "GetSprav_RegionZone",
                new { controller = "Cabinet", action = "GetSprav_RegionZone" }
            );

            routes.MapRoute(
                "RegionZoneAddRoute",
                "RegionZoneAdd",
                new { controller = "Cabinet", action = "RegionZoneAdd" }
            );

            routes.MapRoute(
                "RegionZoneEditRoute",
                "RegionZoneEdit",
                new { controller = "Cabinet", action = "RegionZoneEdit" }
            );

            routes.MapRoute(
                "RegionZoneDelRoute",
                "RegionZoneDel",
                new { controller = "Cabinet", action = "RegionZoneDel" }
            );

            routes.MapRoute(
                "RegionPriceLimitSaveRoute",
                "RegionPriceLimitSave",
                new { controller = "Cabinet", action = "RegionPriceLimitSave" }
            );

            routes.MapRoute(
                "RegionPriceLimitDelRoute",
                "RegionPriceLimitDel",
                new { controller = "Cabinet", action = "RegionPriceLimitDel" }
            );

            routes.MapRoute(
                "GetRegionZonePriceLimitListRoute",
                "GetRegionZonePriceLimitList",
                new { controller = "Cabinet", action = "GetRegionZonePriceLimitList" }
            );

            routes.MapRoute(
                "RegionZonePriceLimitSaveRoute",
                "RegionZonePriceLimitSave",
                new { controller = "Cabinet", action = "RegionZonePriceLimitSave" }
            );

            routes.MapRoute(
                "RegionZonePriceLimitDelRoute",
                "RegionZonePriceLimitDel",
                new { controller = "Cabinet", action = "RegionZonePriceLimitDel" }
            );
            
            #endregion

            #region Service

            routes.MapRoute(
                "GetServiceListRoute",
                "GetServiceList",
                new { controller = "Cabinet", action = "GetServiceList" }
            );

            /*
            routes.MapRoute(
                "ServiceUnregDelRoute",
                "ServiceUnregDel",
                new { controller = "Cabinet", action = "ServiceUnregDel" }
            );
            */

            routes.MapRoute(
                "ServiceRegRoute",
                "ServiceReg",
                new { controller = "Cabinet", action = "ServiceReg" }
            );

            routes.MapRoute(
                "GetClientServiceListRoute",
                "GetClientServiceList",
                new { controller = "Cabinet", action = "GetClientServiceList" }
            );

            routes.MapRoute(
                "GetClientServiceWorkplaceListRoute",
                "GetClientServiceWorkplaceList",
                new { controller = "Cabinet", action = "GetClientServiceWorkplaceList" }
            );

            routes.MapRoute(
                "GetClientServiceUnregListRoute",
                "GetClientServiceUnregList",
                new { controller = "Cabinet", action = "GetClientServiceUnregList" }
            );

            routes.MapRoute(
                "ClientServiceUnregDelRoute",
                "ClientServiceUnregDel",
                new { controller = "Cabinet", action = "ClientServiceUnregDel" }
            );            

            routes.MapRoute(
                "GetServiceRegListRoute",
                "GetServiceRegList",
                new { controller = "Cabinet", action = "GetServiceRegList" }
            );

            routes.MapRoute(
                "ServiceApplyRoute",
                "ServiceApply",
                new { controller = "Cabinet", action = "ServiceApply" }
            );

            routes.MapRoute(
                "ServiceListApplyRoute",
                "ServiceListApply",
                new { controller = "Cabinet", action = "ServiceListApply" }
            );            

            routes.MapRoute(
                "GetClientServiceParamListRoute",
                "GetClientServiceParamList",
                new { controller = "Cabinet", action = "GetClientServiceParamList" }
            );

            routes.MapRoute(
                "ClientServiceParamEditRoute",
                "ClientServiceParamEdit",
                new { controller = "Cabinet", action = "ClientServiceParamEdit" }
            );

            routes.MapRoute(
                "ServiceKitEditRoute",
                "ServiceKitEdit",
                new { controller = "Cabinet", action = "ServiceKitEdit" }
            );

            routes.MapRoute(
                "ServiceKitDelRoute",
                "ServiceKitDel",
                new { controller = "Cabinet", action = "ServiceKitDel" }
            );

            routes.MapRoute(
                "GetServiceKitItemListRoute",
                "GetServiceKitItemList",
                new { controller = "Cabinet", action = "GetServiceKitItemList" }
            );
            
            #endregion

            #region Version

            routes.MapRoute(
                "GetVersionListRoute",
                "GetVersionList",
                new { controller = "Cabinet", action = "GetVersionList" }
            );

            routes.MapRoute(
                "GetServiceVersionListRoute",
                "GetServiceVersionList",
                new { controller = "Cabinet", action = "GetServiceVersionList" }
            );

            routes.MapRoute(
                "VersionAddRoute",
                "VersionAdd",
                new { controller = "Cabinet", action = "VersionAdd" }
            );

            routes.MapRoute(
                "VersionEditRoute",
                "VersionEdit",
                new { controller = "Cabinet", action = "VersionEdit" }
            );

            routes.MapRoute(
                "VersionDelRoute",
                "VersionDel",
                new { controller = "Cabinet", action = "VersionDel" }
            );
            
            #endregion

            #region ClientUser

            routes.MapRoute(
                "GetClientUserListRoute",
                "GetClientUserList",
                new { controller = "Cabinet", action = "GetClientUserList" }
            );

            routes.MapRoute(
                "ClientUserEditRoute",
                "ClientUserEdit",
                new { controller = "Cabinet", action = "ClientUserEdit" }
            );

            routes.MapRoute(
                "ClientUserDelRoute",
                "ClientUserDel",
                new { controller = "Cabinet", action = "ClientUserDel" }
            ); 

            #endregion

            #region Reg

            routes.MapRoute(
                "GetRegActiveListRoute",
                "GetRegActiveList",
                new { controller = "Cabinet", action = "GetRegActiveList" }
            );

            routes.MapRoute(
                "GetRegListRoute",
                "GetRegList",
                new { controller = "Cabinet", action = "GetRegList" }
            );

            routes.MapRoute(
                "RegListRoute",
                "RegList",
                new { controller = "Cabinet", action = "RegList" }
            );

            routes.MapRoute(
                "RegEditRoute",
                "RegEdit",
                new { controller = "Cabinet", action = "RegEdit" }
            );

            #endregion

            #region CabSprav

            routes.MapRoute(
                "CabServiceListRoute",
                "CabServiceList",
                new { controller = "Cabinet", action = "CabServiceList" }
            );

            routes.MapRoute(
                "CabServiceEditRoute",
                "CabServiceEdit",
                new { controller = "Cabinet", action = "CabServiceEdit" }
            );

            routes.MapRoute(
                "VersionBatchAddRoute",
                "VersionBatchAdd",
                new { controller = "Cabinet", action = "VersionBatchAdd" }
            );

            routes.MapRoute(
                "VersionBatchDelRoute",
                "VersionBatchDel",
                new { controller = "Cabinet", action = "VersionBatchDel" }
            );

            routes.MapRoute(
                "GetSprav_TaxSystemTypeRoute",
                "GetSprav_TaxSystemType",
                new { controller = "Cabinet", action = "GetSprav_TaxSystemType" }
            );

            
            #endregion

            #region Delivery

            routes.MapRoute(
                "DeliveryTemplateListRoute",
                "DeliveryTemplateList",
                new { controller = "Cabinet", action = "DeliveryTemplateList" }
            );

            routes.MapRoute(
                "GetDeliveryTemplateListRoute",
                "GetDeliveryTemplateList",
                new { controller = "Cabinet", action = "GetDeliveryTemplateList" }
            );

            routes.MapRoute(
                "GetDeliveryTemplateGroupListRoute",
                "GetDeliveryTemplateGroupList",
                new { controller = "Cabinet", action = "GetDeliveryTemplateGroupList" }
            );

            routes.MapRoute(
                "DeliveryTemplateEditRoute",
                "DeliveryTemplateEdit",
                new { controller = "Cabinet", action = "DeliveryTemplateEdit" }
            );

            routes.MapRoute(
                "DeliveryTemplateDelRoute",
                "DeliveryTemplateDel",
                new { controller = "Cabinet", action = "DeliveryTemplateDel" }
            );

            routes.MapRoute(
                "GetDeliveryListRoute",
                "GetDeliveryList",
                new { controller = "Cabinet", action = "GetDeliveryList" }
            );

            routes.MapRoute(
                "DeliveryListRoute",
                "DeliveryList",
                new { controller = "Cabinet", action = "DeliveryList" }
            );

            routes.MapRoute(
                "DeliveryAddRoute",
                "DeliveryAdd",
                new { controller = "Cabinet", action = "DeliveryAdd" }
            );

            routes.MapRoute(
                "DeliveryEditRoute",
                "DeliveryEdit",
                new { controller = "Cabinet", action = "DeliveryEdit" }
            );

            routes.MapRoute(
                "DeliveryDelRoute",
                "DeliveryDel",
                new { controller = "Cabinet", action = "DeliveryDel" }
            );

            routes.MapRoute(
                "StartDeliveryRoute",
                "StartDelivery",
                new { controller = "Cabinet", action = "StartDelivery" }
            );

            routes.MapRoute(
                "StopDeliveryRoute",
                "StopDelivery",
                new { controller = "Cabinet", action = "StopDelivery" }
            );

            routes.MapRoute(
                "DeliveryDetailRoute",
                "DeliveryDetail",
                new { controller = "Cabinet", action = "DeliveryDetail" }
            );

            routes.MapRoute(
                "DeliveryTemplateFileSaveRoute",
                "DeliveryTemplateFileSave",
                new { controller = "Cabinet", action = "DeliveryTemplateFileSave" }
            );

            routes.MapRoute(
                "DeliveryTemplateFileRemoveRoute",
                "DeliveryTemplateFileRemove",
                new { controller = "Cabinet", action = "DeliveryTemplateFileRemove" }
            );
            
            #endregion            

            #region Compare

            routes.MapRoute(
                "CompareRoute",
                "Compare",
                new { controller = "Cabinet", action = "Compare" }
            );

            routes.MapRoute(
                "GetClientCabListRoute",
                "GetClientCabList",
                new { controller = "Cabinet", action = "GetClientCabList" }
            );

            routes.MapRoute(
                "GetClientCrmListRoute",
                "GetClientCrmList",
                new { controller = "Cabinet", action = "GetClientCrmList" }
            );

            routes.MapRoute(
                "ClientCabListUpdateRoute",
                "ClientCabListUpdate",
                new { controller = "Cabinet", action = "ClientCabListUpdate" }
            );

            #endregion

            #region ClientMaxVersion
            
            routes.MapRoute(
                "ClientMaxVersionRoute",
                "ClientMaxVersion",
                new { controller = "Cabinet", action = "ClientMaxVersion" }
            );

            routes.MapRoute(
                "GetClientMaxVersionListRoute",
                "GetClientMaxVersionList",
                new { controller = "Cabinet", action = "GetClientMaxVersionList" }
            );

            #endregion

            #endregion

            #region CardController

            #region BusinessOrg

            routes.MapRoute(
                "BusinessOrgListRoute",
                "BusinessOrgList",
                new { controller = "Card", action = "BusinessOrgList" }
            );

            routes.MapRoute(
                "GetBusinessOrgListRoute",
                "GetBusinessOrgList",
                new { controller = "Card", action = "GetBusinessOrgList" }
            );

            routes.MapRoute(
                "BusinessOrgAddRoute",
                "BusinessOrgAdd",
                new { controller = "Card", action = "BusinessOrgAdd" }
            );

            routes.MapRoute(
                "BusinessOrgEditRoute",
                "BusinessOrgEdit",
                new { controller = "Card", action = "BusinessOrgEdit" }
            );

            routes.MapRoute(
                "BusinessOrgDelRoute",
                "BusinessOrgDel",
                new { controller = "Card", action = "BusinessOrgDel" }
            );

            #endregion

            #region BusinessOrgUser

            routes.MapRoute(
                "GetBusinessOrgUserListRoute",
                "GetBusinessOrgUserList",
                new { controller = "Card", action = "GetBusinessOrgUserList" }
            );

            routes.MapRoute(
                "BusinessOrgUserEditRoute",
                "BusinessOrgUserEdit",
                new { controller = "Card", action = "BusinessOrgUserEdit" }
            );

            routes.MapRoute(
                "BusinessOrgUserAddRoute",
                "BusinessOrgUserAdd",
                new { controller = "Card", action = "BusinessOrgUserAdd" }
            );

            routes.MapRoute(
                "BusinessOrgUserDelRoute",
                "BusinessOrgUserDel",
                new { controller = "Card", action = "BusinessOrgUserDel" }
            );

            #endregion

            #region Business

            routes.MapRoute(
                "GetBusinessListRoute",
                "GetBusinessList",
                new { controller = "Card", action = "GetBusinessList" }
            );

            routes.MapRoute(
                "GetBusinessList_FullRoute",
                "GetBusinessList_Full",
                new { controller = "Card", action = "GetBusinessList_Full" }
            );

            routes.MapRoute(
                "GetBusinessList_SpravRoute",
                "GetBusinessList_Sprav",
                new { controller = "Card", action = "GetBusinessList_Sprav" }
            );

            routes.MapRoute(
                "BusinessListRoute",
                "BusinessList",
                new { controller = "Card", action = "BusinessList" }
            );

            routes.MapRoute(
                "BusinessEditRoute",
                "BusinessEdit",
                new { controller = "Card", action = "BusinessEdit" }
            );

            routes.MapRoute(
                "BusinessAddRoute",
                "BusinessAdd",
                new { controller = "Card", action = "BusinessAdd" }
            );

            routes.MapRoute(
                "BusinessDelRoute",
                "BusinessDel",
                new { controller = "Card", action = "BusinessDel" }
            );

            #endregion

            #region Business Summary

            routes.MapRoute(
                "GetBusinessSummaryRoute",
                "GetBusinessSummary",
                new { controller = "Card", action = "GetBusinessSummary" }
            );

            routes.MapRoute(
                "GetBusinessSummaryGraphRoute",
                "GetBusinessSummaryGraph",
                new { controller = "Card", action = "GetBusinessSummaryGraph" }
            );

            routes.MapRoute(
                "GetBusinessSummaryGraph2Route",
                "GetBusinessSummaryGraph2",
                new { controller = "Card", action = "GetBusinessSummaryGraph2" }
            );

            #endregion

            #region CardType

            routes.MapRoute(
                "CardTypeListRoute",
                "CardTypeList",
                new { controller = "Card", action = "CardTypeList" }
            );

            routes.MapRoute(
                "CardTypeAddRoute",
                "CardTypeAdd",
                new { controller = "Card", action = "CardTypeAdd" }
            );

            routes.MapRoute(
                "CardTypeEditRoute",
                "CardTypeEdit",
                new { controller = "Card", action = "CardTypeEdit" }
            );

            routes.MapRoute(
                "CardTypeDelRoute",
                "CardTypeDel",
                new { controller = "Card", action = "CardTypeDel" }
            );

            routes.MapRoute(
                "CardTypeBusinessOrgEditRoute",
                "CardTypeBusinessOrgEdit",
                new { controller = "Card", action = "CardTypeBusinessOrgEdit" }
            ); 

            #endregion

            #region CardTypeGroup

            routes.MapRoute(
                "GetCardTypeGroupListRoute",
                "GetCardTypeGroupList",
                new { controller = "Card", action = "GetCardTypeGroupList" }
            );

            #endregion

            #region CardTypeLimit

            routes.MapRoute(
                "GetCardTypeLimitListRoute",
                "GetCardTypeLimitList",
                new { controller = "Card", action = "GetCardTypeLimitList" }
            );

            routes.MapRoute(
                "CardTypeLimitAddRoute",
                "CardTypeLimitAdd",
                new { controller = "Card", action = "CardTypeLimitAdd" }
            );

            routes.MapRoute(
                "CardTypeLimitEditRoute",
                "CardTypeLimitEdit",
                new { controller = "Card", action = "CardTypeLimitEdit" }
            );

            routes.MapRoute(
                "CardTypeLimitDelRoute",
                "CardTypeLimitDel",
                new { controller = "Card", action = "CardTypeLimitDel" }
            );


            routes.MapRoute(
                "CardTypeLimit_SpreadOnCurrYearRoute",
                "CardTypeLimit_SpreadOnCurrYear",
                new { controller = "Card", action = "CardTypeLimit_SpreadOnCurrYear" }
            );

            #endregion

            #region CardClient

            routes.MapRoute(
                "CardClientListRoute",
                "CardClientList",
                new { controller = "Card", action = "CardClientList" }
            );

            routes.MapRoute(
                "GetCardClientListRoute",
                "GetCardClientList",
                new { controller = "Card", action = "GetCardClientList" }
            );

            routes.MapRoute(
                "CardClient_ValueMapperRoute",
                "CardClient_ValueMapper",
                new { controller = "Card", action = "CardClient_ValueMapper" }
            );

            routes.MapRoute(
                "CardClientAddRoute",
                "CardClientAdd",
                new { controller = "Card", action = "CardClientAdd" }
            );

            routes.MapRoute(
                "CardClientEditRoute",
                "CardClientEdit",
                new { controller = "Card", action = "CardClientEdit" }
            );

            routes.MapRoute(
                "CardClientDelRoute",
                "CardClientDel",
                new { controller = "Card", action = "CardClientDel" }
            );

            #endregion

            #region CardTransaction

            routes.MapRoute(
                "CardTransactionListRoute",
                "CardTransactionList",
                new { controller = "Card", action = "CardTransactionList" }
            );

            routes.MapRoute(
                "GetCardTransactionsListRoute",
                "GetCardTransactionsList",
                new { controller = "Card", action = "GetCardTransactionsList" }
            );


            routes.MapRoute(
                "GetLastCardTransactionsListRoute",
                "GetLastCardTransactionsList",
                new { controller = "Card", action = "GetLastCardTransactionsList" }
            );

            routes.MapRoute(
                "GetCardTransactionDetailListRoute",
                "GetCardTransactionDetailList",
                new { controller = "Card", action = "GetCardTransactionDetailList" }
            );            

            routes.MapRoute(
                "CardTransactionViewRoute",
                "CardTransactionView",
                new { controller = "Card", action = "CardTransactionView" }
                );

            routes.MapRoute(
                "GetCardTransactionsList_byCardRoute",
                "GetCardTransactionsList_byCard",
                new { controller = "Card", action = "GetCardTransactionsList_byCard" }
                );            

            #endregion

            #region Card

            routes.MapRoute(
                "CardListRoute",
                "CardList",
                new { controller = "Card", action = "CardList" }
            );

            routes.MapRoute(
                "GetCardListRoute",
                "GetCardList",
                new { controller = "Card", action = "GetCardList" }
            );

            routes.MapRoute(
                "CardAddRoute",
                "CardAdd",
                new { controller = "Card", action = "CardAdd" }
            );

            routes.MapRoute(
                "CardEditRoute",
                "CardEdit",
                new { controller = "Card", action = "CardEdit" }
            );

            routes.MapRoute(
                "CardDelRoute",
                "CardDel",
                new { controller = "Card", action = "CardDel" }
            );

            routes.MapRoute(
                "CardAdvancedSearchRoute",
                "CardAdvancedSearch",
                new { controller = "Card", action = "CardAdvancedSearch" }
            );

            routes.MapRoute(
                "CardListExportRoute",
                "CardListExport",
                new { controller = "Card", action = "CardListExport" }
            );

            routes.MapRoute(
                "GetCardNumListRoute",
                "GetCardNumList",
                new { controller = "Card", action = "GetCardNumList" }
            );

            #endregion

            #region Sprav

            routes.MapRoute(
                "GetSprav_ClientRoute",
                "GetSprav_Client",
                new { controller = "Card", action = "GetSprav_Client" }
            );

            routes.MapRoute(
                "GetSprav_CardTypeRoute",
                "GetSprav_CardType",
                new { controller = "Card", action = "GetSprav_CardType" }
            );

            routes.MapRoute(
                "GetSprav_CardType_byBusinessRoute",
                "GetSprav_CardType_byBusiness",
                new { controller = "Card", action = "GetSprav_CardType_byBusiness" }
            );

            routes.MapRoute(
                "GetCardTypeList_forCampaignRoute",
                "GetCardTypeList_forCampaign",
                new { controller = "Card", action = "GetCardTypeList_forCampaign" }
            );

            routes.MapRoute(
                "GetSprav_CardStatusRoute",
                "GetSprav_CardStatus",
                new { controller = "Card", action = "GetSprav_CardStatus" }
            );

            routes.MapRoute(
                "GetSprav_ProgrammTemplateRoute",
                "GetSprav_ProgrammTemplate",
                new { controller = "Card", action = "GetSprav_ProgrammTemplate" }
            );

            routes.MapRoute(
                "GetSprav_ProgrammRoute",
                "GetSprav_Programm",
                new { controller = "Card", action = "GetSprav_Programm" }
            );

            routes.MapRoute(
                "GetSprav_Client_byTextRoute",
                "GetSprav_Client_byText",
                new { controller = "Card", action = "GetSprav_Client_byText" }
            );

            #endregion

            #region Programm

            routes.MapRoute(
                "ProgrammListRoute",
                "ProgrammList",
                new { controller = "Card", action = "ProgrammList" }
            );

            routes.MapRoute(
                "ProgrammEditRoute",
                "ProgrammEdit",
                new { controller = "Card", action = "ProgrammEdit" }
            );

            routes.MapRoute(
                "ProgrammAddRoute",
                "ProgrammAdd",
                new { controller = "Card", action = "ProgrammAdd" }
            );

            routes.MapRoute(
                "ProgrammDelRoute",
                "ProgrammDel",
                new { controller = "Card", action = "ProgrammDel" }
            );

            routes.MapRoute(
                "GetProgrammListRoute",
                "GetProgrammList",
                new { controller = "Card", action = "GetProgrammList" }
            );

            routes.MapRoute(
                "ProgrammCopyRoute",
                "ProgrammCopy",
                new { controller = "Card", action = "ProgrammCopy" }
            );

            routes.MapRoute(
                "GetProgrammParamListRoute",
                "GetProgrammParamList",
                new { controller = "Card", action = "GetProgrammParamList" }
            );

            routes.MapRoute(
                "ProgramParamEditRoute",
                "ProgramParamEdit",
                new { controller = "Card", action = "ProgramParamEdit" }
            );

            routes.MapRoute(
                "GetProgrammOrderListRoute",
                "GetProgrammOrderList",
                new { controller = "Card", action = "GetProgrammOrderList" }
            );

            routes.MapRoute(
                "ProgrammAddDocRoute",
                "ProgrammAddDoc",
                new { controller = "Card", action = "ProgrammAddDoc" }
            );

            routes.MapRoute(
                "GetProgrammList_byBusinessRoute",
                "GetProgrammList_byBusiness",
                new { controller = "Card", action = "GetProgrammList_byBusiness" }
            );

            //GetProgrammList_Active
            routes.MapRoute(
                "GetProgrammList_ActiveRoute",
                "GetProgrammList_Active",
                new { controller = "Card", action = "GetProgrammList_Active" }
            );

            #endregion           

            #region CardBatchOperations

            routes.MapRoute(
                "CardBatchAddRoute",
                "CardBatchAdd",
                new { controller = "Card", action = "CardBatchAdd" }
            );

            routes.MapRoute(
                "BuildCardNumRoute",
                "BuildCardNum",
                new { controller = "Card", action = "BuildCardNum" }
            );

            routes.MapRoute(
                "CardBatchOperationsRoute",
                "CardBatchOperations",
                new { controller = "Card", action = "CardBatchOperations" }
            );

            routes.MapRoute(
                "GetCardBatchOperationsRoute",
                "GetCardBatchOperations",
                new { controller = "Card", action = "GetCardBatchOperations" }
            );

            routes.MapRoute(
                "CardBatchOperationsLogRoute",
                "CardBatchOperationsLog",
                new { controller = "Card", action = "CardBatchOperationsLog" }
            );

            routes.MapRoute(
                "RefreshBatchOperationsRoute",
                "RefreshBatchOperations",
                new { controller = "Card", action = "RefreshBatchOperations" }
            );

            routes.MapRoute(
                "CardBatchOperationsErrorListRoute",
                "CardBatchOperationsErrorList",
                new { controller = "Card", action = "CardBatchOperationsErrorList" }
            );

            routes.MapRoute(
                "GetCardBatchOperationsErrorsRoute",
                "GetCardBatchOperationsErrors",
                new { controller = "Card", action = "GetCardBatchOperationsErrors" }
            );

            #endregion

            #region CardImport

            routes.MapRoute(
                "CardImportRoute",
                "CardImport",
                new { controller = "Card", action = "CardImport" }
            );

            #endregion

            #region CardAdditionalNum

            routes.MapRoute(
                "GetCardAdditionalNumListRoute",
                "GetCardAdditionalNumList",
                new { controller = "Card", action = "GetCardAdditionalNumList" }
            );

            routes.MapRoute(
                "CardAdditionalNumAddRoute",
                "CardAdditionalNumAdd",
                new { controller = "Card", action = "CardAdditionalNumAdd" }
            );

            routes.MapRoute(
                "CardAdditionalNumEditRoute",
                "CardAdditionalNumEdit",
                new { controller = "Card", action = "CardAdditionalNumEdit" }
            );

            routes.MapRoute(
                "CardAdditionalNumDelRoute",
                "CardAdditionalNumDel",
                new { controller = "Card", action = "CardAdditionalNumDel" }
            );

            #endregion
            
            #region Campaign
            
            routes.MapRoute(
                "CampaignListRoute",
                "CampaignList",
                new { controller = "Card", action = "CampaignList" }
            );

            routes.MapRoute(
                "GetCampaignListRoute",
                "GetCampaignList",
                new { controller = "Card", action = "GetCampaignList" }
            );

            routes.MapRoute(
                "CampaignEditRoute",
                "CampaignEdit",
                new { controller = "Card", action = "CampaignEdit" }
            );

            routes.MapRoute(
                "CampaignDelRoute",
                "CampaignDel",
                new { controller = "Card", action = "CampaignDel" }
            );

            routes.MapRoute(
                "CampaignCheckDatesRoute",
                "CampaignCheckDates",
                new { controller = "Card", action = "CampaignCheckDates" }
            );

            #endregion
            
            #region ProgrammBuilder

            routes.MapRoute(
                "ProgrammBuilderRoute",
                "ProgrammBuilder",
                new { controller = "Card", action = "ProgrammBuilder" }
            );

            routes.MapRoute(
                "ProgrammHeaderAddRoute",
                "ProgrammHeaderAdd",
                new { controller = "Card", action = "ProgrammHeaderAdd" }
            );

            routes.MapRoute(
                "ProgrammHeaderEditRoute",
                "ProgrammHeaderEdit",
                new { controller = "Card", action = "ProgrammHeaderEdit" }
            );

            routes.MapRoute(
                "GetProgrammParamList_forStepParamRoute",
                "GetProgrammParamList_forStepParam",
                new { controller = "Card", action = "GetProgrammParamList_forStepParam" }
            );

            routes.MapRoute(
                "ProgrammParamAddRoute",
                "ProgrammParamAdd",
                new { controller = "Card", action = "ProgrammParamAdd" }
            );

            routes.MapRoute(
                "ProgrammParamEditRoute",
                "ProgrammParamEdit",
                new { controller = "Card", action = "ProgrammParamEdit" }
            );

            routes.MapRoute(
                "ProgrammParamDelRoute",
                "ProgrammParamDel",
                new { controller = "Card", action = "ProgrammParamDel" }
            );
       
            routes.MapRoute(
                "GetProgrammStepListRoute",
                "GetProgrammStepList",
                new { controller = "Card", action = "GetProgrammStepList" }
            );

            routes.MapRoute(
                "ProgrammStepAddRoute",
                "ProgrammStepAdd",
                new { controller = "Card", action = "ProgrammStepAdd" }
            );

            routes.MapRoute(
                "ProgrammStepEditRoute",
                "ProgrammStepEdit",
                new { controller = "Card", action = "ProgrammStepEdit" }
            );

            routes.MapRoute(
                "ProgrammStepDelRoute",
                "ProgrammStepDel",
                new { controller = "Card", action = "ProgrammStepDel" }
            );

            routes.MapRoute(
                "GetProgrammStepParamListRoute",
                "GetProgrammStepParamList",
                new { controller = "Card", action = "GetProgrammStepParamList" }
            );            
            
            routes.MapRoute(
                "GetProgrammStepParamList_forStepParamRoute",
                "GetProgrammStepParamList_forStepParam",
                new { controller = "Card", action = "GetProgrammStepParamList_forStepParam" }
            );

            routes.MapRoute(
                "GetProgrammStepParamList_forStepRoute",
                "GetProgrammStepParamList_forStep",
                new { controller = "Card", action = "GetProgrammStepParamList_forStep" }
            );

            routes.MapRoute(
                "ProgrammStepParamAddRoute",
                "ProgrammStepParamAdd",
                new { controller = "Card", action = "ProgrammStepParamAdd" }
            );

            routes.MapRoute(
                "ProgrammStepParamEditRoute",
                "ProgrammStepParamEdit",
                new { controller = "Card", action = "ProgrammStepParamEdit" }
            );

            routes.MapRoute(
                "ProgrammStepParaDelRoute",
                "ProgrammStepParaDel",
                new { controller = "Card", action = "ProgrammStepParaDel" }
            );
            
            routes.MapRoute(
                "GetProgrammStepConditionListRoute",
                "GetProgrammStepConditionList",
                new { controller = "Card", action = "GetProgrammStepConditionList" }
            );

            routes.MapRoute(
                "GetProgrammStepConditionList_forStepRoute",
                "GetProgrammStepConditionList_forStep",
                new { controller = "Card", action = "GetProgrammStepConditionList_forStep" }
            );

            routes.MapRoute(
                "ProgrammStepConditionAddRoute",
                "ProgrammStepConditionAdd",
                new { controller = "Card", action = "ProgrammStepConditionAdd" }
            );

            routes.MapRoute(
                "ProgrammStepConditionEditRoute",
                "ProgrammStepConditionEdit",
                new { controller = "Card", action = "ProgrammStepConditionEdit" }
            );

            routes.MapRoute(
                "ProgrammStepConditionDelRoute",
                "ProgrammStepConditionDel",
                new { controller = "Card", action = "ProgrammStepConditionDel" }
            );
            
            routes.MapRoute(
                "GetProgrammStepLogicListRoute",
                "GetProgrammStepLogicList",
                new { controller = "Card", action = "GetProgrammStepLogicList" }
            );

            routes.MapRoute(
                "ProgrammStepLogicAddRoute",
                "ProgrammStepLogicAdd",
                new { controller = "Card", action = "ProgrammStepLogicAdd" }
            );

            routes.MapRoute(
                "ProgrammStepLogicEditRoute",
                "ProgrammStepLogicEdit",
                new { controller = "Card", action = "ProgrammStepLogicEdit" }
            );

            routes.MapRoute(
                "ProgrammStepLogicDelRoute",
                "ProgrammStepLogicDel",
                new { controller = "Card", action = "ProgrammStepLogicDel" }
            );

            #endregion

            #region  CardBrief

            routes.MapRoute(
                "CardBriefRoute",
                "CardBrief",
                new { controller = "Card", action = "CardBrief" }
            );

            #endregion

            #endregion

            #region ReportController

            routes.MapRoute(
                "ReportListRoute",
                "ReportList",
                new { controller = "Report", action = "ReportList" }
            );

            routes.MapRoute(
                "GetReportsRoute",
                "GetReports",
                new { controller = "Report", action = "GetReports" }
            );            

            routes.MapRoute(
                "Report1Route",
                "Report1",
                new { controller = "Report", action = "Report1" }
            );

            routes.MapRoute(
                "Report2Route",
                "Report2",
                new { controller = "Report", action = "Report2" }
            );

            #endregion

            #region DefectController

            routes.MapRoute(
                "DefectListRoute",
                "DefectList",
                new { controller = "Defect", action = "DefectList" }
            );

            routes.MapRoute(
                "GetDefectListRoute",
                "GetDefectList",
                new { controller = "Defect", action = "GetDefectList" }
            );

            routes.MapRoute(
                "DefectRequestListRoute",
                "DefectRequestList",
                new { controller = "Defect", action = "DefectRequestList" }
            );

            routes.MapRoute(
                "GetDefectRequestListRoute",
                "GetDefectRequestList",
                new { controller = "Defect", action = "GetDefectRequestList" }
            );

            routes.MapRoute(
                "DefectDownloadRoute",
                "DefectDownload",
                new { controller = "Defect", action = "DefectDownload" }
            );

            #endregion

            #region ExchangeController

            routes.MapRoute(
                "ExchangeClientListRoute",
                "ExchangeClientList",
                new { controller = "Exchange", action = "ExchangeClientList" }
            );

            routes.MapRoute(
                "GetExchangeClientListRoute",
                "GetExchangeClientList",
                new { controller = "Exchange", action = "GetExchangeClientList" }
            );

            routes.MapRoute(
                "ExchangeClientAddRoute",
                "ExchangeClientAdd",
                new { controller = "Exchange", action = "ExchangeClientAdd" }
            );

            routes.MapRoute(
                "ExchangeClientEditRoute",
                "ExchangeClientEdit",
                new { controller = "Exchange", action = "ExchangeClientEdit" }
            );

            routes.MapRoute(
                "ExchangeClientDelRoute",
                "ExchangeClientDel",
                new { controller = "Exchange", action = "ExchangeClientDel" }
            );

            routes.MapRoute(
                "ExchangeRegFromCabClientRoute",
                "ExchangeRegFromCabClient",
                new { controller = "Exchange", action = "ExchangeRegFromCabClient" }
            );  

            routes.MapRoute(
                "GetExchangeUserList_byClientRoute",
                "GetExchangeUserList_byClient",
                new { controller = "Exchange", action = "GetExchangeUserList_byClient" }
            );

            routes.MapRoute(
                "ExchangeUserAddRoute",
                "ExchangeUserAdd",
                new { controller = "Exchange", action = "ExchangeUserAdd" }
            );
            
            routes.MapRoute(
                "ExchangeUserEditRoute",
                "ExchangeUserEdit",
                new { controller = "Exchange", action = "ExchangeUserEdit" }
            );

            routes.MapRoute(
                "ExchangeUserDelRoute",
                "ExchangeUserDel",
                new { controller = "Exchange", action = "ExchangeUserDel" }
            );

            routes.MapRoute(
                "GetExchangeRegPartnerListRoute",
                "GetExchangeRegPartnerList",
                new { controller = "Exchange", action = "GetExchangeRegPartnerList" }
            );

            routes.MapRoute(
                "ExchangeRegPartnerAddRoute",
                "ExchangeRegPartnerAdd",
                new { controller = "Exchange", action = "ExchangeRegPartnerAdd" }
            );

            routes.MapRoute(
                "ExchangeRegPartnerDelRoute",
                "ExchangeRegPartnerDel",
                new { controller = "Exchange", action = "ExchangeRegPartnerDel" }
            );

            routes.MapRoute(
                "ExchangeRegPartnerEditRoute",
                "ExchangeRegPartnerEdit",
                new { controller = "Exchange", action = "ExchangeRegPartnerEdit" }
            );

            routes.MapRoute(
                "GetExchangeRegDocListRoute",
                "GetExchangeRegDocList",
                new { controller = "Exchange", action = "GetExchangeRegDocList" }
            );

            routes.MapRoute(
                "ExchangeRegDocAddRoute",
                "ExchangeRegDocAdd",
                new { controller = "Exchange", action = "ExchangeRegDocAdd" }
            );

            routes.MapRoute(
                "ExchangeRegDocDelRoute",
                "ExchangeRegDocDel",
                new { controller = "Exchange", action = "ExchangeRegDocDel" }
            );

            routes.MapRoute(
                "ExchangeRegDocEditRoute",
                "ExchangeRegDocEdit",
                new { controller = "Exchange", action = "ExchangeRegDocEdit" }
            );

            routes.MapRoute(
                "GetExchangeRegDocSalesListRoute",
                "GetExchangeRegDocSalesList",
                new { controller = "Exchange", action = "GetExchangeRegDocSalesList" }
            );

            routes.MapRoute(
                "ExchangeRegDocSalesEditRoute",
                "ExchangeRegDocSalesEdit",
                new { controller = "Exchange", action = "ExchangeRegDocSalesEdit" }
            );

            routes.MapRoute(
                "GetExchangePartnerListRoute",
                "GetExchangePartnerList",
                new { controller = "Exchange", action = "GetExchangePartnerList" }
            );

            routes.MapRoute(
                "ExchangePartnerListRoute",
                "ExchangePartnerList",
                new { controller = "Exchange", action = "ExchangePartnerList" }
            );

            routes.MapRoute(
                "ExchangePartnerAddRoute",
                "ExchangePartnerAdd",
                new { controller = "Exchange", action = "ExchangePartnerAdd" }
            );

            routes.MapRoute(
                "ExchangePartnerEditRoute",
                "ExchangePartnerEdit",
                new { controller = "Exchange", action = "ExchangePartnerEdit" }
            );

            routes.MapRoute(
                "ExchangePartnerDelRoute",
                "ExchangePartnerDel",
                new { controller = "Exchange", action = "ExchangePartnerDel" }
            );

            routes.MapRoute(
                "ExchangeDataViewRoute",
                "ExchangeDataView",
                new { controller = "Exchange", action = "ExchangeDataView" }
            );

            routes.MapRoute(
                "ExchangeDocDataViewRoute",
                "ExchangeDocDataView",
                new { controller = "Exchange", action = "ExchangeDocDataView" }
            );

            routes.MapRoute(
                "ExchangeLogListRoute",
                "ExchangeLogList",
                new { controller = "Exchange", action = "ExchangeLogList" }
            );

            routes.MapRoute(
                "GetExchangeLogClientListRoute",
                "GetExchangeLogClientList",
                new { controller = "Exchange", action = "GetExchangeLogClientList" }
            );

            routes.MapRoute(
                "GetExchangeLogUserListRoute",
                "GetExchangeLogUserList",
                new { controller = "Exchange", action = "GetExchangeLogUserList" }
            );

            routes.MapRoute(
                "GetExchangeLogDateListRoute",
                "GetExchangeLogDateList",
                new { controller = "Exchange", action = "GetExchangeLogDateList" }
            );

            routes.MapRoute(
                "GetExchangeLogListRoute",
                "GetExchangeLogList",
                new { controller = "Exchange", action = "GetExchangeLogList" }
            );

            routes.MapRoute(
                "ExchangeMonitorRoute",
                "ExchangeMonitor",
                new { controller = "Exchange", action = "ExchangeMonitor" }
            );

            routes.MapRoute(
                "GetExchangeMonitorRoute",
                "GetExchangeMonitor",
                new { controller = "Exchange", action = "GetExchangeMonitor" }
            );

            routes.MapRoute(
                "GetExchangePartnerList_byClientRoute",
                "GetExchangePartnerList_byClient",
                new { controller = "Exchange", action = "GetExchangePartnerList_byClient" }
            );

            routes.MapRoute(
                "ExchangeRegPartnerItemEditRoute",
                "ExchangeRegPartnerItemEdit",
                new { controller = "Exchange", action = "ExchangeRegPartnerItemEdit" }
            );

            routes.MapRoute(
                "GetExchangeMonitorLogListRoute",
                "GetExchangeMonitorLogList",
                new { controller = "Exchange", action = "GetExchangeMonitorLogList" }
            );
            
            routes.MapRoute(
                "GetExchangeTaskListRoute",
                "GetExchangeTaskList",
                new { controller = "Exchange", action = "GetExchangeTaskList" }
            );

            routes.MapRoute(
                "ExchangeTaskAddRoute",
                "ExchangeTaskAdd",
                new { controller = "Exchange", action = "ExchangeTaskAdd" }
            );

            routes.MapRoute(
                "ExchangeTaskEditRoute",
                "ExchangeTaskEdit",
                new { controller = "Exchange", action = "ExchangeTaskEdit" }
            );

            routes.MapRoute(
                "ExchangeTaskDelRoute",
                "ExchangeTaskDel",
                new { controller = "Exchange", action = "ExchangeTaskDel" }
            );

            routes.MapRoute(
                "ExchangeTaskTypeAddRoute",
                "ExchangeTaskTypeAdd",
                new { controller = "Exchange", action = "ExchangeTaskTypeAdd" }
            );

            routes.MapRoute(
                "GetExchangeFileLogListRoute",
                "GetExchangeFileLogList",
                new { controller = "Exchange", action = "GetExchangeFileLogList" }
            );

            routes.MapRoute(
                "GetExchangeFileNodeListRoute",
                "GetExchangeFileNodeList",
                new { controller = "Exchange", action = "GetExchangeFileNodeList" }
            );

            routes.MapRoute(
                "ExchangeFileNodeAddRoute",
                "ExchangeFileNodeAdd",
                new { controller = "Exchange", action = "ExchangeFileNodeAdd" }
            );

            routes.MapRoute(
                "ExchangeFileNodeEditRoute",
                "ExchangeFileNodeEdit",
                new { controller = "Exchange", action = "ExchangeFileNodeEdit" }
            );

            routes.MapRoute(
                "ExchangeFileNodeDelRoute",
                "ExchangeFileNodeDel",
                new { controller = "Exchange", action = "ExchangeFileNodeDel" }
            );

            routes.MapRoute(
                "GetExchangePartnerItemForGetListRoute",
                "GetExchangePartnerItemForGetList",
                new { controller = "Exchange", action = "GetExchangePartnerItemForGetList" }
            );

            routes.MapRoute(
                "GetExchangePartnerItemForSendListRoute",
                "GetExchangePartnerItemForSendList",
                new { controller = "Exchange", action = "GetExchangePartnerItemForSendList" }
            );

            #endregion

            #region Cva
            
            routes.MapRoute(
                "CvaLogListRoute",
                "CvaLogList",
                new { controller = "Cva", action = "CvaLogList" }
            );

            routes.MapRoute(
                "CvaDataViewRoute",
                "CvaDataView",
                new { controller = "Cva", action = "CvaDataView" }
            );

            routes.MapRoute(
                "GetCvaLogListRoute",
                "GetCvaLogList",
                new { controller = "Cva", action = "GetCvaLogList" }
            );
            
            #endregion

            #region HelpDeskController

            routes.MapRoute(
                "HelpDeskRoute",
                "HelpDesk",
                new { controller = "HelpDesk", action = "HelpDesk" }
            );

            routes.MapRoute(
                "GetStoryListRoute",
                "GetStoryList",
                new { controller = "HelpDesk", action = "GetStoryList" }
            );

            routes.MapRoute(
                "StoryEditRoute",
                "StoryEdit",
                new { controller = "HelpDesk", action = "StoryEdit" }
            );

            #endregion

            #region VedController

            routes.MapRoute(
                "GetSprav_ArcTypeRoute",
                "GetSprav_ArcType",
                new { controller = "Ved", action = "GetSprav_ArcType" }
            );

            routes.MapRoute(
                "VedSourceEditRoute",
                "VedSourceEdit",
                new { controller = "Ved", action = "VedSourceEdit" }
            );

            routes.MapRoute(
                "GetVedSourceRegionListRoute",
                "GetVedSourceRegionList",
                new { controller = "Ved", action = "GetVedSourceRegionList" }
            );

            routes.MapRoute(
                "VedSourceRegionListRoute",
                "VedSourceRegionList",
                new { controller = "Ved", action = "VedSourceRegionList" }
            );

            routes.MapRoute(
                "VedSourceRegionEditRoute",
                "VedSourceRegionEdit",
                new { controller = "Ved", action = "VedSourceRegionEdit" }
            );

            routes.MapRoute(
                "VedSourceRegionDelRoute",
                "VedSourceRegionDel",
                new { controller = "Ved", action = "VedSourceRegionDel" }
            );

            routes.MapRoute(
                "GetVedReestrItemListRoute",
                "GetVedReestrItemList",
                new { controller = "Ved", action = "GetVedReestrItemList" }
            );

            routes.MapRoute(
                "VedReestrItemListRoute",
                "VedReestrItemList",
                new { controller = "Ved", action = "VedReestrItemList" }
            );

            routes.MapRoute(
                "GetVedReestrItemOutListRoute",
                "GetVedReestrItemOutList",
                new { controller = "Ved", action = "GetVedReestrItemOutList" }
            );

            routes.MapRoute(
                "VedReestrItemOutListRoute",
                "VedReestrItemOutList",
                new { controller = "Ved", action = "VedReestrItemOutList" }
            );

            routes.MapRoute(
                "GetVedReestrRegionItemOutListRoute",
                "GetVedReestrRegionItemOutList",
                new { controller = "Ved", action = "GetVedReestrRegionItemOutList" }
            );

            routes.MapRoute(
                "VedReestrRegionItemOutListRoute",
                "VedReestrRegionItemOutList",
                new { controller = "Ved", action = "VedReestrRegionItemOutList" }
            );

            routes.MapRoute(
                "GetVedReestrRegionItemListRoute",
                "GetVedReestrRegionItemList",
                new { controller = "Ved", action = "GetVedReestrRegionItemList" }
            );

            routes.MapRoute(
                "VedReestrRegionItemListRoute",
                "VedReestrRegionItemList",
                new { controller = "Ved", action = "VedReestrRegionItemList" }
            );

            routes.MapRoute(
                "GetVedReestrRegionItemErrorListRoute",
                "GetVedReestrRegionItemErrorList",
                new { controller = "Ved", action = "GetVedReestrRegionItemErrorList" }
            );

            routes.MapRoute(
                "VedReestrRegionItemErrorListRoute",
                "VedReestrRegionItemErrorList",
                new { controller = "Ved", action = "VedReestrRegionItemErrorList" }
            );

            routes.MapRoute(
                "GetVedRequestListRoute",
                "GetVedRequestList",
                new { controller = "Ved", action = "GetVedRequestList" }
            );

            routes.MapRoute(
                "VedRequestListRoute",
                "VedRequestList",
                new { controller = "Ved", action = "VedRequestList" }
            );

            routes.MapRoute(
                "GetVedTemplateListRoute",
                "GetVedTemplateList",
                new { controller = "Ved", action = "GetVedTemplateList" }
            );

            routes.MapRoute(
                "VedTemplateListRoute",
                "VedTemplateList",
                new { controller = "Ved", action = "VedTemplateList" }
            );

            routes.MapRoute(
                "VedTemplateEditRoute",
                "VedTemplateEdit",
                new { controller = "Ved", action = "VedTemplateEdit" }
            );

            routes.MapRoute(
                "VedTemplateDelRoute",
                "VedTemplateDel",
                new { controller = "Ved", action = "VedTemplateDel" }
            );

            routes.MapRoute(
                "GetVedTemplateColumnListRoute",
                "GetVedTemplateColumnList",
                new { controller = "Ved", action = "GetVedTemplateColumnList" }
            );

            routes.MapRoute(
                "GetVedReestrItemFreeListRoute",
                "GetVedReestrItemFreeList",
                new { controller = "Ved", action = "GetVedReestrItemFreeList" }
            );

            routes.MapRoute(
                "VedExportRoute",
                "VedExport",
                new { controller = "Ved", action = "VedExport" }
            );

            routes.MapRoute(
                "VedReestrItemExportRoute",
                "VedReestrItemExport",
                new { controller = "Ved", action = "VedReestrItemExport" }
            );

            routes.MapRoute(
                "VedReestrItemOutExportRoute",
                "VedReestrItemOutExport",
                new { controller = "Ved", action = "VedReestrItemOutExport" }
            );

            routes.MapRoute(
                "VedReestrItemErrorExportRoute",
                "VedReestrItemErrorExport",
                new { controller = "Ved", action = "VedReestrItemErrorExport" }
            );
            
            #endregion

            #region CrmController

            #region CrmPriority

            routes.MapRoute(
                "GetCrmPriorityListRoute",
                "GetCrmPriorityList",
                new { controller = "Crm", action = "GetCrmPriorityList" }
            );

            routes.MapRoute(
                "CrmPriorityAddRoute",
                "CrmPriorityAdd",
                new { controller = "Crm", action = "CrmPriorityAdd" }
            );

            routes.MapRoute(
                "CrmPriorityEditRoute",
                "CrmPriorityEdit",
                new { controller = "Crm", action = "CrmPriorityEdit" }
            );

            routes.MapRoute(
                "CrmPriorityDelRoute",
                "CrmPriorityDel",
                new { controller = "Crm", action = "CrmPriorityDel" }
            );
           
            #endregion

            #region CrmProject

            routes.MapRoute(
                "GetCrmProjectListRoute",
                "GetCrmProjectList",
                new { controller = "Crm", action = "GetCrmProjectList" }
            );
            
            routes.MapRoute(
                "GetCrmProjectSpravListRoute",
                "GetCrmProjectSpravList",
                new { controller = "Crm", action = "GetCrmProjectSpravList" }
            );

            routes.MapRoute(
                "CrmProjectAddRoute",
                "CrmProjectAdd",
                new { controller = "Crm", action = "CrmProjectAdd" }
            );

            routes.MapRoute(
                "CrmProjectEditRoute",
                "CrmProjectEdit",
                new { controller = "Crm", action = "CrmProjectEdit" }
            );

            routes.MapRoute(
                "CrmProjectDelRoute",
                "CrmProjectDel",
                new { controller = "Crm", action = "CrmProjectDel" }
            );

            #endregion

            #region CrmState

            routes.MapRoute(
                "GetCrmStateListRoute",
                "GetCrmStateList",
                new { controller = "Crm", action = "GetCrmStateList" }
            );

            routes.MapRoute(
                "CrmStateAddRoute",
                "CrmStateAdd",
                new { controller = "Crm", action = "CrmStateAdd" }
            );

            routes.MapRoute(
                "CrmStateEditRoute",
                "CrmStateEdit",
                new { controller = "Crm", action = "CrmStateEdit" }
            );

            routes.MapRoute(
                "CrmStateDelRoute",
                "CrmStateDel",
                new { controller = "Crm", action = "CrmStateDel" }
            );

            #endregion

            #region CrmModule

            routes.MapRoute(
                "GetCrmModuleListRoute",
                "GetCrmModuleList",
                new { controller = "Crm", action = "GetCrmModuleList" }
            );

            routes.MapRoute(
                "CrmModuleAddRoute",
                "CrmModuleAdd",
                new { controller = "Crm", action = "CrmModuleAdd" }
            );

            routes.MapRoute(
                "CrmModuleEditRoute",
                "CrmModuleEdit",
                new { controller = "Crm", action = "CrmModuleEdit" }
            );

            routes.MapRoute(
                "CrmModuleDelRoute",
                "CrmModuleDel",
                new { controller = "Crm", action = "CrmModuleDel" }
            );

            #endregion            

            #region CrmModuleVersion

            routes.MapRoute(
                "GetCrmModuleVersionListRoute",
                "GetCrmModuleVersionList",
                new { controller = "Crm", action = "GetCrmModuleVersionList" }
            );

            routes.MapRoute(
                "CrmModuleVersionAddRoute",
                "CrmModuleVersionAdd",
                new { controller = "Crm", action = "CrmModuleVersionAdd" }
            );

            routes.MapRoute(
                "CrmModuleVersionEditRoute",
                "CrmModuleVersionEdit",
                new { controller = "Crm", action = "CrmModuleVersionEdit" }
            );

            routes.MapRoute(
                "CrmModuleVersionDelRoute",
                "CrmModuleVersionDel",
                new { controller = "Crm", action = "CrmModuleVersionDel" }
            );

            #endregion

            #region CrmProjectVersion

            routes.MapRoute(
                "GetCrmProjectVersionListRoute",
                "GetCrmProjectVersionList",
                new { controller = "Crm", action = "GetCrmProjectVersionList" }
            );

            routes.MapRoute(
                "CrmProjectVersionAddRoute",
                "CrmProjectVersionAdd",
                new { controller = "Crm", action = "CrmProjectVersionAdd" }
            );

            routes.MapRoute(
                "CrmProjectVersionEditRoute",
                "CrmProjectVersionEdit",
                new { controller = "Crm", action = "CrmProjectVersionEdit" }
            );

            routes.MapRoute(
                "CrmProjectVersionDelRoute",
                "CrmProjectVersionDel",
                new { controller = "Crm", action = "CrmProjectVersionDel" }
            );

            #endregion


            #region GetCrmModulePart

            routes.MapRoute(
                "GetCrmModulePartListRoute",
                "GetCrmModulePartList",
                new { controller = "Crm", action = "GetCrmModulePartList" }
            );

            routes.MapRoute(
                "CrmModulePartAddRoute",
                "CrmModulePartAdd",
                new { controller = "Crm", action = "CrmModulePartAdd" }
            );

            routes.MapRoute(
                "CrmModulePartEditRoute",
                "CrmModulePartEdit",
                new { controller = "Crm", action = "CrmModulePartEdit" }
            );

            routes.MapRoute(
                "CrmModulePartDelRoute",
                "CrmModulePartDel",
                new { controller = "Crm", action = "CrmModulePartDel" }
            );

            #endregion

            #region CrmClient

            routes.MapRoute(
                "GetCrmClientListRoute",
                "GetCrmClientList",
                new { controller = "Crm", action = "GetCrmClientList" }
            );

            routes.MapRoute(
                "CrmClientAddRoute",
                "CrmClientAdd",
                new { controller = "Crm", action = "CrmClientAdd" }
            );

            routes.MapRoute(
                "CrmClientEditRoute",
                "CrmClientEdit",
                new { controller = "Crm", action = "CrmClientEdit" }
            );

            routes.MapRoute(
                "CrmClientDelRoute",
                "CrmClientDel",
                new { controller = "Crm", action = "CrmClientDel" }
            );

            #endregion

            #region CrmProjectUser

            routes.MapRoute(
                "GetCrmProjectUserListRoute",
                "GetCrmProjectUserList",
                new { controller = "Crm", action = "GetCrmProjectUserList" }
            );

            routes.MapRoute(
                "CrmProjectUserAddRoute",
                "CrmProjectUserAdd",
                new { controller = "Crm", action = "CrmProjectUserAdd" }
            );

            routes.MapRoute(
                "CrmProjectUserEditRoute",
                "CrmProjectUserEdit",
                new { controller = "Crm", action = "CrmProjectUserEdit" }
            );

            routes.MapRoute(
                "CrmProjectUserDelRoute",
                "CrmProjectUserDel",
                new { controller = "Crm", action = "CrmProjectUserDel" }
            );

            #endregion

            #region TaskRel
            
            routes.MapRoute(
                "GetTaskRelListRoute",
                "GetTaskRelList",
                new { controller = "Crm", action = "GetTaskRelList" }
            );

            routes.MapRoute(
                "TaskRelAddRoute",
                "TaskRelAdd",
                new { controller = "Crm", action = "TaskRelAdd" }
            );

            routes.MapRoute(
                "TaskRelDelRoute",
                "TaskRelDel",
                new { controller = "Crm", action = "TaskRelDel" }
            );            

            #endregion

            #region TaskRelType

            routes.MapRoute(
                "GetCrmRelTypeListRoute",
                "GetCrmRelTypeList",
                new { controller = "Crm", action = "GetCrmRelTypeList" }
            );

            routes.MapRoute(
                "CrmRelTypeAddRoute",
                "CrmRelTypeAdd",
                new { controller = "Crm", action = "CrmRelTypeAdd" }
            );

            routes.MapRoute(
                "CrmRelTypeEditRoute",
                "CrmRelTypeEdit",
                new { controller = "Crm", action = "CrmRelTypeEdit" }
            );

            routes.MapRoute(
                "CrmRelTypeDelRoute",
                "CrmRelTypeDel",
                new { controller = "Crm", action = "CrmRelTypeDel" }
            );

            #endregion

            #region CrmScheduler
            
            routes.MapRoute(
                "TaskSchedulerRoute",
                "TaskScheduler",
                new { controller = "Crm", action = "TaskScheduler" }
            );

            routes.MapRoute(
                "GetCrmSchedulerListRoute",
                "GetCrmSchedulerList",
                new { controller = "Crm", action = "GetCrmSchedulerList" }
            );

            routes.MapRoute(
                "CrmSchedulerDelRoute",
                "CrmSchedulerDel",
                new { controller = "Crm", action = "CrmSchedulerDel" }
            );

            routes.MapRoute(
                "CrmSchedulerAddRoute",
                "CrmSchedulerAdd",
                new { controller = "Crm", action = "CrmSchedulerAdd" }
            );

            routes.MapRoute(
                "CrmSchedulerEditRoute",
                "CrmSchedulerEdit",
                new { controller = "Crm", action = "CrmSchedulerEdit" }
            );

            routes.MapRoute(
                "GetExecUserListRoute",
                "GetExecUserList",
                new { controller = "Crm", action = "GetExecUserList" }
            );

            routes.MapRoute(
                "TaskSchedulerExportRoute",
                "TaskSchedulerExport",
                new { controller = "Crm", action = "TaskSchedulerExport" }
            );
            

            #endregion

            #region CrmUserRole

            routes.MapRoute(
                "GetCrmUserRoleListRoute",
                "GetCrmUserRoleList",
                new { controller = "Crm", action = "GetCrmUserRoleList" }
            );

            routes.MapRoute(
                "CrmUserRoleDelRoute",
                "CrmUserRoleDel",
                new { controller = "Crm", action = "CrmUserRoleDel" }
            );

            routes.MapRoute(
                "CrmUserRoleAddRoute",
                "CrmUserRoleAdd",
                new { controller = "Crm", action = "CrmUserRoleAdd" }
            );

            routes.MapRoute(
                "CrmUserRoleEditRoute",
                "CrmUserRoleEdit",
                new { controller = "Crm", action = "CrmUserRoleEdit" }
            );

            #endregion

            #region CrmUserRoleStateDone

            routes.MapRoute(
                "GetCrmUserRoleStateDoneListRoute",
                "GetCrmUserRoleStateDoneList",
                new { controller = "Crm", action = "GetCrmUserRoleStateDoneList" }
            );

            routes.MapRoute(
                "CrmUserRoleStateDoneAddRoute",
                "CrmUserRoleStateDoneAdd",
                new { controller = "Crm", action = "CrmUserRoleStateDoneAdd" }
            );

            routes.MapRoute(
                "CrmUserRoleStateDoneEditRoute",
                "CrmUserRoleStateDoneEdit",
                new { controller = "Crm", action = "CrmUserRoleStateDoneEdit" }
            );
            
            routes.MapRoute(
                "CrmUserRoleStateDoneDelRoute",
                "CrmUserRoleStateDoneDel",
                new { controller = "Crm", action = "CrmUserRoleStateDoneDel" }
            );

            #endregion

            #region TaskFile

            routes.MapRoute(
                "GetCrmTaskFileListRoute",
                "GetCrmTaskFileList",
                new { controller = "Crm", action = "GetCrmTaskFileList" }
            );

            routes.MapRoute(
                "GetCrmTaskFileList_forSubtaskRoute",
                "GetCrmTaskFileList_forSubtask",
                new { controller = "Crm", action = "GetCrmTaskFileList_forSubtask" }
            );

            routes.MapRoute(
                "TaskFileSaveRoute",
                "TaskFileSave",
                new { controller = "Crm", action = "TaskFileSave" }
            );

            routes.MapRoute(
                "TaskFileRemoveRoute",
                "TaskFileRemove",
                new { controller = "Crm", action = "TaskFileRemove" }
            );
            
            #endregion

            #region Notify

            routes.MapRoute(
                "TaskNotifyRoute",
                "TaskNotify",
                new { controller = "Crm", action = "TaskNotify" }
            );

            routes.MapRoute(
                "GetCrmNotifyParamUserListRoute",
                "GetCrmNotifyParamUserList",
                new { controller = "Crm", action = "GetCrmNotifyParamUserList" }
            );

            routes.MapRoute(
                "NotifyParamUserEditRoute",
                "NotifyParamUserEdit",
                new { controller = "Crm", action = "NotifyParamUserEdit" }
            );

            routes.MapRoute(
                "GetCrmNotifyParamAttrListRoute",
                "GetCrmNotifyParamAttrList",
                new { controller = "Crm", action = "GetCrmNotifyParamAttrList" }
            );

            routes.MapRoute(
                "NotifyParamAttrEditRoute",
                "NotifyParamAttrEdit",
                new { controller = "Crm", action = "NotifyParamAttrEdit" }
            );

            routes.MapRoute(
                "GetCrmNotifyParamContentListRoute",
                "GetCrmNotifyParamContentList",
                new { controller = "Crm", action = "GetCrmNotifyParamContentList" }
            );

            routes.MapRoute(
                "NotifyParamContentEditRoute",
                "NotifyParamContentEdit",
                new { controller = "Crm", action = "NotifyParamContentEdit" }
            );

            routes.MapRoute(
                "GetCrmNotifyParamTransportListRoute",
                "GetCrmNotifyParamTransportList",
                new { controller = "Crm", action = "GetCrmNotifyParamTransportList" }
            );

            routes.MapRoute(
                "NotifyParamTransportEditRoute",
                "NotifyParamTransportEdit",
                new { controller = "Crm", action = "NotifyParamTransportEdit" }
            );

            #endregion

            #region Report
            
            routes.MapRoute(
                "GetReport1Route",
                "GetReport1",
                new { controller = "Crm", action = "GetReport1" }
            );

            #endregion
            
            #region CrmBatch

            routes.MapRoute(
                "GetCrmBatchListRoute",
                "GetCrmBatchList",
                new { controller = "Crm", action = "GetCrmBatchList" }
            );

            routes.MapRoute(
                "CrmBatchAddRoute",
                "CrmBatchAdd",
                new { controller = "Crm", action = "CrmBatchAdd" }
            );

            routes.MapRoute(
                "CrmBatchEditRoute",
                "CrmBatchEdit",
                new { controller = "Crm", action = "CrmBatchEdit" }
            );

            routes.MapRoute(
                "CrmBatchDelRoute",
                "CrmBatchDel",
                new { controller = "Crm", action = "CrmBatchDel" }
            );

            #endregion

            routes.MapRoute(
                "TaskList1Route",
                "TaskList1",
                new { controller = "Crm", action = "TaskList1" }
            );

            routes.MapRoute(
                "TaskList2Route",
                "TaskList2",
                new { controller = "Crm", action = "TaskList2" }
            );

            routes.MapRoute(
                "TaskListPartialRoute",
                "TaskListPartial",
                new { controller = "Crm", action = "TaskListPartial" }
            );

            routes.MapRoute(
                "TaskListGridConfigSaveRoute",
                "TaskListGridConfigSave",
                new { controller = "Crm", action = "TaskListGridConfigSave" }
            );

            routes.MapRoute(
                "TaskListGridConfigLoadRoute",
                "TaskListGridConfigLoad",
                new { controller = "Crm", action = "TaskListGridConfigLoad" }
            );

            #endregion

            #region SysController

            routes.MapRoute(
                "CabGridColumnUserSettingsRoute",
                "CabGridColumnUserSettings",
                new { controller = "Sys", action = "CabGridColumnUserSettings" }
            );

            routes.MapRoute(
                "CabGridUserSettingsRoute",
                "CabGridUserSettings",
                new { controller = "Sys", action = "CabGridUserSettings" }
            );

            routes.MapRoute(
                "GridConfigSaveRoute",
                "GridConfigSave",
                new { controller = "Sys", action = "GridConfigSave" }
            );

            routes.MapRoute(
                "GridConfigLoadRoute",
                "GridConfigLoad",
                new { controller = "Sys", action = "GridConfigLoad" }
            );

            #endregion

            #region AuthController

            routes.MapRoute(
                "AuthListRoute",
                "AuthList",
                new { controller = "Auth", action = "AuthList" }
            );

            routes.MapRoute(
                "GetAuthListRoute",
                "GetAuthList",
                new { controller = "Auth", action = "GetAuthList" }
            );

            routes.MapRoute(
                "AuthRoleListRoute",
                "AuthRoleList",
                new { controller = "Auth", action = "AuthRoleList" }
            );  


            routes.MapRoute(
                "GetAuthRoleListRoute",
                "GetAuthRoleList",
                new { controller = "Auth", action = "GetAuthRoleList" }
            );


            routes.MapRoute(
                "AuthRoleEditRoute",
                "AuthRoleEdit",
                new { controller = "Auth", action = "AuthRoleEdit" }
            );

            routes.MapRoute(
                "AuthRoleAddRoute",
                "AuthRoleAdd",
                new { controller = "Auth", action = "AuthRoleAdd" }
            );

            routes.MapRoute(
                "AuthRoleDelRoute",
                "AuthRoleDel",
                new { controller = "Auth", action = "AuthRoleDel" }
            );

            routes.MapRoute(
                "AuthRolePermEditRoute",
                "AuthRolePermEdit",
                new { controller = "Auth", action = "AuthRolePermEdit" }
            );

            routes.MapRoute(
                "GetAuthRolePartListRoute",
                "GetAuthRolePartList",
                new { controller = "Auth", action = "GetAuthRolePartList" }
            );

            routes.MapRoute(
                "GetAuthRoleSectionListRoute",
                "GetAuthRoleSectionList",
                new { controller = "Auth", action = "GetAuthRoleSectionList" }
            );

            routes.MapRoute(
                "AuthUserPermEditRoute",
                "AuthUserPermEdit",
                new { controller = "Auth", action = "AuthUserPermEdit" }
            );

            routes.MapRoute(
                "GetAuthUserRoleSectionListRoute",
                "GetAuthUserRoleSectionList",
                new { controller = "Auth", action = "GetAuthUserRoleSectionList" }
            );

            routes.MapRoute(
                "GetAuthUserRolePartListRoute",
                "GetAuthUserRolePartList",
                new { controller = "Auth", action = "GetAuthUserRolePartList" }
            );

            routes.MapRoute(
                "AuthClientListRoute",
                "AuthClientList",
                new { controller = "Auth", action = "AuthClientList" }
            );

            routes.MapRoute(
                "GetAuthClientListRoute",
                "GetAuthClientList",
                new { controller = "Auth", action = "GetAuthClientList" }
            );
            
            #endregion

            #region StorageController

            #region StorageTree

            routes.MapRoute(
                "StorageTreeRoute",
                "StorageTree",
                new { controller = "Storage", action = "StorageTree" }
            );

            #endregion

            #region StorageFolderTree

            routes.MapRoute(
                "GetStorageFolderTreeRoute",
                "GetStorageFolderTree",
                new { controller = "Storage", action = "GetStorageFolderTree" }
            );

            routes.MapRoute(
                "StorageFolderTreeMoveRoute",
                "StorageFolderTreeMove",
                new { controller = "Storage", action = "StorageFolderTreeMove" }
            );

            routes.MapRoute(
                "StorageFolderTreeAddRoute",
                "StorageFolderTreeAdd",
                new { controller = "Storage", action = "StorageFolderTreeAdd" }
            );

            routes.MapRoute(
                "StorageFolderTreeEditRoute",
                "StorageFolderTreeEdit",
                new { controller = "Storage", action = "StorageFolderTreeEdit" }
            );

            routes.MapRoute(
                "StorageFolderTreeDownloadRoute",
                "StorageFolderTreeDownload",
                new { controller = "Storage", action = "StorageFolderTreeDownload" }
            );

            routes.MapRoute(
                "StorageFolderTreeRoute",
                "StorageFolderTree",
                new { controller = "Storage", action = "StorageFolderTree" }
            );
            
            #endregion

            #region StorageFileTree

            routes.MapRoute(
                "GetStorageFileTreeRoute",
                "GetStorageFileTree",
                new { controller = "Storage", action = "GetStorageFileTree" }
            );

            routes.MapRoute(
                "StorageFileTreeSaveRoute",
                "StorageFileTreeSave",
                new { controller = "Storage", action = "StorageFileTreeSave" }
            );

            routes.MapRoute(
                "StorageFileTreeMoveRoute",
                "StorageFileTreeMove",
                new { controller = "Storage", action = "StorageFileTreeMove" }
            );

            routes.MapRoute(
                "StorageFileTreeEditRoute",
                "StorageFileTreeEdit",
                new { controller = "Storage", action = "StorageFileTreeEdit" }
            );

            routes.MapRoute(
                "StorageFileTreeDelRoute",
                "StorageFileTreeDel",
                new { controller = "Storage", action = "StorageFileTreeDel" }
            );

            routes.MapRoute(
                "StorageFileTreeCheckRoute",
                "StorageFileTreeCheck",
                new { controller = "Storage", action = "StorageFileTreeCheck" }
            );

            routes.MapRoute(
                "StorageFileSearchRoute",
                "StorageFileSearch",
                new { controller = "Storage", action = "StorageFileSearch" }
            );

            routes.MapRoute(
                "StorageFileGetRoute",
                "StorageFileGet",
                new { controller = "Storage", action = "StorageFileGet" }
            );
            
            #endregion

            #region StorageLog

            routes.MapRoute(
                "StorageLogRoute",
                "StorageLog",
                new { controller = "Storage", action = "StorageLog" }
            );

            routes.MapRoute(
                "GetStorageLogListRoute",
                "GetStorageLogList",
                new { controller = "Storage", action = "GetStorageLogList" }
            );

            #endregion
            
            #region StorageParam

            routes.MapRoute(
                "StorageParamRoute",
                "StorageParam",
                new { controller = "Storage", action = "StorageParam" }
            );

            routes.MapRoute(
                "StorageParamSaveRoute",
                "StorageParamSave",
                new { controller = "Storage", action = "StorageParamSave" }
            );

            #endregion
            
            #region StorageFileType

            routes.MapRoute(
                "GetStorageFileTypeListRoute",
                "GetStorageFileTypeList",
                new { controller = "Storage", action = "GetStorageFileTypeList" }
            );

            routes.MapRoute(
                "StorageFileTypeAddRoute",
                "StorageFileTypeAdd",
                new { controller = "Storage", action = "StorageFileTypeAdd" }
            );

            routes.MapRoute(
                "StorageFileTypeEditRoute",
                "StorageFileTypeEdit",
                new { controller = "Storage", action = "StorageFileTypeEdit" }
            );

            routes.MapRoute(
                "StorageFileTypeDelRoute",
                "StorageFileTypeDel",
                new { controller = "Storage", action = "StorageFileTypeDel" }
            );

            #endregion

            #region StorageLink

            routes.MapRoute(
                "StorageLinkAddRoute",
                "StorageLinkAdd",
                new { controller = "Storage", action = "StorageLinkAdd" }
            );

            #endregion            

            #region StorageTrash
            
            routes.MapRoute(
                "GetStorageTrashListRoute",
                "GetStorageTrashList",
                new { controller = "Storage", action = "GetStorageTrashList" }
            );

            routes.MapRoute(
                "StorageTrashClearRoute",
                "StorageTrashClear",
                new { controller = "Storage", action = "StorageTrashClear" }
            );

            routes.MapRoute(
                "StorageTrashRestoreRoute",
                "StorageTrashRestore",
                new { controller = "Storage", action = "StorageTrashRestore" }
            );           
            
            #endregion

            #endregion

            #region HelpController

            routes.MapRoute(
                "CabHelpRoute",
                "CabHelp",
                new { controller = "Help", action = "CabHelp" }
            );

            routes.MapRoute(
                "CabHelpPartRoute",
                "CabHelpPart",
                new { controller = "Help", action = "CabHelpPart" }
            );

            routes.MapRoute(
                "CabHelpPartialRoute",
                "CabHelpPartial",
                new { controller = "Help", action = "CabHelpPartial" }
            );

            routes.MapRoute(
                "GetCabHelpTagsRoute",
                "GetCabHelpTags",
                new { controller = "Help", action = "GetCabHelpTags" }
            );

            routes.MapRoute(
                "CabHelpSearchRoute",
                "CabHelpSearch",
                new { controller = "Help", action = "CabHelpSearch" }
            );

            routes.MapRoute(
                "MainHelpPartialRoute",
                "MainHelpPartial",
                new { controller = "Help", action = "MainHelpPartial" }
            );

            routes.MapRoute(
                "ServiceHelpPartialRoute",
                "ServiceHelpPartial",
                new { controller = "Help", action = "ServiceHelpPartial" }
            );

            routes.MapRoute(
                "ServHelpPartialRoute",
                "ServHelpPartial",
                new { controller = "Help", action = "ServHelpPartial" }
            );

            routes.MapRoute(
                "ClientHelpPartialRoute",
                "ClientHelpPartial",
                new { controller = "Help", action = "ClientHelpPartial" }
            );

            routes.MapRoute(
                "AdminHelpPartialRoute",
                "AdminHelpPartial",
                new { controller = "Help", action = "AdminHelpPartial" }
            );

            routes.MapRoute(
                "MyPagesHelpPartialRoute",
                "MyPagesHelpPartial",
                new { controller = "Help", action = "MyPagesHelpPartial" }
            );

            routes.MapRoute(
                "UserMenuHelpPartialRoute",
                "UserMenuHelpPartial",
                new { controller = "Help", action = "UserMenuHelpPartial" }
            );

            routes.MapRoute(
                "UserProfileHelpPartialRoute",
                "UserProfileHelpPartial",
                new { controller = "Help", action = "UserProfileHelpPartial" }
            );

            routes.MapRoute(
                "DiscountHelpPartialRoute",
                "DiscountHelpPartial",
                new { controller = "Help", action = "DiscountHelpPartial" }
            );

            routes.MapRoute(
                "ExchangeHelpPartialRoute",
                "ExchangeHelpPartial",
                new { controller = "Help", action = "ExchangeHelpPartial" }
            );

            routes.MapRoute(
                "DefectHelpPartialRoute",
                "DefectHelpPartial",
                new { controller = "Help", action = "DefectHelpPartial" }
            );

            routes.MapRoute(
                "CvaHelpPartialRoute",
                "CvaHelpPartial",
                new { controller = "Help", action = "CvaHelpPartial" }
            );

            routes.MapRoute(
                "ClientListHelpPartialRoute",
                "ClientListHelpPartial",
                new { controller = "Help", action = "ClientListHelpPartial" }
            );

            routes.MapRoute(
                "RegHelpPartialRoute",
                "RegHelpPartial",
                new { controller = "Help", action = "RegHelpPartial" }
            );

            routes.MapRoute(
                "CabSpravHelpPartialRoute",
                "CabSpravHelpPartial",
                new { controller = "Help", action = "CabSpravHelpPartial" }
            );

            routes.MapRoute(
                "DeliveryHelpPartialRoute",
                "DeliveryHelpPartial",
                new { controller = "Help", action = "DeliveryHelpPartial" }
            );

            routes.MapRoute(
                "HelpDeskHelpPartialRoute",
                "HelpDeskHelpPartial",
                new { controller = "Help", action = "HelpDeskHelpPartial" }
            );

            routes.MapRoute(
                "CrmTaskHelpPartialRoute",
                "CrmTaskHelpPartial",
                new { controller = "Help", action = "CrmTaskHelpPartial" }
            );

            routes.MapRoute(
                "StorageHelpPartialRoute",
                "StorageHelpPartial",
                new { controller = "Help", action = "StorageHelpPartial" }
            );

            routes.MapRoute(
                "LogHelpPartialRoute",
                "LogHelpPartial",
                new { controller = "Help", action = "LogHelpPartial" }
            );

            routes.MapRoute(
                "AuthHelpPartialRoute",
                "AuthHelpPartial",
                new { controller = "Help", action = "AuthHelpPartial" }
            );
            
            routes.MapRoute(
                "ChangePasswordHelpPartialRoute",
                "ChangePasswordHelpPartial",
                new { controller = "Help", action = "ChangePasswordHelpPartial" }
            );

            routes.MapRoute(
                "LogoutHelpPartialRoute",
                "LogoutHelpPartial",
                new { controller = "Help", action = "LogoutHelpPartial" }
            );

            #endregion
            
            #region Update
            
            routes.MapRoute(
                "UpdateSoftListRoute",
                "UpdateSoftList",
                new { controller = "Update", action = "UpdateSoftList" }
            );

            routes.MapRoute(
                "GetUpdateSoftListRoute",
                "GetUpdateSoftList",
                new { controller = "Update", action = "GetUpdateSoftList" }
            );

            routes.MapRoute(
                "UpdateSoftAddRoute",
                "UpdateSoftAdd",
                new { controller = "Update", action = "UpdateSoftAdd" }
            );

            routes.MapRoute(
                "UpdateSoftEditRoute",
                "UpdateSoftEdit",
                new { controller = "Update", action = "UpdateSoftEdit" }
            );

            routes.MapRoute(
                "UpdateSoftDelRoute",
                "UpdateSoftDel",
                new { controller = "Update", action = "UpdateSoftDel" }
            );

            routes.MapRoute(
                "GetUpdateSoftVersionListRoute",
                "GetUpdateSoftVersionList",
                new { controller = "Update", action = "GetUpdateSoftVersionList" }
            );

            routes.MapRoute(
                "UpdateSoftVersionAddRoute",
                "UpdateSoftVersionAdd",
                new { controller = "Update", action = "UpdateSoftVersionAdd" }
            );

            routes.MapRoute(
                "UpdateSoftVersionEditRoute",
                "UpdateSoftVersionEdit",
                new { controller = "Update", action = "UpdateSoftVersionEdit" }
            );

            routes.MapRoute(
                "UpdateSoftVersionDelRoute",
                "UpdateSoftVersionDel",
                new { controller = "Update", action = "UpdateSoftVersionDel" }
            );

            routes.MapRoute(
                "UpdateFileUploadRoute",
                "UpdateFileUpload",
                new { controller = "Update", action = "UpdateFileUpload" }
            );

            routes.MapRoute(
                "GetUpdateBatchListRoute",
                "GetUpdateBatchList",
                new { controller = "Update", action = "GetUpdateBatchList" }
            );

            routes.MapRoute(
                "UpdateBatchListRoute",
                "UpdateBatchList",
                new { controller = "Update", action = "UpdateBatchList" }
            );

            routes.MapRoute(
                "UpdateBatchEditRoute",
                "UpdateBatchEdit",
                new { controller = "Update", action = "UpdateBatchEdit" }
            );

            routes.MapRoute(
                "UpdateBatchDelRoute",
                "UpdateBatchDel",
                new { controller = "Update", action = "UpdateBatchDel" }
            );

            routes.MapRoute(
                "GetUpdateBatchItemListRoute",
                "GetUpdateBatchItemList",
                new { controller = "Update", action = "GetUpdateBatchItemList" }
            );

            #endregion
            
            #region Prj

 
            routes.MapRoute(
                "PrjListRoute",
                "PrjList",
                new { controller = "Prj", action = "PrjList" }
            );

            routes.MapRoute(
                "GetPrjListRoute",
                "GetPrjList",
                new { controller = "Prj", action = "GetPrjList" }
            );

            routes.MapRoute(
                "PrjClaimListPartialRoute",
                "PrjClaimListPartial",
                new { controller = "Prj", action = "PrjClaimListPartial" }
            );

            routes.MapRoute(
                "GetPrjClaimListRoute",
                "GetPrjClaimList",
                new { controller = "Prj", action = "GetPrjClaimList" }
            );
            
            routes.MapRoute(
                "GetPrjClaimList_byPrjRoute",
                "GetPrjClaimList_byPrj",
                new { controller = "Prj", action = "GetPrjClaimList_byPrj" }
            );

            routes.MapRoute(
                "PrjClaimEditPartialRoute",
                "PrjClaimEditPartial",
                new { controller = "Prj", action = "PrjClaimEditPartial" }
            );

            routes.MapRoute(
                "PrjEditPartialRoute",
                "PrjEditPartial",
                new { controller = "Prj", action = "PrjEditPartial" }
            );

            /*
            routes.MapRoute(
                "PrjTaskListPartialRoute",
                "PrjTaskListPartial",
                new { controller = "Prj", action = "PrjTaskListPartial" }
            );
            */

            routes.MapRoute(
                "GetPrjTaskListRoute",
                "GetPrjTaskList",
                new { controller = "Prj", action = "GetPrjTaskList" }
            );

            routes.MapRoute(
                "GetPrjTaskList_byClaimRoute",
                "GetPrjTaskList_byClaim",
                new { controller = "Prj", action = "GetPrjTaskList_byClaim" }
            );

            routes.MapRoute(
                "PrjWorkListPartialRoute",
                "PrjWorkListPartial",
                new { controller = "Prj", action = "PrjWorkListPartial" }
            );

            routes.MapRoute(
                "GetPrjWorkListRoute",
                "GetPrjWorkList",
                new { controller = "Prj", action = "GetPrjWorkList" }
            );

            routes.MapRoute(
                "GetPrjWorkList_byWorkRoute",
                "GetPrjWorkList_byWork",
                new { controller = "Prj", action = "GetPrjWorkList_byWork" }
            );

            routes.MapRoute(
                "GetPrjWorkList_byTaskRoute",
                "GetPrjWorkList_byTask",
                new { controller = "Prj", action = "GetPrjWorkList_byTask" }
            );

            routes.MapRoute(
                "GetPrjWorkList_byClaimRoute",
                "GetPrjWorkList_byClaim",
                new { controller = "Prj", action = "GetPrjWorkList_byClaim" }
            );

            routes.MapRoute(
                "GetPrjMessList_byClaimRoute",
                "GetPrjMessList_byClaim",
                new { controller = "Prj", action = "GetPrjMessList_byClaim" }
            );

            routes.MapRoute(
                "GetPrjMessList_forTask_byClaimRoute",
                "GetPrjMessList_forTask_byClaim",
                new { controller = "Prj", action = "GetPrjMessList_forTask_byClaim" }
            );            
            
            routes.MapRoute(
                "GetPrjMessList_byTaskRoute",
                "GetPrjMessList_byTask",
                new { controller = "Prj", action = "GetPrjMessList_byTask" }
            );

            routes.MapRoute(
                "GetPrjMessList_byWorkRoute",
                "GetPrjMessList_byWork",
                new { controller = "Prj", action = "GetPrjMessList_byWork" }
            );

            routes.MapRoute(
                "GetPrjMessList_byClaimOrTaskRoute",
                "GetPrjMessList_byClaimOrTask",
                new { controller = "Prj", action = "GetPrjMessList_byClaimOrTask" }
            );            

            routes.MapRoute(
                "PrjUserSettingsSaveRoute",
                "PrjUserSettingsSave",
                new { controller = "Prj", action = "PrjUserSettingsSave" }
            );

            routes.MapRoute(
                "GetPrjUserSettingsListRoute",
                "GetPrjUserSettingsList",
                new { controller = "Prj", action = "GetPrjUserSettingsList" }
            );

            routes.MapRoute(
                "PrjPlanPartialRoute",
                "PrjPlanPartial",
                new { controller = "Prj", action = "PrjPlanPartial" }
            );

            routes.MapRoute(
                "GetPrjPlanListRoute",
                "GetPrjPlanList",
                new { controller = "Prj", action = "GetPrjPlanList" }
            );

            routes.MapRoute(
                "PrjPlanAddRoute",
                "PrjPlanAdd",
                new { controller = "Prj", action = "PrjPlanAdd" }
            );

            routes.MapRoute(
                "PrjPlanEditRoute",
                "PrjPlanEdit",
                new { controller = "Prj", action = "PrjPlanEdit" }
            );

            routes.MapRoute(
                "PrjPlanDelRoute",
                "PrjPlanDel",
                new { controller = "Prj", action = "PrjPlanDel" }
            );

            /*
            routes.MapRoute(
                "PrjNewsPartialRoute",
                "PrjNewsPartial",
                new { controller = "Prj", action = "PrjNewsPartial" }
            );
            */

            routes.MapRoute(
                "GetPrjNewsClaimListRoute",
                "GetPrjNewsClaimList",
                new { controller = "Prj", action = "GetPrjNewsClaimList" }
            );

            routes.MapRoute(
                "GetPrjNewsTaskListRoute",
                "GetPrjNewsTaskList",
                new { controller = "Prj", action = "GetPrjNewsTaskList" }
            );


            routes.MapRoute(
                "GetPrjNewsMessListRoute",
                "GetPrjNewsMessList",
                new { controller = "Prj", action = "GetPrjNewsMessList" }
            );

            routes.MapRoute(
                "GetPrjNewsLogListRoute",
                "GetPrjNewsLogList",
                new { controller = "Prj", action = "GetPrjNewsLogList" }
            );

            routes.MapRoute(
                "PrjSettingsPartialRoute",
                "PrjSettingsPartial",
                new { controller = "Prj", action = "PrjSettingsPartial" }
            );

            routes.MapRoute(
                "PrjBinPartialRoute",
                "PrjBinPartial",
                new { controller = "Prj", action = "PrjBinPartial" }
            );

            routes.MapRoute(
                "PrjSpravPartialRoute",
                "PrjSpravPartial",
                new { controller = "Prj", action = "PrjSpravPartial" }
            );

            routes.MapRoute(
                "PrjLogPartialRoute",
                "PrjLogPartial",
                new { controller = "Prj", action = "PrjLogPartial" }
            );
                        
            routes.MapRoute(
                "GetPrjLogListRoute",
                "GetPrjLogList",
                new { controller = "Prj", action = "GetPrjLogList" }
            );

            routes.MapRoute(
                "PrjClaimChangePrjRoute",
                "PrjClaimChangePrj",
                new { controller = "Prj", action = "PrjClaimChangePrj" }
            );

            routes.MapRoute(
                "PrjFileSaveRoute",
                "PrjFileSave",
                new { controller = "Prj", action = "PrjFileSave" }
            );

            routes.MapRoute(
                "PrjFileRemoveRoute",
                "PrjFileRemove",
                new { controller = "Prj", action = "PrjFileRemove" }
            );

            routes.MapRoute(
                "PrjClaimFavorAddRoute",
                "PrjClaimFavorAdd",
                new { controller = "Prj", action = "PrjClaimFavorAdd" }
            );

            routes.MapRoute(
                "PrjClaimFavorDelRoute",
                "PrjClaimFavorDel",
                new { controller = "Prj", action = "PrjClaimFavorDel" }
            );

            routes.MapRoute(
                 "PrjClaimDelRoute",
                 "PrjClaimDel",
                 new { controller = "Prj", action = "PrjClaimDel" }
             );

            routes.MapRoute(
                 "PrjClaimRestoreRoute",
                 "PrjClaimRestore",
                 new { controller = "Prj", action = "PrjClaimRestore" }
             );
            
            routes.MapRoute(
                "PrjNotebookPartialRoute",
                "PrjNotebookPartial",
                new { controller = "Prj", action = "PrjNotebookPartial" }
            );
            
            routes.MapRoute(
                "PrjUserGroupAddRoute",
                "PrjUserGroupAdd",
                new { controller = "Prj", action = "PrjUserGroupAdd" }
            );

            routes.MapRoute(
                "PrjUserGroupDelRoute",
                "PrjUserGroupDel",
                new { controller = "Prj", action = "PrjUserGroupDel" }
            );

            routes.MapRoute(
                "GetPrjClaimList_FavorRoute",
                "GetPrjClaimList_Favor",
                new { controller = "Prj", action = "GetPrjClaimList_Favor" }
            );
           
            routes.MapRoute(
                "PrjBriefPartialRoute",
                "PrjBriefPartial",
                new { controller = "Prj", action = "PrjBriefPartial" }
            );

            routes.MapRoute(
                "GetPrjBriefListRoute",
                "GetPrjBriefList",
                new { controller = "Prj", action = "GetPrjBriefList" }
            );
            
            routes.MapRoute(
                "GetPrjUserGroupItemListRoute",
                "GetPrjUserGroupItemList",
                new { controller = "Prj", action = "GetPrjUserGroupItemList" }
            );

            routes.MapRoute(
                "PrjUserSettingsChangeRoute",
                "PrjUserSettingsChange",
                new { controller = "Prj", action = "PrjUserSettingsChange" }
            );            

            routes.MapRoute(
                "PrjDelRoute",
                "PrjDel",
                new { controller = "Prj", action = "PrjDel" }
            );

            routes.MapRoute(
                "PrjRestoreRoute",
                "PrjRestore",
                new { controller = "Prj", action = "PrjRestore" }
            );
                        
            routes.MapRoute(
                "PrjNoteEditRoute",
                "PrjNoteEdit",
                new { controller = "Prj", action = "PrjNoteEdit" }
            );

            routes.MapRoute(
                "PrjNoteDelRoute",
                "PrjNoteDel",
                new { controller = "Prj", action = "PrjNoteDel" }
            );
            
            routes.MapRoute(
                "PrjNoteReorderRoute",
                "PrjNoteReorder",
                new { controller = "Prj", action = "PrjNoteReorder" }
            );

            routes.MapRoute(
                "PrjTaskUserActionRoute",
                "PrjTaskUserAction",
                new { controller = "Prj", action = "PrjTaskUserAction" }
            );
            
            routes.MapRoute(
                "PrjTaskSimpleAddRoute",
                "PrjTaskSimpleAdd",
                new { controller = "Prj", action = "PrjTaskSimpleAdd" }
            );

            routes.MapRoute(
                "PrjWorkTypeEditRoute",
                "PrjWorkTypeEdit",
                new { controller = "Prj", action = "PrjWorkTypeEdit" }
            );
            
            routes.MapRoute(
                "PrjWorkUserEditRoute",
                "PrjWorkUserEdit",
                new { controller = "Prj", action = "PrjWorkUserEdit" }
            );

            routes.MapRoute(
                "GetPrjClaimTypeListRoute",
                "GetPrjClaimTypeList",
                new { controller = "Prj", action = "GetPrjClaimTypeList" }
            );

            routes.MapRoute(
                "PrjClaimTypeEditRoute",
                "PrjClaimTypeEdit",
                new { controller = "Prj", action = "PrjClaimTypeEdit" }
            );

            routes.MapRoute(
                "GetPrjProjectVersionListRoute",
                "GetPrjProjectVersionList",
                new { controller = "Prj", action = "GetPrjProjectVersionList" }
            );

            routes.MapRoute(
                "PrjProjectVersionEditRoute",
                "PrjProjectVersionEdit",
                new { controller = "Prj", action = "PrjProjectVersionEdit" }
            );

            routes.MapRoute(
                "PrjAddRoute",
                "PrjAdd",
                new { controller = "Prj", action = "PrjAdd" }
            );

            routes.MapRoute(
                "PrjEditRoute",
                "PrjEdit",
                new { controller = "Prj", action = "PrjEdit" }
            );

            routes.MapRoute(
                "PrjDestroyRoute",
                "PrjDestroy",
                new { controller = "Prj", action = "PrjDestroy" }
            );

            routes.MapRoute(
                "PrjProjectVersionNewReleaseRoute",
                "PrjProjectVersionNewRelease",
                new { controller = "Prj", action = "PrjProjectVersionNewRelease" }
            );
            
            routes.MapRoute(
                "GetPrjClaimStageListRoute",
                "GetPrjClaimStageList",
                new { controller = "Prj", action = "GetPrjClaimStageList" }
            );

            routes.MapRoute(
                "PrjClaimStageAddRoute",
                "PrjClaimStageAdd",
                new { controller = "Prj", action = "PrjClaimStageAdd" }
            );

            routes.MapRoute(
                "PrjClaimStageEditRoute",
                "PrjClaimStageEdit",
                new { controller = "Prj", action = "PrjClaimStageEdit" }
            );

            routes.MapRoute(
                "PrjClaimStageDelRoute",
                "PrjClaimStageDel",
                new { controller = "Prj", action = "PrjClaimStageDel" }
            );
            
            routes.MapRoute(
                "PrjClaimFilterEditPartialRoute",
                "PrjClaimFilterEditPartial",
                new { controller = "Prj", action = "PrjClaimFilterEditPartial" }
            );
            
            routes.MapRoute(
                "PrjClaimFilterEditRoute",
                "PrjClaimFilterEdit",
                new { controller = "Prj", action = "PrjClaimFilterEdit" }
            );
            
            routes.MapRoute(
                "GetPrjClaimFilterListRoute",
                "GetPrjClaimFilterList",
                new { controller = "Prj", action = "GetPrjClaimFilterList" }
            );
            
            routes.MapRoute(
                "PrjClaimFilterParamsRoute",
                "PrjClaimFilterParams",
                new { controller = "Prj", action = "PrjClaimFilterParams" }
            );

            routes.MapRoute(
                "PrjClaimFilterItemsEditRoute",
                "PrjClaimFilterItemsEdit",
                new { controller = "Prj", action = "PrjClaimFilterItemsEdit" }
            );
            
            routes.MapRoute(
                "PrjClaimFilterDelRoute",
                "PrjClaimFilterDel",
                new { controller = "Prj", action = "PrjClaimFilterDel" }
            );
            
            routes.MapRoute(
                "PrjFileGetRoute",
                "PrjFileGet",
                new { controller = "Prj", action = "PrjFileGet" }
            );
            
            routes.MapRoute(
                "GetPrjClaimArchiveListRoute",
                "GetPrjClaimArchiveList",
                new { controller = "Prj", action = "GetPrjClaimArchiveList" }
            );
            
            routes.MapRoute(
                "PrjClaimReport1Route",
                "PrjClaimReport1",
                new { controller = "Prj", action = "PrjClaimReport1" }
            );

            #endregion

            #region Stock

            routes.MapRoute(
                "StockListRoute",
                "StockList",
                new { controller = "Stock", action = "StockList" }
            );

            routes.MapRoute(
                "GetStockListRoute",
                "GetStockList",
                new { controller = "Stock", action = "GetStockList" }
            );
  
            routes.MapRoute(
                "StockLogListRoute",
                "StockLogList",
                new { controller = "Stock", action = "StockLogList" }
            );

            routes.MapRoute(
                "GetStockUploadLogListRoute",
                "GetStockUploadLogList",
                new { controller = "Stock", action = "GetStockUploadLogList" }
            );

            routes.MapRoute(
                "GetStockDownloadLogListRoute",
                "GetStockDownloadLogList",
                new { controller = "Stock", action = "GetStockDownloadLogList" }
            );

            routes.MapRoute(
                "StockTaskListRoute",
                "StockTaskList",
                new { controller = "Stock", action = "StockTaskList" }
            );

            routes.MapRoute(
                "GetStockTaskList_bySalesRoute",
                "GetStockTaskList_bySales",
                new { controller = "Stock", action = "GetStockTaskList_bySales" }
            );
            
            routes.MapRoute(
                "GetStockTaskLcList_bySalesRoute",
                "GetStockTaskLcList",
                new { controller = "Stock", action = "GetStockTaskLcList" }
            );

            routes.MapRoute(
                "StockTaskAddRoute",
                "StockTaskAdd",
                new { controller = "Stock", action = "StockTaskAdd" }
            );
            
            routes.MapRoute(
                "StockTaskDelRoute",
                "StockTaskDel",
                new { controller = "Stock", action = "StockTaskDel" }
            );
            
            routes.MapRoute(
                "GetStockLastListRoute",
                "GetStockLastList",
                new { controller = "Stock", action = "GetStockLastList" }
            );

            routes.MapRoute(
                "StockLastListRoute",
                "StockLastList",
                new { controller = "Stock", action = "StockLastList" }
            );

            #endregion

            #region ASNA

            routes.MapRoute(
                "AsnaUserListRoute",
                "AsnaUserList",
                new { controller = "Asna", action = "AsnaUserList" }
            );
            
            routes.MapRoute(
                "GetAsnaUserListRoute",
                "GetAsnaUserList",
                new { controller = "Asna", action = "GetAsnaUserList" }
            );

            routes.MapRoute(
                "GetAsnaClientListRoute",
                "GetAsnaClientList",
                new { controller = "Asna", action = "GetAsnaClientList" }
            );

            routes.MapRoute(
                "AsnaUserEditRoute",
                "AsnaUserEdit",
                new { controller = "Asna", action = "AsnaUserEdit" }
            );

            routes.MapRoute(
                "AsnaUserDelRoute",
                "AsnaUserDel",
                new { controller = "Asna", action = "AsnaUserDel" }
            );

            routes.MapRoute(
                "GetAsnaTaskListRoute",
                "GetAsnaTaskList",
                new { controller = "Asna", action = "GetAsnaTaskList" }
            );

            routes.MapRoute(
                "GetAsnaTaskList_bySalesRoute",
                "GetAsnaTaskList_bySales",
                new { controller = "Asna", action = "GetAsnaTaskList_bySales" }
            );
            
            routes.MapRoute(
                "AsnaTaskAddRoute",
                "AsnaTaskAdd",
                new { controller = "Asna", action = "AsnaTaskAdd" }
            );

            routes.MapRoute(
                "AsnaTaskEditRoute",
                "AsnaTaskEdit",
                new { controller = "Asna", action = "AsnaTaskEdit" }
            );

            routes.MapRoute(
                "AsnaTaskDelRoute",
                "AsnaTaskDel",
                new { controller = "Asna", action = "AsnaTaskDel" }
            );

            routes.MapRoute(
                "GetAsnaUserLogListRoute",
                "GetAsnaUserLogList",
                new { controller = "Asna", action = "GetAsnaUserLogList" }
            );
            
            routes.MapRoute(
                "AsnaLinkListRoute",
                "AsnaLinkList",
                new { controller = "Asna", action = "AsnaLinkList" }
            );

            routes.MapRoute(
                "GetAsnaLinkListRoute",
                "GetAsnaLinkList",
                new { controller = "Asna", action = "GetAsnaLinkList" }
            );

            routes.MapRoute(
                "GetAsnaSalesListRoute",
                "GetAsnaSalesList",
                new { controller = "Asna", action = "GetAsnaSalesList" }
            );

            routes.MapRoute(
                "AsnaLogListRoute",
                "AsnaLogList",
                new { controller = "Asna", action = "AsnaLogList" }
            );

            routes.MapRoute(
                "GetAsnaLogListRoute",
                "GetAsnaLogList",
                new { controller = "Asna", action = "GetAsnaLogList" }
            );

            routes.MapRoute(
                "AsnaTaskListRoute",
                "AsnaTaskList",
                new { controller = "Asna", action = "AsnaTaskList" }
            );

            routes.MapRoute(
                "AsnaSalesEditRoute",
                "AsnaSalesEdit",
                new { controller = "Asna", action = "AsnaSalesEdit" }
            );

            routes.MapRoute(
                "GetAsnaTaskLcListRoute",
                "GetAsnaTaskLcList",
                new { controller = "Asna", action = "GetAsnaTaskLcList" }
            );

            routes.MapRoute(
                "AsnaStockListRoute",
                "AsnaStockList",
                new { controller = "Asna", action = "AsnaStockList" }
            );

            routes.MapRoute(
                "GetAsnaStockRowListRoute",
                "GetAsnaStockRowList",
                new { controller = "Asna", action = "GetAsnaStockRowList" }
            );

            routes.MapRoute(
                "GetAsnaStockChainListRoute",
                "GetAsnaStockChainList",
                new { controller = "Asna", action = "GetAsnaStockChainList" }
            );

            routes.MapRoute(
                "GetAsnaStockChainDatesRoute",
                "GetAsnaStockChainDates",
                new { controller = "Asna", action = "GetAsnaStockChainDates" }
            );

            routes.MapRoute(
                "AsnaActualListRoute",
                "AsnaActualList",
                new { controller = "Asna", action = "AsnaActualList" }
            );

            routes.MapRoute(
                "GetAsnaActualListRoute",
                "GetAsnaActualList",
                new { controller = "Asna", action = "GetAsnaActualList" }
            );

            routes.MapRoute(
                "AsnaOrderStateTypeRoute",
                "AsnaOrderStateType",
                new { controller = "Asna", action = "AsnaOrderStateType" }
            );

            routes.MapRoute(
                "GetAsnaOrderStateTypeListRoute",
                "GetAsnaOrderStateTypeList",
                new { controller = "Asna", action = "GetAsnaOrderStateTypeList" }
            );

            routes.MapRoute(
                "AsnaOrderListRoute",
                "AsnaOrderList",
                new { controller = "Asna", action = "AsnaOrderList" }
            );

            routes.MapRoute(
                "GetAsnaOrderListRoute",
                "GetAsnaOrderList",
                new { controller = "Asna", action = "GetAsnaOrderList" }
            );

            routes.MapRoute(
                "GetAsnaOrderRowListRoute",
                "GetAsnaOrderRowList",
                new { controller = "Asna", action = "GetAsnaOrderRowList" }
            );
            
            routes.MapRoute(
                "GetAsnaOrderStateListRoute",
                "GetAsnaOrderStateList",
                new { controller = "Asna", action = "GetAsnaOrderStateList" }
            );

            routes.MapRoute(
                "GetAsnaOrderRowStateListRoute",
                "GetAsnaOrderRowStateList",
                new { controller = "Asna", action = "GetAsnaOrderRowStateList" }
            );

            routes.MapRoute(
                "AsnaTaskCheckStateRoute",
                "AsnaTaskCheckState",
                new { controller = "Asna", action = "AsnaTaskCheckState" }
            );

            #endregion

            #region Default

            routes.MapRoute(
                name: "OnlyAction",
                url: "{action}",
                defaults: new { controller = "Home", action = "Index" },
                namespaces: new string[] { "CabinetMvc.Controllers" }
            );            

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new string[] { "CabinetMvc.Controllers" }
            );            

            #endregion
            
            //logger.Info("RegisterRoutes end");
        }
    }
}
