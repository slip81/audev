namespace CabinetMvc.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    using CabinetMvc.Auth;
    using CabinetMvc.ViewModel;
    using CabinetMvc.ViewModel.Adm;
    using CabinetMvc.ViewModel.Discount;
    using CabinetMvc.ViewModel.Exchange;
    using CabinetMvc.ViewModel.Cabinet;
    using CabinetMvc.ViewModel.Defect;
    using CabinetMvc.ViewModel.HelpDesk;
    using CabinetMvc.ViewModel.User;
    using CabinetMvc.ViewModel.Ved;
    using CabinetMvc.ViewModel.Crm;
    using CabinetMvc.ViewModel.Sys;
    using CabinetMvc.ViewModel.Auth;
    using CabinetMvc.ViewModel.Storage;
    using CabinetMvc.ViewModel.Update;
    using CabinetMvc.ViewModel.Prj;
    using CabinetMvc.ViewModel.Stock;
    using CabinetMvc.ViewModel.Asna;
    using CabinetMvc.ViewModel.Wa;

    public static partial class NinjectWebCommon
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private static void RegisterServicesExt(IKernel ninjectKernel)
        {
            //logger.Info("RegisterServicesExt start");

            ninjectKernel.Settings.InjectNonPublic = true;
            //
            ninjectKernel.Bind<IAuthentication>().To<CustomAuthentication>().InRequestScope();
            //            
            ninjectKernel.Bind<IUserService>().To<UserService>().InRequestScope();
            //
            ninjectKernel.Bind<ILoggerService>().To<LoggerService>().InRequestScope();
            //
            ninjectKernel.Bind<AutoMapper.IMapper>().To<AutoMapperCommon>().InSingletonScope();
            // Adm
            ninjectKernel.Bind<IDiscountLogEventService>().To<DiscountLogEventService>().InRequestScope();
            ninjectKernel.Bind<IEsnLogEventService>().To<EsnLogEventService>().InRequestScope();
            ninjectKernel.Bind<ILogCabService>().To<LogCabService>().InRequestScope();
            ninjectKernel.Bind<ICabActionGroupService>().To<CabActionGroupService>().InRequestScope();
            ninjectKernel.Bind<ICabActionService>().To<CabActionService>().InRequestScope();
            ninjectKernel.Bind<ICabRoleActionService>().To<CabRoleActionService>().InRequestScope();
            ninjectKernel.Bind<ICabRoleService>().To<CabRoleService>().InRequestScope();
            ninjectKernel.Bind<ICabUserRoleActionService>().To<CabUserRoleActionService>().InRequestScope();
            ninjectKernel.Bind<ICabUserRoleService>().To<CabUserRoleService>().InRequestScope();
            ninjectKernel.Bind<ICabUserService>().To<CabUserService>().InRequestScope();            
            // Cabinet
            ninjectKernel.Bind<IClientService>().To<ClientService>().InRequestScope();
            ninjectKernel.Bind<ILicenseService>().To<LicenseService>().InRequestScope();
            ninjectKernel.Bind<IOrderItemService>().To<OrderItemService>().InRequestScope();
            ninjectKernel.Bind<IOrderService>().To<OrderService>().InRequestScope();
            ninjectKernel.Bind<ISalesService>().To<SalesService>().InRequestScope();
            ninjectKernel.Bind<IServiceService>().To<ServiceService>().InRequestScope();            
            ninjectKernel.Bind<ISpravCityService>().To<SpravCityService>().InRequestScope();
            ninjectKernel.Bind<ISpravDistrictService>().To<SpravDistrictService>().InRequestScope();            
            ninjectKernel.Bind<ISpravProjectService>().To<SpravProjectService>().InRequestScope();
            ninjectKernel.Bind<ISpravRegionService>().To<SpravRegionService>().InRequestScope();
            ninjectKernel.Bind<ISpravStreetService>().To<SpravStreetService>().InRequestScope();
            ninjectKernel.Bind<IVersionService>().To<VersionService>().InRequestScope();
            ninjectKernel.Bind<IWorkplaceService>().To<WorkplaceService>().InRequestScope();
            ninjectKernel.Bind<IClientServiceService>().To<ClientServiceService>().InRequestScope();
            ninjectKernel.Bind<IWorkplaceReregService>().To<WorkplaceReregService>().InRequestScope();
            ninjectKernel.Bind<IClientServiceUnregService>().To<ClientServiceUnregService>().InRequestScope();
            ninjectKernel.Bind<IUserProfileService>().To<UserProfileService>().InRequestScope();
            ninjectKernel.Bind<IServiceSenderService>().To<ServiceSenderService>().InRequestScope();
            ninjectKernel.Bind<IServiceSenderLogService>().To<ServiceSenderLogService>().InRequestScope();
            ninjectKernel.Bind<IServiceCvaService>().To<ServiceCvaService>().InRequestScope(); 
            ninjectKernel.Bind<IServiceCvaDataService>().To<ServiceCvaDataService>().InRequestScope();
            ninjectKernel.Bind<IClientUserService>().To<ClientUserService>().InRequestScope();
            ninjectKernel.Bind<IWidgetService>().To<WidgetService>().InRequestScope();
            ninjectKernel.Bind<IClientUserWidgetService>().To<ClientUserWidgetService>().InRequestScope();
            ninjectKernel.Bind<IServiceDefectService>().To<ServiceDefectService>().InRequestScope();
            ninjectKernel.Bind<IClientInfoService>().To<ClientInfoService>().InRequestScope();
            ninjectKernel.Bind<IClientServiceSummaryService>().To<ClientServiceSummaryService>().InRequestScope();
            ninjectKernel.Bind<IServiceGroupService>().To<ServiceGroupService>().InRequestScope();
            ninjectKernel.Bind<IRegService>().To<RegService>().InRequestScope();
            ninjectKernel.Bind<IDeliveryTemplateService>().To<DeliveryTemplateService>().InRequestScope();
            ninjectKernel.Bind<IDeliveryTemplateGroupService>().To<DeliveryTemplateGroupService>().InRequestScope();
            ninjectKernel.Bind<IDeliveryService>().To<DeliveryService>().InRequestScope();
            ninjectKernel.Bind<IDeliveryDetailService>().To<DeliveryDetailService>().InRequestScope();            
            ninjectKernel.Bind<IClientServiceParamService>().To<ClientServiceParamService>().InRequestScope();
            ninjectKernel.Bind<IUserMenuService>().To<UserMenuService>().InRequestScope();
            ninjectKernel.Bind<IServiceKitService>().To<ServiceKitService>().InRequestScope();
            ninjectKernel.Bind<IServiceKitItemService>().To<ServiceKitItemService>().InRequestScope();
            ninjectKernel.Bind<ICabHelpService>().To<CabHelpService>().InRequestScope();
            ninjectKernel.Bind<IClientCabService>().To<ClientCabService>().InRequestScope();
            ninjectKernel.Bind<IClientCrmService>().To<ClientCrmService>().InRequestScope();
            ninjectKernel.Bind<IDeliveryTemplateFileService>().To<DeliveryTemplateFileService>().InRequestScope();
            ninjectKernel.Bind<IClientMaxVersionService>().To<ClientMaxVersionService>().InRequestScope();
            ninjectKernel.Bind<IServiceStockService>().To<ServiceStockService>().InRequestScope();
            ninjectKernel.Bind<ISpravRegionZoneService>().To<SpravRegionZoneService>().InRequestScope();
            ninjectKernel.Bind<ISpravRegionPriceLimitService>().To<SpravRegionPriceLimitService>().InRequestScope();
            ninjectKernel.Bind<ISpravRegionZonePriceLimitService>().To<SpravRegionZonePriceLimitService>().InRequestScope();
            ninjectKernel.Bind<ISpravTaxSystemTypeService>().To<SpravTaxSystemTypeService>().InRequestScope();
            ninjectKernel.Bind<IClientReportService>().To<ClientReportService>().InRequestScope();
            ninjectKernel.Bind<IServiceEsn2Service>().To<ServiceEsn2Service>().InRequestScope();
            ninjectKernel.Bind<IVersionMainService>().To<VersionMainService>().InRequestScope();            
            // Defect
            ninjectKernel.Bind<IDefectService>().To<DefectService>().InRequestScope();
            ninjectKernel.Bind<IDefectLogService>().To<DefectLogService>().InRequestScope();
            ninjectKernel.Bind<IDefectRequestService>().To<DefectRequestService>().InRequestScope();
            ninjectKernel.Bind<IDefectSummaryService>().To<DefectSummaryService>().InRequestScope();            
            // Card
            ninjectKernel.Bind<IBusinessOrgService>().To<BusinessOrgService>().InRequestScope();
            ninjectKernel.Bind<IBusinessOrgUserService>().To<BusinessOrgUserService>().InRequestScope();
            ninjectKernel.Bind<IBusinessService>().To<BusinessService>().InRequestScope();
            ninjectKernel.Bind<ICardAdditionalNumService>().To<CardAdditionalNumService>().InRequestScope();
            ninjectKernel.Bind<ICardBatchOperationsErrorService>().To<CardBatchOperationsErrorService>().InRequestScope();
            ninjectKernel.Bind<ICardBatchOperationsService>().To<CardBatchOperationsService>().InRequestScope();
            ninjectKernel.Bind<ICardService>().To<CardService>().InRequestScope();
            ninjectKernel.Bind<ICardClientService>().To<CardClientService>().InRequestScope();
            ninjectKernel.Bind<ICardImportService>().To<CardImportService>().InRequestScope();
            ninjectKernel.Bind<ICardTransactionDetailService>().To<CardTransactionDetailService>().InRequestScope();
            ninjectKernel.Bind<ICardTransactionService>().To<CardTransactionService>().InRequestScope();
            ninjectKernel.Bind<ICardTypeGroupService>().To<CardTypeGroupService>().InRequestScope();
            ninjectKernel.Bind<ICardTypeLimitService>().To<CardTypeLimitService>().InRequestScope();
            ninjectKernel.Bind<ICardTypeService>().To<CardTypeService>().InRequestScope();
            ninjectKernel.Bind<IProgrammParamService>().To<ProgrammParamService>().InRequestScope();
            ninjectKernel.Bind<IProgrammService>().To<ProgrammService>().InRequestScope();
            ninjectKernel.Bind<ISpravCardClientService>().To<SpravCardClientService>().InRequestScope();
            ninjectKernel.Bind<ISpravCardStatusService>().To<SpravCardStatusService>().InRequestScope();
            ninjectKernel.Bind<ISpravCardTypeService>().To<SpravCardTypeService>().InRequestScope();
            ninjectKernel.Bind<ISpravProgrammService>().To<SpravProgrammService>().InRequestScope();
            ninjectKernel.Bind<ISpravProgrammTemplateService>().To<SpravProgrammTemplateService>().InRequestScope();
            ninjectKernel.Bind<ICardTypeBusinessOrgService>().To<CardTypeBusinessOrgService>().InRequestScope();
            ninjectKernel.Bind<IProgrammOrderService>().To<ProgrammOrderService>().InRequestScope();
            ninjectKernel.Bind<IReportDiscountService>().To<ReportDiscountService>().InRequestScope();
            ninjectKernel.Bind<ICampaignService>().To<CampaignService>().InRequestScope();
            ninjectKernel.Bind<ICampaignBusinessOrgService>().To<CampaignBusinessOrgService>().InRequestScope();
            ninjectKernel.Bind<IProgrammBuilderService>().To<ProgrammBuilderService>().InRequestScope();
            ninjectKernel.Bind<IProgrammParamTypeService>().To<ProgrammParamTypeService>().InRequestScope();
            ninjectKernel.Bind<IProgrammStepParamTypeService>().To<ProgrammStepParamTypeService>().InRequestScope();
            ninjectKernel.Bind<IProgrammCardParamTypeService>().To<ProgrammCardParamTypeService>().InRequestScope();
            ninjectKernel.Bind<IProgrammTransParamTypeService>().To<ProgrammTransParamTypeService>().InRequestScope();
            ninjectKernel.Bind<IProgrammStepParamService>().To<ProgrammStepParamService>().InRequestScope();
            ninjectKernel.Bind<IProgrammStepConditionService>().To<ProgrammStepConditionService>().InRequestScope();
            ninjectKernel.Bind<IProgrammStepConditionTypeService>().To<ProgrammStepConditionTypeService>().InRequestScope();
            ninjectKernel.Bind<IProgrammStepLogicTypeService>().To<ProgrammStepLogicTypeService>().InRequestScope();            
            // Exchange
            ninjectKernel.Bind<IExchangeClientService>().To<ExchangeClientService>().InRequestScope();
            ninjectKernel.Bind<IExchangeDataService>().To<ExchangeDataService>().InRequestScope();
            ninjectKernel.Bind<IExchangeDocDataService>().To<ExchangeDocDataService>().InRequestScope();
            ninjectKernel.Bind<IExchangeLogDateService>().To<ExchangeLogDateService>().InRequestScope();
            ninjectKernel.Bind<IExchangeLogService>().To<ExchangeLogService>().InRequestScope();
            ninjectKernel.Bind<IExchangeMonitorLogService>().To<ExchangeMonitorLogService>().InRequestScope();
            ninjectKernel.Bind<IExchangeMonitorService>().To<ExchangeMonitorService>().InRequestScope();
            ninjectKernel.Bind<IExchangePartnerService>().To<ExchangePartnerService>().InRequestScope();
            ninjectKernel.Bind<IExchangeRegDocSalesService>().To<ExchangeRegDocSalesService>().InRequestScope();
            ninjectKernel.Bind<IExchangeRegDocService>().To<ExchangeRegDocService>().InRequestScope();
            ninjectKernel.Bind<IExchangeRegPartnerItemService>().To<ExchangeRegPartnerItemService>().InRequestScope();
            ninjectKernel.Bind<IExchangeRegPartnerService>().To<ExchangeRegPartnerService>().InRequestScope();
            ninjectKernel.Bind<IExchangeUserService>().To<ExchangeUserService>().InRequestScope();
            ninjectKernel.Bind<IExchangeTaskService>().To<ExchangeTaskService>().InRequestScope();
            ninjectKernel.Bind<IExchangeTaskTypeService>().To<ExchangeTaskTypeService>().InRequestScope();
            ninjectKernel.Bind<IExchangeFileLogService>().To<ExchangeFileLogService>().InRequestScope();
            ninjectKernel.Bind<IExchangeFileNodeService>().To<ExchangeFileNodeService>().InRequestScope();
            ninjectKernel.Bind<IExchangeFileNodeItemService>().To<ExchangeFileNodeItemService>().InRequestScope();
            ninjectKernel.Bind<IExchangePartnerItemService>().To<ExchangePartnerItemService>().InRequestScope();            
            // HelpDesk
            ninjectKernel.Bind<IStoryService>().To<StoryService>().InRequestScope();
            // Home
            ninjectKernel.Bind<IBusinessSummaryService>().To<BusinessSummaryService>().InRequestScope();            
            // Ved
            ninjectKernel.Bind<IVedReestrItemOutService>().To<VedReestrItemOutService>().InRequestScope();
            ninjectKernel.Bind<IVedReestrItemService>().To<VedReestrItemService>().InRequestScope();
            ninjectKernel.Bind<IVedReestrRegionItemErrorService>().To<VedReestrRegionItemErrorService>().InRequestScope();
            ninjectKernel.Bind<IVedReestrRegionItemOutService>().To<VedReestrRegionItemOutService>().InRequestScope();
            ninjectKernel.Bind<IVedReestrRegionItemService>().To<VedReestrRegionItemService>().InRequestScope();
            ninjectKernel.Bind<IVedRequestService>().To<VedRequestService>().InRequestScope();
            ninjectKernel.Bind<IVedSourceRegionService>().To<VedSourceRegionService>().InRequestScope();
            ninjectKernel.Bind<IVedSourceService>().To<VedSourceService>().InRequestScope();
            ninjectKernel.Bind<IVedTemplateColumnService>().To<VedTemplateColumnService>().InRequestScope();
            ninjectKernel.Bind<IVedTemplateService>().To<VedTemplateService>().InRequestScope();
            // Crm
            ninjectKernel.Bind<ICrmClientService>().To<CrmClientService>().InRequestScope();
            ninjectKernel.Bind<ICrmModulePartService>().To<CrmModulePartService>().InRequestScope();
            ninjectKernel.Bind<ICrmModuleService>().To<CrmModuleService>().InRequestScope();
            ninjectKernel.Bind<ICrmModuleVersionService>().To<CrmModuleVersionService>().InRequestScope();
            ninjectKernel.Bind<ICrmPriorityService>().To<CrmPriorityService>().InRequestScope();
            ninjectKernel.Bind<ICrmProjectService>().To<CrmProjectService>().InRequestScope();
            ninjectKernel.Bind<ICrmUserRoleService>().To<CrmUserRoleService>().InRequestScope();
            ninjectKernel.Bind<ICrmProjectVersionService>().To<CrmProjectVersionService>().InRequestScope();
            // Sys
            ninjectKernel.Bind<ICabGridService>().To<CabGridService>().InRequestScope();
            ninjectKernel.Bind<ICabGridColumnService>().To<CabGridColumnService>().InRequestScope();
            ninjectKernel.Bind<ICabGridColumnUserSettingsService>().To<CabGridColumnUserSettingsService>().InRequestScope();
            ninjectKernel.Bind<ICabGridUserSettingService>().To<CabGridUserSettingService>().InRequestScope();
            // Auth
            ninjectKernel.Bind<IAuthGroupService>().To<AuthGroupService>().InRequestScope();
            ninjectKernel.Bind<IAuthSectionService>().To<AuthSectionService>().InRequestScope();
            ninjectKernel.Bind<IAuthSectionPartService>().To<AuthSectionPartService>().InRequestScope();
            ninjectKernel.Bind<IAuthRoleService>().To<AuthRoleService>().InRequestScope();
            ninjectKernel.Bind<IAuthUserRoleService>().To<AuthUserRoleService>().InRequestScope();
            ninjectKernel.Bind<IAuthUserPermService>().To<AuthUserPermService>().InRequestScope();
            ninjectKernel.Bind<IAuthRolePermService>().To<AuthRolePermService>().InRequestScope();            
            // Storage
            ninjectKernel.Bind<IStorageFolderService>().To<StorageFolderService>().InRequestScope();
            ninjectKernel.Bind<IStorageFileService>().To<StorageFileService>().InRequestScope();
            ninjectKernel.Bind<IStorageFileTypeService>().To<StorageFileTypeService>().InRequestScope();
            ninjectKernel.Bind<IStorageFileVersionService>().To<StorageFileVersionService>().InRequestScope();
            ninjectKernel.Bind<IStorageParamService>().To<StorageParamService>().InRequestScope();
            ninjectKernel.Bind<IStorageFileTypeGroupService>().To<StorageFileTypeGroupService>().InRequestScope();
            ninjectKernel.Bind<IStorageLogService>().To<StorageLogService>().InRequestScope();
            ninjectKernel.Bind<IStorageLinkService>().To<StorageLinkService>().InRequestScope();
            ninjectKernel.Bind<IStorageTrashService>().To<StorageTrashService>().InRequestScope();            
            // Update
            ninjectKernel.Bind<IUpdateSoftService>().To<UpdateSoftService>().InRequestScope();
            ninjectKernel.Bind<IUpdateSoftVersionService>().To<UpdateSoftVersionService>().InRequestScope();
            ninjectKernel.Bind<IUpdateBatchService>().To<UpdateBatchService>().InRequestScope();
            ninjectKernel.Bind<IUpdateBatchItemService>().To<UpdateBatchItemService>().InRequestScope();
            ninjectKernel.Bind<IUpdateSoftGroupService>().To<UpdateSoftGroupService>().InRequestScope();            
            // Prj
            ninjectKernel.Bind<IPrjService>().To<PrjService>().InRequestScope();
            ninjectKernel.Bind<IPrjClaimService>().To<PrjClaimService>().InRequestScope();
            ninjectKernel.Bind<IPrjStateTypeService>().To<PrjStateTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjTaskService>().To<PrjTaskService>().InRequestScope();            
            ninjectKernel.Bind<IPrjWorkService>().To<PrjWorkService>().InRequestScope();
            ninjectKernel.Bind<IPrjWorkTypeService>().To<PrjWorkTypeService>().InRequestScope();            
            ninjectKernel.Bind<IPrjClaimStateTypeService>().To<PrjClaimStateTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjTaskStateTypeService>().To<PrjTaskStateTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjWorkStateTypeService>().To<PrjWorkStateTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjMessService>().To<PrjMessService>().InRequestScope();
            ninjectKernel.Bind<IPrjLogService>().To<PrjLogService>().InRequestScope();
            ninjectKernel.Bind<IPrjUserSettingsService>().To<PrjUserSettingsService>().InRequestScope();            
            ninjectKernel.Bind<IPrjClaimDefaultService>().To<PrjClaimDefaultService>().InRequestScope();
            ninjectKernel.Bind<IPrjWorkDefaultService>().To<PrjWorkDefaultService>().InRequestScope();
            ninjectKernel.Bind<IPrjDefaultService>().To<PrjDefaultService>().InRequestScope();
            ninjectKernel.Bind<IPrjFileService>().To<PrjFileService>().InRequestScope();
            ninjectKernel.Bind<IPrjNotifyTypeService>().To<PrjNotifyTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjNotifyService>().To<PrjNotifyService>().InRequestScope();                        
            ninjectKernel.Bind<IPrjUserGroupService>().To<PrjUserGroupService>().InRequestScope();
            ninjectKernel.Bind<IPrjBriefTypeService>().To<PrjBriefTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjBriefIntervalTypeService>().To<PrjBriefIntervalTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjUserGroupItemTypeService>().To<PrjUserGroupItemTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjNoteService>().To<PrjNoteService>().InRequestScope();
            ninjectKernel.Bind<IPrjUserGroupItemService>().To<PrjUserGroupItemService>().InRequestScope();
            ninjectKernel.Bind<IPrjBriefDateCutTypeService>().To<PrjBriefDateCutTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjClaimTypeService>().To<PrjClaimTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjClaimStageService>().To<PrjClaimStageService>().InRequestScope();
            ninjectKernel.Bind<IPrjTaskUserStateTypeService>().To<PrjTaskUserStateTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjProjectVersionService>().To<PrjProjectVersionService>().InRequestScope();
            ninjectKernel.Bind<IPrjClaimFieldService>().To<PrjClaimFieldService>().InRequestScope();
            ninjectKernel.Bind<IPrjClaimFilterOpTypeService>().To<PrjClaimFilterOpTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjClaimFilterService>().To<PrjClaimFilterService>().InRequestScope();
            ninjectKernel.Bind<IPrjClaimFilterItemService>().To<PrjClaimFilterItemService>().InRequestScope();            
            ninjectKernel.Bind<IVersionTypeService>().To<VersionTypeService>().InRequestScope();
            ninjectKernel.Bind<IPrjReport1Service>().To<PrjReport1Service>().InRequestScope();            
            // Stock
            ninjectKernel.Bind<IStockService>().To<StockService>().InRequestScope();
            ninjectKernel.Bind<IStockBatchService>().To<StockBatchService>().InRequestScope();
            ninjectKernel.Bind<IStockSalesDownloadService>().To<StockSalesDownloadService>().InRequestScope();
            ninjectKernel.Bind<IStockLogService>().To<StockLogService>().InRequestScope();
            // Asna
            ninjectKernel.Bind<IAsnaUserService>().To<AsnaUserService>().InRequestScope();
            ninjectKernel.Bind<IAsnaScheduleIntervalTypeService>().To<AsnaScheduleIntervalTypeService>().InRequestScope();
            ninjectKernel.Bind<IAsnaScheduleTimeTypeService>().To<AsnaScheduleTimeTypeService>().InRequestScope();
            ninjectKernel.Bind<IAsnaUserLogService>().To<AsnaUserLogService>().InRequestScope();
            ninjectKernel.Bind<IAsnaLinkService>().To<AsnaLinkService>().InRequestScope();
            ninjectKernel.Bind<IAsnaStockRowService>().To<AsnaStockRowService>().InRequestScope();
            ninjectKernel.Bind<IAsnaStockChainService>().To<AsnaStockChainService>().InRequestScope();
            ninjectKernel.Bind<IAsnaStockRowActualService>().To<AsnaStockRowActualService>().InRequestScope();
            ninjectKernel.Bind<IAsnaOrderStateTypeService>().To<AsnaOrderStateTypeService>().InRequestScope();
            ninjectKernel.Bind<IAsnaOrderService>().To<AsnaOrderService>().InRequestScope();
            ninjectKernel.Bind<IAsnaOrderRowService>().To<AsnaOrderRowService>().InRequestScope();
            ninjectKernel.Bind<IAsnaOrderStateService>().To<AsnaOrderStateService>().InRequestScope();
            ninjectKernel.Bind<IAsnaOrderRowStateService>().To<AsnaOrderRowStateService>().InRequestScope();            
            // Wa
            ninjectKernel.Bind<IWaTaskService>().To<WaTaskService>().InRequestScope();
            ninjectKernel.Bind<IWaRequestTypeService>().To<WaRequestTypeService>().InRequestScope();
            ninjectKernel.Bind<IWaTaskLcService>().To<WaTaskLcService>().InRequestScope();
            //logger.Info("RegisterServicesExt end");
        }
    }
}