﻿using System.Web;
using System.Web.Optimization;
using CabinetMvc.Util;

namespace CabinetMvc
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {


            /*
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));
            //jquery
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            //bootstrap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));
            */

            string kendoVersion = CabinetUtil.GetKendoFolder();
            string jsBundleName = CabinetUtil.GetJsBundleName();
            string cssBundleName = CabinetUtil.GetCssBundleName();

            //string discountBundleName = CabinetUtil.GetDiscountBundleName();

            bundles.Add(new ScriptBundle(jsBundleName).Include(
                     "~/Scripts/jquery-{version}.js",
                     "~/Scripts/kendo/" + kendoVersion + "/jszip.min.js",
                     "~/Scripts/kendo/" + kendoVersion + "/kendo.all.min.js",
                     "~/Scripts/kendo/" + kendoVersion + "/kendo.web.min.js",
                     "~/Scripts/kendo/" + kendoVersion + "/kendo.timezones.min.js",
                     "~/Scripts/kendo/" + kendoVersion + "/kendo.aspnetmvc.min.js",
                     "~/Scripts/bootstrap.js",
                     "~/Scripts/jquery-ui.min.js",
                     "~/Scripts/theme-chooser.js",
                     //"~/Scripts/fixedsticky.js",
                     "~/Scripts/views/cabinet/cab-help-{version}.js",
                     "~/Scripts/Site-{version}.js",
                     "~/Scripts/cultures/kendo.culture.ru-RU.min.js"
                      ));

            /*
            bundles.Add(new ScriptBundle(discountBundleName).Include(
                     "~/Scripts/views/discount/business-add-{version}.js",
                     "~/Scripts/views/discount/business-edit-{version}.js"
                      ));
            */

            bundles.Add(new StyleBundle(cssBundleName).Include(
                      "~/Content/bootstrap.css",
                      
                      "~/Content/kendo/" + kendoVersion + "/kendo.common.min.css",

                      "~/Content/kendo/" + kendoVersion + "/kendo.common-bootstrap.min.css",                      
                      //"~/Content/kendo/" + kendoVersion + "/kendo.common-material.min.css",

                      "~/Content/kendo/" + kendoVersion + "/kendo.bootstrap.min.css",
                      //"~/Content/kendo/" + kendoVersion + "/kendo.material.min.css",

                      "~/Content/kendo/" + kendoVersion + "/kendo.dataviz.min.css",

                      "~/Content/kendo/" + kendoVersion + "/kendo.dataviz.bootstrap.min.css",
                      //"~/Content/kendo/" + kendoVersion + "/kendo.dataviz.material.min.css",

                      "~/Content/jquery-ui.theme.min.css",
                      "~/Content/jquery-ui.structure.min.css",
                      "~/Content/jquery-ui.min.css",
                      //"~/Content/fixedsticky.css",
                      "~/Content/Png-{version}.css",
                      "~/Content/Png-storage-{version}.css",
                      "~/Content/Site-{version}.css"
                      ));

            
            if (!System.Diagnostics.Debugger.IsAttached)
                BundleTable.EnableOptimizations = true;

            /*
                <link href="~/Content/bootstrap.min.css" rel="stylesheet" />
                <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.1.408/styles/kendo.common-bootstrap.min.css" />
                <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.1.408/styles/kendo.bootstrap.min.css" />
                <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.1.408/styles/kendo.dataviz.min.css" />
                <link rel="stylesheet" href="http://cdn.kendostatic.com/2015.1.408/styles/kendo.dataviz.bootstrap.min.css" />

                <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
                <script src="http://cdn.kendostatic.com/2015.1.408/js/kendo.all.min.js"></script>
                <script src="http://cdn.kendostatic.com/2015.1.408/js/kendo.timezones.min.js"></script>
                <script src="http://cdn.kendostatic.com/2015.1.408/js/kendo.aspnetmvc.min.js"></script>

                <script src="~/Scripts/theme-chooser.js"></script>
                <link href="~/Content/styles.css" rel="stylesheet" />
            */
        }
    }
}
