﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Drawing;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Sys;
using CabinetMvc.ViewModel.Prj;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using Kendo.Mvc;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class PrjController : BaseController
    {            

        #region Prj

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjList()
        {
            ViewData["userPrjDefault"] = cachedUserPrjDefault;
            //
            // также см. Web.config: requestFiltering и maxRequestLength                        
            ViewData["maxFileSize"] = cachedPrjFileParam.MaxFileSize;
            ViewData["allowedExtentions"] = cachedPrjFileParam.AllowedExtensions;
            //
            //ViewData["userSettingsList"] = cachedUserSettingsVMList;
            //
            return View();
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjList([DataSourceRequest]DataSourceRequest request, int? state_type_id, string search)
        {
            return Json(prjService.GetList_byFilter(state_type_id.GetValueOrDefault(0), search).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjEditPartial(int? prj_id)
        {
            ViewData["projectList"] = cachedProjectList;
            PrjViewModel model = new PrjViewModel() { date_beg = DateTime.Today, state_type_id = (int)Enums.PrjStateTypeEnum.PREPARE, };
            if (prj_id.GetValueOrDefault(-1) >= 0)
                model = (PrjViewModel)prjService.GetItem(prj_id.GetValueOrDefault(-1));
            return PartialView(model);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public JsonResult PrjEditPartial(PrjViewModel curr_prj)
        {
            if (ModelState.IsValid)
            {
                PrjViewModel result = null;
                int prj_id = curr_prj.prj_id;
                if (prj_id <= -1)
                {
                    result = (PrjViewModel)prjService.Insert(curr_prj, ModelState);
                }
                else
                {
                    result = (PrjViewModel)prjService.Update(curr_prj, ModelState);
                }
                if ((result == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении группы");
                    return Json(new { err = true, mess = ModelState.GetErrorsString(), prj = curr_prj });
                }
                //
                _cachedPrjList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjViewModel>>();
                //
                return Json(new { err = false, mess = "", prj = result });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), prj = curr_prj });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PrjAdd([DataSourceRequest]DataSourceRequest request, PrjViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = prjService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState ));

                _cachedPrjList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjViewModel>>();
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PrjEdit([DataSourceRequest]DataSourceRequest request, PrjViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = prjService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));

                _cachedPrjList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjViewModel>>();

                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PrjDestroy([DataSourceRequest]DataSourceRequest request, PrjViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = prjService.DeletePrj(item.prj_id, ModelState);

                _cachedPrjList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjViewModel>>();
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public ActionResult PrjDel(int prj_id)
        {
            prjService.DeletePrj(prj_id, ModelState);
            _cachedPrjList = null;
            cabCache.DeleteCachedObject<IEnumerable<PrjViewModel>>();
            return new EmptyResult();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public JsonResult PrjRestore(int prj_id)
        {
            var res = prjService.RestorePrj(prj_id, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошбика при попытке восстановления группы");
                _cachedPrjList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjViewModel>>();
                return Json(new { err = true, mess = ModelState.GetErrorsString(), });
            }
            return Json(new { err = false, });
        }
        
        #endregion

        #region PrjClaim

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjClaimListPartial()
        {
            
            ViewData["prjList"] = cachedPrjList;
            ViewData["cabUserList"] = cachedCabUserList;
            ViewData["projectList"] = cachedProjectList;
            ViewData["clientList"] = cachedClientList;
            ViewData["priorityList"] = cachedPriorityList;
            ViewData["claimTypeList"] = cachedClaimTypeList;
            ViewData["claimStageList"] = cachedClaimStageList.OrderBy(ss => ss.claim_stage_name);
            ViewData["prjTaskUserStateTypeList"] = cachedTaskUserStateTypeList;
            ViewData["workTypeList"] = cachedWorkTypeList;
            ViewData["versionMainList"] = cachedVersionMainList.OrderByDescending(ss => ss.version_type).ThenBy(ss => ss.version_main_id);
            ViewData["claimFilterList"] = cachedClaimFilterList;
            //
            var userSettingsCurrent = cachedUserSettingsVMList != null ? cachedUserSettingsVMList.Where(ss => ss.page_type_id == (int)Enums.PrjPageTypeEnum.CLAIM && ss.is_selected).FirstOrDefault() : null;
            ViewData["userSettingsCurrent"] = userSettingsCurrent;
            ViewData["userSettings"] = userSettingsCurrent != null ? userSettingsCurrent.prjUserSettingsList : null;
            //
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjClaimList([DataSourceRequest]DataSourceRequest request, string project_id_list, string version_main_name_list, string claim_stage_id_list, string exec_user_id_list
            , string claim_is_active_list, int? filter_id, string prj_id_list, string search)
        {
            if (filter_id.GetValueOrDefault(0) > 0)
            {
                string filter_raw = prjClaimFilterService.GetFilterRaw(filter_id);
                if (!String.IsNullOrWhiteSpace(filter_raw))
                {                    
                    ModifyFilters(request.Filters, filter_raw);                    
                }
            }
            return Json(prjClaimService.GetList_byFilter(project_id_list, version_main_name_list, claim_stage_id_list, exec_user_id_list, claim_is_active_list, prj_id_list, search).ToDataSourceResult(request));
        }

        private void ModifyFilters(IList<IFilterDescriptor> filters, string filterAdd)
        {
            KendoFilterMain kendoFilterMain = JsonConvert.DeserializeObject<KendoFilterMain>(filterAdd);

            CompositeFilterDescriptor compositeFilterDescriptorMain = new CompositeFilterDescriptor()
            {
                FilterDescriptors = new Kendo.Mvc.Infrastructure.Implementation.FilterDescriptorCollection(),
            };
            compositeFilterDescriptorMain.LogicalOperator = kendoFilterMain.logic == "and" ? FilterCompositionLogicalOperator.And : FilterCompositionLogicalOperator.Or;
            CompositeFilterDescriptor compositeFilterDescriptor = null;
            FilterDescriptor filterDescriptor = null;
            foreach (var kendoFilterItem in kendoFilterMain.filters)
            {
                compositeFilterDescriptor = new CompositeFilterDescriptor()
                {
                    FilterDescriptors = new Kendo.Mvc.Infrastructure.Implementation.FilterDescriptorCollection(),
                };
                compositeFilterDescriptor.LogicalOperator = kendoFilterItem.logic == "and" ? FilterCompositionLogicalOperator.And : FilterCompositionLogicalOperator.Or;
                foreach (var kendoFilterItemValue in kendoFilterItem.filters)
                {
                    if (kendoFilterItemValue.Operator == "in")
                    {
                        var value_splitted = kendoFilterItemValue.Value.Split(';').ToList();
                        filterDescriptor = new FilterDescriptor();
                        filterDescriptor.Operator = StringToKendoFilterOperator("gte");
                        filterDescriptor.Value = value_splitted[0];
                        filterDescriptor.Member = kendoFilterItemValue.Field;
                        compositeFilterDescriptor.LogicalOperator = FilterCompositionLogicalOperator.And;
                        compositeFilterDescriptor.FilterDescriptors.Add(filterDescriptor);
                        filterDescriptor = new FilterDescriptor();
                        filterDescriptor.Operator = StringToKendoFilterOperator("lte");
                        filterDescriptor.Value = value_splitted[1];
                        filterDescriptor.Member = kendoFilterItemValue.Field;
                        compositeFilterDescriptor.LogicalOperator = FilterCompositionLogicalOperator.And;
                        compositeFilterDescriptor.FilterDescriptors.Add(filterDescriptor);
                    }
                    else if (kendoFilterItemValue.Operator == "nin")
                    {
                        var value_splitted = kendoFilterItemValue.Value.Split(';').ToList();
                        filterDescriptor = new FilterDescriptor();
                        filterDescriptor.Operator = StringToKendoFilterOperator("lte");
                        filterDescriptor.Value = value_splitted[0];
                        filterDescriptor.Member = kendoFilterItemValue.Field;
                        compositeFilterDescriptor.FilterDescriptors.Add(filterDescriptor);
                        filterDescriptor = new FilterDescriptor();
                        filterDescriptor.Operator = StringToKendoFilterOperator("gte");
                        filterDescriptor.Value = value_splitted[1];
                        filterDescriptor.Member = kendoFilterItemValue.Field;
                        compositeFilterDescriptor.FilterDescriptors.Add(filterDescriptor);
                    }
                    else
                    {
                        filterDescriptor = new FilterDescriptor();
                        filterDescriptor.Operator = StringToKendoFilterOperator(kendoFilterItemValue.Operator);
                        filterDescriptor.Value = kendoFilterItemValue.Value;
                        filterDescriptor.Member = kendoFilterItemValue.Field;
                        compositeFilterDescriptor.FilterDescriptors.Add(filterDescriptor);
                    }
                }
                compositeFilterDescriptorMain.FilterDescriptors.Add(compositeFilterDescriptor);
            }

            //compositeFilterDescriptorMain.FilterDescriptors.Add(compositeFilterDescriptor);
            filters.Add(compositeFilterDescriptorMain);

            /*
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    compositeFilterDescriptorMain.FilterDescriptors.Add(filter);
                }
            }                       

            List<CompositeFilterDescriptor> compositeFilterDescriptorMainList = new List<CompositeFilterDescriptor>()
            {
                compositeFilterDescriptorMain
            };
            */



            /*
            if (filters.Any())
            {
                foreach (var filter in filters)
                {
                    var descriptor = filter as FilterDescriptor;
                    if (descriptor != null && descriptor.Member == "SomeField")
                    {
                        descriptor.Member = "SomeOtherField";
                    }
                    else if (filter is CompositeFilterDescriptor)
                    {
                        ModifyFilters(((CompositeFilterDescriptor)filter).FilterDescriptors);
                    }
                }
            }
            */
        }

        private FilterOperator StringToKendoFilterOperator(string opType)
        {
            if (String.IsNullOrWhiteSpace(opType))
                return FilterOperator.IsNotEqualTo;
            switch (opType)
            {
                case "eq":
                    return FilterOperator.IsEqualTo;
                case "neq":
                    return FilterOperator.IsNotEqualTo;
                case "gt":
                    return FilterOperator.IsGreaterThan;
                case "gte":
                    return FilterOperator.IsGreaterThanOrEqualTo;
                case "lt":
                    return FilterOperator.IsLessThan;
                case "lte":
                    return FilterOperator.IsLessThanOrEqualTo;
                /*
                 * 
                case "in":
                    return FilterOperator.IsNotEqualTo;
                case "nin":
                    return FilterOperator.IsNotEqualTo;
                */
                case "contains":
                    return FilterOperator.Contains;
                case "doesnotcontain":
                    return FilterOperator.DoesNotContain;
                default:
                    return FilterOperator.IsNotEqualTo;
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjClaimList_byPrj([DataSourceRequest]DataSourceRequest request, int? prj_id, string search)
        {
            return Json(prjClaimService.GetList_byPrj(prj_id, search).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjClaimList_Favor([DataSourceRequest]DataSourceRequest request)
        {
            return Json(prjClaimService.GetList_Favor().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjClaimEditPartial(int? claim_id, int? prj_id, int? project_id, int? task_id)
        {            
            ViewData["clientList"] = cachedClientList;
            ViewData["priorityList"] = cachedPriorityList;
            ViewData["cabUserList"] = cachedCabUserList;
            ViewData["projectList"] = cachedProjectList;
            ViewData["moduleList"] = cachedModuleList;
            ViewData["modulePartList"] = cachedModulePartList;
            //ViewData["moduleVersionList"] = cachedModuleVersionList;
            ViewData["crmProjectVersionList"] = cachedCrmProjectVersionList;
            ViewData["workTypeList"] = cachedWorkTypeList;
            ViewData["workStateTypeActiveList"] = cachedWorkStateTypeActiveList;            
            ViewData["workStateTypeList"] = cachedWorkStateTypeList;
            ViewData["claimTypeList"] = cachedClaimTypeList;
            ViewData["claimStageList"] = cachedClaimStageList;
            ViewData["prj_claim_task_id"] = task_id;
            //
            PrjClaimViewModel model = new PrjClaimViewModel() { project_id = project_id.GetValueOrDefault(0) };
            if (claim_id.GetValueOrDefault(-1) >= 0)
            {                
                model = (PrjClaimViewModel)prjClaimService.GetItem(claim_id.GetValueOrDefault(-1));
                if (model == null)
                {
                    ModelState.AddModelError("", "Не найдена задача");
                    return RedirectToErrorPage(ModelState);
                }
                model.claim_mess_list = prjMessService.GetList_byParent(model.claim_id).AsEnumerable().Cast<PrjMessViewModel>();                
                model.claim_task_mess_list = model.claim_mess_list.Where(ss => ss.task_id.HasValue);
                model.claim_task_list = prjTaskService.GetList_byParent(model.claim_id).AsEnumerable().Cast<PrjTaskViewModel>();
                model.claim_work_list = prjWorkService.GetList_byClaim(model.claim_id).AsEnumerable().Cast<PrjWorkViewModel>();
                model.claim_file_list = prjFileService.GetList_byParent(claim_id.GetValueOrDefault(-1)).AsEnumerable().Cast<PrjFileViewModel>();
                model.prj_id_list_AsList = model.prj_id_list != null ? model.prj_id_list.Split(',').ToList() : null;
                //
                ViewData["prjList"] = cachedPrjList.Where(ss => ((!ss.project_id.HasValue) || (ss.project_id == model.project_id)));
                ViewData["projectVersionList"] = cachedProjectVersionList.Where(ss => ss.project_id == model.project_id);
            }
            else
            {
                PrjClaimDefaultViewModel userClaimDefault = (PrjClaimDefaultViewModel)prjClaimDefaultService.GetItem(CurrentUser.CabUserId);
                if (userClaimDefault == null)
                    userClaimDefault = prjClaimDefaultService.GetItem_Default();
                model.client_id = userClaimDefault.client_id;
                model.module_id = userClaimDefault.module_id;
                model.module_part_id = userClaimDefault.module_part_id;
                //model.module_version_id = userClaimDefault.module_version_id;
                model.priority_id = userClaimDefault.priority_id;                
                //model.project_id = userClaimDefault.project_id;
                model.project_id = model.project_id > 0 ? model.project_id : userClaimDefault.project_id;
                //model.repair_version_id = userClaimDefault.repair_version_id;
                model.resp_user_id = userClaimDefault.resp_user_id;
                model.claim_mess_list = Enumerable.Empty<PrjMessViewModel>();
                model.claim_task_mess_list = Enumerable.Empty<PrjMessViewModel>();
                model.claim_task_list = Enumerable.Empty<PrjTaskViewModel>();
                model.claim_work_list = Enumerable.Empty<PrjWorkViewModel>();
                model.claim_file_list = Enumerable.Empty<PrjFileViewModel>();
                //model.claim_stage_id = (int)Enums.PrjClaimStageEnum.NEW;
                model.claim_stage_id = cachedClaimStageList.Where(ss => ss.claim_stage_id > 0).OrderBy(ss => ss.claim_stage_name).Select(ss => ss.claim_stage_id).FirstOrDefault();
                model.prj_id_list_AsList = new List<string>();
                //
                ViewData["prjList"] = cachedPrjList.Where(ss => ((!ss.project_id.HasValue) || (ss.project_id == project_id.GetValueOrDefault(0))));
                ViewData["projectVersionList"] = cachedProjectVersionList.Where(ss => ss.project_id == project_id.GetValueOrDefault(0));
            }
            return PartialView(model);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult PrjClaimEditPartial(PrjClaimViewModel claim
            , List<PrjMessViewModel> mess_list, List<PrjTaskViewModel> task_list, List<PrjWorkViewModel> work_list
            , List<int> work_del_list, List<PrjTaskUserStateViewModel> task_user_state_list)
        {
            if (ModelState.IsValid)
            {
                PrjClaimViewModel result = null;
                int claim_id = claim.claim_id;
                if (claim.create_claim_from_task.GetValueOrDefault(0) > 0)
                {
                    result = prjClaimService.InsertClaim_FromTask(claim, mess_list, task_list, work_list, task_user_state_list, ModelState);
                }
                else if (claim_id <= 0)
                {
                    result = prjClaimService.InsertClaim(claim, mess_list, task_list, work_list, task_user_state_list, ModelState);
                }
                else
                {
                    result = prjClaimService.UpdateClaim(claim, mess_list, task_list, work_list, work_del_list, task_user_state_list, ModelState);
                }
                if ((result == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении задачи");
                    return Json(new { err = true, mess = ModelState.GetErrorsString(), claim = claim });
                }
                return Json(new { err = false, mess = "", claim = result });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), claim = claim });
        }

 
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public ActionResult PrjClaimChangePrj(int claim_id, int prj_id)
        {
            prjClaimService.ChangePrj(claim_id, prj_id);
            return new EmptyResult();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public ActionResult PrjClaimDel(int claim_id)
        {
            prjClaimService.DeleteClaim(claim_id, ModelState);
            return new EmptyResult();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public JsonResult PrjClaimRestore(int claim_id)
        {
            var res = prjClaimService.RestoreClaim(claim_id, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошбика при попытке восстановления задачи");
                return Json(new { err = true, mess = ModelState.GetErrorsString(), });
            }
            return Json(new { err = false, });
        }

        #endregion

        #region PrjTask

        /*
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjTaskListPartial()
        {
            ViewData["prjList"] = cachedPrjList;
            ViewData["cabUserList"] = cachedCabUserList;
            ViewData["projectList"] = cachedProjectList;
            ViewData["clientList"] = cachedClientList;
            ViewData["priorityList"] = cachedPriorityList;
            ViewData["userSettings"] = cachedUserSettingsVM != null ? cachedUserSettingsVM.prjUserSettingsList : null;
            //
            return PartialView();
        }
        */

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjTaskList([DataSourceRequest]DataSourceRequest request, string prj_id_list, string task_state_type_id_list, string search)
        {
            return Json(prjTaskService.GetList_byFilter(prj_id_list, task_state_type_id_list, search).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjTaskList_byClaim([DataSourceRequest]DataSourceRequest request, int? claim_id, int? exec_user_id, int? task_user_state_type)
        {
            if (task_user_state_type.GetValueOrDefault(0) > 0)
                return Json(prjTaskService.GetList_byClaim(claim_id.GetValueOrDefault(0), exec_user_id.GetValueOrDefault(0), task_user_state_type.GetValueOrDefault(0)).ToDataSourceResult(request));
            else
                return Json(prjTaskService.GetList_byParent(claim_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult PrjTaskUserAction(int? task_id, int? action_id, int? user_id, int? work_type_id, string comment)
        {
            var res = prjTaskService.ExecuteAction(task_id, action_id, user_id, work_type_id, comment, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка выполнения действия");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false });
        }
        
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult PrjTaskSimpleAdd(int? claim_id, string task_text)
        {
            var res = prjTaskService.InsertSimple(claim_id, task_text, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка добавления задания");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false });
        }

        #endregion

        #region PrjWork

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjWorkListPartial()
        {
            ViewData["prjList"] = cachedPrjList;
            ViewData["cabUserList"] = cachedCabUserList;
            ViewData["workTypeList"] = cachedWorkTypeList;
            ViewData["projectList"] = cachedProjectList;
            ViewData["clientList"] = cachedClientList;
            ViewData["priorityList"] = cachedPriorityList;
            //
            var userSettingsCurrent = cachedUserSettingsVMList != null ? cachedUserSettingsVMList.Where(ss => ss.page_type_id == (int)Enums.PrjPageTypeEnum.WORK && ss.is_selected).FirstOrDefault() : null;
            ViewData["userSettingsCurrent"] = userSettingsCurrent;
            ViewData["userSettings"] = userSettingsCurrent != null ? userSettingsCurrent.prjUserSettingsList : null;
            //            
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjWorkList([DataSourceRequest]DataSourceRequest request, string prj_id_list, string exec_user_id_list, string work_state_type_id_list, bool is_accept, string search)
        {
            return Json(prjWorkService.GetList_byFilter(prj_id_list, exec_user_id_list, work_state_type_id_list, is_accept, search).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjWorkList_byWork([DataSourceRequest]DataSourceRequest request, int? work_id)
        {
            return Json(prjWorkService.GetList_byWork(work_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        // ускорения ради        
        //[SysRoleAuthorize("Administrators", "Superadmin")]
        //[AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        //[AllowAnonymous()]
        public ActionResult GetPrjWorkList_byTask([DataSourceRequest]DataSourceRequest request, int? task_id)
        {            
            return Json(prjWorkService.GetList_byParent(task_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjWorkList_byClaim([DataSourceRequest]DataSourceRequest request, int? claim_id)
        {
            return Json(prjWorkService.GetList_byClaim(claim_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult PrjWorkTypeEdit(int? work_id, int? work_type_id)
        {
            var res = prjWorkService.UpdateWorkType(work_id, work_type_id, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка изменения работы");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false });
        }
        
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult PrjWorkUserEdit(int? work_id, int? exec_user_id)
        {
            var res = prjWorkService.UpdateWorkExecUser(work_id, exec_user_id, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка изменения работы");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false });
        }

        #endregion

        #region PrjNews

        /*
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjNewsPartial()
        {
            ViewData["prjList"] = cachedPrjList;
            ViewData["cabUserList"] = cachedCabUserList;
            ViewData["projectList"] = cachedProjectList;
            ViewData["clientList"] = cachedClientList;
            ViewData["priorityList"] = cachedPriorityList;
            ViewData["userSettings"] = cachedUserSettingsVM != null ? cachedUserSettingsVM.prjUserSettingsList : null;
            //
            return PartialView();            
        }
        */

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjNewsClaimList([DataSourceRequest]DataSourceRequest request, string prj_id_list, string crt_user_name_list)
        {
            return Json(prjClaimService.GetList_New(prj_id_list, crt_user_name_list).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjNewsTaskList([DataSourceRequest]DataSourceRequest request, string prj_id_list, string crt_user_name_list)
        {
            return Json(prjTaskService.GetList_New(prj_id_list, crt_user_name_list).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjNewsMessList([DataSourceRequest]DataSourceRequest request, string prj_id_list, string crt_user_name_list)
        {
            return Json(prjMessService.GetList_New(prj_id_list, crt_user_name_list).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjNewsLogList([DataSourceRequest]DataSourceRequest request, string prj_id_list, string crt_user_name_list)
        {
            return Json(prjLogService.GetList_New(prj_id_list, crt_user_name_list).ToDataSourceResult(request));
        }

        #endregion

        #region PrjMess


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjMessList_byClaim([DataSourceRequest]DataSourceRequest request, int? claim_id)
        {
            return Json(prjMessService.GetList_byParent(claim_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjMessList_forTask_byClaim([DataSourceRequest]DataSourceRequest request, int? claim_id)
        {
            return Json(prjMessService.GetList_forTask_byClaim(claim_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjMessList_byTask([DataSourceRequest]DataSourceRequest request, int? task_id)
        {
            return Json(prjMessService.GetList_byTask(task_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjMessList_byWork([DataSourceRequest]DataSourceRequest request, int? work_id)
        {
            return Json(prjMessService.GetList_byWork(work_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjMessList_byClaimOrTask([DataSourceRequest]DataSourceRequest request, int? claim_id, int? task_id)
        {
            if (claim_id.GetValueOrDefault(0) > 0)
                return Json(prjMessService.GetList_byParent(claim_id.GetValueOrDefault(0)).ToDataSourceResult(request));
            else
                return Json(prjMessService.GetList_byTask(task_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        #endregion

        #region PrjSettings

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjSettingsPartial()
        {
            ViewData["prjList"] = cachedPrjList;
            ViewData["projectList"] = cachedProjectList;
            ViewData["moduleList"] = cachedModuleList;
            ViewData["modulePartList"] = cachedModulePartList;
            ViewData["moduleVersionList"] = cachedModuleVersionList;
            ViewData["clientList"] = cachedClientList;
            ViewData["priorityList"] = cachedPriorityList;
            ViewData["cabUserList"] = cachedCabUserList; 
            //
            ViewData["userPrjDefault"] = cachedUserPrjDefault;
            //
            PrjClaimDefaultViewModel userClaimDefault = (PrjClaimDefaultViewModel)prjClaimDefaultService.GetItem(CurrentUser.CabUserId);
            if (userClaimDefault == null)
                userClaimDefault = prjClaimDefaultService.GetItem_Default();
            ViewData["userClaimDefault"] = userClaimDefault;
            //
            List<PrjWorkDefaultViewModel> userWorkListDefault = prjWorkDefaultService.GetList_byParent(CurrentUser.CabUserId).AsEnumerable().Cast<PrjWorkDefaultViewModel>().ToList();
            ViewData["userWorkListDefault"] = userWorkListDefault;
            //
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        [ValidateInput(false)]
        public JsonResult PrjSettingsPartial(PrjDefaultViewModel prj, PrjClaimDefaultViewModel claim, IEnumerable<PrjWorkDefaultViewModel> workList)
        {
            if (ModelState.IsValid)
            {
                var res1 = prjDefaultService.Insert(prj, ModelState);
                if ((res1 == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении параметров");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                //
                var res2 = prjClaimDefaultService.Insert(claim, ModelState);
                if ((res2 == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении параметров");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                //
                var res3 = prjWorkDefaultService.InsertList(workList, ModelState);
                if ((!res3) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении параметров");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                //
                _cachedUserPrjDefault = null;
                cabCache.DeleteCachedObject<PrjDefaultViewModel>(CurrentUser.CabUserId.ToString());
                //
                return Json(new { err = false, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        #endregion

        #region PrjSprav

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpGet]
        public ActionResult PrjSpravPartial()
        {
            ViewData["projectList"] = cachedProjectList;            
            ViewData["projectDefault"] = cachedProjectList.FirstOrDefault();                        
            return PartialView();
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]        
        public ActionResult GetPrjProjectVersionList([DataSourceRequest]DataSourceRequest request)
        {            
            return Json(prjProjectVersionService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]        
        [HttpPost]
        public ActionResult PrjProjectVersionEdit([DataSourceRequest]DataSourceRequest request, PrjProjectVersionViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = prjProjectVersionService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));

                _cachedProjectVersionList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjProjectVersionViewModel>>();

                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult PrjProjectVersionNewRelease(string project_name, string new_release_num)
        {
            if (ModelState.IsValid)
            {
                var res = prjProjectVersionService.StartNewRelease(project_name, new_release_num, ModelState);
                if ((!res) || (!ModelState.IsValid))
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });

                _cachedProjectVersionList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjProjectVersionViewModel>>();

                return Json(new { err = false });
            }

            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetPrjClaimStageList([DataSourceRequest]DataSourceRequest request)
        {            
            return Json(prjClaimStageService.GetList_Edit().ToDataSourceResult(request));
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult PrjClaimStageAdd([DataSourceRequest]DataSourceRequest request, PrjClaimStageViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = prjClaimStageService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));

                _cachedClaimStageList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjClaimStageViewModel>>();

                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult PrjClaimStageEdit([DataSourceRequest]DataSourceRequest request, PrjClaimStageViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = prjClaimStageService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));

                _cachedClaimStageList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjClaimStageViewModel>>();

                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult PrjClaimStageDel([DataSourceRequest]DataSourceRequest request, PrjClaimStageViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = prjClaimStageService.Delete(item, ModelState);
                if ((!res) || (!ModelState.IsValid))
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));

                _cachedClaimStageList = null;
                cabCache.DeleteCachedObject<IEnumerable<PrjClaimStageViewModel>>();

                return Json(new[] { item }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region PrjUserSettings

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjUserSettingsList(int? page_type_id)
        {
            return Json(prjUserSettingsService.GetList_byPage(CurrentUser.CabUserId, page_type_id.GetValueOrDefault(0)), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [ValidateInput(false)]
        [HttpPost]
        public JsonResult PrjUserSettingsSave(int page_type_id, int mode, int? setting_id, string setting_name, string settings, string settings_grid, string tag)
        {
            var res = prjUserSettingsService.UpdateSettings(page_type_id, mode, setting_id, setting_name, settings, settings_grid, tag, ModelState);
            if ((res == null) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка при сохранении конфигурации");
                return Json(new { err = true, mess = ModelState.GetErrorsString(), });
            }
            //
            _cachedUserSettingsVMList = null;
            cabCache.DeleteCachedObject<List<PrjUserSettingsViewModel>>(CurrentUser.CabUserId.ToString());
            return Json(new { err = false, setting = res });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [ValidateInput(false)]
        [HttpPost]
        public JsonResult PrjUserSettingsChange(int? old_setting_id, int? new_setting_id)
        {
            bool res = prjUserSettingsService.ChangeSettings(old_setting_id, new_setting_id, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка при смене конфигурации");
                return Json(new { err = true, mess = ModelState.GetErrorsString(), });
            }
            //
            _cachedUserSettingsVMList = null;
            cabCache.DeleteCachedObject<List<PrjUserSettingsViewModel>>(CurrentUser.CabUserId.ToString());
            return Json(new { err = false,  });
        }

        #endregion

        #region PrjLog

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjLogPartial()
        {
            return PartialView();
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjLogList([DataSourceRequest]DataSourceRequest request, bool show_deleted, string search)
        {
            return Json(prjLogService.GetList_byFilter(show_deleted, search).ToDataSourceResult(request));
        }

        #endregion

        #region PrjFile

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        public ActionResult PrjFileSave(IEnumerable<HttpPostedFileBase> uplPrjFile, int claim_id)
        {
            if (uplPrjFile != null)
            {                
                foreach (var file in uplPrjFile)
                {
                    var res = prjFileService.UploadFile(file, claim_id, ModelState);
                    if ((!res) || (!ModelState.IsValid))
                    {
                        if (ModelState.IsValid)
                            ModelState.AddModelError("", "Ошибка при добавлении файла");
                        return Content(ModelState.GetErrorsString());
                    }
                }
            }            
            return Content("");
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [ValidateInput(false)]
        public JsonResult PrjFileRemove(string fileNames, int claim_id)
        {
            var res = prjFileService.RemoveFile(fileNames, claim_id, ModelState);            
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка при удалении файла");
                return Json(new { err = true, mess = ModelState.GetErrorsString(), });
            }
            return Json(new { err = false, mess = "", });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjFileGet(int? file_id)
        {

            if (file_id.GetValueOrDefault(0) > 0)
            {
                PrjFileViewModel res_file = null;                

                res_file = (PrjFileViewModel)prjFileService.GetItem((int)file_id);

                if ((res_file == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при скачивании файла");
                    return RedirectToErrorPage(ModelState);
                }
                    
                var res = GlobalUtil.GetFile(res_file.file_path, Path.Combine(res_file.file_path, res_file.file_name));
                return File(res, System.Net.Mime.MediaTypeNames.Application.Octet, res_file.file_name);
                /*
                System.Net.NetworkCredential writeCredentials = new System.Net.NetworkCredential("Михаил Павлов", "123456", "aptekaural");                            
                if (!System.Diagnostics.Debugger.IsAttached)
                {
                    using (new AuDev.Common.Network.NetworkConnection(res_file.physical_path, writeCredentials))
                    {
                        return File(Path.Combine(res_file.physical_path, res_file.physical_name), System.Net.Mime.MediaTypeNames.Application.Octet, res_file.file_name_with_extention);
                    }
                }
                else
                {
                    return File(Path.Combine(res_file.physical_path, res_file.physical_name), System.Net.Mime.MediaTypeNames.Application.Octet, res_file.file_name_with_extention);
                }
                */

            }
            ModelState.AddModelError("", "Не указан файл");
            return RedirectToErrorPage(ModelState);
        }

        #endregion

        #region PrjClaimFavor

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult PrjClaimFavorAdd(int? claim_id)
        {
            if (claim_id.HasValue)
                /*
                Task.Run(() => {
                    prjClaimService.PrjClaimFavorAdd((int)claim_id);
                });
                */
                prjClaimService.PrjClaimFavorAdd((int)claim_id);
            return new EmptyResult();
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult PrjClaimFavorDel(int? claim_id)
        {
            if (claim_id.HasValue)
                /*
                Task.Run(() =>
                {
                    prjClaimService.PrjClaimFavorDel((int)claim_id);
                });
                */
                prjClaimService.PrjClaimFavorDel((int)claim_id);
            return new EmptyResult();
        }

        #endregion

        #region PrjNotebook

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjNotebookPartial(int? user_id)
        {
            ViewData["cabUserList"] = cachedCabUserList;
            ViewData["workTypeList"] = cachedWorkTypeList;
            //            
            var userSettingsCurrent = cachedUserSettingsVMList != null ? cachedUserSettingsVMList.Where(ss => ss.page_type_id == (int)Enums.PrjPageTypeEnum.NOTEBOOK && ss.is_selected).FirstOrDefault() : null;
            var userSettings = userSettingsCurrent != null ? userSettingsCurrent.prjUserSettingsList : null;
            ViewData["userSettingsCurrent"] = userSettingsCurrent;            
            ViewData["userSettings"] = userSettings;
            //
            //curr_user_id = user_id.GetValueOrDefault(0) > 0 ? (int)user_id : CurrentUser.CabUserId;
            int curr_user_id = 0;
            var userSettings_exec_user = userSettings != null ? userSettings.GetStringValue("exec_user") : null;
            if (user_id.GetValueOrDefault(0) > 0)
            {
                curr_user_id = user_id.GetValueOrDefault(0);
            }
            else if (!String.IsNullOrWhiteSpace(userSettings_exec_user))
            {
                curr_user_id = int.Parse(userSettings_exec_user);
            }
            else
            {
                curr_user_id = CurrentUser.CabUserId;
            }            
            ViewData["currExecUserId"] = curr_user_id;
            ViewData["prjUserGroups"] = prjUserGroupService.GetList_byParent(curr_user_id);
            //
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public JsonResult PrjUserGroupAdd(int? exec_user_id, string new_group_name)
        {
            if (ModelState.IsValid)
            {                
                PrjUserGroupViewModel itemAdd = new PrjUserGroupViewModel()
                {                    
                    group_name = new_group_name,
                    user_id = exec_user_id.GetValueOrDefault(0),
                };
                PrjUserGroupViewModel res = (PrjUserGroupViewModel)prjUserGroupService.Insert(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении группы");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }

                HtmlHelper htmlHelper = this.GetHtmlHelper();
                string group_html = htmlHelper.GetPrjGroupPanel(res, CurrentUser.CabUserId);
                res.group_html = group_html;

                return Json(new { err = false, new_item_html = res.group_html });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public JsonResult PrjUserGroupDel(int? exec_user_id, int? group_id)
        {
            if (ModelState.IsValid)
            {
                PrjUserGroupViewModel itemDel = new PrjUserGroupViewModel()
                {
                    group_id = group_id.GetValueOrDefault(0),
                    user_id = exec_user_id.GetValueOrDefault(0),
                };
                bool res = prjUserGroupService.Delete(itemDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении группы");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjUserGroupItemList([DataSourceRequest]DataSourceRequest request, int? group_id, int? period_id)
        {
            return Json(prjUserGroupItemService.GetList_byFilter(group_id.GetValueOrDefault(0), period_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public JsonResult PrjNoteEdit(int? exec_user_id, int? group_id, int? note_id, string note_text)
        {
            if (ModelState.IsValid)
            {
                PrjNoteViewModel res = null;
                if (note_id.GetValueOrDefault(0) > 0)
                    res = (PrjNoteViewModel)prjNoteService.Update_forGroup(exec_user_id.GetValueOrDefault(0), group_id, note_id, note_text, ModelState);
                else
                    res = (PrjNoteViewModel)prjNoteService.Insert_forGroup(exec_user_id.GetValueOrDefault(0), group_id, note_text, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении записи");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public JsonResult PrjNoteDel(int? exec_user_id, int? group_id, int? note_id)
        {
            if (ModelState.IsValid)
            {
                bool res = prjNoteService.Delete_forGroup(exec_user_id.GetValueOrDefault(0), group_id, note_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении записи");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult PrjNoteReorder(int? source_item_id, int? source_group_id, int? target_group_id, int? ord, int? ord_prev)
        {
            prjUserGroupItemService.Reorder(source_item_id.GetValueOrDefault(0), source_group_id.GetValueOrDefault(0), target_group_id.GetValueOrDefault(0), ord.GetValueOrDefault(0), ord_prev);
            return new EmptyResult();
        }

        #endregion

        #region PrjBrief

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjBriefPartial(int? selected_brief_type_id)
        {
            ViewData["prjBriefTypes"] = prjBriefTypeService.GetList();
            //ViewData["prjBriefIntervalTypes"] = prjBriefIntervalTypeService.GetList();
            ViewData["prjBriefDateCutTypes"] = prjBriefDateCutTypeService.GetList();
            ViewData["prjList"] = cachedPrjList;
            ViewData["cabUserList"] = cachedCabUserList;
            ViewData["projectList"] = cachedProjectList;            
            ViewData["priorityList"] = cachedPriorityList;
            ViewData["claimTypeList"] = cachedClaimTypeList;
            ViewData["claimStageList"] = cachedClaimStageList;
            
            PrjUserSettingsViewModel userSettingsCurrent = null;
            if (selected_brief_type_id.GetValueOrDefault(0) <= 0)
            {
                userSettingsCurrent = cachedUserSettingsVMList != null ? cachedUserSettingsVMList
                    .Where(ss => ss.page_type_id == (int)Enums.PrjPageTypeEnum.BRIEF && ss.is_selected)                    
                    .FirstOrDefault() : null;
            }
            else if (selected_brief_type_id == (int)Enums.PrjBriefEnum.DEV_WORK_MONITORING)
            {
                userSettingsCurrent = cachedUserSettingsVMList != null ? cachedUserSettingsVMList
                    .Where(ss => ss.page_type_id == (int)Enums.PrjPageTypeEnum.BRIEF && ss.tag == "work")
                    .OrderByDescending(ss => ss.is_selected)
                    .FirstOrDefault() : null;
            }
            else if (selected_brief_type_id == (int)Enums.PrjBriefEnum.DEV_TASK_MONITORING)
            {
                userSettingsCurrent = cachedUserSettingsVMList != null ? cachedUserSettingsVMList
                    .Where(ss => ss.page_type_id == (int)Enums.PrjPageTypeEnum.BRIEF && ss.tag == "task")
                    .OrderByDescending(ss => ss.is_selected)
                    .FirstOrDefault() : null;
            }
            else
            {
                userSettingsCurrent = cachedUserSettingsVMList != null ? cachedUserSettingsVMList
                    .Where(ss => ss.page_type_id == (int)Enums.PrjPageTypeEnum.BRIEF && String.IsNullOrWhiteSpace(ss.tag))
                    .OrderByDescending(ss => ss.is_selected)
                    .FirstOrDefault() : null;
            }
            ViewData["userSettingsCurrent"] = userSettingsCurrent;
            Dictionary<string, string> userSettings = userSettingsCurrent != null ? userSettingsCurrent.prjUserSettingsList : null;
            ViewData["userSettings"] = userSettings;

            string userSettings_brief_type = userSettings != null ? userSettings.GetStringValue("brief_type") : "";
            int? userSettings_selected_brief_type = selected_brief_type_id.GetValueOrDefault(0) > 0 ? selected_brief_type_id : (String.IsNullOrWhiteSpace(userSettings_brief_type) ? null : (int?)int.Parse(userSettings_brief_type));
            ViewData["selectedBriefType"] = userSettings_selected_brief_type;
            string userSettings_grid_name = userSettings != null ? userSettings.GetStringValue("brief_grid_name") : "";
            string gridName = "#prjBriefGrid";
            if (userSettings_selected_brief_type == (int)Enums.PrjBriefEnum.DEV_TASK_MONITORING)
            {
                gridName = "#prjBriefTaskGrid";
            }
            else if (userSettings_selected_brief_type == (int)Enums.PrjBriefEnum.DEV_WORK_MONITORING)
            {
                gridName = "#prjBriefWorkGrid";
            }
            ViewData["selectedGridName"] = gridName;
                       

            return PartialView();                        
        }



        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjBriefList([DataSourceRequest]DataSourceRequest request, int? brief_type_id, DateTime? date_beg, DateTime? date_end, int? date_cut_type_id)
        {
            Enums.PrjBriefEnum prjBrief = Enums.PrjBriefEnum.DEV_CLAIM_MONITORING;
            if (!Enum.TryParse<Enums.PrjBriefEnum>(brief_type_id.GetValueOrDefault(0).ToString(), out prjBrief))
                prjBrief = Enums.PrjBriefEnum.DEV_CLAIM_MONITORING;

            switch (prjBrief)
            {
                case Enums.PrjBriefEnum.DEV_CLAIM_MONITORING:
                    return Json(prjClaimService.GetList_Brief(brief_type_id.GetValueOrDefault(0), date_beg, date_end, date_cut_type_id.GetValueOrDefault(0)).ToDataSourceResult(request));
                case Enums.PrjBriefEnum.DEV_WORK_MONITORING:
                    return Json(prjWorkService.GetList_Brief(brief_type_id.GetValueOrDefault(0), date_beg, date_end, date_cut_type_id.GetValueOrDefault(0)).ToDataSourceResult(request));
                case Enums.PrjBriefEnum.DEV_TASK_MONITORING:
                    return Json(prjTaskService.GetList_Brief(brief_type_id.GetValueOrDefault(0), date_beg, date_end, date_cut_type_id.GetValueOrDefault(0)).ToDataSourceResult(request));
                default:
                    return Json(prjClaimService.GetList_Brief(brief_type_id.GetValueOrDefault(0), date_beg, date_end, date_cut_type_id.GetValueOrDefault(0)).ToDataSourceResult(request));
            }            
        }
        #endregion

        #region PrjClaimType

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjClaimTypeList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(prjClaimTypeService.GetList_forEdit().ToDataSourceResult(request));
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public ActionResult PrjClaimTypeEdit([DataSourceRequest]DataSourceRequest request, PrjClaimTypeViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                prjClaimTypeService.Update(itemEdit, ModelState);
            }

            _cachedClaimTypeList = null;
            cabCache.DeleteCachedObject<IEnumerable<PrjClaimViewModel>>();

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region PrjBin

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjBinPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjClaimArchiveList([DataSourceRequest]DataSourceRequest request, string search)
        {
            return Json(prjClaimService.GetList_Archive(search).ToDataSourceResult(request));
        }

        #endregion

        #region PrjClaimFilter

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        public ActionResult GetPrjClaimFilterList()
        {
            return Json(prjClaimFilterService.GetList_byParent(CurrentUser.CabUserId), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjClaimFilterEditPartial(int? filter_id)
        {
            PrjClaimFilterViewModel model = prjClaimFilterService.GetItem_Full(filter_id);
            return PartialView(model);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public ActionResult PrjClaimFilterEdit(int? filter_id, string filter_name, bool is_active, bool is_global, List<PrjClaimFilterItem> filter_items)
        {
            if (ModelState.IsValid)
            {
                PrjClaimFilterViewModel model = new PrjClaimFilterViewModel()
                {
                    filter_id = filter_id.GetValueOrDefault(0),
                    user_id = is_global ? null : (int?)CurrentUser.CabUserId,
                    filter_name = filter_name,
                    is_active = is_active,
                    filter_items_raw = filter_items
                };

                PrjClaimFilterViewModel res = null;
                if (filter_id.GetValueOrDefault(0) <= 0)
                {
                    res = (PrjClaimFilterViewModel)prjClaimFilterService.Insert(model, ModelState);
                }
                else
                {
                    res = (PrjClaimFilterViewModel)prjClaimFilterService.Update(model, ModelState);
                }                    
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении фильтра");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }

                _cachedClaimFilterList = null;
                cabCache.DeleteCachedObject<List<PrjClaimFilterViewModel>>(CurrentUser.CabUserId.ToString());
                if (is_global)
                {
                    foreach (var user in cachedCabUserList)
                    {
                        cabCache.DeleteCachedObject<List<PrjClaimFilterViewModel>>(user.user_id.ToString());
                    }
                }

                return Json(new { err = false, res.filter_id, res.filter_name, res.user_id, res.is_active, res.is_fixed });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public ActionResult PrjClaimFilterDel(int? filter_id)
        {
            if (ModelState.IsValid)
            {
                PrjClaimFilterViewModel model = (PrjClaimFilterViewModel)prjClaimFilterService.GetItem(filter_id.GetValueOrDefault(0));
                if (model == null)
                {
                    ModelState.AddModelError("", "Не найден фильтр с кодом " + filter_id.GetValueOrDefault(0).ToString());
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                bool is_global = !model.user_id.HasValue;

                bool res = prjClaimFilterService.Delete(model, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении фильтра");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }

                _cachedClaimFilterList = null;
                cabCache.DeleteCachedObject<List<PrjClaimFilterViewModel>>(CurrentUser.CabUserId.ToString());
                if (is_global)
                {
                    foreach (var user in cachedCabUserList)
                    {
                        cabCache.DeleteCachedObject<List<PrjClaimFilterViewModel>>(user.user_id.ToString());
                    }
                }

                return Json(new { err = false });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public ActionResult PrjClaimFilterItemsEdit(int? filter_id,  List<PrjClaimFilterItem> filter_items)
        {
            if (ModelState.IsValid)
            {
                PrjClaimFilterViewModel model = (PrjClaimFilterViewModel)prjClaimFilterService.GetItem(filter_id.GetValueOrDefault(0));
                if (model == null)
                {
                    ModelState.AddModelError("", "Не найден фильтр с кодом " + filter_id.GetValueOrDefault(0).ToString());
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }

                model.filter_items_raw = filter_items;

                PrjClaimFilterViewModel res = (PrjClaimFilterViewModel)prjClaimFilterService.Update(model, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении фильтра");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }

                return Json(new { err = false });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }        

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpGet]
        public ActionResult PrjClaimFilterParams(int? filter_id)
        {
            var model = prjClaimFilterService.GetItem_Full(filter_id.GetValueOrDefault(0));
            return View(model);
        }

        #endregion

        #region PrjClaimReport1

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ)]
        [HttpPost]
        public ActionResult PrjClaimReport1()
        {
            var res = prjReport1Service.GetReport1();
            return Json(new { err = false, text = res });
        }

        #endregion
    }
}