﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using System.Data;
using System.Drawing;
using Npgsql;
using NpgsqlTypes;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public class ReportController : BaseController
    {        
        public ActionResult Index()
        {
            return View();
        }

        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_REPORT)]
        [HttpGet]
        public ActionResult ReportList()
        {
            //return View(new ReportParam() { date_beg = GlobalUtil.GetFirstDayLastMonth(), date_end = GlobalUtil.GetLastDayLastMonth(), });
            return View(new ReportParam());
        }

        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_REPORT)]
        public ActionResult GetReports([DataSourceRequest]DataSourceRequest request)
        {
            return Json(reportDiscountService.GetList().ToDataSourceResult(request));
        }

        //[ActionAuthorize("Report_CardInfo", "Report_CheckCount")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_REPORT)]
        [HttpPost]
        public ActionResult ReportList(ReportParam reportParam)
        {
            long business_id = CurrentUser.BusinessId;
            //reportParam.date_beg = String.IsNullOrEmpty(reportParam.date_beg_str) ? null : (DateTime?)Convert.ToDateTime(reportParam.date_beg_str);
            //reportParam.date_end = String.IsNullOrEmpty(reportParam.date_end_str) ? null : (DateTime?)Convert.ToDateTime(reportParam.date_end_str);
            DateTime date_beg = reportParam.date_beg.HasValue ? (DateTime)reportParam.date_beg : new DateTime(1990, 1, 1);
            DateTime date_end = reportParam.date_end.HasValue ? (DateTime)reportParam.date_end : new DateTime(2100, 1, 1);
            string date_beg_str = reportParam.date_beg.HasValue ? date_beg.ToShortDateString() : "[не задано]";
            string date_end_str = reportParam.date_end.HasValue ? date_end.ToShortDateString() : "[не задано]";
            long report_id = reportParam.report_id;
            string connString = CabinetUtil.GetAuMainDbConnectionString();
                       
            string commText = "";
            string commText2 = "";
            string commText_tmp = "";
            string reportTitle = "";
            string reportSubtitle = "Актуальность данных: " + DateTime.Now.ToString("dd.MM.yyyy HH:mm");

            switch ((Enums.ReportType)report_id)
            {
                case Enums.ReportType.CARD_INFO:
                    #region
                    commText = @"select * from discount.vw_report_card_info where business_id = " + business_id.ToString() + " order by card_num";
                    reportTitle = "Общий отчет по картам";
                    break;
                    #endregion
                case Enums.ReportType.CHECK_COUNT:
                    #region
                    commText = @"select count(distinct card_trans_id) as curr_trans_count"
                        + " , sum(unit_value_with_discount) as sum_unit_value_with_discount"
                        + " , sum(unit_value_discount) as sum_unit_value_discount"
                        + " , sum(unit_value) as sum_unit_value"
                        + " , count(distinct detail_id) as curr_unit_count"
                        + " , card_num, curr_discount_percent, curr_trans_sum, card_holder_name, card_type_name"
                        + " , curr_bonus_percent"
                        + " , sum(unit_value_bonus_for_card) as sum_unit_value_bonus_for_card"
                        + " , sum(unit_value_bonus_for_pay) as sum_unit_value_bonus_for_pay"
                        + " , curr_bonus_value"
                        + " from discount.vw_report_check_count where business_id = " + business_id.ToString()
                        + " and date_beg >= '" + date_beg.ToShortDateString() + "'"
                        + " and date_beg <= '" + date_end.ToShortDateString() + "'"
                        + " group by card_num, curr_discount_percent, curr_bonus_percent, curr_bonus_value, curr_trans_sum, card_holder_name, card_type_name"
                        + " order by card_num";
                    reportTitle = "Количество чеков за период c " + date_beg_str + " по " + date_end_str;
                    break;
                    #endregion
                case Enums.ReportType.UNUSED_CARDS:
                    #region
                    commText = @"select * from discount.vw_report_unused_cards where business_id = " + business_id.ToString();
                    if (reportParam.date_beg.HasValue)
                    {
                        commText += " and last_date_check is not null and last_date_check >= '" + date_beg.ToShortDateString() + "'";
                    }
                    if (reportParam.date_end.HasValue)
                    {
                        commText += " and last_date_check is not null and last_date_check <= '" + date_end.ToShortDateString() + "'";
                    }
                    commText += " order by card_num";
                    reportTitle = "Неиспользуемые карты за период c " + date_beg_str + " по " + date_end_str;
                    break;
                    #endregion
                case Enums.ReportType.SVOD_DSCOUNT:
                    #region
                    commText = @"select * from discount.report_discount_svod1";
                    commText2 = @"select * from discount.report_discount_svod2";

                    commText_tmp = @"(" + business_id.ToString() + ", " + ((int)Enums.CardTypeGroup.DISCOUNT).ToString();                    
                    if (reportParam.date_beg.HasValue)
                    {
                        commText_tmp += ", '" + date_beg.ToShortDateString() + "'";
                    }
                    else
                    {
                        commText_tmp += ", null";
                    }

                    if (reportParam.date_end.HasValue)
                    {
                        commText_tmp += ", '" + date_end.ToShortDateString() + "'";
                    }
                    else
                    {
                        commText_tmp += ", null";
                    }

                    //commText_tmp += ") order by org_name, num;";                    

                    commText += commText_tmp + ") order by org_name, num;";
                    commText2 += commText_tmp + ") order by date_check_date_only;";

                    reportTitle = "Сводный отчет по скидкам за период с " + date_beg_str + " по " + date_end_str;
                    break;
                    #endregion
                case Enums.ReportType.CHECK_COUNT2:
                    #region
                    commText = @"select row_number() over(order by (select 0)) as id, t2.business_id, t1.date_check::date as date_check_date_only, t2.card_num, t3.card_holder_surname, t3.card_holder_name, t3.card_holder_fname,"
                        + " sum(t1.trans_sum) as trans_sum, count(t1.card_trans_id) as trans_count"
                        + " from discount.card_transaction t1"
                        + " inner join discount.card t2 on t1.card_id = t2.card_id"
                        + " left join discount.card_holder t3 on t2.curr_holder_id = t3.card_holder_id"
                        + " where t1.trans_kind = 1 and t1.status = 0"
                        + " and t2.business_id = " + business_id.ToString()
                        + " and t1.date_beg >= '" + date_beg.ToShortDateString() + "' and t1.date_beg <= '" + date_end.ToShortDateString() + "'"
                        + " group by t2.business_id, t1.date_check::date, t2.card_num, t3.card_holder_name, t3.card_holder_surname, t3.card_holder_fname"
                        + " having count(t1.card_trans_id) > 1"
                        + " order by t2.business_id, date_check_date_only, trans_count desc, card_num"
                        ;
                    reportTitle = "Количество чеков за день больше одного за период с " + date_beg_str + " по " + date_end_str;
                    break;
                    #endregion
                case Enums.ReportType.FIRST_SALE:
                    #region
                    commText = @"select * from discount.vw_report_card_fstsale where business_id = " + business_id.ToString() + " order by card_num";
                    reportTitle = "Первые продажи по картам";
                    break;
                #endregion
                case Enums.ReportType.ASNA_DISCOUNT:
                    #region
                    commText = @"select extract(year from t2.date_check) as date_check_year, extract(month from t2.date_check) as date_check_month,"
                            + " t12.org_name, t12.org_name_alt, t13.business_name, t13.business_name_alt,"
                            + " coalesce(t1.mess, t5.programm_name) as programm_name, t1.unit_name, 'Чеки'::text as doc_kind, t2.card_trans_id, t1.unit_count, t1.unit_value, t1.unit_value_discount, t1.unit_value_with_discount,"
                            + " t2.date_check"
                            + " from discount.card_transaction_detail t1"
                            + " inner join discount.card_transaction t2 on t1.card_trans_id = t2.card_trans_id"
                            + " and t2.trans_kind = 1 and coalesce(t2.status, 0) = 0"
                            + " and t2.date_check >= '" + date_beg.ToShortDateString() + "' and t2.date_check < '" + date_end.AddDays(1).ToShortDateString() + "'"
                            + " inner join discount.card t3 ON t2.card_id = t3.card_id and t3.business_id = " + business_id.ToString()
                            //+ " and t3.curr_card_type_id in (137, 138, 139)"
                            + " inner join discount.card_type_programm_rel t4 ON t3.curr_card_type_id = t4.card_type_id AND t4.date_end is null"
                            + " inner join discount.programm t5 ON t4.programm_id = t5.programm_id"
                            + " left outer join discount.business_org_user t11 ON btrim(lower(COALESCE(t2.user_name, ''::character varying)::text)) = btrim(lower(COALESCE(t11.user_name, ''::character varying)::text))"
                            + " left outer join discount.business_org t12 ON t11.org_id = t12.org_id"
                            + " left outer join discount.business t13 ON t3.business_id = t13.business_id"
                            + " where t1.unit_type = 0 and coalesce(t1.unit_value_discount, 0) > 0"
                            + " order by extract(year from t2.date_check), extract(month from t2.date_check), t12.org_name, t5.programm_name, t1.unit_name, t2.date_check;"
                            ;
                    reportTitle = "Отчет по дисконтам с " + date_beg_str + " по " + date_end_str;
                    break;
                #endregion
                default:
                    return null;
            }

            if (String.IsNullOrEmpty(commText))
                return null;
            
            string filePath = @"c:\Release\Cabinet\Report\report1.xlsx";
            string contentType = "application/vnd.ms-excel";

            using (Npgsql.NpgsqlConnection conn = new NpgsqlConnection(connString))
            {
                conn.Open();
                NpgsqlCommand comm = new NpgsqlCommand(commText, conn);
                comm.ExecuteNonQuery();

                using (NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(comm.CommandText, conn))
                {
                    DataTable t = new DataTable();
                    dataAdapter.Fill(t);

                    using (ExcelPackage p = new ExcelPackage())
                    {                      
                        p.Workbook.Properties.Author = "Аурит";
                        p.Workbook.Properties.Title = reportTitle;

                        p.Workbook.Worksheets.Add("Страница 1");
                        ExcelWorksheet ws = p.Workbook.Worksheets[1];
                        ws.Cells.Style.Font.Size = 11;
                        ws.Cells.Style.Font.Name = "Calibri";

                        int reportsfieldscount = 5;
                        int colIndex = 1;
                        int rowIndex = 3;
                        int rowIndex_group = 0;
                        string rowIndex_sum_string1 = "";
                        string rowIndex_sum_string2 = "";
                        string rowIndex_sum_string3 = "";
                        string rowIndex_sum_string4 = "";
                        string rowIndex_sum_string5 = "";
                        string rowIndex_sum_string6 = "";
                        decimal sum1 = 0;
                        decimal sum2 = 0;
                        decimal sum3 = 0;
                        decimal sum4 = 0;
                        decimal sum5 = 0;
                        decimal sum6 = 0;
                        int i = 1;

                        long card_num = 0;
                        decimal curr_discount_percent = 0;
                        decimal curr_bonus_percent = 0;                        
                        decimal curr_trans_sum = 0;
                        string card_holder_name = "";
                        string card_holder_surname = "";
                        string card_holder_fname = "";
                        string card_holder_phone_num = "";
                        int curr_trans_count = 0;
                        string card_type_name = "";
                        decimal sum_unit_value = 0;
                        decimal sum_unit_value_discount = 0;
                        decimal sum_unit_value_with_discount = 0;                        
                        decimal sum_unit_value_bonus_for_card = 0;
                        decimal sum_unit_value_bonus_for_pay = 0;
                        decimal sum_tax_value = 0;
                        decimal curr_bonus_value = 0;
                        int curr_unit_count = 0;
                        string last_date_check = "";
                        string org_name = "";
                        string org_name_prev = "";
                        string org_name_alt = "";
                        int num = 0;
                        decimal unit_percent_discount = 0;
                        int pos_count = 0;
                        int check_count = 0;
                        DateTime? date_check_date_only = null;
                        string date_check_date_only_str = "";
                        DateTime? date_check = null;
                        string date_check_str = "";
                        int date_check_year = 0;
                        string date_check_month = "";
                        string business_name = "";
                        string business_name_alt = "";
                        string programm_name = "";
                        string unit_name = "";
                        long card_trans_id = 0;
                        string doc_kind = "";

                        switch ((Enums.ReportType)report_id)
                        {
                            case Enums.ReportType.CARD_INFO:
                                #region
                                reportsfieldscount = 5;
                                i = 1;

                                card_num = 0;
                                curr_discount_percent = 0;
                                curr_trans_sum = 0;
                                card_holder_name = "";
                                curr_trans_count = 0;
                                card_type_name = "";
                                sum_unit_value = 0;
                                sum_unit_value_discount = 0;
                                curr_unit_count = 0;

                                using (var range = ws.Cells[1, 1, rowIndex - 1, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = true;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                while (i <= reportsfieldscount)
                                {
                                    ws.Column(1).Width = 40;
                                    i++;
                                }                                             

                                //Merging cells and create a center heading for out table
                                ws.Cells[rowIndex - 2, 1].Value = reportTitle;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                ws.Cells[rowIndex - 1, 1].Value = reportSubtitle;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "№ карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Процент скидки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма на карте";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во покупок";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Владелец карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;

                                foreach (DataRow row in t.Rows)
                                {

                                    var col1 = row["card_num"];
                                    var col2 = row["curr_discount_percent"];
                                    var col3 = row["curr_trans_sum"];
                                    var col4 = row["card_holder_name"];
                                    var col5 = row["curr_trans_count"];

                                    card_num = col1 is DBNull ? 0 : Convert.ToInt64(col1);
                                    curr_discount_percent = col2 is DBNull ? 0 : Convert.ToDecimal(col2);
                                    curr_trans_sum = col3 is DBNull ? 0 : Convert.ToDecimal(col3);
                                    card_holder_name = col4 is DBNull ? "" : Convert.ToString(col4);
                                    curr_trans_count = col5 is DBNull ? 0 : Convert.ToInt32(col5);
                            
                                    colIndex = 1;
                                    rowIndex++;

                                    ws.Cells[rowIndex, colIndex].Formula = "T(" + "\"" + card_num.ToString() + "\"" +")";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex++;
                            
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_discount_percent;
                                    colIndex++;
                            
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_trans_sum;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = curr_trans_count;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;
                                }
                                #endregion
                                break;
                            case Enums.ReportType.CHECK_COUNT:
                                #region
                                reportsfieldscount = 13;
                                i = 1;

                                card_num = 0;
                                curr_discount_percent = 0;
                                curr_bonus_percent = 0;
                                curr_trans_sum = 0;
                                card_holder_name = "";
                                curr_trans_count = 0;
                                card_type_name = "";
                                sum_unit_value = 0;
                                sum_unit_value_discount = 0;
                                sum_unit_value_bonus_for_card = 0;
                                sum_unit_value_bonus_for_pay = 0;
                                curr_bonus_value = 0;
                                curr_unit_count = 0;

                                using (var range = ws.Cells[1, 1, rowIndex - 1, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = true;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                while (i <= reportsfieldscount)
                                {
                                    ws.Column(1).Width = 40;
                                    i++;
                                }                                             

                                //Merging cells and create a center heading for out table
                                ws.Cells[rowIndex - 2, 1].Value = reportTitle;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                ws.Cells[rowIndex - 1, 1].Value = reportSubtitle;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "№ карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Тип карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Процент скидки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Бонусный процент";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма покупок по карте";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во покупок за период";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма покупок за период";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во отпущенных позиций за период";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма скидок за период";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Бонусы накопленные за период";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Бонусы потраченные за период";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма бонусов на карте";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Владелец карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;

                                foreach (DataRow row in t.Rows)
                                {

                                    var col1 = row["card_num"];
                                    var col2 = row["card_type_name"];
                                    var col3 = row["curr_discount_percent"];
                                    var col4 = row["curr_bonus_percent"];                                    
                                    var col5 = row["curr_trans_sum"];
                                    var col6 = row["curr_trans_count"];                                     
                                    var col7 = row["sum_unit_value"];
                                    var col8 = row["curr_unit_count"];
                                    var col9 = row["sum_unit_value_discount"];
                                    var col10 = row["sum_unit_value_bonus_for_card"];
                                    var col11 = row["sum_unit_value_bonus_for_pay"];
                                    var col12 = row["curr_bonus_value"];                                    
                                    var col13 = row["card_holder_name"];

                                    card_num = col1 is DBNull ? 0 : Convert.ToInt64(col1);
                                    card_type_name = col2 is DBNull ? "" : Convert.ToString(col2);
                                    curr_discount_percent = col3 is DBNull ? 0 : Convert.ToDecimal(col3);
                                    curr_bonus_percent = col4 is DBNull ? 0 : Convert.ToDecimal(col4);
                                    curr_trans_sum = col5 is DBNull ? 0 : Convert.ToDecimal(col5);
                                    curr_trans_count = col6 is DBNull ? 0 : Convert.ToInt32(col6);
                                    sum_unit_value = col7 is DBNull ? 0 : Convert.ToDecimal(col7);
                                    curr_unit_count = col8 is DBNull ? 0 : Convert.ToInt32(col8);
                                    sum_unit_value_discount = col9 is DBNull ? 0 : Convert.ToDecimal(col9);
                                    sum_unit_value_bonus_for_card = col10 is DBNull ? 0 : Convert.ToDecimal(col10);
                                    sum_unit_value_bonus_for_pay = col11 is DBNull ? 0 : Convert.ToDecimal(col11);
                                    curr_bonus_value = col12 is DBNull ? 0 : Convert.ToDecimal(col12);
                                    card_holder_name = col13 is DBNull ? "" : Convert.ToString(col13);
                                                                
                                    colIndex = 1;
                                    rowIndex++;

                                    ws.Cells[rowIndex, colIndex].Formula = "T(" + "\"" + card_num.ToString() + "\"" +")";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    ws.Cells[rowIndex, colIndex].Calculate();                                    
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_type_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    colIndex++;
                            
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_discount_percent;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_bonus_percent;
                                    colIndex++;
                            
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_trans_sum;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = curr_trans_count;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_unit_count;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value_discount;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value_bonus_for_card;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value_bonus_for_pay;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_bonus_value;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;
                                }

                                rowIndex++;

                                colIndex = 1;
                                ws.Cells[rowIndex, colIndex].Value = "Итого:";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                colIndex = 5;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(E3:E" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 6;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(F3:F" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 7;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(G3:G" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 8;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(H3:H" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 9;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(I3:I" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 10;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(K3:K" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 11;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(L3:L" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 12;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(M3:M" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                
                                #endregion
                                break;
                            case Enums.ReportType.UNUSED_CARDS:
                                #region
                                reportsfieldscount = 7;
                                i = 1;

                                card_num = 0;
                                curr_discount_percent = 0;
                                curr_bonus_percent = 0;
                                curr_bonus_value = 0;
                                curr_trans_sum = 0;
                                card_holder_name = "";
                                last_date_check = "";

                                using (var range = ws.Cells[1, 1, rowIndex - 1, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = true;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                while (i <= reportsfieldscount)
                                {
                                    ws.Column(1).Width = 40;
                                    i++;
                                }                                             

                                //Merging cells and create a center heading for out table
                                ws.Cells[rowIndex - 2, 1].Value = reportTitle;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                ws.Cells[rowIndex - 1, 1].Value = reportSubtitle;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "№ карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Процент скидки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Бонусный процент";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма бонусов";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма на карте";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Дата последней покупки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Владелец карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;

                                foreach (DataRow row in t.Rows)
                                {

                                    var col1 = row["card_num"];
                                    var col2 = row["curr_discount_percent"];
                                    var col3 = row["curr_bonus_percent"];
                                    var col4 = row["curr_bonus_value"];
                                    var col5 = row["curr_trans_sum"];
                                    var col6 = row["last_date_check"];
                                    var col7 = row["card_holder_name"];
                                    

                                    card_num = col1 is DBNull ? 0 : Convert.ToInt64(col1);
                                    curr_discount_percent = col2 is DBNull ? 0 : Convert.ToDecimal(col2);
                                    curr_bonus_percent = col3 is DBNull ? 0 : Convert.ToDecimal(col3);
                                    curr_bonus_value = col4 is DBNull ? 0 : Convert.ToDecimal(col4);
                                    curr_trans_sum = col5 is DBNull ? 0 : Convert.ToDecimal(col5);
                                    last_date_check = col6 is DBNull ? "" : Convert.ToString(col6);
                                    card_holder_name = col7 is DBNull ? "" : Convert.ToString(col7);
                                    
                            
                                    colIndex = 1;
                                    rowIndex++;

                                    ws.Cells[rowIndex, colIndex].Formula = "T(" + "\"" + card_num.ToString() + "\"" +")";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex++;
                            
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_discount_percent;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_bonus_percent;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_bonus_value;
                                    colIndex++;
                            
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = curr_trans_sum;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = last_date_check;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;
                                }
                                #endregion
                                break;
                            case Enums.ReportType.SVOD_DSCOUNT:
                                #region
                                reportsfieldscount = 10;
                                i = 1;

                                org_name = "";
                                num = 0;
                                card_type_name = "";
                                unit_percent_discount = 0;
                                sum_unit_value_discount = 0;
                                sum_unit_value = 0;
                                sum_unit_value_with_discount = 0;
                                sum_tax_value = 0;
                                pos_count = 0;
                                check_count = 0;

                                using (var range = ws.Cells[1, 1, rowIndex - 1, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = true;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                while (i <= reportsfieldscount)
                                {
                                    ws.Column(1).Width = 40;
                                    i++;
                                }

                                //Merging cells and create a center heading for out table
                                ws.Cells[rowIndex - 2, 1].Value = reportTitle;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                ws.Cells[rowIndex - 1, 1].Value = reportSubtitle;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                rowIndex++;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "Итого по скидкам:";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                rowIndex++;
                                rowIndex++;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "Отделение";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "№";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Тип карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "% скидки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма скидки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во позиций";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во чеков";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма розничная без скидки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма розничная со скидкой";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма маржи";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;

                                bool firstRow = true;
                                rowIndex_group = rowIndex;
                                foreach (DataRow row in t.Rows)
                                {
                                    var col1 = row["org_name"];
                                    var col2 = row["num"];
                                    var col3 = row["card_type_name"];                                    
                                    var col4 = row["unit_percent_discount"];
                                    var col5 = row["sum_unit_value_discount"];
                                    var col6 = row["pos_count"];
                                    var col7 = row["check_count"];
                                    var col8 = row["sum_unit_value"];
                                    var col9 = row["sum_unit_value_with_discount"];
                                    var col10 = row["sum_tax_value"];

                                    org_name = col1 is DBNull ? "" : Convert.ToString(col1);
                                    num = col2 is DBNull ? 0 : Convert.ToInt32(col2);
                                    card_type_name = col3 is DBNull ? "" : Convert.ToString(col3);
                                    unit_percent_discount = col4 is DBNull ? 0 : Convert.ToDecimal(col4);
                                    sum_unit_value_discount = col5 is DBNull ? 0 : Convert.ToDecimal(col5);
                                    pos_count = col6 is DBNull ? 0 : Convert.ToInt32(col6);
                                    check_count = col7 is DBNull ? 0 : Convert.ToInt32(col7);
                                    sum_unit_value = col8 is DBNull ? 0 : Convert.ToDecimal(col8);
                                    sum_unit_value_with_discount = col9 is DBNull ? 0 : Convert.ToDecimal(col9);
                                    sum_tax_value = col10 is DBNull ? 0 : Convert.ToDecimal(col10);

                                    sum1 += sum_unit_value_discount;
                                    sum2 += pos_count;
                                    sum3 += check_count;
                                    sum4 += sum_unit_value;
                                    sum5 += sum_unit_value_with_discount;
                                    sum6 += sum_tax_value;

                                    colIndex = 1;
                                    rowIndex++;                                    

                                    if ((!(org_name.Trim().ToLower().Equals(org_name_prev.Trim().ToLower()))) && (!firstRow))
                                    {
                                        colIndex = 3;
                                        ws.Cells[rowIndex, colIndex].Value = "Итого";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;

                                        using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                        {
                                            range.Style.Font.Bold = false;
                                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                            range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                            range.Style.Font.Color.SetColor(Color.Black);
                                            range.Style.ShrinkToFit = false;
                                        }

                                        colIndex = 5;
                                        ws.Cells[rowIndex, colIndex].Formula = "SUM(E" + rowIndex_group.ToString() + ":E" + (rowIndex - 1).ToString() + ")";
                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                        ws.Cells[rowIndex, colIndex].Calculate();
                                        colIndex = 6;
                                        ws.Cells[rowIndex, colIndex].Formula = "SUM(F" + rowIndex_group.ToString() + ":F" + (rowIndex - 1).ToString() + ")";
                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                        ws.Cells[rowIndex, colIndex].Calculate();
                                        colIndex = 7;
                                        ws.Cells[rowIndex, colIndex].Formula = "SUM(G" + rowIndex_group.ToString() + ":G" + (rowIndex - 1).ToString() + ")";
                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                        ws.Cells[rowIndex, colIndex].Calculate();
                                        colIndex = 8;
                                        ws.Cells[rowIndex, colIndex].Formula = "SUM(H" + rowIndex_group.ToString() + ":H" + (rowIndex - 1).ToString() + ")";
                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                        ws.Cells[rowIndex, colIndex].Calculate();
                                        colIndex = 9;
                                        ws.Cells[rowIndex, colIndex].Formula = "SUM(I" + rowIndex_group.ToString() + ":I" + (rowIndex - 1).ToString() + ")";
                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                        ws.Cells[rowIndex, colIndex].Calculate();
                                        colIndex = 10;
                                        ws.Cells[rowIndex, colIndex].Formula = "SUM(J" + rowIndex_group.ToString() + ":J" + (rowIndex - 1).ToString() + ")";
                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                        ws.Cells[rowIndex, colIndex].Calculate();

                                        rowIndex_sum_string1 += "E" + rowIndex.ToString() + ";";
                                        rowIndex_sum_string2 += "F" + rowIndex.ToString() + ";";
                                        rowIndex_sum_string3 += "G" + rowIndex.ToString() + ";";
                                        rowIndex_sum_string4 += "H" + rowIndex.ToString() + ";";
                                        rowIndex_sum_string5 += "I" + rowIndex.ToString() + ";";
                                        rowIndex_sum_string6 += "J" + rowIndex.ToString() + ";";

                                        colIndex = 1;
                                        rowIndex++;

                                        rowIndex_group = rowIndex;
                                    }

                                    if (!(org_name.Trim().ToLower().Equals(org_name_prev.Trim().ToLower())))
                                    {
                                        ws.Cells[rowIndex, colIndex].Value = org_name;
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;                                        
                                    }

                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = num;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_type_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = unit_percent_discount;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value_discount;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = pos_count;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = check_count;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value_with_discount;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Value = sum_tax_value;
                                    colIndex++;

                                    org_name_prev = org_name;

                                    firstRow = false;
                                }

                                rowIndex++;

                                colIndex = 3;
                                ws.Cells[rowIndex, colIndex].Value = "Итого";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                colIndex = 5;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(E" + rowIndex_group.ToString() + ":E" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                ws.Cells[rowIndex, colIndex].Calculate();

                                colIndex = 6;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(F" + rowIndex_group.ToString() + ":F" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                ws.Cells[rowIndex, colIndex].Calculate();
                                
                                colIndex = 7;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(G" + rowIndex_group.ToString() + ":G" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                ws.Cells[rowIndex, colIndex].Calculate();

                                colIndex = 8;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(H" + rowIndex_group.ToString() + ":H" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                ws.Cells[rowIndex, colIndex].Calculate();

                                colIndex = 9;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(I" + rowIndex_group.ToString() + ":I" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                ws.Cells[rowIndex, colIndex].Calculate();

                                colIndex = 10;
                                ws.Cells[rowIndex, colIndex].Formula = "SUM(J" + rowIndex_group.ToString() + ":J" + (rowIndex - 1).ToString() + ")";
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                ws.Cells[rowIndex, colIndex].Calculate();


                                rowIndex_sum_string1 += "E" + rowIndex.ToString() + ";";
                                rowIndex_sum_string2 += "F" + rowIndex.ToString() + ";";
                                rowIndex_sum_string3 += "G" + rowIndex.ToString() + ";";
                                rowIndex_sum_string4 += "H" + rowIndex.ToString() + ";";
                                rowIndex_sum_string5 += "I" + rowIndex.ToString() + ";";
                                rowIndex_sum_string6 += "J" + rowIndex.ToString() + ";";

                                colIndex = 1; 
                               
                                rowIndex++;

                                colIndex = 3;
                                ws.Cells[rowIndex, colIndex].Value = "Итого по всем отделениям:";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }
                                /*
                                rowIndex_sum_string1 = rowIndex_sum_string1.TrimEnd(new[] { ';' });
                                rowIndex_sum_string2 = rowIndex_sum_string2.TrimEnd(new[] { ';' });
                                rowIndex_sum_string3 = rowIndex_sum_string3.TrimEnd(new[] { ';' });
                                */
                                colIndex = 5;
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(E3:E" + (rowIndex - 1).ToString() + ")";
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(" + rowIndex_sum_string1 + ")";
                                ws.Cells[rowIndex, colIndex].Value = sum1;
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                //ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 6;
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(F3:F" + (rowIndex - 1).ToString() + ")";
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(" + rowIndex_sum_string2 + ")";
                                ws.Cells[rowIndex, colIndex].Value = sum2;
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                //ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 7;
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(G3:G" + (rowIndex - 1).ToString() + ")";
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(" + rowIndex_sum_string3 + ")";
                                ws.Cells[rowIndex, colIndex].Value = sum3;
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                //ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 8;
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(E3:E" + (rowIndex - 1).ToString() + ")";
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(" + rowIndex_sum_string1 + ")";
                                ws.Cells[rowIndex, colIndex].Value = sum4;
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                //ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 9;
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(E3:E" + (rowIndex - 1).ToString() + ")";
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(" + rowIndex_sum_string1 + ")";
                                ws.Cells[rowIndex, colIndex].Value = sum5;
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                //ws.Cells[rowIndex, colIndex].Calculate();
                                colIndex = 10;
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(E3:E" + (rowIndex - 1).ToString() + ")";
                                //ws.Cells[rowIndex, colIndex].Formula = "SUM(" + rowIndex_sum_string1 + ")";
                                ws.Cells[rowIndex, colIndex].Value = sum6;
                                ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                //ws.Cells[rowIndex, colIndex].Calculate();

                                colIndex = 1;
                                
                                rowIndex++;
                                rowIndex++;
                                rowIndex++;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "Итого по дате:";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                rowIndex++;
                                rowIndex++;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }


                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "№";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Дата";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма скидки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во позиций";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во чеков";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма розничная без скидки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма розничная со скидкой";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма маржи";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;


                                //reportsfieldscount = 5;

                                bool partTwoFirstLine = true;
                                int rowIndex_partTwo = 0;
                                NpgsqlCommand comm2 = new NpgsqlCommand(commText2, conn);
                                comm2.ExecuteNonQuery();
                                using (NpgsqlDataAdapter dataAdapter2 = new NpgsqlDataAdapter(comm2.CommandText, conn))
                                {
                                    DataTable t2 = new DataTable();
                                    dataAdapter2.Fill(t2);
                                    foreach (DataRow row2 in t2.Rows)
                                    {
                                        var col1 = row2["num"];
                                        var col2 = row2["date_check_date_only"];
                                        var col3 = row2["sum_unit_value_discount"];
                                        var col4 = row2["pos_count"];
                                        var col5 = row2["check_count"];
                                        var col6 = row2["sum_unit_value"];
                                        var col7 = row2["sum_unit_value_with_discount"];
                                        var col8 = row2["sum_tax_value"];
                                        
                                        num = col1 is DBNull ? 0 : Convert.ToInt32(col1);
                                        date_check_date_only = col2 is DBNull ? null : (DateTime?)Convert.ToDateTime(col2);
                                        date_check_date_only_str = date_check_date_only == null ? "" : ((DateTime)date_check_date_only).ToString("dd MMMM yyyy", System.Globalization.CultureInfo.CurrentCulture);
                                        sum_unit_value_discount = col3 is DBNull ? 0 : Convert.ToDecimal(col3);
                                        pos_count = col4 is DBNull ? 0 : Convert.ToInt32(col4);
                                        check_count = col5 is DBNull ? 0 : Convert.ToInt32(col5);
                                        sum_unit_value = col6 is DBNull ? 0 : Convert.ToDecimal(col6);
                                        sum_unit_value_with_discount = col7 is DBNull ? 0 : Convert.ToDecimal(col7);
                                        sum_tax_value = col8 is DBNull ? 0 : Convert.ToDecimal(col8);
                                        
                                        colIndex = 1;
                                        rowIndex++;

                                        if (partTwoFirstLine)
                                        {
                                            partTwoFirstLine = false;
                                            rowIndex_partTwo = rowIndex;
                                        }

                                        colIndex++;

                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Value = num;
                                        colIndex++;

                                        ws.Cells[rowIndex, colIndex].Value = date_check_date_only_str;
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                        colIndex++;

                                        colIndex++;

                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Value = sum_unit_value_discount;
                                        colIndex++;

                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Value = pos_count;
                                        colIndex++;

                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Value = check_count;
                                        colIndex++;

                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Value = sum_unit_value;
                                        colIndex++;

                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Value = sum_unit_value_with_discount;
                                        colIndex++;

                                        ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                        ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                        ws.Cells[rowIndex, colIndex].Value = sum_tax_value;
                                        colIndex++;
                                    }

                                    rowIndex++;

                                    colIndex = 3;
                                    ws.Cells[rowIndex, colIndex].Value = "Итого:";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;

                                    using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                    {
                                        range.Style.Font.Bold = false;
                                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                        range.Style.Font.Color.SetColor(Color.Black);
                                        range.Style.ShrinkToFit = false;
                                    }

                                    colIndex = 5;
                                    ws.Cells[rowIndex, colIndex].Formula = "SUM(E" + (rowIndex_partTwo - 1).ToString() + ":E" + (rowIndex - 1).ToString() + ")";
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex = 6;
                                    ws.Cells[rowIndex, colIndex].Formula = "SUM(F" + (rowIndex_partTwo - 1).ToString() + ":F" + (rowIndex - 1).ToString() + ")";
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex = 7;
                                    ws.Cells[rowIndex, colIndex].Formula = "SUM(G" + (rowIndex_partTwo - 1).ToString() + ":G" + (rowIndex - 1).ToString() + ")";
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex = 8;
                                    ws.Cells[rowIndex, colIndex].Formula = "SUM(H" + (rowIndex_partTwo - 1).ToString() + ":H" + (rowIndex - 1).ToString() + ")";
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex = 9;
                                    ws.Cells[rowIndex, colIndex].Formula = "SUM(I" + (rowIndex_partTwo - 1).ToString() + ":I" + (rowIndex - 1).ToString() + ")";
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex = 10;
                                    ws.Cells[rowIndex, colIndex].Formula = "SUM(J" + (rowIndex_partTwo - 1).ToString() + ":J" + (rowIndex - 1).ToString() + ")";
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    ws.Cells[rowIndex, colIndex].Style.Font.Bold = true;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                }

                                #endregion
                                break;
                            case Enums.ReportType.CHECK_COUNT2:
                                #region
                                reportsfieldscount = 7;
                                i = 1;

                                date_check_date_only = null;
                                date_check_date_only_str = "";
                                card_num = 0;
                                card_holder_name = "";
                                card_holder_surname = "";
                                card_holder_fname = "";
                                curr_trans_sum = 0;
                                curr_trans_count = 0;                                

                                using (var range = ws.Cells[1, 1, rowIndex - 1, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = true;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                while (i <= reportsfieldscount)
                                {
                                    ws.Column(1).Width = 40;
                                    i++;
                                }

                                //Merging cells and create a center heading for out table
                                ws.Cells[rowIndex - 2, 1].Value = reportTitle;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                ws.Cells[rowIndex - 1, 1].Value = reportSubtitle;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "Дата";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "№ карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Фамилия";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Имя";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Отчество";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма покупок";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во покупок";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;

                                foreach (DataRow row in t.Rows)
                                {

                                    var col1 = row["date_check_date_only"];
                                    var col2 = row["card_num"];
                                    var col3 = row["card_holder_surname"];
                                    var col4 = row["card_holder_name"];
                                    var col5 = row["card_holder_fname"];
                                    var col6 = row["trans_sum"];
                                    var col7 = row["trans_count"];

                                    date_check_date_only = col1 is DBNull ? null : (DateTime?)Convert.ToDateTime(col1);
                                    date_check_date_only_str = date_check_date_only.HasValue ? ((DateTime)date_check_date_only).ToString("dd.MM.yyyy") : "";
                                    card_num = col2 is DBNull ? 0 : Convert.ToInt64(col2);
                                    card_holder_surname = col3 is DBNull ? "" : Convert.ToString(col3);
                                    card_holder_name = col4 is DBNull ? "" : Convert.ToString(col4);
                                    card_holder_fname = col5 is DBNull ? "" : Convert.ToString(col5);
                                    curr_trans_sum = col6 is DBNull ? 0 : Convert.ToDecimal(col6);
                                    curr_trans_count = col7 is DBNull ? 0 : Convert.ToInt32(col7);    

                                    colIndex = 1;
                                    rowIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = date_check_date_only_str;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Formula = "T(" + "\"" + card_num.ToString() + "\"" + ")";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_surname;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_fname;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = curr_trans_sum;
                                    ws.Cells[rowIndex, colIndex].Style.Numberformat.Format = "0.00";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;                                    
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = curr_trans_count;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                                    colIndex++;
                                }

                                rowIndex++;

                                #endregion
                                break;
                            case Enums.ReportType.FIRST_SALE:
                                #region
                                reportsfieldscount = 6;
                                i = 1;

                                card_num = 0;
                                card_holder_surname = "";
                                card_holder_name = "";
                                card_holder_fname = "";                                
                                card_holder_phone_num = "";
                                org_name = "";

                                using (var range = ws.Cells[1, 1, rowIndex - 1, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = true;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                while (i <= reportsfieldscount)
                                {
                                    ws.Column(1).Width = 40;
                                    i++;
                                }

                                //Merging cells and create a center heading for out table
                                ws.Cells[rowIndex - 2, 1].Value = reportTitle;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                ws.Cells[rowIndex - 1, 1].Value = reportSubtitle;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "№ карты";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Фамилия";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Имя";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Отчество";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Телефон";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Адрес первой продажи";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;                                

                                foreach (DataRow row in t.Rows)
                                {

                                    var col1 = row["card_num"];
                                    var col2 = row["curr_holder_name"];
                                    var col3 = row["curr_holder_first_name"];
                                    var col4 = row["curr_holder_second_name"];
                                    var col5 = row["curr_holder_phone_num"];
                                    var col6 = row["card_fstsale_org_name"];
                                    

                                    card_num = col1 is DBNull ? 0 : Convert.ToInt64(col1);
                                    card_holder_surname = col2 is DBNull ? "" : Convert.ToString(col2);
                                    card_holder_name = col3 is DBNull ? "" : Convert.ToString(col3);
                                    card_holder_fname = col4 is DBNull ? "" : Convert.ToString(col4);
                                    card_holder_phone_num = col5 is DBNull ? "" : Convert.ToString(col5);
                                    org_name = col6 is DBNull ? "" : Convert.ToString(col6);
                                    

                                    colIndex = 1;
                                    rowIndex++;

                                    ws.Cells[rowIndex, colIndex].Formula = "T(" + "\"" + card_num.ToString() + "\"" + ")";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_surname;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_fname;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = card_holder_phone_num;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = org_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;
                                }
                                #endregion
                                break;
                            case Enums.ReportType.ASNA_DISCOUNT:
                                #region
                                reportsfieldscount = 15;
                                i = 1;

                                date_check_year = 0;
                                date_check_month = "";
                                org_name = "";
                                org_name_alt = "";
                                business_name = "";
                                business_name_alt = "";
                                programm_name = "";
                                unit_name = "";
                                card_trans_id = 0;
                                curr_unit_count = 0;
                                sum_unit_value = 0;
                                sum_unit_value_discount = 0;
                                sum_unit_value_with_discount = 0;
                                doc_kind = "";
                                date_check = null;
                                date_check_str = "";

                                using (var range = ws.Cells[1, 1, rowIndex - 1, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = true;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                while (i <= reportsfieldscount)
                                {
                                    ws.Column(1).Width = 40;
                                    i++;
                                }

                                //Merging cells and create a center heading for out table
                                ws.Cells[rowIndex - 2, 1].Value = reportTitle;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                ws.Cells[rowIndex - 1, 1].Value = reportSubtitle;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Merge = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.Font.Bold = true;
                                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                                {
                                    range.Style.Font.Bold = false;
                                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                                    range.Style.Font.Color.SetColor(Color.Black);
                                    range.Style.ShrinkToFit = false;
                                }

                                ws.Cells[rowIndex, colIndex].Value = "Год";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Месяц";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Название аптеки (как зарегистрировано в АСНА)";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Название сети (в АСНА)";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Юридическое лицо";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Фактический адрес аптеки";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Название Дисконтной программы";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Товар";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Вид документа";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Номер чека";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Кол-во, уп.";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма реализации (до скидки)";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма скидки по дисконту";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Сумма реализации (после скидки)";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                colIndex++;
                                ws.Cells[rowIndex, colIndex].Value = "Дата чека";
                                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                foreach (DataRow row in t.Rows)
                                {
                                    var col1 = row["date_check_year"];
                                    var col2 = row["date_check_month"];
                                    var col3 = row["org_name_alt"];
                                    var col4 = row["business_name_alt"];
                                    var col5 = row["business_name"];
                                    var col6 = row["org_name"];
                                    var col7 = row["programm_name"];
                                    var col8 = row["unit_name"];
                                    var col9 = row["doc_kind"];
                                    var col10 = row["card_trans_id"];
                                    var col11 = row["unit_count"];
                                    var col12 = row["unit_value"];
                                    var col13 = row["unit_value_discount"];
                                    var col14 = row["unit_value_with_discount"];
                                    var col15 = row["date_check"];

                                    date_check_year = col1 is DBNull ? 0 : Convert.ToInt32(col1);
                                    date_check_month = col2 is DBNull ? "" : Convert.ToString(col2);
                                    org_name_alt = col3 is DBNull ? "" : Convert.ToString(col3);
                                    business_name_alt = col4 is DBNull ? "" : Convert.ToString(col4);
                                    business_name = col5 is DBNull ? "" : Convert.ToString(col5);
                                    org_name = col6 is DBNull ? "" : Convert.ToString(col6);                                    
                                    programm_name = col7 is DBNull ? "" : Convert.ToString(col7);
                                    unit_name = col8 is DBNull ? "" : Convert.ToString(col8);
                                    doc_kind = col9 is DBNull ? "" : Convert.ToString(col9);
                                    card_trans_id = col10 is DBNull ? 0 : Convert.ToInt64(col10);
                                    curr_unit_count = col11 is DBNull ? 0 : Convert.ToInt32(col11);
                                    sum_unit_value = col12 is DBNull ? 0 : Convert.ToDecimal(col12);
                                    sum_unit_value_discount = col13 is DBNull ? 0 : Convert.ToDecimal(col13);
                                    sum_unit_value_with_discount = col14 is DBNull ? 0 : Convert.ToDecimal(col14);
                                    date_check = col15 is DBNull ? null : (DateTime?)Convert.ToDateTime(col15);
                                    date_check_str = date_check.HasValue ? ((DateTime)date_check).ToString("dd.MM.yyyy HH:mm:ss") : "";

                                    colIndex = 1;
                                    rowIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = date_check_year;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = date_check_month;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = org_name_alt;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = business_name_alt;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = business_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = org_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = programm_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = unit_name;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = doc_kind;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Formula = "T(" + "\"" + card_trans_id.ToString() + "\"" + ")";
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    ws.Cells[rowIndex, colIndex].Calculate();
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = curr_unit_count;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value_discount;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = sum_unit_value_with_discount;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;

                                    ws.Cells[rowIndex, colIndex].Value = date_check_str;
                                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                                    colIndex++;
                                }
                                #endregion
                                break;
                            default:
                                return null;
                        }

                        // AutoFitColumns
                        ws.Cells[ws.Dimension.Address].AutoFitColumns();

                        Byte[] bin = p.GetAsByteArray();
                        System.IO.File.WriteAllBytes(filePath, bin);
                    }
                    
                    ViewBag.Message = "Файл отчета сохранен в " + filePath;
                    
                    return File(filePath, contentType, "Report1.xlsx");
                }
            }
        }

    }
}

