﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Defect;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public class DefectController : BaseController
    {        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DEFECT)]
        [AuthUserPermView(Enums.AuthPartEnum.DEFECT_LIST)]
        public ActionResult Index()
        {
            return View();
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DEFECT)]
        [AuthUserPermView(Enums.AuthPartEnum.DEFECT_LIST)]
        public ActionResult DefectList()
        {
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DEFECT)]
        [AuthUserPermView(Enums.AuthPartEnum.DEFECT_LIST)]
        public ActionResult GetDefectList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(defectService.GetList().ToDataSourceResult(request));
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DEFECT)]
        [AuthUserPermView(Enums.AuthPartEnum.DEFECT_LOG)]
        public ActionResult DefectRequestList()
        {
            return View();
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DEFECT)]
        [AuthUserPermView(Enums.AuthPartEnum.DEFECT_LOG)]
        public ActionResult GetDefectRequestList([DataSourceRequest]DataSourceRequest request)
        {
            if (CurrentUser.IsAdmin)
            {
                return Json(defectRequestService.GetList().ToDataSourceResult(request));
            }
            else
            {
                return Json(defectRequestService.GetList_byParent(CurrentUser.CabClientId).ToDataSourceResult(request));
            }
        }
                        
        [AllowAnonymous]
        [HttpGet]
        public FileResult DefectDownload(string workplace)
        {
            //bool ok = ((CurrentUser != null) && (CurrentUser.HaveDefect));
            bool ok = ((CurrentUser != null) && (CurrentUser.IsAdmin));
            if ((!ok) && (String.IsNullOrEmpty(workplace)))
            {
                ok = defectService.IsDefectDownloadAllowed_forClient(CurrentUser.CabClientId);
            }
            if (!ok)
            {
                if (String.IsNullOrEmpty(workplace))
                    return null;
                ok = defectService.IsDefectDownloadAllowed(workplace);
                //if (!workplace.Trim().Equals("pleasepleaseplease"))                    
            }
            if (!ok)
                return null;

            byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\Release\Defect\File\Arc\remove.zip");
            string fileName = "remove.zip";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

    }
}
