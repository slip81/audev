﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.Util;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Ved;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
using NPOI.HSSF.UserModel;
#endregion

namespace CabinetMvc.Controllers
{
    public class VedController : BaseController
    {        
        #region Sprav

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetSprav_ArcType([DataSourceRequest]DataSourceRequest request)
        {
            List<SimpleCombo> arcTypeList = GlobalUtil.CreateSimpleCombo<Enums.ArcType>();
            return Json(arcTypeList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region GetRegionList

        private IQueryable<AuBaseViewModel> GetRegionList()
        {
            var s1 = vedSourceRegionService.GetList().AsEnumerable().Cast<VedSourceRegionViewModel>();
            if ((s1 == null) || (s1.Count() <= 0))
                return null;

            var s2 = s1.Select(ss => ss.region_id).Distinct().ToList();
            if ((s2 == null) || (s2.Count <= 0))
                return null;

            return spravRegionService.GetList_byIdList(s2);            
        }

        #endregion

        #region VedSource

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedSourceEdit()
        {
            var vedSourceEdit = vedSourceService.GetItem_Single();
            if ((!(ModelState.IsValid)) || (vedSourceEdit == null))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("error", "Не найден источник госреестра ЖНВЛП");
                return RedirectToErrorPage(ModelState);
            }            
            ViewBag.ArcTypeList = GlobalUtil.CreateSimpleCombo<Enums.ArcType>();
            ViewBag.TemplateList = vedTemplateService.GetList_Sprav();
            return View(vedSourceEdit);            
        }

        [HttpPost]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedSourceEdit(VedSourceViewModel vedSourceEdit)
        {
            ViewBag.ArcTypeList = GlobalUtil.CreateSimpleCombo<Enums.ArcType>();
            ViewBag.TemplateList = vedTemplateService.GetList_Sprav();
            if (ModelState.IsValid)
            {
                var res = vedSourceService.Update(vedSourceEdit, ModelState);
                if ((!(ModelState.IsValid)) || (res == null))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("error", "Ошибка при корректировании источника");
                    return RedirectToErrorPage(ModelState);
                }
                return View(vedSourceEdit);
            }
            return View(vedSourceEdit);
        }

        #endregion

        #region VedSourceRegion

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedSourceRegionList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedSourceRegionList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(vedSourceRegionService.GetList().ToDataSourceResult(request));
        }


        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedSourceRegionEdit(long item_id)
        {
            VedSourceRegionViewModel vedSourceRegionEdit = (VedSourceRegionViewModel)vedSourceRegionService.GetItem(item_id);
            if (vedSourceRegionEdit == null)
                vedSourceRegionEdit = new VedSourceRegionViewModel() { AddMode = true, };
            else
                vedSourceRegionEdit.AddMode = false;
            if (!(ModelState.IsValid))
            {
                return RedirectToErrorPage(ModelState);
            }
            ViewBag.ArcTypeList = GlobalUtil.CreateSimpleCombo<Enums.ArcType>();
            ViewBag.TemplateList = vedTemplateService.GetList_Sprav();
            ViewBag.RegionList = spravRegionService.GetList();
            return View(vedSourceRegionEdit);
        }

        [HttpPost]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedSourceRegionEdit(VedSourceRegionViewModel vedSourceRegionEdit)
        {
            if (vedSourceRegionEdit == null)
            {
                ModelState.AddModelError("error", "Не указан источник");
                return RedirectToErrorPage(ModelState);
            }

            ViewBag.ArcTypeList = GlobalUtil.CreateSimpleCombo<Enums.ArcType>();
            ViewBag.TemplateList = vedTemplateService.GetList_Sprav();
            ViewBag.RegionList = spravRegionService.GetList();
            if (ModelState.IsValid)
            {
                var res = vedSourceRegionEdit.AddMode ? vedSourceRegionService.Insert(vedSourceRegionEdit, ModelState) : vedSourceRegionService.Update(vedSourceRegionEdit, ModelState);
                if ((!(ModelState.IsValid)) || (res == null))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("error", "Ошибка при корректировании источника");
                    return RedirectToErrorPage(ModelState);
                }
                ((VedSourceRegionViewModel)res).AddMode = false;
                return View(vedSourceRegionEdit);
            }
            return View(vedSourceRegionEdit);
        }

        [HttpPost]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedSourceRegionDel([DataSourceRequest] DataSourceRequest request, VedSourceRegionViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                vedSourceRegionService.Delete(itemDel, ModelState);
            }
            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region VedReestrItem

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedReestrItemList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedReestrItemList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(vedReestrItemService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedReestrItemFreeList([DataSourceRequest]DataSourceRequest request, int region_id)
        {
            return Json(vedReestrItemService.GetList_Free(region_id).ToDataSourceResult(request));
        }
        

        #endregion

        #region VedReestrItemOut

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedReestrItemOutList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedReestrItemOutList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(vedReestrItemOutService.GetList().ToDataSourceResult(request));
        }

        #endregion

        #region VedReestrRegionItem

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedReestrRegionItemList()
        {
            //ViewBag.RegionList = GetService<SpravRegionService>().GetList();
            ViewBag.RegionList = GetRegionList();          
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedReestrRegionItemList([DataSourceRequest]DataSourceRequest request, long region_id)
        {
            return Json(vedReestrRegionItemService.GetList_byParent(region_id).ToDataSourceResult(request));
        }

        #endregion

        #region VedReestrRegionItemOut

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedReestrRegionItemOutList()
        {
            //ViewBag.RegionList = GetService<SpravRegionService>().GetList();
            ViewBag.RegionList = GetRegionList();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedReestrRegionItemOutList([DataSourceRequest]DataSourceRequest request, long region_id)
        {
            return Json(vedReestrRegionItemOutService.GetList_byParent(region_id).ToDataSourceResult(request));
        }

        #endregion

        #region VedReestrRegionItemError

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedReestrRegionItemErrorList()
        {
            //ViewBag.RegionList = GetService<SpravRegionService>().GetList();
            ViewBag.RegionList = GetRegionList();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedReestrRegionItemErrorList([DataSourceRequest]DataSourceRequest request, long region_id)
        {
            return Json(vedReestrRegionItemErrorService.GetList_byParent(region_id).ToDataSourceResult(request));
        }

        #endregion

        #region VedRequest

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedRequestList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedRequestList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(vedRequestService.GetList().ToDataSourceResult(request));
        }


        #endregion

        #region VedTemplate

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedTemplateList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetVedTemplateList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(vedTemplateService.GetList().ToDataSourceResult(request));
        }

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedTemplateEdit(long template_id)
        {
            VedTemplateViewModel vedTemplateEdit = (VedTemplateViewModel)vedTemplateService.GetItem(template_id);
            if (vedTemplateEdit == null)
                vedTemplateEdit = new VedTemplateViewModel() { AddMode = true, };
            else
                vedTemplateEdit.AddMode = false;
            if (!(ModelState.IsValid))
            {
                return RedirectToErrorPage(ModelState);
            }

            ViewBag.ColumnList = vedTemplateEdit.AddMode ? vedTemplateColumnService.GetList_forNewTemplate() : vedTemplateColumnService.GetList_byParent(vedTemplateEdit.template_id);
            return View(vedTemplateEdit);
        }

        [HttpPost]
        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult VedTemplateEdit(IEnumerable<VedTemplateColumnViewModel> templ_data, long template_id, string template_name, int start_row_num, string list_name)
        {
            VedTemplateViewModel vedTemplateEdit = new VedTemplateViewModel() 
            {
                AddMode = template_id <= 0,
                list_name = list_name,
                start_row_num = start_row_num,
                template_name = template_name,
                template_id = template_id,
                template_columns = templ_data,
            };

            if (vedTemplateEdit == null)
            {
                ModelState.AddModelError("error", "Не указан шаблон");
                return RedirectToErrorPage(ModelState);
            }

            if (ModelState.IsValid)
            {
                var res = vedTemplateEdit.AddMode ? vedTemplateService.Insert(vedTemplateEdit, ModelState) : vedTemplateService.Update(vedTemplateEdit, ModelState);
                if ((!(ModelState.IsValid)) || (res == null))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("error", "Ошибка при корректировании шаблона");
                    return RedirectToErrorPage(ModelState);
                }
                ((VedTemplateViewModel)res).AddMode = false;
                ViewBag.Message = "Шаблон сохранен !";
                ViewBag.ColumnList = vedTemplateColumnService.GetList_byParent(((VedTemplateViewModel)res).template_id);
                return View(vedTemplateEdit);
            }
            else
            {
                ViewBag.ColumnList = vedTemplateColumnService.GetList_forNewTemplate();
                return View(vedTemplateEdit);
            }
        }

        [HttpPost]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult VedTemplateDel([DataSourceRequest] DataSourceRequest request, VedTemplateViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                vedTemplateService.Delete(itemDel, ModelState);
            }
            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }


        #endregion

        #region VedExport

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public FileResult VedReestrItemExport([DataSourceRequest]DataSourceRequest request, int region_id)
        {
            string title = "";
            IEnumerable<VedReestrItemViewModel> items = null;
            IEnumerable<VedReestrRegionItemViewModel> items2 = null;


            if (region_id <= 0)
            {
                title = "Список основных строк госреестра ЖНВЛП от " + @DateTime.Today.ToString("dd.MM.yyyy") + ".xls";
                //Get the data representing the current grid state - page, sort and filter
                items = vedReestrItemService.GetList().AsEnumerable().Cast<VedReestrItemViewModel>();
            }
            else
            {
                title = "Список основных строк реестра ЖНВЛП от " + @DateTime.Today.ToString("dd.MM.yyyy") + ".xls";
                //Get the data representing the current grid state - page, sort and filter
                items2 = vedReestrRegionItemService.GetList_byParent(region_id).AsEnumerable().Cast<VedReestrRegionItemViewModel>();
            }

          

            //Create new Excel workbook
            var workbook = new HSSFWorkbook();

            //Create new Excel sheet
            var sheet = workbook.CreateSheet();

            HSSFFont boldFont = (HSSFFont)workbook.CreateFont();
            boldFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            HSSFCellStyle boldFontStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            boldFontStyle.SetFont(boldFont);
            HSSFCell cell;

            //(Optional) set the width of the columns
            sheet.SetColumnWidth(0, 50 * 256);
            sheet.SetColumnWidth(1, 50 * 256);
            sheet.SetColumnWidth(2, 50 * 256);
            sheet.SetColumnWidth(3, 50 * 256);
            sheet.SetColumnWidth(4, 50 * 256);
            sheet.SetColumnWidth(5, 50 * 256);
            sheet.SetColumnWidth(6, 50 * 256);
            sheet.SetColumnWidth(7, 50 * 256);
            sheet.SetColumnWidth(8, 50 * 256);
            sheet.SetColumnWidth(9, 50 * 256);

            //Create a title row
            var titleRow = sheet.CreateRow(0);
            cell = (HSSFCell)titleRow.CreateCell(0);
            cell.SetCellValue(title);
            cell.CellStyle = boldFontStyle;

            //Create a header row
            var headerRow = sheet.CreateRow(1);

            //Set the column names in the header row            
            cell = (HSSFCell)headerRow.CreateCell(0);
            cell.SetCellValue("МНН");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(1);
            cell.SetCellValue("Торговое наименование");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(2);
            cell.SetCellValue("Упаковка");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(3);
            cell.SetCellValue("Изготовитель");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(4);
            cell.SetCellValue("Кол-во в упаковке");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(5);
            cell.SetCellValue("Предельная цена");
            cell.CellStyle = boldFontStyle;            
            cell = (HSSFCell)headerRow.CreateCell(6);
            cell.SetCellValue("№ РУ");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(7);
            cell.SetCellValue("Дата решения");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(8);
            cell.SetCellValue("Номер решения");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(9);
            cell.SetCellValue("Штрих-код");
            cell.CellStyle = boldFontStyle;

            //(Optional) freeze the header row so it is not scrolled
            //sheet.CreateFreezePane(0, 1, 0, 1);

            int rowNumber = 2;

            //Populate the sheet with values from the grid data
            if (region_id <= 0)
            {
                foreach (var item in items)
                {
                    //Create a new row                
                    var row = sheet.CreateRow(rowNumber++);

                    //Set values for the cells
                    row.CreateCell(0).SetCellValue(item.mnn);
                    row.CreateCell(1).SetCellValue(item.comm_name);
                    row.CreateCell(2).SetCellValue(item.pack_name);
                    row.CreateCell(3).SetCellValue(item.producer);
                    row.CreateCell(4).SetCellValue(item.pack_count.HasValue ? item.pack_count.ToString() : "");
                    row.CreateCell(5, NPOI.SS.UserModel.CellType.Numeric).SetCellValue(item.limit_price.HasValue ? item.limit_price.ToString() : "");
                    row.CreateCell(6).SetCellValue(item.reg_num);
                    row.CreateCell(7).SetCellValue(item.price_reg_date);
                    row.CreateCell(8).SetCellValue(item.price_reg_ndoc);
                    row.CreateCell(9).SetCellValue(item.ean13);
                }
            }
            else
            {
                foreach (var item2 in items2)
                {
                    //Create a new row                
                    var row = sheet.CreateRow(rowNumber++);

                    //Set values for the cells
                    row.CreateCell(0).SetCellValue(item2.mnn);
                    row.CreateCell(1).SetCellValue(item2.comm_name);
                    row.CreateCell(2).SetCellValue(item2.pack_name);
                    row.CreateCell(3).SetCellValue(item2.producer);
                    row.CreateCell(4).SetCellValue(item2.pack_count.HasValue ? item2.pack_count.ToString() : "");
                    row.CreateCell(5, NPOI.SS.UserModel.CellType.Numeric).SetCellValue(item2.limit_price.HasValue ? item2.limit_price.ToString() : "");
                    row.CreateCell(6).SetCellValue(item2.reg_num);
                    row.CreateCell(7).SetCellValue(item2.price_reg_date);
                    row.CreateCell(8).SetCellValue(item2.price_reg_ndoc);
                    row.CreateCell(9).SetCellValue(item2.ean13);
                }
            }

            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel", //MIME type of Excel files
                title);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public FileResult VedReestrItemOutExport([DataSourceRequest]DataSourceRequest request, int region_id)
        {
            string title = "";
            IEnumerable<VedReestrItemOutViewModel> items = null;
            IEnumerable<VedReestrRegionItemOutViewModel> items2 = null;


            if (region_id <= 0)
            {
                title = "Список исключенных строк госреестра ЖНВЛП от " + @DateTime.Today.ToString("dd.MM.yyyy") + ".xls";
                //Get the data representing the current grid state - page, sort and filter
                items = vedReestrItemOutService.GetList().AsEnumerable().Cast<VedReestrItemOutViewModel>();
            }
            else
            {
                title = "Список исключенных строк реестра ЖНВЛП от " + @DateTime.Today.ToString("dd.MM.yyyy") + ".xls";
                //Get the data representing the current grid state - page, sort and filter
                items2 = vedReestrRegionItemOutService.GetList_byParent(region_id).AsEnumerable().Cast<VedReestrRegionItemOutViewModel>();
            }

            //Create new Excel workbook
            var workbook = new HSSFWorkbook();

            //Create new Excel sheet
            var sheet = workbook.CreateSheet();

            HSSFFont boldFont = (HSSFFont)workbook.CreateFont();
            boldFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            HSSFCellStyle boldFontStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            boldFontStyle.SetFont(boldFont);
            HSSFCell cell;

            //(Optional) set the width of the columns
            sheet.SetColumnWidth(0, 50 * 256);
            sheet.SetColumnWidth(1, 50 * 256);
            sheet.SetColumnWidth(2, 50 * 256);
            sheet.SetColumnWidth(3, 50 * 256);
            sheet.SetColumnWidth(4, 50 * 256);
            sheet.SetColumnWidth(5, 50 * 256);
            sheet.SetColumnWidth(6, 50 * 256);
            sheet.SetColumnWidth(7, 50 * 256);
            sheet.SetColumnWidth(8, 50 * 256);
            sheet.SetColumnWidth(9, 50 * 256);
            sheet.SetColumnWidth(10, 50 * 256);
            sheet.SetColumnWidth(11, 50 * 256);

            //Create a title row
            var titleRow = sheet.CreateRow(0);
            cell = (HSSFCell)titleRow.CreateCell(0);
            cell.SetCellValue(title);
            cell.CellStyle = boldFontStyle;

            //Create a header row
            var headerRow = sheet.CreateRow(1);

            //Set the column names in the header row            
            cell = (HSSFCell)headerRow.CreateCell(0);
            cell.SetCellValue("МНН");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(1);
            cell.SetCellValue("Торговое наименование");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(2);
            cell.SetCellValue("Упаковка");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(3);
            cell.SetCellValue("Изготовитель");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(4);
            cell.SetCellValue("Кол-во в упаковке");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(5);
            cell.SetCellValue("Предельная цена");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(6);
            cell.SetCellValue("№ РУ");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(7);
            cell.SetCellValue("Дата решения");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(8);
            cell.SetCellValue("Номер решения");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(9);
            cell.SetCellValue("Штрих-код");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(10);
            cell.SetCellValue("Дата исключения");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(11);
            cell.SetCellValue("Причина исключения");
            cell.CellStyle = boldFontStyle;

            //(Optional) freeze the header row so it is not scrolled
            //sheet.CreateFreezePane(0, 1, 0, 1);

            int rowNumber = 2;

            //Populate the sheet with values from the grid data
            if (region_id <= 0)
            {
                foreach (var item in items)
                {
                    //Create a new row                
                    var row = sheet.CreateRow(rowNumber++);

                    //Set values for the cells
                    row.CreateCell(0).SetCellValue(item.mnn);
                    row.CreateCell(1).SetCellValue(item.comm_name);
                    row.CreateCell(2).SetCellValue(item.pack_name);
                    row.CreateCell(3).SetCellValue(item.producer);
                    row.CreateCell(4).SetCellValue(item.pack_count.HasValue ? item.pack_count.ToString() : "");
                    row.CreateCell(5, NPOI.SS.UserModel.CellType.Numeric).SetCellValue(item.limit_price.HasValue ? item.limit_price.ToString() : "");
                    row.CreateCell(6).SetCellValue(item.reg_num);
                    row.CreateCell(7).SetCellValue(item.price_reg_date);
                    row.CreateCell(8).SetCellValue(item.price_reg_ndoc);
                    row.CreateCell(9).SetCellValue(item.ean13);
                    row.CreateCell(10).SetCellValue(item.out_date.HasValue ? ((DateTime)item.out_date).ToString("dd.MM.yyyy") : "");
                    row.CreateCell(11).SetCellValue(item.out_reazon);
                }
            }
            else
            {
                foreach (var item2 in items2)
                {
                    //Create a new row                
                    var row = sheet.CreateRow(rowNumber++);

                    //Set values for the cells
                    row.CreateCell(0).SetCellValue(item2.mnn);
                    row.CreateCell(1).SetCellValue(item2.comm_name);
                    row.CreateCell(2).SetCellValue(item2.pack_name);
                    row.CreateCell(3).SetCellValue(item2.producer);
                    row.CreateCell(4).SetCellValue(item2.pack_count.HasValue ? item2.pack_count.ToString() : "");
                    row.CreateCell(5, NPOI.SS.UserModel.CellType.Numeric).SetCellValue(item2.limit_price.HasValue ? item2.limit_price.ToString() : "");
                    row.CreateCell(6).SetCellValue(item2.reg_num);
                    row.CreateCell(7).SetCellValue(item2.price_reg_date);
                    row.CreateCell(8).SetCellValue(item2.price_reg_ndoc);
                    row.CreateCell(9).SetCellValue(item2.ean13);
                    row.CreateCell(10).SetCellValue(item2.out_date.HasValue ? ((DateTime)item2.out_date).ToString("dd.MM.yyyy") : "");
                    row.CreateCell(11).SetCellValue(item2.out_reazon);
                }
            }

            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel", //MIME type of Excel files
                title);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public FileResult VedReestrItemErrorExport([DataSourceRequest]DataSourceRequest request, int region_id)
        {
            string title = "";
            
            IEnumerable<VedReestrRegionItemErrorViewModel> items2 = null;

            title = "Список несопоставленных строк реестра ЖНВЛП от " + @DateTime.Today.ToString("dd.MM.yyyy") + ".xls";
            //Get the data representing the current grid state - page, sort and filter            
            items2 = vedReestrRegionItemErrorService.GetList_byParent(region_id).AsEnumerable().Cast<VedReestrRegionItemErrorViewModel>();
            
            //Create new Excel workbook
            var workbook = new HSSFWorkbook();

            //Create new Excel sheet
            var sheet = workbook.CreateSheet();

            HSSFFont boldFont = (HSSFFont)workbook.CreateFont();
            boldFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            HSSFCellStyle boldFontStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            boldFontStyle.SetFont(boldFont);
            HSSFCell cell;

            //(Optional) set the width of the columns
            sheet.SetColumnWidth(0, 50 * 256);
            sheet.SetColumnWidth(1, 50 * 256);
            sheet.SetColumnWidth(2, 50 * 256);
            sheet.SetColumnWidth(3, 50 * 256);
            sheet.SetColumnWidth(4, 50 * 256);
            sheet.SetColumnWidth(5, 50 * 256);
            sheet.SetColumnWidth(6, 50 * 256);
            sheet.SetColumnWidth(7, 50 * 256);
            sheet.SetColumnWidth(8, 50 * 256);
            sheet.SetColumnWidth(9, 50 * 256);

            //Create a title row
            var titleRow = sheet.CreateRow(0);
            cell = (HSSFCell)titleRow.CreateCell(0);
            cell.SetCellValue(title);
            cell.CellStyle = boldFontStyle;

            //Create a header row
            var headerRow = sheet.CreateRow(1);

            //Set the column names in the header row            
            cell = (HSSFCell)headerRow.CreateCell(0);
            cell.SetCellValue("МНН");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(1);
            cell.SetCellValue("Торговое наименование");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(2);
            cell.SetCellValue("Упаковка");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(3);
            cell.SetCellValue("Изготовитель");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(4);
            cell.SetCellValue("Кол-во в упаковке");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(5);
            cell.SetCellValue("Предельная цена");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(6);
            cell.SetCellValue("№ РУ");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(7);
            cell.SetCellValue("Дата решения");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(8);
            cell.SetCellValue("Номер решения");
            cell.CellStyle = boldFontStyle;
            cell = (HSSFCell)headerRow.CreateCell(9);
            cell.SetCellValue("Штрих-код");
            cell.CellStyle = boldFontStyle;

            //(Optional) freeze the header row so it is not scrolled
            //sheet.CreateFreezePane(0, 1, 0, 1);

            int rowNumber = 2;

            //Populate the sheet with values from the grid data
            foreach (var item2 in items2)
            {
                //Create a new row                
                var row = sheet.CreateRow(rowNumber++);

                //Set values for the cells
                row.CreateCell(0).SetCellValue(item2.mnn);
                row.CreateCell(1).SetCellValue(item2.comm_name);
                row.CreateCell(2).SetCellValue(item2.pack_name);
                row.CreateCell(3).SetCellValue(item2.producer);
                row.CreateCell(4).SetCellValue(item2.pack_count.HasValue ? item2.pack_count.ToString() : "");
                row.CreateCell(5, NPOI.SS.UserModel.CellType.Numeric).SetCellValue(item2.limit_price.HasValue ? item2.limit_price.ToString() : "");
                row.CreateCell(6).SetCellValue(item2.reg_num);
                row.CreateCell(7).SetCellValue(item2.price_reg_date);
                row.CreateCell(8).SetCellValue(item2.price_reg_ndoc);
                row.CreateCell(9).SetCellValue(item2.ean13);
            }

            //Write the workbook to a memory stream
            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            //Return the result to the end user
            return File(output.ToArray(),   //The binary data of the XLS file
                "application/vnd.ms-excel", //MIME type of Excel files
                title);
        }

        #endregion
    }
}
