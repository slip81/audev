﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using Ninject;
using Ninject.Web.Common;
using CabinetMvc.Util;
using CabinetMvc.Auth;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.User;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Defect;
using CabinetMvc.ViewModel.HelpDesk;
using CabinetMvc.ViewModel.Exchange;
using CabinetMvc.ViewModel.Ved;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.ViewModel.Sys;
using CabinetMvc.ViewModel.Auth;
using CabinetMvc.ViewModel.Storage;
using CabinetMvc.ViewModel.Update;
using CabinetMvc.ViewModel.Prj;
using CabinetMvc.ViewModel.Stock;
using CabinetMvc.ViewModel.Asna;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.Controllers
{
    public partial class BaseController
    {
        [Inject]
        public IAuthentication Auth { get; set; }
        public UserViewModel CurrentUser { get { return Auth.CurrentUser != null ? ((IUserProvider)Auth.CurrentUser.Identity).User : null; } }

        #region Inject services (Adm)

        protected IDiscountLogEventService discountLogEventService
        {
            get
            {                
                if (_discountLogEventService == null)
                {
                    _discountLogEventService = ninjectKernel.Get<IDiscountLogEventService>();
                }
                return _discountLogEventService;
            }
        }
        private IDiscountLogEventService _discountLogEventService;

        protected IEsnLogEventService esnLogEventService
        {
            get
            {
                if (_esnLogEventService == null)
                {
                    _esnLogEventService = ninjectKernel.Get<IEsnLogEventService>();
                }
                return _esnLogEventService;
            }
        }
        private IEsnLogEventService _esnLogEventService;


        protected ILogCabService logCabService
        {
            get
            {
                if (_logCabService == null)
                {
                    _logCabService = ninjectKernel.Get<ILogCabService>();
                }
                return _logCabService;
            }
        }
        private ILogCabService _logCabService;

        protected ICabActionGroupService cabActionGroupService
        {
            get
            {
                if (_cabActionGroupService == null)
                {
                    _cabActionGroupService = ninjectKernel.Get<ICabActionGroupService>();
                }
                return _cabActionGroupService;
            }
        }
        private ICabActionGroupService _cabActionGroupService;

        protected ICabActionService cabActionService
        {
            get
            {
                if (_cabActionService == null)
                {
                    _cabActionService = ninjectKernel.Get<ICabActionService>();
                }
                return _cabActionService;
            }
        }
        private ICabActionService _cabActionService;

        protected ICabRoleActionService cabRoleActionService
        {
            get
            {
                if (_cabRoleActionService == null)
                {
                    _cabRoleActionService = ninjectKernel.Get<ICabRoleActionService>();
                }
                return _cabRoleActionService;
            }
        }
        private ICabRoleActionService _cabRoleActionService;

        protected ICabRoleService cabRoleService
        {
            get
            {
                if (_cabRoleService == null)
                {
                    _cabRoleService = ninjectKernel.Get<ICabRoleService>();
                }
                return _cabRoleService;
            }
        }
        private ICabRoleService _cabRoleService;

        protected ICabUserRoleActionService cabUserRoleActionService
        {
            get
            {
                if (_cabUserRoleActionService == null)
                {
                    _cabUserRoleActionService = ninjectKernel.Get<ICabUserRoleActionService>();
                }
                return _cabUserRoleActionService;
            }
        }
        private ICabUserRoleActionService _cabUserRoleActionService;

        protected ICabUserRoleService cabUserRoleService
        {
            get
            {
                if (_cabUserRoleService == null)
                {
                    _cabUserRoleService = ninjectKernel.Get<ICabUserRoleService>();
                }
                return _cabUserRoleService;
            }
        }
        private ICabUserRoleService _cabUserRoleService;

        protected ICabUserService cabUserService
        {
            get
            {
                if (_cabUserService == null)
                {
                    _cabUserService = ninjectKernel.Get<ICabUserService>();
                }
                return _cabUserService;
            }
        }
        private ICabUserService _cabUserService;

        #endregion

        #region Inject services (Cabinet)

        protected IClientService clientService
        {
            get
            {
                if (_clientService == null)
                {
                    _clientService = ninjectKernel.Get<IClientService>();                    
                }
                return _clientService;
            }
        }
        private IClientService _clientService;

        protected ILicenseService licenseService
        {
            get
            {
                if (_licenseService == null)
                {
                    _licenseService = ninjectKernel.Get<ILicenseService>();
                }
                return _licenseService;
            }
        }
        private ILicenseService _licenseService;

        protected IOrderItemService orderItemService
        {
            get
            {
                if (_orderItemService == null)
                {
                    _orderItemService = ninjectKernel.Get<IOrderItemService>();
                }
                return _orderItemService;
            }
        }
        private IOrderItemService _orderItemService;

        protected IOrderService orderService
        {
            get
            {
                if (_orderService == null)
                {
                    _orderService = ninjectKernel.Get<IOrderService>();
                }
                return _orderService;
            }
        }
        private IOrderService _orderService;

        protected ISalesService salesService
        {
            get
            {
                if (_salesService == null)
                {
                    _salesService = ninjectKernel.Get<ISalesService>();
                }
                return _salesService;
            }
        }
        private ISalesService _salesService;

        protected IServiceService serviceService
        {
            get
            {
                if (_serviceService == null)
                {
                    _serviceService = ninjectKernel.Get<IServiceService>();
                }
                return _serviceService;
            }
        }
        private IServiceService _serviceService;

        protected ISpravCityService spravCityService
        {
            get
            {
                if (_spravCityService == null)
                {
                    _spravCityService = ninjectKernel.Get<ISpravCityService>();
                }
                return _spravCityService;
            }
        }
        private ISpravCityService _spravCityService;

        protected ISpravDistrictService spravDistrictService
        {
            get
            {
                if (_spravDistrictService == null)
                {
                    _spravDistrictService = ninjectKernel.Get<ISpravDistrictService>();
                }
                return _spravDistrictService;
            }
        }
        private ISpravDistrictService _spravDistrictService;

                protected ISpravRegionService spravRegionService
        {
            get
            {
                if (_spravRegionService == null)
                {
                    _spravRegionService = ninjectKernel.Get<ISpravRegionService>();
                }
                return _spravRegionService;
            }
        }
        private ISpravRegionService _spravRegionService;

        protected ISpravStreetService spravStreetService
        {
            get
            {
                if (_spravStreetService == null)
                {
                    _spravStreetService = ninjectKernel.Get<ISpravStreetService>();
                }
                return _spravStreetService;
            }
        }
        private ISpravStreetService _spravStreetService;

        protected IVersionService versionService
        {
            get
            {
                if (_versionService == null)
                {
                    _versionService = ninjectKernel.Get<IVersionService>();
                }
                return _versionService;
            }
        }
        private IVersionService _versionService;

        protected IWorkplaceService workplaceService
        {
            get
            {
                if (_workplaceService == null)
                {
                    _workplaceService = ninjectKernel.Get<IWorkplaceService>();
                }
                return _workplaceService;
            }
        }
        private IWorkplaceService _workplaceService;

        protected IClientServiceService clientServiceService
        {
            get
            {
                if (_clientServiceService == null)
                {
                    _clientServiceService = ninjectKernel.Get<IClientServiceService>();
                }
                return _clientServiceService;
            }
        }
        private IClientServiceService _clientServiceService;

        protected IWorkplaceReregService workplaceReregService
        {
            get
            {
                if (_workplaceReregService == null)
                {
                    _workplaceReregService = ninjectKernel.Get<IWorkplaceReregService>();
                }
                return _workplaceReregService;
            }
        }
        private IWorkplaceReregService _workplaceReregService;

        protected IClientServiceUnregService clientServiceUnregService
        {
            get
            {
                if (_clientServiceUnregService == null)
                {
                    _clientServiceUnregService = ninjectKernel.Get<IClientServiceUnregService>();
                }
                return _clientServiceUnregService;
            }
        }
        private IClientServiceUnregService _clientServiceUnregService;


        protected IUserProfileService userProfileService
        {
            get
            {
                if (_userProfileService == null)
                {
                    _userProfileService = ninjectKernel.Get<IUserProfileService>();
                }
                return _userProfileService;
            }
        }
        private IUserProfileService _userProfileService;

        protected IServiceSenderService serviceSenderService
        {
            get
            {
                if (_serviceSenderService == null)
                {
                    _serviceSenderService = ninjectKernel.Get<IServiceSenderService>();
                }
                return _serviceSenderService;
            }
        }
        private IServiceSenderService _serviceSenderService;

        protected IServiceSenderLogService serviceSenderLogService
        {
            get
            {
                if (_serviceSenderLogService == null)
                {
                    _serviceSenderLogService = ninjectKernel.Get<IServiceSenderLogService>();
                }
                return _serviceSenderLogService;
            }
        }
        private IServiceSenderLogService _serviceSenderLogService;

        protected IServiceCvaService serviceCvaService
        {
            get
            {
                if (_serviceCvaService == null)
                {
                    _serviceCvaService = ninjectKernel.Get<IServiceCvaService>();
                }
                return _serviceCvaService;
            }
        }
        private IServiceCvaService _serviceCvaService;

        protected IServiceCvaDataService serviceCvaDataService
        {
            get
            {
                if (_serviceCvaDataService == null)
                {
                    _serviceCvaDataService = ninjectKernel.Get<IServiceCvaDataService>();
                }
                return _serviceCvaDataService;
            }
        }
        private IServiceCvaDataService _serviceCvaDataService;

        protected IClientUserService clientUserService
        {
            get
            {
                if (_clientUserService == null)
                {
                    _clientUserService = ninjectKernel.Get<IClientUserService>();
                }
                return _clientUserService;
            }
        }
        private IClientUserService _clientUserService;

        protected IWidgetService widgetService
        {
            get
            {
                if (_widgetService == null)
                {
                    _widgetService = ninjectKernel.Get<IWidgetService>();
                }
                return _widgetService;
            }
        }
        private IWidgetService _widgetService;

        protected IClientUserWidgetService clientUserWidgetService
        {
            get
            {
                if (_clientUserWidgetService == null)
                {
                    _clientUserWidgetService = ninjectKernel.Get<IClientUserWidgetService>();
                }
                return _clientUserWidgetService;
            }
        }
        private IClientUserWidgetService _clientUserWidgetService;

        protected IServiceDefectService serviceDefectService
        {
            get
            {
                if (_serviceDefectService == null)
                {
                    _serviceDefectService = ninjectKernel.Get<IServiceDefectService>();
                }
                return _serviceDefectService;
            }
        }
        private IServiceDefectService _serviceDefectService;

        protected IClientInfoService clientInfoService
        {
            get
            {
                if (_clientInfoService == null)
                {
                    _clientInfoService = ninjectKernel.Get<IClientInfoService>();
                }
                return _clientInfoService;
            }
        }
        private IClientInfoService _clientInfoService;

        protected IClientServiceSummaryService clientServiceSummaryService
        {
            get
            {
                if (_clientServiceSummaryService == null)
                {
                    _clientServiceSummaryService = ninjectKernel.Get<IClientServiceSummaryService>();
                }
                return _clientServiceSummaryService;
            }
        }
        private IClientServiceSummaryService _clientServiceSummaryService;

        protected IServiceGroupService serviceGroupService
        {
            get
            {
                if (_serviceGroupService == null)
                {
                    _serviceGroupService = ninjectKernel.Get<IServiceGroupService>();
                }
                return _serviceGroupService;
            }
        }
        private IServiceGroupService _serviceGroupService;

        protected IRegService regService
        {
            get
            {
                if (_regService == null)
                {
                    _regService = ninjectKernel.Get<IRegService>();
                }
                return _regService;
            }
        }
        private IRegService _regService;

        protected IDeliveryTemplateService deliveryTemplateService
        {
            get
            {
                if (_deliveryTemplateService == null)
                {
                    _deliveryTemplateService = ninjectKernel.Get<IDeliveryTemplateService>();
                }
                return _deliveryTemplateService;
            }
        }
        private IDeliveryTemplateService _deliveryTemplateService;

        protected IDeliveryTemplateGroupService deliveryTemplateGroupService
        {
            get
            {
                if (_deliveryTemplateGroupService == null)
                {
                    _deliveryTemplateGroupService = ninjectKernel.Get<IDeliveryTemplateGroupService>();
                }
                return _deliveryTemplateGroupService;
            }
        }
        private IDeliveryTemplateGroupService _deliveryTemplateGroupService;

        protected IDeliveryService deliveryService
        {
            get
            {
                if (_deliveryService == null)
                {
                    _deliveryService = ninjectKernel.Get<IDeliveryService>();
                }
                return _deliveryService;
            }
        }
        private IDeliveryService _deliveryService;

        protected IClientServiceParamService clientServiceParamService
        {
            get
            {
                if (_clientServiceParamService == null)
                {
                    _clientServiceParamService = ninjectKernel.Get<IClientServiceParamService>();
                }
                return _clientServiceParamService;
            }
        }
        private IClientServiceParamService _clientServiceParamService;

        protected IDeliveryDetailService deliveryDetailService
        {
            get
            {
                if (_deliveryDetailService == null)
                {
                    _deliveryDetailService = ninjectKernel.Get<IDeliveryDetailService>();
                }
                return _deliveryDetailService;
            }
        }
        private IDeliveryDetailService _deliveryDetailService;

        protected IUserMenuService userMenuService
        {
            get
            {
                if (_userMenuService == null)
                {
                    _userMenuService = ninjectKernel.Get<IUserMenuService>();
                }
                return _userMenuService;
            }
        }
        private IUserMenuService _userMenuService;

        protected IServiceKitService serviceKitService
        {
            get
            {
                if (_serviceKitService == null)
                {
                    _serviceKitService = ninjectKernel.Get<IServiceKitService>();
                }
                return _serviceKitService;
            }
        }
        private IServiceKitService _serviceKitService;

        protected IServiceKitItemService serviceKitItemService
        {
            get
            {
                if (_serviceKitItemService == null)
                {
                    _serviceKitItemService = ninjectKernel.Get<IServiceKitItemService>();
                }
                return _serviceKitItemService;
            }
        }
        private IServiceKitItemService _serviceKitItemService;

        protected ICabHelpService cabHelpService
        {
            get
            {
                if (_cabHelpService == null)
                {
                    _cabHelpService = ninjectKernel.Get<ICabHelpService>();
                }
                return _cabHelpService;
            }
        }
        private ICabHelpService _cabHelpService;

        protected IClientCabService clientCabService
        {
            get
            {
                if (_clientCabService == null)
                {
                    _clientCabService = ninjectKernel.Get<IClientCabService>();
                }
                return _clientCabService;
            }
        }
        private IClientCabService _clientCabService;

        protected IClientCrmService clientCrmService
        {
            get
            {
                if (_clientCrmService == null)
                {
                    _clientCrmService = ninjectKernel.Get<IClientCrmService>();
                }
                return _clientCrmService;
            }
        }
        private IClientCrmService _clientCrmService;

        protected IDeliveryTemplateFileService deliveryTemplateFileService
        {
            get
            {
                if (_deliveryTemplateFileService == null)
                {
                    _deliveryTemplateFileService = ninjectKernel.Get<IDeliveryTemplateFileService>();
                }
                return _deliveryTemplateFileService;
            }
        }
        private IDeliveryTemplateFileService _deliveryTemplateFileService;

        protected IClientMaxVersionService clientMaxVersionService
        {
            get
            {
                if (_clientMaxVersionService == null)
                {
                    _clientMaxVersionService = ninjectKernel.Get<IClientMaxVersionService>();
                }
                return _clientMaxVersionService;
            }
        }
        private IClientMaxVersionService _clientMaxVersionService;

        protected IServiceStockService serviceStockService
        {
            get
            {
                if (_serviceStockService == null)
                {
                    _serviceStockService = ninjectKernel.Get<IServiceStockService>();
                }
                return _serviceStockService;
            }
        }
        private IServiceStockService _serviceStockService;

        protected ISpravRegionZoneService spravRegionZoneService
        {
            get
            {
                if (_spravRegionZoneService == null)
                {
                    _spravRegionZoneService = ninjectKernel.Get<ISpravRegionZoneService>();
                }
                return _spravRegionZoneService;
            }
        }
        private ISpravRegionZoneService _spravRegionZoneService;

        protected ISpravRegionPriceLimitService spravRegionPriceLimitService
        {
            get
            {
                if (_spravRegionPriceLimitService == null)
                {
                    _spravRegionPriceLimitService = ninjectKernel.Get<ISpravRegionPriceLimitService>();
                }
                return _spravRegionPriceLimitService;
            }
        }
        private ISpravRegionPriceLimitService _spravRegionPriceLimitService;

        protected ISpravRegionZonePriceLimitService spravRegionZonePriceLimitService
        {
            get
            {
                if (_spravRegionZonePriceLimitService == null)
                {
                    _spravRegionZonePriceLimitService = ninjectKernel.Get<ISpravRegionZonePriceLimitService>();
                }
                return _spravRegionZonePriceLimitService;
            }
        }
        private ISpravRegionZonePriceLimitService _spravRegionZonePriceLimitService;

        protected ISpravTaxSystemTypeService spravTaxSystemTypeService
        {
            get
            {
                if (_spravTaxSystemTypeService == null)
                {
                    _spravTaxSystemTypeService = ninjectKernel.Get<ISpravTaxSystemTypeService>();
                }
                return _spravTaxSystemTypeService;
            }
        }
        private ISpravTaxSystemTypeService _spravTaxSystemTypeService;

        protected IClientReportService clientReportService
        {
            get
            {
                if (_clientReportService == null)
                {
                    _clientReportService = ninjectKernel.Get<IClientReportService>();
                }
                return _clientReportService;
            }
        }
        private IClientReportService _clientReportService;

        protected IServiceEsn2Service serviceEsn2Service
        {
            get
            {
                if (_serviceEsn2Service == null)
                {
                    _serviceEsn2Service = ninjectKernel.Get<IServiceEsn2Service>();
                }
                return _serviceEsn2Service;
            }
        }
        private IServiceEsn2Service _serviceEsn2Service;

        protected IVersionMainService versionMainService
        {
            get
            {
                if (_versionMainService == null)
                {
                    _versionMainService = ninjectKernel.Get<IVersionMainService>();
                }
                return _versionMainService;
            }
        }
        private IVersionMainService _versionMainService;        

        #endregion

        #region Inject services (Defect)

        protected IDefectService defectService
        {
            get
            {
                if (_defectService == null)
                {
                    _defectService = ninjectKernel.Get<IDefectService>();
                }
                return _defectService;
            }
        }
        private IDefectService _defectService;

        protected IDefectRequestService defectRequestService
        {
            get
            {
                if (_defectRequestService == null)
                {
                    _defectRequestService = ninjectKernel.Get<IDefectRequestService>();
                }
                return _defectRequestService;
            }
        }
        private IDefectRequestService _defectRequestService;

        protected IDefectSummaryService defectSummaryService
        {
            get
            {
                if (_defectSummaryService == null)
                {
                    _defectSummaryService = ninjectKernel.Get<IDefectSummaryService>();
                }
                return _defectSummaryService;
            }
        }
        private IDefectSummaryService _defectSummaryService;        

        #endregion

        #region Inject services (Card)

        protected IBusinessOrgService businessOrgService
        {
            get
            {
                if (_businessOrgService == null)
                {
                    _businessOrgService = ninjectKernel.Get<IBusinessOrgService>();
                }
                return _businessOrgService;
            }
        }
        private IBusinessOrgService _businessOrgService;

        protected IBusinessOrgUserService businessOrgUserService
        {
            get
            {
                if (_businessOrgUserService == null)
                {
                    _businessOrgUserService = ninjectKernel.Get<IBusinessOrgUserService>();
                }
                return _businessOrgUserService;
            }
        }
        private IBusinessOrgUserService _businessOrgUserService;

        protected IBusinessService businessService
        {
            get
            {
                if (_businessService == null)
                {
                    _businessService = ninjectKernel.Get<IBusinessService>();
                }
                return _businessService;
            }
        }
        private IBusinessService _businessService;

        protected ICardAdditionalNumService cardAdditionalNumService
        {
            get
            {
                if (_cardAdditionalNumService == null)
                {
                    _cardAdditionalNumService = ninjectKernel.Get<ICardAdditionalNumService>();
                }
                return _cardAdditionalNumService;
            }
        }
        private ICardAdditionalNumService _cardAdditionalNumService;

        protected ICardBatchOperationsErrorService cardBatchOperationsErrorService
        {
            get
            {
                if (_cardBatchOperationsErrorService == null)
                {
                    _cardBatchOperationsErrorService = ninjectKernel.Get<ICardBatchOperationsErrorService>();
                }
                return _cardBatchOperationsErrorService;
            }
        }
        private ICardBatchOperationsErrorService _cardBatchOperationsErrorService;

        protected ICardBatchOperationsService cardBatchOperationsService
        {
            get
            {
                if (_cardBatchOperationsService == null)
                {
                    _cardBatchOperationsService = ninjectKernel.Get<ICardBatchOperationsService>();
                }
                return _cardBatchOperationsService;
            }
        }
        private ICardBatchOperationsService _cardBatchOperationsService;

        protected ICardService cardService
        {
            get
            {
                if (_cardService == null)
                {
                    _cardService = ninjectKernel.Get<ICardService>();
                }
                return _cardService;
            }
        }
        private ICardService _cardService;

        protected ICardClientService cardClientService
        {
            get
            {
                if (_cardClientService == null)
                {
                    _cardClientService = ninjectKernel.Get<ICardClientService>();
                }
                return _cardClientService;
            }
        }
        private ICardClientService _cardClientService;

        protected ICardImportService cardImportService
        {
            get
            {
                if (_cardImportService == null)
                {
                    _cardImportService = ninjectKernel.Get<ICardImportService>();
                }
                return _cardImportService;
            }
        }
        private ICardImportService _cardImportService;

        protected ICardTransactionDetailService cardTransactionDetailService
        {
            get
            {
                if (_cardTransactionDetailService == null)
                {
                    _cardTransactionDetailService = ninjectKernel.Get<ICardTransactionDetailService>();
                }
                return _cardTransactionDetailService;
            }
        }
        private ICardTransactionDetailService _cardTransactionDetailService;

        protected ICardTransactionService cardTransactionService
        {
            get
            {
                if (_cardTransactionService == null)
                {
                    _cardTransactionService = ninjectKernel.Get<ICardTransactionService>();
                }
                return _cardTransactionService;
            }
        }
        private ICardTransactionService _cardTransactionService;

        protected ICardTypeGroupService cardTypeGroupService
        {
            get
            {
                if (_cardTypeGroupService == null)
                {
                    _cardTypeGroupService = ninjectKernel.Get<ICardTypeGroupService>();
                }
                return _cardTypeGroupService;
            }
        }
        private ICardTypeGroupService _cardTypeGroupService;

        protected ICardTypeLimitService cardTypeLimitService
        {
            get
            {
                if (_cardTypeLimitService == null)
                {
                    _cardTypeLimitService = ninjectKernel.Get<ICardTypeLimitService>();
                }
                return _cardTypeLimitService;
            }
        }
        private ICardTypeLimitService _cardTypeLimitService;

        /*
        protected ICardTypeRestrRuleLeftService cardTypeRestrRuleLeftService
        {
            get
            {
                if (_cardTypeRestrRuleLeftService == null)
                {
                    _cardTypeRestrRuleLeftService = ninjectKernel.Get<ICardTypeRestrRuleLeftService>();
                }
                return _cardTypeRestrRuleLeftService;
            }
        }
        private ICardTypeRestrRuleLeftService _cardTypeRestrRuleLeftService;

        protected ICardTypeRestrRuleService cardTypeRestrRuleService
        {
            get
            {
                if (_cardTypeRestrRuleService == null)
                {
                    _cardTypeRestrRuleService = ninjectKernel.Get<ICardTypeRestrRuleService>();
                }
                return _cardTypeRestrRuleService;
            }
        }
        private ICardTypeRestrRuleService _cardTypeRestrRuleService;

        protected ICardTypeRestrRuleSpentService cardTypeRestrRuleSpentService
        {
            get
            {
                if (_cardTypeRestrRuleSpentService == null)
                {
                    _cardTypeRestrRuleSpentService = ninjectKernel.Get<ICardTypeRestrRuleSpentService>();
                }
                return _cardTypeRestrRuleSpentService;
            }
        }
        private ICardTypeRestrRuleSpentService _cardTypeRestrRuleSpentService;

        protected ICardTypeRestrRuleTransService cardTypeRestrRuleTransService
        {
            get
            {
                if (_cardTypeRestrRuleTransService == null)
                {
                    _cardTypeRestrRuleTransService = ninjectKernel.Get<ICardTypeRestrRuleTransService>();
                }
                return _cardTypeRestrRuleTransService;
            }
        }
        private ICardTypeRestrRuleTransService _cardTypeRestrRuleTransService;

        protected ICardTypeRestrService cardTypeRestrService
        {
            get
            {
                if (_cardTypeRestrService == null)
                {
                    _cardTypeRestrService = ninjectKernel.Get<ICardTypeRestrService>();
                }
                return _cardTypeRestrService;
            }
        }
        private ICardTypeRestrService _cardTypeRestrService;

        protected ICardTypeRestrValueService cardTypeRestrValueService
        {
            get
            {
                if (_cardTypeRestrValueService == null)
                {
                    _cardTypeRestrValueService = ninjectKernel.Get<ICardTypeRestrValueService>();
                }
                return _cardTypeRestrValueService;
            }
        }
        private ICardTypeRestrValueService _cardTypeRestrValueService;
       */
        protected ICardTypeService cardTypeService
        {
            get
            {
                if (_cardTypeService == null)
                {
                    _cardTypeService = ninjectKernel.Get<ICardTypeService>();
                }
                return _cardTypeService;
            }
        }
        private ICardTypeService _cardTypeService;

        protected IProgrammParamService programmParamService
        {
            get
            {
                if (_programmParamService == null)
                {
                    _programmParamService = ninjectKernel.Get<IProgrammParamService>();
                }
                return _programmParamService;
            }
        }
        private IProgrammParamService _programmParamService;

        protected IProgrammService programmService
        {
            get
            {
                if (_programmService == null)
                {
                    _programmService = ninjectKernel.Get<IProgrammService>();
                }
                return _programmService;
            }
        }
        private IProgrammService _programmService;

        protected ISpravCardClientService spravCardClientService
        {
            get
            {
                if (_spravCardClientService == null)
                {
                    _spravCardClientService = ninjectKernel.Get<ISpravCardClientService>();
                }
                return _spravCardClientService;
            }
        }
        private ISpravCardClientService _spravCardClientService;

        protected ISpravCardStatusService spravCardStatusService
        {
            get
            {
                if (_spravCardStatusService == null)
                {
                    _spravCardStatusService = ninjectKernel.Get<ISpravCardStatusService>();
                }
                return _spravCardStatusService;
            }
        }
        private ISpravCardStatusService _spravCardStatusService;

        protected ISpravCardTypeService spravCardTypeService
        {
            get
            {
                if (_spravCardTypeService == null)
                {
                    _spravCardTypeService = ninjectKernel.Get<ISpravCardTypeService>();
                }
                return _spravCardTypeService;
            }
        }
        private ISpravCardTypeService _spravCardTypeService;

        protected ISpravProgrammService spravProgrammService
        {
            get
            {
                if (_spravProgrammService == null)
                {
                    _spravProgrammService = ninjectKernel.Get<ISpravProgrammService>();
                }
                return _spravProgrammService;
            }
        }
        private ISpravProgrammService _spravProgrammService;

        protected ISpravProgrammTemplateService spravProgrammTemplateService
        {
            get
            {
                if (_spravProgrammTemplateService == null)
                {
                    _spravProgrammTemplateService = ninjectKernel.Get<ISpravProgrammTemplateService>();
                }
                return _spravProgrammTemplateService;
            }
        }
        private ISpravProgrammTemplateService _spravProgrammTemplateService;

        protected ICardTypeBusinessOrgService cardTypeBusinessOrgService
        {
            get
            {
                if (_cardTypeBusinessOrgService == null)
                {
                    _cardTypeBusinessOrgService = ninjectKernel.Get<ICardTypeBusinessOrgService>();
                }
                return _cardTypeBusinessOrgService;
            }
        }
        private ICardTypeBusinessOrgService _cardTypeBusinessOrgService;

        protected IProgrammOrderService programmOrderService
        {
            get
            {
                if (_programmOrderService == null)
                {
                    _programmOrderService = ninjectKernel.Get<IProgrammOrderService>();
                }
                return _programmOrderService;
            }
        }
        private IProgrammOrderService _programmOrderService;

        protected IReportDiscountService reportDiscountService
        {
            get
            {
                if (_reportDiscountService == null)
                {
                    _reportDiscountService = ninjectKernel.Get<IReportDiscountService>();
                }
                return _reportDiscountService;
            }
        }
        private IReportDiscountService _reportDiscountService;

        protected ICampaignService campaignService
        {
            get
            {
                if (_campaignService == null)
                {
                    _campaignService = ninjectKernel.Get<ICampaignService>();
                }
                return _campaignService;
            }
        }
        private ICampaignService _campaignService;

        protected ICampaignBusinessOrgService campaignBusinessOrgService
        {
            get
            {
                if (_campaignBusinessOrgService == null)
                {
                    _campaignBusinessOrgService = ninjectKernel.Get<ICampaignBusinessOrgService>();
                }
                return _campaignBusinessOrgService;
            }
        }
        private ICampaignBusinessOrgService _campaignBusinessOrgService;

        protected IProgrammBuilderService programmBuilderService
        {
            get
            {
                if (_programmBuilderService == null)
                {
                    _programmBuilderService = ninjectKernel.Get<IProgrammBuilderService>();
                }
                return _programmBuilderService;
            }
        }
        private IProgrammBuilderService _programmBuilderService;

        protected IProgrammParamTypeService programmParamTypeService
        {
            get
            {
                if (_programmParamTypeService == null)
                {
                    _programmParamTypeService = ninjectKernel.Get<IProgrammParamTypeService>();
                }
                return _programmParamTypeService;
            }
        }
        private IProgrammParamTypeService _programmParamTypeService;

        protected IProgrammStepParamTypeService programmStepParamTypeService
        {
            get
            {
                if (_programmStepParamTypeService == null)
                {
                    _programmStepParamTypeService = ninjectKernel.Get<IProgrammStepParamTypeService>();
                }
                return _programmStepParamTypeService;
            }
        }
        private IProgrammStepParamTypeService _programmStepParamTypeService;
        
        protected IProgrammCardParamTypeService programmCardParamTypeService
        {
            get
            {
                if (_programmCardParamTypeService == null)
                {
                    _programmCardParamTypeService = ninjectKernel.Get<IProgrammCardParamTypeService>();
                }
                return _programmCardParamTypeService;
            }
        }
        private IProgrammCardParamTypeService _programmCardParamTypeService;
        
        protected IProgrammTransParamTypeService programmTransParamTypeService
        {
            get
            {
                if (_programmTransParamTypeService == null)
                {
                    _programmTransParamTypeService = ninjectKernel.Get<IProgrammTransParamTypeService>();
                }
                return _programmTransParamTypeService;
            }
        }
        private IProgrammTransParamTypeService _programmTransParamTypeService;

        protected IProgrammStepParamService programmStepParamService
        {
            get
            {
                if (_programmStepParamService == null)
                {
                    _programmStepParamService = ninjectKernel.Get<IProgrammStepParamService>();
                }
                return _programmStepParamService;
            }
        }
        private IProgrammStepParamService _programmStepParamService;

        protected IProgrammStepConditionService programmStepConditionService
        {
            get
            {
                if (_programmStepConditionService == null)
                {
                    _programmStepConditionService = ninjectKernel.Get<IProgrammStepConditionService>();
                }
                return _programmStepConditionService;
            }
        }
        private IProgrammStepConditionService _programmStepConditionService;

        protected IProgrammStepConditionTypeService programmStepConditionTypeService
        {
            get
            {
                if (_programmStepConditionTypeService == null)
                {
                    _programmStepConditionTypeService = ninjectKernel.Get<IProgrammStepConditionTypeService>();
                }
                return _programmStepConditionTypeService;
            }
        }
        private IProgrammStepConditionTypeService _programmStepConditionTypeService;
        
        protected IProgrammStepLogicTypeService programmStepLogicTypeService
        {
            get
            {
                if (_programmStepLogicTypeService == null)
                {
                    _programmStepLogicTypeService = ninjectKernel.Get<IProgrammStepLogicTypeService>();
                }
                return _programmStepLogicTypeService;
            }
        }
        private IProgrammStepLogicTypeService _programmStepLogicTypeService;

        #endregion

        #region Inject services (Exchange)

        protected IExchangeClientService exchangeClientService
        {
            get
            {
                if (_exchangeClientService == null)
                {
                    _exchangeClientService = ninjectKernel.Get<IExchangeClientService>();
                }
                return _exchangeClientService;
            }
        }
        private IExchangeClientService _exchangeClientService;

        protected IExchangeDataService exchangeDataService
        {
            get
            {
                if (_exchangeDataService == null)
                {
                    _exchangeDataService = ninjectKernel.Get<IExchangeDataService>();
                }
                return _exchangeDataService;
            }
        }
        private IExchangeDataService _exchangeDataService;

        protected IExchangeDocDataService exchangeDocDataService
        {
            get
            {
                if (_exchangeDocDataService == null)
                {
                    _exchangeDocDataService = ninjectKernel.Get<IExchangeDocDataService>();
                }
                return _exchangeDocDataService;
            }
        }
        private IExchangeDocDataService _exchangeDocDataService;

        protected IExchangeLogDateService exchangeLogDateService
        {
            get
            {
                if (_exchangeLogDateService == null)
                {
                    _exchangeLogDateService = ninjectKernel.Get<IExchangeLogDateService>();
                }
                return _exchangeLogDateService;
            }
        }
        private IExchangeLogDateService _exchangeLogDateService;

        protected IExchangeLogService exchangeLogService
        {
            get
            {
                if (_exchangeLogService == null)
                {
                    _exchangeLogService = ninjectKernel.Get<IExchangeLogService>();
                }
                return _exchangeLogService;
            }
        }
        private IExchangeLogService _exchangeLogService;

        protected IExchangeMonitorLogService exchangeMonitorLogService
        {
            get
            {
                if (_exchangeMonitorLogService == null)
                {
                    _exchangeMonitorLogService = ninjectKernel.Get<IExchangeMonitorLogService>();
                }
                return _exchangeMonitorLogService;
            }
        }
        private IExchangeMonitorLogService _exchangeMonitorLogService;

        protected IExchangeMonitorService exchangeMonitorService
        {
            get
            {
                if (_exchangeMonitorService == null)
                {
                    _exchangeMonitorService = ninjectKernel.Get<IExchangeMonitorService>();
                }
                return _exchangeMonitorService;
            }
        }
        private IExchangeMonitorService _exchangeMonitorService;

        protected IExchangePartnerService exchangePartnerService
        {
            get
            {
                if (_exchangePartnerService == null)
                {
                    _exchangePartnerService = ninjectKernel.Get<IExchangePartnerService>();
                }
                return _exchangePartnerService;
            }
        }
        private IExchangePartnerService _exchangePartnerService;

        protected IExchangeRegDocSalesService exchangeRegDocSalesService
        {
            get
            {
                if (_exchangeRegDocSalesService == null)
                {
                    _exchangeRegDocSalesService = ninjectKernel.Get<IExchangeRegDocSalesService>();
                }
                return _exchangeRegDocSalesService;
            }
        }
        private IExchangeRegDocSalesService _exchangeRegDocSalesService;

        protected IExchangeRegDocService exchangeRegDocService
        {
            get
            {
                if (_exchangeRegDocService == null)
                {
                    _exchangeRegDocService = ninjectKernel.Get<IExchangeRegDocService>();
                }
                return _exchangeRegDocService;
            }
        }
        private IExchangeRegDocService _exchangeRegDocService;

        protected IExchangeRegPartnerItemService exchangeRegPartnerItemService
        {
            get
            {
                if (_exchangeRegPartnerItemService == null)
                {
                    _exchangeRegPartnerItemService = ninjectKernel.Get<IExchangeRegPartnerItemService>();
                }
                return _exchangeRegPartnerItemService;
            }
        }
        private IExchangeRegPartnerItemService _exchangeRegPartnerItemService;

        protected IExchangeRegPartnerService exchangeRegPartnerService
        {
            get
            {
                if (_exchangeRegPartnerService == null)
                {
                    _exchangeRegPartnerService = ninjectKernel.Get<IExchangeRegPartnerService>();
                }
                return _exchangeRegPartnerService;
            }
        }
        private IExchangeRegPartnerService _exchangeRegPartnerService;

        protected IExchangeUserService exchangeUserService
        {
            get
            {
                if (_exchangeUserService == null)
                {
                    _exchangeUserService = ninjectKernel.Get<IExchangeUserService>();
                }
                return _exchangeUserService;
            }
        }
        private IExchangeUserService _exchangeUserService;

        protected IExchangeTaskService exchangeTaskService
        {
            get
            {
                if (_exchangeTaskService == null)
                {
                    _exchangeTaskService = ninjectKernel.Get<IExchangeTaskService>();
                }
                return _exchangeTaskService;
            }
        }
        private IExchangeTaskService _exchangeTaskService;

        protected IExchangeTaskTypeService exchangeTaskTypeService
        {
            get
            {
                if (_exchangeTaskTypeService == null)
                {
                    _exchangeTaskTypeService = ninjectKernel.Get<IExchangeTaskTypeService>();
                }
                return _exchangeTaskTypeService;
            }
        }
        private IExchangeTaskTypeService _exchangeTaskTypeService;

        protected IExchangeFileLogService exchangeFileLogService
        {
            get
            {
                if (_exchangeFileLogService == null)
                {
                    _exchangeFileLogService = ninjectKernel.Get<IExchangeFileLogService>();
                }
                return _exchangeFileLogService;
            }
        }
        private IExchangeFileLogService _exchangeFileLogService;

        protected IExchangeFileNodeService exchangeFileNodeService
        {
            get
            {
                if (_exchangeFileNodeService == null)
                {
                    _exchangeFileNodeService = ninjectKernel.Get<IExchangeFileNodeService>();
                }
                return _exchangeFileNodeService;
            }
        }
        private IExchangeFileNodeService _exchangeFileNodeService;

        protected IExchangeFileNodeItemService exchangeFileNodeItemService
        {
            get
            {
                if (_exchangeFileNodeItemService == null)
                {
                    _exchangeFileNodeItemService = ninjectKernel.Get<IExchangeFileNodeItemService>();
                }
                return _exchangeFileNodeItemService;
            }
        }
        private IExchangeFileNodeItemService _exchangeFileNodeItemService;

        protected IExchangePartnerItemService exchangePartnerItemService
        {
            get
            {
                if (_exchangePartnerItemService == null)
                {
                    _exchangePartnerItemService = ninjectKernel.Get<IExchangePartnerItemService>();
                }
                return _exchangePartnerItemService;
            }
        }
        private IExchangePartnerItemService _exchangePartnerItemService;
        
        #endregion

        #region Inject services (HelpDesk)

        protected IStoryService storyService
        {
            get
            {
                if (_storyService == null)
                {
                    _storyService = ninjectKernel.Get<IStoryService>();
                }
                return _storyService;
            }
        }
        private IStoryService _storyService;

        #endregion

        #region Inject services (Home)

        protected IBusinessSummaryService businessSummaryService
        {
            get
            {
                if (_businessSummaryService == null)
                {
                    _businessSummaryService = ninjectKernel.Get<IBusinessSummaryService>();
                }
                return _businessSummaryService;
            }
        }
        private IBusinessSummaryService _businessSummaryService;

        #endregion

        #region Inject services (User)

        protected ISpravProjectService spravProjectService
        {
            get
            {
                if (_spravProjectService == null)
                {
                    _spravProjectService = ninjectKernel.Get<ISpravProjectService>();
                }
                return _spravProjectService;
            }
        }
        private ISpravProjectService _spravProjectService;

        #endregion

        #region Inject services (Ved)

        protected IVedReestrItemOutService vedReestrItemOutService
        {
            get
            {
                if (_vedReestrItemOutService == null)
                {
                    _vedReestrItemOutService = ninjectKernel.Get<IVedReestrItemOutService>();
                }
                return _vedReestrItemOutService;
            }
        }
        private IVedReestrItemOutService _vedReestrItemOutService;

        protected IVedReestrItemService vedReestrItemService
        {
            get
            {
                if (_vedReestrItemService == null)
                {
                    _vedReestrItemService = ninjectKernel.Get<IVedReestrItemService>();
                }
                return _vedReestrItemService;
            }
        }
        private IVedReestrItemService _vedReestrItemService;

        protected IVedReestrRegionItemErrorService vedReestrRegionItemErrorService
        {
            get
            {
                if (_vedReestrRegionItemErrorService == null)
                {
                    _vedReestrRegionItemErrorService = ninjectKernel.Get<IVedReestrRegionItemErrorService>();
                }
                return _vedReestrRegionItemErrorService;
            }
        }
        private IVedReestrRegionItemErrorService _vedReestrRegionItemErrorService;

        protected IVedReestrRegionItemOutService vedReestrRegionItemOutService
        {
            get
            {
                if (_vedReestrRegionItemOutService == null)
                {
                    _vedReestrRegionItemOutService = ninjectKernel.Get<IVedReestrRegionItemOutService>();
                }
                return _vedReestrRegionItemOutService;
            }
        }
        private IVedReestrRegionItemOutService _vedReestrRegionItemOutService;

        protected IVedReestrRegionItemService vedReestrRegionItemService
        {
            get
            {
                if (_vedReestrRegionItemService == null)
                {
                    _vedReestrRegionItemService = ninjectKernel.Get<IVedReestrRegionItemService>();
                }
                return _vedReestrRegionItemService;
            }
        }
        private IVedReestrRegionItemService _vedReestrRegionItemService;

        protected IVedRequestService vedRequestService
        {
            get
            {
                if (_vedRequestService == null)
                {
                    _vedRequestService = ninjectKernel.Get<IVedRequestService>();
                }
                return _vedRequestService;
            }
        }
        private IVedRequestService _vedRequestService;

        protected IVedSourceRegionService vedSourceRegionService
        {
            get
            {
                if (_vedSourceRegionService == null)
                {
                    _vedSourceRegionService = ninjectKernel.Get<IVedSourceRegionService>();
                }
                return _vedSourceRegionService;
            }
        }
        private IVedSourceRegionService _vedSourceRegionService;

        protected IVedSourceService vedSourceService
        {
            get
            {
                if (_vedSourceService == null)
                {
                    _vedSourceService = ninjectKernel.Get<IVedSourceService>();
                }
                return _vedSourceService;
            }
        }
        private IVedSourceService _vedSourceService;

        protected IVedTemplateColumnService vedTemplateColumnService
        {
            get
            {
                if (_vedTemplateColumnService == null)
                {
                    _vedTemplateColumnService = ninjectKernel.Get<IVedTemplateColumnService>();
                }
                return _vedTemplateColumnService;
            }
        }
        private IVedTemplateColumnService _vedTemplateColumnService;

        protected IVedTemplateService vedTemplateService
        {
            get
            {
                if (_vedTemplateService == null)
                {
                    _vedTemplateService = ninjectKernel.Get<IVedTemplateService>();
                }
                return _vedTemplateService;
            }
        }
        private IVedTemplateService _vedTemplateService;


        #endregion

        #region Inject services (Crm)

        protected ICrmClientService crmClientService
        {
            get
            {
                if (_crmClientService == null)
                {
                    _crmClientService = ninjectKernel.Get<ICrmClientService>();
                }
                return _crmClientService;
            }
        }
        private ICrmClientService _crmClientService;

        protected ICrmModulePartService crmModulePartService
        {
            get
            {
                if (_crmModulePartService == null)
                {
                    _crmModulePartService = ninjectKernel.Get<ICrmModulePartService>();
                }
                return _crmModulePartService;
            }
        }
        private ICrmModulePartService _crmModulePartService;

        protected ICrmModuleService crmModuleService
        {
            get
            {
                if (_crmModuleService == null)
                {
                    _crmModuleService = ninjectKernel.Get<ICrmModuleService>();
                }
                return _crmModuleService;
            }
        }
        private ICrmModuleService _crmModuleService;

        protected ICrmModuleVersionService crmModuleVersionService
        {
            get
            {
                if (_crmModuleVersionService == null)
                {
                    _crmModuleVersionService = ninjectKernel.Get<ICrmModuleVersionService>();
                }
                return _crmModuleVersionService;
            }
        }
        private ICrmModuleVersionService _crmModuleVersionService;

        protected ICrmPriorityService crmPriorityService
        {
            get
            {
                if (_crmPriorityService == null)
                {
                    _crmPriorityService = ninjectKernel.Get<ICrmPriorityService>();
                }
                return _crmPriorityService;
            }
        }
        private ICrmPriorityService _crmPriorityService;

        protected ICrmProjectService crmProjectService
        {
            get
            {
                if (_crmProjectService == null)
                {
                    _crmProjectService = ninjectKernel.Get<ICrmProjectService>();
                }
                return _crmProjectService;
            }
        }
        private ICrmProjectService _crmProjectService;

        protected ICrmUserRoleService crmUserRoleService
        {
            get
            {
                if (_crmUserRoleService == null)
                {
                    _crmUserRoleService = ninjectKernel.Get<ICrmUserRoleService>();
                }
                return _crmUserRoleService;
            }
        }
        private ICrmUserRoleService _crmUserRoleService;
        
        protected ICrmProjectVersionService crmProjectVersionService
        {
            get
            {
                if (_crmProjectVersionService == null)
                {
                    _crmProjectVersionService = ninjectKernel.Get<ICrmProjectVersionService>();
                }
                return _crmProjectVersionService;
            }
        }
        private ICrmProjectVersionService _crmProjectVersionService;

        #endregion

        #region Inject services (Sys)

        protected ICabGridService cabGridService
        {
            get
            {
                if (_cabGridService == null)
                {
                    _cabGridService = ninjectKernel.Get<ICabGridService>();
                }
                return _cabGridService;
            }
        }
        private ICabGridService _cabGridService;

        protected ICabGridColumnService cabGridColumnService
        {
            get
            {
                if (_cabGridColumnService == null)
                {
                    _cabGridColumnService = ninjectKernel.Get<ICabGridColumnService>();
                }
                return _cabGridColumnService;
            }
        }
        private ICabGridColumnService _cabGridColumnService;

        protected ICabGridColumnUserSettingsService cabGridColumnUserSettingsService
        {
            get
            {
                if (_cabGridColumnUserSettingsService == null)
                {
                    _cabGridColumnUserSettingsService = ninjectKernel.Get<ICabGridColumnUserSettingsService>();
                }
                return _cabGridColumnUserSettingsService;
            }
        }
        private ICabGridColumnUserSettingsService _cabGridColumnUserSettingsService;

        protected ICabGridUserSettingService cabGridUserSettingService
        {
            get
            {
                if (_cabGridUserSettingService == null)
                {
                    _cabGridUserSettingService = ninjectKernel.Get<ICabGridUserSettingService>();
                }
                return _cabGridUserSettingService;
            }
        }
        private ICabGridUserSettingService _cabGridUserSettingService;
        
        #endregion

        #region Inject services (Auth)

        protected IAuthGroupService authGroupService
        {
            get
            {
                if (_authGroupService == null)
                {
                    _authGroupService = ninjectKernel.Get<IAuthGroupService>();
                }
                return _authGroupService;
            }
        }
        private IAuthGroupService _authGroupService;

        protected IAuthSectionService authSectionService
        {
            get
            {
                if (_authSectionService == null)
                {
                    _authSectionService = ninjectKernel.Get<IAuthSectionService>();
                }
                return _authSectionService;
            }
        }
        private IAuthSectionService _authSectionService;

        protected IAuthSectionPartService authSectionPartService
        {
            get
            {
                if (_authSectionPartService == null)
                {
                    _authSectionPartService = ninjectKernel.Get<IAuthSectionPartService>();
                }
                return _authSectionPartService;
            }
        }
        private IAuthSectionPartService _authSectionPartService;

        protected IAuthRoleService authRoleService
        {
            get
            {
                if (_authRoleService == null)
                {
                    _authRoleService = ninjectKernel.Get<IAuthRoleService>();
                }
                return _authRoleService;
            }
        }
        private IAuthRoleService _authRoleService;

        protected IAuthUserRoleService authUserRoleService
        {
            get
            {
                if (_authUserRoleService == null)
                {
                    _authUserRoleService = ninjectKernel.Get<IAuthUserRoleService>();
                }
                return _authUserRoleService;
            }
        }
        private IAuthUserRoleService _authUserRoleService;

        protected IAuthUserPermService authUserPermService
        {
            get
            {
                if (_authUserPermService == null)
                {
                    _authUserPermService = ninjectKernel.Get<IAuthUserPermService>();
                }
                return _authUserPermService;
            }
        }
        private IAuthUserPermService _authUserPermService;

        protected IAuthRolePermService authRolePermService
        {
            get
            {
                if (_authRolePermService == null)
                {
                    _authRolePermService = ninjectKernel.Get<IAuthRolePermService>();
                }
                return _authRolePermService;
            }
        }
        private IAuthRolePermService _authRolePermService;

        #endregion

        #region Inject services (Storage)

        protected IStorageFolderService storageFolderService
        {
            get
            {
                if (_storageFolderService == null)
                {
                    _storageFolderService = ninjectKernel.Get<IStorageFolderService>();
                }
                return _storageFolderService;
            }
        }
        private IStorageFolderService _storageFolderService;

        protected IStorageFileService storageFileService
        {
            get
            {
                if (_storageFileService == null)
                {
                    _storageFileService = ninjectKernel.Get<IStorageFileService>();
                }
                return _storageFileService;
            }
        }
        private IStorageFileService _storageFileService;

        protected IStorageFileTypeService storageFileTypeService
        {
            get
            {
                if (_storageFileTypeService == null)
                {
                    _storageFileTypeService = ninjectKernel.Get<IStorageFileTypeService>();
                }
                return _storageFileTypeService;
            }
        }
        private IStorageFileTypeService _storageFileTypeService;

        protected IStorageFileVersionService storageFileVersionService
        {
            get
            {
                if (_storageFileVersionService == null)
                {
                    _storageFileVersionService = ninjectKernel.Get<IStorageFileVersionService>();
                }
                return _storageFileVersionService;
            }
        }
        private IStorageFileVersionService _storageFileVersionService;

        protected IStorageParamService storageParamService
        {
            get
            {
                if (_storageParamService == null)
                {
                    _storageParamService = ninjectKernel.Get<IStorageParamService>();
                }
                return _storageParamService;
            }
        }
        private IStorageParamService _storageParamService;

        protected IStorageFileTypeGroupService storageFileTypeGroupService
        {
            get
            {
                if (_storageFileTypeGroupService == null)
                {
                    _storageFileTypeGroupService = ninjectKernel.Get<IStorageFileTypeGroupService>();
                }
                return _storageFileTypeGroupService;
            }
        }
        private IStorageFileTypeGroupService _storageFileTypeGroupService;

        protected IStorageLogService storageLogService
        {
            get
            {
                if (_storageLogService == null)
                {
                    _storageLogService = ninjectKernel.Get<IStorageLogService>();
                }
                return _storageLogService;
            }
        }
        private IStorageLogService _storageLogService;

        protected IStorageLinkService storageLinkService
        {
            get
            {
                if (_storageLinkService == null)
                {
                    _storageLinkService = ninjectKernel.Get<IStorageLinkService>();
                }
                return _storageLinkService;
            }
        }
        private IStorageLinkService _storageLinkService;

        protected IStorageTrashService storageTrashService
        {
            get
            {
                if (_storageTrashService == null)
                {
                    _storageTrashService = ninjectKernel.Get<IStorageTrashService>();
                }
                return _storageTrashService;
            }
        }
        private IStorageTrashService _storageTrashService;
        
        #endregion        
            
        #region Inject services (Update)

        protected IUpdateSoftService updateSoftService
        {
            get
            {
                if (_updateSoftService == null)
                {
                    _updateSoftService = ninjectKernel.Get<IUpdateSoftService>();
                }
                return _updateSoftService;
            }
        }
        private IUpdateSoftService _updateSoftService;

        protected IUpdateSoftVersionService updateSoftVersionService
        {
            get
            {
                if (_updateSoftVersionService == null)
                {
                    _updateSoftVersionService = ninjectKernel.Get<IUpdateSoftVersionService>();
                }
                return _updateSoftVersionService;
            }
        }
        private IUpdateSoftVersionService _updateSoftVersionService;

        protected IUpdateBatchService updateBatchService
        {
            get
            {
                if (_updateBatchService == null)
                {
                    _updateBatchService = ninjectKernel.Get<IUpdateBatchService>();
                }
                return _updateBatchService;
            }
        }
        private IUpdateBatchService _updateBatchService;

        protected IUpdateBatchItemService updateBatchItemService
        {
            get
            {
                if (_updateBatchItemService == null)
                {
                    _updateBatchItemService = ninjectKernel.Get<IUpdateBatchItemService>();
                }
                return _updateBatchItemService;
            }
        }
        private IUpdateBatchItemService _updateBatchItemService;

        protected IUpdateSoftGroupService updateSoftGroupService
        {
            get
            {
                if (_updateSoftGroupService == null)
                {
                    _updateSoftGroupService = ninjectKernel.Get<IUpdateSoftGroupService>();
                }
                return _updateSoftGroupService;
            }
        }
        private IUpdateSoftGroupService _updateSoftGroupService;

        #endregion
        
        #region Inject services (Prj)

        protected IPrjService prjService
        {
            get
            {
                if (_prjService == null)
                {
                    _prjService = ninjectKernel.Get<IPrjService>();
                }
                return _prjService;
            }
        }
        private IPrjService _prjService;

        protected IPrjClaimService prjClaimService
        {
            get
            {
                if (_prjClaimService == null)
                {
                    _prjClaimService = ninjectKernel.Get<IPrjClaimService>();
                }
                return _prjClaimService;
            }
        }
        private IPrjClaimService _prjClaimService;

        protected IPrjStateTypeService prjStateTypeService
        {
            get
            {
                if (_prjStateTypeService == null)
                {
                    _prjStateTypeService = ninjectKernel.Get<IPrjStateTypeService>();
                }
                return _prjStateTypeService;
            }
        }
        private IPrjStateTypeService _prjStateTypeService;

        protected IPrjTaskService prjTaskService
        {
            get
            {
                if (_prjTaskService == null)
                {
                    _prjTaskService = ninjectKernel.Get<IPrjTaskService>();
                }
                return _prjTaskService;
            }
        }
        private IPrjTaskService _prjTaskService;

        protected IPrjWorkService prjWorkService
        {
            get
            {
                if (_prjWorkService == null)
                {
                    _prjWorkService = ninjectKernel.Get<IPrjWorkService>();
                }
                return _prjWorkService;
            }
        }
        private IPrjWorkService _prjWorkService;

        protected IPrjWorkTypeService prjWorkTypeService
        {
            get
            {
                if (_prjWorkTypeService == null)
                {
                    _prjWorkTypeService = ninjectKernel.Get<IPrjWorkTypeService>();
                }
                return _prjWorkTypeService;
            }
        }
        private IPrjWorkTypeService _prjWorkTypeService;

        protected IPrjClaimStateTypeService prjClaimStateTypeService
        {
            get
            {
                if (_prjClaimStateTypeService == null)
                {
                    _prjClaimStateTypeService = ninjectKernel.Get<IPrjClaimStateTypeService>();
                }
                return _prjClaimStateTypeService;
            }
        }
        private IPrjClaimStateTypeService _prjClaimStateTypeService;

        protected IPrjTaskStateTypeService prjTaskStateTypeService
        {
            get
            {
                if (_prjTaskStateTypeService == null)
                {
                    _prjTaskStateTypeService = ninjectKernel.Get<IPrjTaskStateTypeService>();
                }
                return _prjTaskStateTypeService;
            }
        }
        private IPrjTaskStateTypeService _prjTaskStateTypeService;

        protected IPrjWorkStateTypeService prjWorkStateTypeService
        {
            get
            {
                if (_prjWorkStateTypeService == null)
                {
                    _prjWorkStateTypeService = ninjectKernel.Get<IPrjWorkStateTypeService>();
                }
                return _prjWorkStateTypeService;
            }
        }
        private IPrjWorkStateTypeService _prjWorkStateTypeService;

        protected IPrjMessService prjMessService
        {
            get
            {
                if (_prjMessService == null)
                {
                    _prjMessService = ninjectKernel.Get<IPrjMessService>();
                }
                return _prjMessService;
            }
        }
        private IPrjMessService _prjMessService;

        protected IPrjLogService prjLogService
        {
            get
            {
                if (_prjLogService == null)
                {
                    _prjLogService = ninjectKernel.Get<IPrjLogService>();
                }
                return _prjLogService;
            }
        }
        private IPrjLogService _prjLogService;

        protected IPrjUserSettingsService prjUserSettingsService
        {
            get
            {
                if (_prjUserSettingsService == null)
                {
                    _prjUserSettingsService = ninjectKernel.Get<IPrjUserSettingsService>();
                }
                return _prjUserSettingsService;
            }
        }
        private IPrjUserSettingsService _prjUserSettingsService;

        protected IPrjClaimDefaultService prjClaimDefaultService
        {
            get
            {
                if (_prjClaimDefaultService == null)
                {
                    _prjClaimDefaultService = ninjectKernel.Get<IPrjClaimDefaultService>();
                }
                return _prjClaimDefaultService;
            }
        }
        private IPrjClaimDefaultService _prjClaimDefaultService;
        
        protected IPrjWorkDefaultService prjWorkDefaultService
        {
            get
            {
                if (_prjWorkDefaultService == null)
                {
                    _prjWorkDefaultService = ninjectKernel.Get<IPrjWorkDefaultService>();
                }
                return _prjWorkDefaultService;
            }
        }
        private IPrjWorkDefaultService _prjWorkDefaultService;

        protected IPrjDefaultService prjDefaultService
        {
            get
            {
                if (_prjDefaultService == null)
                {
                    _prjDefaultService = ninjectKernel.Get<IPrjDefaultService>();
                }
                return _prjDefaultService;
            }
        }
        private IPrjDefaultService _prjDefaultService;

        protected IPrjFileService prjFileService
        {
            get
            {
                if (_prjFileService == null)
                {
                    _prjFileService = ninjectKernel.Get<IPrjFileService>();
                }
                return _prjFileService;
            }
        }
        private IPrjFileService _prjFileService;

        protected IPrjNotifyTypeService prjNotifyTypeService
        {
            get
            {
                if (_prjNotifyTypeService == null)
                {
                    _prjNotifyTypeService = ninjectKernel.Get<IPrjNotifyTypeService>();
                }
                return _prjNotifyTypeService;
            }
        }
        private IPrjNotifyTypeService _prjNotifyTypeService;

        protected IPrjNotifyService prjNotifyService
        {
            get
            {
                if (_prjNotifyService == null)
                {
                    _prjNotifyService = ninjectKernel.Get<IPrjNotifyService>();
                }
                return _prjNotifyService;
            }
        }
        private IPrjNotifyService _prjNotifyService;

        protected IPrjUserGroupService prjUserGroupService
        {
            get
            {
                if (_prjUserGroupService == null)
                {
                    _prjUserGroupService = ninjectKernel.Get<IPrjUserGroupService>();
                }
                return _prjUserGroupService;
            }
        }
        private IPrjUserGroupService _prjUserGroupService;

        protected IPrjBriefTypeService prjBriefTypeService
        {
            get
            {
                if (_prjBriefTypeService == null)
                {
                    _prjBriefTypeService = ninjectKernel.Get<IPrjBriefTypeService>();
                }
                return _prjBriefTypeService;
            }
        }
        private IPrjBriefTypeService _prjBriefTypeService;

        protected IPrjBriefIntervalTypeService prjBriefIntervalTypeService
        {
            get
            {
                if (_prjBriefIntervalTypeService == null)
                {
                    _prjBriefIntervalTypeService = ninjectKernel.Get<IPrjBriefIntervalTypeService>();
                }
                return _prjBriefIntervalTypeService;
            }
        }
        private IPrjBriefIntervalTypeService _prjBriefIntervalTypeService;

        protected IPrjUserGroupItemTypeService prjUserGroupItemTypeService
        {
            get
            {
                if (_prjUserGroupItemTypeService == null)
                {
                    _prjUserGroupItemTypeService = ninjectKernel.Get<IPrjUserGroupItemTypeService>();
                }
                return _prjUserGroupItemTypeService;
            }
        }
        private IPrjUserGroupItemTypeService _prjUserGroupItemTypeService;

        protected IPrjNoteService prjNoteService
        {
            get
            {
                if (_prjNoteService == null)
                {
                    _prjNoteService = ninjectKernel.Get<IPrjNoteService>();
                }
                return _prjNoteService;
            }
        }
        private IPrjNoteService _prjNoteService;

        protected IPrjUserGroupItemService prjUserGroupItemService
        {
            get
            {
                if (_prjUserGroupItemService == null)
                {
                    _prjUserGroupItemService = ninjectKernel.Get<IPrjUserGroupItemService>();
                }
                return _prjUserGroupItemService;
            }
        }
        private IPrjUserGroupItemService _prjUserGroupItemService;

        protected IPrjBriefDateCutTypeService prjBriefDateCutTypeService
        {
            get
            {
                if (_prjBriefDateCutTypeService == null)
                {
                    _prjBriefDateCutTypeService = ninjectKernel.Get<IPrjBriefDateCutTypeService>();
                }
                return _prjBriefDateCutTypeService;
            }
        }
        private IPrjBriefDateCutTypeService _prjBriefDateCutTypeService;

        protected IPrjClaimTypeService prjClaimTypeService
        {
            get
            {
                if (_prjClaimTypeService == null)
                {
                    _prjClaimTypeService = ninjectKernel.Get<IPrjClaimTypeService>();
                }
                return _prjClaimTypeService;
            }
        }
        private IPrjClaimTypeService _prjClaimTypeService;

        protected IPrjClaimStageService prjClaimStageService
        {
            get
            {
                if (_prjClaimStageService == null)
                {
                    _prjClaimStageService = ninjectKernel.Get<IPrjClaimStageService>();
                }
                return _prjClaimStageService;
            }
        }
        private IPrjClaimStageService _prjClaimStageService;
        
        protected IPrjTaskUserStateTypeService prjTaskUserStateTypeService
        {
            get
            {
                if (_prjTaskUserStateTypeService == null)
                {
                    _prjTaskUserStateTypeService = ninjectKernel.Get<IPrjTaskUserStateTypeService>();
                }
                return _prjTaskUserStateTypeService;
            }
        }
        private IPrjTaskUserStateTypeService _prjTaskUserStateTypeService;

        protected IPrjProjectVersionService prjProjectVersionService
        {
            get
            {
                if (_prjProjectVersionService == null)
                {
                    _prjProjectVersionService = ninjectKernel.Get<IPrjProjectVersionService>();
                }
                return _prjProjectVersionService;
            }
        }
        private IPrjProjectVersionService _prjProjectVersionService;
        
        protected IPrjClaimFieldService prjClaimFieldService
        {
            get
            {
                if (_prjClaimFieldService == null)
                {
                    _prjClaimFieldService = ninjectKernel.Get<IPrjClaimFieldService>();
                }
                return _prjClaimFieldService;
            }
        }
        private IPrjClaimFieldService _prjClaimFieldService;

        protected IPrjClaimFilterOpTypeService prjClaimFilterOpTypeService
        {
            get
            {
                if (_prjClaimFilterOpTypeService == null)
                {
                    _prjClaimFilterOpTypeService = ninjectKernel.Get<IPrjClaimFilterOpTypeService>();
                }
                return _prjClaimFilterOpTypeService;
            }
        }
        private IPrjClaimFilterOpTypeService _prjClaimFilterOpTypeService;

        protected IPrjClaimFilterService prjClaimFilterService
        {
            get
            {
                if (_prjClaimFilterService == null)
                {
                    _prjClaimFilterService = ninjectKernel.Get<IPrjClaimFilterService>();
                }
                return _prjClaimFilterService;
            }
        }
        private IPrjClaimFilterService _prjClaimFilterService;
        
        protected IPrjClaimFilterItemService prjClaimFilterItemService
        {
            get
            {
                if (_prjClaimFilterItemService == null)
                {
                    _prjClaimFilterItemService = ninjectKernel.Get<IPrjClaimFilterItemService>();
                }
                return _prjClaimFilterItemService;
            }
        }
        private IPrjClaimFilterItemService _prjClaimFilterItemService;
        
        protected IVersionTypeService versionTypeService
        {
            get
            {
                if (_versionTypeService == null)
                {
                    _versionTypeService = ninjectKernel.Get<IVersionTypeService>();
                }
                return _versionTypeService;
            }
        }
        private IVersionTypeService _versionTypeService;
        
        protected IPrjReport1Service prjReport1Service
        {
            get
            {
                if (_prjReport1Service == null)
                {
                    _prjReport1Service = ninjectKernel.Get<IPrjReport1Service>();
                }
                return _prjReport1Service;
            }
        }
        private IPrjReport1Service _prjReport1Service;

        #endregion

        #region Inject services (Stock)

        protected IStockService stockService
        {
            get
            {
                if (_stockService == null)
                {
                    _stockService = ninjectKernel.Get<IStockService>();
                }
                return _stockService;
            }
        }
        private IStockService _stockService;

        protected IStockBatchService stockBatchService
        {
            get
            {
                if (_stockBatchService == null)
                {
                    _stockBatchService = ninjectKernel.Get<IStockBatchService>();
                }
                return _stockBatchService;
            }
        }
        private IStockBatchService _stockBatchService;

        protected IStockSalesDownloadService stockSalesDownloadService
        {
            get
            {
                if (_stockSalesDownloadService == null)
                {
                    _stockSalesDownloadService = ninjectKernel.Get<IStockSalesDownloadService>();
                }
                return _stockSalesDownloadService;
            }
        }
        private IStockSalesDownloadService _stockSalesDownloadService;

        protected IStockLogService stockLogService
        {
            get
            {
                if (_stockLogService == null)
                {
                    _stockLogService = ninjectKernel.Get<IStockLogService>();
                }
                return _stockLogService;
            }
        }
        private IStockLogService _stockLogService;

        #endregion

        #region Inject services (Asna)

        protected IAsnaUserService asnaUserService
        {
            get
            {
                if (_asnaUserService == null)
                {
                    _asnaUserService = ninjectKernel.Get<IAsnaUserService>();
                }
                return _asnaUserService;
            }
        }
        private IAsnaUserService _asnaUserService;

        protected IAsnaScheduleIntervalTypeService asnaScheduleIntervalTypeService
        {
            get
            {
                if (_asnaScheduleIntervalTypeService == null)
                {
                    _asnaScheduleIntervalTypeService = ninjectKernel.Get<IAsnaScheduleIntervalTypeService>();
                }
                return _asnaScheduleIntervalTypeService;
            }
        }
        private IAsnaScheduleIntervalTypeService _asnaScheduleIntervalTypeService;

        protected IAsnaScheduleTimeTypeService asnaScheduleTimeTypeService
        {
            get
            {
                if (_asnaScheduleTimeTypeService == null)
                {
                    _asnaScheduleTimeTypeService = ninjectKernel.Get<IAsnaScheduleTimeTypeService>();
                }
                return _asnaScheduleTimeTypeService;
            }
        }
        private IAsnaScheduleTimeTypeService _asnaScheduleTimeTypeService;

        protected IAsnaUserLogService asnaUserLogService
        {
            get
            {
                if (_asnaUserLogService == null)
                {
                    _asnaUserLogService = ninjectKernel.Get<IAsnaUserLogService>();
                }
                return _asnaUserLogService;
            }
        }
        private IAsnaUserLogService _asnaUserLogService;

        protected IAsnaLinkService asnaLinkService
        {
            get
            {
                if (_asnaLinkService == null)
                {
                    _asnaLinkService = ninjectKernel.Get<IAsnaLinkService>();
                }
                return _asnaLinkService;
            }
        }
        private IAsnaLinkService _asnaLinkService;

        protected IAsnaStockRowService asnaStockRowService
        {
            get
            {
                if (_asnaStockRowService == null)
                {
                    _asnaStockRowService = ninjectKernel.Get<IAsnaStockRowService>();
                }
                return _asnaStockRowService;
            }
        }
        private IAsnaStockRowService _asnaStockRowService;

        protected IAsnaStockChainService asnaStockChainService
        {
            get
            {
                if (_asnaStockChainService == null)
                {
                    _asnaStockChainService = ninjectKernel.Get<IAsnaStockChainService>();
                }
                return _asnaStockChainService;
            }
        }
        private IAsnaStockChainService _asnaStockChainService;
        
        protected IAsnaStockRowActualService asnaStockRowActualService
        {
            get
            {
                if (_asnaStockRowActualService == null)
                {
                    _asnaStockRowActualService = ninjectKernel.Get<IAsnaStockRowActualService>();
                }
                return _asnaStockRowActualService;
            }
        }
        private IAsnaStockRowActualService _asnaStockRowActualService;

        protected IAsnaOrderStateTypeService asnaOrderStateTypeService
        {
            get
            {
                if (_asnaOrderStateTypeService == null)
                {
                    _asnaOrderStateTypeService = ninjectKernel.Get<IAsnaOrderStateTypeService>();
                }
                return _asnaOrderStateTypeService;
            }
        }
        private IAsnaOrderStateTypeService _asnaOrderStateTypeService;
        
        protected IAsnaOrderService asnaOrderService
        {
            get
            {
                if (_asnaOrderService == null)
                {
                    _asnaOrderService = ninjectKernel.Get<IAsnaOrderService>();
                }
                return _asnaOrderService;
            }
        }
        private IAsnaOrderService _asnaOrderService;
        
        protected IAsnaOrderRowService asnaOrderRowService
        {
            get
            {
                if (_asnaOrderRowService == null)
                {
                    _asnaOrderRowService = ninjectKernel.Get<IAsnaOrderRowService>();
                }
                return _asnaOrderRowService;
            }
        }
        private IAsnaOrderRowService _asnaOrderRowService;
        
        protected IAsnaOrderStateService asnaOrderStateService
        {
            get
            {
                if (_asnaOrderStateService == null)
                {
                    _asnaOrderStateService = ninjectKernel.Get<IAsnaOrderStateService>();
                }
                return _asnaOrderStateService;
            }
        }
        private IAsnaOrderStateService _asnaOrderStateService;

        protected IAsnaOrderRowStateService asnaOrderRowStateService
        {
            get
            {
                if (_asnaOrderRowStateService == null)
                {
                    _asnaOrderRowStateService = ninjectKernel.Get<IAsnaOrderRowStateService>();
                }
                return _asnaOrderRowStateService;
            }
        }
        private IAsnaOrderRowStateService _asnaOrderRowStateService;

        #endregion

        #region Inject services (Wa)

        protected IWaTaskService waTaskService
        {
            get
            {
                if (_waTaskService == null)
                {
                    _waTaskService = ninjectKernel.Get<IWaTaskService>();
                }
                return _waTaskService;
            }
        }
        private IWaTaskService _waTaskService;

        protected IWaRequestTypeService waRequestTypeService
        {
            get
            {
                if (_waRequestTypeService == null)
                {
                    _waRequestTypeService = ninjectKernel.Get<IWaRequestTypeService>();
                }
                return _waRequestTypeService;
            }
        }
        private IWaRequestTypeService _waRequestTypeService;

        protected IWaTaskLcService waTaskLcService
        {
            get
            {
                if (_waTaskLcService == null)
                {
                    _waTaskLcService = ninjectKernel.Get<IWaTaskLcService>();
                }
                return _waTaskLcService;
            }
        }
        private IWaTaskLcService _waTaskLcService;
        

        #endregion
    }
}
