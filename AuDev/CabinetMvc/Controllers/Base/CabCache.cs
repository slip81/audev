﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Drawing;
using System.Web.Script.Serialization;
using System.IO;
using System.Runtime.Caching;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public class CabCache
    {
        private DateTimeOffset cachePeriod = DateTime.Now.AddHours(8);

        public T GetCachedObject<T>(string key2 = "")
            where T : class
        {            
            string key = typeof(T).AssemblyQualifiedName;
            if (!String.IsNullOrWhiteSpace(key2))
                key = key + ", " + key2;
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Get(key) as T;
        }


        public void RefreshCachedObject<T>(T data, string key2 = "")
            where T : class
        {
            if (data == null)
                return;

            string key = typeof(T).AssemblyQualifiedName;
            if (!String.IsNullOrWhiteSpace(key2))
                key = key + ", " + key2;
            MemoryCache memoryCache = MemoryCache.Default;
            if (memoryCache.Contains(key))
            {
                memoryCache.Remove(key);
            }
            memoryCache.Add(key, data, cachePeriod);
        }

        public void DeleteCachedObject<T>(string key2 = "")
            where T : class
        {
            string key = typeof(T).AssemblyQualifiedName;
            if (!String.IsNullOrWhiteSpace(key2))
                key = key + ", " + key2;
            MemoryCache memoryCache = MemoryCache.Default;
            if (memoryCache.Contains(key))
            {
                memoryCache.Remove(key);
            }
        }

        public void Clear()
        {
            List<string> cacheKeys = MemoryCache.Default.Select(kvp => kvp.Key).ToList();
            foreach (string cacheKey in cacheKeys)
            {
                MemoryCache.Default.Remove(cacheKey);
            }
        }

    }
}