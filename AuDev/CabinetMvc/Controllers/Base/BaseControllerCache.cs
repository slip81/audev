﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using Ninject;
using Ninject.Web.Common;
using CabinetMvc.Util;
using CabinetMvc.Auth;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.ViewModel.Prj;
using CabinetMvc.ViewModel.Cabinet;

namespace CabinetMvc.Controllers
{
    public partial class BaseController
    {
        protected static CabCache cabCache;

        #region Cache

        #region IEnumerable        

        protected IEnumerable<PrjViewModel> cachedPrjList
        {
            get
            {
                if (_cachedPrjList == null)
                {
                    _cachedPrjList = cabCache.GetCachedObject<IEnumerable<PrjViewModel>>();
                    if (_cachedPrjList == null)
                    {
                        _cachedPrjList = prjService.GetList().AsEnumerable().Cast<PrjViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedPrjList);
                    }
                }
                return _cachedPrjList;
            }
        }
        protected IEnumerable<PrjViewModel> _cachedPrjList;

        protected IEnumerable<CabUserViewModel> cachedCabUserList
        {
            get
            {
                if (_cachedCabUserList == null)
                {
                    _cachedCabUserList = cabCache.GetCachedObject<IEnumerable<CabUserViewModel>>();
                    if (_cachedCabUserList == null)
                    {
                        _cachedCabUserList = cabUserService.GetList_CrmUser().AsEnumerable().Cast<CabUserViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedCabUserList);
                    }
                }
                return _cachedCabUserList;
            }
        }
        protected IEnumerable<CabUserViewModel> _cachedCabUserList;

        protected IEnumerable<CrmProjectViewModel> cachedProjectList
        {
            get
            {
                if (_cachedProjectList == null)
                {
                    _cachedProjectList = cabCache.GetCachedObject<IEnumerable<CrmProjectViewModel>>();
                    if (_cachedProjectList == null)
                    {
                        _cachedProjectList = crmProjectService.GetList().AsEnumerable().Cast<CrmProjectViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedProjectList);
                    }
                }
                return _cachedProjectList;
            }
        }
        protected IEnumerable<CrmProjectViewModel> _cachedProjectList;

        protected IEnumerable<CrmClientViewModel> cachedClientList
        {
            get
            {
                if (_cachedClientList == null)
                {
                    _cachedClientList = cabCache.GetCachedObject<IEnumerable<CrmClientViewModel>>();
                    if (_cachedClientList == null)
                    {
                        _cachedClientList = crmClientService.GetList().AsEnumerable().Cast<CrmClientViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedClientList);
                    }
                }
                return _cachedClientList;
            }
        }
        protected IEnumerable<CrmClientViewModel> _cachedClientList;

        protected IEnumerable<CrmPriorityViewModel> cachedPriorityList
        {
            get
            {
                if (_cachedPriorityList == null)
                {
                    _cachedPriorityList = cabCache.GetCachedObject<IEnumerable<CrmPriorityViewModel>>();
                    if (_cachedPriorityList == null)
                    {
                        _cachedPriorityList = crmPriorityService.GetList().AsEnumerable().Cast<CrmPriorityViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedPriorityList);
                    }
                }
                return _cachedPriorityList;
            }
        }
        protected IEnumerable<CrmPriorityViewModel> _cachedPriorityList;

        protected IEnumerable<CrmModuleViewModel> cachedModuleList
        {
            get
            {
                if (_cachedModuleList == null)
                {
                    _cachedModuleList = cabCache.GetCachedObject<IEnumerable<CrmModuleViewModel>>();
                    if (_cachedModuleList == null)
                    {
                        _cachedModuleList = crmModuleService.GetList().AsEnumerable().Cast<CrmModuleViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedModuleList);
                    }
                }
                return _cachedModuleList;
            }
        }
        protected IEnumerable<CrmModuleViewModel> _cachedModuleList;

        protected IEnumerable<CrmModulePartViewModel> cachedModulePartList
        {
            get
            {
                if (_cachedModulePartList == null)
                {
                    _cachedModulePartList = cabCache.GetCachedObject<IEnumerable<CrmModulePartViewModel>>();
                    if (_cachedModulePartList == null)
                    {
                        _cachedModulePartList = crmModulePartService.GetList().AsEnumerable().Cast<CrmModulePartViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedModulePartList);
                    }
                }
                return _cachedModulePartList;
            }
        }
        protected IEnumerable<CrmModulePartViewModel> _cachedModulePartList;

        protected IEnumerable<CrmModuleVersionViewModel> cachedModuleVersionList
        {
            get
            {
                if (_cachedModuleVersionList == null)
                {
                    _cachedModuleVersionList = cabCache.GetCachedObject<IEnumerable<CrmModuleVersionViewModel>>();
                    if (_cachedModuleVersionList == null)
                    {
                        _cachedModuleVersionList = crmModuleVersionService.GetList().AsEnumerable().Cast<CrmModuleVersionViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedModuleVersionList);
                    }
                }
                return _cachedModuleVersionList;
            }
        }
        protected IEnumerable<CrmModuleVersionViewModel> _cachedModuleVersionList;

        protected IEnumerable<PrjWorkTypeViewModel> cachedWorkTypeList
        {
            get
            {
                if (_cachedWorkTypeList == null)
                {
                    _cachedWorkTypeList = cabCache.GetCachedObject<IEnumerable<PrjWorkTypeViewModel>>();
                    if (_cachedWorkTypeList == null)
                    {
                        _cachedWorkTypeList = prjWorkTypeService.GetList().AsEnumerable().Cast<PrjWorkTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedWorkTypeList);
                    }
                }
                return _cachedWorkTypeList;
            }
        }
        protected IEnumerable<PrjWorkTypeViewModel> _cachedWorkTypeList;

        protected IEnumerable<PrjWorkStateTypeViewModel> cachedWorkStateTypeActiveList
        {
            get
            {
                if (_cachedWorkStateTypeActiveList == null)
                {
                    _cachedWorkStateTypeActiveList = cabCache.GetCachedObject<IEnumerable<PrjWorkStateTypeViewModel>>();
                    if (_cachedWorkStateTypeActiveList == null)
                    {
                        _cachedWorkStateTypeActiveList = prjWorkStateTypeService.GetList_Active().AsEnumerable().Cast<PrjWorkStateTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedWorkStateTypeActiveList);
                    }
                }
                return _cachedWorkStateTypeActiveList;
            }
        }
        protected IEnumerable<PrjWorkStateTypeViewModel> _cachedWorkStateTypeActiveList;

        protected IEnumerable<PrjWorkStateTypeViewModel> cachedWorkStateTypeList
        {
            get
            {
                if (_cachedWorkStateTypeList == null)
                {
                    _cachedWorkStateTypeList = cabCache.GetCachedObject<IEnumerable<PrjWorkStateTypeViewModel>>("cachedWorkStateTypeList");
                    if (_cachedWorkStateTypeList == null)
                    {
                        _cachedWorkStateTypeList = prjWorkStateTypeService.GetList().AsEnumerable().Cast<PrjWorkStateTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedWorkStateTypeList, "cachedWorkStateTypeList");
                    }
                }
                return _cachedWorkStateTypeList;
            }
        }
        protected IEnumerable<PrjWorkStateTypeViewModel> _cachedWorkStateTypeList;

        protected IEnumerable<PrjNotifyTypeViewModel> cachedNotifyTypeList
        {
            get
            {
                if (_cachedNotifyTypeList == null)
                {
                    _cachedNotifyTypeList = cabCache.GetCachedObject<IEnumerable<PrjNotifyTypeViewModel>>();
                    if (_cachedNotifyTypeList == null)
                    {
                        _cachedNotifyTypeList = prjNotifyTypeService.GetList().AsEnumerable().Cast<PrjNotifyTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedNotifyTypeList);
                    }
                }
                return _cachedNotifyTypeList;
            }
        }
        protected IEnumerable<PrjNotifyTypeViewModel> _cachedNotifyTypeList;

        protected List<PrjUserSettingsViewModel> cachedUserSettingsVMList
        {
            get
            {
                if (_cachedUserSettingsVMList == null)
                {
                    _cachedUserSettingsVMList = cabCache.GetCachedObject<List<PrjUserSettingsViewModel>>(CurrentUser.CabUserId.ToString());
                    if (_cachedUserSettingsVMList == null)
                    {
                        _cachedUserSettingsVMList = prjUserSettingsService.GetList_byUser(CurrentUser.CabUserId);
                        cabCache.RefreshCachedObject(_cachedUserSettingsVMList, CurrentUser.CabUserId.ToString());
                    }
                }
                return _cachedUserSettingsVMList;
            }
        }
        protected List<PrjUserSettingsViewModel> _cachedUserSettingsVMList;

        protected IEnumerable<PrjClaimTypeViewModel> cachedClaimTypeList
        {
            get
            {
                if (_cachedClaimTypeList == null)
                {
                    _cachedClaimTypeList = cabCache.GetCachedObject<IEnumerable<PrjClaimTypeViewModel>>();
                    if (_cachedClaimTypeList == null)
                    {
                        _cachedClaimTypeList = prjClaimTypeService.GetList().AsEnumerable().Cast<PrjClaimTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedClaimTypeList);
                    }
                }
                return _cachedClaimTypeList;
            }
        }
        protected IEnumerable<PrjClaimTypeViewModel> _cachedClaimTypeList;

        protected IEnumerable<PrjClaimStageViewModel> cachedClaimStageList
        {
            get
            {
                if (_cachedClaimStageList == null)
                {
                    _cachedClaimStageList = cabCache.GetCachedObject<IEnumerable<PrjClaimStageViewModel>>();
                    if (_cachedClaimStageList == null)
                    {
                        _cachedClaimStageList = prjClaimStageService.GetList().AsEnumerable().Cast<PrjClaimStageViewModel>().OrderBy(ss => ss.claim_stage_name).ToList();
                        cabCache.RefreshCachedObject(_cachedClaimStageList);
                    }
                }
                return _cachedClaimStageList;
            }
        }
        protected IEnumerable<PrjClaimStageViewModel> _cachedClaimStageList;

        protected IEnumerable<PrjTaskUserStateTypeViewModel> cachedTaskUserStateTypeList
        {
            get
            {
                if (_cachedTaskUserStateTypeList == null)
                {
                    _cachedTaskUserStateTypeList = cabCache.GetCachedObject<IEnumerable<PrjTaskUserStateTypeViewModel>>();
                    if (_cachedTaskUserStateTypeList == null)
                    {
                        _cachedTaskUserStateTypeList = prjTaskUserStateTypeService.GetList().AsEnumerable().Cast<PrjTaskUserStateTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedTaskUserStateTypeList);
                    }
                }
                return _cachedTaskUserStateTypeList;
            }
        }
        protected IEnumerable<PrjTaskUserStateTypeViewModel> _cachedTaskUserStateTypeList;

        protected IEnumerable<VersionMainViewModel> cachedVersionMainList
        {
            get
            {
                if (_cachedVersionMainList == null)
                {
                    _cachedVersionMainList = cabCache.GetCachedObject<IEnumerable<VersionMainViewModel>>();
                    if (_cachedVersionMainList == null)
                    {
                        _cachedVersionMainList = versionMainService.GetList().AsEnumerable().Cast<VersionMainViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedVersionMainList);
                    }
                }
                return _cachedVersionMainList;
            }
        }
        protected IEnumerable<VersionMainViewModel> _cachedVersionMainList;

        protected IEnumerable<PrjProjectVersionViewModel> cachedProjectVersionList
        {
            get
            {
                if (_cachedProjectVersionList == null)
                {
                    _cachedProjectVersionList = cabCache.GetCachedObject<IEnumerable<PrjProjectVersionViewModel>>();
                    if (_cachedProjectVersionList == null)
                    {
                        _cachedProjectVersionList = prjProjectVersionService.GetList_Full().AsEnumerable().Cast<PrjProjectVersionViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedProjectVersionList);
                    }
                }
                return _cachedProjectVersionList;
            }
        }
        protected IEnumerable<PrjProjectVersionViewModel> _cachedProjectVersionList;

        protected IEnumerable<PrjClaimStateTypeViewModel> cachedClaimStateTypeList
        {
            get
            {
                if (_cachedClaimStateTypeList == null)
                {
                    _cachedClaimStateTypeList = cabCache.GetCachedObject<IEnumerable<PrjClaimStateTypeViewModel>>();
                    if (_cachedClaimStateTypeList == null)
                    {
                        _cachedClaimStateTypeList = prjClaimStateTypeService.GetList().AsEnumerable().Cast<PrjClaimStateTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedClaimStateTypeList);
                    }
                }
                return _cachedClaimStateTypeList;
            }
        }
        protected IEnumerable<PrjClaimStateTypeViewModel> _cachedClaimStateTypeList;

        protected IEnumerable<PrjClaimFilterOpTypeViewModel> cachedOpTypeList
        {
            get
            {
                if (_cachedOpTypeList == null)
                {
                    _cachedOpTypeList = cabCache.GetCachedObject<IEnumerable<PrjClaimFilterOpTypeViewModel>>();
                    if (_cachedOpTypeList == null)
                    {
                        _cachedOpTypeList = prjClaimFilterOpTypeService.GetList().AsEnumerable().Cast<PrjClaimFilterOpTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedOpTypeList);
                    }
                }
                return _cachedOpTypeList;
            }
        }
        protected IEnumerable<PrjClaimFilterOpTypeViewModel> _cachedOpTypeList;

        protected IEnumerable<VersionTypeViewModel> cachedVersionTypeList
        {
            get
            {
                if (_cachedVersionTypeList == null)
                {
                    _cachedVersionTypeList = cabCache.GetCachedObject<IEnumerable<VersionTypeViewModel>>();
                    if (_cachedVersionTypeList == null)
                    {
                        _cachedVersionTypeList = versionTypeService.GetList().AsEnumerable().Cast<VersionTypeViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedVersionTypeList);
                    }
                }
                return _cachedVersionTypeList;
            }
        }
        protected IEnumerable<VersionTypeViewModel> _cachedVersionTypeList;

        //claimFilterList
        protected List<PrjClaimFilterViewModel> cachedClaimFilterList
        {
            get
            {
                if (_cachedClaimFilterList == null)
                {
                    _cachedClaimFilterList = cabCache.GetCachedObject<List<PrjClaimFilterViewModel>>(CurrentUser.CabUserId.ToString());
                    if (_cachedClaimFilterList == null)
                    {
                        _cachedClaimFilterList = prjClaimFilterService.GetList_byParent(CurrentUser.CabUserId).AsEnumerable().Cast<PrjClaimFilterViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedClaimFilterList, CurrentUser.CabUserId.ToString());
                    }
                }
                return _cachedClaimFilterList;
            }
        }
        protected List<PrjClaimFilterViewModel> _cachedClaimFilterList;

        protected IEnumerable<CrmProjectVersionViewModel> cachedCrmProjectVersionList
        {
            get
            {
                if (_cachedCrmProjectVersionList == null)
                {
                    _cachedCrmProjectVersionList = cabCache.GetCachedObject<IEnumerable<CrmProjectVersionViewModel>>();
                    if (_cachedCrmProjectVersionList == null)
                    {
                        _cachedCrmProjectVersionList = crmProjectVersionService.GetList().AsEnumerable().Cast<CrmProjectVersionViewModel>().ToList();
                        cabCache.RefreshCachedObject(_cachedCrmProjectVersionList);
                    }
                }
                return _cachedCrmProjectVersionList;
            }
        }
        protected IEnumerable<CrmProjectVersionViewModel> _cachedCrmProjectVersionList;


        #endregion

        #region Single

        protected PrjDefaultViewModel cachedUserPrjDefault
        {
            get
            {
                if (_cachedUserPrjDefault == null)
                {
                    _cachedUserPrjDefault = cabCache.GetCachedObject<PrjDefaultViewModel>(CurrentUser.CabUserId.ToString());
                    if (_cachedUserPrjDefault == null)
                    {
                        _cachedUserPrjDefault = (PrjDefaultViewModel)prjDefaultService.GetItem(CurrentUser.CabUserId);
                        if (_cachedUserPrjDefault == null)
                            _cachedUserPrjDefault = prjDefaultService.GetItem_Default();
                        cabCache.RefreshCachedObject(_cachedUserPrjDefault, CurrentUser.CabUserId.ToString());
                    }
                }
                return _cachedUserPrjDefault;
            }
        }
        protected PrjDefaultViewModel _cachedUserPrjDefault;

        protected PrjFileParam cachedPrjFileParam
        {
            get
            {
                if (_cachedPrjFileParam == null)
                {
                    _cachedPrjFileParam = cabCache.GetCachedObject<PrjFileParam>();
                    if (_cachedPrjFileParam == null)
                    {
                        _cachedPrjFileParam = prjFileService.GetPrjFileParam();
                        cabCache.RefreshCachedObject(_cachedPrjFileParam);
                    }
                }
                return _cachedPrjFileParam;
            }
        }
        protected PrjFileParam _cachedPrjFileParam;

        #endregion

        #endregion

    }
}