﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace CabinetMvc.Controllers
{
    [Compress]
    public partial class BaseController : Controller
    {

        protected static Ninject.IKernel ninjectKernel;
        protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        static BaseController()
        {
            //logger.Info("BaseController static start");
            ninjectKernel = DependencyResolver.Current.GetService<Ninject.IKernel>();
            //
            #region SiteMapManager

            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("cab"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("cab", map => map.LoadFrom("~/cab.sitemap"));
            }
            registerSiteMaps(new List<string>() { "main", "discount", "defect", "crm", "exchange", "log", "client", "cva", "delivery"
                , "cabsprav", "helpdesk", "reg", "cabuser", "auth", "storage", "update", "prj", "stock", "asna" });

            #region old
            /*
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("main"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("main", map => map.LoadFrom("~/cab.main.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("discount"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("discount", map => map.LoadFrom("~/cab.discount.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("defect"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("defect", map => map.LoadFrom("~/cab.defect.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("crm"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("crm", map => map.LoadFrom("~/cab.crm.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("exchange"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("exchange", map => map.LoadFrom("~/cab.exchange.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("log"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("log", map => map.LoadFrom("~/cab.log.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("client"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("client", map => map.LoadFrom("~/cab.client.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("cva"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("cva", map => map.LoadFrom("~/cab.cva.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("delivery"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("delivery", map => map.LoadFrom("~/cab.delivery.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("cabsprav"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("cabsprav", map => map.LoadFrom("~/cab.cabsprav.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("helpdesk"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("helpdesk", map => map.LoadFrom("~/cab.helpdesk.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("reg"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("reg", map => map.LoadFrom("~/cab.reg.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("cabuser"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("cabuser", map => map.LoadFrom("~/cab.cabuser.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("auth"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("auth", map => map.LoadFrom("~/cab.auth.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("storage"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("storage", map => map.LoadFrom("~/cab.storage.sitemap"));
            }
            if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey("planner"))
            {
                Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>("planner", map => map.LoadFrom("~/cab.planner.sitemap"));
            }
            */
            #endregion

            #endregion
            //
            cabCache = new CabCache();
            //logger.Info("BaseController static end");
        }

        #region registerSiteMaps
        private static void registerSiteMaps(List<string> siteMaps)
        {
            foreach (var siteMap in siteMaps)
            {
                if (!Kendo.Mvc.SiteMapManager.SiteMaps.ContainsKey(siteMap))
                {
                    Kendo.Mvc.SiteMapManager.SiteMaps.Register<Kendo.Mvc.XmlSiteMap>(siteMap, map => map.LoadFrom("~/cab." + siteMap + ".sitemap"));
                }
            }
        }
        #endregion

        public BaseController()
            : base()
        {            
            //      
            //logger.Info("BaseController constructor");
        }

        #region IsAuthorize

        [AllowAnonymous]
        [HttpPost]
        public JsonResult IsAuthorized()
        {            
            if (Request.IsAuthenticated)
            {
                var notifyList = prjNotifyService.GetPrjNotifyList_forSend();
                //return Json(new { is_auth = true, notify = JsonConvert.SerializeObject(notifyList) }, JsonRequestBehavior.AllowGet);
                return Json(new { is_auth = true, notify = notifyList }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { is_auth = false, }, JsonRequestBehavior.AllowGet);
            }            
            //return Json(new { is_auth = false }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CabCacheClear
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult CabCacheClear()
        {            
            _cachedCabUserList = null;
            _cachedClaimStageList = null;
            _cachedClaimTypeList = null;
            _cachedClientList = null;
            _cachedModuleList = null;
            _cachedModulePartList = null;
            _cachedModuleVersionList = null;
            _cachedNotifyTypeList = null;
            _cachedPriorityList = null;
            _cachedPrjFileParam = null;
            _cachedPrjList = null;            
            _cachedProjectList = null;
            _cachedTaskUserStateTypeList = null;
            _cachedUserPrjDefault = null;
            _cachedUserSettingsVMList = null;
            _cachedWorkStateTypeActiveList = null;            
            _cachedWorkStateTypeList = null;            
            _cachedWorkTypeList = null;
            _cachedClaimStateTypeList = null;
            _cachedOpTypeList = null;
            _cachedClaimFilterList = null;
            _cachedCrmProjectVersionList = null;

            //cabCache = null;
            //cabCache = new CabCache();
            cabCache.Clear();
            return new EmptyResult();
        }
        #endregion

        #region RedirectToErrorPage

        protected RedirectToRouteResult RedirectToErrorPage(ModelStateDictionary modelStateWithErrors)
        {
            string err_mess = "";
            if (!modelStateWithErrors.IsValid)
            {
                foreach (var errors in modelStateWithErrors.Values.Where(ss => ss.Errors != null).Select(ss => ss.Errors))
                {
                    foreach (var err in errors)
                    {
                        err_mess += err.ErrorMessage + "; ";
                    }
                }
                TempData.Clear();
                TempData.Add("error", err_mess);                
            }
            return RedirectToAction("Oops", "Home");
        }

        #endregion
        
    }
}
