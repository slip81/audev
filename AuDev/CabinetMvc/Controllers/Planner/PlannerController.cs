﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Drawing;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Planner;
using CabinetMvc.ViewModel.Sys;
using CabinetMvc.Extensions;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public class PlannerController : BaseController
    {

        #region Planner

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PLANNER)]
        public ActionResult Planner()
        {
            ViewBag.PriorityList = plannerEventPriorityService.GetList().AsEnumerable();

            ViewBag.TaskGridColumnSettings = cabGridColumnUserSettingsService.GetList_byParent((int)Enums.CabGrid.TaskList);
            var gridUserSettings = cabGridUserSettingService.GetItem((int)Enums.CabGrid.TaskList);
            string gridSortOptions = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).sort_options : "";
            string gridFilterOptions = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).filter_options : "";
            ViewBag.TaskGridSortOptions = new JavaScriptSerializer().Serialize(gridSortOptions);
            ViewBag.TaskGridFilterOptions = new JavaScriptSerializer().Serialize(gridFilterOptions);
            ViewBag.TaskGridColumnWidths = gridUserSettings == null ? "" : ((CabGridUserSettingsViewModel)gridUserSettings).columns_options;
            ViewBag.TaskGridColorColumn = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).back_color_column_id : null;
            
            ViewBag.ExecuterList = cabUserService.GetList_Executer().Cast<CabinetMvc.ViewModel.Adm.CabUserViewModel>().ToList();
            ViewBag.EventStateList = crmStateService.GetList_forEvent();

            ViewBag.FilterStateList = crmStateService.GetList();            
            ViewBag.FilterUserList = cabUserService.GetList_CrmUser();
            ViewBag.FilterProjectList = crmProjectService.GetList();
            ViewBag.FilterClientList = crmClientService.GetList();
            ViewBag.FilterPriorityList = crmPriorityService.GetList();
            ViewBag.UserFilter = crmTaskUserFilterService.GetItem((int)Enums.CabGrid.TaskList);
            ViewBag.FilterBatchList = crmBatchService.GetList();

            ViewBag.OperationList = crmTaskGroupOperationService.GetList();

            // очень плохо, надо переделать
            var stateList = crmStateService.GetList_forEdit();            
            var state1_color = stateList.Where(ss => ss.state_id == 1).Select(ss => ss.fore_color).FirstOrDefault();
            var state2_color = stateList.Where(ss => ss.state_id == 2).Select(ss => ss.fore_color).FirstOrDefault();
            var state3_color = stateList.Where(ss => ss.state_id == 3).Select(ss => ss.fore_color).FirstOrDefault();
            var state4_color = stateList.Where(ss => ss.state_id == 4).Select(ss => ss.fore_color).FirstOrDefault();
            var state5_color = stateList.Where(ss => ss.state_id == 5).Select(ss => ss.fore_color).FirstOrDefault();
            var state6_color = stateList.Where(ss => ss.state_id == 6).Select(ss => ss.fore_color).FirstOrDefault();
            var state7_color = stateList.Where(ss => ss.state_id == 7).Select(ss => ss.fore_color).FirstOrDefault();
            var state8_color = stateList.Where(ss => ss.state_id == 8).Select(ss => ss.fore_color).FirstOrDefault();

            ViewBag.state1_color = state1_color;
            ViewBag.state2_color = state2_color;
            ViewBag.state3_color = state3_color;
            ViewBag.state4_color = state4_color;
            ViewBag.state5_color = state5_color;
            ViewBag.state6_color = state6_color;
            ViewBag.state7_color = state7_color;
            ViewBag.state8_color = state8_color;

            return View();
        }

        #endregion

        #region PlannerEvent

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult GetPlannerEventList([DataSourceRequest] DataSourceRequest request, DateTime date_beg, DateTime date_end, int? exec_user_id, int public_filter_mode)
        {
            return Json(plannerEventService.GetList_byFilter(date_beg, date_end, exec_user_id, public_filter_mode).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult GetPlannerEventExecUserList(DataSourceRequest request, DateTime date_beg, DateTime date_end, int? exec_user_id, int public_filter_mode)
        {
            return Json(new { ids = plannerEventService.GetPlannerEventExecUserList(date_beg, date_end, exec_user_id, public_filter_mode) });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult PlannerEventAdd([DataSourceRequest] DataSourceRequest request, PlannerEventViewModel item, int? curr_user_id)
        {
            if (ModelState.IsValid)
            {
                item.exec_user_id = curr_user_id.HasValue ? curr_user_id : item.exec_user_id;
                if (!item.exec_user_id.HasValue)
                {
                    ModelState.AddModelError("", "Не указан исполнитель");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }
                var res = plannerEventService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении события");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }
                PlannerEventViewModel itemNew = (PlannerEventViewModel)res;
                return Json(new[] { itemNew }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult PlannerEventEdit([DataSourceRequest] DataSourceRequest request, PlannerEventViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = plannerEventService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании события");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }
                PlannerEventViewModel itemNew = (PlannerEventViewModel)res;
                return Json(new[] { itemNew }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult PlannerEventDel([DataSourceRequest] DataSourceRequest request, PlannerEventViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = plannerEventService.Delete(item, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении события");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region UpcomingEvent

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult GetUpcomingEventList([DataSourceRequest] DataSourceRequest request, int days_forward, int? exec_user_id)
        {            
            DateTime date_beg = DateTime.Today.AddDays(1);
            DateTime date_end = DateTime.Today.AddDays(days_forward);
            return Json(plannerEventService.GetUpcomingEventList(date_beg, date_end, exec_user_id.GetValueOrDefault(-1), (int)Enums.PlannerEventPublicFilterModeEnum.MY).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        [HttpPost]
        public JsonResult UpcomingEventAdd(int? note_id, int? event_id, int? task_id, bool fromNotes, bool fromScheduler, bool fromTask, DateTime? upcoming_event_date, int? days_add, int? exec_user_id)
        {
            if (ModelState.IsValid)
            {
                var res = plannerEventService.UpcomingEventAdd(note_id, event_id, task_id, fromNotes, fromScheduler, fromTask, upcoming_event_date, days_add, exec_user_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении события");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        #endregion
        
        #region OverdueEvent

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult GetOverdueEventList([DataSourceRequest] DataSourceRequest request, int? exec_user_id)
        {
            return Json(plannerEventService.GetList_Overdue(exec_user_id).ToDataSourceResult(request));
        }

        #endregion
        
        #region StrategEvent

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult GetStrategEventList([DataSourceRequest] DataSourceRequest request, int? exec_user_id, bool is_active)
        {
            return Json(plannerEventService.GetList_Strateg(exec_user_id, is_active).ToDataSourceResult(request));
        }

        #endregion

        #region PlannerNote

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult GetPlannerNoteList([DataSourceRequest] DataSourceRequest request, int? exec_user_id)
        {
            return Json(plannerNoteService.GetList_byParent(exec_user_id.GetValueOrDefault(-1)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult PlannerNoteAdd([DataSourceRequest] DataSourceRequest request, PlannerNoteViewModel item, int? curr_user_id)
        {
            if (ModelState.IsValid)
            {
                if (String.IsNullOrEmpty(item.description))
                {
                    ModelState.AddModelError("", "Не задан текст");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }

                item.exec_user_id = curr_user_id.HasValue ? curr_user_id : item.exec_user_id;
                var res = plannerNoteService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении записи");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }
                //PlannerNoteViewModel itemNew = (PlannerNoteViewModel)res;
                //return Json(new[] { itemNew }.ToDataSourceResult(request, ModelState));
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        public virtual JsonResult PlannerNoteEdit([DataSourceRequest] DataSourceRequest request, PlannerNoteViewModel item)
        {            
            if (ModelState.IsValid)
            {
                if (String.IsNullOrEmpty(item.description))
                    return PlannerNoteDel(item.note_id);

                var res = plannerNoteService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании записи");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }
                //PlannerNoteViewModel itemNew = (PlannerNoteViewModel)res;
                //return Json(new[] { itemNew }.ToDataSourceResult(request, ModelState));
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        [HttpPost]
        public JsonResult PlannerNoteDel(int note_id)
        {
            if (ModelState.IsValid)
            {
                var res = plannerNoteService.Delete_byId(note_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении записи");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
            }
            return Json(new { err = false });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        [HttpPost]
        public void PlannerNoteReorder(int note_id, int new_index)
        {
            plannerNoteService.PlannerNoteReorder(note_id, new_index);
        }

        //PlannerNoteAddTasks
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        [HttpPost]
        public JsonResult PlannerNoteAddTasks(string task_id_list, int? curr_user_id)
        {
            if (ModelState.IsValid)
            {
                var res = plannerNoteService.PlannerNoteAddTasks(task_id_list, curr_user_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении задач");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
            }
            return Json(new { err = false });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        [HttpGet]
        public ActionResult PlannerNoteEditWin(int? note_id)
        {
            ViewData["eventStateList"] = crmStateService.GetList_forEvent();
            var model = note_id.GetValueOrDefault(0) > 0 ? plannerNoteService.GetItem((int)note_id) : new CabinetMvc.ViewModel.Planner.PlannerNoteViewModel() { is_public = true, };
            return View(model);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PLANNER)]
        [HttpPost]
        public ActionResult PlannerNoteEditWin(PlannerNoteViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel note_res = null;
                if (itemEdit.note_id > 0)
                {
                    note_res = plannerNoteService.Update(itemEdit, ModelState);
                }
                else
                {
                    note_res = plannerNoteService.Insert(itemEdit, ModelState);
                }
                if ((note_res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении записи");
                    TempData["error"] = ModelState.GetErrorsString();

                    return Json(new { err = true, mess = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                }                
                return Json(new { err = false, planner_note = note_res }, JsonRequestBehavior.AllowGet);                                    
            }
            else
            {
                TempData["error"] = ModelState.GetErrorsString();
                return Json(new { err = true, mess = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region PlannerExport
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PLANNER)]
        [HttpGet]
        public ActionResult PlannerExcelExport(DateTime date_beg, DateTime date_end, int? user_id, string user_name)
        {
            string date_beg_str = date_beg.ToShortDateString();
            string date_end_str = date_end.ToShortDateString();

            string reportTitle = "Список задач за период c " + date_beg_str + " по " + date_end_str;
            if (user_id.GetValueOrDefault(0) > 0)
                reportTitle += " [Исполнитель: " + user_name + "]";
            else
                reportTitle += " [Исполнители: все]";

            string filePath = @"c:\Release\Cabinet\Report\planner_event_" + CurrentUser.CabUserId.ToString() + ".xlsx";
            string contentType = "application/vnd.ms-excel";

            var events = crmTaskService.GetList_forPlannerExport(date_beg, date_end, user_id).ToList();
            //var events = plannerEventService.GetList_byFilter(date_beg, date_end, user_id, (int)Enums.PlannerEventPublicFilterModeEnum.ALL).ToList();
            if ((events == null) || (events.Count <= 0))
            {
                ModelState.AddModelError("", "Не найдены задачи");
                return RedirectToErrorPage(ModelState);
            }

            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Properties.Author = "Аурит";
                p.Workbook.Properties.Title = reportTitle;

                p.Workbook.Worksheets.Add("Страница 1");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";

                int reportsfieldscount = 18;
                int colIndex = 1;
                int rowIndex = 2;
                int i = 1;

                string task_num = "";
                string task_name = "";
                string task_text = "";
                string state_name = "";
                string exec_user_name = "";
                string assigned_user_name = "";
                string client_name = "";
                string fin_cost = "";
                string project_name = "";
                string module_name = "";
                string module_version_name = "";
                string date_plan = "";
                string date_fact = "";
                string repair_version_name = "";
                string owner_user_name = "";
                string crt_date = "";
                string upd_date = "";
                string priority_name = "";
                string task_id = "";

                #region

                using (var range = ws.Cells[1, 1, 1, reportsfieldscount])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                while (i <= reportsfieldscount)
                {
                    ws.Column(i).Width = 40;
                    i++;
                }

                //Merging cells and create a center heading for out table
                ws.Cells[1, 1].Value = reportTitle;
                ws.Cells[1, 1, 1, reportsfieldscount].Merge = true;
                ws.Cells[1, 1, 1, reportsfieldscount].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                using (var range = ws.Cells[2, 1, 2, reportsfieldscount])
                {
                    range.Style.Font.Bold = false;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                ws.Cells[rowIndex, colIndex].Value = "№ задачи";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Кратко";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Подробно";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Статус";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Исполнитель";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Ответственный";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Клиент";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Цена";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Проект";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Модуль";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Версия модуля";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Плановая дата";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Фактическая дата";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                //ws.Cells[rowIndex, colIndex].Value = "Версия исправления";
                //ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Автор";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Дата создания";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Дата изменения";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Приоритет";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Код задачи";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;

                foreach (var ev in events)
                {
                    task_num = ev.task_num;
                    task_name = ev.task_name;
                    task_text = ev.task_text;
                    state_name = ev.state_name;
                    exec_user_name = ev.exec_user_name;
                    assigned_user_name = ev.assigned_user_name;
                    client_name = ev.client_name;
                    fin_cost = ev.fin_cost.HasValue ? ev.fin_cost.ToString() : "";
                    project_name = ev.project_name;
                    module_name = ev.module_name;
                    module_version_name = ev.module_version_name;
                    date_plan = ev.required_date.HasValue ? ((DateTime)ev.required_date).ToString("dd.MM.yyyy") : "";
                    date_fact = ev.repair_date.HasValue ? ((DateTime)ev.repair_date).ToString("dd.MM.yyyy") : "";
                    owner_user_name = ev.owner_user_name;
                    crt_date = ev.crt_date.HasValue ? ((DateTime)ev.crt_date).ToString("dd.MM.yyyy HH:mm:ss") : "";
                    upd_date = ev.upd_date.HasValue ? ((DateTime)ev.upd_date).ToString("dd.MM.yyyy HH:mm:ss") : "";
                    priority_name = ev.priority_name;
                    task_id = ev.task_id.ToString();

                    colIndex = 1;
                    rowIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_num;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    ws.Cells[rowIndex, colIndex].Style.WrapText = true;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_text;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    ws.Cells[rowIndex, colIndex].Style.WrapText = true;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = state_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = exec_user_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = assigned_user_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = client_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = fin_cost;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = project_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = module_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = module_version_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = date_plan;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = date_fact;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = owner_user_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = crt_date;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = upd_date;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = priority_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_id;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                }
                #endregion

                // AutoFitColumns
                // ws.Cells[ws.Dimension.Address].AutoFitColumns();

                Byte[] bin = p.GetAsByteArray();
                System.IO.File.WriteAllBytes(filePath, bin);
            }

            return File(filePath, contentType, "Календарь задач.xlsx");
        }
        #endregion

    }
}
