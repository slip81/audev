﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CabinetMvc.Auth;
using CabinetMvc.ViewModel.User;
using CabinetMvc.ViewModel.Auth;
using AuDev.Common.Util;
namespace CabinetMvc.Controllers
{
    public class AuthUserPermEditAttribute : AuthorizeAttribute
    {
        private readonly int currPart;
        public AuthUserPermEditAttribute(Enums.AuthPartEnum part)
        {
            this.currPart = (int)part;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var user = ((IUserProvider)auth.CurrentUser.Identity).User;

            if (currPart <= 0)
                return authorize;
            if (user == null)
                return authorize;

            // !!!
            if (user.AllowedParts_Edit == null)
                return true;

            authorize = user.AllowedParts_Edit.Where(ss => ss == currPart).Any();
            return authorize;

            /*

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var user = ((IUserProvider)auth.CurrentUser.Identity).User;

            if (currPart <= 0)
                return authorize;
            if (user == null)
                return authorize;

            var authUserPermServ = DependencyResolver.Current.GetService<IAuthUserPermService>();
            authorize = authUserPermServ.IsAllowedEdit(user.CabUserId, currPart);
            return authorize;
            */
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }

}