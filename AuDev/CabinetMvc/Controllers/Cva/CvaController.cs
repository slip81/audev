﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Exchange;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public class CvaController : BaseController
    {        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult Index()
        {
            return View();
        }

        #region CvaLogList

        [ClientServiceAuthorize(Enums.ClientServiceEnum.CVA)]
        [AuthUserPermView(Enums.AuthPartEnum.CVA)]
        public ActionResult CvaLogList()
        {
            ViewBag.ClientList = clientService.GetList_byService((int)Enums.ClientServiceEnum.CVA);
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.CVA)]
        [AuthUserPermView(Enums.AuthPartEnum.CVA)]
        public ActionResult GetCvaLogList([DataSourceRequest]DataSourceRequest request, int? client_id, int? sales_id)
        {
            return Json(serviceCvaDataService.GetList_byClientOrSales(client_id.GetValueOrDefault(0), sales_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        #endregion

        #region CvaDataView

        [ClientServiceAuthorize(Enums.ClientServiceEnum.CVA)]
        [AuthUserPermView(Enums.AuthPartEnum.CVA)]
        public ActionResult CvaDataView(int obj_id)
        {
            var cva_data = serviceCvaDataService.GetItem_Data(obj_id);           
            if (cva_data == null)
            {
                cva_data = "Данные отсутствуют";
            }
            ServiceCvaDataViewModel model = new ServiceCvaDataViewModel() { cva_full_data = cva_data };
            return View(model);
        }

        #endregion
    }
}
