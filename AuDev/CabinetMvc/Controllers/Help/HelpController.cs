﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class HelpController : BaseController
    {
        #region CabHelp

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpGet]
        public ActionResult CabHelp(int? help_id)
        {
            ViewBag.HelpId = help_id.GetValueOrDefault(0) > 0 ? help_id.GetValueOrDefault(0).ToString() : "1";
            string menuPath = "";

            Enums.CabHelpEnum cabHelpEnum = Enums.CabHelpEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.CabHelpEnum>(help_id.GetValueOrDefault(0).ToString(), out cabHelpEnum);
            if (!parseOk)
                cabHelpEnum = Enums.CabHelpEnum.NONE;
            switch (cabHelpEnum)
            {
                case Enums.CabHelpEnum.CAB:
                case Enums.CabHelpEnum.MAIN:
                    ViewBag.MenuName = "main";
                    ViewBag.MenuHeader = "Главные разделы";
                    menuPath = "<a href='/Main'>Главная</a> - <a href='/CabHelp?help_id=4'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.DISCOUNT:
                    ViewBag.MenuName = "discount";
                    ViewBag.MenuHeader = "Дисконтные карты";
                    menuPath = "<a href='/CardList'>Услуги</a> - <a href='/CardList'>Дисконтные карты</a> - <a href='/CabHelp?help_id=4'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.EXCHANGE:
                    ViewBag.MenuName = "exchange";
                    ViewBag.MenuHeader = "Обмен с партнерами";
                    menuPath = "<a href='/CardList'>Услуги</a> - <a href='/ExchangeLogList'>Обмен с партнерами</a> - <a href='/CabHelp?help_id=12'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.DEFECT:
                    ViewBag.MenuName = "defect";
                    ViewBag.MenuHeader = "Брак";
                    menuPath = "<a href='/CardList'>Услуги</a> - <a href='/DefectList'>Брак</a> - <a href='/CabHelp?help_id=13'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.CVA:
                    ViewBag.MenuName = "cva";
                    ViewBag.MenuHeader = "ЦеныВАптеках.РФ";
                    menuPath = "<a href='/CardList'>Услуги</a> - <a href='/CvaLogList'>ЦВА</a> - <a href='/CabHelp?help_id=14'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.CLIENT:
                case Enums.CabHelpEnum.CLIENT_LIST:
                    ViewBag.MenuName = "client";
                    ViewBag.MenuHeader = "Клиенты";
                    menuPath = "<a href='/ClientList'>Клиенты</a> - <a href='/CabHelp?help_id=6'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.SPRAV:
                    ViewBag.MenuName = "cabsprav";
                    ViewBag.MenuHeader = "Справочники";
                    menuPath = "<a href='/ClientList'>Клиенты</a> - <a href='/CabServiceList'>Справочники</a> - <a href='/CabHelp?help_id=17'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.DELIVERY:
                    ViewBag.MenuName = "delivery";
                    ViewBag.MenuHeader = "Рассылка";
                    menuPath = "<a href='/ClientList'>Клиенты</a> - <a href='/DeliveryList'>Рассылка</a> - <a href='/CabHelp?help_id=18'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.CRM_TASK:
                    ViewBag.MenuName = "crm";
                    ViewBag.MenuHeader = "Задачи";
                    menuPath = "<a href='/TaskList'>Сервис</a> - <a href='/TaskList'>Задачи</a> - <a href='/CabHelp?help_id=20'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.STORAGE: 
                    ViewBag.MenuName = "storage";
                    ViewBag.MenuHeader = "Хранилище";
                    menuPath = "<a href='/TaskList'>Сервис</a> - <a href='/StorageTree'>Хранилище</a> - <a href='/CabHelp?help_id=21'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.CAB_LOG:
                    ViewBag.MenuName = "log";
                    ViewBag.MenuHeader = "Журнал событий";
                    menuPath = "<a href='/CabinetLogList'>Админ</a> - <a href='/CabinetLogList'>Журнал событий</a> - <a href='/CabHelp?help_id=22'>Справка</a>";
                    break;
                case Enums.CabHelpEnum.AUTH:
                    ViewBag.MenuName = "auth";
                    ViewBag.MenuHeader = "Полномочия";
                    menuPath = "<a href='/CabinetLogList'>Админ</a> - <a href='/AuthList'>Полномочия</a> - <a href='/CabHelp?help_id=23'>Справка</a>";
                    break;
                default:
                    ViewBag.MenuName = "";
                    ViewBag.MenuHeader = "";
                    break;
            }

            Session["cabHelpTags"] = cabHelpService.GetTagList();
            
            if (help_id.GetValueOrDefault(0) > 1)
            {
                ViewBag.MenuPath = menuPath;
            }
            else
            {
                ViewBag.MenuPath = "";
            }

            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult CabHelpPart(int? help_id)
        {
            ViewBag.HelpId = help_id.GetValueOrDefault(0) > 0 ? help_id.GetValueOrDefault(0).ToString() : "1";
            ViewBag.CabHelpTree = cabHelpService.GetTreeList();
            ViewBag.CabHelpCollapsed = help_id.GetValueOrDefault(0) > 1;
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult CabHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetCabHelpTags(string text)
        {
            if (String.IsNullOrEmpty(text))
                return null;
            List<string> cabHelpTags = (List<string>)Session["cabHelpTags"];
            if (cabHelpTags == null)
            {
                cabHelpTags = cabHelpService.GetTagList();
            }            
            var result = cabHelpTags.Where(ss => ss.Trim().ToLower().StartsWith(text.Trim().ToLower())).ToList();
            if ((result == null) || (result.Count <= 0))
                result = cabHelpTags.Where(ss => ss.Trim().ToLower().Contains(text.Trim().ToLower())).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public JsonResult CabHelpSearch(string help_name)
        {
            if (ModelState.IsValid)
            {
                var res = cabHelpService.Find(help_name);
                if ((res == null) || (res.Count() <= 0) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Разделы не найдены");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }                
                return Json(new { err = false, mess = "", helpList = JsonConvert.SerializeObject(res) });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        #endregion

        #region Help

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult MainHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult ServiceHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult ClientHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult ServHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult AdminHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult MyPagesHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult UserMenuHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult UserProfileHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult DiscountHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult ExchangeHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult DefectHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult CvaHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult ClientListHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult RegHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult CabSpravHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult DeliveryHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult HelpDeskHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult CrmTaskHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult StorageHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult LogHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult AuthHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult ChangePasswordHelpPartial()
        {
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult LogoutHelpPartial()
        {
            return PartialView();
        }

        #endregion
    }
}
