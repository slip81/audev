﻿using System.Web.Mvc;
using Kendo.Mvc.UI;

namespace CabinetMvc.Controllers
{
    public class CustomDataSourceRequestAttribute : DataSourceRequestAttribute
    {
        public override IModelBinder GetBinder()
        {
            return new CustomDataSourceRequestModelBinder();
        }
    }
}