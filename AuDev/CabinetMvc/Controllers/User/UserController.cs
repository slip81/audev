﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.User;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.Auth;
using CabinetMvc.Extensions;
using AuDev.Common.Util;


namespace CabinetMvc.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        #region Login / Logout

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string ReturnUrl)
        {            
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(UserViewModel user)
        {
            if (ModelState.IsValid) 
            {
                var res = Auth.Login(user.UserName, user.Password, user.RememberMe); 
                if (res != null) 
                {
                    if (!String.IsNullOrEmpty(user.ReturnUrl))
                    {
                        return Redirect(user.ReturnUrl);
                        //return RedirectToAction("Index", "Home"); 
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home"); 
                    }
                    
                } 
                
                ModelState["Password"].Errors.Add("Неверный логин/пароль"); 
            }
            return View(user);
        }

        public ActionResult Logout()
        {
            Auth.LogOut();
            return RedirectToAction("Index", "Home");
        }

        #endregion
        
        #region Register

        /*
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Register()
        {
            RegViewModel reg = new RegViewModel() { login = "1", pwd = "2", };
            return View(reg);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(RegViewModel regAdd)
        {
            
            if (ModelState.IsValid)
            {
                var res = regService.Insert(regAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Неизвестная ошибка при регистрации");
                    //return RedirectToErrorPage(ModelState);
                    return View(regAdd);
                }
                return RedirectToAction("RegisterSuccess", "User");
            }
            return View(regAdd);
        }
        */

        [AllowAnonymous]
        [HttpGet]
        public ActionResult RegisterSuccess()
        {
            return View();
        }

        #endregion

        #region UserProfile

        [HttpGet]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult UserProfile()
        {            
            string curr_user_name = CurrentUser.CabUserLogin;
            UserProfileViewModel item = userProfileService.GetItem_byUserName(curr_user_name);
            if (item == null)
                item = new UserProfileViewModel() { user_name = curr_user_name, };

            List<UserMenuViewModel> startPageList = new List<UserMenuViewModel>();

            startPageList.Add(new UserMenuViewModel()
            {
                //Text = "Главная",
                //Value = "",
                //Group = new SelectListGroup() { Name = m.Value.RootNode.Title, }
                title = "Главная",
                action = "Main",
                controller = "Home",
                img = "start.png",
            });

            foreach (var m in Kendo.Mvc.SiteMapManager.SiteMaps)
            {
                if (m.Value.RootNode.Title.Trim().ToLower().Equals("cabsitemap"))
                    continue;

                startPageList.Add(new UserMenuViewModel()
                {
                    //Text = "----- " + m.Value.RootNode.Title + " -----",
                    //Value = "-1",
                    //Group = new SelectListGroup() { Name = m.Value.RootNode.Title, }
                    title = "----- " + m.Value.RootNode.Title + " -----",
                    action = "-1",
                    controller = "",
                    img = "",
                });

                foreach (var page in m.Value.RootNode.ChildNodes)
                {
                    startPageList.Add(new UserMenuViewModel()
                    {
                        //Text = page.Title,
                        //Value = page.ActionName,
                        //Group = new SelectListGroup() { Name = m.Value.RootNode.Title, }
                        title = page.Title,
                        action = page.ActionName,
                        controller = page.ControllerName,                        
                        img = CabinetUtil.SiteMapNode_GetImagePath(page),
                    });
                }
            }

            ViewBag.UserMenuList = userMenuService.GetList_byUserName(curr_user_name).ToList();
            ViewBag.StartPageList = startPageList;
            item.start_page = String.IsNullOrEmpty(item.start_page) ? "Main" : item.start_page.ToLower().Trim();
            item.start_page_title = String.IsNullOrEmpty(item.start_page) ? "Главная" : startPageList.Where(ss => ss.action.ToLower().Trim() == item.start_page.ToLower().Trim()).FirstOrDefault().title;
            item.user_name = curr_user_name;
            return View(item);
        }

        [HttpPost]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public JsonResult UserProfile(UserProfileViewModel item)
        {

            // !!!
            // return null;

            if (ModelState.IsValid)
            {
                var exisitingItem = userProfileService.GetItem(item.profile_id);

                ViewModel.AuBaseViewModel res = null;
                if (exisitingItem == null)
                {
                    res = userProfileService.Insert(item, ModelState);
                }
                else
                {
                    res = userProfileService.Update(item, ModelState);
                }

                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошбика при сохранении профиля");
                    //return RedirectToErrorPage(ModelState);
                    return Json(new { err = true, mess = ModelState.GetErrorsString(), });
                }

                //return RedirectToAction("UserProfile");
                return Json(new { err = false, mess ="", });
            }
            //return View(item);
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }

        #endregion

        #region ChangePassword

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]        
        public JsonResult UserPasswordEdit(string pwd1, string pwd2, string pwd3)
        {
            var res = cabUserService.ChangePassword(pwd1, pwd2, pwd3, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка при смене пароля");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false, mess = "" });
        }
            
        #endregion

        #region RestorePassword

        [AllowAnonymous]
        [HttpPost]
        public JsonResult RestorePassword(string user_email)
        {
            var res = cabUserService.RestorePassword(user_email, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка восстановления");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false, mess = "" });
        }

        #endregion
    }
}