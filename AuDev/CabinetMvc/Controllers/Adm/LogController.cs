﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class AdmController
    {
        #region CabinetLogList

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult CabinetLogList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult GetCabinetLogList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(logCabService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult CabinetLogView(long log_id)
        {
            var ev = logCabService.GetItem(log_id);
            if (ev == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найдено событие с кодом " + log_id.ToString());
                return RedirectToAction("Oops", "Home");
            }

            LogCabViewModel log = (LogCabViewModel)ev;
            log.modelObj1 = log.obj1 != null ? (AuBaseViewModel)log.obj1.DeserializeObjValue(log.obj_type_name) : null;
            log.modelObj2 = log.obj2 != null ? (AuBaseViewModel)log.obj2.DeserializeObjValue(log.obj_type_name) : null;
            return View(log);
        }

        #endregion

        #region DiscountLogList

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult DiscountLogList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult GetDiscountLogList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(discountLogEventService.GetList().ToDataSourceResult(request));
            //return new JsonResultExt(discountLogEventService.GetList().ToDataSourceResult(request));
        }

        #endregion

        #region ServiceLogList

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult ServiceLogList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult GetServiceLogList([DataSourceRequest]DataSourceRequest request, int scope, bool error_only)
        {
            return Json(esnLogEventService.GetList_byScope(scope, error_only).ToDataSourceResult(request));
            //return Json(GetService<EsnLogEventService>().GetList_byParent(scope).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult ServiceLogView(int scope, long? obj_id)
        {
            if (!obj_id.HasValue)
            {
                ModelState.AddModelError("", "Не задан код объекта");
                return RedirectToErrorPage(ModelState);
            }

            Enums.LogScope logScope = Enums.LogScope.CABINET;
            try
            {
                logScope = (Enums.LogScope)scope;
            }
            catch
            {
                ModelState.AddModelError("", "Неизвестный тип журнала событий " + scope.ToString());
                return RedirectToErrorPage(ModelState);
            }

            switch (logScope)
            {
                case Enums.LogScope.EXCHANGE_DOC:
                    return RedirectToAction("ExchangeDocDataView", "Exchange", new { obj_id = obj_id });
                case Enums.LogScope.EXCHANGE_PARTNER:
                    return RedirectToAction("ExchangeDataView", "Exchange", new { obj_id = obj_id});
                case Enums.LogScope.CVA:
                    return RedirectToAction("CvaDataView", "Cva", new { obj_id = obj_id });
                default:
                    ModelState.AddModelError("", "Неизвестный тип журнала событий " + scope.ToString());
                    return RedirectToErrorPage(ModelState);
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_LOG)]
        public ActionResult GetLogDetailMess(long log_id)
        {
            return Content(esnLogEventService.GetLogDetailMess(log_id));
        }

        #endregion
    }
}
