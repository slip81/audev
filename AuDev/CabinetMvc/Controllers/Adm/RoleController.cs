﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class AdmController
    {
        #region Role

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult RoleList()
        {
            ViewBag.MainList = cabRoleService.GetList();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult RoleAdd([DataSourceRequest] DataSourceRequest request, CabRoleViewModel roleAdd)
        {
            if (roleAdd != null && ModelState.IsValid)
            {
                var res = cabRoleService.Insert(roleAdd, ModelState);
            }

            return Json(new[] { roleAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpGet]
        public ActionResult RoleEdit(long role_id)
        {
            var role = cabRoleService.GetItem(role_id);
            if (role == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найдена роль с кодом " + role_id.ToString());
                return RedirectToAction("Oops", "Home");
            }
            
            var res = cabRoleActionService.GetList_AllActions(role_id);
            ViewBag.MainList = res;
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult RoleEdit([DataSourceRequest]DataSourceRequest request, CabRoleViewModel roleEdit)
        {
            if ((ModelState.IsValid) && (roleEdit != null))
            {
                cabRoleService.Update(roleEdit, ModelState);
            }

            return Json(new[] { roleEdit }.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public ActionResult RoleActionEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CabRoleActionViewModel> items)        
        {
            if (ModelState.IsValid)
            {
                long role_id = 0;
                List<long> action_id_list = null;                

                if (items != null)
                {
                    action_id_list = items.Where(ss => ss.have_action).Select(ss => (long)ss.action_id).ToList();
                    role_id = items.Select(ss => ss.role_id).FirstOrDefault();
                    cabRoleActionService.UpdateActions(role_id, action_id_list, ModelState);
                }
            }

            return Json(items.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult RoleDel([DataSourceRequest]DataSourceRequest request, CabRoleViewModel roleDel)
        {
            if (ModelState.IsValid)
            {
                cabRoleService.Delete(roleDel, ModelState);
            }

            return Json(new[] { roleDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region User

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult UserList()
        {
            //ViewBag.MainList = cabUserRoleService.GetList();
            ViewBag.BusinessList = businessService.GetList();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult GetUserRoleList([DataSourceRequest]DataSourceRequest request, string business_id_str)
        {
            if (!String.IsNullOrEmpty(business_id_str))
                return Json(cabUserRoleService.GetList_byParent(Convert.ToInt64(business_id_str)).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            else
                return Json(cabUserRoleService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult UserEdit(string user_name)
        {            
            var user = cabUserRoleService.GetItem_byUserName(user_name);
            if (user == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найден пользрватель " + user_name);
                return RedirectToAction("Oops", "Home");
            }
            ViewData["role_id"] = ((CabUserRoleViewModel)user).role_id;
            ViewBag.ActionGroupList = cabActionGroupService.GetList();
            return View(user);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetUserRoleActionList([DataSourceRequest]DataSourceRequest request, long role_id, string user_name, bool use_defaults, int? group_id)
        {
            int group_id_not_null = group_id.GetValueOrDefault(0) > 0 ? (int)group_id : 0;
            return Json(cabUserRoleActionService.GetList_AllActions(role_id, user_name, use_defaults, group_id_not_null).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UserRoleActionEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CabUserRoleActionViewModel> items
            , long role_id, string user_name, bool use_defaults)
        {
            if (ModelState.IsValid)
            {                
                List<long> action_id_list = null;
                
                if (items != null)
                {
                    action_id_list = items.Where(ss => ss.have_action).Select(ss => (long)ss.action_id).ToList();
                    cabUserRoleActionService.UpdateActions(role_id, user_name, use_defaults, action_id_list, ModelState);
                }
            }

            return Json(items.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult UserDel([DataSourceRequest]DataSourceRequest request, CabUserRoleViewModel user)
        {
            if (ModelState.IsValid)
            {
                cabUserRoleService.Delete(user, ModelState);
            }

            return Json(new[] { user }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Admin

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        [HttpGet]
        public ActionResult AdminEdit(int user_id)
        {
            ViewBag.AuthRoleList = authRoleService.GetList_byGroup((int)Enums.AuthGroupEnum.PERS);
            ViewBag.CrmUserRoleList = crmUserRoleService.GetList();
            if (user_id <= 0)
            {
                return View(new CabUserViewModel() { is_active = true, is_crm_user = false, is_executer = false, });
            }
            else
            {
                var cabUser = cabUserService.GetItem(user_id);
                if (cabUser == null)
                {
                    ModelState.AddModelError("", "Не найден сотрудник с кодом " + user_id.ToString());
                    return RedirectToErrorPage(ModelState);
                }
                return View(cabUser);
            }
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ADM_USER)]
        [HttpPost]
        public ActionResult AdminEdit(CabUserViewModel item)
        {
            if (ModelState.IsValid)
            {
                AuBaseViewModel res = null;
                if (item.user_id > 0)
                    res = cabUserService.Update(item, ModelState);
                else
                    res = cabUserService.Insert(item, ModelState);                
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении сотрудника");
                    return RedirectToErrorPage(ModelState);                    
                }
                return RedirectToAction("AuthList", "Auth");
            }
            return RedirectToErrorPage(ModelState);
        }

        /*
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        public ActionResult AdminList()
        {
            ViewBag.MainList = cabUserService.GetList_Actual();
            var crmUserRoleList = crmUserRoleService.GetList();
            var authRoleList = authRoleService.GetList();
            ViewData["CrmUserRoleList"] = crmUserRoleList;
            ViewData["CrmUserRoleDefault"] = crmUserRoleList.FirstOrDefault();
            ViewData["AuthRoleList"] = authRoleList;
            ViewData["AuthRoleDefault"] = authRoleList.FirstOrDefault();
            return View();
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ADM_USER)]
        [HttpPost]
        public ActionResult AdminAdd([DataSourceRequest]DataSourceRequest request, CabUserViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = cabUserService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении сотрудника");
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ADM_USER)]
        [HttpPost]
        public ActionResult AdminEdit([DataSourceRequest]DataSourceRequest request, CabUserViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = cabUserService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при обновлении сотрудника");                    
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }
        */

        #endregion

    }
}
