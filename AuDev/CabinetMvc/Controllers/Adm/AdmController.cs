﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.User;
using CabinetMvc.Auth;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class AdmController : BaseController
    {
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult Index()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult ChangeBusiness(long? id)
        {            
            //long business_id = id.GetValueOrDefault(0) <= 0 ? 0 : (long)id;
            long client_id = id.GetValueOrDefault(0) <= 0 ? 0 : (long)id;
            try
            {
                UserViewModel user = ((IUserProvider)Auth.CurrentUser.Identity).User;
                //Auth.ChangeBusiness(user.UserName, business_id, user.RoleId.GetValueOrDefault(0), user.CabClientId, user.RememberMe);
                Auth.ChangeBusiness(user.UserName, 0, user.RoleId.GetValueOrDefault(0), client_id, user.RememberMe);
                return Redirect(Request.UrlReferrer.OriginalString);
            }
            catch (Exception ex)
            {
                TempData.Clear();
                TempData.Add("error", "Не удалось установить организацию с кодом " + id.ToString() + ": " + GlobalUtil.ExceptionInfo(ex));
                return RedirectToAction("Oops", "Home");
            }
        }
    }
}
