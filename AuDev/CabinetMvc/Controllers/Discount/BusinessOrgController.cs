﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        public ActionResult BusinessOrgList()
        {
            ViewBag.MainList = businessOrgService.GetList_byParent(CurrentUser.BusinessId);
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        public ActionResult GetBusinessOrgList([DataSourceRequest] DataSourceRequest request, string business_id_str)
        {
            long business_id = 0;
            long.TryParse(business_id_str, out business_id);
            return Json(businessOrgService.GetList_byParent(business_id).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessOrgAdd([DataSourceRequest] DataSourceRequest request, BusinessOrgViewModel businessOrgAdd, string business_id_str)
        {
            if (ModelState.IsValid)
            {
                long business_id = 0;
                if (!long.TryParse(business_id_str, out business_id))
                {
                    TempData.Clear();
                    TempData.Add("error", "Не найдена организация с кодом " + business_id_str.ToString());
                    return RedirectToAction("Oops", "Home");
                }
                businessOrgAdd.add_to_timezone = businessOrgAdd.add_to_timezone.GetValueOrDefault(0) - 2;
                businessOrgAdd.business_id = business_id;
                var res = businessOrgService.Insert(businessOrgAdd, ModelState);
                ViewBag.Message = res == null ? "" : "Отделение " + businessOrgAdd.org_name.Trim() + " добавлено !";
                return Json(new[] { businessOrgAdd }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { businessOrgAdd }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessOrgEdit([DataSourceRequest] DataSourceRequest request, BusinessOrgViewModel businessOrgEdit, string business_id_str, string org_id_str)
        {
            if (ModelState.IsValid)
            {
                long business_id = 0;
                if (!long.TryParse(business_id_str, out business_id))
                {
                    TempData.Clear();
                    TempData.Add("error", "Не найдена организация с кодом " + business_id_str.ToString());
                    return RedirectToAction("Oops", "Home");
                }
                long org_id = 0;
                if (!long.TryParse(org_id_str, out org_id))
                {
                    TempData.Clear();
                    TempData.Add("error", "Не найдено отделение с кодом " + org_id_str.ToString());
                    return RedirectToAction("Oops", "Home");
                }
                businessOrgEdit.add_to_timezone = businessOrgEdit.add_to_timezone.GetValueOrDefault(0) - 2;
                businessOrgEdit.business_id = business_id;
                businessOrgEdit.org_id = org_id;
                var res = businessOrgService.Update(businessOrgEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return Json(new[] { businessOrgEdit }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { businessOrgEdit }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessOrgDel([DataSourceRequest]DataSourceRequest request, BusinessOrgViewModel businessOrgDel, string org_id_str)
        {
            if (ModelState.IsValid)
            {
                long org_id = 0;
                if (!long.TryParse(org_id_str, out org_id))
                {
                    ModelState.AddModelError("serv_result", "Некорректные параметры");
                    return Json(new[] { businessOrgDel }.ToDataSourceResult(request, ModelState));
                }
                businessOrgDel.org_id = org_id;
                businessOrgService.Delete(businessOrgDel, ModelState);
            }

            return Json(new[] { businessOrgDel }.ToDataSourceResult(request, ModelState));
        }
    }
}
