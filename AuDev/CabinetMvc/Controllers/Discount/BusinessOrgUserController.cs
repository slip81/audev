﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        public ActionResult GetBusinessOrgUserList([DataSourceRequest] DataSourceRequest request, string org_id_str)
        {
            long org_id = 0;
            if (!long.TryParse(org_id_str, out org_id))
            {
                TempData.Clear();
                TempData.Add("error", "Не найдено отделение с кодом " + org_id_str);
                return RedirectToAction("Oops", "Home");
            }
            return Json(businessOrgUserService.GetList_byParent(org_id).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessOrgUserAdd([DataSourceRequest] DataSourceRequest request, BusinessOrgUserViewModel businessOrgUserAdd, string orgIdStr)
        {
            if (ModelState.IsValid)
            {
                long org_id = 0;
                if (!long.TryParse(orgIdStr, out org_id))
                {
                    TempData.Clear();
                    TempData.Add("error", "Не найден отделение с кодом " + orgIdStr);
                    return RedirectToAction("Oops", "Home");
                }
                businessOrgUserAdd.org_id = org_id;
                var res = businessOrgUserService.Insert(businessOrgUserAdd, ModelState);
                ViewBag.Message = res == null ? "" : "Пользователь " + businessOrgUserAdd.user_name.Trim() + " добавлен !";
                return Json(new[] { businessOrgUserAdd }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { businessOrgUserAdd }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessOrgUserEdit([DataSourceRequest] DataSourceRequest request, BusinessOrgUserViewModel businessOrgUserEdit, string org_user_id_str, string org_id_str)
        {
            if (ModelState.IsValid)
            {
                long org_user_id = 0;
                if (!long.TryParse(org_user_id_str, out org_user_id))
                {
                    TempData.Clear();
                    TempData.Add("error", "Не найден пользователь с кодом " + org_user_id_str);
                    return RedirectToAction("Oops", "Home");
                }
                long org_id = 0;
                if (!long.TryParse(org_id_str, out org_id))
                {
                    TempData.Clear();
                    TempData.Add("error", "Не найдено отделение с кодом " + org_id_str);
                    return RedirectToAction("Oops", "Home");
                }
                businessOrgUserEdit.org_user_id = org_user_id;
                businessOrgUserEdit.org_id = org_id;
                var res = businessOrgUserService.Update(businessOrgUserEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return Json(new[] { businessOrgUserEdit }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { businessOrgUserEdit }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessOrgUserDel([DataSourceRequest]DataSourceRequest request, BusinessOrgUserViewModel businessOrgUserDel, string org_user_id_str, string org_id_str)
        {
            if (ModelState.IsValid)
            {
                long org_user_id = 0;
                if (!long.TryParse(org_user_id_str, out org_user_id))
                {
                    ModelState.AddModelError("serv_result", "Некорректные параметры");
                    return Json(new[] { businessOrgUserDel }.ToDataSourceResult(request, ModelState));
                }
                long org_id = 0;
                if (!long.TryParse(org_id_str, out org_id))
                {
                    ModelState.AddModelError("serv_result", "Некорректные параметры");
                    return Json(new[] { businessOrgUserDel }.ToDataSourceResult(request, ModelState));
                }
                businessOrgUserDel.org_user_id = org_user_id;
                businessOrgUserDel.org_id = org_id;
                businessOrgUserService.Delete(businessOrgUserDel, ModelState);
            }

            return Json(new[] { businessOrgUserDel }.ToDataSourceResult(request, ModelState));
        }
    }
}
