﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        public ActionResult CardClientList()
        {
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        public JsonResult GetCardClientList([DataSourceRequest]DataSourceRequest request)
        {
            //return Json(GetService<CardClientService>().GetList_byParent(CurrentUser.BusinessId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            return Json(cardClientService.GetList_byParent(CurrentUser.BusinessId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);            
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        public ActionResult GetSprav_Client([DataSourceRequest]DataSourceRequest request)
        {
            return Json(spravCardClientService.GetList_byParent(CurrentUser.BusinessId).ToDataSourceResult(request));            
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        public ActionResult GetSprav_Client_byText(string text)
        {
            return Json(spravCardClientService.GetList_byText(CurrentUser.BusinessId, text), JsonRequestBehavior.AllowGet);
            //return Json(spravCardClientService.GetList_byText(CurrentUser.BusinessId, text));
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        public ActionResult CardClient_ValueMapper(long[] values)
        {
            var indices = new List<long>();

            if (values != null && values.Any())
            {
                var index = 0;

                foreach (var client in spravCardClientService.GetList_byParent(CurrentUser.BusinessId).Cast<SpravCardClientViewModel>())
                {
                    if (values.Contains(long.Parse(client.client_id_str)))
                    {
                        indices.Add(index);
                    }

                    index += 1;
                }
            }

            return Json(indices, JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        public ActionResult CardClientAdd()
        {
            return View(new CardClientViewModel());
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        [HttpPost]
        public ActionResult CardClientAdd(CardClientViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = cardClientService.Insert(item, ModelState);
                if (res != null)
                {
                    return RedirectToAction("CardClientEdit", "Card", new { client_id = ((CardClientViewModel)res).client_id.ToString(), });
                }
                return View(item);
            }
            return View(item);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        public ActionResult CardClientEdit(long client_id)
        {
            var client = cardClientService.GetItem(client_id);
            //var client = AuBaseService2.GetItem(client_id);            
            if (client == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найден владелец карты с кодом " + client_id.ToString());
                return RedirectToAction("Oops", "Home");
            }
            return View(client);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        [HttpPost]
        public ActionResult CardClientEdit(CardClientViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = cardClientService.Update(item, ModelState);
                //var res = AuBaseService2.Update(item, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return View(res == null ? item : res);
            }
            return View(item);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_HOLDER)]
        [HttpPost]
        public ActionResult CardClientDel([DataSourceRequest]DataSourceRequest request, CardClientViewModel item, string client_id_str)
        {
            if (ModelState.IsValid)            
            {
                long client_id = 0;
                if (!long.TryParse(client_id_str, out client_id))
                {
                    ModelState.AddModelError("serv_result", "Некорректные параметры");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }
                //var client = GetService<CardClientService>().GetItem(client_id);
                item.client_id = client_id;
                cardClientService.Delete(item, ModelState);
                //AuBaseService2.Delete(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

    }
}
