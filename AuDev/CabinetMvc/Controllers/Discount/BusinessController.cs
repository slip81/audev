﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
        #region Business

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        public ActionResult GetBusinessList_Full()
        {
            return Json(businessService.GetList(), JsonRequestBehavior.AllowGet);            
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        public ActionResult GetBusinessList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(businessService.GetList().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        public ActionResult GetBusinessList_Sprav()
        {
            return Json(businessService.GetList_Sprav(), JsonRequestBehavior.AllowGet);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpGet]
        public ActionResult BusinessList()
        {
            ViewBag.MainList = businessService.GetList();
            return View();
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpGet]
        public ActionResult BusinessAdd()
        {
            return View(new BusinessViewModel());
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessAdd(BusinessViewModel businessAdd)
        {
            if (ModelState.IsValid)
            {
                businessAdd.add_to_timezone = businessAdd.add_to_timezone.GetValueOrDefault(0) - 2;
                var res = businessService.Insert(businessAdd, ModelState);
                if (res != null)
                {
                    return RedirectToAction("BusinessEdit", "Card", new { business_id = ((BusinessViewModel)res).business_id.ToString(), });
                }
                ViewBag.Message = res == null ? "" : "Организация " + businessAdd.business_name.Trim() + " добавлена !";
                return View(res == null ? businessAdd : res);
            }
            return View(businessAdd);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpGet]
        public ActionResult BusinessEdit(long business_id)
        {
            var business = businessService.GetItem(business_id);
            if (business == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найдена организация с кодом " + business_id.ToString());
                return RedirectToAction("Oops", "Home");
            }
            ((BusinessViewModel)business).add_to_timezone = ((BusinessViewModel)business).add_to_timezone.GetValueOrDefault(0) + 2;

            var roles = businessService.GetList().AsEnumerable();
            ViewData["roles"] = roles;
            return View(business);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessEdit(BusinessViewModel businessEdit)
        {
            if (ModelState.IsValid)
            {
                businessEdit.add_to_timezone = businessEdit.add_to_timezone.GetValueOrDefault(0) - 2;
                var res = businessService.Update(businessEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                var roles = cabRoleService.GetList().AsEnumerable();
                ViewData["roles"] = roles;
                return View(res == null ? businessEdit : res);
            }
            return View(businessEdit);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public ActionResult BusinessDel([DataSourceRequest]DataSourceRequest request, BusinessViewModel businessDel, string business_id_str)
        {
            if (ModelState.IsValid)
            {
                long business_id = 0;
                if (!long.TryParse(business_id_str, out business_id))
                {
                    ModelState.AddModelError("serv_result", "Некорректные параметры");
                    return Json(new[] { businessDel }.ToDataSourceResult(request, ModelState));
                }
                businessDel.business_id = business_id;
                businessService.Delete(businessDel, ModelState);
            }

            return Json(new[] { businessDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region BusinessSummary

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        [HttpPost]
        public JsonResult GetBusinessSummary()
        {
            return Json(businessSummaryService.GetItem(CurrentUser.BusinessId));
        }
       
        [HttpPost]
        public JsonResult GetBusinessSummaryGraph()
        {
            IEnumerable<BusinessSummaryGraphViewModel> graph1 = businessSummaryService.GetGraph1(CurrentUser.BusinessId);
            return Json(graph1);
        }

        [HttpPost]
        public JsonResult GetBusinessSummaryGraph2()
        {
            List<BusinessSummaryGraph2ViewModel> graph2 = businessSummaryService.GetGraph2(CurrentUser.BusinessId);
            return Json(graph2);
        }

        #endregion

        #region CardBrief

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BUSINESS)]
        public ActionResult CardBrief()
        {
            return View();
        }

        #endregion

    }
}
