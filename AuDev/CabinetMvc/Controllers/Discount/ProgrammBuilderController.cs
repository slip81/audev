﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
                
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult ProgrammBuilder()
        {
            ViewData["cardTypeGroupList"] = cardTypeGroupService.GetList();
            ViewData["programmParamTypeList"] = programmParamTypeService.GetList();
            ViewData["programmStepParamTypeList"] = programmStepParamTypeService.GetList();
            ViewData["programmCardParamTypeList"] = programmCardParamTypeService.GetList();
            ViewData["programmTransParamTypeList"] = programmTransParamTypeService.GetList();
            ViewData["programmStepConditionTypeList"] = programmStepConditionTypeService.GetList();
            ViewData["programmStepLogicTypeList"] = programmStepLogicTypeService.GetList();
            return View();
        }

        #region ProgrammHeader

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammHeaderAdd(ProgrammViewModel itemAdd)
        {
            if (ModelState.IsValid)
            {
                var res = programmBuilderService.InsertProgramm(itemAdd, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении алгоритма");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammHeaderEdit(ProgrammViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                var res = programmBuilderService.UpdateProgramm(itemEdit, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении алгоритма");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }            
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        #endregion

        #region ProgrammParam

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult GetProgrammParamList_forStepParam(string programm_id_str)
        {
            long programm_id = 0;
            long.TryParse(programm_id_str, out programm_id);
            return Json(programmParamService.GetList_byParent(programm_id), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammParamAdd([DataSourceRequest] DataSourceRequest request, ProgrammParamViewModel itemAdd, string programm_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(programm_id_str, out programm_id);
                itemAdd.programm_id = programm_id;
                var res = programmBuilderService.InsertProgrammParam(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении параметра алгоритма");
                    return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammParamEdit([DataSourceRequest] DataSourceRequest request, ProgrammParamViewModel itemEdit, string programm_id_str, string param_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(programm_id_str, out programm_id);
                itemEdit.programm_id = programm_id;
                long param_id = 0;
                long.TryParse(param_id_str, out param_id);                
                itemEdit.param_id = param_id;
                var res = programmBuilderService.UpdateProgrammParam(itemEdit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении параметра алгоритма");
                    return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammParamDel([DataSourceRequest] DataSourceRequest request, ProgrammParamViewModel itemDel, string programm_id_str, string param_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(programm_id_str, out programm_id);
                itemDel.programm_id = programm_id;
                long param_id = 0;
                long.TryParse(param_id_str, out param_id);
                itemDel.param_id = param_id;
                var res = programmBuilderService.DeleteProgrammParam(itemDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении параметра алгоритма");
                    return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ProgrammStep

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult GetProgrammStepList([DataSourceRequest] DataSourceRequest request, string programm_id_str)
        {
            long programm_id = 0;
            if (!long.TryParse(programm_id_str, out programm_id))
            {
                programm_id = 0;
            }
            return Json(programmBuilderService.GetProgrammStepList(programm_id).ToDataSourceResult(request));            
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepAdd([DataSourceRequest] DataSourceRequest request, ProgrammStepViewModel itemAdd, string programm_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(programm_id_str, out programm_id);
                itemAdd.programm_id = programm_id;
                var res = programmBuilderService.InsertProgrammStep(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении шага алгоритма");
                    return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepEdit([DataSourceRequest] DataSourceRequest request, ProgrammStepViewModel itemEdit, string programm_id_str, string step_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(programm_id_str, out programm_id);
                itemEdit.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(step_id_str, out step_id);
                itemEdit.step_id = step_id;
                var res = programmBuilderService.UpdateProgrammStep(itemEdit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении шага алгоритма");
                    return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepDel([DataSourceRequest] DataSourceRequest request, ProgrammStepViewModel itemDel, string programm_id_str, string step_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(programm_id_str, out programm_id);
                itemDel.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(step_id_str, out step_id);
                itemDel.step_id = step_id;
                var res = programmBuilderService.DeleteProgrammStep(itemDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении шага алгоритма");
                    return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ProgrammStepParam

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult GetProgrammStepParamList([DataSourceRequest] DataSourceRequest request, string parent_programm_id_str, string parent_step_id_str)
        {
            long step_id = 0;
            if (!long.TryParse(parent_step_id_str, out step_id))
            {
                step_id = 0;
            }
            return Json(programmBuilderService.GetProgrammStepParamList(step_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult GetProgrammStepParamList_forStepParam(string parent_programm_id_str, string parent_step_id_str)
        {
            long programm_id = 0;
            long.TryParse(parent_programm_id_str, out programm_id);
            long step_id = 0;
            long.TryParse(parent_step_id_str, out step_id);
            return Json(programmStepParamService.GetList_forStepParam(programm_id, step_id), JsonRequestBehavior.AllowGet);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult GetProgrammStepParamList_forStep(string parent_programm_id_str, string parent_step_id_str)
        {
            long programm_id = 0;
            long.TryParse(parent_programm_id_str, out programm_id);
            long step_id = 0;
            long.TryParse(parent_step_id_str, out step_id);
            return Json(programmStepParamService.GetList_forStep(programm_id, step_id), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepParamAdd([DataSourceRequest] DataSourceRequest request, ProgrammStepParamViewModel itemAdd, string parent_programm_id_str, string parent_step_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemAdd.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemAdd.step_id = step_id;
                var res = programmBuilderService.InsertProgrammStepParam(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении параметра шага алгоритма");
                    return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepParamEdit([DataSourceRequest] DataSourceRequest request, ProgrammStepParamViewModel itemEdit, string parent_programm_id_str, string parent_step_id_str, string param_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemEdit.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemEdit.step_id = step_id;
                long param_id = 0;
                long.TryParse(param_id_str, out param_id);
                itemEdit.param_id = param_id;
                var res = programmBuilderService.UpdateProgrammStepParam(itemEdit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении параметра шага алгоритма");
                    return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepParamDel([DataSourceRequest] DataSourceRequest request, ProgrammStepParamViewModel itemDel, string parent_programm_id_str, string parent_step_id_str, string param_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemDel.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemDel.step_id = step_id;
                long param_id = 0;
                long.TryParse(param_id_str, out param_id);
                itemDel.param_id = param_id;
                var res = programmBuilderService.DeleteProgrammStepParam(itemDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении параметра шага алгоритма");
                    return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion
        
        #region ProgrammStepCondition

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult GetProgrammStepConditionList([DataSourceRequest] DataSourceRequest request, string parent_programm_id_str, string parent_step_id_str)
        {
            long step_id = 0;
            if (!long.TryParse(parent_step_id_str, out step_id))
            {
                step_id = 0;
            }
            return Json(programmBuilderService.GetProgrammStepConditionList(step_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult GetProgrammStepConditionList_forStep(string parent_programm_id_str, string parent_step_id_str)
        {
            long programm_id = 0;
            long.TryParse(parent_programm_id_str, out programm_id);
            long step_id = 0;
            long.TryParse(parent_step_id_str, out step_id);
            return Json(programmStepConditionService.GetList_forStep(step_id), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepConditionAdd([DataSourceRequest] DataSourceRequest request, ProgrammStepConditionViewModel itemAdd, string parent_programm_id_str, string parent_step_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemAdd.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemAdd.step_id = step_id;
                var res = programmBuilderService.InsertProgrammStepCondition(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении условия шага алгоритма");
                    return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepConditionEdit([DataSourceRequest] DataSourceRequest request, ProgrammStepConditionViewModel itemEdit, string parent_programm_id_str, string parent_step_id_str, string condition_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemEdit.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemEdit.step_id = step_id;
                long condition_id = 0;
                long.TryParse(condition_id_str, out condition_id);
                itemEdit.condition_id = condition_id;
                var res = programmBuilderService.UpdateProgrammStepCondition(itemEdit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении условия шага алгоритма");
                    return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepConditionDel([DataSourceRequest] DataSourceRequest request, ProgrammStepConditionViewModel itemDel, string parent_programm_id_str, string parent_step_id_str, string condition_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemDel.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemDel.step_id = step_id;
                long condition_id = 0;
                long.TryParse(condition_id_str, out condition_id);
                itemDel.condition_id = condition_id;
                var res = programmBuilderService.DeleteProgrammStepCondition(itemDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении условия шага алгоритма");
                    return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ProgrammStepLogic

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        public ActionResult GetProgrammStepLogicList([DataSourceRequest] DataSourceRequest request, string parent_programm_id_str, string parent_step_id_str)
        {
            long step_id = 0;
            if (!long.TryParse(parent_step_id_str, out step_id))
            {
                step_id = 0;
            }
            return Json(programmBuilderService.GetProgrammStepLogicList(step_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepLogicAdd([DataSourceRequest] DataSourceRequest request, ProgrammStepLogicViewModel itemAdd, string parent_programm_id_str, string parent_step_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemAdd.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemAdd.step_id = step_id;
                var res = programmBuilderService.InsertProgrammStepLogic(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении логики шага алгоритма");
                    return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepLogicEdit([DataSourceRequest] DataSourceRequest request, ProgrammStepLogicViewModel itemEdit, string parent_programm_id_str, string parent_step_id_str, string logic_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemEdit.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemEdit.step_id = step_id;
                long logic_id = 0;
                long.TryParse(logic_id_str, out logic_id);
                itemEdit.logic_id = logic_id;
                var res = programmBuilderService.UpdateProgrammStepLogic(itemEdit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении логики шага алгоритма");
                    return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PROGRAMM_BUILDER)]
        [HttpPost]
        public JsonResult ProgrammStepLogicDel([DataSourceRequest] DataSourceRequest request, ProgrammStepLogicViewModel itemDel, string parent_programm_id_str, string parent_step_id_str, string logic_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                long.TryParse(parent_programm_id_str, out programm_id);
                itemDel.programm_id = programm_id;
                long step_id = 0;
                long.TryParse(parent_step_id_str, out step_id);
                itemDel.step_id = step_id;
                long logic_id = 0;
                long.TryParse(logic_id_str, out logic_id);
                itemDel.logic_id = logic_id;
                var res = programmBuilderService.DeleteProgrammStepLogic(itemDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении условия шага алгоритма");
                    return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion
    }
}
