﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult CampaignList()
        {                                    
            return View();            
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetCampaignList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(campaignService.GetList_byParent(CurrentUser.BusinessId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpGet]
        [ValidateInput(false)]
        public ActionResult CampaignEdit(int campaign_id)
        {
            DateTime today = DateTime.Today;
            if (campaign_id > 0)
            {
                ViewBag.OrgList = campaignBusinessOrgService.GetList_byParent(campaign_id);
                var campaign = campaignService.GetItem(campaign_id);
                return View(campaign);
            }
            else
            {
                ViewBag.OrgList = campaignBusinessOrgService.GetList_New(CurrentUser.BusinessId);
                return View(new CampaignViewModel() 
                {
                    business_id = CurrentUser.BusinessId, 
                    date_beg = today, 
                    date_end = today.AddMonths(1),
                    issue_date_beg = today,
                    issue_date_end = today.AddMonths(1),
                    issue_rule = (int)Enums.CampaignIssueRuleEnum.BEFORE,
                    issue_condition_type = (int)Enums.CampaignIssueConditionTypeEnum.NONE,
                    issue_condition_sale_sum = 0,
                    issue_condition_pos_count = 1,
                    card_money_value = 1,
                    apply_condition_sale_sum = 0,
                    max_card_cnt = 0,
                });
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult CampaignEdit(CampaignViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel res = null;
                if (itemEdit.campaign_id > 0)
                {
                    res = campaignService.Update(itemEdit, ModelState);
                }
                else
                {
                    res = campaignService.Insert(itemEdit, ModelState);
                }                
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении акции");
                    return View(itemEdit);
                }
                
                //ViewBag.Message ="Изменения сохранены !";                
                //return View(res);
                return RedirectToAction("CampaignList", "Card");
            }
            return View(itemEdit);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult CampaignDel([DataSourceRequest]DataSourceRequest request, CampaignViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                campaignService.Delete(itemDel, ModelState);
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult CampaignCheckDates(int campaign_id, DateTime? date_beg, DateTime? date_end)
        {
            var res = campaignService.CheckDates(campaign_id, date_beg, date_end);
            return Json(new { err = !res, mess = (!res ? "Уже есть акция на такие даты" : "") });
        }

    }
}
