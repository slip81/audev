﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        public ActionResult CardTypeList()
        {
            ViewBag.MainList = cardTypeService.GetList_byParent(CurrentUser.BusinessId);
            return View();            
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        public ActionResult GetSprav_CardType([DataSourceRequest]DataSourceRequest request)
        {
            return Json(spravCardTypeService.GetList_byParent(CurrentUser.BusinessId), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        public ActionResult GetSprav_CardType_byBusiness([DataSourceRequest]DataSourceRequest request, string business_id_str)
        {
            long business_id_long = 0;
            var res = long.TryParse(business_id_str, out business_id_long);
            if (!res)
                business_id_long = 0;
            return Json(spravCardTypeService.GetList_byParent(business_id_long), JsonRequestBehavior.AllowGet);
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        public ActionResult GetCardTypeList_forCampaign([DataSourceRequest]DataSourceRequest request, int campaign_id)
        {
            return Json(spravCardTypeService.GetCardTypeList_forCampaign(campaign_id), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpGet]
        public ActionResult CardTypeAdd()
        {            
            return View(new CardTypeViewModel());
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpPost]
        public ActionResult CardTypeAdd(CardTypeViewModel card_type_add)
        {
            if (ModelState.IsValid)
            {
                var res = cardTypeService.Insert(card_type_add, ModelState);
                if (res != null)
                {
                    var card_type_id_new = ((CardTypeViewModel)res).card_type_id;
                    return RedirectToAction("CardTypeEdit", "Card", new { card_type_id = card_type_id_new, });
                }
                else
                {
                    ViewBag.Message = res == null ? "" : "Тип карты добавлен !";
                    return View(card_type_add);
                }
            }
            return View(card_type_add);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        public ActionResult CardTypeEdit(long card_type_id)
        {
            var card_type = cardTypeService.GetItem(card_type_id);
            if (card_type == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найден тип карты с кодом " + card_type_id.ToString());
                return RedirectToAction("Oops", "Home");
            }
            //ViewBag.RestrList = cardTypeRestrService.GetList_byParent(card_type_id);            
            ViewBag.BusinessOrgList = cardTypeBusinessOrgService.GetList_byParent(card_type_id);
            return View(card_type);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpPost]        
        public ActionResult CardTypeEdit(CardTypeViewModel card_type_edit)
        {            
            if (ModelState.IsValid)
            {
                var res = cardTypeService.Update(card_type_edit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                //ViewBag.RestrList = cardTypeRestrService.GetList_byParent(card_type_edit.card_type_id);
                ViewBag.BusinessOrgList = cardTypeBusinessOrgService.GetList_byParent(card_type_edit.card_type_id);
                return View(res == null ? card_type_edit : res);
            }
            return View(card_type_edit);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpPost]
        public ActionResult CardTypeDel([DataSourceRequest]DataSourceRequest request, CardTypeViewModel card_type)
        {
            if (ModelState.IsValid)
            {
                //GetService<CardTypeService>().Delete(card_type.card_type_id, ModelState);
                cardTypeService.Delete(card_type, ModelState);
            }

            return Json(new[] { card_type }.ToDataSourceResult(request, ModelState));
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        public ActionResult GetCardTypeGroupList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(cardTypeGroupService.GetList(), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        public ActionResult GetCardTypeLimitList([DataSourceRequest] DataSourceRequest request, long parent_id)
        {
            return Json(cardTypeLimitService.GetList_byParent(parent_id).ToDataSourceResult(request));
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpPost]
        public ActionResult CardTypeLimitAdd([DataSourceRequest] DataSourceRequest request, CardTypeLimitViewModel cardTypeLimitAdd, long parent_id)
        {
            if (ModelState.IsValid)
            {
                //cardTypeLimitAdd.card_type_id = long.Parse(card_type_id);
                cardTypeLimitAdd.card_type_id = parent_id;
                var res = cardTypeLimitService.Insert(cardTypeLimitAdd, ModelState);
                ViewBag.Message = res == null ? "" : "Лимит добавлен !";
                return Json(new[] { cardTypeLimitAdd }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { cardTypeLimitAdd }.ToDataSourceResult(request, ModelState));
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpPost]
        public ActionResult CardTypeLimitEdit([DataSourceRequest] DataSourceRequest request, CardTypeLimitViewModel cardTypeLimitEdit, long parent_id)
        {
            if (ModelState.IsValid)
            {
                var res = cardTypeLimitService.Update(cardTypeLimitEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return Json(new[] { cardTypeLimitEdit }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { cardTypeLimitEdit }.ToDataSourceResult(request, ModelState));
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpPost]
        public ActionResult CardTypeLimitDel([DataSourceRequest]DataSourceRequest request, CardTypeLimitViewModel cardTypeLimitDel, long parent_id)
        {
            if (ModelState.IsValid)
            {
                cardTypeLimitService.Delete(cardTypeLimitDel, ModelState);
            }
            return Json(new[] { cardTypeLimitDel }.ToDataSourceResult(request, ModelState));
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpPost]
        public ActionResult CardTypeLimit_SpreadOnCurrYear(long id)
        {
            // !!!
            // todo
            return null;
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_TYPE)]
        [HttpPost]
        public ActionResult CardTypeBusinessOrgEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CardTypeBusinessOrgViewModel> items)
        {
            if (items != null && ModelState.IsValid)
            {
                foreach (var item in items)
                {
                    cardTypeBusinessOrgService.Update(item, ModelState);
                }
            }

            return Json(items.ToDataSourceResult(request, ModelState));
        }

    }
}
