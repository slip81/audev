﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {

        #region Programm

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        public ActionResult ProgrammList()
        {            
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        public ActionResult GetSprav_Programm([DataSourceRequest]DataSourceRequest request)
        {
            return Json(spravProgrammService.GetList_byParent(CurrentUser.BusinessId), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        public JsonResult GetProgrammList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(programmService.GetList_byParent(CurrentUser.BusinessId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        public JsonResult GetProgrammList_byBusiness(string business_id_str)
        {
            long business_id = 0;
            long.TryParse(business_id_str, out business_id);
            return Json(programmService.GetList_byParent(business_id), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        public JsonResult GetProgrammList_Active([DataSourceRequest]DataSourceRequest request, string business_id_str)
        {
            long business_id = 0;
            long.TryParse(business_id_str, out business_id);
            return Json(programmService.GetList_Active(business_id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        [HttpGet]
        public ActionResult ProgrammEdit(long programm_id)
        {
            var programm = programmService.GetItem(programm_id);
            if (programm == null)
            {                
                TempData.Clear();
                TempData.Add("error", "Не найден алгоритм с кодом " + programm_id.ToString());
                return RedirectToAction("Oops", "Home");
            }            
            return View(programm);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        [HttpPost]
        public ActionResult ProgrammEdit(ProgrammViewModel programmEdit)
        {
            if (ModelState.IsValid)
            {
                var res = programmService.Update(programmEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return View(res == null ? programmEdit : res);
            }
            return View(programmEdit);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        [HttpGet]
        public ActionResult ProgrammAdd()
        {
            ViewBag.BusinessList = businessService.GetList_Sprav();
            return View(new ProgrammOrderViewModel() { 
                date_beg = DateTime.Today,
                add_to_card_trans_sum_with_disc = true,
                add_to_card_trans_sum = false,
            });
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        [HttpPost]
        public JsonResult ProgrammAdd(ProgrammOrderViewModel programmAdd)
        {
            if (ModelState.IsValid)
            {
                var res = programmOrderService.Insert(programmAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                { 
                    if (ModelState.IsValid)
                    {
                        ModelState.AddModelError("", "Ошибка при добавлении заявки на созданние алгоритма");
                    }                                        
                    return Json(new { error = ModelState.GetErrorsString(), is_done = false,});
                }                
                return Json(new { error = "", is_done = ((ProgrammOrderViewModel)res).programm_id.GetValueOrDefault(0) > 0, });
            }
            return Json(new { error = ModelState.GetErrorsString(), is_done = false, });
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        [HttpPost]
        public ActionResult ProgrammDel([DataSourceRequest]DataSourceRequest request, ProgrammViewModel item, string programm_id_str)
        {
            if (ModelState.IsValid)
            {
                long programm_id = 0;
                if (!long.TryParse(programm_id_str, out programm_id))
                {
                    ModelState.AddModelError("serv_result", "Некорректные параметры");
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                }                
                item.programm_id = programm_id;
                programmService.Delete(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ProgrammParam

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        public ActionResult GetProgrammParamList([DataSourceRequest] DataSourceRequest request, string programm_id_str)
        {
            long programm_id = 0;
            if (!long.TryParse(programm_id_str, out programm_id))
            {
                TempData.Clear();
                TempData.Add("error", "Не найден алгоритм с кодом " + programm_id_str.ToString());
                return RedirectToAction("Oops", "Home");
            }
            return Json(programmParamService.GetList_byParent(programm_id).ToDataSourceResult(request));
            //return Json(GetService<BusinessOrgService>().GetList(), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        [HttpPost]
        public ActionResult ProgramParamEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ProgrammParamViewModel> items, long programm_id)
        {
            if (items != null && ModelState.IsValid)
            {
                var serv = programmParamService;
                foreach (var item in items)
                {
                    item.programm_id = programm_id;
                    serv.Update(item, ModelState);
                    //productService.Update(product);
                }
            }

            return Json(items.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Other

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        public ActionResult GetSprav_ProgrammTemplate([DataSourceRequest]DataSourceRequest request)
        {
            return Json(spravProgrammTemplateService.GetList(), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        [HttpPost]
        public ActionResult ProgrammCopy(long programm_id, string business_id_str)
        {
            //return RedirectToAction("ProgrammList", "Card");
            var serv = programmService;
            var programmAdd = serv.GetItem(programm_id);
            long business_id = CurrentUser.BusinessId;
            bool parseOk = long.TryParse(business_id_str, out business_id);
            if (!parseOk)
                business_id = CurrentUser.BusinessId;
            var res = serv.CopyProgramm(programmAdd, business_id, ModelState);
            return Json(new[] { programm_id }, JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_PROGRAMM)]
        public JsonResult GetProgrammOrderList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(programmOrderService.GetList().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        
        [AllowAnonymous]
        [HttpGet]
        public FileResult ProgrammAddDoc(string name)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\Release\Cabinet\File\Download\" + name);
            string fileName = name;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        #endregion
        
    }
}
