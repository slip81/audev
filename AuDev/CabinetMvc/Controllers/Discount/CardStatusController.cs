﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD)]
        public ActionResult GetSprav_CardStatus([DataSourceRequest]DataSourceRequest request)
        {
            return Json(spravCardStatusService.GetList(), JsonRequestBehavior.AllowGet);
        }
    }
}
