﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public partial class CardController
    {
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TRANS)]
        public ActionResult CardTransactionList()
        {
            CardTransSearch cardTransSearch = new CardTransSearch() { DateBeg = DateTime.Today, DateEnd = DateTime.Today, };
            return View(cardTransSearch);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TRANS)]
        public ActionResult GetCardTransactionsList([DataSourceRequest]DataSourceRequest request)
        {
            long parent_id = CurrentUser.BusinessId;
            return Json(cardTransactionService.GetList_byParent(parent_id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TRANS)]
        public ActionResult GetLastCardTransactionsList([DataSourceRequest]DataSourceRequest request)
        {
            long parent_id = CurrentUser.BusinessId;
            return Json(cardTransactionService.GetLastCardTransactionList_byParent(parent_id, 10).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TRANS)]
        public ActionResult CardTransactionView(long card_trans_id)
        {
            var card_trans = cardTransactionService.GetItem(card_trans_id);
            if (card_trans == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найдена транзакция клиент с кодом " + card_trans_id.ToString());
                return RedirectToAction("Oops", "Home");
            }
            return View(card_trans);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TRANS)]
        public ActionResult GetCardTransactionDetailList([DataSourceRequest]DataSourceRequest request, long card_trans_id)
        {
            return Json(cardTransactionDetailService.GetList_byParent(card_trans_id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_TRANS)]
        public ActionResult GetCardTransactionsList_byCard([DataSourceRequest]DataSourceRequest request, string card_id)
        {
            long card_id_long = 0;
            if (!long.TryParse(card_id, out card_id_long))
                card_id_long = 0;
            return Json(cardTransactionService.GetCardTransactionList_byCard(card_id_long).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);            
        }
    }
}
