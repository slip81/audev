﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using System.Data;
using System.Data.EntityClient;
using System.Data.OleDb;
using System.Configuration;
using System.Xml;
using System.IO;
using Npgsql;
using NpgsqlTypes;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.Util;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public partial class CardController : BaseController
    {                

        [ActionAuthorize("Card_Read")]
        public ActionResult Index()
        {
            return View();
        }

        #region Card

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD)]
        public ActionResult CardList()
        {            
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD)]
        public ActionResult GetCardList([DataSourceRequest]DataSourceRequest request, string card_search_string, string card_adv_search_string)
        {
            if ((!String.IsNullOrEmpty(card_search_string)) || (!String.IsNullOrEmpty(card_adv_search_string)))
            {                                                
                return Json(cardService.GetList_bySearch(card_search_string, card_adv_search_string).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            else
            {                
                return Json(cardService.GetList_byParent(CurrentUser.BusinessId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD)]
        [HttpGet]
        public ActionResult GetCardNumList([DataSourceRequest]DataSourceRequest request, string text)
        {            
            var res = cardService.GetList_Sprav(text).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD)]
        public ActionResult CardAdvancedSearch()
        {            
            CardSearch cardSearch = new CardSearch();
            return View(cardSearch);            
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD)]
        [HttpPost]
        public ActionResult CardAdvancedSearch(CardSearch cardSearch)
        {            
            if (ModelState.IsValid)
            {                
                if (!GlobalUtil.HasAllEmptyProperties(cardSearch))
                {
                    string cardSearchString = JsonConvert.SerializeObject(cardSearch);
                    return RedirectToAction("CardList", "Card", new { advanced_search = cardSearchString });
                }
                else
                {
                    ModelState.AddModelError("", "Не заданы параметры поиска");
                    return View(cardSearch);
                }
            }
            return View(cardSearch);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD)]
        public ActionResult CardAdd()
        {
            ViewBag.CardTypeList = spravCardTypeService.GetList_byParent(CurrentUser.BusinessId);
            ViewBag.CardStatusList = spravCardStatusService.GetList();
            return View(new CardViewModel());
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD)]
        [HttpPost]
        public ActionResult CardAdd(CardViewModel card_add)
        {            
            if (ModelState.IsValid)
            {
                var res = cardService.Insert(card_add, ModelState);
                if (res != null)
                {
                    var card_id_new = ((CardViewModel)res).card_id.ToString();
                    return RedirectToAction("CardEdit", "Card", new { card_id = card_id_new, });
                }
                else
                {
                    var card_types = ViewBag.CardTypeList;
                    if (card_types == null)
                    {
                        ViewBag.CardTypeList = spravCardTypeService.GetList_byParent(CurrentUser.BusinessId);
                    }
                    var card_statuses = ViewBag.CardStatusList;
                    if (card_statuses == null)
                    {
                        ViewBag.CardStatusList = spravCardStatusService.GetList();
                    }
                    return View(card_add);
                }                
            }
            return View(card_add);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD)]
        public ActionResult CardEdit(long card_id)
        {
            var card = cardService.GetItem(card_id);
            if (card == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найдена карта с кодом " + card_id.ToString());
                return RedirectToAction("Oops", "Home");
            }                        
            return View(card);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD)]
        [HttpPost]
        public ActionResult CardEdit(CardViewModel card_edit)
        {
            if (ModelState.IsValid)
            {
                CardViewModel res = (CardViewModel)cardService.Update(card_edit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                //return View(res == null ? card_edit : res);
                return RedirectToAction("CardEdit", "Card", new { card_id = res.card_id });
            }
            return View(card_edit);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD)]
        [HttpPost]
        public ActionResult CardDel([DataSourceRequest]DataSourceRequest request, CardViewModel card)
        {
            if (ModelState.IsValid)
            {
                long card_id = 0;
                if (!long.TryParse(card.card_id_str, out card_id))
                {
                    ModelState.AddModelError("serv_result", "Некорректные параметры");
                    return Json(new[] { card }.ToDataSourceResult(request, ModelState));
                }
                card.card_id = card_id;
                cardService.Delete(card, ModelState);
            }
            
            return Json(new[] { card }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region CardBatch

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        [HttpGet]
        public ActionResult CardBatchAdd()
        {
            CardBatchOperationsViewModel cardBatchAdd = new CardBatchOperationsViewModel() { batch_type = Enums.BatchType.BatchAdd, };
            var res = cardBatchOperationsService.BuildCardNum(cardBatchAdd);
            cardBatchAdd.result_num_first = res.Item1;
            cardBatchAdd.result_num_last = res.Item2;
            return View(cardBatchAdd);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        [HttpPost]
        public ActionResult CardBatchAdd(CardBatchOperationsViewModel cardBatchAdd)
        {
            if (ModelState.IsValid)
            {
                cardBatchAdd.batch_type = Enums.BatchType.BatchAdd;
                var res = cardBatchOperationsService.Insert(cardBatchAdd, ModelState);
                if (res == null)
                {
                    ViewBag.Message = "Ошибка при запуске операции";
                    return View(cardBatchAdd);
                }
                else
                {
                    return RedirectToAction("CardBatchOperationsLog", "Card");
                }
            }
            return View(cardBatchAdd);
        }

        #endregion

        #region BuildCardNum

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        [HttpPost]
        public ActionResult BuildCardNum(CardBatchOperationsViewModel cardBatchAdd)
        {
            if (ModelState.IsValid)
            {
                var res = cardBatchOperationsService.BuildCardNum(cardBatchAdd);
                cardBatchAdd.result_num_first = res.Item1;
                cardBatchAdd.result_num_last = res.Item2;
                return Json(cardBatchAdd, JsonRequestBehavior.AllowGet);
            }
            return Json(cardBatchAdd, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CardBatchOperations

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        [HttpGet]
        public ActionResult CardBatchOperations()
        {
            ViewBag.CardTypeList = spravCardTypeService.GetList_byParent(CurrentUser.BusinessId);
            return View(new CardBatchOperationsViewModel());
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        [HttpPost]
        public ActionResult CardBatchOperations(CardBatchOperationsViewModel cardBatchOperations)
        {
            if (ModelState.IsValid)
            {
                var res = cardBatchOperationsService.Insert(cardBatchOperations, ModelState);
                return Json(new CardBatchOperationsViewModel { Message = "Операция выполняется..." });           
            }
            return View(cardBatchOperations);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        [HttpGet]
        public ActionResult CardBatchOperationsLog()
        {
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        public JsonResult GetCardBatchOperations([DataSourceRequest]DataSourceRequest request)
        {
            return Json(cardBatchOperationsService.GetList_byParent(CurrentUser.BusinessId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        [HttpPost]
        public JsonResult RefreshBatchOperations(long id)
        {
            return Json(cardBatchOperationsService.GetItem(id), JsonRequestBehavior.AllowGet);
            //return Json(GetService<CardBatchOperationsService>().GetList_byParent(Util.UserManager.CurrUser.BusinessId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        [HttpGet]
        public ActionResult CardBatchOperationsErrorList(long batch_id)
        {
            return View(cardBatchOperationsService.GetItem(batch_id));
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_BATCH)]
        public JsonResult GetCardBatchOperationsErrors([DataSourceRequest]DataSourceRequest request, long batch_id)
        {            
            return Json(cardBatchOperationsErrorService.GetList_byParent(batch_id).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CardImport

        private const string cardImportFolderBase = @"C:\Release\Cabinet\File\Done\";
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD_IMPORT)]
        [HttpGet]
        public ActionResult CardImport()
        {
            return View(new CardImportViewModel());
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD_IMPORT)]
        [HttpPost]
        public ActionResult CardImport(HttpPostedFileBase file1, HttpPostedFileBase file2, CardImportViewModel cardImport)
        {
            try
            {
                if ((cardImport.card_type_id.GetValueOrDefault(0) <= 0) || (cardImport.card_status_id.GetValueOrDefault(0) <= 0))
                {
                    ViewBag.Message = "Не заданы параметры импорта";
                    return View(cardImport);
                }

                if ((file1 == null) || (file1.ContentLength <= 0) || (file2 == null) || (file2.ContentLength <= 0))
                {
                    ViewBag.Message = "Не указан файл для импорта";
                    return View(cardImport);
                }
                
                var folder = Path.Combine(cardImportFolderBase, CurrentUser.UserName);
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                var fileLocation1 = Path.Combine(folder, Path.GetFileName(file1.FileName));
                if (System.IO.File.Exists(fileLocation1))
                {
                    System.IO.File.Delete(fileLocation1);
                }
                file1.SaveAs(fileLocation1);
                var fileLocation2 = Path.Combine(folder, Path.GetFileName(file2.FileName));
                if (System.IO.File.Exists(fileLocation2))
                {
                    System.IO.File.Delete(fileLocation2);
                }
                file2.SaveAs(fileLocation2);

                //return RedirectToAction("Edit", new { id = assetID });

                var res = cardImportService.ImportCards(fileLocation1, fileLocation2, cardImport);
                if (res)
                {
                    //ViewBag.Message = "Импорт завершен !";
                    //return View(cardImport);
                    return RedirectToAction("CardBatchOperationsLog", "Card");
                }
                else
                {
                    TempData.Clear();
                    TempData.Add("error", "Неизвестная ошибка при импорте карт");
                    return RedirectToAction("Oops", "Home");
                }
            }
            catch (Exception ex)
            {
                TempData.Clear();
                TempData.Add("error", GlobalUtil.ExceptionInfo(ex));
                return RedirectToAction("Oops", "Home");
            }
        }

        #endregion

        #region CardListExport

        //[ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [HttpPost]
        public ActionResult CardListExport(string contentType, string base64, string fileName)
        {            
            var fileContents = Convert.FromBase64String(base64);            
            return File(fileContents, contentType, fileName);            
        }

        #endregion

        #region CardAdditionalNum

        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermView(Enums.AuthPartEnum.DISCOUNT_CARD)]
        public ActionResult GetCardAdditionalNumList([DataSourceRequest] DataSourceRequest request, string card_id_str)
        {
            long card_id = 0;
            if (!long.TryParse(card_id_str, out card_id))
            {
                ModelState.AddModelError("", "Не найдена карта с кодом " + card_id_str);
                return RedirectToErrorPage(ModelState);
            }
            return Json(cardAdditionalNumService.GetList_byParent(card_id).ToDataSourceResult(request));
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD)]
        [HttpPost]
        public ActionResult CardAdditionalNumAdd([DataSourceRequest] DataSourceRequest request, CardAdditionalNumViewModel cardNumAdd, string cardIdStr)
        {
            if (ModelState.IsValid)
            {
                long card_id = 0;
                if (!long.TryParse(cardIdStr, out card_id))
                {
                    ModelState.AddModelError("", "Не найдена карта с кодом " + cardIdStr);
                    //return RedirectToErrorPage(ModelState);
                    return Json(new[] { cardNumAdd }.ToDataSourceResult(request, ModelState));
                }
                cardNumAdd.card_id = card_id;
                var res = cardAdditionalNumService.Insert(cardNumAdd, ModelState);
                ViewBag.Message = res == null ? "" : "Номер " + cardNumAdd.card_num + " добавлен !";
                return Json(new[] { cardNumAdd }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { cardNumAdd }.ToDataSourceResult(request, ModelState));
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD)]
        [HttpPost]
        public ActionResult CardAdditionalNumEdit([DataSourceRequest] DataSourceRequest request, CardAdditionalNumViewModel cardNumEdit, string card_id_str)
        {
            if (ModelState.IsValid)
            {
                long card_id = 0;
                if (!long.TryParse(card_id_str, out card_id))
                {
                    ModelState.AddModelError("", "Не найдена карта с кодом " + card_id_str);
                    //return RedirectToErrorPage(ModelState);
                    return Json(new[] { cardNumEdit }.ToDataSourceResult(request, ModelState));
                }                
                cardNumEdit.card_id = card_id;
                var res = cardAdditionalNumService.Update(cardNumEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return Json(new[] { cardNumEdit }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { cardNumEdit }.ToDataSourceResult(request, ModelState));
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.DISCOUNT)]
        [AuthUserPermEdit(Enums.AuthPartEnum.DISCOUNT_CARD)]
        [HttpPost]
        public ActionResult CardAdditionalNumDel([DataSourceRequest]DataSourceRequest request, CardAdditionalNumViewModel cardNumDel, string card_id_str)
        {
            if (ModelState.IsValid)
            {
                long card_id = 0;
                if (!long.TryParse(card_id_str, out card_id))
                {
                    ModelState.AddModelError("serv_result", "Некорректные параметры");
                    return Json(new[] { cardNumDel }.ToDataSourceResult(request, ModelState));
                }                
                cardNumDel.card_id = card_id;
                cardAdditionalNumService.Delete(cardNumDel, ModelState);
            }

            return Json(new[] { cardNumDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion
    }
}
