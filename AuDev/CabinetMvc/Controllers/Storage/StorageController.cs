﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Drawing;
using System.Web.Script.Serialization;
using System.IO;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Storage;
using CabinetMvc.ViewModel.Sys;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class StorageController : BaseController
    {
        #region StorageTree

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FOLDER)]
        [HttpGet]
        public ActionResult StorageTree(int? folder_id, int? file_id)
        {
            string viewbagFolderId = "";
            string viewbagFileId = "";
            if (file_id.GetValueOrDefault(0) > 0)
            {
                var res1 = storageFileService.GetItem((int)file_id);
                if (res1 != null)
                {
                    viewbagFolderId = ((StorageFileViewModel)res1).folder_id.ToString();
                    viewbagFileId = ((StorageFileViewModel)res1).file_id.ToString();
                }
            }
            else if (folder_id.GetValueOrDefault(0) > 0)
            {
                var res2 = storageFolderService.GetItem((int)folder_id);
                if (res2 != null)
                {
                    viewbagFolderId = ((StorageFolderViewModel)res2).folder_id.ToString();                    
                }
            }
            ViewBag.FolderId = viewbagFolderId;
            ViewBag.FileId = viewbagFileId;

            ViewBag.StorageFolderTree = storageFolderService.GetTreeList();
            
            //ViewBag.MaxFileSize = 20971520; // 20 Mb
            //ViewBag.MaxFileSize = 25971520;   // 25 Mb
            
            // также см. Web.config: requestFiltering и maxRequestLength            
            ViewBag.MaxFileSize = storageParamService.GetMaxFileSize((int)Enums.StorageEnum.MAIN);
            ViewBag.AllowedExtentions = storageParamService.GetAllowedExtentions((int)Enums.StorageEnum.MAIN);
            return View();
        }

        #endregion

        #region StorageFolderTree

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FOLDER)]
        [HttpGet]
        public ActionResult GetStorageFolderTree(int? id)
        {            
            if (id.GetValueOrDefault(0) > 0)
                return Json(storageFolderService.GetList_byParent((long)id), JsonRequestBehavior.AllowGet);
            else
                return Json(storageFolderService.GetList(), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FOLDER)]
        [HttpPost]
        public JsonResult StorageFolderTreeMove(int id, int? parent_id)
        {
            var res = storageFolderService.Move(id, parent_id, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошбика при перемещении папки");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false, mess = "" });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FOLDER)]
        [HttpPost]
        public JsonResult StorageFolderTreeAdd(string folder_name, int? parent_id)
        {
            if (ModelState.IsValid)
            {
                var res = storageFolderService.Insert(new StorageFolderViewModel() { folder_name = folder_name, parent_id = parent_id }, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении папки");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }                
                StorageFolderViewModel folder_new = (StorageFolderViewModel)res;
                return Json(new { err = false, mess = "", folder = folder_new });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FOLDER)]
        [HttpPost]
        public JsonResult StorageFolderTreeEdit(int folder_id, string folder_name)
        {
            if (ModelState.IsValid)
            {
                var itemEdit = storageFolderService.GetItem(folder_id);
                ((StorageFolderViewModel)itemEdit).folder_name = folder_name;
                var res = storageFolderService.Update(itemEdit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при перименовывании папки");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                StorageFolderViewModel folder_new = (StorageFolderViewModel)res;
                return Json(new { err = false, mess = "", folder = folder_new });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FOLDER)]
        [HttpPost]
        public JsonResult StorageFolderTreeDel(int folder_id)
        {
            if (ModelState.IsValid)
            {
                var itemDel = storageFolderService.GetItem(folder_id);
                var res = storageFolderService.Delete(itemDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении папки");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FOLDER)]
        [HttpPost]
        public JsonResult StorageFolderTreeDownload()
        {
            if (!CurrentUser.IsSuperAdmin)
            {
                ModelState.AddModelError("", "Нет прав");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }

            if (ModelState.IsValid)
            {
                string res = "";                
                res = storageFolderService.Download(ModelState);
                if ((String.IsNullOrEmpty(res)) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при скачивании хранилища");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "", storage_download_link = res });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FOLDER)]        
        public PartialViewResult StorageFolderTree()
        {
            ViewBag.StorageFolderTree = storageFolderService.GetTreeList();
            ViewBag.MaxFileSize = storageParamService.GetMaxFileSize((int)Enums.StorageEnum.MAIN);
            ViewBag.AllowedExtentions = storageParamService.GetAllowedExtentions((int)Enums.StorageEnum.MAIN);
            return PartialView();
        }

        #endregion

        #region StorageFileTree

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FILE)]
        [HttpGet]
        public ActionResult GetStorageFileTree(int folder_id)
        {
            return Json(storageFileService.GetList_byParent(folder_id), JsonRequestBehavior.AllowGet);            
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FILE)]
        [HttpPost]
        public JsonResult StorageFileTreeMove(int id, int parent_id, int item_type)
        {
            Enums.StorageItemEnum item_type_enum = Enums.StorageItemEnum.FILE;
            bool parseOk = Enum.TryParse<Enums.StorageItemEnum>(item_type.ToString(), out item_type_enum);
            if (!parseOk)
                item_type_enum = Enums.StorageItemEnum.FILE;

            bool res = false;
            switch (item_type_enum)
            {
                case Enums.StorageItemEnum.FILE:
                    res = storageFileService.Move(id, parent_id, ModelState);
                    break;
                case Enums.StorageItemEnum.LINK:
                    res = storageLinkService.Move(id, parent_id, ModelState);
                    break;
                default:
                    break;
            }
            
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошбика при перемещении файла");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false, mess = "" });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FILE)]
        public ActionResult StorageFileTreeSave(IEnumerable<HttpPostedFileBase> uplFile, int? folder_id, bool overwrite, string file_date)
        {
            // The Name of the Upload component is "uplFile"
            if (uplFile != null)
            {
                if (folder_id.GetValueOrDefault(0) <= 0)
                {
                    ModelState.AddModelError("", "Не указана папка");
                    return Content(ModelState.GetErrorsString());
                }

                foreach (var file in uplFile)
                {
                    var res = storageFileService.UploadFile(file, (int)folder_id, overwrite, file_date, ModelState);
                    if ((res == null) || (!ModelState.IsValid))
                    {
                        if (ModelState.IsValid)
                            ModelState.AddModelError("", "Ошибка при добавлении файла");
                        return Content(ModelState.GetErrorsString());
                    }
                }
            }

            // Return an empty string to signify success
            return Content("");
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FILE)]
        [HttpPost]
        public JsonResult StorageFileTreeEdit(int file_id, string file_name, int item_type)
        {
            Enums.StorageItemEnum item_type_enum = Enums.StorageItemEnum.FILE;
            bool parseOk = Enum.TryParse<Enums.StorageItemEnum>(item_type.ToString(), out item_type_enum);
            if (!parseOk)
                item_type_enum = Enums.StorageItemEnum.FILE;

            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel res = null;
                ViewModel.AuBaseViewModel itemEdit = null;                
                switch (item_type_enum)
                {
                    case Enums.StorageItemEnum.FILE:     
                        itemEdit = storageFileService.GetItem(file_id);
                        ((StorageFileViewModel)itemEdit).file_name = file_name;
                        res = storageFileService.Update(itemEdit, ModelState);
                        break;
                    case Enums.StorageItemEnum.LINK:                        
                        itemEdit = storageLinkService.GetItem(file_id);
                        ((StorageLinkViewModel)itemEdit).link_name = file_name;
                        res = storageLinkService.Update(itemEdit, ModelState);                 
                        break;
                    default:
                        break;
                }

                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при перименовывании файла");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }

                switch (item_type_enum)
                {
                    case Enums.StorageItemEnum.FILE:
                        return Json(new { err = false, mess = "", file = (StorageFileViewModel)res });
                    case Enums.StorageItemEnum.LINK:
                        return Json(new { err = false, mess = "", file = (StorageLinkViewModel)res });                        
                    default:
                        return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FILE)]
        [HttpPost]
        public JsonResult StorageFileTreeDel(int file_id, int item_type)
        {
            Enums.StorageItemEnum item_type_enum = Enums.StorageItemEnum.FILE;
            bool parseOk = Enum.TryParse<Enums.StorageItemEnum>(item_type.ToString(), out item_type_enum);
            if (!parseOk)
                item_type_enum = Enums.StorageItemEnum.FILE;

            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel itemDel = null;
                bool res = false;
                switch (item_type_enum)
                {
                    case Enums.StorageItemEnum.FILE:
                        itemDel = storageFileService.GetItem(file_id);
                        res = storageFileService.Delete(itemDel, ModelState);
                        break;
                    case Enums.StorageItemEnum.LINK:
                        itemDel = storageLinkService.GetItem(file_id);
                        res = storageLinkService.Delete(itemDel, ModelState);
                        break;
                    default:
                        break;
                }

                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении файла");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FILE)]
        [HttpPost]
        public JsonResult StorageFileTreeCheck(string name, int parent_id)
        {
            if (ModelState.IsValid)
            {
                var res = storageFileService.CheckExists(name, parent_id);
                return Json(new { err = false, mess = "", fileExists = res });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FILE)]
        [HttpPost]
        public JsonResult StorageFileSearch(string file_name)
        {
            if (ModelState.IsValid)
            {
                var res = storageFileService.Find(file_name);
                if ((res == null) || (res.Count() <= 0) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Файлы не найдены");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }                
                //return Json(new { err = false, mess = "", folder_id = res.folder_id, file_id = res.file_id });
                return Json(new { err = false, mess = "", fileList = JsonConvert.SerializeObject(res) });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FILE)]
        [HttpGet]
        public ActionResult StorageFileGet(int? file_id, int? version)
        {             

            if (file_id.GetValueOrDefault(0) > 0)
            {
                StorageFileViewModel res_file = null;
                StorageFileVersionViewModel res_version = null;
                
                res_file = (StorageFileViewModel)storageFileService.GetItem((int)file_id);

                if (version.GetValueOrDefault(0) > 0)
                {
                    res_version = (StorageFileVersionViewModel)storageFileVersionService.GetFileVersion((int)file_id, (int)version);
                    if ((res_version == null) || (!ModelState.IsValid))
                    {
                        if (ModelState.IsValid)
                            ModelState.AddModelError("", "Ошибка при скачивании версии файла");
                        return RedirectToErrorPage(ModelState);
                    }
                    return File(Path.Combine(res_version.physical_path, res_version.physical_name), System.Net.Mime.MediaTypeNames.Application.Octet, res_file.file_name_with_extention);
                }
                else
                {                    
                    if ((res_file == null) || (!ModelState.IsValid))
                    {
                        if (ModelState.IsValid)
                            ModelState.AddModelError("", "Ошибка при скачивании файла");
                        return RedirectToErrorPage(ModelState);
                    }
                    switch ((Enums.StorageItemEnum)res_file.item_type)
                    {
                        case Enums.StorageItemEnum.FILE:
                            var res = GlobalUtil.GetFile(res_file.physical_path, Path.Combine(res_file.physical_path, res_file.physical_name));
                            return File(res, System.Net.Mime.MediaTypeNames.Application.Octet, res_file.file_name_with_extention);
                            /*
                            System.Net.NetworkCredential writeCredentials = new System.Net.NetworkCredential("Михаил Павлов", "123456", "aptekaural");                            
                            if (!System.Diagnostics.Debugger.IsAttached)
                            {
                                using (new AuDev.Common.Network.NetworkConnection(res_file.physical_path, writeCredentials))
                                {
                                    return File(Path.Combine(res_file.physical_path, res_file.physical_name), System.Net.Mime.MediaTypeNames.Application.Octet, res_file.file_name_with_extention);
                                }
                            }
                            else
                            {
                                return File(Path.Combine(res_file.physical_path, res_file.physical_name), System.Net.Mime.MediaTypeNames.Application.Octet, res_file.file_name_with_extention);
                            }
                            */

                            //return File(Path.Combine(res_file.physical_path, res_file.physical_name), System.Net.Mime.MediaTypeNames.Application.Octet, res_file.file_name_with_extention);
                        case Enums.StorageItemEnum.LINK:
                            return Redirect(res_file.download_link);
                        default:
                            ModelState.AddModelError("", "Неизвестный тип файла");
                            return RedirectToErrorPage(ModelState);
                    }
                }
                
            }
            ModelState.AddModelError("", "Не указан файл");
            return RedirectToErrorPage(ModelState);            
        }


        #endregion

        #region StorageParam

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_PARAM)]
        [HttpGet]
        public ActionResult StorageParam()
        {
            var fileTypeGroupList = storageFileTypeGroupService.GetList();
            ViewData["FileTypeGroupList"] = fileTypeGroupList;
            ViewData["FileTypeDefault"] = fileTypeGroupList.FirstOrDefault();
            
            List<SelectListItem> maxFileSizeList = new List<SelectListItem>() 
            { 
                new SelectListItem() 
                { 
                    Text = "5 Мб",
                    Value = "5242880",
                }, 
                new SelectListItem() 
                { 
                    Text = "10 Мб",
                    Value = "10485760",
                }, 
                new SelectListItem() 
                { 
                    Text = "15 Мб",
                    Value = "15728640",
                }, 
                new SelectListItem() 
                { 
                    Text = "20 Мб",
                    Value = "20971520",
                }, 
            };
            ViewBag.MaxFileSizeList = maxFileSizeList;
            return View(storageParamService.GetItem((int)Enums.StorageEnum.MAIN));
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_LOG)]
        [HttpPost]
        public JsonResult StorageParamSave(string max_file_size)
        {
            if (ModelState.IsValid)
            {
                var res = storageParamService.Update(new StorageParamViewModel() { max_file_size = max_file_size, storage_id = (int)Enums.StorageEnum.MAIN }, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении параметров");
                    return Json(new { err = true, mess = ModelState.GetErrorsString(), });
                }
                return Json(new { err = false, mess = "", });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }

        #endregion

        #region StorageFileType

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_PARAM)]
        public ActionResult GetStorageFileTypeList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(storageFileTypeService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_PARAM)]
        [HttpPost]
        public ActionResult StorageFileTypeAdd([DataSourceRequest] DataSourceRequest request, StorageFileTypeViewModel itemAdd)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                var res = storageFileTypeService.Insert(itemAdd, ModelState);
            }

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_PARAM)]
        [HttpPost]
        public ActionResult StorageFileTypeEdit([DataSourceRequest]DataSourceRequest request, StorageFileTypeViewModel itemEdit)
        {
            /*
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                storageFileTypeService.Update(itemEdit, ModelState);
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
            */
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_PARAM)]
        [HttpPost]
        public ActionResult StorageFileTypeDel([DataSourceRequest]DataSourceRequest request, StorageFileTypeViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                storageFileTypeService.Delete(itemDel, ModelState);
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region StorageLog

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_LOG)]
        [HttpGet]
        public ActionResult StorageLog()
        {            
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_LOG)]
        public ActionResult GetStorageLogList([DataSourceRequest]DataSourceRequest request, int? file_id)
        {
            if (file_id.HasValue)
                return Json(storageLogService.GetList_byParent((int)file_id).ToDataSourceResult(request));
            else
                return Json(storageLogService.GetList().ToDataSourceResult(request));
        }

        //GetStorageLogForFileList

        //GetStorageLogList

        #endregion        

        #region StorageLinkAdd
                
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STORAGE_FILE)]
        [HttpPost]
        public JsonResult StorageLinkAdd(int folder_id, string link_url, string link_descr)
        {
            if (ModelState.IsValid)
            {
                var res = storageLinkService.Insert(new StorageLinkViewModel() {
                    download_link = link_url,
                    folder_id = folder_id,
                    link_name = String.IsNullOrEmpty(link_descr) ? link_url : link_descr,
                }, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении ссылки");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }                
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        #endregion        

        #region StorageTrash

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FOLDER)]
        public ActionResult GetStorageTrashList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(storageTrashService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STORAGE_FOLDER)]
        public JsonResult StorageTrashClear([DataSourceRequest]DataSourceRequest request)
        {
            if (ModelState.IsValid)
            {
                var res = storageTrashService.Clear(ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при очистке корзины");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        public JsonResult StorageTrashRestore(int item_id, int? folder_id)
        {
            if (ModelState.IsValid)
            {
                var res = storageTrashService.Restore(item_id, folder_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при восстановлении из корзины");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }
        
        #endregion

        // download with rename
        // <a href="http://server/site/test.txt" download="test001.txt">Download Your File</a>
        
        /*
            If you are using IIS for hosting your application, then the default upload file size if 4MB. To increase it, please use this below section in your web.config -

            <configuration>
                <system.web>
                    <httpRuntime maxRequestLength="25971520" />
                </system.web>
            </configuration>

            For IIS7 and above, you also need to add the lines below:

             <system.webServer>
               <security>
                  <requestFiltering>
                     <requestLimits maxAllowedContentLength="25971520" />
                  </requestFiltering>
               </security>
             </system.webServer>
        */
    }
}
