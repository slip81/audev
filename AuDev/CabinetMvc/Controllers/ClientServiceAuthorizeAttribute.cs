﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CabinetMvc.Auth;
using CabinetMvc.ViewModel.User;
using AuDev.Common.Util;

namespace CabinetMvc.Controllers
{
    public class ClientServiceAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly Enums.ClientServiceEnum[] allowedservices;
        public ClientServiceAuthorizeAttribute(params Enums.ClientServiceEnum[] services)
        {
            this.allowedservices = services;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var srv = allowedservices.FirstOrDefault();
            if (srv == Enums.ClientServiceEnum.DISCOUNT)
            {
                //
            }
            bool authorize = false;

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var user = ((IUserProvider)auth.CurrentUser.Identity).User;

            if (user == null)
                return authorize;
            if (user.IsAdmin)
            {
                authorize = true;
                return authorize;
            }

            foreach (var serv in allowedservices)
            {
                authorize = true;
                if (!user.ServiceList.Contains(serv))
                {
                    authorize = false;
                    break;
                }
            }

            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
        /*
        private readonly string[] allowedroles;
        public ClientServiceAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var user = ((IUserProvider)auth.CurrentUser.Identity).User;

            if (user == null)
                return authorize;
            if (user.IsAdmin)
            {
                authorize = true;
                return authorize;
            }

            if (allowedroles.Contains("HaveDiscount"))
                return user.HaveDiscount;

            if (allowedroles.Contains("HaveDefect"))
                return user.HaveDefect;

            if (allowedroles.Contains("HaveExchange"))
                return user.HaveExchange;

            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
        */
    }

}