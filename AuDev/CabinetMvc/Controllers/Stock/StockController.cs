﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.ViewModel.Wa;
using AuDev.Common.Util;
using CabinetMvc.Extensions;
#endregion

namespace CabinetMvc.Controllers
{    
    public class StockController : BaseController
    {        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult Index()
        {
            return View();
        }

        #region StockList

        [ClientServiceAuthorize(Enums.ClientServiceEnum.STOCK)]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK)]
        public ActionResult StockList()
        {            
            return View();
        }
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.STOCK)]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK)]
        public ActionResult GetStockList([DataSourceRequest]DataSourceRequest request, int? client_id, int? sales_id, string search)
        {
            return Json(stockService.GetList_bySales(client_id, sales_id, search).ToDataSourceResult(request));
        }

        #endregion

        #region StockLogList

        [ClientServiceAuthorize(Enums.ClientServiceEnum.STOCK)]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK_LOG)]
        public ActionResult StockLogList()
        {
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.STOCK)]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK_LOG)]
        public ActionResult GetStockUploadLogList([DataSourceRequest]DataSourceRequest request, int? client_id, int? sales_id)
        {
            return Json(stockBatchService.GetList_bySales(client_id, sales_id).ToDataSourceResult(request));
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.STOCK)]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK_LOG)]
        public ActionResult GetStockDownloadLogList([DataSourceRequest]DataSourceRequest request, int? client_id, int? sales_id)
        {
            return Json(stockSalesDownloadService.GetList_bySales(client_id, sales_id).ToDataSourceResult(request));
        }

        #endregion

        #region StockTaskList

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK)]
        public ActionResult StockTaskList()
        {
            List<WaRequestTypeViewModel> waRequestTypeList = waRequestTypeService.GetList_byParent((int)Enums.WaRequestTypeGroup1Enum.STOCK)
                .AsEnumerable().Cast<WaRequestTypeViewModel>().ToList();
            ViewData["requestTypeList"] = waRequestTypeList;
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK)]
        public ActionResult GetStockTaskList_bySales([DataSourceRequest]DataSourceRequest request, int? sales_id)
        {
            return Json(waTaskService.GetList_bySales(sales_id.GetValueOrDefault(0), (int)Enums.WaRequestTypeGroup1Enum.STOCK, null).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK)]
        public ActionResult GetStockTaskLcList([DataSourceRequest]DataSourceRequest request, int? task_id)
        {
            return Json(waTaskLcService.GetList_byParent(task_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STOCK)]
        [HttpPost]
        public JsonResult StockTaskAdd(int? sales_id, int request_type_id)
        {
            if (ModelState.IsValid)
            {
                var res = stockService.InsertTask(sales_id.GetValueOrDefault(0), request_type_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении задания");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.STOCK)]
        [HttpPost]
        public ActionResult StockTaskDel(int task_id)
        {
            if (ModelState.IsValid)
            {                                
                var res = stockService.CancelTask(task_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении задания");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        #endregion

        #region StockLastList

        [ClientServiceAuthorize(Enums.ClientServiceEnum.STOCK)]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK_LOG)]
        public ActionResult StockLastList()
        {
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.STOCK)]
        [AuthUserPermView(Enums.AuthPartEnum.STOCK_LOG)]
        public ActionResult GetStockLastList([DataSourceRequest]DataSourceRequest request, int? client_id, int? sales_id, int? day_cnt)
        {
            return Json(stockLogService.GetList_bySales(client_id, sales_id, day_cnt).ToDataSourceResult(request));
        }

        #endregion

    }
}
