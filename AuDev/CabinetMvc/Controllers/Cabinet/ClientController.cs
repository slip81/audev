﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.User;
using CabinetMvc.ViewModel.Sys;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {
        #region Client

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientList([DataSourceRequest]DataSourceRequest request, string client_name = "", bool is_jur = true, bool is_phis = true, string wp_key = "", string reg_key = "", int? service_id = null)
        {
            //return Json(clientService.GetList().ToDataSourceResult(request));
            return Json(clientService.GetList_byFilter(client_name, is_jur, is_phis, wp_key, reg_key, service_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientList_Sprav([DataSourceRequest]DataSourceRequest request)
        {
            return Json(clientService.GetList_Sprav().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult ClientList()
        {
            ViewBag.ClientGridColumnSettings = cabGridColumnUserSettingsService.GetList_byParent((int)Enums.CabGrid.ClientList);

            var gridUserSettings = cabGridUserSettingService.GetItem((int)Enums.CabGrid.ClientList);
            ViewBag.ClientGridColumnWidths = gridUserSettings == null ? "" : ((CabGridUserSettingsViewModel)gridUserSettings).columns_options;

            ViewBag.ServiceList = serviceService.GetList();
            return View();
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult ClientAdd()
        {
            var client = clientService.GetItem(0);
            ViewData["defRegion"] = 1;
            ViewData["defCity"] = 26;
            return View(client);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ClientAdd(ClientViewModel clientAdd)
        {
            if (ModelState.IsValid)
            {
                var res = clientService.Insert(clientAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении клиента");
                    return RedirectToErrorPage(ModelState);
                }
                return RedirectToAction("ClientEdit", "Cabinet", new { client_id = ((ClientViewModel)res).id });
            }
            ViewData["defRegion"] = 1;
            ViewData["defCity"] = 26;
            return View(clientAdd);
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult ClientEdit(int client_id, int? active_page, string mess)
        {
           
            var client = clientService.GetItem(client_id);
            if (client == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найден клиент с кодом " + client_id.ToString());
                return RedirectToAction("Oops", "Home");
            }
            ViewBag.SalesList = salesService.GetList_byParent(client_id);
            ViewBag.ActivePage = active_page.GetValueOrDefault(0);
            ViewBag.Message = mess;
            return View(client);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ClientEdit(ClientViewModel client_edit)
        {            
            if (ModelState.IsValid)
            {
                var res = clientService.Update(client_edit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении данных клиента");
                    return RedirectToErrorPage(ModelState);
                }

                //return ClientEdit(client_edit.id, null, "Изменения успешно сохранены !");
                return RedirectToAction("ClientEdit", "Cabinet", new { @client_id = client_edit.id, @active_page = 3, @mess = "Изменения успешно сохранены !" });
            }
            return View(client_edit);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ClientActivate(int client_id)
        {
            if (ModelState.IsValid)
            {
                var res = clientService.ClientActivate(client_id, ModelState);
                if ((res == null) || (!res.Item1))
                {                    
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", res != null ? res.Item2 : "Неизвестная ошибка при активации потребителя");
                }
                return Json(new[] { client_id });      
            }
            else
            {
                return RedirectToErrorPage(ModelState);
            }
        }

        public ActionResult GetClientList_Stock([DataSourceRequest]DataSourceRequest request)
        {
            return Json(clientService.GetList_byService((int)Enums.ClientServiceEnum.STOCK), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region ClientService

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientServiceList([DataSourceRequest]DataSourceRequest request, int client_id)
        {
            return Json(clientServiceService.GetList_Service(client_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientServiceWorkplaceList([DataSourceRequest]DataSourceRequest request, int client_id, int service_id)
        {
            return Json(clientServiceService.GetList_Workplace(client_id, service_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientServiceUnregList([DataSourceRequest]DataSourceRequest request, int client_id)
        {
            return Json(clientServiceUnregService.GetList_byParent(client_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public JsonResult ClientServiceUnregDel(IEnumerable<ClientServiceUnregViewModel> servListForDel)
        {
            if (ModelState.IsValid)
            {
                var res = clientServiceUnregService.DeleteList(servListForDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении списка услуг");
                    return Json(new { error = true, mess = ModelState.GetErrorsString(), });
                }
                return Json(new { error = false, mess = "" });
            }
            return Json(new { error = true, mess = ModelState.GetErrorsString(), });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientServiceParamList([DataSourceRequest]DataSourceRequest request, int client_id)
        {            
            return Json(clientServiceParamService.GetList_byParent(client_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ClientServiceParamEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ClientServiceParamViewModel> items, int client_id)
        {
            if (items != null && ModelState.IsValid)
            {
                var serv = clientServiceParamService;
                foreach (var item in items)
                {                    
                    serv.Update(item, ModelState);                    
                }
            }
            return Json(items.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ClientUser

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientUserList([DataSourceRequest]DataSourceRequest request, int client_id)
        {
            return Json(clientUserService.GetList_byParent(client_id).ToDataSourceResult(request));            
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult ClientUserEdit(int client_id, int user_id)
        {            
            ViewBag.CurrClient = clientService.GetItem_Short(client_id);
            ViewBag.SalesList = salesService.GetList_Sprav(client_id);
            ViewData["RolesList"] = client_id == 1000 ? authRoleService.GetList_byGroup((int)Enums.AuthGroupEnum.PERS) : authRoleService.GetList_byGroup((int)Enums.AuthGroupEnum.CLIENTS);
            if (user_id <= 0)
            {                
                return View(new ClientUserViewModel() { client_id = client_id, is_active = 1 });
            }
            else
            {
                var user = clientUserService.GetItem(user_id);
                if (user == null)
                {
                    ModelState.AddModelError("", "Не найден пользователь с кодом " + user_id.ToString());
                    return RedirectToErrorPage(ModelState);
                }                
                return View(user);
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ClientUserEdit(ClientUserViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel res = null;
                if (itemEdit.user_id > 0)
                {
                    res = clientUserService.Update(itemEdit, ModelState);
                }
                else
                {
                    res = clientUserService.Insert(itemEdit, ModelState);
                }

                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании пользователя");
                    return View(itemEdit);
                }
                return RedirectToAction("ClientEdit", "Cabinet", new { client_id = ((ClientUserViewModel)res).client_id, active_page = 2, });                
            }
            return View(itemEdit);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ClientUserDel([DataSourceRequest]DataSourceRequest request, ClientUserViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                clientUserService.Delete(itemDel, ModelState);
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ClientMaxVersion

        [SysRoleAuthorize("Superadmin")]        
        public ActionResult ClientMaxVersion()
        {
            return View();
        }

        [SysRoleAuthorize("Superadmin")]        
        public ActionResult GetClientMaxVersionList([DataSourceRequest]DataSourceRequest request)
        {            
            return Json(clientMaxVersionService.GetList().ToDataSourceResult(request));
        }

        #endregion

        #region ClientReport

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult ClientReport()
        {
            ViewData["serviceList"] = serviceService.GetList();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientReport([DataSourceRequest]DataSourceRequest request, int? is_client, string service_name)
        {
            bool is_client_only = is_client == 1 ? true : false;
            return Json(clientReportService.GetList_byFilter(is_client_only, service_name).ToDataSourceResult(request));
        }

        #endregion

    }
}
