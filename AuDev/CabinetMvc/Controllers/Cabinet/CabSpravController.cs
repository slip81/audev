﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.User;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {
        #region CabService

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpGet]
        public ActionResult CabServiceList()
        {
            ViewBag.CabServiceList = serviceService.GetList();
            ViewBag.CabServiceGroupList = serviceGroupService.GetList();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public ActionResult CabServiceEdit([DataSourceRequest]DataSourceRequest request, ServiceViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                serviceService.Update(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ServiceKit

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpGet]
        public ActionResult ServiceKitEdit(int service_kit_id, int? client_id)
        {
            ViewBag.ServiceList1 = serviceService.GetList_notInKit(service_kit_id);            
            ViewBag.ServiceList2 = serviceService.GetList_inKit(service_kit_id);
            ViewBag.BackToClient = client_id.GetValueOrDefault(0) > 0 ? client_id.ToString() : null;

            if (service_kit_id <= 0)
            {
                return View(new ServiceKitViewModel());
            }
            else
            {
                var serviceKit = serviceKitService.GetItem(service_kit_id);
                if (serviceKit == null)
                {
                    ModelState.AddModelError("", "Не найден набор с кодом " + service_kit_id.ToString());
                    return RedirectToErrorPage(ModelState);
                }
                return View(serviceKit);
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public JsonResult ServiceKitEdit(int service_kit_id, string service_kit_name, IEnumerable<ServiceViewModel> serviceList)
        {
            if (ModelState.IsValid)
            {
                bool res = serviceKitService.ServiceKitAdd(service_kit_id, service_kit_name, serviceList, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении набора");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public JsonResult ServiceKitDel(int service_kit_id)
        {
            if (ModelState.IsValid)
            {
                bool res = serviceKitService.Delete(new ServiceKitViewModel() { service_kit_id = service_kit_id }, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении набора");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public JsonResult GetServiceKitItemList(int service_kit_id)
        {            
            return Json(new { err = false, mess = "", serviceKitItemList = JsonConvert.SerializeObject(serviceKitItemService.GetList_byParent(service_kit_id)) });
        }

        #endregion
        
        #region VersionBatch

        public JsonResult VersionBatchAdd(string version_num, int? group_id)
        {
            if (ModelState.IsValid)
            {
                var res = versionService.VersionBatchAdd(version_num, group_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении версий");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        public JsonResult VersionBatchDel(string version_num, int? group_id)
        {
            if (ModelState.IsValid)
            {
                var res = versionService.VersionBatchDel(version_num, group_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении версий");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        #endregion

        #region Region

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpGet]
        public ActionResult RegionList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.SPRAV_SERVICE)]
        public ActionResult GetRegionList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(spravRegionService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public ActionResult RegionAdd([DataSourceRequest]DataSourceRequest request, SpravRegionViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = spravRegionService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении региона");
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public ActionResult RegionEdit([DataSourceRequest]DataSourceRequest request, SpravRegionViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = spravRegionService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении региона");
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public ActionResult RegionDel([DataSourceRequest]DataSourceRequest request, SpravRegionViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = spravRegionService.Delete(item, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении региона");
                }
                return Json(new[] { item }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region RegionZone

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.SPRAV_SERVICE)]
        public ActionResult GetRegionZoneList([DataSourceRequest]DataSourceRequest request, int? region_id)
        {
            return Json(spravRegionZoneService.GetList_byParent(region_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public ActionResult RegionZoneAdd([DataSourceRequest]DataSourceRequest request, SpravRegionZoneViewModel item, int? regionId)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                item.region_id = regionId.GetValueOrDefault(0);
                var res = spravRegionZoneService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении зоны региона");
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public ActionResult RegionZoneEdit([DataSourceRequest]DataSourceRequest request, SpravRegionZoneViewModel item, int? regionId)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                item.region_id = regionId.GetValueOrDefault(0);
                var res = spravRegionZoneService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении зоны региона");
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public ActionResult RegionZoneDel([DataSourceRequest]DataSourceRequest request, SpravRegionZoneViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = spravRegionZoneService.Delete(item, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении зоны региона");
                }
                return Json(new[] { item }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region RegionPriceLimit

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.SPRAV_SERVICE)]
        public ActionResult GetRegionPriceLimitList([DataSourceRequest]DataSourceRequest request, int? region_id)
        {
            return Json(spravRegionPriceLimitService.GetList_byParent(region_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public JsonResult RegionPriceLimitSave(int? region_id, List<SpravRegionPriceLimitViewModel> price_limit_list)
        {
            if (ModelState.IsValid)
            {
                if (region_id.GetValueOrDefault(0) <= 0)
                    return Json(new { err = true, mess = "Не задан регион" });
                if ((price_limit_list == null) || (price_limit_list.Count <= 0))
                    return Json(new { err = false, });

                foreach (var price_limit_list_item in price_limit_list)
                {
                    var res = spravRegionPriceLimitService.Update(price_limit_list_item, ModelState);
                    if ((res == null) || (!ModelState.IsValid))
                    {
                        if (ModelState.IsValid)
                            ModelState.AddModelError("", "Ошибка при сохранении наценки");
                        return Json(new { err = true, mess = ModelState.GetErrorsString() });
                    }
                }

                return Json(new { err = false, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public JsonResult RegionPriceLimitDel(int? region_id)
        {
            if (ModelState.IsValid)
            {
                if (region_id.GetValueOrDefault(0) <= 0)
                    return Json(new { err = true, mess = "Не задан регион" });

                var res = spravRegionPriceLimitService.Delete_byRegion(region_id.GetValueOrDefault(0), ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении наценок");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }

                return Json(new { err = false, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }

        #endregion

        #region RegionZonePriceLimit

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.SPRAV_SERVICE)]
        public ActionResult GetRegionZonePriceLimitList([DataSourceRequest]DataSourceRequest request, int? zone_id)
        {
            return Json(spravRegionZonePriceLimitService.GetList_byParent(zone_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public JsonResult RegionZonePriceLimitSave(int? zone_id, List<SpravRegionZonePriceLimitViewModel> price_limit_list)
        {
            if (ModelState.IsValid)
            {
                if (zone_id.GetValueOrDefault(0) <= 0)
                    return Json(new { err = true, mess = "Не задана зона" });
                if ((price_limit_list == null) || (price_limit_list.Count <= 0))
                    return Json(new { err = false, });

                foreach (var price_limit_list_item in price_limit_list)
                {
                    var res = spravRegionZonePriceLimitService.Update(price_limit_list_item, ModelState);
                    if ((res == null) || (!ModelState.IsValid))
                    {
                        if (ModelState.IsValid)
                            ModelState.AddModelError("", "Ошибка при сохранении наценки");
                        return Json(new { err = true, mess = ModelState.GetErrorsString() });
                    }
                }

                return Json(new { err = false, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.SPRAV_SERVICE)]
        [HttpPost]
        public JsonResult RegionZonePriceLimitDel(int? zone_id)
        {
            if (ModelState.IsValid)
            {
                if (zone_id.GetValueOrDefault(0) <= 0)
                    return Json(new { err = true, mess = "Не задана зона" });

                var res = spravRegionZonePriceLimitService.Delete_byZone(zone_id.GetValueOrDefault(0), ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении наценок");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }

                return Json(new { err = false, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }

        #endregion
    }
}
