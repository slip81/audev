﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {

        //[SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetLicenseList([DataSourceRequest]DataSourceRequest request, int client_id, int service_id)
        {
            return Json(licenseService.GetList_byClientAndService(client_id, service_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult LicenseEdit(int license_id)
        {
            var lic = licenseService.GetItem(license_id);
            if (lic == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найдена лицензия с кодом " + license_id.ToString());
                return RedirectToAction("Oops", "Home");
            }            
            return View(lic);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]        
        public ActionResult LicenseEdit(LicenseViewModel lic)
        {            
            if (ModelState.IsValid)
            {
                var res = licenseService.Update(lic, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return View(res == null ? lic : res);
            }
            return View(lic);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult LicenseDel(string license_id_str)
        {
            TempData.Clear();
            TempData.Add("error", "Пока не реализовано");
            return RedirectToAction("Oops", "Home");
        }

    }
}
