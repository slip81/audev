﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {
        #region Compare

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult Compare()
        {
            ViewBag.StateList = clientCrmService.GetStateList();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientCabList([DataSourceRequest]DataSourceRequest request, string group_id)
        {
            int group_int = 0;
            bool parseOk = int.TryParse(group_id, out group_int);
            if (!parseOk)
                group_int = 0;
            return Json(clientCabService.GetList_byFilter(group_int).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetClientCrmList([DataSourceRequest]DataSourceRequest request, string group_id)
        {
            int group_int = 0;
            bool parseOk = int.TryParse(group_id, out group_int);
            if (!parseOk)
                group_int = 0;
            return Json(clientCrmService.GetList_byFilter(group_int).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public JsonResult ClientCabListUpdate(IEnumerable<ClientCabViewModel> items)
        {
            if (items == null)
                return Json(new { err = true, mess = "Нет новых строк", });
            bool res = clientCabService.UpdateCabinetClientFromCrmClient(items);
            if (!res)
                return Json(new { err = true, mess = "Ошибка сохранения", });
            return Json(new { err = false, });
        }

        #endregion
    }
}
