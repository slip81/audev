﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
using CabinetMvc.Extensions;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult SalesAdd(int client_id)
        {
            ViewBag.CurrClient = clientService.GetItem_Short(client_id);
            return View(new SalesViewModel() { client_id = client_id, });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult SalesAdd(SalesViewModel sales_add)
        {            
            if (ModelState.IsValid)
            {
                var res = salesService.Insert(sales_add, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении торговой точки");
                    return RedirectToErrorPage(ModelState);
                }

                return RedirectToAction("ClientEdit", "Cabinet", new { client_id = ((SalesViewModel)res).client_id });                
            }
            return View(sales_add);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult SalesEdit(int sales_id)
        {
            var sales = salesService.GetItem(sales_id);
            if (sales == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найдена точка с кодом " + sales_id.ToString());
                return RedirectToAction("Oops", "Home");
            }            
            return View(sales);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult SalesEdit(SalesViewModel sales_edit)
        {            
            if (ModelState.IsValid)
            {
                var res = salesService.Update(sales_edit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                //return View(res == null ? sales_edit : res);
                if (!ModelState.IsValid)
                {
                    return RedirectToErrorPage(ModelState);
                }
                return RedirectToAction("ClientEdit", "Cabinet", new { client_id = ((SalesViewModel)res).client_id });
            }
            return View(sales_edit);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult SalesDel([DataSourceRequest]DataSourceRequest request, SalesViewModel sales)
        {
            if (ModelState.IsValid)
            {
                salesService.Delete(sales, ModelState);
            }

            return Json(new[] { sales }.ToDataSourceResult(request, ModelState));
        }

        //[SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetSalesList_Sprav([DataSourceRequest]DataSourceRequest request, int? client_id)
        {            
            return Json(salesService.GetList_Sprav(client_id.GetValueOrDefault(0)), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSalesList_Cva([DataSourceRequest]DataSourceRequest request, int? client_id)
        {            
            return Json(salesService.GetList_Cva(client_id.GetValueOrDefault(0)), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSalesList_Stock([DataSourceRequest]DataSourceRequest request, int? client_id)
        {
            return Json(salesService.GetList_Stock(client_id.GetValueOrDefault(0)), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public JsonResult SalesSearch(int client_id, string workplace, string key)
        {
            if (ModelState.IsValid)
            {
                var res = salesService.Find(client_id, workplace, key);
                if ((!res.Item1) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Рабочее место не найдено");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "", sales_id = (int)res.Item2, workplace_id = (int)res.Item3 });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

    }
}

