﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult OrderList(int? client_id)
        {
            ViewBag.ParentClientId = client_id;
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetOrderList([DataSourceRequest]DataSourceRequest request, int client_id, bool? is_active_only)
        {
            if (is_active_only.GetValueOrDefault(false))
                return Json(orderService.GetList_Active(client_id).ToDataSourceResult(request));
            else
                return Json(orderService.GetList_byParent(client_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetOrderActiveList([DataSourceRequest]DataSourceRequest request, int client_id)
        {
            return Json(orderService.GetList_Active(client_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetOrderUnactiveList([DataSourceRequest]DataSourceRequest request, int client_id)
        {
            return Json(orderService.GetList_Unactive(client_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetOrderListFull([DataSourceRequest]DataSourceRequest request, int? client_id)
        {
            if (client_id.GetValueOrDefault(0) > 0)
                return Json(orderService.GetList_byParent((int)client_id).ToDataSourceResult(request));
            else
                return Json(orderService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetOrderItemList([DataSourceRequest]DataSourceRequest request, int order_id)
        {
            return Json(orderItemService.GetList_byParent(order_id).ToDataSourceResult(request));
        }

        //[SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult OrderAdd(int client_id)
        {            
            OrderViewModel order_add = new OrderViewModel() { client_id = client_id, };
            order_add.ServiceList = serviceService.GetList().AsEnumerable().Cast<ServiceViewModel>();
            ViewData["versionList"] = versionService.GetList_Sprav();
            ViewBag.ServiceKitList = serviceKitService.GetList();
            return View(order_add);
        }

        //[SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public JsonResult OrderAdd(IEnumerable<ServiceViewModel> serv_data, int client_id, string comment, string comment_manager, bool activate)
        {
            OrderViewModel order_data = new OrderViewModel() { client_id = client_id, commentary = comment, commentary_manager = comment_manager, NeedActivate = activate, };
            order_data.ServiceList = serv_data;
            
            if (ModelState.IsValid)
            {
                OrderViewModel res = (OrderViewModel)orderService.Insert(order_data, ModelState);                
                if ((!ModelState.IsValid) || (res == null))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошбика при оформлении заявки");                    
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }                
                return Json(new { err = false });
            }            
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]        
        [HttpGet]
        public ActionResult OrderEdit(int order_id)
        {
            var order = orderService.GetItem(order_id);
            if (order == null)
            {
                ModelState.AddModelError("", "Не найдена заявка с кодом " + order_id.ToString());
                return RedirectToErrorPage(ModelState);
            }
            IEnumerable<OrderItemViewModel> order_items = orderItemService.GetList_byParent(order_id).AsEnumerable().Cast<OrderItemViewModel>();
            ViewBag.OrderItemList = order_items;
            return View(order);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult OrderEdit(OrderViewModel order_edit)
        {            
            if (ModelState.IsValid)
            {
                var res = orderService.Update(order_edit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                if ((!ModelState.IsValid) || (res == null))
                {
                    return RedirectToErrorPage(ModelState);
                }

                ViewBag.OrderItemList = orderItemService.GetList_byParent(order_edit.id).AsEnumerable().Cast<OrderItemViewModel>();
                return View(res);
            }
            return View(order_edit);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult OrderConfirm(int order_id)
        {            
            if (ModelState.IsValid)
            {
                var res = orderService.Confirm(order_id, ModelState);
                ViewBag.Message = res == null ? "" : "Заявка согласована !";
                if ((!ModelState.IsValid) || (res == null))
                {
                    return RedirectToErrorPage(ModelState);
                }
                
                return Json(order_id, JsonRequestBehavior.AllowGet);
            }
            return Json(order_id, JsonRequestBehavior.AllowGet);            
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult OrderActivate(IEnumerable<OrderItemViewModel> order_data, string commentary, string commentary_manager)
        {
            if (order_data == null)
                return RedirectToErrorPage(ModelState);            
            if (ModelState.IsValid)
            {
                var res = orderService.Activate(order_data, commentary, commentary_manager, ModelState);
                ViewBag.Message = res == null ? "" : "Заявка активирована !";
                if ((!ModelState.IsValid) || (res == null))
                {
                    return RedirectToErrorPage(ModelState);
                }
                
                return Json(order_data, JsonRequestBehavior.AllowGet);
            }
            return Json(new { order_data }, JsonRequestBehavior.AllowGet);            
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult GetAllowedQuantity([DataSourceRequest]DataSourceRequest request, int max_cnt)
        {
            List<QuantityViewModel> QuantityList = new List<QuantityViewModel>();
            int i = 0;
            while (i <= max_cnt)
            {
                QuantityList.Add(new QuantityViewModel() { QuantityId = i, QuantityName = i.ToString(), });
                i++;
            }
            return Json(QuantityList.OrderByDescending(ss => ss.QuantityId), JsonRequestBehavior.AllowGet);
        }

        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public JsonResult ServiceOrder(int service_id)
        {
            List<ServiceViewModel> serv_data = new List<ServiceViewModel>() 
            { 
                new ServiceViewModel() 
                {
                    id = service_id,
                    OrderedLicenseCount = 1,
                    OrderedVersionId = 0,
                }
            };
            OrderViewModel order_data = new OrderViewModel() { client_id = (int?)CurrentUser.CabClientId, commentary = "Заказано клиентом с https://cab.aptekaural.ru", commentary_manager = "", NeedActivate = false, };
            order_data.ServiceList = serv_data;
            var res = orderService.Insert(order_data, ModelState);
            if ((res == null) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка при заказе услуги");
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }

            return Json(new { err = false, mess = "" });
        }

    }
}
