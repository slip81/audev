﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.Extensions;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {
        #region Service

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetServiceList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(serviceService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult ServiceReg(int client_id)
        {
            ViewBag.SalesList = salesService.GetList_byParent(client_id);
            return View(new ClientViewModel() { id = client_id, });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetServiceRegList([DataSourceRequest]DataSourceRequest request, int client_id, int workplace_id)
        {
            return Json(clientServiceService.GetList_RegAndUnreg_byWorkplaceId(client_id, workplace_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public JsonResult ServiceApply(int client_id, int workplace_id, int service_id, int version_id)
        {
            if (ModelState.IsValid)
            {
                var res = clientServiceService.ApplyServiceToWorkplace(client_id, workplace_id, service_id, version_id);
                if (!res.Item1)
                {
                    ModelState.AddModelError("", res.Item2);
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public JsonResult ServiceListApply(int client_id, int? workplace_id, string serviceList)
        {
            List<VersionViewModel> versions = JsonConvert.DeserializeObject<List<VersionViewModel>>(serviceList);
            var res = clientServiceService.ApplyServiceListToWorkplace(client_id, workplace_id, versions);
            if (!res.Item1)
            {
                ModelState.AddModelError("", res.Item2);
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }
            return Json(new { err = false, mess = "" });            
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ServiceRemove(int client_id, int workplace_id, int service_id, int version_id)
        {
            if (ModelState.IsValid)
            {
                var res = clientServiceService.RemoveServiceFromWorkplace(client_id, workplace_id, service_id, version_id);
                if (!res.Item1)
                {
                    ModelState.AddModelError("", res.Item2);
                }
                return Json(new[] { client_id, workplace_id, service_id, version_id });
            }
            else
            {
                return RedirectToErrorPage(ModelState);
            }
        }

        #endregion

        #region Version

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetVersionList([DataSourceRequest]DataSourceRequest request, int service_id)
        {            
            return Json(versionService.GetList_byParent(service_id), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetServiceVersionList([DataSourceRequest]DataSourceRequest request, int service_id)
        {
            return Json(versionService.GetList_byParent(service_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult VersionAdd([DataSourceRequest] DataSourceRequest request, VersionViewModel itemAdd, string serviceIdStr)
        {
            if (ModelState.IsValid)
            {
                int service_id = 0;
                if (!int.TryParse(serviceIdStr, out service_id))
                {
                    ModelState.AddModelError("", "Неверный код услуги: " + serviceIdStr);
                    return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
                }
                itemAdd.service_id = service_id;
                var res = versionService.Insert(itemAdd, ModelState);
                return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult VersionEdit([DataSourceRequest] DataSourceRequest request, VersionViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                var res = versionService.Update(itemEdit, ModelState);
                return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult VersionDel([DataSourceRequest]DataSourceRequest request, VersionViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                versionService.Delete(itemDel, ModelState);
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion
                
    }
}
