﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {

        #region DeliveryTemplate

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        public ActionResult GetDeliveryTemplateList([DataSourceRequest]DataSourceRequest request)
        {            
            return Json(deliveryTemplateService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        [HttpGet]
        public ActionResult DeliveryTemplateList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        [HttpGet]
        public ActionResult GetDeliveryTemplateGroupList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(deliveryTemplateGroupService.GetList(), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        [HttpGet]
        public ActionResult DeliveryTemplateEdit(int? template_id)
        {            
            if (template_id.HasValue)
            {
                var res = deliveryTemplateService.GetItem((int)template_id);
                if (res == null)
                {
                    ModelState.AddModelError("", "Не найден шаблон с кодом " + template_id.ToString());
                    return RedirectToErrorPage(ModelState);
                }
                ViewData["defTemplate"] = ((DeliveryTemplateViewModel)res).group_id;
                return View(res);
            }
            else
            {
                ViewData["defTemplate"] = null;
                return View(new DeliveryTemplateViewModel());
            }
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        [HttpPost]
        public JsonResult DeliveryTemplateEdit(DeliveryTemplateViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel res = null;
                if (itemEdit.template_id > 0)
                {
                    res = deliveryTemplateService.Update(itemEdit, ModelState);
                }
                else
                {
                    res = deliveryTemplateService.Insert(itemEdit, ModelState);
                }

                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании шаблона");
                    return Json(new { err = true, mess = ModelState.GetErrorsString(), });
                }
                return Json(new { err = false, id = ((DeliveryTemplateViewModel)res).template_id, });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }        

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        [HttpPost]
        public ActionResult DeliveryTemplateDel([DataSourceRequest]DataSourceRequest request, DeliveryTemplateViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                deliveryTemplateService.Delete(itemDel, ModelState);
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        public ActionResult DeliveryTemplateFileSave(IEnumerable<HttpPostedFileBase> uplDeliveryTemplateFile, int template_id)
        {
            // The Name of the Upload component is "uplDeliveryTemplateFile"
            if (uplDeliveryTemplateFile != null)
            {
                DeliveryTemplateFileViewModel itemAdd = null;
                foreach (var file in uplDeliveryTemplateFile)
                {
                    itemAdd = new DeliveryTemplateFileViewModel();
                    itemAdd.file_ext = Path.GetExtension(file.FileName);
                    itemAdd.file_name = Path.GetFileNameWithoutExtension(file.FileName);
                    itemAdd.file_size = file.ContentLength;
                    itemAdd.template_id = template_id;

                    var res = deliveryTemplateFileService.UploadFile(itemAdd, file, ModelState);
                    if ((res == null) || (!ModelState.IsValid))
                    {
                        if (ModelState.IsValid)
                            ModelState.AddModelError("", "Ошибка при добавлении файла");
                        return Content(ModelState.GetErrorsString());
                    }
                }                
            }

            // Return an empty string to signify success
            return Content("");
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        public JsonResult DeliveryTemplateFileRemove(string fileNames, int template_id)
        {
            var res = deliveryTemplateFileService.RemoveFile(fileNames, template_id, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка при удалении файла");
                return Json(new { err = true, mess = ModelState.GetErrorsString(), });
            }
            
            return Json(new { err = false, mess = "", });
        }

        #endregion

        #region Delivery

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        public ActionResult GetDeliveryList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(deliveryService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        [HttpGet]
        public ActionResult DeliveryList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        [HttpGet]
        public ActionResult DeliveryDetail(int delivery_id)
        {
            var res = deliveryService.GetItem(delivery_id);
            if (res == null)
            {
                ModelState.AddModelError("", "Не найдена рассылка с кодом " + delivery_id.ToString());
                return RedirectToErrorPage(ModelState);
            }            
            return View(res);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        [HttpGet]
        public ActionResult DeliveryEdit(int delivery_id)
        {
            var res = deliveryService.GetItem(delivery_id);
            if (res == null)
            {
                ModelState.AddModelError("", "Не найдена рассылка с кодом " + delivery_id.ToString());
                return RedirectToErrorPage(ModelState);
            }
            ViewBag.TemplateList = deliveryTemplateService.GetList();            
            ViewBag.EmptyClientList = deliveryDetailService.GetList_asClientList(delivery_id);
            return View(res);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        [HttpPost]        
        public JsonResult DeliveryEdit(int delivery_id, string descr, int template_id, IEnumerable<ClientViewModel> detailList)
        {
            foreach (var modelValue in ModelState.Values)
            {
                modelValue.Errors.Clear();
            }

            if (ModelState.IsValid)
            {
                DeliveryViewModel itemEdit = (DeliveryViewModel)deliveryService.GetItem(delivery_id);
                itemEdit.descr = descr;
                itemEdit.template_id = template_id;
                itemEdit.ClientList = detailList;
                var res = deliveryService.Update(itemEdit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании рассылки");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });

            /*
            if (ModelState.IsValid)
            {
                var res = deliveryService.Update(itemEdit, ModelState);                
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании рассылки");
                    return View(itemEdit);
                }
                return RedirectToAction("DeliveryList", "Cabinet");
            }
            return View(itemEdit);
            */
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        [HttpGet]
        public ActionResult DeliveryAdd()
        {
            ViewBag.TemplateList = deliveryTemplateService.GetList();
            ViewBag.EmptyClientList = new List<ClientViewModel>() { };
            return View(new DeliveryViewModel());
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        [HttpPost]
        public JsonResult DeliveryAdd(string descr, int template_id, IEnumerable<ClientViewModel> detailList, bool andStart)
        {
            //if (ModelState.IsValid)

            foreach (var modelValue in ModelState.Values)
            {
                modelValue.Errors.Clear();
            }

            if (ModelState.IsValid)
            {
                DeliveryViewModel itemAdd = new DeliveryViewModel();
                itemAdd.descr = descr;
                itemAdd.template_id = template_id;
                itemAdd.ClientList = detailList;
                itemAdd.andStart = andStart;
                itemAdd.num = 0;
                var res = deliveryService.Insert(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении рассылки");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }        

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        [HttpPost]
        public ActionResult DeliveryDel([DataSourceRequest]DataSourceRequest request, DeliveryViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                deliveryService.Delete(itemDel, ModelState);
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        [HttpPost]
        public JsonResult StartDelivery(int delivery_id)
        {
            if (ModelState.IsValid)
            {
                var res = deliveryService.StartDelivery(delivery_id, ModelState);
                if (!res)
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при запуске рассылки " + delivery_id.ToString());
                    return Json(new { err = true, mess = ModelState.GetErrorsString(), });
                }
                return Json(new { err = false, mess = "", });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.DELIVERY)]
        [HttpPost]
        public JsonResult StopDelivery(int delivery_id)
        {
            if (ModelState.IsValid)
            {
                var res = deliveryService.StopDelivery(delivery_id, ModelState);
                if (!res)
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при остановке рассылки " + delivery_id.ToString());
                    return Json(new { err = true, mess = ModelState.GetErrorsString(), });
                }
                return Json(new { err = false, mess = "", });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString(), });
        }  

        #endregion

        #region DeliveryDetail

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.DELIVERY)]
        public ActionResult GetDeliveryDetailList([DataSourceRequest]DataSourceRequest request, int delivery_id)
        {
            return Json(deliveryDetailService.GetList_byParent(delivery_id).ToDataSourceResult(request));
        }

        #endregion
    }
}
