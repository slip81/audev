﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetSprav_Region([DataSourceRequest]DataSourceRequest request)
        {
            return Json(spravRegionService.GetList(), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetSprav_City([DataSourceRequest]DataSourceRequest request, int region_id)
        {
            return Json(spravCityService.GetList_byParent(region_id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSprav_District([DataSourceRequest]DataSourceRequest request, int city_id)
        {
            return Json(spravDistrictService.GetList_byParent(city_id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSprav_Street([DataSourceRequest]DataSourceRequest request, int district_id)
        {
            return Json(spravStreetService.GetList_byParent(district_id), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetSprav_RegionZone(int? region_id)
        {
            return Json(spravRegionZoneService.GetList_byParent(region_id.GetValueOrDefault(0)), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetSprav_TaxSystemType()
        {
            return Json(spravTaxSystemTypeService.GetList(), JsonRequestBehavior.AllowGet);
        }
    }
}
