﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {
        #region Workplace

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetWorkplaceList([DataSourceRequest]DataSourceRequest request, int sales_id)
        {
            return Json(workplaceService.GetList_byParent(sales_id).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult WorkplaceAdd([DataSourceRequest] DataSourceRequest request, WorkplaceViewModel wpAdd, string salesIdStr)
        {
            if (ModelState.IsValid)
            {                            
                int sales_id = 0;
                if (!int.TryParse(salesIdStr, out sales_id))
                {
                    ModelState.AddModelError("", "Неверный код точки: " + salesIdStr);
                    return Json(new[] { wpAdd }.ToDataSourceResult(request, ModelState));
                }
                wpAdd.sales_id = sales_id;
                var res = workplaceService.Insert(wpAdd, ModelState);
                ViewBag.Message = res == null ? "" : "Рабочее место добавлено !";
                return Json(new[] { wpAdd }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { wpAdd }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult WorkplaceEdit([DataSourceRequest] DataSourceRequest request, WorkplaceViewModel wpEdit)
        {
            if (ModelState.IsValid)
            {
                var res = workplaceService.Update(wpEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return Json(new[] { wpEdit }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { wpEdit }.ToDataSourceResult(request, ModelState));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult WorkplaceDel([DataSourceRequest]DataSourceRequest request, WorkplaceViewModel wpDel)
        {
            if (ModelState.IsValid)
            {
                workplaceService.Delete(wpDel, ModelState);
            }

            return Json(new[] { wpDel }.ToDataSourceResult(request, ModelState));
        }

        public ActionResult GetWorkplaceList_bySales([DataSourceRequest]DataSourceRequest request, int? sales_id)
        {
            return Json(workplaceService.GetList_byParent(sales_id.GetValueOrDefault(0)), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region WorkplaceRereg

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult WorkplaceReregList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetWorkplaceReregList([DataSourceRequest]DataSourceRequest request, int? parent_client_id)
        {
            if (parent_client_id.GetValueOrDefault(0) > 0)
                return Json(workplaceReregService.GetList_byParent(parent_client_id.GetValueOrDefault(0)).ToDataSourceResult(request));
            else
                return Json(workplaceReregService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult WorkplaceRereg(int workplace_id)
        {
            var wp = (WorkplaceViewModel)workplaceService.GetItem(workplace_id);
            if (wp == null)
            {
                ModelState.AddModelError("", "Не найдено рабочее место с кодом " + workplace_id.ToString());
                return RedirectToErrorPage(ModelState);
            }
            return View(new WorkplaceReregViewModel() { workplace_id = workplace_id, person = CurrentUser.UserName, description = wp.description, });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult WorkplaceRereg(WorkplaceReregViewModel itemAdd)
        {
            if (ModelState.IsValid)
            {
                var res = workplaceReregService.Insert(itemAdd, ModelState);                
                if (!ModelState.IsValid)
                {
                    //return RedirectToErrorPage(ModelState);
                    return View(itemAdd);
                }
                ViewBag.Message = res == null ? "" : "Заявка добавлена !";
                return RedirectToAction("ClientEdit", "Cabinet", new { client_id = ((WorkplaceReregViewModel)res).client_id });
            }
            return View(itemAdd);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult WorkplaceReregConfirm([DataSourceRequest] DataSourceRequest request, WorkplaceReregViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                var res = workplaceReregService.Update(itemEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region WorkplaceService
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public ActionResult WorkplaceService(int workplace_id, int service_id, int client_id)
        {                    
            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;
            switch (servEnum)
            {
                case Enums.ClientServiceEnum.SENDER_DEFECT:
                case Enums.ClientServiceEnum.SENDER_GRLS:
                    var itemSender = serviceSenderService.GeItem_byService(workplace_id, service_id);
                    if (itemSender == null)
                        return View("ServiceCommon");
                    else
                    {
                        itemSender.ClientId = client_id;
                        return View("ServiceSender", itemSender);
                    }
                case Enums.ClientServiceEnum.CVA:
                    var itemCva = serviceCvaService.GeItem_byService(workplace_id, service_id);
                    if (itemCva == null)
                        return View("ServiceCommon");
                    else
                    {
                        itemCva.ClientId = client_id;
                        return View("ServiceCva", itemCva);
                    }
                case Enums.ClientServiceEnum.DEFECT:
                    var itemDefect = serviceDefectService.GeItem_byService(workplace_id, service_id);
                    if (itemDefect == null)
                        return View("ServiceCommon");
                    else
                    {
                        itemDefect.ClientId = client_id;
                        return View("ServiceDefect", itemDefect);
                    }
                case Enums.ClientServiceEnum.STOCK:
                    var itemStock = serviceStockService.GeItem_byService(workplace_id, service_id);
                    if (itemStock == null)
                        return View("ServiceCommon");
                    else
                    {
                        itemStock.ClientId = client_id;
                        return View("ServiceStock", itemStock);
                    }
                case Enums.ClientServiceEnum.ASNA:
                    var itemAsna = asnaUserService.GetItem_byWorkplace(workplace_id);
                    int asna_user_id = itemAsna != null ? itemAsna.asna_user_id : 0;
                    return RedirectToAction("AsnaUserEdit", "Asna", new { @asna_user_id = asna_user_id });
                case Enums.ClientServiceEnum.UND:
                    ViewData["clientId"] = client_id;
                    ViewData["workplaceId"] = workplace_id;
                    return View("ServiceEsn2");
                default:
                    return View("ServiceCommon");
            }

        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ServiceSender(ServiceSenderViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = serviceSenderService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании параметров услуги");
                    return RedirectToErrorPage(ModelState);
                }

                ViewBag.Message = "Изменения успешно сохранены !";
                ((ServiceSenderViewModel)res).ClientId = item.ClientId;
                return View(res);
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetWorkplaceServiceLogList([DataSourceRequest]DataSourceRequest request, int workplace_id, int service_id)
        {

            return Json(serviceSenderLogService.GeList_byService(workplace_id, service_id).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ServiceCva(ServiceCvaViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = serviceCvaService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании параметров услуги");
                    return RedirectToErrorPage(ModelState);
                }

                ViewBag.Message = "Изменения успешно сохранены !";
                ((ServiceCvaViewModel)res).ClientId = item.ClientId;
                return View(res);
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        [HttpGet]
        public FileResult ServiceCvaDoc()
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\Release\Exchange\Doc\Описание формата ЦеныВАптеках JSON v2_2.doc");
            string fileName = "Описание формата ЦеныВАптеках JSON v2_2.doc";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ServiceDefect(ServiceDefectViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = serviceDefectService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании параметров услуги");
                    return RedirectToErrorPage(ModelState);
                }

                ViewBag.Message = "Изменения успешно сохранены !";
                ((ServiceDefectViewModel)res).ClientId = item.ClientId;
                return View(res);
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ServiceStock(ServiceStockViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = serviceStockService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании параметров услуги");
                    return RedirectToErrorPage(ModelState);
                }

                ViewBag.Message = "Изменения успешно сохранены !";
                ((ServiceStockViewModel)res).ClientId = item.ClientId;
                return View(res);
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.CLIENT)]
        public ActionResult GetServiceEsn2List([DataSourceRequest]DataSourceRequest request, int clientId, int workplaceId)
        {            
            return Json(serviceEsn2Service.GeItems_byService(workplaceId, (int)Enums.ClientServiceEnum.UND).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.CLIENT)]
        [HttpPost]
        public ActionResult ServiceEsn2Add([DataSourceRequest] DataSourceRequest request, ServiceEsn2ViewModel itemAdd, int clientId, int workplaceId)
        {
            if (ModelState.IsValid)
            {
                itemAdd.workplace_id = workplaceId;
                itemAdd.service_id = (int)Enums.ClientServiceEnum.UND;
                var res = serviceEsn2Service.Insert(itemAdd, ModelState);
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult ServiceEsn2Del([DataSourceRequest]DataSourceRequest request, ServiceEsn2ViewModel itemDel, int clientId, int workplaceId)
        {
            if (ModelState.IsValid)
            {
                itemDel.workplace_id = workplaceId;
                itemDel.service_id = (int)Enums.ClientServiceEnum.UND;
                serviceEsn2Service.Delete(itemDel, ModelState);
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion 
    }
}
