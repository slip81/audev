﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using UnidecodeSharpFork;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.User;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CabinetController
    {
        #region Reg

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.REG)]
        public ActionResult GetRegActiveList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(regService.GetList_Active().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.REG)]
        public ActionResult GetRegList([DataSourceRequest]DataSourceRequest request, int scope)
        {
            return Json(regService.GetList(scope).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.REG)]
        [HttpGet]
        public ActionResult RegList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.REG)]
        [HttpGet]
        public ActionResult RegEdit(int reg_id)
        {
            var res = regService.GetItem(reg_id);
            if ((res == null) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Не найдена заявка с кодом " + reg_id.ToString());
                return RedirectToErrorPage(ModelState);                
            }
            res.login = res.org_name.Unidecode().Replace(" ", "_");
            return View(res);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.REG)]
        [HttpPost]
        public ActionResult RegEdit(RegViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                var res = regService.Update(itemEdit, ModelState);
                if ((res < 0) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при активации пользователя");
                    return View(itemEdit);
                }

                if (res == 0)
                {
                    return RedirectToAction("ClientList", "Cabinet");
                }
                else
                {
                    return RedirectToAction("ClientEdit", "Cabinet", new { client_id = res });
                }
            }
            return View(itemEdit);
        }

        #endregion
    }
}
