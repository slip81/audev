﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel.Defect;
using CabinetMvc.ViewModel.User;

namespace CabinetMvc.Controllers
{    
    public partial class HomeController : BaseController
    {        

        public ActionResult Index()
        {
            //logger.Info("Index start");

            if ((CurrentUser == null) || ((CurrentUser.BusinessId <= 0) && (CurrentUser.CabClientId <= 0)))
            {                 
                return RedirectToAction("Login", "User");
            }

            //logger.Info("Index end");

            var profile = userProfileService.GetItem_byUserName(CurrentUser.UserName);
            if ((profile != null) && (!String.IsNullOrEmpty(profile.start_page)))
            {
                return RedirectToAction(profile.start_page);
            }

            return RedirectToAction("Main");
        }

        public ActionResult Main()
        {
            ViewBag.AvailableWidgetList = clientUserWidgetService.GetList_Avialable();
            ViewBag.InstalledWidgetList = clientUserWidgetService.GetList_Installed();
            return View();
        }

        public ActionResult Oops()
        {
            return View();
        }
                
        public JsonResult GetSelectListFake([DataSourceRequest]DataSourceRequest request)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            return Json(lst.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

    }


}
