﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel.Defect;
using CabinetMvc.ViewModel.User;

namespace CabinetMvc.Controllers
{    
    public partial class HomeController : BaseController
    {        
        [AllowAnonymous]
        [HttpPost]
        [JsonContentType(Parameter = "food", JsonDataType = typeof(Food))]
        public JsonResult Test_CreateFood(Food food)
        {
            return Json(new { err = false, mess = "ok" });
        }

        [AllowAnonymous]
        [HttpGet]
        [JsonContentType()]
        public JsonResult Test_GetAdminList()
        {
            var res = cabUserService.GetList_Actual();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
    }

    public class Food
    {
        public Food()
        {
            //
        }

        public string name { get; set; }
    }
}
