﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel.Defect;
using CabinetMvc.ViewModel.User;

namespace CabinetMvc.Controllers
{    
    public partial class HomeController
    {        
        public JsonResult SetWidget(string id, int? oldIndex, int? newIndex)
        {
            var res = clientUserWidgetService.SetWidget(id, oldIndex, newIndex);            
            return Json(new {err = !res, mess = (!res ? "Ошибка при установке виджета" : "")});
        }

        public JsonResult UpdateWidget(string id, string param)
        {
            var res = clientUserWidgetService.UpdateWidget(id, param);
            return Json(new { err = !res, mess = (!res ? "Ошибка при обновлении параметров виджета" : "") });
        }
    }
}
