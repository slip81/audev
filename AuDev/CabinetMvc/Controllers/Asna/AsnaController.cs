﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.User;
using CabinetMvc.ViewModel.Asna;
using CabinetMvc.ViewModel.Wa;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class AsnaController : BaseController
    {
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult Index()
        {
            return View();
        }

        #region AsnaUser

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult AsnaUserList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaUserList([DataSourceRequest]DataSourceRequest request, int? client_id)
        {
            if (client_id.HasValue)
            {
                return Json(asnaUserService.GetList_byParent((int)client_id).ToDataSourceResult(request));
            }
            else
            {
                return Json(asnaUserService.GetList().ToDataSourceResult(request));
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaClientList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(clientService.GetList_Asna(), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaSalesList([DataSourceRequest]DataSourceRequest request, int? client_id)
        {
            return Json(salesService.GetList_Asna(client_id), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        [HttpGet]
        public ActionResult AsnaUserEdit(int? asna_user_id)
        {
            ViewData["ScheduleTimeTypeList"] = asnaScheduleTimeTypeService.GetList();

            List<AsnaScheduleIntervalTypeViewModel> scheduleIntervalTypeList = asnaScheduleIntervalTypeService.GetList().AsEnumerable().Cast<AsnaScheduleIntervalTypeViewModel>().ToList();
            ViewData["ScheduleChangeIntervalTypeList"] = scheduleIntervalTypeList;
            ViewData["ScheduleFullIntervalTypeList"] = scheduleIntervalTypeList.Where(ss => ss.interval_type_id >= 15);

            ViewData["WaRequestTypeList"] = waRequestTypeService.GetList_byGroups((int)Enums.WaRequestTypeGroup1Enum.ASNA, (int)Enums.WaRequestTypeGroup2Enum.ASNA_SendTo, false, true);
            if (asna_user_id.GetValueOrDefault(0) > 0)
            {
                var res = asnaUserService.GetItem((int)asna_user_id);
                if (res == null)
                {
                    ModelState.AddModelError("", "Не найден пользователь с кодом " + asna_user_id.ToString());
                    return RedirectToErrorPage(ModelState);
                }
                return View(res);
            }
            else
            {
                return View(new AsnaUserViewModel()
                {
                    is_active = true,
                    sch_full_is_active = true,
                    sch_ch_is_active = true,
                });
            }
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ASNA_USER)]
        [HttpPost]
        public ActionResult AsnaUserEdit(AsnaUserViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel res = null;
                bool isAddMode = itemEdit.asna_user_id <= 0;
                if (!isAddMode)
                {
                    res = asnaUserService.Update(itemEdit, ModelState);
                }
                else
                {
                    res = asnaUserService.Insert(itemEdit, ModelState);
                }

                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании пользователя");
                    return RedirectToErrorPage(ModelState);
                }
                if (isAddMode)
                    return RedirectToAction("AsnaUserEdit", "Asna", new { @asna_user_id = ((AsnaUserViewModel)res).asna_user_id });
                else
                    return RedirectToAction("AsnaUserList", "Asna");
            }
            return RedirectToErrorPage(ModelState);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ASNA_USER)]
        [HttpPost]
        public ActionResult AsnaUserDel([DataSourceRequest]DataSourceRequest request, AsnaUserViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                var res = asnaUserService.Delete(itemDel, ModelState);
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        [HttpGet]
        public ActionResult AsnaSalesEdit(int? sales_id)
        {
            AsnaUserViewModel asnaUser = asnaUserService.GetItem_bySales(sales_id.GetValueOrDefault(0));
            return RedirectToAction("AsnaUserEdit", "Asna", new { @asna_user_id = asnaUser != null ? asnaUser.asna_user_id : 0 });
        }

        #endregion

        #region AsnaTask

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult AsnaTaskList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaTaskList([DataSourceRequest]DataSourceRequest request, int? workplace_id)
        {
            return Json(waTaskService.GetList_byWorkplace(workplace_id.GetValueOrDefault(0), (int)Enums.WaRequestTypeGroup1Enum.ASNA).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaTaskList_bySales([DataSourceRequest]DataSourceRequest request, int? sales_id, string task_state_id_list)
        {
            return Json(waTaskService.GetList_bySales(sales_id.GetValueOrDefault(0), (int)Enums.WaRequestTypeGroup1Enum.ASNA, task_state_id_list).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ASNA_USER)]
        [HttpPost]
        public JsonResult AsnaTaskAdd(int asna_user_id, int request_type_id)
        {
            if (ModelState.IsValid)
            {
                var res = asnaUserService.InsertTask(asna_user_id, request_type_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении задания");
                    return Json(new { err = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ASNA_USER)]
        [HttpPost]
        public JsonResult AsnaTaskEdit(int asna_user_id, int task_id, int request_type_id)
        {
            if (ModelState.IsValid)
            {
                var res = asnaUserService.UpdateTask(asna_user_id, task_id, request_type_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении задания");
                    return Json(new { err = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ASNA_USER)]
        [HttpPost]
        public JsonResult AsnaTaskDel(int asna_user_id, int task_id)
        {
            if (ModelState.IsValid)
            {
                var res = asnaUserService.CancelTask(asna_user_id, task_id, ModelState);                
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении задания");
                    return Json(new { err = ModelState.GetErrorsString() });
                }
                return Json(new { err = false });
            }
            return Json(new { err = ModelState.GetErrorsString() });
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaTaskLcList([DataSourceRequest]DataSourceRequest request, int? task_id)
        {
            return Json(waTaskLcService.GetList_byParent(task_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ASNA_USER)]
        [HttpPost]
        public JsonResult AsnaTaskCheckState(int user_id, int task_id)
        {
            if (ModelState.IsValid)
            {
                string res = waTaskService.CheckState(user_id, task_id, ModelState);
                if (!ModelState.IsValid)
                {
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = res });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        #endregion

        #region AsnaUserLog

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaUserLogList([DataSourceRequest]DataSourceRequest request, int? asna_user_id)
        {
            if (asna_user_id.HasValue)
            {
                return Json(asnaUserLogService.GetList_byParent((int)asna_user_id).ToDataSourceResult(request));
            }
            else
            {
                return Json(asnaUserLogService.GetList().ToDataSourceResult(request));
            }
        }

        #endregion

        #region AsnaLink

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_LINK)]
        public ActionResult AsnaLinkList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_LINK)]
        public ActionResult GetAsnaLinkList([DataSourceRequest]DataSourceRequest request, int? sales_id)
        {
            return Json(asnaLinkService.GetList_bySales(sales_id).ToDataSourceResult(request));
        }

        #endregion

        #region AsnaLog

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult AsnaLogList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaLogList([DataSourceRequest]DataSourceRequest request, int? client_id, int? sales_id)
        {
            return Json(asnaUserLogService.GetList_bySales(client_id, sales_id).ToDataSourceResult(request));
        }

        #endregion

        #region AsnaStock

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_LINK)]
        public ActionResult AsnaStockList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_LINK)]
        public ActionResult GetAsnaStockRowList([DataSourceRequest]DataSourceRequest request, int? chain_id)
        {
            return Json(asnaStockRowService.GetList_byParent(chain_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_LINK)]
        public ActionResult GetAsnaStockChainList([DataSourceRequest]DataSourceRequest request, int? sales_id, string chain_date)
        {
            DateTime? chain_date_dt = String.IsNullOrWhiteSpace(chain_date) ? null : chain_date.ToDateTimeStrict("dd.MM.yyyy");
            DateTime chain_date_actual = chain_date_dt.HasValue ? (DateTime)chain_date_dt : DateTime.Today;
            return Json(asnaStockChainService.GetList_byDate(sales_id.GetValueOrDefault(0), chain_date_actual), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_LINK)]
        public ActionResult GetAsnaStockChainDates([DataSourceRequest]DataSourceRequest request, int? sales_id)
        {
            return Json(new { dates = asnaStockChainService.GetChainDates(sales_id.GetValueOrDefault(0)) });            
        }

        #endregion

        #region AsnaActual

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_LINK)]
        public ActionResult AsnaActualList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_LINK)]
        public ActionResult GetAsnaActualList([DataSourceRequest]DataSourceRequest request, int? sales_id)
        {
            return Json(asnaStockRowActualService.GetList_byParent(sales_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        #endregion

        #region AsnaOrderStateType

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult AsnaOrderStateType()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaOrderStateTypeList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(asnaOrderStateTypeService.GetList().ToDataSourceResult(request));
        }

        #endregion

        #region AsnaOrder

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult AsnaOrderList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaOrderList([DataSourceRequest]DataSourceRequest request, int? sales_id)
        {
            return Json(asnaOrderService.GetList_byParent(sales_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaOrderRowList([DataSourceRequest]DataSourceRequest request, int? order_id)
        {
            return Json(asnaOrderRowService.GetList_byParent(order_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaOrderStateList([DataSourceRequest]DataSourceRequest request, int? order_id)
        {
            return Json(asnaOrderStateService.GetList_byParent(order_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ASNA_USER)]
        public ActionResult GetAsnaOrderRowStateList([DataSourceRequest]DataSourceRequest request, int? row_id)
        {
            return Json(asnaOrderRowStateService.GetList_byParent(row_id.GetValueOrDefault(0)).ToDataSourceResult(request));
        }

        #endregion
    }
}