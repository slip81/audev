﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel.Auth;
using CabinetMvc.ViewModel.User;
using CabinetMvc.Auth;
using CabinetMvc.Extensions;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class AuthController : BaseController
    {

        #region Auth

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        public ActionResult AuthList()
        {            
            return View();
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        public ActionResult GetAuthList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(authUserRoleService.GetList_byParent((int)Enums.AuthGroupEnum.PERS).ToDataSourceResult(request));            
        }

        #endregion

        #region AuthRole

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_ROLE)]
        public ActionResult AuthRoleList()
        {
            return View();
        }
        
        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_ROLE)]
        public ActionResult GetAuthRoleList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(authRoleService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ADM_ROLE)]
        [HttpPost]
        public ActionResult AuthRoleAdd([DataSourceRequest]DataSourceRequest request, AuthRoleViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = authRoleService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении роли");
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ADM_ROLE)]
        [HttpPost]
        public ActionResult AuthRoleEdit([DataSourceRequest]DataSourceRequest request, AuthRoleViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = authRoleService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при изменении роли");
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ADM_ROLE)]
        [HttpPost]
        public ActionResult AuthRoleDel([DataSourceRequest]DataSourceRequest request, AuthRoleViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                var res = authRoleService.Delete(item, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении роли");
                }
                return Json(new[] { item }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region AuthRolePerm

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_ROLE)]
        [HttpGet]
        public ActionResult AuthRolePermEdit(int role_id)
        {
            var model = authRoleService.GetItem(role_id);
            return View(model);
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ADM_ROLE)]
        [HttpPost]
        public JsonResult AuthRolePermEdit(List<CabinetMvc.ViewModel.Auth.AuthRolePermViewModel>[] permList, int role_id)
        {
            if (ModelState.IsValid)
            {
                var res = authRolePermService.ApplyChanges(permList, role_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении полномочий");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_ROLE)]
        public ActionResult GetAuthRoleSectionList([DataSourceRequest]DataSourceRequest request, int role_id, int group_id)
        {

            return Json(authRolePermService.GetSectionList_byRole(role_id, group_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_ROLE)]
        public ActionResult GetAuthRolePartList([DataSourceRequest]DataSourceRequest request, int role_id, int section_id, int group_id)
        {
            return Json(authRolePermService.GetPartList_byRoleAndSection(role_id, section_id, group_id).ToDataSourceResult(request));
        }

        #endregion

        #region AuthUserPerm

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        [HttpGet]
        public ActionResult AuthUserPermEdit(int user_id)
        {
            var model = authUserRoleService.GetItem(user_id);
            ViewBag.AuthRoleList = authRoleService.GetList_byGroup((int)Enums.AuthGroupEnum.PERS);
            return View(model);
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.ADM_USER)]
        [HttpPost]
        public JsonResult AuthUserPermEdit(List<CabinetMvc.ViewModel.Auth.AuthUserPermViewModel>[] permList, int role_id, int user_id)
        {
            if (ModelState.IsValid)
            {
                var res = authUserPermService.ApplyChanges(permList, role_id, user_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении полномочий");
                    return Json(new { err = true, mess = ModelState.GetErrorsString() });
                }
                return Json(new { err = false, mess = "" });
            }
            return Json(new { err = true, mess = ModelState.GetErrorsString() });
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        public ActionResult GetAuthUserRoleSectionList([DataSourceRequest]DataSourceRequest request, int user_id, int role_id, int group_id)
        {

            return Json(authUserPermService.GetSectionList_byUser(user_id, role_id, group_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        public ActionResult GetAuthUserRolePartList([DataSourceRequest]DataSourceRequest request, int user_id, int section_id, int role_id, int group_id)
        {
            return Json(authUserPermService.GetPartList_byUserAndSection(user_id, section_id, role_id, group_id).ToDataSourceResult(request));
        }

        #endregion

        #region AuthClient

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        public ActionResult AuthClientList()
        {
            return View();
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.ADM_USER)]
        public ActionResult GetAuthClientList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(authUserRoleService.GetList_byParent((int)Enums.AuthGroupEnum.CLIENTS).ToDataSourceResult(request));
        }

        #endregion

    }
}
