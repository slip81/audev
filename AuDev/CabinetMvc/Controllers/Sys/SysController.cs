﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Sys;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.Extensions;
using AuDev.Common.Util;

namespace CabinetMvc.Controllers
{
    public partial class SysController : BaseController
    {
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult Index()
        {
            return View();
        }

        #region CabGridColumnUserSettings

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public JsonResult CabGridColumnUserSettings(IEnumerable<CabGridColumnUserSettingsViewModel> settings, int? color_column_id)        
        {
            if (ModelState.IsValid)
            {
                var res = cabGridColumnUserSettingsService.UpdateSettings(settings, color_column_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {                    
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при обновлении конфигурации списка");
                    TempData["error"] = ModelState.GetErrorsString();
                    
                    //return View("TaskList");                   
                    return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                }                
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["error"] = ModelState.GetErrorsString();                
                return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region CabGridUserSettings

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public JsonResult CabGridUserSettings(int grid_id, string sort_options, string filter_options, int? page_size, string columns_options)
        {
            if (ModelState.IsValid)
            {
                
                var col_deser = JsonConvert.DeserializeObject<List<KendoGridColumn>>(columns_options);
                string columns_options_ser = JsonConvert.SerializeObject(col_deser);

                //
                var res = cabGridUserSettingService.UpdateSettings(grid_id, sort_options, filter_options, page_size, columns_options_ser);
                //var res = cabGridUserSettingService.UpdateSettings(grid_id, sort_options, filter_options, page_size, columns_options);
                //var res = cabGridUserSettingService.UpdateSettings(grid_id, sort_options, filter_options, page_size, col_deser);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при обновлении конфигурации списка");
                    TempData["error"] = ModelState.GetErrorsString();

                    //return View("TaskList");                   
                    return Json(new { error = true, mess = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { error = false, mess = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["error"] = ModelState.GetErrorsString();
                return Json(new { error = true, mess = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region GridConfig

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult GridConfigSave(int grid_id, string data)
        {
            cabGridUserSettingService.SaveGridOptions(grid_id, data);
            return new EmptyResult();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult GridConfigLoad(int grid_id)
        {
            string grid_options = cabGridUserSettingService.GetGridOptions(grid_id);
            return Json(grid_options, JsonRequestBehavior.AllowGet);
        }
        #endregion
        
    }
}
