﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

//using Ninject;
using CabinetMvc.Auth;
using CabinetMvc.ViewModel.User;

namespace CabinetMvc.Controllers
{
    public class SysRoleAuthorizeAttribute : AuthorizeAttribute
    {
        /*
        [Inject]
        public IAuthentication Auth { get; set; }
        public UserViewModel CurrentUser { get { return ((IUserProvider)Auth.CurrentUser.Identity).User; } }
        */

        private readonly string[] allowedroles;
        public SysRoleAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;

            //var user = CurrentUser;
            //var user = ((IUserProvider)httpContext.User.Identity).User;
            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var user = auth.CurrentUser == null ? null : ((IUserProvider)auth.CurrentUser.Identity).User;
            
            if (allowedroles == null)
                return authorize;
            if (user == null)
                return authorize;
            if (String.IsNullOrEmpty(user.RoleName))
                return authorize;

            foreach (var role in allowedroles)
            {
                if (user.RoleName.ToString().Trim().ToLower().Equals(role.Trim().ToLower()))
                {
                    authorize = true;
                    break;
                }                    
            }
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }

}