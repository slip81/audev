﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;
using Newtonsoft.Json;
//using Ninject;
using CabinetMvc.Auth;
using CabinetMvc.ViewModel.User;

namespace CabinetMvc.Controllers
{
    public class JsonContentTypeAttribute : ActionFilterAttribute
    {
        public string Parameter { get; set; }
        public Type JsonDataType { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //filterContext.HttpContext.Request.Headers["Content-Type"] = "application/json; charset=UTF-8";
            
            if (filterContext.HttpContext.Request.RequestType == "POST")
            {
                string inputContent;
                using (var sr = new StreamReader(filterContext.HttpContext.Request.InputStream))
                {
                    inputContent = sr.ReadToEnd();
                }

                var result = JsonConvert.DeserializeObject(inputContent, JsonDataType);
                filterContext.ActionParameters[Parameter] = result;
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.HttpContext.Response != null)
                filterContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            
            base.OnActionExecuted(filterContext);
        }

    }

}