﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Exchange;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{    
    public class ExchangeController : BaseController
    {        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        public ActionResult Index()
        {
            return View();
        }

        #region ExchangeClient

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        public ActionResult ExchangeClientList()
        {
            var taskTypeList = exchangeTaskTypeService.GetList();
            var partnerList = exchangePartnerService.GetList();            
            ViewData["TaskTypeList"] = taskTypeList;
            ViewData["TaskTypeDefault"] = taskTypeList.FirstOrDefault();
            ViewData["DsList"] = partnerList;
            ViewData["DsDefault"] = partnerList.FirstOrDefault();
            ViewBag.ClientList = exchangeClientService.GetList();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        public ActionResult GetExchangeClientList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(exchangeClientService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        public ActionResult ExchangeClientAdd()
        {
            return View(new ExchangeClientViewModel());
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeClientAdd(ExchangeClientViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeClientService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении участника");
                    return View(item);
                }
                //return View(res == null ? item : res);
                return RedirectToAction("ExchangeClientList", "Exchange");
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        public ActionResult ExchangeClientEdit(long client_id)
        {
            var client = exchangeClientService.GetItem(client_id);
            if (client == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найден участник обмена с кодом " + client_id.ToString());
                return RedirectToAction("Oops", "Home");
            }
            return View(client);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeClientEdit(ExchangeClientViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeClientService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании участника");
                    return View(item);
                }
                return RedirectToAction("ExchangeClientList", "Exchange");
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeClientDel([DataSourceRequest]DataSourceRequest request, ExchangeClientViewModel item)
        {
            if (ModelState.IsValid)
            {
                exchangeClientService.Delete(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ExchangeUser

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        public ActionResult GetExchangeUserList_byClient([DataSourceRequest]DataSourceRequest request, int? client_id)
        {
            if (client_id.HasValue)
                return Json(exchangeUserService.GetList_byParent((int)client_id).ToDataSourceResult(request));
            else
                return Json(exchangeUserService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        public ActionResult ExchangeUserAdd(long client_id)
        {
            ExchangeClientViewModel exchangeClient = (ExchangeClientViewModel)exchangeClientService.GetItem(client_id);
            if (exchangeClient == null)
            {
                ModelState.AddModelError("", "Не найден участник обмена с кодом " + client_id.ToString());
                return RedirectToErrorPage(ModelState);
            }
            long? cabinet_client_id = exchangeClient.cabinet_client_id;
            ViewBag.SalesList = salesService.GetList_Sprav(cabinet_client_id);
            ViewBag.WorkplaceList = workplaceService.GetList_Sprav(cabinet_client_id);
            return View(new ExchangeUserViewModel() { client_id = client_id });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeUserAdd(ExchangeUserViewModel item)
        {
            ExchangeClientViewModel exchangeClient;
            long? cabinet_client_id;
            if (ModelState.IsValid)
            {
                var res = exchangeUserService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении пользователя");
                    //
                    exchangeClient = (ExchangeClientViewModel)exchangeClientService.GetItem(item.client_id.GetValueOrDefault(0));
                    if (exchangeClient == null)
                    {
                        ModelState.AddModelError("", "Не найден участник обмена с кодом " + item.client_id.GetValueOrDefault(0).ToString());
                        return RedirectToErrorPage(ModelState);
                    }
                    cabinet_client_id = exchangeClient.cabinet_client_id;
                    ViewBag.SalesList = salesService.GetList_Sprav(cabinet_client_id);
                    ViewBag.WorkplaceList = workplaceService.GetList_Sprav(cabinet_client_id);
                    //
                    return View(item);
                }
                //return View(res == null ? item : res);
                return RedirectToAction("ExchangeClientList", "Exchange");
            }
            //
            exchangeClient = (ExchangeClientViewModel)exchangeClientService.GetItem(item.client_id.GetValueOrDefault(0));
            if (exchangeClient == null)
            {
                ModelState.AddModelError("", "Не найден участник обмена с кодом " + item.client_id.GetValueOrDefault(0).ToString());
                return RedirectToErrorPage(ModelState);
            }
            cabinet_client_id = exchangeClient.cabinet_client_id;
            ViewBag.SalesList = salesService.GetList_Sprav(cabinet_client_id);
            ViewBag.WorkplaceList = workplaceService.GetList_Sprav(cabinet_client_id);
            //
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        public ActionResult ExchangeUserEdit(long user_id)
        {
            ExchangeUserViewModel exchangeUser = (ExchangeUserViewModel)exchangeUserService.GetItem(user_id);
            if (exchangeUser == null)
            {
                ModelState.AddModelError("", "Не найден пользователь с кодом " + user_id.ToString());
                return RedirectToErrorPage(ModelState);
            }
            ExchangeClientViewModel exchangeClient = (ExchangeClientViewModel)exchangeClientService.GetItem(exchangeUser.client_id.GetValueOrDefault(0));
            if (exchangeClient == null)
            {
                ModelState.AddModelError("", "Не найден участник обмена с кодом " + exchangeUser.client_id.GetValueOrDefault(0).ToString());
                return RedirectToErrorPage(ModelState);
            }
            long? cabinet_client_id = exchangeClient.cabinet_client_id;
            ViewBag.SalesList = salesService.GetList_Sprav(cabinet_client_id);
            ViewBag.WorkplaceList = workplaceService.GetList_Sprav(cabinet_client_id);
            //
            return View(exchangeUser);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeUserEdit(ExchangeUserViewModel item)
        {
            ExchangeClientViewModel exchangeClient;
            long? cabinet_client_id;
            if (ModelState.IsValid)
            {
                var res = exchangeUserService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении пользователя");
                    //
                    exchangeClient = (ExchangeClientViewModel)exchangeClientService.GetItem(item.client_id.GetValueOrDefault(0));
                    if (exchangeClient == null)
                    {
                        ModelState.AddModelError("", "Не найден участник обмена с кодом " + item.client_id.GetValueOrDefault(0).ToString());
                        return RedirectToErrorPage(ModelState);
                    }
                    cabinet_client_id = exchangeClient.cabinet_client_id;
                    ViewBag.SalesList = salesService.GetList_Sprav(cabinet_client_id);
                    ViewBag.WorkplaceList = workplaceService.GetList_Sprav(cabinet_client_id);
                    //
                    return View(item);
                }
                //return View(res == null ? item : res);
                return RedirectToAction("ExchangeClientList", "Exchange");
            }
            //
            exchangeClient = (ExchangeClientViewModel)exchangeClientService.GetItem(item.client_id.GetValueOrDefault(0));
            if (exchangeClient == null)
            {
                ModelState.AddModelError("", "Не найден участник обмена с кодом " + item.client_id.GetValueOrDefault(0).ToString());
                return RedirectToErrorPage(ModelState);
            }
            cabinet_client_id = exchangeClient.cabinet_client_id;
            ViewBag.SalesList = salesService.GetList_Sprav(cabinet_client_id);
            ViewBag.WorkplaceList = workplaceService.GetList_Sprav(cabinet_client_id);
            //
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeUserDel([DataSourceRequest]DataSourceRequest request, ExchangeUserViewModel item)
        {
            if (ModelState.IsValid)
            {
                exchangeUserService.Delete(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ExchangeRegFromCabClient

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpGet]
        public ActionResult ExchangeRegFromCabClient(int reg_type, int w_id)
        {
            // 1. нужно получить login и workplace, по которому найти exchange_user
            // 2. если exchange_user == null - создаем его (вместе с exchange_client)
            // 3. у этого exchange_user нужно найти exchange_reg_partner или exchange_reg_doc (в зависимости от reg_type)
            // 4. если есть - перейти в редактор услуги, если нет - перейти к добавлению услуги
            
            var vw_workplace = workplaceService.GetItem_VwWorkplace(w_id);
            if (vw_workplace == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найден логин и рабочее место для данной точки");
                return RedirectToAction("Oops", "Home");
            }
            var serv2 = exchangeUserService;
            var exchange_user = serv2.GetItem_byLoginAndWorkplace(vw_workplace.user_name, vw_workplace.registration_key);
            if (exchange_user == null)
            {
                var serv3 = exchangeClientService;
                var exchange_client = serv3.GetItem_byCabinetClientId(vw_workplace.client_id);
                if (exchange_client == null)
                {
                    exchange_client = new ExchangeClientViewModel()
                    {
                        client_name = vw_workplace.client_name,
                        cabinet_client_id = vw_workplace.client_id,
                    }
                    ;
                    var exchange_client_new = serv3.Insert(exchange_client, ModelState);
                    if (exchange_client_new == null)
                    {
                        TempData.Clear();
                        TempData.Add("error", "Не удалось создать Участника обмена для данной точки");
                        return RedirectToAction("Oops", "Home");
                    }

                    exchange_client = (ExchangeClientViewModel)exchange_client_new;
                }

                exchange_user = new ExchangeUserViewModel()
                {
                    login = vw_workplace.user_name,
                    workplace = vw_workplace.registration_key,
                    client_id = exchange_client.client_id,
                }
                ;
                var exchange_user_new = serv2.Insert(exchange_user, ModelState);
                if (exchange_user_new == null)
                {
                    TempData.Clear();
                    TempData.Add("error", "Не удалось создать пользователя для данной точки");
                    return RedirectToAction("Oops", "Home");
                }
                exchange_user = (ExchangeUserViewModel)exchange_user_new;
            }

            IQueryable<AuBaseViewModel> regs = null;
            switch (reg_type)
            {
                case (int)Enums.ExchangeRegType.PARTNER:
                    regs = exchangeRegPartnerService.GetList_byParent(exchange_user.user_id);
                    if ((regs == null) || (regs.Count() <= 0))
                    {
                        return RedirectToAction("ExchangeRegPartnerAdd", "Exchange", new { user_id = exchange_user.user_id });
                    }
                    else
                    {
                        return RedirectToAction("ExchangeRegPartnerEdit", "Exchange", new { item_id = ((ExchangeRegPartnerViewModel)(regs.FirstOrDefault())).item_id });
                    }
                case (int)Enums.ExchangeRegType.DOC:
                    regs = exchangeRegDocService.GetList_byParent(exchange_user.user_id);
                    if ((regs == null) || (regs.Count() <= 0))
                    {
                        return RedirectToAction("ExchangeRegDocAdd", "Exchange", new { user_id = exchange_user.user_id });
                    }
                    else
                    {
                        return RedirectToAction("ExchangeRegDocEdit", "Exchange", new { item_id = ((ExchangeRegDocViewModel)(regs.FirstOrDefault())).item_id });
                    }
                    /*
                    regs = GetService<ExchangeRegDocService>().GetList_byParent(exchange_user.user_id);
                    if ((regs == null) || (regs.Count() <= 0))
                    {
                        return RedirectToAction("ExchangeRegEmailAdd", "Exchange", new { user_id = exchange_user.user_id });
                    }
                    else
                    {
                        return RedirectToAction("ExchangeRegEmailEdit", "Exchange", new { item_id = ((ExchangeRegEmailViewModel)(regs.FirstOrDefault())).item_id });
                    }
                    */
                default:
                    TempData.Clear();
                    TempData.Add("error", "Некорректный тип регистрации " + reg_type.ToString());
                    return RedirectToAction("Oops", "Home");
            }

        }

        #endregion

        #region ExchangeRegPartner

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        public ActionResult GetExchangeRegPartnerList([DataSourceRequest]DataSourceRequest request, long user_id)
        {
            return Json(exchangeRegPartnerService.GetList_byParent(user_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        [HttpGet]
        public ActionResult ExchangeRegPartnerAdd(long user_id)
        {
            ViewBag.ExchangePartnerList = exchangePartnerService.GetList();
            return View(new ExchangeRegPartnerViewModel() { user_id = user_id, });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        [HttpPost]
        public ActionResult ExchangeRegPartnerAdd(ExchangeRegPartnerViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeRegPartnerService.Insert(item, ModelState);
                ViewBag.Message = res == null ? "" : "Регистрация добавлена !";
                //ViewBag.ExchangePartnerList = GetService<ExchangePartnerService>().GetList();
                //return View(res == null ? item : res);
                return RedirectToAction("ExchangeClientList", "Exchange");
            }
            return View(item);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        [HttpGet]
        public ActionResult ExchangeRegPartnerEdit(long item_id)
        {
            var reg = exchangeRegPartnerService.GetItem(item_id);
            if ((!(ModelState.IsValid)) || (reg == null))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("error", "Не найдена регистрация с кодом " + item_id.ToString());
                return RedirectToErrorPage(ModelState);
            }
            ExchangeRegPartnerViewModel reg_edit = (ExchangeRegPartnerViewModel)reg;
            var transport_type = reg_edit.transport_type;
            switch ((Enums.ExchangeTransport)transport_type)
            {
                case Enums.ExchangeTransport.FTP:
                    reg_edit.login_ftp = reg_edit.login_name;
                    reg_edit.password_ftp = reg_edit.password;
                    reg_edit.password_email = "";
                    reg_edit.download_url_ftp = reg_edit.download_url;
                    reg_edit.download_url_email = "";
                    reg_edit.upload_equals_download_email = false;
                    reg_edit.upload_url_ftp = reg_edit.upload_url;
                    reg_edit.upload_url_email = "";
                    reg_edit.upload_url_alias_email = "";
                    reg_edit.pop3_address_email = "";
                    reg_edit.pop3_port_email = null;
                    reg_edit.pop3_use_ssl_email = false;
                    break;
                case Enums.ExchangeTransport.EMAIL:
                    reg_edit.login_ftp = "";
                    reg_edit.password_ftp = "";
                    reg_edit.password_email = reg_edit.password;
                    reg_edit.download_url_ftp = "";
                    reg_edit.download_url_email = reg_edit.download_url;
                    reg_edit.upload_equals_download_email = reg_edit.upload_equals_download;
                    reg_edit.upload_url_ftp = "";
                    reg_edit.upload_url_email = reg_edit.upload_url;
                    reg_edit.upload_url_alias_email = reg_edit.upload_url_alias;
                    reg_edit.pop3_address_email = reg_edit.pop3_address;
                    reg_edit.pop3_port_email = reg_edit.pop3_port;
                    reg_edit.pop3_use_ssl_email = reg_edit.pop3_use_ssl;
                    break;
                default:
                    break;
            }
            ViewBag.ExchangePartnerList = exchangePartnerService.GetList();
            ViewBag.ExchangeItemList = exchangeRegPartnerItemService.GetList_byParent(reg_edit.reg_id);
            return View(reg_edit);
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        [HttpPost]
        public ActionResult ExchangeRegPartnerEdit(ExchangeRegPartnerViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeRegPartnerService.Update(item, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                ViewBag.ExchangePartnerList = exchangePartnerService.GetList();
                ViewBag.ExchangeItemList = exchangeRegPartnerItemService.GetList_byParent(item.reg_id);
                return View(res == null ? item : res);
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        [HttpPost]
        public ActionResult ExchangeRegPartnerDel([DataSourceRequest]DataSourceRequest request, ExchangeRegPartnerViewModel item)
        {
            if (ModelState.IsValid)
            {
                exchangeRegPartnerService.Delete(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ExchangeRegDoc

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetExchangeRegDocList([DataSourceRequest]DataSourceRequest request, long user_id)
        {
            return Json(exchangeRegDocService.GetList_byParent(user_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpGet]
        public ActionResult ExchangeRegDocAdd(long user_id)
        {            
            return View(new ExchangeRegDocViewModel() { user_id = user_id, });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult ExchangeRegDocAdd(ExchangeRegDocViewModel item)
        {
            if (ModelState.IsValid)
            {
                // !!!
                // создаем список ExchangeRegDocSales - список всех торговых точек данной регистрации
                var user = exchangeUserService.GetItem(item.user_id);
                ExchangeUserViewModel user_vm = (ExchangeUserViewModel)user;
                var client = exchangeClientService.GetItem(user_vm.client_id.GetValueOrDefault(0));
                ExchangeClientViewModel client_vm = (ExchangeClientViewModel)client;
                var sales = salesService.GetList_byParent((int)(client_vm.cabinet_client_id.GetValueOrDefault(0)));
                IEnumerable<SalesViewModel> sales_vm = sales.AsEnumerable().Cast<SalesViewModel>();

                var res = exchangeRegDocService.Insert_withSales(item, sales_vm, ModelState);
                ViewBag.Message = res == null ? "" : "Регистрация добавлена !";                
                //return View(res == null ? item : res);
                return RedirectToAction("ExchangeRegDocEdit", "Exchange", new { item_id = ((ExchangeRegDocViewModel)res).item_id });
            }
            return View(item);
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpGet]
        public ActionResult ExchangeRegDocEdit(long item_id)
        {
            var reg = exchangeRegDocService.GetItem(item_id);
            if (reg == null)
            {
                TempData.Clear();
                TempData.Add("error", "Не найдена регистрация с кодом " + item_id.ToString());
                return RedirectToAction("Oops", "Home");
            }            
            return View(reg);
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult ExchangeRegDocEdit(ExchangeRegDocViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeRegDocService.Update(item, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";                
                return View(res == null ? item : res);
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult ExchangeRegDocDel([DataSourceRequest]DataSourceRequest request, ExchangeRegDocViewModel item)
        {
            if (ModelState.IsValid)
            {
                exchangeRegDocService.Delete(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ExchangeRegDocSales

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetExchangeRegDocSalesList([DataSourceRequest] DataSourceRequest request, int reg_id)
        {
            return Json(exchangeRegDocSalesService.GetList_byParent(reg_id).ToDataSourceResult(request));
        }

        [HttpPost]
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult ExchangeRegDocSalesEdit([DataSourceRequest] DataSourceRequest request, ExchangeRegDocSalesViewModel regSalesEdit)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeRegDocSalesService.Update(regSalesEdit, ModelState);
                ViewBag.Message = res == null ? "" : "Изменения успешно сохранены !";
                return Json(new[] { regSalesEdit }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { regSalesEdit }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ExchangePartner

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_PARTNER)]
        public ActionResult ExchangePartnerList([DataSourceRequest]DataSourceRequest request)
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_PARTNER)]
        public ActionResult GetExchangePartnerList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(exchangePartnerService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_PARTNER)]
        public ActionResult GetExchangePartnerList_byClient(long client_id)
        {
            return Json(exchangePartnerService.GetList_byParent(client_id), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_PARTNER)]
        [HttpPost]
        public ActionResult ExchangePartnerAdd([DataSourceRequest]DataSourceRequest request, ExchangePartnerViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                exchangePartnerService.Insert(item, ModelState);
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_PARTNER)]
        [HttpPost]
        public ActionResult ExchangePartnerEdit([DataSourceRequest]DataSourceRequest request, ExchangePartnerViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                exchangePartnerService.Update(item, ModelState);
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_PARTNER)]
        [HttpPost]
        public ActionResult ExchangePartnerDel([DataSourceRequest]DataSourceRequest request, ExchangePartnerViewModel item)
        {
            if ((ModelState.IsValid) && (item != null))
            {
                exchangePartnerService.Delete(item, ModelState);
            }
            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_PARTNER)]
        public ActionResult GetExchangePartnerItemForGetList([DataSourceRequest]DataSourceRequest request, long ds_id)
        {
            return Json(exchangeFileNodeItemService.GetList_New(ds_id, false, true).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_PARTNER)]
        public ActionResult GetExchangePartnerItemForSendList([DataSourceRequest]DataSourceRequest request, long ds_id)
        {            
            return Json(exchangeFileNodeItemService.GetList_New(ds_id, true, false).ToDataSourceResult(request));
        }

        #endregion

        #region ExchangeDataView

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        public ActionResult ExchangeDataView(long obj_id)
        {
            var exchange_data = exchangeDataService.GetItem(obj_id);
            if (exchange_data == null)
            {
                ModelState.AddModelError("error", "Не найдены данные с кодом " + obj_id.ToString());
                return RedirectToErrorPage(ModelState);
            }
            return View(exchange_data);
        }

        #endregion

        #region ExchangeDocDataView

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        public ActionResult ExchangeDocDataView(long obj_id)
        {
            var exchange_doc_data = exchangeDocDataService.GetItem(obj_id);
            if (exchange_doc_data == null)
            {
                ModelState.AddModelError("error", "Не найдены данные с кодом " + obj_id.ToString());
                return RedirectToErrorPage(ModelState);
            }
            return View(exchange_doc_data);
        }

        #endregion

        #region ExchangeLog
        
        [ClientServiceAuthorize(Enums.ClientServiceEnum.EXCHANGE)]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_LOG)]
        public ActionResult ExchangeLogList()
        {
            ViewBag.DsList = exchangePartnerService.GetList();
            ViewBag.ClientList = exchangeFileNodeService.GetList_Sprav();
            return View();
        }

        [ClientServiceAuthorize(Enums.ClientServiceEnum.EXCHANGE)]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_LOG)]
        public ActionResult GetExchangeFileLogList([DataSourceRequest]DataSourceRequest request, int? ds_id, int? client_id, bool? error_only)
        {
            return Json(exchangeFileLogService.GetList_byFilter(ds_id, client_id, error_only).ToDataSourceResult(request));
        }        

        #endregion

        #region ExchangeMonitor

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        public ActionResult ExchangeMonitor()
        {
            ViewBag.ClientList = exchangeClientService.GetList();            
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        public ActionResult GetExchangeMonitor([DataSourceRequest]DataSourceRequest request, long client_id, long ds_id, bool errors_only)
        {
            return Json(exchangeMonitorService.GetList_byClientAndPartner(client_id, ds_id, errors_only).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_MONITOR)]
        public ActionResult GetExchangeMonitorLogList([DataSourceRequest]DataSourceRequest request, long reg_id, long ds_id, int exchange_direction)
        {
            return Json(exchangeMonitorLogService.GetList_forMonitor(reg_id, ds_id, exchange_direction).ToDataSourceResult(request));
        }

        #endregion

        #region ExchangeRegPartnerItem

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult ExchangeRegPartnerItemEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<ExchangeRegPartnerItemViewModel> items)
        {
            if (items != null && ModelState.IsValid)
            {
                var serv = exchangeRegPartnerItemService;
                foreach (var item in items)
                {                    
                    serv.Update(item, ModelState);                    
                }
            }

            return Json(items.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ExchangeTask

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        public ActionResult GetExchangeTaskList([DataSourceRequest]DataSourceRequest request, long user_id)
        {
            return Json(exchangeTaskService.GetList_byParent(user_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeTaskAdd(ExchangeTaskViewModel item, long user_id)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeTaskService.InsertTask_forUser(item, user_id, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении задачи");
                    return View(item);
                }
                //return View(res == null ? item : res);
                return RedirectToAction("ExchangeClientList", "Exchange");
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeTaskEdit(ExchangeTaskViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeTaskService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при корректировании задачи");
                    return View(item);
                }
                //return View(res == null ? item : res);
                return RedirectToAction("ExchangeClientList", "Exchange");
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeTaskDel([DataSourceRequest]DataSourceRequest request, ExchangeTaskViewModel item)
        {
            if (ModelState.IsValid)
            {
                exchangeTaskService.Delete(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_CLIENT)]
        [HttpPost]
        public ActionResult ExchangeTaskTypeAdd(string type_name)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeTaskTypeService.InsertType(type_name, ModelState);
                return Json(new[] { type_name });            
            }
            else
            {
                return RedirectToErrorPage(ModelState);
            }
        }

        #endregion

        #region ExchangeFile

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_LOG)]
        public ActionResult GetExchangeFileNodeList([DataSourceRequest]DataSourceRequest request)
        {            
            return Json(exchangeFileNodeService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_LOG)]
        [HttpGet]
        public ActionResult ExchangeFileNodeAdd()
        {
            ViewBag.DsList = exchangePartnerService.GetList();
            return View(new ExchangeFileNodeViewModel());
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_LOG)]
        [HttpPost]
        public ActionResult ExchangeFileNodeAdd(ExchangeFileNodeViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeFileNodeService.Insert(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении узла");
                    return RedirectToErrorPage(ModelState);
                }
                //return RedirectToAction("ExchangeFileNodeList", "Exchange");
                return RedirectToAction("ExchangeLogList", "Exchange");
            }
            return View(item);
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.EXCHANGE_LOG)]
        [HttpGet]
        public ActionResult ExchangeFileNodeEdit(int node_id)
        {
            var item = exchangeFileNodeService.GetItem(node_id);
            ViewBag.DsList = exchangePartnerService.GetList();
            ViewBag.NodeItemSendList = exchangeFileNodeItemService.GetList_Exisiting(node_id, true, false).AsEnumerable().Cast<ExchangeFileNodeItemViewModel>().ToList();
            ViewBag.NodeItemGetList = exchangeFileNodeItemService.GetList_Exisiting(node_id, false, true).AsEnumerable().Cast<ExchangeFileNodeItemViewModel>().ToList();
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_LOG)]
        [HttpPost]
        public ActionResult ExchangeFileNodeEdit(ExchangeFileNodeViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = exchangeFileNodeService.Update(item, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании узла");
                    return RedirectToErrorPage(ModelState);
                }
                //return RedirectToAction("ExchangeFileNodeList", "Exchange");
                return RedirectToAction("ExchangeLogList", "Exchange");
            }
            return View(item);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.EXCHANGE_LOG)]
        [HttpPost]
        public JsonResult ExchangeFileNodeDel(int node_id)
        {
            if (ModelState.IsValid)
            {
                var item = exchangeFileNodeService.GetItem(node_id);
                exchangeFileNodeService.Delete(item, ModelState);
            }
            return Json(node_id, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
