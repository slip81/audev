﻿namespace CabinetMvc.Controllers
{
    using System.Web.Mvc;
    using Kendo.Mvc.UI;
    using Kendo.Mvc.Extensions;
    using CabinetMvc.ViewModel.Scheduler;

    public partial class SchedulerController : BaseController
    {                
        private SchedulerMeetingService meetingService;

        /*
        public SchedulerController()
            :base()
        {
            this.meetingService = new SchedulerMeetingService();
            this.meetingService.currUser = CurrentUser;
        }
        */

        public ActionResult Index()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (meetingService != null)
                meetingService.Dispose();
            base.Dispose(disposing);
        }
    }
}
