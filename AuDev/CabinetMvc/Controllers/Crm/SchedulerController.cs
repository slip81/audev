﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Drawing;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.ViewModel.Sys;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CrmController : BaseController
    {
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpGet]
        public ActionResult TaskScheduler(int? task_id, int? event_id)
        {
            ViewBag.TaskGridColumnSettings = cabGridColumnUserSettingsService.GetList_byParent((int)Enums.CabGrid.TaskList);

            CabGridUserSettingsViewModel gridCols = (CabGridUserSettingsViewModel)(cabGridUserSettingService.GetItem((int)Enums.CabGrid.TaskList));
            ViewBag.TaskGridColumnWidths = gridCols == null ? "" : gridCols.columns_options;            

            List<CabinetMvc.ViewModel.Adm.CabUserViewModel> lst = cabUserService.GetList_Executer().Cast<CabinetMvc.ViewModel.Adm.CabUserViewModel>().ToList();
            ViewBag.ExecuterList = lst;

            List<CabinetMvc.ViewModel.Crm.CrmEventGroupViewModel> lst2 = crmEventGroupService.GetList().AsEnumerable().Cast<CabinetMvc.ViewModel.Crm.CrmEventGroupViewModel>().ToList();
            ViewBag.GroupList = lst2.OrderByDescending(ss => ss.group_id);
            
            
            var gridUserSettings = cabGridUserSettingService.GetItem((int)Enums.CabGrid.TaskList);
            ViewBag.TaskGridColorColumn = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).back_color_column_id : null;

            CrmSchedulerViewModel crm_event = null;
            ViewBag.SchDate = DateTime.Today;
            ViewBag.ExecUserId = null;
            if (task_id.GetValueOrDefault(0) > 0)
            {
                crm_event = crmSchedulerService.GetItem_byTask((int)task_id);
                if (crm_event != null)
                {
                    ViewBag.SchDate = crm_event.Start.Date;
                    ViewBag.ExecUserId = crm_event.exec_user_id;
                }
            }
            else if (event_id.GetValueOrDefault(0) > 0)
            {
                crm_event = (CrmSchedulerViewModel)crmSchedulerService.GetItem((int)event_id);
                if (crm_event != null)
                {
                    ViewBag.SchDate = crm_event.Start.Date;
                    ViewBag.ExecUserId = crm_event.exec_user_id;
                }
            }

            // очень плохо, надо переделать
            var stateList = crmStateService.GetList_forEdit();
            var state1_color = stateList.Where(ss => ss.state_id == 1).Select(ss => ss.fore_color).FirstOrDefault();
            var state2_color = stateList.Where(ss => ss.state_id == 2).Select(ss => ss.fore_color).FirstOrDefault();
            var state3_color = stateList.Where(ss => ss.state_id == 3).Select(ss => ss.fore_color).FirstOrDefault();
            var state4_color = stateList.Where(ss => ss.state_id == 4).Select(ss => ss.fore_color).FirstOrDefault();
            var state5_color = stateList.Where(ss => ss.state_id == 5).Select(ss => ss.fore_color).FirstOrDefault();
            var state6_color = stateList.Where(ss => ss.state_id == 6).Select(ss => ss.fore_color).FirstOrDefault();
            var state7_color = stateList.Where(ss => ss.state_id == 7).Select(ss => ss.fore_color).FirstOrDefault();
            var state8_color = stateList.Where(ss => ss.state_id == 8).Select(ss => ss.fore_color).FirstOrDefault();

            ViewBag.state1_color = state1_color;
            ViewBag.state2_color = state2_color;
            ViewBag.state3_color = state3_color;
            ViewBag.state4_color = state4_color;
            ViewBag.state5_color = state5_color;
            ViewBag.state6_color = state6_color;
            ViewBag.state7_color = state7_color;
            ViewBag.state8_color = state8_color;

            ViewBag.OverdueDates = crmSchedulerService.GetDates_OverdueTasks();

            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public virtual JsonResult GetCrmSchedulerList([DataSourceRequest] DataSourceRequest request, bool? show_done, bool? show_canceled, int? user_id, bool? overdue_only, bool? priority_control, DateTime? date_beg, DateTime? date_end, bool? canceled_only)
        {
            return Json(crmSchedulerService.GetList_byFilter(show_done.GetValueOrDefault(true), show_canceled.GetValueOrDefault(true), user_id, overdue_only.GetValueOrDefault(false), priority_control.GetValueOrDefault(false), date_beg, date_end, canceled_only.GetValueOrDefault(false)).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public virtual JsonResult CrmSchedulerAdd([DataSourceRequest] DataSourceRequest request, CrmSchedulerViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = crmSchedulerService.Insert(item, ModelState);
                if (res == null)
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));
                
                CrmSchedulerViewModel itemNew = (CrmSchedulerViewModel)res;
                return Json(new[] { itemNew }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public virtual JsonResult CrmSchedulerEdit([DataSourceRequest] DataSourceRequest request, CrmSchedulerViewModel item)
        {
            if (ModelState.IsValid)
            {
                var res = crmSchedulerService.Update(item, ModelState);
                if (res == null)
                    return Json(new[] { item }.ToDataSourceResult(request, ModelState));

                CrmSchedulerViewModel itemNew = (CrmSchedulerViewModel)res;
                return Json(new[] { itemNew }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        } 

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public virtual JsonResult CrmSchedulerDel([DataSourceRequest] DataSourceRequest request, CrmSchedulerViewModel item)
        {
            if (ModelState.IsValid)
            {
                crmSchedulerService.Delete(item, ModelState);
            }

            return Json(new[] { item }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetExecUserList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(cabUserService.GetList_Executer().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpGet]
        public ActionResult TaskSchedulerExport(DateTime date_beg, DateTime date_end, int? user_id, string user_name, bool overdue_only, bool priority_control)
        {
            string date_beg_str = date_beg.ToShortDateString();
            string date_end_str =date_end.ToShortDateString();

            string reportTitle = "Список задач за период c " + date_beg_str + " по " + date_end_str;
            if (user_id.GetValueOrDefault(0) > 0)
                reportTitle += " [Исполнитель: " + user_name + "]";
            else
                reportTitle += " [Исполнители: все]";
            if (overdue_only)
                reportTitle += " [Только просроченные задачи]";
            if (priority_control)
                reportTitle += " [Задачи на контроле]";
            
            string filePath = @"c:\Release\Cabinet\Report\crm_scheduler_" + CurrentUser.CabUserId.ToString() + ".xlsx";
            string contentType = "application/vnd.ms-excel";

            var events = crmSchedulerService.GetList_byFilter(true, true, user_id, overdue_only, priority_control, date_beg, date_end, false).ToList();
            if ((events == null) || (events.Count <= 0))
            {
                ModelState.AddModelError("", "Не найдены задачи");
                return RedirectToErrorPage(ModelState);
            }

            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Properties.Author = "Аурит";
                p.Workbook.Properties.Title = reportTitle;

                p.Workbook.Worksheets.Add("Страница 1");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";

                int reportsfieldscount = 18;
                int colIndex = 1;
                int rowIndex = 2;
                int i = 1;

                string task_num = "";
                string task_name = "";
                string task_text = "";
                string state_name = "";                
                string exec_user_name = "";
                string assigned_user_name = "";
                string client_name = "";
                string fin_cost = "";
                string project_name = "";
                string module_name = "";
                string module_version_name = "";
                string date_plan = "";
                string date_fact = "";
                string repair_version_name = "";
                string owner_user_name = "";
                string crt_date = "";
                string upd_date = "";
                string priority_name = "";
                string task_id = "";

                #region

                using (var range = ws.Cells[1, 1, 1, reportsfieldscount])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                while (i <= reportsfieldscount)
                {
                    ws.Column(i).Width = 40;
                    i++;
                }

                //Merging cells and create a center heading for out table
                ws.Cells[1, 1].Value = reportTitle;
                ws.Cells[1, 1, 1, reportsfieldscount].Merge = true;
                ws.Cells[1, 1, 1, reportsfieldscount].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                using (var range = ws.Cells[2, 1, 2, reportsfieldscount])
                {
                    range.Style.Font.Bold = false;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                ws.Cells[rowIndex, colIndex].Value = "№ задачи";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Кратко";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Подробно";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Статус";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Исполнитель";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Ответственный";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Клиент";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Цена";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Проект";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Модуль";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Версия модуля";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Плановая дата";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Фактическая дата";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                //ws.Cells[rowIndex, colIndex].Value = "Версия исправления";
                //ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Автор";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Дата создания";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Дата изменения";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Приоритет";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Код задачи";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;

                foreach (var ev in events)
                {
                    task_num = ev.task_num;
                    task_name = ev.task_name;
                    task_text = ev.task_text;
                    state_name = ev.state_name;
                    exec_user_name = ev.exec_user_name;
                    assigned_user_name = ev.assigned_user_name;
                    client_name = ev.client_name;
                    fin_cost = ev.fin_cost.HasValue ? ev.fin_cost.ToString() : "";
                    project_name = ev.project_name;
                    module_name = ev.module_name;
                    module_version_name = ev.module_version_name;
                    date_plan = ev.date_plan.HasValue ? ((DateTime)ev.date_plan).ToString("dd.MM.yyyy") : "";
                    date_fact = ev.date_fact.HasValue ? ((DateTime)ev.date_fact).ToString("dd.MM.yyyy") : "";                    
                    owner_user_name = ev.owner_user_name;
                    crt_date = ev.crt_date.HasValue ? ((DateTime)ev.crt_date).ToString("dd.MM.yyyy HH:mm:ss") : "";
                    upd_date = ev.upd_date.HasValue ? ((DateTime)ev.upd_date).ToString("dd.MM.yyyy HH:mm:ss") : "";
                    priority_name = ev.priority_name;
                    task_id = ev.task_id.HasValue ? ev.task_id.ToString() : "";

                    colIndex = 1;
                    rowIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_num;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    ws.Cells[rowIndex, colIndex].Style.WrapText = true;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_text;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    ws.Cells[rowIndex, colIndex].Style.WrapText = true;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = state_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = exec_user_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = assigned_user_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = client_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = fin_cost;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = project_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = module_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = module_version_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = date_plan;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = date_fact;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = owner_user_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = crt_date;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = upd_date;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = priority_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_id;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    colIndex++;

                }
                #endregion

                // AutoFitColumns
                // ws.Cells[ws.Dimension.Address].AutoFitColumns();

                Byte[] bin = p.GetAsByteArray();
                System.IO.File.WriteAllBytes(filePath, bin);
            }

            return File(filePath, contentType, "Календарь задач.xlsx");
        }               
    }
}
