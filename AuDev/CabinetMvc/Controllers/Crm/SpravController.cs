﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Crm;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CrmController : BaseController
    {

        #region Priority

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmPriorityList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(crmPriorityService.GetList_forEdit().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmPriorityAdd([DataSourceRequest] DataSourceRequest request, CrmPriorityViewModel itemAdd)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                var res = crmPriorityService.Insert(itemAdd, ModelState);
            }

            _cachedPriorityList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmPriorityViewModel>>();

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmPriorityEdit([DataSourceRequest]DataSourceRequest request, CrmPriorityViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                crmPriorityService.Update(itemEdit, ModelState);
            }

            _cachedPriorityList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmPriorityViewModel>>();

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmPriorityDel([DataSourceRequest]DataSourceRequest request, CrmPriorityViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                crmPriorityService.Delete(itemDel, ModelState);
            }

            _cachedPriorityList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmPriorityViewModel>>();

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Project

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmProjectList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(crmProjectService.GetList_forEdit().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmProjectSpravList()
        {
            return Json(crmProjectService.GetList_Sprav(), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmProjectAdd([DataSourceRequest] DataSourceRequest request, CrmProjectViewModel itemAdd)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                var res = crmProjectService.Insert(itemAdd, ModelState);
            }

            _cachedProjectList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmProjectViewModel>>();

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmProjectEdit([DataSourceRequest]DataSourceRequest request, CrmProjectViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                crmProjectService.Update(itemEdit, ModelState);
            }

            _cachedProjectList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmProjectViewModel>>();

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmProjectDel([DataSourceRequest]DataSourceRequest request, CrmProjectViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                crmProjectService.Delete(itemDel, ModelState);
            }

            _cachedProjectList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmProjectViewModel>>();

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Module

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmModuleList([DataSourceRequest]DataSourceRequest request, int parent_project_id)
        {
            return Json(crmModuleService.GetList_byParent(parent_project_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModuleAdd([DataSourceRequest] DataSourceRequest request, CrmModuleViewModel itemAdd, int parent_project_id)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                itemAdd.project_id = parent_project_id;
                var res = crmModuleService.Insert(itemAdd, ModelState);
            }

            _cachedModuleList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModuleViewModel>>();

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModuleEdit([DataSourceRequest]DataSourceRequest request, CrmModuleViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                crmModuleService.Update(itemEdit, ModelState);
            }

            _cachedModuleList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModuleViewModel>>();

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModuleDel([DataSourceRequest]DataSourceRequest request, CrmModuleViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                crmModuleService.Delete(itemDel, ModelState);
            }
            
            _cachedModuleList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModuleViewModel>>();

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region CrmProjectVersion

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmProjectVersionList([DataSourceRequest]DataSourceRequest request, int parent_project_id)
        {
            return Json(crmProjectVersionService.GetList_byParent(parent_project_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmProjectVersionAdd([DataSourceRequest] DataSourceRequest request, CrmProjectVersionViewModel itemAdd, int parent_project_id)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                itemAdd.project_id = parent_project_id;
                var res = crmProjectVersionService.Insert(itemAdd, ModelState);
            }

            _cachedCrmProjectVersionList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmProjectVersionViewModel>>();

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmProjectVersionEdit([DataSourceRequest]DataSourceRequest request, CrmProjectVersionViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                crmProjectVersionService.Update(itemEdit, ModelState);
            }

            _cachedCrmProjectVersionList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmProjectVersionViewModel>>();

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmProjectVersionDel([DataSourceRequest]DataSourceRequest request, CrmProjectVersionViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                crmProjectVersionService.Delete(itemDel, ModelState);
            }

            _cachedCrmProjectVersionList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmProjectVersionViewModel>>();

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ModuleVersion

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmModuleVersionList([DataSourceRequest]DataSourceRequest request, int parent_module_id)
        {
            return Json(crmModuleVersionService.GetList_byParent(parent_module_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModuleVersionAdd([DataSourceRequest] DataSourceRequest request, CrmModuleVersionViewModel itemAdd, int parent_module_id)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                itemAdd.module_id = parent_module_id;
                var res = crmModuleVersionService.Insert(itemAdd, ModelState);
            }

            _cachedModuleVersionList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModuleVersionViewModel>>();

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModuleVersionEdit([DataSourceRequest]DataSourceRequest request, CrmModuleVersionViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                crmModuleVersionService.Update(itemEdit, ModelState);
            }

            _cachedModuleVersionList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModuleVersionViewModel>>();

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModuleVersionDel([DataSourceRequest]DataSourceRequest request, CrmModuleVersionViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                crmModuleVersionService.Delete(itemDel, ModelState);
            }
            
            _cachedModuleVersionList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModuleVersionViewModel>>();

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region ModulePart

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmModulePartList([DataSourceRequest]DataSourceRequest request, int parent_module_id)
        {
            return Json(crmModulePartService.GetList_byParent(parent_module_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModulePartAdd([DataSourceRequest] DataSourceRequest request, CrmModulePartViewModel itemAdd, int parent_module_id)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                itemAdd.module_id = parent_module_id;
                var res = crmModulePartService.Insert(itemAdd, ModelState);
            }

            _cachedModulePartList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModulePartViewModel>>();

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModulePartEdit([DataSourceRequest]DataSourceRequest request, CrmModulePartViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                crmModulePartService.Update(itemEdit, ModelState);
            }

            _cachedModulePartList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModulePartViewModel>>();

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmModulePartDel([DataSourceRequest]DataSourceRequest request, CrmModulePartViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                crmModulePartService.Delete(itemDel, ModelState);
            }

            _cachedModulePartList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmModulePartViewModel>>();

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region Client

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmClientList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(crmClientService.GetList_forEdit().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmClientAdd([DataSourceRequest] DataSourceRequest request, CrmClientViewModel itemAdd)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                var res = crmClientService.Insert(itemAdd, ModelState);
            }

            _cachedClientList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmClientViewModel>>();

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmClientEdit([DataSourceRequest]DataSourceRequest request, CrmClientViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                crmClientService.Update(itemEdit, ModelState);
            }

            _cachedClientList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmClientViewModel>>();

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmClientDel([DataSourceRequest]DataSourceRequest request, CrmClientViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                crmClientService.Delete(itemDel, ModelState);
            }
            
            _cachedClientList = null;
            cabCache.DeleteCachedObject<IEnumerable<CrmClientViewModel>>();

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion
            
        #region UserRole

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.PRJ_SPRAV)]
        public ActionResult GetCrmUserRoleList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(crmUserRoleService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmUserRoleAdd([DataSourceRequest] DataSourceRequest request, CrmUserRoleViewModel itemAdd)
        {
            if (itemAdd != null && ModelState.IsValid)
            {
                var res = crmUserRoleService.Insert(itemAdd, ModelState);
            }

            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmUserRoleEdit([DataSourceRequest]DataSourceRequest request, CrmUserRoleViewModel itemEdit)
        {
            if ((ModelState.IsValid) && (itemEdit != null))
            {
                crmUserRoleService.Update(itemEdit, ModelState);
            }

            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.PRJ_SPRAV)]
        [HttpPost]
        public ActionResult CrmUserRoleDel([DataSourceRequest]DataSourceRequest request, CrmUserRoleViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                crmUserRoleService.Delete(itemDel, ModelState);
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion
    }
}
