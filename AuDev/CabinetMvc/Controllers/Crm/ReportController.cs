﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Drawing;
using System.IO;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.ViewModel.Sys;
using CabinetMvc.Extensions;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CrmController : BaseController
    {

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public JsonResult GetReport1(int user_id, string date_beg, string date_end)
        {
            string fileName = "crmreport1_" + CurrentUser.CabUserId.ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx";
            string filePath = @"c:\Release\Cabinet\Report\" + fileName;
            //string contentType = "application/vnd.ms-excel";
            string reportTitle = "Активность пользователя в задачах за период";
            //string reportSubtitle = "Актуальность данных: " + DateTime.Now.ToString("dd.MM.yyyy HH:mm");
            string reportSubtitle = "";
            /*
                DateTime? date_beg_dt = String.IsNullOrEmpty(date_beg) ? null : (DateTime?)DateTime.ParseExact(date_beg, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                DateTime? date_end_dt = String.IsNullOrEmpty(date_end) ? null : (DateTime?)DateTime.ParseExact(date_end, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            */
            /*
                DateTime? date_beg_dt = GlobalUtil.JavaScriptDateToDate(date_beg);
                DateTime? date_end_dt = GlobalUtil.JavaScriptDateToDate(date_end);
            */
            DateTime? date_beg_dt = String.IsNullOrEmpty(date_beg) ? null : (DateTime?)DateTime.Parse(date_beg);
            DateTime? date_end_dt = String.IsNullOrEmpty(date_end) ? null : (DateTime?)DateTime.Parse(date_end);
            if (date_end_dt.HasValue)
                date_end_dt = ((DateTime)date_end_dt).AddDays(1);

            var report = crmReport1Service.GetReport(user_id, date_beg_dt, date_end_dt, ModelState);
            if ((report == null) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid) 
                    ModelState.AddModelError("", "Нет данных для формировании отчета");
                //return Content(ModelState.GetErrorsString());
                return Json(new { err = true, mess = ModelState.GetErrorsString() });
            }

            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Properties.Author = "Аурит";
                p.Workbook.Properties.Title = reportTitle;

                p.Workbook.Worksheets.Add("Страница 1");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";

                int reportsfieldscount = 13;
                int colIndex = 1;
                int rowIndex = 3;
                int i = 1;

                string task_num = "";
                string task_name = "";
                string mess = "";
                string required_date = "";
                string repair_date = "";
                string state_name = "";
                string client_name = "";
                string project_name = "";
                string module_name = "";
                string module_version_name = "";
                string repair_version_name = "";
                string priority_name = "";
                string group_name = "";
                string subtasks_text = "";
                string comments_text = "";

                #region

                using (var range = ws.Cells[1, 1, rowIndex - 1, reportsfieldscount])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                
                while (i <= reportsfieldscount)
                {
                    ws.Column(1).Width = 40;
                    i++;
                }


                //Merging cells and create a center heading for out table
                ws.Cells[rowIndex - 2, 1].Value = reportTitle;
                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Merge = true;
                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.Font.Bold = true;
                ws.Cells[rowIndex - 2, 1, rowIndex - 2, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                ws.Cells[rowIndex - 1, 1].Value = reportSubtitle;
                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Merge = true;
                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.Font.Bold = true;
                ws.Cells[rowIndex - 1, 1, rowIndex - 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                using (var range = ws.Cells[rowIndex, 1, rowIndex, reportsfieldscount])
                {
                    range.Style.Font.Bold = false;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }                

                ws.Cells[rowIndex, colIndex].Value = "№ задачи";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Кратко";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Действия";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Плановая дата";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Фактическая дата";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Статус";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Клиент";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Проект";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Модуль";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Версия";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Версия исправления";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Приоритет";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Группа";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                /*
                ws.Cells[rowIndex, colIndex].Value = "Подзадачи";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Комментарии";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                */
                foreach (var reportItem in report.OrderBy(ss => ss.task_id))
                {

                    task_num = reportItem.task_num;
                    task_name = reportItem.task_name;
                    mess = reportItem.mess;
                    required_date = reportItem.required_date.HasValue ? ((DateTime)reportItem.required_date).ToString("dd.MM.yyyy") : "";
                    repair_date = reportItem.repair_date.HasValue ? ((DateTime)reportItem.repair_date).ToString("dd.MM.yyyy") : "";
                    state_name = reportItem.state_name;
                    client_name = reportItem.client_name;
                    project_name = reportItem.project_name;
                    module_name = reportItem.module_name;
                    module_version_name = reportItem.module_version_name;
                    repair_version_name = reportItem.repair_version_name;
                    priority_name = reportItem.priority_name;
                    group_name = reportItem.group_name;
                    subtasks_text = reportItem.subtasks_text;
                    comments_text = reportItem.comments_text;

                    colIndex = 1;
                    rowIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_num;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = task_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = mess;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    ws.Cells[rowIndex, colIndex].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                    ws.Cells[rowIndex, colIndex].Style.WrapText = true;                    
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = required_date;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = repair_date;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = state_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = client_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = project_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = module_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = module_version_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = repair_version_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = priority_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = group_name;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    /*
                    ws.Cells[rowIndex, colIndex].Value = subtasks_text;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = comments_text;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    colIndex++;
                    */
                }
                #endregion

                // AutoFitColumns
                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                Byte[] bin = p.GetAsByteArray();
                System.IO.File.WriteAllBytes(filePath, bin);                
            }

            //return  File(filePath, contentType, "crmreport1.xlsx");
            return Json(new { err = false, file_name = fileName });
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpGet]
        public FileResult GetReport1(string file_name)
        {
            string contentType = "application/vnd.ms-excel";
            string filePath = @"c:\Release\Cabinet\Report\" + file_name;
            if (System.IO.File.Exists(filePath))
                return File(filePath, contentType, file_name);
            else
                return null;
        }
        
    }
}