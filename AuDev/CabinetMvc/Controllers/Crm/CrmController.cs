﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Routing;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;

namespace CabinetMvc.Controllers
{    
    public partial class CrmController : BaseController
    {
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult Index()
        {
            return View();
        }
        
    }
}
