﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Drawing;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using CabinetMvc.Util;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.ViewModel.Sys;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CrmController : BaseController
    {

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult TaskNotify()
        {
            //ViewBag.TaskGridColumnSettings = cabGridColumnUserSettingsService.GetList_byParent((int)Enums.CabGrid.TaskList);
            int user_id = CurrentUser.CabUserId;
            CrmNotifyViewModel notify = (CrmNotifyViewModel)crmNotifyService.GetItem(user_id);
            if (notify == null)
            {
                notify = crmNotifyService.CreateNew(ModelState);
            }
            notify.paramTransport = crmNotifyService.GetParam_Transport(notify.user_id, ModelState);
            notify.paramUsers_icq = crmNotifyParamUserService.GetItems_byUserAndTransport(user_id, (int)Enums.NotifyTransportTypeEnum.ICQ);
            notify.paramAttr_icq = crmNotifyParamAttrService.GetItems_byUserAndTransport(user_id, (int)Enums.NotifyTransportTypeEnum.ICQ);
            notify.paramContent_icq = crmNotifyParamContentService.GetItems_byUserAndTransport(user_id, (int)Enums.NotifyTransportTypeEnum.ICQ);
            return View(notify);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public JsonResult GetCrmNotifyParamTransportList([DataSourceRequest] DataSourceRequest request, int user_id)
        {
            return Json(crmNotifyService.GetParam_Transport(user_id, ModelState).ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult NotifyParamTransportEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<SelectListItem> items, int user_id)
        {

            if (items != null && ModelState.IsValid)            
            {
                var serv = crmNotifyService;
                CrmNotifyViewModel notify = (CrmNotifyViewModel)serv.GetItem(user_id);
                foreach (var item in items)
                {
                    if (item.Value.Trim().ToLower().Equals("icq"))
                    {
                        notify.icq = item.Text;
                        notify.is_active_icq = !item.Disabled;
                    }
                    else if (item.Value.Trim().ToLower().Equals("email"))
                    {
                        notify.email = item.Text;
                        notify.is_active_email = !item.Disabled;
                    }
                    serv.Update(notify, ModelState);
                }
            }

            return Json(crmNotifyService.GetParam_Transport(user_id, ModelState).ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public JsonResult GetCrmNotifyParamUserList([DataSourceRequest] DataSourceRequest request, int user_id, int transport_type)
        {
            return Json(crmNotifyParamUserService.GetItems_byUserAndTransport(user_id, transport_type).ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult NotifyParamUserEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CrmNotifyParamUserViewModel> items, int user_id, int transport_type)
        {
            if (items != null && ModelState.IsValid)
            {
                var serv = crmNotifyParamUserService;
                foreach (var item in items)
                {
                    item.user_id = user_id;
                    item.transport_type = transport_type;
                    serv.Update(item, ModelState);
                }
            }

            return Json(crmNotifyParamUserService.GetItems_byUserAndTransport(user_id, transport_type).ToDataSourceResult(request, ModelState));
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        public JsonResult GetCrmNotifyParamAttrList([DataSourceRequest] DataSourceRequest request, int user_id, int transport_type)
        {
            return Json(crmNotifyParamAttrService.GetItems_byUserAndTransport(user_id, transport_type).ToDataSourceResult(request, ModelState));
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult NotifyParamAttrEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CrmNotifyParamAttrViewModel> items, int user_id, int transport_type)
        {
            if (items != null && ModelState.IsValid)
            {
                var serv = crmNotifyParamAttrService;
                foreach (var item in items)
                {
                    item.user_id = user_id;
                    item.transport_type = transport_type;
                    serv.Update(item, ModelState);
                }
            }

            return Json(crmNotifyParamAttrService.GetItems_byUserAndTransport(user_id, transport_type).ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public JsonResult GetCrmNotifyParamContentList([DataSourceRequest] DataSourceRequest request, int user_id, int transport_type)
        {
            return Json(crmNotifyParamContentService.GetItems_byUserAndTransport(user_id, transport_type).ToDataSourceResult(request, ModelState));
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public ActionResult NotifyParamContentEdit([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<CrmNotifyParamContentViewModel> items, int user_id, int transport_type)
        {            
            if (items != null && ModelState.IsValid)
            {
                var serv = crmNotifyParamContentService;
                foreach (var item in items)
                {
                    item.user_id = user_id;
                    item.transport_type = transport_type;
                    serv.Update(item, ModelState);
                }
            }

            return Json(crmNotifyParamContentService.GetItems_byUserAndTransport(user_id, transport_type).ToDataSourceResult(request, ModelState));
        }
    }
}