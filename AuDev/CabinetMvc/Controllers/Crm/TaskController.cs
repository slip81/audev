﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Drawing;
using System.Web.Script.Serialization;
using System.IO;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.ViewModel.Sys;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class CrmController : BaseController
    {
        #region Task
        
        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult TaskList()
        {            
            ViewBag.TaskGridColumnSettings = cabGridColumnUserSettingsService.GetList_byParent((int)Enums.CabGrid.TaskList);
                        
            var gridUserSettings = cabGridUserSettingService.GetItem((int)Enums.CabGrid.TaskList);
            string gridSortOptions = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).sort_options : "";
            string gridFilterOptions = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).filter_options : "";            
            ViewBag.TaskGridSortOptions = new JavaScriptSerializer().Serialize(gridSortOptions);
            ViewBag.TaskGridFilterOptions = new JavaScriptSerializer().Serialize(gridFilterOptions);
            ViewBag.TaskGridColumnWidths = gridUserSettings == null ? "" : ((CabGridUserSettingsViewModel)gridUserSettings).columns_options;

            ViewBag.TaskGridColorColumn = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).back_color_column_id : null;
            ViewBag.TaskGridPageSize = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).page_size.GetValueOrDefault(10) : 10;                        
            ViewBag.FilterStateList = crmStateService.GetList();
            //ViewBag.FilterUserList = cabUserService.GetList();
            ViewBag.FilterUserList = cabUserService.GetList_CrmUser();
            ViewBag.FilterProjectList = crmProjectService.GetList();
            ViewBag.FilterClientList = crmClientService.GetList();
            ViewBag.FilterPriorityList = crmPriorityService.GetList();
            ViewBag.UserFilter = crmTaskUserFilterService.GetItem((int)Enums.CabGrid.TaskList);
            ViewBag.FilterBatchList = crmBatchService.GetList();
            ViewBag.OperationList = crmTaskGroupOperationService.GetList();
            var list1 = crmTaskRelTypeService.GetList();
            ViewBag.RelTypeList = list1;
            ViewBag.TaskGridColumnColorList = cabGridColumnService.GetList_Color((int)Enums.CabGrid.TaskList);
            return View();            
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult GetTaskList([CustomDataSourceRequest]DataSourceRequest request, int group_id, string phrase)
        {
            return Json(crmTaskService.GetList_byGroup(group_id, phrase).ToDataSourceResult(request));          
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult TaskEditSimple(int? task_id)
        {
            ViewBag.ProjectList = crmProjectService.GetList();
            ViewBag.ModuleList = crmModuleService.GetList();            
            ViewBag.ModuleVersionList = crmModuleVersionService.GetList();
            ViewBag.ClientList = crmClientService.GetList();
            ViewBag.PriorityList = crmPriorityService.GetList();
            ViewBag.StateList = crmStateService.GetList();
            //ViewBag.UserList = cabUserService.GetList();
            ViewBag.UserList = cabUserService.GetList_CrmUser();
            var model = task_id.GetValueOrDefault(0) > 0 ? crmTaskService.GetItem((int)task_id) : new CabinetMvc.ViewModel.Crm.CrmTaskViewModel();
            return View(model);
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult TaskEditFull(int? task_id)
        {
            ViewBag.ProjectList = crmProjectService.GetList();
            List<CabinetMvc.ViewModel.Crm.CrmModuleViewModel> moduleList = crmModuleService.GetList().Cast<CabinetMvc.ViewModel.Crm.CrmModuleViewModel>().ToList();
            ViewBag.ModuleList = moduleList;
            ViewBag.ModuleVersionList = crmModuleVersionService.GetList();
            ViewBag.ClientList = crmClientService.GetList();
            ViewBag.PriorityList = crmPriorityService.GetList();
            ViewBag.StateList = crmStateService.GetList();
            //ViewBag.UserList = cabUserService.GetList();
            ViewBag.UserList = cabUserService.GetList_CrmUser();
            ViewBag.GroupList = crmGroupService.GetList();
            ViewBag.BatchList = crmBatchService.GetList();

            IList<SelectListItem> eventUserList = new List<SelectListItem>
            {
                new SelectListItem{Text = "Исполнителю", Value = "true"},
                new SelectListItem{Text = "Себе", Value = "false"},
            };
            ViewBag.EventUserList = eventUserList;
            
            var model = task_id.GetValueOrDefault(0) > 0 ? crmTaskService.GetItem((int)task_id) : new CabinetMvc.ViewModel.Crm.CrmTaskViewModel();            
            return View(model);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult TaskEditScheduler(int? task_id, int? exec_user_id, string date_plan)
        {
            ViewBag.ProjectList = crmProjectService.GetList();
            List<CabinetMvc.ViewModel.Crm.CrmModuleViewModel> moduleList = crmModuleService.GetList().Cast<CabinetMvc.ViewModel.Crm.CrmModuleViewModel>().ToList();
            ViewBag.ModuleList = moduleList;
            ViewBag.ModuleVersionList = crmModuleVersionService.GetList();
            ViewBag.ClientList = crmClientService.GetList();
            ViewBag.PriorityList = crmPriorityService.GetList();
            ViewBag.StateList = crmStateService.GetList();
            //ViewBag.UserList = cabUserService.GetList();
            ViewBag.UserList = cabUserService.GetList_CrmUser();
            ViewBag.GroupList = crmGroupService.GetList();
            ViewBag.BatchList = crmBatchService.GetList();

            IList<SelectListItem> eventUserList = new List<SelectListItem>
            {
                new SelectListItem{Text = "Исполнителю", Value = "true"},
                new SelectListItem{Text = "Себе", Value = "false"},
            };
            ViewBag.EventUserList = eventUserList;

            bool existingTask = task_id.GetValueOrDefault(0) > 0;
            DateTime? date_plan_parsed = String.IsNullOrEmpty(date_plan) ? null : (DateTime?)DateTime.ParseExact(date_plan.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            CrmTaskViewModel model = existingTask ? (CrmTaskViewModel)crmTaskService.GetItem((int)task_id) : new CrmTaskViewModel() { exec_user_id = exec_user_id.GetValueOrDefault(0), required_date = date_plan_parsed, is_event = true };
            return View(model);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult TaskEditPersonal(int? task_id)
        {
            ViewBag.ProjectList = crmProjectService.GetList();
            List<CabinetMvc.ViewModel.Crm.CrmModuleViewModel> moduleList = crmModuleService.GetList().Cast<CabinetMvc.ViewModel.Crm.CrmModuleViewModel>().ToList();
            ViewBag.ModuleList = moduleList;
            ViewBag.ModuleVersionList = crmModuleVersionService.GetList();
            ViewBag.ClientList = crmClientService.GetList();
            ViewBag.PriorityList = crmPriorityService.GetList();
            ViewBag.StateList = crmStateService.GetList();
            //ViewBag.UserList = cabUserService.GetList();
            ViewBag.UserList = cabUserService.GetList_CrmUser();
            ViewBag.GroupList = crmGroupService.GetList();
            ViewBag.BatchList = crmBatchService.GetList();

            IList<SelectListItem> eventUserList = new List<SelectListItem>
            {
                new SelectListItem{Text = "Исполнителю", Value = "true"},
                new SelectListItem{Text = "Себе", Value = "false"},
            };
            ViewBag.EventUserList = eventUserList;

            var model = task_id.GetValueOrDefault(0) > 0 ? crmTaskService.GetItem((int)task_id) : new CabinetMvc.ViewModel.Crm.CrmTaskViewModel();
            return View(model);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult TaskEditPlanner(int? task_id, int? exec_user_id, string date_plan)
        {
            ViewBag.ProjectList = crmProjectService.GetList();
            List<CabinetMvc.ViewModel.Crm.CrmModuleViewModel> moduleList = crmModuleService.GetList().Cast<CabinetMvc.ViewModel.Crm.CrmModuleViewModel>().ToList();
            ViewBag.ModuleList = moduleList;
            ViewBag.ModuleVersionList = crmModuleVersionService.GetList();
            ViewBag.ClientList = crmClientService.GetList();
            ViewBag.PriorityList = crmPriorityService.GetList();
            ViewBag.StateList = crmStateService.GetList();
            //ViewBag.UserList = cabUserService.GetList();
            ViewBag.UserList = cabUserService.GetList_CrmUser();
            ViewBag.GroupList = crmGroupService.GetList();
            ViewBag.BatchList = crmBatchService.GetList();

            IList<SelectListItem> eventUserList = new List<SelectListItem>
            {
                new SelectListItem{Text = "Исполнителю", Value = "true"},
                new SelectListItem{Text = "Себе", Value = "false"},
            };
            ViewBag.EventUserList = eventUserList;

            bool existingTask = task_id.GetValueOrDefault(0) > 0;
            DateTime? date_plan_parsed = String.IsNullOrEmpty(date_plan) ? null : (DateTime?)DateTime.ParseExact(date_plan.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            CrmTaskViewModel model = null;
            if (existingTask)
            {
                model = (CrmTaskViewModel)crmTaskService.GetItem((int)task_id);
                if ((exec_user_id.HasValue) && (date_plan_parsed.HasValue))
                {
                    model.exec_user_id = (int)exec_user_id;
                    model.required_date = date_plan_parsed;
                    model.is_event = true;
                }
            }
            else
            {
                model = new CrmTaskViewModel();
            }
            
            return View(model);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public JsonResult TaskAdd(CrmTaskViewModel task, IEnumerable<CrmTaskNoteViewModel> notes, IEnumerable<CrmSubtaskViewModel> subtasks)
        {
            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel task_res = null;
                task.notes = notes;
                task.subtasks = subtasks;
                if (task.task_id > 0)
                {
                    task_res = crmTaskService.Update(task, ModelState);
                }
                else
                {
                    task_res = crmTaskService.Insert(task, ModelState);
                }
                if ((task_res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении задачи");
                    TempData["error"] = ModelState.GetErrorsString();

                    //return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                    return Json(new[] { task }, JsonRequestBehavior.AllowGet);
                }
                //return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                return Json(task_res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["error"] = ModelState.GetErrorsString();
                //return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                return Json(new[] { task }, JsonRequestBehavior.AllowGet);
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public JsonResult TaskOperationExecute(string task_id_list, int operation_id, int? batch_id)
        {
            if (ModelState.IsValid)
            {
                var res = crmTaskService.TaskOperationExecute(task_id_list, operation_id, batch_id, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при выполнении операции со списком задач");
                    TempData["error"] = ModelState.GetErrorsString();

                    //return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                    return Json(new { task_id_list, operation_id }, JsonRequestBehavior.AllowGet);
                }
                //return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                return Json(new { task_id_list, operation_id }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["error"] = ModelState.GetErrorsString();
                //return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                return Json(new { task_id_list, operation_id }, JsonRequestBehavior.AllowGet);
            }
        }
        
        [HttpPost]
        public ActionResult TaskListExport(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }       

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public JsonResult TaskUserFilterSave(int grid_id, string raw_filter, string project_filter, string client_filter, string priority_filter, string state_filter
            , string exec_user_filter, string assigned_user_filter,  DateTime? required_date1_filter, DateTime? required_date2_filter
            , DateTime? repair_date1_filter, DateTime? repair_date2_filter, double? fin_cost_filter, int curr_group_id, bool is_moderated_filter, string batch_filter)
        {
            if (ModelState.IsValid)
            {
                CrmTaskUserFilterViewModel vm = new CrmTaskUserFilterViewModel()
                {
                    grid_id = grid_id,
                    raw_filter = raw_filter,
                    project_filter = project_filter,
                    client_filter = client_filter,
                    priority_filter = priority_filter,
                    state_filter = state_filter,
                    exec_user_filter = exec_user_filter,
                    //owner_user_filter = owner_user_filter,
                    owner_user_filter = "",
                    assigned_user_filter = assigned_user_filter,
                    required_date1_filter = required_date1_filter,
                    required_date2_filter = required_date2_filter,
                    repair_date1_filter = repair_date1_filter,
                    repair_date2_filter = repair_date2_filter,
                    fin_cost_filter = fin_cost_filter,
                    curr_group_id = curr_group_id,
                    is_moderated_filter = is_moderated_filter,
                    batch_filter = batch_filter,
                }
                ;
                var res = crmTaskUserFilterService.UpdateFilter(vm, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при сохранении фильтра списка задачи");
                    TempData["error"] = ModelState.GetErrorsString();

                    //return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                    return Json(new { grid_id, raw_filter }, JsonRequestBehavior.AllowGet);
                }
                //return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                return Json(new { grid_id, raw_filter }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                TempData["error"] = ModelState.GetErrorsString();
                //return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
                return Json(new { grid_id, raw_filter }, JsonRequestBehavior.AllowGet);
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpGet]
        public ActionResult GetCrmTaskNumList([DataSourceRequest]DataSourceRequest request, string text)
        {
            var res = crmTaskService.GetNumList(text).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpGet]
        public ActionResult GetCrmTaskShortList([DataSourceRequest]DataSourceRequest request, string text, int exec_user_id)
        {
            //Response.ContentType = "application/x-javascript";
            var res = crmTaskService.GetShortList_ForExecUser(text, exec_user_id).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpPost]
        public JsonResult GetTaskText(int task_id)
        {
            if (ModelState.IsValid)
            {
                var res = crmTaskService.GetTaskText(task_id);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при попытке получения текста задачи");

                    return Json(new { error = true, mess = ModelState.GetErrorsString(), text = "" }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { error = false, mess = "", text = res }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { error = true, mess = ModelState.GetErrorsString(), text = "" }, JsonRequestBehavior.AllowGet);
            }
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpGet]
        public ActionResult TaskEdit(int task_id)
        {
            ViewBag.ProjectList = crmProjectService.GetList();
            List<CabinetMvc.ViewModel.Crm.CrmModuleViewModel> moduleList = crmModuleService.GetList().Cast<CabinetMvc.ViewModel.Crm.CrmModuleViewModel>().ToList();
            ViewBag.ModuleList = moduleList;
            ViewBag.ModuleVersionList = crmModuleVersionService.GetList();
            ViewBag.ClientList = crmClientService.GetList();
            ViewBag.PriorityList = crmPriorityService.GetList();
            ViewBag.StateList = crmStateService.GetList();            
            ViewBag.UserList = cabUserService.GetList_CrmUser();
            ViewBag.GroupList = crmGroupService.GetList();
            ViewBag.FileList = crmTaskFileService.GetList_byParent(task_id);
            ViewBag.BatchList = crmBatchService.GetList();

            IList<SelectListItem> eventUserList = new List<SelectListItem>
            {
                new SelectListItem{Text = "Исполнителю", Value = "true"},
                new SelectListItem{Text = "Себе", Value = "false"},
            };
            ViewBag.EventUserList = eventUserList;

            if (task_id <= 0)
            {
                return View(new CrmTaskViewModel() {  });
            }
            else
            {
                var task = crmTaskService.GetItem(task_id);
                if (task == null)
                {
                    ModelState.AddModelError("", "Не найдена задачас кодом " + task_id.ToString());
                    return RedirectToErrorPage(ModelState);
                }
                return View(task);
            }
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public ActionResult TaskEdit(CrmTaskViewModel itemEdit)
        {
            bool isAdd = itemEdit.task_id <= 0;
            if (ModelState.IsValid)
            {                
                ViewModel.AuBaseViewModel res = null;
                if (!isAdd)
                {
                    res = crmTaskService.Update(itemEdit, ModelState);
                }
                else
                {
                    res = crmTaskService.Insert(itemEdit, ModelState);
                }

                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании задачи");
                    //return View(itemEdit);
                    return RedirectToAction("TaskEdit", "Crm", new { task_id = itemEdit.task_id });
                }

                if (isAdd)
                    //return TaskEdit(((CrmTaskViewModel)res).task_id);
                    return RedirectToAction("TaskEdit", "Crm", new { task_id = ((CrmTaskViewModel)res).task_id });
                else
                    return RedirectToAction("TaskList", "Crm");
            }
            else
            {
                //return View(itemEdit);
                return RedirectToAction("TaskEdit", "Crm", itemEdit.task_id);
            }
        }
        
        #endregion

        #region TaskSaveBefore

        [HttpPost]
        public JsonResult TaskSaveBefore(int? task_id, int? task_upd_num)
        {
            if ((task_id.HasValue) && (task_upd_num.HasValue))
            {
                var res = crmTaskService.TaskSaveBefore(task_id.GetValueOrDefault(0), task_upd_num.GetValueOrDefault(0));
                if (res.Item1)
                    return Json(new { is_ok = true, upd_user = "", upd_date = DateTime.Now, mess = "ok" }, JsonRequestBehavior.AllowGet);
                string res_mess = "! Задача уже была изменена ранее: " + (String.IsNullOrEmpty(res.Item2) ? "[-]" : res.Item2) + " " + (res.Item3.HasValue ? (((DateTime)(res.Item3)).ToString("dd.MM.yyyy HH:mm")) : "[-]") + ". Все равно сохранить ?";
                return Json(new { is_ok = res.Item1, upd_user = res.Item2, upd_date = res.Item3, mess = res_mess }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { is_ok = true, upd_user = "", upd_date = DateTime.Now, mess = "ok" }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region MyTask
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [HttpGet]
        public ActionResult MyTaskList()
        {
            ViewBag.TaskGridColumnSettings = cabGridColumnUserSettingsService.GetList_byParent((int)Enums.CabGrid.TaskList);
            var gridUserSettings = cabGridUserSettingService.GetItem((int)Enums.CabGrid.TaskList);
            string gridSortOptions = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).sort_options : "";
            string gridFilterOptions = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).filter_options : "";
            ViewBag.TaskGridSortOptions = new JavaScriptSerializer().Serialize(gridSortOptions);
            ViewBag.TaskGridFilterOptions = new JavaScriptSerializer().Serialize(gridFilterOptions);
            ViewBag.TaskGridColumnWidths = gridUserSettings == null ? "" : ((CabGridUserSettingsViewModel)gridUserSettings).columns_options;
            ViewBag.TaskGridColorColumn = gridUserSettings != null ? ((CabGridUserSettingsViewModel)gridUserSettings).back_color_column_id : null;
            ViewBag.TaskGridColumnColorList = cabGridColumnService.GetList_Color((int)Enums.CabGrid.TaskList);
            return View();
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetMyTaskList([CustomDataSourceRequest]DataSourceRequest request, int role_id, string phrase)
        {
            var cab_user_id = CurrentUser.CabUserId;
            if (cab_user_id <= 0)
                return null;
            return Json(crmTaskService.GetList_byUser(cab_user_id, role_id, phrase).ToDataSourceResult(request));
        }

        #endregion

        #region TaskLog

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpGet]
        public ActionResult TaskLog()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetCrmLogList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(crmLogService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetTaskLogList([DataSourceRequest]DataSourceRequest request, int task_id)
        {
            return Json(crmLogService.GetList_byParent(task_id).ToDataSourceResult(request));
        }

        #endregion

        #region TaskArchive

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult TaskArchive()
        {
            ViewBag.TaskGridColumnSettings = cabGridColumnUserSettingsService.GetList_byParent((int)Enums.CabGrid.TaskList);
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetTaskArcList([CustomDataSourceRequest]DataSourceRequest request)
        {
            return Json(crmTaskService.GetList_Arc().ToDataSourceResult(request));
        }

        #endregion

        #region TaskRel

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetTaskRelList([DataSourceRequest]DataSourceRequest request, int task_id)
        {
            return Json(crmTaskRelService.GetList_byParent(task_id).ToDataSourceResult(request));
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public ActionResult TaskRelAdd(int parent_task_id, string child_task_num, int rel_type_id)
        {
            var res = crmTaskService.TaskRelAdd(parent_task_id, child_task_num, rel_type_id, ModelState);
            if (res)
            {
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
            }
                        
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public ActionResult TaskRelDel(int parent_task_id, int child_task_id, int rel_type_id)
        {
            var res = crmTaskService.TaskRelDel(parent_task_id, child_task_id, rel_type_id, ModelState);
            if (res)
            {
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = ModelState.GetErrorsString() }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Subtask

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetSubtaskList([CustomDataSourceRequest]DataSourceRequest request, int task_id)
        {
            return Json(crmSubtaskService.GetList_byParent(task_id).ToDataSourceResult(request));
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public ActionResult SubtaskAdd([DataSourceRequest] DataSourceRequest request, CrmSubtaskViewModel itemAdd, int taskId)
        {
            if (ModelState.IsValid)
            {
                itemAdd.task_id = taskId;
                var res = crmSubtaskService.Insert(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении подзадачи");
                    return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public ActionResult SubtaskEdit([DataSourceRequest] DataSourceRequest request, CrmSubtaskViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                var res = crmSubtaskService.Update(itemEdit, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании подзадачи");
                    return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        
        [HttpPost]
        public ActionResult SubtaskDel([DataSourceRequest]DataSourceRequest request, CrmSubtaskViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                var res = crmSubtaskService.Delete(itemDel, ModelState);
                if ((!res) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при удалении подзадачи");
                    return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }
        
        [HttpPost]
        public ActionResult SubtaskExportExcel(long task_id)
        {
            string reportTitle = "";

            IEnumerable<CrmSubtaskViewModel> subtask_list = crmSubtaskService.GetList_byParent(task_id).AsEnumerable().Cast<CrmSubtaskViewModel>();
            if (subtask_list == null)
                return null;
                        
            reportTitle = "Список подзадач";

            string fileName = "subtask_" + task_id.ToString() + "_" + CurrentUser.CabUserLogin + ".xls";
            string fileNameRus = reportTitle + ".xls";
            string filePath = @"c:\Release\Cabinet\Report\" + fileName;
            string contentType = "application/vnd.ms-excel";

            using (ExcelPackage p = new ExcelPackage())
            {
                p.Workbook.Properties.Author = "Аурит";
                p.Workbook.Properties.Title = reportTitle;

                p.Workbook.Worksheets.Add("Страница 1");
                ExcelWorksheet ws = p.Workbook.Worksheets[1];
                ws.Cells.Style.Font.Size = 11;
                ws.Cells.Style.Font.Name = "Calibri";

                int reportsfieldscount = 5;
                int colIndex = 1;
                int rowIndex = 2;
                int i = 1;

                reportsfieldscount = 6;
                colIndex = 1;
                rowIndex = 2;
                i = 1;

                using (var range = ws.Cells[1, 1, 1, reportsfieldscount])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Orange);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                while (i <= reportsfieldscount)
                {
                    ws.Column(1).Width = 40;
                    i++;
                }

                //Merging cells and create a center heading for out table
                ws.Cells[1, 1].Value = reportTitle;
                ws.Cells[1, 1, 1, reportsfieldscount].Merge = true;
                ws.Cells[1, 1, 1, reportsfieldscount].Style.Font.Bold = true;
                ws.Cells[1, 1, 1, reportsfieldscount].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                using (var range = ws.Cells[2, 1, 2, reportsfieldscount])
                {
                    range.Style.Font.Bold = false;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.ShrinkToFit = false;
                }

                ws.Cells[rowIndex, colIndex].Value = "№";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Статус";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Описание";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Создано";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Изменено";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;
                ws.Cells[rowIndex, colIndex].Value = "Автор";
                ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                colIndex++;

                foreach (var item in subtask_list)
                {

                    var col1 = item.task_num;
                    var col2 = item.state_name;
                    var col3 = item.subtask_text;
                    var col4 = item.crt_date;
                    var col5 = item.upd_date;
                    var col6 = item.owner_user_name;

                    colIndex = 1;
                    rowIndex++;

                    ws.Cells[rowIndex, colIndex].Value = col1;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = col2;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = col3;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = col4;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = col5;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    colIndex++;

                    ws.Cells[rowIndex, colIndex].Value = col6;
                    ws.Cells[rowIndex, colIndex].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    colIndex++;
                }

                // AutoFitColumns
                ws.Cells[ws.Dimension.Address].AutoFitColumns();

                Byte[] bin = p.GetAsByteArray();
                System.IO.File.WriteAllBytes(filePath, bin);
            }

            ViewBag.Message = "Файл отчета сохранен в " + filePath;

            return File(filePath, contentType, fileNameRus);
        }

        #endregion

        #region TaskNote

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult GetTaskNoteList([CustomDataSourceRequest]DataSourceRequest request, int task_id)
        {
            return Json(crmTaskNoteService.GetList_byParent(task_id).ToDataSourceResult(request));
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        public ActionResult TaskNoteAdd([DataSourceRequest] DataSourceRequest request, CrmTaskNoteViewModel itemAdd, int taskId)
        {
            if (ModelState.IsValid)
            {
                itemAdd.task_id = taskId;
                var res = crmTaskNoteService.Insert(itemAdd, ModelState);
                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при добавлении комментария");
                    return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
                }
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region TaskFile
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        public JsonResult GetCrmTaskFileList([CustomDataSourceRequest]DataSourceRequest request, int task_id)
        {
            return Json(crmTaskFileService.GetList_byParent(task_id).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public JsonResult GetCrmTaskFileList_forSubtask(int task_id)
        {
            return Json(crmTaskFileService.GetList_byParent(task_id), JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]                
        public ActionResult TaskFileSave(IEnumerable<HttpPostedFileBase> uplTaskFile, int task_id)        
        {
            // The Name of the Upload component is "uplTaskFile"
            if (uplTaskFile != null)
            {
                foreach (var file in uplTaskFile)
                {
                    //var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);
                    var res = crmTaskFileService.UploadFile(file, task_id, ModelState);
                    if ((res == null) || (!ModelState.IsValid))
                    {
                        if (ModelState.IsValid)
                            ModelState.AddModelError("", "Ошибка при добавлении файла");
                        return Content(ModelState.GetErrorsString());
                    }
                }
                // Return an empty string to signify success
                return Content("");
            }
            else
            {
                ModelState.AddModelError("", "Не найден файл для загрузки");
                return Content(ModelState.GetErrorsString());
            }

        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public JsonResult TaskFileRemove(int file_id)
        {
            var res = crmTaskFileService.RemoveFile(file_id, ModelState);
            if ((!res) || (!ModelState.IsValid))
            {
                if (ModelState.IsValid)
                    ModelState.AddModelError("", "Ошибка при удалении файла");
                return Json(new { err = true, mess = ModelState.GetErrorsString(), });
            }
            return Json(new { err = false, mess = "", });
        }

        #endregion

        #region TaskList1

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult TaskList1()
        {
            ViewBag.TaskFilter = crmTaskFilterService.GetTreeList(1);
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult TaskList2()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public PartialViewResult TaskListPartial()
        {            
            return PartialView();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult TaskListGridConfigSave(string data)
        {
            //Session["taskListGridConfig"] = data;
            cabGridUserSettingService.SaveGridOptions((int)Enums.CabGrid.TaskList, data);
            return new EmptyResult();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        public ActionResult TaskListGridConfigLoad(string data)
        {
            string grid_options = cabGridUserSettingService.GetGridOptions((int)Enums.CabGrid.TaskList);
            return Json(grid_options, JsonRequestBehavior.AllowGet);
            //return Json(Session["taskListGridConfig"], JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
