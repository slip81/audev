﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CabinetMvc.Util;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.HelpDesk;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public class HelpDeskController : BaseController
    {        

        [SysRoleAuthorize("Administrators", "Superadmin")]
        public ActionResult Index()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.HELP_DESK)]
        public ActionResult HelpDesk()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.HELP_DESK)]
        public ActionResult GetStoryList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(storyService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.HELP_DESK)]
        [HttpPost]
        public ActionResult StoryEdit([DataSourceRequest]DataSourceRequest request, long story_id, int crm_num, bool toTask)
        {
            StoryViewModel storyEdit = null;
            if (ModelState.IsValid)
            {                
                storyEdit = (StoryViewModel)storyService.GetItem(story_id);
                if (storyEdit != null)
                {
                    if (toTask)
                    {                        
                        /*
                        var res1 = crmTaskService.Insert_Story(story_id, ModelState);
                        if ((res1 != null) && (ModelState.IsValid))
                        {
                            task_id = ((CabinetMvc.ViewModel.Crm.CrmTaskViewModel)res1).task_id;
                            task_num = ((CabinetMvc.ViewModel.Crm.CrmTaskViewModel)res1).task_num;
                            ((StoryViewModel)storyEdit).task_id = task_id;
                            ((StoryViewModel)storyEdit).task_num = task_num;
                        }
                        */
                        CabinetMvc.ViewModel.Prj.PrjClaimViewModel res1 = (CabinetMvc.ViewModel.Prj.PrjClaimViewModel)prjClaimService.Insert_Story(story_id, ModelState);
                        if ((res1 != null) && (ModelState.IsValid))
                        {
                            storyEdit.claim_id = res1.claim_id;
                            storyEdit.claim_num = res1.claim_num;
                        }
                    }
                    ((StoryViewModel)storyEdit).crm_num = crm_num;
                    var res = storyService.Update(storyEdit, ModelState);
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new[] { storyEdit }, JsonRequestBehavior.AllowGet);
        }

        [SysRoleAuthorize("Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.HELP_DESK)]
        [HttpPost]
        public ActionResult StoryBatchDel([DataSourceRequest]DataSourceRequest request, int del_mode, string id_list)
        {
            string Mess = "";
            if (ModelState.IsValid)
            {                
                var serv_res = storyService.DeleteBatch(del_mode, id_list);                
                return Json(new[] { Mess }, JsonRequestBehavior.AllowGet);
            }
            return Json(new[] { Mess }, JsonRequestBehavior.AllowGet);
        }
        
    }
}
