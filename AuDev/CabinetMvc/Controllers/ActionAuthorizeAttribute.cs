﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

//using Ninject;
using CabinetMvc.Auth;
using CabinetMvc.ViewModel.User;

namespace CabinetMvc.Controllers
{
    public class ActionAuthorizeAttribute : AuthorizeAttribute
    {
        /*
        [Inject]
        public IAuthentication Auth { get; set; }
        public UserViewModel CurrentUser { get { return ((IUserProvider)Auth.CurrentUser.Identity).User; } }
        */

        private readonly string[] allowedroles;
        public ActionAuthorizeAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;

            //var user = CurrentUser;
            //var user = ((IUserProvider)httpContext.User.Identity).User;
            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var user = ((IUserProvider)auth.CurrentUser.Identity).User;
            
            if (allowedroles == null)
                return authorize;
            if (user == null)
                return authorize;
            if (user.IsAdmin)
            {
                authorize = true;
                return authorize;
            }
            if (String.IsNullOrEmpty(user.ActionList))
                return authorize;

            foreach (var role in allowedroles)
            {
                if (user.ActionList.Contains(role))
                {
                    authorize = true;
                    break;
                }                    
            }
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new HttpUnauthorizedResult();
        }
    }

}