﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Web.Mvc;
using System.Drawing;
using System.Web.Script.Serialization;
using System.IO;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using CabinetMvc.Util;
using CabinetMvc.Extensions;
using CabinetMvc.ViewModel.Update;
using CabinetMvc.ViewModel.Sys;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Controllers
{
    public partial class UpdateController : BaseController
    {
        #region UpdateSoft

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.UPDATE_SOFT)]        
        [HttpGet]
        public ActionResult UpdateSoftList()
        {
            var groupList = updateSoftGroupService.GetList();
            ViewData["UpdateSoftGroupList"] = groupList;
            ViewData["UpdateSoftGroupDefault"] = groupList.FirstOrDefault();
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.UPDATE_SOFT)]
        public ActionResult GetUpdateSoftList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(updateSoftService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_SOFT)]
        public ActionResult UpdateSoftAdd([DataSourceRequest] DataSourceRequest request, UpdateSoftViewModel itemAdd)
        {
            if (ModelState.IsValid)
            {
                var res = updateSoftService.Insert(itemAdd, ModelState);
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_SOFT)]
        public ActionResult UpdateSoftEdit([DataSourceRequest] DataSourceRequest request, UpdateSoftViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                var res = updateSoftService.Update(itemEdit, ModelState);
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]        
        [HttpPost]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_SOFT)]
        public ActionResult UpdateSoftDel([DataSourceRequest]DataSourceRequest request, UpdateSoftViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                var res = updateSoftService.Delete(itemDel, ModelState);
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region UpdateSoftVersion

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.UPDATE_SOFT)]
        public ActionResult GetUpdateSoftVersionList([DataSourceRequest]DataSourceRequest request, int softId)
        {
            return Json(updateSoftVersionService.GetList_byParent(softId).ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_SOFT)]
        [HttpPost]
        public ActionResult UpdateSoftVersionAdd([DataSourceRequest] DataSourceRequest request, UpdateSoftVersionViewModel itemAdd, int softId)
        {
            if (ModelState.IsValid)
            {
                itemAdd.soft_id = softId;

                string file_path = null;
                try
                {
                    file_path = (string)Session["updateFileUpload_path"];
                }
                catch
                {
                    file_path = null;
                }
                itemAdd.file_path = file_path;
                Session["updateFileUpload_path"] = null;

                string xml_file_path = null;
                try
                {
                    xml_file_path = (string)Session["updateXmlFileUpload_path"];
                }
                catch
                {
                    xml_file_path = null;
                }
                itemAdd.xml_file_path = xml_file_path;
                Session["updateXmlFileUpload_path"] = null;

                var res = updateSoftVersionService.Insert(itemAdd, ModelState);
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemAdd }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_SOFT)]
        [HttpPost]
        public ActionResult UpdateSoftVersionEdit([DataSourceRequest] DataSourceRequest request, UpdateSoftVersionViewModel itemEdit, int softId)
        {
            if (ModelState.IsValid)
            {
                itemEdit.soft_id = softId;
                
                string file_path = null;
                try
                {
                    file_path = (string)Session["updateFileUpload_path"];
                }
                catch
                {
                    file_path = null;
                }
                itemEdit.file_path = String.IsNullOrEmpty(file_path) ? itemEdit.file_path : file_path;
                Session["updateFileUpload_path"] = null;

                string xml_file_path = null;
                try
                {
                    xml_file_path = (string)Session["updateXmlFileUpload_path"];
                }
                catch
                {
                    xml_file_path = null;
                }
                itemEdit.xml_file_path = String.IsNullOrEmpty(xml_file_path) ? itemEdit.xml_file_path : xml_file_path;
                Session["updateXmlFileUpload_path"] = null;

                var res = updateSoftVersionService.Update(itemEdit, ModelState);
                return Json(new[] { res }.ToDataSourceResult(request, ModelState));
            }
            return Json(new[] { itemEdit }.ToDataSourceResult(request, ModelState));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_SOFT)]
        [HttpPost]
        public ActionResult UpdateSoftVersionDel([DataSourceRequest]DataSourceRequest request, UpdateSoftVersionViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                var res = updateSoftVersionService.Delete(itemDel, ModelState);
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region UpdateFileUpload

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_SOFT)]
        public ActionResult UpdateFileUpload(HttpPostedFileBase updateFileUpload)
        {
            var fileName = Path.GetFileName(updateFileUpload.FileName);
            var folder = Server.MapPath("~/App_Data/pub/" + System.Guid.NewGuid().ToString());
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            var physicalPath = Path.Combine(folder, fileName);

            updateFileUpload.SaveAs(physicalPath);

            Session["updateFileUpload_path"] = physicalPath;

            return Json(new { err = false }, "text/plain");
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_SOFT)]
        public ActionResult UpdateXmlFileUpload(HttpPostedFileBase updateXmlFileUpload)
        {
            var fileName = Path.GetFileName(updateXmlFileUpload.FileName);
            var folder = Server.MapPath("~/App_Data/pub/" + System.Guid.NewGuid().ToString());
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            var physicalPath = Path.Combine(folder, fileName);

            updateXmlFileUpload.SaveAs(physicalPath);

            Session["updateXmlFileUpload_path"] = physicalPath;

            return Json(new { err = false }, "text/plain");
        }

        #endregion

        #region UpdateBatch
                
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.UPDATE_BATCH)]
        [HttpGet]
        public ActionResult UpdateBatchList()
        {
            return View();
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.UPDATE_BATCH)]
        public ActionResult GetUpdateBatchList([DataSourceRequest]DataSourceRequest request)
        {
            return Json(updateBatchService.GetList().ToDataSourceResult(request));
        }

        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.UPDATE_BATCH)]
        [HttpGet]
        public ActionResult UpdateBatchEdit(int? batch_id)
        {
            if (batch_id.GetValueOrDefault(0) > 0)
            {
                var res = updateBatchService.GetItem((int)batch_id);
                if (res == null)
                {
                    ModelState.AddModelError("", "Не найден пакет с кодом " + batch_id.ToString());
                    return RedirectToErrorPage(ModelState);
                }                
                return View(res);
            }
            else
            {                
                return View(new UpdateBatchViewModel());
            }
        }


        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_BATCH)]
        [HttpPost]
        public ActionResult UpdateBatchEdit(UpdateBatchViewModel itemEdit)
        {
            if (ModelState.IsValid)
            {
                ViewModel.AuBaseViewModel res = null;
                if (itemEdit.batch_id > 0)
                {
                    res = updateBatchService.Update(itemEdit, ModelState);
                }
                else
                {
                    res = updateBatchService.Insert(itemEdit, ModelState);
                }

                if ((res == null) || (!ModelState.IsValid))
                {
                    if (ModelState.IsValid)
                        ModelState.AddModelError("", "Ошибка при редактировании пакета");
                    return RedirectToErrorPage(ModelState);
                }
                return RedirectToAction("UpdateBatchList", "Update");
            }
            return RedirectToErrorPage(ModelState);
        }
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermEdit(Enums.AuthPartEnum.UPDATE_BATCH)]
        [HttpPost]
        public ActionResult UpdateBatchDel([DataSourceRequest]DataSourceRequest request, UpdateBatchViewModel itemDel)
        {
            if (ModelState.IsValid)
            {
                var res = updateBatchService.Delete(itemDel, ModelState);
                return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
            }

            return Json(new[] { itemDel }.ToDataSourceResult(request, ModelState));
        }

        #endregion

        #region UpdateBatchItem
        
        [SysRoleAuthorize("Administrators", "Superadmin")]
        [AuthUserPermView(Enums.AuthPartEnum.UPDATE_BATCH)]
        public ActionResult GetUpdateBatchItemList([DataSourceRequest]DataSourceRequest request, int batch_id)
        {
            return Json(updateBatchItemService.GetList_byParent(batch_id).ToDataSourceResult(request));
        }

        #endregion
    }
}