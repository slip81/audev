﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Reflection;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using Newtonsoft.Json;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Sys;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Defect;
using CabinetMvc.ViewModel.User;
using CabinetMvc.Util;
using CabinetMvc.Auth;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.Extensions
{
    public static partial class HtmlExtensions
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        #region Grid

        public static void ColumnsForModel<T>(GridColumnFactory<T> gridColumns,
            IEnumerable<CabGridColumnUserSettingsViewModel> columnSettings,
            string columnWidths,
            bool insideTemplate = false
            )
            where T : AuBaseViewModel
        {
            List<KendoGridColumn> cols_deser = null;
            if (!String.IsNullOrEmpty(columnWidths))
            {
                cols_deser = JsonConvert.DeserializeObject<List<KendoGridColumn>>(columnWidths);                
            }

            foreach (var setting in columnSettings.Where(ss => ss.is_visible).OrderBy(ss => ss.column_num))
            {
                var col_bound = gridColumns.Bound(p => p);
                var col = col_bound.Column;                
                col.Member = setting.column_name;
                col.Title = setting.column_name_rus;

                string col_deser_width = "";
                if ((cols_deser != null) && (cols_deser.Count > 0))
                {
                    var col_deser = cols_deser.Where(ss => ss.field.Trim().ToLower().Equals(setting.column_name.Trim().ToLower())).FirstOrDefault();
                    if (col_deser != null)
                    {
                        col_deser_width = col_deser.width;
                        if (!String.IsNullOrEmpty(col_deser_width))
                        {
                            if (!col_deser_width.EndsWith("px", true, System.Globalization.CultureInfo.CurrentCulture))
                                col_deser_width = col_deser_width + "px";
                            col.Width = col_deser_width;
                        }
                    }
                }
                
                col.Width = !String.IsNullOrEmpty(col_deser_width) ? col_deser_width : (setting.width + "px");
                col.Format = String.IsNullOrEmpty(setting.format) ? "" : setting.format;
                if (!(String.IsNullOrEmpty(setting.title)))
                {
                    // !!!
                    // setting.title = HttpUtility.HtmlEncode(setting.title);
                    col.HtmlAttributes.Add("title", (insideTemplate ? setting.title.Replace(@"#", @"\\#") : setting.title));
                }
                
                bool hasTemplate = !String.IsNullOrEmpty(setting.templ);
                col.ClientTemplate = hasTemplate ? setting.templ : "";
                // !!!
                // col.ClientTemplate = HttpUtility.HtmlEncode(col.ClientTemplate);
                if ((insideTemplate) && (hasTemplate))
                {
                    col.ClientTemplate = col.ClientTemplate.Replace(@"#", @"\\#");
                }

                col.Filterable = setting.is_filterable;
                if (setting.is_filterable)
                {
                    col.FilterableSettings.CellSettings.Operator = "contains";
                }

                col.Sortable = setting.is_sortable;                
            }
        }

        #endregion

        #region Menu

        public static MenuBuilder CabMenu(this HtmlHelper helper, string cab_node, bool isSideMenu)
        {
            //logger.Info("CabMenu start");

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            //var currUser = auth == null ? null : ((IUserProvider)auth.CurrentUser.Identity).User;
            var currUser = auth == null ? null : (auth.CurrentUser == null ? null : ((IUserProvider)auth.CurrentUser.Identity).User);
            var userName = currUser == null ? "[не определен]" : currUser.CabUserName;
            bool isAdmin = currUser == null ? false : currUser.IsAdmin;

            UrlHelper urlHelper = new UrlHelper(helper.ViewContext.RequestContext);
            try
            {
                var res = helper.Kendo().Menu().Name(isSideMenu ? "sideMenu" : "MainMenu")
                            .SecurityTrimming(s => { s.Enabled(true); s.HideParent(true); })
                            .BindTo(cab_node, (menu, node) =>
                            {
                                Kendo.Mvc.SiteMapNode menuNode = (Kendo.Mvc.SiteMapNode)node;

                                bool is_separator = CabinetUtil.SiteMapNode_IsSeparator(menuNode);
                                if (is_separator)
                                {
                                    menu.Text = "<hr style='margin-bottom: 2px !important; margin-top: 2px !important;' >";
                                    menu.Encoded = false;
                                    menu.Enabled = false;
                                }
                                else
                                {
                                    string img_path = CabinetUtil.SiteMapNode_GetImagePath(menuNode);

                                    //menu.ImageUrl = String.IsNullOrEmpty(img_path) ? "" : urlHelper.Content("~/Images/new/" + img_path);
                                    if (img_path.EndsWith(".png"))
                                    {
                                        img_path = img_path.Substring(0, img_path.Length - 4);
                                    }
                                    img_path = "cab-png cab-png-" + img_path;
                                    menu.SpriteCssClasses = img_path;

                                    var route_values = CabinetUtil.SiteMapNode_GetRouteValues(menuNode);
                                    if (route_values.Item1)
                                    {
                                        menu.RouteValues.Add(route_values.Item2, route_values.Item3);
                                    }

                                    Tuple<bool, string> is_external_link = CabinetUtil.SiteMapNode_IsExternalLink(menuNode);
                                    if (is_external_link.Item1)
                                    {
                                        menu.Url = is_external_link.Item2;
                                        menu.LinkHtmlAttributes.Add(new KeyValuePair<string, object>("target", "_blank"));
                                    }

                                    var is_action_fake = CabinetUtil.SiteMapNode_IsActionFake(menuNode);
                                    if (is_action_fake)
                                    {
                                        menu.HtmlAttributes.Add("class", "action-fake-menu");
                                    }
                                }
                            }
                            )
                            .Events(ev => ev.Select("onMenuItemSelect"))
                    ;

                if (isSideMenu)
                {
                    res.Orientation(MenuOrientation.Vertical).Direction(MenuDirection.Left)
                        .HtmlAttributes(new { @class = "noborder", @style = "overflow: visible;" });                    
                }
                else
                {
                    if (isAdmin)
                    {
                        res.Items(itemCustom => itemCustom.Add().Text("Мои страницы")
                            //.ImageUrl(urlHelper.Content("~/Images/new/my.png"))                        
                            .SpriteCssClasses("cab-png cab-png-my")
                            .Action("UserProfile", "User")
                            .Items(itemCustomContent =>
                            {
                                var userMenuService = DependencyResolver.Current.GetService<IUserMenuService>();
                                var customMenu = userMenuService.GetList_byUserName(currUser.CabUserLogin).ToList();
                                if ((customMenu != null) && (customMenu.Count > 0))
                                {
                                    foreach (var customMenuItem in customMenu)
                                    {
                                        string img = customMenuItem.img;
                                        if (img.EndsWith(".png"))
                                        {
                                            img = img.Substring(0, img.Length - 4);
                                        }
                                        img = "cab-png cab-png-" + img;
                                        itemCustomContent.Add()
                                            .Text(customMenuItem.title)
                                            //.ImageUrl(urlHelper.Content("~/Images/new/" + img))
                                            .SpriteCssClasses(img)
                                            .Action(customMenuItem.action, customMenuItem.controller);
                                    }
                                }
                                
                                itemCustomContent.Add()
                                    .Text("Настроить")
                                    //.ImageUrl(urlHelper.Content("~/Images/new/user-profile-edit.png"))
                                    .SpriteCssClasses("cab-png cab-png-user-profile-edit")
                                    .Action("UserProfile", "User");
                            }));
                    }

                    if (isAdmin)
                    {
                        res.Items(itemHelp => itemHelp.Add().Text(" ")
                            .SpriteCssClasses("cab-png cab-png-help")
                            .Action("CabHelp", "Help")
                            .HtmlAttributes(new { @style = "float:right;", @title = "Справка" }));
                    }

                    res.Items(itemUser => itemUser.Add().Text(userName)
                        //.ImageUrl(urlHelper.Content("~/Images/new/user-profile.png"))
                        .SpriteCssClasses("cab-png cab-png-user-profile")
                        .Items(itemSubUser => {                            
                            if (isAdmin)
                            {
                                /*
                                itemSubUser.Add().Text("Справка")
                                    .SpriteCssClasses("cab-png cab-png-help")
                                    .Action("CabHelp", "Cabinet");
                                */
                                itemSubUser.Add().Text("Профиль")                                    
                                    .SpriteCssClasses("cab-png cab-png-user-profile-edit")
                                    .Action("UserProfile", "User");
                                itemSubUser.Add().Text("Очистить кэш")
                                    .SpriteCssClasses("cab-png cab-png-cross")
                                    .HtmlAttributes(new { @class = "clear-cache-menu" })
                                    //.Action("UserProfile", "User")
                                    ;
                            }
                            itemSubUser.Add().Text("Сменить пароль")
                                .SpriteCssClasses("cab-png cab-png-change")
                                .Action("ChangePassword", "User");
                            itemSubUser.Add().Text("Выход")
                                //.ImageUrl(urlHelper.Content("~/Images/new/exit.png"))
                                .SpriteCssClasses("cab-png cab-png-exit")
                                .Action("Logout", "User");                                
                        })
                        .HtmlAttributes(new { @style = "float:right;" }));

                    res.HtmlAttributes(new { @style = "font-size: 12px;" })
                        .Animation(anim =>
                        {
                            anim.Open(open =>
                            {
                                open.Expand(ExpandDirection.Vertical);
                                open.Fade(FadeDirection.In);
                            }
                            );
                        })
                        .HoverDelay(100);
                }

                urlHelper = null;
                //logger.Info("CabMenu end");
                return res;
            }
            finally
            {
                urlHelper = null;
            }
        }

        #endregion 

        #region SidePanel

        public static PanelBarBuilder CabSidePanel(this HtmlHelper helper, string menu_name = null, string menu_header = null)
        {
            //logger.Info("CabSidePanel start");

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var currUser = auth == null ? null : ((IUserProvider)auth.CurrentUser.Identity).User;
            var clientInfo = currUser == null ? null : currUser.clientInfo;

            var user_name = currUser == null ? "-" : currUser.CabUserName;
            var client_id = clientInfo == null ? 0 : clientInfo.client_id;

            var client_name = clientInfo == null ? "" : clientInfo.client_name;
            var client_address = clientInfo == null ? "" : clientInfo.client_address;
            var client_phone = clientInfo == null ? "" : clientInfo.client_phone;
            var client_contact = clientInfo == null ? "" : clientInfo.client_contact;
            var sales_count = clientInfo == null ? 0 : clientInfo.sales_count;
            var installed_service_cnt = clientInfo == null ? 0 : clientInfo.installed_service_cnt;
            var ordered_service_cnt = clientInfo == null ? 0 : clientInfo.ordered_service_cnt;    
            
            var res = helper.Kendo().PanelBar()
                    .Name("sideMenuPanel")
                    .ExpandMode(PanelBarExpandMode.Multiple)
                    ;

            UrlHelper urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

            if (!String.IsNullOrEmpty(menu_name))
            {
                res.Items(panelbar =>
                    {
                        panelbar.Add().Text(String.IsNullOrEmpty(menu_header) ? "Меню" : menu_header)
                            .Expanded(true)
                            .Content(helper.CabMenu(menu_name, true).ToHtmlString());
                    });
            }

            string client_name_link = client_name;
            if ((currUser != null) && (currUser.IsAdmin))
            {
                client_name_link = "<a href='#' id='btn-client-change' title='Сменить организацию'>" + client_name + "</a>";
                //client_name_link += "<a href='/ClientEdit?client_id=" + client_id.ToString() + "' id='btn-client-edit' title='Данные клиента'><img title='Данные клиента' alt='Данные клиента' src='" + urlHelper.Content("~/Images/new/cardclient.png") +"' height='13' style='margin-left: .5em;' ></a>";
                // !!!
                client_name_link += "<a href='/ClientEdit?client_id=" + client_id.ToString() + "' id='btn-client-edit' title='Данные клиента'><span class='cab-png cab-png-cardclient' title='Данные клиента' style='margin-left: .5em; height: 15px;' ></span></a>";
            }
            

            res.Items(panelbar =>
            {   
                /*
                panelbar.Add().Text("Профиль")
                    .Expanded(true)
                    .Content("<dl id='cab-client-info' class='cab-side-info'>"
                    + "<dt>Организация</dt>"
                    + "<dd style='text-align:right'>" + client_name_link + "</dd>"
                    + "<dt>Контакт</dt>"
                    + "<dd style='text-align:right'>" + contact_person + "</dd>"
                    + "<dt>Телефон</dt>"
                    + "<dd style='text-align:right'>" + phone + "</dd>"
                    + "<dt>Регион</dt>"
                    + "<dd style='text-align:right'>" + region_name + "</dd>"
                    + "<dt>Город</dt>"
                    + "<dd style='text-align:right'>" + city_name + "</dd>"
                    + "</dl>"
                    );
                */

                string content = "<div id='cab-client-info' class='cab-side-info'>"
                    + "<p>" + client_name_link + "</p>";

                if (!currUser.IsAdmin)
                {

                    if (!String.IsNullOrEmpty(client_address))
                        content += "<p>" + client_address.Trim() + "</p>";
                    if (!String.IsNullOrEmpty(client_phone))
                        content += "<p>" + client_phone.Trim() + "</p>";
                    if (!String.IsNullOrEmpty(client_contact))
                        content += "<p>" + client_contact.Trim() + "</p>";

                    content += "<p>" + "Торговых точек: " + sales_count.ToString().Trim() + "</p>";
                    content += "<p>" + "Услуг: " + installed_service_cnt.ToString().Trim() + "</p>";

                    if (ordered_service_cnt.GetValueOrDefault(0) > 0)
                        content += "<p>" + "Заказано услуг: " + ordered_service_cnt.ToString().Trim() + "</p>";
                }

                content += "</div>";

                panelbar.Add().Text("Профиль")
                    .Expanded(true)
                    .Content(content);
                /*
                if ((currUser.IsAdmin) && ((currUser.EventOverdueCount > 0) || (currUser.EventStrategCount > 0)))
                {
                    content = "<div id='cab-event-info' class='cab-side-info bg-danger'>";
                    if (currUser.EventOverdueCount > 0)
                        content += "<p>Просроченных: <strong><a class='text-danger pull-right' href='/Planner'>" + currUser.EventOverdueCount.ToString() + "</a></strong></p>";
                    if (currUser.EventStrategCount > 0)
                        content += "<p>Стратегических: <strong><a class='text-danger pull-right' href='/Planner'>" + currUser.EventStrategCount.ToString() + "</a></strong></p>";

                    panelbar.Add().Text("Задачи")
                        .Expanded(true)
                        .Content(content);
                }
                */
            })
           ;

            //logger.Info("CabSidePanel end");
            return res;
        }

        public static PanelBarBuilder CabSidePanel_Crm(this HtmlHelper helper, string menu_name = null, string menu_header = null)
        {
            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var currUser = auth == null ? null : ((IUserProvider)auth.CurrentUser.Identity).User;            

            var res = helper.Kendo().PanelBar()
                    .Name("sideMenuPanel")
                    .ExpandMode(PanelBarExpandMode.Multiple)
                    ;

            if (!String.IsNullOrEmpty(menu_name))
            {
                res.Items(panelbar =>
                {
                    panelbar.Add().Text(String.IsNullOrEmpty(menu_header) ? "Меню" : menu_header)
                        .Expanded(true)
                        .Selected(false)
                        .Content(helper.CabMenu(menu_name, true).ToHtmlString());
                });
            }

            res.Items(panelbar =>
            {
                panelbar.Add().Text("Календарь")
                    .Expanded(true)
                    .Selected(false)                    
                    .Content(helper.Kendo().Calendar().Name("sideCalendar").Footer(" ")
                    .Events(ev => ev.Change("onSideCalendarChange"))
                    .HtmlAttributes(new { @style = "width: 100%;" })                    
                    .ToHtmlString()
                    );
            })
           ;

            return res;
        }

        #endregion

        #region KendoCheckBox

        public static CheckBoxBuilder KendoCheckBox(this HtmlHelper helper, string name, string caption, bool isChecked, bool isEnabled = true)
        {                                  
            return helper.Kendo().CheckBox().Name(name).Checked(isChecked).Label(caption)
                .Enable(isEnabled)                
                .HtmlAttributes(new { @class = "k-checkbox", @id = name, });
        }

        #endregion

        #region DiscountChart

        public static ChartBuilder<BusinessSummaryGraphViewModel> DiscountChart_Simple(this HtmlHelper helper,
            string dataBindingEventHandler, 
            string dataBoundEventHandler, 
            bool visible)
        {
            var res = helper.Kendo().Chart<BusinessSummaryGraphViewModel>()
                                                .Name("businessGraph")
                                                .Transitions(false)
                                                .Title("График продаж")
                                                .Legend(l => l.Visible(false))
                                                .ChartArea(c => c.Margin(10, 8, 0, 10).Background("transparent"))
                                                .SeriesDefaults(s => s
                                                    .Area()
                                                    .Line(l => l.Style(ChartAreaStyle.Smooth))
                                                    .Stack(true))
                                                .CategoryAxis(a => a
                                                    .Categories(m => m.date_beg_date)
                                                    .Labels(l => l.Step(30).Format("{0:dd.MM.yy}"))
                                                    .MajorGridLines(l => l.Visible(false))
                                                    )
                                                       .Tooltip(t => t.Visible(true).Template("#= series.name # <br /> #=kendo.format('{0:dd.MM.yy}',category)#: #= value #"))
                                               .Series(series =>
                                               {
                                                   series.Area(model => model.trans_sum_total).Name("Сумма продаж");
                                               })
                                                .ValueAxis(v => v.Numeric().Labels(l => l.Step(2)))
                                                .AutoBind(visible)
                                                .DataSource(ds => { ds.Read("GetBusinessSummaryGraph", "Card"); ds.Events(dse => dse.RequestStart(dataBindingEventHandler)); })                                                
                                                .Events(ev => { ev.DataBound(dataBoundEventHandler); })
                                                ;
            if (!visible)
                res = res.HtmlAttributes(new { @class = "hidden" });

            return res;
        }

        public static ChartBuilder<BusinessSummaryGraph2ViewModel> DiscountChart_Stacked(this HtmlHelper helper,
            string dataBindingEventHandler,
            string dataBoundEventHandler,
            bool visible)
        {
            var res = helper.Kendo().Chart<BusinessSummaryGraph2ViewModel>()
                .Name("businessGraph2")
                .Title("Скидки и бонусы")
                .Legend(legend => legend
                    .Position(ChartLegendPosition.Top)
                 )
                .ChartArea(chartArea => chartArea
                    .Background("transparent")
                )
                .SeriesDefaults(sd => sd.Bar().Stack(true))
                .Series(series =>
                {
                    /*
                    series.Column(ss => ss.trans_sum_total).Name("Всего");
                    series.Column(ss => ss.trans_sum_discount).Name("Скидки");
                    series.Column(ss => ss.trans_sum_bonus_for_pay).Name("Бонусов потрачено");
                    series.Column(ss => ss.trans_sum_bonus_for_card).Name("Бонусов накоплено");
                    */
                    series.Bar(ss => ss.trans_sum_total).Name("Всего");
                    series.Bar(ss => ss.trans_sum_discount).Name("Скидки");
                    series.Bar(ss => ss.trans_sum_bonus_for_pay).Name("Бонусов потрачено");
                    series.Bar(ss => ss.trans_sum_bonus_for_card).Name("Бонусов накоплено");
                })                
                .CategoryAxis(axis => axis
                    .Name("cat-axis")
                    .Categories(ss => ss.date_beg_date)
                    .Labels(lbl => { lbl.Format("{0:dd.MM.yy}"); })
                    .MajorGridLines(lines => lines.Visible(false))
                )
                .ValueAxis(axis => axis
                    .Numeric()
                    .Labels(labels => labels.Format("{0:N0}"))
                    .MajorUnit(3000)
                    .Line(line => line.Visible(false))
                )
                .Tooltip(tooltip => tooltip
                    .Visible(true)
                    .Format("{0}")
                    .Template("#= series.name #: #= value #")
                )                
                .DataSource(ds => { ds.Read("GetBusinessSummaryGraph2", "Card"); ds.Events(dse => dse.RequestStart(dataBindingEventHandler)); })
                .AutoBind(visible)
                .Events(ev => { ev.DataBound(dataBoundEventHandler); })
            ;

            if (!visible)
                res = res.HtmlAttributes(new { @class = "hidden" });

            return res;
        }

        #endregion
    }

}