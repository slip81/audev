﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Linq;
using CabinetMvc.ViewModel.Cabinet;

namespace CabinetMvc.Extensions
{
    public static class ModelStateExtensions
    {
        public static IEnumerable<string> GetErrors(this ModelStateDictionary modelState)
        {
            return modelState.Keys.SelectMany(key => modelState[key].Errors.Select(ss => ss.ErrorMessage));
        }

        public static string GetErrorsString(this ModelStateDictionary modelState)
        {
            var res = string.Join("; ", modelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
            return String.IsNullOrEmpty(res) ? null : res;
        }
    }
}