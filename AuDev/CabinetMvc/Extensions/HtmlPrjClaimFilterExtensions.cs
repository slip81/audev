﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Reflection;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Prj;
using CabinetMvc.Controllers;

using AuDev.Common.Util;

namespace CabinetMvc.Extensions
{
    public static partial class HtmlExtensions
    {
        public static string GetPrjClaimFilterLayout(this HtmlHelper html, PrjClaimFilterViewModel model, BaseController baseController, string scriptName)
        {
            if ((model == null) || (model.filter_items == null) || (model.filter_items.Count() <= 0))
                return "Не найдены элементы фильтра";

            //var cache = baseController.CabCache;
            string res = "";            
            string checkbox1NamePrefix = "chb1_";
            string opTypeNamePrefix = "op_";
            string controlNamePrefix = "pcf_";            
            string checkbox2NamePrefix = "chb2_";
            string dataFieldId = null;
            string[] fieldNameValueArray = null;

            List<string> field_name_value_splitted = null;

            IEnumerable<PrjClaimFilterOpTypeViewModel> cacheOpTypeValueFull = (IEnumerable<PrjClaimFilterOpTypeViewModel>)baseController.GetType().BaseType.GetProperty("cachedOpTypeList", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(baseController, null);
            IEnumerable<PrjClaimFilterOpTypeViewModel> cacheOpTypeValue = null;

            CheckBoxBuilder kendoCheckbox1 = null;
            DropDownListBuilder kendoDropDownList1 = null;
            MultiSelectBuilder kendoMultiSelect = null;
            TextBoxBuilder<string> kendoTextBox = null;
            DatePickerBuilder kendoDatePicker1 = null;
            DatePickerBuilder kendoDatePicker2 = null;
            NumericTextBoxBuilder<decimal> kendoNumericTextBox1 = null;
            NumericTextBoxBuilder<decimal> kendoNumericTextBox2 = null;
            DropDownListBuilder kendoDropDownList2 = null;
            CheckBoxBuilder kendoCheckbox2 = null;

            foreach (var filter_item in model.filter_items.OrderBy(ss => ss.ord))
            {
                dataFieldId = "data-field-id='" + filter_item.field_id.ToString() + "'";
                res += "<div " + dataFieldId + " class='prj-claim-filter-items'>";
                fieldNameValueArray = String.IsNullOrWhiteSpace(filter_item.field_name_value) ? null : filter_item.field_name_value.Split(';');

                kendoCheckbox1 = null;
                kendoDropDownList1 = null;
                kendoMultiSelect = null;
                kendoTextBox = null;
                kendoDatePicker1 = null;
                kendoDatePicker2 = null;
                kendoNumericTextBox1 = null;
                kendoNumericTextBox2 = null;
                kendoDropDownList2 = null;
                kendoCheckbox2 = null;

                kendoCheckbox1 = html.KendoCheckBox(checkbox1NamePrefix + filter_item.field_name, filter_item.field_name_rus, filter_item.is_field_in_filter)
                    .HtmlAttributes(new { @style = "width: 140px;", @class = "k-checkbox prj-claim-filter-item", @data_control_type = "1", @data_field_type = "is_field_in_filter" });

                kendoCheckbox2 = html.KendoCheckBox(checkbox2NamePrefix + filter_item.field_name, "значение фиксировано", filter_item.is_value_fixed == true)
                    .HtmlAttributes(new { @style = "width: 140px;", @class = "k-checkbox prj-claim-filter-item", @data_control_type = "1", @data_field_type = "is_value_fixed" });                

                switch ((Enums.PrjClaimFieldTypeEnum)filter_item.field_type_id)
                {
                    case Enums.PrjClaimFieldTypeEnum.REF:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.EQ || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.NEQ);
                        object cachePropValue = baseController.GetType().BaseType.GetProperty(filter_item.cache_name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(baseController, null);
                        kendoMultiSelect = html.Kendo().MultiSelect()
                                                            .Placeholder("...")
                                                            .AutoClose(false)
                                                            .AutoBind(true)
                                                            .BindTo((System.Collections.IEnumerable)cachePropValue)
                                                            .Name(controlNamePrefix + filter_item.field_name)
                                                            .DataTextField(filter_item.cache_text_field)
                                                            .DataValueField(filter_item.cache_value_field)
                                                            //.Value(filter_item.field_name_value)
                                                            .Value(fieldNameValueArray)
                                                            .HtmlAttributes(new { @id = controlNamePrefix + filter_item.field_name, @class = "form-control input-sm prj-claim-filter-item", @style = "min-width: 250px;", @data_control_type = "2", @data_field_type = "field_name_value" })
                                                            ;
                        break;
                    case Enums.PrjClaimFieldTypeEnum.STRING:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.EQ || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.NEQ
                            || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.CONTAINS || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.NOT_CONTAINS);
                        kendoTextBox = html.Kendo().TextBox().Name(controlNamePrefix + filter_item.field_name).Value(filter_item.field_name_value)
                            .HtmlAttributes(new { @id = controlNamePrefix + filter_item.field_name, @class = "form-control input-sm prj-claim-filter-item", @style = "min-width: 250px;", @data_control_type = "3", @data_field_type = "field_name_value" });
                        break;
                    case Enums.PrjClaimFieldTypeEnum.DATE:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id != (int)Enums.PrjClaimFilterOpTypeEnum.CONTAINS && ss.op_type_id != (int)Enums.PrjClaimFilterOpTypeEnum.NOT_CONTAINS);
                        field_name_value_splitted = filter_item.field_name_value != null ? filter_item.field_name_value.Split(';').ToList() : null;
                        kendoDatePicker1 = html.Kendo().DatePicker().Name(controlNamePrefix + filter_item.field_name + "_1")
                                                    .Value(((field_name_value_splitted != null) && (field_name_value_splitted.Count > 0) && (field_name_value_splitted[0] != null)) ? field_name_value_splitted[0] : null)
                                                    .HtmlAttributes(new { @type = "text", @class = "form-control input-sm prj-claim-filter-item", @id = controlNamePrefix + filter_item.field_name + "_1", @style = "width: 120px;", @data_control_type = "4", @data_field_type = "field_name_value" });
                        kendoDatePicker2 = html.Kendo().DatePicker().Name(controlNamePrefix + filter_item.field_name + "_2")
                                                    .Value(((field_name_value_splitted != null) && (field_name_value_splitted.Count > 1) && (field_name_value_splitted[1] != null)) ? field_name_value_splitted[1] : null)
                                                    // !!!
                                                    .Enable(((filter_item.op_type_id.GetValueOrDefault(0) == 7) || (filter_item.op_type_id.GetValueOrDefault(0) == 8)))
                                                    .HtmlAttributes(new { @type = "text", @class = "form-control input-sm prj-claim-filter-item", @id = controlNamePrefix + filter_item.field_name + "_2", @style = "width: 120px;", @data_control_type = "4", @data_field_type = "field_name_value" });
                        break;
                    case Enums.PrjClaimFieldTypeEnum.NUMERIC:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id != (int)Enums.PrjClaimFilterOpTypeEnum.CONTAINS && ss.op_type_id != (int)Enums.PrjClaimFilterOpTypeEnum.NOT_CONTAINS);
                        field_name_value_splitted = filter_item.field_name_value != null ? filter_item.field_name_value.Split(';').ToList() : null;
                        kendoNumericTextBox1 = html.Kendo().NumericTextBox<decimal>().Name(controlNamePrefix + filter_item.field_name + "_1")
                            //.Value(String.IsNullOrWhiteSpace(filter_item.field_name_value) ? null : (decimal?)Convert.ToDecimal(filter_item.field_name_value))
                            .Value(((field_name_value_splitted != null) && (field_name_value_splitted.Count > 0) && (field_name_value_splitted[0] != null)) ? (decimal?)Convert.ToDecimal(field_name_value_splitted[0]) : null)
                            .Format("#.00")
                            .HtmlAttributes(new { @type = "text", @class = "form-control input-sm prj-claim-filter-item", @id = controlNamePrefix + filter_item.field_name + "_1", @style = "display: inline !important;width: 120px;", @data_control_type = "5", @data_field_type = "field_name_value" })
                            ;
                        kendoNumericTextBox2 = html.Kendo().NumericTextBox<decimal>().Name(controlNamePrefix + filter_item.field_name + "_2")
                            //.Value(String.IsNullOrWhiteSpace(filter_item.field_name_value) ? null : (decimal?)Convert.ToDecimal(filter_item.field_name_value))
                            .Value(((field_name_value_splitted != null) && (field_name_value_splitted.Count > 1) && (field_name_value_splitted[1] != null)) ? (decimal?)Convert.ToDecimal(field_name_value_splitted[1]) : null)
                            .Format("#.00")
                            // !!!
                            .Enable(((filter_item.op_type_id.GetValueOrDefault(0) == 7) || (filter_item.op_type_id.GetValueOrDefault(0) == 8)))
                            .HtmlAttributes(new { @type = "text", @class = "form-control input-sm prj-claim-filter-item", @id = controlNamePrefix + filter_item.field_name + "_2", @style = "display: inline !important;width: 120px;", @data_control_type = "5", @data_field_type = "field_name_value" })
                            ;
                        break;
                    case Enums.PrjClaimFieldTypeEnum.BOOLEAN:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.EQ || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.NEQ);
                        kendoDropDownList2 = html.Kendo()
                            .DropDownList()
                            .Name(controlNamePrefix + filter_item.field_name)
                            .BindTo(new List<SimpleCombo>() { new SimpleCombo() { obj_id = 1, obj_name = "Да" }, new SimpleCombo() { obj_id = 2, obj_name = "Нет" } })
                            .DataTextField("obj_name")
                            .DataValueField("obj_id")
                            .Value(filter_item.field_name_value)
                            .HtmlAttributes(new { @id = controlNamePrefix + filter_item.field_name, @class = "form-control input-sm prj-claim-filter-item", @style = "width: 250px;", @data_control_type = "6", @data_field_type = "field_name_value" })
                            ;                        
                        break;
                    default:
                        break;
                }

                kendoDropDownList1 = html.Kendo()
                    .DropDownList()
                    .Name(opTypeNamePrefix + filter_item.field_name)
                    .BindTo(cacheOpTypeValue)
                    .DataTextField("op_type_symbol")
                    .DataValueField("op_type_id")
                    .Value(filter_item.op_type_id.GetValueOrDefault(0) > 0 ? filter_item.op_type_id.ToString() : "1")                    
                    .HtmlAttributes(new { @id = opTypeNamePrefix + filter_item.field_name, @class = "form-control input-sm prj-claim-filter-item", @style = "width: 80px;", @data_control_type = "6", @data_field_type = "op_type_id" })
                    ;
                switch ((Enums.PrjClaimFieldTypeEnum)filter_item.field_type_id)
                {
                    case Enums.PrjClaimFieldTypeEnum.DATE:
                        kendoDropDownList1.Events(ev => ev.Change(scriptName + ".onOpTypeForDatePickerChange('" + controlNamePrefix + filter_item.field_name + "_1','" + controlNamePrefix + filter_item.field_name + "_2')"));
                        break;
                    case Enums.PrjClaimFieldTypeEnum.NUMERIC:
                        kendoDropDownList1.Events(ev => ev.Change(scriptName + ".onOpTypeForNumericChange('" + controlNamePrefix + filter_item.field_name + "_1','" + controlNamePrefix + filter_item.field_name + "_2')"));
                        break;
                    default:
                        break;
                }                

                res += kendoCheckbox1 != null ? ("<div class='form-group'>" + kendoCheckbox1.ToHtmlString() + "</div>") : "";
                res += kendoDropDownList1 != null ? ("<div class='form-group'>" + kendoDropDownList1.ToHtmlString() + "</div>") : "";

                res += kendoMultiSelect != null ? ("<div class='form-group'>" + kendoMultiSelect.ToHtmlString() + "</div>") : "";
                res += kendoTextBox != null ? ("<div class='form-group'>" + kendoTextBox.ToHtmlString() + "</div>") : "";
                res += kendoDatePicker1 != null ? ("<div class='form-group'>" + kendoDatePicker1.ToHtmlString() + "</div>") : "";
                res += kendoDatePicker2 != null ? ("<div class='form-group'>" + kendoDatePicker2.ToHtmlString() + "</div>") : "";
                res += kendoNumericTextBox1 != null ? ("<div class='form-group'>" + kendoNumericTextBox1.ToHtmlString() + "</div>") : "";
                res += kendoNumericTextBox2 != null ? ("<div class='form-group'>" + kendoNumericTextBox2.ToHtmlString() + "</div>") : "";
                res += kendoDropDownList2 != null ? ("<div class='form-group'>" + kendoDropDownList2.ToHtmlString() + "</div>") : "";

                res += kendoCheckbox2 != null ? ("<div class='form-group'>" + kendoCheckbox2.ToHtmlString() + "</div>") : "";
                
                res += "</div>";
                res += "<br />";
            }
            
            return res;
        }

        public static string GetPrjClaimFilterParams(this HtmlHelper html, PrjClaimFilterViewModel model, BaseController baseController)
        {
            if ((model == null) || (model.filter_items == null) || (model.filter_items.Count() <= 0))
                return "Не найдены элементы фильтра";

            string res = "";
            string checkbox1NamePrefix = "chb1_";
            string checkbox2NamePrefix = "chb2_";
            string opTypeNamePrefix = "opp_";
            string controlNamePrefix = "pcfp_";            
            string dataFieldId = null;
            string[] fieldNameValueArray = null;

            List<string> field_name_value_splitted = null;

            IEnumerable<PrjClaimFilterOpTypeViewModel> cacheOpTypeValueFull = (IEnumerable<PrjClaimFilterOpTypeViewModel>)baseController.GetType().BaseType.GetProperty("cachedOpTypeList", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(baseController, null);
            IEnumerable<PrjClaimFilterOpTypeViewModel> cacheOpTypeValue = null;

            //MvcHtmlString htmlLabel = null;
            CheckBoxBuilder kendoCheckbox1 = null;
            CheckBoxBuilder kendoCheckbox2 = null;
            DropDownListBuilder kendoDropDownList1 = null;
            MultiSelectBuilder kendoMultiSelect = null;
            TextBoxBuilder<string> kendoTextBox = null;
            DatePickerBuilder kendoDatePicker1 = null;
            DatePickerBuilder kendoDatePicker2 = null;
            NumericTextBoxBuilder<decimal> kendoNumericTextBox1 = null;
            NumericTextBoxBuilder<decimal> kendoNumericTextBox2 = null;
            DropDownListBuilder kendoDropDownList2 = null;

            foreach (var filter_item in model.filter_items.Where(ss => ss.is_field_in_filter).OrderBy(ss => ss.ord))
            {
                dataFieldId = "data-field-id='" + filter_item.field_id.ToString() + "'";
                res += "<div " + dataFieldId + " class='prj-claim-filter-items'>";
                fieldNameValueArray = String.IsNullOrWhiteSpace(filter_item.field_name_value) ? null : filter_item.field_name_value.Split(';');

                //htmlLabel = null;
                kendoCheckbox1 = null;
                kendoCheckbox2 = null;
                kendoDropDownList1 = null;
                kendoMultiSelect = null;
                kendoTextBox = null;
                kendoDatePicker1 = null;
                kendoDatePicker2 = null;
                kendoNumericTextBox1 = null;
                kendoNumericTextBox2 = null;
                kendoDropDownList2 = null;

                //htmlLabel = html.Label(filter_item.field_name_rus, new { @for = controlNamePrefix + filter_item.field_name, });                
                kendoCheckbox1 = html.KendoCheckBox(checkbox1NamePrefix + filter_item.field_name, filter_item.field_name_rus + " " + filter_item.op_type_symbol, true, false)
                    .HtmlAttributes(new { @style = "width: 140px;", @class = "k-checkbox prj-claim-filter-item", @data_control_type = "1", @data_field_type = "is_field_in_filter" });

                kendoCheckbox2 = html.KendoCheckBox(checkbox2NamePrefix + filter_item.field_name, "", filter_item.is_value_fixed == true)
                    .HtmlAttributes(new { @class = "k-checkbox prj-claim-filter-item", @data_control_type = "1", @data_field_type = "is_value_fixed" });

                switch ((Enums.PrjClaimFieldTypeEnum)filter_item.field_type_id)
                {
                    case Enums.PrjClaimFieldTypeEnum.REF:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.EQ || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.NEQ);
                        object cachePropValue = baseController.GetType().BaseType.GetProperty(filter_item.cache_name, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(baseController, null);
                        kendoMultiSelect = html.Kendo().MultiSelect()
                                                            .Placeholder("...")
                                                            .AutoClose(false)
                                                            .AutoBind(true)
                                                            .BindTo((System.Collections.IEnumerable)cachePropValue)
                                                            .Name(controlNamePrefix + filter_item.field_name)
                                                            .DataTextField(filter_item.cache_text_field)
                                                            .DataValueField(filter_item.cache_value_field)
                                                            //.Value(filter_item.field_name_value)
                                                            .Value(fieldNameValueArray)
                                                            .Enable(filter_item.is_value_fixed != true)
                                                            .HtmlAttributes(new { @id = controlNamePrefix + filter_item.field_name, @class = "form-control input-sm prj-claim-filter-item", @style = "min-width: 250px;", @data_control_type = "2", @data_field_type = "field_name_value" })
                                                            ;
                        break;
                    case Enums.PrjClaimFieldTypeEnum.STRING:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.EQ || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.NEQ
                            || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.CONTAINS || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.NOT_CONTAINS);
                        kendoTextBox = html.Kendo().TextBox().Name(controlNamePrefix + filter_item.field_name).Value(filter_item.field_name_value)
                            .Enable(filter_item.is_value_fixed != true)
                            .HtmlAttributes(new { @id = controlNamePrefix + filter_item.field_name, @class = "form-control input-sm prj-claim-filter-item", @style = "min-width: 250px;", @data_control_type = "3", @data_field_type = "field_name_value" });
                        break;
                    case Enums.PrjClaimFieldTypeEnum.DATE:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id != (int)Enums.PrjClaimFilterOpTypeEnum.CONTAINS && ss.op_type_id != (int)Enums.PrjClaimFilterOpTypeEnum.NOT_CONTAINS);
                        field_name_value_splitted = filter_item.field_name_value != null ? filter_item.field_name_value.Split(';').ToList() : null;
                        kendoDatePicker1 = html.Kendo().DatePicker().Name(controlNamePrefix + filter_item.field_name + "_1")
                                                    .Value(((field_name_value_splitted != null) && (field_name_value_splitted.Count > 0) && (field_name_value_splitted[0] != null)) ? field_name_value_splitted[0] : null)
                                                    .Enable(filter_item.is_value_fixed != true)
                                                    .HtmlAttributes(new { @type = "text", @class = "form-control input-sm prj-claim-filter-item", @id = controlNamePrefix + filter_item.field_name + "_1", @style = "width: 120px;", @data_control_type = "4", @data_field_type = "field_name_value" });
                        if ((filter_item.op_type_id.GetValueOrDefault(0) == 7) || (filter_item.op_type_id.GetValueOrDefault(0) == 8))
                        {
                            kendoDatePicker2 = html.Kendo().DatePicker().Name(controlNamePrefix + filter_item.field_name + "_2")
                                                        .Value(((field_name_value_splitted != null) && (field_name_value_splitted.Count > 1) && (field_name_value_splitted[1] != null)) ? field_name_value_splitted[1] : null)
                                                        // !!!                                                    
                                                        .Enable(filter_item.is_value_fixed != true)
                                                        .HtmlAttributes(new { @type = "text", @class = "form-control input-sm prj-claim-filter-item", @id = controlNamePrefix + filter_item.field_name + "_2", @style = "width: 120px;", @data_control_type = "4", @data_field_type = "field_name_value" });
                        }
                        break;
                    case Enums.PrjClaimFieldTypeEnum.NUMERIC:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id != (int)Enums.PrjClaimFilterOpTypeEnum.CONTAINS && ss.op_type_id != (int)Enums.PrjClaimFilterOpTypeEnum.NOT_CONTAINS);
                        field_name_value_splitted = filter_item.field_name_value != null ? filter_item.field_name_value.Split(';').ToList() : null;
                        kendoNumericTextBox1 = html.Kendo().NumericTextBox<decimal>().Name(controlNamePrefix + filter_item.field_name + "_1")
                            //.Value(String.IsNullOrWhiteSpace(filter_item.field_name_value) ? null : (decimal?)Convert.ToDecimal(filter_item.field_name_value))
                            .Value(((field_name_value_splitted != null) && (field_name_value_splitted.Count > 0) && (field_name_value_splitted[0] != null)) ? (decimal?)Convert.ToDecimal(field_name_value_splitted[0]) : null)
                            .Format("#.00")
                            .HtmlAttributes(new { @type = "text", @class = "form-control input-sm prj-claim-filter-item", @id = controlNamePrefix + filter_item.field_name + "_1", @style = "display: inline !important;width: 120px;", @data_control_type = "5", @data_field_type = "field_name_value" })
                            ;
                        if ((filter_item.op_type_id.GetValueOrDefault(0) == 7) || (filter_item.op_type_id.GetValueOrDefault(0) == 8))
                        {
                            kendoNumericTextBox2 = html.Kendo().NumericTextBox<decimal>().Name(controlNamePrefix + filter_item.field_name + "_2")
                            //.Value(String.IsNullOrWhiteSpace(filter_item.field_name_value) ? null : (decimal?)Convert.ToDecimal(filter_item.field_name_value))
                            .Value(((field_name_value_splitted != null) && (field_name_value_splitted.Count > 1) && (field_name_value_splitted[1] != null)) ? (decimal?)Convert.ToDecimal(field_name_value_splitted[1]) : null)
                            .Format("#.00")
                            // !!!                            
                            .Enable(filter_item.is_value_fixed != true)
                            .HtmlAttributes(new { @type = "text", @class = "form-control input-sm prj-claim-filter-item", @id = controlNamePrefix + filter_item.field_name + "_2", @style = "display: inline !important;width: 120px;", @data_control_type = "5", @data_field_type = "field_name_value" })
                            ;
                        }
                        break;
                    case Enums.PrjClaimFieldTypeEnum.BOOLEAN:
                        cacheOpTypeValue = cacheOpTypeValueFull.Where(ss => ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.EQ || ss.op_type_id == (int)Enums.PrjClaimFilterOpTypeEnum.NEQ);
                        kendoDropDownList2 = html.Kendo()
                            .DropDownList()
                            .Name(controlNamePrefix + filter_item.field_name)
                            .BindTo(new List<SimpleCombo>() { new SimpleCombo() { obj_id = 1, obj_name = "Да" }, new SimpleCombo() { obj_id = 2, obj_name = "Нет" } })
                            .DataTextField("obj_name")
                            .DataValueField("obj_id")
                            .Value(filter_item.field_name_value)
                            .Enable(filter_item.is_value_fixed != true)
                            .HtmlAttributes(new { @id = controlNamePrefix + filter_item.field_name, @class = "form-control input-sm prj-claim-filter-item", @style = "width: 250px;", @data_control_type = "6", @data_field_type = "field_name_value" })
                            ;
                        break;
                    default:
                        break;
                }

                kendoDropDownList1 = html.Kendo()
                    .DropDownList()
                    .Name(opTypeNamePrefix + filter_item.field_name)
                    .BindTo(cacheOpTypeValue)
                    .DataTextField("op_type_symbol")
                    .DataValueField("op_type_id")
                    .Value(filter_item.op_type_id.GetValueOrDefault(0) > 0 ? filter_item.op_type_id.ToString() : "1")
                    .Enable(false)                    
                    .HtmlAttributes(new { @id = opTypeNamePrefix + filter_item.field_name, @class = "form-control input-sm prj-claim-filter-item", @style = "width:50px;visibility:hidden;", @data_control_type = "6", @data_field_type = "op_type_id" })
                    ;
                /*@style = "width: 50px;margin-left:5px;visibility:hidden;"*/


                res += kendoMultiSelect != null ? ("<div class='form-group'>" + kendoCheckbox1.ToHtmlString() + kendoDropDownList1.ToHtmlString() 
                    + "<span class='hidden'>" + kendoCheckbox2.ToHtmlString() + "</span>" 
                    + kendoMultiSelect.ToHtmlString() + "</div>") : "";
                res += kendoTextBox != null ? ("<div class='form-group'>" + kendoCheckbox1.ToHtmlString() + kendoDropDownList1.ToHtmlString()
                    + "<span class='hidden'>" + kendoCheckbox2.ToHtmlString() + "</span>"
                    + kendoTextBox.ToHtmlString() + "</div>") : "";
                res += kendoDatePicker1 != null ? ("<div class='form-group'>" + kendoCheckbox1.ToHtmlString() + kendoDropDownList1.ToHtmlString()
                    + "<span class='hidden'>" + kendoCheckbox2.ToHtmlString() + "</span>"
                    + "<br/>" + kendoDatePicker1.ToHtmlString() + "</div>") : "";
                res += kendoDatePicker2 != null ? ("<div class='form-group'>" + kendoDatePicker2.ToHtmlString() + "</div>") : "";
                res += kendoNumericTextBox1 != null ? ("<div class='form-group'>" + kendoCheckbox1.ToHtmlString() + kendoDropDownList1.ToHtmlString()
                    + "<span class='hidden'>" + kendoCheckbox2.ToHtmlString() + "</span>"
                    + "<br/>" + kendoNumericTextBox1.ToHtmlString() + "</div>") : "";
                res += kendoNumericTextBox2 != null ? ("<div class='form-group'>" + kendoNumericTextBox2.ToHtmlString() + "</div>") : "";
                res += kendoDropDownList2 != null ? ("<div class='form-group'>" + kendoCheckbox1.ToHtmlString() + kendoDropDownList1.ToHtmlString()
                    + "<span class='hidden'>" + kendoCheckbox2.ToHtmlString() + "</span>"
                    + "<br/>" + kendoDropDownList2.ToHtmlString() + "</div>") : "";

                res += "</div>";
                res += "<br />";
            }

            return res;
        }
    }
}