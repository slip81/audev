﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Reflection;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Sys;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Defect;
using CabinetMvc.Util;
using CabinetMvc.Auth;
using AuDev.Common.Util;

namespace CabinetMvc.Extensions
{
    public static partial class HtmlExtensions
    {
        public static HtmlWidget CabWidget(this HtmlHelper html, ClientUserWidgetViewModel widget)
        {
            if (widget == null)
            {
                return new HtmlWidget(html.ViewContext);
            }

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var currUser = auth == null ? null : ((IUserProvider)auth.CurrentUser.Identity).User;

            bool parseOk = false;
            string widget_content = "";
            string widget_header = "";
            string widget_img = "";
            UrlHelper urlHelper = new UrlHelper(html.ViewContext.RequestContext);

            Enums.CabWidgetEnum widgetEnum = Enums.CabWidgetEnum.NONE;
            parseOk = Enum.TryParse<Enums.CabWidgetEnum>(widget.widget_id.ToString(), out widgetEnum);
            if (!parseOk)
                widgetEnum = Enums.CabWidgetEnum.NONE;

            Enums.ClientServiceStateEnum widgetServiceStateEnum = Enums.ClientServiceStateEnum.NONE;
            parseOk = Enum.TryParse<Enums.ClientServiceStateEnum>(widget.state.HasValue ? widget.state.ToString() : "", out widgetServiceStateEnum);
            if (!parseOk)
                widgetServiceStateEnum = Enums.ClientServiceStateEnum.NONE;
            
            /*
            widget_img += "<span class='widget-main-only'>";
            switch (widgetEnum)
            {
                case Enums.CabWidgetEnum.DISCOUNT:
                    widget_img += "<img class='widget-icon-svg' src='" + urlHelper.Content("~/Images/svg/picture.svg") + "' />";
                    break;
                case Enums.CabWidgetEnum.CVA:
                    widget_img += "<img class='widget-icon-svg' src='" + urlHelper.Content("~/Images/svg/bullish.svg") + "' />";
                    break;
                default:
                    break;
            }
            widget_img += "</span>";
            */

            switch (widgetServiceStateEnum)
            {
                case Enums.ClientServiceStateEnum.INSTALLED:

                    switch (widgetEnum)
                    {
                        case Enums.CabWidgetEnum.DISCOUNT:
                            var businessSummaryService = DependencyResolver.Current.GetService<IBusinessSummaryService>();
                            BusinessSummaryViewModel summary = (BusinessSummaryViewModel)businessSummaryService.GetItem(currUser.BusinessId);
                            widget_content += "<dl class='dl-horizontal'>" +
                                "<dt>всего карт: " + summary.card_cnt.ToString() + " (дисконтные - " + summary.card_type_group_1_cnt.ToString() + ", бонусные - " + summary.card_type_group_2_cnt.ToString() + ", смешанные - " + summary.card_type_group_3_cnt.ToString() + ")</dt>" +
                                "<dd><a href='/CardList'>перейти к списку карт</a></dd>" +
                                "<dt>владельцев карт: " + summary.card_holder_cnt.ToString() + "</dt>" +
                                "<dd><a href='/CardClientList'>перейти к владельцам</a></dd>" +
                                "<dt>продаж: " + summary.card_trans_cnt.ToString() + " (за сегодня - " + summary.card_trans_today_cnt.ToString() + ")</dt>" +
                                "<dd><a href='/CardTransactionList)'>перейти к продажам</a></dd>" +
                                "</dl>";


                            bool isVisible_graph1 = false;
                            bool isVisible_graph2 = false;
                            if (!String.IsNullOrEmpty(widget.param))
                            {
                                DiscountWidgetParam paramDiscount = JsonConvert.DeserializeObject<DiscountWidgetParam>(widget.param);
                                if (paramDiscount != null)
                                {
                                    isVisible_graph1 = paramDiscount.IsVisible_Graph1;
                                    isVisible_graph2 = paramDiscount.IsVisible_Graph2;
                                }
                            }

                            widget_content +=
                                KendoCheckBox(html, "chbGraphSale", "график продаж", isVisible_graph1) +
                                "<span>&nbsp&nbsp&nbsp</span>" +
                                KendoCheckBox(html, "chbGraphSale2", "скидки и бонусы", isVisible_graph2) +
                                "<div id='graph-sale-loading' class='hidden' style='text-align:center'>... загрузка</div>" +
                                DiscountChart_Simple(html, "onBusinessGraphDataBinding", "onBusinessGraphDataBound", isVisible_graph1).ToHtmlString() +
                                "<div id='graph-sale2-loading' class='hidden' style='text-align:center'>... загрузка</div>" +
                                DiscountChart_Stacked(html, "onBusinessGraph2DataBinding", "onBusinessGraph2DataBound", isVisible_graph2).ToHtmlString()
                                ;
                            widget_content += "<script>" +                                
                                "$('#chbGraphSale').on('change', function() { " +
                                "var is_checked = $('#chbGraphSale').is(':checked');" +
                                "if (is_checked) {" +                                
                                "$('#businessGraph').getKendoChart().dataSource.read();" +
                                "} else {" +
                                "$('#businessGraph').addClass('hidden');" +
                                "};" +
                                "var is_checked2 = $('#chbGraphSale2').is(':checked');" +
                                "var widgetParam = {IsVisible_Graph1: is_checked, IsVisible_Graph2: is_checked2};" +
                                "main.updateWidget('" +  widget.widget_element_id +"', JSON.stringify(widgetParam));" +
                                "});" +
                                "function onBusinessGraphDataBound(e) {" +
                                "$('#graph-sale-loading').addClass('hidden');" +
                                "$('#businessGraph').removeClass('hidden');" +
                                "};" +
                                "function onBusinessGraphDataBinding(e) {" +
                                "$('#businessGraph').addClass('hidden');" +
                                "$('#graph-sale-loading').removeClass('hidden');" +
                                "};" +
                                //
                                "$('#chbGraphSale2').on('change', function() { " +
                                "var is_checked = $('#chbGraphSale2').is(':checked');" +
                                "if (is_checked) {" +
                                "$('#businessGraph2').getKendoChart().dataSource.read();" +
                                "} else {" +
                                "$('#businessGraph2').addClass('hidden');" +
                                "};" +
                                "var is_checked1 = $('#chbGraphSale').is(':checked');" +
                                "var widgetParam = {IsVisible_Graph1: is_checked1, IsVisible_Graph2: is_checked};" +
                                "main.updateWidget('" + widget.widget_element_id + "', JSON.stringify(widgetParam));" +
                                "});" +
                                "function onBusinessGraph2DataBound(e) {" +
                                "$('#graph-sale2-loading').addClass('hidden');" +
                                "$('#businessGraph2').removeClass('hidden');" +
                                "};" +
                                "function onBusinessGraph2DataBinding(e) {" +
                                "$('#businessGraph2').addClass('hidden');" +
                                "$('#graph-sale2-loading').removeClass('hidden');" +
                                "};" +
                                "</script>"
                                ;
                            break;
                        case Enums.CabWidgetEnum.DEFECT:
                            var defectSummaryService = DependencyResolver.Current.GetService<IDefectSummaryService>();
                            DefectSummaryViewModel defectSummary = (DefectSummaryViewModel)defectSummaryService.GetItem(currUser.CabClientId);
                            widget_content += "<dl class='dl-horizontal'>" +
                                "<dt>подключено точек: " + currUser.serviceSummary.Where(ss => ss.service_id == (int)Enums.ClientServiceEnum.DEFECT).Select(ss => ss.sales_cnt).FirstOrDefault() + "</dt>" +
                                "<dt>обновлено на сайте: " + defectSummary.defect_upd_date.ToString() + "</dt>" +
                                "<dd><a href='/DefectList'>перейти к списку брака</a></dd>" +
                                "<dt>актуальных строк: " + defectSummary.defect_cnt.ToString() + "</dt>" +
                                "<dd> </dd>" +
                                "</dl>"
                                ;
                            if (defectSummary.is_allow_download)
                            {
                                //widget_content += "<button type='button' class='btn btn-success' id='btnDownloadDefect'><img src='" + urlHelper.Content("~/Images/new/download.png") + "' />Скачать</button>"
                                widget_content += "<button type='button' class='btn btn-success' id='btnDownloadDefect'><span title='Скачать' class='cab-png cab-png-download'></span>Скачать</button>"
                                    ;
                                widget_content += "<script>" +
                                    "$('#btnDownloadDefect').click(function (e) {" +
                                    "window.location = '/DefectDownload';" +
                                    "});" +
                                    "</script>"
                                    ;
                            }                                
                            break;
                        case Enums.CabWidgetEnum.EXCHANGE:
                            widget_content += "<dl class='dl-horizontal'>" +
                                "<dt>подключено точек: " + currUser.serviceSummary.Where(ss => ss.service_id == (int)Enums.ClientServiceEnum.EXCHANGE).Select(ss => ss.sales_cnt).FirstOrDefault() + "</dt>" +
                                "<dt>" +
                                "партнеры:" +
                                "<span class='dt-link'>" +
                                "<a href='/ExchangeLogList'>Мелодия здоровья</a>" +
                                "|" +
                                "<a href='/ExchangeLogList'>АСНА</a>" +
                                "</span>" +
                                "</dt>" +
                                "<dd><a href='/ExchangeLogList'>перейти к журналу обмена</a></dd>" +
                                "</dl>" +
                                //"<button type='button' class='btn btn-success' id='btnExchangeLog'><img src='" + urlHelper.Content("~/Images/new/log.png") + "' />Журнал обмена</button>"
                                "<button type='button' class='btn btn-success' id='btnExchangeLog'><span title='>Журнал обмена' class='cab-png cab-png-log'></span>Журнал обмена</button>"
                                
                                ;
                            widget_content += "<script>" +
                                "$('#btnExchangeLog').click(function (e) {" +
                                "window.location = '/ExchangeLogList';" +
                                "});" +
                                "</script>"
                                ;
                            break;
                        case Enums.CabWidgetEnum.CVA:
                            widget_content = "<dl class='dl-horizontal'>" +
                                "<dt>подключено точек: " + currUser.serviceSummary.Where(ss => ss.service_id == widget.service_id).Select(ss => ss.sales_cnt).FirstOrDefault() + "</dt>" +
                                "<dd><a href='/CvaLogList'>перейти к журналу обмена</a></dd>" +
                                "</dl>" +
                                //"<button type='button' class='btn btn-success' id='btnCvaLog'><img src='" + urlHelper.Content("~/Images/new/log.png") + "' />Журнал обмена</button>"
                                "<button type='button' class='btn btn-success' id='btnCvaLog'><span title='>Журнал обмена' class='cab-png cab-png-log'></span>Журнал обмена</button>"
                                ;
                            widget_content += "<script>" +
                                "$('#btnCvaLog').click(function (e) {" +
                                "window.location = '/CvaLogList';" +
                                "});" +
                                "</script>"
                                ;
                            break;
                        case Enums.CabWidgetEnum.SEND_DEFECT:
                        case Enums.CabWidgetEnum.SEND_GRLS:
                            var senderLogService = DependencyResolver.Current.GetService<IServiceSenderLogService>();
                            var senderLogLast = senderLogService.GetItem_Last((int)currUser.CabClientId, (int)widget.service_id);
                            widget_content += "<dl class='dl-horizontal'>" +
                                "<dt>подключено точек: " + currUser.serviceSummary.Where(ss => ss.service_id == widget.service_id).Select(ss => ss.sales_cnt).FirstOrDefault() + "</dt>" +
                                "<dd></dd>"
                                ;
                            if (senderLogLast != null)
                            {
                                widget_content += "<dt>предыдущая отправка: " + ((DateTime)senderLogLast.date_beg).ToString("dd.MM.yyyy HH:mm:ss") + "</dt>" +
                                "<dd><a href='/'>перейти к журналу отправки</a></dd>"
                                ;
                            }
                            else
                            {
                                widget_content += "<dt>отправок пока не было</dt>" +
                                "<dd></dd>"
                                ;
                            }
                            widget_content += "</dl>" +
                                //"<button type='button' class='btn btn-success' id='btnSendDefectLog'><img src='" + urlHelper.Content("~/Images/new/log.png") + "' />Журнал отправки</button>"
                                "<button type='button' class='btn btn-success' id='btnSendDefectLog'><span title='>Журнал отправки' class='cab-png cab-png-log'></span>Журнал отправки</button>"                                
                                ;
                            widget_content += "<script>" +
                                "$('#btnSendDefectLog').click(function (e) {" +
                                "alert('Пока не реализовано');" +
                                "});" +
                                "</script>"
                                ;
                            break;
                        default:
                            widget_content += "контент не реализован";
                            break;
                    }

                    widget_header += "<span title='Услуга установлена'><img class='widget-icon-svg widget-sidebar-only' src='" + urlHelper.Content("~/Images/svg/checkmark.svg") + "' /></span>";
                    break;
                case Enums.ClientServiceStateEnum.ORDERED:
                    widget_content += "услуга заказана";
                    widget_header += "<span title='Услуга заказана'><img class='widget-icon-svg widget-sidebar-only' src='" + urlHelper.Content("~/Images/svg/medium_priority.svg") + "' /></span>";
                    break;
                default:
                    widget_content += "услуга не зарегистрирована";
                    widget_content += "<span title='Заказать' class='widget-main-only pull-right' onclick='main.btnOrderService(" + "\"" + widget.widget_element_id + "\"" + "," + widget.service_id.ToString() + ")'><button type='button' class='btn widget-btn-svg'><img class='widget-icon-svg widget-main-only' src='" + urlHelper.Content("~/Images/svg/low_priority.svg") + "' /></button></span>";
                    widget_header += "<span title='Заказать' onclick='main.btnOrderService(" + "\"" + widget.widget_element_id + "\"" + "," + widget.service_id.ToString() + ")'><button type='button' class='btn widget-btn-svg'><img class='widget-icon-svg widget-sidebar-only' src='" + urlHelper.Content("~/Images/svg/low_priority.svg") + "' /></button></span>";
                    break;
            }

            html.ViewContext.Writer.Write(
                "<div id='" + widget.widget_element_id + "' class='widget'>" +
                //"<span class='widget-descr'>" + widget_img +
                "<h3>" + widget.widget_name +                
                "<span class='widget-sidebar-header'>" + widget_header + "</span>" +
                "</h3>" +
                //"</span>" +
                "<div class='widget-content'>" + widget_content +
                "</div>" +
                "</div>"
                );

            return new HtmlWidget(html.ViewContext);
        }
    }

    public class HtmlWidget : IDisposable
    {
        private readonly ViewContext _viewContext;

        public HtmlWidget(ViewContext viewContext)
        {
            _viewContext = viewContext;
        }

        public void Dispose()
        {
            _viewContext.Writer.Write("");
        }
    }
}