﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Reflection;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using Newtonsoft.Json;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Prj;
using CabinetMvc.Util;
using CabinetMvc.Auth;
using AuDev.Common.Util;

namespace CabinetMvc.Extensions
{
    public static partial class HtmlExtensions
    {

        public static string GetPrjGroupPanel(this HtmlHelper html, PrjUserGroupViewModel prjUserGroup, int currUserId, List<bool> fixedKindVisibility = null, string prjWidgetHeight = null, string prjWidgetWidth = null)
        {
            string header = html.GetPrjGroupPanelToolbar(prjUserGroup, currUserId);

            int defHeight = 230;
            int height = defHeight;
            if (!String.IsNullOrWhiteSpace(prjWidgetHeight))
            {
                var prjUserSettingsSimple = JsonConvert.DeserializeObject<List<PrjUserSettingsSimple>>(prjWidgetHeight);
                if ((prjUserSettingsSimple != null) && (prjUserSettingsSimple.Count > 0))
                {
                    var val = prjUserSettingsSimple.Where(ss => ss.id == "prj-user-group-panel-" + prjUserGroup.group_id.ToString()).Select(ss => ss.val).FirstOrDefault();
                    if (!String.IsNullOrWhiteSpace(val))
                    {
                        if (!int.TryParse(val, out height))
                            height = defHeight;
                    }
                }
            }

            int defWidth = 400;
            int width = defWidth;
            if (!String.IsNullOrWhiteSpace(prjWidgetWidth))
            {
                var prjUserSettingsSimple = JsonConvert.DeserializeObject<List<PrjUserSettingsSimple>>(prjWidgetWidth);
                if ((prjUserSettingsSimple != null) && (prjUserSettingsSimple.Count > 0))
                {
                    var val = prjUserSettingsSimple.Where(ss => ss.id == "prj-user-group-panel-" + prjUserGroup.group_id.ToString()).Select(ss => ss.val).FirstOrDefault();
                    if (!String.IsNullOrWhiteSpace(val))
                    {
                        if (!int.TryParse(val, out width))
                            width = defWidth;
                    }
                }
            }

            //string content = "контент";
            string content = html.GetPrjGroupPanelList(prjUserGroup);

            /*
            return "<div id='prj-user-group-panel-" + prjUserGroup.group_id.ToString() + "' class='prj-widget'>" +
                    "<h3 class='widget-handler'>" + prjUserGroup.group_name +
                    "<span class='widget-header'>заголовок</span>" +
                    "</h3>" +
                    "<div class='widget-content'>" +
                    content +
                    "</div>" +
                    "</div>";
            */
            var curr_fixed_kind = fixedKindVisibility == null ? 0 : prjUserGroup.fixed_kind.GetValueOrDefault(0);
            string curr_fixed_kind_display = "";
            if (curr_fixed_kind > 0)
            {
                if (!fixedKindVisibility[curr_fixed_kind - 1])
                    curr_fixed_kind_display = "display:none;";
            }
            return "<div id='prj-user-group-panel-" + prjUserGroup.group_id.ToString() + "' class='prj-widget' style='height:" + height.ToString() + "px;width:" + width.ToString() + "px;" + curr_fixed_kind_display + "'>" +
                "<div class='widget-header'>" +
                header +
                "</div>" +
                "<div class='widget-content'>" +
                "<div class='prj-group-content-container' data-group-id='" + prjUserGroup.group_id.ToString() + "' data-fixed-kind='" + prjUserGroup.fixed_kind.GetValueOrDefault(0).ToString() + "'>" +
                content +
                "</div>" +
                "</div>" +
                "</div>";
        }

        public static string GetPrjGroupPanelList(this HtmlHelper html, PrjUserGroupViewModel prjUserGroup)
        {
            var list = html.Kendo().ListView<CabinetMvc.ViewModel.Prj.PrjUserGroupItemViewModel>()
                .Name("prjGroupPanelList_" + prjUserGroup.group_id.ToString())
                .ClientTemplateId("templPrjUserGroupItemList")
                .TagName("div")
                .DataSource(ds =>
                {
                    ds.Model(m => m.Id("item_id"));
                    ds.Read(read => read.Action("GetPrjUserGroupItemList", "Prj", new { @group_id = prjUserGroup.group_id.ToString() }).Data("prjNotebookPartial.getPrjGroupPanelListData(" + prjUserGroup.group_id.ToString() + ")"));
                    ds.Sort(s => s.Add("item_num").Ascending());
                })
                .Selectable(selectable => selectable.Mode(ListViewSelectionMode.Single))
                .Events(ev => ev.DataBound("prjNotebookPartial.onPrjGroupPanelListDataBound"))                
                ;

            return list.ToHtmlString();
        }

        public static string GetPrjGroupPanelToolbar(this HtmlHelper html, PrjUserGroupViewModel prjUserGroup, int currUserId)
        {
            bool isCurrUserGroup = prjUserGroup.user_id == currUserId;
            var prjUserGroupService = DependencyResolver.Current.GetService<IPrjUserGroupService>();
            var perriodDs = prjUserGroupService.GetList_Period();

            var tb = html.Kendo().ToolBar()
                    .Name("prjGroupPanelToolbar_" + prjUserGroup.group_id.ToString())
                        .Items(item =>
                        {
                            item.Add().Template("<span class='widget-handler'>" + prjUserGroup.group_name + "</span>");
                            if (prjUserGroup.fixed_kind.GetValueOrDefault(0) <= 0)
                            {
                                item.Add().Type(CommandType.Separator).Overflow(ShowInOverflowPopup.Never);
                                item.Add().Type(CommandType.SplitButton)
                                    .Text("")
                                    .Icon("plus")
                                    .Click("prjNotebookPartial.onBtnPrjGroupNoteClick")
                                    .HtmlAttributes(new { @title = "Добавить запись", @data_group_id = prjUserGroup.group_id.ToString(), @data_config_action_type = "1" })
                                    .MenuButtons(buttons =>
                                    {
                                        buttons.Add().Text("Редактировать запись")
                                            .Icon("pencil")
                                            .HtmlAttributes(new { @title = "Редактировать запись", @data_group_id = prjUserGroup.group_id.ToString(), @data_config_action_type = "2" });
                                        buttons.Add().Text("Удалить запись")
                                            .Icon("cancel")
                                            .HtmlAttributes(new { @title = "Удалить запись", @data_group_id = prjUserGroup.group_id.ToString(), @data_config_action_type = "3" });
                                    })
                                .Overflow(ShowInOverflowPopup.Never);
                                item.Add().Type(CommandType.Button).Icon("cancel").Text("Удалить группу")
                                    .Overflow(ShowInOverflowPopup.Always)
                                    .Click("prjNotebookPartial.onBtnPrjGroupPanelDelClick")
                                    .HtmlAttributes(new { @title = "Удалить группу", @data_group_id = prjUserGroup.group_id.ToString() });
                            }
                            else
                            {
                                if (isCurrUserGroup)
                                {
                                    item.Add().Type(CommandType.Separator)
                                        .Overflow(ShowInOverflowPopup.Never);

                                    switch ((Enums.PrjUserGroupFixedKindEnum)prjUserGroup.fixed_kind)
                                    {
                                        case Enums.PrjUserGroupFixedKindEnum.RECEIVED:
                                            item.Add().Type(CommandType.SplitButton)
                                                .Text("")
                                                .SpriteCssClass("k-icon cab-png cab-png-done-task")
                                                .Click("prjNotebookPartial.onBtnPrjGroupPanelChildActionsClick(" + prjUserGroup.group_id.ToString() + ")")
                                                .HtmlAttributes(new { @title = "Выполнить работу и передать задание дальше", @data_config_action_type = "4" })
                                                .MenuButtons(buttons =>
                                                {
                                                    buttons.Add().Text("Выполнить и передать дальше")
                                                        .SpriteCssClass("k-icon cab-png cab-png-done-task")
                                                        .HtmlAttributes(new { @title = "Выполнить работу и передать задание дальше", @data_config_action_type = "4" });
                                                    buttons.Add().Text("Выполнить и вернуть")
                                                        .SpriteCssClass("k-icon cab-png cab-png-return-task")
                                                        .HtmlAttributes(new { @title = "Выполнить работу и вернуть задание на доработку", @data_config_action_type = "5" });
                                                    buttons.Add().Text("Выполнить и завершить")
                                                        .SpriteCssClass("k-icon cab-png cab-png-finish-task")
                                                        .HtmlAttributes(new { @title = "Выполнить работу и завершить задание", @data_config_action_type = "6" });
                                                    buttons.Add().Text("Отменить")
                                                        .SpriteCssClass("k-icon cab-png cab-png-cancel-task")
                                                        .HtmlAttributes(new { @title = "Отменить задание", @data_config_action_type = "7" });
                                                })
                                            .Overflow(ShowInOverflowPopup.Never);
                                            break;
                                        case Enums.PrjUserGroupFixedKindEnum.SENT:
                                            item.Add().Type(CommandType.SplitButton)
                                                .Text("")
                                                .SpriteCssClass("k-icon cab-png cab-png-receive-task")
                                                .Click("prjNotebookPartial.onBtnPrjGroupPanelChildActionsClick(" + prjUserGroup.group_id.ToString() + ")")
                                                .HtmlAttributes(new { @title = "Принять задание в работу", @data_config_action_type = "3" })
                                                .MenuButtons(buttons =>
                                                {
                                                    buttons.Add().Text("Принять в работу")
                                                        .SpriteCssClass("k-icon cab-png cab-png-receive-task")
                                                        .HtmlAttributes(new { @title = "Принять задание в работу", @data_config_action_type = "3" });
                                                    buttons.Add().Text("Выполнить и передать дальше")
                                                        .SpriteCssClass("k-icon cab-png cab-png-done-task")
                                                        .HtmlAttributes(new { @title = "Выполнить работу и передать задание дальше", @data_config_action_type = "4" });
                                                    buttons.Add().Text("Выполнить и вернуть")
                                                        .SpriteCssClass("k-icon cab-png cab-png-return-task")
                                                        .HtmlAttributes(new { @title = "Выполнить работу и вернуть задание на доработку", @data_config_action_type = "5" });
                                                    buttons.Add().Text("Выполнить и завершить")
                                                        .SpriteCssClass("k-icon cab-png cab-png-finish-task")
                                                        .HtmlAttributes(new { @title = "Выполнить работу и завершить задание", @data_config_action_type = "6" });
                                                    buttons.Add().Text("Отменить")
                                                        .SpriteCssClass("k-icon cab-png cab-png-cancel-task")
                                                        .HtmlAttributes(new { @title = "Отменить задание", @data_config_action_type = "7" });
                                                })
                                            .Overflow(ShowInOverflowPopup.Never);
                                            break;
                                        case Enums.PrjUserGroupFixedKindEnum.DONE:
                                        case Enums.PrjUserGroupFixedKindEnum.CANCELED:
                                            item.Add().Type(CommandType.SplitButton)
                                                .Text("")
                                                .SpriteCssClass("k-icon cab-png cab-png-send-task")
                                                .Click("prjNotebookPartial.onBtnPrjGroupPanelChildActionsClick(" + prjUserGroup.group_id.ToString() + ")")
                                                .HtmlAttributes(new { @title = "Передать задание в работу", @data_config_action_type = "2" })
                                                .MenuButtons(buttons =>
                                                {
                                                    buttons.Add().Text("Передать в работу")
                                                        .SpriteCssClass("k-icon cab-png cab-png-send-task")
                                                        .HtmlAttributes(new { @title = "Передать задание в работу", @data_config_action_type = "2" });
                                                    buttons.Add().Text("Принять в работу")
                                                        .SpriteCssClass("k-icon cab-png cab-png-receive-task")
                                                        .HtmlAttributes(new { @title = "Принять задание в работу", @data_config_action_type = "3" });
                                                })
                                            .Overflow(ShowInOverflowPopup.Never);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                switch ((Enums.PrjUserGroupFixedKindEnum)prjUserGroup.fixed_kind)
                                {
                                    case Enums.PrjUserGroupFixedKindEnum.DONE:
                                    case Enums.PrjUserGroupFixedKindEnum.CANCELED:
                                        item.Add().Type(CommandType.Separator).Overflow(ShowInOverflowPopup.Never);
                                        item.Add().Template(html.Kendo()
                                            .DropDownList()
                                            .Name("prjGroupPanelPeriodTypeDdl_" + prjUserGroup.group_id.ToString())
                                            .BindTo(perriodDs)
                                            .AutoBind(true)
                                            .DataTextField("obj_name")
                                            .DataValueField("obj_id")
                                            .Events(ev =>
                                            {
                                                ev.Change("prjNotebookPartial.onPrjGroupPanelPeriodDdlChange(" + prjUserGroup.group_id.ToString() + ")");
                                            })
                                            .HtmlAttributes(new { @id = "prjGroupPanelPeriodTypeDdl_" + prjUserGroup.group_id.ToString(), @style = "width: 180px;" })
                                            .ToHtmlString()
                                            )
                                            .Overflow(ShowInOverflowPopup.Never);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        });
            return tb.ToHtmlString();
        }
       
    }

    public class PrjUserSettingsSimple
    {
        public PrjUserSettingsSimple()
        {
            //
        }

        public string id { get; set; }
        public string val { get; set; }
    }
}