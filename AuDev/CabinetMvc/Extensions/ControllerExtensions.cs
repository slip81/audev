﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Reflection;
using Kendo.Mvc.UI;
using Kendo.Mvc.UI.Fluent;
using Newtonsoft.Json;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Sys;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Defect;
using CabinetMvc.ViewModel.User;
using CabinetMvc.Util;
using CabinetMvc.Auth;
using AuDev.Common.Util;
using System.IO;
#endregion

namespace CabinetMvc.Extensions
{
    public static partial class ControllerExtensions
    {
        public static HtmlHelper GetHtmlHelper(this Controller controller)
        {
            var viewContext = new ViewContext(controller.ControllerContext, new FakeView(), controller.ViewData, controller.TempData, TextWriter.Null);
            return new HtmlHelper(viewContext, new ViewPage());
        }
    }

    public class FakeView : IView
    {
        public void Render(ViewContext viewContext, TextWriter writer)
        {
            throw new InvalidOperationException();
        }
    }

}