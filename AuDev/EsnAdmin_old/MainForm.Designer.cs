﻿namespace EsnAdmin
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor3 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor4 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem7 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuComboItem1 = new Telerik.WinControls.UI.RadMenuComboItem();
            this.radMenuItem6 = new Telerik.WinControls.UI.RadMenuItem();
            this.pageMain = new Telerik.WinControls.UI.RadPageView();
            this.pageMainEtalon = new Telerik.WinControls.UI.RadPageViewPage();
            this.splitEtalonMain = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitEtalonMainTop = new Telerik.WinControls.UI.SplitPanel();
            this.grbEtalonOpenFile = new Telerik.WinControls.UI.RadGroupBox();
            this.btnImportConfigAdd = new Telerik.WinControls.UI.RadButton();
            this.btnImportConfigEdit = new Telerik.WinControls.UI.RadButton();
            this.btnOpenFileCancel = new Telerik.WinControls.UI.RadButton();
            this.btnOpenFileOk = new Telerik.WinControls.UI.RadButton();
            this.ddlOpenFileConfig = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.txtOpenFileName = new Telerik.WinControls.UI.RadTextBox();
            this.barEtalonMain = new Telerik.WinControls.UI.RadCommandBar();
            this.barEtalonMainRow1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.barEtalonMainElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.barEtalonOpenFile = new Telerik.WinControls.UI.CommandBarButton();
            this.splitEtalonMainCenter = new Telerik.WinControls.UI.SplitPanel();
            this.splitEtalonCenter = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitEtalonCenterLeft = new Telerik.WinControls.UI.SplitPanel();
            this.treeDocProductFilter = new Telerik.WinControls.UI.RadTreeView();
            this.splitEtalonCenterRight = new Telerik.WinControls.UI.SplitPanel();
            this.splitEtalonCenter2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitEtalonCenterTop = new Telerik.WinControls.UI.SplitPanel();
            this.gridEtalonMain = new Telerik.WinControls.UI.RadGridView();
            this.barEtalonGrid = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement1 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement1 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnGridEtalonSave = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarStripElement2 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnGridEtalonCancel = new Telerik.WinControls.UI.CommandBarButton();
            this.splitEtalonCenterBottom = new Telerik.WinControls.UI.SplitPanel();
            this.splitEtalonCenter3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitEtalonCenterBottomLeft = new Telerik.WinControls.UI.SplitPanel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.gridEtalonGood = new Telerik.WinControls.UI.RadGridView();
            this.splitEtalonCenterBottomRight = new Telerik.WinControls.UI.SplitPanel();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.gridEtalonSyn = new Telerik.WinControls.UI.RadGridView();
            this.splitEtalonMainBottom = new Telerik.WinControls.UI.SplitPanel();
            this.pageMainSupplier = new Telerik.WinControls.UI.RadPageViewPage();
            this.splitSupplier = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitSupplierTop = new Telerik.WinControls.UI.SplitPanel();
            this.grbSupplierOpenFile = new Telerik.WinControls.UI.RadGroupBox();
            this.btnSupplierImportConfigAdd = new Telerik.WinControls.UI.RadButton();
            this.btnSupplierImportConfigEdit = new Telerik.WinControls.UI.RadButton();
            this.btnSupplierOpenFileCancel = new Telerik.WinControls.UI.RadButton();
            this.btnSupplierOpenFileOk = new Telerik.WinControls.UI.RadButton();
            this.ddlSupplierOpenFileConfig = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txtSupplierOpenFileName = new Telerik.WinControls.UI.RadTextBox();
            this.barSupplierMain = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement2 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement3 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.barSupplierOpenFile = new Telerik.WinControls.UI.CommandBarButton();
            this.splitSupplierMain = new Telerik.WinControls.UI.SplitPanel();
            this.splitSupplierCenter = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitSupplieCenterLeft = new Telerik.WinControls.UI.SplitPanel();
            this.treePartnerGoodFilter = new Telerik.WinControls.UI.RadTreeView();
            this.splitSupplieCenterRight = new Telerik.WinControls.UI.SplitPanel();
            this.gridSupplierMain = new Telerik.WinControls.UI.RadGridView();
            this.barSupplierGrid = new Telerik.WinControls.UI.RadCommandBar();
            this.commandBarRowElement3 = new Telerik.WinControls.UI.CommandBarRowElement();
            this.commandBarStripElement4 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnGridSupplierSave = new Telerik.WinControls.UI.CommandBarButton();
            this.commandBarStripElement5 = new Telerik.WinControls.UI.CommandBarStripElement();
            this.btnGridSupplierCancel = new Telerik.WinControls.UI.CommandBarButton();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem1.ComboBoxElement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageMain)).BeginInit();
            this.pageMain.SuspendLayout();
            this.pageMainEtalon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonMain)).BeginInit();
            this.splitEtalonMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonMainTop)).BeginInit();
            this.splitEtalonMainTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbEtalonOpenFile)).BeginInit();
            this.grbEtalonOpenFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportConfigAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportConfigEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenFileCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenFileOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlOpenFileConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpenFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barEtalonMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonMainCenter)).BeginInit();
            this.splitEtalonMainCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenter)).BeginInit();
            this.splitEtalonCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterLeft)).BeginInit();
            this.splitEtalonCenterLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeDocProductFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterRight)).BeginInit();
            this.splitEtalonCenterRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenter2)).BeginInit();
            this.splitEtalonCenter2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterTop)).BeginInit();
            this.splitEtalonCenterTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonMain.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barEtalonGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterBottom)).BeginInit();
            this.splitEtalonCenterBottom.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenter3)).BeginInit();
            this.splitEtalonCenter3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterBottomLeft)).BeginInit();
            this.splitEtalonCenterBottomLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterBottomRight)).BeginInit();
            this.splitEtalonCenterBottomRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonSyn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonSyn.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonMainBottom)).BeginInit();
            this.pageMainSupplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplier)).BeginInit();
            this.splitSupplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplierTop)).BeginInit();
            this.splitSupplierTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbSupplierOpenFile)).BeginInit();
            this.grbSupplierOpenFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSupplierImportConfigAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSupplierImportConfigEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSupplierOpenFileCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSupplierOpenFileOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSupplierOpenFileConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSupplierOpenFileName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barSupplierMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplierMain)).BeginInit();
            this.splitSupplierMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplierCenter)).BeginInit();
            this.splitSupplierCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplieCenterLeft)).BeginInit();
            this.splitSupplieCenterLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treePartnerGoodFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplieCenterRight)).BeginInit();
            this.splitSupplieCenterRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSupplierMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSupplierMain.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barSupplierGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.DisplayStyle = Telerik.WinControls.DisplayStyle.ImageAndText;
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "";
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Эталонный справочник";
            this.radMenuItem2.AccessibleName = "Эталонный справочник";
            this.radMenuItem2.AutoSize = true;
            this.radMenuItem2.Font = new System.Drawing.Font("Segoe UI", 10.25F);
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "Эталонный справочник";
            this.radMenuItem2.Click += new System.EventHandler(this.radMenuItem2_Click);
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.AccessibleDescription = "Справочники поставщиков";
            this.radMenuItem3.AccessibleName = "Справочники поставщиков";
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Text = "Справочники поставщиков";
            this.radMenuItem3.Click += new System.EventHandler(this.radMenuItem3_Click);
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.AccessibleDescription = "Справочники клиентов";
            this.radMenuItem4.AccessibleName = "Справочники клиентов";
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Text = "Справочники клиентов";
            // 
            // radMenuItem5
            // 
            this.radMenuItem5.AccessibleDescription = "Параметры";
            this.radMenuItem5.AccessibleName = "Параметры";
            this.radMenuItem5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem7,
            this.radMenuComboItem1});
            this.radMenuItem5.Name = "radMenuItem5";
            this.radMenuItem5.Text = "Параметры";
            // 
            // radMenuItem7
            // 
            this.radMenuItem7.AccessibleDescription = "Темы";
            this.radMenuItem7.AccessibleName = "Темы";
            this.radMenuItem7.Name = "radMenuItem7";
            this.radMenuItem7.Text = "Темы";
            // 
            // radMenuComboItem1
            // 
            this.radMenuComboItem1.AccessibleDescription = "radMenuComboItem1";
            this.radMenuComboItem1.AccessibleName = "radMenuComboItem1";
            // 
            // 
            // 
            this.radMenuComboItem1.ComboBoxElement.ArrowButtonMinWidth = 17;
            this.radMenuComboItem1.ComboBoxElement.AutoCompleteAppend = null;
            this.radMenuComboItem1.ComboBoxElement.AutoCompleteDataSource = null;
            this.radMenuComboItem1.ComboBoxElement.AutoCompleteSuggest = null;
            this.radMenuComboItem1.ComboBoxElement.DataMember = "";
            this.radMenuComboItem1.ComboBoxElement.DataSource = null;
            this.radMenuComboItem1.ComboBoxElement.DefaultValue = null;
            this.radMenuComboItem1.ComboBoxElement.DisplayMember = "";
            this.radMenuComboItem1.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.radMenuComboItem1.ComboBoxElement.DropDownAnimationEnabled = true;
            this.radMenuComboItem1.ComboBoxElement.EditableElementText = "";
            this.radMenuComboItem1.ComboBoxElement.EditorElement = this.radMenuComboItem1.ComboBoxElement;
            this.radMenuComboItem1.ComboBoxElement.EditorManager = null;
            this.radMenuComboItem1.ComboBoxElement.Filter = null;
            this.radMenuComboItem1.ComboBoxElement.FilterExpression = "";
            this.radMenuComboItem1.ComboBoxElement.Focusable = true;
            this.radMenuComboItem1.ComboBoxElement.FormatString = "";
            this.radMenuComboItem1.ComboBoxElement.FormattingEnabled = true;
            this.radMenuComboItem1.ComboBoxElement.ItemHeight = 18;
            this.radMenuComboItem1.ComboBoxElement.MaxDropDownItems = 0;
            this.radMenuComboItem1.ComboBoxElement.MaxLength = 32767;
            this.radMenuComboItem1.ComboBoxElement.MaxValue = null;
            this.radMenuComboItem1.ComboBoxElement.MinValue = null;
            this.radMenuComboItem1.ComboBoxElement.NullValue = null;
            this.radMenuComboItem1.ComboBoxElement.OwnerOffset = 0;
            this.radMenuComboItem1.ComboBoxElement.ShowImageInEditorArea = true;
            this.radMenuComboItem1.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.radMenuComboItem1.ComboBoxElement.Value = null;
            this.radMenuComboItem1.ComboBoxElement.ValueMember = "";
            this.radMenuComboItem1.Name = "radMenuComboItem1";
            this.radMenuComboItem1.Text = "radMenuComboItem1";
            // 
            // radMenuItem6
            // 
            this.radMenuItem6.AccessibleDescription = "Выход";
            this.radMenuItem6.AccessibleName = "Выход";
            this.radMenuItem6.Alignment = System.Drawing.ContentAlignment.TopLeft;
            this.radMenuItem6.Name = "radMenuItem6";
            this.radMenuItem6.Text = "Выход";
            this.radMenuItem6.Click += new System.EventHandler(this.radMenuItem6_Click_1);
            // 
            // pageMain
            // 
            this.pageMain.Controls.Add(this.pageMainEtalon);
            this.pageMain.Controls.Add(this.pageMainSupplier);
            this.pageMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageMain.Location = new System.Drawing.Point(0, 24);
            this.pageMain.Name = "pageMain";
            this.pageMain.SelectedPage = this.pageMainEtalon;
            this.pageMain.Size = new System.Drawing.Size(1196, 635);
            this.pageMain.TabIndex = 2;
            this.pageMain.SelectedPageChanged += new System.EventHandler(this.pageMain_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pageMain.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // pageMainEtalon
            // 
            this.pageMainEtalon.Controls.Add(this.splitEtalonMain);
            this.pageMainEtalon.ItemSize = new System.Drawing.SizeF(139F, 28F);
            this.pageMainEtalon.Location = new System.Drawing.Point(10, 37);
            this.pageMainEtalon.Name = "pageMainEtalon";
            this.pageMainEtalon.Size = new System.Drawing.Size(1175, 587);
            this.pageMainEtalon.Text = "Эталонный справочник";
            // 
            // splitEtalonMain
            // 
            this.splitEtalonMain.Controls.Add(this.splitEtalonMainTop);
            this.splitEtalonMain.Controls.Add(this.splitEtalonMainCenter);
            this.splitEtalonMain.Controls.Add(this.splitEtalonMainBottom);
            this.splitEtalonMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitEtalonMain.Location = new System.Drawing.Point(0, 0);
            this.splitEtalonMain.Name = "splitEtalonMain";
            this.splitEtalonMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.splitEtalonMain.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonMain.Size = new System.Drawing.Size(1175, 587);
            this.splitEtalonMain.TabIndex = 0;
            this.splitEtalonMain.TabStop = false;
            this.splitEtalonMain.Text = "radSplitContainer1";
            // 
            // splitEtalonMainTop
            // 
            this.splitEtalonMainTop.Controls.Add(this.grbEtalonOpenFile);
            this.splitEtalonMainTop.Controls.Add(this.barEtalonMain);
            this.splitEtalonMainTop.Location = new System.Drawing.Point(0, 0);
            this.splitEtalonMainTop.Name = "splitEtalonMainTop";
            // 
            // 
            // 
            this.splitEtalonMainTop.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonMainTop.Size = new System.Drawing.Size(1175, 144);
            this.splitEtalonMainTop.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.08462868F);
            this.splitEtalonMainTop.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -28);
            this.splitEtalonMainTop.TabIndex = 0;
            this.splitEtalonMainTop.TabStop = false;
            // 
            // grbEtalonOpenFile
            // 
            this.grbEtalonOpenFile.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grbEtalonOpenFile.Controls.Add(this.btnImportConfigAdd);
            this.grbEtalonOpenFile.Controls.Add(this.btnImportConfigEdit);
            this.grbEtalonOpenFile.Controls.Add(this.btnOpenFileCancel);
            this.grbEtalonOpenFile.Controls.Add(this.btnOpenFileOk);
            this.grbEtalonOpenFile.Controls.Add(this.ddlOpenFileConfig);
            this.grbEtalonOpenFile.Controls.Add(this.radLabel2);
            this.grbEtalonOpenFile.Controls.Add(this.radLabel1);
            this.grbEtalonOpenFile.Controls.Add(this.txtOpenFileName);
            this.grbEtalonOpenFile.Dock = System.Windows.Forms.DockStyle.Left;
            this.grbEtalonOpenFile.HeaderText = "Параметры загрузки";
            this.grbEtalonOpenFile.Location = new System.Drawing.Point(0, 30);
            this.grbEtalonOpenFile.Name = "grbEtalonOpenFile";
            this.grbEtalonOpenFile.Size = new System.Drawing.Size(459, 114);
            this.grbEtalonOpenFile.TabIndex = 1;
            this.grbEtalonOpenFile.Text = "Параметры загрузки";
            this.grbEtalonOpenFile.Visible = false;
            // 
            // btnImportConfigAdd
            // 
            this.btnImportConfigAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnImportConfigAdd.Image")));
            this.btnImportConfigAdd.Location = new System.Drawing.Point(395, 48);
            this.btnImportConfigAdd.Name = "btnImportConfigAdd";
            this.btnImportConfigAdd.Size = new System.Drawing.Size(19, 21);
            this.btnImportConfigAdd.TabIndex = 6;
            this.btnImportConfigAdd.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnS_ToolTipTextNeeded);
            this.btnImportConfigAdd.Click += new System.EventHandler(this.btnS_Click);
            // 
            // btnImportConfigEdit
            // 
            this.btnImportConfigEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnImportConfigEdit.Image")));
            this.btnImportConfigEdit.Location = new System.Drawing.Point(370, 48);
            this.btnImportConfigEdit.Name = "btnImportConfigEdit";
            this.btnImportConfigEdit.Size = new System.Drawing.Size(19, 21);
            this.btnImportConfigEdit.TabIndex = 7;
            this.btnImportConfigEdit.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.radButton1_ToolTipTextNeeded);
            this.btnImportConfigEdit.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // btnOpenFileCancel
            // 
            this.btnOpenFileCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFileCancel.Image")));
            this.btnOpenFileCancel.Location = new System.Drawing.Point(214, 74);
            this.btnOpenFileCancel.Name = "btnOpenFileCancel";
            this.btnOpenFileCancel.Size = new System.Drawing.Size(150, 24);
            this.btnOpenFileCancel.TabIndex = 5;
            this.btnOpenFileCancel.Text = "Отмена";
            this.btnOpenFileCancel.Click += new System.EventHandler(this.btnOpenFileCancel_Click);
            // 
            // btnOpenFileOk
            // 
            this.btnOpenFileOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFileOk.Image")));
            this.btnOpenFileOk.Location = new System.Drawing.Point(58, 74);
            this.btnOpenFileOk.Name = "btnOpenFileOk";
            this.btnOpenFileOk.Size = new System.Drawing.Size(150, 24);
            this.btnOpenFileOk.TabIndex = 4;
            this.btnOpenFileOk.Text = "Начать загрузку";
            this.btnOpenFileOk.Click += new System.EventHandler(this.btnOpenFileOk_Click);
            // 
            // ddlOpenFileConfig
            // 
            this.ddlOpenFileConfig.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlOpenFileConfig.Location = new System.Drawing.Point(140, 48);
            this.ddlOpenFileConfig.Name = "ddlOpenFileConfig";
            this.ddlOpenFileConfig.NullText = "не выбрана";
            this.ddlOpenFileConfig.Size = new System.Drawing.Size(224, 20);
            this.ddlOpenFileConfig.TabIndex = 3;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(5, 47);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(129, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Конфигурация загрузки";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(5, 23);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(33, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Файл";
            // 
            // txtOpenFileName
            // 
            this.txtOpenFileName.Location = new System.Drawing.Point(140, 21);
            this.txtOpenFileName.Name = "txtOpenFileName";
            this.txtOpenFileName.ReadOnly = true;
            this.txtOpenFileName.Size = new System.Drawing.Size(224, 20);
            this.txtOpenFileName.TabIndex = 0;
            // 
            // barEtalonMain
            // 
            this.barEtalonMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.barEtalonMain.Location = new System.Drawing.Point(0, 0);
            this.barEtalonMain.Name = "barEtalonMain";
            this.barEtalonMain.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.barEtalonMainRow1});
            this.barEtalonMain.Size = new System.Drawing.Size(1175, 30);
            this.barEtalonMain.TabIndex = 0;
            // 
            // barEtalonMainRow1
            // 
            this.barEtalonMainRow1.MinSize = new System.Drawing.Size(25, 25);
            this.barEtalonMainRow1.Name = "barEtalonMainRow1";
            this.barEtalonMainRow1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.barEtalonMainElement1});
            // 
            // barEtalonMainElement1
            // 
            this.barEtalonMainElement1.DisplayName = "Эталонный справочник";
            this.barEtalonMainElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.barEtalonOpenFile});
            this.barEtalonMainElement1.Name = "barEtalonMainElement1";
            // 
            // barEtalonOpenFile
            // 
            this.barEtalonOpenFile.AccessibleDescription = "Загрузить из файла";
            this.barEtalonOpenFile.AccessibleName = "Загрузить из файла";
            this.barEtalonOpenFile.DisplayName = "Загрузить из файла";
            this.barEtalonOpenFile.DrawText = true;
            this.barEtalonOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("barEtalonOpenFile.Image")));
            this.barEtalonOpenFile.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.barEtalonOpenFile.Name = "barEtalonOpenFile";
            this.barEtalonOpenFile.Text = "Загрузить из файла";
            this.barEtalonOpenFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.barEtalonOpenFile.TextWrap = false;
            this.barEtalonOpenFile.Click += new System.EventHandler(this.commandBarButton1_Click);
            // 
            // splitEtalonMainCenter
            // 
            this.splitEtalonMainCenter.Controls.Add(this.splitEtalonCenter);
            this.splitEtalonMainCenter.Location = new System.Drawing.Point(0, 148);
            this.splitEtalonMainCenter.Name = "splitEtalonMainCenter";
            // 
            // 
            // 
            this.splitEtalonMainCenter.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonMainCenter.Size = new System.Drawing.Size(1175, 410);
            this.splitEtalonMainCenter.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.3747841F);
            this.splitEtalonMainCenter.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 176);
            this.splitEtalonMainCenter.TabIndex = 1;
            this.splitEtalonMainCenter.TabStop = false;
            // 
            // splitEtalonCenter
            // 
            this.splitEtalonCenter.Controls.Add(this.splitEtalonCenterLeft);
            this.splitEtalonCenter.Controls.Add(this.splitEtalonCenterRight);
            this.splitEtalonCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitEtalonCenter.Location = new System.Drawing.Point(0, 0);
            this.splitEtalonCenter.Name = "splitEtalonCenter";
            // 
            // 
            // 
            this.splitEtalonCenter.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonCenter.Size = new System.Drawing.Size(1175, 410);
            this.splitEtalonCenter.TabIndex = 0;
            this.splitEtalonCenter.TabStop = false;
            // 
            // splitEtalonCenterLeft
            // 
            this.splitEtalonCenterLeft.Controls.Add(this.treeDocProductFilter);
            this.splitEtalonCenterLeft.Location = new System.Drawing.Point(0, 0);
            this.splitEtalonCenterLeft.Name = "splitEtalonCenterLeft";
            // 
            // 
            // 
            this.splitEtalonCenterLeft.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonCenterLeft.Size = new System.Drawing.Size(207, 410);
            this.splitEtalonCenterLeft.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.323228F, 0F);
            this.splitEtalonCenterLeft.SizeInfo.SplitterCorrection = new System.Drawing.Size(-344, 0);
            this.splitEtalonCenterLeft.TabIndex = 0;
            this.splitEtalonCenterLeft.TabStop = false;
            // 
            // treeDocProductFilter
            // 
            this.treeDocProductFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeDocProductFilter.Location = new System.Drawing.Point(0, 0);
            this.treeDocProductFilter.Name = "treeDocProductFilter";
            this.treeDocProductFilter.Size = new System.Drawing.Size(207, 410);
            this.treeDocProductFilter.SpacingBetweenNodes = -1;
            this.treeDocProductFilter.TabIndex = 0;
            this.treeDocProductFilter.Text = "radTreeView1";
            this.treeDocProductFilter.NodeCheckedChanged += new Telerik.WinControls.UI.TreeNodeCheckedEventHandler(this.treeDocProductFilter_NodeCheckedChanged);
            // 
            // splitEtalonCenterRight
            // 
            this.splitEtalonCenterRight.Controls.Add(this.splitEtalonCenter2);
            this.splitEtalonCenterRight.Location = new System.Drawing.Point(211, 0);
            this.splitEtalonCenterRight.Name = "splitEtalonCenterRight";
            // 
            // 
            // 
            this.splitEtalonCenterRight.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonCenterRight.Size = new System.Drawing.Size(964, 410);
            this.splitEtalonCenterRight.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.323228F, 0F);
            this.splitEtalonCenterRight.SizeInfo.SplitterCorrection = new System.Drawing.Size(344, 0);
            this.splitEtalonCenterRight.TabIndex = 1;
            this.splitEtalonCenterRight.TabStop = false;
            // 
            // splitEtalonCenter2
            // 
            this.splitEtalonCenter2.Controls.Add(this.splitEtalonCenterTop);
            this.splitEtalonCenter2.Controls.Add(this.splitEtalonCenterBottom);
            this.splitEtalonCenter2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitEtalonCenter2.Location = new System.Drawing.Point(0, 0);
            this.splitEtalonCenter2.Name = "splitEtalonCenter2";
            this.splitEtalonCenter2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.splitEtalonCenter2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonCenter2.Size = new System.Drawing.Size(964, 410);
            this.splitEtalonCenter2.TabIndex = 0;
            this.splitEtalonCenter2.TabStop = false;
            // 
            // splitEtalonCenterTop
            // 
            this.splitEtalonCenterTop.Controls.Add(this.gridEtalonMain);
            this.splitEtalonCenterTop.Controls.Add(this.barEtalonGrid);
            this.splitEtalonCenterTop.Location = new System.Drawing.Point(0, 0);
            this.splitEtalonCenterTop.Name = "splitEtalonCenterTop";
            // 
            // 
            // 
            this.splitEtalonCenterTop.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonCenterTop.Size = new System.Drawing.Size(964, 208);
            this.splitEtalonCenterTop.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.01231527F);
            this.splitEtalonCenterTop.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -32);
            this.splitEtalonCenterTop.TabIndex = 0;
            this.splitEtalonCenterTop.TabStop = false;
            // 
            // gridEtalonMain
            // 
            this.gridEtalonMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEtalonMain.Location = new System.Drawing.Point(0, 30);
            // 
            // 
            // 
            gridViewTextBoxColumn1.AllowFiltering = false;
            gridViewTextBoxColumn1.FieldName = "product_id";
            gridViewTextBoxColumn1.HeaderText = "Код";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 150;
            gridViewTextBoxColumn2.FieldName = "product_name";
            gridViewTextBoxColumn2.HeaderText = "Наименование";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn2.Width = 550;
            gridViewTextBoxColumn3.AllowFiltering = false;
            gridViewTextBoxColumn3.FieldName = "good_cnt";
            gridViewTextBoxColumn3.HeaderText = "Кол-во товаров";
            gridViewTextBoxColumn3.Name = "column3";
            gridViewTextBoxColumn3.Width = 100;
            this.gridEtalonMain.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3});
            this.gridEtalonMain.MasterTemplate.EnableFiltering = true;
            this.gridEtalonMain.MasterTemplate.EnableGrouping = false;
            sortDescriptor1.PropertyName = "column2";
            this.gridEtalonMain.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.gridEtalonMain.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridEtalonMain.Name = "gridEtalonMain";
            this.gridEtalonMain.Size = new System.Drawing.Size(964, 178);
            this.gridEtalonMain.TabIndex = 0;
            this.gridEtalonMain.Text = "radGridView1";
            this.gridEtalonMain.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.gridEtalonMain_RowFormatting);
            this.gridEtalonMain.SelectionChanged += new System.EventHandler(this.gridEtalonMain_SelectionChanged);
            this.gridEtalonMain.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.gridEtalonMain_RowsChanged);
            this.gridEtalonMain.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridEtalonMain_CellValueChanged);
            this.gridEtalonMain.FilterExpressionChanged += new Telerik.WinControls.UI.GridViewFilterExpressionChangedEventHandler(this.gridEtalonMain_FilterExpressionChanged);
            // 
            // barEtalonGrid
            // 
            this.barEtalonGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.barEtalonGrid.Location = new System.Drawing.Point(0, 0);
            this.barEtalonGrid.Name = "barEtalonGrid";
            this.barEtalonGrid.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement1});
            this.barEtalonGrid.Size = new System.Drawing.Size(964, 30);
            this.barEtalonGrid.TabIndex = 0;
            this.barEtalonGrid.Text = "radCommandBar1";
            // 
            // commandBarRowElement1
            // 
            this.commandBarRowElement1.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement1.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement1,
            this.commandBarStripElement2});
            // 
            // commandBarStripElement1
            // 
            this.commandBarStripElement1.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement1.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnGridEtalonSave});
            this.commandBarStripElement1.Name = "commandBarStripElement1";
            // 
            // btnGridEtalonSave
            // 
            this.btnGridEtalonSave.AccessibleDescription = "Сохранить изменения";
            this.btnGridEtalonSave.AccessibleName = "Сохранить изменения";
            this.btnGridEtalonSave.DisplayName = "commandBarButton1";
            this.btnGridEtalonSave.DrawText = true;
            this.btnGridEtalonSave.Enabled = false;
            this.btnGridEtalonSave.Image = ((System.Drawing.Image)(resources.GetObject("btnGridEtalonSave.Image")));
            this.btnGridEtalonSave.Name = "btnGridEtalonSave";
            this.btnGridEtalonSave.Text = "Сохранить изменения";
            this.btnGridEtalonSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGridEtalonSave.Click += new System.EventHandler(this.btnGridEtalonSave_Click);
            // 
            // commandBarStripElement2
            // 
            this.commandBarStripElement2.DisplayName = "commandBarStripElement2";
            this.commandBarStripElement2.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnGridEtalonCancel});
            this.commandBarStripElement2.Name = "commandBarStripElement2";
            // 
            // btnGridEtalonCancel
            // 
            this.btnGridEtalonCancel.AccessibleDescription = "Отменить изменения";
            this.btnGridEtalonCancel.AccessibleName = "Отменить изменения";
            this.btnGridEtalonCancel.DisplayName = "commandBarButton1";
            this.btnGridEtalonCancel.DrawText = true;
            this.btnGridEtalonCancel.Enabled = false;
            this.btnGridEtalonCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnGridEtalonCancel.Image")));
            this.btnGridEtalonCancel.Name = "btnGridEtalonCancel";
            this.btnGridEtalonCancel.Text = "Отменить изменения";
            this.btnGridEtalonCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGridEtalonCancel.Click += new System.EventHandler(this.btnGridEtalonCancel_Click);
            // 
            // splitEtalonCenterBottom
            // 
            this.splitEtalonCenterBottom.Controls.Add(this.splitEtalonCenter3);
            this.splitEtalonCenterBottom.Location = new System.Drawing.Point(0, 212);
            this.splitEtalonCenterBottom.Name = "splitEtalonCenterBottom";
            this.splitEtalonCenterBottom.Size = new System.Drawing.Size(964, 198);
            this.splitEtalonCenterBottom.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.01231527F);
            this.splitEtalonCenterBottom.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 32);
            this.splitEtalonCenterBottom.TabIndex = 1;
            this.splitEtalonCenterBottom.TabStop = false;
            // 
            // splitEtalonCenter3
            // 
            this.splitEtalonCenter3.Controls.Add(this.splitEtalonCenterBottomLeft);
            this.splitEtalonCenter3.Controls.Add(this.splitEtalonCenterBottomRight);
            this.splitEtalonCenter3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitEtalonCenter3.Location = new System.Drawing.Point(0, 0);
            this.splitEtalonCenter3.Name = "splitEtalonCenter3";
            // 
            // 
            // 
            this.splitEtalonCenter3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonCenter3.Size = new System.Drawing.Size(964, 198);
            this.splitEtalonCenter3.TabIndex = 0;
            this.splitEtalonCenter3.TabStop = false;
            this.splitEtalonCenter3.Text = "radSplitContainer1";
            // 
            // splitEtalonCenterBottomLeft
            // 
            this.splitEtalonCenterBottomLeft.Controls.Add(this.radGroupBox2);
            this.splitEtalonCenterBottomLeft.Location = new System.Drawing.Point(0, 0);
            this.splitEtalonCenterBottomLeft.Name = "splitEtalonCenterBottomLeft";
            // 
            // 
            // 
            this.splitEtalonCenterBottomLeft.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonCenterBottomLeft.Size = new System.Drawing.Size(960, 198);
            this.splitEtalonCenterBottomLeft.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.5F, 0F);
            this.splitEtalonCenterBottomLeft.SizeInfo.SplitterCorrection = new System.Drawing.Size(480, 0);
            this.splitEtalonCenterBottomLeft.TabIndex = 0;
            this.splitEtalonCenterBottomLeft.TabStop = false;
            this.splitEtalonCenterBottomLeft.Text = "splitPanel1";
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.gridEtalonGood);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox2.HeaderText = "Товары";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(960, 198);
            this.radGroupBox2.TabIndex = 1;
            this.radGroupBox2.Text = "Товары";
            // 
            // gridEtalonGood
            // 
            this.gridEtalonGood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEtalonGood.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            this.gridEtalonGood.MasterTemplate.AllowAddNewRow = false;
            this.gridEtalonGood.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn4.FieldName = "source_partner_name";
            gridViewTextBoxColumn4.HeaderText = "Поставщик";
            gridViewTextBoxColumn4.Name = "column1";
            gridViewTextBoxColumn5.FieldName = "partner_name";
            gridViewTextBoxColumn5.HeaderText = "Наименование";
            gridViewTextBoxColumn5.Name = "column2";
            gridViewTextBoxColumn5.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn5.Width = 350;
            gridViewTextBoxColumn6.FieldName = "mfr_name";
            gridViewTextBoxColumn6.HeaderText = "Производитель";
            gridViewTextBoxColumn6.Name = "column4";
            gridViewTextBoxColumn6.Width = 150;
            gridViewTextBoxColumn7.FieldName = "barcode";
            gridViewTextBoxColumn7.HeaderText = "ЕАН";
            gridViewTextBoxColumn7.Name = "column3";
            gridViewTextBoxColumn7.Width = 100;
            gridViewTextBoxColumn8.FieldName = "partner_code";
            gridViewTextBoxColumn8.HeaderText = "Код у поставщика";
            gridViewTextBoxColumn8.Name = "column5";
            gridViewTextBoxColumn8.Width = 100;
            this.gridEtalonGood.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8});
            this.gridEtalonGood.MasterTemplate.EnableGrouping = false;
            sortDescriptor2.PropertyName = "column2";
            this.gridEtalonGood.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.gridEtalonGood.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridEtalonGood.Name = "gridEtalonGood";
            this.gridEtalonGood.Size = new System.Drawing.Size(956, 178);
            this.gridEtalonGood.TabIndex = 2;
            this.gridEtalonGood.Text = "radGridView1";
            // 
            // splitEtalonCenterBottomRight
            // 
            this.splitEtalonCenterBottomRight.Controls.Add(this.radGroupBox1);
            this.splitEtalonCenterBottomRight.Location = new System.Drawing.Point(964, 0);
            this.splitEtalonCenterBottomRight.Name = "splitEtalonCenterBottomRight";
            // 
            // 
            // 
            this.splitEtalonCenterBottomRight.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonCenterBottomRight.Size = new System.Drawing.Size(0, 198);
            this.splitEtalonCenterBottomRight.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.5F, 0F);
            this.splitEtalonCenterBottomRight.SizeInfo.SplitterCorrection = new System.Drawing.Size(-480, 0);
            this.splitEtalonCenterBottomRight.TabIndex = 1;
            this.splitEtalonCenterBottomRight.TabStop = false;
            this.splitEtalonCenterBottomRight.Text = "splitPanel2";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.gridEtalonSyn);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox1.HeaderText = "Синонимы";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(0, 198);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Синонимы";
            // 
            // gridEtalonSyn
            // 
            this.gridEtalonSyn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEtalonSyn.Location = new System.Drawing.Point(2, 18);
            // 
            // 
            // 
            gridViewTextBoxColumn9.FieldName = "alias_id";
            gridViewTextBoxColumn9.HeaderText = "column1";
            gridViewTextBoxColumn9.IsVisible = false;
            gridViewTextBoxColumn9.Name = "column1";
            gridViewTextBoxColumn10.FieldName = "alias_name";
            gridViewTextBoxColumn10.HeaderText = "Наименование";
            gridViewTextBoxColumn10.Name = "column2";
            gridViewTextBoxColumn10.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn10.Width = 550;
            this.gridEtalonSyn.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10});
            this.gridEtalonSyn.MasterTemplate.EnableGrouping = false;
            sortDescriptor3.PropertyName = "column2";
            this.gridEtalonSyn.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor3});
            this.gridEtalonSyn.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gridEtalonSyn.Name = "gridEtalonSyn";
            this.gridEtalonSyn.Size = new System.Drawing.Size(0, 178);
            this.gridEtalonSyn.TabIndex = 1;
            this.gridEtalonSyn.Text = "radGridView1";
            this.gridEtalonSyn.RowsChanged += new Telerik.WinControls.UI.GridViewCollectionChangedEventHandler(this.gridEtalonSyn_RowsChanged);
            this.gridEtalonSyn.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridEtalonSyn_CellValueChanged);
            // 
            // splitEtalonMainBottom
            // 
            this.splitEtalonMainBottom.Location = new System.Drawing.Point(0, 562);
            this.splitEtalonMainBottom.Name = "splitEtalonMainBottom";
            // 
            // 
            // 
            this.splitEtalonMainBottom.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitEtalonMainBottom.Size = new System.Drawing.Size(1175, 25);
            this.splitEtalonMainBottom.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2901554F);
            this.splitEtalonMainBottom.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -148);
            this.splitEtalonMainBottom.TabIndex = 2;
            this.splitEtalonMainBottom.TabStop = false;
            // 
            // pageMainSupplier
            // 
            this.pageMainSupplier.Controls.Add(this.splitSupplier);
            this.pageMainSupplier.ItemSize = new System.Drawing.SizeF(159F, 28F);
            this.pageMainSupplier.Location = new System.Drawing.Point(10, 37);
            this.pageMainSupplier.Name = "pageMainSupplier";
            this.pageMainSupplier.Size = new System.Drawing.Size(1175, 587);
            this.pageMainSupplier.Text = "Справочники поставщиков";
            // 
            // splitSupplier
            // 
            this.splitSupplier.Controls.Add(this.splitSupplierTop);
            this.splitSupplier.Controls.Add(this.splitSupplierMain);
            this.splitSupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitSupplier.Location = new System.Drawing.Point(0, 0);
            this.splitSupplier.Name = "splitSupplier";
            this.splitSupplier.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.splitSupplier.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitSupplier.Size = new System.Drawing.Size(1175, 587);
            this.splitSupplier.TabIndex = 0;
            this.splitSupplier.TabStop = false;
            this.splitSupplier.Text = "radSplitContainer1";
            // 
            // splitSupplierTop
            // 
            this.splitSupplierTop.Controls.Add(this.grbSupplierOpenFile);
            this.splitSupplierTop.Controls.Add(this.barSupplierMain);
            this.splitSupplierTop.Location = new System.Drawing.Point(0, 0);
            this.splitSupplierTop.Name = "splitSupplierTop";
            // 
            // 
            // 
            this.splitSupplierTop.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitSupplierTop.Size = new System.Drawing.Size(1175, 164);
            this.splitSupplierTop.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2186964F);
            this.splitSupplierTop.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -128);
            this.splitSupplierTop.TabIndex = 0;
            this.splitSupplierTop.TabStop = false;
            this.splitSupplierTop.Text = "splitPanel1";
            // 
            // grbSupplierOpenFile
            // 
            this.grbSupplierOpenFile.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grbSupplierOpenFile.Controls.Add(this.btnSupplierImportConfigAdd);
            this.grbSupplierOpenFile.Controls.Add(this.btnSupplierImportConfigEdit);
            this.grbSupplierOpenFile.Controls.Add(this.btnSupplierOpenFileCancel);
            this.grbSupplierOpenFile.Controls.Add(this.btnSupplierOpenFileOk);
            this.grbSupplierOpenFile.Controls.Add(this.ddlSupplierOpenFileConfig);
            this.grbSupplierOpenFile.Controls.Add(this.radLabel3);
            this.grbSupplierOpenFile.Controls.Add(this.radLabel4);
            this.grbSupplierOpenFile.Controls.Add(this.txtSupplierOpenFileName);
            this.grbSupplierOpenFile.Dock = System.Windows.Forms.DockStyle.Left;
            this.grbSupplierOpenFile.HeaderText = "Параметры загрузки";
            this.grbSupplierOpenFile.Location = new System.Drawing.Point(0, 0);
            this.grbSupplierOpenFile.Name = "grbSupplierOpenFile";
            this.grbSupplierOpenFile.Size = new System.Drawing.Size(459, 164);
            this.grbSupplierOpenFile.TabIndex = 2;
            this.grbSupplierOpenFile.Text = "Параметры загрузки";
            this.grbSupplierOpenFile.Visible = false;
            // 
            // btnSupplierImportConfigAdd
            // 
            this.btnSupplierImportConfigAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnSupplierImportConfigAdd.Image")));
            this.btnSupplierImportConfigAdd.Location = new System.Drawing.Point(395, 48);
            this.btnSupplierImportConfigAdd.Name = "btnSupplierImportConfigAdd";
            this.btnSupplierImportConfigAdd.Size = new System.Drawing.Size(19, 21);
            this.btnSupplierImportConfigAdd.TabIndex = 6;
            this.btnSupplierImportConfigAdd.Click += new System.EventHandler(this.btnSupplierImportConfigAdd_Click);
            // 
            // btnSupplierImportConfigEdit
            // 
            this.btnSupplierImportConfigEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnSupplierImportConfigEdit.Image")));
            this.btnSupplierImportConfigEdit.Location = new System.Drawing.Point(370, 48);
            this.btnSupplierImportConfigEdit.Name = "btnSupplierImportConfigEdit";
            this.btnSupplierImportConfigEdit.Size = new System.Drawing.Size(19, 21);
            this.btnSupplierImportConfigEdit.TabIndex = 7;
            this.btnSupplierImportConfigEdit.Click += new System.EventHandler(this.btnSupplierImportConfigEdit_Click);
            // 
            // btnSupplierOpenFileCancel
            // 
            this.btnSupplierOpenFileCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnSupplierOpenFileCancel.Image")));
            this.btnSupplierOpenFileCancel.Location = new System.Drawing.Point(214, 74);
            this.btnSupplierOpenFileCancel.Name = "btnSupplierOpenFileCancel";
            this.btnSupplierOpenFileCancel.Size = new System.Drawing.Size(150, 24);
            this.btnSupplierOpenFileCancel.TabIndex = 5;
            this.btnSupplierOpenFileCancel.Text = "Отмена";
            this.btnSupplierOpenFileCancel.Click += new System.EventHandler(this.btnSupplierOpenFileCancel_Click);
            // 
            // btnSupplierOpenFileOk
            // 
            this.btnSupplierOpenFileOk.Image = ((System.Drawing.Image)(resources.GetObject("btnSupplierOpenFileOk.Image")));
            this.btnSupplierOpenFileOk.Location = new System.Drawing.Point(58, 74);
            this.btnSupplierOpenFileOk.Name = "btnSupplierOpenFileOk";
            this.btnSupplierOpenFileOk.Size = new System.Drawing.Size(150, 24);
            this.btnSupplierOpenFileOk.TabIndex = 4;
            this.btnSupplierOpenFileOk.Text = "Начать загрузку";
            this.btnSupplierOpenFileOk.Click += new System.EventHandler(this.btnSupplierOpenFileOk_Click);
            // 
            // ddlSupplierOpenFileConfig
            // 
            this.ddlSupplierOpenFileConfig.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlSupplierOpenFileConfig.Location = new System.Drawing.Point(140, 48);
            this.ddlSupplierOpenFileConfig.Name = "ddlSupplierOpenFileConfig";
            this.ddlSupplierOpenFileConfig.NullText = "не выбрана";
            this.ddlSupplierOpenFileConfig.Size = new System.Drawing.Size(224, 20);
            this.ddlSupplierOpenFileConfig.TabIndex = 3;
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(5, 47);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(129, 18);
            this.radLabel3.TabIndex = 2;
            this.radLabel3.Text = "Конфигурация загрузки";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(5, 23);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(33, 18);
            this.radLabel4.TabIndex = 1;
            this.radLabel4.Text = "Файл";
            // 
            // txtSupplierOpenFileName
            // 
            this.txtSupplierOpenFileName.Location = new System.Drawing.Point(140, 21);
            this.txtSupplierOpenFileName.Name = "txtSupplierOpenFileName";
            this.txtSupplierOpenFileName.ReadOnly = true;
            this.txtSupplierOpenFileName.Size = new System.Drawing.Size(224, 20);
            this.txtSupplierOpenFileName.TabIndex = 0;
            // 
            // barSupplierMain
            // 
            this.barSupplierMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.barSupplierMain.Location = new System.Drawing.Point(0, 0);
            this.barSupplierMain.Name = "barSupplierMain";
            this.barSupplierMain.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement2});
            this.barSupplierMain.Size = new System.Drawing.Size(1175, 0);
            this.barSupplierMain.TabIndex = 1;
            // 
            // commandBarRowElement2
            // 
            this.commandBarRowElement2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement2.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement2.Name = "barEtalonMainRow1";
            this.commandBarRowElement2.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement3});
            this.commandBarRowElement2.Text = "";
            this.commandBarRowElement2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarStripElement3
            // 
            this.commandBarStripElement3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement3.DisplayName = "Эталонный справочник";
            this.commandBarStripElement3.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.barSupplierOpenFile});
            this.commandBarStripElement3.Name = "barEtalonMainElement1";
            this.commandBarStripElement3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // barSupplierOpenFile
            // 
            this.barSupplierOpenFile.AccessibleDescription = "Загрузить из файла";
            this.barSupplierOpenFile.AccessibleName = "Загрузить из файла";
            this.barSupplierOpenFile.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.barSupplierOpenFile.DisplayName = "Загрузить из файла";
            this.barSupplierOpenFile.DrawText = true;
            this.barSupplierOpenFile.Image = ((System.Drawing.Image)(resources.GetObject("barSupplierOpenFile.Image")));
            this.barSupplierOpenFile.ImageAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.barSupplierOpenFile.Name = "barSupplierOpenFile";
            this.barSupplierOpenFile.Text = "Загрузить из файла";
            this.barSupplierOpenFile.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.barSupplierOpenFile.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.barSupplierOpenFile.TextWrap = false;
            this.barSupplierOpenFile.Click += new System.EventHandler(this.barSupplierOpenFile_Click);
            // 
            // splitSupplierMain
            // 
            this.splitSupplierMain.Controls.Add(this.splitSupplierCenter);
            this.splitSupplierMain.Location = new System.Drawing.Point(0, 168);
            this.splitSupplierMain.Name = "splitSupplierMain";
            // 
            // 
            // 
            this.splitSupplierMain.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitSupplierMain.Size = new System.Drawing.Size(1175, 419);
            this.splitSupplierMain.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2186964F);
            this.splitSupplierMain.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 128);
            this.splitSupplierMain.TabIndex = 1;
            this.splitSupplierMain.TabStop = false;
            this.splitSupplierMain.Text = "splitPanel2";
            // 
            // splitSupplierCenter
            // 
            this.splitSupplierCenter.Controls.Add(this.splitSupplieCenterLeft);
            this.splitSupplierCenter.Controls.Add(this.splitSupplieCenterRight);
            this.splitSupplierCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitSupplierCenter.Location = new System.Drawing.Point(0, 0);
            this.splitSupplierCenter.Name = "splitSupplierCenter";
            // 
            // 
            // 
            this.splitSupplierCenter.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitSupplierCenter.Size = new System.Drawing.Size(1175, 419);
            this.splitSupplierCenter.TabIndex = 0;
            this.splitSupplierCenter.TabStop = false;
            this.splitSupplierCenter.Text = "radSplitContainer1";
            // 
            // splitSupplieCenterLeft
            // 
            this.splitSupplieCenterLeft.Controls.Add(this.treePartnerGoodFilter);
            this.splitSupplieCenterLeft.Location = new System.Drawing.Point(0, 0);
            this.splitSupplieCenterLeft.Name = "splitSupplieCenterLeft";
            // 
            // 
            // 
            this.splitSupplieCenterLeft.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitSupplieCenterLeft.Size = new System.Drawing.Size(209, 419);
            this.splitSupplieCenterLeft.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.3215201F, 0F);
            this.splitSupplieCenterLeft.SizeInfo.SplitterCorrection = new System.Drawing.Size(-377, 0);
            this.splitSupplieCenterLeft.TabIndex = 0;
            this.splitSupplieCenterLeft.TabStop = false;
            this.splitSupplieCenterLeft.Text = "splitPanel1";
            // 
            // treePartnerGoodFilter
            // 
            this.treePartnerGoodFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treePartnerGoodFilter.Location = new System.Drawing.Point(0, 0);
            this.treePartnerGoodFilter.Name = "treePartnerGoodFilter";
            this.treePartnerGoodFilter.Size = new System.Drawing.Size(209, 419);
            this.treePartnerGoodFilter.SpacingBetweenNodes = -1;
            this.treePartnerGoodFilter.TabIndex = 1;
            this.treePartnerGoodFilter.Text = "radTreeView1";
            this.treePartnerGoodFilter.NodeCheckedChanged += new Telerik.WinControls.UI.TreeNodeCheckedEventHandler(this.treePartnerGoodFilter_NodeCheckedChanged);
            // 
            // splitSupplieCenterRight
            // 
            this.splitSupplieCenterRight.Controls.Add(this.gridSupplierMain);
            this.splitSupplieCenterRight.Controls.Add(this.barSupplierGrid);
            this.splitSupplieCenterRight.Location = new System.Drawing.Point(213, 0);
            this.splitSupplieCenterRight.Name = "splitSupplieCenterRight";
            // 
            // 
            // 
            this.splitSupplieCenterRight.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitSupplieCenterRight.Size = new System.Drawing.Size(962, 419);
            this.splitSupplieCenterRight.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.3215201F, 0F);
            this.splitSupplieCenterRight.SizeInfo.SplitterCorrection = new System.Drawing.Size(377, 0);
            this.splitSupplieCenterRight.TabIndex = 1;
            this.splitSupplieCenterRight.TabStop = false;
            this.splitSupplieCenterRight.Text = "splitPanel2";
            // 
            // gridSupplierMain
            // 
            this.gridSupplierMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSupplierMain.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridSupplierMain.MasterTemplate.AllowAddNewRow = false;
            this.gridSupplierMain.MasterTemplate.AllowDeleteRow = false;
            this.gridSupplierMain.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn11.FieldName = "partner_code";
            gridViewTextBoxColumn11.HeaderText = "Код у поставщика";
            gridViewTextBoxColumn11.Name = "column1";
            gridViewTextBoxColumn11.Width = 100;
            gridViewTextBoxColumn12.FieldName = "partner_name";
            gridViewTextBoxColumn12.HeaderText = "Наименование у поставщика";
            gridViewTextBoxColumn12.Name = "column2";
            gridViewTextBoxColumn12.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn12.Width = 350;
            gridViewTextBoxColumn13.FieldName = "barcode";
            gridViewTextBoxColumn13.HeaderText = "Штрих-код";
            gridViewTextBoxColumn13.Name = "column3";
            gridViewTextBoxColumn13.Width = 100;
            gridViewTextBoxColumn14.FieldName = "product_name";
            gridViewTextBoxColumn14.HeaderText = "Эталонное наименование";
            gridViewTextBoxColumn14.Name = "column4";
            gridViewTextBoxColumn14.Width = 350;
            gridViewTextBoxColumn15.FieldName = "mfr_name";
            gridViewTextBoxColumn15.HeaderText = "Изготовитель";
            gridViewTextBoxColumn15.Name = "column5";
            gridViewTextBoxColumn15.Width = 150;
            gridViewTextBoxColumn16.FieldName = "source_partner_name";
            gridViewTextBoxColumn16.HeaderText = "Поставщик";
            gridViewTextBoxColumn16.Name = "column8";
            gridViewTextBoxColumn16.Width = 100;
            gridViewTextBoxColumn17.FieldName = "good_id";
            gridViewTextBoxColumn17.HeaderText = "Код товара";
            gridViewTextBoxColumn17.Name = "column6";
            gridViewTextBoxColumn17.ReadOnly = true;
            gridViewTextBoxColumn17.Width = 100;
            gridViewTextBoxColumn18.FieldName = "product_id";
            gridViewTextBoxColumn18.HeaderText = "Код наименования";
            gridViewTextBoxColumn18.Name = "column7";
            gridViewTextBoxColumn18.ReadOnly = true;
            gridViewTextBoxColumn18.Width = 100;
            this.gridSupplierMain.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18});
            this.gridSupplierMain.MasterTemplate.EnableFiltering = true;
            this.gridSupplierMain.MasterTemplate.EnableGrouping = false;
            sortDescriptor4.PropertyName = "column2";
            this.gridSupplierMain.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor4});
            this.gridSupplierMain.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.gridSupplierMain.Name = "gridSupplierMain";
            this.gridSupplierMain.Size = new System.Drawing.Size(962, 419);
            this.gridSupplierMain.TabIndex = 2;
            this.gridSupplierMain.Text = "radGridView1";
            // 
            // barSupplierGrid
            // 
            this.barSupplierGrid.Dock = System.Windows.Forms.DockStyle.Top;
            this.barSupplierGrid.Location = new System.Drawing.Point(0, 0);
            this.barSupplierGrid.Name = "barSupplierGrid";
            this.barSupplierGrid.Rows.AddRange(new Telerik.WinControls.UI.CommandBarRowElement[] {
            this.commandBarRowElement3});
            this.barSupplierGrid.Size = new System.Drawing.Size(962, 0);
            this.barSupplierGrid.TabIndex = 1;
            this.barSupplierGrid.Text = "radCommandBar1";
            // 
            // commandBarRowElement3
            // 
            this.commandBarRowElement3.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarRowElement3.MinSize = new System.Drawing.Size(25, 25);
            this.commandBarRowElement3.Strips.AddRange(new Telerik.WinControls.UI.CommandBarStripElement[] {
            this.commandBarStripElement4,
            this.commandBarStripElement5});
            this.commandBarRowElement3.Text = "";
            this.commandBarRowElement3.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarStripElement4
            // 
            this.commandBarStripElement4.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement4.DisplayName = "commandBarStripElement1";
            this.commandBarStripElement4.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnGridSupplierSave});
            this.commandBarStripElement4.Name = "commandBarStripElement1";
            this.commandBarStripElement4.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // btnGridSupplierSave
            // 
            this.btnGridSupplierSave.AccessibleDescription = "Сохранить изменения";
            this.btnGridSupplierSave.AccessibleName = "Сохранить изменения";
            this.btnGridSupplierSave.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnGridSupplierSave.DisplayName = "commandBarButton1";
            this.btnGridSupplierSave.DrawText = true;
            this.btnGridSupplierSave.Enabled = false;
            this.btnGridSupplierSave.Image = ((System.Drawing.Image)(resources.GetObject("btnGridSupplierSave.Image")));
            this.btnGridSupplierSave.Name = "btnGridSupplierSave";
            this.btnGridSupplierSave.Text = "Сохранить изменения";
            this.btnGridSupplierSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGridSupplierSave.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // commandBarStripElement5
            // 
            this.commandBarStripElement5.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.commandBarStripElement5.DisplayName = "commandBarStripElement2";
            this.commandBarStripElement5.Items.AddRange(new Telerik.WinControls.UI.RadCommandBarBaseItem[] {
            this.btnGridSupplierCancel});
            this.commandBarStripElement5.Name = "commandBarStripElement2";
            this.commandBarStripElement5.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // btnGridSupplierCancel
            // 
            this.btnGridSupplierCancel.AccessibleDescription = "Отменить изменения";
            this.btnGridSupplierCancel.AccessibleName = "Отменить изменения";
            this.btnGridSupplierCancel.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.btnGridSupplierCancel.DisplayName = "commandBarButton1";
            this.btnGridSupplierCancel.DrawText = true;
            this.btnGridSupplierCancel.Enabled = false;
            this.btnGridSupplierCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnGridSupplierCancel.Image")));
            this.btnGridSupplierCancel.Name = "btnGridSupplierCancel";
            this.btnGridSupplierCancel.Text = "Отменить изменения";
            this.btnGridSupplierCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGridSupplierCancel.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // radMenu1
            // 
            this.radMenu1.Font = new System.Drawing.Font("Segoe UI", 10.25F);
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem2,
            this.radMenuItem3,
            this.radMenuItem4,
            this.radMenuItem5,
            this.radMenuItem6});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(1196, 24);
            this.radMenu1.TabIndex = 1;
            this.radMenu1.Text = "radMenu1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1196, 659);
            this.Controls.Add(this.pageMain);
            this.Controls.Add(this.radMenu1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ЕСН Админ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radMenuComboItem1.ComboBoxElement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pageMain)).EndInit();
            this.pageMain.ResumeLayout(false);
            this.pageMainEtalon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonMain)).EndInit();
            this.splitEtalonMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonMainTop)).EndInit();
            this.splitEtalonMainTop.ResumeLayout(false);
            this.splitEtalonMainTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbEtalonOpenFile)).EndInit();
            this.grbEtalonOpenFile.ResumeLayout(false);
            this.grbEtalonOpenFile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportConfigAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportConfigEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenFileCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenFileOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlOpenFileConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOpenFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barEtalonMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonMainCenter)).EndInit();
            this.splitEtalonMainCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenter)).EndInit();
            this.splitEtalonCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterLeft)).EndInit();
            this.splitEtalonCenterLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeDocProductFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterRight)).EndInit();
            this.splitEtalonCenterRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenter2)).EndInit();
            this.splitEtalonCenter2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterTop)).EndInit();
            this.splitEtalonCenterTop.ResumeLayout(false);
            this.splitEtalonCenterTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonMain.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barEtalonGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterBottom)).EndInit();
            this.splitEtalonCenterBottom.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenter3)).EndInit();
            this.splitEtalonCenter3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterBottomLeft)).EndInit();
            this.splitEtalonCenterBottomLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonCenterBottomRight)).EndInit();
            this.splitEtalonCenterBottomRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonSyn.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonSyn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitEtalonMainBottom)).EndInit();
            this.pageMainSupplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplier)).EndInit();
            this.splitSupplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplierTop)).EndInit();
            this.splitSupplierTop.ResumeLayout(false);
            this.splitSupplierTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbSupplierOpenFile)).EndInit();
            this.grbSupplierOpenFile.ResumeLayout(false);
            this.grbSupplierOpenFile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSupplierImportConfigAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSupplierImportConfigEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSupplierOpenFileCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSupplierOpenFileOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSupplierOpenFileConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSupplierOpenFileName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barSupplierMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplierMain)).EndInit();
            this.splitSupplierMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplierCenter)).EndInit();
            this.splitSupplierCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplieCenterLeft)).EndInit();
            this.splitSupplieCenterLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treePartnerGoodFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitSupplieCenterRight)).EndInit();
            this.splitSupplieCenterRight.ResumeLayout(false);
            this.splitSupplieCenterRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSupplierMain.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSupplierMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barSupplierGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem6;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem7;
        private Telerik.WinControls.UI.RadMenuComboItem radMenuComboItem1;
        private Telerik.WinControls.UI.RadPageView pageMain;
        private Telerik.WinControls.UI.RadPageViewPage pageMainEtalon;
        private Telerik.WinControls.UI.RadPageViewPage pageMainSupplier;
        private Telerik.WinControls.UI.RadSplitContainer splitEtalonMain;
        private Telerik.WinControls.UI.SplitPanel splitEtalonMainTop;
        private Telerik.WinControls.UI.SplitPanel splitEtalonMainCenter;
        private Telerik.WinControls.UI.RadSplitContainer splitEtalonCenter;
        private Telerik.WinControls.UI.SplitPanel splitEtalonCenterLeft;
        private Telerik.WinControls.UI.SplitPanel splitEtalonCenterRight;
        private Telerik.WinControls.UI.RadSplitContainer splitEtalonCenter2;
        private Telerik.WinControls.UI.SplitPanel splitEtalonCenterTop;
        private Telerik.WinControls.UI.SplitPanel splitEtalonCenterBottom;
        private Telerik.WinControls.UI.SplitPanel splitEtalonMainBottom;
        private Telerik.WinControls.UI.RadCommandBar barEtalonMain;
        private Telerik.WinControls.UI.CommandBarRowElement barEtalonMainRow1;
        private Telerik.WinControls.UI.CommandBarStripElement barEtalonMainElement1;
        private Telerik.WinControls.UI.CommandBarButton barEtalonOpenFile;
        private Telerik.WinControls.UI.RadGridView gridEtalonMain;
        private Telerik.WinControls.UI.RadGroupBox grbEtalonOpenFile;
        private Telerik.WinControls.UI.RadButton btnOpenFileCancel;
        private Telerik.WinControls.UI.RadButton btnOpenFileOk;
        private Telerik.WinControls.UI.RadDropDownList ddlOpenFileConfig;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadTextBox txtOpenFileName;
        private Telerik.WinControls.UI.RadButton btnImportConfigAdd;
        private Telerik.WinControls.UI.RadButton btnImportConfigEdit;
        private Telerik.WinControls.UI.RadCommandBar barEtalonGrid;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement1;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement1;
        private Telerik.WinControls.UI.CommandBarButton btnGridEtalonSave;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement2;
        private Telerik.WinControls.UI.CommandBarButton btnGridEtalonCancel;
        private Telerik.WinControls.UI.RadSplitContainer splitEtalonCenter3;
        private Telerik.WinControls.UI.SplitPanel splitEtalonCenterBottomLeft;
        private Telerik.WinControls.UI.SplitPanel splitEtalonCenterBottomRight;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGridView gridEtalonSyn;
        private Telerik.WinControls.UI.RadGridView gridEtalonGood;
        private Telerik.WinControls.UI.RadTreeView treeDocProductFilter;
        private Telerik.WinControls.UI.RadSplitContainer splitSupplier;
        private Telerik.WinControls.UI.SplitPanel splitSupplierTop;
        private Telerik.WinControls.UI.RadCommandBar barSupplierMain;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement2;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement3;
        private Telerik.WinControls.UI.CommandBarButton barSupplierOpenFile;
        private Telerik.WinControls.UI.SplitPanel splitSupplierMain;
        private Telerik.WinControls.UI.RadSplitContainer splitSupplierCenter;
        private Telerik.WinControls.UI.SplitPanel splitSupplieCenterLeft;
        private Telerik.WinControls.UI.SplitPanel splitSupplieCenterRight;
        private Telerik.WinControls.UI.RadGroupBox grbSupplierOpenFile;
        private Telerik.WinControls.UI.RadButton btnSupplierImportConfigAdd;
        private Telerik.WinControls.UI.RadButton btnSupplierImportConfigEdit;
        private Telerik.WinControls.UI.RadButton btnSupplierOpenFileCancel;
        private Telerik.WinControls.UI.RadButton btnSupplierOpenFileOk;
        private Telerik.WinControls.UI.RadDropDownList ddlSupplierOpenFileConfig;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txtSupplierOpenFileName;
        private Telerik.WinControls.UI.RadGridView gridSupplierMain;
        private Telerik.WinControls.UI.RadCommandBar barSupplierGrid;
        private Telerik.WinControls.UI.CommandBarRowElement commandBarRowElement3;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement4;
        private Telerik.WinControls.UI.CommandBarButton btnGridSupplierSave;
        private Telerik.WinControls.UI.CommandBarStripElement commandBarStripElement5;
        private Telerik.WinControls.UI.CommandBarButton btnGridSupplierCancel;
        private Telerik.WinControls.UI.RadTreeView treePartnerGoodFilter;

    }
}

