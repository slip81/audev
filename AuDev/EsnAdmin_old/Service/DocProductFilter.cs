﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{

    public class DocProductFilterService : UndBaseService
    {
        public DocProductFilterService(AppManager appManager)
            : base(appManager)
        {
            //
        }

        public List<DocProductFilter> GetDocProductFilter()
        {
            List<DocProductFilter> res = dbContext.vw_doc_product_filter
                .Select(ss => new DocProductFilter
                {
                    doc_id = ss.doc_id,
                    doc_name = ss.doc_name,
                    product_cnt = ss.product_cnt,
                    parent_id = -1,
                })
                .ToList();

            res.Add(new DocProductFilter()
                {
                    doc_id = -1,
                    doc_name = "Все источники",
                    product_cnt = null,
                    parent_id = null,
                });

            return res;
        }
    }
}