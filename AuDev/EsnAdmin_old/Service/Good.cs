﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{
    public class GoodServiceResult : ServiceResult
    {
        public GoodServiceResult()
            : base()
        {
            //
        }

        public GoodServiceResult(Exception ex)
            : base(ex)
        {
            //
        }

        public List<Good> goods { get; set; }
        public int good_id_max { get; set; }
    }

    public class GoodService : UndBaseService
    {
        public GoodService(AppManager appManager)
            : base(appManager)
        {
            //
        }

        public GoodServiceResult GetGoods(int? partner_id)
        {
            try
            {
                List<Good> res = dbContext.vw_partner_good
                    .Where(ss => (partner_id.HasValue && ss.partner_id == partner_id) || (!partner_id.HasValue))
                    .Select(ss => new Good()
                    {
                        item_id = ss.item_id,
                        partner_id = ss.partner_id,
                        good_id = ss.good_id,
                        partner_name = ss.partner_name,
                        source_partner_name = ss.source_partner_name,
                        partner_code = ss.partner_code,
                        product_id = ss.product_id,
                        mfr_id = ss.mfr_id,
                        barcode = ss.barcode,
                        doc_id = ss.doc_id,
                        doc_row_id = ss.doc_row_id,
                        product_name = ss.product_name,
                        mfr_name = ss.mfr_name,
                    })
                    .ToList();
                int? res_max = res.Max(ss => (int?)ss.good_id);
                return new GoodServiceResult() { Error = null, Id = 0, goods = new List<Good>(res), good_id_max = res_max.GetValueOrDefault(0), };
            }
            catch (Exception ex)
            {
                return new GoodServiceResult(ex);
            }            
        }
    }
}