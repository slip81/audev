﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Core;

namespace EsnAdmin.Service
{

    public class DocImportResult : ServiceResult
    {
        public DocImportResult()
            : base()
        {
            //
        }

        public DocImportResult(Exception ex)
            : base(ex)
        {
            //
        }

        public int? import_id { get; set; }
        public int? doc_id { get; set; }
    }

    public class DocImportEtalonResult : DocImportResult
    {
        public DocImportEtalonResult()
            :base()
        {
            //
        }

        public DocImportEtalonResult(Exception ex)
            :base(ex)
        {
            //
        }

        public int? product_cnt { get; set; }
    }

    public class DocImportSupplierResult : DocImportResult
    {
        public DocImportSupplierResult()
            : base()
        {
            //
        }

        public DocImportSupplierResult(Exception ex)
            : base(ex)
        {
            //
        }

        public int? good_cnt { get; set; }
        public int? partner_id { get; set; }
    }

    public class DocImportService: UndBaseService
    {
        public DocImportService(AppManager appManager)
            :base(appManager)
        {
            //
        }

        public DocImportEtalonResult ImportEtalonFromXlsx(string file_path, int config_type_id)
        {
            try
            {
                string file_name = Path.GetFileName(file_path);                
                int? doc_id = 0;
                int product_cnt = 0;
                string userName = appManager.CurrentUser.Name;

                DocImportResult importRes = xImportFromXlsx(file_path, config_type_id);
                if (importRes.Error != null)
                {
                    return new DocImportEtalonResult()
                    {
                        Id = importRes.Id,
                        Error = importRes.Error,
                    };
                }
                int? import_id = importRes.import_id;
                product_cnt = dbContext.Database.SqlQuery<int>("select * from und.move_import_to_etalon(@p0, @p1)", import_id, userName).FirstOrDefault();
                doc_id = dbContext.doc_import.Where(ss => ss.import_id == import_id).Select(ss => ss.doc_id).FirstOrDefault();

                return new DocImportEtalonResult() { Id = 0, Error = null, import_id = import_id, product_cnt = product_cnt, doc_id = doc_id };
                
            }
            catch (Exception ex)
            {
                return new DocImportEtalonResult(ex);
            }
        }

        public DocImportSupplierResult ImportSupplierFromXlsx(string file_path, int config_type_id)
        {
            try
            {
                string file_name = Path.GetFileName(file_path);                                
                string userName = appManager.CurrentUser.Name;

                DocImportResult importRes = xImportFromXlsx(file_path, config_type_id);
                if (importRes.Error != null)
                {
                    return new DocImportSupplierResult()
                    {
                        Id = importRes.Id,
                        Error = importRes.Error,
                    };
                }
                int? import_id = importRes.import_id;
                int good_cnt = dbContext.Database.SqlQuery<int>("select * from und.move_import_to_good(@p0, @p1)", import_id, userName).FirstOrDefault();
                doc_import doc_import = dbContext.doc_import.Where(ss => ss.import_id == import_id).FirstOrDefault();
                int? doc_id = doc_import != null ? doc_import.doc_id : null;
                int? partner_id = doc_import != null ? (int?)dbContext.doc_import_config_type.Where(ss => ss.config_type_id == doc_import.config_type_id).Select(ss => ss.source_partner_id).FirstOrDefault() : null;


                return new DocImportSupplierResult() { Id = 0, Error = null, import_id = import_id, good_cnt = good_cnt, doc_id = doc_id, partner_id = partner_id };

            }
            catch (Exception ex)
            {
                return new DocImportSupplierResult(ex);
            }
        }

        private DocImportResult xImportFromXlsx(string file_path, int config_type_id)
        {
            string file_name = Path.GetFileName(file_path);
            int import_id = 0;
            int? doc_id = 0;            
            using (var stream = File.OpenRead(file_path))
            {
                DateTime now = DateTime.Now;
                string userName = appManager.CurrentUser.Name;

                doc_import_config_type doc_import_config_type = dbContext.doc_import_config_type.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
                if (doc_import_config_type == null)
                {
                    return new DocImportResult()
                    {
                        Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                        Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найдена конфигурация импорта с кодом " + config_type_id.ToString() },
                    };
                }

                Workbook workbook;
                IWorkbookFormatProvider formatProvider = new XlsxFormatProvider();

                workbook = formatProvider.Import(stream);

                Worksheet workSheet = workbook.Worksheets[(int)doc_import_config_type.list_num];

                CellRange usedCellRange = workSheet.UsedCellRange;

                int columnFirstIndex = usedCellRange.FromIndex.ColumnIndex;
                int columnLastIndex = usedCellRange.ToIndex.ColumnIndex;

                //BindingList<doc_import_row> res_rows = null;
                doc_import doc_import = null;

                doc_import = new doc_import();
                doc_import.config_type_id = doc_import_config_type.config_type_id;
                doc_import.file_path = file_path;
                doc_import.file_name = file_name;
                doc_import.crt_date = now;
                doc_import.crt_user = appManager.CurrentUser.Name;
                doc_import.upd_date = now;
                doc_import.upd_user = appManager.CurrentUser.Name;
                dbContext.doc_import.Add(doc_import);
                dbContext.SaveChanges();
                import_id = doc_import.import_id;

                doc_import_row doc_import_row = null;
                int cnt = 0;
                AuMainDb transactionContext = null;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
                {
                    transactionContext = null;
                    try
                    {
                        transactionContext = new AuMainDb();
                        transactionContext.Configuration.AutoDetectChangesEnabled = false;


                        cnt = 0;
                        // !!!
                        for (int i = doc_import_config_type.row_num; i <= usedCellRange.ToIndex.RowIndex; i++)
                        //for (int i = usedCellRange.FromIndex.RowIndex + 1; i <= usedCellRange.ToIndex.RowIndex; i++)
                        {
                            ++cnt;

                            doc_import_row = new doc_import_row();
                            //doc_import_row.doc_import = doc_import;
                            doc_import_row.import_id = import_id;
                            for (int j = columnFirstIndex; j <= columnLastIndex; j++)
                            {
                                CellSelection cell = workSheet.Cells[i, j];
                                string cellValue = cell.GetValue().Value.RawValue;

                                if (j == doc_import_config_type.barcode_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.barcode = cellValue;
                                }
                                if (j == doc_import_config_type.code_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.code = cellValue;
                                }
                                if (j == doc_import_config_type.etalon_name_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.etalon_name = cellValue;
                                }
                                if (j == doc_import_config_type.mfr_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.mfr = cellValue;
                                }
                                if (j == doc_import_config_type.mfr_group_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.mfr_group = cellValue;
                                }
                                if (j == doc_import_config_type.source_name_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.source_name = cellValue;
                                }

                            }
                            doc_import_row.crt_date = now;
                            doc_import_row.crt_user = userName;
                            doc_import_row.upd_date = now;
                            doc_import_row.upd_user = userName;

                            transactionContext = AddToContext<doc_import_row>(transactionContext, doc_import_row, cnt, 100, true);
                        }

                        transactionContext.SaveChanges();

                    }
                    finally
                    {
                        if (transactionContext != null)
                            transactionContext.Dispose();
                    }

                    scope.Complete();

                }
            }
                
            return new DocImportResult() { Id = 0, Error = null, import_id = import_id, doc_id = doc_id };
        }

        public BindingList<doc_import_row> GetDocImportRows(int? import_id)
        {
            dbContext.doc_import_row.Where(ss => ss.import_id == import_id).Load();
            return dbContext.doc_import_row.Local.ToBindingList();
        }
        
        public List<partner> GetDocImportSources()
        {
            return dbContext.partner.Where(ss => ss.is_source).ToList();
        }

        public doc_import_config_type GetDocImportConfigType(int config_type_id)
        {
            return dbContext.doc_import_config_type.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
        }

        public List<doc_import_config_type> GetDocImportConfigTypes()
        {
            return dbContext.doc_import_config_type.ToList();
        }

        public bool InsertDocImportConfigType(string config_type_name, int source_partner_id,
                int list_num, int row_num, int source_name_col, int? etalon_name_col, int? code_col,
                int? mfr_col, int? mfr_group_col, int? barcode_col
            )
        {
            doc_import_config_type doc_import_config_type = new doc_import_config_type();
            doc_import_config_type.config_type_name = config_type_name;
            doc_import_config_type.source_partner_id = source_partner_id;
            doc_import_config_type.list_num = list_num;
            doc_import_config_type.row_num = row_num;
            doc_import_config_type.source_name_col = source_name_col;
            doc_import_config_type.etalon_name_col = etalon_name_col;
            doc_import_config_type.code_col = code_col;
            doc_import_config_type.mfr_col = mfr_col;
            doc_import_config_type.mfr_group_col = mfr_group_col;
            doc_import_config_type.barcode_col = barcode_col;
            dbContext.doc_import_config_type.Add(doc_import_config_type);
            dbContext.SaveChanges();
            return true;
        }

        public bool UpdateDocImportConfigType(int config_type_id, string config_type_name, int source_partner_id,
                int list_num, int row_num, int source_name_col, int? etalon_name_col, int? code_col,
                int? mfr_col, int? mfr_group_col, int? barcode_col
            )
        {
            doc_import_config_type doc_import_config_type = dbContext.doc_import_config_type.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
            doc_import_config_type.config_type_name = config_type_name;
            doc_import_config_type.source_partner_id = source_partner_id;
            doc_import_config_type.list_num = list_num;
            doc_import_config_type.row_num = row_num;
            doc_import_config_type.source_name_col = source_name_col;
            doc_import_config_type.etalon_name_col = etalon_name_col;
            doc_import_config_type.code_col = code_col;
            doc_import_config_type.mfr_col = mfr_col;
            doc_import_config_type.mfr_group_col = mfr_group_col;
            doc_import_config_type.barcode_col = barcode_col;            
            dbContext.SaveChanges();
            return true;
        }

    }
}

