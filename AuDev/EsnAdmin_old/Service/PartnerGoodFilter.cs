﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{

    public class PartnerGoodFilterService : UndBaseService
    {
        public PartnerGoodFilterService(AppManager appManager)
            : base(appManager)
        {
            //
        }

        public List<PartnerGoodFilter> GetPartnerGoodFilter()
        {
            List<PartnerGoodFilter> res = dbContext.vw_partner_good_filter
                .Select(ss => new PartnerGoodFilter
                {
                    partner_id = ss.partner_id,
                    partner_name = ss.partner_name,
                    good_cnt = ss.good_cnt,
                    parent_id = -1,
                })
                .ToList();

            res.Add(new PartnerGoodFilter()
                {
                    partner_id = -1,
                    partner_name = "Все поставщики",
                    good_cnt = null,
                    parent_id = null,
                });

            return res;
        }
    }
}