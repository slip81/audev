﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using EsnAdmin.Core;


namespace EsnAdmin.Service
{
    public class UndBaseService : IDisposable
    {
        protected AppManager appManager;

        protected AuMainDb dbContext
        {
            get
            {
                /*
                if (_dbContext == null)
                {
                    _dbContext = new AuMainDb();
                }
                return _dbContext;
                */
                return appManager != null ? appManager.dbContext : null;
            }
        }
        //private AuMainDb _dbContext;

        
        public UndBaseService()
        {                      
            //
        }
        
        public UndBaseService(AppManager _appManager)
        {
            appManager = _appManager;
        }        

        public void Dispose()
        {
            /*
            if (_dbContext != null)
            {                
                _dbContext.Dispose();
            }
            */
        }

        // Fastest Way of Inserting in Entity Framework
        // http://stackoverflow.com/questions/5940225/fastest-way-of-inserting-in-entity-framework

        protected AuMainDb AddToContext<TEntity>(AuMainDb context,
            TEntity entity, int count, int commitCount, bool recreateContext)
            where TEntity : class
        {
            context.Set<TEntity>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }
    }

}
