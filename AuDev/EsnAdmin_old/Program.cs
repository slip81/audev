﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using EsnAdmin.Core;

namespace EsnAdmin
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            MainForm mainForm = new MainForm(AppManagerFactory.CreateAppManager());
            Application.Run(mainForm);
            //                        
        }

    }
}
