﻿namespace EsnAdmin
{
    partial class ImportConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportConfigForm));
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.ddlImportConfigPartner = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.edImportConfigPageNum = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txtImportConfigName = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.edImportConfigSourceNameCol = new Telerik.WinControls.UI.RadSpinEditor();
            this.edImportConfigEtalonNameCol = new Telerik.WinControls.UI.RadSpinEditor();
            this.edImportConfigEtalonCodeCol = new Telerik.WinControls.UI.RadSpinEditor();
            this.edImportConfigMfrCol = new Telerik.WinControls.UI.RadSpinEditor();
            this.btnOpenFileCancel = new Telerik.WinControls.UI.RadButton();
            this.btnOpenFileOk = new Telerik.WinControls.UI.RadButton();
            this.edImportConfigMfrGroupCol = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.edImportConfigBarcodeCol = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.edImportConfigRowNum = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel12 = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlImportConfigPartner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigPageNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImportConfigName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigSourceNameCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigEtalonNameCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigEtalonCodeCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigMfrCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenFileCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenFileOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigMfrGroupCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigBarcodeCol)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigRowNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(13, 54);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(67, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "Источник *:";
            this.radLabel1.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.radLabel5_ToolTipTextNeeded);
            // 
            // ddlImportConfigPartner
            // 
            this.ddlImportConfigPartner.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlImportConfigPartner.Location = new System.Drawing.Point(216, 52);
            this.ddlImportConfigPartner.Name = "ddlImportConfigPartner";
            this.ddlImportConfigPartner.Size = new System.Drawing.Size(175, 20);
            this.ddlImportConfigPartner.TabIndex = 1;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(13, 93);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(157, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Номер листа в Excel-файле *:";
            this.radLabel2.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.radLabel5_ToolTipTextNeeded);
            // 
            // edImportConfigPageNum
            // 
            this.edImportConfigPageNum.Location = new System.Drawing.Point(216, 91);
            this.edImportConfigPageNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.edImportConfigPageNum.Name = "edImportConfigPageNum";
            this.edImportConfigPageNum.Size = new System.Drawing.Size(100, 20);
            this.edImportConfigPageNum.TabIndex = 3;
            this.edImportConfigPageNum.TabStop = false;
            this.edImportConfigPageNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel3.Location = new System.Drawing.Point(13, 171);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(110, 18);
            this.radLabel3.TabIndex = 4;
            this.radLabel3.Text = "Привязка колонок";
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(13, 12);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(145, 18);
            this.radLabel4.TabIndex = 5;
            this.radLabel4.Text = "Название конфигурации *:";
            this.radLabel4.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.radLabel5_ToolTipTextNeeded);
            // 
            // txtImportConfigName
            // 
            this.txtImportConfigName.Location = new System.Drawing.Point(216, 12);
            this.txtImportConfigName.Name = "txtImportConfigName";
            this.txtImportConfigName.Size = new System.Drawing.Size(175, 20);
            this.txtImportConfigName.TabIndex = 6;
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(13, 244);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(158, 18);
            this.radLabel5.TabIndex = 7;
            this.radLabel5.Text = "Наименование у источника *";
            this.radLabel5.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.radLabel5_ToolTipTextNeeded);
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(13, 279);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(141, 18);
            this.radLabel6.TabIndex = 8;
            this.radLabel6.Text = "Наименование эталонное";
            // 
            // radLabel7
            // 
            this.radLabel7.Location = new System.Drawing.Point(13, 315);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(91, 18);
            this.radLabel7.TabIndex = 9;
            this.radLabel7.Text = "Код у источника";
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(13, 350);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(87, 18);
            this.radLabel8.TabIndex = 10;
            this.radLabel8.Text = "Производитель";
            // 
            // radLabel9
            // 
            this.radLabel9.AutoSize = false;
            this.radLabel9.Location = new System.Drawing.Point(13, 195);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(257, 35);
            this.radLabel9.TabIndex = 11;
            this.radLabel9.Text = "нужно указать номера колонок в Excel-файле, соответствующие следующим атрибутам:";
            // 
            // edImportConfigSourceNameCol
            // 
            this.edImportConfigSourceNameCol.Location = new System.Drawing.Point(216, 244);
            this.edImportConfigSourceNameCol.Name = "edImportConfigSourceNameCol";
            this.edImportConfigSourceNameCol.Size = new System.Drawing.Size(100, 20);
            this.edImportConfigSourceNameCol.TabIndex = 12;
            this.edImportConfigSourceNameCol.TabStop = false;
            // 
            // edImportConfigEtalonNameCol
            // 
            this.edImportConfigEtalonNameCol.Location = new System.Drawing.Point(216, 279);
            this.edImportConfigEtalonNameCol.Name = "edImportConfigEtalonNameCol";
            this.edImportConfigEtalonNameCol.Size = new System.Drawing.Size(100, 20);
            this.edImportConfigEtalonNameCol.TabIndex = 13;
            this.edImportConfigEtalonNameCol.TabStop = false;
            // 
            // edImportConfigEtalonCodeCol
            // 
            this.edImportConfigEtalonCodeCol.Location = new System.Drawing.Point(216, 315);
            this.edImportConfigEtalonCodeCol.Name = "edImportConfigEtalonCodeCol";
            this.edImportConfigEtalonCodeCol.Size = new System.Drawing.Size(100, 20);
            this.edImportConfigEtalonCodeCol.TabIndex = 14;
            this.edImportConfigEtalonCodeCol.TabStop = false;
            // 
            // edImportConfigMfrCol
            // 
            this.edImportConfigMfrCol.Location = new System.Drawing.Point(216, 350);
            this.edImportConfigMfrCol.Name = "edImportConfigMfrCol";
            this.edImportConfigMfrCol.Size = new System.Drawing.Size(100, 20);
            this.edImportConfigMfrCol.TabIndex = 15;
            this.edImportConfigMfrCol.TabStop = false;
            // 
            // btnOpenFileCancel
            // 
            this.btnOpenFileCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFileCancel.Image")));
            this.btnOpenFileCancel.Location = new System.Drawing.Point(256, 486);
            this.btnOpenFileCancel.Name = "btnOpenFileCancel";
            this.btnOpenFileCancel.Size = new System.Drawing.Size(150, 24);
            this.btnOpenFileCancel.TabIndex = 17;
            this.btnOpenFileCancel.Text = "Отмена";
            this.btnOpenFileCancel.Click += new System.EventHandler(this.btnOpenFileCancel_Click);
            // 
            // btnOpenFileOk
            // 
            this.btnOpenFileOk.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenFileOk.Image")));
            this.btnOpenFileOk.Location = new System.Drawing.Point(100, 486);
            this.btnOpenFileOk.Name = "btnOpenFileOk";
            this.btnOpenFileOk.Size = new System.Drawing.Size(150, 24);
            this.btnOpenFileOk.TabIndex = 16;
            this.btnOpenFileOk.Text = "Сохранить";
            this.btnOpenFileOk.Click += new System.EventHandler(this.btnOpenFileOk_Click);
            // 
            // edImportConfigMfrGroupCol
            // 
            this.edImportConfigMfrGroupCol.Location = new System.Drawing.Point(216, 387);
            this.edImportConfigMfrGroupCol.Name = "edImportConfigMfrGroupCol";
            this.edImportConfigMfrGroupCol.Size = new System.Drawing.Size(100, 20);
            this.edImportConfigMfrGroupCol.TabIndex = 19;
            this.edImportConfigMfrGroupCol.TabStop = false;
            // 
            // radLabel10
            // 
            this.radLabel10.Location = new System.Drawing.Point(13, 387);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(145, 18);
            this.radLabel10.TabIndex = 18;
            this.radLabel10.Text = "Групповой производитель";
            // 
            // edImportConfigBarcodeCol
            // 
            this.edImportConfigBarcodeCol.Location = new System.Drawing.Point(216, 422);
            this.edImportConfigBarcodeCol.Name = "edImportConfigBarcodeCol";
            this.edImportConfigBarcodeCol.Size = new System.Drawing.Size(100, 20);
            this.edImportConfigBarcodeCol.TabIndex = 21;
            this.edImportConfigBarcodeCol.TabStop = false;
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(13, 422);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(27, 18);
            this.radLabel11.TabIndex = 20;
            this.radLabel11.Text = "ЕАН";
            // 
            // edImportConfigRowNum
            // 
            this.edImportConfigRowNum.Location = new System.Drawing.Point(216, 128);
            this.edImportConfigRowNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.edImportConfigRowNum.Name = "edImportConfigRowNum";
            this.edImportConfigRowNum.Size = new System.Drawing.Size(100, 20);
            this.edImportConfigRowNum.TabIndex = 23;
            this.edImportConfigRowNum.TabStop = false;
            this.edImportConfigRowNum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // radLabel12
            // 
            this.radLabel12.Location = new System.Drawing.Point(14, 130);
            this.radLabel12.Name = "radLabel12";
            this.radLabel12.Size = new System.Drawing.Size(200, 18);
            this.radLabel12.TabIndex = 22;
            this.radLabel12.Text = "Номер начальной строки с данными:";
            // 
            // ImportConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 522);
            this.Controls.Add(this.edImportConfigRowNum);
            this.Controls.Add(this.radLabel12);
            this.Controls.Add(this.edImportConfigBarcodeCol);
            this.Controls.Add(this.radLabel11);
            this.Controls.Add(this.edImportConfigMfrGroupCol);
            this.Controls.Add(this.radLabel10);
            this.Controls.Add(this.btnOpenFileCancel);
            this.Controls.Add(this.btnOpenFileOk);
            this.Controls.Add(this.edImportConfigMfrCol);
            this.Controls.Add(this.edImportConfigEtalonCodeCol);
            this.Controls.Add(this.edImportConfigEtalonNameCol);
            this.Controls.Add(this.edImportConfigSourceNameCol);
            this.Controls.Add(this.radLabel9);
            this.Controls.Add(this.radLabel8);
            this.Controls.Add(this.radLabel7);
            this.Controls.Add(this.radLabel6);
            this.Controls.Add(this.radLabel5);
            this.Controls.Add(this.txtImportConfigName);
            this.Controls.Add(this.radLabel4);
            this.Controls.Add(this.radLabel3);
            this.Controls.Add(this.edImportConfigPageNum);
            this.Controls.Add(this.radLabel2);
            this.Controls.Add(this.ddlImportConfigPartner);
            this.Controls.Add(this.radLabel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportConfigForm";
            this.Text = "Конфигурация импорта";
            this.Load += new System.EventHandler(this.ImportConfigForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlImportConfigPartner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigPageNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImportConfigName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigSourceNameCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigEtalonNameCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigEtalonCodeCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigMfrCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenFileCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnOpenFileOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigMfrGroupCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigBarcodeCol)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportConfigRowNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel12)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList ddlImportConfigPartner;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadSpinEditor edImportConfigPageNum;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadSpinEditor edImportConfigSourceNameCol;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txtImportConfigName;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadSpinEditor edImportConfigEtalonNameCol;
        private Telerik.WinControls.UI.RadSpinEditor edImportConfigEtalonCodeCol;
        private Telerik.WinControls.UI.RadSpinEditor edImportConfigMfrCol;
        private Telerik.WinControls.UI.RadButton btnOpenFileCancel;
        private Telerik.WinControls.UI.RadButton btnOpenFileOk;
        private Telerik.WinControls.UI.RadSpinEditor edImportConfigMfrGroupCol;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadSpinEditor edImportConfigBarcodeCol;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadSpinEditor edImportConfigRowNum;
        private Telerik.WinControls.UI.RadLabel radLabel12;
    }
}