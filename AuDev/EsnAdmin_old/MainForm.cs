﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Telerik.WinControls;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Localization;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Service;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;



namespace EsnAdmin
{
    public partial class MainForm : Form
    {
        AppManager appManager;
        ImportConfigForm importConfigForm;

        private DocImportService docImportService
        {
            get
            {
                if (_docImportService == null)
                    _docImportService = new DocImportService(appManager);
                return _docImportService;
            }
        }
        private DocImportService _docImportService;

        private ProductService productService
        {
            get
            {
                if (_productService == null)
                    _productService = new ProductService(appManager);
                return _productService;
            }
        }
        private ProductService _productService;

        private GoodService goodService
        {
            get
            {
                if (_goodService == null)
                    _goodService = new GoodService(appManager);
                return _goodService;
            }
        }
        private GoodService _goodService;

        private DocProductFilterService docProductFilterService
        {
            get
            {
                if (_docProductFilterService == null)
                    _docProductFilterService = new DocProductFilterService(appManager);
                return _docProductFilterService;
            }
        }
        private DocProductFilterService _docProductFilterService;

        private PartnerGoodFilterService partnerGoodFilterService
        {
            get
            {
                if (_partnerGoodFilterService == null)
                    _partnerGoodFilterService = new PartnerGoodFilterService(appManager);
                return _partnerGoodFilterService;
            }
        }
        private PartnerGoodFilterService _partnerGoodFilterService;

        private List<Product> addedProducts
        {
            get
            {
                if (_addedProducts == null)
                    _addedProducts = new List<Product>();
                return _addedProducts;
            }
        }
        private List<Product> _addedProducts;

        private List<Product> modifiedProducts
        {
            get
            {
                if (_modifiedProducts == null)
                    _modifiedProducts = new List<Product>();
                return _modifiedProducts;
            }
        }
        private List<Product> _modifiedProducts;

        private List<Product> deletedProducts
        {
            get
            {
                if (_deletedProducts == null)
                    _deletedProducts = new List<Product>();
                return _deletedProducts;
            }
        }
        private List<Product> _deletedProducts;


        private List<ProductAlias> addedProductAliases
        {
            get
            {
                if (_addedProductAliases == null)
                    _addedProductAliases = new List<ProductAlias>();
                return _addedProductAliases;
            }
        }
        private List<ProductAlias> _addedProductAliases;

        private List<ProductAlias> modifiedProductAliases
        {
            get
            {
                if (_modifiedProductAliases == null)
                    _modifiedProductAliases = new List<ProductAlias>();
                return _modifiedProductAliases;
            }
        }
        private List<ProductAlias> _modifiedProductAliases;

        private List<ProductAlias> deletedProductAliases
        {
            get
            {
                if (_deletedProductAliases == null)
                    _deletedProductAliases = new List<ProductAlias>();
                return _deletedProductAliases;
            }
        }
        private List<ProductAlias> _deletedProductAliases;

        private int product_id_next { get; set; }
        private int alias_id_next { get; set; }

        private bool lockEvents { get; set; }
        private bool gridSupplierGood_needRefresh { get; set; }

        public MainForm()
        {
            InitializeComponent();
        }

        public MainForm(AppManager _appManager)            
        {            
            InitializeComponent();
            appManager = _appManager;
            //
            RadGridLocalizationProvider.CurrentProvider = new RadGridLocalization();
            Application.ApplicationExit += Application_ApplicationExit;
            lockEvents = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            gridSupplierGood_needRefresh = true;
            //
            OpenFileConfigDdl_bind();
            SupplierOpenFileConfigDdl_bind();
            //            
            var productServiceResult = productService.GetProducts();
            if (productServiceResult.Error == null)
            {
                product_id_next = productServiceResult.product_id_max + 1;
                alias_id_next = productServiceResult.alias_id_max + 1;
                //                
                GridViewSummaryItem summaryItemRowCount = new GridViewSummaryItem("column2", "Всего строк: {0}", GridAggregateFunction.Count);
                GridViewSummaryRowItem summaryRowItem = new GridViewSummaryRowItem(
                    new GridViewSummaryItem[] { summaryItemRowCount });
                gridEtalonMain.SummaryRowsBottom.Add(summaryRowItem);
                gridEtalonMain.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Bottom;                    
                //
                gridEtalonMain.DataSource = productServiceResult.products;
                gridEtalonMain.TableElement.ScrollToRow(0);
                //
                summaryItemRowCount = new GridViewSummaryItem("column2", "Всего строк: {0}", GridAggregateFunction.Count);
                summaryRowItem = new GridViewSummaryRowItem(
                    new GridViewSummaryItem[] { summaryItemRowCount });
                gridSupplierMain.SummaryRowsBottom.Add(summaryRowItem);
                gridSupplierMain.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Bottom;  
                //
                TreeDocProductFilter_bind(null);
                TreePartnerGoodFilter_bind(null);
            }
            else
            {
                MessageBox.Show("Ошибка получения списка наименований: " + productServiceResult.Error.Mess);
            }
        }

        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            appManager.Dispose();
        }

        private void radMenuItem2_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Эталонный справочник");            
            pageMain.SelectedPage = pageMainEtalon;
        }

        private void radMenuItem6_Click_1(object sender, EventArgs e)
        {            
            if (RadMessageBox.Show(this, "Закрыть программу?", "Выход", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                Application.Exit();
        }

        private void btnOpenFileCancel_Click(object sender, EventArgs e)
        {
            grbEtalonOpenFile.Visible = false;
        }

        private void commandBarButton1_Click(object sender, EventArgs e)
        {            
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.InitialDirectory = "c:\\";
            //openFile.Filter = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx|XML файлы (*.xml)|*.xml";
            //openFile.Filter = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx";
            openFile.Filter = "Excel файлы (*.xlsx)|*.xlsx";
            //openFile.FilterIndex = 1;
            //openFile.RestoreDirectory = false;

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                txtOpenFileName.Text = openFile.FileName;
                grbEtalonOpenFile.Visible = true;
            }
        }

        //private void StartDocImport(Stream stream, int config_type_id)
        private void StartDocImport(string file_path, int config_type_id)
        {
            var res = docImportService.ImportEtalonFromXlsx(file_path, config_type_id);
            grbEtalonOpenFile.Visible = false;
            if (res.Error != null)
            {
                MessageBox.Show("Ошибка: " + res.Error.Mess);
            }
            else
            {
                var productServiceResult = productService.GetProducts();
                if (productServiceResult.Error == null)
                {
                    product_id_next = productServiceResult.product_id_max + 1;
                    alias_id_next = productServiceResult.alias_id_max + 1;
                    gridEtalonMain.DataSource = productServiceResult.products;
                    gridEtalonMain.TableElement.ScrollToRow(0);
                    if (res.product_cnt > 0)
                    {
                        TreeDocProductFilter_bind(res.doc_id);                        
                    }
                    MessageBox.Show("Новых строк: " + res.product_cnt.ToString());
                }
                else
                {
                    MessageBox.Show("Ошибка получения списка наименований: " + productServiceResult.Error.Mess);
                }
            }
        }

        private void btnOpenFileOk_Click(object sender, EventArgs e)
        {
            if (ddlOpenFileConfig.SelectedIndex <= -1)
            {
                MessageBox.Show("Не выбрана конфигурация загрузки");
                return;
            }

            var stream = File.OpenRead(txtOpenFileName.Text);
            Cursor = Cursors.WaitCursor;
            try
            {
                /*
                if (stream != null)
                {
                    StartDocImport(stream, (int)ddlOpenFileConfig.SelectedValue);
                }
                */
                StartDocImport(txtOpenFileName.Text, (int)ddlOpenFileConfig.SelectedValue);

                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при чтении файла: " + GlobalUtil.ExceptionInfo(ex));
            }
        }

        private void btnS_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Создать новую конфигурацию";
        }

        private void btnS_Click(object sender, EventArgs e)
        {
            if (importConfigForm == null)
                importConfigForm = new ImportConfigForm(appManager);
            importConfigForm.CurrentConfigTypeId = 0;

            if (importConfigForm.ShowDialog() == DialogResult.OK)
            {
                //MessageBox.Show("DialogResult.OK");
                OpenFileConfigDdl_bind(true);
            }
        }

        private void OpenFileConfigDdl_bind(bool selectLast = false)
        {
            ddlOpenFileConfig.DataSource = null;
            ddlOpenFileConfig.DataSource = docImportService.GetDocImportConfigTypes();
            ddlOpenFileConfig.DisplayMember = "config_type_name";
            ddlOpenFileConfig.ValueMember = "config_type_id";            
            if ((ddlOpenFileConfig.Items != null) && (selectLast))
            {
                ddlOpenFileConfig.SelectedIndex = ddlOpenFileConfig.Items.Count - 1;
            }
            else
            {
                ddlOpenFileConfig.SelectedIndex = 0;
            }
        }

        private void SupplierOpenFileConfigDdl_bind(bool selectLast = false)
        {
            ddlSupplierOpenFileConfig.DataSource = null;
            ddlSupplierOpenFileConfig.DataSource = docImportService.GetDocImportConfigTypes();
            ddlSupplierOpenFileConfig.DisplayMember = "config_type_name";
            ddlSupplierOpenFileConfig.ValueMember = "config_type_id";
            if ((ddlSupplierOpenFileConfig.Items != null) && (selectLast))
            {
                ddlSupplierOpenFileConfig.SelectedIndex = ddlSupplierOpenFileConfig.Items.Count - 1;
            }
            else
            {
                ddlSupplierOpenFileConfig.SelectedIndex = 0;
            }
        }

        private void TreeDocProductFilter_bind(int? selected_id)
        {
            treeDocProductFilter.DataSource = docProductFilterService.GetDocProductFilter();
            treeDocProductFilter.DisplayMember = "doc_name";
            treeDocProductFilter.ParentMember = "parent_id";
            treeDocProductFilter.ChildMember = "doc_id";
            treeDocProductFilter.CheckBoxes = true;
            treeDocProductFilter.ShowExpandCollapse = false;            
            treeDocProductFilter.ExpandAll();
            //
            if (selected_id.HasValue)
            {
                Predicate<RadTreeNode> match = new Predicate<RadTreeNode>(delegate(RadTreeNode node)
                {
                    return ((DocProductFilter)node.DataBoundItem).doc_id == selected_id ? true : false;
                });
                RadTreeNode selected_node = treeDocProductFilter.FindNodes(match).FirstOrDefault();
                if (selected_node != null)
                {
                    selected_node.Checked = true;
                }                
            }
        }

        private void TreePartnerGoodFilter_bind(int? selected_id)
        {
            treePartnerGoodFilter.DataSource = partnerGoodFilterService.GetPartnerGoodFilter();
            treePartnerGoodFilter.DisplayMember = "partner_name";
            treePartnerGoodFilter.ParentMember = "parent_id";
            treePartnerGoodFilter.ChildMember = "partner_id";
            treePartnerGoodFilter.CheckBoxes = true;
            treePartnerGoodFilter.ShowExpandCollapse = false;
            treePartnerGoodFilter.ExpandAll();
            // !!!
            /*
            if (selected_id.HasValue)
            {
                Predicate<RadTreeNode> match = new Predicate<RadTreeNode>(delegate(RadTreeNode node)
                {
                    return ((DocProductFilter)node.DataBoundItem).doc_id == selected_id ? true : false;
                });
                RadTreeNode selected_node = treeDocProductFilter.FindNodes(match).FirstOrDefault();
                if (selected_node != null)
                {
                    selected_node.Checked = true;
                }
            }
            */
        }
    

        private void radButton1_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Изменить выбранную конфигурацию";
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            if (ddlOpenFileConfig.SelectedIndex <= -1)
                return;

            if (importConfigForm == null)
                importConfigForm = new ImportConfigForm(appManager);
            importConfigForm.CurrentConfigTypeId = (int)ddlOpenFileConfig.SelectedValue;
            importConfigForm.ShowDialog();
        }


        private void btnGridEtalonSave_Click(object sender, EventArgs e)
        {
            var saveProductsResult = productService.SaveProducts(addedProducts, modifiedProducts, deletedProducts, addedProductAliases, modifiedProductAliases, deletedProductAliases);
            if (saveProductsResult.Error != null)
            {
                MessageBox.Show("Ошибка сохранения списка наименований: " + saveProductsResult.Error.Mess);
                return;
            }
            //
            addedProducts.Clear();
            deletedProducts.Clear();
            modifiedProducts.Clear();
            addedProductAliases.Clear();
            modifiedProductAliases.Clear();
            deletedProductAliases.Clear();
            //
            int selectedRowIndex = gridEtalonMain.SelectedRows != null ? gridEtalonMain.SelectedRows[0].Index : 0;
            //
            var productServiceResult = productService.GetProducts();
            if (productServiceResult.Error == null)
            {
                product_id_next = productServiceResult.product_id_max + 1;
                alias_id_next = productServiceResult.alias_id_max + 1;
                gridEtalonMain.DataSource = productServiceResult.products;
                gridEtalonMain.TableElement.ScrollToRow(selectedRowIndex);             
            }
            else
            {
                MessageBox.Show("Ошибка получения списка наименований: " + productServiceResult.Error.Mess);
            }
            //
            btnGridEtalonSave.Enabled = false;
            btnGridEtalonCancel.Enabled = false;
        }

        private void btnGridEtalonCancel_Click(object sender, EventArgs e)
        {
            addedProducts.Clear();
            deletedProducts.Clear();
            modifiedProducts.Clear();
            addedProductAliases.Clear();
            modifiedProductAliases.Clear();
            deletedProductAliases.Clear();
            //
            int selectedRowIndex = gridEtalonMain.SelectedRows != null ? gridEtalonMain.SelectedRows[0].Index : 0;
            //
            var productServiceResult = productService.GetProducts();
            if (productServiceResult.Error == null)
            {
                product_id_next = productServiceResult.product_id_max + 1;
                alias_id_next = productServiceResult.alias_id_max + 1;
                gridEtalonMain.DataSource = productServiceResult.products;
                gridEtalonMain.TableElement.ScrollToRow(selectedRowIndex);
            }
            else
            {
                MessageBox.Show("Ошибка получения списка наименований: " + productServiceResult.Error.Mess);
            }
            //
            btnGridEtalonSave.Enabled = false;
            btnGridEtalonCancel.Enabled = false;            
        }

        private void treeDocProductFilter_NodeCheckedChanged(object sender, Telerik.WinControls.UI.TreeNodeCheckedEventArgs e)
        {
           
            bool showAll = false;
            DocProductFilter selectedNode = (DocProductFilter)e.Node.DataBoundItem;
            int doc_id = selectedNode.doc_id;
            if (doc_id == -1)
            {
                showAll = true;
                getTreeDocProductFilterNodes(e.Node, treeDocProductFilter.Nodes, e.Node.Checked);
            }            
        }

        private void getTreeDocProductFilterNodes(RadTreeNode sourceNode, IEnumerable<RadTreeNode> treeNodes, bool check)
        {
            foreach (var treeNode in treeNodes)
            {
                if (treeNode != sourceNode)
                {
                    treeNode.Checked = check;
                }
                getTreeDocProductFilterNodes(sourceNode, treeNode.Nodes, check);
            }

        }

        private void gridEtalonMain_RowFormatting(object sender, RowFormattingEventArgs e)
        {            
            List<int> checkedDocIdList = new List<int>();
            var checkedNodes = treeDocProductFilter.CheckedNodes;            
            if (checkedNodes.Count > 0)
            {
                checkedDocIdList = checkedNodes.Select(ss => ((DocProductFilter)ss.DataBoundItem).doc_id).ToList();
                Product currRowProduct = (Product)e.RowElement.RowInfo.DataBoundItem;
                if ((currRowProduct != null) && (checkedDocIdList.Contains(currRowProduct.doc_id.GetValueOrDefault(0))))                
                {
                    e.RowElement.ForeColor = Color.Green;
                }
                else
                {
                    e.RowElement.ResetValue(LightVisualElement.ForeColorProperty, ValueResetFlags.Local);
                }
            }
            else
            {
                e.RowElement.ResetValue(LightVisualElement.ForeColorProperty, ValueResetFlags.Local);
            }
        }

        private void radMenuItem3_Click(object sender, EventArgs e)
        {
            pageMain.SelectedPage = pageMainSupplier;
        }

        private void gridEtalonMain_CellValueChanged(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            //MessageBox.Show("gridEtalonMain_CellValueChanged !");
            if (lockEvents)
                return;
            if (e.Row is GridViewFilteringRowInfo)
                return;
            lockEvents = true;
            try
            {
                Product currProduct = (Product)e.Row.DataBoundItem;
                bool addMode = currProduct == null;
                if (addMode)
                {
                    Product product = new Product();
                    product.product_id = product_id_next;
                    product.product_name = e.Value.ToString();
                    addedProducts.Add(product);
                    //
                    e.Row.Cells[0].Value = product_id_next;
                    //
                    product_id_next = product_id_next + 1;
                }
                else
                {
                    var modifiedProduct = modifiedProducts.Where(ss => ss.product_id == currProduct.product_id).FirstOrDefault();
                    if (modifiedProduct != null)
                    {
                        modifiedProduct.product_name = currProduct.product_name;
                    }
                    else
                    {
                        modifiedProducts.Add(new Product()
                        {
                            product_id = currProduct.product_id,
                            product_name = currProduct.product_name,
                        }
                            );
                    }
                    //
                    var deletedProduct = deletedProducts.Where(ss => ss.product_id == currProduct.product_id).FirstOrDefault();
                    if (deletedProduct != null)
                    {
                        deletedProducts.Remove(deletedProduct);
                    }
                    //
                    var addedProduct = addedProducts.Where(ss => ss.product_id == currProduct.product_id).FirstOrDefault();
                    if (addedProduct != null)
                    {
                        addedProducts.Remove(addedProduct);
                    }
                }
                btnGridEtalonCancel.Enabled = true;
                btnGridEtalonSave.Enabled = true;
            }
            finally
            {
                lockEvents = false;
            }
        }

        private void gridEtalonMain_RowsChanged(object sender, GridViewCollectionChangedEventArgs e)
        {
            if (lockEvents)
                return;
            lockEvents = true;
            try
            {
                if (e.Action == NotifyCollectionChangedAction.Remove)
                {
                    GridViewDataRowInfo deletedRow = e.OldItems.Cast<GridViewDataRowInfo>().FirstOrDefault();
                    if (deletedRow != null)
                    {
                        var deletedProductId = deletedRow.Cells[0].Value;
                        var deletedProductName = deletedRow.Cells[1].Value;                        
                        if ((int)deletedProductId > 0)
                        {
                            int deletedProductId_int = (int)deletedProductId;
                            string deletedProductName_string = (string)deletedProductName;
                            var deletedProduct = deletedProducts.Where(ss => ss.product_id == deletedProductId_int).FirstOrDefault();
                            if (deletedProduct == null)
                            {
                                deletedProducts.Add(new Product()
                                {
                                    product_id = deletedProductId_int,
                                    product_name = deletedProductName_string,
                                }
                                );
                                //
                                var modifiedProduct = modifiedProducts.Where(ss => ss.product_id == deletedProductId_int).FirstOrDefault();
                                if (modifiedProduct != null)
                                {
                                    modifiedProducts.Remove(modifiedProduct);
                                }
                                //
                                var addedProduct = addedProducts.Where(ss => ss.product_id == deletedProductId_int).FirstOrDefault();
                                if (addedProduct != null)
                                {
                                    addedProducts.Remove(addedProduct);
                                }
                            }
                            btnGridEtalonCancel.Enabled = true;
                            btnGridEtalonSave.Enabled = true;
                        }
                    }
                }
            }
            finally
            {
                lockEvents = false;
            }
        }

        private void gridEtalonMain_SelectionChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("gridEtalonMain_SelectionChanged !");
            if (lockEvents)
                return;
            lockEvents = true;
            try
            {
                var currentProductId = gridEtalonMain.SelectedRows != null && gridEtalonMain.SelectedRows.Count > 0 ? gridEtalonMain.SelectedRows[0].Cells[0].Value : null;
                if (currentProductId != null)
                {
                    GridEtalonSyn_bind((int)currentProductId);
                    GridEtalonGood_bind((int)currentProductId);
                }
            }
            finally
            {
                lockEvents = false;
            }

        }

        private void GridEtalonSyn_bind(int currentProductId)
        {
            //gridEtalonSyn.DataSource = productServiceResult.products;
            var aliases = ((List<Product>)gridEtalonMain.DataSource).Where(ss => ss.product_id == currentProductId).Select(ss => ss.aliases).FirstOrDefault();
            gridEtalonSyn.DataSource = aliases;
        }

        private void gridEtalonSyn_CellValueChanged(object sender, GridViewCellEventArgs e)
        {
            if (lockEvents)
                return;
            lockEvents = true;
            try
            {
                int? currentProductId = gridEtalonMain.SelectedRows != null && gridEtalonMain.SelectedRows.Count > 0 ? (int?)gridEtalonMain.SelectedRows[0].Cells[0].Value : null;
                if (!currentProductId.HasValue)
                    currentProductId = 0;
                ProductAlias currAlias = (ProductAlias)e.Row.DataBoundItem;
                bool addMode = currAlias == null;
                if (addMode)
                {
                    ProductAlias product_alias = new ProductAlias();                    
                    product_alias.product_id = (int)currentProductId;
                    product_alias.alias_id = alias_id_next;
                    product_alias.alias_name = e.Value.ToString();
                    addedProductAliases.Add(product_alias);
                    //
                    e.Row.Cells[0].Value = alias_id_next;
                    //
                    alias_id_next = alias_id_next + 1;
                }
                else
                {
                    var modifiedProductAlias = modifiedProductAliases.Where(ss => ss.alias_id == currAlias.alias_id).FirstOrDefault();
                    if (modifiedProductAlias != null)
                    {
                        modifiedProductAlias.alias_name = currAlias.alias_name;
                    }
                    else
                    {
                        modifiedProductAliases.Add(new ProductAlias()
                        {
                            product_id = (int)currentProductId,
                            alias_id = currAlias.alias_id,
                            alias_name = currAlias.alias_name,
                        }
                            );
                    }
                    //
                    var deletedProductAlias = deletedProductAliases.Where(ss => ss.alias_id == currAlias.alias_id).FirstOrDefault();
                    if (deletedProductAlias != null)
                    {
                        deletedProductAliases.Remove(deletedProductAlias);
                    }
                    //
                    var addedProductAlias = addedProductAliases.Where(ss => ss.alias_id == currAlias.alias_id).FirstOrDefault();
                    if (addedProductAlias != null)
                    {
                        addedProductAliases.Remove(addedProductAlias);
                    }
                }
                btnGridEtalonCancel.Enabled = true;
                btnGridEtalonSave.Enabled = true;
            }
            finally
            {
                lockEvents = false;
            }
        }

        private void gridEtalonSyn_RowsChanged(object sender, GridViewCollectionChangedEventArgs e)
        {
            if (lockEvents)
                return;
            lockEvents = true;
            try
            {
                if (e.Action == NotifyCollectionChangedAction.Remove)
                {
                    GridViewDataRowInfo deletedRow = e.OldItems.Cast<GridViewDataRowInfo>().FirstOrDefault();
                    if (deletedRow != null)
                    {
                        var deletedAliasId = deletedRow.Cells[0].Value;
                        var deletedAliasName = deletedRow.Cells[1].Value;
                        if ((int)deletedAliasId > 0)
                        {
                            int? currentProductId = gridEtalonMain.SelectedRows != null && gridEtalonMain.SelectedRows.Count > 0 ? (int?)gridEtalonMain.SelectedRows[0].Cells[0].Value : null;
                            if (!currentProductId.HasValue)
                                currentProductId = 0;
                            int deletedAliasId_int = (int)deletedAliasId;
                            string deletedAliasName_string = (string)deletedAliasName;
                            var deletedProductAlias = deletedProductAliases.Where(ss => ss.alias_id == deletedAliasId_int).FirstOrDefault();
                            if (deletedProductAlias == null)
                            {
                                deletedProductAliases.Add(new ProductAlias()
                                {
                                    alias_id = deletedAliasId_int,
                                    alias_name = deletedAliasName_string,
                                    product_id = (int)currentProductId,
                                }
                                );
                                //
                                var modifiedProductAlias = modifiedProductAliases.Where(ss => ss.alias_id == deletedAliasId_int).FirstOrDefault();
                                if (modifiedProductAlias != null)
                                {
                                    modifiedProductAliases.Remove(modifiedProductAlias);
                                }
                                //
                                var addedProductAlias = addedProductAliases.Where(ss => ss.alias_id == deletedAliasId_int).FirstOrDefault();
                                if (addedProductAlias != null)
                                {
                                    addedProductAliases.Remove(addedProductAlias);
                                }
                            }
                            btnGridEtalonCancel.Enabled = true;
                            btnGridEtalonSave.Enabled = true;
                        }
                    }
                }
            }
            finally
            {
                lockEvents = false;
            }
        }

        private void barSupplierOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.InitialDirectory = "c:\\";
            openFile.Filter = "Excel файлы (*.xlsx)|*.xlsx";

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                txtSupplierOpenFileName.Text = openFile.FileName;
                grbSupplierOpenFile.Visible = true;
            }
        }

        private void btnSupplierOpenFileOk_Click(object sender, EventArgs e)
        {
            if (ddlSupplierOpenFileConfig.SelectedIndex <= -1)
            {
                MessageBox.Show("Не выбрана конфигурация загрузки");
                return;
            }

            var stream = File.OpenRead(txtSupplierOpenFileName.Text);
            Cursor = Cursors.WaitCursor;
            try
            {                
                StartSupplierDocImport(txtSupplierOpenFileName.Text, (int)ddlSupplierOpenFileConfig.SelectedValue);
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при чтении файла: " + GlobalUtil.ExceptionInfo(ex));
            }
        }

        private void btnSupplierOpenFileCancel_Click(object sender, EventArgs e)
        {
            grbSupplierOpenFile.Visible = false;
        }

        private void btnSupplierImportConfigEdit_Click(object sender, EventArgs e)
        {
            if (ddlSupplierOpenFileConfig.SelectedIndex <= -1)
                return;

            if (importConfigForm == null)
                importConfigForm = new ImportConfigForm(appManager);
            importConfigForm.CurrentConfigTypeId = (int)ddlSupplierOpenFileConfig.SelectedValue;
            importConfigForm.ShowDialog();
        }

        private void btnSupplierImportConfigAdd_Click(object sender, EventArgs e)
        {
            if (importConfigForm == null)
                importConfigForm = new ImportConfigForm(appManager);
            importConfigForm.CurrentConfigTypeId = 0;

            if (importConfigForm.ShowDialog() == DialogResult.OK)
            {      
                SupplierOpenFileConfigDdl_bind(true);
            }
        }

        private void StartSupplierDocImport(string file_path, int config_type_id)
        {
            var res = docImportService.ImportSupplierFromXlsx(file_path, config_type_id);
            grbSupplierOpenFile.Visible = false;
            if (res.Error != null)
            {
                MessageBox.Show("Ошибка: " + res.Error.Mess);
            }
            else
            {
                // !!!
                GridSupplierGood_bind(res.partner_id.GetValueOrDefault(0));
                MessageBox.Show("Новых строк: " + res.good_cnt.ToString());
            }
        }

        private void gridEtalonMain_FilterExpressionChanged(object sender, FilterExpressionChangedEventArgs e)
        {
            // !!!
            // column2 LIKE '%асп 20%' -> (column2 LIKE '%асп%') AND (column2 LIKE '%20%')

            string currentFilter = e.FilterExpression;
            string origFilter = currentFilter;
            try
            {
                string col_name = "column2";
                string operator_name = "LIKE";
                string first_replace_symbol = "'%";
                string last_replace_symbol = "%'";
                string border_symbol = "%";
                string filterValue = "";

                if (String.IsNullOrWhiteSpace(currentFilter))
                    return;

                bool forReplace = currentFilter.Contains(col_name) && currentFilter.Contains(operator_name);
                if (!forReplace)
                    return;

                string currentFilter_forReplace = currentFilter.Substring(currentFilter.IndexOf(col_name), currentFilter.LastIndexOf(last_replace_symbol) - currentFilter.IndexOf(col_name) + 1);
                if (String.IsNullOrWhiteSpace(currentFilter_forReplace))
                    return;

                filterValue = currentFilter_forReplace.Substring(currentFilter_forReplace.IndexOf(border_symbol), currentFilter_forReplace.LastIndexOf(border_symbol) - currentFilter_forReplace.IndexOf(border_symbol) + 1);
                List<string> filterValue_split = filterValue.Split(' ').Select(ss => ss.Replace("%", String.Empty)).Where(ss => !String.IsNullOrWhiteSpace(ss)).ToList();
                if ((filterValue_split == null) || (filterValue_split.Count <= 1))
                    return;

                currentFilter = "";
                bool first = true;
                foreach (var filterValue_splitItem in filterValue_split)
                {
                    if (!first)
                        currentFilter += " AND ";
                    currentFilter += "(" + col_name + " " + operator_name + " " + first_replace_symbol + filterValue_splitItem + last_replace_symbol + ")";
                    first = false;
                }
                e.FilterExpression = currentFilter;
            }
            catch
            {
                e.FilterExpression = origFilter;
            }
        }

        private void pageMain_SelectedPageChanged(object sender, EventArgs e)
        {
            if (pageMain.SelectedPage == pageMainSupplier)
            {
                if (gridSupplierGood_needRefresh)
                {
                    gridSupplierGood_needRefresh = false;
                    // !!!
                    GridSupplierGood_bind(null);
                }
            }
        }

        private void GridSupplierGood_bind(int? partner_id)
        {
            var goodServiceResult = goodService.GetGoods(partner_id);
            if (goodServiceResult.Error == null)
            {                
                gridSupplierMain.DataSource = goodServiceResult.goods;
                gridSupplierMain.TableElement.ScrollToRow(0);                
            }
            else
            {
                MessageBox.Show("Ошибка получения списка товаров: " + goodServiceResult.Error.Mess);
            }
        }

        private void GridEtalonGood_bind(int currentProductId)
        {                    
            var goods = ((List<Product>)gridEtalonMain.DataSource).Where(ss => ss.product_id == currentProductId).Select(ss => ss.goods).FirstOrDefault();
            gridEtalonGood.DataSource = goods;
        }

        private void treePartnerGoodFilter_NodeCheckedChanged(object sender, TreeNodeCheckedEventArgs e)
        {
            bool showAll = false;
            PartnerGoodFilter selectedNode = (PartnerGoodFilter)e.Node.DataBoundItem;
            int partner_id = selectedNode.partner_id;
            if (partner_id == -1)
            {
                showAll = true;
                getTreePartnerGoodFilterNodes(e.Node, treePartnerGoodFilter.Nodes, e.Node.Checked);
            }  
        }

        private void getTreePartnerGoodFilterNodes(RadTreeNode sourceNode, IEnumerable<RadTreeNode> treeNodes, bool check)
        {
            foreach (var treeNode in treeNodes)
            {
                if (treeNode != sourceNode)
                {
                    treeNode.Checked = check;
                }
                getTreePartnerGoodFilterNodes(sourceNode, treeNode.Nodes, check);
            }

        }

    }
}


