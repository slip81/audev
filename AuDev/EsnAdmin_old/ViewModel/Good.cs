﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace EsnAdmin.ViewModel
{
    public class Good
    {
        public Good()
        {
            // vw_partner_good
        }

        // db
        public int item_id { get; set; }
        public int partner_id { get; set; }
        public int good_id { get; set; }
        public string partner_code { get; set; }
        public string partner_name { get; set; }
        public string source_partner_name { get; set; }
        public int product_id { get; set; }
        public int mfr_id { get; set; }
        public string barcode { get; set; }
        public Nullable<int> doc_id { get; set; }
        public Nullable<int> doc_row_id { get; set; }
        public string product_name { get; set; }
        public string mfr_name { get; set; }

        // additional
        // public List<ProductAlias> aliases { get; set; }
    }
}