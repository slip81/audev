﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace EsnAdmin.ViewModel
{
    public class Product
    {
        public Product()
        {
            //product
            //vw_product
        }

        // db
        public int product_id { get; set; }
        public Nullable<int> product_form_id { get; set; }
        public Nullable<int> pharm_market_id { get; set; }
        public Nullable<int> inn_id { get; set; }
        public Nullable<int> dosage_id { get; set; }
        public Nullable<int> brand_id { get; set; }
        public Nullable<int> space_id { get; set; }
        public Nullable<int> metaname_id { get; set; }
        public string product_name { get; set; }
        public string trade_name { get; set; }
        public string trade_name_lat { get; set; }
        public string trade_name_eng { get; set; }
        public Nullable<decimal> storage_period { get; set; }
        public bool is_vital { get; set; }
        public bool is_powerful { get; set; }
        public bool is_poison { get; set; }
        public bool is_receipt { get; set; }
        public Nullable<int> drug { get; set; }
        public string size { get; set; }
        public Nullable<decimal> potion_amount { get; set; }
        public Nullable<int> pack_count { get; set; }
        public string link { get; set; }
        public string mess { get; set; }
        public Nullable<int> doc_id { get; set; }
        public Nullable<int> doc_row_id { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
        public Nullable<int> good_cnt { get; set; }

        // additional
        public List<ProductAlias> aliases { get; set; }
        public List<Good> goods { get; set; }
    }
}