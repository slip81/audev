﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsnAdmin.Core
{
    public static class AppManagerFactory
    {
        private static bool inited = false;

        public static AppManager CreateAppManager()
        {
            if (inited)
                throw new Exception("CreateAppManager: already inited");
            inited = true;
            return new AppManager();
        }
    }
}
