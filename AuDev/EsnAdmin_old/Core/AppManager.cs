﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuDev.Common.Db.Model;

namespace EsnAdmin.Core
{
    public class AppManager : IDisposable
    {        
        private static AppUser currentUser;

        public AuMainDb dbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = new AuMainDb();
                }
                return _dbContext;
            }
        }
        private AuMainDb _dbContext;

        public AppManager()
        {
            //
        }

        static AppManager()
        {
            currentUser = new AppUser();
        }

        public AppUser CurrentUser
        {
            get { return currentUser; }
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
