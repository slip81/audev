﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsnAdmin.Core
{
    public class AppUser
    {
        private string name;
        private bool isAuthorized;

        public AppUser()
        {
            // !!!
            name = "ПМС";
        }

        public string Name
        {
            get { return name; }
        }

        public bool IsAuthorized
        {
            get { return isAuthorized; }
        }
        
    }
}
