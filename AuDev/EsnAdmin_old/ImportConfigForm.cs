﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EsnAdmin.Core;
using EsnAdmin.Service;

namespace EsnAdmin
{
    public partial class ImportConfigForm : Form
    {

        bool ddlImportConfigPartner_inited = false;
        AppManager appManager;

        private DocImportService docImportService
        {
            get
            {
                if (_docImportService == null)
                    _docImportService = new DocImportService(appManager);
                return _docImportService;
            }
        }
        private DocImportService _docImportService;

        public int CurrentConfigTypeId { internal get; set; }        

        public ImportConfigForm()
        {
            InitializeComponent();
        }

        public ImportConfigForm(AppManager _appManager)
        {            
            InitializeComponent();
            appManager = _appManager;
        }

        private void btnOpenFileOk_Click(object sender, EventArgs e)
        {
            if (!ValidateData())
                return;
            bool res = false;
            if (CurrentConfigTypeId > 0)
            {
                res = docImportService.UpdateDocImportConfigType(
                    CurrentConfigTypeId,
                    txtImportConfigName.Text,
                    (int)ddlImportConfigPartner.SelectedValue,
                    (int)edImportConfigPageNum.Value - 1,
                    (int)edImportConfigRowNum.Value - 1,
                    (int)edImportConfigSourceNameCol.Value - 1,
                    (int?)edImportConfigEtalonNameCol.Value - 1,
                    (int?)edImportConfigEtalonCodeCol.Value - 1,
                    (int?)edImportConfigMfrCol.Value - 1,
                    (int?)edImportConfigMfrGroupCol.Value - 1,
                    (int?)edImportConfigBarcodeCol.Value - 1
                    );
            }
            else
            {
                res = docImportService.InsertDocImportConfigType(
                    txtImportConfigName.Text,
                    (int)ddlImportConfigPartner.SelectedValue,
                    (int)edImportConfigPageNum.Value - 1,
                    (int)edImportConfigRowNum.Value - 1,
                    (int)edImportConfigSourceNameCol.Value - 1,
                    (int?)edImportConfigEtalonNameCol.Value - 1,
                    (int?)edImportConfigEtalonCodeCol.Value - 1,
                    (int?)edImportConfigMfrCol.Value - 1,
                    (int?)edImportConfigMfrGroupCol.Value - 1,
                    (int?)edImportConfigBarcodeCol.Value - 1
                    );
            }
            if (!res)
                return;
                
            DialogResult = DialogResult.OK;
        }

        private void btnOpenFileCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void radLabel5_ToolTipTextNeeded(object sender, Telerik.WinControls.ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Обязательно к заполнению";
        }

        private bool ValidateData()
        {
            string mess = "Не заполнено:";
            bool result = true;

            if (String.IsNullOrWhiteSpace(txtImportConfigName.Text))
            {
                result = false;
                mess += System.Environment.NewLine;
                mess += " - название кофигурации";
            }

            if (ddlImportConfigPartner.SelectedIndex <= -1)
            {
                result = false;
                mess += System.Environment.NewLine;
                mess += " - источник";
            }

            if (edImportConfigSourceNameCol.Value <= 0)
            {
                result = false;
                mess += System.Environment.NewLine;
                mess += " - колонка с наименованием у источника";
            }

            //edImportConfigSourceNameCol

            if (!result)
            {
                MessageBox.Show(mess);               
            }

            return result;
        }

        private void ImportConfigForm_Load(object sender, EventArgs e)
        {
            if (!ddlImportConfigPartner_inited)
            {
                ddlImportConfigPartner.DataSource = null;
                ddlImportConfigPartner.DataSource = docImportService.GetDocImportSources();
                ddlImportConfigPartner.DisplayMember = "partner_name";
                ddlImportConfigPartner.ValueMember = "partner_id";
                ddlImportConfigPartner_inited = true;
            }
            //
            if (CurrentConfigTypeId > 0)
            {
                var currentConfigType = docImportService.GetDocImportConfigType(CurrentConfigTypeId);
                if (currentConfigType == null)
                {
                    MessageBox.Show("Не найдена конфигурация импорта с кодом " + CurrentConfigTypeId.ToString());
                    return;
                }
                txtImportConfigName.Text = currentConfigType.config_type_name;
                ddlImportConfigPartner.SelectedValue = currentConfigType.source_partner_id;
                edImportConfigPageNum.Value = currentConfigType.list_num + 1;
                edImportConfigRowNum.Value = currentConfigType.row_num + 1;
                edImportConfigSourceNameCol.Value = currentConfigType.source_name_col.GetValueOrDefault(0) + 1;
                edImportConfigEtalonNameCol.Value = currentConfigType.etalon_name_col.GetValueOrDefault(0) + 1;
                edImportConfigEtalonCodeCol.Value = currentConfigType.code_col.GetValueOrDefault(0) + 1;
                edImportConfigMfrCol.Value = currentConfigType.mfr_col.GetValueOrDefault(0) + 1;
                edImportConfigMfrGroupCol.Value = currentConfigType.mfr_group_col.GetValueOrDefault(0) + 1;
                edImportConfigBarcodeCol.Value = currentConfigType.barcode_col.GetValueOrDefault(0) + 1;
            }
        }
    }
}
