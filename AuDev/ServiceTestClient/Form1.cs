﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace ServiceTestClient
{
    public partial class Form1 : Form
    {

        int curr_unit_num = 1;
        List<BonuscardServ.CalcUnit> calcUnitList;

        public Form1()
        {
            InitializeComponent();
            //
            System.Net.ServicePointManager.Expect100Continue = false;
            //
            tabControl1.ItemSize = new Size(0, 1);
            tabControl1.SizeMode = TabSizeMode.Fixed;            
            tabControl2.ItemSize = new Size(0, 1);
            tabControl2.SizeMode = TabSizeMode.Fixed;            
            //
            tabControl1.SelectedIndex = 0;
            tabControl2.SelectedIndex = 0;
            //
        }
        /*
                    <basicHttpBinding>
                      <binding name="BasicHttpBinding_ICabinetService" closeTimeout="00:20:00" openTimeout="00:20:00" receiveTimeout="00:20:00" sendTimeout="00:20:00" maxReceivedMessageSize="2147483647"/>
                      <binding name="BasicHttpBinding_ILoggerService" closeTimeout="00:20:00" openTimeout="00:20:00" receiveTimeout="00:20:00" sendTimeout="00:20:00" maxReceivedMessageSize="2147483647"/>
                      <binding name="BasicHttpBinding_ISenderService" closeTimeout="00:20:00" openTimeout="00:20:00" receiveTimeout="00:20:00" sendTimeout="00:20:00" maxReceivedMessageSize="2147483647"/>
                      <binding name="BasicHttpBinding_IDefectService" closeTimeout="00:20:00" openTimeout="00:20:00" receiveTimeout="00:20:00" sendTimeout="00:20:00" maxReceivedMessageSize="2147483647"/>
                      <binding name="BasicHttpBinding_IBonusCardService" closeTimeout="00:20:00" openTimeout="00:20:00" receiveTimeout="00:20:00" sendTimeout="00:20:00" maxReceivedMessageSize="2147483647"/>
                      <binding name="BasicHttpBinding_IExchangeService" closeTimeout="00:20:00" openTimeout="00:20:00" receiveTimeout="00:20:00" sendTimeout="00:20:00" maxReceivedMessageSize="2147483647"/>
                    </basicHttpBinding>
        */

        public string CurrLogin
        {
            get
            {
                refreshCurrLogin(textBox1);
                return currLogin;
            }
        }
        private string currLogin;

        public string CurrWorkplace
        {
            get
            {
                refreshCurrWorkplace(textBox2);
                return currWorkplace;
            }
        }
        private string currWorkplace;

        public string CurrVersion
        {
            get
            {
                refreshCurrVersion(textBox3);
                return currVersion;
            }
        }
        private string currVersion;

        public int CurrPartnerId
        {
            get
            {
                refreshExchangePartnerId(comboBox1);
                return currPartnerId;
            }
        }
        private int currPartnerId;


        public ExchangeServ.UserInfo exchangeUserInfo
        {
            get { return new ExchangeServ.UserInfo() { login = textBox1.Text, workplace = textBox2.Text, version_num = textBox3.Text, ds_id = CurrPartnerId }; }
        }

        public SenderServ.UserInfo senderUserInfo
        {
            get { return new SenderServ.UserInfo() { login = textBox1.Text, workplace = textBox2.Text, version_num = textBox3.Text,  }; }
        }

        private void GoToTab(int index)
        {
            tabControl1.SelectedIndex = index;
            tabControl2.SelectedIndex = index;
        }

        private void startText(TextBox control, string text)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<TextBox, string>(startText), new object[] { control, text });
                return;
            }

            control.Clear();
            control.Text = text;
        }

        private void addText(TextBox control, string text)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<TextBox, string>(addText), new object[] { control, text });
                return;
            }

            control.Text = control.Text + System.Environment.NewLine;
            control.Text = control.Text + text;
        }

        private void clearCombo(ComboBox control)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<ComboBox>(clearCombo), new object[] { control });
                return;
            }
            control.Items.Clear();
        }

        private void addCombo(ComboBox control, object item)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<ComboBox, object>(addCombo), new object[] { control, item });
                return;
            }
            control.Items.Add(item);
        }

        private void selectCombo(ComboBox control, int index)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<ComboBox, int>(selectCombo), new object[] { control, index });
                return;
            }
            control.SelectedIndex = index;
        }

        private void refreshExchangePartnerId(ComboBox control)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<ComboBox>(refreshExchangePartnerId), new object[] { control });
                return;
            }
            currPartnerId = control.SelectedItem != null ? 1 : 0;            
        }

        private void refreshCurrLogin(TextBox control)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<TextBox>(refreshCurrLogin), new object[] { control });
                return;
            }
            currLogin = control.Text;
        }

        private void refreshCurrWorkplace(TextBox control)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<TextBox>(refreshCurrWorkplace), new object[] { control });
                return;
            }
            currWorkplace = control.Text;
        }

        private void refreshCurrVersion(TextBox control)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<TextBox>(refreshCurrVersion), new object[] { control });
                return;
            }
            currVersion = control.Text;
        }

        private void DisableControls()
        {
            changeControlsEnabledProperty(false);
        }

        private void EnableControls()
        {
            changeControlsEnabledProperty(true);
        }

        private void changeControlsEnabledProperty(bool enabled)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<bool>(changeControlsEnabledProperty), new object[] { enabled });
                return;
            }

            GetSelfAndChildrenRecursive(this).OfType<Button>().ToList()
                  .ForEach(b => b.Enabled = enabled);
        }

        private IEnumerable<Control> GetSelfAndChildrenRecursive(Control parent)
        {
            List<Control> controls = new List<Control>();

            foreach (Control child in parent.Controls)
            {
                controls.AddRange(GetSelfAndChildrenRecursive(child));
            }

            controls.Add(parent);

            return controls;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GoToTab(0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GoToTab(1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            GoToTab(2);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GoToTab(3);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            GoToTab(4);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            GoToTab(5);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            GoToTab(6);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            new Thread(checkAll).Start();
        }

        private void checkAll()
        {
            startText(textBox4, "Дисконтный сервис...");

            DisableControls();
            try
            {
                BonuscardServ.BonusCardServiceClient bonusCardServ = new BonuscardServ.BonusCardServiceClient();
                try
                {
                    try
                    {
                        var res1 = bonusCardServ.GetVersion("session_id");
                        addText(textBox4, "Дисконтный сервис работает !");
                    }
                    catch (Exception ex)
                    {
                        addText(textBox4, "Ошибка: нет связи с дисконтным сервисом");
                        addText(textBox4, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    bonusCardServ = null;
                }

                addText(textBox4, "Обмен с партнерами...");
                ExchangeServ.ExchangeServiceClient exchangeServ = new ExchangeServ.ExchangeServiceClient();
                try
                {
                    try
                    {
                        var res1 = exchangeServ.GetPartnerList();
                        addText(textBox4, "Обмен с партнерами работает !");
                    }
                    catch (Exception ex)
                    {
                        addText(textBox4, "Ошибка: нет связи с сервисом обмена с партнерами");
                        addText(textBox4, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    exchangeServ = null;
                }

                addText(textBox4, "Брак...");
                DefectServ.DefectServiceClient defectServ = new DefectServ.DefectServiceClient();
                try
                {
                    try
                    {
                        var res1 = defectServ.GetDocumentTypeList();
                        addText(textBox4, "Брак работает !");
                    }
                    catch (Exception ex)
                    {
                        addText(textBox4, "Ошибка: нет связи с сервисом брака");
                        addText(textBox4, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    defectServ = null;
                }

                addText(textBox4, "ЦеныВАптеках.РФ...");
                SenderServ.SenderServiceClient senderServ = new SenderServ.SenderServiceClient();
                try
                {
                    try
                    {
                        var res1 = senderServ.GetErrorCodeList();
                        addText(textBox4, "ЦеныВАптеках.РФ работает !");
                    }
                    catch (Exception ex)
                    {
                        addText(textBox4, "Ошибка: нет связи с сервисом ЦеныВАптеках.РФ");
                        addText(textBox4, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    senderServ = null;
                }

                addText(textBox4, "Стукач...");
                LoggerServ.LoggerServiceClient loggerServ = new LoggerServ.LoggerServiceClient();
                try
                {
                    try
                    {
                        var res1 = loggerServ.Log("", null);
                        addText(textBox4, "Стукач работает !");
                    }
                    catch (Exception ex)
                    {
                        addText(textBox4, "Ошибка: нет связи с сервисом стукач");
                        addText(textBox4, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    loggerServ = null;
                }

                addText(textBox4, "Ночные сервисы...");
                CabinetServ.CabinetServiceClient cabinetServ = new CabinetServ.CabinetServiceClient();
                try
                {
                    try
                    {
                        var res1 = cabinetServ.GetLastNotify("");
                        addText(textBox4, "Ночные сервисы работают !");
                    }
                    catch (Exception ex)
                    {
                        addText(textBox4, "Ошибка: нет связи с ночными сервисами");
                        addText(textBox4, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    cabinetServ = null;
                }
            }
            finally
            {
                EnableControls();
            }            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //textBox5
            new Thread(getBusinessList).Start();
        }

        private void getBusinessList()
        {
            startText(textBox5, "Получение списка организаций...");

            DisableControls();
            try
            {
                BonuscardServ.BonusCardServiceClient bonusCardServ = new BonuscardServ.BonusCardServiceClient();
                try
                {
                    try
                    {
                        var res1 = bonusCardServ.GetBusinessList(textBox6.Text);
                        if (res1.error == null)
                        {
                            foreach (var business in res1.Business_list)
                            {
                                addText(textBox5, business.business_name + " [код: " + business.business_id + "]");
                            }
                        }
                        else
                        {
                            addText(textBox5, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox5, "Ошибка:");
                        addText(textBox5, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    bonusCardServ = null;
                }
            }
            finally
            {
                EnableControls();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            new Thread(startSession).Start();
        }

        private void startSession()
        {
            startText(textBox5, "Старт сессии...");
            startText(textBox6, "");
            startText(textBox11, "");
            startText(textBox12, "");

            EncrHelper2 encrHelper = new EncrHelper2();
            DisableControls();
            try
            {
                BonuscardServ.BonusCardServiceClient bonusCardServ = new BonuscardServ.BonusCardServiceClient();
                try
                {
                    try
                    {                                                
                        var res1 = bonusCardServ.StartSession(textBox7.Text, encrHelper._SetHashValue(textBox8.Text), null);
                        //var res1 = bonusCardServ.StartSession(textBox7.Text, encrHelper._SetHashValue(textBox8.Text) + "1", null);
                        if (!String.IsNullOrEmpty(res1))
                        {
                            addText(textBox5, "Сессия создана, код сессии: " + res1);
                            startText(textBox6, res1);
                            var res2 = bonusCardServ.GetSession(res1);
                            if (res2.error == null)
                            {
                                addText(textBox5, "Организация: " + res2.business_name);
                                startText(textBox11, res2.business_id.ToString());
                                startText(textBox12, res2.business_name);
                            }
                            else
                            {
                                addText(textBox5, "[Ошибка " + res2.error.Id.ToString() + "]: " + res2.error.Message);
                            }
                        }
                        else
                        {
                            addText(textBox5, "[Ошибка: сессия не создана]");
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox5, "Ошибка:");
                        addText(textBox5, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    bonusCardServ = null;
                }
            }
            finally
            {
                EnableControls();
                encrHelper = null;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            new Thread(getCard).Start();
        }

        private void getCard()
        {
            startText(textBox5, "Поиск карты...");
                        
            DisableControls();
            try
            {
                BonuscardServ.BonusCardServiceClient bonusCardServ = new BonuscardServ.BonusCardServiceClient();
                try
                {
                    try
                    {
                        long business_id = Convert.ToInt64(textBox11.Text);
                        long card_num = Convert.ToInt64(textBox10.Text);
                        var res1 = bonusCardServ.GetCardList_byCardNum(textBox6.Text, business_id, card_num, textBox10.Text, 5, "");
                        if (res1.error == null)
                        {
                            var card = res1.Card_list.FirstOrDefault();
                            if (card != null)
                            {
                                addText(textBox5, "Карта найдена: " + card.card_num);
                                addText(textBox5, "Сумма на карте: " + card.curr_trans_sum.ToString());
                                addText(textBox5, "Номиналы:");
                                var res2 = bonusCardServ.GetCardNominalList_byCard(textBox6.Text, business_id, card.card_id);
                                if (res2.error == null)
                                {
                                    foreach (var nom in res2.CardNominal_list)
                                    {                                        
                                        addText(textBox5, nom.unit_type.EnumName<Enums.CardUnitTypeEnum>() + ": " + nom.unit_value.ToString() );
                                    }
                                }
                                else
                                {
                                    addText(textBox5, "[Ошибка " + res2.error.Id.ToString() + "]: " + res2.error.Message);
                                }
                            }
                            else
                            {
                                addText(textBox5, "Карта не найдена");
                            }
                        }
                        else
                        {
                            addText(textBox5, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox5, "Ошибка:");
                        addText(textBox5, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    bonusCardServ = null;
                }
            }
            finally
            {
                EnableControls();                
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            new Thread(closeSession).Start();
        }

        private void closeSession()
        {
            startText(textBox5, "Конец сессии...");
            
            DisableControls();
            try
            {
                BonuscardServ.BonusCardServiceClient bonusCardServ = new BonuscardServ.BonusCardServiceClient();
                try
                {
                    try
                    {
                        var res1 = bonusCardServ.CloseSession(textBox6.Text);                        
                        if (res1 == 0)
                        {
                            addText(textBox5, "Сессия закрыта");
                            startText(textBox6, "");
                            startText(textBox11, "");
                            startText(textBox12, "");
                        }
                        else
                        {
                            addText(textBox5, "[Ошибка " + res1.ToString() + "]: сессия не закрыта]");
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox5, "Ошибка:");
                        addText(textBox5, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    bonusCardServ = null;
                }
            }
            finally
            {
                EnableControls();             
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            new Thread(getPartnerList).Start();
        }

        private void getPartnerList()
        {
            startText(textBox9, "Список партнеров...");
            clearCombo(comboBox1);
            
            DisableControls();
            try
            {
                ExchangeServ.ExchangeServiceClient exchangeServ = new ExchangeServ.ExchangeServiceClient();
                try
                {
                    try
                    {
                        var res1 = exchangeServ.GetPartnerList();
                        if (res1.error == null)
                        {
                            foreach (var par in res1.Partner_list)
                            {
                                addText(textBox9, "[" + par.ds_id.ToString() + "] " + par.ds_name);
                                addCombo(comboBox1, par);                                
                            }
                            selectCombo(comboBox1, 0);
                        }
                        else
                        {
                            addText(textBox9, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox9, "Ошибка:");
                        addText(textBox9, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    exchangeServ = null;
                }
            }
            finally
            {
                EnableControls();                
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            new Thread(getParams).Start();
        }

        private void getParams()
        {
            startText(textBox9, "Параметры обмена...");            
            
            DisableControls();
            try
            {
                ExchangeServ.ExchangeServiceClient exchangeServ = new ExchangeServ.ExchangeServiceClient();
                try
                {
                    try
                    {
                        var res1 = exchangeServ.GetParams(exchangeUserInfo);
                        if (res1.error == null)
                        {
                            foreach (var par in res1.ExchangeParams_list)
                            {
                                addText(textBox9, "Партнер: " + par.ds_name);                                
                                addText(textBox9, "Название регистрации в ЛК: " + par.reg_name);
                                addText(textBox9, "Код в системе Партнера: " + par.reg_code);
                                addText(textBox9, "Кол-во дней для выгрузки движений: " + par.days_cnt_for_mov.ToString());
                                addText(textBox9, "Признак 'передавать все данные': " + (par.send_all == 1 ? "да" : "нет"));
                                addText(textBox9, "Файлы: ");
                                foreach (var item in par.exchange_item_list.ExchangeItem_list)
                                {
                                    addText(textBox9, item.item_name);                                    
                                }
                            }                            
                        }
                        else
                        {
                            addText(textBox9, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox9, "Ошибка:");
                        addText(textBox9, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    exchangeServ = null;
                }
            }
            finally
            {
                EnableControls();             
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            new Thread(getExchangeData).Start();
        }

        private void getExchangeData()
        {
            startText(textBox9, "Данные обмена...");
            
            DisableControls();
            try
            {
                ExchangeServ.ExchangeServiceClient exchangeServ = new ExchangeServ.ExchangeServiceClient();
                try
                {
                    try
                    {
                        var res1 = exchangeServ.GetExchangeData(exchangeUserInfo, Convert.ToInt32(textBox13.Text));
                        if (res1.error == null)
                        {                            
                            addText(textBox9, res1.data_binary != null ? GlobalUtil.GetString(res1.data_binary) : "пустой файл");
                        }
                        else
                        {
                            addText(textBox9, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox9, "Ошибка:");
                        addText(textBox9, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    exchangeServ = null;
                }
            }
            finally
            {
                EnableControls();                
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            new Thread(getDefectAll).Start();
        }

        private void getDefectAll()
        {
            startText(textBox14, "Получение списка брака...");
            
            DisableControls();
            try
            {
                DefectServ.DefectServiceClient defectServ = new DefectServ.DefectServiceClient();
                try
                {
                    try
                    {
                        var res1 = defectServ.GetDefectXmlInit_Arc(exchangeUserInfo.login, exchangeUserInfo.workplace, exchangeUserInfo.version_num);
                        if (res1.error == null)
                        {
                            var res_binary = res1.DataBinary;
                            string tmp_path = Path.GetTempPath();
                            string file_path = Path.Combine(tmp_path, "remove.zip");
                            File.WriteAllBytes(file_path, res_binary);
                            addText(textBox14, "Файл сохранен в " + file_path);
                            var res2 = defectServ.CommitRequest(exchangeUserInfo.login, exchangeUserInfo.workplace, exchangeUserInfo.version_num, res1.RequestId);
                            if (res2.error == null)
                            {
                                addText(textBox14, "Скачивание подтверждено");
                            }
                            else
                            {
                                addText(textBox14, "[Ошибка " + res2.error.Id.ToString() + "]: " + res2.error.Message);
                            }
                        }
                        else
                        {
                            addText(textBox14, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox14, "Ошибка:");
                        addText(textBox14, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    defectServ = null;
                }
            }
            finally
            {
                EnableControls();                
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            new Thread(getDefectNew).Start();
        }

        private void getDefectNew()
        {
            startText(textBox14, "Получение списка брака...");
            
            DisableControls();
            try
            {
                DefectServ.DefectServiceClient defectServ = new DefectServ.DefectServiceClient();
                try
                {
                    try
                    {
                        var res1 = defectServ.GetDefectXml(exchangeUserInfo.login, exchangeUserInfo.workplace, exchangeUserInfo.version_num);
                        if (res1.error == null)
                        {
                            addText(textBox14, "Данные получены:");
                            addText(textBox14, res1.Data);
                            var res2 = defectServ.CommitRequest(exchangeUserInfo.login, exchangeUserInfo.workplace, exchangeUserInfo.version_num, res1.RequestId);
                            if (res2.error == null)
                            {
                                addText(textBox14, "Скачивание подтверждено");
                            }
                            else
                            {
                                addText(textBox14, "[Ошибка " + res2.error.Id.ToString() + "]: " + res2.error.Message);
                            }
                        }
                        else
                        {
                            addText(textBox14, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox14, "Ошибка:");
                        addText(textBox14, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    defectServ = null;
                }
            }
            finally
            {
                EnableControls();                
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            new Thread(getRequestList).Start();
        }

        private void getRequestList()
        {
            startText(textBox14, "Получение истории скачивания...");
            
            DisableControls();
            try
            {
                DefectServ.DefectServiceClient defectServ = new DefectServ.DefectServiceClient();
                try
                {
                    try
                    {
                        var res1 = defectServ.GetRequestList(exchangeUserInfo.login, exchangeUserInfo.workplace, exchangeUserInfo.version_num);
                        if (res1.error == null)
                        {
                            if ((res1.Request_list == null) || (res1.Request_list.Count <= 0))
                            {
                                addText(textBox14, "Нет истории");
                            }
                            else
                            {
                                foreach (var req in res1.Request_list.OrderBy(ss => ss.request_date))
                                {                                    
                                    addText(textBox14, "Дата: " + req.request_date.ToString() + " || Статус: " + req.state.EnumName<Enums.RequestStateEnum>() + " || Тип: " + req.state.EnumName<Enums.RequestTypeEnum>());
                                }
                            }
                        }
                        else
                        {
                            addText(textBox14, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox14, "Ошибка:");
                        addText(textBox14, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    defectServ = null;
                }
            }
            finally
            {
                EnableControls();                
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            new Thread(cvaGetSendInfo).Start();
        }

        private void cvaGetSendInfo()
        {
            startText(textBox15, "Получение данных...");
            
            DisableControls();
            try
            {
                SenderServ.SenderServiceClient senderServ = new SenderServ.SenderServiceClient();
                try
                {
                    try
                    {
                        var res1 = senderServ.CVA_GetSendInfo(senderUserInfo);
                        if (res1.error == null)
                        {
                            if (res1.cvaInfo == null)
                            {
                                addText(textBox15, "Нет данных");
                            }
                            else
                            {
                                addText(textBox15, "Интервал отправки: " + res1.cvaInfo.send_interval.ToString());
                                addText(textBox15, "Последняя дата отправки: " + res1.cvaInfo.last_sent_date);
                                addText(textBox15, "Всего пакетов отправлено: " + res1.cvaInfo.batch_sent_count.ToString());                                
                            }
                        }
                        else
                        {
                            addText(textBox15, "[Ошибка " + res1.error.Id.ToString() + "]: " + res1.error.Message);
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox15, "Ошибка:");
                        addText(textBox15, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    senderServ = null;
                }
            }
            finally
            {
                EnableControls();                
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            new Thread(updateSaleSummary).Start();
        }

        private void updateSaleSummary()
        {
            startText(textBox17, "Обновление статистики продаж по картам...");

            DisableControls();
            try
            {
                CabinetServ.CabinetServiceClient cabinetServ = new CabinetServ.CabinetServiceClient();
                try
                {
                    try
                    {
                        var res1 = cabinetServ.UpdateSaleSummary("iguana");
                        if (res1)
                        {
                            addText(textBox17, "Обновление завершено !");
                        }
                        else
                        {
                            addText(textBox17, "[Ошибка]: ошибка при обновлении статистики продаж по картам");
                        }
                    }
                    catch (Exception ex)
                    {
                        addText(textBox17, "Ошибка:");
                        addText(textBox17, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    cabinetServ = null;
                }
            }
            finally
            {
                EnableControls();
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            if (calcUnitList == null)
                calcUnitList = new List<BonuscardServ.CalcUnit>();
            //calcUnitList.Clear();

            BonuscardServ.CalcUnit calcUnit = new BonuscardServ.CalcUnit()
            {
                artikul = 1112,
                group1_property_initialized = 1,
                group2_property_initialized = 1,
                group3_property_initialized = 0,
                live_prep = checkBox1.Checked ? 1 : 0,
                price_margin_percent = numericUpDown6.Value,
                promo = checkBox2.Checked ? 1 : 0,
                tax_percent = numericUpDown7.Value,
                tax_summa = numericUpDown10.Value,
                unit_count = (int)numericUpDown2.Value,
                unit_max_bonus_for_card_percent = numericUpDown5.Value,
                unit_max_bonus_for_pay_percent = numericUpDown4.Value,
                unit_max_discount_percent = numericUpDown3.Value,
                //unit_name = curr_unit_num.ToString(),
                unit_name = String.IsNullOrWhiteSpace(textBox19.Text) ? ("Товар " + curr_unit_num.ToString()) : textBox19.Text.Trim(),
                unit_num = curr_unit_num,
                unit_type = 0,
                unit_value = numericUpDown1.Value,
                unit_price_tax = numericUpDown11.Value,
            };

            calcUnitList.Add(calcUnit);

            dataGridView1.Rows.Add(
                calcUnit.unit_name,
                calcUnit.unit_num.ToString(),
                calcUnit.unit_count.ToString(),
                calcUnit.unit_value.ToString(),
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                calcUnit.unit_max_discount_percent.ToString(),
                calcUnit.unit_max_bonus_for_card_percent,
                calcUnit.unit_max_bonus_for_pay_percent.ToString(),
                calcUnit.price_margin_percent.ToString(),
                calcUnit.tax_percent.ToString(),
                calcUnit.live_prep.ToString(),
                calcUnit.promo.ToString(),
                calcUnit.unit_type.ToString()
                );

            curr_unit_num++;
        }

        private void button23_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            BonuscardServ.BonusCardServiceClient bonusCardServ = new BonuscardServ.BonusCardServiceClient();            
            try
            {
                try
                {
                    var calcStartResult = bonusCardServ.Card_CalcStartTest(textBox6.Text, long.Parse(textBox11.Text), long.Parse(textBox10.Text), calcUnitList, numericUpDown8.Value, (short)numericUpDown9.Value, checkBox3.Checked ? (short)1 : (short)0, (short)0);
                    if (calcStartResult.error == null)
                    {
                        long result_id = calcStartResult.trans_id;
                        var programmResultDetailList = bonusCardServ.GetProgrammResultDetailList(textBox6.Text, result_id);
                        if (programmResultDetailList.error == null)
                        {
                            dataGridView1.Rows.Clear();
                            dataGridView2.Rows.Clear();
                            foreach (var programmResultDetail in programmResultDetailList.ProgrammResultDetail_list.OrderBy(ss => ss.unit_num))
                            {
                                dataGridView1.Rows.Add(
                                    programmResultDetail.unit_name,
                                    programmResultDetail.unit_num.ToString(),
                                    programmResultDetail.unit_count.ToString(),
                                    programmResultDetail.unit_value.ToString(),
                                    programmResultDetail.unit_value_discount.ToString(),
                                    programmResultDetail.unit_value_with_discount.ToString(),
                                    programmResultDetail.unit_percent_discount.ToString(),
                                    programmResultDetail.unit_value_bonus_for_card.ToString(),
                                    programmResultDetail.unit_value_bonus_for_pay.ToString(),
                                    programmResultDetail.unit_percent_bonus_for_card.ToString(),
                                    programmResultDetail.unit_percent_bonus_for_pay.ToString(),
                                    programmResultDetail.unit_max_discount_percent.ToString(),
                                    programmResultDetail.unit_max_bonus_for_card_percent,
                                    programmResultDetail.unit_max_bonus_for_pay_percent.ToString(),
                                    programmResultDetail.price_margin_percent.ToString(),
                                    programmResultDetail.tax_percent.ToString(),
                                    programmResultDetail.live_prep.ToString(),
                                    programmResultDetail.promo.ToString(),
                                    programmResultDetail.unit_type.ToString()
                                    );
                            }
                            var programmStepResultList = bonusCardServ.GetProgrammStepResultList(textBox6.Text, result_id);
                            if (programmStepResultList.error == null)
                            {
                                foreach (var programmStepResult in programmStepResultList.ProgrammStepResult_list.OrderBy(ss => ss.step_num).ThenBy(ss => ss.logic_num).ThenBy(ss => ss.condition_num).ThenBy(ss => ss.position_num))
                                {
                                    dataGridView2.Rows.Add(
                                        programmStepResult.step_num.ToString(),
                                        programmStepResult.position_name,
                                        programmStepResult.position_num,
                                        programmStepResult.condition_num.ToString(),
                                        programmStepResult.condition_name,
                                        programmStepResult.condition_op1_param_value,
                                        programmStepResult.condition_op2_param_value,
                                        programmStepResult.condition_result.ToString(),
                                        programmStepResult.logic_num.ToString(),
                                        programmStepResult.logic_op_type.EnumName<Enums.LogicType>(),
                                        programmStepResult.logic_op1_param_name,
                                        programmStepResult.logic_op1_param_value,
                                        programmStepResult.logic_op1_is_programm_output.ToString(),
                                        programmStepResult.logic_op2_param_name,
                                        programmStepResult.logic_op2_param_value,
                                        programmStepResult.logic_op2_is_programm_output.ToString(),
                                        programmStepResult.logic_op3_param_name,
                                        programmStepResult.logic_op3_param_value,
                                        programmStepResult.logic_op3_is_programm_output.ToString(),
                                        programmStepResult.logic_op4_param_name,
                                        programmStepResult.logic_op4_param_value,
                                        programmStepResult.logic_op4_is_programm_output.ToString()
                                        );
                                }
                            }
                            else
                            {
                                MessageBox.Show(programmStepResultList.error.Message);
                            }
                        }
                        else
                        {
                            MessageBox.Show(programmResultDetailList.error.Message);
                        }
                    }
                    else
                    {
                        MessageBox.Show(calcStartResult.error.Message);
                    }
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            finally
            {
                Cursor = Cursors.Default;
                bonusCardServ = null;
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            curr_unit_num = 1;
            if (calcUnitList == null)
                calcUnitList = new List<BonuscardServ.CalcUnit>();
            calcUnitList.Clear();
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
        }

        private void button25_Click(object sender, EventArgs e)
        {
            new Thread(getCardBonusSum).Start();
        }

        private void getCardBonusSum()
        {
            startText(textBox5, "Поиск бонусов...");

            DisableControls();
            try
            {
                BonuscardServ.BonusCardServiceClient bonusCardServ = new BonuscardServ.BonusCardServiceClient();
                try
                {
                    try
                    {
                        long business_id = Convert.ToInt64(textBox11.Text);
                        long card_num = Convert.ToInt64(textBox10.Text);
                        var res1 = bonusCardServ.GetCardValueForPay(textBox6.Text, business_id, card_num);
                        addText(textBox5, "Бонусов на карте: " + res1.ToString());
                    }
                    catch (Exception ex)
                    {
                        addText(textBox5, "Ошибка:");
                        addText(textBox5, GlobalUtil.ExceptionInfo(ex));
                    }
                }
                finally
                {
                    bonusCardServ = null;
                }
            }
            finally
            {
                EnableControls();
            }
        }
        
    }
}
