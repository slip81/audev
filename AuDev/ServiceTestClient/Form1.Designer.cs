﻿namespace ServiceTestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button9 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.button25 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button14 = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button20 = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.button21 = new System.Windows.Forms.Button();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.unit_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_value_discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_value_with_discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_percent_discount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_value_bonus_for_card = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_value_bonus_for_pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_percent_bonus_for_card = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_percent_bonus_for_pay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_max_discount_percent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_max_bonus_for_card_percent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_max_bonus_for_pay_percent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price_margin_percent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tax_percent = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.live_prep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.promo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unit_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.step_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.position_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.position_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condition_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condition_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condition_op1_param_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condition_op2_param_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condition_result = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_num = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.condition_op_type = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op1_param_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op1_param_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op1_is_programm_output = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op2_param_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op2_param_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op2_is_programm_output = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op3_param_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op3_param_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op3_is_programm_output = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op4_param_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op4_param_value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.logic_op4_is_programm_output = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.numericUpDown10 = new System.Windows.Forms.NumericUpDown();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.numericUpDown9 = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.button22 = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.numericUpDown11 = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.tabPage16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.textBox3);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.textBox2);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.textBox1);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1072, 816);
            this.splitContainer1.SplitterDistance = 93;
            this.splitContainer1.TabIndex = 0;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(812, 41);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(171, 20);
            this.textBox3.TabIndex = 5;
            this.textBox3.Text = "3.2.0.0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(705, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Версия модуля:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(491, 41);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(171, 20);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "F634-7A54-9EAB-E958";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(287, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Идентификатор рабочего места:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(83, 41);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(171, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "test3104";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(30, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Логин:";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer3);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.button8);
            this.splitContainer2.Size = new System.Drawing.Size(1072, 719);
            this.splitContainer2.SplitterDistance = 599;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.button7);
            this.splitContainer3.Panel1.Controls.Add(this.button6);
            this.splitContainer3.Panel1.Controls.Add(this.button5);
            this.splitContainer3.Panel1.Controls.Add(this.button4);
            this.splitContainer3.Panel1.Controls.Add(this.button3);
            this.splitContainer3.Panel1.Controls.Add(this.button2);
            this.splitContainer3.Panel1.Controls.Add(this.button1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer3.Size = new System.Drawing.Size(1072, 599);
            this.splitContainer3.SplitterDistance = 158;
            this.splitContainer3.TabIndex = 0;
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Top;
            this.button7.Location = new System.Drawing.Point(0, 231);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(158, 37);
            this.button7.TabIndex = 6;
            this.button7.Text = "Ночные сервисы";
            this.button7.UseCompatibleTextRendering = true;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Dock = System.Windows.Forms.DockStyle.Top;
            this.button6.Location = new System.Drawing.Point(0, 194);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(158, 37);
            this.button6.TabIndex = 5;
            this.button6.Text = "Стукач";
            this.button6.UseCompatibleTextRendering = true;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Location = new System.Drawing.Point(0, 157);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(158, 37);
            this.button5.TabIndex = 4;
            this.button5.Text = "ЦВА";
            this.button5.UseCompatibleTextRendering = true;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.Location = new System.Drawing.Point(0, 120);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(158, 37);
            this.button4.TabIndex = 3;
            this.button4.Text = "Брак";
            this.button4.UseCompatibleTextRendering = true;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.Location = new System.Drawing.Point(0, 80);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(158, 40);
            this.button3.TabIndex = 2;
            this.button3.Text = "Обмен с партнерами";
            this.button3.UseCompatibleTextRendering = true;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.Location = new System.Drawing.Point(0, 40);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(158, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "Дисконтный сервис";
            this.button2.UseCompatibleTextRendering = true;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(158, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "Основное";
            this.button1.UseCompatibleTextRendering = true;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.tabControl2);
            this.splitContainer4.Size = new System.Drawing.Size(910, 599);
            this.splitContainer4.SplitterDistance = 167;
            this.splitContainer4.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(910, 167);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(902, 141);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Основное";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(3, 3);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(107, 23);
            this.button9.TabIndex = 1;
            this.button9.Text = "Проверка связи";
            this.button9.UseCompatibleTextRendering = true;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox18);
            this.tabPage2.Controls.Add(this.button25);
            this.tabPage2.Controls.Add(this.button13);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.textBox12);
            this.tabPage2.Controls.Add(this.textBox11);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.textBox10);
            this.tabPage2.Controls.Add(this.button12);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.textBox8);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.textBox7);
            this.tabPage2.Controls.Add(this.textBox6);
            this.tabPage2.Controls.Add(this.button11);
            this.tabPage2.Controls.Add(this.button10);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(902, 141);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Дисконты";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(646, 60);
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(144, 20);
            this.textBox18.TabIndex = 21;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(494, 56);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(145, 23);
            this.button25.TabIndex = 20;
            this.button25.Text = "4. Сколько бонусов есть";
            this.button25.UseCompatibleTextRendering = true;
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(6, 105);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(145, 23);
            this.button13.TabIndex = 19;
            this.button13.Text = "5. Конец сессии";
            this.button13.UseCompatibleTextRendering = true;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(658, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(123, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "название организации";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(522, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "код организации";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(645, 8);
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(145, 20);
            this.textBox12.TabIndex = 16;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(494, 8);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(145, 20);
            this.textBox11.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(370, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "номер карты";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(343, 58);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(145, 20);
            this.textBox10.TabIndex = 13;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(163, 56);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(170, 23);
            this.button12.TabIndex = 10;
            this.button12.Text = "3. Найти карту организации";
            this.button12.UseCompatibleTextRendering = true;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(382, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "код сессии";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(278, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "пароль";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(251, 9);
            this.textBox8.Name = "textBox8";
            this.textBox8.PasswordChar = '*';
            this.textBox8.Size = new System.Drawing.Size(82, 20);
            this.textBox8.TabIndex = 7;
            this.textBox8.Text = "j8afKj";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(190, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "логин";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(163, 9);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(82, 20);
            this.textBox7.TabIndex = 5;
            this.textBox7.Text = "m00001";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(343, 9);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(145, 20);
            this.textBox6.TabIndex = 4;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(6, 6);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(145, 23);
            this.button11.TabIndex = 3;
            this.button11.Text = "1. Старт сессии";
            this.button11.UseCompatibleTextRendering = true;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(6, 57);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(145, 23);
            this.button10.TabIndex = 2;
            this.button10.Text = "2. Список организаций";
            this.button10.UseCompatibleTextRendering = true;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.textBox13);
            this.tabPage3.Controls.Add(this.button16);
            this.tabPage3.Controls.Add(this.button15);
            this.tabPage3.Controls.Add(this.comboBox1);
            this.tabPage3.Controls.Add(this.button14);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(902, 141);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Обмен с партнерами";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(179, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "код данных в ЛК";
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(157, 90);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(150, 20);
            this.textBox13.TabIndex = 8;
            this.textBox13.Text = "0";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(6, 88);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(145, 23);
            this.button16.TabIndex = 7;
            this.button16.Text = "3. Просмотр данных";
            this.button16.UseCompatibleTextRendering = true;
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(6, 48);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(145, 23);
            this.button15.TabIndex = 6;
            this.button15.Text = "2. Параметры обмена";
            this.button15.UseCompatibleTextRendering = true;
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "ds_name";
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(157, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(150, 21);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.ValueMember = "ds_id";
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(6, 6);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(145, 23);
            this.button14.TabIndex = 4;
            this.button14.Text = "1. Список партнеров";
            this.button14.UseCompatibleTextRendering = true;
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button19);
            this.tabPage4.Controls.Add(this.button18);
            this.tabPage4.Controls.Add(this.button17);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(902, 141);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Брак";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(18, 98);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(191, 23);
            this.button19.TabIndex = 7;
            this.button19.Text = "3. История скачиваний";
            this.button19.UseCompatibleTextRendering = true;
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(18, 58);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(191, 23);
            this.button18.TabIndex = 6;
            this.button18.Text = "2. Скачать брак (только новое)";
            this.button18.UseCompatibleTextRendering = true;
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(18, 18);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(191, 23);
            this.button17.TabIndex = 5;
            this.button17.Text = "1. Скачать брак (весь список)";
            this.button17.UseCompatibleTextRendering = true;
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button20);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(902, 141);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "ЦВА";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(18, 18);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(191, 23);
            this.button20.TabIndex = 6;
            this.button20.Text = "1. Получить данные";
            this.button20.UseCompatibleTextRendering = true;
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(902, 141);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Стукач";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.button21);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(902, 141);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Ночные сервисы";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(15, 27);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(191, 23);
            this.button21.TabIndex = 7;
            this.button21.Text = "1. Обновить статистику продаж";
            this.button21.UseCompatibleTextRendering = true;
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Controls.Add(this.tabPage9);
            this.tabControl2.Controls.Add(this.tabPage10);
            this.tabControl2.Controls.Add(this.tabPage11);
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Controls.Add(this.tabPage13);
            this.tabControl2.Controls.Add(this.tabPage14);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(910, 428);
            this.tabControl2.TabIndex = 1;
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.textBox4);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(902, 402);
            this.tabPage8.TabIndex = 0;
            this.tabPage8.Text = "Основное";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox4.Location = new System.Drawing.Point(3, 3);
            this.textBox4.Multiline = true;
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox4.Size = new System.Drawing.Size(896, 396);
            this.textBox4.TabIndex = 0;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.tabControl3);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(902, 402);
            this.tabPage9.TabIndex = 1;
            this.tabPage9.Text = "Дисконты";
            this.tabPage9.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage15);
            this.tabControl3.Controls.Add(this.tabPage16);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(3, 3);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(896, 396);
            this.tabControl3.TabIndex = 2;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.textBox5);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(888, 370);
            this.tabPage15.TabIndex = 0;
            this.tabPage15.Text = "Сообщения";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // textBox5
            // 
            this.textBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox5.Location = new System.Drawing.Point(3, 3);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox5.Size = new System.Drawing.Size(882, 364);
            this.textBox5.TabIndex = 1;
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.splitContainer5);
            this.tabPage16.Controls.Add(this.panel1);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(888, 370);
            this.tabPage16.TabIndex = 1;
            this.tabPage16.Text = "Тестовая продажа";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(3, 125);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.dataGridView2);
            this.splitContainer5.Size = new System.Drawing.Size(882, 242);
            this.splitContainer5.SplitterDistance = 114;
            this.splitContainer5.TabIndex = 2;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.unit_name,
            this.unit_num,
            this.unit_count,
            this.unit_value,
            this.unit_value_discount,
            this.unit_value_with_discount,
            this.unit_percent_discount,
            this.unit_value_bonus_for_card,
            this.unit_value_bonus_for_pay,
            this.unit_percent_bonus_for_card,
            this.unit_percent_bonus_for_pay,
            this.unit_max_discount_percent,
            this.unit_max_bonus_for_card_percent,
            this.unit_max_bonus_for_pay_percent,
            this.price_margin_percent,
            this.tax_percent,
            this.live_prep,
            this.promo,
            this.unit_type});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(882, 114);
            this.dataGridView1.TabIndex = 0;
            // 
            // unit_name
            // 
            this.unit_name.HeaderText = "Товар";
            this.unit_name.Name = "unit_name";
            this.unit_name.ReadOnly = true;
            // 
            // unit_num
            // 
            this.unit_num.HeaderText = "№ товара";
            this.unit_num.Name = "unit_num";
            this.unit_num.ReadOnly = true;
            // 
            // unit_count
            // 
            this.unit_count.HeaderText = "Кол-во товара";
            this.unit_count.Name = "unit_count";
            this.unit_count.ReadOnly = true;
            // 
            // unit_value
            // 
            this.unit_value.HeaderText = "Цена товара";
            this.unit_value.Name = "unit_value";
            this.unit_value.ReadOnly = true;
            // 
            // unit_value_discount
            // 
            this.unit_value_discount.HeaderText = "Сумма скидки";
            this.unit_value_discount.Name = "unit_value_discount";
            this.unit_value_discount.ReadOnly = true;
            // 
            // unit_value_with_discount
            // 
            this.unit_value_with_discount.HeaderText = "Сумма с учетом скидки";
            this.unit_value_with_discount.Name = "unit_value_with_discount";
            this.unit_value_with_discount.ReadOnly = true;
            // 
            // unit_percent_discount
            // 
            this.unit_percent_discount.HeaderText = "% скидки";
            this.unit_percent_discount.Name = "unit_percent_discount";
            this.unit_percent_discount.ReadOnly = true;
            // 
            // unit_value_bonus_for_card
            // 
            this.unit_value_bonus_for_card.HeaderText = "Бонусов на карту";
            this.unit_value_bonus_for_card.Name = "unit_value_bonus_for_card";
            this.unit_value_bonus_for_card.ReadOnly = true;
            // 
            // unit_value_bonus_for_pay
            // 
            this.unit_value_bonus_for_pay.HeaderText = "Бонусов к оплате";
            this.unit_value_bonus_for_pay.Name = "unit_value_bonus_for_pay";
            this.unit_value_bonus_for_pay.ReadOnly = true;
            // 
            // unit_percent_bonus_for_card
            // 
            this.unit_percent_bonus_for_card.HeaderText = "% бонусов на карту";
            this.unit_percent_bonus_for_card.Name = "unit_percent_bonus_for_card";
            this.unit_percent_bonus_for_card.ReadOnly = true;
            // 
            // unit_percent_bonus_for_pay
            // 
            this.unit_percent_bonus_for_pay.HeaderText = "% бонусов к оплате";
            this.unit_percent_bonus_for_pay.Name = "unit_percent_bonus_for_pay";
            this.unit_percent_bonus_for_pay.ReadOnly = true;
            // 
            // unit_max_discount_percent
            // 
            this.unit_max_discount_percent.HeaderText = "Макс. % скидки";
            this.unit_max_discount_percent.Name = "unit_max_discount_percent";
            this.unit_max_discount_percent.ReadOnly = true;
            // 
            // unit_max_bonus_for_card_percent
            // 
            this.unit_max_bonus_for_card_percent.HeaderText = "Макс. % бонусов на крту";
            this.unit_max_bonus_for_card_percent.Name = "unit_max_bonus_for_card_percent";
            this.unit_max_bonus_for_card_percent.ReadOnly = true;
            // 
            // unit_max_bonus_for_pay_percent
            // 
            this.unit_max_bonus_for_pay_percent.HeaderText = "Макс.% бонусов к оплате";
            this.unit_max_bonus_for_pay_percent.Name = "unit_max_bonus_for_pay_percent";
            this.unit_max_bonus_for_pay_percent.ReadOnly = true;
            // 
            // price_margin_percent
            // 
            this.price_margin_percent.HeaderText = "% розн. наценки";
            this.price_margin_percent.Name = "price_margin_percent";
            this.price_margin_percent.ReadOnly = true;
            // 
            // tax_percent
            // 
            this.tax_percent.HeaderText = "% НДС";
            this.tax_percent.Name = "tax_percent";
            this.tax_percent.ReadOnly = true;
            // 
            // live_prep
            // 
            this.live_prep.HeaderText = "ЖВ";
            this.live_prep.Name = "live_prep";
            this.live_prep.ReadOnly = true;
            // 
            // promo
            // 
            this.promo.HeaderText = "Промо";
            this.promo.Name = "promo";
            this.promo.ReadOnly = true;
            // 
            // unit_type
            // 
            this.unit_type.HeaderText = "0-товар,1-итого по продаже";
            this.unit_type.Name = "unit_type";
            this.unit_type.ReadOnly = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.step_num,
            this.position_name,
            this.position_num,
            this.condition_num,
            this.condition_name,
            this.condition_op1_param_value,
            this.condition_op2_param_value,
            this.condition_result,
            this.logic_num,
            this.condition_op_type,
            this.logic_op1_param_name,
            this.logic_op1_param_value,
            this.logic_op1_is_programm_output,
            this.logic_op2_param_name,
            this.logic_op2_param_value,
            this.logic_op2_is_programm_output,
            this.logic_op3_param_name,
            this.logic_op3_param_value,
            this.logic_op3_is_programm_output,
            this.logic_op4_param_name,
            this.logic_op4_param_value,
            this.logic_op4_is_programm_output});
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(882, 124);
            this.dataGridView2.TabIndex = 1;
            // 
            // step_num
            // 
            this.step_num.HeaderText = "Шаг";
            this.step_num.Name = "step_num";
            this.step_num.ReadOnly = true;
            this.step_num.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // position_name
            // 
            this.position_name.HeaderText = "Товар";
            this.position_name.Name = "position_name";
            this.position_name.ReadOnly = true;
            this.position_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // position_num
            // 
            this.position_num.HeaderText = "Номер товара";
            this.position_num.Name = "position_num";
            this.position_num.ReadOnly = true;
            this.position_num.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // condition_num
            // 
            this.condition_num.HeaderText = "№ условия";
            this.condition_num.Name = "condition_num";
            this.condition_num.ReadOnly = true;
            this.condition_num.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // condition_name
            // 
            this.condition_name.HeaderText = "Наименование условия";
            this.condition_name.Name = "condition_name";
            this.condition_name.ReadOnly = true;
            this.condition_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // condition_op1_param_value
            // 
            this.condition_op1_param_value.HeaderText = "Пар1 условия";
            this.condition_op1_param_value.Name = "condition_op1_param_value";
            this.condition_op1_param_value.ReadOnly = true;
            this.condition_op1_param_value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // condition_op2_param_value
            // 
            this.condition_op2_param_value.HeaderText = "Пар2 условия";
            this.condition_op2_param_value.Name = "condition_op2_param_value";
            this.condition_op2_param_value.ReadOnly = true;
            this.condition_op2_param_value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // condition_result
            // 
            this.condition_result.HeaderText = "Результат условия";
            this.condition_result.Name = "condition_result";
            this.condition_result.ReadOnly = true;
            this.condition_result.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_num
            // 
            this.logic_num.HeaderText = "№ операции";
            this.logic_num.Name = "logic_num";
            this.logic_num.ReadOnly = true;
            this.logic_num.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // condition_op_type
            // 
            this.condition_op_type.HeaderText = "Тип операции";
            this.condition_op_type.Name = "condition_op_type";
            this.condition_op_type.ReadOnly = true;
            this.condition_op_type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op1_param_name
            // 
            this.logic_op1_param_name.HeaderText = "Пар1: наименование";
            this.logic_op1_param_name.Name = "logic_op1_param_name";
            this.logic_op1_param_name.ReadOnly = true;
            this.logic_op1_param_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op1_param_value
            // 
            this.logic_op1_param_value.HeaderText = "Пар1: значение";
            this.logic_op1_param_value.Name = "logic_op1_param_value";
            this.logic_op1_param_value.ReadOnly = true;
            this.logic_op1_param_value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op1_is_programm_output
            // 
            this.logic_op1_is_programm_output.HeaderText = "Пар1: выход. для алгоритма";
            this.logic_op1_is_programm_output.Name = "logic_op1_is_programm_output";
            this.logic_op1_is_programm_output.ReadOnly = true;
            this.logic_op1_is_programm_output.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op2_param_name
            // 
            this.logic_op2_param_name.HeaderText = "Пар2: наименование";
            this.logic_op2_param_name.Name = "logic_op2_param_name";
            this.logic_op2_param_name.ReadOnly = true;
            this.logic_op2_param_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op2_param_value
            // 
            this.logic_op2_param_value.HeaderText = "Пар2: значение";
            this.logic_op2_param_value.Name = "logic_op2_param_value";
            this.logic_op2_param_value.ReadOnly = true;
            this.logic_op2_param_value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op2_is_programm_output
            // 
            this.logic_op2_is_programm_output.HeaderText = "Пар2: выход. для алгоритма";
            this.logic_op2_is_programm_output.Name = "logic_op2_is_programm_output";
            this.logic_op2_is_programm_output.ReadOnly = true;
            this.logic_op2_is_programm_output.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op3_param_name
            // 
            this.logic_op3_param_name.HeaderText = "Пар3: наименование";
            this.logic_op3_param_name.Name = "logic_op3_param_name";
            this.logic_op3_param_name.ReadOnly = true;
            this.logic_op3_param_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op3_param_value
            // 
            this.logic_op3_param_value.HeaderText = "Пар3: значение";
            this.logic_op3_param_value.Name = "logic_op3_param_value";
            this.logic_op3_param_value.ReadOnly = true;
            this.logic_op3_param_value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op3_is_programm_output
            // 
            this.logic_op3_is_programm_output.HeaderText = "Пар3: выход. для алгоритма";
            this.logic_op3_is_programm_output.Name = "logic_op3_is_programm_output";
            this.logic_op3_is_programm_output.ReadOnly = true;
            this.logic_op3_is_programm_output.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op4_param_name
            // 
            this.logic_op4_param_name.HeaderText = "Пар4: наименование";
            this.logic_op4_param_name.Name = "logic_op4_param_name";
            this.logic_op4_param_name.ReadOnly = true;
            this.logic_op4_param_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op4_param_value
            // 
            this.logic_op4_param_value.HeaderText = "Пар4: значение";
            this.logic_op4_param_value.Name = "logic_op4_param_value";
            this.logic_op4_param_value.ReadOnly = true;
            this.logic_op4_param_value.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // logic_op4_is_programm_output
            // 
            this.logic_op4_is_programm_output.HeaderText = "Пар4: выход. для алгоритма";
            this.logic_op4_is_programm_output.Name = "logic_op4_is_programm_output";
            this.logic_op4_is_programm_output.ReadOnly = true;
            this.logic_op4_is_programm_output.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.numericUpDown11);
            this.panel1.Controls.Add(this.textBox19);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.numericUpDown10);
            this.panel1.Controls.Add(this.button24);
            this.panel1.Controls.Add(this.button23);
            this.panel1.Controls.Add(this.checkBox3);
            this.panel1.Controls.Add(this.numericUpDown9);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.numericUpDown8);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.button22);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.numericUpDown7);
            this.panel1.Controls.Add(this.numericUpDown6);
            this.panel1.Controls.Add(this.numericUpDown5);
            this.panel1.Controls.Add(this.numericUpDown4);
            this.panel1.Controls.Add(this.numericUpDown3);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.numericUpDown2);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.numericUpDown1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(882, 122);
            this.panel1.TabIndex = 1;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(3, 49);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(290, 20);
            this.textBox19.TabIndex = 26;
            this.textBox19.Text = "название товара";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(667, 10);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(42, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "Маржа";
            // 
            // numericUpDown10
            // 
            this.numericUpDown10.DecimalPlaces = 2;
            this.numericUpDown10.Location = new System.Drawing.Point(670, 26);
            this.numericUpDown10.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown10.Name = "numericUpDown10";
            this.numericUpDown10.Size = new System.Drawing.Size(59, 20);
            this.numericUpDown10.TabIndex = 24;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(735, 96);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(132, 23);
            this.button24.TabIndex = 23;
            this.button24.Text = "Очистить";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(735, 74);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(132, 23);
            this.button23.TabIndex = 22;
            this.button23.Text = "Расчет";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Location = new System.Drawing.Point(515, 76);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(111, 17);
            this.checkBox3.TabIndex = 21;
            this.checkBox3.Text = "тестовый режим";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // numericUpDown9
            // 
            this.numericUpDown9.Location = new System.Drawing.Point(394, 74);
            this.numericUpDown9.Name = "numericUpDown9";
            this.numericUpDown9.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown9.TabIndex = 20;
            this.numericUpDown9.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(258, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(137, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "Карта выбрана сканером";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 76);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(138, 13);
            this.label18.TabIndex = 18;
            this.label18.Text = "Сколько бонусов списать";
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.DecimalPlaces = 2;
            this.numericUpDown8.Location = new System.Drawing.Point(147, 74);
            this.numericUpDown8.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown8.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown8.TabIndex = 17;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(592, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 13);
            this.label17.TabIndex = 16;
            this.label17.Text = "% НДС";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(489, 10);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 13);
            this.label16.TabIndex = 15;
            this.label16.Text = "% розн. наценки";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(398, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(165, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Макс. % бонусов к зачислению";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(300, 9);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(139, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Макс. % бонусов к оплате";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(198, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Макс. % скидки";
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(735, 53);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(132, 23);
            this.button22.TabIndex = 11;
            this.button22.Text = "Добавить товар";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(636, 82);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(100, 17);
            this.checkBox2.TabIndex = 10;
            this.checkBox2.Text = "в промо-акции";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(636, 59);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(68, 17);
            this.checkBox1.TabIndex = 9;
            this.checkBox1.Text = "ЖНВЛП";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.DecimalPlaces = 2;
            this.numericUpDown7.Location = new System.Drawing.Point(595, 26);
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(59, 20);
            this.numericUpDown7.TabIndex = 8;
            this.numericUpDown7.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.DecimalPlaces = 2;
            this.numericUpDown6.Location = new System.Drawing.Point(502, 26);
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(77, 20);
            this.numericUpDown6.TabIndex = 7;
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.DecimalPlaces = 2;
            this.numericUpDown5.Location = new System.Drawing.Point(401, 26);
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown5.TabIndex = 6;
            this.numericUpDown5.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.DecimalPlaces = 2;
            this.numericUpDown4.Location = new System.Drawing.Point(303, 26);
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown4.TabIndex = 5;
            this.numericUpDown4.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.DecimalPlaces = 2;
            this.numericUpDown3.Location = new System.Drawing.Point(201, 26);
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown3.TabIndex = 4;
            this.numericUpDown3.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(98, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Кол-во";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(101, 26);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown2.TabIndex = 2;
            this.numericUpDown2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(33, 13);
            this.label11.TabIndex = 1;
            this.label11.Text = "Цена";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 2;
            this.numericUpDown1.Location = new System.Drawing.Point(3, 26);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(92, 20);
            this.numericUpDown1.TabIndex = 0;
            this.numericUpDown1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.textBox9);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(902, 402);
            this.tabPage10.TabIndex = 2;
            this.tabPage10.Text = "Обмен с партнерами";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // textBox9
            // 
            this.textBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox9.Location = new System.Drawing.Point(3, 3);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox9.Size = new System.Drawing.Size(896, 396);
            this.textBox9.TabIndex = 2;
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.textBox14);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage11.Size = new System.Drawing.Size(902, 402);
            this.tabPage11.TabIndex = 3;
            this.tabPage11.Text = "Брак";
            this.tabPage11.UseVisualStyleBackColor = true;
            // 
            // textBox14
            // 
            this.textBox14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox14.Location = new System.Drawing.Point(3, 3);
            this.textBox14.Multiline = true;
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox14.Size = new System.Drawing.Size(896, 396);
            this.textBox14.TabIndex = 3;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.textBox15);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(902, 402);
            this.tabPage12.TabIndex = 4;
            this.tabPage12.Text = "ЦВА";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // textBox15
            // 
            this.textBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox15.Location = new System.Drawing.Point(3, 3);
            this.textBox15.Multiline = true;
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox15.Size = new System.Drawing.Size(896, 396);
            this.textBox15.TabIndex = 4;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.textBox16);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(902, 402);
            this.tabPage13.TabIndex = 5;
            this.tabPage13.Text = "Стукач";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // textBox16
            // 
            this.textBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox16.Location = new System.Drawing.Point(3, 3);
            this.textBox16.Multiline = true;
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox16.Size = new System.Drawing.Size(896, 396);
            this.textBox16.TabIndex = 5;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.textBox17);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage14.Size = new System.Drawing.Size(902, 402);
            this.tabPage14.TabIndex = 6;
            this.tabPage14.Text = "Ночные сервисы";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // textBox17
            // 
            this.textBox17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox17.Location = new System.Drawing.Point(3, 3);
            this.textBox17.Multiline = true;
            this.textBox17.Name = "textBox17";
            this.textBox17.ReadOnly = true;
            this.textBox17.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox17.Size = new System.Drawing.Size(896, 396);
            this.textBox17.TabIndex = 6;
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Location = new System.Drawing.Point(911, 76);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(158, 37);
            this.button8.TabIndex = 7;
            this.button8.Text = "Выход";
            this.button8.UseCompatibleTextRendering = true;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // numericUpDown11
            // 
            this.numericUpDown11.DecimalPlaces = 2;
            this.numericUpDown11.Location = new System.Drawing.Point(748, 26);
            this.numericUpDown11.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown11.Name = "numericUpDown11";
            this.numericUpDown11.Size = new System.Drawing.Size(59, 20);
            this.numericUpDown11.TabIndex = 27;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(745, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(92, 13);
            this.label21.TabIndex = 28;
            this.label21.Text = "Цена за единицу";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1072, 816);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "Тест веб-сервисов";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabPage9.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.tabPage16.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.tabPage13.ResumeLayout(false);
            this.tabPage13.PerformLayout();
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.NumericUpDown numericUpDown9;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_count;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_value_discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_value_with_discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_percent_discount;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_value_bonus_for_card;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_value_bonus_for_pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_percent_bonus_for_card;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_percent_bonus_for_pay;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_max_discount_percent;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_max_bonus_for_card_percent;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_max_bonus_for_pay_percent;
        private System.Windows.Forms.DataGridViewTextBoxColumn price_margin_percent;
        private System.Windows.Forms.DataGridViewTextBoxColumn tax_percent;
        private System.Windows.Forms.DataGridViewTextBoxColumn live_prep;
        private System.Windows.Forms.DataGridViewTextBoxColumn promo;
        private System.Windows.Forms.DataGridViewTextBoxColumn unit_type;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.DataGridViewTextBoxColumn step_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn position_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn position_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn condition_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn condition_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn condition_op1_param_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn condition_op2_param_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn condition_result;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_num;
        private System.Windows.Forms.DataGridViewTextBoxColumn condition_op_type;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op1_param_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op1_param_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op1_is_programm_output;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op2_param_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op2_param_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op2_is_programm_output;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op3_param_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op3_param_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op3_is_programm_output;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op4_param_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op4_param_value;
        private System.Windows.Forms.DataGridViewTextBoxColumn logic_op4_is_programm_output;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown numericUpDown10;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown numericUpDown11;
    }
}

