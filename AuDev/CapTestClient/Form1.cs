﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapTestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {                        
            Cursor = Cursors.WaitCursor;
            CapServ.CapServiceClient serv = new CapServ.CapServiceClient();
            try
            {
                int client_id = -1;
                int sales_id = -1;
                //var res = serv.Login2("test3104", "EA5B-678A-9798-E6CB", 34, "3.2.0.0", out client_id, out sales_id);
                var res = serv.Login2("test_3215", "EA5B-678A-9798-E6CB", 34, "3.2.0.0", out client_id, out sales_id);
                MessageBox.Show("res = " + res.ToString() + ", client_id = " + client_id.ToString() + ", sales_id = " + sales_id.ToString());
            }
            finally
            {
                Cursor = Cursors.Default;
            }            
         
        }



        /*
        POST http://lic.aptekaural.ru:6789/CapService.svc/plain HTTP/1.1
        SOAPAction: "http://tempuri.org/ICapService/Login2"
        Content-Type: text/xml; charset="utf-8"
        User-Agent: Borland SOAP 1.2
        Host: lic.aptekaural.ru:6789
        Content-Length: 427
        Connection: Keep-Alive
        Pragma: no-cache

        <?xml version="1.0"?>
        <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><Login2 xmlns="http://tempuri.org/"><login>bai_test_17</login><hardwareId>EA5B-678A-9798-E6CB</hardwareId><applicationId>34</applicationId><version>3.0.1.0</version></Login2></SOAP-ENV:Body></SOAP-ENV:Envelope>

        */
    }
}
