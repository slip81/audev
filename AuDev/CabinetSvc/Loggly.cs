﻿using System;
using System.Text;
using System.Net;
using UnidecodeSharpFork;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc
{
    public static class Loggly
    {
        private static bool isBanLog = false;

        public static bool InsertLog_Discount(string mess, long? type_id, long? business_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            // отсылаем в БД
            using (var dbContext = new AuMainDb())
            {
                log_event log_event = new log_event();
                log_event.date_beg = date_beg;
                log_event.mess = mess;
                log_event.log_event_type_id = type_id;
                log_event.business_id = business_id;
                log_event.user_name = user_name;
                log_event.obj_id = obj_id;
                log_event.scope = (int)Enums.LogScope.DISCOUNT;
                log_event.date_end = DateTime.Now;
                log_event.mess2 = mess2;
                dbContext.log_event.Add(log_event);
                dbContext.SaveChanges();
            }

            return true;
        }

        public static bool InsertLog_ExchangeDoc(string mess, long? type_id, long? business_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            // отсылаем в БД
            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess;
                log_esn.log_esn_type_id = type_id;
                log_esn.user_name = user_name;
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.EXCHANGE_DOC;
                log_esn.date_end = DateTime.Now;
                log_esn.mess2 = mess2;
                dbContext.log_esn.Add(log_esn);
                dbContext.SaveChanges();
            }

            return true;
        }


        public static bool InsertLog_License(string mess, long? type_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess;
                log_esn.log_esn_type_id = type_id;
                log_esn.user_name = user_name;
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.LICENSE;
                log_esn.date_end = DateTime.Now;
                log_esn.mess2 = mess2;
                dbContext.log_esn.Add(log_esn);
                dbContext.SaveChanges();
            }

            return true;
        }

        public static bool InsertLog_Cabinet(string mess, long? type_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess;
                log_esn.log_esn_type_id = type_id;  //51, error: 101
                log_esn.user_name = user_name;
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.CABINET; //2
                log_esn.date_end = DateTime.Now;
                log_esn.mess2 = mess2;
                dbContext.log_esn.Add(log_esn);
                dbContext.SaveChanges();
            }

            return true;
        }

    }
}
