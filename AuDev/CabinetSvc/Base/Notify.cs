﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace CabinetMvc
{
    [DataContract]
    public class Notify : CabinetBase
    {
        public Notify()
        {
            //
        }

        public Notify(vw_notify item)
        {
            db_notify = item;
            notify_id = item.notify_id;
            scope = item.scope;
            target_user_id = item.target_user_id;
            message = item.message;
            message_sent = item.message_sent;
            crt_date = item.crt_date;
            sent_date = item.sent_date;
            transport_type = item.transport_type;            
            icq = item.icq;
            email = item.email;
            is_active_icq = item.is_active_icq;
            is_active_email = item.is_active_email;
        }

        [DataMember]
        public int notify_id { get; set; }
        [DataMember]
        public int scope { get; set; }
        [DataMember]
        public Nullable<int> target_user_id { get; set; }
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public bool message_sent { get; set; }
        [DataMember]
        public Nullable<System.DateTime> crt_date { get; set; }
        [DataMember]
        public Nullable<System.DateTime> sent_date { get; set; }
        [DataMember]
        public int transport_type { get; set; }
        [DataMember]
        public string icq { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public Nullable<bool> is_active_icq { get; set; }
        [DataMember]
        public Nullable<bool> is_active_email { get; set; }

        public vw_notify Db_notify { get { return db_notify; } }
        private vw_notify db_notify;
    }

    [DataContract]
    public class NotifyList : CabinetBase
    {
        public NotifyList()
            : base()
        {
            //
        }

        public NotifyList(List<vw_notify> notifyList)
        {
            if (notifyList != null)
            {
                Notify_list = new List<Notify>();
                foreach (var item in notifyList)
                {
                    Notify_list.Add(new Notify(item));
                }
            }
        }

        [DataMember]
        public List<Notify> Notify_list;
    }
}
