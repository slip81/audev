﻿using System;
using System.Net;
using System.Windows;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CabinetMvc
{
    public class KendoSort
    {
        public KendoSort()
        {
            //
        }

        [JsonProperty("sort")]
        public List<KendoSortItemValue> sorts { get; set; }
        [JsonIgnore()]
        public string OrderByString
        {
            get
            {                                
                bool isFirst = true;
                string res = "";
                if (sorts != null)
                {
                    foreach (var item in sorts)
                    {
                        if (!isFirst)
                            res += ", ";
                        res += item.Field + " " + item.Dir.ToUpper();
                        isFirst = false;
                    }
                }
                return res;        
            }
        }
    }


    //[JsonObject("sort")]
    public class KendoSortItemValue
    {
        public KendoSortItemValue()
        {
            //
        }

        [JsonProperty("field")]
        public string Field { get; set; }
        [JsonProperty("dir")]
        public string Dir { get; set; }
    }
}
