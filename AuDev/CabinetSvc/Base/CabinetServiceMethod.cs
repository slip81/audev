﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
#endregion

namespace CabinetMvc
{
    public partial class CabinetService : ICabinetService
    {
        #region ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus

        private bool xChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus(string s1)
        {
            DateTime now = DateTime.Now;

            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }

            DateTime today = DateTime.Today;            
            DateTime prev_today = today.AddDays(-1);
            long? curr_limit_id = null;
            long? prev_limit_id = null;
            long? card_status_id_old = 0;
            int card_ext1_cnt = 0;
            bool something_changed = false;
            decimal card_trans_sum = 0;

            using (var context = new AuMainDb())
            {
                List<card_type> card_type_list = null;
                List<card_ext1> card_ext1_list = null;
                card card = null;
                card_card_status_rel card_card_status_rel_existing = null;
                card_card_status_rel card_card_status_rel_new = null;


                List<long> business_list = context.business.Select(ss => ss.business_id).ToList();

                foreach (var curr_business in business_list)
                {
                    card_type_list = (from ct in context.card_type
                                      from ctl in context.card_type_limit
                                      where ct.card_type_id == ctl.card_type_id
                                      && ct.business_id == curr_business
                                      //&& ctl.limit_type == (int)Enums.CardLimitType.MoneyMonthly
                                      && ((ctl.limit_type == (int)Enums.CardLimitType.MoneyMonthly) || (ctl.limit_type == (int)Enums.CardLimitType.MoneyYearly))
                                      select ct)
                                      .Distinct()
                                      .ToList();

                    if (card_type_list != null)
                    {
                        foreach (var curr_card_type in card_type_list)
                        {

                            var curr_limit = context.card_type_limit.Where(ss => ss.card_type_id == curr_card_type.card_type_id
                                //&& ss.limit_type == (int)Enums.CardLimitType.MoneyMonthly
                                && ((ss.limit_type == (int)Enums.CardLimitType.MoneyMonthly) || (ss.limit_type == (int)Enums.CardLimitType.MoneyYearly))
                                && ss.limit_date_beg <= today && ss.limit_date_end >= today)
                                .FirstOrDefault();
                            if (curr_limit != null)
                            {
                                curr_limit_id = curr_limit.limit_id;
                                var prev_limit = context.card_type_limit.Where(ss => ss.card_type_id == curr_card_type.card_type_id
                                    //&& ss.limit_type == (int)Enums.CardLimitType.MoneyMonthly
                                    && ((ss.limit_type == (int)Enums.CardLimitType.MoneyMonthly) || (ss.limit_type == (int)Enums.CardLimitType.MoneyYearly))
                                    && ss.limit_date_end < curr_limit.limit_date_beg
                                    )
                                    .OrderByDescending(ss => ss.limit_date_beg)
                                    .FirstOrDefault();
                                if (prev_limit != null)
                                {
                                    prev_limit_id = prev_limit.limit_id;
                                }
                            }


                            if ((curr_limit_id.GetValueOrDefault(0) > 0) && (prev_limit_id.GetValueOrDefault(0) > 0) && (curr_limit_id != prev_limit_id))
                            {
                                card_ext1_list = (from c in context.card
                                                  from cext in context.card_ext1
                                                  where c.card_id == cext.card_id
                                                  && c.curr_card_type_id == curr_card_type.card_type_id
                                                  && cext.curr_limit_id == prev_limit_id
                                                  select cext)
                                                      .Distinct()
                                                      .ToList();

                                if ((card_ext1_list != null) && (card_ext1_list.Count > 0))
                                {
                                    card_ext1_cnt = card_ext1_list.Count;
                                    foreach (var card_id in card_ext1_list.Select(ss => ss.card_id).Distinct().ToList())
                                    {
                                        card = context.card.Where(ss => ss.card_id == card_id).FirstOrDefault();
                                        if (card != null)
                                        {
                                            // сбрасываем накопленные суммы
                                            card_trans_sum = card.curr_trans_sum.GetValueOrDefault(0);
                                            card.curr_trans_sum = 0;
                                            card.curr_trans_count = 0;

                                            Loggly.InsertLog_Discount("[ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus] Сброс накопленной суммы: карта номер " + card.card_num.ToString() + ", сброшенная сумма: " + card_trans_sum.ToString(), (long)Enums.LogEventType.SCHEDULER, curr_business, "scheduler", card.card_id, now, null);
                                            something_changed = true;

                                            if (card.curr_card_status_id != 2)
                                            {
                                                card_card_status_rel_existing = context.card_card_status_rel.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).FirstOrDefault();
                                                if (card_card_status_rel_existing != null)
                                                {
                                                    card_card_status_rel_existing.date_end = today;
                                                }
                                                card_card_status_rel_new = new card_card_status_rel();
                                                card_card_status_rel_new.card_id = card.card_id;
                                                card_card_status_rel_new.card_status_id = 2;
                                                card_card_status_rel_new.date_beg = today;
                                                card_card_status_rel_new.date_end = null;
                                                context.card_card_status_rel.Add(card_card_status_rel_new);

                                                card_status_id_old = card.curr_card_status_id;
                                                card.curr_card_status_id = card_card_status_rel_new.card_status_id;

                                                Loggly.InsertLog_Discount("[ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus] Смена статуса: карта номер " + card.card_num.ToString() + ", старый статус: " + card_status_id_old.ToString() + ", новый статус: " + card.curr_card_status_id.ToString(), (long)Enums.LogEventType.SCHEDULER, curr_business, "scheduler", card.card_id, now, null);
                                                //something_changed = true;
                                            }

                                            context.SaveChanges();
                                        }
                                    }

                                    card_ext1_cnt = card_ext1_list.Count;
                                    context.card_ext1.RemoveRange(card_ext1_list);
                                    context.SaveChanges();

                                    Loggly.InsertLog_Discount("[ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus] Удаление использованных лимитов: тип карт " + curr_card_type.card_type_id.ToString() + ", старый лимит: " + prev_limit_id.ToString() + ", новый лимит: " + curr_limit_id.ToString() + ", удалено строк: " + card_ext1_cnt.ToString(), (long)Enums.LogEventType.SCHEDULER, curr_business, "scheduler", curr_card_type.card_type_id, now, null);
                                    something_changed = true;
                                }
                            }
                        }
                    }

                    context.SaveChanges();
                }

                if (!something_changed)
                    Loggly.InsertLog_Discount("[ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);

                return true;
            }
        }

        #endregion

        #region ChangeBonusDailyPeriod

        private bool xChangeBonusDailyPeriod(string s1)
        {
            DateTime now = DateTime.Now;

            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[ChangeBonusDailyPeriod] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }

            DateTime today = DateTime.Today;            
            DateTime prev_today = today.AddDays(-1);
            int card_ext1_cnt = 0;
            bool something_changed = false;

            using (var context = new AuMainDb())
            {
                List<card_type> card_type_list = null;
                List<card_ext1> card_ext1_list = null;
                card card = null;

                List<long> business_list = context.business.Select(ss => ss.business_id).ToList();

                foreach (var curr_business in business_list)
                {
                    card_type_list = (from ct in context.card_type
                                      from ctl in context.card_type_limit
                                      where ct.card_type_id == ctl.card_type_id
                                      && ct.business_id == curr_business
                                      && ctl.limit_type == (int)Enums.CardLimitType.BonusDaily
                                      select ct)
                                      .Distinct()
                                      .ToList();

                    if (card_type_list != null)
                    {
                        foreach (var curr_card_type in card_type_list)
                        {

                            var curr_limit = context.card_type_limit
                                .Where(ss => ss.card_type_id == curr_card_type.card_type_id
                                && ss.limit_type == (int)Enums.CardLimitType.BonusDaily
                                && ss.limit_date_beg <= today && ss.limit_date_end >= today
                                )
                                .FirstOrDefault();

                            if (curr_limit != null)
                            {
                                card_ext1_list = (from c in context.card
                                                  from cext in context.card_ext1
                                                  where c.card_id == cext.card_id
                                                  && c.curr_card_type_id == curr_card_type.card_type_id
                                                  && cext.curr_limit_id == curr_limit.limit_id
                                                  select cext)
                                                      .Distinct()
                                                      .ToList();

                                if ((card_ext1_list != null) && (card_ext1_list.Count > 0))
                                {
                                    card_ext1_cnt = card_ext1_list.Count;
                                    context.card_ext1.RemoveRange(card_ext1_list);
                                    context.SaveChanges();

                                    Loggly.InsertLog_Discount("[ChangeBonusDailyPeriod] Удаление использованных лимитов: тип карт " + curr_card_type.card_type_id.ToString() + ", код лимита: " + curr_limit.limit_id.ToString() + ", удалено строк: " + card_ext1_cnt.ToString(), (long)Enums.LogEventType.SCHEDULER, curr_business, "scheduler", curr_card_type.card_type_id, now, null);
                                    something_changed = true;
                                }
                            }
                        }
                    }

                    context.SaveChanges();
                }

                if (!something_changed)
                    Loggly.InsertLog_Discount("[ChangeBonusDailyPeriod] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);

                return true;
            }
        }

        #endregion

        #region MakeInactiveNominalActive

        private bool xMakeInactiveNominalActive(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[MakeInactiveNominalActive] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }
            
            DateTime today = DateTime.Today;
            bool something_changed = false;
            int cnt = 0;

            using (var context = new AuMainDb())
            {
                var nominal_inactive_list = context.card_nominal_inactive.Where(ss => ss.active_date <= now).ToList();
                if ((nominal_inactive_list == null) || (nominal_inactive_list.Count <= 0))
                {
                    Loggly.InsertLog_Discount("[MakeInactiveNominalActive] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);
                    return true;
                }

                var nominal_inactive_list_grouped = nominal_inactive_list
                    .GroupBy(ss => new { ss.card_id, ss.unit_type })
                    .Select(ss => new { card_id = ss.Key.card_id, unit_type = ss.Key.unit_type, unit_value = ss.Sum(tt => tt.unit_value.GetValueOrDefault(0)) })
                    ;

                foreach (var nominal_inactive in nominal_inactive_list_grouped)
                {
                    switch (nominal_inactive.unit_type)
                    {
                        case (int)Enums.CardUnitTypeEnum.BONUS_VALUE:
                            var nominal = context.card_nominal.Where(ss => ss.card_id == nominal_inactive.card_id && ss.unit_type == nominal_inactive.unit_type && !ss.date_end.HasValue).OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                            if (nominal == null)
                            {
                                nominal = new card_nominal();
                                nominal.card_id = nominal_inactive.card_id;
                                nominal.crt_date = now;
                                nominal.crt_user = "scheduler";
                                nominal.date_beg = today;
                                nominal.date_end = null;
                                nominal.unit_type = nominal_inactive.unit_type;
                                nominal.unit_value = nominal_inactive.unit_value;
                            }
                            else
                            {
                                nominal.date_end = today;
                                var new_nominal = new card_nominal();
                                new_nominal.card_id = nominal_inactive.card_id;
                                new_nominal.crt_date = now;
                                new_nominal.crt_user = "scheduler";
                                new_nominal.date_beg = today;
                                new_nominal.date_end = null;
                                new_nominal.unit_type = nominal_inactive.unit_type;
                                new_nominal.unit_value = nominal.unit_value.GetValueOrDefault(0) + nominal_inactive.unit_value;
                                context.card_nominal.Add(new_nominal);
                            }
                            //id_for_del.Add(nominal_inactive.inactive_id);
                            //context.card_nominal_inactive.Remove(nominal_inactive);
                            something_changed = true;
                            cnt++;
                            break;
                        default:
                            break;
                    }

                    //context.SaveChanges();
                }

                //context.card_nominal_inactive.RemoveRange(context.card_nominal_inactive.Where(ss => id_for_del.Contains(ss.inactive_id)));
                context.card_nominal_inactive.RemoveRange(nominal_inactive_list);
                context.SaveChanges();
                if (something_changed)
                    Loggly.InsertLog_Discount("[MakeInactiveNominalActive] Перенесено номиналов: " + cnt.ToString(), (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);
                else
                    Loggly.InsertLog_Discount("[MakeInactiveNominalActive] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);

                return true;
            }
        }

        #endregion

        #region DeleteActiveCardTrans

        private bool xDeleteActiveCardTrans(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[DeleteActiveCardTrans] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }

            DateTime curr_date_for_del = now.AddHours(-1);
            int cnt = 0;
            bool something_changed = false;
            List<card_transaction> card_trans_list_for_del = null;
            using (AuMainDb dbContext = new AuMainDb())
            {

                card_trans_list_for_del = (from ct in dbContext.card_transaction
                                           where ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                           && ct.date_beg <= curr_date_for_del
                                           select ct)
                                           .ToList();

                if ((card_trans_list_for_del != null) && (card_trans_list_for_del.Count > 0))
                {
                    cnt = card_trans_list_for_del.Count;

                    dbContext.card_nominal_uncommitted.RemoveRange(from ct in dbContext.card_transaction
                                                                   from unc in dbContext.card_nominal_uncommitted
                                                                   where ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                   && ct.date_beg <= curr_date_for_del
                                                                   && ct.card_trans_id == unc.card_trans_id
                                                                   select unc);

                    dbContext.programm_step_result.RemoveRange(from ct in dbContext.card_transaction
                                                               from pr in dbContext.programm_result
                                                               from pst in dbContext.programm_step_result
                                                               where ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                               && ct.date_beg <= curr_date_for_del
                                                               && ct.programm_result_id.HasValue
                                                               && ct.programm_result_id == pr.result_id
                                                               && pr.result_id == pst.result_id
                                                               select pst);

                    dbContext.programm_result_detail.RemoveRange(from ct in dbContext.card_transaction
                                                                 from pr in dbContext.programm_result
                                                                 from prd in dbContext.programm_result_detail
                                                                 where ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                 && ct.date_beg <= curr_date_for_del
                                                                 && ct.programm_result_id.HasValue
                                                                 && ct.programm_result_id == pr.result_id
                                                                 && pr.result_id == prd.result_id
                                                                 select prd);

                    dbContext.programm_result.RemoveRange(from ct in dbContext.card_transaction
                                                          from pr in dbContext.programm_result
                                                          where ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                          && ct.date_beg <= curr_date_for_del
                                                          && ct.programm_result_id.HasValue
                                                          && ct.programm_result_id == pr.result_id
                                                          select pr);

                    dbContext.card_transaction_detail.RemoveRange(from ct in dbContext.card_transaction
                                                                  from ctd in dbContext.card_transaction_detail
                                                                  where ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                  && ct.date_beg <= curr_date_for_del
                                                                  && ct.card_trans_id == ctd.card_trans_id
                                                                  select ctd);

                    dbContext.card_transaction_unit.RemoveRange(from ct in dbContext.card_transaction
                                                                from ctu in dbContext.card_transaction_unit
                                                                where ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                && ct.date_beg <= curr_date_for_del
                                                                && ct.card_trans_id == ctu.card_trans_id
                                                                select ctu);
                                        
                    dbContext.card_transaction.RemoveRange(card_trans_list_for_del);
                    
                    dbContext.SaveChanges();

                    something_changed = true;
                }

                if (!something_changed)
                    Loggly.InsertLog_Discount("[DeleteActiveCardTrans] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);
                else
                    Loggly.InsertLog_Discount("[DeleteActiveCardTrans] Удалено неактуальных транзакций: " + cnt.ToString(), (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);

                return true;
            }
        }

        #endregion

        #region UpdateSaleSummary

        private bool xUpdateSaleSummary(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[UpdateSaleSummary] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }
            
            DateTime last_month_start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1, 0, 00, 00);
            DateTime last_day_start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 0, 00, 00);
            DateTime prev_day_start = last_day_start.AddDays(-1);

            using (var context = new AuMainDb())
            {
                List<long> business_id_list = context.business.OrderBy(ss => ss.business_id).Select(ss => ss.business_id).ToList();

                foreach (var business_id in business_id_list)
                {
                    /*
                    var info1 = (from ct in context.vw_card_transaction
                                              where ct.business_id == business_id
                                              && ct.trans_kind == (short)Enums.CardTransactionType.CALC
                                              && ct.status == (int)Enums.CardTransactionStatus.DONE
                                              group ct by 1 into group1
                                                       select new 
                                              {
                                                  sale_sum = group1.Sum(ss => ss.trans_sum),
                                                  sale_sum_discount = group1.Sum(ss => ss.trans_sum_discount),
                                                  sale_sum_with_discount = group1.Sum(ss => ss.trans_sum_with_discount),
                                                  bonus_for_card_sum = group1.Sum(ss => ss.trans_sum_bonus_for_card),
                                                  bonus_for_pay_sum = group1.Sum(ss => ss.trans_sum_bonus_for_pay),
                                                  sale_cnt = group1.Count(),
                                                  business_id = business_id,
                                                  summary_type = 1,                                                  
                                              })
                              .FirstOrDefault();
                    */

                    var info1 = (from ct in context.card_transaction
                                 from ctd in context.card_transaction_detail
                                 from c in context.card
                                 where ct.card_trans_id == ctd.card_trans_id
                                 && ct.card_id == c.card_id
                                 && c.business_id == business_id
                                 && ct.trans_kind == (short)Enums.CardTransactionType.CALC
                                 && ct.status == (int)Enums.CardTransactionStatus.DONE
                                 //select ctd;
                                 //group new { ct, ctd, c} by 1
                                 group ctd by 1
                                     into group1
                                     select new
                                     {
                                         sale_sum = group1.Sum(ss => ss.unit_value),
                                         sale_sum_discount = group1.Sum(ss => ss.unit_value_discount),
                                         sale_sum_with_discount = group1.Sum(ss => ss.unit_value_with_discount),
                                         bonus_for_card_sum = group1.Sum(ss => ss.unit_value_bonus_for_card),
                                         bonus_for_pay_sum = group1.Sum(ss => ss.unit_value_bonus_for_pay),
                                         sale_cnt = group1.Count(),
                                         business_id = business_id,
                                         summary_type = 1,
                                     })
                                   .FirstOrDefault();

                    sale_summary sale_summary_info1 = null;
                    if (info1 != null)
                    {
                        sale_summary_info1 = new sale_summary()
                        {
                            summary_type = 1,
                            business_id = business_id,
                            bonus_for_card_sum = info1.bonus_for_card_sum,
                            bonus_for_pay_sum = info1.bonus_for_pay_sum,
                            sale_cnt = info1.sale_cnt,
                            sale_sum = info1.sale_sum,
                            sale_sum_discount = info1.sale_sum_discount,
                            sale_sum_with_discount = info1.sale_sum_with_discount,
                        };
                    }
                    else
                    {
                        sale_summary_info1 = new sale_summary() { summary_type = 1, business_id = business_id, };
                    }

                    /*
                    var info2 = (from ct in context.vw_card_transaction
                                              where ct.business_id == business_id
                                              && ct.date_beg >= last_month_start
                                              && ct.trans_kind == (short)Enums.CardTransactionType.CALC
                                              && ct.status == (int)Enums.CardTransactionStatus.DONE
                                              group ct by 1 into group1
                                                       select new 
                                              {
                                                  sale_sum = group1.Sum(ss => ss.trans_sum),
                                                  sale_sum_discount = group1.Sum(ss => ss.trans_sum_discount),
                                                  sale_sum_with_discount = group1.Sum(ss => ss.trans_sum_with_discount),
                                                  bonus_for_card_sum = group1.Sum(ss => ss.trans_sum_bonus_for_card),
                                                  bonus_for_pay_sum = group1.Sum(ss => ss.trans_sum_bonus_for_pay),
                                                  sale_cnt = group1.Count(),
                                                  business_id = business_id,
                                                  summary_type = 2,                                                  
                                              })
                             .FirstOrDefault();
                    */

                    var info2 = (from ct in context.card_transaction
                                 from ctd in context.card_transaction_detail
                                 from c in context.card
                                 where ct.card_trans_id == ctd.card_trans_id
                                 && ct.card_id == c.card_id
                                 && c.business_id == business_id
                                 && ct.date_beg >= last_month_start
                                 && ct.trans_kind == (short)Enums.CardTransactionType.CALC
                                 && ct.status == (int)Enums.CardTransactionStatus.DONE
                                 //select ctd;
                                 //group new { ct, ctd, c} by 1
                                 group ctd by 1
                                     into group1
                                     select new
                                     {
                                         sale_sum = group1.Sum(ss => ss.unit_value),
                                         sale_sum_discount = group1.Sum(ss => ss.unit_value_discount),
                                         sale_sum_with_discount = group1.Sum(ss => ss.unit_value_with_discount),
                                         bonus_for_card_sum = group1.Sum(ss => ss.unit_value_bonus_for_card),
                                         bonus_for_pay_sum = group1.Sum(ss => ss.unit_value_bonus_for_pay),
                                         sale_cnt = group1.Count(),
                                         business_id = business_id,
                                         summary_type = 2,
                                     })
                                    .FirstOrDefault();

                    sale_summary sale_summary_info2 = null;
                    if (info2 != null)
                    {
                        sale_summary_info2 = new sale_summary()
                        {
                            summary_type = 2,
                            business_id = business_id,
                            bonus_for_card_sum = info2.bonus_for_card_sum,
                            bonus_for_pay_sum = info2.bonus_for_pay_sum,
                            sale_cnt = info2.sale_cnt,
                            sale_sum = info2.sale_sum,
                            sale_sum_discount = info2.sale_sum_discount,
                            sale_sum_with_discount = info2.sale_sum_with_discount,
                        };
                    }
                    else
                    {
                        sale_summary_info2 = new sale_summary() { summary_type = 2, business_id = business_id, };
                    }

                    /*
                    var info3 = (from ct in context.vw_card_transaction
                                              where ct.business_id == business_id
                                              && ct.date_beg >= prev_day_start
                                              && ct.date_beg <= last_day_start
                                              && ct.trans_kind == (short)Enums.CardTransactionType.CALC
                                              && ct.status == (int)Enums.CardTransactionStatus.DONE
                                              group ct by 1 into group1
                                                       select new 
                                              {
                                                  sale_sum = group1.Sum(ss => ss.trans_sum),
                                                  sale_sum_discount = group1.Sum(ss => ss.trans_sum_discount),
                                                  sale_sum_with_discount = group1.Sum(ss => ss.trans_sum_with_discount),
                                                  bonus_for_card_sum = group1.Sum(ss => ss.trans_sum_bonus_for_card),
                                                  bonus_for_pay_sum = group1.Sum(ss => ss.trans_sum_bonus_for_pay),
                                                  sale_cnt = group1.Count(),
                                                  business_id = business_id,
                                                  summary_type = 3,                                                  
                                              })
                             .FirstOrDefault();
                    */
                    var info3 = (from ct in context.card_transaction
                                 from ctd in context.card_transaction_detail
                                 from c in context.card
                                 where ct.card_trans_id == ctd.card_trans_id
                                 && ct.card_id == c.card_id
                                 && c.business_id == business_id
                                 && ct.date_beg >= prev_day_start
                                 && ct.date_beg <= last_day_start
                                 && ct.trans_kind == (short)Enums.CardTransactionType.CALC
                                 && ct.status == (int)Enums.CardTransactionStatus.DONE
                                 //select ctd;
                                 //group new { ct, ctd, c} by 1
                                 group ctd by 1
                                     into group1
                                     select new
                                     {
                                         sale_sum = group1.Sum(ss => ss.unit_value),
                                         sale_sum_discount = group1.Sum(ss => ss.unit_value_discount),
                                         sale_sum_with_discount = group1.Sum(ss => ss.unit_value_with_discount),
                                         bonus_for_card_sum = group1.Sum(ss => ss.unit_value_bonus_for_card),
                                         bonus_for_pay_sum = group1.Sum(ss => ss.unit_value_bonus_for_pay),
                                         sale_cnt = group1.Count(),
                                         business_id = business_id,
                                         summary_type = 3,
                                     })
                                    .FirstOrDefault();

                    sale_summary sale_summary_info3 = null;
                    if (info3 != null)
                    {
                        sale_summary_info3 = new sale_summary()
                        {
                            summary_type = 3,
                            business_id = business_id,
                            bonus_for_card_sum = info3.bonus_for_card_sum,
                            bonus_for_pay_sum = info3.bonus_for_pay_sum,
                            sale_cnt = info3.sale_cnt,
                            sale_sum = info3.sale_sum,
                            sale_sum_discount = info3.sale_sum_discount,
                            sale_sum_with_discount = info3.sale_sum_with_discount,
                        };
                    }
                    else
                    {
                        sale_summary_info3 = new sale_summary() { summary_type = 3, business_id = business_id, };
                    }

                    context.sale_summary.RemoveRange(context.sale_summary.Where(ss => ss.business_id == business_id));
                    context.sale_summary.Add(sale_summary_info1);
                    context.sale_summary.Add(sale_summary_info2);
                    context.sale_summary.Add(sale_summary_info3);
                    context.SaveChanges();
                }

                Loggly.InsertLog_Discount("[UpdateSaleSummary] Статистика продаж обновлена в " + DateTime.Now.ToString(), (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);

                return true;
            }
        }

        #endregion

        #region TerminateExchangeDoc

        private bool xTerminateExchangeDoc(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_ExchangeDoc("[TerminateExchangeDoc] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }
            
            bool something_changed = false;
            int cnt = 0;
            using (var dbContext = new AuMainDb())
            {
                var docs_for_del = dbContext.exchange_doc.Where(ss => !ss.is_terminated && ss.terminate_date.HasValue && ss.terminate_date <= now).ToList();
                if ((docs_for_del == null) || (docs_for_del.Count <= 0))
                {
                    Loggly.InsertLog_ExchangeDoc("[TerminateExchangeDoc] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);
                    return true;
                }
                foreach (var doc_for_del in docs_for_del)
                {
                    doc_for_del.is_terminated = true;
                    doc_for_del.terminated_date = now;
                    var exchange_doc_data = dbContext.exchange_doc_data.Where(ss => ss.item_id == doc_for_del.item_id).FirstOrDefault();
                    if (exchange_doc_data != null)
                        dbContext.exchange_doc_data.Remove(exchange_doc_data);
                    cnt++;
                    something_changed = true;
                }
                dbContext.SaveChanges();

                if (!something_changed)
                    Loggly.InsertLog_ExchangeDoc("[TerminateExchangeDoc] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);
                else
                    Loggly.InsertLog_ExchangeDoc("[TerminateExchangeDoc] Удалено просроченных документов: " + cnt.ToString(), (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);

                return true;
            }
        }

        #endregion

        #region RunBatch_Operations

        private bool xRunBatch_Operations(string s1, long batch_id, string user_name, bool new_thread)
        {
            if (new_thread)
            {
                Task taskBatch = new Task(() =>
                {
                    xxRunBatch_Operations(s1, batch_id, user_name);
                }
                );
                taskBatch.Start();
                return true;
            }
            else
            {
                return xxRunBatch_Operations(s1, batch_id, user_name);
            }
        }

        private bool xxRunBatch_Operations(string s1, long batch_id, string user_name)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[RunBatch_Operations] " + s1, (long)Enums.LogEventType.ERROR, null, "batchworker", null, now, null);
                return false;
            }

            DateTime today = DateTime.Today;

            IQueryable<vw_card> cards = null;
            batch batch = null;
            List<card> cards_list = null;
            batch_error err = null;

            string id_list = "";

            using (var context = new AuMainDb())
            {

                batch = context.batch.Where(ss => ss.batch_id == batch_id).FirstOrDefault();

                string batch_name = "";
                if (batch.trans_sum_add)
                    batch_name += "Добавление сумм (" + batch.trans_sum_add_value.ToString() + "); ";
                if (batch.disc_percent_add)
                    batch_name += "Добавление % скидки (" + batch.disc_percent_add_value.ToString() + "); ";
                if (batch.bonus_percent_add)
                    batch_name += "Добавление бонусного % (" + batch.bonus_percent_add_value.ToString() + "); ";
                if (batch.bonus_add)
                    batch_name += "Добавление бонусов (" + batch.bonus_add_value.ToString() + "); ";
                if (batch.money_add)
                    batch_name += "Добавление сумм на сертификаты (" + batch.money_add_value.ToString() + "); ";
                if (batch.mess_add)
                    batch_name += "Добавление комментария (" + batch.mess_add_value + "); ";
                if (batch.change_card_status)
                    batch_name += "Смена статуса (" + batch.card_status_id.ToString() + "); ";
                if (batch.change_card_type)
                    batch_name += "Смена типа (" + batch.card_type_id.ToString() + "); ";
                if (batch.del_cards)
                    batch_name += "Удаление карт;";
                if (String.IsNullOrEmpty(batch_name))
                {
                    batch.state = (int)Enums.BatchState.Finished;
                    batch.state_date = DateTime.Now;
                    batch.mess = "Не заданы операции";
                    batch.record_cnt = 0;
                    batch.processed_cnt = 0;
                    context.SaveChanges();
                    return false;
                }
                batch.batch_name = batch_name;

                try
                {
                    #region

                    var filter_object = JsonConvert.DeserializeObject<KendoFilter>(batch.filter_string);
                    var filter_string = "business_id=" + batch.business_id.ToString();
                    var filter_string_for_save = filter_string;
                    object[] filter_string_params = null;

                    var sort_object = JsonConvert.DeserializeObject<KendoSort>(batch.sort_string);
                    var order_by_string = "";

                    if ((Enums.BatchCardListMode)batch.card_list_mode == Enums.BatchCardListMode.AllCards)
                    {
                        cards = context.vw_card.AsQueryable().Where(ss => ss.business_id == batch.business_id);
                    }
                    else if ((Enums.BatchCardListMode)batch.card_list_mode == Enums.BatchCardListMode.SelectedCards)
                    {
                        id_list = batch.card_id_list;
                        if (String.IsNullOrEmpty(id_list))
                        {
                            batch.state = (int)Enums.BatchState.Finished;
                            batch.state_date = DateTime.Now;
                            batch.mess = "Не задан список карт";
                            batch.record_cnt = 0;
                            batch.processed_cnt = 0;
                            context.SaveChanges();
                            return false;
                        }
                        if (id_list.EndsWith(","))
                            id_list = id_list.Trim().Remove(id_list.Trim().Length - 1);
                        batch.card_id_list = id_list;
                        cards = context.vw_card.AsQueryable().Where(ss => ss.business_id == batch.business_id && id_list.Contains(ss.card_id_str));
                    }
                    else
                    {
                        if ((filter_object != null) && (!String.IsNullOrEmpty(filter_object.FilterString)))
                        {
                            filter_string += " AND " + filter_object.FilterString;                            
                            filter_string_params = filter_object.FilterStringParams.ToArray();
                            filter_string_for_save = filter_string + "; params: " + string.Join(";", filter_object.FilterStringParams.ToArray());
                        }

                        batch.filter_string = filter_string_for_save;

                        cards = context.vw_card.AsQueryable().Where(filter_string, filter_string_params);

                        if (sort_object != null)
                        {
                            order_by_string = sort_object.OrderByString;
                        }
                        if (!String.IsNullOrEmpty(order_by_string))
                            cards = cards.OrderBy(order_by_string);

                        batch.sort_string = order_by_string;

                        int items_to_skip = 0;
                        switch ((Enums.BatchCardListMode)batch.card_list_mode)
                        {
                            case Enums.BatchCardListMode.CurrentPage:
                                if (batch.page_num.GetValueOrDefault(1) > 1)
                                {
                                    items_to_skip = (batch.page_num.GetValueOrDefault(1) - 1) * batch.page_size.GetValueOrDefault(10);
                                    cards = cards.Skip(items_to_skip).Take(batch.page_size.GetValueOrDefault(10));
                                }
                                else
                                {
                                    cards = cards.Take(batch.page_size.GetValueOrDefault(10));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    
                    cards_list = (from c in context.card
                                  from vwc in cards
                                  where c.card_id == vwc.card_id
                                  select c)
                                     .ToList();

                    if (cards_list == null)
                    {
                        batch.state = (int)Enums.BatchState.Finished;
                        batch.state_date = DateTime.Now;
                        batch.mess = "Не найдены подходящие карты";
                        batch.record_cnt = 0;
                        batch.processed_cnt = 0;
                        context.SaveChanges();
                        return false;
                    }

                    batch.record_cnt = cards_list.Count;
                    batch.processed_cnt = 0;
                    batch.error_cnt = 0;

                    #endregion
                }
                catch (Exception ex)
                {
                    batch.state = (int)Enums.BatchState.Finished;
                    batch.state_date = DateTime.Now;
                    batch.mess = "Ошибка перед запуском";
                    batch.processed_cnt = 0;
                    batch.error_cnt = 1;
                    err = new batch_error();
                    err.batch_id = batch_id;
                    err.mess = GlobalUtil.ExceptionInfo(ex);
                    context.batch_error.Add(err);
                    context.SaveChanges();
                    return false;
                }

                
                // !!!
                AuMainDb batchCardAddContext = null;
                int cnt = 0;
                List<batch_card> batch_card_list = new List<batch_card>();
                batch_card batch_card = null;
                try
                {
                    try
                    {
                        batchCardAddContext = new AuMainDb();
                        batchCardAddContext.Configuration.AutoDetectChangesEnabled = false;

                        cnt = 0;
                        foreach (var cards_list_item in cards_list)
                        {
                            ++cnt;
                            batch_card = new batch_card() { batch_id = batch.batch_id, card_id = cards_list_item.card_id, };
                            batchCardAddContext = AddToContext<batch_card>(batchCardAddContext, batch_card, cnt, 100, true);
                        }

                        batchCardAddContext.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        batch.state = (int)Enums.BatchState.Finished;
                        batch.state_date = DateTime.Now;
                        batch.mess = "Ошибка перед запуском";
                        batch.processed_cnt = 0;
                        batch.error_cnt = 1;
                        err = new batch_error();
                        err.batch_id = batch_id;
                        err.mess = GlobalUtil.ExceptionInfo(ex);
                        context.batch_error.Add(err);
                        context.SaveChanges();
                        if (batchCardAddContext != null)
                            batchCardAddContext.Dispose();
                        return false;
                    }
                }
                finally
                {
                    if (batchCardAddContext != null)
                        batchCardAddContext.Dispose();
                }

                context.SaveChanges();


                int processed_cnt = 0;
                int curr_processed_cnt = 0;
                int error_cnt = 0;
                decimal trans_sum_old = 0;
                card_transaction trans_sum_add_transaction = null;
                card_nominal disc_percent_nominal = null;
                card_nominal disc_percent_nominal_new = null;
                decimal disc_percent_nominal_old_value = 0;
                card_nominal bonus_percent_nominal = null;
                card_nominal bonus_percent_nominal_new = null;
                decimal bonus_percent_nominal_old_value = 0;
                card_nominal bonus_value_nominal = null;
                card_nominal bonus_value_nominal_new = null;
                decimal bonus_value_nominal_old_value = 0;
                card_nominal money_value_nominal = null;
                card_nominal money_value_nominal_new = null;
                decimal money_value_nominal_old_value = 0;
                card_card_status_rel status_rel = null;
                card_card_status_rel status_rel_new = null;
                card_card_type_rel type_rel = null;
                card_card_type_rel type_rel_new = null;                

                string card_del_err_str = "";
                
                long? res = context.Database.SqlQuery<long?>("select * from discount.batch_update_card(" + batch.batch_id.ToString() + ")").FirstOrDefault();                                
                
                context.batch_card.RemoveRange(context.batch_card.Where(ss => ss.batch_id == batch.batch_id));
                context.SaveChanges();

                batch.state = (int)Enums.BatchState.Finished;
                batch.state_date = DateTime.Now;
                batch.mess = "Операция завершена";
                batch.processed_cnt = batch.record_cnt;
                batch.error_cnt = error_cnt;

                context.SaveChanges();
            }

            return true;
        }

        private bool DeleteCard(long card_id)
        {

            long id = card_id;

            using (var dbContext = new AuMainDb())
            {
                if (dbContext.card_transaction.Where(ss => ss.card_id == id
                    && ((ss.trans_kind == (short)Enums.CardTransactionType.CALC) || (ss.trans_kind == (short)Enums.CardTransactionType.RETURN))
                    ).Count() > 0)
                {
                    //modelState.AddModelError("serv_result", "По данной карте есть транзакции, код карты " + id.ToString());
                    return false;
                }

                var card = (from c in dbContext.card
                            where c.card_id == id
                            select c)
                           .FirstOrDefault();

                if (card == null)
                {
                    //modelState.AddModelError("serv_result", "Не найдена карта с кодом " + id.ToString());
                    return false;
                }

                // доч объекты: card_card_status_rel, card_card_type_rel, card_nominal, client_card_rel, card_transaction
                List<card_card_status_rel> card_card_status_rel_list = dbContext.card_card_status_rel.Where(ss => ss.card_id == id).ToList();
                if ((card_card_status_rel_list != null) && (card_card_status_rel_list.Count > 0))
                {
                    dbContext.card_card_status_rel.RemoveRange(card_card_status_rel_list);
                }

                List<card_card_type_rel> card_card_type_rel_list = dbContext.card_card_type_rel.Where(ss => ss.card_id == id).ToList();
                if ((card_card_type_rel_list != null) && (card_card_type_rel_list.Count > 0))
                {
                    dbContext.card_card_type_rel.RemoveRange(card_card_type_rel_list);
                }

                List<card_nominal> card_nominal_list = dbContext.card_nominal.Where(ss => ss.card_id == id).ToList();
                if ((card_nominal_list != null) && (card_nominal_list.Count > 0))
                {
                    dbContext.card_nominal.RemoveRange(card_nominal_list);
                }

                dbContext.card_holder_card_rel.RemoveRange(dbContext.card_holder_card_rel.Where(ss => ss.card_id == id));

                List<card_transaction> card_transaction_list = dbContext.card_transaction.Where(ss => ss.card_id == id).ToList();
                if ((card_transaction_list != null) && (card_transaction_list.Count > 0))
                {
                    var card_transaction_detail_list = from ct in dbContext.card_transaction
                                                       from ctd in dbContext.card_transaction_detail
                                                       where ct.card_id == id
                                                       && ct.card_trans_id == ctd.card_trans_id
                                                       select ctd;
                    var card_transaction_unit_list = from ct in dbContext.card_transaction
                                                     from ctu in dbContext.card_transaction_unit
                                                     where ct.card_id == id
                                                     && ct.card_trans_id == ctu.card_trans_id
                                                     select ctu;

                    dbContext.card_transaction_detail.RemoveRange(card_transaction_detail_list);
                    dbContext.card_transaction_unit.RemoveRange(card_transaction_unit_list);
                    dbContext.card_transaction.RemoveRange(card_transaction_list);
                }

                dbContext.card_additional_num.RemoveRange(dbContext.card_additional_num.Where(ss => ss.card_id == id));

                dbContext.card.Remove(card);
                dbContext.SaveChanges();

                return true;
            }
        }

        #endregion

        #region RunBatch_Add

        private bool xRunBatch_Add(string s1, long batch_id, string user_name, bool new_thread)
        {
            if (new_thread)
            {
                Task taskBatch = new Task(() =>
                {
                    xxRunBatch_Add(s1, batch_id, user_name);
                }
                );
                taskBatch.Start();
                return true;
            }
            else
            {
                return xxRunBatch_Add(s1, batch_id, user_name);
            }
        }

        #region old
        /*
        private bool xxRunBatch_Add_old(string s1, long batch_id, string user_name)
        {

            if (s1 != "iguana")
            {
                Loggly.InsertLog("[RunBatch_Add] " + s1, 51, null, "scheduler");
                return false;
            }

            DateTime today = DateTime.Today;

            using (var context = new AuMainDb())
            {
                batch batch = context.batch.Where(ss => ss.batch_id == batch_id).FirstOrDefault();

                long business_id = batch.business_id.GetValueOrDefault(0);

                long card_type_id = batch.card_type_id.GetValueOrDefault(0);
                long card_status_id = batch.card_status_id.GetValueOrDefault(0);
                DateTime date_beg = batch.date_beg.HasValue ? (DateTime)batch.date_beg : DateTime.Today;
                int card_count = batch.card_count.GetValueOrDefault(0);
                int num_first = batch.num_first.GetValueOrDefault(0);
                int num_count = batch.num_count.GetValueOrDefault(13);
                int next_num_first = batch.next_num_first.GetValueOrDefault(1);
                int next_num_step = batch.next_num_step.GetValueOrDefault(1);
                bool use_check_value = batch.use_check_value;

                long curr_card_num = 0;
                string curr_card_num_str = "";

                string batch_name = "Создание карт (" + card_count.ToString() + ")";
                batch.batch_name = batch_name;

                batch.record_cnt = card_count;
                batch.processed_cnt = 0;
                batch.error_cnt = 0;

                int processed_cnt = 0;
                int curr_processed_cnt = 0;
                int error_cnt = 0;

                batch_error err = null;

                context.SaveChanges();

                List<card_type_unit> card_type_unit_list = context.card_type_unit.Where(ss => ss.card_type_id == card_type_id).ToList();
                List<long> card_num_existing_list = context.card.Where(ss => ss.business_id == business_id
                            && ss.card_num.HasValue).Select(ss => (long)ss.card_num).ToList();

                while (processed_cnt < card_count)
                {
                    curr_card_num_str = "";
                    curr_card_num = 0;
                    try
                    {
                        curr_card_num_str = xBuildCardNum(num_first, num_count, next_num_first, use_check_value);
                        curr_card_num = Convert.ToInt64(curr_card_num_str);

                        if ((card_num_existing_list != null) && (card_num_existing_list.Count > 0))
                        {
                            long card_num_existing = card_num_existing_list.Where(ss => curr_card_num == ss).FirstOrDefault();
                            if (card_num_existing > 0)
                            {
                                err = new batch_error();
                                err.batch_id = batch_id;
                                err.card_num = curr_card_num;
                                err.card_num2 = curr_card_num_str;
                                err.mess = "Уже существует карта с номером " + curr_card_num.ToString();
                                context.batch_error.Add(err);
                                context.SaveChanges();
                                processed_cnt++;
                                curr_processed_cnt++;
                                error_cnt++;
                                next_num_first = next_num_first + next_num_step;
                                continue;
                            }
                        }

                        card card = new card();
                        card.business_id = business_id;
                        card.batch_id = batch_id;

                        card.card_num = curr_card_num;
                        card.card_num2 = Convert.ToString(next_num_first);

                        card.date_beg = date_beg;
                        card.date_end = null;
                        card.curr_card_status_id = card_status_id > 0 ? (long?)card_status_id : null;
                        card.curr_card_type_id = card_type_id > 0 ? (long?)card_type_id : null;

                        context.card.Add(card);

                        next_num_first = next_num_first + next_num_step;

                        // вставка в card_card_type_rel, card_card_status_rel
                        card_card_status_rel card_card_status_rel = null;
                        card_card_type_rel card_card_type_rel = null;
                        card_transaction card_transaction = null;
                        card_transaction_unit card_transaction_unit = null;
                        card_nominal card_nominal = null;
                        if (card_type_id > 0)
                        {
                            card_card_type_rel = new card_card_type_rel();
                            card_card_type_rel.card = card;
                            card_card_type_rel.card_type_id = card_type_id;
                            card_card_type_rel.date_beg = date_beg;
                            card_card_type_rel.date_end = null;
                            card_card_type_rel.ord = 1;

                            context.card_card_type_rel.Add(card_card_type_rel);

                            // insert empty card_nominal                            
                            if ((card_type_unit_list != null) && (card_type_unit_list.Count > 0))
                            {
                                card_transaction = new card_transaction();
                                card_transaction.card = card;
                                card_transaction.trans_num = 1;
                                card_transaction.date_beg = DateTime.Now;
                                card_transaction.trans_kind = (short)Enums.CardTransactionType.START_SALDO;
                                card_transaction.status = (int)Enums.CardTransactionStatus.DONE;
                                card_transaction.user_name = user_name;
                                card_transaction.batch_id = batch_id;
                                context.card_transaction.Add(card_transaction);

                                foreach (var card_type_unit in card_type_unit_list)
                                {
                                    card_transaction_unit = new card_transaction_unit();
                                    //card_transaction_unit.card_trans_id = card_transaction.card_trans_id;
                                    card_transaction_unit.card_transaction = card_transaction;
                                    card_transaction_unit.unit_type = card_type_unit.unit_type;
                                    card_transaction_unit.unit_value = 0;
                                    context.card_transaction_unit.Add(card_transaction_unit);

                                    card_nominal = new card_nominal();
                                    card_nominal.card = card;
                                    card_nominal.date_beg = card.date_beg;
                                    card_nominal.card_transaction = card_transaction;
                                    card_nominal.unit_type = card_type_unit.unit_type;
                                    card_nominal.unit_value = 0;
                                    card_nominal.batch_id = batch_id;
                                    card_nominal.crt_date = DateTime.Now;
                                    card_nominal.crt_user = user_name;
                                    context.card_nominal.Add(card_nominal);
                                }
                            }
                        }

                        if (card_status_id > 0)
                        {
                            card_card_status_rel = new card_card_status_rel();
                            card_card_status_rel.card = card;
                            card_card_status_rel.card_status_id = card_status_id;
                            card_card_status_rel.date_beg = date_beg;
                            card_card_status_rel.date_end = null;
                            context.card_card_status_rel.Add(card_card_status_rel);
                        }



                        if (curr_processed_cnt > 100)
                        {
                            batch.processed_cnt = processed_cnt;
                            batch.error_cnt = error_cnt;
                            curr_processed_cnt = 0;
                        }
                        context.SaveChanges();
                        processed_cnt++;
                        curr_processed_cnt++;
                    }
                    catch (Exception ex)
                    {
                        err = new batch_error();
                        err.batch_id = batch_id;
                        err.card_num = curr_card_num;
                        err.card_num2 = curr_card_num_str;
                        err.mess = "Ошибка: " + GlobalUtil.ExceptionInfo(ex);
                        context.batch_error.Add(err);
                        context.SaveChanges();
                        processed_cnt++;
                        curr_processed_cnt++;
                        error_cnt++;
                        next_num_first = next_num_first + next_num_step;
                        continue;
                    }
                }

                batch.state = (int)Enums.BatchState.Finished;
                batch.state_date = DateTime.Now;
                batch.mess = "Операция завершена";
                batch.processed_cnt = batch.record_cnt;
                batch.error_cnt = error_cnt;

                context.SaveChanges();
                return true;
            }
        }
        */
        #endregion

        private bool xxRunBatch_Add(string s1, long batch_id, string user_name)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[RunBatch_Add] " + s1, (long)Enums.LogEventType.ERROR, null, "batchworker", batch_id, now, null);
                return false;
            }

            // свой try-catch, т.к. может выполняться в отдельном потоке
            try
            {
                string connString = CabinetSvcUtil.GetCardDbConnectionString();
                string commText = "select discount.batch_add_card(" + batch_id.ToString() + ")";
                using (Npgsql.NpgsqlConnection conn = new NpgsqlConnection(connString))
                {
                    conn.Open();
                    using (NpgsqlCommand comm = new NpgsqlCommand(commText, conn))
                    {
                        //comm.ExecuteNonQuery();
                        comm.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[RunBatch_Add] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "batchworker", batch_id, now, null);
                return false;
            }

            return true;
        }

        public Tuple<string, string> BuildCardNum(batch batch)
        {
            try
            {
                string res1 = xBuildCardNum(batch.num_first.GetValueOrDefault(0), batch.num_count.GetValueOrDefault(0), batch.next_num_first.GetValueOrDefault(0), batch.use_check_value);
                if (res1.Length > 19)
                    throw new Exception("Превышена длина (19 символов)");

                string res2 = xBuildCardNum(batch.num_first.GetValueOrDefault(0), batch.num_count.GetValueOrDefault(0), batch.next_num_first.GetValueOrDefault(0) + (batch.next_num_step.GetValueOrDefault(0) * (batch.card_count.GetValueOrDefault(0) - 1)), batch.use_check_value);

                if (res2.Length > 19)
                    throw new Exception("Превышена длина (19 символов)");


                Tuple<string, string> res = new Tuple<string, string>(res1, res2);
                return res;

            }
            catch (Exception ex)
            {
                return new Tuple<string, string>(GlobalUtil.ExceptionInfo(ex), "");
            }
        }

        private string xBuildCardNum(int num_first, int num_count, int num_curr, bool use_check_value)
        {

            string mask_num_string = num_first.ToString()
                + new string('0', num_count - num_first.ToString().Length - num_curr.ToString().Length - (use_check_value ? 1 : 0))
                + num_curr.ToString();

            if (use_check_value)
            {
                char[] mask_char_array = (mask_num_string + "x").ToCharArray();
                Array.Reverse(mask_char_array);

                int summ1 = 0;
                int summ2 = 0;
                int summ3 = 0;
                int summ4 = 0;
                int i;
                int k = 0;

                for (i = 1; i <= mask_char_array.Length - 1; i = i + 2)
                {
                    summ1 += Convert.ToInt16(mask_char_array[i].ToString());
                }
                summ1 = summ1 * 3;

                for (i = 2; i <= mask_char_array.Length - 1; i = i + 2)
                {
                    summ2 += Convert.ToInt16(mask_char_array[i].ToString());
                }

                summ3 = summ1 + summ2;

                summ4 = summ3;
                while (true)
                {
                    if (((summ4 + k) % 10) == 0)
                        break;
                    k++;
                }

                mask_num_string += k.ToString();
            }

            return mask_num_string;

        }

        #endregion

        #region Notify

        private Notify xGetLastNotify(string s1, Enums.NotifyTransportTypeEnum notifyTransportTypeEnum)
        {
            if (s1 != "iguana")
                return new Notify() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            using (var dbContext = new AuMainDb())
            {
                var notify = dbContext.vw_notify.Where(ss => ss.transport_type == (int)notifyTransportTypeEnum
                    && (
                    ((ss.icq.Trim().ToLower() != "") && (notifyTransportTypeEnum == Enums.NotifyTransportTypeEnum.ICQ) && (ss.is_active_icq == true) && (ss.message_sent))
                    ||
                    ((ss.email.Trim().ToLower() != "") && (notifyTransportTypeEnum == Enums.NotifyTransportTypeEnum.EMAIL) && (ss.is_active_email == true) && (ss.message_sent_email))
                    )
                    )
                    .OrderByDescending(ss => ss.notify_id).FirstOrDefault();
                if (notify == null)
                    notify = dbContext.vw_notify.Where(ss => ss.transport_type == (int)notifyTransportTypeEnum
                        && (
                        ((ss.icq.Trim().ToLower() != "") && (notifyTransportTypeEnum == Enums.NotifyTransportTypeEnum.ICQ) && (ss.is_active_icq == true) && (!ss.message_sent))
                        ||
                        ((ss.email.Trim().ToLower() != "") && (notifyTransportTypeEnum == Enums.NotifyTransportTypeEnum.EMAIL) && (ss.is_active_email == true) && (!ss.message_sent_email))
                        )
                        )
                        .OrderByDescending(ss => ss.notify_id).FirstOrDefault();
                if (notify == null)
                    notify = dbContext.vw_notify.Where(ss => ss.transport_type == (int)notifyTransportTypeEnum
                        && (
                        ((ss.icq.Trim().ToLower() != "") && (notifyTransportTypeEnum == Enums.NotifyTransportTypeEnum.ICQ) && (ss.is_active_icq == true))
                        ||
                        ((ss.email.Trim().ToLower() != "") && (notifyTransportTypeEnum == Enums.NotifyTransportTypeEnum.EMAIL) && (ss.is_active_email == true))
                        )
                        )
                        .OrderByDescending(ss => ss.notify_id).FirstOrDefault();
                if (notify == null)
                    return new Notify() { error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нет сообщений в таблице") };

                return new Notify(notify);
            }
        }

        private NotifyList xGetNotifyList(string s1, int last_id, Enums.NotifyTransportTypeEnum notifyTransportTypeEnum)
        {
            if (s1 != "iguana")
                return new NotifyList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            using (var dbContext = new AuMainDb())
            {
                var notify_list = dbContext.vw_notify.Where(ss => ss.notify_id >= last_id 
                    && ss.transport_type == (int)notifyTransportTypeEnum
                    && (
                    ((ss.icq.Trim().ToLower() != "") && (notifyTransportTypeEnum == Enums.NotifyTransportTypeEnum.ICQ) && (ss.is_active_icq == true) && (!ss.message_sent))
                    ||
                    ((ss.email.Trim().ToLower() != "") && (notifyTransportTypeEnum == Enums.NotifyTransportTypeEnum.EMAIL) && (ss.is_active_email == true) && (!ss.message_sent_email))
                    )
                    )
                    .ToList();

                if ((notify_list == null) || (notify_list.Count <= 0))                    
                    return new NotifyList() { error = null, Notify_list = new List<Notify>(), };

                if (notify_list.Count > 3)
                {
                    notify_list = notify_list.OrderByDescending(ss => ss.notify_id).Take(3).OrderBy(ss => ss.notify_id).ToList();
                }

                return new NotifyList(notify_list);
            }
        }

        private ServiceOperationResult xUpdateNotifyList(string s1, string id_list, Enums.NotifyTransportTypeEnum notifyTransportTypeEnum)
        {
            if (s1 != "iguana")
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            if (String.IsNullOrEmpty(id_list))
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы коды строк") };

            DateTime now = DateTime.Now;
            string flag = "0";
            List<int> id_list_int = null;
            try
            {
                id_list_int = id_list.Split(',').Select(int.Parse).ToList();
            }
            catch (Exception e1)
            {
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Ошибка ппри конвертации строки в List<int>") };
            }

            if ((id_list_int == null) || (id_list_int.Count <= 0))
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы коды строк (int)") };

            flag = "1";
            using (var dbContext = new AuMainDb())
            {
                var notify_list = dbContext.notify.Join(id_list_int, up => up.notify_id, id => id, (up, id) => up).ToList();

                flag = "2";
                if ((notify_list == null) || (notify_list.Count <= 0))
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены строки") };

                flag = "3";
                foreach (var notify in notify_list)
                {
                    switch (notifyTransportTypeEnum)
                    {
                        case Enums.NotifyTransportTypeEnum.ICQ:
                            notify.message_sent = true;
                            break;
                        case Enums.NotifyTransportTypeEnum.EMAIL:
                            notify.message_sent_email = true;
                            break;
                        default:
                            break;
                    }                    
                    notify.sent_date = now;
                }

                flag = "4";
                dbContext.SaveChanges();

                flag = "5";
                return new ServiceOperationResult() { error = null, Id = 0, Message = "Строки обновлены"};
            }
        }

        #endregion        

        #region StorageTrashClear

        private bool xStorageTrashClear(string s1)
        {
            DateTime now = DateTime.Now;

            if (s1 != "iguana")
            {
                Loggly.InsertLog_Cabinet("[StorageTrashClear] " + s1, (long)Enums.LogEventType.ERROR, "scheduler", null, now, null);
                return false;
            }

            int cnt = 0;

            // !!!
            // todo

            /*
            using (var context = new AuMainDb())
            {

            }
            */
            
            Loggly.InsertLog_Cabinet("[StorageTrashClear] Удалено из корзины: " + cnt.ToString(), (long)Enums.LogEventType.SCHEDULER, "scheduler", null, now, null);

            return true;
        }

        #endregion

        #region VacuumAnalyze

        private bool xVacuumAnalyze(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Cabinet("[VacuumAnalyze] " + s1, (long)Enums.LogEventType.ERROR, "scheduler", null, now, null);
                return false;

            }

            using (var context = new AuMainDb())
            {
                int res = context.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.DoNotEnsureTransaction, "VACUUM ANALYZE;");
                Loggly.InsertLog_Cabinet("[VacuumAnalyze] Выполнена команда VACUUM ANALYZE [" + res.ToString() + "]", (long)Enums.LogEventType.SCHEDULER, "scheduler", null, now, null);
                return true;
            }
        }

        #endregion

        #region DelOldData

        private bool xDelOldData(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Cabinet("[DelOldData] " + s1, (long)Enums.LogEventType.ERROR, "scheduler", null, now, null);
                return false;

            }

            using (var context = new AuMainDb())
            {
                int res = context.Database.SqlQuery<int>("select * from esn.del_old_data();").FirstOrDefault();
                Loggly.InsertLog_Cabinet("[DelOldData] Выполнена команда del_old_data [" + res.ToString() + "]", (long)Enums.LogEventType.SCHEDULER, "scheduler", null, now, null);
                return true;
            }
        }

        #endregion

        #region UpdateCardDiscountPercentVip

        private bool xUpdateCardDiscountPercentVip(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {                
                Loggly.InsertLog_Discount("[UpdateCardDiscountPercentVip] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }

            // Item1 = business_id, Item2 = card_type_id
            List<Tuple<long, long>> target_list = new List<Tuple<long, long>>() { 
                // Столица
                new Tuple<long, long>(1105604012503205132, 88),
                // !Тест_Монолит
                new Tuple<long, long>(1191989625543984144, 87),
            };

            using (var context = new AuMainDb())
            {
                foreach (var target in target_list)
                {
                    int res = context.Database.SqlQuery<int>("select * from discount.update_card_discount_percent_vip(" + target.Item1.ToString() + ", " + target.Item2.ToString() + ", 'scheduler');").FirstOrDefault();
                    Loggly.InsertLog_Discount("[UpdateCardDiscountPercentVip] Организация " + target.Item1.ToString() + ", тип карт " + target.Item2.ToString() + ", обновлено карт: " + res.ToString(), (long)Enums.LogEventType.SCHEDULER, target.Item1, "scheduler", target.Item2, now, null);                    
                }
                return true;
            }
        }

        #endregion

        #region ResetExpiredBonus

        private bool xResetExpiredBonus(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[ResetExpiredBonus] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }

            /*
            select t2.curr_card_type_id, t1.card_id, sum(t3.unit_value_bonus_for_card) AS trans_sum_bonus_for_card, sum(t3.unit_value_bonus_for_pay) AS trans_sum_bonus_for_pay
            from discount.card_transaction t1
            inner join discount.card t2 on t1.card_id = t2.card_id and t2.business_id = 1302087673820742754
            inner join discount.card_transaction_detail t3 ON t1.card_trans_id = t3.card_trans_id AND t3.unit_type = 0
            where t1.date_check >= '2017-11-01' and t1.date_check < '2017-12-04'
            group by t2.curr_card_type_id, t1.card_id
            */

            // !!!
            // todo

            int cnt = 0;
            DateTime today = DateTime.Today;
            List<card_type> card_type_list = null;
            List<card_nominal> card_nominal_list = null;
            card_nominal card_nominal = null;
            IQueryable<card_transaction> query = null;
            DateTime? date_beg_for_reset = today;
            DateTime date_end_for_reset = today;
            int reset_interval_bonus_value = 0;
            decimal curr_bonus_value = 0;
            decimal new_bonus_value = 0;
            using (AuMainDb dbContext = new AuMainDb())
            {
                // part 1 - только для типов карт с признаком is_reset_without_sales == 0
                #region
                card_type_list = dbContext.card_type.Where(ss => ss.reset_interval_bonus_value.HasValue && ss.reset_interval_bonus_value > 0 && ss.is_reset_without_sales == 0).ToList();
                if ((card_type_list != null) && (card_type_list.Count > 0))
                {
                    foreach (var card_type in card_type_list)
                    {
                        date_beg_for_reset = card_type.reset_date;

                        reset_interval_bonus_value = (int)card_type.reset_interval_bonus_value;
                        date_end_for_reset = today.AddMonths(-reset_interval_bonus_value);

                        // сколько было накоплено за период [date_beg_for_reset - date_end_for_reset]
                        query = dbContext.card_transaction.Where(ss => ss.trans_kind == (short)Enums.CardTransactionType.CALC
                                     && ss.status == (int)Enums.CardTransactionStatus.DONE
                                     && ss.date_check < date_end_for_reset);
                        if (date_beg_for_reset.HasValue)
                            query = query.Where(ss => ss.date_check > date_beg_for_reset);

                        var sum_bonus_for_card_list = (from ct in query
                                                       from ctd in dbContext.card_transaction_detail
                                                       from c in dbContext.card
                                                       where ct.card_trans_id == ctd.card_trans_id
                                                       && ct.card_id == c.card_id
                                                       && c.curr_card_type_id == card_type.card_type_id
                                                       group ctd by ct.card_id into group1
                                                       select new
                                                       {
                                                           card_id = group1.Key,
                                                           sum_bonus = group1.Sum(ss => ss.unit_value_bonus_for_card),
                                                       })
                                                       .ToList();

                        if ((sum_bonus_for_card_list == null) || (sum_bonus_for_card_list.Count <= 0))
                            continue;

                        query = dbContext.card_transaction.Where(ss => ss.trans_kind == (short)Enums.CardTransactionType.CALC
                                     && ss.status == (int)Enums.CardTransactionStatus.DONE
                                     && ss.date_check >= date_end_for_reset
                                     && ss.date_check < today);

                        // сколько было потрачено за период [date_end_for_reset - today]
                        var sum_bonus_for_pay_list = (from ct in query
                                                      from ctd in dbContext.card_transaction_detail
                                                      from c in dbContext.card
                                                      where ct.card_trans_id == ctd.card_trans_id
                                                      && ct.card_id == c.card_id
                                                      && c.curr_card_type_id == card_type.card_type_id
                                                      group ctd by ct.card_id into group1
                                                      select new
                                                      {
                                                          card_id = group1.Key,
                                                          sum_bonus = group1.Sum(ss => ss.unit_value_bonus_for_pay),
                                                      })
                                                      .ToList();

                        foreach (var sum_bonus_for_card_item in sum_bonus_for_card_list)
                        {
                            decimal? sum_bonus_for_pay = sum_bonus_for_pay_list.Where(ss => ss.card_id == sum_bonus_for_card_item.card_id).Select(ss => ss.sum_bonus).FirstOrDefault();
                            decimal bonus_diff = sum_bonus_for_card_item.sum_bonus.GetValueOrDefault(0) - sum_bonus_for_pay.GetValueOrDefault(0);

                            if (bonus_diff <= 0)
                                continue;

                            curr_bonus_value = 0;

                            card_nominal_list = dbContext.card_nominal
                                .Where(ss => ss.card_id == sum_bonus_for_card_item.card_id && ss.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_VALUE && !ss.date_end.HasValue)
                                .ToList();
                            if ((card_nominal_list != null) && (card_nominal_list.Count > 0))
                            {
                                foreach (var card_nominal_list_item in card_nominal_list)
                                {
                                    curr_bonus_value = card_nominal_list_item.unit_value.GetValueOrDefault(0);
                                    card_nominal_list_item.date_end = today;
                                }
                            }

                            new_bonus_value = curr_bonus_value - bonus_diff;
                            new_bonus_value = new_bonus_value < 0 ? 0 : new_bonus_value;

                            card_nominal = new card_nominal();
                            card_nominal.card_id = (long)sum_bonus_for_card_item.card_id;
                            card_nominal.date_beg = today;
                            card_nominal.date_end = null;
                            card_nominal.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                            card_nominal.unit_value = new_bonus_value;
                            card_nominal.crt_date = now;
                            card_nominal.crt_user = "scheduler";
                            dbContext.card_nominal.Add(card_nominal);

                            cnt++;
                        }

                        card_type.reset_date = now;
                    }
                }
                #endregion

                // part 2 - только для типов карт с признаком is_reset_without_sales == 1
                #region
                card_type_list = dbContext.card_type.Where(ss => ss.reset_interval_bonus_value.HasValue && ss.reset_interval_bonus_value > 0 && ss.is_reset_without_sales == 1).ToList();
                if ((card_type_list != null) && (card_type_list.Count > 0))
                {
                    foreach (var card_type in card_type_list)
                    {
                        date_beg_for_reset = card_type.reset_date;

                        reset_interval_bonus_value = (int)card_type.reset_interval_bonus_value;
                        date_end_for_reset = today.AddMonths(-reset_interval_bonus_value);
                        
                        var cards = dbContext.card.Where(ss => ss.curr_card_type_id == card_type.card_type_id).ToList();
                        if ((cards == null) || (cards.Count <= 0))
                            continue;

                        foreach (var cards_item in cards)
                        {
                            var last_transaction = dbContext.card_transaction.Where(ss =>
                                    ss.card_id == cards_item.card_id
                                    && ss.trans_kind == (short) Enums.CardTransactionType.CALC
                                    && ss.status == (int) Enums.CardTransactionStatus.DONE)
                                .OrderByDescending(ss => ss.date_check)
                                .FirstOrDefault();

                            //if ((last_transaction == null) || (last_transaction.date_check <= date_end_for_reset))
                            if ((last_transaction != null) && (last_transaction.date_check <= date_end_for_reset))
                            {
                                card_nominal_list = dbContext.card_nominal
                                    .Where(ss => ss.card_id == cards_item.card_id 
                                                && ss.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_VALUE 
                                                 && !ss.date_end.HasValue)
                                    .ToList();
                                if ((card_nominal_list != null) && (card_nominal_list.Count > 0))
                                {
                                    foreach (var card_nominal_list_item in card_nominal_list)
                                    {                                        
                                        card_nominal_list_item.date_end = today;
                                    }
                                }

                                new_bonus_value = 0;
                                card_nominal = new card_nominal();
                                card_nominal.card_id = cards_item.card_id;
                                card_nominal.date_beg = today;
                                card_nominal.date_end = null;
                                card_nominal.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                                card_nominal.unit_value = new_bonus_value;
                                card_nominal.crt_date = now;
                                card_nominal.crt_user = "scheduler";
                                dbContext.card_nominal.Add(card_nominal);                                

                                card_type.reset_date = now;

                                cnt++;
                            }
                        }                      

                    }
                }
                #endregion

                dbContext.SaveChanges();
            }                

            if (cnt <= 0)
                Loggly.InsertLog_Discount("[ResetExpiredBonus] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);
            else
                Loggly.InsertLog_Discount("[ResetExpiredBonus] Сброшены бонусы, карт: " + cnt.ToString(), (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);

            return true;
        }

        #endregion

        #region ChangeCampaignState

        private bool xChangeCampaignState(string s1)
        {
            DateTime now = DateTime.Now;
            if (s1 != "iguana")
            {
                Loggly.InsertLog_Discount("[ChangeCampaignState] " + s1, (long)Enums.LogEventType.ERROR, null, "scheduler", null, now, null);
                return false;
            }

            bool somethingChanged = false;
            int campaign_finished_cnt = 0;
            int campaign_started_cnt = 0;
            DateTime today = DateTime.Today;
            List<campaign> campaign_list = null;
            using (var context = new AuMainDb())
            {
                // переводим закончившиеся акции в статус "завершена"
                campaign_list = context.campaign.Where(ss => ss.date_end < today && ss.state != (int)Enums.CampaignStateEnum.FINISHED).ToList();
                if ((campaign_list != null) && (campaign_list.Count > 0))
                {
                    somethingChanged = true;
                    foreach (var campaign in campaign_list)
                    {
                        campaign.upd_date = now;
                        campaign.upd_user = "scheduler";
                        campaign.state = (int)Enums.CampaignStateEnum.FINISHED;
                        campaign.mess = null;
                        campaign_finished_cnt++;
                    }
                }

                // переводим начавшиеся акции в статус "активна"
                campaign_list = context.campaign.Where(ss => ss.date_beg >= today && ss.date_end >= today && ss.state == (int)Enums.CampaignStateEnum.READY).ToList();
                if ((campaign_list != null) && (campaign_list.Count > 0))
                {
                    somethingChanged = true;
                    foreach (var campaign in campaign_list)
                    {
                        campaign.upd_date = now;
                        campaign.upd_user = "scheduler";
                        campaign.state = (int)Enums.CampaignStateEnum.ACTIVE;
                        campaign.mess = null;
                        campaign_started_cnt++;
                    }
                }

                if (somethingChanged)
                {
                    context.SaveChanges();
                    Loggly.InsertLog_Discount("[ChangeCampaignState] Закончено акций: " + campaign_finished_cnt.ToString() + ", начато акций: " + campaign_started_cnt.ToString(), (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);
                }
                else
                {
                    Loggly.InsertLog_Discount("[ChangeCampaignState] Ничего не изменено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, now, null);
                }

                return true;
            }
        }

        #endregion
                    
    }        
}
