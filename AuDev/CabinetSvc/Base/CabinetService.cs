﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Newtonsoft.Json;
using Npgsql;
using NpgsqlTypes;
using AuDev.Common.Util;

namespace CabinetMvc
{
    public partial class CabinetService : ICabinetService
    {

        #region CreateErrCabinetBase
        public T CreateErrCabinetBase<T>(Exception e)
            where T : CabinetBase, new()
        {
            //T res = Activator.CreateInstance<T>();
            T res = new T();
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                )
                );
            return res;
        }
        #endregion

        #region CardStuffRefresh (Nightly methods)

        // обновление даных по картам
        public bool CardStuffRefresh(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // метод ищет все карты с типом, на котором есть месячные лимиты продаж, смотрит, не наступил ли следующий месячный интервал, и если наступил - удаляет все старые лимиты из card_ext1 и все карты со статусом "временно приостановлена" переводит в "активна"
            // + сбрасывает накопленные суммы
            #region ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus
            try
            {
                xChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            // метод ищет все карты с типом, на котором есть дневные лимиты бонусов, и удаляет все старые лимиты из card_ext1
            #region ChangeBonusDailyPeriod
            try
            {
                xChangeBonusDailyPeriod(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[ChangeBonusDailyPeriod] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            // метод переносит все значения из card_nominal_inactive, у которых прошел срок active_date, в card_nominal
            #region MakeInactiveNominalActive
            try
            {
                xMakeInactiveNominalActive(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[MakeInactiveNominalActive] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            // метод проставляет картам заданных организаций и типов заданный процент скидки
            #region UpdateCardDiscountPercentVip
            try
            {
                xUpdateCardDiscountPercentVip(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[UpdateCardDiscountPercentVip] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            // метод сбрасывает накопленные бонусы с истекшим сроком действия
            #region ResetExpiredBonus
            try
            {
                xResetExpiredBonus(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[ResetExpiredBonus] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            // метод удаляет все транзакции со статусом Активна со сроком давности час и ранее
            #region DeleteActiveCardTrans
            try
            {                
                xDeleteActiveCardTrans(s1);
                // Loggly.InsertLog_Discount("[DeleteActiveCardTrans] Временно отключено", (long)Enums.LogEventType.SCHEDULER, null, "scheduler", null, log_date_beg, null);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[DeleteActiveCardTrans] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        public bool ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // [часть метода CardStuffRefresh]
            // метод ищет все карты с типом, на котором есть месячные лимиты продаж, смотрит, не наступил ли следующий месячный интервал, и если наступил - удаляет все старые лимиты из card_ext1 и все карты со статусом "временно приостановлена" переводит в "активна"
            #region ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus
            try
            {
                xChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        public bool ChangeBonusDailyPeriod(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // [часть метода CardStuffRefresh]
            // метод ищет все карты с типом, на котором есть дневные лимиты бонусов, и удаляет все старые лимиты из card_ext1
            #region ChangeBonusDailyPeriod
            try
            {
                xChangeBonusDailyPeriod(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[ChangeBonusDailyPeriod] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        public bool MakeInactiveNominalActive(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // [часть метода CardStuffRefresh]
            // метод переносит все значения из card_nominal_inactive, у которых прошел срок active_date, в card_nominal
            #region MakeInactiveNominalActive
            try
            {
                xMakeInactiveNominalActive(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[MakeInactiveNominalActive] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        public bool UpdateCardDiscountPercentVip(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // [часть метода CardStuffRefresh]
            // метод проставляет картам заданных организаций и типов заданный процент скидки
            #region UpdateCardDiscountPercentVip
            try
            {
                xUpdateCardDiscountPercentVip(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[UpdateCardDiscountPercentVip] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        public bool ResetExpiredBonus(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // [часть метода CardStuffRefresh]
            // метод сбрасывает накопленные бонусы с истекшим сроком действия
            #region ResetExpiredBonus
            try
            {
                xResetExpiredBonus(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[ResetExpiredBonus] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        public bool DeleteActiveCardTrans(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // [часть метода CardStuffRefresh]
            // метод удаляет все транзакции со статусом Активна со сроком давности час и ранее
            #region DeleteActiveCardTrans
            try
            {
                xDeleteActiveCardTrans(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[DeleteActiveCardTrans] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        #endregion

        #region UpdateSaleSummary

        // обновление статистики продаж
        public bool UpdateSaleSummary(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // метод обновляет статистику продаж по организациям - за все время, за последний месяц, за вчерашние сутки
            #region UpdateSaleSummary
            try
            {
                xUpdateSaleSummary(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[UpdateSaleSummary] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        #endregion       

        #region StorageTrashClear

        // очистка корзины в Хранилище
        public bool StorageTrashClear(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // очистка корзины в Хранилище
            #region StorageTrashClear
            try
            {
                xStorageTrashClear(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Cabinet("[StorageTrashClear] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        #endregion

        #region ChangeCampaignState

        // перевод маркетинговых акции в статус "активна" и из статуса "активна"
        public bool ChangeCampaignState(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // перевод маркетинговых акции в статус "активна" и из статуса "активна"
            #region ChangeCampaignState
            try
            {
                xChangeCampaignState(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[ChangeCampaignState] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        #endregion

        #region TerminateExchangeDoc

        // удаление документов (exchange_doc) с наступившей датой terminate_date
        public bool TerminateExchangeDoc(string s1)
        {
            DateTime log_date_beg = DateTime.Now;
            // метод удаление документы (exchange_doc) с наступившей датой terminate_date
            #region TerminateExchangeDoc
            try
            {
                xTerminateExchangeDoc(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_ExchangeDoc("[TerminateExchangeDoc] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "scheduler", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        #endregion

        #region Batch operations

        // добавление сумм и номиналов на карты, удаление карт
        public bool RunBatch_Operations(string s1, long batch_id, string user_name, bool new_thread)
        {
            DateTime log_date_beg = DateTime.Now;
            #region RunBatch_Operations
            try
            {
                xRunBatch_Operations(s1, batch_id, user_name, new_thread);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[RunBatch_Operations] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "batchworker", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        // массовое создание карт
        public bool RunBatch_Add(string s1, long batch_id, string user_name, bool new_thread)
        {
            DateTime log_date_beg = DateTime.Now;
            #region RunBatch_Add
            try
            {
                xRunBatch_Add(s1, batch_id, user_name, new_thread);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Discount("[RunBatch_Add] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, null, "batchworker", null, log_date_beg, null);
                return false;
            }
            #endregion

            return true;
        }

        #endregion

        #region Notify oprerations

        #region ICQ

        public Notify GetLastNotify(string s1)
        {
            try
            {
                return xGetLastNotify(s1, Enums.NotifyTransportTypeEnum.ICQ);
            }
            catch (Exception ex)
            {
                return CreateErrCabinetBase<Notify>(ex);
            } 
        }
        
        public NotifyList GetNotifyList(string s1, int last_id)
        {            
            try
            {
                return xGetNotifyList(s1, last_id, Enums.NotifyTransportTypeEnum.ICQ);
            }
            catch (Exception ex)
            {
                return CreateErrCabinetBase<NotifyList>(ex);
            }
        }

        public ServiceOperationResult UpdateNotifyList(string s1, string id_list)
        {
            try
            {
                return xUpdateNotifyList(s1, id_list, Enums.NotifyTransportTypeEnum.ICQ);
            }
            catch (Exception ex)
            {
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, };
            }
        }

        #endregion

        #region Email

        public Notify GetLastNotify_Email(string s1)
        {
            try
            {
                return xGetLastNotify(s1, Enums.NotifyTransportTypeEnum.EMAIL);
            }
            catch (Exception ex)
            {
                return CreateErrCabinetBase<Notify>(ex);
            }
        }

        public NotifyList GetNotifyList_Email(string s1, int last_id)
        {
            try
            {
                return xGetNotifyList(s1, last_id, Enums.NotifyTransportTypeEnum.EMAIL);
            }
            catch (Exception ex)
            {
                return CreateErrCabinetBase<NotifyList>(ex);
            }
        }

        public ServiceOperationResult UpdateNotifyList_Email(string s1, string id_list)
        {
            try
            {
                return xUpdateNotifyList(s1, id_list, Enums.NotifyTransportTypeEnum.EMAIL);
            }
            catch (Exception ex)
            {
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, };
            }
        }

        #endregion

        #endregion

        #region VacuumAnalyze

        public bool VacuumAnalyze(string s1)
        {
            try
            {
                return xVacuumAnalyze(s1);
            }
            catch (Exception ex)
            {                
                Loggly.InsertLog_Cabinet("[VacuumAnalyze] " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, "scheduler", null, DateTime.Now, null);
                return false;
            }
        }

        #endregion

        #region DelOldData

        public bool DelOldData(string s1)
        {
            try
            {
                return xDelOldData(s1);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Cabinet("[DelOldData] " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, "scheduler", null, DateTime.Now, null);
                return false;
            }
        }

        #endregion

        #region JSON Support

        public void GetOptions()
        {
            // Заголовки обработаются в EnableCorsMessageInspector 
        }

        #endregion

    }
        
}
