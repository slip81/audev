﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace CabinetMvc
{
    #region CabinetBase

    [DataContract]
    public class CabinetBase
    {
        public CabinetBase()
        {
            //            
        }

        [DataMember]
        public ErrInfo error { get; set; }
    }

    #endregion

    #region ErrInfo

    [DataContract]
    public class ErrInfo
    {
        public ErrInfo(long? id, string message = "")
        {
            Id = id;
            Message = message;
        }

        public ErrInfo(long? id, Exception e)
        {
            Id = id;
            if (e == null)
                return;

            Message = e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                );

        }

        [DataMember]
        public long? Id { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    #endregion

    #region ServiceOperationResult

    [DataContract]
    public class ServiceOperationResult
    {
        public ServiceOperationResult()
        {
        }

        public ServiceOperationResult(long _id, ErrInfo _error)
        {
            Id = _id;
            error = _error;
            Message = "";
        }

        public ServiceOperationResult(long _id, Exception _e)
        {
            Id = _id;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
            Message = "";
        }

        public ServiceOperationResult(ErrInfo _error)
        {
            Id = (_error == null) ? (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION : (long)_error.Id;
            error = _error;
            Message = "";
        }

        public ServiceOperationResult(Exception _e)
        {
            Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
            Message = "";
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    #endregion
}
