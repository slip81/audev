﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.EntityClient;
using NodaTime;
using NodaTime.Text;

namespace CabinetMvc
{
    public static class CabinetSvcUtil
    {
        public static DateTime KendoFilterDateToLocal(DateTime filter_date)
        {
            var pattern = InstantPattern.CreateWithInvariantCulture("ddMMyyHHmmss");
            var parseResult = pattern.Parse(filter_date.ToString("ddMMyyHHmmss"));
            if (!parseResult.Success)
                throw new Exception("...whatever...");
            var instant = parseResult.Value;
            var timeZone = DateTimeZoneProviders.Tzdb["Asia/Yekaterinburg"];
            var zonedDateTime = instant.InZone(timeZone);

            return zonedDateTime.ToDateTimeUnspecified().Date;
        }
        
        public static string GetCardDbConnectionString()
        {
            string entityConnectionString = ConfigurationManager.ConnectionStrings["AuMainDb"].ConnectionString;
            return new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;
        }
    }
}
