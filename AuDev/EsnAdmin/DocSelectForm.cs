﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using EsnAdmin.ViewModel;

namespace EsnAdmin
{
    public partial class DocSelectForm : RadFormExt
    {
        public Doc SelectedDoc
        {
            get
            {
                return selectedDoc;
            }
        }
        private Doc selectedDoc;

        public DocSelectForm()
        {
            InitializeComponent();
            //
            lblError.Text = "";
            selectedDoc = null;
        }

        private void DocSelectForm_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                GridDoc_bind();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void GridDoc_bind()
        {            
            var docs = DocService.GetDocs(3);
            gridDoc.DataSource = docs;
            if (docs.Count > 0)
            {                
                gridDoc.ClearSelection();
                //gridDoc.Rows[docs.Count - 1].IsSelected = true;
                //gridDoc.Rows[docs.Count - 1].IsCurrent = true;
                gridDoc.Rows[0].IsSelected = true;
                gridDoc.Rows[0].IsCurrent = true;
            }
        }

        private void DocSelectForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            lblError.Text = "";
            selectedDoc = null;
            if (this.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                if ((gridDoc.SelectedRows == null) || (gridDoc.SelectedRows.Count <= 0))
                {
                    lblError.Text = "Не выбран документ";
                    e.Cancel = true;
                    return;
                }

                selectedDoc = (Doc)gridDoc.SelectedRows[0].DataBoundItem;
                if (selectedDoc == null)
                {
                    lblError.Text = "Не выбран документ";
                    e.Cancel = true;
                    return;
                }
            }
        }

    }
}
