﻿namespace EsnAdmin
{
    partial class AutoMatchParamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.btnAutoMatchCancel = new Telerik.WinControls.UI.RadButton();
            this.btnAutoMatchStart = new Telerik.WinControls.UI.RadButton();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.edAutoMatchPartMfrPercent = new Telerik.WinControls.UI.RadSpinEditor();
            this.edAutoMatchPartNamePercent = new Telerik.WinControls.UI.RadSpinEditor();
            this.edAutoMatchPartBarcodePercent = new Telerik.WinControls.UI.RadSpinEditor();
            this.chbAutoMatchPartSumPercent = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.edAutoMatchPartPercent = new Telerik.WinControls.UI.RadSpinEditor();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.chbAutoMatchPartApproved = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoMatchPart = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoMatchPartBarcode = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoMatchPartMfr = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoMatchPartName = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.chbAutoMatchFullApproved = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoMatchFullMfr = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoMatchFull = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoMatchFullBarcode = new Telerik.WinControls.UI.RadCheckBox();
            this.chbAutoMatchFullName = new Telerik.WinControls.UI.RadCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnAutoMatchCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAutoMatchStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edAutoMatchPartMfrPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edAutoMatchPartNamePercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edAutoMatchPartBarcodePercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartSumPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edAutoMatchPartPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartApproved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartBarcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartMfr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFullApproved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFullMfr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFullBarcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFullName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.Gainsboro;
            this.radPanel1.Controls.Add(this.btnAutoMatchCancel);
            this.radPanel1.Controls.Add(this.btnAutoMatchStart);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 321);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(495, 60);
            this.radPanel1.TabIndex = 0;
            // 
            // btnAutoMatchCancel
            // 
            this.btnAutoMatchCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAutoMatchCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAutoMatchCancel.Location = new System.Drawing.Point(373, 17);
            this.btnAutoMatchCancel.Name = "btnAutoMatchCancel";
            this.btnAutoMatchCancel.Size = new System.Drawing.Size(110, 24);
            this.btnAutoMatchCancel.TabIndex = 1;
            this.btnAutoMatchCancel.Text = "Отмена";
            // 
            // btnAutoMatchStart
            // 
            this.btnAutoMatchStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAutoMatchStart.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAutoMatchStart.Location = new System.Drawing.Point(245, 17);
            this.btnAutoMatchStart.Name = "btnAutoMatchStart";
            this.btnAutoMatchStart.Size = new System.Drawing.Size(110, 24);
            this.btnAutoMatchStart.TabIndex = 0;
            this.btnAutoMatchStart.Text = "Запуск";
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.Gainsboro;
            this.radPanel2.Controls.Add(this.radGroupBox2);
            this.radPanel2.Controls.Add(this.radGroupBox1);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(0, 0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(495, 321);
            this.radPanel2.TabIndex = 1;
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.edAutoMatchPartMfrPercent);
            this.radGroupBox2.Controls.Add(this.edAutoMatchPartNamePercent);
            this.radGroupBox2.Controls.Add(this.edAutoMatchPartBarcodePercent);
            this.radGroupBox2.Controls.Add(this.chbAutoMatchPartSumPercent);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.edAutoMatchPartPercent);
            this.radGroupBox2.Controls.Add(this.radLabel1);
            this.radGroupBox2.Controls.Add(this.chbAutoMatchPartApproved);
            this.radGroupBox2.Controls.Add(this.chbAutoMatchPart);
            this.radGroupBox2.Controls.Add(this.chbAutoMatchPartBarcode);
            this.radGroupBox2.Controls.Add(this.chbAutoMatchPartMfr);
            this.radGroupBox2.Controls.Add(this.chbAutoMatchPartName);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGroupBox2.HeaderText = "Частичное совпадение";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 120);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(495, 201);
            this.radGroupBox2.TabIndex = 9;
            this.radGroupBox2.Text = "Частичное совпадение";
            // 
            // edAutoMatchPartMfrPercent
            // 
            this.edAutoMatchPartMfrPercent.DecimalPlaces = 2;
            this.edAutoMatchPartMfrPercent.Location = new System.Drawing.Point(324, 104);
            this.edAutoMatchPartMfrPercent.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.edAutoMatchPartMfrPercent.Name = "edAutoMatchPartMfrPercent";
            this.edAutoMatchPartMfrPercent.Size = new System.Drawing.Size(53, 20);
            this.edAutoMatchPartMfrPercent.TabIndex = 15;
            this.edAutoMatchPartMfrPercent.TabStop = false;
            this.edAutoMatchPartMfrPercent.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // edAutoMatchPartNamePercent
            // 
            this.edAutoMatchPartNamePercent.DecimalPlaces = 2;
            this.edAutoMatchPartNamePercent.Location = new System.Drawing.Point(201, 104);
            this.edAutoMatchPartNamePercent.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.edAutoMatchPartNamePercent.Name = "edAutoMatchPartNamePercent";
            this.edAutoMatchPartNamePercent.Size = new System.Drawing.Size(53, 20);
            this.edAutoMatchPartNamePercent.TabIndex = 14;
            this.edAutoMatchPartNamePercent.TabStop = false;
            this.edAutoMatchPartNamePercent.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // edAutoMatchPartBarcodePercent
            // 
            this.edAutoMatchPartBarcodePercent.DecimalPlaces = 2;
            this.edAutoMatchPartBarcodePercent.Location = new System.Drawing.Point(67, 104);
            this.edAutoMatchPartBarcodePercent.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.edAutoMatchPartBarcodePercent.Name = "edAutoMatchPartBarcodePercent";
            this.edAutoMatchPartBarcodePercent.Size = new System.Drawing.Size(53, 20);
            this.edAutoMatchPartBarcodePercent.TabIndex = 13;
            this.edAutoMatchPartBarcodePercent.TabStop = false;
            this.edAutoMatchPartBarcodePercent.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // chbAutoMatchPartSumPercent
            // 
            this.chbAutoMatchPartSumPercent.Location = new System.Drawing.Point(45, 142);
            this.chbAutoMatchPartSumPercent.Name = "chbAutoMatchPartSumPercent";
            this.chbAutoMatchPartSumPercent.Size = new System.Drawing.Size(201, 18);
            this.chbAutoMatchPartSumPercent.TabIndex = 12;
            this.chbAutoMatchPartSumPercent.Text = "использовать суммарный процент";
            this.chbAutoMatchPartSumPercent.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbAutoMatchPartSumPercent_ToggleStateChanged);
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(397, 122);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(77, 18);
            this.radLabel2.TabIndex = 11;
            this.radLabel2.Text = "(от 30 до 100)";
            // 
            // edAutoMatchPartPercent
            // 
            this.edAutoMatchPartPercent.DecimalPlaces = 2;
            this.edAutoMatchPartPercent.Enabled = false;
            this.edAutoMatchPartPercent.Location = new System.Drawing.Point(324, 140);
            this.edAutoMatchPartPercent.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.edAutoMatchPartPercent.Name = "edAutoMatchPartPercent";
            this.edAutoMatchPartPercent.Size = new System.Drawing.Size(53, 20);
            this.edAutoMatchPartPercent.TabIndex = 10;
            this.edAutoMatchPartPercent.TabStop = false;
            this.edAutoMatchPartPercent.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(45, 80);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(194, 18);
            this.radLabel1.TabIndex = 9;
            this.radLabel1.Text = "минимальный процент совпадения:";
            // 
            // chbAutoMatchPartApproved
            // 
            this.chbAutoMatchPartApproved.Location = new System.Drawing.Point(45, 177);
            this.chbAutoMatchPartApproved.Name = "chbAutoMatchPartApproved";
            this.chbAutoMatchPartApproved.Size = new System.Drawing.Size(286, 18);
            this.chbAutoMatchPartApproved.TabIndex = 8;
            this.chbAutoMatchPartApproved.Text = "Сопоставлять заново  уже сопоставленные товары";
            this.chbAutoMatchPartApproved.Visible = false;
            // 
            // chbAutoMatchPart
            // 
            this.chbAutoMatchPart.Location = new System.Drawing.Point(19, 30);
            this.chbAutoMatchPart.Name = "chbAutoMatchPart";
            this.chbAutoMatchPart.Size = new System.Drawing.Size(242, 18);
            this.chbAutoMatchPart.TabIndex = 4;
            this.chbAutoMatchPart.Text = "Сопоставлять по частичному совпадению:";
            this.chbAutoMatchPart.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbAutoMatchPart_ToggleStateChanged);
            // 
            // chbAutoMatchPartBarcode
            // 
            this.chbAutoMatchPartBarcode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAutoMatchPartBarcode.Location = new System.Drawing.Point(45, 56);
            this.chbAutoMatchPartBarcode.Name = "chbAutoMatchPartBarcode";
            this.chbAutoMatchPartBarcode.Size = new System.Drawing.Size(75, 18);
            this.chbAutoMatchPartBarcode.TabIndex = 5;
            this.chbAutoMatchPartBarcode.Text = "штрих-код";
            this.chbAutoMatchPartBarcode.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chbAutoMatchPartBarcode.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbAutoMatchPartBarcode_ToggleStateChanged);
            // 
            // chbAutoMatchPartMfr
            // 
            this.chbAutoMatchPartMfr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAutoMatchPartMfr.Location = new System.Drawing.Point(278, 56);
            this.chbAutoMatchPartMfr.Name = "chbAutoMatchPartMfr";
            this.chbAutoMatchPartMfr.Size = new System.Drawing.Size(99, 18);
            this.chbAutoMatchPartMfr.TabIndex = 7;
            this.chbAutoMatchPartMfr.Text = "производитель";
            this.chbAutoMatchPartMfr.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chbAutoMatchPartMfr.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbAutoMatchPartMfr_ToggleStateChanged);
            // 
            // chbAutoMatchPartName
            // 
            this.chbAutoMatchPartName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAutoMatchPartName.Location = new System.Drawing.Point(157, 56);
            this.chbAutoMatchPartName.Name = "chbAutoMatchPartName";
            this.chbAutoMatchPartName.Size = new System.Drawing.Size(97, 18);
            this.chbAutoMatchPartName.TabIndex = 6;
            this.chbAutoMatchPartName.Text = "наименование";
            this.chbAutoMatchPartName.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chbAutoMatchPartName.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbAutoMatchPartName_ToggleStateChanged);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.chbAutoMatchFullApproved);
            this.radGroupBox1.Controls.Add(this.chbAutoMatchFullMfr);
            this.radGroupBox1.Controls.Add(this.chbAutoMatchFull);
            this.radGroupBox1.Controls.Add(this.chbAutoMatchFullBarcode);
            this.radGroupBox1.Controls.Add(this.chbAutoMatchFullName);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "Полное совпадение";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(495, 120);
            this.radGroupBox1.TabIndex = 8;
            this.radGroupBox1.Text = "Полное совпадение";
            // 
            // chbAutoMatchFullApproved
            // 
            this.chbAutoMatchFullApproved.Location = new System.Drawing.Point(45, 87);
            this.chbAutoMatchFullApproved.Name = "chbAutoMatchFullApproved";
            this.chbAutoMatchFullApproved.Size = new System.Drawing.Size(286, 18);
            this.chbAutoMatchFullApproved.TabIndex = 4;
            this.chbAutoMatchFullApproved.Text = "Сопоставлять заново  уже сопоставленные товары";
            this.chbAutoMatchFullApproved.Visible = false;
            // 
            // chbAutoMatchFullMfr
            // 
            this.chbAutoMatchFullMfr.Location = new System.Drawing.Point(278, 46);
            this.chbAutoMatchFullMfr.Name = "chbAutoMatchFullMfr";
            this.chbAutoMatchFullMfr.Size = new System.Drawing.Size(99, 18);
            this.chbAutoMatchFullMfr.TabIndex = 3;
            this.chbAutoMatchFullMfr.Text = "производитель";
            // 
            // chbAutoMatchFull
            // 
            this.chbAutoMatchFull.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAutoMatchFull.Location = new System.Drawing.Point(19, 22);
            this.chbAutoMatchFull.Name = "chbAutoMatchFull";
            this.chbAutoMatchFull.Size = new System.Drawing.Size(226, 18);
            this.chbAutoMatchFull.TabIndex = 0;
            this.chbAutoMatchFull.Text = "Сопоставлять по полному совпадению:";
            this.chbAutoMatchFull.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            this.chbAutoMatchFull.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbAutoMatchFull_ToggleStateChanged);
            // 
            // chbAutoMatchFullBarcode
            // 
            this.chbAutoMatchFullBarcode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAutoMatchFullBarcode.Location = new System.Drawing.Point(45, 46);
            this.chbAutoMatchFullBarcode.Name = "chbAutoMatchFullBarcode";
            this.chbAutoMatchFullBarcode.Size = new System.Drawing.Size(75, 18);
            this.chbAutoMatchFullBarcode.TabIndex = 1;
            this.chbAutoMatchFullBarcode.Text = "штрих-код";
            this.chbAutoMatchFullBarcode.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // chbAutoMatchFullName
            // 
            this.chbAutoMatchFullName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAutoMatchFullName.Location = new System.Drawing.Point(157, 46);
            this.chbAutoMatchFullName.Name = "chbAutoMatchFullName";
            this.chbAutoMatchFullName.Size = new System.Drawing.Size(97, 18);
            this.chbAutoMatchFullName.TabIndex = 2;
            this.chbAutoMatchFullName.Text = "наименование";
            this.chbAutoMatchFullName.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // AutoMatchParamForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 381);
            this.Controls.Add(this.radPanel2);
            this.Controls.Add(this.radPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AutoMatchParamForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Параметры сопоставления";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AutoMatchParamForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnAutoMatchCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAutoMatchStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edAutoMatchPartMfrPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edAutoMatchPartNamePercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edAutoMatchPartBarcodePercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartSumPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edAutoMatchPartPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartApproved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartBarcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartMfr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchPartName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFullApproved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFullMfr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFullBarcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbAutoMatchFullName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton btnAutoMatchCancel;
        private Telerik.WinControls.UI.RadButton btnAutoMatchStart;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchPartMfr;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchPartName;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchPartBarcode;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchPart;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchFullMfr;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchFullName;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchFullBarcode;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchFull;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadSpinEditor edAutoMatchPartPercent;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchPartApproved;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchFullApproved;
        private Telerik.WinControls.UI.RadSpinEditor edAutoMatchPartMfrPercent;
        private Telerik.WinControls.UI.RadSpinEditor edAutoMatchPartNamePercent;
        private Telerik.WinControls.UI.RadSpinEditor edAutoMatchPartBarcodePercent;
        private Telerik.WinControls.UI.RadCheckBox chbAutoMatchPartSumPercent;
    }
}
