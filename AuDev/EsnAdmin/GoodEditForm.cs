﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace EsnAdmin
{
    public partial class GoodEditForm : RadFormExt
    {
        private bool is_add = true;
        private int? curr_good_id = null;

        public string new_product_name = "";
        public int? new_apply_group_id = null;
        public string new_good_name = "";
        public int new_mfr_id = -1;
        public string new_barcode = "";
        public int? new_source_partner_id = null;

        public GoodEditForm()
        {
            InitializeComponent();
            //
            InitForm();
        }

        private void InitForm()
        {
            ddlApplyGroup.DataSource = null;
            ddlApplyGroup.DataSource = ApplyGroupService.GetApplyGroups().apply_groups;
            ddlApplyGroup.DisplayMember = "apply_group_name";
            ddlApplyGroup.ValueMember = "apply_group_id";
            ddlApplyGroup.SelectedIndex = -1;
            //
            ddlMfr.DataSource = null;
            ddlMfr.DataSource = MfrService.GetManufacturers().manufacturers;
            ddlMfr.DisplayMember = "mfr_name";
            ddlMfr.ValueMember = "mfr_id";
            ddlMfr.SelectedIndex = -1;
            //
            ddlSupplier.DataSource = null;
            ddlSupplier.DataSource = SupplierService.GetSuppliers();
            ddlSupplier.DisplayMember = "partner_name";
            ddlSupplier.ValueMember = "partner_id";
            ddlSupplier.SelectedIndex = -1;
        }

        public void InitForm_Add()
        {
            is_add = true;
            curr_good_id = null;
            //
            ClearForm();
            //
            edProductName.Enabled = true;
            ddlApplyGroup.Enabled = true;
        }

        public void InitForm_AddSyn(string product_name, int? apply_group_id)
        {
            InitForm_Add();
            //
            edProductName.Text = product_name;
            ddlApplyGroup.SelectedValue = apply_group_id;
            edProductName.Enabled = false;
            ddlApplyGroup.Enabled = false;
        }

        public void InitForm_Edit(int good_id, string product_name, int? apply_group_id, string good_name, int? mfr_id, string barcode, int? source_partner_id)
        {
            is_add = false;
            curr_good_id = good_id;
            //
            edProductName.Text = product_name;
            ddlApplyGroup.SelectedValue = apply_group_id;
            edGoodName.Text = good_name;
            ddlMfr.SelectedValue = mfr_id;            
            edBarcode.Text = barcode;
            ddlSupplier.SelectedValue = source_partner_id;
        }

        private void ClearForm()
        {
            edProductName.Text = "";
            ddlApplyGroup.SelectedIndex = -1;
            edGoodName.Text = "";
            ddlMfr.SelectedIndex = -1;            
            edBarcode.Text = "";
            ddlSupplier.SelectedIndex = -1;
        }

        private void GoodEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                if (String.IsNullOrWhiteSpace(edProductName.Text))
                {
                    e.Cancel = true;
                    MessageBox.Show("Не задано наименование препарата");
                    return;
                }

                if (String.IsNullOrWhiteSpace(edGoodName.Text))
                {
                    e.Cancel = true;
                    MessageBox.Show("Не задано наименование товара");
                    return;
                }

                if (ddlMfr.SelectedIndex < 0)
                {
                    e.Cancel = true;
                    MessageBox.Show("Не задан изготовитель");
                    return;
                }

                new_product_name = edProductName.Text;
                new_apply_group_id = (int?)ddlApplyGroup.SelectedValue;
                new_good_name = edGoodName.Text;
                new_mfr_id = (int)ddlMfr.SelectedValue;
                new_barcode = edBarcode.Text;
                new_source_partner_id = (int?)ddlSupplier.SelectedValue;
            }
        }
    }
}
