﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using Telerik.WinControls;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using Telerik.WinControls.Data;
using Telerik.WinControls.UI;
using Telerik.WinControls.UI.Localization;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Util;


namespace EsnAdmin
{
    public partial class MainForm : RadFormExt
    {
        private Dictionary<EsnAdminEnums.FormEnum, Tuple<RadFormExt, int>> usedForms { get; set; }
        private int formIndex { get; set; }

        public MainForm()
        {
            InitializeComponent();
            //
            // !!!
            Thread.Sleep(3500);
            //
            RadGridLocalizationProvider.CurrentProvider = new RadGridLocalization();
            Application.ApplicationExit += Application_ApplicationExit;
            //
            usedForms = new Dictionary<EsnAdminEnums.FormEnum, Tuple<RadFormExt, int>>();
            formIndex = 0;
            //
            InitThemeDdl();
        }

        private void InitThemeDdl()
        {
            ddlTheme.ComboBoxElement.DataSource = null;

            List<AppTheme> appThemeList = new List<AppTheme>()
            {
                new AppTheme() { Value = "Default", Name = "Default", },
                new AppTheme() { Value = "Aqua", Name = "Aqua", },
                new AppTheme() { Value = "Breeze", Name = "Breeze", },
                new AppTheme() { Value = "HighContrastBlack", Name = "HighContrastBlack", },
                new AppTheme() { Value = "Office2007Black", Name = "Office2007Black", },
                new AppTheme() { Value = "Office2007Silver", Name = "Office2007Silver", },
                new AppTheme() { Value = "Office2010Black", Name = "Office2010Black", },
                new AppTheme() { Value = "Office2010Blue", Name = "Office2010Blue", },
                new AppTheme() { Value = "Office2010Silver", Name = "Office2010Silver", },
                new AppTheme() { Value = "Office2013Dark", Name = "Office2013Dark", },
                new AppTheme() { Value = "Office2013Light", Name = "Office2013Light", },
                new AppTheme() { Value = "TelerikMetro", Name = "TelerikMetro", },
                new AppTheme() { Value = "TelerikMetroBlue", Name = "TelerikMetroBlue", },
                new AppTheme() { Value = "TelerikMetroTouch", Name = "TelerikMetroTouch", },
                new AppTheme() { Value = "VisualStudio2012Dark", Name = "VisualStudio2012Dark", },
                new AppTheme() { Value = "VisualStudio2012Light", Name = "VisualStudio2012Light", },
                new AppTheme() { Value = "Windows7", Name = "Windows7", },
                new AppTheme() { Value = "Windows8", Name = "Windows8", },
                
            };

            ddlTheme.ComboBoxElement.DataSource = appThemeList;
            ddlTheme.ComboBoxElement.ValueMember = "Value";
            ddlTheme.ComboBoxElement.DisplayMember = "Name";
            ddlTheme.ComboBoxElement.SelectedValue = "Default";
            
        }

        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            //appManager.Dispose();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //
        }

        private void MainForm_MdiChildActivate(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild == null)
                tabForms.Visible = false;
            else
            {
                this.ActiveMdiChild.WindowState = FormWindowState.Maximized;

                if (this.ActiveMdiChild.Tag == null)
                {
                    RadPageViewPage tp = new RadPageViewPage(this.ActiveMdiChild.Text);
                    tp.Tag = this.ActiveMdiChild;
                    tp.Parent = tabForms;
                    tabForms.SelectedPage = tp;

                    this.ActiveMdiChild.Tag = tp;
                    this.ActiveMdiChild.FormClosed += new FormClosedEventHandler(ActiveMdiChild_FormClosed);
                }
                else
                {
                    tabForms.SelectedPage = tabForms.Pages[formIndex];
                }

                if (!tabForms.Visible)
                    tabForms.Visible = true;
            }
        }

        private void tabForms_SelectedPageChanged(object sender, EventArgs e)
        {
            if ((tabForms.SelectedPage != null) && (tabForms.SelectedPage.Tag != null))
            {
                //formIndex = tabForms.TabIndex;
                formIndex = tabForms.Pages.IndexOf(tabForms.SelectedPage);
                (tabForms.SelectedPage.Tag as Form).Select();
            }            
        }

        private void ActiveMdiChild_FormClosed(object sender, FormClosedEventArgs e)
        {
            var existingUsedForm = usedForms.Where(ss => ss.Value.Item1.GetType() == sender.GetType()).Select(ss => ss.Key).FirstOrDefault();
            if (existingUsedForm != null)
            {
                usedForms.Remove(existingUsedForm);
            }
            /*
            if (sender is EtalonNameForm)
                usedForms.Remove(EsnAdminEnums.FormEnum.ETALON_NAME_FORM);
            if (sender is MatchingForm)
                usedForms.Remove(EsnAdminEnums.FormEnum.MATCHING_FORM);
            */
            ((sender as RadFormExt).Tag as RadPageViewPage).Dispose();
            
        }

        private void openForm<T>(EsnAdminEnums.FormEnum formEnum)
            where T : RadFormExt, new()
        {
            RadFormExt f1 = null;
            if (usedForms.ContainsKey(formEnum))
            {
                //f1 = (RadFormExt)usedForms[formEnum].Item1;
                f1 = (T)usedForms[formEnum].Item1;
                formIndex = usedForms[formEnum].Item2;
            }
            if (f1 == null)
            {
                //f1 = new RadFormExt();
                f1 = new T();
                f1.MdiParent = this;
                formIndex = usedForms.Count;
                usedForms.Add(formEnum, new Tuple<RadFormExt, int>(f1, formIndex));                
            }            
            f1.Show();
            f1.Activate();
        }

        private void radMenuItem2_Click(object sender, EventArgs e)
        {
            openForm<EtalonNameForm>(EsnAdminEnums.FormEnum.ETALON_NAME_FORM);
        }

        private void radMenuItem3_Click(object sender, EventArgs e)
        {
            openForm<MatchingForm>(EsnAdminEnums.FormEnum.MATCHING_FORM);
        }

        private void radMenuItem4_Click(object sender, EventArgs e)
        {
            openForm<SupplierNameForm>(EsnAdminEnums.FormEnum.SUPPLIER_NAME_FORM);
        }

        private void radMenuItem9_Click(object sender, EventArgs e)
        {
            //
        }

        private void radMenuItem12_Click(object sender, EventArgs e)
        {
            if (RadMessageBox.Show(this, "Закрыть программу?", "Выход", MessageBoxButtons.YesNo, RadMessageIcon.Question) == DialogResult.Yes)
                Application.Exit();
        }


        private void radMenuComboItem1_ComboBoxElement_SelectedValueChanged(object sender, Telerik.WinControls.UI.Data.ValueChangedEventArgs e)
        {
            ThemeResolutionService.ApplicationThemeName = ((sender as RadDropDownListElement).SelectedValue as string);
        }

        private void radMenuItem13_Click(object sender, EventArgs e)
        {
            ChangeLogForm changeLogForm = new ChangeLogForm();
            changeLogForm.ShowDialog();
        }

        private void radMenuItem14_Click(object sender, EventArgs e)
        {
            openForm<MfrForm>(EsnAdminEnums.FormEnum.MFR_FORM);
        }

        private void radMenuItem15_Click(object sender, EventArgs e)
        {
            openForm<ApplyGroupForm>(EsnAdminEnums.FormEnum.APPLY_GROUP_FORM);
        }

    }
}
