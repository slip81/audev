﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace EsnAdmin.ViewModel
{
    public class Mfr
    {
        public Mfr()
        {
            //manufacturer
        }

        public int mfr_id { get; set; }
        public string mfr_name { get; set; }
    }

    public class MfrAlias
    {
        public MfrAlias()
        {
            //manufacturer_alias
        }

        public int mfr_id { get; set; }
        public int alias_id { get; set; }
        public string alias_name { get; set; }
    }
}