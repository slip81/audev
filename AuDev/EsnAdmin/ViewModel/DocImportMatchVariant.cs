﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace EsnAdmin.ViewModel
{
    public class DocImportMatchVariant
    {
        public DocImportMatchVariant()
        {
            //vw_doc_import_match_variant
        }

        public int variant_id { get; set; }
        public int row_id { get; set; }
        public int match_product_id { get; set; }
        public Nullable<int> match_level { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<int> match_good_id { get; set; }
        public string percent { get; set; }
        public string match_product_name { get; set; }
        public string match_barcode { get; set; }
        public Nullable<int> match_mfr_id { get; set; }
        public string match_mfr_name { get; set; }
        public string barcode_percent { get; set; }
        public string name_percent { get; set; }
        public string mfr_percent { get; set; }

    }
}