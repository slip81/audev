﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace EsnAdmin.ViewModel
{
    public class Doc
    {
        public Doc()
        {
            //vw_doc
        }

        public int doc_id { get; set; }
        public string doc_num { get; set; }
        public string doc_name { get; set; }
        public Nullable<System.DateTime> doc_date { get; set; }
        public Nullable<int> doc_type_id { get; set; }
        public Nullable<int> doc_state_id { get; set; }
        public Nullable<int> partner_id { get; set; }
        public Nullable<int> partner_target_id { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public string doc_type_name { get; set; }
        public string doc_state_name { get; set; }
        public string partner_name { get; set; }
        public Nullable<int> row_cnt { get; set; }
        public Nullable<int> unmatch_cnt { get; set; }
        public Nullable<int> new_cnt { get; set; }
        public Nullable<int> syn_cnt { get; set; }
    }
}