﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace EsnAdmin.ViewModel
{
    public class PartnerGoodFilter
    {
        public PartnerGoodFilter()
        {
            //
        }

        public int partner_id { get; set; }
        public string partner_name { get; set; }
        public Nullable<int> good_cnt { get; set; }
        public Nullable<int> parent_id { get; set; }
    }
}