﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace EsnAdmin.ViewModel
{
    public class DocImportMatch
    {
        public DocImportMatch()
        {
            //vw_doc_import_match
        }

        public int row_id { get; set; }
        public int import_id { get; set; }
        public int row_num { get; set; }
        public string source_name { get; set; }
        public string code { get; set; }
        public string mfr { get; set; }
        public string mfr_group { get; set; }
        public string barcode { get; set; }
        public int state { get; set; }
        public Nullable<int> match_product_id { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public int doc_id { get; set; }
        public string doc_num { get; set; }
        public string doc_name { get; set; }
        public Nullable<System.DateTime> doc_date { get; set; }
        public Nullable<int> doc_type_id { get; set; }
        public Nullable<int> doc_state_id { get; set; }
        public Nullable<int> partner_id { get; set; }
        public string doc_type_name { get; set; }
        public string doc_state_name { get; set; }
        public string partner_name { get; set; }
        public string match_product_name { get; set; }
        public Nullable<int> match_good_id { get; set; }
        public string match_barcode { get; set; }
        public Nullable<int> match_mfr_id { get; set; }
        public string match_mfr_name { get; set; }
        public bool is_approved { get; set; }
        public string percent { get; set; }
        public bool is_auto_match { get; set; }
        public string name_percent { get; set; }
        public string barcode_percent { get; set; }
        public string mfr_percent { get; set; }
        public string init_source_name { get; set; }
        public string apply_group_name { get; set; }

        // additional
        public string state_name
        {
            get
            {
                return state.EnumName<Enums.UndMatchStateEnum>();
            }
            set
            {
                _state_name = value;
            }
        }
        private string _state_name;

        public bool is_changed { get; set; }
        public bool is_confirmed { get; set; }

    }
}