﻿namespace EsnAdmin
{
    partial class SupplierNameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem1 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.ddlSupplier = new Telerik.WinControls.UI.RadDropDownList();
            this.btnRefresh = new Telerik.WinControls.UI.RadButton();
            this.gridSupplier = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.btnImportStart = new Telerik.WinControls.UI.RadButton();
            this.ddlImportConfig = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.edImportFile = new Telerik.WinControls.UI.RadBrowseEditor();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSupplier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSupplier.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlImportConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(12, 21);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(67, 18);
            this.radLabel1.TabIndex = 2;
            this.radLabel1.Text = "Поставщик:";
            // 
            // ddlSupplier
            // 
            this.ddlSupplier.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlSupplier.Location = new System.Drawing.Point(116, 19);
            this.ddlSupplier.Name = "ddlSupplier";
            this.ddlSupplier.Size = new System.Drawing.Size(164, 20);
            this.ddlSupplier.TabIndex = 1;
            this.ddlSupplier.Text = "radDropDownList1";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(148, 66);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(130, 24);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Обновить список";
            this.btnRefresh.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // gridSupplier
            // 
            this.gridSupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSupplier.Location = new System.Drawing.Point(0, 122);
            // 
            // 
            // 
            this.gridSupplier.MasterTemplate.AllowAddNewRow = false;
            this.gridSupplier.MasterTemplate.AllowDeleteRow = false;
            this.gridSupplier.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.AllowGroup = false;
            gridViewTextBoxColumn1.FieldName = "product_code";
            gridViewTextBoxColumn1.HeaderText = "Код";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 100;
            gridViewTextBoxColumn2.AllowGroup = false;
            gridViewTextBoxColumn2.FieldName = "partner_name";
            gridViewTextBoxColumn2.HeaderText = "Наименование";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn2.Width = 450;
            gridViewTextBoxColumn3.AllowGroup = false;
            gridViewTextBoxColumn3.FieldName = "barcode";
            gridViewTextBoxColumn3.HeaderText = "Штрих-код";
            gridViewTextBoxColumn3.Name = "column5";
            gridViewTextBoxColumn3.Width = 150;
            gridViewTextBoxColumn4.AllowGroup = false;
            gridViewTextBoxColumn4.FieldName = "mfr_name";
            gridViewTextBoxColumn4.HeaderText = "Производитель";
            gridViewTextBoxColumn4.Name = "column3";
            gridViewTextBoxColumn4.Width = 200;
            gridViewTextBoxColumn5.AllowGroup = false;
            gridViewTextBoxColumn5.FieldName = "source_partner_name";
            gridViewTextBoxColumn5.HeaderText = "Поставщик";
            gridViewTextBoxColumn5.Name = "column4";
            gridViewTextBoxColumn5.Width = 150;
            gridViewTextBoxColumn6.FieldName = "product_name";
            gridViewTextBoxColumn6.HeaderText = "Эталонное наименование";
            gridViewTextBoxColumn6.Name = "column6";
            gridViewTextBoxColumn6.Width = 350;
            gridViewTextBoxColumn7.FieldName = "product_id";
            gridViewTextBoxColumn7.HeaderText = "Код эталонного наименования";
            gridViewTextBoxColumn7.Name = "column7";
            gridViewTextBoxColumn7.Width = 150;
            this.gridSupplier.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.gridSupplier.MasterTemplate.EnableFiltering = true;
            this.gridSupplier.MasterTemplate.EnableGrouping = false;
            sortDescriptor1.PropertyName = "column2";
            this.gridSupplier.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            gridViewSummaryItem1.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Count;
            gridViewSummaryItem1.FormatString = "Всео строк: {0}";
            gridViewSummaryItem1.Name = "column2";
            this.gridSupplier.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem1}));
            this.gridSupplier.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridSupplier.Name = "gridSupplier";
            this.gridSupplier.ReadOnly = true;
            this.gridSupplier.Size = new System.Drawing.Size(920, 402);
            this.gridSupplier.TabIndex = 3;
            this.gridSupplier.Text = "radGridView1";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.Gainsboro;
            this.radPanel1.Controls.Add(this.radGroupBox2);
            this.radPanel1.Controls.Add(this.radGroupBox1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(920, 122);
            this.radPanel1.TabIndex = 4;
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.radLabel4);
            this.radGroupBox2.Controls.Add(this.btnImportStart);
            this.radGroupBox2.Controls.Add(this.ddlImportConfig);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.radLabel3);
            this.radGroupBox2.Controls.Add(this.edImportFile);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.radGroupBox2.HeaderText = "Загрузка из файла";
            this.radGroupBox2.Location = new System.Drawing.Point(296, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(407, 122);
            this.radGroupBox2.TabIndex = 4;
            this.radGroupBox2.Text = "Загрузка из файла";
            // 
            // radLabel4
            // 
            this.radLabel4.AutoSize = false;
            this.radLabel4.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radLabel4.ForeColor = System.Drawing.Color.Sienna;
            this.radLabel4.Location = new System.Drawing.Point(7, 69);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(259, 47);
            this.radLabel4.TabIndex = 5;
            this.radLabel4.Text = "ВНИМАНИЕ ! Загруженные строки сразу же будут привязаны к эталонному справочнику н" +
    "аименований";
            // 
            // btnImportStart
            // 
            this.btnImportStart.Location = new System.Drawing.Point(272, 68);
            this.btnImportStart.Name = "btnImportStart";
            this.btnImportStart.Size = new System.Drawing.Size(130, 24);
            this.btnImportStart.TabIndex = 4;
            this.btnImportStart.Text = "Начать загрузку";
            this.btnImportStart.Click += new System.EventHandler(this.btnImportStart_Click);
            // 
            // ddlImportConfig
            // 
            this.ddlImportConfig.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlImportConfig.Location = new System.Drawing.Point(166, 42);
            this.ddlImportConfig.Name = "ddlImportConfig";
            this.ddlImportConfig.Size = new System.Drawing.Size(236, 20);
            this.ddlImportConfig.TabIndex = 3;
            this.ddlImportConfig.Text = "radDropDownList1";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(7, 45);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(84, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Конфигурация:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(7, 16);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(35, 18);
            this.radLabel3.TabIndex = 1;
            this.radLabel3.Text = "Файл:";
            // 
            // edImportFile
            // 
            this.edImportFile.Location = new System.Drawing.Point(166, 16);
            this.edImportFile.Name = "edImportFile";
            this.edImportFile.Size = new System.Drawing.Size(236, 20);
            this.edImportFile.TabIndex = 0;
            this.edImportFile.Text = "radBrowseEditor1";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.radLabel1);
            this.radGroupBox1.Controls.Add(this.btnRefresh);
            this.radGroupBox1.Controls.Add(this.ddlSupplier);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radGroupBox1.HeaderText = "Формирование списка";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(296, 122);
            this.radGroupBox1.TabIndex = 3;
            this.radGroupBox1.Text = "Формирование списка";
            // 
            // SupplierNameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 524);
            this.Controls.Add(this.gridSupplier);
            this.Controls.Add(this.radPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SupplierNameForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.Text = "Эталонный справочник поставщиков";
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSupplier.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSupplier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlImportConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadButton btnRefresh;
        private Telerik.WinControls.UI.RadGridView gridSupplier;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDropDownList ddlSupplier;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadButton btnImportStart;
        private Telerik.WinControls.UI.RadDropDownList ddlImportConfig;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadBrowseEditor edImportFile;
    }
}
