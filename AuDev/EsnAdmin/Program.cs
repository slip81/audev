﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Configuration;

namespace EsnAdmin
{
    static class Program
    {
        public static StartForm startForm = null;        

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*
            try
            {
                EsnAdmin.Properties.Settings.Default.Reload();
            }
            catch (ConfigurationErrorsException ex)
            { //(requires System.Configuration)
                string filename = ((ConfigurationErrorsException)ex.InnerException).Filename;
                System.IO.File.Delete(filename);
                EsnAdmin.Properties.Settings.Default.Reload();
            }
            */

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DialogResult result;
            using (var loginForm = new LoginForm() { StartPosition = FormStartPosition.CenterScreen })
            {
                result = loginForm.ShowDialog();
                if (result != DialogResult.OK)
                {
                    if (Application.MessageLoop)
                    {                 
                        Application.Exit();
                    }
                    else
                    {                        
                        Environment.Exit(1);
                    }
                }
                else
                {
                    /*
                    login_session_id = loginForm.session_id;
                    login_user_name = loginForm.user_name;
                    login_db_name = loginForm.db_name;
                    */
                }
            }

            Thread splashThread = new Thread(new ThreadStart(
                delegate
                {
                    startForm = new StartForm();                    
                    Application.Run(startForm);
                }));
            splashThread.SetApartmentState(ApartmentState.STA);
            splashThread.Start();

            //MainForm mainForm = new MainForm(login_session_id, login_user_name, login_db_name);
            //mainForm.Load += new EventHandler(mainForm_Load);
            MainForm mainForm = new MainForm();
            mainForm.Load += new EventHandler(mainForm_Load);
            Application.Run(mainForm);
        }

        static void mainForm_Load(object sender, EventArgs e)
        {
            //close splash
            if (startForm == null)
            {
                return;
            }

            startForm.Invoke(new Action(startForm.Close));
            startForm.Dispose();
            startForm = null;
        }
    }
}