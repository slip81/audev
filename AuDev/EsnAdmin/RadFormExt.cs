﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EsnAdmin.Core;
using EsnAdmin.Service;

namespace EsnAdmin
{
    public class RadFormExt : Telerik.WinControls.UI.RadForm
    {
        public RadFormExt()
        {
            AppManager = AppManagerFactory.CreateAppManager;
        }

        public AppManager AppManager { get; internal set; }
        public int FormId { get; set; }

        #region Service

        protected DocImportService DocImportService
        {
            get
            {
                return AppManager.DocImportService;
            }
        }        

        public ProductService ProductService
        {
            get
            {
                return AppManager.ProductService;
            }
        }        

        public GoodService GoodService
        {
            get
            {
                return AppManager.GoodService;
            }
        }        

        public DocProductFilterService DocProductFilterService
        {
            get
            {
                return AppManager.DocProductFilterService;
            }
        }
        
        public PartnerGoodFilterService PartnerGoodFilterService
        {
            get
            {
                return AppManager.PartnerGoodFilterService;
            }
        }

        public SupplierService SupplierService
        {
            get
            {
                return AppManager.SupplierService;
            }
        }

        public AdminService AdminService
        {
            get
            {
                return AppManager.AdminService;
            }
        }

        public DocService DocService
        {
            get
            {
                return AppManager.DocService;
            }
        }

        public MfrService MfrService
        {
            get
            {
                return AppManager.MfrService;
            }
        }

        public ApplyGroupService ApplyGroupService
        {
            get
            {
                return AppManager.ApplyGroupService;
            }
        }

        #endregion
    }
}
