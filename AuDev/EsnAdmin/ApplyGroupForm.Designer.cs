﻿namespace EsnAdmin
{
    partial class ApplyGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem1 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ApplyGroupForm));
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.gridApplyGroup = new Telerik.WinControls.UI.RadGridView();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.btnApplyGroupDel = new Telerik.WinControls.UI.RadButton();
            this.btnApplyGroupEdit = new Telerik.WinControls.UI.RadButton();
            this.btnApplyGroupAdd = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridApplyGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridApplyGroup.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnApplyGroupDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApplyGroupEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApplyGroupAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(958, 561);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.BackColor = System.Drawing.Color.Gainsboro;
            this.splitPanel1.Controls.Add(this.radPanel7);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(958, 100);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.3204668F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -178);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.gridApplyGroup);
            this.splitPanel2.Location = new System.Drawing.Point(0, 104);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(958, 457);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.3204668F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 147);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // gridApplyGroup
            // 
            this.gridApplyGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridApplyGroup.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridApplyGroup.MasterTemplate.AllowAddNewRow = false;
            this.gridApplyGroup.MasterTemplate.AllowDeleteRow = false;
            this.gridApplyGroup.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.AllowGroup = false;
            gridViewTextBoxColumn1.FieldName = "apply_group_id";
            gridViewTextBoxColumn1.HeaderText = "Код";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 150;
            gridViewTextBoxColumn2.AllowGroup = false;
            gridViewTextBoxColumn2.FieldName = "apply_group_name";
            gridViewTextBoxColumn2.HeaderText = "Наименование";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn2.Width = 450;
            this.gridApplyGroup.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.gridApplyGroup.MasterTemplate.EnableFiltering = true;
            this.gridApplyGroup.MasterTemplate.EnableGrouping = false;
            sortDescriptor1.PropertyName = "column2";
            this.gridApplyGroup.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            gridViewSummaryItem1.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Count;
            gridViewSummaryItem1.FormatString = "Всего строк: {0}";
            gridViewSummaryItem1.Name = "column2";
            this.gridApplyGroup.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem1}));
            this.gridApplyGroup.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridApplyGroup.Name = "gridApplyGroup";
            this.gridApplyGroup.ReadOnly = true;
            this.gridApplyGroup.Size = new System.Drawing.Size(958, 457);
            this.gridApplyGroup.TabIndex = 2;
            this.gridApplyGroup.Text = "radGridView1";
            // 
            // radPanel7
            // 
            this.radPanel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radPanel7.Controls.Add(this.btnApplyGroupDel);
            this.radPanel7.Controls.Add(this.btnApplyGroupEdit);
            this.radPanel7.Controls.Add(this.btnApplyGroupAdd);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel7.Location = new System.Drawing.Point(0, 65);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(958, 35);
            this.radPanel7.TabIndex = 13;
            // 
            // btnApplyGroupDel
            // 
            this.btnApplyGroupDel.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyGroupDel.Image")));
            this.btnApplyGroupDel.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnApplyGroupDel.Location = new System.Drawing.Point(75, 5);
            this.btnApplyGroupDel.Name = "btnApplyGroupDel";
            this.btnApplyGroupDel.Size = new System.Drawing.Size(32, 26);
            this.btnApplyGroupDel.TabIndex = 8;
            this.btnApplyGroupDel.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnApplyGroupDel_ToolTipTextNeeded);
            this.btnApplyGroupDel.Click += new System.EventHandler(this.btnApplyGroupDel_Click);
            // 
            // btnApplyGroupEdit
            // 
            this.btnApplyGroupEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyGroupEdit.Image")));
            this.btnApplyGroupEdit.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnApplyGroupEdit.Location = new System.Drawing.Point(42, 5);
            this.btnApplyGroupEdit.Name = "btnApplyGroupEdit";
            this.btnApplyGroupEdit.Size = new System.Drawing.Size(27, 26);
            this.btnApplyGroupEdit.TabIndex = 7;
            this.btnApplyGroupEdit.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnApplyGroupEdit_ToolTipTextNeeded);
            this.btnApplyGroupEdit.Click += new System.EventHandler(this.btnApplyGroupEdit_Click);
            // 
            // btnApplyGroupAdd
            // 
            this.btnApplyGroupAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnApplyGroupAdd.Image")));
            this.btnApplyGroupAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnApplyGroupAdd.Location = new System.Drawing.Point(4, 5);
            this.btnApplyGroupAdd.Name = "btnApplyGroupAdd";
            this.btnApplyGroupAdd.Size = new System.Drawing.Size(32, 26);
            this.btnApplyGroupAdd.TabIndex = 6;
            this.btnApplyGroupAdd.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnApplyGroupAdd_ToolTipTextNeeded);
            this.btnApplyGroupAdd.Click += new System.EventHandler(this.btnApplyGroupAdd_Click);
            // 
            // ApplyGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(958, 561);
            this.Controls.Add(this.radSplitContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ApplyGroupForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.Text = "Группы по применению";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridApplyGroup.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridApplyGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnApplyGroupDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApplyGroupEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnApplyGroupAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView gridApplyGroup;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadButton btnApplyGroupDel;
        private Telerik.WinControls.UI.RadButton btnApplyGroupEdit;
        private Telerik.WinControls.UI.RadButton btnApplyGroupAdd;
    }
}
