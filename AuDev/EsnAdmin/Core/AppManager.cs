﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AuDev.Common.Db.Model;
using EsnAdmin.Service;

namespace EsnAdmin.Core
{
    public class AppManager : IDisposable
    {
        #region Constructor

        public AppManager()
        {
            //
        }

        static AppManager()
        {
            //currentUser = new AppUser();
            currentUser = null;
        }

        #endregion

        #region DbContext

        public AuMainDb dbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = new AuMainDb();                 
                }
                return _dbContext;
            }
        }
        private AuMainDb _dbContext;

        public AuMainDb dbParallelContext
        {
            get
            {
                if (_dbParallelContext == null)
                {
                    _dbParallelContext = new AuMainDb();
                }
                return _dbParallelContext;
            }
        }
        private AuMainDb _dbParallelContext;

        #endregion

        #region CurrentUser

        private static AppUser currentUser;

        public AppUser CurrentUser
        {
            get { return currentUser; }
        }

        public void CreateUser(string n)
        {
            currentUser = new AppUser(n, true);            
        }

        #endregion

        #region AutoMapper

        public AutoMapper.IMapper ModelMapper
        {
            get
            {
                if (modelMapper == null)
                {
                    modelMapper = new AutoMapperCommon();                    
                }
                return modelMapper;
            }
        }
        private AutoMapper.IMapper modelMapper;

        #endregion

        #region Service

        public DocImportService DocImportService
        {
            get
            {
                if (_docImportService == null)
                    _docImportService = new DocImportService(this);
                return _docImportService;
            }
        }
        private DocImportService _docImportService;

        public ProductService ProductService
        {
            get
            {
                if (_productService == null)
                    _productService = new ProductService(this);
                return _productService;
            }
        }
        private ProductService _productService;

        public GoodService GoodService
        {
            get
            {
                if (_goodService == null)
                    _goodService = new GoodService(this);
                return _goodService;
            }
        }
        private GoodService _goodService;

        public DocProductFilterService DocProductFilterService
        {
            get
            {
                if (_docProductFilterService == null)
                    _docProductFilterService = new DocProductFilterService(this);
                return _docProductFilterService;
            }
        }
        private DocProductFilterService _docProductFilterService;

        public PartnerGoodFilterService PartnerGoodFilterService
        {
            get
            {
                if (_partnerGoodFilterService == null)
                    _partnerGoodFilterService = new PartnerGoodFilterService(this);
                return _partnerGoodFilterService;
            }
        }
        private PartnerGoodFilterService _partnerGoodFilterService;

        public SupplierService SupplierService
        {
            get
            {
                if (_supplierService == null)
                    _supplierService = new SupplierService(this);
                return _supplierService;
            }
        }
        private SupplierService _supplierService;

        public AdminService AdminService
        {
            get
            {
                if (_adminService == null)
                    _adminService = new AdminService(this);
                return _adminService;
            }
        }
        private AdminService _adminService;

        public DocService DocService
        {
            get
            {
                if (_docService == null)
                    _docService = new DocService(this);
                return _docService;
            }
        }
        private DocService _docService;

        public MfrService MfrService
        {
            get
            {
                if (_mfrService == null)
                    _mfrService = new MfrService(this);
                return _mfrService;
            }
        }
        private MfrService _mfrService;

        public ApplyGroupService ApplyGroupService
        {
            get
            {
                if (_applyGroupService == null)
                    _applyGroupService = new ApplyGroupService(this);
                return _applyGroupService;
            }
        }
        private ApplyGroupService _applyGroupService;

        //ApplyGroupService

        #endregion

        #region Dispose

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }

        #endregion
    }
}
