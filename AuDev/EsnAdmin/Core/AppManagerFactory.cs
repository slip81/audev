﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsnAdmin.Core
{
    public static class AppManagerFactory
    {        
        private static AppManager appManager;

        public static AppManager CreateAppManager
        {
            get
            {
                if (appManager == null)
                    appManager = new AppManager();
                return appManager;
            }
        }
    }
}
