﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using Telerik.WinControls.Enumerations;

namespace EsnAdmin
{
    public partial class AutoMatchParamForm : RadFormExt
    {
        public AutoMatchParam autoMatchParam { get; set; }

        public AutoMatchParamForm()
        {
            InitializeComponent();
            //
            autoMatchParam = new AutoMatchParam();
            updateControls(chbAutoMatchFull.Checked, 
                chbAutoMatchPart.Checked,
                ((chbAutoMatchPart.Checked) && (chbAutoMatchPartSumPercent.Checked)),
                ((chbAutoMatchPart.Checked) && (chbAutoMatchPartBarcode.Checked) && (!chbAutoMatchPartSumPercent.Checked)),
                ((chbAutoMatchPart.Checked) && (chbAutoMatchPartName.Checked) && (!chbAutoMatchPartSumPercent.Checked)),
                ((chbAutoMatchPart.Checked) && (chbAutoMatchPartMfr.Checked) && (!chbAutoMatchPartSumPercent.Checked))
                );
        }

        private void AutoMatchParamForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            autoMatchParam.MatchFull = chbAutoMatchFull.Checked;                        
            autoMatchParam.MatchFull_barcode = chbAutoMatchFullBarcode.Checked;
            autoMatchParam.MatchFull_name = chbAutoMatchFullName.Checked;
            autoMatchParam.MatchFull_mfr = chbAutoMatchFullMfr.Checked;
            autoMatchParam.MatchFull_approved = chbAutoMatchFullApproved.Checked;
            //
            autoMatchParam.MatchPart = chbAutoMatchPart.Checked;
            autoMatchParam.MatchPart_barcode = chbAutoMatchPartBarcode.Checked;
            autoMatchParam.MatchPart_name = chbAutoMatchPartName.Checked;
            autoMatchParam.MatchPart_mfr = chbAutoMatchPartMfr.Checked;
            autoMatchParam.MatchPart_sum_percent = chbAutoMatchPartSumPercent.Checked;
            autoMatchParam.MatchPart_barcode_percent = edAutoMatchPartBarcodePercent.Value;
            autoMatchParam.MatchPart_name_percent = edAutoMatchPartNamePercent.Value;
            autoMatchParam.MatchPart_mfr_percent = edAutoMatchPartMfrPercent.Value;
            autoMatchParam.MatchPart_percent = edAutoMatchPartPercent.Value;
            autoMatchParam.MatchPart_approved = chbAutoMatchPartApproved.Checked;
        }

        private void chbAutoMatchFull_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            bool enabled = args.ToggleState == ToggleState.On;
            updateControls1(enabled);
        }

        private void chbAutoMatchPart_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            bool enabled = args.ToggleState == ToggleState.On;
            updateControls2(enabled);
        }

        private void updateControls(bool enabled1, bool enabled2, bool enabled3, bool enabled4, bool enabled5, bool enabled6)
        {
            updateControls1(enabled1);
            updateControls2(enabled2);
            updateControls3(enabled3);
            updateControls4(enabled4);
            updateControls5(enabled5);
            updateControls6(enabled6);
        }

        private void updateControls1(bool enabled1)
        {
            chbAutoMatchFullBarcode.Enabled = enabled1;
            chbAutoMatchFullName.Enabled = enabled1;
            chbAutoMatchFullMfr.Enabled = enabled1;
            chbAutoMatchFullApproved.Enabled = enabled1;
        }

        private void updateControls2(bool enabled2)
        {
            chbAutoMatchPartBarcode.Enabled = enabled2;
            chbAutoMatchPartName.Enabled = enabled2;
            chbAutoMatchPartMfr.Enabled = enabled2;
            edAutoMatchPartPercent.Enabled = enabled2 && chbAutoMatchPartSumPercent.Checked;
            chbAutoMatchPartApproved.Enabled = enabled2;
            chbAutoMatchPartSumPercent.Enabled = enabled2;
            edAutoMatchPartBarcodePercent.Enabled = enabled2;
            edAutoMatchPartNamePercent.Enabled = enabled2;
            edAutoMatchPartMfrPercent.Enabled = enabled2;
        }

        private void updateControls3(bool enabled3)
        {
            edAutoMatchPartPercent.Enabled = enabled3;
            edAutoMatchPartBarcodePercent.Enabled = !enabled3 && chbAutoMatchPart.Checked && chbAutoMatchPartBarcode.Checked;
            edAutoMatchPartNamePercent.Enabled = !enabled3 && chbAutoMatchPart.Checked && chbAutoMatchPartName.Checked;
            edAutoMatchPartMfrPercent.Enabled = !enabled3 && chbAutoMatchPart.Checked && chbAutoMatchPartMfr.Checked;
        }

        private void updateControls4(bool enabled4)
        {
            edAutoMatchPartBarcodePercent.Enabled = enabled4;
        }

        private void updateControls5(bool enabled5)
        {
            edAutoMatchPartNamePercent.Enabled = enabled5;
        }

        private void updateControls6(bool enabled6)
        {
            edAutoMatchPartMfrPercent.Enabled = enabled6;
        }

        private void chbAutoMatchPartSumPercent_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            bool enabled = args.ToggleState == ToggleState.On;
            updateControls3(enabled);
        }

        private void chbAutoMatchPartBarcode_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            bool enabled = ((args.ToggleState == ToggleState.On) && (!chbAutoMatchPartSumPercent.Checked));
            updateControls4(enabled);            
        }

        private void chbAutoMatchPartName_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            bool enabled = ((args.ToggleState == ToggleState.On) && (!chbAutoMatchPartSumPercent.Checked));
            updateControls5(enabled);
        }

        private void chbAutoMatchPartMfr_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            bool enabled = ((args.ToggleState == ToggleState.On) && (!chbAutoMatchPartSumPercent.Checked));
            updateControls6(enabled);
        }
    }

    public class AutoMatchParam
    {
        public AutoMatchParam()
        {
            //
        }

        public bool MatchFull { get; set; }        
        public bool MatchFull_barcode { get; set; }
        public bool MatchFull_name { get; set; }
        public bool MatchFull_mfr { get; set; }
        public bool MatchFull_approved { get; set; }
        //
        public bool MatchPart { get; set; }
        public bool MatchPart_barcode { get; set; }
        public bool MatchPart_name { get; set; }
        public bool MatchPart_mfr { get; set; }
        public bool MatchPart_sum_percent { get; set; }
        public decimal? MatchPart_barcode_percent { get; set; }
        public decimal? MatchPart_name_percent { get; set; }
        public decimal? MatchPart_mfr_percent { get; set; }
        public decimal? MatchPart_percent { get; set; }
        public bool MatchPart_approved { get; set; }
    }
}
