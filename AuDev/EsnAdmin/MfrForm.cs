﻿#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using EsnAdmin.ViewModel;
using System.Collections;
#endregion

namespace EsnAdmin
{
    public partial class MfrForm : RadFormExt
    {
        private bool lockEvents = false;
        private List<Mfr> mfrDs = null;
        private List<MfrAlias> mfrAliasDs = null;

        private MfrEditForm mfrEditForm = null;

        public MfrForm()
        {
            InitializeComponent();
            //
            InitMfrGrid();
        }

        private void InitMfrGrid()
        {
            RadDragDropService svc = gridMfr.GridViewElement.GetService<RadDragDropService>();
            svc.PreviewDragStart += svc_PreviewDragStart;
            svc.PreviewDragDrop += svc_PreviewDragDrop;
            svc.PreviewDragOver += svc_PreviewDragOver;

            //register the custom row selection behavior
            var gridBehavior = gridMfr.GridBehavior as BaseGridBehavior;
            gridBehavior.UnregisterBehavior(typeof(GridViewDataRowInfo));
            gridBehavior.RegisterBehavior(typeof(GridViewDataRowInfo),
                new RowSelectionGridBehavior());
            //
            Cursor = Cursors.WaitCursor;
            try
            {
                GridMfr_bind();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void GridMfr_bind()
        {
            var mfrs = MfrService.GetManufacturers();
            if (mfrs.Error != null)
            {
                MessageBox.Show(mfrs.Error.Mess);
                return;
            }

            mfrDs = new List<Mfr>(mfrs.manufacturers);
            mfrAliasDs = new List<MfrAlias>(mfrs.manufacturer_aliases);

            gridMfr.DataSource = mfrDs;
            gridMfr.TableElement.ScrollToRow(0);
            gridMfr.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Bottom;
        }


        private void GridMfrAlias_bind(int currentMfrId)
        {
            List<MfrAlias> aliases = mfrAliasDs.Where(ss => ss.mfr_id == currentMfrId).ToList();
            gridMfrAlias.DataSource = aliases;
        }

        private void gridMfr_SelectionChanged(object sender, EventArgs e)
        {
            if (lockEvents)
                return;
            lockEvents = true;
            try
            {
                var currentMfrId = gridMfr.SelectedRows != null && gridMfr.SelectedRows.Count > 0 ? gridMfr.SelectedRows[0].Cells[0].Value : null;
                if (currentMfrId != null)
                {
                    GridMfrAlias_bind((int)currentMfrId);
                }
            }
            finally
            {
                lockEvents = false;
            }
        }

        private void RefreshMfrAliasList()
        {
            mfrAliasDs = null;
            mfrAliasDs = MfrService.GetManufacturerAliases();
        }

        private void btnMfrAdd_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Добавить нового изготовителя";
        }

        private void btnMfrEdit_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Изменить выбранного изготовителя";
        }

        private void btnMfrDel_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Удалить выбранного изготовителя";
        }

        private void btnMfrAdd_Click(object sender, EventArgs e)
        {
            if (mfrEditForm == null)
                mfrEditForm = new MfrEditForm();
            mfrEditForm.InitForm_Add();
            if (mfrEditForm.ShowDialog() == DialogResult.OK)
            {
                var newMfr = MfrService.InsertMfr(mfrEditForm.new_mfr_name);
                if (newMfr == null)
                {
                    MessageBox.Show("Ошибка: изготовитель не добавлен");
                    return;
                }
                mfrDs.Add(newMfr);
            }
        }

        private void btnMfrEdit_Click(object sender, EventArgs e)
        {
            Mfr currentMfr = gridMfr.SelectedRows != null && gridMfr.SelectedRows.Count > 0 ? (Mfr)gridMfr.SelectedRows[0].DataBoundItem : null;
            if (currentMfr == null)
            {
                MessageBox.Show("Не выбран изготовитель");
                return;
            }

            if (mfrEditForm == null)
                mfrEditForm = new MfrEditForm();
            mfrEditForm.InitForm_Edit(currentMfr.mfr_id, currentMfr.mfr_name);
            if (mfrEditForm.ShowDialog() == DialogResult.OK)
            {
                var newMfr = MfrService.UpdateMfr(currentMfr.mfr_id, mfrEditForm.new_mfr_name);
                if (newMfr == null)
                {
                    MessageBox.Show("Ошибка: изготовитель не изменен");
                    return;
                }

                var newMfr_Ds = mfrDs.Where(ss => ss.mfr_id == newMfr.mfr_id).FirstOrDefault();
                if (newMfr_Ds != null)
                {
                    newMfr_Ds.mfr_name = newMfr.mfr_name;
                }
            }
        }

        private void btnMfrDel_Click(object sender, EventArgs e)
        {
            Mfr currentMfr = gridMfr.SelectedRows != null && gridMfr.SelectedRows.Count > 0 ? (Mfr)gridMfr.SelectedRows[0].DataBoundItem : null;
            if (currentMfr == null)
            {
                MessageBox.Show("Не выбран изготовитель");
                return;
            }

            if (RadMessageBox.Show(this, "Удалить изготовителя ?", "Подтверждение", MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                return;

            var del = MfrService.DeleteMfr(currentMfr.mfr_id);
            if (!del)
            {
                MessageBox.Show("Ошибка: изготовитель не удален");
                return;
            }
            var delMfr_Ds = mfrDs.Where(ss => ss.mfr_id == currentMfr.mfr_id).FirstOrDefault();
            if (delMfr_Ds != null)
            {
                mfrDs.Remove(delMfr_Ds);
            }
        }

        private void btnMfrSynAdd_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Добавить синоним изготовителя";
        }

        private void btnMfrSynEdit_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Изменить выбранный синоним изготовителя";
        }

        private void btnMfrSynDel_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Удалить выбранный синоним изготовителя";
        }

        private void btnMfrSynAdd_Click(object sender, EventArgs e)
        {
            Mfr currentMfr = gridMfr.SelectedRows != null && gridMfr.SelectedRows.Count > 0 ? (Mfr)gridMfr.SelectedRows[0].DataBoundItem : null;
            if (currentMfr == null)
            {
                MessageBox.Show("Не выбран изготовитель");
                return;
            }

            if (mfrEditForm == null)
                mfrEditForm = new MfrEditForm();
            mfrEditForm.InitForm_Add();
            if (mfrEditForm.ShowDialog() == DialogResult.OK)
            {
                var newMfrSyn = MfrService.InsertMfr_Syn(currentMfr.mfr_id, mfrEditForm.new_mfr_name);
                if (newMfrSyn == null)
                {
                    MessageBox.Show("Ошибка: синоним не добавлен");
                    return;
                }
                mfrAliasDs.Add(newMfrSyn);
                GridMfrAlias_bind(currentMfr.mfr_id);
            }
        }

        private void btnMfrSynEdit_Click(object sender, EventArgs e)
        {
            Mfr currentMfr = gridMfr.SelectedRows != null && gridMfr.SelectedRows.Count > 0 ? (Mfr)gridMfr.SelectedRows[0].DataBoundItem : null;
            if (currentMfr == null)
            {
                MessageBox.Show("Не выбран изготовитель");
                return;
            }

            MfrAlias currentSyn = gridMfrAlias.SelectedRows != null && gridMfrAlias.SelectedRows.Count > 0 ? (MfrAlias)gridMfrAlias.SelectedRows[0].DataBoundItem : null;
            if (currentSyn == null)
            {
                MessageBox.Show("Не выбран синоним");
                return;
            }

            if (mfrEditForm == null)
                mfrEditForm = new MfrEditForm();
            mfrEditForm.InitForm_Edit(currentSyn.alias_id, currentSyn.alias_name);
            if (mfrEditForm.ShowDialog() == DialogResult.OK)
            {
                var newMfrSyn = MfrService.UpdateMfr_Syn(currentMfr.mfr_id, currentSyn.alias_id, mfrEditForm.new_mfr_name);
                if (newMfrSyn == null)
                {
                    MessageBox.Show("Ошибка: синоним не изменен");
                    return;
                }

                var newMfrAlias_Ds = mfrAliasDs.Where(ss => ss.alias_id == newMfrSyn.alias_id).FirstOrDefault();
                if (newMfrAlias_Ds != null)
                {
                    newMfrAlias_Ds.alias_name = newMfrSyn.alias_name;
                    GridMfrAlias_bind(currentMfr.mfr_id);
                }
            }
        }

        private void btnMfrSynDel_Click(object sender, EventArgs e)
        {
            Mfr currentMfr = gridMfr.SelectedRows != null && gridMfr.SelectedRows.Count > 0 ? (Mfr)gridMfr.SelectedRows[0].DataBoundItem : null;
            if (currentMfr == null)
            {
                MessageBox.Show("Не выбран изготовитель");
                return;
            }

            MfrAlias currentSyn = gridMfrAlias.SelectedRows != null && gridMfrAlias.SelectedRows.Count > 0 ? (MfrAlias)gridMfrAlias.SelectedRows[0].DataBoundItem : null;
            if (currentSyn == null)
            {
                MessageBox.Show("Не выбран синоним");
                return;
            }

            if (RadMessageBox.Show(this, "Удалить синоним ?", "Подтверждение", MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                return;

            var del = MfrService.DeleteMfr_Syn(currentSyn.alias_id);
            if (!del)
            {
                MessageBox.Show("Ошибка: синоним не удален");
                return;
            }
            var delMfrAlias_Ds = mfrAliasDs.Where(ss => ss.alias_id == currentSyn.alias_id).FirstOrDefault();
            if (delMfrAlias_Ds != null)
            {
                mfrAliasDs.Remove(delMfrAlias_Ds);
                GridMfrAlias_bind(currentMfr.mfr_id);
            }
        }

        #region Drag Drop

        private void svc_PreviewDragOver(object sender, RadDragOverEventArgs e)
        {
            if (e.DragInstance is GridDataRowElement)
            {
                e.CanDrop = e.HitTarget is GridDataRowElement ||
                            e.HitTarget is GridTableElement ||
                            e.HitTarget is GridSummaryRowElement;
            }
        }

        //initiate the move of selected row
        private void svc_PreviewDragDrop(object sender, RadDropEventArgs e)
        {
            GridDataRowElement rowElement = e.DragInstance as GridDataRowElement;

            if (rowElement == null)
            {
                return;
            }
            e.Handled = true;

            RadItem dropTarget = e.HitTarget as RadItem;
            RadGridView targetGrid = dropTarget.ElementTree.Control as RadGridView;
            if (targetGrid == null)
            {
                return;
            }

            var dragGrid = rowElement.ElementTree.Control as RadGridView;
            if (targetGrid == dragGrid)
            {
                e.Handled = true;

                GridDataRowElement dropTargetRow = dropTarget as GridDataRowElement;

                Mfr targetMfr = (Mfr)dropTargetRow.Data.DataBoundItem;
                if (targetMfr == null)
                {
                    MessageBox.Show("Не найдена строка");
                    return;
                }

                this.MoveRows(dragGrid, dragGrid.SelectedRows, targetMfr);
            }
        }

        private int GetTargetRowIndex(GridDataRowElement row, Point dropLocation)
        {
            int halfHeight = row.Size.Height / 2;
            int index = row.RowInfo.Index;
            if (dropLocation.Y > halfHeight)
            {
                index++;
            }
            return index;
        }

        //required to initiate drag and drop when grid is in bound mode
        private void svc_PreviewDragStart(object sender, PreviewDragStartEventArgs e)
        {
            e.CanStart = true;
        }

        private void MoveRows(RadGridView dragGrid, GridViewSelectedRowsCollection selectedRows, Mfr targetMfr)
        {
            dragGrid.BeginUpdate();

            if (dragGrid.DataSource != null && typeof(IList).IsAssignableFrom(dragGrid.DataSource.GetType()))
            {
                //bound to a list of objects scenario
                var sourceCollection = (IList)dragGrid.DataSource;
                for (int i = selectedRows.Count - 1; i >= 0; i--)
                {
                    GridViewRowInfo row = selectedRows[i];

                    if (row is GridViewSummaryRowInfo)
                    {
                        return;
                    }

                    string mess = "Переместить изготовителя в синонимы:";
                    mess += System.Environment.NewLine;
                    mess += targetMfr.mfr_name + " ?";
                    if (RadMessageBox.Show(this, mess, "Подтверждение", MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                        return;

                    int sourceMfrId = (int)((Mfr)row.DataBoundItem).mfr_id;

                    bool moveRes = MfrService.MoveMfr_FromMainToSyn(targetMfr.mfr_id, sourceMfrId);
                    if (!moveRes)
                    {
                        MessageBox.Show("Ошибка: перемещение в синонимы не выполнено");
                        return;
                    }
                    
                    RefreshMfrAliasList();

                    sourceCollection.Remove(row.DataBoundItem);
                    dragGrid.Rows.Remove(row);                    
                }
            }
            else
            {
                throw new ApplicationException("Непредусмотренное действие");
            }

            dragGrid.EndUpdate(true);
        }

        #endregion
    }
}
