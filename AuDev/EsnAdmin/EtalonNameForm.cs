﻿#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using EsnAdmin.ViewModel;
#endregion

namespace EsnAdmin
{
    public partial class EtalonNameForm : RadFormExt
    {
        private bool lockEvents = false;
        private List<Product> productDs = null;
        private List<Good> goodDs = null;


        public EtalonNameForm()
        {
            InitializeComponent();
            //
            InitImportParams();
        }

        private void InitImportParams()
        {
            InitImportFileEditor();
            InitImportConfigDdl();
        }

        private void InitImportFileEditor()
        {
            OpenFileDialog dlg = (OpenFileDialog)edImportFile.Dialog;
            dlg.InitialDirectory = "c:\\";
            //openFile.Filter = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx|XML файлы (*.xml)|*.xml";
            //openFile.Filter = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx";
            dlg.Filter = "Excel файлы (*.xlsx)|*.xlsx";
        }

        private void InitImportConfigDdl()
        {
            ddlImportConfig.DataSource = null;
            ddlImportConfig.DataSource = DocImportService.GetDocImportConfigTypes();
            ddlImportConfig.DisplayMember = "config_type_name";
            ddlImportConfig.ValueMember = "config_type_id";
            ddlImportConfig.SelectedIndex = 0;
        }
       

        private void radButton1_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                GridEtalon_bind();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void gridEtalon_SelectionChanged(object sender, EventArgs e)
        {
            if (lockEvents)
                return;
            lockEvents = true;
            try
            {
                var currentProductId = gridEtalon.SelectedRows != null && gridEtalon.SelectedRows.Count > 0 ? gridEtalon.SelectedRows[0].Cells[0].Value : null;
                if (currentProductId != null)
                {                    
                    GridEtalonGood_bind((int)currentProductId);
                }
            }
            finally
            {
                lockEvents = false;
            }
        }

        private void GridEtalon_bind()
        {
            var products = ProductService.GetProducts_Etalon();
            if (products.Error != null)
            {
                MessageBox.Show(products.Error.Mess);
                return;
            }

            productDs = new List<Product>(products.products);            
            goodDs = new List<Good>(products.goods);

            gridEtalon.DataSource = productDs;
            gridEtalon.TableElement.ScrollToRow(0);
            gridEtalon.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Bottom;
        }

        private void GridEtalonGood_bind(int currentProductId)
        {
            /*
            List<Good> goods = goodDs.Where(ss => ss.product_id == currentProductId).ToList();
            gridEtalonGood.DataSource = goods;
            */
            var goodsResult = GoodService.GetGoods_byProduct(currentProductId);
            if (goodsResult.Error != null)
            {
                MessageBox.Show(goodsResult.Error.Mess);
                return;
            }
            List<Good> goods = goodsResult.goods;
            gridEtalonGood.DataSource = goods;
        }

        private void btnImportStart_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                var import_id = DocImportService.CreateDocImport();
                var importResult = DocImportService.ImportEtalon(import_id, edImportFile.Value, (int)ddlImportConfig.SelectedValue);
                if (importResult.Error != null)
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show(importResult.Error.Mess);
                }
                else
                {
                    if (importResult.product_cnt > 0)
                    {
                        GridEtalon_bind();
                    }
                    MessageBox.Show("Новых строк: " + importResult.product_cnt.ToString());
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void radButton1_Click_1(object sender, EventArgs e)
        {
            // !!!
        }
    }
}
