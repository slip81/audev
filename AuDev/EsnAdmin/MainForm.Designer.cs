﻿namespace EsnAdmin
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuSeparatorItem1 = new Telerik.WinControls.UI.RadMenuSeparatorItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem5 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem14 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem15 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem9 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem11 = new Telerik.WinControls.UI.RadMenuItem();
            this.ddlTheme = new Telerik.WinControls.UI.RadMenuComboItem();
            this.radMenuItem13 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem12 = new Telerik.WinControls.UI.RadMenuItem();
            this.tabForms = new Telerik.WinControls.UI.RadPageView();
            this.aquaTheme1 = new Telerik.WinControls.Themes.AquaTheme();
            this.breezeTheme1 = new Telerik.WinControls.Themes.BreezeTheme();
            this.desertTheme1 = new Telerik.WinControls.Themes.DesertTheme();
            this.highContrastBlackTheme1 = new Telerik.WinControls.Themes.HighContrastBlackTheme();
            this.office2007BlackTheme1 = new Telerik.WinControls.Themes.Office2007BlackTheme();
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.office2010BlackTheme1 = new Telerik.WinControls.Themes.Office2010BlackTheme();
            this.office2010BlueTheme1 = new Telerik.WinControls.Themes.Office2010BlueTheme();
            this.office2010SilverTheme1 = new Telerik.WinControls.Themes.Office2010SilverTheme();
            this.office2013DarkTheme1 = new Telerik.WinControls.Themes.Office2013DarkTheme();
            this.office2013LightTheme1 = new Telerik.WinControls.Themes.Office2013LightTheme();
            this.telerikMetroTheme1 = new Telerik.WinControls.Themes.TelerikMetroTheme();
            this.telerikMetroBlueTheme1 = new Telerik.WinControls.Themes.TelerikMetroBlueTheme();
            this.telerikMetroTouchTheme1 = new Telerik.WinControls.Themes.TelerikMetroTouchTheme();
            this.visualStudio2012DarkTheme1 = new Telerik.WinControls.Themes.VisualStudio2012DarkTheme();
            this.visualStudio2012LightTheme1 = new Telerik.WinControls.Themes.VisualStudio2012LightTheme();
            this.windows7Theme1 = new Telerik.WinControls.Themes.Windows7Theme();
            this.windows8Theme1 = new Telerik.WinControls.Themes.Windows8Theme();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            ((System.ComponentModel.ISupportInitialize)(this.ddlTheme.ComboBoxElement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabForms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "Основное";
            this.radMenuItem1.AccessibleName = "Основное";
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem2,
            this.radMenuItem4,
            this.radMenuSeparatorItem1,
            this.radMenuItem3});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Text = "Основное";
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "Эталонный справочник";
            this.radMenuItem2.AccessibleName = "Эталонный справочник";
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Text = "Эталонный справочник наименований";
            this.radMenuItem2.Click += new System.EventHandler(this.radMenuItem2_Click);
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.AccessibleDescription = "Справочники поставщиков";
            this.radMenuItem4.AccessibleName = "Справочники поставщиков";
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Text = "Эталонный справочник поставщиков";
            this.radMenuItem4.Click += new System.EventHandler(this.radMenuItem4_Click);
            // 
            // radMenuSeparatorItem1
            // 
            this.radMenuSeparatorItem1.AccessibleDescription = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.AccessibleName = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Name = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.Text = "radMenuSeparatorItem1";
            this.radMenuSeparatorItem1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.AccessibleDescription = "Сопоставление";
            this.radMenuItem3.AccessibleName = "Сопоставление";
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Text = "Сопоставление справочников";
            this.radMenuItem3.Click += new System.EventHandler(this.radMenuItem3_Click);
            // 
            // radMenuItem5
            // 
            this.radMenuItem5.AccessibleDescription = "Справочники";
            this.radMenuItem5.AccessibleName = "Справочники";
            this.radMenuItem5.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem14,
            this.radMenuItem15});
            this.radMenuItem5.Name = "radMenuItem5";
            this.radMenuItem5.Text = "Справочники";
            // 
            // radMenuItem14
            // 
            this.radMenuItem14.AccessibleDescription = "Изготовители";
            this.radMenuItem14.AccessibleName = "Изготовители";
            this.radMenuItem14.Name = "radMenuItem14";
            this.radMenuItem14.Text = "Изготовители";
            this.radMenuItem14.Click += new System.EventHandler(this.radMenuItem14_Click);
            // 
            // radMenuItem15
            // 
            this.radMenuItem15.AccessibleDescription = "Группы по применению";
            this.radMenuItem15.AccessibleName = "Группы по применению";
            this.radMenuItem15.Name = "radMenuItem15";
            this.radMenuItem15.Text = "Группы по применению";
            this.radMenuItem15.Click += new System.EventHandler(this.radMenuItem15_Click);
            // 
            // radMenuItem9
            // 
            this.radMenuItem9.AccessibleDescription = "Выход";
            this.radMenuItem9.AccessibleName = "Выход";
            this.radMenuItem9.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem11,
            this.ddlTheme});
            this.radMenuItem9.Name = "radMenuItem9";
            this.radMenuItem9.Text = "Настройки";
            this.radMenuItem9.Click += new System.EventHandler(this.radMenuItem9_Click);
            // 
            // radMenuItem11
            // 
            this.radMenuItem11.AccessibleDescription = "Тема";
            this.radMenuItem11.AccessibleName = "Тема";
            this.radMenuItem11.Name = "radMenuItem11";
            this.radMenuItem11.Text = "Тема";
            // 
            // ddlTheme
            // 
            this.ddlTheme.AccessibleDescription = "radMenuComboItem1";
            this.ddlTheme.AccessibleName = "radMenuComboItem1";
            // 
            // 
            // 
            this.ddlTheme.ComboBoxElement.ArrowButtonMinWidth = 17;
            this.ddlTheme.ComboBoxElement.AutoCompleteAppend = null;
            this.ddlTheme.ComboBoxElement.AutoCompleteDataSource = null;
            this.ddlTheme.ComboBoxElement.AutoCompleteSuggest = null;
            this.ddlTheme.ComboBoxElement.DataMember = "";
            this.ddlTheme.ComboBoxElement.DataSource = null;
            this.ddlTheme.ComboBoxElement.DefaultValue = null;
            this.ddlTheme.ComboBoxElement.DisplayMember = "";
            this.ddlTheme.ComboBoxElement.DropDownAnimationEasing = Telerik.WinControls.RadEasingType.InQuad;
            this.ddlTheme.ComboBoxElement.DropDownAnimationEnabled = true;
            this.ddlTheme.ComboBoxElement.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlTheme.ComboBoxElement.EditableElementText = "";
            this.ddlTheme.ComboBoxElement.EditorElement = this.ddlTheme.ComboBoxElement;
            this.ddlTheme.ComboBoxElement.EditorManager = null;
            this.ddlTheme.ComboBoxElement.Filter = null;
            this.ddlTheme.ComboBoxElement.FilterExpression = "";
            this.ddlTheme.ComboBoxElement.Focusable = true;
            this.ddlTheme.ComboBoxElement.FormatString = "";
            this.ddlTheme.ComboBoxElement.FormattingEnabled = true;
            this.ddlTheme.ComboBoxElement.ItemHeight = 18;
            this.ddlTheme.ComboBoxElement.MaxDropDownItems = 0;
            this.ddlTheme.ComboBoxElement.MaxLength = 32767;
            this.ddlTheme.ComboBoxElement.MaxValue = null;
            this.ddlTheme.ComboBoxElement.MinValue = null;
            this.ddlTheme.ComboBoxElement.NullValue = null;
            this.ddlTheme.ComboBoxElement.OwnerOffset = 0;
            this.ddlTheme.ComboBoxElement.ShowImageInEditorArea = true;
            this.ddlTheme.ComboBoxElement.SortStyle = Telerik.WinControls.Enumerations.SortStyle.None;
            this.ddlTheme.ComboBoxElement.Value = null;
            this.ddlTheme.ComboBoxElement.ValueMember = "";
            this.ddlTheme.ComboBoxElement.SelectedValueChanged += new Telerik.WinControls.UI.Data.ValueChangedEventHandler(this.radMenuComboItem1_ComboBoxElement_SelectedValueChanged);
            this.ddlTheme.Name = "ddlTheme";
            this.ddlTheme.Text = "radMenuComboItem1";
            ((Telerik.WinControls.UI.RadDropDownListElement)(this.ddlTheme.GetChildAt(3))).DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            // 
            // radMenuItem13
            // 
            this.radMenuItem13.AccessibleDescription = "О программе";
            this.radMenuItem13.AccessibleName = "О программе";
            this.radMenuItem13.Name = "radMenuItem13";
            this.radMenuItem13.Text = "О программе";
            this.radMenuItem13.Click += new System.EventHandler(this.radMenuItem13_Click);
            // 
            // radMenuItem12
            // 
            this.radMenuItem12.AccessibleDescription = "Выход";
            this.radMenuItem12.AccessibleName = "Выход";
            this.radMenuItem12.Name = "radMenuItem12";
            this.radMenuItem12.Text = "Выход";
            this.radMenuItem12.Click += new System.EventHandler(this.radMenuItem12_Click);
            // 
            // tabForms
            // 
            this.tabForms.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabForms.Location = new System.Drawing.Point(0, 20);
            this.tabForms.Name = "tabForms";
            this.tabForms.Size = new System.Drawing.Size(739, 41);
            this.tabForms.TabIndex = 3;
            this.tabForms.Text = "radPageView1";
            this.tabForms.Visible = false;
            this.tabForms.SelectedPageChanged += new System.EventHandler(this.tabForms_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.tabForms.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // radMenu1
            // 
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1,
            this.radMenuItem5,
            this.radMenuItem9,
            this.radMenuItem13,
            this.radMenuItem12});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(739, 20);
            this.radMenu1.TabIndex = 2;
            this.radMenu1.Text = "radMenu1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 499);
            this.Controls.Add(this.tabForms);
            this.Controls.Add(this.radMenu1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ЕСН Админ 1.0.1.7";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MdiChildActivate += new System.EventHandler(this.MainForm_MdiChildActivate);
            ((System.ComponentModel.ISupportInitialize)(this.ddlTheme.ComboBoxElement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabForms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadPageView tabForms;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem5;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem9;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem11;
        private Telerik.WinControls.UI.RadMenuComboItem ddlTheme;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem12;
        private Telerik.WinControls.UI.RadMenuSeparatorItem radMenuSeparatorItem1;
        private Telerik.WinControls.Themes.AquaTheme aquaTheme1;
        private Telerik.WinControls.Themes.BreezeTheme breezeTheme1;
        private Telerik.WinControls.Themes.DesertTheme desertTheme1;
        private Telerik.WinControls.Themes.HighContrastBlackTheme highContrastBlackTheme1;
        private Telerik.WinControls.Themes.Office2007BlackTheme office2007BlackTheme1;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private Telerik.WinControls.Themes.Office2010BlackTheme office2010BlackTheme1;
        private Telerik.WinControls.Themes.Office2010BlueTheme office2010BlueTheme1;
        private Telerik.WinControls.Themes.Office2010SilverTheme office2010SilverTheme1;
        private Telerik.WinControls.Themes.Office2013DarkTheme office2013DarkTheme1;
        private Telerik.WinControls.Themes.Office2013LightTheme office2013LightTheme1;
        private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private Telerik.WinControls.Themes.TelerikMetroBlueTheme telerikMetroBlueTheme1;
        private Telerik.WinControls.Themes.TelerikMetroTouchTheme telerikMetroTouchTheme1;
        private Telerik.WinControls.Themes.VisualStudio2012DarkTheme visualStudio2012DarkTheme1;
        private Telerik.WinControls.Themes.VisualStudio2012LightTheme visualStudio2012LightTheme1;
        private Telerik.WinControls.Themes.Windows7Theme windows7Theme1;
        private Telerik.WinControls.Themes.Windows8Theme windows8Theme1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem13;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem14;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem15;
        private Telerik.WinControls.UI.RadMenu radMenu1;

    }
}