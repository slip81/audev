﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Threading;
using System.Configuration;
using System.IO;


namespace EsnAdmin
{
    public partial class LoginForm : RadFormExt
    {
        public string session_id
        {
            get
            {
                return _session_id;
            }
        }
        private string _session_id;

        public string user_name
        {
            get
            {
                return _user_name;
            }
        }
        private string _user_name;

        public string db_name
        {
            get
            {
                return _db_name;
            }
        }
        private string _db_name;

        private Dictionary<string, string> users { get; set; }

        public LoginForm()
        {
            InitializeComponent();       
            /*
            //
            string login = Properties.Settings.Default.curr_login;
            if (!String.IsNullOrEmpty(login))
            {
                txtUser.Text = login;
            }
            else
            {
                txtUser.Text = ConfigurationManager.AppSettings.Get("def_login");
            }
            //            
            List<KeyValuePair<string, string>> ds = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("work_db", ConfigurationManager.AppSettings.Get("work_db")),
                new KeyValuePair<string, string>("test_db", ConfigurationManager.AppSettings.Get("test_db")),
            }
            ;
            ddlDb.DataSource = ds;
            ddlDb.ValueMember = "Key";
            ddlDb.DisplayMember = "Value";            
            //
            string db = Properties.Settings.Default.curr_db;
            if ((!String.IsNullOrEmpty(db)) && (!String.IsNullOrEmpty(ds.Where(ss => ss.Key == db).Select(ss => ss.Key).FirstOrDefault())))
            {
                ddlDb.SelectedValue = db;
            }
            */
            users = new Dictionary<string, string>();
            users.Add("pavlov", "123");
            users.Add("kov", "123");
            users.Add("son", "123");
            users.Add("test", "123");
            users.Add("esnadmin", "123");
        }

        private void LoginForm_Shown(object sender, EventArgs e)
        {
            txtUser.Focus();
            //txtPwd.Focus();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Login(txtUser.Text, txtPwd.Text);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void Login(string user_name, string user_pwd)
        {
            //Thread.Sleep(2000);
            bool ok = false;
            if (users.ContainsKey(user_name))
            {
                ok = users[user_name] == user_pwd;
            }
            if (!ok)
            {
                MessageBox.Show("Неверный логин-пароль !");
                return;
            }
            AppManager.CreateUser(user_name);
            this.DialogResult = ok ? System.Windows.Forms.DialogResult.OK : System.Windows.Forms.DialogResult.Cancel;
        }


    }
}
