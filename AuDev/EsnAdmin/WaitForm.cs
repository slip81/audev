﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace EsnAdmin
{
    public partial class WaitForm : RadFormExt
    {
        private System.Timers.Timer timerWait = null;
        private int currStepNum = 0;

        public bool WaitDone { get; set; }

        public WaitForm()
        {
            InitializeComponent();
            //
            InitForm();
        }

        private void InitForm()
        {
            WaitDone = false;
            currStepNum = 0;
            //
            lblWait.Text = "Загрузка данных...";
            //
            prgWait.Maximum = 80;
            prgWait.Minimum = 0;
            prgWait.Value1 = 0;
            prgWait.Value2 = 0;
            prgWait.Step = 1;
            //
            timerWait = new System.Timers.Timer();
            timerWait.Elapsed += timerWaitElapsed;
            timerWait.Interval = 500; // every 0.5 second
            timerWait.AutoReset = false; // makes it fire only once 
            timerWait.Enabled = false;
            timerWait.Enabled = true;            
        }

        private void timerWaitElapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            /*
            if (currStepNum == 78)
            {
                WaitDone = true;
            }
            */

            if ((!WaitDone) && (currStepNum >= prgWait.Maximum - 2))
            {
                currStepNum = prgWait.Maximum - 2;
            }

            prgWait.Value1 = currStepNum <= prgWait.Maximum ? currStepNum : prgWait.Maximum;
            if (WaitDone)
            {
                prgWait.Value1 = prgWait.Maximum;
                timerWait.Enabled = false;
                //lblWait.Text = "Загрузка завершена";
                setLabelText(lblWait, "Загрузка завершена");
            }
            else
            {
                timerWait.Start(); // re-enables the timer
            }
            currStepNum = currStepNum + 1;
        }

        
        private void setLabelText(RadLabel control, string text)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke(new Action<RadLabel, string>(setLabelText), new object[] { control, text });
                return;
            }
            control.Text = text;
        }
        

        private void WaitForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
