﻿namespace EsnAdmin
{
    partial class WaitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.prgWait = new Telerik.WinControls.UI.RadProgressBar();
            this.lblWait = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.prgWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWait)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // prgWait
            // 
            this.prgWait.Location = new System.Drawing.Point(12, 56);
            this.prgWait.Name = "prgWait";
            this.prgWait.Size = new System.Drawing.Size(315, 24);
            this.prgWait.TabIndex = 0;
            // 
            // lblWait
            // 
            this.lblWait.Location = new System.Drawing.Point(12, 32);
            this.lblWait.Name = "lblWait";
            this.lblWait.Size = new System.Drawing.Size(100, 18);
            this.lblWait.TabIndex = 1;
            this.lblWait.Text = "Загрузка данных...";
            // 
            // WaitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 136);
            this.ControlBox = false;
            this.Controls.Add(this.lblWait);
            this.Controls.Add(this.prgWait);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "WaitForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Пожалуйста подождите...";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WaitForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.prgWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWait)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadProgressBar prgWait;
        private Telerik.WinControls.UI.RadLabel lblWait;
    }
}
