﻿namespace EsnAdmin
{
    partial class MatchingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MatchingForm));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem1 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn18 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn19 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor3 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn20 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn1 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn21 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn22 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn23 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewCheckBoxColumn gridViewCheckBoxColumn2 = new Telerik.WinControls.UI.GridViewCheckBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn24 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn25 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn26 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn27 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn28 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn29 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn30 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn31 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn32 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor4 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem2 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn33 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn34 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn35 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn36 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn37 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn38 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn39 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn40 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor5 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.pageMatch = new Telerik.WinControls.UI.RadPageView();
            this.tabMatchDocSelect = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.gridDoc = new Telerik.WinControls.UI.RadGridView();
            this.radPanel6 = new Telerik.WinControls.UI.RadPanel();
            this.btnSelectDoc = new Telerik.WinControls.UI.RadButton();
            this.btnImportDoc = new Telerik.WinControls.UI.RadButton();
            this.radPanel5 = new Telerik.WinControls.UI.RadPanel();
            this.prgImportDoc = new Telerik.WinControls.UI.RadProgressBar();
            this.grbImportDoc = new Telerik.WinControls.UI.RadGroupBox();
            this.btnImportConfigEdit = new Telerik.WinControls.UI.RadButton();
            this.btnImportConfigAdd = new Telerik.WinControls.UI.RadButton();
            this.btnImportStart = new Telerik.WinControls.UI.RadButton();
            this.ddlImportConfig = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.edImportFile = new Telerik.WinControls.UI.RadBrowseEditor();
            this.tabMatchDoc = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.txtEtalonMfr = new Telerik.WinControls.UI.RadTextBox();
            this.txtEtalonBarcode = new Telerik.WinControls.UI.RadTextBox();
            this.txtMatchMfr = new Telerik.WinControls.UI.RadTextBox();
            this.txtEtalonName = new Telerik.WinControls.UI.RadTextBox();
            this.txtMatchName = new Telerik.WinControls.UI.RadTextBox();
            this.txtMatchBarcode = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.btnCancelAll = new Telerik.WinControls.UI.RadButton();
            this.btnMatchToNew = new Telerik.WinControls.UI.RadDropDownButton();
            this.btnMatchAsIsToNew = new Telerik.WinControls.UI.RadMenuItem();
            this.btnMatchChangeToNew = new Telerik.WinControls.UI.RadMenuItem();
            this.btnMatchToSyn = new Telerik.WinControls.UI.RadDropDownButton();
            this.btnMatchAsIsToSyn = new Telerik.WinControls.UI.RadMenuItem();
            this.btnMatchChangeToSyn = new Telerik.WinControls.UI.RadMenuItem();
            this.btnMatchClear = new Telerik.WinControls.UI.RadButton();
            this.btnSaveAll = new Telerik.WinControls.UI.RadButton();
            this.chbUnapprovedOnly = new Telerik.WinControls.UI.RadCheckBox();
            this.chbMatchNew = new Telerik.WinControls.UI.RadCheckBox();
            this.chbMatchSyn = new Telerik.WinControls.UI.RadCheckBox();
            this.radLabel11 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel10 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel9 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel8 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel7 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.txtSelectedDoc = new Telerik.WinControls.UI.RadTextBox();
            this.chbUnmatchOnly = new Telerik.WinControls.UI.RadCheckBox();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.btnEtalonDel = new Telerik.WinControls.UI.RadButton();
            this.btnEtalonEdit = new Telerik.WinControls.UI.RadButton();
            this.btnEtalonAdd = new Telerik.WinControls.UI.RadButton();
            this.chbShowVariant = new Telerik.WinControls.UI.RadCheckBox();
            this.btnAutoMatch = new Telerik.WinControls.UI.RadButton();
            this.btnEtalonFilterReset = new Telerik.WinControls.UI.RadButton();
            this.btnEtalonFilterApply = new Telerik.WinControls.UI.RadButton();
            this.radLabel6 = new Telerik.WinControls.UI.RadLabel();
            this.txtEtalonBarcodeFilter = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.txtEtalonMfrFilter = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.txtEtalonNameFilter = new Telerik.WinControls.UI.RadTextBox();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.gridEtalon = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.gridEtalonGood = new Telerik.WinControls.UI.RadGridView();
            this.radPanel8 = new Telerik.WinControls.UI.RadPanel();
            this.btnSynDel = new Telerik.WinControls.UI.RadButton();
            this.btnSynEdit = new Telerik.WinControls.UI.RadButton();
            this.btnSynAdd = new Telerik.WinControls.UI.RadButton();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.gridMatch = new Telerik.WinControls.UI.RadGridView();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.lblMatchCheckedCnt = new Telerik.WinControls.UI.RadLabel();
            this.lblMatchSelectedCnt = new Telerik.WinControls.UI.RadLabel();
            this.splitPanel6 = new Telerik.WinControls.UI.SplitPanel();
            this.pageMatchVariant = new Telerik.WinControls.UI.RadPageView();
            this.tabMatchVariant = new Telerik.WinControls.UI.RadPageViewPage();
            this.gridMatchVariant = new Telerik.WinControls.UI.RadGridView();
            this.radColorDialog1 = new Telerik.WinControls.RadColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageMatch)).BeginInit();
            this.pageMatch.SuspendLayout();
            this.tabMatchDocSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDoc.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).BeginInit();
            this.radPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).BeginInit();
            this.radPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prgImportDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbImportDoc)).BeginInit();
            this.grbImportDoc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportConfigEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportConfigAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlImportConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportFile)).BeginInit();
            this.tabMatchDoc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonMfr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonBarcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatchMfr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatchName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatchBarcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCancelAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMatchToNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMatchToSyn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMatchClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbUnapprovedOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbMatchNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbMatchSyn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSelectedDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbUnmatchOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbShowVariant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAutoMatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonFilterReset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonFilterApply)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonBarcodeFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonMfrFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonNameFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalon.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).BeginInit();
            this.radPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSynDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSynEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSynAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            this.splitPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatch.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMatchCheckedCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMatchSelectedCnt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).BeginInit();
            this.splitPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pageMatchVariant)).BeginInit();
            this.pageMatchVariant.SuspendLayout();
            this.tabMatchVariant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatchVariant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatchVariant.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.BackColor = System.Drawing.Color.Gainsboro;
            this.radGroupBox2.Controls.Add(this.pageMatch);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox2.HeaderText = "Обрабатываемый справочник";
            this.radGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(666, 238);
            this.radGroupBox2.TabIndex = 1;
            this.radGroupBox2.Text = "Обрабатываемый справочник";
            // 
            // pageMatch
            // 
            this.pageMatch.Controls.Add(this.tabMatchDocSelect);
            this.pageMatch.Controls.Add(this.tabMatchDoc);
            this.pageMatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageMatch.Location = new System.Drawing.Point(2, 18);
            this.pageMatch.Name = "pageMatch";
            this.pageMatch.SelectedPage = this.tabMatchDocSelect;
            this.pageMatch.Size = new System.Drawing.Size(662, 218);
            this.pageMatch.TabIndex = 10;
            this.pageMatch.Text = "Обрабатываемый справочник";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pageMatch.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // tabMatchDocSelect
            // 
            this.tabMatchDocSelect.BackColor = System.Drawing.Color.Gainsboro;
            this.tabMatchDocSelect.Controls.Add(this.radPanel4);
            this.tabMatchDocSelect.Controls.Add(this.grbImportDoc);
            this.tabMatchDocSelect.ItemSize = new System.Drawing.SizeF(108F, 28F);
            this.tabMatchDocSelect.Location = new System.Drawing.Point(10, 37);
            this.tabMatchDocSelect.Name = "tabMatchDocSelect";
            this.tabMatchDocSelect.Size = new System.Drawing.Size(641, 170);
            this.tabMatchDocSelect.Text = "Выбор документа";
            // 
            // radPanel4
            // 
            this.radPanel4.Controls.Add(this.gridDoc);
            this.radPanel4.Controls.Add(this.radPanel6);
            this.radPanel4.Controls.Add(this.radPanel5);
            this.radPanel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.radPanel4.Location = new System.Drawing.Point(0, 0);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(383, 170);
            this.radPanel4.TabIndex = 7;
            // 
            // gridDoc
            // 
            this.gridDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridDoc.Location = new System.Drawing.Point(0, 27);
            // 
            // 
            // 
            this.gridDoc.MasterTemplate.AllowAddNewRow = false;
            this.gridDoc.MasterTemplate.AllowDeleteRow = false;
            this.gridDoc.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.AllowFiltering = false;
            gridViewTextBoxColumn1.AllowGroup = false;
            gridViewTextBoxColumn1.FieldName = "doc_id";
            gridViewTextBoxColumn1.HeaderText = "Код";
            gridViewTextBoxColumn1.IsVisible = false;
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Descending;
            gridViewTextBoxColumn1.Width = 70;
            gridViewTextBoxColumn2.AllowFiltering = false;
            gridViewTextBoxColumn2.AllowGroup = false;
            gridViewTextBoxColumn2.FieldName = "doc_name";
            gridViewTextBoxColumn2.HeaderText = "Название";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 200;
            gridViewTextBoxColumn3.AllowFiltering = false;
            gridViewTextBoxColumn3.AllowGroup = false;
            gridViewTextBoxColumn3.FieldName = "doc_state_name";
            gridViewTextBoxColumn3.HeaderText = "Статус";
            gridViewTextBoxColumn3.Name = "column7";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 100;
            gridViewTextBoxColumn4.AllowFiltering = false;
            gridViewTextBoxColumn4.AllowGroup = false;
            gridViewTextBoxColumn4.FieldName = "row_cnt";
            gridViewTextBoxColumn4.HeaderText = "Строк всего";
            gridViewTextBoxColumn4.Name = "column5";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 80;
            gridViewTextBoxColumn5.AllowFiltering = false;
            gridViewTextBoxColumn5.AllowGroup = false;
            gridViewTextBoxColumn5.FieldName = "unmatch_cnt";
            gridViewTextBoxColumn5.HeaderText = "Несопоставлено";
            gridViewTextBoxColumn5.Name = "column6";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 80;
            gridViewTextBoxColumn6.AllowFiltering = false;
            gridViewTextBoxColumn6.AllowGroup = false;
            gridViewTextBoxColumn6.FieldName = "crt_date";
            gridViewTextBoxColumn6.HeaderText = "Дата";
            gridViewTextBoxColumn6.Name = "column3";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 150;
            gridViewTextBoxColumn7.AllowFiltering = false;
            gridViewTextBoxColumn7.AllowGroup = false;
            gridViewTextBoxColumn7.FieldName = "partner_name";
            gridViewTextBoxColumn7.HeaderText = "Источник";
            gridViewTextBoxColumn7.Name = "column4";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.Width = 150;
            this.gridDoc.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7});
            this.gridDoc.MasterTemplate.EnableGrouping = false;
            sortDescriptor1.Direction = System.ComponentModel.ListSortDirection.Descending;
            sortDescriptor1.PropertyName = "column1";
            this.gridDoc.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            this.gridDoc.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridDoc.Name = "gridDoc";
            this.gridDoc.ReadOnly = true;
            this.gridDoc.Size = new System.Drawing.Size(383, 113);
            this.gridDoc.TabIndex = 7;
            this.gridDoc.Text = "radGridView1";
            this.gridDoc.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridDoc_CellDoubleClick);
            // 
            // radPanel6
            // 
            this.radPanel6.BackColor = System.Drawing.Color.Gainsboro;
            this.radPanel6.Controls.Add(this.btnSelectDoc);
            this.radPanel6.Controls.Add(this.btnImportDoc);
            this.radPanel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel6.Location = new System.Drawing.Point(0, 140);
            this.radPanel6.Name = "radPanel6";
            this.radPanel6.Size = new System.Drawing.Size(383, 30);
            this.radPanel6.TabIndex = 2;
            // 
            // btnSelectDoc
            // 
            this.btnSelectDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectDoc.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectDoc.Image")));
            this.btnSelectDoc.Location = new System.Drawing.Point(3, 3);
            this.btnSelectDoc.Name = "btnSelectDoc";
            this.btnSelectDoc.Size = new System.Drawing.Size(141, 24);
            this.btnSelectDoc.TabIndex = 2;
            this.btnSelectDoc.Text = "Выбрать документ";
            this.btnSelectDoc.Click += new System.EventHandler(this.btnSelectDoc_Click);
            // 
            // btnImportDoc
            // 
            this.btnImportDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImportDoc.Image = ((System.Drawing.Image)(resources.GetObject("btnImportDoc.Image")));
            this.btnImportDoc.Location = new System.Drawing.Point(239, 3);
            this.btnImportDoc.Name = "btnImportDoc";
            this.btnImportDoc.Size = new System.Drawing.Size(141, 24);
            this.btnImportDoc.TabIndex = 3;
            this.btnImportDoc.Text = "Загрузить новый";
            this.btnImportDoc.Click += new System.EventHandler(this.btnImportDoc_Click);
            // 
            // radPanel5
            // 
            this.radPanel5.BackColor = System.Drawing.Color.Gainsboro;
            this.radPanel5.Controls.Add(this.prgImportDoc);
            this.radPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel5.Location = new System.Drawing.Point(0, 0);
            this.radPanel5.Name = "radPanel5";
            this.radPanel5.Size = new System.Drawing.Size(383, 27);
            this.radPanel5.TabIndex = 2;
            // 
            // prgImportDoc
            // 
            this.prgImportDoc.Dock = System.Windows.Forms.DockStyle.Top;
            this.prgImportDoc.Location = new System.Drawing.Point(0, 0);
            this.prgImportDoc.Maximum = 10000;
            this.prgImportDoc.Name = "prgImportDoc";
            this.prgImportDoc.Size = new System.Drawing.Size(383, 24);
            this.prgImportDoc.TabIndex = 6;
            this.prgImportDoc.Visible = false;
            // 
            // grbImportDoc
            // 
            this.grbImportDoc.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.grbImportDoc.BackColor = System.Drawing.Color.Gainsboro;
            this.grbImportDoc.Controls.Add(this.btnImportConfigEdit);
            this.grbImportDoc.Controls.Add(this.btnImportConfigAdd);
            this.grbImportDoc.Controls.Add(this.btnImportStart);
            this.grbImportDoc.Controls.Add(this.ddlImportConfig);
            this.grbImportDoc.Controls.Add(this.radLabel2);
            this.grbImportDoc.Controls.Add(this.radLabel3);
            this.grbImportDoc.Controls.Add(this.edImportFile);
            this.grbImportDoc.HeaderText = "Загрузка из файла";
            this.grbImportDoc.Location = new System.Drawing.Point(389, 3);
            this.grbImportDoc.Name = "grbImportDoc";
            this.grbImportDoc.Size = new System.Drawing.Size(249, 124);
            this.grbImportDoc.TabIndex = 5;
            this.grbImportDoc.Text = "Загрузка из файла";
            this.grbImportDoc.Visible = false;
            // 
            // btnImportConfigEdit
            // 
            this.btnImportConfigEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnImportConfigEdit.Image")));
            this.btnImportConfigEdit.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnImportConfigEdit.Location = new System.Drawing.Point(195, 63);
            this.btnImportConfigEdit.Name = "btnImportConfigEdit";
            this.btnImportConfigEdit.Size = new System.Drawing.Size(19, 19);
            this.btnImportConfigEdit.TabIndex = 6;
            this.btnImportConfigEdit.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnImportConfigEdit_ToolTipTextNeeded);
            this.btnImportConfigEdit.Click += new System.EventHandler(this.btnImportConfigEdit_Click);
            // 
            // btnImportConfigAdd
            // 
            this.btnImportConfigAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnImportConfigAdd.Image")));
            this.btnImportConfigAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnImportConfigAdd.Location = new System.Drawing.Point(220, 63);
            this.btnImportConfigAdd.Name = "btnImportConfigAdd";
            this.btnImportConfigAdd.Size = new System.Drawing.Size(19, 19);
            this.btnImportConfigAdd.TabIndex = 5;
            this.btnImportConfigAdd.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnImportConfigAdd_ToolTipTextNeeded);
            this.btnImportConfigAdd.Click += new System.EventHandler(this.btnImportConfigAdd_Click);
            // 
            // btnImportStart
            // 
            this.btnImportStart.Image = ((System.Drawing.Image)(resources.GetObject("btnImportStart.Image")));
            this.btnImportStart.Location = new System.Drawing.Point(109, 88);
            this.btnImportStart.Name = "btnImportStart";
            this.btnImportStart.Size = new System.Drawing.Size(130, 24);
            this.btnImportStart.TabIndex = 4;
            this.btnImportStart.Text = "Начать загрузку";
            this.btnImportStart.Click += new System.EventHandler(this.btnImportStart_Click);
            // 
            // ddlImportConfig
            // 
            this.ddlImportConfig.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlImportConfig.Location = new System.Drawing.Point(99, 42);
            this.ddlImportConfig.Name = "ddlImportConfig";
            this.ddlImportConfig.Size = new System.Drawing.Size(140, 20);
            this.ddlImportConfig.TabIndex = 3;
            this.ddlImportConfig.Text = "radDropDownList1";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(7, 45);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(84, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Конфигурация:";
            // 
            // radLabel3
            // 
            this.radLabel3.Location = new System.Drawing.Point(7, 16);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(35, 18);
            this.radLabel3.TabIndex = 1;
            this.radLabel3.Text = "Файл:";
            // 
            // edImportFile
            // 
            this.edImportFile.Location = new System.Drawing.Point(99, 16);
            this.edImportFile.Name = "edImportFile";
            this.edImportFile.Size = new System.Drawing.Size(140, 20);
            this.edImportFile.TabIndex = 0;
            this.edImportFile.Text = "radBrowseEditor1";
            // 
            // tabMatchDoc
            // 
            this.tabMatchDoc.BackColor = System.Drawing.Color.Gainsboro;
            this.tabMatchDoc.Controls.Add(this.radPanel3);
            this.tabMatchDoc.Controls.Add(this.radPanel1);
            this.tabMatchDoc.Controls.Add(this.chbUnapprovedOnly);
            this.tabMatchDoc.Controls.Add(this.chbMatchNew);
            this.tabMatchDoc.Controls.Add(this.chbMatchSyn);
            this.tabMatchDoc.Controls.Add(this.radLabel11);
            this.tabMatchDoc.Controls.Add(this.radLabel10);
            this.tabMatchDoc.Controls.Add(this.radLabel9);
            this.tabMatchDoc.Controls.Add(this.radLabel8);
            this.tabMatchDoc.Controls.Add(this.radLabel7);
            this.tabMatchDoc.Controls.Add(this.radLabel1);
            this.tabMatchDoc.Controls.Add(this.txtSelectedDoc);
            this.tabMatchDoc.Controls.Add(this.chbUnmatchOnly);
            this.tabMatchDoc.ItemSize = new System.Drawing.SizeF(130F, 28F);
            this.tabMatchDoc.Location = new System.Drawing.Point(10, 37);
            this.tabMatchDoc.Name = "tabMatchDoc";
            this.tabMatchDoc.Size = new System.Drawing.Size(641, 170);
            this.tabMatchDoc.Text = "Выбранный документ";
            // 
            // radPanel3
            // 
            this.radPanel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radPanel3.Controls.Add(this.txtEtalonMfr);
            this.radPanel3.Controls.Add(this.txtEtalonBarcode);
            this.radPanel3.Controls.Add(this.txtMatchMfr);
            this.radPanel3.Controls.Add(this.txtEtalonName);
            this.radPanel3.Controls.Add(this.txtMatchName);
            this.radPanel3.Controls.Add(this.txtMatchBarcode);
            this.radPanel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel3.Location = new System.Drawing.Point(0, 91);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(641, 48);
            this.radPanel3.TabIndex = 22;
            // 
            // txtEtalonMfr
            // 
            this.txtEtalonMfr.BackColor = System.Drawing.Color.Gainsboro;
            this.txtEtalonMfr.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Bold);
            this.txtEtalonMfr.Location = new System.Drawing.Point(481, 27);
            this.txtEtalonMfr.Name = "txtEtalonMfr";
            this.txtEtalonMfr.ReadOnly = true;
            this.txtEtalonMfr.Size = new System.Drawing.Size(147, 18);
            this.txtEtalonMfr.TabIndex = 7;
            // 
            // txtEtalonBarcode
            // 
            this.txtEtalonBarcode.BackColor = System.Drawing.Color.Gainsboro;
            this.txtEtalonBarcode.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Bold);
            this.txtEtalonBarcode.Location = new System.Drawing.Point(382, 27);
            this.txtEtalonBarcode.Name = "txtEtalonBarcode";
            this.txtEtalonBarcode.ReadOnly = true;
            this.txtEtalonBarcode.Size = new System.Drawing.Size(93, 18);
            this.txtEtalonBarcode.TabIndex = 6;
            // 
            // txtMatchMfr
            // 
            this.txtMatchMfr.BackColor = System.Drawing.Color.Gainsboro;
            this.txtMatchMfr.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Bold);
            this.txtMatchMfr.Location = new System.Drawing.Point(481, 3);
            this.txtMatchMfr.Name = "txtMatchMfr";
            this.txtMatchMfr.ReadOnly = true;
            this.txtMatchMfr.Size = new System.Drawing.Size(147, 18);
            this.txtMatchMfr.TabIndex = 4;
            // 
            // txtEtalonName
            // 
            this.txtEtalonName.BackColor = System.Drawing.Color.Gainsboro;
            this.txtEtalonName.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Bold);
            this.txtEtalonName.Location = new System.Drawing.Point(4, 27);
            this.txtEtalonName.Name = "txtEtalonName";
            this.txtEtalonName.ReadOnly = true;
            this.txtEtalonName.Size = new System.Drawing.Size(372, 18);
            this.txtEtalonName.TabIndex = 5;
            // 
            // txtMatchName
            // 
            this.txtMatchName.BackColor = System.Drawing.Color.Gainsboro;
            this.txtMatchName.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Bold);
            this.txtMatchName.Location = new System.Drawing.Point(4, 3);
            this.txtMatchName.Name = "txtMatchName";
            this.txtMatchName.ReadOnly = true;
            this.txtMatchName.Size = new System.Drawing.Size(372, 18);
            this.txtMatchName.TabIndex = 2;
            // 
            // txtMatchBarcode
            // 
            this.txtMatchBarcode.BackColor = System.Drawing.Color.Gainsboro;
            this.txtMatchBarcode.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Bold);
            this.txtMatchBarcode.Location = new System.Drawing.Point(382, 3);
            this.txtMatchBarcode.Name = "txtMatchBarcode";
            this.txtMatchBarcode.ReadOnly = true;
            this.txtMatchBarcode.Size = new System.Drawing.Size(93, 18);
            this.txtMatchBarcode.TabIndex = 3;
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.Gainsboro;
            this.radPanel1.Controls.Add(this.btnCancelAll);
            this.radPanel1.Controls.Add(this.btnMatchToNew);
            this.radPanel1.Controls.Add(this.btnMatchToSyn);
            this.radPanel1.Controls.Add(this.btnMatchClear);
            this.radPanel1.Controls.Add(this.btnSaveAll);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 139);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(641, 31);
            this.radPanel1.TabIndex = 1;
            // 
            // btnCancelAll
            // 
            this.btnCancelAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelAll.Location = new System.Drawing.Point(511, 3);
            this.btnCancelAll.Name = "btnCancelAll";
            this.btnCancelAll.Size = new System.Drawing.Size(127, 24);
            this.btnCancelAll.TabIndex = 7;
            this.btnCancelAll.Text = "Отменить изменения";
            this.btnCancelAll.Click += new System.EventHandler(this.btnCancelAll_Click);
            // 
            // btnMatchToNew
            // 
            this.btnMatchToNew.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnMatchAsIsToNew,
            this.btnMatchChangeToNew});
            this.btnMatchToNew.Location = new System.Drawing.Point(112, 3);
            this.btnMatchToNew.Name = "btnMatchToNew";
            this.btnMatchToNew.Size = new System.Drawing.Size(103, 24);
            this.btnMatchToNew.TabIndex = 11;
            this.btnMatchToNew.Text = "+ в эталонные";
            // 
            // btnMatchAsIsToNew
            // 
            this.btnMatchAsIsToNew.AccessibleDescription = "как есть";
            this.btnMatchAsIsToNew.AccessibleName = "как есть";
            this.btnMatchAsIsToNew.Name = "btnMatchAsIsToNew";
            this.btnMatchAsIsToNew.Text = "как есть";
            this.btnMatchAsIsToNew.Click += new System.EventHandler(this.btnMatchAsIsToNew_Click);
            // 
            // btnMatchChangeToNew
            // 
            this.btnMatchChangeToNew.AccessibleDescription = "изменить";
            this.btnMatchChangeToNew.AccessibleName = "изменить";
            this.btnMatchChangeToNew.Name = "btnMatchChangeToNew";
            this.btnMatchChangeToNew.Text = "изменить";
            this.btnMatchChangeToNew.Click += new System.EventHandler(this.btnMatchChangeToNew_Click);
            // 
            // btnMatchToSyn
            // 
            this.btnMatchToSyn.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.btnMatchAsIsToSyn,
            this.btnMatchChangeToSyn});
            this.btnMatchToSyn.Location = new System.Drawing.Point(3, 3);
            this.btnMatchToSyn.Name = "btnMatchToSyn";
            this.btnMatchToSyn.Size = new System.Drawing.Size(103, 24);
            this.btnMatchToSyn.TabIndex = 10;
            this.btnMatchToSyn.Text = "+ в синонимы";
            // 
            // btnMatchAsIsToSyn
            // 
            this.btnMatchAsIsToSyn.AccessibleDescription = "как есть";
            this.btnMatchAsIsToSyn.AccessibleName = "как есть";
            this.btnMatchAsIsToSyn.Name = "btnMatchAsIsToSyn";
            this.btnMatchAsIsToSyn.Text = "как есть";
            this.btnMatchAsIsToSyn.Click += new System.EventHandler(this.btnMatchAsIsToSyn_Click);
            // 
            // btnMatchChangeToSyn
            // 
            this.btnMatchChangeToSyn.AccessibleDescription = "изменить";
            this.btnMatchChangeToSyn.AccessibleName = "изменить";
            this.btnMatchChangeToSyn.Name = "btnMatchChangeToSyn";
            this.btnMatchChangeToSyn.Text = "изменить";
            this.btnMatchChangeToSyn.Click += new System.EventHandler(this.btnMatchChangeToSyn_Click);
            // 
            // btnMatchClear
            // 
            this.btnMatchClear.Location = new System.Drawing.Point(221, 3);
            this.btnMatchClear.Name = "btnMatchClear";
            this.btnMatchClear.Size = new System.Drawing.Size(103, 24);
            this.btnMatchClear.TabIndex = 9;
            this.btnMatchClear.Text = "- соспоставление";
            this.btnMatchClear.Click += new System.EventHandler(this.btnMatchClear_Click);
            // 
            // btnSaveAll
            // 
            this.btnSaveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveAll.Location = new System.Drawing.Point(378, 3);
            this.btnSaveAll.Name = "btnSaveAll";
            this.btnSaveAll.Size = new System.Drawing.Size(127, 24);
            this.btnSaveAll.TabIndex = 6;
            this.btnSaveAll.Text = "Сохранить отмеченное";
            this.btnSaveAll.Click += new System.EventHandler(this.btnSaveAll_Click);
            // 
            // chbUnapprovedOnly
            // 
            this.chbUnapprovedOnly.Location = new System.Drawing.Point(411, 63);
            this.chbUnapprovedOnly.Name = "chbUnapprovedOnly";
            this.chbUnapprovedOnly.Size = new System.Drawing.Size(15, 15);
            this.chbUnapprovedOnly.TabIndex = 20;
            this.chbUnapprovedOnly.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbUnmatchOnly_ToggleStateChanged);
            // 
            // chbMatchNew
            // 
            this.chbMatchNew.Location = new System.Drawing.Point(411, 41);
            this.chbMatchNew.Name = "chbMatchNew";
            this.chbMatchNew.Size = new System.Drawing.Size(15, 15);
            this.chbMatchNew.TabIndex = 19;
            this.chbMatchNew.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbUnmatchOnly_ToggleStateChanged);
            // 
            // chbMatchSyn
            // 
            this.chbMatchSyn.Location = new System.Drawing.Point(411, 24);
            this.chbMatchSyn.Name = "chbMatchSyn";
            this.chbMatchSyn.Size = new System.Drawing.Size(15, 15);
            this.chbMatchSyn.TabIndex = 18;
            this.chbMatchSyn.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbUnmatchOnly_ToggleStateChanged);
            // 
            // radLabel11
            // 
            this.radLabel11.Location = new System.Drawing.Point(545, 60);
            this.radLabel11.Name = "radLabel11";
            this.radLabel11.Size = new System.Drawing.Size(88, 18);
            this.radLabel11.TabIndex = 17;
            this.radLabel11.Text = "несохраненные";
            this.radLabel11.Click += new System.EventHandler(this.radLabel10_Click);
            // 
            // radLabel10
            // 
            this.radLabel10.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radLabel10.Location = new System.Drawing.Point(428, 60);
            this.radLabel10.Name = "radLabel10";
            this.radLabel10.Size = new System.Drawing.Size(103, 18);
            this.radLabel10.TabIndex = 16;
            this.radLabel10.Text = "жирным шритом";
            this.radLabel10.Click += new System.EventHandler(this.radLabel10_Click);
            // 
            // radLabel9
            // 
            this.radLabel9.Location = new System.Drawing.Point(498, 41);
            this.radLabel9.Name = "radLabel9";
            this.radLabel9.Size = new System.Drawing.Size(137, 18);
            this.radLabel9.TabIndex = 15;
            this.radLabel9.Text = "сопоставленные (эталон)";
            this.radLabel9.Click += new System.EventHandler(this.radLabel7_Click);
            // 
            // radLabel8
            // 
            this.radLabel8.Location = new System.Drawing.Point(487, 24);
            this.radLabel8.Name = "radLabel8";
            this.radLabel8.Size = new System.Drawing.Size(148, 18);
            this.radLabel8.TabIndex = 14;
            this.radLabel8.Text = "сопоставленные (синоним)";
            this.radLabel8.Click += new System.EventHandler(this.radLabel1_Click);
            // 
            // radLabel7
            // 
            this.radLabel7.AutoSize = false;
            this.radLabel7.BackColor = System.Drawing.Color.PaleGreen;
            this.radLabel7.Location = new System.Drawing.Point(432, 41);
            this.radLabel7.Name = "radLabel7";
            this.radLabel7.Size = new System.Drawing.Size(43, 15);
            this.radLabel7.TabIndex = 13;
            this.radLabel7.Click += new System.EventHandler(this.radLabel7_Click);
            // 
            // radLabel1
            // 
            this.radLabel1.AutoSize = false;
            this.radLabel1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.radLabel1.Location = new System.Drawing.Point(432, 24);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(43, 15);
            this.radLabel1.TabIndex = 12;
            this.radLabel1.Click += new System.EventHandler(this.radLabel1_Click);
            // 
            // txtSelectedDoc
            // 
            this.txtSelectedDoc.BackColor = System.Drawing.Color.Gainsboro;
            this.txtSelectedDoc.Enabled = false;
            this.txtSelectedDoc.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Bold);
            this.txtSelectedDoc.Location = new System.Drawing.Point(3, 3);
            this.txtSelectedDoc.Name = "txtSelectedDoc";
            this.txtSelectedDoc.ReadOnly = true;
            this.txtSelectedDoc.Size = new System.Drawing.Size(630, 18);
            this.txtSelectedDoc.TabIndex = 1;
            this.txtSelectedDoc.Text = "(документ не выбран)";
            // 
            // chbUnmatchOnly
            // 
            this.chbUnmatchOnly.Location = new System.Drawing.Point(4, 27);
            this.chbUnmatchOnly.Name = "chbUnmatchOnly";
            this.chbUnmatchOnly.Size = new System.Drawing.Size(205, 18);
            this.chbUnmatchOnly.TabIndex = 8;
            this.chbUnmatchOnly.Text = "показать только несопоставленные";
            this.chbUnmatchOnly.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbUnmatchOnly_ToggleStateChanged);
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.BackColor = System.Drawing.Color.Gainsboro;
            this.radGroupBox1.Controls.Add(this.radPanel7);
            this.radGroupBox1.Controls.Add(this.chbShowVariant);
            this.radGroupBox1.Controls.Add(this.btnAutoMatch);
            this.radGroupBox1.Controls.Add(this.btnEtalonFilterReset);
            this.radGroupBox1.Controls.Add(this.btnEtalonFilterApply);
            this.radGroupBox1.Controls.Add(this.radLabel6);
            this.radGroupBox1.Controls.Add(this.txtEtalonBarcodeFilter);
            this.radGroupBox1.Controls.Add(this.radLabel5);
            this.radGroupBox1.Controls.Add(this.txtEtalonMfrFilter);
            this.radGroupBox1.Controls.Add(this.radLabel4);
            this.radGroupBox1.Controls.Add(this.txtEtalonNameFilter);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radGroupBox1.HeaderText = "Эталонный справочник";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(602, 238);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Эталонный справочник";
            // 
            // radPanel7
            // 
            this.radPanel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radPanel7.Controls.Add(this.btnEtalonDel);
            this.radPanel7.Controls.Add(this.btnEtalonEdit);
            this.radPanel7.Controls.Add(this.btnEtalonAdd);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel7.Location = new System.Drawing.Point(2, 201);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(598, 35);
            this.radPanel7.TabIndex = 11;
            // 
            // btnEtalonDel
            // 
            this.btnEtalonDel.Image = ((System.Drawing.Image)(resources.GetObject("btnEtalonDel.Image")));
            this.btnEtalonDel.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEtalonDel.Location = new System.Drawing.Point(75, 5);
            this.btnEtalonDel.Name = "btnEtalonDel";
            this.btnEtalonDel.Size = new System.Drawing.Size(32, 26);
            this.btnEtalonDel.TabIndex = 8;
            this.btnEtalonDel.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnEtalonDel_ToolTipTextNeeded);
            this.btnEtalonDel.Click += new System.EventHandler(this.btnEtalonDel_Click);
            // 
            // btnEtalonEdit
            // 
            this.btnEtalonEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEtalonEdit.Image")));
            this.btnEtalonEdit.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEtalonEdit.Location = new System.Drawing.Point(42, 5);
            this.btnEtalonEdit.Name = "btnEtalonEdit";
            this.btnEtalonEdit.Size = new System.Drawing.Size(27, 26);
            this.btnEtalonEdit.TabIndex = 7;
            this.btnEtalonEdit.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnEtalonEdit_ToolTipTextNeeded);
            this.btnEtalonEdit.Click += new System.EventHandler(this.btnEtalonEdit_Click);
            // 
            // btnEtalonAdd
            // 
            this.btnEtalonAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnEtalonAdd.Image")));
            this.btnEtalonAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEtalonAdd.Location = new System.Drawing.Point(4, 5);
            this.btnEtalonAdd.Name = "btnEtalonAdd";
            this.btnEtalonAdd.Size = new System.Drawing.Size(32, 26);
            this.btnEtalonAdd.TabIndex = 6;
            this.btnEtalonAdd.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnEtalonAdd_ToolTipTextNeeded);
            this.btnEtalonAdd.Click += new System.EventHandler(this.btnEtalonAdd_Click);
            // 
            // chbShowVariant
            // 
            this.chbShowVariant.Location = new System.Drawing.Point(185, 164);
            this.chbShowVariant.Name = "chbShowVariant";
            this.chbShowVariant.Size = new System.Drawing.Size(115, 18);
            this.chbShowVariant.TabIndex = 10;
            this.chbShowVariant.Text = "показать похожие";
            this.chbShowVariant.ToggleStateChanged += new Telerik.WinControls.UI.StateChangedEventHandler(this.chbShowVariant_ToggleStateChanged);
            // 
            // btnAutoMatch
            // 
            this.btnAutoMatch.Location = new System.Drawing.Point(7, 161);
            this.btnAutoMatch.Name = "btnAutoMatch";
            this.btnAutoMatch.Size = new System.Drawing.Size(161, 24);
            this.btnAutoMatch.TabIndex = 9;
            this.btnAutoMatch.Text = "Автоматическая стыковка...";
            this.btnAutoMatch.Click += new System.EventHandler(this.btnAutoMatch_Click);
            // 
            // btnEtalonFilterReset
            // 
            this.btnEtalonFilterReset.Location = new System.Drawing.Point(250, 102);
            this.btnEtalonFilterReset.Name = "btnEtalonFilterReset";
            this.btnEtalonFilterReset.Size = new System.Drawing.Size(115, 24);
            this.btnEtalonFilterReset.TabIndex = 8;
            this.btnEtalonFilterReset.Text = "Сбросить фильтр";
            this.btnEtalonFilterReset.Click += new System.EventHandler(this.btnEtalonFilterReset_Click);
            // 
            // btnEtalonFilterApply
            // 
            this.btnEtalonFilterApply.Location = new System.Drawing.Point(129, 102);
            this.btnEtalonFilterApply.Name = "btnEtalonFilterApply";
            this.btnEtalonFilterApply.Size = new System.Drawing.Size(115, 24);
            this.btnEtalonFilterApply.TabIndex = 7;
            this.btnEtalonFilterApply.Text = "Применить фильтр";
            this.btnEtalonFilterApply.Click += new System.EventHandler(this.btnEtalonFilterApply_Click);
            // 
            // radLabel6
            // 
            this.radLabel6.Location = new System.Drawing.Point(12, 81);
            this.radLabel6.Name = "radLabel6";
            this.radLabel6.Size = new System.Drawing.Size(90, 18);
            this.radLabel6.TabIndex = 5;
            this.radLabel6.Text = "Штрих-код (фр.)";
            // 
            // txtEtalonBarcodeFilter
            // 
            this.txtEtalonBarcodeFilter.Location = new System.Drawing.Point(129, 79);
            this.txtEtalonBarcodeFilter.Name = "txtEtalonBarcodeFilter";
            this.txtEtalonBarcodeFilter.Size = new System.Drawing.Size(273, 20);
            this.txtEtalonBarcodeFilter.TabIndex = 4;
            this.txtEtalonBarcodeFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEtalonNameFilter_KeyDown);
            // 
            // radLabel5
            // 
            this.radLabel5.Location = new System.Drawing.Point(12, 55);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(114, 18);
            this.radLabel5.TabIndex = 3;
            this.radLabel5.Text = "Производитель (фр.)";
            // 
            // txtEtalonMfrFilter
            // 
            this.txtEtalonMfrFilter.Location = new System.Drawing.Point(129, 53);
            this.txtEtalonMfrFilter.Name = "txtEtalonMfrFilter";
            this.txtEtalonMfrFilter.Size = new System.Drawing.Size(273, 20);
            this.txtEtalonMfrFilter.TabIndex = 2;
            this.txtEtalonMfrFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEtalonNameFilter_KeyDown);
            // 
            // radLabel4
            // 
            this.radLabel4.Location = new System.Drawing.Point(12, 30);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(111, 18);
            this.radLabel4.TabIndex = 1;
            this.radLabel4.Text = "Наименование (фр.)";
            // 
            // txtEtalonNameFilter
            // 
            this.txtEtalonNameFilter.Location = new System.Drawing.Point(129, 28);
            this.txtEtalonNameFilter.Name = "txtEtalonNameFilter";
            this.txtEtalonNameFilter.Size = new System.Drawing.Size(273, 20);
            this.txtEtalonNameFilter.TabIndex = 0;
            this.txtEtalonNameFilter.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.txtEtalonNameFilter_ToolTipTextNeeded);
            this.txtEtalonNameFilter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEtalonNameFilter_KeyDown);
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(1272, 684);
            this.radSplitContainer1.TabIndex = 1;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radSplitContainer2);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(602, 684);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.02523661F, 0F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-32, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            this.radSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer2.Size = new System.Drawing.Size(602, 684);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            this.radSplitContainer2.Text = "radSplitContainer2";
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.gridEtalon);
            this.splitPanel3.Controls.Add(this.radGroupBox1);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(602, 490);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2205882F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 82);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // gridEtalon
            // 
            this.gridEtalon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEtalon.Location = new System.Drawing.Point(0, 238);
            // 
            // 
            // 
            this.gridEtalon.MasterTemplate.AllowAddNewRow = false;
            this.gridEtalon.MasterTemplate.AllowRowReorder = true;
            this.gridEtalon.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn8.AllowFiltering = false;
            gridViewTextBoxColumn8.FieldName = "product_id";
            gridViewTextBoxColumn8.HeaderText = "Код";
            gridViewTextBoxColumn8.Name = "column1";
            gridViewTextBoxColumn8.Width = 80;
            gridViewTextBoxColumn9.AllowFiltering = false;
            gridViewTextBoxColumn9.FieldName = "product_name";
            gridViewTextBoxColumn9.HeaderText = "Эталонное наименование";
            gridViewTextBoxColumn9.Name = "column2";
            gridViewTextBoxColumn9.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn9.Width = 350;
            gridViewTextBoxColumn10.AllowFiltering = false;
            gridViewTextBoxColumn10.FieldName = "mfr_name";
            gridViewTextBoxColumn10.HeaderText = "Производитель";
            gridViewTextBoxColumn10.Name = "column3";
            gridViewTextBoxColumn10.Width = 150;
            gridViewTextBoxColumn11.AllowFiltering = false;
            gridViewTextBoxColumn11.FieldName = "barcode";
            gridViewTextBoxColumn11.HeaderText = "Штрих-код";
            gridViewTextBoxColumn11.Name = "column4";
            gridViewTextBoxColumn11.Width = 150;
            gridViewTextBoxColumn12.AllowFiltering = false;
            gridViewTextBoxColumn12.FieldName = "partner_name";
            gridViewTextBoxColumn12.HeaderText = "Наименование поставщика";
            gridViewTextBoxColumn12.IsVisible = false;
            gridViewTextBoxColumn12.Name = "column5";
            gridViewTextBoxColumn12.Width = 250;
            gridViewTextBoxColumn13.AllowFiltering = false;
            gridViewTextBoxColumn13.FieldName = "source_partner_name";
            gridViewTextBoxColumn13.HeaderText = "Поставщик";
            gridViewTextBoxColumn13.IsVisible = false;
            gridViewTextBoxColumn13.Name = "column6";
            gridViewTextBoxColumn13.Width = 100;
            gridViewTextBoxColumn14.AllowFiltering = false;
            gridViewTextBoxColumn14.FieldName = "apply_group_name";
            gridViewTextBoxColumn14.HeaderText = "Группа по применению";
            gridViewTextBoxColumn14.Name = "column7";
            gridViewTextBoxColumn14.Width = 150;
            this.gridEtalon.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14});
            this.gridEtalon.MasterTemplate.EnableFiltering = true;
            this.gridEtalon.MasterTemplate.EnableGrouping = false;
            sortDescriptor2.PropertyName = "column2";
            this.gridEtalon.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            gridViewSummaryItem1.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Count;
            gridViewSummaryItem1.FormatString = "Всего строк: {0}";
            gridViewSummaryItem1.Name = "column2";
            this.gridEtalon.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem1}));
            this.gridEtalon.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridEtalon.Name = "gridEtalon";
            this.gridEtalon.ReadOnly = true;
            this.gridEtalon.Size = new System.Drawing.Size(602, 252);
            this.gridEtalon.TabIndex = 0;
            this.gridEtalon.Text = "radGridView1";
            this.gridEtalon.SelectionChanged += new System.EventHandler(this.gridEtalon_SelectionChanged);
            this.gridEtalon.FilterExpressionChanged += new Telerik.WinControls.UI.GridViewFilterExpressionChangedEventHandler(this.gridEtalon_FilterExpressionChanged);
            // 
            // splitPanel4
            // 
            this.splitPanel4.Controls.Add(this.gridEtalonGood);
            this.splitPanel4.Controls.Add(this.radPanel8);
            this.splitPanel4.Location = new System.Drawing.Point(0, 494);
            this.splitPanel4.Name = "splitPanel4";
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel4.Size = new System.Drawing.Size(602, 190);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2205882F);
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -82);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // gridEtalonGood
            // 
            this.gridEtalonGood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEtalonGood.Location = new System.Drawing.Point(0, 35);
            // 
            // 
            // 
            this.gridEtalonGood.MasterTemplate.AllowAddNewRow = false;
            this.gridEtalonGood.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn15.FieldName = "partner_code";
            gridViewTextBoxColumn15.HeaderText = "Код";
            gridViewTextBoxColumn15.Name = "column1";
            gridViewTextBoxColumn15.Width = 150;
            gridViewTextBoxColumn16.FieldName = "partner_name";
            gridViewTextBoxColumn16.HeaderText = "Наименование";
            gridViewTextBoxColumn16.Name = "column2";
            gridViewTextBoxColumn16.Width = 250;
            gridViewTextBoxColumn17.FieldName = "barcode";
            gridViewTextBoxColumn17.HeaderText = "Штрих-код";
            gridViewTextBoxColumn17.Name = "column3";
            gridViewTextBoxColumn17.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn17.Width = 200;
            gridViewTextBoxColumn18.FieldName = "mfr_name";
            gridViewTextBoxColumn18.HeaderText = "Производитель";
            gridViewTextBoxColumn18.Name = "column4";
            gridViewTextBoxColumn18.Width = 200;
            gridViewTextBoxColumn19.FieldName = "source_partner_name";
            gridViewTextBoxColumn19.HeaderText = "Поставщик";
            gridViewTextBoxColumn19.Name = "column5";
            gridViewTextBoxColumn19.Width = 150;
            this.gridEtalonGood.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17,
            gridViewTextBoxColumn18,
            gridViewTextBoxColumn19});
            this.gridEtalonGood.MasterTemplate.EnableFiltering = true;
            this.gridEtalonGood.MasterTemplate.EnableGrouping = false;
            sortDescriptor3.PropertyName = "column3";
            this.gridEtalonGood.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor3});
            this.gridEtalonGood.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.gridEtalonGood.Name = "gridEtalonGood";
            this.gridEtalonGood.ReadOnly = true;
            this.gridEtalonGood.Size = new System.Drawing.Size(602, 155);
            this.gridEtalonGood.TabIndex = 1;
            this.gridEtalonGood.Text = "radGridView2";
            // 
            // radPanel8
            // 
            this.radPanel8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radPanel8.Controls.Add(this.btnSynDel);
            this.radPanel8.Controls.Add(this.btnSynEdit);
            this.radPanel8.Controls.Add(this.btnSynAdd);
            this.radPanel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel8.Location = new System.Drawing.Point(0, 0);
            this.radPanel8.Name = "radPanel8";
            this.radPanel8.Size = new System.Drawing.Size(602, 35);
            this.radPanel8.TabIndex = 12;
            // 
            // btnSynDel
            // 
            this.btnSynDel.Image = ((System.Drawing.Image)(resources.GetObject("btnSynDel.Image")));
            this.btnSynDel.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSynDel.Location = new System.Drawing.Point(75, 5);
            this.btnSynDel.Name = "btnSynDel";
            this.btnSynDel.Size = new System.Drawing.Size(32, 26);
            this.btnSynDel.TabIndex = 8;
            this.btnSynDel.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnSynDel_ToolTipTextNeeded);
            this.btnSynDel.Click += new System.EventHandler(this.btnSynDel_Click);
            // 
            // btnSynEdit
            // 
            this.btnSynEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnSynEdit.Image")));
            this.btnSynEdit.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSynEdit.Location = new System.Drawing.Point(42, 5);
            this.btnSynEdit.Name = "btnSynEdit";
            this.btnSynEdit.Size = new System.Drawing.Size(27, 26);
            this.btnSynEdit.TabIndex = 7;
            this.btnSynEdit.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnSynEdit_ToolTipTextNeeded);
            this.btnSynEdit.Click += new System.EventHandler(this.btnSynEdit_Click);
            // 
            // btnSynAdd
            // 
            this.btnSynAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnSynAdd.Image")));
            this.btnSynAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSynAdd.Location = new System.Drawing.Point(4, 5);
            this.btnSynAdd.Name = "btnSynAdd";
            this.btnSynAdd.Size = new System.Drawing.Size(32, 26);
            this.btnSynAdd.TabIndex = 6;
            this.btnSynAdd.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnSynAdd_ToolTipTextNeeded);
            this.btnSynAdd.Click += new System.EventHandler(this.btnSynAdd_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer3);
            this.splitPanel2.Location = new System.Drawing.Point(606, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(666, 684);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.02523661F, 0F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(32, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.Controls.Add(this.splitPanel5);
            this.radSplitContainer3.Controls.Add(this.splitPanel6);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            this.radSplitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer3.Size = new System.Drawing.Size(666, 684);
            this.radSplitContainer3.TabIndex = 0;
            this.radSplitContainer3.TabStop = false;
            this.radSplitContainer3.Text = "radSplitContainer3";
            // 
            // splitPanel5
            // 
            this.splitPanel5.Controls.Add(this.gridMatch);
            this.splitPanel5.Controls.Add(this.radPanel2);
            this.splitPanel5.Controls.Add(this.radGroupBox2);
            this.splitPanel5.Location = new System.Drawing.Point(0, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel5.Size = new System.Drawing.Size(666, 489);
            this.splitPanel5.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2191176F);
            this.splitPanel5.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 85);
            this.splitPanel5.TabIndex = 0;
            this.splitPanel5.TabStop = false;
            this.splitPanel5.Text = "splitPanel5";
            // 
            // gridMatch
            // 
            this.gridMatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMatch.Location = new System.Drawing.Point(0, 238);
            // 
            // 
            // 
            this.gridMatch.MasterTemplate.AllowAddNewRow = false;
            this.gridMatch.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn20.FieldName = "code";
            gridViewTextBoxColumn20.HeaderText = "Код";
            gridViewTextBoxColumn20.IsVisible = false;
            gridViewTextBoxColumn20.Name = "column1";
            gridViewTextBoxColumn20.ReadOnly = true;
            gridViewTextBoxColumn20.Width = 150;
            gridViewCheckBoxColumn1.AllowFiltering = false;
            gridViewCheckBoxColumn1.AllowGroup = false;
            gridViewCheckBoxColumn1.EditMode = Telerik.WinControls.UI.EditMode.OnValueChange;
            gridViewCheckBoxColumn1.EnableHeaderCheckBox = true;
            gridViewCheckBoxColumn1.FieldName = "is_confirmed";
            gridViewCheckBoxColumn1.HeaderText = "";
            gridViewCheckBoxColumn1.IsPinned = true;
            gridViewCheckBoxColumn1.Name = "column10";
            gridViewCheckBoxColumn1.PinPosition = Telerik.WinControls.UI.PinnedColumnPosition.Left;
            gridViewTextBoxColumn21.AllowGroup = false;
            gridViewTextBoxColumn21.FieldName = "source_name";
            gridViewTextBoxColumn21.HeaderText = "Наименование";
            gridViewTextBoxColumn21.Name = "column2";
            gridViewTextBoxColumn21.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn21.Width = 450;
            gridViewTextBoxColumn22.AllowGroup = false;
            gridViewTextBoxColumn22.FieldName = "barcode";
            gridViewTextBoxColumn22.HeaderText = "Штрих-код";
            gridViewTextBoxColumn22.Name = "column3";
            gridViewTextBoxColumn22.ReadOnly = true;
            gridViewTextBoxColumn22.Width = 200;
            gridViewTextBoxColumn23.AllowGroup = false;
            gridViewTextBoxColumn23.FieldName = "mfr";
            gridViewTextBoxColumn23.HeaderText = "Производитель";
            gridViewTextBoxColumn23.Name = "column4";
            gridViewTextBoxColumn23.ReadOnly = true;
            gridViewTextBoxColumn23.Width = 200;
            gridViewCheckBoxColumn2.AllowFiltering = false;
            gridViewCheckBoxColumn2.AllowGroup = false;
            gridViewCheckBoxColumn2.FieldName = "is_approved";
            gridViewCheckBoxColumn2.HeaderText = "Сохранено";
            gridViewCheckBoxColumn2.Name = "column12";
            gridViewCheckBoxColumn2.ReadOnly = true;
            gridViewCheckBoxColumn2.Width = 100;
            gridViewTextBoxColumn24.AllowGroup = false;
            gridViewTextBoxColumn24.FieldName = "state_name";
            gridViewTextBoxColumn24.HeaderText = "Статус";
            gridViewTextBoxColumn24.Name = "column5";
            gridViewTextBoxColumn24.ReadOnly = true;
            gridViewTextBoxColumn24.Width = 100;
            gridViewTextBoxColumn25.AllowGroup = false;
            gridViewTextBoxColumn25.FieldName = "match_product_name";
            gridViewTextBoxColumn25.HeaderText = "Сопоставлен с: наименование";
            gridViewTextBoxColumn25.Name = "column6";
            gridViewTextBoxColumn25.ReadOnly = true;
            gridViewTextBoxColumn25.Width = 250;
            gridViewTextBoxColumn26.AllowGroup = false;
            gridViewTextBoxColumn26.FieldName = "match_mfr_name";
            gridViewTextBoxColumn26.HeaderText = "Сопоставлен с: производитель";
            gridViewTextBoxColumn26.Name = "column7";
            gridViewTextBoxColumn26.ReadOnly = true;
            gridViewTextBoxColumn26.Width = 100;
            gridViewTextBoxColumn27.AllowGroup = false;
            gridViewTextBoxColumn27.FieldName = "match_barcode";
            gridViewTextBoxColumn27.HeaderText = "Сопоставлен с: штрих-код";
            gridViewTextBoxColumn27.Name = "column8";
            gridViewTextBoxColumn27.ReadOnly = true;
            gridViewTextBoxColumn27.Width = 150;
            gridViewTextBoxColumn28.AllowGroup = false;
            gridViewTextBoxColumn28.FieldName = "match_good_id";
            gridViewTextBoxColumn28.HeaderText = "Сопоставлен с: код";
            gridViewTextBoxColumn28.IsVisible = false;
            gridViewTextBoxColumn28.Name = "column9";
            gridViewTextBoxColumn28.ReadOnly = true;
            gridViewTextBoxColumn28.Width = 80;
            gridViewTextBoxColumn29.AllowGroup = false;
            gridViewTextBoxColumn29.FieldName = "percent";
            gridViewTextBoxColumn29.HeaderText = "Общий %";
            gridViewTextBoxColumn29.Name = "column11";
            gridViewTextBoxColumn29.ReadOnly = true;
            gridViewTextBoxColumn29.Width = 100;
            gridViewTextBoxColumn30.AllowGroup = false;
            gridViewTextBoxColumn30.FieldName = "name_percent";
            gridViewTextBoxColumn30.HeaderText = "% наименования";
            gridViewTextBoxColumn30.Name = "column13";
            gridViewTextBoxColumn30.ReadOnly = true;
            gridViewTextBoxColumn30.Width = 80;
            gridViewTextBoxColumn31.AllowGroup = false;
            gridViewTextBoxColumn31.FieldName = "barcode_percent";
            gridViewTextBoxColumn31.HeaderText = "% штрих-кода";
            gridViewTextBoxColumn31.Name = "column14";
            gridViewTextBoxColumn31.ReadOnly = true;
            gridViewTextBoxColumn31.Width = 80;
            gridViewTextBoxColumn32.AllowGroup = false;
            gridViewTextBoxColumn32.FieldName = "mfr_percent";
            gridViewTextBoxColumn32.HeaderText = "% производителя";
            gridViewTextBoxColumn32.Name = "column15";
            gridViewTextBoxColumn32.ReadOnly = true;
            gridViewTextBoxColumn32.Width = 80;
            this.gridMatch.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn20,
            gridViewCheckBoxColumn1,
            gridViewTextBoxColumn21,
            gridViewTextBoxColumn22,
            gridViewTextBoxColumn23,
            gridViewCheckBoxColumn2,
            gridViewTextBoxColumn24,
            gridViewTextBoxColumn25,
            gridViewTextBoxColumn26,
            gridViewTextBoxColumn27,
            gridViewTextBoxColumn28,
            gridViewTextBoxColumn29,
            gridViewTextBoxColumn30,
            gridViewTextBoxColumn31,
            gridViewTextBoxColumn32});
            this.gridMatch.MasterTemplate.EnableFiltering = true;
            this.gridMatch.MasterTemplate.EnableGrouping = false;
            this.gridMatch.MasterTemplate.MultiSelect = true;
            sortDescriptor4.PropertyName = "column2";
            this.gridMatch.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor4});
            gridViewSummaryItem2.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Count;
            gridViewSummaryItem2.FormatString = "Всего строк: {0}";
            gridViewSummaryItem2.Name = "column2";
            this.gridMatch.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem2}));
            this.gridMatch.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.gridMatch.Name = "gridMatch";
            this.gridMatch.Size = new System.Drawing.Size(666, 225);
            this.gridMatch.TabIndex = 1;
            this.gridMatch.Text = "radGridView1";
            this.gridMatch.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.gridMatch_CellFormatting);
            this.gridMatch.CurrentRowChanged += new Telerik.WinControls.UI.CurrentRowChangedEventHandler(this.gridMatch_CurrentRowChanged);
            this.gridMatch.SelectionChanged += new System.EventHandler(this.gridMatch_SelectionChanged);
            this.gridMatch.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridMatch_CellDoubleClick);
            this.gridMatch.CellValueChanged += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridMatch_CellValueChanged);
            // 
            // radPanel2
            // 
            this.radPanel2.BackColor = System.Drawing.Color.Gainsboro;
            this.radPanel2.Controls.Add(this.lblMatchCheckedCnt);
            this.radPanel2.Controls.Add(this.lblMatchSelectedCnt);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel2.Location = new System.Drawing.Point(0, 463);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(666, 26);
            this.radPanel2.TabIndex = 2;
            // 
            // lblMatchCheckedCnt
            // 
            this.lblMatchCheckedCnt.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblMatchCheckedCnt.Location = new System.Drawing.Point(155, 4);
            this.lblMatchCheckedCnt.Name = "lblMatchCheckedCnt";
            this.lblMatchCheckedCnt.Size = new System.Drawing.Size(99, 18);
            this.lblMatchCheckedCnt.TabIndex = 1;
            this.lblMatchCheckedCnt.Text = "Отмечено строк:";
            // 
            // lblMatchSelectedCnt
            // 
            this.lblMatchSelectedCnt.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblMatchSelectedCnt.Location = new System.Drawing.Point(12, 4);
            this.lblMatchSelectedCnt.Name = "lblMatchSelectedCnt";
            this.lblMatchSelectedCnt.Size = new System.Drawing.Size(100, 18);
            this.lblMatchSelectedCnt.TabIndex = 0;
            this.lblMatchSelectedCnt.Text = "Выделено строк:";
            // 
            // splitPanel6
            // 
            this.splitPanel6.Controls.Add(this.pageMatchVariant);
            this.splitPanel6.Location = new System.Drawing.Point(0, 493);
            this.splitPanel6.Name = "splitPanel6";
            // 
            // 
            // 
            this.splitPanel6.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel6.Size = new System.Drawing.Size(666, 191);
            this.splitPanel6.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2191176F);
            this.splitPanel6.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -85);
            this.splitPanel6.TabIndex = 1;
            this.splitPanel6.TabStop = false;
            this.splitPanel6.Text = "splitPanel6";
            // 
            // pageMatchVariant
            // 
            this.pageMatchVariant.BackColor = System.Drawing.Color.Gainsboro;
            this.pageMatchVariant.Controls.Add(this.tabMatchVariant);
            this.pageMatchVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pageMatchVariant.Location = new System.Drawing.Point(0, 0);
            this.pageMatchVariant.Name = "pageMatchVariant";
            this.pageMatchVariant.SelectedPage = this.tabMatchVariant;
            this.pageMatchVariant.Size = new System.Drawing.Size(666, 191);
            this.pageMatchVariant.TabIndex = 0;
            this.pageMatchVariant.Text = "radPageView1";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.pageMatchVariant.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            // 
            // tabMatchVariant
            // 
            this.tabMatchVariant.Controls.Add(this.gridMatchVariant);
            this.tabMatchVariant.ItemSize = new System.Drawing.SizeF(105F, 28F);
            this.tabMatchVariant.Location = new System.Drawing.Point(10, 37);
            this.tabMatchVariant.Name = "tabMatchVariant";
            this.tabMatchVariant.Size = new System.Drawing.Size(645, 143);
            this.tabMatchVariant.Text = "Похожие товары";
            // 
            // gridMatchVariant
            // 
            this.gridMatchVariant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMatchVariant.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridMatchVariant.MasterTemplate.AllowAddNewRow = false;
            this.gridMatchVariant.MasterTemplate.AllowDeleteRow = false;
            this.gridMatchVariant.MasterTemplate.AllowEditRow = false;
            this.gridMatchVariant.MasterTemplate.AllowRowResize = false;
            gridViewTextBoxColumn33.FieldName = "variant_id";
            gridViewTextBoxColumn33.HeaderText = "Код";
            gridViewTextBoxColumn33.IsVisible = false;
            gridViewTextBoxColumn33.Name = "column1";
            gridViewTextBoxColumn33.Width = 150;
            gridViewTextBoxColumn34.FieldName = "percent";
            gridViewTextBoxColumn34.HeaderText = "Общий %";
            gridViewTextBoxColumn34.Name = "column5";
            gridViewTextBoxColumn34.SortOrder = Telerik.WinControls.UI.RadSortOrder.Descending;
            gridViewTextBoxColumn34.Width = 70;
            gridViewTextBoxColumn35.FieldName = "name_percent";
            gridViewTextBoxColumn35.HeaderText = "% наименования";
            gridViewTextBoxColumn35.Name = "column6";
            gridViewTextBoxColumn35.Width = 100;
            gridViewTextBoxColumn36.FieldName = "barcode_percent";
            gridViewTextBoxColumn36.HeaderText = "% штрих-кода";
            gridViewTextBoxColumn36.Name = "column7";
            gridViewTextBoxColumn36.Width = 80;
            gridViewTextBoxColumn37.FieldName = "mfr_percent";
            gridViewTextBoxColumn37.HeaderText = "% производителя";
            gridViewTextBoxColumn37.Name = "column8";
            gridViewTextBoxColumn37.Width = 80;
            gridViewTextBoxColumn38.FieldName = "match_product_name";
            gridViewTextBoxColumn38.HeaderText = "Наименование";
            gridViewTextBoxColumn38.Name = "column2";
            gridViewTextBoxColumn38.Width = 250;
            gridViewTextBoxColumn39.FieldName = "match_barcode";
            gridViewTextBoxColumn39.HeaderText = "Штрих-код";
            gridViewTextBoxColumn39.Name = "column3";
            gridViewTextBoxColumn39.Width = 200;
            gridViewTextBoxColumn40.FieldName = "match_mfr_name";
            gridViewTextBoxColumn40.HeaderText = "Производитель";
            gridViewTextBoxColumn40.Name = "column4";
            gridViewTextBoxColumn40.Width = 200;
            this.gridMatchVariant.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn33,
            gridViewTextBoxColumn34,
            gridViewTextBoxColumn35,
            gridViewTextBoxColumn36,
            gridViewTextBoxColumn37,
            gridViewTextBoxColumn38,
            gridViewTextBoxColumn39,
            gridViewTextBoxColumn40});
            this.gridMatchVariant.MasterTemplate.EnableFiltering = true;
            this.gridMatchVariant.MasterTemplate.EnableGrouping = false;
            sortDescriptor5.Direction = System.ComponentModel.ListSortDirection.Descending;
            sortDescriptor5.PropertyName = "column5";
            this.gridMatchVariant.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor5});
            this.gridMatchVariant.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.gridMatchVariant.Name = "gridMatchVariant";
            this.gridMatchVariant.ReadOnly = true;
            this.gridMatchVariant.Size = new System.Drawing.Size(645, 143);
            this.gridMatchVariant.TabIndex = 2;
            this.gridMatchVariant.Text = "radGridView2";
            this.gridMatchVariant.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.gridMatchVariant_CellDoubleClick);
            // 
            // radColorDialog1
            // 
            this.radColorDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("radColorDialog1.Icon")));
            this.radColorDialog1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radColorDialog1.SelectedColor = System.Drawing.Color.Red;
            this.radColorDialog1.SelectedHslColor = Telerik.WinControls.HslColor.FromAhsl(0D, 1D, 1D);
            // 
            // MatchingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1272, 684);
            this.Controls.Add(this.radSplitContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MatchingForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.Text = "Сопоставление справочников";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MatchingForm_Load);
            this.Shown += new System.EventHandler(this.MatchingForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pageMatch)).EndInit();
            this.pageMatch.ResumeLayout(false);
            this.tabMatchDocSelect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDoc.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel6)).EndInit();
            this.radPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSelectDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel5)).EndInit();
            this.radPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prgImportDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbImportDoc)).EndInit();
            this.grbImportDoc.ResumeLayout(false);
            this.grbImportDoc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportConfigEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportConfigAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlImportConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportFile)).EndInit();
            this.tabMatchDoc.ResumeLayout(false);
            this.tabMatchDoc.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.radPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonMfr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonBarcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatchMfr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatchName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMatchBarcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnCancelAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMatchToNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMatchToSyn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMatchClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbUnapprovedOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbMatchNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbMatchSyn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSelectedDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbUnmatchOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            this.radGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chbShowVariant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAutoMatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonFilterReset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEtalonFilterApply)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonBarcodeFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonMfrFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEtalonNameFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalon.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel8)).EndInit();
            this.radPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSynDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSynEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSynAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            this.splitPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMatch.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblMatchCheckedCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMatchSelectedCnt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).EndInit();
            this.splitPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pageMatchVariant)).EndInit();
            this.pageMatchVariant.ResumeLayout(false);
            this.tabMatchVariant.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMatchVariant.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMatchVariant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.SplitPanel splitPanel6;
        private Telerik.WinControls.UI.RadGridView gridEtalon;
        private Telerik.WinControls.UI.RadGridView gridEtalonGood;
        private Telerik.WinControls.UI.RadButton btnImportDoc;
        private Telerik.WinControls.UI.RadButton btnSelectDoc;
        private Telerik.WinControls.UI.RadTextBox txtSelectedDoc;
        private Telerik.WinControls.UI.RadGroupBox grbImportDoc;
        private Telerik.WinControls.UI.RadButton btnImportStart;
        private Telerik.WinControls.UI.RadDropDownList ddlImportConfig;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadBrowseEditor edImportFile;
        private Telerik.WinControls.UI.RadGridView gridMatch;
        private Telerik.WinControls.UI.RadButton btnCancelAll;
        private Telerik.WinControls.UI.RadButton btnSaveAll;
        private Telerik.WinControls.UI.RadButton btnMatchClear;
        private Telerik.WinControls.UI.RadButton btnEtalonFilterReset;
        private Telerik.WinControls.UI.RadButton btnEtalonFilterApply;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadTextBox txtEtalonBarcodeFilter;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadTextBox txtEtalonMfrFilter;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox txtEtalonNameFilter;
        private Telerik.WinControls.UI.RadCheckBox chbUnmatchOnly;
        private Telerik.WinControls.UI.RadButton btnAutoMatch;
        private Telerik.WinControls.RadColorDialog radColorDialog1;
        private Telerik.WinControls.UI.RadPageView pageMatch;
        private Telerik.WinControls.UI.RadPageViewPage tabMatchDoc;
        private Telerik.WinControls.UI.RadPageViewPage tabMatchDocSelect;
        private Telerik.WinControls.UI.RadDropDownButton btnMatchToSyn;
        private Telerik.WinControls.UI.RadDropDownButton btnMatchToNew;
        private Telerik.WinControls.UI.RadMenuItem btnMatchAsIsToSyn;
        private Telerik.WinControls.UI.RadMenuItem btnMatchChangeToSyn;
        private Telerik.WinControls.UI.RadMenuItem btnMatchAsIsToNew;
        private Telerik.WinControls.UI.RadMenuItem btnMatchChangeToNew;
        private Telerik.WinControls.UI.RadPageView pageMatchVariant;
        private Telerik.WinControls.UI.RadPageViewPage tabMatchVariant;
        private Telerik.WinControls.UI.RadGridView gridMatchVariant;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadCheckBox chbShowVariant;
        private Telerik.WinControls.UI.RadCheckBox chbUnapprovedOnly;
        private Telerik.WinControls.UI.RadCheckBox chbMatchNew;
        private Telerik.WinControls.UI.RadCheckBox chbMatchSyn;
        private Telerik.WinControls.UI.RadButton btnImportConfigAdd;
        private Telerik.WinControls.UI.RadButton btnImportConfigEdit;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadTextBox txtEtalonMfr;
        private Telerik.WinControls.UI.RadTextBox txtEtalonBarcode;
        private Telerik.WinControls.UI.RadTextBox txtEtalonName;
        private Telerik.WinControls.UI.RadTextBox txtMatchMfr;
        private Telerik.WinControls.UI.RadTextBox txtMatchBarcode;
        private Telerik.WinControls.UI.RadTextBox txtMatchName;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadLabel lblMatchCheckedCnt;
        private Telerik.WinControls.UI.RadLabel lblMatchSelectedCnt;
        private Telerik.WinControls.UI.RadProgressBar prgImportDoc;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadGridView gridDoc;
        private Telerik.WinControls.UI.RadPanel radPanel6;
        private Telerik.WinControls.UI.RadPanel radPanel5;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadButton btnEtalonDel;
        private Telerik.WinControls.UI.RadButton btnEtalonEdit;
        private Telerik.WinControls.UI.RadButton btnEtalonAdd;
        private Telerik.WinControls.UI.RadPanel radPanel8;
        private Telerik.WinControls.UI.RadButton btnSynDel;
        private Telerik.WinControls.UI.RadButton btnSynEdit;
        private Telerik.WinControls.UI.RadButton btnSynAdd;
    }
}
