﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace EsnAdmin
{
    public partial class ApplyGroupEditForm : RadFormExt
    {
        private bool is_add = true;
        private int? curr_apply_group_id = null;

        public string new_apply_group_name = "";

        public ApplyGroupEditForm()
        {
            InitializeComponent();
            //
            InitForm();
        }


        private void InitForm()
        {
            //
        }

        public void InitForm_Add()
        {
            is_add = true;
            curr_apply_group_id = null;
            //
            ClearForm();
        }

        public void InitForm_Edit(int apply_group_id, string apply_group_name)
        {
            is_add = false;
            curr_apply_group_id = apply_group_id;
            //
            edApplyGroupName.Text = apply_group_name;            
        }

        private void ClearForm()
        {
            edApplyGroupName.Text = "";
        }

        private void ApplyGroupEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                if (String.IsNullOrWhiteSpace(edApplyGroupName.Text))
                {
                    e.Cancel = true;
                    MessageBox.Show("Не задано наименование группы");
                    return;
                }

                new_apply_group_name = edApplyGroupName.Text;
            }
        }
    }
}
