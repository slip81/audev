﻿namespace EsnAdmin
{
    partial class MfrForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MfrForm));
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem1 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radPanel7 = new Telerik.WinControls.UI.RadPanel();
            this.btnMfrDel = new Telerik.WinControls.UI.RadButton();
            this.btnMfrEdit = new Telerik.WinControls.UI.RadButton();
            this.btnMfrAdd = new Telerik.WinControls.UI.RadButton();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.gridMfr = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.gridMfrAlias = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.btnMfrSynDel = new Telerik.WinControls.UI.RadButton();
            this.btnMfrSynEdit = new Telerik.WinControls.UI.RadButton();
            this.btnMfrSynAdd = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).BeginInit();
            this.radPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMfr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMfr.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridMfrAlias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMfrAlias.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrSynDel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrSynEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrSynAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.BackColor = System.Drawing.Color.Gainsboro;
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Controls.Add(this.splitPanel3);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(977, 539);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radPanel7);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(977, 66);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2090396F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -113);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            // 
            // radPanel7
            // 
            this.radPanel7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radPanel7.Controls.Add(this.btnMfrDel);
            this.radPanel7.Controls.Add(this.btnMfrEdit);
            this.radPanel7.Controls.Add(this.btnMfrAdd);
            this.radPanel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel7.Location = new System.Drawing.Point(0, 31);
            this.radPanel7.Name = "radPanel7";
            this.radPanel7.Size = new System.Drawing.Size(977, 35);
            this.radPanel7.TabIndex = 12;
            // 
            // btnMfrDel
            // 
            this.btnMfrDel.Image = ((System.Drawing.Image)(resources.GetObject("btnMfrDel.Image")));
            this.btnMfrDel.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnMfrDel.Location = new System.Drawing.Point(75, 5);
            this.btnMfrDel.Name = "btnMfrDel";
            this.btnMfrDel.Size = new System.Drawing.Size(32, 26);
            this.btnMfrDel.TabIndex = 8;
            this.btnMfrDel.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnMfrDel_ToolTipTextNeeded);
            this.btnMfrDel.Click += new System.EventHandler(this.btnMfrDel_Click);
            // 
            // btnMfrEdit
            // 
            this.btnMfrEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnMfrEdit.Image")));
            this.btnMfrEdit.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnMfrEdit.Location = new System.Drawing.Point(42, 5);
            this.btnMfrEdit.Name = "btnMfrEdit";
            this.btnMfrEdit.Size = new System.Drawing.Size(27, 26);
            this.btnMfrEdit.TabIndex = 7;
            this.btnMfrEdit.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnMfrEdit_ToolTipTextNeeded);
            this.btnMfrEdit.Click += new System.EventHandler(this.btnMfrEdit_Click);
            // 
            // btnMfrAdd
            // 
            this.btnMfrAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnMfrAdd.Image")));
            this.btnMfrAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnMfrAdd.Location = new System.Drawing.Point(4, 5);
            this.btnMfrAdd.Name = "btnMfrAdd";
            this.btnMfrAdd.Size = new System.Drawing.Size(32, 26);
            this.btnMfrAdd.TabIndex = 6;
            this.btnMfrAdd.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnMfrAdd_ToolTipTextNeeded);
            this.btnMfrAdd.Click += new System.EventHandler(this.btnMfrAdd_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.gridMfr);
            this.splitPanel2.Location = new System.Drawing.Point(0, 70);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(977, 269);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.173258F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 94);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // gridMfr
            // 
            this.gridMfr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMfr.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridMfr.MasterTemplate.AllowAddNewRow = false;
            this.gridMfr.MasterTemplate.AllowDeleteRow = false;
            this.gridMfr.MasterTemplate.AllowEditRow = false;
            this.gridMfr.MasterTemplate.AllowRowReorder = true;
            gridViewTextBoxColumn1.AllowGroup = false;
            gridViewTextBoxColumn1.FieldName = "mfr_id";
            gridViewTextBoxColumn1.HeaderText = "Код";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 150;
            gridViewTextBoxColumn2.AllowGroup = false;
            gridViewTextBoxColumn2.FieldName = "mfr_name";
            gridViewTextBoxColumn2.HeaderText = "Наименование";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn2.Width = 450;
            this.gridMfr.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2});
            this.gridMfr.MasterTemplate.EnableFiltering = true;
            this.gridMfr.MasterTemplate.EnableGrouping = false;
            sortDescriptor1.PropertyName = "column2";
            this.gridMfr.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            gridViewSummaryItem1.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Count;
            gridViewSummaryItem1.FormatString = "Всего строк: {0}";
            gridViewSummaryItem1.Name = "column2";
            this.gridMfr.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem1}));
            this.gridMfr.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridMfr.Name = "gridMfr";
            this.gridMfr.ReadOnly = true;
            this.gridMfr.Size = new System.Drawing.Size(977, 269);
            this.gridMfr.TabIndex = 1;
            this.gridMfr.Text = "radGridView1";
            this.gridMfr.SelectionChanged += new System.EventHandler(this.gridMfr_SelectionChanged);
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.gridMfrAlias);
            this.splitPanel3.Controls.Add(this.radPanel1);
            this.splitPanel3.Location = new System.Drawing.Point(0, 343);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(977, 196);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.03578153F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 19);
            this.splitPanel3.TabIndex = 2;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPnel3";
            // 
            // gridMfrAlias
            // 
            this.gridMfrAlias.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridMfrAlias.Location = new System.Drawing.Point(0, 35);
            // 
            // 
            // 
            this.gridMfrAlias.MasterTemplate.AllowAddNewRow = false;
            this.gridMfrAlias.MasterTemplate.AllowDeleteRow = false;
            this.gridMfrAlias.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn3.AllowGroup = false;
            gridViewTextBoxColumn3.FieldName = "alias_id";
            gridViewTextBoxColumn3.HeaderText = "Код";
            gridViewTextBoxColumn3.Name = "column1";
            gridViewTextBoxColumn3.Width = 150;
            gridViewTextBoxColumn4.AllowGroup = false;
            gridViewTextBoxColumn4.FieldName = "alias_name";
            gridViewTextBoxColumn4.HeaderText = "Наименование";
            gridViewTextBoxColumn4.Name = "column2";
            gridViewTextBoxColumn4.Width = 450;
            this.gridMfrAlias.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.gridMfrAlias.MasterTemplate.EnableFiltering = true;
            this.gridMfrAlias.MasterTemplate.EnableGrouping = false;
            sortDescriptor2.PropertyName = "column5";
            this.gridMfrAlias.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.gridMfrAlias.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridMfrAlias.Name = "gridMfrAlias";
            this.gridMfrAlias.ReadOnly = true;
            this.gridMfrAlias.Size = new System.Drawing.Size(977, 161);
            this.gridMfrAlias.TabIndex = 2;
            this.gridMfrAlias.Text = "radGridView1";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.radPanel1.Controls.Add(this.btnMfrSynDel);
            this.radPanel1.Controls.Add(this.btnMfrSynEdit);
            this.radPanel1.Controls.Add(this.btnMfrSynAdd);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(977, 35);
            this.radPanel1.TabIndex = 13;
            // 
            // btnMfrSynDel
            // 
            this.btnMfrSynDel.Image = ((System.Drawing.Image)(resources.GetObject("btnMfrSynDel.Image")));
            this.btnMfrSynDel.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnMfrSynDel.Location = new System.Drawing.Point(75, 5);
            this.btnMfrSynDel.Name = "btnMfrSynDel";
            this.btnMfrSynDel.Size = new System.Drawing.Size(32, 26);
            this.btnMfrSynDel.TabIndex = 8;
            this.btnMfrSynDel.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnMfrSynDel_ToolTipTextNeeded);
            this.btnMfrSynDel.Click += new System.EventHandler(this.btnMfrSynDel_Click);
            // 
            // btnMfrSynEdit
            // 
            this.btnMfrSynEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnMfrSynEdit.Image")));
            this.btnMfrSynEdit.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnMfrSynEdit.Location = new System.Drawing.Point(42, 5);
            this.btnMfrSynEdit.Name = "btnMfrSynEdit";
            this.btnMfrSynEdit.Size = new System.Drawing.Size(27, 26);
            this.btnMfrSynEdit.TabIndex = 7;
            this.btnMfrSynEdit.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnMfrSynEdit_ToolTipTextNeeded);
            this.btnMfrSynEdit.Click += new System.EventHandler(this.btnMfrSynEdit_Click);
            // 
            // btnMfrSynAdd
            // 
            this.btnMfrSynAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnMfrSynAdd.Image")));
            this.btnMfrSynAdd.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnMfrSynAdd.Location = new System.Drawing.Point(4, 5);
            this.btnMfrSynAdd.Name = "btnMfrSynAdd";
            this.btnMfrSynAdd.Size = new System.Drawing.Size(32, 26);
            this.btnMfrSynAdd.TabIndex = 6;
            this.btnMfrSynAdd.ToolTipTextNeeded += new Telerik.WinControls.ToolTipTextNeededEventHandler(this.btnMfrSynAdd_ToolTipTextNeeded);
            this.btnMfrSynAdd.Click += new System.EventHandler(this.btnMfrSynAdd_Click);
            // 
            // MfrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 539);
            this.Controls.Add(this.radSplitContainer1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MfrForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.Text = "Изготовители";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel7)).EndInit();
            this.radPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMfr.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMfr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridMfrAlias.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridMfrAlias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrSynDel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrSynEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMfrSynAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadGridView gridMfr;
        private Telerik.WinControls.UI.RadGridView gridMfrAlias;
        private Telerik.WinControls.UI.RadPanel radPanel7;
        private Telerik.WinControls.UI.RadButton btnMfrDel;
        private Telerik.WinControls.UI.RadButton btnMfrEdit;
        private Telerik.WinControls.UI.RadButton btnMfrAdd;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadButton btnMfrSynDel;
        private Telerik.WinControls.UI.RadButton btnMfrSynEdit;
        private Telerik.WinControls.UI.RadButton btnMfrSynAdd;

    }
}
