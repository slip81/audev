﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace EsnAdmin
{
    public partial class MfrEditForm : RadFormExt
    {
        private bool is_add = true;
        private int? curr_mfr_id = null;

        public string new_mfr_name = "";

        public MfrEditForm()
        {
            InitializeComponent();
            //
            InitForm();
        }


        private void InitForm()
        {
            //
        }

        public void InitForm_Add()
        {
            is_add = true;
            curr_mfr_id = null;
            //
            ClearForm();
        }

        public void InitForm_Edit(int mfr_id, string mfr_name)
        {
            is_add = false;
            curr_mfr_id = mfr_id;
            //
            edMfrName.Text = mfr_name;            
        }

        private void ClearForm()
        {
            edMfrName.Text = "";
        }

        private void MfrEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                if (String.IsNullOrWhiteSpace(edMfrName.Text))
                {
                    e.Cancel = true;
                    MessageBox.Show("Не задано наименование препарата");
                    return;
                }

                new_mfr_name = edMfrName.Text;
            }
        }
    }
}
