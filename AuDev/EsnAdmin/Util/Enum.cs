﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsnAdmin.Util
{
    public static class EsnAdminEnums
    {
        public enum FormEnum
        {            
            MAIN_FORM = 1,
            ETALON_NAME_FORM = 2,
            MATCHING_FORM = 3,
            SUPPLIER_NAME_FORM = 4,
            MFR_FORM = 5,
            APPLY_GROUP_FORM = 6,
        }
    }
}
