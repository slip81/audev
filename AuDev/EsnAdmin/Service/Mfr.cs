﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{
    public class MfrServiceResult : ServiceResult
    {
        public MfrServiceResult()
            : base()
        {
            //
        }

        public MfrServiceResult(Exception ex)
            : base(ex)
        {
            //
        }

        public List<Mfr> manufacturers { get; set; }
        public List<MfrAlias> manufacturer_aliases { get; set; }
    }

    public class MfrService: UndBaseService
    {
        public MfrService(AppManager appManager)
            :base(appManager)
        {
            //
        }

        #region GetManufacturers

        public MfrServiceResult GetManufacturers()
        {
            try
            {
                List<Mfr> res1 = dbContext.manufacturer
                    .ProjectTo<Mfr>(ModelMapper.ConfigurationProvider)
                    .OrderBy(ss => ss.mfr_name)
                    .ToList();

                List<MfrAlias> res2 = dbContext.manufacturer_alias
                    .ProjectTo<MfrAlias>(ModelMapper.ConfigurationProvider)
                    .ToList();
                
                return new MfrServiceResult() 
                { 
                    Error = null,
                    Id = 0,
                    manufacturers = new List<Mfr>(res1),
                    manufacturer_aliases = new List<MfrAlias>(res2),
                };
            }
            catch (Exception ex)
            {
                return new MfrServiceResult(ex);
            }
        }

        #endregion

        #region GetManufacturerAliases

        public List<MfrAlias> GetManufacturerAliases()
        {
            return dbContext.manufacturer_alias
                .ProjectTo<MfrAlias>(ModelMapper.ConfigurationProvider)
                .ToList();
        }

        #endregion

        #region InsertMfr

        public Mfr InsertMfr(string mfr_name)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            manufacturer manufacturer = new manufacturer();
            manufacturer.mfr_name = mfr_name;
            manufacturer.crt_date = now;
            manufacturer.crt_user = user;
            manufacturer.upd_date = now;
            manufacturer.upd_user = user;
            manufacturer.is_deleted = false;
            dbContext.manufacturer.Add(manufacturer);

            dbContext.SaveChanges();

            return dbContext.manufacturer.Where(ss => ss.mfr_id == manufacturer.mfr_id).ProjectTo<Mfr>().FirstOrDefault();
        }

        #endregion

        #region InsertMfr_Syn

        public MfrAlias InsertMfr_Syn(int main_mfr_id, string mfr_name)
        {            
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            manufacturer main_manufacturer = dbContext.manufacturer.Where(ss => ss.mfr_id == main_mfr_id).FirstOrDefault();
            if (main_manufacturer == null)
                return null;

            manufacturer_alias manufacturer_alias = new manufacturer_alias();
            manufacturer_alias.mfr_id = main_mfr_id;
            manufacturer_alias.alias_name = mfr_name;
            manufacturer_alias.crt_date = now;
            manufacturer_alias.crt_user = user;
            manufacturer_alias.upd_date = now;
            manufacturer_alias.upd_user = user;
            manufacturer_alias.is_deleted = false;
            dbContext.manufacturer_alias.Add(manufacturer_alias);

            dbContext.SaveChanges();

            return dbContext.manufacturer_alias.Where(ss => ss.alias_id == manufacturer_alias.alias_id).ProjectTo<MfrAlias>().FirstOrDefault();
        }

        #endregion

        #region UpdateMfr

        public Mfr UpdateMfr(int mfr_id, string mfr_name)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            manufacturer manufacturer = dbContext.manufacturer.Where(ss => ss.mfr_id == mfr_id).FirstOrDefault();
            if (manufacturer == null)
                return null;

            manufacturer.mfr_name = mfr_name;            
            manufacturer.upd_date = now;
            manufacturer.upd_user = user;            

            dbContext.SaveChanges();

            return dbContext.manufacturer.Where(ss => ss.mfr_id == manufacturer.mfr_id).ProjectTo<Mfr>().FirstOrDefault();
        }

        #endregion

        #region UpdateMfr_Syn

        public MfrAlias UpdateMfr_Syn(int main_mfr_id, int mfr_id, string mfr_name)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            manufacturer main_manufacturer = dbContext.manufacturer.Where(ss => ss.mfr_id == main_mfr_id).FirstOrDefault();
            if (main_manufacturer == null)
                return null;

            manufacturer_alias manufacturer_alias = dbContext.manufacturer_alias.Where(ss => ss.alias_id == mfr_id).FirstOrDefault();
            if (manufacturer_alias == null)
                return null;

            manufacturer_alias.alias_name = mfr_name;
            manufacturer_alias.upd_date = now;
            manufacturer_alias.upd_user = user;

            dbContext.SaveChanges();

            return dbContext.manufacturer_alias.Where(ss => ss.alias_id == manufacturer_alias.alias_id).ProjectTo<MfrAlias>().FirstOrDefault();
        }

        #endregion

        #region DeleteMfr

        public bool DeleteMfr(int mfr_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            manufacturer manufacturer = dbContext.manufacturer.Where(ss => ss.mfr_id == mfr_id).FirstOrDefault();
            if (manufacturer == null)
                return false;

            var good_existing = dbContext.good.Where(ss => ss.mfr_id == mfr_id).FirstOrDefault();
            if (good_existing != null)
                return false;

            dbContext.manufacturer_alias.RemoveRange(dbContext.manufacturer_alias.Where(ss => ss.mfr_id == mfr_id));
            dbContext.manufacturer.Remove(manufacturer);

            dbContext.SaveChanges();

            return true;
        }

        #endregion

        #region DeleteMfr_Syn

        public bool DeleteMfr_Syn(int mfr_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            manufacturer_alias manufacturer_alias = dbContext.manufacturer_alias.Where(ss => ss.alias_id == mfr_id).FirstOrDefault();
            if (manufacturer_alias == null)
                return false;

            dbContext.manufacturer_alias.Remove(manufacturer_alias);            
            dbContext.SaveChanges();

            return true;
        }

        #endregion

        #region MoveMfr_FromMainToSyn

        public bool MoveMfr_FromMainToSyn(int target_mfr_id, int source_mfr_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            manufacturer target_mfr = dbContext.manufacturer.Where(ss => ss.mfr_id == target_mfr_id).FirstOrDefault();
            if (target_mfr == null)
                return false;

            manufacturer source_mfr = dbContext.manufacturer.Where(ss => ss.mfr_id == source_mfr_id).FirstOrDefault();
            if (source_mfr == null)
                return false;

            manufacturer_alias manufacturer_alias = null;
            
            manufacturer_alias = new manufacturer_alias();
            manufacturer_alias.alias_name = source_mfr.mfr_name;
            manufacturer_alias.mfr_id = target_mfr_id;
            manufacturer_alias.crt_date = now;
            manufacturer_alias.crt_user = user;
            manufacturer_alias.upd_date = now;
            manufacturer_alias.upd_user = user;
            manufacturer_alias.is_deleted = false;
            dbContext.manufacturer_alias.Add(manufacturer_alias);

            List<manufacturer_alias> source_mfr_with_synonyms = dbContext.manufacturer_alias.Where(ss => ss.mfr_id == source_mfr.mfr_id).ToList();
            if ((source_mfr_with_synonyms != null) && (source_mfr_with_synonyms.Count > 0))
            {
                foreach (var mfr in source_mfr_with_synonyms)
                {
                    mfr.mfr_id = target_mfr_id;
                    mfr.upd_date = now;
                    mfr.upd_user = user;
                }
            }

            List<good> good_list = dbContext.good.Where(ss => ss.mfr_id == source_mfr.mfr_id).ToList();
            if ((good_list != null) && (good_list.Count > 0))
            {
                foreach (var good in good_list)
                {
                    good.mfr_id = target_mfr_id;
                    good.upd_date = now;
                    good.upd_user = user;
                }
            }

            dbContext.manufacturer.Remove(source_mfr);

            dbContext.SaveChanges();

            return true;
        }

        #endregion

    }
}

