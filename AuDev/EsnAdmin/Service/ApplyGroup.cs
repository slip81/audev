﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{
    public class ApplyGroupServiceResult : ServiceResult
    {
        public ApplyGroupServiceResult()
            : base()
        {
            //
        }

        public ApplyGroupServiceResult(Exception ex)
            : base(ex)
        {
            //
        }

        public List<ApplyGroup> apply_groups { get; set; }        
    }

    public class ApplyGroupService: UndBaseService
    {
        public ApplyGroupService(AppManager appManager)
            :base(appManager)
        {
            //
        }

        #region GetApplyGroups

        public ApplyGroupServiceResult GetApplyGroups()
        {
            try
            {
                List<ApplyGroup> res = dbContext.apply_group
                    .ProjectTo<ApplyGroup>(ModelMapper.ConfigurationProvider)
                    .OrderBy(ss => ss.apply_group_name)
                    .ToList();

                return new ApplyGroupServiceResult() 
                { 
                    Error = null,
                    Id = 0,
                    apply_groups = new List<ApplyGroup>(res),
                };
            }
            catch (Exception ex)
            {
                return new ApplyGroupServiceResult(ex);
            }
        }

        #endregion

        #region InsertApplyGroup

        public ApplyGroup InsertApplyGroup(string apply_group_name)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            apply_group apply_group_last = dbContext.apply_group.OrderByDescending(ss => ss.apply_group_id).FirstOrDefault();
            int apply_group_id = apply_group_last != null ? apply_group_last.apply_group_id + 1 : 1;

            apply_group apply_group = new apply_group();
            apply_group.apply_group_id = apply_group_id;
            apply_group.apply_group_name = apply_group_name;
            apply_group.crt_date = now;
            apply_group.crt_user = user;
            apply_group.upd_date = now;
            apply_group.upd_user = user;
            apply_group.is_deleted = false;
            dbContext.apply_group.Add(apply_group);

            dbContext.SaveChanges();

            return dbContext.apply_group.Where(ss => ss.apply_group_id == apply_group.apply_group_id).ProjectTo<ApplyGroup>().FirstOrDefault();
        }

        #endregion

        #region UpdateApplyGroup

        public ApplyGroup UpdateApplyGroup(int apply_group_id, string apply_group_name)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            apply_group apply_group = dbContext.apply_group.Where(ss => ss.apply_group_id == apply_group_id).FirstOrDefault();
            if (apply_group == null)
                return null;

            apply_group.apply_group_name = apply_group_name;
            apply_group.upd_date = now;
            apply_group.upd_user = user;

            dbContext.SaveChanges();

            return dbContext.apply_group.Where(ss => ss.apply_group_id == apply_group.apply_group_id).ProjectTo<ApplyGroup>().FirstOrDefault();
        }

        #endregion

        #region DeleteApplyGroup

        public bool DeleteApplyGroup(int apply_group_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            apply_group apply_group = dbContext.apply_group.Where(ss => ss.apply_group_id == apply_group_id).FirstOrDefault();
            if (apply_group == null)
                return false;

            var product_existing = dbContext.product.Where(ss => ss.apply_group_id == apply_group_id).FirstOrDefault();
            if (product_existing != null)
                return false;
            
            dbContext.apply_group.Remove(apply_group);
            dbContext.SaveChanges();

            return true;
        }

        #endregion

    }
}

