﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using System.Xml;
using System.Xml.Linq;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Core;

namespace EsnAdmin.Service
{
    #region DocImportResult

    public class DocImportResult : ServiceResult
    {
        public DocImportResult()
            : base()
        {
            //
        }

        public DocImportResult(Exception ex)
            : base(ex)
        {
            //
        }

        public int? import_id { get; set; }
        public int? doc_id { get; set; }
    }

    #endregion

    #region DocImportEtalonResult

    public class DocImportEtalonResult : DocImportResult
    {
        public DocImportEtalonResult()
            :base()
        {
            //
        }

        public DocImportEtalonResult(Exception ex)
            :base(ex)
        {
            //
        }

        public int? product_cnt { get; set; }
    }

    #endregion

    #region DocImportSupplierResult

    public class DocImportSupplierResult : DocImportResult
    {
        public DocImportSupplierResult()
            : base()
        {
            //
        }

        public DocImportSupplierResult(Exception ex)
            : base(ex)
        {
            //
        }

        public int? good_cnt { get; set; }
        public int? partner_id { get; set; }
    }

    #endregion

    #region DocImportInfoResult

    public class DocImportInfoResult : DocImportResult
    {
        public DocImportInfoResult()
            : base()
        {
            //
        }

        public DocImportInfoResult(Exception ex)
            : base(ex)
        {
            //
        }

        public int? row_all_cnt { get; set; }
        public int? row_done_cnt { get; set; }
        public int? state { get; set; }
        public string doc_name { get; set; }
    }

    #endregion

    public class DocImportService: UndBaseService
    {
        public DocImportService(AppManager appManager)
            :base(appManager)
        {
            //
        }

        #region ImportEtalon

        public DocImportEtalonResult ImportEtalon(int import_id, string file_path, int config_type_id)
        {
            try
            {
                string file_name = Path.GetFileName(file_path);                
                int? doc_id = 0;
                int product_cnt = 0;
                string userName = appManager.CurrentUser.Name;

                DocImportResult importRes = xImport(import_id, file_path, config_type_id);
                if (importRes.Error != null)
                {
                    return new DocImportEtalonResult()
                    {
                        Id = importRes.Id,
                        Error = importRes.Error,
                    };
                }
                product_cnt = dbContext.Database.SqlQuery<int>("select * from und.move_import_to_etalon(@p0, @p1)", import_id, userName).FirstOrDefault();
                doc_id = dbContext.doc_import.AsNoTracking().Where(ss => ss.import_id == import_id).Select(ss => ss.doc_id).FirstOrDefault();

                return new DocImportEtalonResult() { Id = 0, Error = null, import_id = import_id, product_cnt = product_cnt, doc_id = doc_id };
                
            }
            catch (Exception ex)
            {
                return new DocImportEtalonResult(ex);
            }
        }

        #endregion

        #region ImportSupplier

        public DocImportSupplierResult ImportSupplier(int import_id, string file_path, int config_type_id)
        {
            try
            {
                string file_name = Path.GetFileName(file_path);                                
                string userName = appManager.CurrentUser.Name;

                DocImportResult importRes = xImport(import_id, file_path, config_type_id);
                if (importRes.Error != null)
                {
                    return new DocImportSupplierResult()
                    {
                        Id = importRes.Id,
                        Error = importRes.Error,
                    };
                }                
                int good_cnt = dbContext.Database.SqlQuery<int>("select * from und.move_import_to_good(@p0, @p1)", import_id, userName).FirstOrDefault();
                doc_import doc_import = dbContext.doc_import.AsNoTracking().Where(ss => ss.import_id == import_id).FirstOrDefault();
                if (doc_import.state != (int)Enums.UndDocImportStateEnum.READY)
                {
                    doc_import.state = (int)Enums.UndDocImportStateEnum.READY;
                    doc_import.upd_date = DateTime.Now;
                    doc_import.upd_user = appManager.CurrentUser.Name;
                    dbContext.SaveChanges();
                }
                int? doc_id = doc_import != null ? doc_import.doc_id : null;
                int? partner_id = doc_import != null ? (int?)dbContext.doc_import_config_type.AsNoTracking().Where(ss => ss.config_type_id == doc_import.config_type_id).Select(ss => ss.source_partner_id).FirstOrDefault() : null;


                return new DocImportSupplierResult() { Id = 0, Error = null, import_id = import_id, good_cnt = good_cnt, doc_id = doc_id, partner_id = partner_id };

            }
            catch (Exception ex)
            {
                return new DocImportSupplierResult(ex);
            }
        }

        #endregion

        #region ImportSupplierForMatch

        public DocImportSupplierResult ImportSupplierForMatch(int import_id, string file_path, int config_type_id)
        {
            try
            {
                string file_name = Path.GetFileName(file_path);
                string userName = appManager.CurrentUser.Name;

                DocImportResult importRes = xImport(import_id, file_path, config_type_id);
                if (importRes.Error != null)
                {
                    return new DocImportSupplierResult()
                    {
                        Id = importRes.Id,
                        Error = importRes.Error,
                    };
                }                
                int good_cnt = dbContext.Database.SqlQuery<int>("select * from und.move_import_to_match(@p0, @p1)", import_id, userName).FirstOrDefault();
                
                doc_import doc_import = dbContext.doc_import.AsNoTracking().Where(ss => ss.import_id == import_id).FirstOrDefault();
                
                /*
                if (doc_import.state != (int)Enums.UndDocImportStateEnum.READY)
                {
                    doc_import.state = (int)Enums.UndDocImportStateEnum.READY;
                    doc_import.upd_date = DateTime.Now;
                    doc_import.upd_user = appManager.CurrentUser.Name;
                    dbContext.SaveChanges();
                }
                */
                
                int? doc_id = doc_import != null ? doc_import.doc_id : null;
                int? partner_id = doc_import != null ? (int?)dbContext.doc_import_config_type.AsNoTracking().Where(ss => ss.config_type_id == doc_import.config_type_id).Select(ss => ss.source_partner_id).FirstOrDefault() : null;

                return new DocImportSupplierResult() { Id = 0, Error = null, import_id = import_id, good_cnt = good_cnt, doc_id = doc_id, partner_id = partner_id };

            }
            catch (Exception ex)
            {
                return new DocImportSupplierResult(ex);
            }
        }

        #endregion

        #region xImport

        private DocImportResult xImport(int import_id, string file_path, int config_type_id)
        {
            doc_import_config_type doc_import_config_type = dbContext.doc_import_config_type.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
            if (doc_import_config_type == null)
            {
                return new DocImportResult()
                {
                    Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                    Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найдена конфигурация импорта с кодом " + config_type_id.ToString() },
                };
            }

            switch ((Enums.UndImportFileTypeEnum)doc_import_config_type.file_type)
            {
                case Enums.UndImportFileTypeEnum.EXCEL:
                    return xImportFromXlsx(import_id, file_path, config_type_id);
                case Enums.UndImportFileTypeEnum.XML:
                    return xImportFromXml(import_id, file_path, config_type_id);
                default:
                    return new DocImportResult()
                    {
                        Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                        Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Неподдерживаемый формат файла" },
                    };

            }


        }

        #endregion

        #region xImportFromXlsx

        private DocImportResult xImportFromXlsx(int import_id, string file_path, int config_type_id)
        {
            doc_import_config_type_excel doc_import_config_type = dbContext.doc_import_config_type_excel.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
            if (doc_import_config_type == null)
            {
                return new DocImportResult()
                {
                    Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                    Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найдена конфигурация импорта из Excel с кодом " + config_type_id.ToString() },
                };
            }

            string file_name = Path.GetFileName(file_path);
            int? doc_id = 0;

            using (var stream = File.OpenRead(file_path))
            {
                DateTime now = DateTime.Now;
                string userName = appManager.CurrentUser.Name;

                Workbook workbook;
                IWorkbookFormatProvider formatProvider = new XlsxFormatProvider();
                workbook = formatProvider.Import(stream);
                Worksheet workSheet = workbook.Worksheets[(int)doc_import_config_type.list_num];
                CellRange usedCellRange = workSheet.UsedCellRange;
                int columnFirstIndex = usedCellRange.FromIndex.ColumnIndex;
                int columnLastIndex = usedCellRange.ToIndex.ColumnIndex;

                int? source_partner_id = dbContext.doc_import_config_type.Where(ss => ss.config_type_id == config_type_id).Select(ss => ss.source_partner_id).FirstOrDefault();

                if (!source_partner_id.HasValue)
                    return new DocImportResult()
                    {
                        Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                        Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найдены данные о поставщике" },
                    };

                doc_import doc_import = dbContext.doc_import.Where(ss => ss.import_id == import_id).FirstOrDefault();
                if (doc_import == null)
                {
                    return new DocImportResult()
                    {
                        Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                        Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найден импорт с кодом " + import_id.ToString() },
                    };
                }
                doc_import.config_type_id = doc_import_config_type.config_type_id;
                doc_import.source_partner_id = source_partner_id;
                doc_import.file_path = file_path;
                doc_import.file_name = file_name;
                doc_import.row_cnt = usedCellRange.ToIndex.RowIndex - doc_import_config_type.row_num + 1;
                doc_import.state = (int)Enums.UndDocImportStateEnum.UPLOAD;
                dbContext.SaveChanges();

                doc_import_row doc_import_row = null;
                int cnt = 0;
                AuMainDb transactionContext = null;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
                {
                    transactionContext = null;
                    try
                    {
                        transactionContext = new AuMainDb();
                        transactionContext.Configuration.AutoDetectChangesEnabled = false;


                        cnt = 0;
                        // !!!
                        for (int i = doc_import_config_type.row_num; i <= usedCellRange.ToIndex.RowIndex; i++)
                        //for (int i = usedCellRange.FromIndex.RowIndex + 1; i <= usedCellRange.ToIndex.RowIndex; i++)
                        {
                            ++cnt;

                            doc_import_row = new doc_import_row();
                            //doc_import_row.doc_import = doc_import;
                            doc_import_row.import_id = import_id;
                            for (int j = columnFirstIndex; j <= columnLastIndex; j++)
                            {
                                CellSelection cell = workSheet.Cells[i, j];
                                string cellValue = cell.GetValue().Value.RawValue;

                                if (j == doc_import_config_type.barcode_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.barcode = cellValue;
                                }
                                if (j == doc_import_config_type.code_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.code = cellValue;
                                }
                                if (j == doc_import_config_type.etalon_name_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.etalon_name = cellValue;
                                }
                                if (j == doc_import_config_type.mfr_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.mfr = cellValue;
                                }
                                if (j == doc_import_config_type.mfr_group_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.mfr_group = cellValue;
                                }
                                if (j == doc_import_config_type.source_name_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.source_name = cellValue;
                                }
                                if (j == doc_import_config_type.apply_group_col.GetValueOrDefault(0))
                                {
                                    doc_import_row.apply_group_name = cellValue;
                                }
                            }
                            doc_import_row.crt_date = now;
                            doc_import_row.crt_user = userName;
                            doc_import_row.upd_date = now;
                            doc_import_row.upd_user = userName;

                            transactionContext = AddToContext<doc_import_row>(transactionContext, doc_import_row, cnt, 100, true);
                        }

                        transactionContext.SaveChanges();

                    }
                    finally
                    {
                        if (transactionContext != null)
                            transactionContext.Dispose();
                    }

                    scope.Complete();

                }
            }

            return new DocImportResult() { Id = 0, Error = null, import_id = import_id, doc_id = doc_id };
        }

        #endregion

        #region xImportFromXml

        private DocImportResult xImportFromXml(int import_id, string file_path, int config_type_id)
        {
            doc_import_config_type_xml doc_import_config_type = dbContext.doc_import_config_type_xml.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
            if (doc_import_config_type == null)
            {
                return new DocImportResult()
                {
                    Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                    Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найдена конфигурация импорта из XML с кодом " + config_type_id.ToString() },
                };
            }

            DateTime now = DateTime.Now;
            string userName = appManager.CurrentUser.Name;

            string file_name = Path.GetFileName(file_path);
            int? doc_id = 0;
            int? source_partner_id = null;

            string stringForEmpty = "xxx";
            string elSourcePartnerName = String.IsNullOrWhiteSpace(doc_import_config_type.source_partner_name_element) ? stringForEmpty : doc_import_config_type.source_partner_name_element.Trim();
            string elGood = String.IsNullOrWhiteSpace(doc_import_config_type.good_element) ? stringForEmpty : doc_import_config_type.good_element.Trim();
            string elName = String.IsNullOrWhiteSpace(doc_import_config_type.source_name_element) ? stringForEmpty : doc_import_config_type.source_name_element.Trim();
            string elEtalonName = String.IsNullOrWhiteSpace(doc_import_config_type.etalon_name_element) ? stringForEmpty : doc_import_config_type.etalon_name_element.Trim();
            string elMfr = String.IsNullOrWhiteSpace(doc_import_config_type.mfr_element) ? stringForEmpty : doc_import_config_type.mfr_element.Trim();
            string elMfrGroup = String.IsNullOrWhiteSpace(doc_import_config_type.mfr_group_element) ? stringForEmpty : doc_import_config_type.mfr_group_element.Trim();
            string elCode = String.IsNullOrWhiteSpace(doc_import_config_type.code_element) ? stringForEmpty : doc_import_config_type.code_element.Trim();
            string elBarcode = String.IsNullOrWhiteSpace(doc_import_config_type.barcode_element) ? stringForEmpty : doc_import_config_type.barcode_element.Trim();
            string elApplyGroup = String.IsNullOrWhiteSpace(doc_import_config_type.apply_group_element) ? stringForEmpty : doc_import_config_type.apply_group_element.Trim();
            elEtalonName = elEtalonName == stringForEmpty ? elName : elEtalonName;

            using (var stream = File.OpenRead(file_path))
            {                
                var xRoot = XElement.Load(stream);

                if (elSourcePartnerName != stringForEmpty)
                {
                    var attrPartner = xRoot.Attributes().Where(ss => ss.Name.ToString().Trim().Equals(elSourcePartnerName)).FirstOrDefault();
                    if ((attrPartner == null) || (String.IsNullOrWhiteSpace(attrPartner.Value)))
                        return new DocImportResult()
                        {
                            Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                            Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "В файле отсутствуют данные о поставщике" },
                        };
                    string partnerName = attrPartner.Value.Trim();
                    partner partner = dbContext.partner.Where(ss => ss.is_supplier && ss.partner_name.Trim().ToLower().Equals(partnerName.Trim().ToLower())).FirstOrDefault();
                    if (partner == null)
                    {
                        partner = new partner();
                        partner.is_source = false;
                        partner.is_supplier = true;
                        partner.partner_name = partnerName;
                        partner.crt_date = now;
                        partner.crt_user = userName;                        
                        partner.upd_date = now;
                        partner.upd_user = userName;
                        partner.is_deleted = false;
                        dbContext.partner.Add(partner);
                        dbContext.SaveChanges();                        
                    }
                    source_partner_id = partner.partner_id;
                }
                else
                {
                    source_partner_id = dbContext.doc_import_config_type.Where(ss => ss.config_type_id == config_type_id).Select(ss => ss.source_partner_id).FirstOrDefault();
                }
                if (!source_partner_id.HasValue)
                    return new DocImportResult()
                    {
                        Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                        Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найдены данные о поставщике" },
                    };

                var goods = xRoot.Elements().Where(ss => ss.Name.ToString().Trim().Equals(elGood)).ToList();

                if (goods.Count <= 0)
                    return new DocImportResult()
                    {
                        Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                        Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "В файле отсутствуют данные о товарах" },
                    };

                doc_import doc_import = dbContext.doc_import.Where(ss => ss.import_id == import_id).FirstOrDefault();
                if (doc_import == null)
                {
                    return new DocImportResult()
                    {
                        Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                        Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найден импорт с кодом " + import_id.ToString() },
                    };
                }
                doc_import.config_type_id = doc_import_config_type.config_type_id;
                doc_import.source_partner_id = source_partner_id;
                doc_import.file_path = file_path;
                doc_import.file_name = file_name;
                doc_import.row_cnt = goods.Count;
                doc_import.state = (int)Enums.UndDocImportStateEnum.UPLOAD;
                dbContext.SaveChanges();                

                doc_import_row doc_import_row = null;
                int cnt = 0;
                AuMainDb transactionContext = null;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
                {
                    transactionContext = null;
                    try
                    {
                        transactionContext = new AuMainDb();
                        transactionContext.Configuration.AutoDetectChangesEnabled = false;

                        cnt = 0; 
                        foreach (var good in goods)                        
                        {
                            ++cnt;

                            doc_import_row = new doc_import_row();           
                 
                            doc_import_row.import_id = import_id;
                            doc_import_row.barcode = good.Attribute(elBarcode) != null ? good.Attribute(elBarcode).Value : null;
                            doc_import_row.code = good.Attribute(elCode) != null ? good.Attribute(elCode).Value : null;
                            doc_import_row.etalon_name = good.Attribute(elEtalonName) != null ? good.Attribute(elEtalonName).Value : null;
                            doc_import_row.mfr = good.Attribute(elMfr) != null ? good.Attribute(elMfr).Value : null;
                            doc_import_row.mfr_group = good.Attribute(elMfrGroup) != null ? good.Attribute(elMfrGroup).Value : null;
                            doc_import_row.source_name = good.Attribute(elName) != null ? good.Attribute(elName).Value : null;
                            doc_import_row.apply_group_name = good.Attribute(elApplyGroup) != null ? good.Attribute(elApplyGroup).Value : null;

                            doc_import_row.crt_date = now;
                            doc_import_row.crt_user = userName;
                            doc_import_row.upd_date = now;
                            doc_import_row.upd_user = userName;

                            transactionContext = AddToContext<doc_import_row>(transactionContext, doc_import_row, cnt, 100, true);
                        }

                        transactionContext.SaveChanges();

                    }
                    finally
                    {
                        if (transactionContext != null)
                            transactionContext.Dispose();
                    }

                    scope.Complete();
                }
            }

            return new DocImportResult() { Id = 0, Error = null, import_id = import_id, doc_id = doc_id };
        }

        #endregion

        #region CreateDocImport

        public int CreateDocImport()
        {
            try
            {
                DateTime now = DateTime.Now;
                string user = appManager.CurrentUser.Name;

                doc_import doc_import = new doc_import();
                doc_import.state = (int)Enums.UndDocImportStateEnum.WAIT;
                doc_import.crt_date = now;
                doc_import.crt_user = user;
                doc_import.upd_date = now;
                doc_import.upd_user = user;
                doc_import.is_deleted = false;
                dbContext.doc_import.Add(doc_import);
                dbContext.SaveChanges();

                return doc_import.import_id;
            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        #endregion

        #region GetDocImportRowsCount

        public DocImportInfoResult GetDocImportRowsInfo(int? import_id)
        {

            vw_doc_import vw_doc_import = dbParallelContext.vw_doc_import.AsNoTracking().Where(ss => ss.import_id == import_id).FirstOrDefault();
            if (vw_doc_import == null)
                return new DocImportInfoResult()
                {
                    Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND,
                    Error = new ErrInfo() { Id = (int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, Mess = "Не найде импорт с кодом " + import_id.ToString() },
                };

            return new DocImportInfoResult() 
            {
                Id = 0,
                Error = null,
                doc_id = vw_doc_import.doc_id,
                import_id = vw_doc_import.import_id,
                row_all_cnt = vw_doc_import.row_cnt,
                row_done_cnt = vw_doc_import.row_done_cnt,
                state = vw_doc_import.state,
                doc_name = vw_doc_import.doc_name,
            };
        }

        #endregion

        #region GetDocImportRows

        public BindingList<doc_import_row> GetDocImportRows(int? import_id)
        {
            dbContext.doc_import_row.Where(ss => ss.import_id == import_id).Load();
            return dbContext.doc_import_row.Local.ToBindingList();
        }

        #endregion

        #region GetDocImportSources

        public List<partner> GetDocImportSources()
        {
            return dbContext.partner.Where(ss => ss.is_source).ToList();
        }

        #endregion

        #region GetDocImportConfigType

        public doc_import_config_type GetDocImportConfigType(int config_type_id)
        {
            return dbContext.doc_import_config_type.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
        }

        #endregion

        #region GetDocImportConfigTypes

        public List<doc_import_config_type> GetDocImportConfigTypes()
        {
            return dbContext.doc_import_config_type.ToList();
        }

        #endregion

        #region InsertDocImportConfigType_Excel

        public bool InsertDocImportConfigType_Excel(string config_type_name, int source_partner_id,
                int list_num, int row_num, int source_name_col, int? etalon_name_col, int? code_col,
                int? mfr_col, int? mfr_group_col, int? barcode_col
            )
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            doc_import_config_type doc_import_config_type = new doc_import_config_type();
            doc_import_config_type.config_type_name = config_type_name;
            doc_import_config_type.source_partner_id = source_partner_id;
            doc_import_config_type.file_type = (int)Enums.UndImportFileTypeEnum.EXCEL;
            doc_import_config_type.crt_date = now;
            doc_import_config_type.crt_user = user;
            doc_import_config_type.upd_date = now;
            doc_import_config_type.upd_user = user;
            dbContext.doc_import_config_type.Add(doc_import_config_type);

            doc_import_config_type_excel doc_import_config_type_excel = new doc_import_config_type_excel();
            doc_import_config_type_excel.doc_import_config_type = doc_import_config_type;
            doc_import_config_type_excel.list_num = list_num;
            doc_import_config_type_excel.row_num = row_num;
            doc_import_config_type_excel.source_name_col = source_name_col;
            doc_import_config_type_excel.etalon_name_col = etalon_name_col;
            doc_import_config_type_excel.code_col = code_col;
            doc_import_config_type_excel.mfr_col = mfr_col;
            doc_import_config_type_excel.mfr_group_col = mfr_group_col;
            doc_import_config_type_excel.barcode_col = barcode_col;
            dbContext.doc_import_config_type_excel.Add(doc_import_config_type_excel);
            
            dbContext.SaveChanges();

            return true;
        }

        #endregion

        #region UpdateDocImportConfigType_Excel

        public bool UpdateDocImportConfigType_Excel(int config_type_id, string config_type_name, int source_partner_id,
                int list_num, int row_num, int source_name_col, int? etalon_name_col, int? code_col,
                int? mfr_col, int? mfr_group_col, int? barcode_col
            )
        {
            doc_import_config_type doc_import_config_type = dbContext.doc_import_config_type.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
            doc_import_config_type.config_type_name = config_type_name;
            doc_import_config_type.source_partner_id = source_partner_id;

            doc_import_config_type_excel doc_import_config_type_excel = dbContext.doc_import_config_type_excel.Where(ss => ss.config_type_id == config_type_id).FirstOrDefault();
            doc_import_config_type_excel.list_num = list_num;
            doc_import_config_type_excel.row_num = row_num;
            doc_import_config_type_excel.source_name_col = source_name_col;
            doc_import_config_type_excel.etalon_name_col = etalon_name_col;
            doc_import_config_type_excel.code_col = code_col;
            doc_import_config_type_excel.mfr_col = mfr_col;
            doc_import_config_type_excel.mfr_group_col = mfr_group_col;
            doc_import_config_type_excel.barcode_col = barcode_col;            
            dbContext.SaveChanges();
            return true;
        }

        #endregion

    }
}

