﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{
    public class ProductServiceResult : ServiceResult
    {
        public ProductServiceResult()
            : base()
        {
            //
        }

        public ProductServiceResult(Exception ex)
            : base(ex)
        {
            //
        }

        public List<Product> products_all { get; set; }
        public List<Product> products { get; set; }
        public List<Good> goods { get; set; }
        public int product_id_max { get; set; }
        //public int alias_id_max { get; set; }
    }

    public class ProductService: UndBaseService
    {
        public ProductService(AppManager appManager)
            :base(appManager)
        {
            //
        }

        public ProductServiceResult GetProducts_Etalon()
        {
            try
            {
                /*
                List<Product> res = dbContext.vw_product_and_good
                    .ProjectTo<Product>(ModelMapper.ConfigurationProvider)
                    .ToList();
                int? res_max = res.Max(ss => (int?)ss.product_id);
                int? res_alias_max = dbContext.product_alias.Max(ss => (int?)ss.alias_id);

                return new ProductServiceResult() 
                { 
                    Error = null,
                    Id = 0,
                    products_all = new List<Product>(res),
                    products = res.GroupBy(ss => ss.product_id)
                        .Select(grp => grp.First())
                        .Select(ss => new Product()
                        {
                            product_id = ss.product_id,
                            product_name = ss.product_name,
                            doc_id = ss.doc_id,
                            doc_row_id = ss.doc_row_id,
                            is_deleted = ss.is_deleted,
                            good_cnt = ss.good_cnt,
                            apply_group_name = ss.apply_group_name,
                        }).ToList(),
                    goods = res.Where(ss => ss.good_id.HasValue)
                        .GroupBy(ss => ss.good_id)
                        .Select(grp => grp.First())
                        .Select(ss => new Good()
                        {
                            product_id = ss.product_id,
                            partner_id = (int)ss.partner_id,
                            good_id = (int)ss.good_id,
                            partner_code = ss.partner_code,
                            partner_name = ss.partner_name,
                            source_partner_name = ss.source_partner_name,
                            mfr_id = (int)ss.mfr_id,
                            barcode = ss.barcode,
                            doc_id = ss.good_doc_id,
                            doc_row_id = ss.good_doc_row_id,
                            mfr_name = ss.mfr_name,                       
                            apply_group_name = ss.apply_group_name,
                        }).Distinct().ToList(),
                    //
                    product_id_max = res_max.GetValueOrDefault(0),
                    alias_id_max = res_alias_max.GetValueOrDefault(0),
                };
                */
                List<Product> res = dbContext.vw_product
                    .Select(ss => new Product()
                    {
                        product_id = ss.product_id,
                        product_name = ss.product_name,
                        doc_id = ss.doc_id,
                        doc_row_id = ss.doc_row_id,
                        is_deleted = ss.is_deleted,
                        good_cnt = ss.good_cnt,
                        apply_group_name = ss.apply_group_name,
                    })
                    .ToList();
                int? res_max = res.Max(ss => (int?)ss.product_id);
                //int? res_alias_max = dbContext.product_alias.Max(ss => (int?)ss.alias_id);

                return new ProductServiceResult()
                {
                    Error = null,
                    Id = 0,
                    products_all = new List<Product>(),
                    products = new List<Product>(res),
                    goods = new List<Good>(),
                    /*
                    products = res.GroupBy(ss => ss.product_id)
                        .Select(grp => grp.First())
                        .Select(ss => new Product()
                        {
                            product_id = ss.product_id,
                            product_name = ss.product_name,
                            doc_id = ss.doc_id,
                            doc_row_id = ss.doc_row_id,
                            is_deleted = ss.is_deleted,
                            good_cnt = ss.good_cnt,
                            apply_group_name = ss.apply_group_name,
                        }).ToList(),
                    goods = res.Where(ss => ss.good_id.HasValue)
                        .GroupBy(ss => ss.good_id)
                        .Select(grp => grp.First())
                        .Select(ss => new Good()
                        {
                            product_id = ss.product_id,
                            partner_id = (int)ss.partner_id,
                            good_id = (int)ss.good_id,
                            partner_code = ss.partner_code,
                            partner_name = ss.partner_name,
                            source_partner_name = ss.source_partner_name,
                            mfr_id = (int)ss.mfr_id,
                            barcode = ss.barcode,
                            doc_id = ss.good_doc_id,
                            doc_row_id = ss.good_doc_row_id,
                            mfr_name = ss.mfr_name,
                            apply_group_name = ss.apply_group_name,
                        }).Distinct().ToList(),
                    */
                    //
                    product_id_max = res_max.GetValueOrDefault(0),
                    //alias_id_max = res_alias_max.GetValueOrDefault(0),
                };

            }
            catch (Exception ex)
            {
                return new ProductServiceResult(ex);
            }
        }

        public ProductServiceResult GetProducts_Full()
        {
            try
            {
                /*
                List<Product> res = dbContext.vw_product_and_good
                    .ProjectTo<Product>(ModelMapper.ConfigurationProvider)
                    .ToList();
                int? res_max = res.Max(ss => (int?)ss.product_id);
                int? res_alias_max = dbContext.product_alias.Max(ss => (int?)ss.alias_id);

                return new ProductServiceResult() 
                { 
                    Error = null,
                    Id = 0,
                    products_all = new List<Product>(res),
                    products = res.GroupBy(ss => ss.product_id)
                        .Select(grp => grp.First())
                        .Select(ss => new Product()
                        {
                            product_id = ss.product_id,
                            product_name = ss.product_name,
                            doc_id = ss.doc_id,
                            doc_row_id = ss.doc_row_id,
                            is_deleted = ss.is_deleted,
                            good_cnt = ss.good_cnt,
                            apply_group_name = ss.apply_group_name,
                        }).ToList(),
                    goods = res.Where(ss => ss.good_id.HasValue)
                        .GroupBy(ss => ss.good_id)
                        .Select(grp => grp.First())
                        .Select(ss => new Good()
                        {
                            product_id = ss.product_id,
                            partner_id = (int)ss.partner_id,
                            good_id = (int)ss.good_id,
                            partner_code = ss.partner_code,
                            partner_name = ss.partner_name,
                            source_partner_name = ss.source_partner_name,
                            mfr_id = (int)ss.mfr_id,
                            barcode = ss.barcode,
                            doc_id = ss.good_doc_id,
                            doc_row_id = ss.good_doc_row_id,
                            mfr_name = ss.mfr_name,                       
                            apply_group_name = ss.apply_group_name,
                        }).Distinct().ToList(),
                    //
                    product_id_max = res_max.GetValueOrDefault(0),
                    alias_id_max = res_alias_max.GetValueOrDefault(0),
                };
                */
                List<Product> res = dbContext.vw_product_and_good
                    .Where(ss => ss.is_main == true)
                    .ProjectTo<Product>(ModelMapper.ConfigurationProvider)
                    .ToList();
                int? res_max = res.Max(ss => (int?)ss.product_id);
                //int? res_alias_max = dbContext.product_alias.Max(ss => (int?)ss.alias_id);

                return new ProductServiceResult()
                {
                    Error = null,
                    Id = 0,
                    products_all = new List<Product>(res),
                    products = new List<Product>(),
                    goods = new List<Good>(),
                    /*
                    products = res.GroupBy(ss => ss.product_id)
                        .Select(grp => grp.First())
                        .Select(ss => new Product()
                        {
                            product_id = ss.product_id,
                            product_name = ss.product_name,
                            doc_id = ss.doc_id,
                            doc_row_id = ss.doc_row_id,
                            is_deleted = ss.is_deleted,
                            good_cnt = ss.good_cnt,
                            apply_group_name = ss.apply_group_name,
                        }).ToList(),
                    goods = res.Where(ss => ss.good_id.HasValue)
                        .GroupBy(ss => ss.good_id)
                        .Select(grp => grp.First())
                        .Select(ss => new Good()
                        {
                            product_id = ss.product_id,
                            partner_id = (int)ss.partner_id,
                            good_id = (int)ss.good_id,
                            partner_code = ss.partner_code,
                            partner_name = ss.partner_name,
                            source_partner_name = ss.source_partner_name,
                            mfr_id = (int)ss.mfr_id,
                            barcode = ss.barcode,
                            doc_id = ss.good_doc_id,
                            doc_row_id = ss.good_doc_row_id,
                            mfr_name = ss.mfr_name,
                            apply_group_name = ss.apply_group_name,
                        }).Distinct().ToList(),
                    */
                    //
                    product_id_max = res_max.GetValueOrDefault(0),
                    //alias_id_max = res_alias_max.GetValueOrDefault(0),
                };

            }
            catch (Exception ex)
            {
                return new ProductServiceResult(ex);
            }
        }

        public ServiceResult SaveProducts(List<Product> addedProducts, List<Product> modifiedProducts, List<Product> deletedProducts,
            List<ProductAlias> addedProductAliases, List<ProductAlias> modifiedProductAliases, List<ProductAlias> deletedProductAliases
            )
        {
            try
            {
                DateTime now = DateTime.Now;
                string userName = appManager.CurrentUser.Name;

                product product = null;
                Product Product = null;
                product_alias product_alias = null;

                if ((addedProducts != null) && (addedProducts.Count > 0))
                {
                    foreach (var addedProduct in addedProducts)
                    {
                        if (String.IsNullOrWhiteSpace(addedProduct.product_name))
                            continue;
                        product = dbContext.product.Where(ss => ss.product_name.Trim().ToLower().Equals(addedProduct.product_name.Trim().ToLower())).FirstOrDefault();
                        if (product != null)
                            continue;
                        Product = addedProducts.Where(ss => ss.product_id > addedProduct.product_id && ss.product_name.Trim().ToLower().Equals(addedProduct.product_name.Trim().ToLower())).FirstOrDefault();
                        if (Product != null)
                            continue;
                        product = new product();
                        product.product_name = addedProduct.product_name;
                        product.crt_date = now;
                        product.crt_user = userName;
                        product.upd_date = now;
                        product.upd_user = userName;
                        dbContext.product.Add(product);
                        if ((addedProductAliases != null) && (addedProductAliases.Count > 0))
                        {
                            foreach (var addedProductAlias in addedProductAliases.Where(ss => ss.product_id == addedProduct.product_id).ToList())
                            {
                                product_alias = new product_alias();
                                product_alias.product = product;
                                product_alias.alias_name = addedProductAlias.alias_name;
                                product_alias.crt_date = now;
                                product_alias.crt_user = userName;
                                product_alias.upd_date = now;
                                product_alias.upd_user = userName;
                                dbContext.product_alias.Add(product_alias);
                            }
                        }
                    }
                }
                if ((modifiedProducts != null) && (modifiedProducts.Count > 0))
                {
                    foreach (var modifiedProduct in modifiedProducts)
                    {
                        if (String.IsNullOrWhiteSpace(modifiedProduct.product_name))
                            continue;
                        product = dbContext.product.Where(ss => ss.product_id != modifiedProduct.product_id && ss.product_name.Trim().ToLower().Equals(modifiedProduct.product_name.Trim().ToLower())).FirstOrDefault();
                        if (product != null)
                            continue;
                        Product = modifiedProducts.Where(ss => ss.product_id > modifiedProduct.product_id && ss.product_name.Trim().ToLower().Equals(modifiedProduct.product_name.Trim().ToLower())).FirstOrDefault();
                        if (Product != null)
                            continue;
                        product = dbContext.product.Where(ss => ss.product_id == modifiedProduct.product_id).FirstOrDefault();
                        if (product != null)
                        {
                            product.upd_date = now;
                            product.upd_user = userName;
                            product.doc_id = null;
                            product.doc_row_id = null;
                            if ((addedProductAliases != null) && (addedProductAliases.Count > 0))
                            {
                                foreach (var addedProductAlias in addedProductAliases.Where(ss => ss.product_id == modifiedProduct.product_id).ToList())
                                {
                                    product_alias = new product_alias();
                                    product_alias.product = product;
                                    product_alias.alias_name = addedProductAlias.alias_name;
                                    product_alias.crt_date = now;
                                    product_alias.crt_user = userName;
                                    product_alias.upd_date = now;
                                    product_alias.upd_user = userName;
                                    dbContext.product_alias.Add(product_alias);
                                }
                            }
                            if ((modifiedProductAliases != null) && (modifiedProductAliases.Count > 0))
                            {
                                foreach (var modifiedProductAlias in modifiedProductAliases.Where(ss => ss.product_id == modifiedProduct.product_id).ToList())
                                {
                                    product_alias = dbContext.product_alias.Where(ss => ss.product_id == product.product_id && ss.alias_id == modifiedProductAlias.alias_id).FirstOrDefault();
                                    if (product_alias == null)
                                        continue;
                                    product_alias.alias_name = modifiedProductAlias.alias_name;
                                    product_alias.upd_date = now;
                                    product_alias.upd_user = userName;                             
                                }
                            }
                            if ((deletedProductAliases != null) && (deletedProductAliases.Count > 0))
                            {
                                foreach (var deletedProductAlias in deletedProductAliases.Where(ss => ss.product_id == modifiedProduct.product_id).ToList())
                                {
                                    product_alias = dbContext.product_alias.Where(ss => ss.product_id == product.product_id && ss.alias_id == deletedProductAlias.alias_id).FirstOrDefault();
                                    if (product_alias == null)
                                        continue;
                                    dbContext.product_alias.Remove(product_alias);
                                }
                            }
                        }
                    }
                }
                if ((deletedProducts != null) && (deletedProducts.Count > 0))
                {
                    foreach (var deletedProduct in deletedProducts)
                    {
                        product = dbContext.product.Where(ss => ss.product_id == deletedProduct.product_id).FirstOrDefault();
                        if (product != null)
                        {
                            dbContext.product_alias.RemoveRange(dbContext.product_alias.Where(ss => ss.product_id == product.product_id));
                            dbContext.product.Remove(product);
                        }
                    }
                }

                if ((addedProductAliases != null) && (addedProductAliases.Count > 0))
                {
                    foreach (var addedProductAlias in addedProductAliases.Where(ss => !addedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        &&
                        !modifiedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        &&
                        !deletedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        ).ToList())
                    {
                        product = dbContext.product.Where(ss => ss.product_id == addedProductAlias.product_id).FirstOrDefault();
                        if (product == null)
                            continue;
                        product_alias = new product_alias();
                        product_alias.product = product;
                        product_alias.alias_name = addedProductAlias.alias_name;
                        product_alias.crt_date = now;
                        product_alias.crt_user = userName;
                        product_alias.upd_date = now;
                        product_alias.upd_user = userName;
                        dbContext.product_alias.Add(product_alias);
                    }
                }
                if ((modifiedProductAliases != null) && (modifiedProductAliases.Count > 0))
                {
                    foreach (var modifiedProductAlias in modifiedProductAliases.Where(ss => !addedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        &&
                        !modifiedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        &&
                        !deletedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        ).ToList())
                    {
                        product_alias = dbContext.product_alias.Where(ss => ss.product_id == modifiedProductAlias.product_id && ss.alias_id == modifiedProductAlias.alias_id).FirstOrDefault();
                        if (product_alias == null)
                            continue;
                        product_alias.alias_name = modifiedProductAlias.alias_name;
                        product_alias.upd_date = now;
                        product_alias.upd_user = userName;
                    }
                }
                if ((deletedProductAliases != null) && (deletedProductAliases.Count > 0))
                {
                    foreach (var deletedProductAlias in deletedProductAliases.Where(ss => !addedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        &&
                        !modifiedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        &&
                        !deletedProducts.Where(tt => tt.product_id == ss.product_id).Any()
                        ).ToList())
                    {
                        product_alias = dbContext.product_alias.Where(ss => ss.product_id == deletedProductAlias.product_id && ss.alias_id == deletedProductAlias.alias_id).FirstOrDefault();
                        if (product_alias == null)
                            continue;
                        dbContext.product_alias.Remove(product_alias);
                    }
                }

                dbContext.SaveChanges();

                return new ServiceResult() { Error = null, Id = 0, };
            }
            catch (Exception ex)
            {
                return new ServiceResult(ex);
            }
        }

    }
}

