﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{

    public class SupplierService : UndBaseService
    {
        public SupplierService(AppManager appManager)
            : base(appManager)
        {
            //
        }

        public List<Partner> GetSuppliers()
        {
            return dbContext.partner
                .Where(ss => ss.is_supplier)                
                .ProjectTo<Partner>(ModelMapper.ConfigurationProvider)
                .OrderBy(ss => ss.partner_name)
                .ToList();
        }
    }
}