﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using EsnAdmin.ViewModel;


namespace EsnAdmin.Service
{

    public class AutoMapperCommon: IMapper
    {
        static AutoMapperCommon() 
        {        
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<vw_product, Product>();
                cfg.CreateMap<vw_product_and_good, Product>();
                cfg.CreateMap<vw_partner_good, Good>();
                cfg.CreateMap<partner, Partner>();
                cfg.CreateMap<vw_doc, Doc>();
                cfg.CreateMap<vw_doc_import_match, DocImportMatch>()
                    .ForMember(ss => ss.is_changed, tt => tt.UseValue<bool>(false));
                cfg.CreateMap<vw_doc_import_match_variant, DocImportMatchVariant>();
                cfg.CreateMap<manufacturer, Mfr>();
                cfg.CreateMap<manufacturer_alias, MfrAlias>();
                cfg.CreateMap<apply_group, ApplyGroup>();
            });            
        }

        public IConfigurationProvider ConfigurationProvider { get { return Mapper.Configuration; } }

        public Func<Type, object> ServiceCtor { get { return Mapper.Configuration.ServiceCtor; } }

        
        public object Map(object source, Type sourceType, Type destinationType)
        {                        
            return Mapper.Map(source, sourceType, destinationType); 
        }

        public TDestination Map<TDestination>(object source)
        {
            return Mapper.Map<TDestination>(source);
        }

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return Mapper.Map<TSource, TDestination>(source);
        }

        public TDestination Map<TDestination>(object source, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map<TDestination>(source, opts);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return Mapper.Map<TSource, TDestination>(source, destination);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            return Mapper.Map<TSource, TDestination>(source, destination, opts);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType)
        {
            return Mapper.Map(source, destination, sourceType, destinationType);
        }

        public object Map(object source, Type sourceType, Type destinationType, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map(source, sourceType, destinationType, opts);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map(source, destination, sourceType, destinationType, opts);
        }

        public TDestination Map<TSource, TDestination>(TSource source, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            return Mapper.Map<TSource, TDestination>(source, opts);
        }
    }
}

