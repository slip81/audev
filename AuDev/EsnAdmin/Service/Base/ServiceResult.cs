﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace EsnAdmin.Service
{
    public class ServiceResult
    {
        public ServiceResult()
        {
            Id = 0;
            Error = null;
        }

        public ServiceResult(Exception ex)
        {
            Id = (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            Error = new ErrInfo(ex);
        }

        public int Id { get; set; }
        public ErrInfo Error { get; set; }
    }
}