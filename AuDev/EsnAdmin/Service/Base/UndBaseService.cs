﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using EsnAdmin.Core;


namespace EsnAdmin.Service
{
    public class UndBaseService : IDisposable
    {
        protected AppManager appManager;

        public AutoMapper.IMapper ModelMapper
        {
            get
            {
                return appManager.ModelMapper;
            }
        }

        protected AuMainDb dbContext
        {
            get
            {
                return appManager != null ? appManager.dbContext : null;
            }
        }

        protected AuMainDb dbParallelContext
        {
            get
            {
                return appManager != null ? appManager.dbParallelContext : null;
            }
        }

        public UndBaseService()
        {                      
            //
        }
        
        public UndBaseService(AppManager _appManager)
        {
            appManager = _appManager;
        }        

        public void Dispose()
        {
            //
        }

        // Fastest Way of Inserting in Entity Framework
        // http://stackoverflow.com/questions/5940225/fastest-way-of-inserting-in-entity-framework

        protected AuMainDb AddToContext<TEntity>(AuMainDb context,
            TEntity entity, int count, int commitCount, bool recreateContext)
            where TEntity : class
        {
            context.Set<TEntity>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        protected AuMainDb AddToContext_2<TAddEntity1, TAddEntity2>(AuMainDb context,
            TAddEntity1 entity1, TAddEntity2 entity2, int count, int commitCount, bool recreateContext)
            where TAddEntity1 : class
            where TAddEntity2 : class            
        {
            if (entity1 != null)
                context.Set<TAddEntity1>().Add(entity1);
            if (entity2 != null)
                context.Set<TAddEntity2>().Add(entity2);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        protected AuMainDb AddToContext_3<TAddEntity1, TAddEntity2, TAddEntity3>(AuMainDb context,
            TAddEntity1 entity1, TAddEntity2 entity2, TAddEntity3 entity3, int count, int commitCount, bool recreateContext)
            where TAddEntity1 : class
            where TAddEntity2 : class
            where TAddEntity3 : class
        {
            if (entity1 != null)
                context.Set<TAddEntity1>().Add(entity1);
            if (entity2 != null)
                context.Set<TAddEntity2>().Add(entity2);
            if (entity3 != null)
                context.Set<TAddEntity3>().Add(entity3);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        protected AuMainDb AddToContext_4<TAddEntity1, TAddEntity2, TAddEntity3, TAddEntity4>(AuMainDb context,
            TAddEntity1 entity1, TAddEntity2 entity2, TAddEntity3 entity3, TAddEntity4 entity4, int count, int commitCount, bool recreateContext)
            where TAddEntity1 : class
            where TAddEntity2 : class
            where TAddEntity3 : class
            where TAddEntity4 : class            
        {
            if (entity1 != null)
                context.Set<TAddEntity1>().Add(entity1);
            if (entity2 != null)
                context.Set<TAddEntity2>().Add(entity2);
            if (entity3 != null)
                context.Set<TAddEntity3>().Add(entity3);
            if (entity4 != null)
                context.Set<TAddEntity4>().Add(entity4);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        protected AuMainDb AddToContext_5<TAddEntity1, TAddEntity2, TAddEntity3, TAddEntity4, TAddEntity5>(AuMainDb context,
            TAddEntity1 entity1, TAddEntity2 entity2, TAddEntity3 entity3, TAddEntity4 entity4, TAddEntity5 entity5, int count, int commitCount, bool recreateContext)
            where TAddEntity1 : class
            where TAddEntity2 : class
            where TAddEntity3 : class
            where TAddEntity4 : class
            where TAddEntity5 : class
        {
            if (entity1 != null)
                context.Set<TAddEntity1>().Add(entity1);
            if (entity2 != null)
                context.Set<TAddEntity2>().Add(entity2);
            if (entity3 != null)
                context.Set<TAddEntity3>().Add(entity3);
            if (entity4 != null)
                context.Set<TAddEntity4>().Add(entity4);
            if (entity5 != null)
                context.Set<TAddEntity5>().Add(entity5);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

    }

}
