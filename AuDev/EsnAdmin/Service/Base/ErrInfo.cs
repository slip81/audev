﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace EsnAdmin.Service
{
    public class ErrInfo
    {
        public ErrInfo()
        {
            //        
        }

        public ErrInfo(Exception ex)
        {
            Id = (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            Mess = GlobalUtil.ExceptionInfo(ex);
        }

        public int Id { get; set; }
        public string Mess { get; set; }

    }
}