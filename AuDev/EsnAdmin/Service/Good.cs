﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{
    public class GoodServiceResult : ServiceResult
    {
        public GoodServiceResult()
            : base()
        {
            //
        }

        public GoodServiceResult(Exception ex)
            : base(ex)
        {
            //
        }

        public List<Good> goods { get; set; }
        public int good_id_max { get; set; }
    }

    public class GoodService : UndBaseService
    {
        public GoodService(AppManager appManager)
            : base(appManager)
        {
            //
        }

        #region GetGoods

        public GoodServiceResult GetGoods(int? partner_id)
        {
            try
            {
                List<Good> res = dbContext.vw_partner_good
                    .Where(ss => (partner_id.HasValue && ss.partner_id == partner_id) || (!partner_id.HasValue))
                    .ProjectTo<Good>(ModelMapper.ConfigurationProvider)
                    .ToList();
                int? res_max = res.Max(ss => (int?)ss.good_id);
                return new GoodServiceResult() { Error = null, Id = 0, goods = new List<Good>(res), good_id_max = res_max.GetValueOrDefault(0), };
            }
            catch (Exception ex)
            {
                return new GoodServiceResult(ex);
            }            
        }

        public GoodServiceResult GetGoods_byProduct(int? product_id, bool without_main = false)
        {
            try
            {
                List<Good> res = dbContext.vw_partner_good
                    .Where(ss => ss.product_id == product_id 
                        && (((without_main) && (!ss.is_main)) || (!without_main))
                    )
                    .ProjectTo<Good>(ModelMapper.ConfigurationProvider)
                    .ToList();
                int? res_max = res.Max(ss => (int?)ss.good_id);
                return new GoodServiceResult() 
                { 
                    Error = null, 
                    Id = 0, 
                    goods = new List<Good>(res), 
                    good_id_max = res_max.GetValueOrDefault(0), 
                };
            }
            catch (Exception ex)
            {
                return new GoodServiceResult(ex);
            }
        }

        #endregion

        // Entity Framework: повышаем производительность при сохранении данных в БД
        // https://habrahabr.ru/post/251397/

        #region InsertGoods_Etalon

        public GoodServiceResult InsertGoods_Etalon(IEnumerable<DocImportMatch> itemsAdd)
        {
            try
            {
                if ((itemsAdd == null) || (itemsAdd.Count() <= 0))
                    return new GoodServiceResult() { Id = 0, Error = null };

                DateTime now = DateTime.Now;
                string user = appManager.CurrentUser.Name;

                product product = null;
                manufacturer mfr = null;
                apply_group apply_group = null;
                good good = null;
                partner_good partner_good = null;
                doc_import_match doc_import_match = null;

                List<manufacturer> mfrList = null;
                bool isMfrNew = false;

                List<apply_group> applyGroupList = null;
                bool isApplyGroupNew = false;

                int log_id = startDocImportLog(Enums.UndDocImportLogTypeEnum.NEW);

                int cnt = 0;
                AuMainDb transactionContext = null;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
                {
                    transactionContext = null;
                    try
                    {
                        transactionContext = new AuMainDb();
                        transactionContext.Configuration.AutoDetectChangesEnabled = false;

                        mfrList = transactionContext.manufacturer.Where(ss => !ss.is_deleted).ToList();
                        if (mfrList == null)
                            mfrList = new List<manufacturer>();

                        applyGroupList = transactionContext.apply_group.Where(ss => !ss.is_deleted).ToList();
                        if (applyGroupList == null)
                            applyGroupList = new List<apply_group>();

                        cnt = 0;
                        foreach (var itemAdd in itemsAdd.OrderBy(ss => ss.source_name).ToList())
                        {
                            ++cnt;

                            isApplyGroupNew = false;
                            apply_group = applyGroupList.Where(ss => ss.apply_group_name.Trim().ToLower().Equals(itemAdd.apply_group_name.Trim().ToLower())).FirstOrDefault();
                            if (apply_group == null)
                            {
                                apply_group = new apply_group();
                                // !!!
                                apply_group.apply_group_id = int.Parse(itemAdd.apply_group_name);
                                apply_group.apply_group_name = itemAdd.apply_group_name;
                                apply_group.crt_date = now;
                                apply_group.crt_user = user;
                                apply_group.upd_date = now;
                                apply_group.upd_user = user;
                                apply_group.is_deleted = false;
                                applyGroupList.Add(apply_group);
                                isApplyGroupNew = true;
                            }

                            product = new product();
                            product.doc_id = itemAdd.doc_id;
                            product.product_name = itemAdd.match_product_name;
                            product.apply_group_id = apply_group.apply_group_id;
                            product.crt_date = now;
                            product.crt_user = user;
                            product.upd_date = now;
                            product.upd_user = user;
                            product.is_deleted = false;
                            
                            isMfrNew = false;
                            mfr = mfrList.Where(ss => ss.mfr_name.Trim().ToLower().Equals(itemAdd.match_mfr_name.Trim().ToLower())).FirstOrDefault();
                            if (mfr == null)
                            {
                                mfr = new manufacturer();
                                mfr.mfr_name = itemAdd.match_mfr_name;
                                mfr.crt_date = now;
                                mfr.crt_user = user;
                                mfr.upd_date = now;
                                mfr.upd_user = user;
                                mfr.is_deleted = false;
                                mfrList.Add(mfr);
                                isMfrNew = true;
                            }

                            good = new good();
                            good.product = product;
                            good.manufacturer = mfr;                            
                            good.barcode = itemAdd.match_barcode;
                            good.doc_id = itemAdd.doc_id;
                            good.log_id = log_id;
                            good.tmp_row_id = itemAdd.row_id;
                            good.crt_date = now;
                            good.crt_user = user;
                            good.upd_date = now;
                            good.upd_user = user;
                            good.is_deleted = false;

                            partner_good = new partner_good();
                            partner_good.good = good;
                            partner_good.partner_id = itemAdd.partner_id.GetValueOrDefault(0);
                            partner_good.partner_code = itemAdd.code;
                            partner_good.partner_name = itemAdd.init_source_name;
                            partner_good.crt_date = now;
                            partner_good.crt_user = user;
                            partner_good.upd_date = now;
                            partner_good.upd_user = user;
                            partner_good.is_deleted = false;

                            /*
                            transactionContext = AddToContext_4<product, manufacturer, good, partner_good>
                                (transactionContext,
                                product,
                                (isMfrNew ? mfr : null),
                                good,
                                partner_good,
                                cnt, 100, false);
                            */
                            transactionContext = AddToContext_5<apply_group, product, manufacturer, good, partner_good>
                                (transactionContext,
                                (isApplyGroupNew ? apply_group : null),
                                product,
                                (isMfrNew ? mfr : null),
                                good,
                                partner_good,
                                cnt, 100, false);

                        }

                        transactionContext.SaveChanges();

                    }
                    finally
                    {
                        if (transactionContext != null)
                            transactionContext.Dispose();
                    }

                    scope.Complete();

                }

                int res = dbContext.Database.SqlQuery<int>("select * from und.approve_doc_import_match(@p0, @p1, @p2)", log_id, (int)Enums.UndMatchStateEnum.NEW, user).FirstOrDefault();

                endDocImportLog(log_id);

                return new GoodServiceResult() { Id = 0, Error = null };
            }
            catch (Exception ex)
            {
                return new GoodServiceResult(ex);
            }
        }

        #endregion

        #region InsertGoods_Syn

        public GoodServiceResult InsertGoods_Syn(IEnumerable<DocImportMatch> itemsAdd)
        {
            if ((itemsAdd == null) || (itemsAdd.Count() <= 0))
                return new GoodServiceResult() { Id = 0, Error = null };

            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            product product = null;
            manufacturer mfr = null;
            good good = null;
            partner_good partner_good = null;
            doc_import_match doc_import_match = null;

            List<manufacturer> mfrList = null;
            bool isMfrNew = false;

            int log_id = startDocImportLog(Enums.UndDocImportLogTypeEnum.SYN);

            int cnt = 0;
            AuMainDb transactionContext = null;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
            {
                transactionContext = null;
                try
                {
                    transactionContext = new AuMainDb();
                    transactionContext.Configuration.AutoDetectChangesEnabled = false;

                    mfrList = transactionContext.manufacturer.Where(ss => !ss.is_deleted).ToList();
                    if (mfrList == null)
                        mfrList = new List<manufacturer>();

                    cnt = 0;
                    foreach (var itemAdd in itemsAdd)
                    {
                        ++cnt;

                        product = transactionContext.product.Where(ss => ss.product_id == itemAdd.match_product_id).FirstOrDefault();
                        if (product == null)
                            continue;
                        
                        isMfrNew = false;
                        mfr = mfrList.Where(ss => ss.mfr_name.Trim().ToLower().Equals(itemAdd.match_mfr_name.Trim().ToLower())).FirstOrDefault();
                        if (mfr == null)
                        {
                            mfr = new manufacturer();
                            mfr.mfr_name = itemAdd.match_mfr_name;
                            mfr.crt_date = now;
                            mfr.crt_user = user;
                            mfr.upd_date = now;
                            mfr.upd_user = user;
                            mfr.is_deleted = false;
                            mfrList.Add(mfr);
                            isMfrNew = true;
                        }

                        good = new good();
                        good.product = product;
                        good.manufacturer = mfr;
                        good.barcode = itemAdd.match_barcode;
                        good.doc_id = itemAdd.doc_id;
                        good.log_id = log_id;
                        good.tmp_row_id = itemAdd.row_id;
                        good.crt_date = now;
                        good.crt_user = user;
                        good.upd_date = now;
                        good.upd_user = user;
                        good.is_deleted = false;

                        partner_good = new partner_good();
                        partner_good.good = good;
                        partner_good.partner_id = itemAdd.partner_id.GetValueOrDefault(0);
                        partner_good.partner_code = itemAdd.code;
                        partner_good.partner_name = itemAdd.source_name;
                        partner_good.crt_date = now;
                        partner_good.crt_user = user;
                        partner_good.upd_date = now;
                        partner_good.upd_user = user;
                        partner_good.is_deleted = false;

                        transactionContext = AddToContext_3<manufacturer, good, partner_good>
                            (transactionContext,                            
                            (isMfrNew ? mfr : null),
                            good,
                            partner_good,
                            cnt, 100, false);

                    }

                    transactionContext.SaveChanges();

                }
                finally
                {
                    if (transactionContext != null)
                        transactionContext.Dispose();
                }

                scope.Complete();
            }

            int res = dbContext.Database.SqlQuery<int>("select * from und.approve_doc_import_match(@p0, @p1, @p2)", log_id, (int)Enums.UndMatchStateEnum.SYN, user).FirstOrDefault();

            endDocImportLog(log_id);

            return new GoodServiceResult() { Id = 0, Error = null };
        }

        #endregion

        #region InsertGood_Etalon

        public Product InsertGood_Etalon(string product_name, int? apply_group_id, string good_name, int mfr_id, string barcode, int? source_partner_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            product product = new product();
            product.product_name = product_name;
            product.apply_group_id = apply_group_id;
            product.crt_date = now;
            product.crt_user = user;
            product.upd_date = now;
            product.upd_user = user;
            product.is_deleted = false;
            dbContext.product.Add(product);

            good good = new good();
            good.product = product;
            good.mfr_id = mfr_id;
            good.barcode = barcode;
            good.is_main = true;
            good.crt_date = now;
            good.crt_user = user;
            good.upd_date = now;
            good.upd_user = user;
            good.is_deleted = false;
            dbContext.good.Add(good);

            partner_good partner_good = new partner_good();
            partner_good.good = good;
            partner_good.partner_id = source_partner_id.GetValueOrDefault(0);
            partner_good.partner_name = good_name;
            partner_good.crt_date = now;
            partner_good.crt_user = user;
            partner_good.upd_date = now;
            partner_good.upd_user = user;
            partner_good.is_deleted = false;
            dbContext.partner_good.Add(partner_good);

            dbContext.SaveChanges();

            partner_good = dbContext.partner_good.Where(ss => ss.good_id == good.good_id && ss.partner_id == source_partner_id).FirstOrDefault();
            if (partner_good != null)
            {
                partner_good.partner_code = partner_good.good_id.ToString();
                dbContext.SaveChanges();
            }

            return dbContext.vw_product_and_good.Where(ss => ss.good_id == good.good_id).ProjectTo<Product>().FirstOrDefault();            
        }

        #endregion

        #region InsertGood_Syn

        public Product InsertGood_Syn(int main_good_id, string product_name, int? apply_group_id, string good_name, int mfr_id, string barcode, int? source_partner_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            good main_good = dbContext.good.Where(ss => ss.good_id == main_good_id && ss.is_main).FirstOrDefault();
            if (main_good == null)
                return null;

            good good = new good();
            good.product_id = main_good.product_id;
            good.mfr_id = mfr_id;
            good.barcode = barcode;
            good.is_main = false;
            good.crt_date = now;
            good.crt_user = user;
            good.upd_date = now;
            good.upd_user = user;
            good.is_deleted = false;
            dbContext.good.Add(good);

            partner_good partner_good = new partner_good();
            partner_good.good = good;
            partner_good.partner_id = source_partner_id.GetValueOrDefault(0);
            partner_good.partner_name = good_name;
            partner_good.crt_date = now;
            partner_good.crt_user = user;
            partner_good.upd_date = now;
            partner_good.upd_user = user;
            partner_good.is_deleted = false;
            dbContext.partner_good.Add(partner_good);

            dbContext.SaveChanges();

            partner_good = dbContext.partner_good.Where(ss => ss.good_id == good.good_id && ss.partner_id == source_partner_id).FirstOrDefault();
            if (partner_good != null)
            {
                partner_good.partner_code = partner_good.good_id.ToString();
                dbContext.SaveChanges();
            }

            return dbContext.vw_product_and_good.Where(ss => ss.good_id == good.good_id).ProjectTo<Product>().FirstOrDefault();
        }

        #endregion

        #region UpdateGood_Etalon

        public Product UpdateGood_Etalon(int good_id, string product_name, int? apply_group_id, string good_name, int mfr_id, string barcode, int? source_partner_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            good good = dbContext.good.Where(ss => ss.good_id == good_id).FirstOrDefault();
            if (good == null)
                return null;

            product product = dbContext.product.Where(ss => ss.product_id == good.product_id).FirstOrDefault();
            if (product == null)
                return null;

            partner_good partner_good = dbContext.partner_good.Where(ss => ss.good_id == good.good_id).FirstOrDefault();
            if (partner_good == null)
                return null;

            product.product_name = product_name;
            product.apply_group_id = apply_group_id;
            product.upd_date = now;
            product.upd_user = user;
            
            good.mfr_id = mfr_id;
            good.barcode = barcode;
            good.upd_date = now;
            good.upd_user = user;

            partner_good.partner_id = source_partner_id.GetValueOrDefault(0);
            partner_good.partner_name = good_name;
            partner_good.upd_date = now;
            partner_good.upd_user = user;

            dbContext.SaveChanges();

            return dbContext.vw_product_and_good.Where(ss => ss.good_id == good.good_id).ProjectTo<Product>().FirstOrDefault();
        }

        #endregion

        #region UpdateGood_Syn

        public Product UpdateGood_Syn(int good_id, string product_name, int? apply_group_id, string good_name, int mfr_id, string barcode, int? source_partner_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            good good = dbContext.good.Where(ss => ss.good_id == good_id).FirstOrDefault();
            if (good == null)
                return null;

            product product = dbContext.product.Where(ss => ss.product_id == good.product_id).FirstOrDefault();
            if (product == null)
                return null;

            partner_good partner_good = dbContext.partner_good.Where(ss => ss.good_id == good.good_id).FirstOrDefault();
            if (partner_good == null)
                return null;

            product.product_name = product_name;
            product.apply_group_id = apply_group_id;
            product.upd_date = now;
            product.upd_user = user;

            good.mfr_id = mfr_id;
            good.barcode = barcode;
            good.upd_date = now;
            good.upd_user = user;

            partner_good.partner_id = source_partner_id.GetValueOrDefault(0);
            partner_good.partner_name = good_name;
            partner_good.upd_date = now;
            partner_good.upd_user = user;

            dbContext.SaveChanges();

            return dbContext.vw_product_and_good.Where(ss => ss.good_id == good.good_id).ProjectTo<Product>().FirstOrDefault();
        }

        #endregion

        #region DeleteGood_Etalon

        public bool DeleteGood_Etalon(int good_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            good good = dbContext.good.Where(ss => ss.good_id == good_id).FirstOrDefault();
            if (good == null)
                return false;

            //dbContext.partner_good.Remove(partner_good);
            dbContext.partner_good.RemoveRange(from t1 in dbContext.partner_good
                                               from t2 in dbContext.good
                                               where t1.good_id == t2.good_id
                                               && t2.product_id == good.product_id
                                               select t1);
            dbContext.good.RemoveRange(dbContext.good.Where(ss => ss.product_id == good.product_id));

            dbContext.SaveChanges();

            return true;
        }

        #endregion

        #region DeleteGood_Syn

        public bool DeleteGood_Syn(int good_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            good good = dbContext.good.Where(ss => ss.good_id == good_id).FirstOrDefault();
            if (good == null)
                return false;

            partner_good partner_good = dbContext.partner_good.Where(ss => ss.good_id == good.good_id).FirstOrDefault();
            if (partner_good == null)
                return false;

            dbContext.partner_good.Remove(partner_good);
            dbContext.good.Remove(good);

            dbContext.SaveChanges();

            return true;
        }

        #endregion

        #region MoveGood_FromEtalonToSyn

        public bool MoveGood_FromEtalonToSyn(int target_good_id, int source_good_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            good target_good = dbContext.good.Where(ss => ss.good_id == target_good_id && ss.is_main).FirstOrDefault();
            if (target_good == null)
                return false;

            good source_good = dbContext.good.Where(ss => ss.good_id == source_good_id && ss.is_main).FirstOrDefault();
            if (source_good == null)
                return false;

            List<good> source_good_with_synonyms = dbContext.good.Where(ss => ss.product_id == source_good.product_id).ToList();
            if ((source_good_with_synonyms == null) || (source_good_with_synonyms.Count <= 0))
                return false;

            foreach (var good in source_good_with_synonyms)
            {
                good.product_id = target_good.product_id;
                good.is_main = false;
                good.upd_date = now;
                good.upd_user = user;
            }

            dbContext.SaveChanges();

            return true;
        }

        #endregion

        #region DeleteMatches

        public GoodServiceResult DeleteMatches(IEnumerable<DocImportMatch> itemsAdd)
        {
            if ((itemsAdd == null) || (itemsAdd.Count() <= 0))
                return new GoodServiceResult() { Id = 0, Error = null };

            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            int log_id = startDocImportLog(Enums.UndDocImportLogTypeEnum.REMOVE);

            foreach (var itemAdd in itemsAdd)
            {
                doc_import_match doc_import_match = dbContext.doc_import_match.Where(ss => ss.row_id == itemAdd.row_id).FirstOrDefault();
                if (doc_import_match == null)
                    continue;

                // удалем partner_good, good, product
                if (doc_import_match.state == (int)Enums.UndMatchStateEnum.NEW)
                {
                    good good = dbContext.good.Where(ss => ss.good_id == doc_import_match.match_good_id).FirstOrDefault();
                    if (good != null)
                    {
                        dbContext.partner_good.RemoveRange(dbContext.partner_good.Where(ss => ss.good_id == good.good_id));
                        dbContext.good.Remove(good);
                    }
                    product product = dbContext.product.Where(ss => ss.product_id == doc_import_match.match_product_id).FirstOrDefault();
                    if (product != null)
                    {
                        dbContext.product.Remove(product);
                    }
                }
                // удалем partner_good, good
                else if (doc_import_match.state == (int)Enums.UndMatchStateEnum.SYN)
                {
                    good good = dbContext.good.Where(ss => ss.good_id == doc_import_match.match_good_id).FirstOrDefault();
                    if (good != null)
                    {
                        dbContext.partner_good.RemoveRange(dbContext.partner_good.Where(ss => ss.good_id == good.good_id));
                        dbContext.good.Remove(good);
                    }
                }

                doc_import_match.state = (int)Enums.UndMatchStateEnum.NONE;
                doc_import_match.match_product_id = null;
                doc_import_match.match_good_id = null;
                doc_import_match.is_approved = false;
                doc_import_match.percent = null;
                doc_import_match.is_auto_match = false;
                doc_import_match.upd_date = now;
                doc_import_match.upd_user = user;
            }

            dbContext.SaveChanges();

            endDocImportLog(log_id);

            return new GoodServiceResult() { Id = 0, Error = null };
        }

        #endregion

        #region DocImportLog

        private int startDocImportLog(Enums.UndDocImportLogTypeEnum logType)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            doc_import_log doc_import_log = new doc_import_log();
            doc_import_log.log_type_id = (int)logType;
            doc_import_log.date_beg = now;
            doc_import_log.is_active = true;
            doc_import_log.crt_date = now;
            doc_import_log.crt_user = user;
            doc_import_log.upd_date = now;
            doc_import_log.upd_user = user;
            doc_import_log.is_deleted = false;
            dbContext.doc_import_log.Add(doc_import_log);
            dbContext.SaveChanges();
            return doc_import_log.log_id;
        }

        private void endDocImportLog(int log_id)
        {
            DateTime now = DateTime.Now;
            string user = appManager.CurrentUser.Name;

            doc_import_log doc_import_log = dbContext.doc_import_log.Where(ss => ss.log_id == log_id).FirstOrDefault();
            if (doc_import_log == null)
                return;
            if (!doc_import_log.is_active)
                return;
            doc_import_log.date_end = now;
            doc_import_log.is_active = false;
            doc_import_log.upd_date = now;
            doc_import_log.upd_user = user;
            dbContext.SaveChanges();
        }

        #endregion
    }
}