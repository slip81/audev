﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;
#endregion

namespace EsnAdmin.Service
{

    public class DocImportMatchServiceResult : ServiceResult
    {
        public DocImportMatchServiceResult()
        {
            //
        }

        public DocImportMatchServiceResult(Exception ex)
            :base(ex)
        {
            //
        }

        public List<DocImportMatch> DocImportMatches { get; set; }
        public List<DocImportMatchVariant> DocImportMatchVariants { get; set; }
    }

    public class DocService : UndBaseService
    {
        public DocService(AppManager appManager)
            : base(appManager)
        {
            //
        }

        public List<Doc> GetDocs(int? doc_type_id)
        {
            return dbParallelContext.vw_doc
                .AsNoTracking()
                .Where(ss => (doc_type_id.HasValue && ss.doc_type_id == doc_type_id) || (!doc_type_id.HasValue))
                .ProjectTo<Doc>(ModelMapper.ConfigurationProvider)
                .ToList();
        }

        public Doc GetDoc(int doc_id)
        {
            return dbContext.vw_doc
                .Where(ss => ss.doc_id == doc_id)
                .ProjectTo<Doc>(ModelMapper.ConfigurationProvider)
                .FirstOrDefault();
        }

        public DocImportMatchServiceResult GetDocImportMatches(int doc_id)
        {
            try
            {
                var list1 = dbContext.vw_doc_import_match
                    .Where(ss => ss.doc_id == doc_id)
                    .ProjectTo<DocImportMatch>(ModelMapper.ConfigurationProvider)
                    .ToList();

                var list2 = dbContext.vw_doc_import_match_variant
                    .Where(ss => ss.doc_id == doc_id)
                    .ProjectTo<DocImportMatchVariant>(ModelMapper.ConfigurationProvider)
                    .ToList();

                return new DocImportMatchServiceResult() { DocImportMatches = new List<DocImportMatch>(list1), DocImportMatchVariants = new List<DocImportMatchVariant>(list2), Error = null };
            }
            catch (Exception ex)
            {
                return new DocImportMatchServiceResult(ex);
            }
        }

        public ServiceResult DocAutoMatchFull(int? doc_id
            , bool is_match_by_barcode
            , bool is_match_by_name
            , bool is_match_by_mfr
            , bool is_match_approved
            )
        {
            try
            {
                var res = dbContext.Database.SqlQuery<int?>("select * from und.auto_match_full(" 
                    + (doc_id.HasValue ? doc_id.ToString() : "null") 
                    + ", "
                    + (is_match_by_barcode ? "1" : "0")
                    + ", "
                    + (is_match_by_name ? "1" : "0")
                    + ", "
                    + (is_match_by_mfr ? "1" : "0")
                    + ", "
                    //+ (is_match_approved ? "1" : "0")
                    + "0"
                    + ", "
                    + "'" + appManager.CurrentUser.Name + "'"
                    + ");"
                    )
                    .FirstOrDefault();
                return new ServiceResult();
            }
            catch (Exception ex)
            {
                return new ServiceResult(ex);
            }
        }

        public ServiceResult DocAutoMatchPart(int? doc_id
            , bool is_match_by_barcode
            , bool is_match_by_name
            , bool is_match_by_mfr
            , bool is_match_approved
            , bool is_match_sum_percent
            , decimal? min_percent
            , decimal? min_barcode_percent
            , decimal? min_name_percent
            , decimal? min_mfr_percent            
            )
        {
            
            // описание работы с pg_trgm
            // https://stackoverflow.com/questions/11249635/finding-similar-strings-with-postgresql-quickly

            try
            {
                string spText = "select * from und.auto_match_part("
                    + (doc_id.HasValue ? doc_id.ToString() : "null")
                    + ", "
                    + (is_match_by_barcode ? "1" : "0")
                    + ", "
                    + (is_match_by_name ? "1" : "0")
                    + ", "
                    + (is_match_by_mfr ? "1" : "0")
                    + ", "
                    //+ (is_match_approved ? "1" : "0")
                    + "0"
                    + ", "
                    + (is_match_sum_percent ? "1" : "0")
                    + ", "
                    + min_percent.GetValueOrDefault(0).ToString().Replace(',', '.')
                    + ", "
                    + min_barcode_percent.GetValueOrDefault(0).ToString().Replace(',', '.')
                    + ", "
                    + min_name_percent.GetValueOrDefault(0).ToString().Replace(',', '.')
                    + ", "
                    + min_mfr_percent.GetValueOrDefault(0).ToString().Replace(',', '.')
                    + ", "
                    + "'" + appManager.CurrentUser.Name + "'"
                    + ");";
                var res = dbContext.Database.SqlQuery<int?>(spText)
                    .FirstOrDefault();
                return new ServiceResult();
            }
            catch (Exception ex)
            {
                return new ServiceResult(ex);
            }
        }
    }
}