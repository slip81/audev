﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Transactions;
using System.Data.Entity;
using System.ComponentModel;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml;
using Telerik.Windows.Documents.Spreadsheet.FormatProviders.OpenXml.Xlsx;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using EsnAdmin.Core;
using EsnAdmin.ViewModel;

namespace EsnAdmin.Service
{ 
    public class AdminService: UndBaseService
    {
        public AdminService(AppManager appManager)
            :base(appManager)
        {
            //
        }

        public ServiceResult DelData()
        {
            try
            {
                dbContext.Database.SqlQuery<string>("select und.del_data(true, true, true)").FirstOrDefault();
                return new ServiceResult() { Id = 0, Error = null, };
            }
            catch (Exception ex)
            {
                return new ServiceResult(ex);
            }
        }
    }
}

