﻿#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using EsnAdmin.ViewModel;
#endregion

namespace EsnAdmin
{
    public partial class ApplyGroupForm : RadFormExt
    {
        private bool lockEvents = false;
        private List<ApplyGroup> applyGroupDs = null;

        private ApplyGroupEditForm applyGroupEditForm = null;

        public ApplyGroupForm()
        {
            InitializeComponent();
            //
            InitApplyGroupGrid();
        }


        private void InitApplyGroupGrid()
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                GridApplyGroup_bind();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        private void GridApplyGroup_bind()
        {
            var groups = ApplyGroupService.GetApplyGroups();
            if (groups.Error != null)
            {
                MessageBox.Show(groups.Error.Mess);
                return;
            }

            applyGroupDs = new List<ApplyGroup>(groups.apply_groups);

            gridApplyGroup.DataSource = applyGroupDs;
            gridApplyGroup.TableElement.ScrollToRow(0);
            gridApplyGroup.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Bottom;
        }

        private void btnApplyGroupAdd_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Добавить новую группу по примененению";
        }

        private void btnApplyGroupEdit_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Изменить выбранную группу по примененению";
        }

        private void btnApplyGroupDel_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Удалить выбранную группу по примененению";
        }

        private void btnApplyGroupAdd_Click(object sender, EventArgs e)
        {
            if (applyGroupEditForm == null)
                applyGroupEditForm = new ApplyGroupEditForm();
            applyGroupEditForm.InitForm_Add();
            if (applyGroupEditForm.ShowDialog() == DialogResult.OK)
            {
                var newApplyGroup = ApplyGroupService.InsertApplyGroup(applyGroupEditForm.new_apply_group_name);
                if (newApplyGroup == null)
                {
                    MessageBox.Show("Ошибка: группа по применению не добавлена");
                    return;
                }
                applyGroupDs.Add(newApplyGroup);
            }
        }

        private void btnApplyGroupEdit_Click(object sender, EventArgs e)
        {
            ApplyGroup currentApplyGroup = gridApplyGroup.SelectedRows != null && gridApplyGroup.SelectedRows.Count > 0 ? (ApplyGroup)gridApplyGroup.SelectedRows[0].DataBoundItem : null;
            if (currentApplyGroup == null)
            {
                MessageBox.Show("Не выбрана группа по применению");
                return;
            }

            if (applyGroupEditForm == null)
                applyGroupEditForm = new ApplyGroupEditForm();
            applyGroupEditForm.InitForm_Edit(currentApplyGroup.apply_group_id, currentApplyGroup.apply_group_name);
            if (applyGroupEditForm.ShowDialog() == DialogResult.OK)
            {
                var newApplyGroup = ApplyGroupService.UpdateApplyGroup(currentApplyGroup.apply_group_id, applyGroupEditForm.new_apply_group_name);
                if (newApplyGroup == null)
                {
                    MessageBox.Show("Ошибка: группа по применению не изменена");
                    return;
                }

                var newApplyGroup_Ds = applyGroupDs.Where(ss => ss.apply_group_id == newApplyGroup.apply_group_id).FirstOrDefault();
                if (newApplyGroup_Ds != null)
                {
                    newApplyGroup_Ds.apply_group_name = newApplyGroup.apply_group_name;
                }
            }
        }

        private void btnApplyGroupDel_Click(object sender, EventArgs e)
        {
            ApplyGroup currentApplyGroup = gridApplyGroup.SelectedRows != null && gridApplyGroup.SelectedRows.Count > 0 ? (ApplyGroup)gridApplyGroup.SelectedRows[0].DataBoundItem : null;
            if (currentApplyGroup == null)
            {
                MessageBox.Show("Не выбрана группа по применению");
                return;
            }

            if (RadMessageBox.Show(this, "Удалить группу по применению ?", "Подтверждение", MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                return;

            var del = ApplyGroupService.DeleteApplyGroup(currentApplyGroup.apply_group_id);
            if (!del)
            {
                MessageBox.Show("Ошибка: группа по применению не удалена");
                return;
            }
            var delApplyGroupDs = applyGroupDs.Where(ss => ss.apply_group_id == currentApplyGroup.apply_group_id).FirstOrDefault();
            if (delApplyGroupDs != null)
            {
                applyGroupDs.Remove(delApplyGroupDs);
            }
        }
    }
}
