﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsnAdmin
{
    public partial class StartForm : RadFormExt
    {
        public StartForm()
        {
            InitializeComponent();
            //
            //lblVersion.Text = Util.VerName;            
            //
            Timer t = new Timer();
            t.Interval = 3000;
            t.Start();
            t.Tick += new EventHandler(t_Tick);
            timer1.Start();

            Opacity = 0;
            Timer timer = new Timer();
            timer.Tick += new EventHandler((sender, e) =>
            {
                if ((Opacity += 0.03d) == 1) timer.Stop();
            });

            timer.Interval = 1;
            timer.Start();
        }

        public void SetState(string state_name)
        {
            lblProgrammName.Text = state_name;
        }

        private void t_Tick(object sender, EventArgs e)
        {
            //Close();
        }
    }
}
