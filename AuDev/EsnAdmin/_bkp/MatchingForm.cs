﻿#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using AuDev.Common.Util;
using EsnAdmin.ViewModel;
using EsnAdmin.Service;
using System.Globalization;
#endregion

namespace EsnAdmin
{
    public partial class MatchingForm : RadFormExt
    {
        private bool lockEvents = false;
        private bool lockEvents_match = false;
        private bool lockEvents_check = false;
        private int importDocTick = 0;
        private List<Product> productDs = null;
        private List<Good> goodDs = null;
        private List<DocImportMatch> docImportMatchDs = null;
        private List<DocImportMatchVariant> docImportMatchVariantDs = null;
        private DocImportSupplierResult docImportSupplierResult = null;

        private List<Product> productDs_filtered = null;
        private List<DocImportMatch> docImportMatchDs_filtered = null;
        private List<DocImportMatchVariant> docImportMatchVariantDs_filtered = null;

        private bool docSelected = false;
        private Doc selectedDoc = null;
        private DocSelectForm docSelectForm = null;        

        private AutoMatchParamForm autoMatchParamForm = null;

        private System.Timers.Timer timerImportDoc = null;
        private int currDocImportId = 0;

        const int MAX_CHECKED_ROWS_FOR_SAVE = 1000;

        public MatchingForm()
        {
            InitializeComponent();
            //
            timerImportDoc = new System.Timers.Timer();
            timerImportDoc.Elapsed += timerImportDocElapsed;
            timerImportDoc.Interval = 2000; // every second
            timerImportDoc.AutoReset = false; // makes it fire only once 
            timerImportDoc.Enabled = false;
        }

        private void MatchingForm_Load(object sender, EventArgs e)
        {
            InitForm();
        }

        private void InitForm()
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                InitEtalonParams();
                InitMatchParams();
                InitImportParams();
                GridEtalon_bind();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void InitEtalonParams()
        {
            splitPanel6.Collapsed = !chbShowVariant.Checked;
        }

        private void InitMatchParams()
        {
            pageMatch.SelectedPage = tabMatchDocSelect;
            tabMatchDoc.Item.Visibility = ElementVisibility.Hidden;
        }

        private void InitImportParams()
        {
            InitImportFileEditor();
            InitImportConfigDdl();
        }

        private void InitImportFileEditor()
        {
            OpenFileDialog dlg = (OpenFileDialog)edImportFile.Dialog;
            dlg.InitialDirectory = "c:\\";
            dlg.Filter = "Excel файлы (*.xlsx)|*.xlsx|XML файлы (*.xml)|*.xml";
        }

        private void InitImportConfigDdl()
        {
            ddlImportConfig.DataSource = null;
            ddlImportConfig.DataSource = DocImportService.GetDocImportConfigTypes();
            ddlImportConfig.DisplayMember = "config_type_name";
            ddlImportConfig.ValueMember = "config_type_id";
            ddlImportConfig.SelectedIndex = 0;
        }

        private void GridEtalon_bind()
        {
            var products = ProductService.GetProducts();
            if (products.Error != null)
            {
                MessageBox.Show(products.Error.Mess);
                return;
            }

            //productDs = new List<Product>(products.products);
            productDs = new List<Product>(products.products_all);
            goodDs = new List<Good>(products.goods);

            gridEtalon.DataSource = productDs;
            gridEtalon.TableElement.ScrollToRow(0);
            gridEtalon.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Bottom;
            gridEtalon.ClearSelection();
            if (gridEtalon.Rows.Count > 0)
            {
                gridEtalon.Rows[0].IsSelected = true;
                gridEtalon.Rows[0].IsCurrent = true;
            }
        }

        private void gridEtalon_SelectionChanged(object sender, EventArgs e)
        {
            if (lockEvents)
                return;
            lockEvents = true;
            try
            {
                var currentProductId = gridEtalon.SelectedRows != null && gridEtalon.SelectedRows.Count > 0 ? gridEtalon.SelectedRows[0].Cells[0].Value : null;
                if (currentProductId != null)
                {
                    GridEtalonGood_bind((int)currentProductId);
                }
            }
            finally
            {
                lockEvents = false;
            }
        }

        private void GridEtalonGood_bind(int currentProductId)
        {
            List<Good> goods = goodDs.Where(ss => ss.product_id == currentProductId).ToList();
            gridEtalonGood.DataSource = goods;
            gridEtalonGood.ClearSelection();
            if (gridEtalonGood.Rows.Count > 0)
            {
                gridEtalonGood.Rows[0].IsSelected = true;
                gridEtalonGood.Rows[0].IsCurrent = true;
            }
        }

        private void btnSelectDoc_Click(object sender, EventArgs e)
        {
            if (docSelectForm == null)
                docSelectForm = new DocSelectForm();
            if (docSelectForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {                
                selectedDoc = docSelectForm.SelectedDoc;
                docSelected = true;
                updateSelectedDocText();                
                Cursor = Cursors.WaitCursor;
                try
                {
                    GridMatch_bind();
                    StartMatch();
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }
            
        }

        private void btnImportDoc_Click(object sender, EventArgs e)
        {            
            grbImportDoc.Visible = true;
        }

        private void btnImportStart_Click(object sender, EventArgs e)
        {
            prgImportDoc.Visible = true;
            timerImportDoc.Enabled = true;

            currDocImportId = DocImportService.CreateDocImport();
            if (currDocImportId <= 0)
            {
                MessageBox.Show("Не удалось начать импорт, попробуйте немного позже");
                return;
            }

            docImportSupplierResult = null;
            Task task = new Task(() =>
            {
                docImportSupplierResult = DocImportService.ImportSupplierForMatch(currDocImportId, edImportFile.Value, (int)ddlImportConfig.SelectedValue);
            }
            );
            task.Start();

            // !!!
            // System.Threading.Thread.Sleep(2000);
            timerImportDoc.Enabled = true;

            // !!!
            /*
            Cursor = Cursors.WaitCursor;
            try
            {
                var importResult = DocImportService.ImportSupplierForMatch(edImportFile.Value, (int)ddlImportConfig.SelectedValue);
                if (importResult.Error != null)
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show(importResult.Error.Mess);
                }
                else
                {
                    selectedDoc = DocService.GetDoc(importResult.doc_id.GetValueOrDefault(0));
                    if (selectedDoc == null)
                    {
                        Cursor = Cursors.Default;
                        MessageBox.Show("Не найден документ с кодом " + importResult.doc_id.GetValueOrDefault(0).ToString());
                        return;
                    }
                    docSelected = true;
                    updateSelectedDocText();

                    GridMatch_bind();
                    StartMatch();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }            
            */
        }

        private void GridMatch_bind()
        {
            lockEvents_match = true;
            try
            {
                var docImportMatchServiceResult = DocService.GetDocImportMatches(selectedDoc.doc_id);
                if (docImportMatchServiceResult.Error != null)
                {
                    MessageBox.Show(docImportMatchServiceResult.Error.Mess);
                    return;
                }
                docImportMatchDs = docImportMatchServiceResult.DocImportMatches;
                docImportMatchVariantDs = docImportMatchServiceResult.DocImportMatchVariants;

                gridMatch.DataSource = docImportMatchDs;
                gridMatch.TableElement.ScrollToRow(0);
                gridMatch.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Bottom;

                gridMatch.ClearSelection();
            }
            finally
            {
                lockEvents_match = false;
            }

            //gridMatch.CurrentRowChanged += gridMatch_CurrentRowChanged;

            if (gridMatch.Rows.Count > 0)
            {
                gridMatch.Rows[0].IsSelected = true;
                gridMatch.Rows[0].IsCurrent = true;
            }
        }

        private void StartMatch()
        {
            tabMatchDoc.Item.Visibility = ElementVisibility.Visible;
            pageMatch.SelectedPage = tabMatchDoc;
            /*
            btnSaveAll.Visible = true;
            btnCancelAll.Visible = true;
            grbImportDoc.Visible = false;
            btnSelectDoc.Visible = false;
            btnImportDoc.Visible = false;
            chbUnapprovedOnly.Visible = true;
            chbUnmatchOnly.Visible = true;
            */
        }

        private void EndMatch(bool save)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                if (save)
                {
                    if (docImportMatchDs_filtered == null)
                        docImportMatchDs_filtered = new List<DocImportMatch>(docImportMatchDs);
                    
                    /*
                    foreach (var newGood in docImportMatchDs_filtered.Where(ss => (ss.is_changed || ss.is_auto_match) && ss.is_confirmed && ss.state == (int)Enums.UndMatchStateEnum.NEW))
                    {
                        GoodService.InsertGood_Etalon(newGood);
                    }
                    */
                    
                    // !!!
                    GoodService.InsertGoods_Etalon(docImportMatchDs_filtered.Where(ss => (ss.is_changed || ss.is_auto_match) && ss.is_confirmed && ss.state == (int)Enums.UndMatchStateEnum.NEW).Take(MAX_CHECKED_ROWS_FOR_SAVE));

                    /*
                    foreach (var newGood in docImportMatchDs_filtered.Where(ss => (ss.is_changed || ss.is_auto_match) && ss.is_confirmed && ss.state == (int)Enums.UndMatchStateEnum.SYN))
                    {
                        GoodService.InsertGood_Syn(newGood);
                    }
                    */

                    // !!!
                    GoodService.InsertGoods_Syn(docImportMatchDs_filtered.Where(ss => (ss.is_changed || ss.is_auto_match) && ss.is_confirmed && ss.state == (int)Enums.UndMatchStateEnum.SYN).Take(MAX_CHECKED_ROWS_FOR_SAVE));

                    /*
                    foreach (var delGood in docImportMatchDs_filtered.Where(ss => (ss.is_changed || ss.is_auto_match) && ss.is_confirmed && ss.state == (int)Enums.UndMatchStateEnum.NONE))
                    {
                        GoodService.DeleteMatch(delGood);
                    }
                    */

                    // !!!
                    GoodService.DeleteMatches(docImportMatchDs_filtered.Where(ss => (ss.is_changed || ss.is_auto_match) && ss.is_confirmed && ss.state == (int)Enums.UndMatchStateEnum.NONE).Take(MAX_CHECKED_ROWS_FOR_SAVE));
                }
                //
                if (save)
                {
                    GridEtalon_bind();
                    updateSelectedDocText(true);
                }
                //
                GridMatch_bind();
                GridMatch_filter();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnSaveAll_Click(object sender, EventArgs e)
        {            
            int checkedRowCnt = gridMatch.Rows.Where(ss => ((DocImportMatch)ss.DataBoundItem).is_confirmed).Count();
            if (checkedRowCnt <= 0)
            {
                MessageBox.Show("Нет отмеченных строк");
                return;
            }

            string winTopic = "Сохранение изменений";
            string winMess = "Отмечено строк: " + checkedRowCnt.ToString();
            winMess += System.Environment.NewLine;
            winMess += "Сохранить изменения ?";
            if (checkedRowCnt > MAX_CHECKED_ROWS_FOR_SAVE)
            {
                winMess += System.Environment.NewLine;
                winMess += "ВНИМАНИЕ ! Будут сохранены изменения только по первым " + MAX_CHECKED_ROWS_FOR_SAVE.ToString() + " строкам";
            }

            if (RadMessageBox.Show(this, winMess, winTopic, MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                return;

            EndMatch(true);
        }

        private void btnCancelAll_Click(object sender, EventArgs e)
        {
            string winTopic = "Отмена изменений";
            string winMess = "Отменить изменения ?";
            if (RadMessageBox.Show(this, winMess, winTopic, MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                return;

            EndMatch(false);
        }

        private void btnMatchClear_Click(object sender, EventArgs e)
        {
            if ((gridMatch.SelectedRows == null) || (gridMatch.SelectedRows.Count <= 0))
            {
                MessageBox.Show("Не выбраны строки для удаления сопоставлений");
                return;
            }

            string winTopic = "Удаление сопоставлений";
            string winMess = "Выделено строк: " + gridMatch.SelectedRows.Count.ToString();
            winMess += System.Environment.NewLine;
            winMess += "Удалить спопставления ?";
            if (RadMessageBox.Show(this, winMess, winTopic, MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                return;

            foreach (var row in gridMatch.SelectedRows)
            {
                DocImportMatch rowItem = (DocImportMatch)row.DataBoundItem;
                if (rowItem == null)
                    continue;
                if (rowItem.state == (int)Enums.UndMatchStateEnum.NONE)
                    continue;
                rowItem.state = (int)Enums.UndMatchStateEnum.NONE;
                rowItem.state_name = "Не привязано";
                rowItem.match_good_id = null;
                rowItem.match_product_name = "";
                rowItem.match_product_id = null;
                rowItem.match_mfr_name = null;
                rowItem.match_barcode = null;
                rowItem.is_changed = true;
                rowItem.is_confirmed = true;
                row.InvalidateRow();
            }
            //GridMatch_filter();
        }

        private void gridEtalon_FilterExpressionChanged(object sender, FilterExpressionChangedEventArgs e)
        {
            string currentFilter = e.FilterExpression;
            string origFilter = currentFilter;
            try
            {
                string col_name = "column2";
                string operator_name = "LIKE";
                string first_replace_symbol = "'%";
                string last_replace_symbol = "%'";
                string border_symbol = "%";
                string filterValue = "";

                if (String.IsNullOrWhiteSpace(currentFilter))
                    return;

                currentFilter = currentFilter.Trim();

                bool forReplace = currentFilter.Contains(col_name) && currentFilter.Contains(operator_name);
                if (!forReplace)
                    return;

                string currentFilter_forReplace = currentFilter.Substring(currentFilter.IndexOf(col_name), currentFilter.LastIndexOf(last_replace_symbol) - currentFilter.IndexOf(col_name) + 1);
                if (String.IsNullOrWhiteSpace(currentFilter_forReplace))
                    return;

                filterValue = currentFilter_forReplace.Substring(currentFilter_forReplace.IndexOf(border_symbol), currentFilter_forReplace.LastIndexOf(border_symbol) - currentFilter_forReplace.IndexOf(border_symbol) + 1);
                List<string> filterValue_split = filterValue.Split(' ').Select(ss => ss.Replace("%", String.Empty)).Where(ss => !String.IsNullOrWhiteSpace(ss)).ToList();
                if ((filterValue_split == null) || (filterValue_split.Count <= 1))
                    return;

                currentFilter = "";
                bool first = true;
                foreach (var filterValue_splitItem in filterValue_split)
                {
                    if (!first)
                        currentFilter += " AND ";
                    currentFilter += "(" + col_name + " " + operator_name + " " + first_replace_symbol + filterValue_splitItem + last_replace_symbol + ")";
                    first = false;
                }
                e.FilterExpression = currentFilter;
            }
            catch
            {
                e.FilterExpression = origFilter;
            }
        }

        private void btnEtalonFilterApply_Click(object sender, EventArgs e)
        {
            //radColorDialog1.ShowDialog(); return;
            
            Cursor = Cursors.WaitCursor;
            try
            {
                GridEtalon_filter();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
             
        }

        private void GridEtalon_filter()
        {
            productDs_filtered = new List<Product>(productDs);
                
            if (!String.IsNullOrWhiteSpace(txtEtalonNameFilter.Text))
            {
                List<string> filterValue_split = txtEtalonNameFilter.Text.Split(' ').Select(ss => ss.Trim().ToLower()).Where(ss => !String.IsNullOrWhiteSpace(ss)).ToList();
                if ((filterValue_split == null) || (filterValue_split.Count <= 0))
                    return;
                foreach (var filterValue in filterValue_split)
                {
                    productDs_filtered = productDs_filtered
                        .Where(ss => ss.product_name.Trim().ToLower().Contains(filterValue))
                        .ToList();
                }

                /*
                productDs_filtered = productDs_filtered
                    .Where(ss => ss.product_name.Trim().ToLower().Contains(txtEtalonNameFilter.Text.Trim().ToLower()))                    
                    .ToList();
                */
            }

            if (!String.IsNullOrWhiteSpace(txtEtalonMfrFilter.Text))
            {
                productDs_filtered = productDs_filtered
                    .Where(ss => ss.mfr_name.Trim().ToLower().Contains(txtEtalonMfrFilter.Text.Trim().ToLower()))                    
                    .ToList();
            }

            if (!String.IsNullOrWhiteSpace(txtEtalonBarcodeFilter.Text))
            {
                productDs_filtered = productDs_filtered
                    .Where(ss => ss.barcode.Trim().ToLower().Contains(txtEtalonBarcodeFilter.Text.Trim().ToLower()))                    
                    .ToList();
            }

            gridEtalon.DataSource = productDs_filtered;
            gridEtalon.TableElement.ScrollToRow(0);
        }

        private void btnEtalonFilterReset_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                txtEtalonNameFilter.Text = "";
                txtEtalonMfrFilter.Text = "";
                txtEtalonBarcodeFilter.Text = "";
                //chbEtalonMatchVariant.Checked = false;
                gridEtalon.DataSource = productDs;
                gridEtalon.TableElement.ScrollToRow(0);
                if (productDs_filtered != null)
                    productDs_filtered.Clear();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void chbUnmatchOnly_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {            
            Cursor = Cursors.WaitCursor;
            try
            {
                GridMatch_filter();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void GridMatch_filter()
        {
            docImportMatchDs_filtered = new List<DocImportMatch>(docImportMatchDs);

            if (chbUnapprovedOnly.Checked)
            {
                docImportMatchDs_filtered = docImportMatchDs_filtered
                    .Where(ss => (ss.is_changed || ss.is_auto_match) && !ss.is_approved)
                    .ToList();
            }

            List<int> stateList = new List<int>();
            if (chbUnmatchOnly.Checked)
            {
                stateList.Add((int)Enums.UndMatchStateEnum.NONE);
            }
            if (chbMatchNew.Checked)
            {
                stateList.Add((int)Enums.UndMatchStateEnum.NEW);
            }
            if (chbMatchSyn.Checked)
            {
                stateList.Add((int)Enums.UndMatchStateEnum.SYN);
            }

            if (stateList.Count > 0)
            {
                docImportMatchDs_filtered = docImportMatchDs_filtered
                    .Where(ss => stateList.Contains(ss.state))
                    .ToList();
            }

            gridMatch.DataSource = docImportMatchDs_filtered;
            //gridMatch.TableElement.ScrollToRow(0);
        }

        private void txtEtalonNameFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                btnEtalonFilterApply.PerformClick();                
            }
        }

        private void txtEtalonNameFilter_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Поиск по вхождениям фрагментов, перечисленных здесь через пробел";
        }

        private void gridMatch_SelectionChanged(object sender, EventArgs e)
        {
            lblMatchSelectedCnt.Text = "Выделено строк: " + (gridMatch.SelectedRows != null ? gridMatch.SelectedRows.Count.ToString() : "0");
        }

        private void btnAutoMatch_Click(object sender, EventArgs e)
        {
            if (!docSelected)
            {
                MessageBox.Show("Сначала выберите документ для сопоставления");
                return;
            }

            if (autoMatchParamForm == null)
                autoMatchParamForm = new AutoMatchParamForm();
            if (autoMatchParamForm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                return;

            AutoMatchParam autoMatchParam = autoMatchParamForm.autoMatchParam;

            Cursor = Cursors.WaitCursor;
            try
            {
                if (autoMatchParam.MatchFull)
                {
                    var res = DocService.DocAutoMatchFull(selectedDoc.doc_id,
                        autoMatchParam.MatchFull_barcode,
                        autoMatchParam.MatchFull_name,
                        autoMatchParam.MatchFull_mfr,
                        autoMatchParam.MatchFull_approved
                        );
                    if (res.Error != null)
                    {
                        MessageBox.Show(res.Error.Mess);
                        return;
                    }
                }

                if (autoMatchParam.MatchPart)
                {
                    var res = DocService.DocAutoMatchPart(selectedDoc.doc_id,
                        autoMatchParam.MatchPart_barcode,
                        autoMatchParam.MatchPart_name,
                        autoMatchParam.MatchPart_mfr,
                        autoMatchParam.MatchPart_approved,
                        autoMatchParam.MatchPart_sum_percent,
                        autoMatchParam.MatchPart_percent,
                        autoMatchParam.MatchPart_barcode_percent,
                        autoMatchParam.MatchPart_name_percent,
                        autoMatchParam.MatchPart_mfr_percent                        
                        );
                    if (res.Error != null)
                    {
                        MessageBox.Show(res.Error.Mess);
                        return;
                    }
                }

                GridMatch_bind();

                chbShowVariant.Checked = true;                
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void gridMatch_CellFormatting(object sender, CellFormattingEventArgs e)
        {
            if (e.Row.DataBoundItem != null)
            {
                DocImportMatch rowMatchDataItem = (DocImportMatch)e.Row.DataBoundItem;
                if (rowMatchDataItem == null)
                    return;

                bool fontBold = !rowMatchDataItem.is_approved;

                bool draw = true;
                Color cellBackColor = Color.PaleGreen;                
                switch ((Enums.UndMatchStateEnum)rowMatchDataItem.state)
                {
                    case Enums.UndMatchStateEnum.NEW:
                        cellBackColor = Color.PaleGreen;
                        break;
                    case Enums.UndMatchStateEnum.SYN:
                        cellBackColor = Color.PaleTurquoise;
                        break;
                    case Enums.UndMatchStateEnum.NONE:
                        draw = false;
                        break;
                    default:
                        draw = false;
                        break;
                }

                e.CellElement.DrawFill = draw;
                if (draw)
                {
                    e.CellElement.NumberOfColors = 1;
                    e.CellElement.BackColor = cellBackColor;
                    if (fontBold)
                        e.CellElement.Font = new Font(e.CellElement.Font, FontStyle.Bold);
                }
            }

        }

        private void gridMatch_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                DocImportMatch rowMatchDataItem = (DocImportMatch)e.Row.DataBoundItem;
                if (rowMatchDataItem == null)
                    return;
                if (!rowMatchDataItem.match_good_id.HasValue)
                    return;
                var rowEtalon = gridEtalon.Rows.Where(ss => ((Product)ss.DataBoundItem).good_id == rowMatchDataItem.match_good_id).FirstOrDefault();
                if (rowEtalon == null)
                    return;
                gridEtalon.ClearSelection();
                rowEtalon.IsCurrent = true;
                rowEtalon.IsSelected = true;                
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void btnMatchAsIsToSyn_Click(object sender, EventArgs e)
        {
            if ((gridMatch.SelectedRows == null) || (gridMatch.SelectedRows.Count <= 0))
            {
                MessageBox.Show("Не выбраны строки для добавления в синонимы");
                return;
            }

            if ((gridEtalon.SelectedRows == null) || (gridEtalon.SelectedRows.Count <= 0))
            {
                MessageBox.Show("Не выбрана строка в эталонном справочнике");
                return;
            }

            Product etalonRowItem = (Product)gridEtalon.SelectedRows[0].DataBoundItem;
            if (etalonRowItem == null)
            {
                MessageBox.Show("Не выбрана строка в эталонном справочнике");
                return;
            }

            string winTopic = "Добавление в синонимы";
            string winMess = "Выделено строк: " + gridMatch.SelectedRows.Count.ToString();
            winMess += System.Environment.NewLine;
            winMess += "Добавить в синонимы ?";
            if (RadMessageBox.Show(this, winMess, winTopic, MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                return;

            foreach (var row in gridMatch.SelectedRows)
            {
                DocImportMatch rowItem = (DocImportMatch)row.DataBoundItem;
                if (rowItem == null)
                    continue;
                rowItem.state = (int)Enums.UndMatchStateEnum.SYN;
                rowItem.state_name = "Синоним";
                rowItem.match_product_name = etalonRowItem.partner_name;
                rowItem.match_product_id = etalonRowItem.product_id;
                rowItem.match_mfr_name = etalonRowItem.mfr_name;
                rowItem.match_barcode = etalonRowItem.barcode;
                rowItem.match_good_id = etalonRowItem.good_id;
                rowItem.is_changed = true;
                rowItem.is_confirmed = true;
                row.InvalidateRow();
            }
            //GridMatch_filter();
        }

        private void btnMatchChangeToSyn_Click(object sender, EventArgs e)
        {
            btnMatchAsIsToSyn_Click(sender, e);
        }

        private void btnMatchAsIsToNew_Click(object sender, EventArgs e)
        {
            if ((gridMatch.SelectedRows == null) || (gridMatch.SelectedRows.Count <= 0))
            {
                MessageBox.Show("Не выбраны строки для добавления в эталонные");
                return;
            }

            string winTopic = "Добавление в эталонные";
            string winMess = "Выделено строк: " + gridMatch.SelectedRows.Count.ToString();
            winMess += System.Environment.NewLine;
            winMess += "Добавить в эталонные ?";
            if (RadMessageBox.Show(this, winMess, winTopic, MessageBoxButtons.YesNo, RadMessageIcon.Question) != DialogResult.Yes)
                return;

            foreach (var row in gridMatch.SelectedRows)
            {
                DocImportMatch rowItem = (DocImportMatch)row.DataBoundItem;
                if (rowItem == null)
                    continue;
                if (rowItem.state == (int)Enums.UndMatchStateEnum.NEW)
                    continue;
                rowItem.state = (int)Enums.UndMatchStateEnum.NEW;
                rowItem.state_name = "Новое";
                rowItem.match_product_name = rowItem.source_name;
                rowItem.match_barcode = rowItem.barcode;
                rowItem.match_mfr_name = rowItem.mfr;
                rowItem.is_changed = true;
                rowItem.is_confirmed = true;
                row.InvalidateRow();
            }
            //GridMatch_filter();
        }

        private void btnMatchChangeToNew_Click(object sender, EventArgs e)
        {
            btnMatchAsIsToNew_Click(sender, e);
        }

        private void gridMatchVariant_CellDoubleClick(object sender, GridViewCellEventArgs e)
        {
            //MessageBox.Show("переход к товару в левом списке");
            DocImportMatchVariant rowDataItem = (DocImportMatchVariant)e.Row.DataBoundItem;
            if (rowDataItem == null)
                return;
            Cursor = Cursors.WaitCursor;
            try
            {
                var rowEtalon = gridEtalon.Rows.Where(ss => ((Product)ss.DataBoundItem).good_id == rowDataItem.match_good_id).FirstOrDefault();
                if (rowEtalon == null)
                    return;
                gridEtalon.ClearSelection();
                rowEtalon.IsSelected = true;
                rowEtalon.IsCurrent = true;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void gridMatch_CurrentRowChanged(object sender, CurrentRowChangedEventArgs e)
        {
            if (lockEvents_match)
                return;
            if (e.CurrentRow == null)
                return;
            DocImportMatch rowMatchDataItem = (DocImportMatch)e.CurrentRow.DataBoundItem;
            if (rowMatchDataItem == null)
                return;

            Cursor = Cursors.WaitCursor;
            try
            {
                GridMatchVariant_bind(rowMatchDataItem.row_id);
                UpdateMatchInfo(rowMatchDataItem);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void GridMatchVariant_bind(int row_id)
        {
            docImportMatchVariantDs_filtered = docImportMatchVariantDs.Where(ss => ss.row_id == row_id).ToList();
            gridMatchVariant.DataSource = docImportMatchVariantDs_filtered;
        }

        private void UpdateMatchInfo(DocImportMatch rowMatchDataItem)
        {
            txtMatchName.Text = rowMatchDataItem.source_name;
            txtMatchBarcode.Text = rowMatchDataItem.barcode;
            txtMatchMfr.Text = rowMatchDataItem.mfr;
            txtEtalonName.Text = rowMatchDataItem.match_product_name;
            txtEtalonBarcode.Text = rowMatchDataItem.match_barcode;
            txtEtalonMfr.Text = rowMatchDataItem.match_mfr_name;
        }

        private void updateSelectedDocText(bool refreshDoc = false)
        {
            if ((refreshDoc) && (selectedDoc != null))
            {
                selectedDoc = DocService.GetDoc(selectedDoc.doc_id);
            }
            txtSelectedDoc.Text = selectedDoc.doc_name;
            txtSelectedDoc.Text += " / ";
            txtSelectedDoc.Text += selectedDoc.partner_name;
            txtSelectedDoc.Text += " / ";
            txtSelectedDoc.Text += "строк всего: " + selectedDoc.row_cnt.GetValueOrDefault(0).ToString();
            txtSelectedDoc.Text += " / ";
            txtSelectedDoc.Text += "несопоставлено: " + selectedDoc.unmatch_cnt.GetValueOrDefault(0).ToString();            
        }

        private void chbShowVariant_ToggleStateChanged(object sender, StateChangedEventArgs args)
        {
            InitEtalonParams();
        }

        private void radLabel1_Click(object sender, EventArgs e)
        {
            chbMatchSyn.Checked = !chbMatchSyn.Checked;
        }

        private void radLabel7_Click(object sender, EventArgs e)
        {
            chbMatchNew.Checked = !chbMatchNew.Checked;
        }

        private void radLabel10_Click(object sender, EventArgs e)
        {
            chbUnapprovedOnly.Checked = !chbUnapprovedOnly.Checked;
        }

        private void btnImportConfigAdd_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Добавить новую конфигурацию";
        }

        private void btnImportConfigAdd_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Пока не реализовано");
        }

        private void btnImportConfigEdit_ToolTipTextNeeded(object sender, ToolTipTextNeededEventArgs e)
        {
            e.ToolTipText = "Изменить параметры конфигурации";
        }

        private void btnImportConfigEdit_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Пока не реализовано");
        }

        private void gridMatch_CellValueChanged(object sender, GridViewCellEventArgs e)
        {
            lblMatchCheckedCnt.Text = "Отмечено строк: " + (gridMatch.Rows.Where(ss => ((DocImportMatch)ss.DataBoundItem).is_confirmed).Count()).ToString();
        }

        private void timerImportDocElapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            // do whatever needs to be done
            DocImportInfoResult docImportInfoResult = DocImportService.GetDocImportRowsInfo(currDocImportId);
            if (docImportInfoResult.Error != null)
            {
                prgImportDoc.Maximum = 0;
                importDocTick = 0;
            }
            else
            {
                prgImportDoc.Maximum = docImportInfoResult.row_all_cnt.GetValueOrDefault(0);
                importDocTick = docImportInfoResult.row_done_cnt.GetValueOrDefault(0);
            }
            prgImportDoc.Text = importDocTick.ToString() + "/" + prgImportDoc.Maximum.ToString();
            
            prgImportDoc.Value1 = importDocTick;
            if ((importDocTick >= prgImportDoc.Maximum) || (docImportInfoResult.state == (int)Enums.UndDocImportStateEnum.READY))
            {
                //prgImportDoc.Visible = false;
                //prgImportDoc.Value1 = 0;
                //importDocTick = 0;
                timerImportDoc.Enabled = false;
            }
            else
            {
                timerImportDoc.Start(); // re-enables the timer
            }
        }
    }
}
