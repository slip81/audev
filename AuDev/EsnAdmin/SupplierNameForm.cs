﻿#region
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using EsnAdmin.ViewModel;
#endregion

namespace EsnAdmin
{
    public partial class SupplierNameForm : RadFormExt
    {
        public SupplierNameForm()
        {
            InitializeComponent();
            //
            InitSuppplierDdl();
            //
            InitImportParams();
        }

        private void InitSuppplierDdl()
        {
            ddlSupplier.DataSource = null;
            ddlSupplier.DataSource = SupplierService.GetSuppliers();
            ddlSupplier.DisplayMember = "partner_name";
            ddlSupplier.ValueMember = "partner_id";
            ddlSupplier.SelectedIndex = 0;
        }

        private void InitImportParams()
        {
            InitImportFileEditor();
            InitImportConfigDdl();
        }

        private void InitImportFileEditor()
        {
            OpenFileDialog dlg = (OpenFileDialog)edImportFile.Dialog;
            dlg.InitialDirectory = "c:\\";
            //openFile.Filter = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx|XML файлы (*.xml)|*.xml";
            //openFile.Filter = "Excel файлы (*.xls, *.xlsx)|*.xls;*.xlsx";
            dlg.Filter = "Excel файлы (*.xlsx)|*.xlsx|XML файлы (*.xml)|*.xml";            
        }

        private void InitImportConfigDdl()
        {
            ddlImportConfig.DataSource = null;
            ddlImportConfig.DataSource = DocImportService.GetDocImportConfigTypes();
            ddlImportConfig.DisplayMember = "config_type_name";
            ddlImportConfig.ValueMember = "config_type_id";
            ddlImportConfig.SelectedIndex = 0;
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                GridSupplier_bind();                
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void GridSupplier_bind()
        {
            var goods = GoodService.GetGoods((int)ddlSupplier.SelectedValue);

            if (goods.Error != null)
            {
                MessageBox.Show(goods.Error.Mess);
                return;
            }
            var goodDs = new List<Good>(goods.goods);

            gridSupplier.DataSource = goodDs;
            gridSupplier.TableElement.ScrollToRow(0);
            gridSupplier.MasterView.SummaryRows[0].PinPosition = PinnedRowPosition.Bottom;
        }

        private void btnImportStart_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                var import_id = DocImportService.CreateDocImport();
                var importResult = DocImportService.ImportSupplier(import_id, edImportFile.Value, (int)ddlImportConfig.SelectedValue);
                if (importResult.Error != null)
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show(importResult.Error.Mess);
                }
                else
                {
                    if (importResult.good_cnt > 0)
                    {
                        int? imported_partner_id = importResult.partner_id;
                        int current_partner_id = (int)ddlSupplier.SelectedValue;
                        if (current_partner_id != imported_partner_id)
                        {
                            InitSuppplierDdl();
                            ddlSupplier.SelectedValue = imported_partner_id;
                        }
                        GridSupplier_bind();
                    }
                    MessageBox.Show("Новых строк: " + importResult.good_cnt.ToString());
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
