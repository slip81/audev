﻿namespace EsnAdmin
{
    partial class EtalonNameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem1 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.gridEtalon = new Telerik.WinControls.UI.RadGridView();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.gridEtalonGood = new Telerik.WinControls.UI.RadGridView();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radGroupBox2 = new Telerik.WinControls.UI.RadGroupBox();
            this.btnImportStart = new Telerik.WinControls.UI.RadButton();
            this.ddlImportConfig = new Telerik.WinControls.UI.RadDropDownList();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.edImportFile = new Telerik.WinControls.UI.RadBrowseEditor();
            this.radGroupBox1 = new Telerik.WinControls.UI.RadGroupBox();
            this.btnRefresh = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalon.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).BeginInit();
            this.radGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlImportConfig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).BeginInit();
            this.radGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 113);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(955, 416);
            this.radSplitContainer1.TabIndex = 2;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.gridEtalon);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(955, 226);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.04906542F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 19);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // gridEtalon
            // 
            this.gridEtalon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEtalon.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridEtalon.MasterTemplate.AllowAddNewRow = false;
            this.gridEtalon.MasterTemplate.AllowDeleteRow = false;
            this.gridEtalon.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn1.AllowGroup = false;
            gridViewTextBoxColumn1.FieldName = "product_id";
            gridViewTextBoxColumn1.HeaderText = "Код";
            gridViewTextBoxColumn1.Name = "column1";
            gridViewTextBoxColumn1.Width = 150;
            gridViewTextBoxColumn2.AllowGroup = false;
            gridViewTextBoxColumn2.FieldName = "product_name";
            gridViewTextBoxColumn2.HeaderText = "Наименование";
            gridViewTextBoxColumn2.Name = "column2";
            gridViewTextBoxColumn2.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn2.Width = 650;
            gridViewTextBoxColumn3.AllowGroup = false;
            gridViewTextBoxColumn3.FieldName = "apply_group_name";
            gridViewTextBoxColumn3.HeaderText = "Группа по применению";
            gridViewTextBoxColumn3.Name = "column4";
            gridViewTextBoxColumn3.Width = 150;
            gridViewTextBoxColumn4.AllowGroup = false;
            gridViewTextBoxColumn4.FieldName = "good_cnt";
            gridViewTextBoxColumn4.HeaderText = "Кол-во товаров";
            gridViewTextBoxColumn4.Name = "column3";
            gridViewTextBoxColumn4.Width = 150;
            this.gridEtalon.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4});
            this.gridEtalon.MasterTemplate.EnableFiltering = true;
            this.gridEtalon.MasterTemplate.EnableGrouping = false;
            sortDescriptor1.PropertyName = "column2";
            this.gridEtalon.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1});
            gridViewSummaryItem1.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Count;
            gridViewSummaryItem1.FormatString = "Всего строк: {0}";
            gridViewSummaryItem1.Name = "column2";
            this.gridEtalon.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem1}));
            this.gridEtalon.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.gridEtalon.Name = "gridEtalon";
            this.gridEtalon.ReadOnly = true;
            this.gridEtalon.Size = new System.Drawing.Size(955, 226);
            this.gridEtalon.TabIndex = 0;
            this.gridEtalon.Text = "radGridView1";
            this.gridEtalon.SelectionChanged += new System.EventHandler(this.gridEtalon_SelectionChanged);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.gridEtalonGood);
            this.splitPanel2.Location = new System.Drawing.Point(0, 230);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(955, 186);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.04906542F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -19);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // gridEtalonGood
            // 
            this.gridEtalonGood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEtalonGood.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.gridEtalonGood.MasterTemplate.AllowAddNewRow = false;
            this.gridEtalonGood.MasterTemplate.AllowDeleteRow = false;
            this.gridEtalonGood.MasterTemplate.AllowEditRow = false;
            gridViewTextBoxColumn5.AllowGroup = false;
            gridViewTextBoxColumn5.FieldName = "partner_code";
            gridViewTextBoxColumn5.HeaderText = "Код";
            gridViewTextBoxColumn5.Name = "column1";
            gridViewTextBoxColumn5.Width = 100;
            gridViewTextBoxColumn6.AllowGroup = false;
            gridViewTextBoxColumn6.FieldName = "partner_name";
            gridViewTextBoxColumn6.HeaderText = "Наименование";
            gridViewTextBoxColumn6.Name = "column2";
            gridViewTextBoxColumn6.Width = 450;
            gridViewTextBoxColumn7.AllowGroup = false;
            gridViewTextBoxColumn7.FieldName = "barcode";
            gridViewTextBoxColumn7.HeaderText = "Штрих-код";
            gridViewTextBoxColumn7.Name = "column5";
            gridViewTextBoxColumn7.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn7.Width = 150;
            gridViewTextBoxColumn8.AllowGroup = false;
            gridViewTextBoxColumn8.FieldName = "mfr_name";
            gridViewTextBoxColumn8.HeaderText = "Производитель";
            gridViewTextBoxColumn8.Name = "column3";
            gridViewTextBoxColumn8.Width = 200;
            gridViewTextBoxColumn9.AllowGroup = false;
            gridViewTextBoxColumn9.FieldName = "source_partner_name";
            gridViewTextBoxColumn9.HeaderText = "Поставщик";
            gridViewTextBoxColumn9.Name = "column4";
            gridViewTextBoxColumn9.Width = 150;
            this.gridEtalonGood.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9});
            this.gridEtalonGood.MasterTemplate.EnableFiltering = true;
            this.gridEtalonGood.MasterTemplate.EnableGrouping = false;
            sortDescriptor2.PropertyName = "column5";
            this.gridEtalonGood.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor2});
            this.gridEtalonGood.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.gridEtalonGood.Name = "gridEtalonGood";
            this.gridEtalonGood.ReadOnly = true;
            this.gridEtalonGood.Size = new System.Drawing.Size(955, 186);
            this.gridEtalonGood.TabIndex = 1;
            this.gridEtalonGood.Text = "radGridView1";
            // 
            // radPanel1
            // 
            this.radPanel1.BackColor = System.Drawing.Color.Gainsboro;
            this.radPanel1.Controls.Add(this.radGroupBox2);
            this.radPanel1.Controls.Add(this.radGroupBox1);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(955, 113);
            this.radPanel1.TabIndex = 3;
            // 
            // radGroupBox2
            // 
            this.radGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox2.Controls.Add(this.btnImportStart);
            this.radGroupBox2.Controls.Add(this.ddlImportConfig);
            this.radGroupBox2.Controls.Add(this.radLabel2);
            this.radGroupBox2.Controls.Add(this.radLabel1);
            this.radGroupBox2.Controls.Add(this.edImportFile);
            this.radGroupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.radGroupBox2.HeaderText = "Загрузка из файла";
            this.radGroupBox2.Location = new System.Drawing.Point(263, 0);
            this.radGroupBox2.Name = "radGroupBox2";
            this.radGroupBox2.Size = new System.Drawing.Size(365, 113);
            this.radGroupBox2.TabIndex = 1;
            this.radGroupBox2.Text = "Загрузка из файла";
            // 
            // btnImportStart
            // 
            this.btnImportStart.Location = new System.Drawing.Point(217, 83);
            this.btnImportStart.Name = "btnImportStart";
            this.btnImportStart.Size = new System.Drawing.Size(130, 24);
            this.btnImportStart.TabIndex = 4;
            this.btnImportStart.Text = "Начать загрузку";
            this.btnImportStart.Click += new System.EventHandler(this.btnImportStart_Click);
            // 
            // ddlImportConfig
            // 
            this.ddlImportConfig.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.ddlImportConfig.Location = new System.Drawing.Point(111, 42);
            this.ddlImportConfig.Name = "ddlImportConfig";
            this.ddlImportConfig.Size = new System.Drawing.Size(236, 20);
            this.ddlImportConfig.TabIndex = 3;
            this.ddlImportConfig.Text = "radDropDownList1";
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(7, 45);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(84, 18);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "Конфигурация:";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(7, 16);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(35, 18);
            this.radLabel1.TabIndex = 1;
            this.radLabel1.Text = "Файл:";
            // 
            // edImportFile
            // 
            this.edImportFile.Location = new System.Drawing.Point(111, 16);
            this.edImportFile.Name = "edImportFile";
            this.edImportFile.Size = new System.Drawing.Size(236, 20);
            this.edImportFile.TabIndex = 0;
            this.edImportFile.Text = "radBrowseEditor1";
            // 
            // radGroupBox1
            // 
            this.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this.radGroupBox1.Controls.Add(this.btnRefresh);
            this.radGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radGroupBox1.HeaderText = "Формирование списка";
            this.radGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.radGroupBox1.Name = "radGroupBox1";
            this.radGroupBox1.Size = new System.Drawing.Size(263, 113);
            this.radGroupBox1.TabIndex = 0;
            this.radGroupBox1.Text = "Формирование списка";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(12, 21);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(130, 24);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Обновить список";
            this.btnRefresh.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // EtalonNameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 529);
            this.Controls.Add(this.radSplitContainer1);
            this.Controls.Add(this.radPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EtalonNameForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.Text = "Эталонный справочник наименований";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalon.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridEtalonGood)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox2)).EndInit();
            this.radGroupBox2.ResumeLayout(false);
            this.radGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnImportStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlImportConfig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edImportFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGroupBox1)).EndInit();
            this.radGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadGridView gridEtalon;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadGridView gridEtalonGood;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox2;
        private Telerik.WinControls.UI.RadGroupBox radGroupBox1;
        private Telerik.WinControls.UI.RadButton btnRefresh;
        private Telerik.WinControls.UI.RadButton btnImportStart;
        private Telerik.WinControls.UI.RadDropDownList ddlImportConfig;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadBrowseEditor edImportFile;
    }
}
