﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.ServiceModel.Web;
using System.Web.Script.Services;
using AuDev.Common.WCF;

namespace SenderSvc
{
    [ServiceContract(Namespace = "http://services.aurit.ru/sender/")]
    public interface ISenderService
    {


        #region License

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [WsdlDocumentation("Получает информацию о лицензии на заданную услугу для заданного userInfo")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult GetLicense([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Код услуги")]int service_id);

        #endregion

        #region Sender

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]        
        [WsdlDocumentation("Отправляет на почту зарегистрированных торговых точек файл с браком")]
        [return: WsdlParamOrReturnDocumentation("0 - отправка прошла удачно, > 0 - код ошибки"
            )]
        int SendMail_Defect();

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [WsdlDocumentation("Отправляет на почту зарегистрированных торговых точек файл с госреестром")]
        [return: WsdlParamOrReturnDocumentation("0 - отправка прошла удачно, > 0 - код ошибки"
            )]
        int SendMail_Grls();

        #endregion

        #region CVA

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [WsdlDocumentation("Получает информацию о лицензии на услугу ЦеныВАптеках для заданного userInfo")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult CVA_GetLicense([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [WsdlDocumentation("Отправляет пакет с данными на портал ЦеныВАптеках для заданного userInfo")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        CvaBatchSendResult CVA_SendBatch([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Отправляемый пакет")]CvaData batch,
            [WsdlParamOrReturnDocumentation("1 - отправлять раздел info, иначе - не отправлять")]int shortformat
            );

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [WsdlDocumentation("Получает информацию об отправленных пакетах на ЦеныВАптеках для заданного userInfo")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура CvaSendInfoResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), cvaInfo - информация об отправленных пакетах:"
            + " LastSendDate - последняя дата успешной отправки пакета, BatchNumForSend - номер пакета, находящегося в очереди на отправку, BatchDateForSend - дата пакета, находящегося в очереди на отправку,"
            + " BatchSendCount - кол-во успешно отправленных пакетов, MinSendInterval - минимальный интервал для отправки пакетов"
            )]
        CvaSendInfo CVA_GetSendInfo([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo);

        #endregion

        #region RPT

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [WsdlDocumentation("Получает информацию о лицензии на услугу Сервис отчетности для заданного userInfo")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult RPT_GetLicense([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo);

        #endregion

        #region GetErrorCodeList

        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        [WsdlDocumentation("Возвращает список всех возможных кодов ошибок")]
        [return: WsdlParamOrReturnDocumentation("Возвращает коллекцию элементов с атрибутами:"
            + "Id - код ошибки, Name - описание ошибки"
            )]
        List<ErrorCode> GetErrorCodeList();

        #endregion

        #region Exchange

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        ExchangeData GetExchangeData_Plan(UserInfo userInfo);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        ExchangeData GetExchangeData_Discount(UserInfo userInfo);

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        ExchangeData GetExchangeData_Recommend(UserInfo userInfo);

        #endregion

        #region JSON Support


        [OperationContract]
        [WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
        void GetOptions();

        #endregion

    }
}
