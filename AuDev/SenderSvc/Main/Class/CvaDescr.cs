﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AuDev.Common.Db.Model;

namespace SenderSvc
{    
    public class CvaDescr
    {
        public CvaDescr()
        {
            //
        }

        public CvaDescr(service_cva item)
        {
            //info_internet_trade = item.info_internet_trade;
            info_internet_trade = "0";
            info_name = item.info_name;
            info_bank_cards = item.info_bank_cards;
            info_sale_phone = item.info_sale_phone;
            info_additional_info = item.info_additional_info;
            info_www = item.info_www;
            info_city = item.info_city;
            info_city_area = item.info_city_area;
            info_address = item.info_address;
            info_sale_shedule = item.info_sale_shedule;
            info_map_gps = item.info_map_gps;
            info_map_caption = item.info_map_caption;
        }

        [JsonProperty("internet_trade")]
        public string info_internet_trade { get; set; }
        [JsonProperty("name")]
        public string info_name { get; set; }
        [JsonProperty("bank_cards")]
        public string info_bank_cards { get; set; }
        [JsonProperty("sale_phone")]
        public string info_sale_phone { get; set; }
        [JsonProperty("additional_info")]
        public string info_additional_info { get; set; }
        [JsonProperty("www")]
        public string info_www { get; set; }
        [JsonProperty("city")]
        public string info_city { get; set; }
        [JsonProperty("city_area")]
        public string info_city_area { get; set; }
        [JsonProperty("address")]
        public string info_address { get; set; }
        [JsonProperty("sale_shedule")]
        public string info_sale_shedule { get; set; }
        [JsonProperty("map_gps")]
        public string info_map_gps { get; set; }
        [JsonProperty("map_caption")]
        public string info_map_caption { get; set; }
    }
}
