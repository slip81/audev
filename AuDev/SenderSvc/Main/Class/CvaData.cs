﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Linq;
using System.Runtime.Serialization;

namespace SenderSvc
{
    [DataContract]
    public class CvaData
    {
        [DataMember]
        public List<CvaItemData> data { get; set; }
        [DataMember]
        public List<CvaConsItemData> consolidated_data { get; set; }
    }

    public class CvaItemData
    {
        public string code { get; set; }
        public string name { get; set; }
        public string manufactor { get; set; }
        public string mnfcountry { get; set; }
        public string image { get; set; }
        public string href { get; set; }
        public string price { get; set; }
        public string onrequest { get; set; }
        public string qty { get; set; }
        public List<CvaCoCode> co_codes { get; set; }
    }

    public class CvaConsItemData
    {
        public string code { get; set; }
        public List<CvaCoCode> co_codes { get; set; }
    }

    public class CvaCoCode
    {
        public string code { get; set; }
        public string code_type { get; set; }
    }
}
