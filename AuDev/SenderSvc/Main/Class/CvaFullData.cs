﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AuDev.Common.Db.Model;

namespace SenderSvc
{    
    public class CvaFullData : CvaData
    {
        public CvaFullData()
        {
            //
        }

        public CvaFullData(service_cva item, int shortformat)
        {
            pharm_code = item.pharm_code;
            active = item.active.ToString();
            data_type = "0";
            //
            info = (shortformat == 1) ? null : new CvaDescr(item);
        }

        [JsonProperty("pharm_code")]
        public string pharm_code { get; set; }

        [JsonProperty("active")]
        public string active { get; set; }

        [JsonProperty("data_type")]
        public string data_type { get; set; }

        [JsonProperty("info")]
        public CvaDescr info { get; set; }                
    }
}
