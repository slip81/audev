﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace SenderSvc
{
    [DataContract]
    public class CvaInfo
    {
        public CvaInfo(vw_service_cva_info item)
        {
            last_sent_date = item.last_sent_date.HasValue ? ((DateTime)item.last_sent_date).ToString("dd.MM.yyyy HH:mm:ss") : "";
            batch_num_for_send = item.batch_num_for_send;
            batch_date_for_send = item.batch_date_for_send.HasValue ? ((DateTime)item.batch_date_for_send).ToString("dd.MM.yyyy HH:mm:ss") : "";
            batch_sent_count = item.batch_sent_count.HasValue ? (int)item.batch_sent_count : 0;
            send_interval = item.send_interval.HasValue ? (int)item.send_interval : 0;
        }

        [DataMember]
        public string last_sent_date { get; set; }
        [DataMember]
        public int? batch_num_for_send { get; set; }
        [DataMember]
        public string batch_date_for_send { get; set; }
        [DataMember]
        public int batch_sent_count { get; set; }
        [DataMember]
        public int send_interval { get; set; }
    }
}
