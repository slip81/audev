﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;
using WinSCP;
using UnidecodeSharpFork;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
#endregion

namespace SenderSvc
{
    public partial class SenderService : ISenderService
    {
        private ServiceOperationResult xGetLicense(UserInfo userInfo, int service_id, bool check_version = false)
        {
            DateTime now = DateTime.Now;
            string err_mess = "";

            string login = String.IsNullOrEmpty(userInfo.login) ? "" : userInfo.login.Trim().ToLower();
            string workplace = String.IsNullOrEmpty(userInfo.workplace) ? "" : userInfo.workplace.Trim().ToLower();
            string version_num = (String.IsNullOrEmpty(userInfo.version_num) || (!check_version)) ? "" : userInfo.version_num.Trim().ToLower();
            //string version_num = String.IsNullOrEmpty(userInfo.version_num) ? "" : userInfo.version_num.Trim().ToLower();

            if (String.IsNullOrEmpty(login))
            {
                err_mess = "Не задан логин";
                Loggly.InsertLog_License(err_mess, (long)Enums.LogEventType.ERROR, "license-checker", null, now, null);
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, err_mess), };
            }

            if (String.IsNullOrEmpty(workplace))
            {
                err_mess = "Не задан железный ключ";
                Loggly.InsertLog_License(err_mess, (long)Enums.LogEventType.ERROR, login, null, now, null);
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, err_mess), };
            }

            if (service_id <= 0)
            {
                err_mess = "Не задан код услуги";
                Loggly.InsertLog_License(err_mess, (long)Enums.LogEventType.ERROR, login, null, now, null);
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, err_mess), };
            }

            using (var dbContext = new AuMainDb())
            {
                var user = (from t1 in dbContext.vw_client_user
                            from t2 in dbContext.workplace
                            where t1.is_active == 1
                            && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                            && t2.sales_id == t1.sales_id
                            && t2.is_deleted != 1
                            && t2.registration_key.Trim().ToLower().Equals(workplace)
                            select new { client_id = t1.client_id, sales_id = t1.sales_id, workplace_id = t2.id })
                           .FirstOrDefault();

                if (user == null)
                {
                    err_mess = "Не найден пользователь с логином " + login.Trim() + " и жел. ключом " + workplace.Trim();
                    Loggly.InsertLog_License(err_mess, (long)Enums.LogEventType.ERROR, login, null, now, null);
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, err_mess), };
                }

                bool versionSpecified = !String.IsNullOrEmpty(version_num);
                string version_name_start = !versionSpecified ? "" : new string(version_num.Take(4).ToArray());
                var license = (from t1 in dbContext.vw_client_service
                               where t1.workplace_id == user.workplace_id
                               && t1.service_id == service_id
                               && ((versionSpecified && t1.version_name != null && t1.version_name.StartsWith(version_name_start)) || (!versionSpecified))
                               select t1)
                              .FirstOrDefault();

                if (license == null)
                {
                    var service = dbContext.service.Where(ss => ss.id == service_id).FirstOrDefault();
                    err_mess = "Не найдена лицензия на услугу " + (service != null ? service.name.Trim() : ("с кодом " + service_id.ToString())) + ", " + (versionSpecified ? ("версия " + version_num.Trim() + ", ") : "") + "пользователь с логином " + login + " и жел. ключом " + workplace;
                    Loggly.InsertLog_License(err_mess, (long)Enums.LogEventType.ERROR, login, null, now, null);
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, err_mess), };
                }

                return new ServiceOperationResult() { Id = 0, };
            }
        }
    }
}
