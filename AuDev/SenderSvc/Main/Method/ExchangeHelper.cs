﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;
using WinSCP;
using UnidecodeSharpFork;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
#endregion

namespace SenderSvc
{
    public partial class SenderService : ISenderService
    {
        /*
            [Display(Name = "Товары ЦП")]
            Plan = 4,
            [Display(Name = "Товары по акциям")]
            Discount = 5,
            [Display(Name = "Товары ПР")]
            Recommend = 6,
        */

        const int SERVICE_ID_EXCHANGE = 52;

        private ServiceOperationResult xExchange_GetLicense(UserInfo userInfo)
        {
            return xGetLicense(userInfo, SERVICE_ID_EXCHANGE, false);

            /*
            if ((userInfo == null) || (String.IsNullOrEmpty(userInfo.login)) || (String.IsNullOrEmpty(userInfo.workplace)))
            {
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Некорректно задан атрибут userInfo"), };
            }

            DateTime today = DateTime.Today;
            using (var dbContext = new AuMainDb())
            {
                var lic = dbContext.vw_license2.Where(ss => ss.service_id == SERVICE_ID_EXCHANGE
                    && ss.user_name != null
                    && ss.user_name.Trim().ToLower().Equals(userInfo.login.Trim().ToLower())
                    && ss.registration_key != null
                    && ss.registration_key.Trim().ToLower().Equals(userInfo.workplace.Trim().ToLower())
                    )
                    .FirstOrDefault();
                if ((lic == null) || ((lic.dt_start.HasValue) && (lic.dt_start > today)) || ((lic.dt_end.HasValue) && (lic.dt_end < today)))
                {
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Не найдена лицензия для логина " + userInfo.login.Trim() + " и рабочего места " + userInfo.workplace.Trim()), };
                }

                if (!lic.workplace_id.HasValue)
                {
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Не найден код рабочего места для лицензии для логина " + userInfo.login.Trim() + " и рабочего места " + userInfo.workplace.Trim()), };
                }

                return new ServiceOperationResult() { Id = 0, };
            }
            */
        }

        private ExchangeData xGetExchangeData(UserInfo userInfo, Enums.ExchangeItemEnum exchangeItem)
        {       
            //var lic = xExchange_GetLicense(userInfo);
            var lic = xGetLicense(userInfo, SERVICE_ID_EXCHANGE, false);
            if (lic.Id > 0)
            {
                return new ExchangeData() { error = new ErrInfo(lic.Id, lic.error.Message), };
            }

            using (var dbContext = new AuMainDb())
            {
                // !!!
                // test: exchange_type = 4 (Plan)
                // var exchange_data = dbContext.exchange_data.Where(ss => ss.exchange_id == 10645 && ss.data_binary != null).FirstOrDefault();

                //var exchange_user = dbContext.exchange_user.Where(ss => ss.login.Trim().ToLower().Equals(userInfo.login.Trim().ToLower()) && ss.workplace.Trim().ToLower().Equals(userInfo.workplace.Trim().ToLower())).FirstOrDefault();
                var exchange_user = dbContext.exchange_user.Where(ss => ss.login.Trim().ToLower().Equals(userInfo.login.Trim().ToLower())).FirstOrDefault();
                if (exchange_user == null)
                {
                    return new ExchangeData() { error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден участник обмена с логином " + userInfo.login.Trim() + " и рабочим местом " + userInfo.workplace.Trim()), data = null, crt_date = null, };
                }
                                    
                var exchange_data = (from t1 in dbContext.exchange
                                     from t2 in dbContext.exchange_data
                                     where t1.exchange_id == t2.exchange_id
                                     && t1.user_id == exchange_user.user_id
                                     && t1.exchange_type == (int)exchangeItem
                                     orderby t1.exchange_id descending
                                     select new { t1.crt_date, t2.data_binary }
                                    ).FirstOrDefault();
                
                if ((exchange_data == null) || (exchange_data.data_binary == null))
                {
                    return new ExchangeData() { error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены данные в БД"), data = null, crt_date = null, };
                }

                var exchange_data_text = GlobalUtil.GetString(exchange_data.data_binary);
                string crt_date_text = exchange_data.crt_date.HasValue ? ((DateTime)exchange_data.crt_date).ToString("dd.MM.yyyy HH:mm:ss") : null;

                return new ExchangeData() { data = exchange_data_text, crt_date = crt_date_text, error = null, };

                //string exchange_data_text_json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(exchange_data_text);
                //string crt_date_text = new DateTime(2016, 10, 12).ToString("dd.MM.yyyy HH:mm:ss");
                //return new ExchangeData() { data = exchange_data_text_json, crt_date = crt_date_text, error = null, };
            }
            
            //return new ExchangeData() { data = "1", error = null, };            
        }
    }
}
