﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;
using WinSCP;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
#endregion

namespace SenderSvc
{
    public partial class SenderService : ISenderService
    {
        const int SERVICE_ID_MAIL_DEFECT = 57;
        const int SERVICE_ID_MAIL_GRLS = 58;

        #region SendMail_Defect

        private int xSendMail_Defect()        
        {
            int res = 0;
                        
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            bool sendDone = false;

            using (var dbContext = new AuMainDb())
            {
                var recipient_list = (from t1 in dbContext.service_sender
                                     from t2 in dbContext.vw_workplace                                     
                                     where t1.service_id == SERVICE_ID_MAIL_DEFECT
                                     && t1.workplace_id == t2.workplace_id
                                     select new { 
                                         date_beg = t1.date_beg,
                                         date_end = t1.date_end,
                                         email = t1.email,
                                         interval_in_days = t1.interval_in_days,
                                         is_disabled = t1.is_disabled,                                         
                                         workplace_id = t1.workplace_id,
                                         client_id = t2.client_id, 
                                         client_name = t2.client_name, 
                                         sales_id = t2.sales_id, 
                                         sales_address = t2.sales_address, 
                                         sales_email = t1.email,
                                         schedule_type = t1.schedule_type,
                                         schedule_weekly = t1.schedule_weekly,
                                         schedule_monthly = t1.schedule_monthly
                                     }
                                     )
                                     .Distinct()
                                     .ToList();

                string client_name = "";
                string sales_address = "";
                string email = "";
                int todayIndex = 0;
                int days_cnt_from_date_beg = 0;
                foreach (var recipient in recipient_list)
                {
                    if (recipient.is_disabled)
                        continue;

                    if ((recipient.date_end.HasValue) && (recipient.date_end < today))
                        continue;

                    if (((recipient.date_beg.HasValue) && (recipient.date_beg > today)) || (!recipient.date_beg.HasValue))
                        continue;

                    switch ((Enums.ScheduleTypeEnum)recipient.schedule_type)
                    {
                        case Enums.ScheduleTypeEnum.INTERVAL_DAILY:
                            if (recipient.interval_in_days <= 0)
                                continue;
                            days_cnt_from_date_beg = Convert.ToInt32(Math.Floor((today - (DateTime)recipient.date_beg).TotalDays));                    
                            long days = 0;
                            long remainder = 0;
                            days = Math.DivRem(days_cnt_from_date_beg, recipient.interval_in_days, out remainder);
                            if (remainder != 0)
                                continue;
                            break;
                        case Enums.ScheduleTypeEnum.SCHEDULE_WEEKLY:
                            if (String.IsNullOrEmpty(recipient.schedule_weekly))
                                continue;
                            // 0 - пн, 1 - вт, ..., 6 - вс
                            todayIndex = (((int)now.DayOfWeek == 0) ? 7 : (int)now.DayOfWeek) - 1;
                            int[] weeklyDays = recipient.schedule_weekly.Split(',').Select(int.Parse).ToArray();
                            if (weeklyDays[todayIndex] != 1)
                                continue;
                            break;
                        case Enums.ScheduleTypeEnum.SCHEDULE_MONTHLY:
                            if (String.IsNullOrEmpty(recipient.schedule_monthly))
                                continue;
                            // zero-based day of month (0 to 30)
                            todayIndex = now.Day - 1;
                            int[] monthlyDays = recipient.schedule_monthly.Split(',').Select(int.Parse).ToArray();
                            if (monthlyDays[todayIndex] != 1)
                                continue;
                            break;
                        default:
                            break;
                    }

                    sendDone = true;

                    client_name = recipient.client_name;
                    sales_address = (String.IsNullOrEmpty(recipient.sales_address) ? recipient.sales_id.ToString() : recipient.sales_address);
                    email = recipient.email;                                       

                    if (String.IsNullOrEmpty(recipient.email))
                    {
                        Loggly.InsertLog_Sender("[" + client_name + "][" + sales_address + "] Не указана электронная почта", (long)Enums.LogEsnType.SENDER, "sender-defect", null, now, null);
                        continue;
                    }

                    SendMail(email, "Список брака от " + today.ToString("dd.MM.yyyy"), "remove.zip", @"C:\Release\Defect\File\Arc\remove.zip", true);
                    Loggly.InsertLog_Sender("[" + client_name + "][" + sales_address + "] Файл remove.zip выслан на " + email, (long)Enums.LogEsnType.SENDER, "sender-defect", null, now, null);

                    service_sender_log service_sender_log = new service_sender_log();
                    service_sender_log.date_beg = now;
                    service_sender_log.mess = "Файл remove.zip выслан на " + email;
                    service_sender_log.service_id = SERVICE_ID_MAIL_DEFECT;
                    service_sender_log.workplace_id = recipient.workplace_id;
                    dbContext.service_sender_log.Add(service_sender_log);
                    dbContext.SaveChanges();
                }

                if (!sendDone)
                    Loggly.InsertLog_Sender("Нет торговых точек для рассылки брака", (long)Enums.LogEsnType.SENDER, "sender-defect", null, now, null);

                return res;
            }
            
        }        

        #endregion

        #region SendMail_Grls

        private int xSendMail_Grls()
        {
            int res = 0;

            // это не надо, т.к. сейчас Госреестр копируется в папку C:\Release\Defect\File\Arc\ сервисом скачивания брака
            /*
            SessionOptions sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Scp,
                HostName = "aptekaural.ru",
                UserName = "aptekaural",
                //Password = "U435JkjDF",
                //Password = "U447JkjDF",
                Password = "U447JkjDf",
                //SshHostKeyFingerprint = "ssh-rsa 2048 53:d7:cf:bb:c2:29:50:08:a7:7e:4d:6f:62:f8:b8:da",
                SshHostKeyFingerprint = "ssh-rsa 2048 00:a9:f8:0a:52:7e:12:c2:b5:54:2b:d5:b6:42:f0:f6",
            };

            using (Session session = new Session())
            {
                // Connect
                session.Open(sessionOptions);

                // Download file
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = WinSCP.TransferMode.Binary;

                TransferOperationResult transferResult;
                transferResult = session.GetFiles("/web/aptekaural/site/www/docs/reestr/reestr.zip", @"C:\Release\Defect\File\Arc\", false, transferOptions);

                // Throw on any error
                transferResult.Check();
            }
            */

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            bool sendDone = false;

            using (var dbContext = new AuMainDb())
            {
                var recipient_list = (from t1 in dbContext.service_sender
                                      from t2 in dbContext.vw_workplace
                                      where t1.service_id == SERVICE_ID_MAIL_GRLS
                                      && t1.workplace_id == t2.workplace_id
                                      select new
                                      {
                                          date_beg = t1.date_beg,
                                          date_end = t1.date_end,
                                          email = t1.email,
                                          interval_in_days = t1.interval_in_days,
                                          is_disabled = t1.is_disabled,
                                          workplace_id = t1.workplace_id,
                                          client_id = t2.client_id,
                                          client_name = t2.client_name,
                                          sales_id = t2.sales_id,
                                          sales_address = t2.sales_address,
                                          sales_email = t1.email,
                                          schedule_type = t1.schedule_type,
                                          schedule_weekly = t1.schedule_weekly,
                                          schedule_monthly = t1.schedule_monthly
                                      }
                                     )
                                     .Distinct()
                                     .ToList();

                string client_name = "";
                string sales_address = "";
                string email = "";
                int days_cnt_from_date_beg = 0;
                int todayIndex = 0;
                foreach (var recipient in recipient_list)
                {
                    if (recipient.is_disabled)
                        continue;

                    if ((recipient.date_end.HasValue) && (recipient.date_end < today))
                        continue;

                    if (((recipient.date_beg.HasValue) && (recipient.date_beg > today)) || (!recipient.date_beg.HasValue))
                        continue;

                    switch ((Enums.ScheduleTypeEnum)recipient.schedule_type)
                    {
                        case Enums.ScheduleTypeEnum.INTERVAL_DAILY:
                            if (recipient.interval_in_days <= 0)
                                continue;
                            days_cnt_from_date_beg = Convert.ToInt32(Math.Floor((today - (DateTime)recipient.date_beg).TotalDays));
                            long days = 0;
                            long remainder = 0;
                            days = Math.DivRem(days_cnt_from_date_beg, recipient.interval_in_days, out remainder);
                            if (remainder != 0)
                                continue;
                            break;
                        case Enums.ScheduleTypeEnum.SCHEDULE_WEEKLY:
                            if (String.IsNullOrEmpty(recipient.schedule_weekly))
                                continue;
                            // 0 - пн, 1 - вт, ..., 6 - вс
                            todayIndex = (((int)now.DayOfWeek == 0) ? 7 : (int)now.DayOfWeek) - 1;
                            int[] weeklyDays = recipient.schedule_weekly.Split(',').Select(int.Parse).ToArray();
                            if (weeklyDays[todayIndex] != 1)
                                continue;
                            break;
                        case Enums.ScheduleTypeEnum.SCHEDULE_MONTHLY:
                            if (String.IsNullOrEmpty(recipient.schedule_monthly))
                                continue;
                            // zero-based day of month (0 to 30)
                            todayIndex = now.Day - 1;
                            int[] monthlyDays = recipient.schedule_monthly.Split(',').Select(int.Parse).ToArray();
                            if (monthlyDays[todayIndex] != 1)
                                continue;
                            break;
                        default:
                            break;
                    }

                    sendDone = true;

                    client_name = recipient.client_name;
                    sales_address = (String.IsNullOrEmpty(recipient.sales_address) ? recipient.sales_id.ToString() : recipient.sales_address);
                    email = recipient.email;

                    if (String.IsNullOrEmpty(recipient.email))
                    {
                        Loggly.InsertLog_Sender("[" + client_name + "][" + sales_address + "] Не указана электронная почта", (long)Enums.LogEsnType.SENDER, "sender-grls", null, now, null);
                        continue;
                    }

                    SendMail(email, "Госреестр от " + today.ToString("dd.MM.yyyy"), "reestr.zip", @"C:\Release\Defect\File\Arc\reestr.zip", true);
                    Loggly.InsertLog_Sender("[" + client_name + "][" + sales_address + "] Файл reestr.zip выслан на " + email, (long)Enums.LogEsnType.SENDER, "sender-grls", null, now, null);

                    service_sender_log service_sender_log = new service_sender_log();
                    service_sender_log.date_beg = now;
                    service_sender_log.mess = "Файл reestr.zip выслан на " + email;
                    service_sender_log.service_id = SERVICE_ID_MAIL_GRLS;
                    service_sender_log.workplace_id = recipient.workplace_id;
                    dbContext.service_sender_log.Add(service_sender_log);
                    dbContext.SaveChanges();
                }

                if (!sendDone)
                    Loggly.InsertLog_Sender("Нет торговых точек для рассылки госреестра", (long)Enums.LogEsnType.SENDER, "sender-grls", null, now, null);

                return res;
            }
        }

        #endregion

        #region SendMail

        private bool SendMail(string target_address, string topic, string attachment_name, string attachment_path, bool is_arc)
        {
            string sender_smtp_address = "smtp.gmail.com";
            int sender_smtp_port = 587;
            bool sender_smtp_ssl = true;
            string sender_address = "cab.aptekaural@gmail.com";
            string sender_login = "cab.aptekaural@gmail.com";
            string sender_alias = "cab.aptekaural";
            string sender_password = "aptekaural";

            DateTime now = DateTime.Now;
            using (SmtpClient c = new SmtpClient(sender_smtp_address, sender_smtp_port))
            {
                MailAddress add = new MailAddress(target_address);
                MailMessage msg = new MailMessage();
                msg.To.Add(add);
                msg.From = new MailAddress(sender_address, sender_alias);
                msg.IsBodyHtml = true;
                msg.Subject = topic;
                msg.Body = "";

                var zipCt = new ContentType { MediaType = (is_arc ? MediaTypeNames.Application.Zip : MediaTypeNames.Application.Octet), };
                var attachmentA = new Attachment(attachment_path, zipCt);
                attachmentA.ContentDisposition.FileName = attachment_name;
                attachmentA.Name = attachment_name;
                msg.Attachments.Add(attachmentA);

                c.Credentials = new System.Net.NetworkCredential(sender_login, sender_password);
                c.EnableSsl = sender_smtp_ssl;
                c.Send(msg);
            }

            return true;
        }

        #endregion

        #region GetOptions (JSON Support)

        public void GetOptions()
        {
            // Заголовки обработаются в EnableCorsMessageInspector 
        }

        #endregion

    }
}
