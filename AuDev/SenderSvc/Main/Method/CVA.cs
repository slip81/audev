﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;
using WinSCP;
using UnidecodeSharpFork;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
#endregion

namespace SenderSvc
{
    public partial class SenderService : ISenderService
    {
        const int SERVICE_ID_CVA = 59;

        private ServiceOperationResult xCVA_GetLicense(UserInfo userInfo, out int workplace_id)
        {            
            workplace_id = 0;
            /*
            return xGetLicense(userInfo, SERVICE_ID_CVA, false);
            */

            if ((userInfo == null) || (String.IsNullOrEmpty(userInfo.login)) || (String.IsNullOrEmpty(userInfo.workplace)))
            {
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Некорректно задан атрибут userInfo"), };
            }            

            DateTime today = DateTime.Today;
            using (var dbContext = new AuMainDb())
            {
                var lic = dbContext.vw_license2.Where(ss => ss.service_id == SERVICE_ID_CVA
                    && ss.user_name != null
                    && ss.user_name.Trim().ToLower().Equals(userInfo.login.Trim().ToLower())
                    && ss.registration_key != null
                    && ss.registration_key.Trim().ToLower().Equals(userInfo.workplace.Trim().ToLower())
                    )
                    .FirstOrDefault();
                if ((lic == null) || ((lic.dt_start.HasValue) && (lic.dt_start > today)) || ((lic.dt_end.HasValue) && (lic.dt_end < today)))
                {
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Не найдена лицензия для логина " + userInfo.login.Trim() + " и рабочего места " + userInfo.workplace.Trim()), };
                }

                if (!lic.workplace_id.HasValue)
                {
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Не найден код рабочего места для лицензии для логина " + userInfo.login.Trim() + " и рабочего места " + userInfo.workplace.Trim()), };
                }

                workplace_id = (int)lic.workplace_id;
                return new ServiceOperationResult() { Id = 0, };
            }
        }

        private CvaBatchSendResult xCVA_SendBatch(UserInfo userInfo, CvaData batch, int shortformat)
        {
            int workplace_id = 0;
            var res1 = xCVA_GetLicense(userInfo, out workplace_id);
            //var res1 = xGetLicense(userInfo, SERVICE_ID_CVA, false);
            if (res1.Id > 0)
            {
                return new CvaBatchSendResult() { Id = res1.Id, error = new ErrInfo(res1.Id, res1.error.Message), };
            }

            if ((batch == null) || ((batch.data == null) && (batch.consolidated_data == null)))
            {
                return new CvaBatchSendResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан входной параметр batch"), };
            }

            string login = userInfo.login;

            using (var dbContext = new AuMainDb())
            {
                service_cva service_cva = dbContext.service_cva.Where(ss => ss.workplace_id == workplace_id && ss.service_id == SERVICE_ID_CVA).FirstOrDefault();
                if (service_cva == null)
                {
                    return new CvaBatchSendResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена услуга ЦВА для логина " + userInfo.login + " и рабочего места " + userInfo.workplace), };
                }

                if (String.IsNullOrEmpty(service_cva.target_url))
                {
                    return new CvaBatchSendResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан адрес отправки файлов для логина " + userInfo.login + " и рабочего места " + userInfo.workplace), };
                }

                if (String.IsNullOrEmpty(service_cva.pharm_login))
                {
                    return new CvaBatchSendResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан логин для отправки файлов для логина " + userInfo.login + " и рабочего места " + userInfo.workplace), };
                }

                service_cva_data service_cva_data = new service_cva_data();
                service_cva_data.batch_date = DateTime.Now;
                service_cva_data.batch_num = dbContext.service_cva_data.Where(ss => ss.workplace_id == workplace_id && ss.service_id == SERVICE_ID_CVA).OrderByDescending(ss => ss.batch_num).Select(ss => ss.batch_num).FirstOrDefault().GetValueOrDefault(0) + 1;
                service_cva_data.batch_state = (int)Enums.CvaSendStateEnum.NOT_SENT;
                service_cva_data.service_id = SERVICE_ID_CVA;
                service_cva_data.workplace_id = workplace_id;
                dbContext.service_cva_data.Add(service_cva_data);

                service_cva_data_ext service_cva_data_ext = new service_cva_data_ext();
                service_cva_data_ext.service_cva_data = service_cva_data;
                service_cva_data_ext.cva_cons_data = JsonConvert.SerializeObject(batch.consolidated_data);
                service_cva_data_ext.cva_data = JsonConvert.SerializeObject(batch.data);
                dbContext.service_cva_data_ext.Add(service_cva_data_ext);

                dbContext.SaveChanges();

                Task task = new Task(() =>
                {
                    xxCVA_SendBatch(batch, workplace_id, service_cva.item_id, service_cva_data.item_id, shortformat, service_cva.pharm_code);
                }
                );
                task.Start();

                return new CvaBatchSendResult() { Id = 0, BatchNum = service_cva_data.batch_num, };
            }
            
        }

        private ServiceOperationResult xxCVA_SendBatch(CvaData batch, int workplace_id, int cva_id, int cva_data_id, int shortformat, string pharm_code)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xxxCVA_SendBatch(batch, workplace_id, cva_id, cva_data_id, shortformat);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_CVA("Ошибка в CVA_SendBatch: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, (String.IsNullOrWhiteSpace(pharm_code) ? "cva" : pharm_code), null, log_date_beg, null);
                return CreateErrSenderBase<ServiceOperationResult>(e);
            }
        }

        private ServiceOperationResult xxxCVA_SendBatch(CvaData batch, int workplace_id, int cva_id, int cva_data_id, int shortformat)
        {
            using (var dbContext = new AuMainDb())
            {
                service_cva service_cva = null;
                service_cva_data service_cva_data = null;
                service_cva_data_ext service_cva_data_ext = null;
                string source_file_path_orig = "";
                string source_file_path_zip = "";
                string target_address = "";
                try
                {
                    service_cva = dbContext.service_cva.Where(ss => ss.item_id == cva_id).FirstOrDefault();
                    service_cva_data = dbContext.service_cva_data.Where(ss => ss.item_id == cva_data_id).FirstOrDefault();
                    service_cva_data_ext = dbContext.service_cva_data_ext.Where(ss => ss.item_id == cva_data_id).FirstOrDefault();

                    service_cva_data.batch_state = (int)Enums.CvaSendStateEnum.SENDING;
                    service_cva_data.sending_date = DateTime.Now;
                    dbContext.SaveChanges();

                    JsonSerializerSettings jsonSettings = new JsonSerializerSettings();
                    jsonSettings.NullValueHandling = NullValueHandling.Ignore;
                    jsonSettings.StringEscapeHandling = StringEscapeHandling.Default;                    

                    CvaFullData cvaFullData = new CvaFullData(service_cva, shortformat) { data = batch.data, consolidated_data = batch.consolidated_data, };
                    var cvaFullDataJson = JsonConvert.SerializeObject(cvaFullData, jsonSettings);
                    cvaFullDataJson = cvaFullDataJson.Replace("/", @"\/");
                    if (!cvaFullDataJson.Trim().StartsWith("["))
                    {
                        cvaFullDataJson = "[" + cvaFullDataJson + "]";
                    }                    

                    DateTime now = DateTime.Now;
                    string ftpUsername = service_cva.pharm_login;
                    string ftpPassword = service_cva.pharm_pwd;
                    string target_url = service_cva.target_url.Trim().EndsWith("/") ? service_cva.target_url.Trim() : (service_cva.target_url.Trim() + "/");
                    string file_name_orig = service_cva.pharm_code.Unidecode() + now.ToString("ddMMyyyyHHmmss") + ".json";
                    string file_name_zip = service_cva.pharm_code.Unidecode() + now.ToString("ddMMyyyyHHmmss") + ".zip";
                    source_file_path_orig = @"C:\Release\Sender\File\" + file_name_orig;
                    source_file_path_zip = @"C:\Release\Sender\File\" + file_name_zip;
                    if (File.Exists(source_file_path_orig))
                    {
                        File.Delete(source_file_path_orig);
                    }
                    if (File.Exists(source_file_path_zip))
                    {
                        File.Delete(source_file_path_zip);
                    }

                    Encoding enc = Encoding.UTF8;
                    File.WriteAllText(source_file_path_orig, cvaFullDataJson, enc);

                    using (var zip = ZipFile.Open(source_file_path_zip, ZipArchiveMode.Create))
                    {
                        zip.CreateEntryFromFile(source_file_path_orig, file_name_orig);
                    }
                    string source_file_path_length = new System.IO.FileInfo(source_file_path_zip).Length.ToString();                    

                    target_address = target_url + file_name_zip;

                    using (WebClient client = new WebClient())
                    {
                        client.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                        client.UploadFile(target_address, "STOR", source_file_path_zip);
                    }

                    service_cva_data_ext.cva_full_data = cvaFullDataJson;
                    service_cva_data_ext.cva_data = null;
                    service_cva_data_ext.cva_cons_data = null;
                    
                    service_cva_data.batch_state = (int)Enums.CvaSendStateEnum.SENT;
                    service_cva_data.sent_date = DateTime.Now;
                    service_cva_data.batch_size = source_file_path_length;
                    service_cva_data.shortformat = shortformat;
                    dbContext.SaveChanges();

                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();

                    if ((!String.IsNullOrEmpty(source_file_path_orig)) && (File.Exists(source_file_path_orig)))
                    {
                        File.Delete(source_file_path_orig);
                    }
                    if ((!String.IsNullOrEmpty(source_file_path_zip)) && (File.Exists(source_file_path_zip)))
                    {
                        File.Delete(source_file_path_zip);
                    }

                    //Loggly.InsertLog_CVA("Отправлен пакет " + service_cva_data.batch_num.ToString() + " от " + service_cva.pharm_code + " на адрес " + target_url + ", размер файла: " + source_file_path_length.ToString() + " байт", (long)Enums.LogEsnType.CVA, "cva", service_cva_data.item_id, DateTime.Now, null);
                    Loggly.InsertLog_CVA("[Отправитель: " + service_cva.pharm_code + "] [Пакет: " + service_cva_data.batch_num.ToString() + "] [Адрес: " + target_url + "] [Байт: " + source_file_path_length.ToString() + "] [Формат: " + (shortformat == 1 ? "краткий" : "полный") + "]", (long)Enums.LogEsnType.CVA, "cva", service_cva_data.item_id, DateTime.Now, null);

                    return new ServiceOperationResult() { Id = 0, };
                }
                catch (Exception e)
                {
                    service_cva_data.batch_state = (int)Enums.CvaSendStateEnum.SENT_ERROR;
                    service_cva_data.mess = GlobalUtil.ExceptionInfo(e);
                    dbContext.SaveChanges();

                    if ((!String.IsNullOrEmpty(source_file_path_orig)) && (File.Exists(source_file_path_orig)))
                    {
                        File.Delete(source_file_path_orig);
                    }
                    if ((!String.IsNullOrEmpty(source_file_path_zip)) && (File.Exists(source_file_path_zip)))
                    {
                        File.Delete(source_file_path_zip);
                    }

                    throw;
                }
            }            
        }

        private CvaSendInfo xCVA_GetSendInfo(UserInfo userInfo)
        {
            int workplace_id = 0;
            var res1 = xCVA_GetLicense(userInfo, out workplace_id);
            //var res1 = xGetLicense(userInfo, SERVICE_ID_CVA, false);
            if (res1.Id > 0)
            {
                return new CvaSendInfo() { error = new ErrInfo(res1.Id, res1.error.Message), };
            }

            using (var dbContext = new AuMainDb())
            {
                vw_service_cva_info vw_service_cva_info = dbContext.vw_service_cva_info.Where(ss => ss.workplace_id == workplace_id).FirstOrDefault();
                if (vw_service_cva_info == null)
                {
                    return new CvaSendInfo() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена информация для логина " + userInfo.login + " и рабочего места " + userInfo.workplace), };
                }

                return new CvaSendInfo() { error = null, cvaInfo = new CvaInfo(vw_service_cva_info), };
            }
        }

    }
}
