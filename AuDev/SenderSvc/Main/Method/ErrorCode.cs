﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace SenderSvc
{
    public partial class SenderService : ISenderService
    {
        private List<ErrorCode> xGetErrorCodeList()
        {
            List<ErrorCode> res = new List<ErrorCode>();
            var errors = GlobalUtil.GetEnumValues<Enums.ErrCodeEnum>();
            foreach (var err in errors)
            {
                res.Add(new ErrorCode() 
                { 
                    Id = (long)err,
                    Name = ((long)err).EnumName<Enums.ErrCodeEnum>(),
                });
            }
            return res;
        }
    }
}
