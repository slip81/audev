﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;
using WinSCP;
using UnidecodeSharpFork;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
#endregion

namespace SenderSvc
{
    public partial class SenderService : ISenderService
    {
        const int SERVICE_ID_RPTSVC = 60;

        private ServiceOperationResult xRPT_GetLicense(UserInfo userInfo, out int workplace_id)
        {
            workplace_id = 0;
            return xGetLicense(userInfo, SERVICE_ID_RPTSVC, false);
            /*
            workplace_id = 0;
            if ((userInfo == null) || (String.IsNullOrEmpty(userInfo.login)) || (String.IsNullOrEmpty(userInfo.workplace)))
            {
                return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Некорректно задан атрибут userInfo"), };
            }

            DateTime today = DateTime.Today;
            using (var dbContext = new AuMainDb())
            {
                var lic = dbContext.vw_license2.Where(ss => ss.service_id == SERVICE_ID_RPTSVC
                    && ss.user_name != null
                    && ss.user_name.Trim().ToLower().Equals(userInfo.login.Trim().ToLower())
                    && ss.registration_key != null
                    && ss.registration_key.Trim().ToLower().Equals(userInfo.workplace.Trim().ToLower())
                    )
                    .FirstOrDefault();
                if ((lic == null) || ((lic.dt_start.HasValue) && (lic.dt_start > today)) || ((lic.dt_end.HasValue) && (lic.dt_end < today)))
                {
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Не найдена лицензия для логина " + userInfo.login.Trim() + " и рабочего места " + userInfo.workplace.Trim()), };
                }

                if (!lic.workplace_id.HasValue)
                {
                    return new ServiceOperationResult() { Id = (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Не найден код рабочего места для лицензии для логина " + userInfo.login.Trim() + " и рабочего места " + userInfo.workplace.Trim()), };
                }

                workplace_id = (int)lic.workplace_id;
                return new ServiceOperationResult() { Id = 0, };
            }
            */
        }
    }
}
