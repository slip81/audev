﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace SenderSvc
{
    public partial class SenderService : ISenderService
    {
        #region CreateErrSenderBase
        public T CreateErrSenderBase<T>(Exception e)
            where T : SenderBase, new()
        {
            //T res = Activator.CreateInstance<T>();
            T res = new T();
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                )
                );
            return res;
        }
        #endregion

        #region License

        public ServiceOperationResult GetLicense(UserInfo userInfo, int service_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetLicense(userInfo, service_id);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_CVA("Ошибка в GetLicense: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "license-checker", null, log_date_beg, null);
                return CreateErrSenderBase<ServiceOperationResult>(e);
            }
        }

        #endregion

        #region SendMail

        public int SendMail_Defect()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xSendMail_Defect();
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Sender("Ошибка в SendMail_Defect: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "sender-defect", null, log_date_beg, null);
                return (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        public int SendMail_Grls()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xSendMail_Grls();
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Sender("Ошибка в SendMail_Grls: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "sender-grls", null, log_date_beg, null);
                return (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        #endregion

        #region CVA

        public ServiceOperationResult CVA_GetLicense(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                int workplace_id = 0;
                return xCVA_GetLicense(userInfo, out workplace_id);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_CVA("Ошибка в CVA_GetLicense: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "cva", null, log_date_beg, null);
                return CreateErrSenderBase<ServiceOperationResult>(e);
            }
        }

        public CvaBatchSendResult CVA_SendBatch(UserInfo userInfo, CvaData batch, int shortformat)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xCVA_SendBatch(userInfo, batch, shortformat);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_CVA("Ошибка в CVA_GetBatch: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "cva", null, log_date_beg, null);
                return CreateErrSenderBase<CvaBatchSendResult>(e);
            }
        }

        public CvaSendInfo CVA_GetSendInfo(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xCVA_GetSendInfo(userInfo);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_CVA("Ошибка в CVA_GetSendInfo: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "cva", null, log_date_beg, null);
                return CreateErrSenderBase<CvaSendInfo>(e);
            }
        }

        #endregion

        #region RPT

        public ServiceOperationResult RPT_GetLicense(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                int workplace_id = 0;
                return xRPT_GetLicense(userInfo, out workplace_id);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_RPT("Ошибка в RPT_GetLicense: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "rptsvc", null, log_date_beg, null);
                return CreateErrSenderBase<ServiceOperationResult>(e);
            }
        }

        #endregion

        #region Exchange

        /*
            [Display(Name = "Товары ЦП")]
            Plan = 4,
            [Display(Name = "Товары по акциям")]
            Discount = 5,
            [Display(Name = "Товары ПР")]
            Recommend = 6,
        */

        public ExchangeData GetExchangeData_Plan(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetExchangeData(userInfo, Enums.ExchangeItemEnum.Plan);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Exchange("Ошибка в GetExchangeData_Plan: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "exchange-helper", null, log_date_beg, null);
                return CreateErrSenderBase<ExchangeData>(e);
            }
        }

        public ExchangeData GetExchangeData_Discount(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetExchangeData(userInfo, Enums.ExchangeItemEnum.Discount);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Exchange("Ошибка в GetExchangeData_Discount: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "exchange-helper", null, log_date_beg, null);
                return CreateErrSenderBase<ExchangeData>(e);
            }
        }

        public ExchangeData GetExchangeData_Recommend(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetExchangeData(userInfo, Enums.ExchangeItemEnum.Recommend);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Exchange("Ошибка в GetExchangeData_Recommend: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "exchange-helper", null, log_date_beg, null);
                return CreateErrSenderBase<ExchangeData>(e);
            }
        }

        #endregion

        #region GetErrorCodeList

        public List<ErrorCode> GetErrorCodeList()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetErrorCodeList();
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Sender("Ошибка в GetErrorCodeList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "cva", null, log_date_beg, null);
                return new List<ErrorCode>();
            }
        }

        #endregion

    }
}
