﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Hosting;
using System.Reflection;
using System.Configuration;
using System.Data.EntityClient;
using System.IO;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Util
{
    public static class ViewModelUtil
    {

        public static string GetAuMainDbConnectionString()
        {            
            string entityConnectionString = ConfigurationManager.ConnectionStrings["AuMainDb"].ConnectionString;
            return new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;
        }

        public static string GetCrmFileStorageLink()
        {
            return "https://cab.aptekaural.ru/PrjFileGet?file_id=";
            //return "http://192.168.0.101:5000/shara/";
            //return "http://server-101/shara/";
        }

        public static string GetCrmFileStoragePath()
        {
            return @"\\192.168.0.101\files";
        }

        public static string GetStorageTmpFilePath()
        {
            return @"C:\Release\Cabinet\File\Storage\Tmp";
        }

        public static string GetStorageFileLink()
        {
            return "https://cab.aptekaural.ru/StorageFileGet?file_id=";
            //return "http://shara.aptekaural.ru/shara/storage/";            
            //return "http://192.168.0.101:5000/shara/storage/";
            //return "http://localhost/shara/storage?file_id=";
        }

        public static string GetStorageArcLink()
        {
            return "http://shara.aptekaural.ru/shara/storage/";
            //return "http://localhost/shara/storage/";
        }

        public static string GetStorageFilePath()
        {
            return @"\\192.168.0.101\files\storage";
            //return @"C:\Release\Cabinet\File\Storage";
        }

    }
}