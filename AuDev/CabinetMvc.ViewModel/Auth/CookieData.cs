﻿using System;

namespace CabinetMvc.Auth
{
    public class CookieData
    {
        public string user_name { get; set; }
        public string business_id { get; set; }
        public string role_id { get; set; }
        public string cab_client_id { get; set; }
        public string exp_date { get; set; }
    }
}