﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Principal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CabinetMvc.ViewModel.User;


namespace CabinetMvc.Auth
{
    public class CustomAuthentication : IAuthentication 
    { 
        //private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        private const string cookieNameNew = "aurit_cab";
        public HttpContext HttpContext { get; set; }

        public UserService userService { get; set; }

        public CustomAuthentication()
        {
            userService = new UserService();
        }         

        public UserViewModel Login(string userName, string Password, bool isPersistent) 
        {
            UserViewModel retUser = userService.Login(userName, Password);
            if (retUser != null) 
            {
                CreateCookie(retUser.UserName, retUser.BusinessId.ToString(), retUser.RoleId.ToString(), retUser.CabClientId.ToString(), isPersistent); 
            } 
            
            return retUser; 
        }

        private void CreateCookie(string user_name, string business_id, string role_id, string cab_client_id, bool isPersistent = false) 
        {
            DateTime now = DateTime.Now;
            DateTime now_with_timeout = now.Add(FormsAuthentication.Timeout);
            
            CookieData cookieData = new CookieData() { 
                user_name = user_name,
                business_id = business_id,
                role_id = role_id,
                cab_client_id = cab_client_id,
                exp_date = now_with_timeout.ToString("dd-MM-yyyy-HH-mm-ss"),
            };
            string cookieStr = JsonConvert.SerializeObject(cookieData);

            //cookieNameNew
            var ticketNew = new FormsAuthenticationTicket(1, cookieStr,
                now, now_with_timeout,
                isPersistent, string.Empty, FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket
            var encTicketNew = FormsAuthentication.Encrypt(ticketNew);
            // Create the cookie
            var AuthCookieNew = new HttpCookie(cookieNameNew)
            {
                Value = encTicketNew,
                Expires = now_with_timeout,
            };

            HttpContext.Response.Cookies.Set(AuthCookieNew);
        }

        public void LogOut() 
        {
            var httpCookieNew = HttpContext.Response.Cookies[cookieNameNew];
            if (httpCookieNew != null)
            {
                httpCookieNew.Value = string.Empty;
            }
        }

        public bool ChangeBusiness(string userName, long business_id, long role_id, long cab_client_id, bool isPersistent) 
        {
            _currentUser = null;
            CreateCookie(userName, business_id.ToString(), role_id.ToString(), cab_client_id.ToString(), isPersistent); 
            return true;
        }

        public bool ChangeRole(string userName, long business_id, long role_id, long cab_client_id)
        {
            if (_currentUser == null)
            {
                return false;
            }

            var new_userName = role_id == 103 ? userName : "d00471"; // Биона
            var new_role_id = role_id == 103 ? 101 : 103; 
            var new_business_id = role_id == 103 ? 0 : 939659093734327298; // Биона
            var new_cab_client_id = role_id == 103 ? 1000 : 169; // Биона

            _currentUser = null;
            CreateCookie(new_userName, new_business_id.ToString(), new_role_id.ToString(), new_cab_client_id.ToString(), true);
            return true;
        }

        private IPrincipal _currentUser;        
        public IPrincipal CurrentUser 
        {
            get 
            { 
                if (_currentUser == null) 
                { 
                    try 
                    {
                        FormsAuthenticationTicket ticketNew;
                        string cookieStr;
                        CookieData cookieData;
                        HttpCookie authCookieNew;

                        authCookieNew = HttpContext.Request.Cookies.Get(cookieNameNew);
                        if ((authCookieNew != null) && (!String.IsNullOrEmpty(authCookieNew.Value)))
                        {
                            ticketNew = FormsAuthentication.Decrypt(authCookieNew.Value);
                            cookieStr = ticketNew.Name;
                            cookieData = JsonConvert.DeserializeObject<CookieData>(cookieStr);
                            var user_name = cookieData.user_name;
                            long business_id = 0;
                            long.TryParse(cookieData.business_id, out business_id);
                            long role_id = 0;
                            long.TryParse(cookieData.role_id, out role_id);
                            long cab_client_id = 0;
                            long.TryParse(cookieData.cab_client_id, out cab_client_id);
                            DateTime now = DateTime.Now;
                            DateTime exp_date = now;
                            DateTime.TryParseExact(cookieData.exp_date, "dd-MM-yyyy-HH-mm-ss", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out exp_date);
                            if (now >= exp_date)
                                throw new HttpException("Закончилось время сессии");
                            // update cookie expire date
                            // !!!
                            if (!(HttpContext.Request.RawUrl.Trim().ToLower().Equals("/isauthorized")))
                                CreateCookie(user_name, business_id.ToString(), role_id.ToString(), cab_client_id.ToString(), ticketNew.IsPersistent);

                            _currentUser = new UserProvider(user_name, business_id, role_id, cab_client_id, userService);
                        }
                    } 
                    catch (Exception ex) 
                    {                         
                        _currentUser = new UserProvider(null, 0, 0, 0, null); 
                    } 
                } 
                return _currentUser; 
            } 
        } 
 
}
}