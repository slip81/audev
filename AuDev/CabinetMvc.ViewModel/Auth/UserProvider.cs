﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using CabinetMvc.ViewModel.User;

namespace CabinetMvc.Auth
{
    public class UserProvider : IPrincipal
    {
        private UserIndentity userIdentity { get; set; }
        
        public IIdentity Identity { get { return userIdentity; } }

        public bool IsInRole(string role) 
        { 
            if (userIdentity.User == null) 
            { 
                return false; 
            } 
            return userIdentity.User.InRoles(role); 
        }

        public UserProvider(string name, long business_id, long role_id, long cabinet_client_id, UserService serv) 
        { 
            userIdentity = new UserIndentity();
            userIdentity.Init(name, business_id, role_id, cabinet_client_id, serv); 
        }

        public override string ToString() 
        { 
            return userIdentity.Name; 
        }
    }
}