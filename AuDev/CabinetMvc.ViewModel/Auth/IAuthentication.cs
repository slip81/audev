﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using CabinetMvc.ViewModel.User;

namespace CabinetMvc.Auth
{
    public interface IAuthentication
    { 
        /// <summary> 
        /// Конекст (тут мы получаем доступ к запросу и кукисам) 
        /// </summary> 
        HttpContext HttpContext { get; set; }
        UserViewModel Login(string login, string password, bool isPersistent);
        //UserViewModel Login(string login);
        void LogOut();
        IPrincipal CurrentUser { get; }
        bool ChangeBusiness(string userName, long business_id, long role_id, long cab_client_id, bool isPersistent);
        bool ChangeRole(string userName, long business_id, long role_id, long cab_client_id);
    }
}