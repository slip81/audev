﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Microsoft.AspNet.SignalR;
//using Microsoft.AspNet.SignalR.Hubs;
//using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.HelpDesk
{

    // http://developer.telerik.com/products/realtime-data-with-signalr-and-websockets/

    // <script src="@Url.Content("~/Scripts/jquery.signalR-2.2.0.min.js")"></script>
    // <script src="@Url.Content("~/signalr/hubs")"></script>

    // app.MapSignalR();

    /*
    public class StoryHub : Hub
    {

        private HelpDeskEdm dbContext;

        public StoryHub()
        {
            dbContext = new HelpDeskEdm();
        }

        public IEnumerable<StoryViewModel> Read()
        {
            return dbContext.story.Select(ss => new StoryViewModel()
            {
                story_id = ss.story_id,
                story_text = ss.story_text,
                story_subject = ss.story_subject,
                org_name = ss.org_name,
                org_address = ss.org_address,
                person_name = ss.person_name,
                person_phone = ss.person_phone,
                person_email = ss.person_email,
                person_id = ss.person_id,
                person_ip = ss.person_ip,
                init_state_id = ss.init_state_id,
                curr_state_id = ss.curr_state_id,
                is_answered = ss.is_answered,
                resp_user = ss.resp_user,
                resp_user_id = ss.resp_user_id,
                crm_num = ss.crm_num,
                crm_id = ss.crm_id,
                crt_date = ss.crt_date,
                crt_user = ss.crt_user,
                upd_user = ss.upd_user,
                upd_date = ss.upd_date,
            }
                );
        }

        public void Update(StoryViewModel item)
        {

            if (!(item is StoryViewModel))
            {
                return;
            }

            StoryViewModel storyEdit = (StoryViewModel)item;
            if (storyEdit == null)
            {
                return;
            }

            story story = dbContext.story.Where(ss => ss.story_id == storyEdit.story_id).FirstOrDefault();
            if (story == null)
            {
                return;
            }

            if (story.is_answered)
            {
                return;
            }

            story_state accepted_state = dbContext.story_state.Where(ss => ss.state_id == (int)Enums.StoryStateEnum.ACCEPTED).FirstOrDefault();
            story.curr_state_id = accepted_state.state_id;
            story.is_answered = accepted_state.is_answered;

            story.resp_user = UserManager.CurrUser.UserName;
            story.resp_user_id = storyEdit.resp_user_id;
            story.crm_num = storyEdit.crm_num;
            story.crm_id = storyEdit.crm_id;

            story.upd_date = DateTime.Now;
            story.upd_user = story.resp_user;

            story_lc curr_story_lc = dbContext.story_lc.Where(ss => ss.story_id == story.story_id && !ss.date_end.HasValue).FirstOrDefault();
            curr_story_lc.date_end = story.upd_date;
            curr_story_lc.upd_date = story.upd_date;
            curr_story_lc.upd_user = story.upd_user;

            story_lc story_lc = new story_lc();
            story_lc.story = story;
            story_lc.state_id = accepted_state.state_id;
            story_lc.date_beg = story.upd_date;
            story_lc.date_end = null;
            story_lc.crt_date = story.upd_date;
            story_lc.crt_user = story.upd_user;
            story_lc.upd_date = null;
            story_lc.upd_user = null;

            dbContext.story_lc.Add(story_lc);

            dbContext.SaveChanges();

            Clients.Others.update(item);
        }
    }
    */

}