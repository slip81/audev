﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.HelpDesk
{
    public class StoryStateViewModel : AuBaseViewModel
    {
        public StoryStateViewModel()
            : base(Enums.MainObjectType.STORY_STATE)
        {
            //
        }

        public StoryStateViewModel(story_state item)
            : base(Enums.MainObjectType.STORY_STATE)
        {
            state_id = item.state_id;
            state_name = item.state_name;
            is_answered = item.is_answered;
        }

        [Key()]
        public long state_id { get; set; }
        public string state_name { get; set; }
        public bool is_answered { get; set; }
    }
}