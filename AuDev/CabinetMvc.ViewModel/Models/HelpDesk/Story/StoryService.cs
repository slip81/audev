﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.HelpDesk
{
    public interface IStoryService : IAuBaseService
    {
        bool DeleteBatch(int del_mode, string id_list);
    }

    public class StoryService : AuBaseService, IStoryService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.story.Select(ss => new StoryViewModel()
            {
                story_id = ss.story_id,
                story_text = ss.story_text,
                story_subject = ss.story_subject,
                org_name = ss.org_name,
                org_address = ss.org_address,
                person_name = ss.person_name,
                person_phone = ss.person_phone,
                person_email = ss.person_email,
                person_id = ss.person_id,
                person_ip = ss.person_ip,
                init_state_id = ss.init_state_id,
                curr_state_id = ss.curr_state_id,
                is_answered = ss.is_answered,
                resp_user = ss.resp_user,
                resp_user_id = ss.resp_user_id,
                crm_num = ss.crm_num,
                crm_id = ss.crm_id,
                crt_date = ss.crt_date,
                crt_user = ss.crt_user,
                upd_user = ss.upd_user,
                upd_date = ss.upd_date,
                version_name = ss.version_name,                
                //task_id = ss.task_id,
                //task_num = ss.task_num,
                claim_id = ss.claim_id,
                claim_num = ss.claim_num,
                ToTask = false,
            }
                );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return new StoryViewModel(dbContext.story.Where(ss => ss.story_id == id).FirstOrDefault());
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StoryViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                //return null;
                return new AuBaseViewModel() { ErrMess = "Некорректные параметры", };
            }

            StoryViewModel storyEdit = (StoryViewModel)item;
            if (storyEdit == null)
            {
                modelState.AddModelError("", "Не задан вопрос");
                //return null;
                return new AuBaseViewModel() { ErrMess = "Не задан вопрос", };
            }

            story story = dbContext.story.Where(ss => ss.story_id == storyEdit.story_id).FirstOrDefault();
            if (story == null)
            {
                modelState.AddModelError("", "Не найден вопрос с кодом" + storyEdit.story_id.ToString());
                //return null;
                return new AuBaseViewModel() { ErrMess = "Не найден вопрос с кодом " + storyEdit.story_id.ToString(), };
            }
            
            if (story.is_answered)
            {
                modelState.AddModelError("", "Вопрос уже принят в работу");
                //return null;
                return new AuBaseViewModel() { ErrMess = "Вопрос уже принят в работу", };
            }                        

            modelBeforeChanges = new StoryViewModel(story);
 
            story_state accepted_state = dbContext.story_state.Where(ss => ss.state_id == (int)Enums.StoryStateEnum.ACCEPTED).FirstOrDefault();
            story.curr_state_id = accepted_state.state_id;
            story.is_answered = accepted_state.is_answered;

            story.resp_user = currUser.UserName;
            story.resp_user_id = storyEdit.resp_user_id;
            story.crm_num = storyEdit.crm_num;
            story.crm_id = storyEdit.crm_id;
            story.version_name = storyEdit.version_name;

            //story.task_id = storyEdit.task_id;
            //story.task_num = storyEdit.task_num;
            story.claim_id = storyEdit.claim_id;
            story.claim_num = storyEdit.claim_num;
            
            story.upd_date = DateTime.Now;
            story.upd_user = story.resp_user;

            story_lc curr_story_lc = dbContext.story_lc.Where(ss => ss.story_id == story.story_id && !ss.date_end.HasValue).FirstOrDefault();
            curr_story_lc.date_end = story.upd_date;
            curr_story_lc.upd_date = story.upd_date;
            curr_story_lc.upd_user = story.upd_user;

            story_lc story_lc = new story_lc();
            story_lc.story = story;
            story_lc.state_id = accepted_state.state_id;
            story_lc.date_beg = story.upd_date;
            story_lc.date_end = null;
            story_lc.crt_date = story.upd_date;
            story_lc.crt_user = story.upd_user;
            story_lc.upd_date = null;
            story_lc.upd_user = null;

            dbContext.story_lc.Add(story_lc);

            dbContext.SaveChanges();

            return new StoryViewModel(story);
        }

        public bool DeleteBatch(int del_mode, string id_list)
        {
            switch (del_mode)
            {
                case 1:
                    if (String.IsNullOrEmpty(id_list))
                        return false;
                    if (id_list.Trim().EndsWith(","))
                        id_list = id_list.Trim().Remove(id_list.Trim().Length - 1);
                    List<long> id_list_long = id_list.Split(',').Select(long.Parse).ToList();
                    if ((id_list_long == null) || (id_list_long.Count <= 0))
                        return false;
                    dbContext.story_lc.RemoveRange(dbContext.story_lc.Where(ss => id_list_long.Contains(ss.story_id)));
                    dbContext.story.RemoveRange(dbContext.story.Where(ss => id_list_long.Contains(ss.story_id)));
                    dbContext.SaveChanges();
                    return true;
                case 2:
                    dbContext.story_lc.RemoveRange(dbContext.story_lc);
                    dbContext.story.RemoveRange(dbContext.story);
                    dbContext.SaveChanges();
                    return true;
                default:
                    return false;
            }
        }
       
    }
}