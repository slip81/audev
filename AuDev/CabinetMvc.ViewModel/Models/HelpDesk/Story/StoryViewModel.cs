﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.HelpDesk
{
    public class StoryViewModel : AuBaseViewModel
    {
        public StoryViewModel()
            : base(Enums.MainObjectType.STORY)
        {
            //
        }

        public StoryViewModel(story item)
            : base(Enums.MainObjectType.STORY)
        {
            story_id = item.story_id;
            story_text = item.story_text;
            story_subject = item.story_subject;
            org_name = item.org_name;
            org_address = item.org_address;
            person_name = item.person_name;
            person_phone = item.person_phone;
            person_email = item.person_email;
            person_id = item.person_id;
            person_ip = item.person_ip;
            init_state_id = item.init_state_id;
            curr_state_id = item.curr_state_id;
            is_answered = item.is_answered;
            resp_user = item.resp_user;
            resp_user_id = item.resp_user_id;
            crm_num = item.crm_num;
            crm_id = item.crm_id;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            upd_user = item.upd_user;
            upd_date = item.upd_date;
            version_name = item.version_name;
            //task_id = item.task_id;
            //task_num = item.task_num;
            claim_id = item.claim_id;
            claim_num = item.claim_num;
            ToTask = false;
        }

        [Key()]
        public long story_id { get; set; }
        public string story_text { get; set; }
        public string story_subject { get; set; }
        public string org_name { get; set; }
        public string org_address { get; set; }
        public string person_name { get; set; }
        public string person_phone { get; set; }
        public string person_email { get; set; }
        public Nullable<long> person_id { get; set; }
        public string person_ip { get; set; }
        public long init_state_id { get; set; }
        public long curr_state_id { get; set; }
        public bool is_answered { get; set; }
        public string resp_user { get; set; }
        public Nullable<long> resp_user_id { get; set; }
        public Nullable<int> crm_num { get; set; }
        public Nullable<int> crm_id { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public string version_name { get; set; }
        //public Nullable<int> task_id { get; set; }
        //public string task_num { get; set; }
        public Nullable<int> claim_id { get; set; }
        public string claim_num { get; set; }

        public bool ToTask { get; set; }

        public string upd_date_str
        {
            get
            {
                //return ((upd_date.HasValue) && (crt_date.HasValue)) ? ((DateTime)upd_date).ToString() + " / " + ((TimeSpan)(((DateTime)upd_date).Subtract((DateTime)crt_date))).Minutes.ToString() + " мин" : "";
                return ((upd_date.HasValue) && (crt_date.HasValue)) ? (((DateTime)upd_date).ToString() + " / " + GlobalUtil.DifffBetweenDates(crt_date, upd_date)) : "";
            }
        }

        [JsonIgnore()]
        public string curr_state_name
        {
            get
            {
                switch ((Enums.StoryStateEnum)curr_state_id)
                {
                    case Enums.StoryStateEnum.WAITING:
                        return "В очереди";
                    case Enums.StoryStateEnum.ACCEPTED:
                        return "Принят";
                    default:
                        return "-";
                }
            }
            set
            {
                _curr_state_name = value;
            }
        }
        private string _curr_state_name;

        [JsonIgnore()]
        public string init_state_name
        {
            get
            {
                switch ((Enums.StoryStateEnum)init_state_id)
                {
                    case Enums.StoryStateEnum.WAITING:
                        return "В очереди";
                    case Enums.StoryStateEnum.ACCEPTED:
                        return "Принят";
                    default:
                        return "-";
                }
            }
            set
            {
                _init_state_name = value;
            }
        }
        private string _init_state_name;

        /*
        [JsonIgnore()]
        public string task_id_str
        {
            get
            {
                return task_id.GetValueOrDefault(0) > 0 ? task_num : "";
            }
        }
        */
        
        
    }
}