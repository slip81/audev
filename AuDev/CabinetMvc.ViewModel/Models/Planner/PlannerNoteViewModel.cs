﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Planner
{
    public class PlannerNoteViewModel : AuBaseViewModel
    {
        public PlannerNoteViewModel()
            : base(Enums.MainObjectType.PLANNER_NOTE)
        {
            //vw_planner_note
        }

        // Db

        [Key()]
        [Display(Name = "Код")]
        public int note_id { get; set; }

        [Display(Name = "Описание")]
        [Required(ErrorMessage="Не задан текст")]
        public string description { get; set; }

        [Display(Name = "Заголовок")]
        public string title { get; set; }

        [Display(Name = "Ход выполнения")]
        public string progress { get; set; }

        [Display(Name = "Результат")]
        public string result { get; set; }

        [Display(Name = "Владелец")]
        public Nullable<int> exec_user_id { get; set; }

        [Display(Name = "Опубликовано")]
        public bool is_public { get; set; }

        [Display(Name = "Опубликовано для всех")]
        public bool is_public_global { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]
        public string crt_user { get; set; }

        [Display(Name = "Дата изменения")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Автор изменения")]
        public string upd_user { get; set; }

        [Display(Name = "Порядок")]
        public Nullable<int> sort_num { get; set; }

        [Display(Name = "trigger_flag")]
        public bool trigger_flag { get; set; }

        [Display(Name = "Задача")]
        public Nullable<int> task_id { get; set; }

        [Display(Name = "Задача")]
        public string task_num { get; set; }

        [Display(Name = "Статус")]
        public int event_state_id { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> state_id { get; set; }

        [Display(Name = "Статус")]
        public string state_name { get; set; }

        // Other
        [Display(Name = "Из будующих")]
        public int? SourceUpcomingEventId { get; set; }


        [Display(Name = "Личное")]
        public bool IsPrivate
        {
            get
            {
                return !is_public;
            }

            set
            {
                is_public = !value;
            }
        }
    }
}