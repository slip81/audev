﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Planner
{
    public class PlannerEventViewModel : AuBaseViewModel, Kendo.Mvc.UI.ISchedulerEvent
    {
        public PlannerEventViewModel()
            : base(Enums.MainObjectType.PLANNER_EVENT)
        {
            //vw_planner_event
        }

        // Db
        [Key()]
        [Display(Name = "Код")]
        public int event_id { get; set; }

        [Display(Name = "Владелец")]
        public Nullable<int> exec_user_id { get; set; }

        [Display(Name = "Публиковать")]
        public bool is_public { get; set; }

        [Display(Name = "Событие в календаре")]
        public Nullable<int> crm_event_id { get; set; }

        [Display(Name = "Приоритет")]        
        public int event_priority_id { get; set; }

        [Display(Name = "Вес")]
        public Nullable<int> rate { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]
        public string crt_user { get; set; }

        [Display(Name = "Дата изменения")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Автор изменения")]
        public string upd_user { get; set; }
        
        [Display(Name = "На весь день")]
        public string is_all_day_str { get; set; }

        [Display(Name = "Опубликовано")]
        public string is_public_str { get; set; }

        [Display(Name = "Опубликовано для всех")]
        public bool is_public_global { get; set; }

        [Display(Name = "Приоритет")]
        public string event_priority_name { get; set; }

        [Display(Name = "Владелец")]
        public string exec_user_name { get; set; }

        [Display(Name = "Задача")]
        public Nullable<int> task_id { get; set; }

        [Display(Name = "Дата начала")]
        public Nullable<System.DateTime> date_beg_date_only { get; set; }

        [Display(Name = "Дата окончания")]
        public Nullable<System.DateTime> date_end_date_only { get; set; }

        [Display(Name = "Событие")]
        public string event_num { get; set; }

        [Display(Name = "Задача")]
        public string task_num { get; set; }

        [Display(Name = "Группа задачи")]
        public Nullable<int> group_id { get; set; }

        [Display(Name = "Статус задачи")]
        public string state_name { get; set; }

        [Display(Name = "Группа задачи")]
        public string group_name { get; set; }

        [Display(Name = "Статус")]
        public int event_state_id { get; set; }

        [Display(Name = "Ход выполнения")]
        public string progress { get; set; }

        [Display(Name = "Результат выполнения")]
        public string result { get; set; }

        [Display(Name = "Группа")]
        public int event_group_id { get; set; }

        [Display(Name = "Группа")]
        public string event_group_name { get; set; }

        [Display(Name = "Просроченные")]
        public bool is_overdue { get; set; }

        [Display(Name = "Приоритет")]
        public Nullable<int> priority_id { get; set; }

        [Display(Name = "Приоритет руководителя")]
        public Nullable<bool> priority_is_boss { get; set; }

        [Display(Name = "Сделано")]
        public Nullable<bool> is_done { get; set; }

        [Display(Name = "Контроль дат")]
        public Nullable<bool> is_control { get; set; }

        [Display(Name = "event_date_beg_orig")]
        public Nullable<System.DateTime> event_date_beg_orig { get; set; }

        [Display(Name = "event_date_end_orig")]
        public Nullable<System.DateTime> event_date_end_orig { get; set; }

        [Display(Name = "event_date_beg_curr")]
        public Nullable<System.DateTime> event_date_beg_curr { get; set; }

        [Display(Name = "event_date_end_curr")]
        public Nullable<System.DateTime> event_date_end_curr { get; set; }

        [Display(Name = "Исполнитель")]
        public Nullable<int> task_exec_user_id { get; set; }

        [Display(Name = "Исполнитель")]
        public string task_exec_user_name { get; set; }

        // ISchedulerEvent        
        [AllowHtml()]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Дата окончания")]
        public DateTime End { get; set; }
        [Display(Name = "EndTimezone")]
        public string EndTimezone { get; set; }
        [Display(Name = "На весь день")]
        public bool IsAllDay { get; set; }
        [Display(Name = "RecurrenceException")]
        public string RecurrenceException { get; set; }
        [Display(Name = "Повторять")]
        public string RecurrenceRule { get; set; }
        [Display(Name = "RecurrenceId")]
        public int? RecurrenceId { get; set; }
        [Display(Name = "Дата начала")]
        public DateTime Start { get; set; }
        [Display(Name = "StartTimezone")]
        public string StartTimezone { get; set; }
        [AllowHtml()]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }

        [Display(Name = "Из записей")]
        public int? SourceNoteId { get; set; }

        [Display(Name = "Из будующих")]
        public int? SourceUpcomingEventId { get; set; }

        [Display(Name = "Из просроченных")]
        public int? SourceOverdueEventId { get; set; }        

        [Display(Name = "Из стратегических")]
        public int? SourceStrategEventId { get; set; }                

        [Display(Name = "Личное")]
        public bool IsPrivate { 
            get
            {
                return !is_public;
            }

            set
            {
                is_public = !value;
            }
        }
    }
}