﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using CabinetMvc.ViewModel.Crm;

namespace CabinetMvc.ViewModel.Planner
{
    public interface IPlannerEventService : IAuBaseService
    {        
        IQueryable<PlannerEventViewModel> GetList_byFilter(DateTime date_beg, DateTime date_end, int? exec_user_id, int public_filter_mode);
        IQueryable<PlannerEventViewModel> GetList_Overdue(int? exec_user_id);
        IQueryable<PlannerEventViewModel> GetList_Strateg(int? exec_user_id, bool is_active);
        bool UpcomingEventAdd(int? note_id, int? event_id, int? task_id, bool fromNotes, bool fromScheduler, bool fromTask, DateTime? upcoming_event_date, int? days_add, int? exec_user_id, ModelStateDictionary modelState);
        List<int> GetPlannerEventExecUserList(DateTime date_beg, DateTime date_end, int? exec_user_id, int public_filter_mode);
        IQueryable<PlannerEventViewModel> GetUpcomingEventList(DateTime date_beg, DateTime date_end, int? exec_user_id, int public_filter_mode);
    }

    public class PlannerEventService : AuBaseService, IPlannerEventService
    {
        ICrmLogService logService;

        #region Get

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_planner_event
                .Where(ss => ss.exec_user_id == parent_id)
                .ProjectTo<PlannerEventViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_planner_event
                .Where(ss => ss.event_id == id)
                .ProjectTo<PlannerEventViewModel>()
                .FirstOrDefault()
                ;
        }

        public IQueryable<PlannerEventViewModel> GetList_byFilter(DateTime date_beg, DateTime date_end, int? exec_user_id, int public_filter_mode)
        {
            
            // при изменении см. также crmTaskService.GetList_forPlannerExport

            int curr_user_id = currUser.CabUserId;
            DateTime date_end_actual = date_end.AddDays(1);
            Enums.PlannerEventPublicFilterModeEnum public_filter_mode_enum = (Enums.PlannerEventPublicFilterModeEnum)public_filter_mode;
            return dbContext.vw_planner_event
                    .Where(ss => 
                        //((ss.date_beg >= date_beg && ss.date_beg < date_end_actual) || (ss.date_end >= date_beg && ss.date_end < date_end_actual))
                        ((date_beg >= ss.date_beg && date_beg <= ss.date_end) || (date_end_actual >= ss.date_beg && date_end_actual <= ss.date_end) || (ss.date_beg >= date_beg && ss.date_beg < date_end_actual) || (ss.date_end >= date_beg && ss.date_end < date_end_actual))
                        && (
                        ((exec_user_id == curr_user_id) && (ss.exec_user_id == exec_user_id))
                        || ((exec_user_id != curr_user_id) && (ss.exec_user_id == exec_user_id) && (ss.is_public))
                        || ((exec_user_id == null) && (ss.is_public))
                        || ((exec_user_id == null) && (ss.exec_user_id == curr_user_id))
                        || ((public_filter_mode_enum == Enums.PlannerEventPublicFilterModeEnum.ALL) && (ss.is_public_global))
                        )
                        )
                    .ProjectTo<PlannerEventViewModel>()                    
                    ;
        }

        public IQueryable<PlannerEventViewModel> GetUpcomingEventList(DateTime date_beg, DateTime date_end, int? exec_user_id, int public_filter_mode)
        {
            int curr_user_id = currUser.CabUserId;
            DateTime date_end_actual = date_end.AddDays(1);
            Enums.PlannerEventPublicFilterModeEnum public_filter_mode_enum = (Enums.PlannerEventPublicFilterModeEnum)public_filter_mode;
            return dbContext.vw_planner_event
                    .Where(ss =>
                        (ss.date_beg >= date_beg && ss.date_beg < date_end_actual)
                        && (
                        ((exec_user_id == curr_user_id) && (ss.exec_user_id == exec_user_id))
                        || ((exec_user_id != curr_user_id) && (ss.exec_user_id == exec_user_id) && (ss.is_public))
                        || ((exec_user_id == null) && (ss.is_public))
                        || ((exec_user_id == null) && (ss.exec_user_id == curr_user_id))
                        || ((public_filter_mode_enum == Enums.PlannerEventPublicFilterModeEnum.ALL) && (ss.is_public_global))
                        )
                        )
                    .ProjectTo<PlannerEventViewModel>()
                    ;
        }

        public IQueryable<PlannerEventViewModel> GetList_Overdue(int? exec_user_id)
        {
            return dbContext.vw_planner_event
                .Where(ss => ((ss.exec_user_id == exec_user_id) || (exec_user_id == null))
                    && ss.is_overdue
                    )
                .ProjectTo<PlannerEventViewModel>();
        }

        public IQueryable<PlannerEventViewModel> GetList_Strateg(int? exec_user_id, bool is_active)
        {
            return dbContext.vw_planner_event
                .Where(ss => ((ss.exec_user_id == exec_user_id) || (exec_user_id == null))
                    && ss.priority_is_boss == true
                    && (((ss.is_done != true) && (is_active)) || (!is_active))
                    )
                .ProjectTo<PlannerEventViewModel>();
        }

        public List<int> GetPlannerEventExecUserList(DateTime date_beg, DateTime date_end, int? exec_user_id, int public_filter_mode)
        {
            return GetList_byFilter(date_beg, date_end, exec_user_id, public_filter_mode)
                .Where(ss => ss.exec_user_id.HasValue)
                .Select(ss => (int)ss.exec_user_id)
                .Distinct()
                .ToList();            
        }

        #endregion

        #region Edit

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PlannerEventViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PlannerEventViewModel itemAdd = (PlannerEventViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты события");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            itemAdd.is_public_global = itemAdd.is_public ? itemAdd.is_public_global : false;

            bool existingPlannerEvent = false;
            planner_event planner_event = new planner_event();
            planner_task_event planner_task_event = null;
            crm_task crm_task = null;
            crm_task_control crm_task_control = null;
            string mess_log = "";
            List<string> log_mess_list = new List<string>();
            cab_user cab_user = null;
            
            int sourceEventId = itemAdd.SourceUpcomingEventId.GetValueOrDefault(itemAdd.SourceOverdueEventId.GetValueOrDefault(itemAdd.SourceStrategEventId.GetValueOrDefault(0)));
            if (sourceEventId > 0)
            {
                planner_event = dbContext.planner_event.Where(ss => ss.event_id == sourceEventId).FirstOrDefault();
                if (planner_event == null)
                {
                    modelState.AddModelError("", "Не найдено исходное событие c кодом " + sourceEventId.ToString());
                    return null;
                }
                existingPlannerEvent = true;
            }

            if (!existingPlannerEvent)
            {
                itemAdd.crt_date = now;
                itemAdd.crt_user = currUser.CabUserName;
                itemAdd.upd_date = now;
                itemAdd.upd_user = currUser.CabUserName;
                itemAdd.event_group_id = (int)Enums.CrmEventGroupEnum.NORMAL;
                //itemAdd.exec_user_id = currUser.CabUserId;
                ModelMapper.Map<PlannerEventViewModel, planner_event>(itemAdd, planner_event);
                //
                if (itemAdd.task_id.GetValueOrDefault(0) > 0)
                {
                    if (cab_user == null)
                        cab_user = dbContext.cab_user.Where(ss => ss.user_id == planner_event.exec_user_id).FirstOrDefault();
                    mess_log = "Отправка в календарь для " + cab_user.user_name + " на " + itemAdd.Start.ToString("dd.MM.yyyy") + (itemAdd.Start != itemAdd.End ? (" - " + itemAdd.End.ToString("dd.MM.yyyy")) : "");
                    log_mess_list.Add(mess_log);
                }
            }            
            else
            {
                if (itemAdd.task_id.GetValueOrDefault(0) > 0)
                {
                    if (cab_user == null)
                        cab_user = dbContext.cab_user.Where(ss => ss.user_id == planner_event.exec_user_id).FirstOrDefault();
                    mess_log = "Смена дат в календаре для " + cab_user.user_name + " с " + planner_event.date_beg.ToString("dd.MM.yyyy") + (planner_event.date_beg != planner_event.date_end ? (" - " + planner_event.date_end.ToString("dd.MM.yyyy")) : "")
                        + " на "
                        + itemAdd.Start.ToString("dd.MM.yyyy") + (itemAdd.Start != itemAdd.End ? (" - " + itemAdd.End.ToString("dd.MM.yyyy")) : "")
                        ;
                    log_mess_list.Add(mess_log);
                }
                //
                planner_event.date_beg = itemAdd.Start;
                planner_event.date_end = itemAdd.End;
            }

            if (!existingPlannerEvent)
            {
                dbContext.planner_event.Add(planner_event);
                if (planner_event.task_id.GetValueOrDefault(0) > 0)
                {
                    planner_task_event = new planner_task_event();
                    planner_task_event.planner_event = planner_event;
                    planner_task_event.task_id = (int)planner_event.task_id;
                    dbContext.planner_task_event.Add(planner_task_event);
                }
            }

            dbContext.SaveChanges();

            if (itemAdd.SourceNoteId.GetValueOrDefault(0) > 0)
            {
                dbContext.planner_note.Remove(dbContext.planner_note.Where(ss => ss.note_id == itemAdd.SourceNoteId).FirstOrDefault());
                dbContext.SaveChanges();
            }

            if (planner_event.task_id.GetValueOrDefault(0) > 0)
            {
                crm_task = (from t1 in dbContext.planner_task_event
                            from t2 in dbContext.crm_task
                            where t1.task_id == t2.task_id
                            && t1.event_id == planner_event.event_id
                            && t1.is_actual
                            select t2)
                            .FirstOrDefault();
                if ((crm_task != null) && (crm_task.is_control))
                {
                    crm_task_control = dbContext.crm_task_control.Where(ss => ss.task_id == planner_event.task_id && !ss.is_orig).FirstOrDefault();
                    if (crm_task_control != null)
                    {
                        crm_task_control.event_date_beg = planner_event.date_beg;
                        crm_task_control.event_date_end = planner_event.date_end;
                        crm_task_control.crt_date = now;
                        crm_task_control.crt_user = currUser.UserName;
                        dbContext.SaveChanges();
                    }
                }

                if ((crm_task != null) && (log_mess_list.Count > 0))
                {
                    foreach (var log_mess in log_mess_list)
                    {
                        ToLog_CrmTask(now, log_mess, crm_task.task_id);
                    }
                }
            }

            return GetItem(planner_event.event_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PlannerEventViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PlannerEventViewModel itemEdit = (PlannerEventViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры события");
                return null;
            }

            planner_event planner_event = dbContext.planner_event.Where(ss => ss.event_id == itemEdit.event_id).FirstOrDefault();
            if (planner_event == null)
            {
                modelState.AddModelError("", "Не найдено событие");
                return null;
            }

            string mess_log = "";
            List<string> log_mess_list = new List<string>();
            cab_user cab_user = null;

            if (planner_event.task_id.GetValueOrDefault(0) > 0)
            {
                if (cab_user == null)
                    cab_user = dbContext.cab_user.Where(ss => ss.user_id == planner_event.exec_user_id).FirstOrDefault();
                mess_log = "Смена дат в календаре для " + cab_user.user_name + " с " + planner_event.date_beg.ToString("dd.MM.yyyy") + (planner_event.date_beg != planner_event.date_end ? (" - " + planner_event.date_end.ToString("dd.MM.yyyy")) : "")
                    + " на "
                    + itemEdit.Start.ToString("dd.MM.yyyy") + (itemEdit.Start != itemEdit.End ? (" - " + itemEdit.End.ToString("dd.MM.yyyy")) : "")
                    ;
                log_mess_list.Add(mess_log);
            }

            itemEdit.is_public_global = itemEdit.is_public ? itemEdit.is_public_global : false;

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            crm_task crm_task = null;
            crm_task_control crm_task_control = null;

            PlannerEventViewModel oldModel = new PlannerEventViewModel();
            ModelMapper.Map<planner_event, PlannerEventViewModel>(planner_event, oldModel);
            modelBeforeChanges = oldModel;

            itemEdit.upd_date = now;
            itemEdit.upd_user = currUser.CabUserName;
            itemEdit.event_group_id = (int)Enums.CrmEventGroupEnum.NORMAL;            

            ModelMapper.Map<PlannerEventViewModel, planner_event>(itemEdit, planner_event);

            dbContext.SaveChanges();

            if (planner_event.task_id.GetValueOrDefault(0) > 0)
            {
                crm_task = (from t1 in dbContext.planner_task_event
                            from t2 in dbContext.crm_task
                            where t1.task_id == t2.task_id
                            && t1.event_id == planner_event.event_id
                            && t1.is_actual
                            select t2)
                            .FirstOrDefault();
                if ((crm_task != null) && (crm_task.is_control))
                {
                    crm_task_control = dbContext.crm_task_control.Where(ss => ss.task_id == planner_event.task_id && !ss.is_orig).FirstOrDefault();
                    if (crm_task_control != null)
                    {
                        crm_task_control.event_date_beg = planner_event.date_beg;
                        crm_task_control.event_date_end = planner_event.date_end;
                        crm_task_control.crt_date = now;
                        crm_task_control.crt_user = currUser.UserName;
                        dbContext.SaveChanges();
                    }
                }

                if ((crm_task != null) && (log_mess_list.Count > 0))
                {
                    foreach (var log_mess in log_mess_list)
                    {
                        ToLog_CrmTask(now, log_mess, crm_task.task_id);
                    }
                }
            }

            return GetItem(planner_event.event_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PlannerEventViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            PlannerEventViewModel itemDel = (PlannerEventViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты события");
                return false;
            }

            planner_event planner_event = dbContext.planner_event.Where(ss => ss.event_id == itemDel.event_id).FirstOrDefault();
            if (planner_event == null)
            {
                modelState.AddModelError("", "Не найдено событие");
                return false;
            }

            string mess_log = "";
            List<string> log_mess_list = new List<string>();
            cab_user cab_user = null;

            if (planner_event.task_id.GetValueOrDefault(0) > 0)
            {
                if (cab_user == null)
                    cab_user = dbContext.cab_user.Where(ss => ss.user_id == planner_event.exec_user_id).FirstOrDefault();
                mess_log = "Удаление из календаря у " + cab_user.user_name;
                log_mess_list.Add(mess_log);
            }

            crm_task crm_task = null;
            crm_task_control crm_task_control = null;
            DateTime now = DateTime.Now;

            if (planner_event.task_id.GetValueOrDefault(0) > 0)
            {
                crm_task = (from t1 in dbContext.planner_task_event
                            from t2 in dbContext.crm_task
                            where t1.task_id == t2.task_id
                            && t1.event_id == planner_event.event_id
                            && t1.is_actual
                            select t2)
                            .FirstOrDefault();
                if ((crm_task != null) && (crm_task.is_control))
                {
                    crm_task_control = dbContext.crm_task_control.Where(ss => ss.task_id == planner_event.task_id && !ss.is_orig).FirstOrDefault();
                    if (crm_task_control != null)
                    {
                        crm_task_control.event_date_beg = null;
                        crm_task_control.event_date_end = null;
                        crm_task_control.crt_date = now;
                        crm_task_control.crt_user = currUser.UserName;
                        dbContext.SaveChanges();
                    }
                }

                if ((crm_task != null) && (log_mess_list.Count > 0))
                {
                    foreach (var log_mess in log_mess_list)
                    {
                        ToLog_CrmTask(now, log_mess, crm_task.task_id);
                    }
                }
            }

            dbContext.planner_task_event.RemoveRange(dbContext.planner_task_event.Where(ss => ss.event_id == planner_event.event_id));
            dbContext.planner_event.Remove(planner_event);
            dbContext.SaveChanges();

            return true;
        }

        #endregion

        #region UpcomingEvent

        public bool UpcomingEventAdd(int? note_id, int? event_id, int? task_id, bool fromNotes, bool fromScheduler, bool fromTask, DateTime? upcoming_event_date, int? days_add, int? exec_user_id, ModelStateDictionary modelState)
        {
            if (fromNotes)
            {
                return upcomingEventAdd_fromNotes(note_id, upcoming_event_date, days_add, exec_user_id, modelState);
            }
            else if (fromScheduler)
            {
                return upcomingEventAdd_fromScheduler(event_id, upcoming_event_date, days_add, exec_user_id, modelState);
            }
            else if (fromTask)
            {
                return upcomingEventAdd_fromTask(task_id, upcoming_event_date, days_add, exec_user_id, modelState);
            }
            else
            {
                modelState.AddModelError("", "Не задан источник создания события");
                return false;
            }
        }

        private bool upcomingEventAdd_fromNotes(int? note_id, DateTime? upcoming_event_date, int? days_add, int? exec_user_id, ModelStateDictionary modelState)
        {
            try
            {
                planner_note planner_note = dbContext.planner_note.Where(ss => ss.note_id == note_id).FirstOrDefault();
                if (planner_note == null)
                {
                    modelState.AddModelError("", "Не найдена запись с кодом " + note_id.ToString());
                    return false;
                }

                DateTime date_beg = upcoming_event_date.HasValue ? ((DateTime)upcoming_event_date).Date : DateTime.Today.AddDays(days_add.GetValueOrDefault(1)).Date;
                DateTime date_end = date_beg;

                PlannerEventViewModel plannerEvent = new PlannerEventViewModel()
                {
                    Description = planner_note.description,
                    End = date_end,
                    event_group_id = (int)Enums.CrmEventGroupEnum.NORMAL,
                    event_priority_id = 0,
                    event_state_id = planner_note.event_state_id,
                    exec_user_id = exec_user_id,
                    is_public = planner_note.is_public,
                    is_public_global = planner_note.is_public_global,
                    IsAllDay = true,                    
                    SourceNoteId = planner_note.note_id,
                    Start = date_beg,
                    task_id = planner_note.task_id,
                    Title = planner_note.title,
                    progress = planner_note.progress,
                    result = planner_note.result,
                };

                var res = Insert(plannerEvent, modelState);
                if ((res == null) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при добавлении события");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        private bool upcomingEventAdd_fromScheduler(int? event_id, DateTime? upcoming_event_date, int? days_add, int? exec_user_id, ModelStateDictionary modelState)
        {
            try
            {
                planner_event planner_event = dbContext.planner_event.Where(ss => ss.event_id == event_id).FirstOrDefault();
                if (planner_event == null)
                {
                    modelState.AddModelError("", "Не найдено событие с кодом " + event_id.ToString());
                    return false;
                }

                DateTime date_beg = upcoming_event_date.HasValue ? ((DateTime)upcoming_event_date).Date : DateTime.Today.AddDays(days_add.GetValueOrDefault(1)).Date;
                DateTime date_end = date_beg;

                if ((planner_event.date_beg == date_beg) && (planner_event.date_end == date_end))
                {
                    return true;
                }

                PlannerEventViewModel plannerEvent = new PlannerEventViewModel();
                ModelMapper.Map<planner_event, PlannerEventViewModel>(planner_event, plannerEvent);
                plannerEvent.Start = date_beg;
                plannerEvent.End = date_end;
                plannerEvent.IsAllDay = true;
                plannerEvent.exec_user_id = exec_user_id;
               
                var res = Update(plannerEvent, modelState);
                if ((res == null) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при переносе события");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        private bool upcomingEventAdd_fromTask(int? task_id, DateTime? upcoming_event_date, int? days_add, int? exec_user_id, ModelStateDictionary modelState)
        {
            try
            {
                crm_task crm_task = dbContext.crm_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
                if (crm_task == null)
                {
                    modelState.AddModelError("", "Не найдена задача с кодом " + task_id.ToString());
                    return false;
                }

                DateTime date_beg = upcoming_event_date.HasValue ? ((DateTime)upcoming_event_date).Date : DateTime.Today.AddDays(days_add.GetValueOrDefault(1)).Date;
                DateTime date_end = date_beg;

                PlannerEventViewModel plannerEvent = new PlannerEventViewModel()
                {
                    Description = crm_task.task_text,
                    End = date_end,
                    event_group_id = (int)Enums.CrmEventGroupEnum.NORMAL,
                    event_priority_id = 0,
                    event_state_id = 1,
                    exec_user_id = exec_user_id,
                    is_public = true,
                    is_public_global = false,
                    IsAllDay = true,                    
                    SourceNoteId = null,
                    Start = date_beg,
                    task_id = crm_task.task_id,
                    Title = crm_task.task_name,
                    progress = null,
                    result = null,
                };

                var res = Insert(plannerEvent, modelState);
                if ((res == null) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при добавлении события");
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region Log_CrmTask

        private void ToLog_CrmTask(DateTime date_beg, string mess, int? task_id)
        {
            if (logService == null)
                logService = DependencyResolver.Current.GetService<ICrmLogService>();
            if (logService == null)
                throw new Exception("logService == null");
            //
            logService.InsertLogCrm(date_beg, mess, task_id);
        }

        #endregion
    }
}
