﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Planner
{
    public interface IPlannerNoteService : IAuBaseService
    {
        void PlannerNoteReorder(int note_id, int new_sort);
        bool Delete_byId(int note_id, ModelStateDictionary modelState);
        bool PlannerNoteAddTasks(string task_id_list, int? curr_user_id, ModelStateDictionary modelState);
    }

    public class PlannerNoteService : AuBaseService, IPlannerNoteService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_planner_note
                .ProjectTo<PlannerNoteViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            bool isPublicOnly = false;
            int cabinet_user_id = currUser.CabUserId;
            bool isBoss = currUser.IsBoss;
            if ((cabinet_user_id != parent_id) && (!isBoss))
            {
                isPublicOnly = true;
            }

            return dbContext.vw_planner_note
                .Where(ss => (ss.exec_user_id == parent_id) 
                    //&& (((isPublicOnly) && (ss.is_public)) || (!isPublicOnly))
                    && (((isPublicOnly) && (1 == 2)) || (!isPublicOnly))
                )
                .ProjectTo<PlannerNoteViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_planner_note
                .Where(ss => ss.note_id == id)
                .ProjectTo<PlannerNoteViewModel>()
                .FirstOrDefault()
                ;
        }
        
        public void PlannerNoteReorder(int note_id, int new_sort)
        {
            planner_note planner_note = dbContext.planner_note.Where(ss => ss.note_id == note_id).FirstOrDefault();
            if (planner_note == null)
                return;
            planner_note planner_note_last = dbContext.planner_note.Where(ss => ss.exec_user_id == planner_note.exec_user_id).OrderByDescending(ss => ss.sort_num).FirstOrDefault();
            if (planner_note_last == null)
                return;

            planner_note.sort_num = planner_note_last.sort_num - new_sort;
            planner_note.trigger_flag = !planner_note.trigger_flag;
            dbContext.SaveChanges();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PlannerNoteViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PlannerNoteViewModel itemAdd = (PlannerNoteViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты записи");
                return null;
            }

            if (itemAdd.task_id.GetValueOrDefault(0) > 0)
            {
                var existing1 = dbContext.planner_note.Where(ss => ss.exec_user_id == itemAdd.exec_user_id && ss.task_id == itemAdd.task_id).FirstOrDefault();
                if (existing1 != null)
                {
                    modelState.AddModelError("", "В записях уже есть эта задача");
                    return null;
                }
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            itemAdd.is_public_global = itemAdd.is_public ? itemAdd.is_public_global : false;

            planner_note planner_note = new planner_note();

            itemAdd.crt_date = now;
            itemAdd.crt_user = currUser.CabUserName;
            itemAdd.upd_date = now;
            itemAdd.upd_user = currUser.CabUserName;
            //itemAdd.exec_user_id = currUser.CabUserId;

            ModelMapper.Map<PlannerNoteViewModel, planner_note>(itemAdd, planner_note);

            dbContext.planner_note.Add(planner_note);

            if (itemAdd.SourceUpcomingEventId.GetValueOrDefault(0) > 0)
            {
                dbContext.planner_task_event.RemoveRange(dbContext.planner_task_event.Where(ss => ss.event_id == itemAdd.SourceUpcomingEventId));
                dbContext.planner_event.Remove(dbContext.planner_event.Where(ss => ss.event_id == itemAdd.SourceUpcomingEventId).FirstOrDefault());
            }

            dbContext.SaveChanges();

            return GetItem(planner_note.note_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PlannerNoteViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PlannerNoteViewModel itemEdit = (PlannerNoteViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры записи");
                return null;
            }

            planner_note planner_note = dbContext.planner_note.Where(ss => ss.note_id == itemEdit.note_id).FirstOrDefault();
            if (planner_note == null)
            {
                modelState.AddModelError("", "Не найдена запись");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            itemEdit.is_public_global = itemEdit.is_public ? itemEdit.is_public_global : false;

            PlannerNoteViewModel oldModel = new PlannerNoteViewModel();
            ModelMapper.Map<planner_note, PlannerNoteViewModel>(planner_note, oldModel);
            modelBeforeChanges = oldModel;

            itemEdit.upd_date = now;
            itemEdit.upd_user = currUser.CabUserName;

            ModelMapper.Map<PlannerNoteViewModel, planner_note>(itemEdit, planner_note);     

            dbContext.SaveChanges();

            return GetItem(planner_note.note_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PlannerNoteViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            PlannerNoteViewModel itemDel = (PlannerNoteViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты записи");
                return false;
            }

            planner_note planner_note = dbContext.planner_note.Where(ss => ss.note_id == itemDel.note_id).FirstOrDefault();
            if (planner_note == null)
            {
                modelState.AddModelError("", "Не найдена запись");
                return false;
            }

            dbContext.planner_note.Remove(planner_note);
            dbContext.SaveChanges();

            return true;
        }

        public bool Delete_byId(int note_id, ModelStateDictionary modelState)
        {
            planner_note planner_note = dbContext.planner_note.Where(ss => ss.note_id == note_id).FirstOrDefault();
            if (planner_note == null)
            {
                modelState.AddModelError("", "Не найдена запись");
                return false;
            }

            dbContext.planner_note.Remove(planner_note);
            dbContext.SaveChanges();

            return true;
        }

        public bool PlannerNoteAddTasks(string task_id_list, int? curr_user_id, ModelStateDictionary modelState)
        {
            if (String.IsNullOrEmpty(task_id_list))
            {
                modelState.AddModelError("", "Не выбраны задачи");
                return false;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string task_id_list_formatted = "";
            List<int> task_id_list_int = new List<int>();
            try
            {
                task_id_list_formatted = task_id_list.EndsWith(",") ? (task_id_list.Remove(task_id_list.Length - 1)) : task_id_list;
                task_id_list_int.AddRange(task_id_list_formatted.Split(',').Select(int.Parse).AsEnumerable());
    
                List<crm_task> crm_task_list = dbContext.crm_task.Where(ss => task_id_list_int.Contains(ss.task_id)
                    && (!dbContext.planner_note.Where(tt => tt.exec_user_id == curr_user_id && tt.task_id == ss.task_id).Any())
                    )
                    .ToList();

                if ((crm_task_list != null) && (crm_task_list.Count > 0))
                {
                    foreach (var crm_task in crm_task_list)
                    {
                        planner_note planner_note = new planner_note();
                        planner_note.crt_date = now;
                        planner_note.crt_user = currUser.CabUserName;
                        planner_note.description = crm_task.task_text;
                        planner_note.title = crm_task.task_name;
                        planner_note.exec_user_id = curr_user_id;
                        planner_note.is_public = false;
                        planner_note.is_public_global = false;
                        planner_note.task_id = crm_task.task_id;
                        planner_note.event_state_id = 0;
                        dbContext.planner_note.Add(planner_note);
                    }
                    dbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }                                   
        }
    }
}
