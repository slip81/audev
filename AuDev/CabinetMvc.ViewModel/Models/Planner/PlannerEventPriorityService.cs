﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Planner
{
    public interface IPlannerEventPriorityService : IAuBaseService
    {
        //
    }

    public class PlannerEventPriorityService : AuBaseService, IPlannerEventPriorityService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.planner_event_priority
                .ProjectTo<PlannerEventPriorityViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.planner_event_priority
                .Where(ss => ss.priority_id == id)
                .ProjectTo<PlannerEventPriorityViewModel>()
                .FirstOrDefault()
                ;
        }

    }
}
