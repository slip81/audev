﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Planner
{
    public class PlannerEventPriorityViewModel : AuBaseViewModel
    {
        public PlannerEventPriorityViewModel()
            : base(Enums.MainObjectType.PLANNER_EVENT_PRIORITY)
        {
            //planner_event_priority
        }

        
        [Key()]
        [Display(Name = "Код")]
        public int priority_id { get; set; }

        [Display(Name = "Наименование")]
        public string priority_name { get; set; }

    }
}