﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimTypeViewModel : AuBaseViewModel
    {

        public PrjClaimTypeViewModel()
            : base()
        {
            //prj_claim_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int claim_type_id { get; set; }

        [Display(Name = "Название")]
        public string claim_type_name { get; set; }

        [Display(Name = "Кфт")]
        [UIHint("NumberDecimal")]
        [Range(0, 1)]
        public Nullable<decimal> rate { get; set; }
    }
}