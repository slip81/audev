﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text.RegularExpressions;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjWorkService : IAuBaseService
    {
        IQueryable<PrjWorkViewModel> GetList_byFilter(string prj_id_list, string exec_user_id_list, string work_state_type_id_list, bool is_accept, string search);
        IQueryable<PrjWorkViewModel> GetList_byClaim(int claim_id);
        IQueryable<PrjWorkViewModel> GetList_byWork(int work_id);
        IQueryable<PrjWorkViewModel> GetList_Brief(int brief_type_id, DateTime? date_beg, DateTime? date_end, int date_cut_type_id);
        bool UpdateWorkType(int? work_id, int? work_type_id, ModelStateDictionary modelState);
        bool UpdateWorkExecUser(int? work_id, int? exec_user_id, ModelStateDictionary modelState);        
    }

    public class PrjWorkService : AuBaseService, IPrjWorkService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_prj_work
                .Where(ss => ss.work_id == id)
                .ProjectTo<PrjWorkViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_prj_work
                .ProjectTo<PrjWorkViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_prj_work
                .Where(ss => ss.task_id == parent_id)
                .ProjectTo<PrjWorkViewModel>();          
        }

        public IQueryable<PrjWorkViewModel> GetList_byClaim(int claim_id) 
        {
            return dbContext.vw_prj_work
                .Where(ss => ss.claim_id == claim_id)
                .ProjectTo<PrjWorkViewModel>();
        }

        public IQueryable<PrjWorkViewModel> GetList_byWork(int work_id)
        {
            return (from t1 in dbContext.prj_work
                    from t2 in dbContext.vw_prj_work
                    where t1.work_id == work_id
                    && t1.task_id == t2.task_id
                    && t1.work_id != t2.work_id
                    select t2)
                   .ProjectTo<PrjWorkViewModel>();
        }

        public IQueryable<PrjWorkViewModel> GetList_byFilter(string prj_id_list, string exec_user_id_list, string work_state_type_id_list, bool is_accept, string search)
        {
            List<int?> prj_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(prj_id_list))
            {
                prj_id_list_int = prj_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<int?> exec_user_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(exec_user_id_list))
            {
                exec_user_id_list_int = exec_user_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<int?> work_state_type_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(work_state_type_id_list))
            {
                work_state_type_id_list_int = work_state_type_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            bool prj_id_list_ok = prj_id_list_int.Count > 0;
            bool exec_user_id_list_ok = exec_user_id_list_int.Count > 0;
            bool work_state_type_id_list_ok = work_state_type_id_list_int.Count > 0;

            if (!String.IsNullOrEmpty(search))
            {
                // кратко, подробно, номер, комментарии, задания

                return (from t1 in dbContext.vw_prj_work
                        where ((t1.claim_name.Trim().ToLower().Contains(search.Trim().ToLower())) || (t1.claim_text.Trim().ToLower().Contains(search.Trim().ToLower())) || (t1.claim_num.Trim().ToLower().Contains(search.Trim().ToLower())) || (t1.task_text.Trim().ToLower().Contains(search.Trim().ToLower())))                        
                        && (((exec_user_id_list_ok) && (exec_user_id_list_int.Contains(t1.exec_user_id))) || (!exec_user_id_list_ok))
                        && (((work_state_type_id_list_ok) && (work_state_type_id_list_int.Contains(t1.state_type_id))) || (!work_state_type_id_list_ok))
                        && (((is_accept) && (t1.is_accept)) || (!is_accept))
                        select t1)
                       .ProjectTo<PrjWorkViewModel>();
            }
            else
            {
                return (from t1 in dbContext.vw_prj_work
                        where (((exec_user_id_list_ok) && (exec_user_id_list_int.Contains(t1.exec_user_id))) || (!exec_user_id_list_ok))
                        && (((work_state_type_id_list_ok) && (work_state_type_id_list_int.Contains(t1.state_type_id))) || (!work_state_type_id_list_ok))
                        && (((is_accept) && (t1.is_accept)) || (!is_accept))
                        select t1)
                       .ProjectTo<PrjWorkViewModel>();
            }
        }

        public IQueryable<PrjWorkViewModel> GetList_Brief(int brief_type_id, DateTime? date_beg, DateTime? date_end, int date_cut_type_id)
        {

            IQueryable<vw_prj_work> query = dbContext.vw_prj_work;

            DateTime date_beg_for_new = DateTime.Today.AddBusinessDays(-1);

            DateTime date_beg_actual = date_beg.HasValue ? (DateTime)date_beg : DateTime.Today.AddMonths(-2);
            DateTime date_end_actual = date_end.HasValue ? ((DateTime)date_end).AddDays(1) : DateTime.Today.AddDays(1);

            switch ((Enums.PrjBriefEnum)brief_type_id)
            {
                case Enums.PrjBriefEnum.DEV_WORK_MONITORING:
                    switch ((Enums.PrjBriefDateCutTypeEnum)date_cut_type_id)
                    {
                        case Enums.PrjBriefDateCutTypeEnum.CRT_DATE:
                            query = query.Where(ss => ss.crt_date >= date_beg_actual && ss.crt_date < date_end_actual);
                            break;
                        case Enums.PrjBriefDateCutTypeEnum.UPD_DATE:
                            query = query.Where(ss => ss.upd_date >= date_beg_actual && ss.upd_date < date_end_actual);
                            break;
                        default:
                            query = query.Where(ss => 1 == 0);
                            break;
                    }
                    break;
                default:
                    query = query.Where(ss => 1 == 0);
                    break;
            }

            return query
                .Select(t1 => new PrjWorkViewModel()
                {
                    work_id = t1.work_id,
                    task_id = t1.task_id,
                    claim_id = t1.claim_id,
                    work_num = t1.work_num,
                    work_type_id = t1.work_type_id,
                    exec_user_id = t1.exec_user_id,
                    state_type_id = t1.state_type_id,
                    is_plan = t1.is_plan,
                    date_beg = t1.date_beg,
                    date_end = t1.date_end,
                    is_accept = t1.is_accept,
                    crt_date = t1.crt_date,
                    crt_user = t1.crt_user,
                    upd_date = t1.upd_date,
                    upd_user = t1.upd_user,
                    upd_num = t1.upd_num,
                    cost_plan = t1.cost_plan,
                    cost_fact = t1.cost_fact,
                    mess = t1.mess,
                    old_task_id = t1.old_task_id,
                    old_subtask_id = t1.old_subtask_id,
                    work_type_name = t1.work_type_name,
                    exec_user_name = t1.exec_user_name,
                    state_type_name = t1.state_type_name,
                    state_type_templ = t1.state_type_templ,
                    task_num = t1.task_num,
                    task_text = t1.task_text,
                    task_state_type_id = t1.task_state_type_id,
                    task_date_plan = t1.task_date_plan,
                    task_date_fact = t1.task_date_fact,
                    task_state_type_name = t1.task_state_type_name,
                    task_state_type_templ = t1.task_state_type_templ,
                    claim_num = t1.claim_num,
                    claim_name = t1.claim_name,
                    claim_text = t1.claim_text,
                    claim_project_id = t1.claim_project_id,
                    claim_module_id = t1.claim_module_id,
                    claim_module_part_id = t1.claim_module_part_id,
                    claim_project_version_id = t1.claim_project_version_id,
                    claim_client_id = t1.claim_client_id,
                    claim_priority_id = t1.claim_priority_id,
                    claim_resp_user_id = t1.claim_resp_user_id,
                    claim_date_plan = t1.claim_date_plan,
                    claim_date_fact = t1.claim_date_fact,
                    claim_project_repair_version_id = t1.claim_project_repair_version_id,
                    claim_is_archive = t1.claim_is_archive,
                    claim_is_control = t1.claim_is_control,
                    claim_project_name = t1.claim_project_name,
                    claim_module_name = t1.claim_module_name,
                    claim_module_part_name = t1.claim_module_part_name,
                    claim_project_version_name = t1.claim_project_version_name,
                    claim_client_name = t1.claim_client_name,
                    claim_priority_name = t1.claim_priority_name,
                    claim_resp_user_name = t1.claim_resp_user_name,
                    claim_project_repair_version_name = t1.claim_project_repair_version_name,
                    claim_state_type_id = t1.claim_state_type_id,
                    claim_state_type_name = t1.claim_state_type_name,
                    claim_state_type_templ = t1.claim_state_type_templ,
                    is_overdue = t1.is_overdue,
                    date_time_beg = t1.date_time_beg,
                    date_time_end = t1.date_time_end,
                    is_all_day = t1.is_all_day,
                    claim_type_id = t1.claim_type_id,
                    claim_stage_id = t1.claim_stage_id,
                    claim_type_name = t1.claim_type_name,
                    claim_stage_name = t1.claim_stage_name,
                    brief_state_type_id = t1.crt_date >= date_beg_for_new ? (int)Enums.PrjBriefStateTypeEnum.NEW : t1.brief_state_type_id,
                    brief_state_type_name = t1.crt_date >= date_beg_for_new ? "1. Новые" : t1.brief_state_type_name,
                    date_send = t1.date_send,
                    is_new = false,
                    prj_id_list = t1.prj_id_list,
                    prj_name_list = t1.prj_name_list,
                });
        }

        public bool UpdateWorkType(int? work_id, int? work_type_id, ModelStateDictionary modelState)
        {
            try
            {
                prj_work prj_work = dbContext.prj_work.Where(ss => ss.work_id == work_id).FirstOrDefault();
                if (prj_work == null)
                {
                    modelState.AddModelError("", "Не найдена работа с кодом " + prj_work.work_id.ToString());
                    return false;
                }

                prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == prj_work.claim_id).FirstOrDefault();

                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;

                prj_work.work_type_id = work_type_id.GetValueOrDefault(0);
                prj_work.upd_date = now;
                prj_work.upd_user = user;
                prj_work.upd_num = prj_work.upd_num + 1;

                prj_claim.upd_date = now;
                prj_claim.upd_user = user;
                prj_claim.upd_num = prj_claim.upd_num + 1;

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }
        
        public bool UpdateWorkExecUser(int? work_id, int? exec_user_id, ModelStateDictionary modelState)
        {
            try
            {
                prj_work prj_work = dbContext.prj_work.Where(ss => ss.work_id == work_id).FirstOrDefault();
                if (prj_work == null)
                {
                    modelState.AddModelError("", "Не найдена работа с кодом " + prj_work.work_id.ToString());
                    return false;
                }
                if (prj_work.exec_user_id == exec_user_id.GetValueOrDefault(0))                   
                {                    
                    return true;
                }
                if ((prj_work.state_type_id == (int)Enums.PrjWorkStateTypeEnum.DONE) 
                    || (prj_work.state_type_id == (int)Enums.PrjWorkStateTypeEnum.CANCELED))
                {
                    modelState.AddModelError("", "Нельзя изменить исполнителя неактивной работы");
                    return false;
                }

                int old_exec_user_id = prj_work.exec_user_id;
                prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == prj_work.claim_id).FirstOrDefault();

                prj_task_user_state prj_task_user_state = dbContext.prj_task_user_state.Where(ss => ss.task_id == prj_work.task_id && ss.user_id == old_exec_user_id && ss.is_current).FirstOrDefault();
                prj_user_group_item prj_user_group_item_existing = (from t1 in dbContext.prj_user_group
                                                                    from t2 in dbContext.prj_user_group_item
                                                                    where t1.group_id == t2.group_id
                                                                    && t1.user_id == old_exec_user_id
                                                                    && t2.task_id == prj_work.task_id
                                                                    && t1.fixed_kind.HasValue
                                                                    select t2)
                                                                    .FirstOrDefault();

                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;

                prj_work.exec_user_id = exec_user_id.GetValueOrDefault(0);
                prj_work.upd_date = now;
                prj_work.upd_user = user;
                prj_work.upd_num = prj_work.upd_num + 1;

                if (prj_task_user_state != null)
                {
                    if (prj_task_user_state.state_type_id == (int)Enums.PrjTaskUserStateTypeEnum.RECEIVED_TO_WORK)
                        prj_work.is_accept = false;
                    prj_task_user_state.user_id = prj_work.exec_user_id;
                    prj_task_user_state.upd_date = now;
                    prj_task_user_state.upd_user = user;
                    prj_task_user_state.state_type_id = prj_task_user_state.state_type_id == (int)Enums.PrjTaskUserStateTypeEnum.RECEIVED_TO_WORK ? (int)Enums.PrjTaskUserStateTypeEnum.SENT_TO_WORK : prj_task_user_state.state_type_id;

                    List<prj_task_user_state> prj_task_user_state_current_existing = dbContext.prj_task_user_state.Where(ss => ss.task_id == prj_task_user_state.task_id && ss.user_id == prj_task_user_state.user_id && ss.is_current && ss.item_id != prj_task_user_state.item_id)
                        .ToList();
                    if (prj_task_user_state_current_existing != null)
                    {
                        foreach (var prj_task_user_state_current_existing_item in prj_task_user_state_current_existing)
                        {
                            prj_task_user_state_current_existing_item.is_current = false;
                            prj_task_user_state_current_existing_item.upd_date = now;
                            prj_task_user_state_current_existing_item.upd_user = user;
                        }
                    }
                }

                if (prj_user_group_item_existing != null)
                {
                    prj_user_group prj_user_group_existing = dbContext.prj_user_group.Where(ss => ss.group_id == prj_user_group_item_existing.group_id).FirstOrDefault();
                    int? fixedKind = prj_user_group_existing.fixed_kind == (int?)Enums.PrjUserGroupFixedKindEnum.RECEIVED ? (int?)Enums.PrjUserGroupFixedKindEnum.SENT : prj_user_group_existing.fixed_kind;
                    prj_user_group prj_user_group_new = dbContext.prj_user_group.Where(ss => ss.user_id == prj_work.exec_user_id && ss.fixed_kind == fixedKind).FirstOrDefault();
                    if (prj_user_group_new != null)
                    {
                        prj_user_group_item prj_user_group_item_new = new prj_user_group_item();
                        prj_user_group_item_new.group_id = prj_user_group_new.group_id;
                        prj_user_group_item_new.item_type_id = (int)Enums.PrjUserGroupItemTypeEnum.TASK;
                        prj_user_group_item_new.task_id = prj_work.task_id;
                        dbContext.prj_user_group_item.Add(prj_user_group_item_new);
                    }
                    //
                    dbContext.prj_user_group_item.Remove(prj_user_group_item_existing);
                }

                prj_claim.upd_date = now;
                prj_claim.upd_user = user;
                prj_claim.upd_num = prj_claim.upd_num + 1;

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }
    }
}
