﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjNotifyTypeService : IAuBaseService
    {
        //
    }

    public class PrjNotifyTypeService : AuBaseService, IPrjNotifyTypeService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_notify_type
                .Where(ss => ss.notify_type_id == id)
                .ProjectTo<PrjNotifyTypeViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_notify_type.ProjectTo<PrjNotifyTypeViewModel>();
        }
    }
}
