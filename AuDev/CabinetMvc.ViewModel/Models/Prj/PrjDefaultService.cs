﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text.RegularExpressions;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjDefaultService : IAuBaseService
    {
        PrjDefaultViewModel GetItem_Default();
    }

    public class PrjDefaultService : AuBaseService, IPrjDefaultService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_default
                .Where(ss => ss.user_id == id)
                .ProjectTo<PrjDefaultViewModel>()
                .FirstOrDefault();
        }

        public PrjDefaultViewModel GetItem_Default()
        {
            return new PrjDefaultViewModel()
            {
                start_page = "claim",
            };
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjDefaultViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjDefaultViewModel itemAdd = (PrjDefaultViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы параметры");
                return null;
            }

            int user_id = currUser.CabUserId;
            prj_default prj_default_existing = dbContext.prj_default.Where(ss => ss.user_id == user_id).FirstOrDefault();
            if (prj_default_existing != null)
            {
                return Update(item, modelState);
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string user = currUser.CabUserName;

            prj_default prj_default = new prj_default();
            prj_default.start_page = itemAdd.start_page;
            prj_default.user_id = user_id;
            dbContext.prj_default.Add(prj_default);

            dbContext.SaveChanges();

            return xGetItem(user_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjDefaultViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjDefaultViewModel itemEdit = (PrjDefaultViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры");
                return null;
            }

            int user_id = currUser.CabUserId;
            prj_default prj_default = dbContext.prj_default.Where(ss => ss.user_id == user_id).FirstOrDefault();
            if (prj_default == null)
            {
                modelState.AddModelError("", "Не найдены параметры");
                return null;
            }

            PrjDefaultViewModel oldModel = new PrjDefaultViewModel();
            ModelMapper.Map<prj_default, PrjDefaultViewModel>(prj_default, oldModel);
            modelBeforeChanges = oldModel;

            prj_default.start_page = itemEdit.start_page;
            prj_default.user_id = user_id;

            dbContext.SaveChanges();

            return xGetItem(user_id);
        }
    }
}