﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjBriefIntervalTypeViewModel : AuBaseViewModel
    {

        public PrjBriefIntervalTypeViewModel()
            : base(Enums.MainObjectType.PRJ_BRIEF_INTERVAL_TYPE)
        {
            //prj_brief_interval_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int interval_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string interval_type_name { get; set; }
    }
}