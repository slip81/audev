﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjUserGroupItemService : IAuBaseService
    {
        IQueryable<PrjUserGroupItemViewModel> GetList_byFilter(long group_id, long period_id);
        void Reorder(int source_item_id, int source_group_id, int target_group_id, int ord, int? ord_prev);
    }

    public class PrjUserGroupItemService : AuBaseService, IPrjUserGroupItemService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_user_group_item.Where(ss => ss.item_id == id).ProjectTo<PrjUserGroupItemViewModel>().FirstOrDefault();
        }

        public IQueryable<PrjUserGroupItemViewModel> GetList_byFilter(long group_id, long period_id)
        {
            IQueryable<vw_prj_user_group_item> query = dbContext.vw_prj_user_group_item.Where(ss => ss.group_id == group_id);
            
            prj_user_group prj_user_group = dbContext.prj_user_group.Where(ss => ss.group_id == group_id).FirstOrDefault();
            if (prj_user_group == null)
            {
                return query.Where(ss => 1 == 0).ProjectTo<PrjUserGroupItemViewModel>();
            }

            if (period_id > 0)
            {
                DateTime date_beg = DateTime.Today.AddMonths(-1);
                switch ((Enums.PrjUserGroupPeriodTypeEnum)period_id)
                {
                    case Enums.PrjUserGroupPeriodTypeEnum.TODAY:
                        date_beg = DateTime.Today;
                        break;
                    case Enums.PrjUserGroupPeriodTypeEnum.TODAY_AND_YESTERDAY:
                        date_beg = DateTime.Today.AddDays(-1);
                        break;
                    case Enums.PrjUserGroupPeriodTypeEnum.THREE_DAYS:
                        date_beg = DateTime.Today.AddDays(-3);
                        break;
                    case Enums.PrjUserGroupPeriodTypeEnum.SEVEN_DAYS:
                        date_beg = DateTime.Today.AddDays(-7);
                        break;
                    default:
                        break;
                }

                query = from t1 in query
                        from t2 in dbContext.prj_task_user_state
                        where t1.task_id == t2.task_id
                        && t1.user_id == t2.user_id
                        && t2.is_current
                        && t2.crt_date >= date_beg
                        select t1;
            }

            return query                
                .ProjectTo<PrjUserGroupItemViewModel>();
        }

        public void Reorder(int source_item_id, int source_group_id, int target_group_id, int ord, int? ord_prev)
        {
            prj_user_group_item prj_user_group_item = dbContext.prj_user_group_item.Where(ss => ss.item_id == source_item_id && ss.group_id == source_group_id).FirstOrDefault();
            if (prj_user_group_item == null)
                return;

            if ((source_group_id == target_group_id) && (ord == ord_prev))
                return;

            if ((prj_user_group_item.group_id == target_group_id) && (prj_user_group_item.ord == ord))
                return;

            prj_user_group_item.group_id = target_group_id;
            prj_user_group_item.ord = ord;
            prj_user_group_item.ord_prev = ord_prev;
            prj_user_group_item.group_id_prev = source_group_id;
            prj_user_group_item.trigger_flag = !prj_user_group_item.trigger_flag;
            dbContext.SaveChanges();
        }
    }
}