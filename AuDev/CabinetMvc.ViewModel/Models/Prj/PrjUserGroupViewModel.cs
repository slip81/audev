﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjUserGroupViewModel : AuBaseViewModel
    {

        public PrjUserGroupViewModel()
            : base(Enums.MainObjectType.PRJ_USER_GROUP)
        {
            //prj_user_group
        }

        // db
        
        [Key()]
        [Display(Name = "Код")]
        public int group_id { get; set; }

        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [Display(Name = "Порядок")]
        public Nullable<int> ord { get; set; }

        [Display(Name = "fixed_kind")]
        public Nullable<int> fixed_kind { get; set; }

        // additional
        [Display(Name = "group_html")]
        public string group_html { get; set; }        
    }
}