﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text.RegularExpressions;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuDev.Common.Util;
using System.Data.Entity;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimService : IAuBaseService
    {
        //IQueryable<PrjClaimViewModel> GetList_byFilter(string project_id_list, string prj_id_list, string version_main_name_list, string exec_user_id_list, string claim_state_type_id_list, string search);
        IQueryable<PrjClaimViewModel> GetList_byFilter(string project_id_list, string version_main_name_list, string claim_stage_id_list, string exec_user_id_list, string claim_is_active_list, string prj_id_list, string search);
        IQueryable<PrjClaimViewModel> GetList_byPrj(int? prj_id, string search);
        IQueryable<PrjClaimViewModel> GetList_New(string prj_id_list, string crt_user_name_list);
        IQueryable<PrjClaimViewModel> GetList_Favor();
        IQueryable<PrjClaimViewModel> GetList_Brief(int brief_type_id, DateTime? date_beg, DateTime? date_end, int date_cut_type_id);
        IQueryable<PrjClaimViewModel> GetList_Archive(string search);
        PrjClaimViewModel InsertClaim(PrjClaimViewModel itemAdd, List<PrjMessViewModel> mess_list, List<PrjTaskViewModel> task_list, List<PrjWorkViewModel> work_list, List<PrjTaskUserStateViewModel> task_user_state_list, ModelStateDictionary modelState);
        PrjClaimViewModel InsertClaim_FromTask(PrjClaimViewModel itemAdd, List<PrjMessViewModel> mess_list, List<PrjTaskViewModel> task_list, List<PrjWorkViewModel> work_list, List<PrjTaskUserStateViewModel> task_user_state_list, ModelStateDictionary modelState);
        PrjClaimViewModel UpdateClaim(PrjClaimViewModel itemEdit, List<PrjMessViewModel> mess_list, List<PrjTaskViewModel> task_list, List<PrjWorkViewModel> work_list, List<int> work_del_list, List<PrjTaskUserStateViewModel> task_user_state_list, ModelStateDictionary modelState);
        bool DeleteClaim(int claim_id, ModelStateDictionary modelState);
        bool RestoreClaim(int claim_id, ModelStateDictionary modelState);        
        PrjClaimViewModel Insert_Story(long story_id, ModelStateDictionary modelState);
        void ChangePrj(int claim_id, int prj_id);
        void PrjClaimFavorAdd(int claim_id);
        void PrjClaimFavorDel(int claim_id);
        void UpdateTaskUserState(int task_id, int state_type_id, int user_id, int? from_user_id, string comment, bool sync_with_notebook);
        void UpdateClaimVersion(int claim_id, bool is_active_state, bool is_version_fixed);
    }

    public class PrjClaimService : AuBaseService, IPrjClaimService
    {
        IPrjLogService logService;

        /*
        Где может изменяться задача:
            this.InsertClaim
            this.UpdateClaim
            prjTaskService.InsertSimple
            prjTaskService.ExecuteAction
        */

        #region Get

        protected override AuBaseViewModel xGetItem(long id)
        {
            int user_id = currUser.CabUserId;
            return (from t1 in dbContext.vw_prj_claim
                    from t2 in dbContext.prj_claim_favor.Where(ss => ss.claim_id == t1.claim_id && ss.user_id == user_id).DefaultIfEmpty()
                    where t1.claim_id == id
                    select new PrjClaimViewModel()
                    {
                        claim_id = t1.claim_id,
                        claim_name = t1.claim_name,
                        claim_num = t1.claim_num,
                        claim_text = t1.claim_text,                        
                        project_id = t1.project_id,
                        module_id = t1.module_id,
                        module_part_id = t1.module_part_id,
                        project_version_id = t1.project_version_id,
                        client_id = t1.client_id,
                        priority_id = t1.priority_id,
                        resp_user_id = t1.resp_user_id,
                        date_plan = t1.date_plan,
                        date_fact = t1.date_fact,
                        project_repair_version_id = t1.project_repair_version_id,
                        is_archive = t1.is_archive,
                        crt_date = t1.crt_date,
                        crt_user = t1.crt_user,
                        upd_date = t1.upd_date,
                        upd_user = t1.upd_user,
                        upd_num = t1.upd_num,
                        is_control = t1.is_control,
                        old_task_id = t1.old_task_id,
                        project_name = t1.project_name,
                        module_name = t1.module_name,
                        module_part_name = t1.module_part_name,
                        project_version_name = t1.project_version_name,
                        client_name = t1.client_name,
                        priority_name = t1.priority_name,
                        resp_user_name = t1.resp_user_name,
                        project_repair_version_name = t1.project_repair_version_name,
                        task_cnt = t1.task_cnt,
                        new_task_cnt = t1.new_task_cnt,
                        active_task_cnt = t1.active_task_cnt,
                        done_task_cnt = t1.done_task_cnt,
                        canceled_task_cnt = t1.canceled_task_cnt,
                        claim_state_type_id = t1.claim_state_type_id,
                        claim_state_type_name = t1.claim_state_type_name,
                        claim_state_type_templ = t1.claim_state_type_templ,
                        old_group_id = t1.old_group_id,
                        old_group_name = t1.old_group_name,
                        old_state_id = t1.old_state_id,
                        old_state_name = t1.old_state_name,
                        active_work_type_id = t1.active_work_type_id,
                        active_work_type_name = t1.active_work_type_name,
                        is_overdue = t1.is_overdue,
                        is_favor = t2 == null ? false : true,                        
                        brief_state_type_id = t1.brief_state_type_id,
                        brief_state_type_name = t1.brief_state_type_name,
                        exec_user_name_list = t1.exec_user_name_list,
                        claim_type_id = t1.claim_type_id,
                        claim_type_name = t1.claim_type_name,
                        claim_stage_id = t1.claim_stage_id,
                        claim_stage_name = t1.claim_stage_name,
                        mess_cnt = t1.mess_cnt,
                        priority_rate = t1.priority_rate,
                        claim_type_rate = t1.claim_type_rate,
                        summary_rate = t1.summary_rate,
                        is_active_state = t1.is_active_state,
                        prj_id_list = t1.prj_id_list,
                        prj_name_list = t1.prj_name_list,                        
                        version_id = t1.version_id,
                        is_version_fixed = t1.is_version_fixed,
                        version_name = t1.version_name,
                        version_value = t1.version_value,
                        version_value_full = t1.version_value_full,
                        version_type_id = t1.version_type_id,
                        version_type_name = t1.version_type_name,
                        arc_date = t1.arc_date,
                        arc_user = t1.arc_user,
                    })
                   .FirstOrDefault();         
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            int user_id = currUser.CabUserId;
            return (from t1 in dbContext.vw_prj_claim
                    from t2 in dbContext.prj_claim_favor.Where(ss => ss.claim_id == t1.claim_id && ss.user_id == user_id).DefaultIfEmpty() 
                    where !t1.is_archive
                    select new PrjClaimViewModel()
                    {
                        claim_id = t1.claim_id,
                        claim_name = t1.claim_name,
                        claim_num = t1.claim_num,
                        claim_text = t1.claim_text,
                        project_id = t1.project_id,
                        module_id = t1.module_id,
                        module_part_id = t1.module_part_id,
                        project_version_id = t1.project_version_id,
                        client_id = t1.client_id,
                        priority_id = t1.priority_id,
                        resp_user_id = t1.resp_user_id,
                        date_plan = t1.date_plan,
                        date_fact = t1.date_fact,
                        project_repair_version_id = t1.project_repair_version_id,
                        is_archive = t1.is_archive,
                        crt_date = t1.crt_date,
                        crt_user = t1.crt_user,
                        upd_date = t1.upd_date,
                        upd_user = t1.upd_user,
                        upd_num = t1.upd_num,
                        is_control = t1.is_control,
                        old_task_id = t1.old_task_id,
                        project_name = t1.project_name,
                        module_name = t1.module_name,
                        module_part_name = t1.module_part_name,
                        project_version_name = t1.project_version_name,
                        client_name = t1.client_name,
                        priority_name = t1.priority_name,
                        resp_user_name = t1.resp_user_name,
                        project_repair_version_name = t1.project_repair_version_name,
                        task_cnt = t1.task_cnt,
                        new_task_cnt = t1.new_task_cnt,
                        active_task_cnt = t1.active_task_cnt,
                        done_task_cnt = t1.done_task_cnt,
                        canceled_task_cnt = t1.canceled_task_cnt,
                        claim_state_type_id = t1.claim_state_type_id,
                        claim_state_type_name = t1.claim_state_type_name,
                        claim_state_type_templ = t1.claim_state_type_templ,
                        old_group_id = t1.old_group_id,
                        old_group_name = t1.old_group_name,
                        old_state_id = t1.old_state_id,
                        old_state_name = t1.old_state_name,
                        active_work_type_id = t1.active_work_type_id,
                        active_work_type_name = t1.active_work_type_name,
                        is_overdue = t1.is_overdue,
                        is_favor = t2 == null ? false : true,
                        brief_state_type_id = t1.brief_state_type_id,
                        brief_state_type_name = t1.brief_state_type_name,
                        exec_user_name_list = t1.exec_user_name_list,
                        claim_type_id = t1.claim_type_id,
                        claim_type_name = t1.claim_type_name,
                        claim_stage_id = t1.claim_stage_id,
                        claim_stage_name = t1.claim_stage_name,
                        mess_cnt = t1.mess_cnt,
                        priority_rate = t1.priority_rate,
                        claim_type_rate = t1.claim_type_rate,
                        summary_rate = t1.summary_rate,
                        is_active_state = t1.is_active_state,
                        prj_id_list = t1.prj_id_list,
                        prj_name_list = t1.prj_name_list,
                        version_id = t1.version_id,
                        is_version_fixed = t1.is_version_fixed,
                        version_name = t1.version_name,
                        version_value = t1.version_value,
                        version_value_full = t1.version_value_full,
                        version_type_id = t1.version_type_id,
                        version_type_name = t1.version_type_name,
                        arc_date = t1.arc_date,
                        arc_user = t1.arc_user,
                    });
        }

        public IQueryable<PrjClaimViewModel> GetList_byFilter(string project_id_list, string version_main_name_list, string claim_stage_id_list
            , string exec_user_id_list, string claim_is_active_list, string prj_id_list, string search)
        {
            int user_id = currUser.CabUserId;

            List<int?> project_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(project_id_list))
            {
                project_id_list_int = project_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<int?> prj_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(prj_id_list))
            {
                prj_id_list_int = prj_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            string version_main_name = null;
            if (!String.IsNullOrEmpty(version_main_name_list))
            {
                version_main_name = version_main_name_list.Trim().ToLower();
            }

            List<int?> exec_user_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(exec_user_id_list))
            {
                exec_user_id_list_int = exec_user_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<int?> claim_stage_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(claim_stage_id_list))
            {
                claim_stage_id_list_int = claim_stage_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<bool> claim_is_active_list_bool = new List<bool>();
            if (!String.IsNullOrEmpty(claim_is_active_list))
            {
                claim_is_active_list_bool = claim_is_active_list.Split(',').Select(ss => bool.Parse(ss)).ToList();
            }

            bool project_id_list_ok = project_id_list_int.Count > 0;
            bool prj_id_list_ok = prj_id_list_int.Count > 0;
            bool version_main_name_ok = !String.IsNullOrWhiteSpace(version_main_name);
            bool exec_user_id_list_ok = exec_user_id_list_int.Count > 0;                        
            bool claim_stage_id_list_ok = claim_stage_id_list_int.Count > 0;
            bool claim_is_active_list_ok = claim_is_active_list_bool.Count > 0;

            var query1 = from t1 in dbContext.vw_prj_claim
                         where !t1.is_archive
                         select t1;
            if (!String.IsNullOrEmpty(search))
            {
                query1 = from t1 in query1
                         where
                         ((t1.claim_name.Trim().ToLower().Contains(search.Trim().ToLower())) || (t1.claim_text.Trim().ToLower().Contains(search.Trim().ToLower())) || (t1.claim_num.Trim().ToLower().Contains(search.Trim().ToLower())))
                         select t1;
            }
            else
            {
                if (prj_id_list_ok)
                {
                    query1 = from t1 in query1
                             from t2 in dbContext.prj_claim_prj
                             where t1.claim_id == t2.claim_id
                             && prj_id_list_int.Contains(t2.prj_id)
                             select t1;
                }
                if (exec_user_id_list_ok)
                    query1 = query1.Where(t1 =>
                            ((exec_user_id_list_int.Contains(t1.resp_user_id))
                            ||
                            (dbContext.prj_work.Where(ss => ss.claim_id == t1.claim_id && exec_user_id_list_int.Contains(ss.exec_user_id)).Any()))
                            );
                if (project_id_list_ok)
                    query1 = query1.Where(t1 => project_id_list_int.Contains(t1.project_id));
                if (version_main_name_ok)
                {
                    bool isNumericVersion = Regex.IsMatch(version_main_name, @"^(\d+[,.]\d*|\d*[,.]?\d+)$");
                    query1 = query1.Where(t1 => t1.is_active_state == !isNumericVersion 
                    && ((t1.version_name.Trim().ToLower().StartsWith(version_main_name)) || (t1.version_value.Trim().ToLower().StartsWith(version_main_name))));                    
                }
                if (claim_stage_id_list_ok)
                    query1 = query1.Where(t1 => claim_stage_id_list_int.Contains(t1.claim_stage_id));
                if (claim_is_active_list_ok)
                    query1 = query1.Where(t1 => claim_is_active_list_bool.Contains(t1.is_active_state));
            }

            return from t1 in query1
                   from t2 in dbContext.prj_claim_favor.Where(ss => ss.claim_id == t1.claim_id && ss.user_id == user_id).DefaultIfEmpty()
                   select new PrjClaimViewModel()
                   {
                       claim_id = t1.claim_id,
                       claim_name = t1.claim_name,
                       claim_num = t1.claim_num,
                       claim_text = t1.claim_text,
                       project_id = t1.project_id,
                       module_id = t1.module_id,
                       module_part_id = t1.module_part_id,
                       project_version_id = t1.project_version_id,
                       client_id = t1.client_id,
                       priority_id = t1.priority_id,
                       resp_user_id = t1.resp_user_id,
                       date_plan = t1.date_plan,
                       date_fact = t1.date_fact,
                       project_repair_version_id = t1.project_repair_version_id,
                       is_archive = t1.is_archive,
                       crt_date = t1.crt_date,
                       crt_user = t1.crt_user,
                       upd_date = t1.upd_date,
                       upd_user = t1.upd_user,
                       upd_num = t1.upd_num,
                       is_control = t1.is_control,
                       old_task_id = t1.old_task_id,
                       project_name = t1.project_name,
                       module_name = t1.module_name,
                       module_part_name = t1.module_part_name,
                       project_version_name = t1.project_version_name,
                       client_name = t1.client_name,
                       priority_name = t1.priority_name,
                       resp_user_name = t1.resp_user_name,
                       project_repair_version_name = t1.project_repair_version_name,
                       task_cnt = t1.task_cnt,
                       new_task_cnt = t1.new_task_cnt,
                       active_task_cnt = t1.active_task_cnt,
                       done_task_cnt = t1.done_task_cnt,
                       canceled_task_cnt = t1.canceled_task_cnt,
                       claim_state_type_id = t1.claim_state_type_id,
                       claim_state_type_name = t1.claim_state_type_name,
                       claim_state_type_templ = t1.claim_state_type_templ,
                       old_group_id = t1.old_group_id,
                       old_group_name = t1.old_group_name,
                       old_state_id = t1.old_state_id,
                       old_state_name = t1.old_state_name,
                       active_work_type_id = t1.active_work_type_id,
                       active_work_type_name = t1.active_work_type_name,
                       is_overdue = t1.is_overdue,
                       is_favor = t2 == null ? false : true,
                       brief_state_type_id = t1.brief_state_type_id,
                       brief_state_type_name = t1.brief_state_type_name,
                       exec_user_name_list = t1.exec_user_name_list,
                       claim_type_id = t1.claim_type_id,
                       claim_type_name = t1.claim_type_name,
                       claim_stage_id = t1.claim_stage_id,
                       claim_stage_name = t1.claim_stage_name,
                       mess_cnt = t1.mess_cnt,
                       priority_rate = t1.priority_rate,
                       claim_type_rate = t1.claim_type_rate,
                       summary_rate = t1.summary_rate,
                       is_active_state = t1.is_active_state,
                       prj_id_list = t1.prj_id_list,
                       prj_name_list = t1.prj_name_list,
                       version_id = t1.version_id,
                       is_version_fixed = t1.is_version_fixed,
                       version_name = t1.version_name,
                       version_value = t1.version_value,
                       version_value_full = t1.version_value_full,
                       version_type_id = t1.version_type_id,
                       version_type_name = t1.version_type_name,
                       arc_date = t1.arc_date,
                       arc_user = t1.arc_user,
                   };
        }

        public IQueryable<PrjClaimViewModel> GetList_byPrj(int? prj_id, string search)
        {            
            int user_id = currUser.CabUserId;
            /*
            var query = dbContext.vw_prj_claim
                .Where(ss => !ss.is_archive);
            */
            var query = from t1 in dbContext.vw_prj_claim
                        from t2 in dbContext.prj_claim_prj
                        where t1.claim_id == t2.claim_id
                        && t2.prj_id == prj_id
                        && !t1.is_archive
                        select t1;

            if (!String.IsNullOrWhiteSpace(search))
            {
                query = query.Where(ss => ss.claim_num.Trim().ToLower().Contains(search.Trim().ToLower())
                    ||
                    ss.claim_name.Trim().ToLower().Contains(search.Trim().ToLower())
                    ||
                    ss.claim_text.Trim().ToLower().Contains(search.Trim().ToLower())
                    );
            }

            return from t1 in query
                   from t2 in dbContext.prj_claim_favor.Where(ss => ss.claim_id == t1.claim_id && ss.user_id == user_id).DefaultIfEmpty()
                   select new PrjClaimViewModel()
                   {
                       claim_id = t1.claim_id,
                       claim_name = t1.claim_name,
                       claim_num = t1.claim_num,
                       claim_text = t1.claim_text,                       
                       project_id = t1.project_id,
                       module_id = t1.module_id,
                       module_part_id = t1.module_part_id,
                       project_version_id = t1.project_version_id,
                       client_id = t1.client_id,
                       priority_id = t1.priority_id,
                       resp_user_id = t1.resp_user_id,
                       date_plan = t1.date_plan,
                       date_fact = t1.date_fact,
                       project_repair_version_id = t1.project_repair_version_id,
                       is_archive = t1.is_archive,
                       crt_date = t1.crt_date,
                       crt_user = t1.crt_user,
                       upd_date = t1.upd_date,
                       upd_user = t1.upd_user,
                       upd_num = t1.upd_num,
                       is_control = t1.is_control,
                       old_task_id = t1.old_task_id,
                       project_name = t1.project_name,
                       module_name = t1.module_name,
                       module_part_name = t1.module_part_name,
                       project_version_name = t1.project_version_name,
                       client_name = t1.client_name,
                       priority_name = t1.priority_name,
                       resp_user_name = t1.resp_user_name,
                       project_repair_version_name = t1.project_repair_version_name,
                       task_cnt = t1.task_cnt,
                       new_task_cnt = t1.new_task_cnt,
                       active_task_cnt = t1.active_task_cnt,
                       done_task_cnt = t1.done_task_cnt,
                       canceled_task_cnt = t1.canceled_task_cnt,
                       claim_state_type_id = t1.claim_state_type_id,
                       claim_state_type_name = t1.claim_state_type_name,
                       claim_state_type_templ = t1.claim_state_type_templ,
                       old_group_id = t1.old_group_id,
                       old_group_name = t1.old_group_name,
                       old_state_id = t1.old_state_id,
                       old_state_name = t1.old_state_name,
                       active_work_type_id = t1.active_work_type_id,
                       active_work_type_name = t1.active_work_type_name,
                       is_overdue = t1.is_overdue,
                       is_favor = t2 == null ? false : true,
                       brief_state_type_id = t1.brief_state_type_id,
                       brief_state_type_name = t1.brief_state_type_name,
                       exec_user_name_list = t1.exec_user_name_list,
                       claim_type_id = t1.claim_type_id,
                       claim_type_name = t1.claim_type_name,
                       claim_stage_id = t1.claim_stage_id,
                       claim_stage_name = t1.claim_stage_name,
                       mess_cnt = t1.mess_cnt,
                       priority_rate = t1.priority_rate,
                       claim_type_rate = t1.claim_type_rate,
                       summary_rate = t1.summary_rate,
                       is_active_state = t1.is_active_state,
                       prj_id_list = t1.prj_id_list,
                       prj_name_list = t1.prj_name_list,
                       version_id = t1.version_id,
                       is_version_fixed = t1.is_version_fixed,
                       version_name = t1.version_name,
                       version_value = t1.version_value,
                       version_value_full = t1.version_value_full,
                       version_type_id = t1.version_type_id,
                       version_type_name = t1.version_type_name,
                       arc_date = t1.arc_date,
                       arc_user = t1.arc_user,
                   };
        }

        public IQueryable<PrjClaimViewModel> GetList_New(string prj_id_list, string crt_user_name_list)
        {
            int user_id = currUser.CabUserId;

            List<int?> prj_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(prj_id_list))
            {
                prj_id_list_int = prj_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<string> crt_user_name_list_string = new List<string>();
            if (!String.IsNullOrEmpty(crt_user_name_list))
            {
                crt_user_name_list_string = crt_user_name_list.Split(',').Select(ss => ss).ToList();
            }

            bool prj_id_list_ok = prj_id_list_int.Count > 0;
            bool crt_user_name_list_ok = crt_user_name_list_string.Count > 0;            

            return (from t1 in dbContext.vw_prj_claim
                    from t2 in dbContext.prj_claim_favor.Where(ss => ss.claim_id == t1.claim_id && ss.user_id == user_id).DefaultIfEmpty()
                    where !t1.is_archive                    
                    &&
                    (((crt_user_name_list_ok) && (crt_user_name_list_string.Contains(t1.crt_user))) || (!crt_user_name_list_ok))
                    select new PrjClaimViewModel()
                    {
                        claim_id = t1.claim_id,
                        claim_name = t1.claim_name,
                        claim_num = t1.claim_num,
                        claim_text = t1.claim_text,
                        project_id = t1.project_id,
                        module_id = t1.module_id,
                        module_part_id = t1.module_part_id,
                        project_version_id = t1.project_version_id,
                        client_id = t1.client_id,
                        priority_id = t1.priority_id,
                        resp_user_id = t1.resp_user_id,
                        date_plan = t1.date_plan,
                        date_fact = t1.date_fact,
                        project_repair_version_id = t1.project_repair_version_id,
                        is_archive = t1.is_archive,
                        crt_date = t1.crt_date,
                        crt_user = t1.crt_user,
                        upd_date = t1.upd_date,
                        upd_user = t1.upd_user,
                        upd_num = t1.upd_num,
                        is_control = t1.is_control,
                        old_task_id = t1.old_task_id,
                        project_name = t1.project_name,
                        module_name = t1.module_name,
                        module_part_name = t1.module_part_name,
                        project_version_name = t1.project_version_name,
                        client_name = t1.client_name,
                        priority_name = t1.priority_name,
                        resp_user_name = t1.resp_user_name,
                        project_repair_version_name = t1.project_repair_version_name,
                        task_cnt = t1.task_cnt,
                        new_task_cnt = t1.new_task_cnt,
                        active_task_cnt = t1.active_task_cnt,
                        done_task_cnt = t1.done_task_cnt,
                        canceled_task_cnt = t1.canceled_task_cnt,
                        claim_state_type_id = t1.claim_state_type_id,
                        claim_state_type_name = t1.claim_state_type_name,
                        claim_state_type_templ = t1.claim_state_type_templ,
                        old_group_id = t1.old_group_id,
                        old_group_name = t1.old_group_name,
                        old_state_id = t1.old_state_id,
                        old_state_name = t1.old_state_name,
                        active_work_type_id = t1.active_work_type_id,
                        active_work_type_name = t1.active_work_type_name,
                        is_overdue = t1.is_overdue,
                        is_favor = t2 == null ? false : true,
                        brief_state_type_id = t1.brief_state_type_id,
                        brief_state_type_name = t1.brief_state_type_name,
                        exec_user_name_list = t1.exec_user_name_list,
                        claim_type_id = t1.claim_type_id,
                        claim_type_name = t1.claim_type_name,
                        claim_stage_id = t1.claim_stage_id,
                        claim_stage_name = t1.claim_stage_name,
                        mess_cnt = t1.mess_cnt,
                        priority_rate = t1.priority_rate,
                        claim_type_rate = t1.claim_type_rate,
                        summary_rate = t1.summary_rate,
                        is_active_state = t1.is_active_state,
                        prj_id_list = t1.prj_id_list,
                        prj_name_list = t1.prj_name_list,
                        version_id = t1.version_id,
                        is_version_fixed = t1.is_version_fixed,
                        version_name = t1.version_name,
                        version_value = t1.version_value,
                        version_value_full = t1.version_value_full,
                        version_type_id = t1.version_type_id,
                        version_type_name = t1.version_type_name,
                        arc_date = t1.arc_date,
                        arc_user = t1.arc_user,
                    })
                .OrderByDescending(ss => ss.crt_date)
                .Take(10);
                   
        }

        public IQueryable<PrjClaimViewModel> GetList_Favor()
        {
            int user_id = currUser.CabUserId;
            return (from t1 in dbContext.vw_prj_claim
                    from t2 in dbContext.prj_claim_favor//.Where(ss => ss.claim_id == t1.claim_id && ss.user_id == user_id).DefaultIfEmpty()
                    where !t1.is_archive
                    && t1.claim_id == t2.claim_id && t2.user_id == user_id
                    select new PrjClaimViewModel()
                    {
                        claim_id = t1.claim_id,
                        claim_name = t1.claim_name,
                        claim_num = t1.claim_num,
                        claim_text = t1.claim_text,
                        project_id = t1.project_id,
                        module_id = t1.module_id,
                        module_part_id = t1.module_part_id,
                        project_version_id = t1.project_version_id,
                        client_id = t1.client_id,
                        priority_id = t1.priority_id,
                        resp_user_id = t1.resp_user_id,
                        date_plan = t1.date_plan,
                        date_fact = t1.date_fact,
                        project_repair_version_id = t1.project_repair_version_id,
                        is_archive = t1.is_archive,
                        crt_date = t1.crt_date,
                        crt_user = t1.crt_user,
                        upd_date = t1.upd_date,
                        upd_user = t1.upd_user,
                        upd_num = t1.upd_num,
                        is_control = t1.is_control,
                        old_task_id = t1.old_task_id,
                        project_name = t1.project_name,
                        module_name = t1.module_name,
                        module_part_name = t1.module_part_name,
                        project_version_name = t1.project_version_name,
                        client_name = t1.client_name,
                        priority_name = t1.priority_name,
                        resp_user_name = t1.resp_user_name,
                        project_repair_version_name = t1.project_repair_version_name,
                        task_cnt = t1.task_cnt,
                        new_task_cnt = t1.new_task_cnt,
                        active_task_cnt = t1.active_task_cnt,
                        done_task_cnt = t1.done_task_cnt,
                        canceled_task_cnt = t1.canceled_task_cnt,
                        claim_state_type_id = t1.claim_state_type_id,
                        claim_state_type_name = t1.claim_state_type_name,
                        claim_state_type_templ = t1.claim_state_type_templ,
                        old_group_id = t1.old_group_id,
                        old_group_name = t1.old_group_name,
                        old_state_id = t1.old_state_id,
                        old_state_name = t1.old_state_name,
                        active_work_type_id = t1.active_work_type_id,
                        active_work_type_name = t1.active_work_type_name,
                        is_overdue = t1.is_overdue,
                        is_favor = t2 == null ? false : true,
                        brief_state_type_id = t1.brief_state_type_id,
                        brief_state_type_name = t1.brief_state_type_name,
                        exec_user_name_list = t1.exec_user_name_list,
                        claim_type_id = t1.claim_type_id,
                        claim_type_name = t1.claim_type_name,
                        claim_stage_id = t1.claim_stage_id,
                        claim_stage_name = t1.claim_stage_name,
                        mess_cnt = t1.mess_cnt,
                        priority_rate = t1.priority_rate,
                        claim_type_rate = t1.claim_type_rate,
                        summary_rate = t1.summary_rate,
                        is_active_state = t1.is_active_state,
                        prj_id_list = t1.prj_id_list,
                        prj_name_list = t1.prj_name_list,
                        version_id = t1.version_id,
                        is_version_fixed = t1.is_version_fixed,
                        version_name = t1.version_name,
                        version_value = t1.version_value,
                        version_value_full = t1.version_value_full,
                        version_type_id = t1.version_type_id,
                        version_type_name = t1.version_type_name,
                        arc_date = t1.arc_date,
                        arc_user = t1.arc_user,
                    });
        }       

        public IQueryable<PrjClaimViewModel> GetList_Brief(int brief_type_id, DateTime? date_beg, DateTime? date_end, int date_cut_type_id)
        {

            IQueryable<vw_prj_claim> query = dbContext.vw_prj_claim.Where(ss => !ss.is_archive);

            DateTime date_beg_for_new = DateTime.Today.AddBusinessDays(-1);

            DateTime date_beg_actual = date_beg.HasValue ? (DateTime)date_beg : DateTime.Today.AddMonths(-2);
            DateTime date_end_actual = date_end.HasValue ? ((DateTime)date_end).AddDays(1) : DateTime.Today.AddDays(1);

            switch ((Enums.PrjBriefEnum)brief_type_id)
            {
                case Enums.PrjBriefEnum.DEV_CLAIM_MONITORING:
                    switch ((Enums.PrjBriefDateCutTypeEnum)date_cut_type_id)
                    {
                        case Enums.PrjBriefDateCutTypeEnum.CRT_DATE:
                            query = query.Where(ss => ss.crt_date >= date_beg_actual && ss.crt_date < date_end_actual);
                            break;
                        case Enums.PrjBriefDateCutTypeEnum.UPD_DATE:
                            query = query.Where(ss => ss.upd_date >= date_beg_actual && ss.upd_date < date_end_actual);
                            break;
                        default:
                            query = query.Where(ss => 1 == 0);
                            break;
                    }                                                                 
                    break;
                default:
                    query = query.Where(ss => 1 == 0);
                    break;
            }

            return query
                .Select(t1 => new PrjClaimViewModel()
                {
                    claim_id = t1.claim_id,
                    claim_name = t1.claim_name,
                    claim_num = t1.claim_num,
                    claim_text = t1.claim_text,
                    project_id = t1.project_id,
                    module_id = t1.module_id,
                    module_part_id = t1.module_part_id,
                    project_version_id = t1.project_version_id,
                    client_id = t1.client_id,
                    priority_id = t1.priority_id,
                    resp_user_id = t1.resp_user_id,
                    date_plan = t1.date_plan,
                    date_fact = t1.date_fact,
                    project_repair_version_id = t1.project_repair_version_id,
                    is_archive = t1.is_archive,
                    crt_date = t1.crt_date,
                    crt_user = t1.crt_user,
                    upd_date = t1.upd_date,
                    upd_user = t1.upd_user,
                    upd_num = t1.upd_num,
                    is_control = t1.is_control,
                    old_task_id = t1.old_task_id,
                    project_name = t1.project_name,
                    module_name = t1.module_name,
                    module_part_name = t1.module_part_name,
                    project_version_name = t1.project_version_name,
                    client_name = t1.client_name,
                    priority_name = t1.priority_name,
                    resp_user_name = t1.resp_user_name,
                    project_repair_version_name = t1.project_repair_version_name,
                    task_cnt = t1.task_cnt,
                    new_task_cnt = t1.new_task_cnt,
                    active_task_cnt = t1.active_task_cnt,
                    done_task_cnt = t1.done_task_cnt,
                    canceled_task_cnt = t1.canceled_task_cnt,
                    claim_state_type_id = t1.claim_state_type_id,
                    claim_state_type_name = t1.claim_state_type_name,
                    claim_state_type_templ = t1.claim_state_type_templ,
                    old_group_id = t1.old_group_id,
                    old_group_name = t1.old_group_name,
                    old_state_id = t1.old_state_id,
                    old_state_name = t1.old_state_name,
                    active_work_type_id = t1.active_work_type_id,
                    active_work_type_name = t1.active_work_type_name,
                    is_overdue = t1.is_overdue,
                    is_favor = false,
                    brief_state_type_id = t1.crt_date >= date_beg_for_new ? (int)Enums.PrjBriefStateTypeEnum.NEW : t1.brief_state_type_id,
                    brief_state_type_name = t1.crt_date >= date_beg_for_new ? "1. Новые" : t1.brief_state_type_name,
                    exec_user_name_list = t1.exec_user_name_list,
                    claim_type_id = t1.claim_type_id,
                    claim_type_name = t1.claim_type_name,
                    claim_stage_id = t1.claim_stage_id,
                    claim_stage_name = t1.claim_stage_name,
                    mess_cnt = t1.mess_cnt,
                    priority_rate = t1.priority_rate,
                    claim_type_rate = t1.claim_type_rate,
                    summary_rate = t1.summary_rate,
                    is_active_state = t1.is_active_state,
                    prj_id_list = t1.prj_id_list,
                    prj_name_list = t1.prj_name_list,
                    version_id = t1.version_id,
                    is_version_fixed = t1.is_version_fixed,
                    version_name = t1.version_name,
                    version_value = t1.version_value,
                    version_value_full = t1.version_value_full,
                    version_type_id = t1.version_type_id,
                    version_type_name = t1.version_type_name,
                    arc_date = t1.arc_date,
                    arc_user = t1.arc_user,
                });
        }

        public IQueryable<PrjClaimViewModel> GetList_Archive(string search)
        {
            var query = dbContext.vw_prj_claim.Where(ss => ss.is_archive);
            if (!String.IsNullOrWhiteSpace(search))
            {
                query = query.Where(ss => (ss.claim_name.Trim().ToLower().Contains(search.Trim().ToLower())) || (ss.claim_text.Trim().ToLower().Contains(search.Trim().ToLower())) || (ss.claim_num.Trim().ToLower().Contains(search.Trim().ToLower())));
            }

            return query.Select(ss => new PrjClaimViewModel()
            {
                claim_id = ss.claim_id,
                claim_name = ss.claim_name,
                claim_num = ss.claim_num,
                claim_text = ss.claim_text,
                is_archive = ss.is_archive,
                arc_date = ss.arc_date,
                arc_user = ss.arc_user,
            });
        }

        #endregion

        #region Insert

        public PrjClaimViewModel InsertClaim(PrjClaimViewModel itemAdd
            , List<PrjMessViewModel> mess_list, List<PrjTaskViewModel> task_list, List<PrjWorkViewModel> work_list, List<PrjTaskUserStateViewModel> task_user_state_list
            , ModelStateDictionary modelState)
        {
            try
            {

                if (itemAdd == null)
                {
                    modelState.AddModelError("", "Не заданы атрибуты задачи");
                    return null;
                }


                DateTime now = DateTime.Now;
                DateTime today = DateTime.Today;
                string user = currUser.CabUserName;
                string logMess = "";

                prj_claim prj_claim = new prj_claim();
                ModelMapper.Map<PrjClaimViewModel, prj_claim>(itemAdd, prj_claim);

                prj_claim prj_claim_last = dbContext.prj_claim.OrderByDescending(ss => ss.claim_num).FirstOrDefault();
                string claim_num = prj_claim_last != null ? prj_claim_last.claim_num : "0";
                int claim_num_int = int.Parse(Regex.Replace(claim_num, "[^0-9]", "")) + 1;

                prj_claim.claim_num = "AU-" + claim_num_int.ToExactLength(4, "0");
                prj_claim.is_archive = false;
                prj_claim.crt_date = now;
                prj_claim.crt_user = user;
                prj_claim.upd_date = now;
                prj_claim.upd_user = user;
                prj_claim.upd_num = 0;
                prj_claim.is_control = false;                

                string claim_text = itemAdd.claim_text;
                if (!String.IsNullOrWhiteSpace(itemAdd.claim_text_solution))
                {
                    claim_text += System.Environment.NewLine;
                    claim_text += ("Решение: " + itemAdd.claim_text_solution);
                }
                if (!String.IsNullOrWhiteSpace(itemAdd.claim_text_reprod))
                {
                    claim_text += System.Environment.NewLine;
                    claim_text += ("Воспроизведение: " + itemAdd.claim_text_reprod);
                }
                if (!String.IsNullOrWhiteSpace(itemAdd.claim_text_client))
                {
                    claim_text += System.Environment.NewLine;
                    claim_text += ("Клиент: " + itemAdd.claim_text_client);
                }
                prj_claim.claim_text = claim_text;

                dbContext.prj_claim.Add(prj_claim);

                prj_mess prj_mess = null;
                if ((mess_list != null) && (mess_list.Count > 0))
                {
                    List<PrjMessViewModel> mess_list_new = mess_list.Where(ss => ss.mess_id <= 0).ToList();
                    if ((mess_list_new != null) && (mess_list_new.Count > 0))
                    {
                        foreach (var mess_new in mess_list_new.OrderBy(ss => ss.crt_date).ToList())
                        {
                            prj_mess = new prj_mess();
                            prj_mess.mess = mess_new.mess;
                            prj_mess.mess_num = mess_new.mess_num;
                            prj_mess.user_id = mess_new.specified_user_id.GetValueOrDefault(0) > 0 ? mess_new.specified_user_id.GetValueOrDefault(0) : currUser.CabUserId;
                            prj_mess.prj_id = mess_new.prj_id;
                            //prj_mess.claim_id = mess_new.claim_id;
                            prj_mess.prj_claim = prj_claim;
                            prj_mess.task_id = mess_new.task_id;
                            prj_mess.work_id = mess_new.work_id;
                            prj_mess.crt_date = mess_new.crt_date;
                            prj_mess.crt_user = user;
                            prj_mess.upd_date = mess_new.crt_date;
                            prj_mess.upd_user = user;
                            prj_mess.upd_num = 0;
                            dbContext.prj_mess.Add(prj_mess);
                        }
                    }
                }

                prj_task prj_task = null;
                prj_work prj_work = null;                
                bool workListNotNull = (work_list != null) && (work_list.Count > 0);
                if ((task_list != null) && (task_list.Count > 0))
                {
                    List<PrjTaskViewModel> task_list_new = task_list.Where(ss => ss.task_id <= 0).ToList();
                    if ((task_list_new != null) && (task_list_new.Count > 0))
                    {
                        foreach (var task_new in task_list_new.OrderBy(ss => ss.task_num).ToList())
                        {
                            prj_task = new prj_task();
                            prj_task.prj_claim = prj_claim;
                            prj_task.task_num = task_new.task_num;
                            prj_task.task_text = task_new.task_text;
                            prj_task.date_plan = task_new.date_plan;
                            prj_task.date_fact = task_new.date_fact;
                            prj_task.crt_date = now;
                            prj_task.crt_user = user;
                            prj_task.upd_date = now;
                            prj_task.upd_user = user;
                            prj_task.upd_num = 0;
                            dbContext.prj_task.Add(prj_task);
                            //
                            if (workListNotNull)
                            {
                                List<PrjWorkViewModel> work_list_new = work_list.Where(ss => ss.task_num == prj_task.task_num && ss.work_id <= 0).ToList();
                                if (work_list_new != null)
                                {
                                    foreach (var work_new in work_list_new)
                                    {
                                        prj_work = new prj_work();
                                        prj_work.prj_claim = prj_claim;
                                        prj_work.prj_task = prj_task;
                                        prj_work.work_num = work_new.work_num;
                                        prj_work.work_type_id = work_new.work_type_id;
                                        prj_work.exec_user_id = work_new.exec_user_id;
                                        prj_work.state_type_id = work_new.state_type_id;
                                        prj_work.is_all_day = work_new.is_all_day;
                                        prj_work.date_send = work_new.date_send;
                                        prj_work.date_time_beg = work_new.is_all_day ? work_new.date_beg : work_new.date_time_beg;
                                        prj_work.date_time_end = work_new.is_all_day ? work_new.date_end : work_new.date_time_end;
                                        prj_work.date_beg = work_new.is_all_day ? work_new.date_beg : (work_new.date_time_beg.HasValue ? (DateTime?)((DateTime)work_new.date_time_beg).Date : null);
                                        prj_work.date_end = work_new.is_all_day ? work_new.date_end : (work_new.date_time_end.HasValue ? (DateTime?)((DateTime)work_new.date_time_end).Date : null);
                                        prj_work.is_accept = work_new.is_accept;
                                        prj_work.cost_plan = work_new.cost_plan;
                                        prj_work.cost_fact = work_new.cost_fact;
                                        prj_work.mess = work_new.mess;
                                        prj_work.crt_date = now;
                                        prj_work.crt_user = user;
                                        prj_work.upd_date = now;
                                        prj_work.upd_user = user;
                                        prj_work.upd_num = 0;
                                        //
                                        dbContext.prj_work.Add(prj_work);
                                    }
                                }
                            }
                        }
                    }
                }

                prj_claim_prj prj_claim_prj = null;                
                if (!String.IsNullOrWhiteSpace(itemAdd.prj_id_list))
                {
                    List<int> prjIds = itemAdd.prj_id_list.Split(',').Select(ss => int.Parse(ss)).ToList();
                    if ((prjIds != null) && (prjIds.Count > 0))
                    {
                        foreach (var prjId in prjIds)
                        {
                            prj_claim_prj = new prj_claim_prj();
                            prj_claim_prj.prj_claim = prj_claim;
                            prj_claim_prj.prj_id = prjId;
                            dbContext.prj_claim_prj.Add(prj_claim_prj);
                        }
                    }                    
                }

                dbContext.SaveChanges();

                var saved_task_list = dbContext.prj_task.Where(ss => ss.claim_id == prj_claim.claim_id).ToList();
                if ((task_user_state_list != null) && (saved_task_list != null) && (saved_task_list.Count > 0))
                {
                    foreach (var saved_task in saved_task_list)
                    {
                        var curr_task_user_state_list = task_user_state_list
                            .Where(ss => ss.task_num == saved_task.task_num && ss.user_state_type_id.GetValueOrDefault(0) > 0 && ss.user_state_user_id.GetValueOrDefault(0) > 0)
                            .OrderBy(ss => ss.user_state_crt_date).ToList();
                        if ((curr_task_user_state_list != null) && (curr_task_user_state_list.Count > 0))
                        {
                            foreach (var curr_task_user_state in curr_task_user_state_list)
                            {
                                UpdateTaskUserState(saved_task.task_id, (int)curr_task_user_state.user_state_type_id, (int)curr_task_user_state.user_state_user_id, currUser.CabUserId, null, true);
                            }
                        }                        
                    }
                }

                logMess = user + " " + ("создал").ToSex(currUser.IsMale) + " задачу " + prj_claim.claim_num;
                ToPrjLog(now, logMess, null, prj_claim.claim_id, null, null);

                string mess_notify = "Создана новая задача:";
                mess_notify += System.Environment.NewLine;
                mess_notify += prj_claim.claim_num;
                mess_notify += System.Environment.NewLine;
                mess_notify += prj_claim.claim_name;
                mess_notify += System.Environment.NewLine;
                mess_notify += "Автор изменений: " + currUser.CabUserName;
                mess_notify += System.Environment.NewLine;
                mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
                mess_notify += System.Environment.NewLine;

                notify notify = null;
                List<cab_user> usersNotify = dbContext.cab_user.Where(ss => ss.is_notify_prj).ToList();
                if ((usersNotify != null) && (usersNotify.Count > 0))
                {
                    foreach (var userNotify in usersNotify)
                    {
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = userNotify.user_id;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.EMAIL;
                        dbContext.notify.Add(notify);
                    }
                    dbContext.SaveChanges();
                }
                

                PrjClaimViewModel res = (PrjClaimViewModel)xGetItem(prj_claim.claim_id);
                UpdateClaimVersion(res.claim_id, res.is_active_state, res.is_version_fixed);

                ToLog("Добавление", (long)Enums.LogEventType.CABINET, new CabinetMvc.ViewModel.Adm.LogObject(res));
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public PrjClaimViewModel InsertClaim_FromTask(PrjClaimViewModel itemAdd, List<PrjMessViewModel> mess_list, List<PrjTaskViewModel> task_list, List<PrjWorkViewModel> work_list, List<PrjTaskUserStateViewModel> task_user_state_list, ModelStateDictionary modelState)
        {
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты задачи");
                return null;
            }

            if (itemAdd.claim_id <= 0)
            {
                modelState.AddModelError("", "Нельзя создать новую задачу не сохранив предыдущую");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string user = currUser.CabUserName;

            try
            {

                int source_claim_id = itemAdd.claim_id;
                string source_claim_num = itemAdd.claim_num;
                int source_task_id = 0;

                int task_num = itemAdd.create_claim_from_task.GetValueOrDefault(0);
                string claim_name = itemAdd.create_claim_name;
                string claim_text = itemAdd.create_claim_text;
                int new_task_num = 1;

                itemAdd.claim_id = 0;
                itemAdd.claim_name = claim_name;
                itemAdd.claim_text = claim_text;

                PrjTaskViewModel task_new = null;
                List<PrjTaskViewModel> task_list_new = new List<PrjTaskViewModel>();
                List<PrjWorkViewModel> work_list_new = new List<PrjWorkViewModel>();
                List<PrjWorkViewModel> work_list_new_db = new List<PrjWorkViewModel>();
                List<PrjMessViewModel> mess_list_new = new List<PrjMessViewModel>();
                List<PrjMessViewModel> mess_list_new_db = new List<PrjMessViewModel>();

                // task
                if (task_list != null)
                {
                    task_new = task_list.Where(ss => ss.task_num == task_num).FirstOrDefault();
                }

                if (task_new == null)
                {
                    task_new = dbContext.vw_prj_task
                        .Where(ss => ss.claim_id == source_claim_id && ss.task_num == task_num)
                        .ProjectTo<PrjTaskViewModel>()
                        .FirstOrDefault();
                }

                if (task_new == null)
                {
                    modelState.AddModelError("", "Не найдено исходное задание с номером " + task_num.ToString());
                    return null;
                }

                source_task_id = task_new.task_id;
                task_new.claim_id = 0;
                task_new.task_id = 0;
                task_new.task_num = new_task_num;                
                task_new.is_new = true;
                task_list_new.Add(task_new);

                // work
                if (work_list != null)
                {
                    work_list_new = work_list.Where(ss => ss.task_num == task_num).ToList();
                }

                work_list_new_db = dbContext.vw_prj_work.
                    Where(ss => ss.claim_id == source_claim_id && ss.task_num == task_num)
                    .ProjectTo<PrjWorkViewModel>()
                    .ToList();

                if (work_list_new_db != null)
                {
                    work_list_new.AddRange(work_list_new_db.Where(ss => !work_list_new.Where(tt => tt.work_num == ss.work_num).Any()));
                }

                foreach (var work_list_new_item in work_list_new)
                {
                    work_list_new_item.claim_id = 0;
                    work_list_new_item.task_id = 0;
                    work_list_new_item.work_id = 0;
                    work_list_new_item.task_num = new_task_num;
                    work_list_new_item.is_new = true;
                }

                // mess
                if (mess_list != null)
                {
                    mess_list_new = mess_list.Where(ss => ss.task_num == task_num).ToList();
                }

                mess_list_new_db = dbContext.vw_prj_mess.Where(ss => ss.claim_id == source_claim_id && ss.task_num == task_num)
                    .ProjectTo<PrjMessViewModel>()
                    .ToList();

                if (mess_list_new_db != null)
                {
                    mess_list_new.AddRange(mess_list_new_db.Where(ss => !mess_list_new.Where(tt => tt.mess_id == ss.mess_id).Any()));
                }

                foreach (var mess_list_new_item in mess_list_new)
                {
                    mess_list_new_item.claim_id = 0;
                    mess_list_new_item.task_id = 0;
                    mess_list_new_item.mess_id = 0;
                    mess_list_new_item.task_num = new_task_num;
                    mess_list_new_item.is_new = true;
                    mess_list_new_item.specified_user_id = mess_list_new_item.user_id;
                }

                PrjMessViewModel mess_new = new PrjMessViewModel();
                mess_new.claim_id = 0;
                mess_new.crt_date = now;
                mess_new.crt_user = user;
                mess_new.is_new = true;
                mess_new.mess_num = null;
                mess_new.upd_num = 1;
                mess_new.user_id = currUser.CabUserId;
                mess_new.user_name = user;
                mess_new.mess = "Задача создана из задания " + task_num.ToString() + " задачи " + source_claim_num;
                mess_list_new.Add(mess_new);

                var insertClaimRes = InsertClaim(itemAdd, mess_list_new, task_list_new, work_list_new, new List<PrjTaskUserStateViewModel>(), modelState);
                if ((insertClaimRes == null) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при переносе задания в задачу");
                    return null;
                }

                var task_new_saved = dbContext.prj_task.Where(ss => ss.claim_id == insertClaimRes.claim_id && ss.task_num == task_new.task_num).FirstOrDefault();
                if (task_new_saved != null)
                {
                    var prj_task_user_state_list = dbContext.prj_task_user_state.Where(ss => ss.task_id == source_task_id).ToList();
                    if ((prj_task_user_state_list != null) && (prj_task_user_state_list.Count > 0))
                    {
                        foreach (var prj_task_user_state in prj_task_user_state_list)
                        {
                            prj_task_user_state.task_id = task_new_saved.task_id;
                        }
                        //dbContext.SaveChanges();
                    }
                    var prj_user_group_item_list = dbContext.prj_user_group_item.Where(ss => ss.task_id == source_task_id && ss.item_type_id == (int)Enums.PrjUserGroupItemTypeEnum.TASK).ToList();
                    if ((prj_user_group_item_list != null) && (prj_user_group_item_list.Count > 0))
                    {
                        foreach (var prj_user_group_item in prj_user_group_item_list)
                        {
                            prj_user_group_item.task_id = task_new_saved.task_id;
                        }
                        //dbContext.SaveChanges();
                    }
                }                

                dbContext.prj_mess.RemoveRange(dbContext.prj_mess.Where(ss => ss.task_id == source_task_id));
                dbContext.prj_work.RemoveRange(dbContext.prj_work.Where(ss => ss.task_id == source_task_id));
                dbContext.prj_task.RemoveRange(dbContext.prj_task.Where(ss => ss.task_id == source_task_id));
                dbContext.SaveChanges();

                string logMess = user + " " + ("создал").ToSex(currUser.IsMale) + " задачу " + insertClaimRes.claim_num + " из задания " + task_num.ToString() + " задачи " + source_claim_num;
                ToPrjLog(now, logMess, null, insertClaimRes.claim_id, null, null);

                PrjClaimViewModel res = (PrjClaimViewModel)xGetItem(insertClaimRes.claim_id);
                ToLog("Добавление", (long)Enums.LogEventType.CABINET, new CabinetMvc.ViewModel.Adm.LogObject(res));
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        #endregion

        #region Update

        public PrjClaimViewModel UpdateClaim(PrjClaimViewModel itemEdit
            , List<PrjMessViewModel> mess_list, List<PrjTaskViewModel> task_list, List<PrjWorkViewModel> work_list, List<int> work_del_list, List<PrjTaskUserStateViewModel> task_user_state_list
            , ModelStateDictionary modelState)
        {
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры задачи");
                return null;
            }

            prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == itemEdit.claim_id).FirstOrDefault();
            if (prj_claim == null)
            {
                modelState.AddModelError("", "Не найдены параметры задачи");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string user = currUser.CabUserName;
            bool isMale = currUser.IsMale;
            List<string> logMessages = new List<string>();                        
            crm_project crm_project = null;            
            crm_module crm_module = null;
            crm_module_part crm_module_part = null;            
            crm_project_version crm_project_version = null;
            crm_client crm_client = null;
            crm_priority crm_priority = null;
            cab_user cab_user = null;
            prj_work_state_type prj_work_state_type = null;
            prj_work_state_type prj_work_state_type_before = null;
            prj_work_type prj_work_type = null;
            prj_work_type prj_work_type_before = null;
            prj_claim_type prj_claim_type = null;
            prj_claim_stage prj_claim_stage = null;
            prj_project_version prj_project_version = null;


            PrjClaimViewModel oldModel = new PrjClaimViewModel();
            ModelMapper.Map<prj_claim, PrjClaimViewModel>(prj_claim, oldModel);
            modelBeforeChanges = oldModel;

            if (prj_claim.claim_name != itemEdit.claim_name)
            {
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " название на " + itemEdit.claim_name);
                prj_claim.claim_name = itemEdit.claim_name;
            }                
            
            if (prj_claim.claim_text != itemEdit.claim_text)
            {
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " формулировку");
                prj_claim.claim_text = itemEdit.claim_text;
            }

            if (prj_claim.project_id != itemEdit.project_id)
            {
                crm_project = dbContext.crm_project.Where(ss => ss.project_id == itemEdit.project_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " проект на " + (crm_project != null ? crm_project.project_name : "-"));
                prj_claim.project_id = itemEdit.project_id;
            }

            if (prj_claim.module_id != itemEdit.module_id)
            {
                crm_module = dbContext.crm_module.Where(ss => ss.module_id == itemEdit.module_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " модуль на " + (crm_module != null ? crm_module.module_name : "-"));
                prj_claim.module_id = itemEdit.module_id;
            }

            if (prj_claim.module_part_id != itemEdit.module_part_id)
            {
                crm_module_part = dbContext.crm_module_part.Where(ss => ss.module_part_id == itemEdit.module_part_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " раздел модуля на " + (crm_module_part != null ? crm_module_part.module_part_name : "-"));
                prj_claim.module_part_id = itemEdit.module_part_id;
            }

            if (prj_claim.project_version_id != itemEdit.project_version_id)
            {
                crm_project_version = dbContext.crm_project_version.Where(ss => ss.project_version_id == itemEdit.project_version_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " версию проекта на " + (crm_project_version != null ? crm_project_version.project_version_name : "-"));
                prj_claim.project_version_id = itemEdit.project_version_id;
            }

            if (prj_claim.client_id != itemEdit.client_id)
            {
                crm_client = dbContext.crm_client.Where(ss => ss.client_id == itemEdit.client_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " истгочник на " + (crm_client != null ? crm_client.client_name: "-"));
                prj_claim.client_id = itemEdit.client_id;
            }

            if (prj_claim.priority_id != itemEdit.priority_id)
            {
                crm_priority = dbContext.crm_priority.Where(ss => ss.priority_id == itemEdit.priority_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " приоритет на " + (crm_priority != null ? crm_priority.priority_name : "-"));
                prj_claim.priority_id = itemEdit.priority_id;
            }
            
            if (prj_claim.resp_user_id != itemEdit.resp_user_id)
            {
                cab_user = dbContext.cab_user.Where(ss => ss.user_id == itemEdit.resp_user_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " ответственного на " + (cab_user != null ? cab_user.user_name : "-"));
                prj_claim.resp_user_id = itemEdit.resp_user_id;
            }

            if (prj_claim.date_plan != itemEdit.date_plan)
            {
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " плановую дату на " + (itemEdit.date_plan != null ? ((DateTime)itemEdit.date_plan).ToString("dd.MM.yyyy") : "-"));
                prj_claim.date_plan = itemEdit.date_plan;
            }
            
            if (prj_claim.date_fact != itemEdit.date_fact)
            {
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " фактическую дату на " + (itemEdit.date_fact != null ? ((DateTime)itemEdit.date_fact).ToString("dd.MM.yyyy") : "-"));
                prj_claim.date_fact = itemEdit.date_fact;
            }

            if ((prj_claim.version_id != itemEdit.version_id) && (itemEdit.version_id > 0))
            {
                prj_project_version = dbContext.prj_project_version.Where(ss => ss.version_id == itemEdit.version_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " версию исправления на " + (prj_project_version != null ? prj_project_version.version_name : "-"));
                prj_claim.version_id = itemEdit.version_id;
            }

            if (prj_claim.claim_type_id != itemEdit.claim_type_id)
            {
                prj_claim_type = dbContext.prj_claim_type.Where(ss => ss.claim_type_id == itemEdit.claim_type_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " тип на " + (prj_claim_type != null ? prj_claim_type.claim_type_name : "-"));
                prj_claim.claim_type_id = itemEdit.claim_type_id;
            }

            if (prj_claim.claim_stage_id != itemEdit.claim_stage_id)
            {
                prj_claim_stage = dbContext.prj_claim_stage.Where(ss => ss.claim_stage_id == itemEdit.claim_stage_id).FirstOrDefault();
                logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " этап на " + (prj_claim_stage != null ? prj_claim_stage.claim_stage_name : "-"));
                prj_claim.claim_stage_id = itemEdit.claim_stage_id;
            }
            
            prj_claim.upd_date = now;
            prj_claim.upd_user = user;
            prj_claim.upd_num = prj_claim.upd_num + 1;

            prj_mess prj_mess = null;
            bool messListNotNull = (mess_list != null) && (mess_list.Count > 0);
            if (messListNotNull)
            {
                // только комменты не по заданиям
                List<PrjMessViewModel> mess_list_new = mess_list.Where(ss => ss.mess_id <= 0 && !ss.task_id.HasValue).ToList();
                if (mess_list_new != null)
                {
                    foreach (var mess_new in mess_list_new)
                    {
                        prj_mess = new prj_mess();
                        prj_mess.mess = mess_new.mess;
                        prj_mess.mess_num = mess_new.mess_num;
                        prj_mess.user_id = currUser.CabUserId;
                        prj_mess.prj_id = mess_new.prj_id;
                        prj_mess.claim_id = mess_new.claim_id;
                        prj_mess.task_id = mess_new.task_id;
                        prj_mess.work_id = mess_new.work_id;
                        prj_mess.crt_date = mess_new.crt_date;
                        prj_mess.crt_user = user;
                        prj_mess.upd_date = now;
                        prj_mess.upd_user = user;
                        prj_mess.upd_num = 0;
                        dbContext.prj_mess.Add(prj_mess);
                        logMessages.Add(user + " " + ("добавил").ToSex(isMale) + " новый комментарий");
                    }
                }
                //
                List<PrjMessViewModel> mess_list_existing = mess_list.Where(ss => ss.mess_id > 0).ToList();
                if (mess_list_existing != null)
                {
                    foreach (var mess_existing in mess_list_existing)
                    {
                        prj_mess = dbContext.prj_mess.Where(ss => ss.mess_id == mess_existing.mess_id).FirstOrDefault();
                        if (prj_mess == null)
                            continue;
                        prj_mess.mess = mess_existing.mess;
                        prj_mess.upd_date = now;
                        prj_mess.upd_user = user;
                        prj_mess.upd_num = prj_mess.upd_num + 1;                        
                    }
                }
            }


            prj_task prj_task = null;
            prj_work prj_work = null;            
            bool workListNotNull = (work_list != null) && (work_list.Count > 0);
            if ((task_list != null) && (task_list.Count > 0))
            {
                List<PrjTaskViewModel> task_list_new = task_list.Where(ss => ss.task_id <= 0).ToList();
                if ((task_list_new != null) && (task_list_new.Count > 0))
                {
                    foreach (var task_new in task_list_new.OrderBy(ss => ss.task_num).ToList())
                    {
                        prj_task = new prj_task();
                        prj_task.prj_claim = prj_claim;
                        prj_task.task_num = task_new.task_num;
                        prj_task.task_text = task_new.task_text;                        
                        prj_task.date_plan = task_new.date_plan;
                        prj_task.date_fact = task_new.date_fact;
                        prj_task.crt_date = now;
                        prj_task.crt_user = user;
                        prj_task.upd_date = now;
                        prj_task.upd_user = user;
                        prj_task.upd_num = 0;
                        dbContext.prj_task.Add(prj_task);
                        logMessages.Add(user + " " + ("добавил").ToSex(isMale) + " задание " + prj_task.task_num);
                        //
                        if (workListNotNull)
                        {
                            List<PrjWorkViewModel> work_list_new = work_list.Where(ss => ss.task_num == prj_task.task_num && ss.work_id <= 0).ToList();
                            if (work_list_new != null)
                            {
                                foreach (var work_new in work_list_new)
                                {
                                    prj_work = new prj_work();
                                    prj_work.prj_claim = prj_claim;
                                    prj_work.prj_task = prj_task;
                                    prj_work.work_num = work_new.work_num;
                                    prj_work.work_type_id = work_new.work_type_id;
                                    prj_work.exec_user_id = work_new.exec_user_id;
                                    prj_work.state_type_id = work_new.state_type_id;
                                    prj_work.is_all_day = work_new.is_all_day;
                                    prj_work.date_send = work_new.date_send;
                                    prj_work.date_time_beg = work_new.is_all_day ? work_new.date_beg : work_new.date_time_beg;
                                    prj_work.date_time_end = work_new.is_all_day ? work_new.date_end : work_new.date_time_end;
                                    prj_work.date_beg = work_new.is_all_day ? work_new.date_beg : (work_new.date_time_beg.HasValue ? (DateTime?)((DateTime)work_new.date_time_beg).Date : null);
                                    prj_work.date_end = work_new.is_all_day ? work_new.date_end : (work_new.date_time_end.HasValue ? (DateTime?)((DateTime)work_new.date_time_end).Date : null);
                                    prj_work.is_accept = work_new.is_accept;
                                    prj_work.cost_plan = work_new.cost_plan;
                                    prj_work.cost_fact = work_new.cost_fact;
                                    prj_work.mess = work_new.mess;
                                    prj_work.crt_date = now;
                                    prj_work.crt_user = user;
                                    prj_work.upd_date = now;
                                    prj_work.upd_user = user;
                                    prj_work.upd_num = 0;
                                    //
                                    dbContext.prj_work.Add(prj_work);
                                    //
                                    logMessages.Add(user + " " + ("добавил").ToSex(isMale) + " работу " + prj_work.work_num + " по заданию " + prj_task.task_num);
                                    //
                                }
                            }
                        }
                        //
                        if (messListNotNull)
                        {
                            // только комменты по заданиям
                            List<PrjMessViewModel> mess_list_new = mess_list.Where(ss => ss.mess_id <= 0 && ss.task_num == prj_task.task_num).ToList();
                            if (mess_list_new != null)
                            {
                                foreach (var mess_new in mess_list_new)
                                {
                                    prj_mess = new prj_mess();
                                    prj_mess.mess = mess_new.mess;
                                    prj_mess.mess_num = mess_new.mess_num;
                                    prj_mess.user_id = currUser.CabUserId;
                                    prj_mess.prj_id = mess_new.prj_id;
                                    prj_mess.claim_id = mess_new.claim_id;
                                    prj_mess.prj_task = prj_task;
                                    prj_mess.work_id = null;
                                    prj_mess.crt_date = mess_new.crt_date;
                                    prj_mess.crt_user = user;
                                    prj_mess.upd_date = now;
                                    prj_mess.upd_user = user;
                                    prj_mess.upd_num = 0;
                                    dbContext.prj_mess.Add(prj_mess);
                                    logMessages.Add(user + " " + ("добавил").ToSex(isMale) + " новый комментарий");
                                }
                            }
                        }
                    }
                }
                //
                List<PrjTaskViewModel> task_list_existing = task_list.Where(ss => ss.task_id > 0).ToList();
                if ((task_list_existing != null) && (task_list_existing.Count > 0))
                {
                    foreach (var task_existing in task_list_existing.OrderBy(ss => ss.task_num).ToList())
                    {
                        prj_task = dbContext.prj_task.Where(ss => ss.task_id == task_existing.task_id).FirstOrDefault();
                        if (prj_task == null)
                            continue;
                        
                        if (prj_task.task_text != task_existing.task_text)
                        {
                            logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " формулировку задания " + prj_task.task_num);
                            prj_task.task_text = task_existing.task_text;
                        }
                        
                        if (prj_task.date_plan != task_existing.date_plan)
                        {
                            logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " плановую дату задания на " + (task_existing.date_plan.HasValue ? ((DateTime)task_existing.date_plan).ToString("dd.MM.yyyy") : "-"));
                            prj_task.date_plan = task_existing.date_plan;
                        }
                        
                        prj_task.upd_date = now;
                        prj_task.upd_user = user;
                        prj_task.upd_num = prj_task.upd_num + 1;                        
                        //
                        if (workListNotNull)
                        {
                            List<PrjWorkViewModel> work_list_new = work_list.Where(ss => ss.task_num == prj_task.task_num && ss.work_id <= 0).ToList();
                            if (work_list_new != null)
                            {
                                foreach (var work_new in work_list_new)
                                {
                                    prj_work = new prj_work();
                                    prj_work.prj_claim = prj_claim;
                                    prj_work.prj_task = prj_task;
                                    prj_work.work_num = work_new.work_num;
                                    prj_work.work_type_id = work_new.work_type_id;
                                    prj_work.exec_user_id = work_new.exec_user_id;
                                    prj_work.state_type_id = work_new.state_type_id;
                                    prj_work.is_all_day = work_new.is_all_day;
                                    prj_work.date_send = work_new.date_send;
                                    prj_work.date_time_beg = work_new.is_all_day ? work_new.date_beg : work_new.date_time_beg;
                                    prj_work.date_time_end = work_new.is_all_day ? work_new.date_end : work_new.date_time_end;
                                    prj_work.date_beg = work_new.is_all_day ? work_new.date_beg : (work_new.date_time_beg.HasValue ? (DateTime?)((DateTime)work_new.date_time_beg).Date : null);
                                    prj_work.date_end = work_new.is_all_day ? work_new.date_end : (work_new.date_time_end.HasValue ? (DateTime?)((DateTime)work_new.date_time_end).Date : null);
                                    prj_work.is_accept = work_new.is_accept;
                                    prj_work.cost_plan = work_new.cost_plan;
                                    prj_work.cost_fact = work_new.cost_fact;
                                    prj_work.mess = work_new.mess;
                                    prj_work.crt_date = now;
                                    prj_work.crt_user = user;
                                    prj_work.upd_date = now;
                                    prj_work.upd_user = user;
                                    prj_work.upd_num = 0;
                                    //
                                    dbContext.prj_work.Add(prj_work);
                                    //
                                    logMessages.Add(user + " " + ("добавил").ToSex(isMale) + " работу " + prj_work.work_num + " по заданию " + prj_task.task_num);
                                }
                            }
                        }
                        //
                        if (messListNotNull)
                        {
                            // только комменты по заданиям
                            List<PrjMessViewModel> mess_list_new = mess_list.Where(ss => ss.mess_id <= 0 && ss.task_num == prj_task.task_num).ToList();
                            if (mess_list_new != null)
                            {
                                foreach (var mess_new in mess_list_new)
                                {
                                    prj_mess = new prj_mess();
                                    prj_mess.mess = mess_new.mess;
                                    prj_mess.mess_num = mess_new.mess_num;
                                    prj_mess.user_id = currUser.CabUserId;
                                    prj_mess.prj_id = mess_new.prj_id;
                                    prj_mess.claim_id = mess_new.claim_id;
                                    prj_mess.prj_task = prj_task;
                                    prj_mess.work_id = null;
                                    prj_mess.crt_date = mess_new.crt_date;
                                    prj_mess.crt_user = user;
                                    prj_mess.upd_date = now;
                                    prj_mess.upd_user = user;
                                    prj_mess.upd_num = 0;
                                    dbContext.prj_mess.Add(prj_mess);
                                    logMessages.Add(user + " " + ("добавил").ToSex(isMale) + " новый комментарий");
                                }
                            }
                        }
                    }
                }
            }

            if (task_list == null)
                task_list = new List<PrjTaskViewModel>();

            if (workListNotNull)
            {
                List<PrjWorkViewModel> work_list_new = work_list.Where(ss => ss.work_id <= 0 && !(task_list.Where(tt => tt.task_num == ss.task_num).Any())).ToList();
                if (work_list_new != null)
                {
                    foreach (var work_new in work_list_new)
                    {
                        prj_task = dbContext.prj_task.Where(ss => ss.claim_id == prj_claim.claim_id && ss.task_num == work_new.task_num).FirstOrDefault();
                        if (prj_task == null)
                            continue;
                        prj_work = new prj_work();
                        prj_work.prj_claim = prj_claim;
                        prj_work.prj_task = prj_task;
                        prj_work.work_num = work_new.work_num;
                        prj_work.work_type_id = work_new.work_type_id;
                        prj_work.exec_user_id = work_new.exec_user_id;
                        prj_work.state_type_id = work_new.state_type_id;
                        prj_work.is_all_day = work_new.is_all_day;
                        prj_work.date_send = work_new.date_send;
                        prj_work.date_time_beg = work_new.is_all_day ? work_new.date_beg : work_new.date_time_beg;
                        prj_work.date_time_end = work_new.is_all_day ? work_new.date_end : work_new.date_time_end;
                        prj_work.date_beg = work_new.is_all_day ? work_new.date_beg : (work_new.date_time_beg.HasValue ? (DateTime?)((DateTime)work_new.date_time_beg).Date : null);
                        prj_work.date_end = work_new.is_all_day ? work_new.date_end : (work_new.date_time_end.HasValue ? (DateTime?)((DateTime)work_new.date_time_end).Date : null);
                        prj_work.is_accept = work_new.is_accept;
                        prj_work.cost_plan = work_new.cost_plan;
                        prj_work.cost_fact = work_new.cost_fact;
                        prj_work.mess = work_new.mess;
                        prj_work.crt_date = now;
                        prj_work.crt_user = user;
                        prj_work.upd_date = now;
                        prj_work.upd_user = user;
                        prj_work.upd_num = 0;
                        //
                        dbContext.prj_work.Add(prj_work);
                        //
                        logMessages.Add(user + " " + ("добавил").ToSex(isMale) + " работу " + prj_work.work_num + " по заданию " + prj_task.task_num);
                    }
                }
                //
                List<PrjWorkViewModel> work_list_existing = work_list.Where(ss => ss.work_id > 0).ToList();
                if (work_list_existing != null)
                {
                    foreach (var work_existing in work_list_existing)
                    {
                        prj_work = dbContext.prj_work.Where(ss => ss.work_id == work_existing.work_id).FirstOrDefault();
                        if (prj_work == null)
                            continue;

                        DateTime? work_date_time_beg = work_existing.is_all_day ? work_existing.date_beg : work_existing.date_time_beg;
                        DateTime? work_date_time_end = work_existing.is_all_day ? work_existing.date_end : work_existing.date_time_end;
                        DateTime? work_date_beg = work_existing.is_all_day ? work_existing.date_beg : (work_existing.date_time_beg.HasValue ? (DateTime?)((DateTime)work_existing.date_time_beg).Date : null);
                        DateTime? work_date_end = work_existing.is_all_day ? work_existing.date_end : (work_existing.date_time_end.HasValue ? (DateTime?)((DateTime)work_existing.date_time_end).Date : null);
                        int work_exec_user_id = work_existing.exec_user_id;                        

                        if (prj_work.is_accept != work_existing.is_accept)
                        {
                            if (work_existing.is_accept)
                            {
                                logMessages.Add(user + " " + ("принял").ToSex(isMale) + " работу " + prj_work.work_num.ToString() + " по заданию " + work_existing.task_num.ToString());
                            }
                            else
                            {
                                logMessages.Add(user + " " + ("не принял").ToSex(isMale) + " работу " + prj_work.work_num.ToString() + " по заданию " + work_existing.task_num.ToString());
                            }
                            prj_work.is_accept = work_existing.is_accept;
                        }
                        
                        if (prj_work.state_type_id != work_existing.state_type_id)
                        {
                            prj_work_state_type = dbContext.prj_work_state_type.Where(ss => ss.state_type_id == work_existing.state_type_id).FirstOrDefault();
                            prj_work_state_type_before = dbContext.prj_work_state_type.Where(ss => ss.state_type_id == prj_work.state_type_id).FirstOrDefault();
                            logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " статус работы " + prj_work.work_num.ToString() + " по заданию " + work_existing.task_num.ToString() + " с " + (prj_work_state_type_before != null ? prj_work_state_type_before.state_type_name : "-") + " на " + (prj_work_state_type != null ? prj_work_state_type.state_type_name : "-"));
                            prj_work.state_type_id = work_existing.state_type_id;
                        }

                        if (prj_work.exec_user_id != work_existing.exec_user_id)
                        {
                            cab_user = dbContext.cab_user.Where(ss => ss.user_id == work_existing.exec_user_id).FirstOrDefault();
                            logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " исполнителя работы " + prj_work.work_num.ToString() + " по заданию " + work_existing.task_num.ToString() + " на " + (cab_user != null ? cab_user.user_name : "-"));                            
                            // !!!
                            // смена исполнителя
                            if ((prj_work.state_type_id == (int)Enums.PrjWorkStateTypeEnum.DONE)
                                || (prj_work.state_type_id == (int)Enums.PrjWorkStateTypeEnum.CANCELED))
                            {
                                modelState.AddModelError("", "Нельзя изменить исполнителя неактивной работы");
                                return null;
                            }
                            //
                            int old_exec_user_id = prj_work.exec_user_id;

                            prj_task_user_state prj_task_user_state = dbContext.prj_task_user_state.Where(ss => ss.task_id == prj_work.task_id && ss.user_id == old_exec_user_id && ss.is_current).FirstOrDefault();
                            prj_user_group_item prj_user_group_item_existing = (from t1 in dbContext.prj_user_group
                                                                                from t2 in dbContext.prj_user_group_item
                                                                                where t1.group_id == t2.group_id
                                                                                && t1.user_id == old_exec_user_id
                                                                                && t2.task_id == prj_work.task_id
                                                                                && t1.fixed_kind.HasValue
                                                                                select t2)
                                                                                .FirstOrDefault();

                            prj_work.exec_user_id = work_existing.exec_user_id;

                            if (prj_task_user_state != null)
                            {
                                if (prj_task_user_state.state_type_id == (int)Enums.PrjTaskUserStateTypeEnum.RECEIVED_TO_WORK)
                                    prj_work.is_accept = false;
                                prj_task_user_state.user_id = prj_work.exec_user_id;
                                prj_task_user_state.upd_date = now;
                                prj_task_user_state.upd_user = user;
                                prj_task_user_state.state_type_id = prj_task_user_state.state_type_id == (int)Enums.PrjTaskUserStateTypeEnum.RECEIVED_TO_WORK ? (int)Enums.PrjTaskUserStateTypeEnum.SENT_TO_WORK : prj_task_user_state.state_type_id;

                                List<prj_task_user_state> prj_task_user_state_current_existing = dbContext.prj_task_user_state.Where(ss => ss.task_id == prj_task_user_state.task_id && ss.user_id == prj_task_user_state.user_id && ss.is_current && ss.item_id != prj_task_user_state.item_id)
                                    .ToList();
                                if (prj_task_user_state_current_existing != null)
                                {
                                    foreach (var prj_task_user_state_current_existing_item in prj_task_user_state_current_existing)
                                    {
                                        prj_task_user_state_current_existing_item.is_current = false;
                                        prj_task_user_state_current_existing_item.upd_date = now;
                                        prj_task_user_state_current_existing_item.upd_user = user;
                                    }
                                }

                            }

                            if (prj_user_group_item_existing != null)
                            {                                                        
                                prj_user_group prj_user_group_existing = dbContext.prj_user_group.Where(ss => ss.group_id == prj_user_group_item_existing.group_id).FirstOrDefault();
                                int? fixedKind = prj_user_group_existing.fixed_kind == (int?)Enums.PrjUserGroupFixedKindEnum.RECEIVED ? (int?)Enums.PrjUserGroupFixedKindEnum.SENT : prj_user_group_existing.fixed_kind;
                                prj_user_group prj_user_group_new = dbContext.prj_user_group.Where(ss => ss.user_id == prj_work.exec_user_id && ss.fixed_kind == fixedKind).FirstOrDefault();
                                if (prj_user_group_new != null)
                                {
                                    prj_user_group_item prj_user_group_item_new = new prj_user_group_item();
                                    prj_user_group_item_new.group_id = prj_user_group_new.group_id;
                                    prj_user_group_item_new.item_type_id = (int)Enums.PrjUserGroupItemTypeEnum.TASK;
                                    prj_user_group_item_new.task_id = prj_work.task_id;
                                    dbContext.prj_user_group_item.Add(prj_user_group_item_new);
                                }
                                //
                                dbContext.prj_user_group_item.Remove(prj_user_group_item_existing);
                            }
                            //                            
                        }

                        if (prj_work.cost_plan != work_existing.cost_plan)
                        {
                            logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " ТЗ (план) работы " + prj_work.work_num.ToString() + " по заданию " + work_existing.task_num.ToString() + " на " + (work_existing.cost_plan != null ? work_existing.cost_plan.ToString() : "-"));
                            prj_work.cost_plan = work_existing.cost_plan;
                        }

                        if (prj_work.cost_fact != work_existing.cost_fact)
                        {
                            logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " ТЗ (факт) работы " + prj_work.work_num.ToString() + " по заданию " + work_existing.task_num.ToString() + " на " + (work_existing.cost_fact != null ? work_existing.cost_fact.ToString() : "-"));
                            prj_work.cost_fact = work_existing.cost_fact;
                        }

                        if ((prj_work.date_time_beg != work_date_time_beg) || (prj_work.date_time_end != work_date_time_end) || (prj_work.date_beg != work_date_beg) || (prj_work.date_end != work_date_end))
                        {
                            string prj_work_date_period =
                                (work_date_time_beg != null ? ((DateTime)work_date_time_beg).ToString("dd.MM.yyyy") : "-")
                                + (work_date_time_end != null ? ((" - " + ((DateTime)work_date_time_end).ToString("dd.MM.yyyy"))) : "")
                                ;
                            logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " период работы " + prj_work.work_num.ToString() + " по заданию " + work_existing.task_num.ToString() + " на " + prj_work_date_period);
                            prj_work.date_beg = work_date_beg;
                            prj_work.date_end = work_date_end.HasValue ? work_date_end : work_date_beg;
                            prj_work.date_time_beg = work_date_time_beg;
                            prj_work.date_time_end = work_date_time_end.HasValue ? work_date_time_end : work_date_time_beg;
                        }

                        if (prj_work.work_type_id != work_existing.work_type_id)
                        {
                            prj_work_type = dbContext.prj_work_type.Where(ss => ss.work_type_id == work_existing.work_type_id).FirstOrDefault();
                            prj_work_type_before = dbContext.prj_work_type.Where(ss => ss.work_type_id == prj_work.work_type_id).FirstOrDefault();
                            logMessages.Add(user + " " + ("изменил").ToSex(isMale) + " тип работы " + prj_work.work_num.ToString() + " по заданию " + work_existing.task_num.ToString() + " с " + (prj_work_type_before != null ? prj_work_type_before.work_type_name : "-") + " на " + (prj_work_type != null ? prj_work_type.work_type_name : "-"));
                            prj_work.work_type_id = work_existing.work_type_id;
                        }

                        prj_work.is_all_day = work_existing.is_all_day;
                        prj_work.date_send = work_existing.date_send;

                        prj_work.work_num = work_existing.work_num;
                        prj_work.upd_date = now;
                        prj_work.upd_user = user;
                        prj_work.upd_num = prj_work.upd_num + 1;
                        prj_work.mess = work_existing.mess;
                    }
                }
            }

            // удаленные работы
            if ((work_del_list != null) && (work_del_list.Count > 0))
            {
                List<prj_work> work_list_for_del = dbContext.prj_work.Where(ss => ss.claim_id == prj_claim.claim_id && work_del_list.Contains(ss.work_id)).ToList();
                if (work_list_for_del != null)
                {
                    foreach (var work_for_del in work_list_for_del)
                    {
                        logMessages.Add(user + " " + ("удалил").ToSex(isMale) + " работу " + work_for_del.work_num.ToString());                        
                        dbContext.prj_work.Remove(work_for_del);
                    }
                }
            }

            prj_claim_prj prj_claim_prj = null;
            List<int> prjIds_existing =  dbContext.prj_claim_prj.Where(ss => ss.claim_id == prj_claim.claim_id).Select(ss => ss.prj_id).ToList();
            List<int> prjIds = new List<int>();
            if (!String.IsNullOrWhiteSpace(itemEdit.prj_id_list))
                prjIds = itemEdit.prj_id_list.Split(',').Select(ss => int.Parse(ss)).ToList();
            var prjIds_equals = (prjIds_existing.All(prjIds.Contains) && (prjIds_existing.Count == prjIds.Count));

            if (!prjIds_equals)
            {
                dbContext.prj_claim_prj.RemoveRange(dbContext.prj_claim_prj.Where(ss => ss.claim_id == prj_claim.claim_id));
                if ((prjIds != null) && (prjIds.Count > 0))
                {
                    foreach (var prjId in prjIds)
                    {
                        prj_claim_prj = new prj_claim_prj();
                        prj_claim_prj.prj_claim = prj_claim;
                        prj_claim_prj.prj_id = prjId;
                        dbContext.prj_claim_prj.Add(prj_claim_prj);
                    }
                }
            }

            dbContext.SaveChanges();

            /*
            bool taskUserStateUpdated = false;
            var saved_task_list = dbContext.prj_task.Where(ss => ss.claim_id == prj_claim.claim_id).ToList();
            if ((saved_task_list != null) && (saved_task_list.Count > 0))
            {
                foreach (var saved_task in saved_task_list)
                {
                    var task = task_list.Where(ss => ss.task_num == saved_task.task_num && ss.user_state_type_id.GetValueOrDefault(0) > 0 && ss.user_state_user_id.GetValueOrDefault(0) > 0).FirstOrDefault();
                    if (task == null)
                        continue;
                    UpdateTaskUserState(saved_task.task_id, (int)task.user_state_type_id, (int)task.user_state_user_id, currUser.CabUserId, null);
                    saved_task.upd_date = now;
                    saved_task.upd_user = user;
                    saved_task.upd_num = saved_task.upd_num + 1;
                    taskUserStateUpdated = true;
                }
            }
            */

            bool taskUserStateUpdated = false;
            var saved_task_list = dbContext.prj_task.Where(ss => ss.claim_id == prj_claim.claim_id).ToList();
            if ((task_user_state_list != null) && (saved_task_list != null) && (saved_task_list.Count > 0))
            {
                foreach (var saved_task in saved_task_list)
                {
                    var curr_task_user_state_list = task_user_state_list
                        .Where(ss => ss.task_num == saved_task.task_num && ss.user_state_type_id.GetValueOrDefault(0) > 0 && ss.user_state_user_id.GetValueOrDefault(0) > 0)
                        .OrderBy(ss => ss.user_state_crt_date).ToList();
                    if ((curr_task_user_state_list != null) && (curr_task_user_state_list.Count > 0))
                    {
                        foreach (var curr_task_user_state in curr_task_user_state_list)
                        {
                            UpdateTaskUserState(saved_task.task_id, (int)curr_task_user_state.user_state_type_id, (int)curr_task_user_state.user_state_user_id, currUser.CabUserId, null, true);
                        }
                        saved_task.upd_date = now;
                        saved_task.upd_user = user;
                        saved_task.upd_num = saved_task.upd_num + 1;
                        taskUserStateUpdated = true;
                    }
                }
            }

            if (taskUserStateUpdated)
                dbContext.SaveChanges();
           
            foreach (var logMess in logMessages)
            {
                ToPrjLog(now, logMess, null, prj_claim.claim_id, null, null);
            }
            
            PrjClaimViewModel res = (PrjClaimViewModel)xGetItem(prj_claim.claim_id);
            UpdateClaimVersion(res.claim_id, res.is_active_state, res.is_version_fixed);

            ToLog("Изменение", (long)Enums.LogEventType.CABINET, modelBeforeChanges == null ? null : new CabinetMvc.ViewModel.Adm.LogObject(modelBeforeChanges), new CabinetMvc.ViewModel.Adm.LogObject(res));
            return res;
        }

        #endregion

        #region Delete

        public bool DeleteClaim(int claim_id, ModelStateDictionary modelState)
        {
            try
            {
                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;
                prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == claim_id && !ss.is_archive).FirstOrDefault();
                if (prj_claim != null)
                {
                    prj_claim.is_archive = true;
                    prj_claim.upd_date = now;
                    prj_claim.upd_user = user;
                    prj_claim.upd_num = prj_claim.upd_num + 1;
                    prj_claim.arc_date = now;
                    prj_claim.arc_user = user;
                    dbContext.SaveChanges();

                    string logMess = user + " " + ("удалил").ToSex(currUser.IsMale) + " задачу " + prj_claim.claim_num;
                    ToPrjLog(now, logMess, null, claim_id, null, null);
                }
                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region Restore

        public bool RestoreClaim(int claim_id, ModelStateDictionary modelState)
        {
            try
            {
                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;
                prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == claim_id && ss.is_archive).FirstOrDefault();
                if (prj_claim != null)
                {
                    prj_claim.is_archive = false;
                    prj_claim.upd_date = now;
                    prj_claim.upd_user = user;
                    prj_claim.upd_num = prj_claim.upd_num + 1;
                    prj_claim.arc_date = null;
                    prj_claim.arc_user = null;
                    dbContext.SaveChanges();

                    string logMess = user + " " + ("восстановил").ToSex(currUser.IsMale) + " задачу " + prj_claim.claim_num;
                    ToPrjLog(now, logMess, null, claim_id, null, null);
                }
                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region ChangePrj

        public void ChangePrj(int claim_id, int prj_id)
        {
            /*
            prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == claim_id).FirstOrDefault();
            if (prj_claim != null)
            {
                prj prj = dbContext.prj.Where(ss => ss.prj_id == prj_id).FirstOrDefault();
                prj_claim.prj_id = prj.prj_id;
                dbContext.SaveChanges();

                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;
                string logMess = user + " " + ("перенес").ToSex(currUser.IsMale) + " задачу " + prj_claim.claim_num + " в группу " + prj.prj_name;
                ToPrjLog(now, logMess, null, claim_id, null, null);
            }
            */
        }

        #endregion

        #region PrjClaimFavor

        public void PrjClaimFavorAdd(int claim_id)
        {
            int userId = currUser.CabUserId;
            prj_claim_favor prj_claim_favor = dbContext.prj_claim_favor.Where(ss => ss.claim_id == claim_id && ss.user_id == userId).FirstOrDefault();
            if (prj_claim_favor == null)
            {
                prj_claim_favor = new prj_claim_favor();
                prj_claim_favor.claim_id = claim_id;
                prj_claim_favor.user_id = userId;
                dbContext.prj_claim_favor.Add(prj_claim_favor);
                dbContext.SaveChanges();
            }
        }
        
        public void PrjClaimFavorDel(int claim_id)
        {
            int userId = currUser.CabUserId;
            dbContext.prj_claim_favor.RemoveRange(dbContext.prj_claim_favor.Where(ss => ss.user_id == userId && ss.claim_id == claim_id));
            dbContext.SaveChanges();
        }

        #endregion

        #region Story

        public PrjClaimViewModel Insert_Story(long story_id, ModelStateDictionary modelState)
        {
            try
            {
                story story = dbContext.story.Where(ss => ss.story_id == story_id).FirstOrDefault();
                if (story == null)
                {
                    modelState.AddModelError("", "Не найден вопрос с кодом " + story_id.ToString());
                    return null;
                }
                crm_client crm_client = dbContext.crm_client.Where(ss => ss.client_name.Trim().ToLower().Equals(story.org_name.Trim().ToLower())).FirstOrDefault();
                if (crm_client == null)
                {
                    crm_client = new crm_client();
                    crm_client.client_name = story.org_name.Trim();
                    crm_client.client_id = dbContext.crm_client.Max(ss => ss.client_id) + 1;
                    dbContext.crm_client.Add(crm_client);
                    dbContext.SaveChanges();
                }

                PrjClaimViewModel claimAdd = new PrjClaimViewModel()
                    {
                        claim_name = story.story_text.CutToLengthWithDots(50),
                        claim_text = story.story_text,
                        project_id = 0,
                        module_id = 0,
                        module_part_id = 0,
                        project_version_id = 0,
                        client_id = crm_client.client_id,
                        priority_id = 0,
                        resp_user_id = 0,
                        project_repair_version_id = 0,
                        claim_type_id = 0,
                        claim_stage_id = 0,
                    };
                List<PrjMessViewModel> messList = new List<PrjMessViewModel>() { new PrjMessViewModel()
                {
                    mess = "Задача создана автоматически из HelpDesk",
                    mess_num = null,
                    prj_id = null,
                    task_id = null,
                    work_id = null,
                    crt_date = DateTime.Now,
                }};

                return InsertClaim(claimAdd, messList, new List<PrjTaskViewModel>(), new List<PrjWorkViewModel>(), new List<PrjTaskUserStateViewModel>(), modelState);
            }
            catch(Exception ex) 
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        #endregion

        #region UpdateTaskUserState

        public void UpdateTaskUserState(int task_id, int state_type_id, int user_id, int? from_user_id, string comment, bool sync_with_notebook = false)
        {
            DateTime now = DateTime.Now;
            string user = currUser.CabUserName;

            prj_task_user_state prj_task_user_state = new prj_task_user_state();

            prj_task_user_state.user_id = user_id;
            prj_task_user_state.task_id = task_id;
            prj_task_user_state.state_type_id = state_type_id;
            prj_task_user_state.from_user_id = from_user_id;
            prj_task_user_state.is_current = true;
            prj_task_user_state.mess = comment;
            prj_task_user_state.crt_date = now;
            prj_task_user_state.crt_user = user;
            prj_task_user_state.upd_date = now;
            prj_task_user_state.upd_user = user;
            dbContext.prj_task_user_state.Add(prj_task_user_state);

            if (sync_with_notebook)
            {
                Enums.PrjTaskUserStateTypeEnum stateTypeEnum = Enums.PrjTaskUserStateTypeEnum.COMPLETED;
                if (Enum.TryParse<Enums.PrjTaskUserStateTypeEnum>(state_type_id.ToString(), out stateTypeEnum))
                {
                    Enums.PrjUserGroupFixedKindEnum fixedKindEnum = Enums.PrjUserGroupFixedKindEnum.NONE;
                    switch (stateTypeEnum)
                    {
                        case Enums.PrjTaskUserStateTypeEnum.SENT_TO_WORK:
                            fixedKindEnum = Enums.PrjUserGroupFixedKindEnum.SENT;
                            break;
                        case Enums.PrjTaskUserStateTypeEnum.RECEIVED_TO_WORK:
                            fixedKindEnum = Enums.PrjUserGroupFixedKindEnum.RECEIVED;
                            break;
                        case Enums.PrjTaskUserStateTypeEnum.COMPLETED:
                            fixedKindEnum = Enums.PrjUserGroupFixedKindEnum.DONE;
                            break;
                        case Enums.PrjTaskUserStateTypeEnum.CANCELED:
                            fixedKindEnum = Enums.PrjUserGroupFixedKindEnum.CANCELED;
                            break;
                        default:
                            break;
                    }

                    switch (fixedKindEnum)
                    {
                        case Enums.PrjUserGroupFixedKindEnum.SENT:
                        case Enums.PrjUserGroupFixedKindEnum.RECEIVED:
                        case Enums.PrjUserGroupFixedKindEnum.DONE:
                        case Enums.PrjUserGroupFixedKindEnum.CANCELED:
                            prj_user_group_item prj_user_group_item = (from t1 in dbContext.prj_user_group_item
                                                                       from t2 in dbContext.prj_user_group
                                                                       where t1.group_id == t2.group_id
                                                                       && t1.task_id == task_id
                                                                       && t2.user_id == user_id
                                                                       && t2.fixed_kind == (int)fixedKindEnum
                                                                       select t1
                                                                      ).FirstOrDefault();

                            if (prj_user_group_item == null)
                            {
                                prj_user_group prj_user_group = dbContext.prj_user_group
                                    .Where(ss => ss.user_id == user_id && ss.fixed_kind == (int)fixedKindEnum)
                                    .FirstOrDefault();
                                if (prj_user_group != null)
                                {
                                    prj_user_group_item prj_user_group_item_last = dbContext.prj_user_group_item
                                        .Where(ss => ss.group_id == prj_user_group.group_id)
                                        .OrderByDescending(ss => ss.ord)
                                        .FirstOrDefault();
                                    prj_user_group_item = new prj_user_group_item();
                                    prj_user_group_item.group_id = prj_user_group.group_id;
                                    prj_user_group_item.item_type_id = (int)Enums.PrjUserGroupItemTypeEnum.TASK;
                                    prj_user_group_item.ord = prj_user_group_item_last != null ? prj_user_group_item_last.ord + 1 : 1;
                                    prj_user_group_item.task_id = task_id;
                                    dbContext.prj_user_group_item.Add(prj_user_group_item);
                                }
                            }
                            break;                            
                        default:
                            break;
                    }

                    dbContext.prj_user_group_item.RemoveRange(from t1 in dbContext.prj_user_group_item
                                                              from t2 in dbContext.prj_user_group
                                                              where t1.group_id == t2.group_id
                                                              && t1.task_id == task_id
                                                              && t2.user_id == user_id
                                                              && t2.fixed_kind != (int)fixedKindEnum
                                                              select t1);
                }
            }

            dbContext.SaveChanges();
        }

        #endregion

        #region UpdateClaimVersion

        public void UpdateClaimVersion(int claim_id, bool is_active_state, bool is_version_fixed)
        {
            //vw_prj_claim
            prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == claim_id).FirstOrDefault();
            prj_project_version prj_project_version = null;
            bool somethingChanged = false;
            int new_version_type_id = 0;

            // закрыли активную задачу
            if ((!is_active_state) && (!is_version_fixed))
            {
                prj_project_version = dbContext.prj_project_version.Where(ss => ss.version_id == prj_claim.version_id).FirstOrDefault();                
                prj_claim.is_version_fixed = true;                                

                var prj_project_version_type_id = prj_project_version.version_type_id;
                int? project_repair_version_id = prj_claim.project_repair_version_id;
                switch ((Enums.PrjVersionTypeEnum)prj_project_version_type_id)
                {
                    case Enums.PrjVersionTypeEnum.RELEASED:
                    case Enums.PrjVersionTypeEnum.CURRENT:
                    case Enums.PrjVersionTypeEnum.NEXT:
                        break;
                    default:
                        //project_repair_version_id = dbContext.prj_project_version.Where(ss => ss.project_id == prj_claim.project_id && ss.version_type_id == (int)Enums.PrjVersionTypeEnum.CURRENT).Select(ss => ss.version_id).FirstOrDefault();
                        prj_project_version = dbContext.prj_project_version.Where(ss => ss.project_id == prj_claim.project_id && ss.version_type_id == (int)Enums.PrjVersionTypeEnum.CURRENT).FirstOrDefault();
                        project_repair_version_id = prj_project_version.version_id;
                        break;
                }
                prj_claim.version_value = prj_project_version.version_value;
                prj_claim.project_repair_version_id = project_repair_version_id;
                

                somethingChanged = true;
            }
            // открыли неактивную задачу
            else if ((is_active_state) && (is_version_fixed))
            {
                /*
                    AU-2071

                    Переносить задачи, которые из состояния закрытых переводятся в открытые (например, при добавлении новой работы):
                    - если задача "восстановлена" из версии, которая меньше "Выпущенной", возвращать ее в версию "Выпущенная"
                    - задачи, возвращенные из закрытых в текущей и следующих версий, вернуть в свою версию (сравнение присвоенной версии исправления и глобальных переменных текущей и выпущенной версий)
                
                    Этап "возврата" в открытые должен быть назначен тот, с которого задача ушла в закрытые. Если этапа еще не было (старые задачи), вернуть в "Заявку".
                */

                if (prj_claim.claim_stage_id <= 0)
                {
                    prj_claim_stage new_prj_claim_stage = dbContext.prj_claim_stage.Where(ss => ss.claim_stage_id > 0).OrderBy(ss => ss.claim_stage_id).FirstOrDefault();
                    if (new_prj_claim_stage != null)
                        prj_claim.claim_stage_id = new_prj_claim_stage.claim_stage_id;
                }                    

                prj_project_version = dbContext.prj_project_version.Where(ss => ss.version_id == prj_claim.version_id).FirstOrDefault();
                if (prj_project_version == null)
                    prj_project_version = dbContext.prj_project_version.Where(ss => ss.project_id == prj_claim.project_id && ss.version_type_id == (int)Enums.PrjVersionTypeEnum.APPLY).FirstOrDefault();
                switch ((Enums.PrjVersionTypeEnum)prj_project_version.version_type_id)
                {
                    case Enums.PrjVersionTypeEnum.APPLY:
                    case Enums.PrjVersionTypeEnum.OLD:
                    case Enums.PrjVersionTypeEnum.RELEASED:
                        new_version_type_id = (int)Enums.PrjVersionTypeEnum.RELEASED;
                        break;
                    default:
                        new_version_type_id = prj_project_version.version_type_id;
                        break;
                }

                prj_project_version = dbContext.prj_project_version.Where(ss => ss.project_id == prj_claim.project_id && ss.version_type_id == new_version_type_id).FirstOrDefault();
                if (prj_project_version != null)
                    prj_claim.version_id = prj_project_version.version_id;
                prj_claim.is_version_fixed = false;
                prj_claim.version_value = null;
                somethingChanged = true;

                #region old
                /*
                prj_project_version = dbContext.prj_project_version.Where(ss => ss.project_id == prj_claim.project_id && ss.version_type_id == 0).FirstOrDefault();
                if (prj_project_version != null)
                    prj_claim.version_id = prj_project_version.version_id;
                prj_claim.is_version_fixed = false;
                prj_claim.version_value = null;
                somethingChanged = true;
                */
                #endregion
            }

            if (somethingChanged)
                dbContext.SaveChanges();
        }

        #endregion

        #region Log

        public void ToPrjLog(DateTime crt_date, string mess, int? prj_id, int? claim_id, int? task_id, int? work_id)
        {
            if (logService == null)
                logService = DependencyResolver.Current.GetService<IPrjLogService>();
            if (logService == null)
                throw new Exception("logService == null");
            //
            logService.InsertLogPrj(crt_date, mess, prj_id, claim_id, task_id, work_id);
        }

        #endregion

    }
}

