﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjProjectVersionService : IAuBaseService
    {
        IQueryable<PrjProjectVersionViewModel> GetList_Full();
        bool StartNewRelease(string project_name, string new_version_num, ModelStateDictionary modelState);
    }

    public class PrjProjectVersionService : AuBaseService, IPrjProjectVersionService
    {
        IPrjLogService logService;

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_prj_project_version
                .Where(ss => ss.version_id == id)
                .ProjectTo<PrjProjectVersionViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_prj_project_version.Where(ss => ((ss.project_id > 0) && (ss.version_type_id > 0))).ProjectTo<PrjProjectVersionViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_prj_project_version.Where(ss => ss.project_id == parent_id).OrderBy(ss => ss.version_type_id).ProjectTo<PrjProjectVersionViewModel>();
        }

        public IQueryable<PrjProjectVersionViewModel> GetList_Full()
        {
            return dbContext.vw_prj_project_version.OrderBy(ss => ss.version_type_id).ProjectTo<PrjProjectVersionViewModel>();
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjProjectVersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjProjectVersionViewModel itemEdit = (PrjProjectVersionViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры версии");
                return null;
            }

            prj_project_version prj_project_version = dbContext.prj_project_version.Where(ss => ss.version_id == itemEdit.version_id).FirstOrDefault();
            if (prj_project_version == null)
            {
                modelState.AddModelError("", "Не заданы параметры версии");
                return null;
            }

            DateTime now = DateTime.Now;
            string userName = currUser.UserName;

            PrjProjectVersionViewModel oldModel = new PrjProjectVersionViewModel();
            ModelMapper.Map<prj_project_version, PrjProjectVersionViewModel>(prj_project_version, oldModel);
            modelBeforeChanges = oldModel;

            prj_project_version.version_name = itemEdit.version_name.Trim();
            prj_project_version.version_value = itemEdit.version_value.Trim();

            prj_project_version.upd_date = now;
            prj_project_version.upd_user = userName;            

            dbContext.SaveChanges();

            return xGetItem(prj_project_version.version_id);
        }

        public bool StartNewRelease(string project_name, string new_version_num, ModelStateDictionary modelState)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(project_name))
                {
                    modelState.AddModelError("", "Не задан проект");
                    return false;
                }

                if (String.IsNullOrWhiteSpace(new_version_num))
                {
                    modelState.AddModelError("", "Не задан номер следующей версии");
                    return false;
                }

                int? project_id = dbContext.crm_project.Where(ss => ss.project_name.Trim().ToLower().Equals(project_name.Trim().ToLower())).Select(ss => ss.project_id).FirstOrDefault();
                if (project_id.GetValueOrDefault(0) <= 0)
                {
                    modelState.AddModelError("", "Не найден проект с наименованием " + project_name);
                    return false;
                }

                string userName = currUser.CabUserName;

                var res = dbContext.Database.SqlQuery<int>("select * from cabinet.prj_start_new_release(" + project_id.ToString() + ",'" + new_version_num.Trim() + "')").FirstOrDefault();
                
                if (logService == null)
                    logService = DependencyResolver.Current.GetService<IPrjLogService>();
                if (logService == null)
                    return true;
                string logMess = userName + " " + ("создал").ToSex(currUser.IsMale) + " следующий релиз " + new_version_num + " по проекту " + project_name;
                logService.InsertLogPrj(DateTime.Now, logMess, null, null, null, null);

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }            
        }


    }
}