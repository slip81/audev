﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjMessService : IAuBaseService
    {
        IQueryable<PrjMessViewModel> GetList_byPrj(int prj_id);
        IQueryable<PrjMessViewModel> GetList_byTask(int task_id);
        IQueryable<PrjMessViewModel> GetList_byWork(int work_id);
        IQueryable<PrjMessViewModel> GetList_forTask_byClaim(int claim_id);
        IQueryable<PrjMessViewModel> GetList_New(string prj_id_list, string crt_user_name_list);
    }

    public class PrjMessService : AuBaseService, IPrjMessService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_prj_mess.ProjectTo<PrjMessViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_prj_mess
                .Where(ss => ss.claim_id == parent_id)
                .ProjectTo<PrjMessViewModel>();
        }

        public IQueryable<PrjMessViewModel> GetList_byPrj(int prj_id) 
        {
            return dbContext.vw_prj_mess
                .Where(ss => ss.prj_id == prj_id)
                .ProjectTo<PrjMessViewModel>();
        }
        
        public IQueryable<PrjMessViewModel> GetList_byTask(int task_id)
        {
            return dbContext.vw_prj_mess
                .Where(ss => ss.task_id == task_id)
                .ProjectTo<PrjMessViewModel>();
        }
        
        public IQueryable<PrjMessViewModel> GetList_byWork(int work_id)
        {
            return dbContext.vw_prj_mess
                .Where(ss => ss.work_id == work_id)
                .ProjectTo<PrjMessViewModel>();
        }

        public IQueryable<PrjMessViewModel> GetList_forTask_byClaim(int claim_id)
        {
            return dbContext.vw_prj_mess
                .Where(ss => ss.claim_id == claim_id && ss.task_id.HasValue)
                .ProjectTo<PrjMessViewModel>();
        }

        public IQueryable<PrjMessViewModel> GetList_New(string prj_id_list, string crt_user_name_list)
        {
            List<int?> prj_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(prj_id_list))
            {
                prj_id_list_int = prj_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<string> crt_user_name_list_string = new List<string>();
            if (!String.IsNullOrEmpty(crt_user_name_list))
            {
                crt_user_name_list_string = crt_user_name_list.Split(',').Select(ss => ss).ToList();
            }

            bool prj_id_list_ok = prj_id_list_int.Count > 0;
            bool crt_user_name_list_ok = crt_user_name_list_string.Count > 0; 

            return dbContext.vw_prj_mess
                .Where(ss => (((crt_user_name_list_ok) && (crt_user_name_list_string.Contains(ss.user_name))) || (!crt_user_name_list_ok))
                        )
                .OrderByDescending(ss => ss.crt_date)
                .Take(10)
                .ProjectTo<PrjMessViewModel>();
        }

    }
}
