﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjReport1ViewModel : AuBaseViewModel
    {

        public PrjReport1ViewModel()
            : base()
        {
            //vw_prj_report1
        }
        
        // db
        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "Группа")]
        public int gr { get; set; }

        [Display(Name = "Группа")]
        public string gr_name { get; set; }

        [Display(Name = "Задание")]
        public string task_num { get; set; }

        [Display(Name = "Исполнитель")]
        public string active_first_exec_user_name { get; set; }

        [Display(Name = "Дата выполнения")]
        public Nullable<System.DateTime> done_last_date_end { get; set; }

        [Display(Name = "Всего в группе")]
        public Nullable<int> gr_cnt { get; set; }

        [Display(Name = "Всего выполненных")]
        public Nullable<int> done_cnt { get; set; }

        [Display(Name = "Всего")]
        public Nullable<int> cnt { get; set; }

        // additional
        [Display(Name = "Всего активных")]
        public Nullable<int> active_cnt { get { return cnt.GetValueOrDefault(0) - done_cnt.GetValueOrDefault(0); } }
    }
}