﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjUserGroupService : IAuBaseService
    {
        IEnumerable<SimpleCombo> GetList_Period();
    }

    public class PrjUserGroupService : AuBaseService, IPrjUserGroupService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.prj_user_group.Where(ss => ss.user_id == parent_id).ProjectTo<PrjUserGroupViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_user_group.Where(ss => ss.group_id == id).ProjectTo<PrjUserGroupViewModel>().FirstOrDefault();
        }

        public IEnumerable<SimpleCombo> GetList_Period()
        {                        
            for (int i = 1; i <= 4; i++)
            {
                yield return new SimpleCombo() { obj_id = i, obj_name = i.EnumName<Enums.PrjUserGroupPeriodTypeEnum>() };
            }
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjUserGroupViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjUserGroupViewModel itemAdd = (PrjUserGroupViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты группы");
                return null;
            }

            if (itemAdd.user_id != currUser.CabUserId)
            {
                modelState.AddModelError("", "Создать новую группу можно только для себя");
                return null;
            }

            DateTime now = DateTime.Now;            

            string group_name = itemAdd.group_name;

            int ord = 1;
            prj_user_group prj_user_group_last = dbContext.prj_user_group.Where(ss => ss.user_id == itemAdd.user_id).OrderByDescending(ss => ss.ord).FirstOrDefault();

            prj_user_group prj_user_group = new prj_user_group();
            prj_user_group.user_id = itemAdd.user_id;
            prj_user_group.group_name = !String.IsNullOrWhiteSpace(group_name) ? group_name : ("Группа от " + now.ToStringStrict("dd.MM.yyyy HH:mm:ss"));
            prj_user_group.ord = prj_user_group_last != null ? prj_user_group_last.ord.GetValueOrDefault(0) + 1 : ord;
            
            dbContext.prj_user_group.Add(prj_user_group);
            dbContext.SaveChanges();

            return xGetItem(prj_user_group.group_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjUserGroupViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            PrjUserGroupViewModel itemDel = (PrjUserGroupViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры группы");
                return false;
            }

            if (itemDel.user_id != currUser.CabUserId)
            {
                modelState.AddModelError("", "Удалить группу можно только для себя");
                return false;
            }

            prj_user_group prj_user_group = dbContext.prj_user_group.Where(ss => ss.group_id == itemDel.group_id).FirstOrDefault();
            if (prj_user_group == null)
            {
                modelState.AddModelError("", "Не найдены параметры группы с кодом " + itemDel.group_id.ToString());
                return false;
            }

            dbContext.prj_user_group_item.RemoveRange(dbContext.prj_user_group_item.Where(ss => ss.group_id == prj_user_group.group_id));
            dbContext.prj_user_group.Remove(prj_user_group);
            dbContext.SaveChanges();

            return true;                
        }
    }
}