﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjLogService : IAuBaseService
    {
        IQueryable<PrjLogViewModel> GetList_byPrj(int prj_id);
        IQueryable<PrjLogViewModel> GetList_byTask(int task_id);
        IQueryable<PrjLogViewModel> GetList_byWork(int work_id);
        IQueryable<PrjLogViewModel> GetList_New(string prj_id_list, string crt_user_name_list);
        IQueryable<PrjLogViewModel> GetList_byFilter(bool show_deleted, string search);
        void InsertLogPrj(DateTime crt_date, string mess, int? prj_id, int? claim_id, int? task_id, int? work_id);
    }

    public class PrjLogService : AuBaseService, IPrjLogService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_prj_log.ProjectTo<PrjLogViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_prj_log
                .Where(ss => ss.claim_id == parent_id)
                .ProjectTo<PrjLogViewModel>();
        }

        public IQueryable<PrjLogViewModel> GetList_byPrj(int prj_id) 
        {
            return dbContext.vw_prj_log
                .Where(ss => ss.prj_id == prj_id)
                .ProjectTo<PrjLogViewModel>();
        }

        public IQueryable<PrjLogViewModel> GetList_byTask(int task_id)
        {
            return dbContext.vw_prj_log
                .Where(ss => ss.task_id == task_id)
                .ProjectTo<PrjLogViewModel>();
        }

        public IQueryable<PrjLogViewModel> GetList_byWork(int work_id)
        {
            return dbContext.vw_prj_log
                .Where(ss => ss.work_id == work_id)
                .ProjectTo<PrjLogViewModel>();
        }

        public IQueryable<PrjLogViewModel> GetList_New(string prj_id_list, string crt_user_name_list)
        {
            List<int?> prj_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(prj_id_list))
            {
                prj_id_list_int = prj_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<string> crt_user_name_list_string = new List<string>();
            if (!String.IsNullOrEmpty(crt_user_name_list))
            {
                crt_user_name_list_string = crt_user_name_list.Split(',').Select(ss => ss).ToList();
            }

            bool prj_id_list_ok = prj_id_list_int.Count > 0;
            bool crt_user_name_list_ok = crt_user_name_list_string.Count > 0; 

            return dbContext.vw_prj_log
                .Where(ss => (((crt_user_name_list_ok) && (crt_user_name_list_string.Contains(ss.crt_user))) || (!crt_user_name_list_ok))
                        )
                .OrderByDescending(ss => ss.crt_date)
                .Take(10)
                .ProjectTo<PrjLogViewModel>();
        }

        public IQueryable<PrjLogViewModel> GetList_byFilter(bool show_deleted, string search)
        {
            var query = dbContext.vw_prj_log
                .Where(ss => (ss.claim_is_archive == true && show_deleted)
                    || (ss.prj_is_archive == true && show_deleted)
                    || (ss.claim_is_archive != true && ss.prj_is_archive != true && !show_deleted)
                );
            if (!String.IsNullOrWhiteSpace(search))
                query = query.Where(ss => ((ss.mess.Trim().ToLower().Contains(search.Trim().ToLower())) || (ss.claim_num.Trim().ToLower().Contains(search.Trim().ToLower())))
                );

            return query
                .ProjectTo<PrjLogViewModel>();
        }

        public void InsertLogPrj(DateTime crt_date, string mess, int? prj_id, int? claim_id, int? task_id, int? work_id)
        {            
            prj_log prj_log = new prj_log();
            prj_log.prj_id = prj_id;
            prj_log.claim_id = claim_id;
            prj_log.task_id = task_id;
            prj_log.work_id = work_id;
            prj_log.mess = mess;
            prj_log.crt_date = crt_date;
            prj_log.crt_user = currUser.CabUserName;                
            dbContext.prj_log.Add(prj_log);
            dbContext.SaveChanges();
        }

    }
}
