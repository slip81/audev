﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjBriefTypeViewModel : AuBaseViewModel
    {

        public PrjBriefTypeViewModel()
            : base(Enums.MainObjectType.PRJ_BRIEF_TYPE)
        {
            //prj_brief_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int brief_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string brief_type_name { get; set; }
    }
}