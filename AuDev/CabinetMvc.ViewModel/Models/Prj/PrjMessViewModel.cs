﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjMessViewModel : AuBaseViewModel
    {

        public PrjMessViewModel()
            : base(Enums.MainObjectType.PRJ_MESS)
        {
            //vw_prj_mess
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int mess_id { get; set; }

        [Display(Name = "Текст")]
        public string mess { get; set; }

        [Display(Name = "#")]
        public string mess_num { get; set; }

        [Display(Name = "Автор")]
        public int user_id { get; set; }

        [Display(Name = "Проект")]
        public Nullable<int> prj_id { get; set; }

        [Display(Name = "Задача")]
        public Nullable<int> claim_id { get; set; }

        [Display(Name = "Задание")]
        public Nullable<int> task_id { get; set; }

        [Display(Name = "Работа")]
        public Nullable<int> work_id { get; set; }

        [Display(Name = "Создан")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменен")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "upd_num")]
        public int upd_num { get; set; }

        [Display(Name = "Создал")]
        public string user_name { get; set; }

        [Display(Name = "Задача")]
        public string claim_num { get; set; }

        [Display(Name = "Задание")]
        public Nullable<int> task_num { get; set; }

        [Display(Name = "Работа")]
        public Nullable<int> work_num { get; set; }
        
        // additional

        [JsonIgnore()]
        public bool is_new { get; set; }

        [JsonIgnore()]
        public int? specified_user_id { get; set; }
    }
}