﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjWorkDefaultService : IAuBaseService
    {
        bool InsertList(IEnumerable<PrjWorkDefaultViewModel> workList, ModelStateDictionary modelState);
    }

    public class PrjWorkDefaultService : AuBaseService, IPrjWorkDefaultService
    {

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_prj_work_default
                .Where(ss => ss.user_id == parent_id)
                .ProjectTo<PrjWorkDefaultViewModel>();
        }

        public bool InsertList(IEnumerable<PrjWorkDefaultViewModel> workList, ModelStateDictionary modelState)
        {
            try
            {
                int user_id = currUser.CabUserId;

                dbContext.prj_work_default.RemoveRange(dbContext.prj_work_default.Where(ss => ss.user_id == user_id));

                prj_work_default prj_work_default = null;
                if ((workList != null) && (workList.Count() > 0))
                {
                    foreach (var work in workList)
                    {
                        prj_work_default = new prj_work_default();
                        prj_work_default.exec_user_id = work.exec_user_id;
                        prj_work_default.is_accept = work.is_accept;
                        prj_work_default.user_id = user_id;
                        prj_work_default.work_type_id = work.work_type_id;
                        dbContext.prj_work_default.Add(prj_work_default);
                    }
                }

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }


    }
}
