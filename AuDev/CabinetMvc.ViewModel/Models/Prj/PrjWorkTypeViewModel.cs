﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjWorkTypeViewModel : AuBaseViewModel
    {

        public PrjWorkTypeViewModel()
            : base(Enums.MainObjectType.PRJ_WORK_TYPE)
        {
            //prj_work_type
        }

        // db
        
        [Key()]
        [Display(Name = "Код")]
        public int work_type_id { get; set; }
        [Display(Name = "Название")]
        public string work_type_name { get; set; }
        [Display(Name = "По-умолчанию")]
        public bool is_default { get; set; }
        [Display(Name = "Акцепт по-умолчанию")]
        public bool is_accept_default { get; set; }

        // additional
        [Display(Name = "exec_user_id")]
        public Nullable<int> exec_user_id { get; set; }
        
    }
}