﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjBriefTypeService : IAuBaseService
    {
        //
    }

    public class PrjBriefTypeService : AuBaseService, IPrjBriefTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_brief_type.ProjectTo<PrjBriefTypeViewModel>();
        }
    }
}
