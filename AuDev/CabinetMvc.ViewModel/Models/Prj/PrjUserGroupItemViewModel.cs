﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjUserGroupItemViewModel : AuBaseViewModel
    {

        public PrjUserGroupItemViewModel()
            : base(Enums.MainObjectType.PRJ_USER_GROUP_ITEM)
        {
            //vw_prj_user_group_item
        }

        // db
        
        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Группа")]
        public int group_id { get; set; }

        [Display(Name = "Тип элемента")]
        public int item_type_id { get; set; }

        [Display(Name = "Задача")]
        public Nullable<int> claim_id { get; set; }

        [Display(Name = "Событие")]
        public Nullable<int> event_id { get; set; }

        [Display(Name = "Запись")]
        public Nullable<int> note_id { get; set; }

        [Display(Name = "Порядок")]
        public int ord { get; set; }

        [Display(Name = "Задание")]
        public Nullable<int> task_id { get; set; }

        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [Display(Name = "fixed_kind")]
        public Nullable<int> fixed_kind { get; set; }

        [Display(Name = "Тип элемента")]
        public string item_type_name { get; set; }

        [Display(Name = "№")]
        public string item_num { get; set; }

        [Display(Name = "Заголовок")]
        public string item_name { get; set; }

        [Display(Name = "Текст")]
        public string item_text { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> item_state_type_id { get; set; }

        [Display(Name = "Статус")]
        public string item_state_type_name { get; set; }

        [Display(Name = "Дата с")]
        public Nullable<System.DateTime> item_date_beg { get; set; }

        [Display(Name = "Дата по")]
        public Nullable<System.DateTime> item_date_end { get; set; }

        [Display(Name = "ord_prev")]
        public Nullable<int> ord_prev { get; set; }

        [Display(Name = "group_id_prev")]
        public Nullable<int> group_id_prev { get; set; }        
    }
}