﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using CabinetMvc.ViewModel.Util;
using System.Net;
using AuDev.Common.Network;
using System.Text.RegularExpressions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjReport1Service : IAuBaseService
    {
        string GetReport1();
    }

    public class PrjReport1Service : AuBaseService, IPrjReport1Service
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_prj_report1.ProjectTo<PrjReport1ViewModel>();
        }

        public string GetReport1()
        {
            string result = "";
            
            PrjReport1ViewModel prjReport1List_first = null;
            var prjReport1List = dbContext.vw_prj_report1.ProjectTo<PrjReport1ViewModel>().ToList();
            if ((prjReport1List != null) && (prjReport1List.Count > 0))
            {
                prjReport1List_first = prjReport1List.FirstOrDefault();
                result += "Краткая сводка по задачам текущей версии Аптека-Урал";
                result += "<br/>";
                result += "<br/>";
                result += "осталось заданий " + prjReport1List_first.active_cnt.GetValueOrDefault(0).ToString() + " из " + prjReport1List_first.cnt.GetValueOrDefault(0).ToString() + " (на " + DateTime.Now.ToString("dd.MM.yyyy HH:mm") + ")";

                result += "<br/>";
                result += "<br/>";

                for (var i = 1; i <= 4; i++)
                {
                    var gr = prjReport1List.Where(ss => ss.gr == i).ToList();
                    if ((gr != null) && (gr.Count > 0))
                    {
                        prjReport1List_first = gr.FirstOrDefault();
                        result += prjReport1List_first.gr_name + " (" + prjReport1List_first.gr_cnt.GetValueOrDefault(0).ToString() + ")";                        
                        result += "<br/>";
                        foreach (var gr_item in gr.OrderByDescending(ss => ss.done_last_date_end).ThenBy(ss => ss.task_num))
                        {
                            result += gr_item.task_num + "&nbsp;&nbsp;&nbsp;&nbsp;" + (i != 4 ? gr_item.active_first_exec_user_name : (gr_item.done_last_date_end.HasValue ? ((DateTime)gr_item.done_last_date_end).ToString("dd.MM.yyyy") : "-"));
                            result += "<br/>";
                        }                        
                    }
                    result += "<br/>";
                }

            }

            return String.IsNullOrWhiteSpace(result) ? "нет данных" : result;
        }
    }
}