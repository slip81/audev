﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjTaskUserStateTypeViewModel : AuBaseViewModel
    {

        public PrjTaskUserStateTypeViewModel()
            : base()
        {
            //prj_task_user_state_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int state_type_id { get; set; }

        [Display(Name = "Название")]
        public string state_type_name { get; set; }
    }
}