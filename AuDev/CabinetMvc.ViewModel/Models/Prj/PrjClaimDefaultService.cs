﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text.RegularExpressions;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimDefaultService : IAuBaseService
    {
        PrjClaimDefaultViewModel GetItem_Default();
    }

    public class PrjClaimDefaultService : AuBaseService, IPrjClaimDefaultService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_claim_default
                .Where(ss => ss.user_id == id)
                .ProjectTo<PrjClaimDefaultViewModel>()
                .FirstOrDefault();
        }

        public PrjClaimDefaultViewModel GetItem_Default()
        {
            return new PrjClaimDefaultViewModel()
            {
                client_id = 0,
                module_id = 0,
                module_part_id = 0,
                module_version_id = 0,
                priority_id = 0,
                prj_id = 0,
                project_id = 0,                
                repair_version_id = null,
                resp_user_id = 0,
            };
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimDefaultViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjClaimDefaultViewModel itemAdd = (PrjClaimDefaultViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы параметры");
                return null;
            }
            int user_id = currUser.CabUserId;
            prj_claim_default prj_claim_default_existing = dbContext.prj_claim_default.Where(ss => ss.user_id == user_id).FirstOrDefault();
            if (prj_claim_default_existing != null)
            {
                return Update(item, modelState);
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string user = currUser.CabUserName;

            prj_claim_default prj_claim_default = new prj_claim_default();
            ModelMapper.Map<PrjClaimDefaultViewModel, prj_claim_default>(itemAdd, prj_claim_default);
            prj_claim_default.user_id = user_id;
            dbContext.prj_claim_default.Add(prj_claim_default);

            dbContext.SaveChanges();

            return xGetItem(user_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimDefaultViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjClaimDefaultViewModel itemEdit = (PrjClaimDefaultViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры");
                return null;
            }

            int user_id = currUser.CabUserId;
            prj_claim_default prj_claim_default = dbContext.prj_claim_default.Where(ss => ss.user_id == user_id).FirstOrDefault();
            if (prj_claim_default == null)
            {
                modelState.AddModelError("", "Не найдены параметры");
                return null;
            }

            PrjClaimDefaultViewModel oldModel = new PrjClaimDefaultViewModel();
            ModelMapper.Map<prj_claim_default, PrjClaimDefaultViewModel>(prj_claim_default, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<PrjClaimDefaultViewModel, prj_claim_default>(itemEdit, prj_claim_default);
            prj_claim_default.user_id = user_id;

            dbContext.SaveChanges();

            return xGetItem(user_id);
        }
    }
}