﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text.RegularExpressions;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjTaskService : IAuBaseService
    {
        IQueryable<PrjTaskViewModel> GetList_byFilter(string prj_id_list, string task_state_type_id_list, string search);
        IQueryable<PrjTaskViewModel> GetList_New(string prj_id_list, string crt_user_name_list);
        IQueryable<PrjTaskViewModel> GetList_Brief(int brief_type_id, DateTime? date_beg, DateTime? date_end, int date_cut_type_id);
        IQueryable<PrjTaskViewModel> GetList_byClaim(int claim_id, int exec_user_id, int task_user_state_type);
        bool ExecuteAction(int? task_id, int? action_id, int? user_id, int? work_type_id, string comment, ModelStateDictionary modelState);
        bool InsertSimple(int? claim_id, string task_text, ModelStateDictionary modelState);
    }

    public class PrjTaskService : AuBaseService, IPrjTaskService
    {
        IPrjClaimService claimService;

        #region Get

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_prj_task
                .Where(ss => ss.task_id == id)
                .ProjectTo<PrjTaskViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_prj_task                
                .ProjectTo<PrjTaskViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            int user_id = currUser.CabUserId;
            var res = from t1 in dbContext.vw_prj_task
                      from t2 in dbContext.vw_prj_task_user_state.Where(ss => t1.task_id == ss.task_id && ss.user_id == user_id && ss.is_current).DefaultIfEmpty()
                      where t1.claim_id == parent_id
                      select new PrjTaskViewModel()
                      {
                          task_id = t1.task_id,
                          claim_id = t1.claim_id,
                          task_num = t1.task_num,
                          task_text = t1.task_text,
                          date_plan = t1.date_plan,
                          date_fact = t1.date_fact,
                          crt_date = t1.crt_date,
                          crt_user = t1.crt_user,
                          upd_date = t1.upd_date,
                          upd_user = t1.upd_user,
                          upd_num = t1.upd_num,
                          old_task_id = t1.old_task_id,
                          old_subtask_id = t1.old_subtask_id,
                          claim_num = t1.claim_num,
                          claim_name = t1.claim_name,
                          claim_text = t1.claim_text,
                          claim_project_id = t1.claim_project_id,
                          claim_module_id = t1.claim_module_id,
                          claim_module_part_id = t1.claim_module_part_id,
                          claim_project_version_id = t1.claim_project_version_id,
                          claim_client_id = t1.claim_client_id,
                          claim_priority_id = t1.claim_priority_id,
                          claim_resp_user_id = t1.claim_resp_user_id,
                          claim_date_plan = t1.claim_date_plan,
                          claim_date_fact = t1.claim_date_fact,
                          claim_project_repair_version_id = t1.claim_project_repair_version_id,
                          claim_is_archive = t1.claim_is_archive,
                          claim_is_control = t1.claim_is_control,
                          claim_project_name = t1.claim_project_name,
                          claim_module_name = t1.claim_module_name,
                          claim_module_part_name = t1.claim_module_part_name,
                          claim_project_version_name = t1.claim_project_version_name,
                          claim_client_name = t1.claim_client_name,
                          claim_priority_name = t1.claim_priority_name,
                          claim_resp_user_name = t1.claim_resp_user_name,
                          claim_project_repair_version_name = t1.claim_project_repair_version_name,
                          claim_state_type_id = t1.claim_state_type_id,
                          claim_state_type_name = t1.claim_state_type_name,
                          claim_state_type_templ = t1.claim_state_type_templ,
                          task_state_type_id = t1.task_state_type_id,
                          work_cnt = t1.work_cnt,
                          active_work_cnt = t1.active_work_cnt,
                          done_work_cnt = t1.done_work_cnt,
                          canceled_work_cnt = t1.canceled_work_cnt,
                          task_state_type_name = t1.task_state_type_name,
                          task_state_type_templ = t1.task_state_type_templ,
                          old_state_id = t1.old_state_id,
                          old_state_name = t1.old_state_name,
                          active_work_type_id = t1.active_work_type_id,
                          active_work_type_name = t1.active_work_type_name,
                          is_overdue = t1.is_overdue,
                          claim_type_id = t1.claim_type_id,
                          claim_stage_id = t1.claim_stage_id,
                          claim_type_name = t1.claim_type_name,
                          claim_stage_name = t1.claim_stage_name,
                          task_mess_cnt = t1.task_mess_cnt,
                          is_new = false,
                          user_state_type_id = t2 == null ? null : (int?)t2.state_type_id,
                          user_state_type_name = t2 == null ? null : t2.state_type_name,
                          user_state_from_user_id = t2 == null ? null : t2.from_user_id,
                          user_state_from_user_name = t2 == null ? null : t2.from_user_name,
                          user_state_mess = t2 == null ? null : t2.mess,
                          user_state_crt_date = t2 == null ? null : t2.crt_date,
                          prj_id_list = t1.prj_id_list,
                          prj_name_list = t1.prj_name_list,
                      };
            return res;
        }


        public IQueryable<PrjTaskViewModel> GetList_byFilter(string prj_id_list, string task_state_type_id_list, string search)
        {
            List<int?> prj_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(prj_id_list))
            {
                prj_id_list_int = prj_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<int?> task_state_type_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(task_state_type_id_list))
            {
                task_state_type_id_list_int = task_state_type_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            bool prj_id_list_ok = prj_id_list_int.Count > 0;
            bool task_state_type_id_list_ok = task_state_type_id_list_int.Count > 0;

            if (!String.IsNullOrEmpty(search))
            {
                // кратко, подробно, номер, комментарии, задания

                return (from t1 in dbContext.vw_prj_task
                        where ((t1.claim_name.Trim().ToLower().Contains(search.Trim().ToLower())) || (t1.claim_text.Trim().ToLower().Contains(search.Trim().ToLower())) || (t1.claim_num.Trim().ToLower().Contains(search.Trim().ToLower())) || (t1.task_text.Trim().ToLower().Contains(search.Trim().ToLower())))                        
                        && (((task_state_type_id_list_ok) && (task_state_type_id_list_int.Contains(t1.task_state_type_id))) || (!task_state_type_id_list_ok))
                        select t1)
                       .ProjectTo<PrjTaskViewModel>();
            }
            else
            {
                return (from t1 in dbContext.vw_prj_task
                        where (((task_state_type_id_list_ok) && (task_state_type_id_list_int.Contains(t1.task_state_type_id))) || (!task_state_type_id_list_ok))
                        select t1)
                       .ProjectTo<PrjTaskViewModel>();
            }
        }

        public IQueryable<PrjTaskViewModel> GetList_New(string prj_id_list, string crt_user_name_list)
        {
            List<int?> prj_id_list_int = new List<int?>();
            if (!String.IsNullOrEmpty(prj_id_list))
            {
                prj_id_list_int = prj_id_list.Split(',').Select(ss => (int?)int.Parse(ss)).ToList();
            }

            List<string> crt_user_name_list_string = new List<string>();
            if (!String.IsNullOrEmpty(crt_user_name_list))
            {
                crt_user_name_list_string = crt_user_name_list.Split(',').Select(ss => ss).ToList();
            }

            bool prj_id_list_ok = prj_id_list_int.Count > 0;
            bool crt_user_name_list_ok = crt_user_name_list_string.Count > 0; 

            return dbContext.vw_prj_task
                .Where(ss => (((crt_user_name_list_ok) && (crt_user_name_list_string.Contains(ss.crt_user))) || (!crt_user_name_list_ok))
                        )
                .OrderByDescending(ss => ss.crt_date)
                .Take(10)
                .ProjectTo<PrjTaskViewModel>();
        }

        public IQueryable<PrjTaskViewModel> GetList_Brief(int brief_type_id, DateTime? date_beg, DateTime? date_end, int date_cut_type_id)
        {

            IQueryable<vw_prj_task_user_state> query = dbContext.vw_prj_task_user_state.Where(ss => ss.is_current);

            DateTime date_beg_for_new = DateTime.Today.AddBusinessDays(-1);

            DateTime date_beg_actual = date_beg.HasValue ? (DateTime)date_beg : DateTime.Today.AddMonths(-2);
            DateTime date_end_actual = date_end.HasValue ? ((DateTime)date_end).AddDays(1) : DateTime.Today.AddDays(1);

            switch ((Enums.PrjBriefEnum)brief_type_id)
            {
                case Enums.PrjBriefEnum.DEV_TASK_MONITORING:
                    switch ((Enums.PrjBriefDateCutTypeEnum)date_cut_type_id)
                    {
                        case Enums.PrjBriefDateCutTypeEnum.CRT_DATE:
                            query = query.Where(ss => ss.crt_date >= date_beg_actual && ss.crt_date < date_end_actual);
                            break;
                        case Enums.PrjBriefDateCutTypeEnum.UPD_DATE:
                            //query = query.Where(ss => ss.upd_date >= date_beg_actual && ss.upd_date < date_end_actual);
                            query = query.Where(ss => ss.crt_date >= date_beg_actual && ss.crt_date < date_end_actual);
                            break;
                        default:
                            query = query.Where(ss => 1 == 0);
                            break;
                    }
                    break;
                default:
                    query = query.Where(ss => 1 == 0);
                    break;
            }
            
            return from t1 in dbContext.vw_prj_task
                   from t2 in query
                   where t1.task_id == t2.task_id
                   select new PrjTaskViewModel()
                   {                       
                       task_id = t1.task_id,
                       claim_id = t1.claim_id,
                       task_num = t1.task_num,
                       task_text = t1.task_text,
                       date_plan = t1.date_plan,
                       date_fact = t1.date_fact,
                       crt_date = t1.crt_date,
                       crt_user = t1.crt_user,
                       upd_date = t1.upd_date,
                       upd_user = t1.upd_user,
                       upd_num = t1.upd_num,
                       old_task_id = t1.old_task_id,
                       old_subtask_id = t1.old_subtask_id,
                       claim_num = t1.claim_num,
                       claim_name = t1.claim_name,
                       claim_text = t1.claim_text,
                       claim_project_id = t1.claim_project_id,
                       claim_module_id = t1.claim_module_id,
                       claim_module_part_id = t1.claim_module_part_id,
                       claim_project_version_id = t1.claim_project_version_id,
                       claim_client_id = t1.claim_client_id,
                       claim_priority_id = t1.claim_priority_id,
                       claim_resp_user_id = t1.claim_resp_user_id,
                       claim_date_plan = t1.claim_date_plan,
                       claim_date_fact = t1.claim_date_fact,
                       claim_project_repair_version_id = t1.claim_project_repair_version_id,
                       claim_is_archive = t1.claim_is_archive,
                       claim_is_control = t1.claim_is_control,
                       claim_project_name = t1.claim_project_name,
                       claim_module_name = t1.claim_module_name,
                       claim_module_part_name = t1.claim_module_part_name,
                       claim_project_version_name = t1.claim_project_version_name,
                       claim_client_name = t1.claim_client_name,
                       claim_priority_name = t1.claim_priority_name,
                       claim_resp_user_name = t1.claim_resp_user_name,
                       claim_project_repair_version_name = t1.claim_project_repair_version_name,
                       claim_state_type_id = t1.claim_state_type_id,
                       claim_state_type_name = t1.claim_state_type_name,
                       claim_state_type_templ = t1.claim_state_type_templ,
                       task_state_type_id = t1.task_state_type_id,
                       work_cnt = t1.work_cnt,
                       active_work_cnt = t1.active_work_cnt,
                       done_work_cnt = t1.done_work_cnt,
                       canceled_work_cnt = t1.canceled_work_cnt,
                       task_state_type_name = t1.task_state_type_name,
                       task_state_type_templ = t1.task_state_type_templ,
                       old_state_id = t1.old_state_id,
                       old_state_name = t1.old_state_name,
                       active_work_type_id = t1.active_work_type_id,
                       active_work_type_name = t1.active_work_type_name,
                       is_overdue = t1.is_overdue,
                       claim_type_id = t1.claim_type_id,
                       claim_stage_id = t1.claim_stage_id,
                       claim_type_name = t1.claim_type_name,
                       claim_stage_name = t1.claim_stage_name,
                       task_mess_cnt = t1.task_mess_cnt,
                       is_new = false,
                       user_state_type_id = t2 == null ? null : (int?)t2.state_type_id,
                       user_state_type_name = t2 == null ? null : t2.state_type_name,
                       user_state_user_id = t2 == null ? null : (int?)t2.user_id,
                       user_state_user_name = t2 == null ? null : t2.user_name,
                       user_state_from_user_id = t2 == null ? null : t2.from_user_id,
                       user_state_from_user_name = t2 == null ? null : t2.from_user_name,
                       user_state_mess = t2 == null ? null : t2.mess,
                       user_state_crt_date = t2 == null ? null : t2.crt_date,
                       prj_id_list = t1.prj_id_list,
                       prj_name_list = t1.prj_name_list,
                   }                   
                   ;

        }

        public IQueryable<PrjTaskViewModel> GetList_byClaim(int claim_id, int exec_user_id, int task_user_state_type)
        {
            //int user_id = currUser.CabUserId;
            int user_id = exec_user_id;            

            return from t1 in dbContext.vw_prj_task
                   from t2 in dbContext.vw_prj_task_user_state
                   where t1.claim_id == claim_id
                   && t1.task_id == t2.task_id
                   && t2.user_id == user_id
                   && t2.is_current == true
                   select new PrjTaskViewModel()
                   {
                       task_id = t1.task_id,
                       claim_id = t1.claim_id,
                       task_num = t1.task_num,
                       task_text = t1.task_text,
                       date_plan = t1.date_plan,
                       date_fact = t1.date_fact,
                       crt_date = t1.crt_date,
                       crt_user = t1.crt_user,
                       upd_date = t1.upd_date,
                       upd_user = t1.upd_user,
                       upd_num = t1.upd_num,
                       old_task_id = t1.old_task_id,
                       old_subtask_id = t1.old_subtask_id,
                       claim_num = t1.claim_num,
                       claim_name = t1.claim_name,
                       claim_text = t1.claim_text,
                       claim_project_id = t1.claim_project_id,
                       claim_module_id = t1.claim_module_id,
                       claim_module_part_id = t1.claim_module_part_id,
                       claim_project_version_id = t1.claim_project_version_id,
                       claim_client_id = t1.claim_client_id,
                       claim_priority_id = t1.claim_priority_id,
                       claim_resp_user_id = t1.claim_resp_user_id,
                       claim_date_plan = t1.claim_date_plan,
                       claim_date_fact = t1.claim_date_fact,
                       claim_project_repair_version_id = t1.claim_project_repair_version_id,
                       claim_is_archive = t1.claim_is_archive,
                       claim_is_control = t1.claim_is_control,
                       claim_project_name = t1.claim_project_name,
                       claim_module_name = t1.claim_module_name,
                       claim_module_part_name = t1.claim_module_part_name,
                       claim_project_version_name = t1.claim_project_version_name,
                       claim_client_name = t1.claim_client_name,
                       claim_priority_name = t1.claim_priority_name,
                       claim_resp_user_name = t1.claim_resp_user_name,
                       claim_project_repair_version_name = t1.claim_project_repair_version_name,
                       claim_state_type_id = t1.claim_state_type_id,
                       claim_state_type_name = t1.claim_state_type_name,
                       claim_state_type_templ = t1.claim_state_type_templ,
                       task_state_type_id = t1.task_state_type_id,
                       work_cnt = t1.work_cnt,
                       active_work_cnt = t1.active_work_cnt,
                       done_work_cnt = t1.done_work_cnt,
                       canceled_work_cnt = t1.canceled_work_cnt,
                       task_state_type_name = t1.task_state_type_name,
                       task_state_type_templ = t1.task_state_type_templ,
                       old_state_id = t1.old_state_id,
                       old_state_name = t1.old_state_name,
                       active_work_type_id = t1.active_work_type_id,
                       active_work_type_name = t1.active_work_type_name,
                       is_overdue = t1.is_overdue,
                       claim_type_id = t1.claim_type_id,
                       claim_stage_id = t1.claim_stage_id,
                       claim_type_name = t1.claim_type_name,
                       claim_stage_name = t1.claim_stage_name,
                       task_mess_cnt = t1.task_mess_cnt,
                       is_new = false,
                       user_state_type_id = t2.state_type_id,
                       user_state_type_name = t2.state_type_name,
                       user_state_from_user_id = t2.from_user_id,
                       user_state_from_user_name = t2.from_user_name,
                       user_state_mess = t2.mess,
                       user_state_crt_date = t2.crt_date,
                       prj_id_list = t1.prj_id_list,
                       prj_name_list = t1.prj_name_list,
                   };
        }

        #endregion
       
        #region InsertSimple

        public bool InsertSimple(int? claim_id, string task_text, ModelStateDictionary modelState)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(task_text))
                    return true;

                prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == claim_id).FirstOrDefault();
                if (prj_claim == null)
                {
                    modelState.AddModelError("", "Не найдена задача с кодом " + prj_claim.claim_id.ToString());
                    return false;
                }

                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;

                prj_task prj_task_last = dbContext.prj_task.Where(ss => ss.claim_id == claim_id).OrderByDescending(ss => ss.task_num).FirstOrDefault();

                prj_task prj_task = new prj_task();
                prj_task.prj_claim = prj_claim;
                prj_task.task_num = prj_task_last != null ? prj_task_last.task_num + 1 : 1;
                prj_task.task_text = task_text;
                prj_task.crt_date = now;
                prj_task.crt_user = user;
                prj_task.upd_date = now;
                prj_task.upd_user = user;
                prj_task.upd_num = 1;
                dbContext.prj_task.Add(prj_task);

                prj_claim.upd_date = now;
                prj_claim.upd_user = user;
                prj_claim.upd_num = prj_claim.upd_num + 1;

                dbContext.SaveChanges();

                // !!!
                // new
                vw_prj_claim vw_prj_claim = dbContext.vw_prj_claim.Where(ss => ss.claim_id == prj_task.claim_id).FirstOrDefault();
                updateClaimVersion(vw_prj_claim.claim_id, vw_prj_claim.is_active_state, vw_prj_claim.is_version_fixed);

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region ExecuteAction

        public bool ExecuteAction(int? task_id, int? action_id, int? user_id, int? work_type_id, string comment, ModelStateDictionary modelState)
        {
            try
            {
                prj_task prj_task = dbContext.prj_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
                if (prj_task == null)
                {
                    modelState.AddModelError("", "Не найдено задание с кодом " + task_id.GetValueOrDefault(0).ToString());
                    return false;
                }

                prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == prj_task.claim_id).FirstOrDefault();
                if (prj_claim == null)
                {
                    modelState.AddModelError("", "Не найдена задача по заданию с кодом " + task_id.GetValueOrDefault(0).ToString());
                    return false;
                }

                DateTime now = DateTime.Now;
                DateTime today = DateTime.Today;
                string user = currUser.CabUserName;
                prj_task_user_state prj_task_user_state = null;

                Enums.PrjTaskActionEnum taskActionEnum = Enums.PrjTaskActionEnum.NONE;
                if (!Enum.TryParse<Enums.PrjTaskActionEnum>(action_id.GetValueOrDefault(0).ToString(), out taskActionEnum))
                    taskActionEnum = Enums.PrjTaskActionEnum.NONE;

                Enums.PrjTaskUserStateTypeEnum stateTypeEnum = Enums.PrjTaskUserStateTypeEnum.SENT_TO_WORK;
                int? curr_user_id = user_id;
                int? curr_work_type_id = work_type_id;
                int? curr_from_user_id = currUser.CabUserId;
                bool ok = false;

                prj_work prj_work = null;
                prj_work prj_work_last = null;                
                prj_mess prj_mess = null;

                cab_user cab_user = null;
                Enums.PrjWorkTypeEnum workTypeEnum = Enums.PrjWorkTypeEnum.REVIEW;

                switch (taskActionEnum)
                {
                    case Enums.PrjTaskActionEnum.SEND_TO_WORK:
                        #region
                        stateTypeEnum = Enums.PrjTaskUserStateTypeEnum.SENT_TO_WORK;
                        //
                        prj_work = dbContext.prj_work.Where(ss => ss.task_id == prj_task.task_id && ss.exec_user_id == curr_user_id && ss.work_type_id == curr_work_type_id && ss.state_type_id == (int)Enums.PrjWorkStateTypeEnum.ACTIVE).OrderByDescending(ss => ss.work_num).FirstOrDefault();
                        if (prj_work == null)
                        {
                            prj_work_last = dbContext.prj_work.Where(ss => ss.task_id == prj_task.task_id).OrderByDescending(ss => ss.work_num).FirstOrDefault();
                            //
                            prj_work = new prj_work();
                            prj_work.task_id = prj_task.task_id;
                            prj_work.claim_id = prj_task.claim_id;
                            prj_work.work_num = prj_work_last == null ? 1 : prj_work_last.work_num + 1;
                            prj_work.work_type_id = curr_work_type_id.GetValueOrDefault(0);
                            prj_work.exec_user_id = curr_user_id.GetValueOrDefault(0);
                            prj_work.state_type_id = (int)Enums.PrjWorkStateTypeEnum.ACTIVE;
                            prj_work.date_send = today;
                            prj_work.crt_date = now;
                            prj_work.crt_user = user;
                            prj_work.upd_date = now;
                            prj_work.upd_user = user;
                            prj_work.upd_num = 0;
                            dbContext.prj_work.Add(prj_work);

                            ok = true;
                        }
                        break;
                        #endregion
                    case Enums.PrjTaskActionEnum.RECEIVE_TO_WORK:
                        #region
                        curr_user_id = currUser.CabUserId;
                        stateTypeEnum = Enums.PrjTaskUserStateTypeEnum.RECEIVED_TO_WORK;
                        //
                        cab_user = dbContext.cab_user.Where(ss => ss.user_id == curr_user_id).FirstOrDefault();
                        if ((cab_user != null) && (cab_user.crm_user_role_id.GetValueOrDefault(0) > 0))
                        {
                            switch ((int)cab_user.crm_user_role_id)
                            {
                                case (int)Enums.CabUserRole.DEVELOPER:
                                    workTypeEnum = Enums.PrjWorkTypeEnum.PROGRAMM;
                                    break;
                                case (int)Enums.CabUserRole.TESTER:
                                    workTypeEnum = Enums.PrjWorkTypeEnum.TEST;
                                    break;
                                case (int)Enums.CabUserRole.SERVICE:
                                    workTypeEnum = Enums.PrjWorkTypeEnum.TEST;
                                    break;
                                default:
                                    break;
                            }
                        }
                        //
                        prj_work = dbContext.prj_work
                            .Where(ss => ss.task_id == prj_task.task_id && ss.exec_user_id == curr_user_id
                                //&& ss.work_type_id == (int)workTypeEnum
                                && ((ss.state_type_id == (int)Enums.PrjWorkStateTypeEnum.ACTIVE) || (ss.state_type_id == (int)Enums.PrjWorkStateTypeEnum.RETURNED)))
                            .OrderByDescending(ss => ss.work_num).FirstOrDefault();
                        if (prj_work == null)
                        {
                            //
                            prj_work_last = dbContext.prj_work.Where(ss => ss.task_id == prj_task.task_id).OrderByDescending(ss => ss.work_num).FirstOrDefault();
                            //
                            prj_work = new prj_work();
                            prj_work.task_id = prj_task.task_id;
                            prj_work.claim_id = prj_task.claim_id;
                            prj_work.work_num = prj_work_last == null ? 1 : prj_work_last.work_num + 1;
                            prj_work.work_type_id = (int)workTypeEnum;
                            prj_work.exec_user_id = curr_user_id.GetValueOrDefault(0);
                            prj_work.state_type_id = (int)Enums.PrjWorkStateTypeEnum.ACTIVE;
                            prj_work.is_accept = true;
                            prj_work.is_plan = true;
                            prj_work.date_send = today;
                            prj_work.date_beg = today;
                            prj_work.date_end = today;
                            prj_work.date_time_beg = today;
                            prj_work.date_time_end = today;
                            prj_work.crt_date = now;
                            prj_work.crt_user = user;
                            prj_work.upd_date = now;
                            prj_work.upd_user = user;
                            prj_work.upd_num = 0;
                            dbContext.prj_work.Add(prj_work);

                            ok = true;
                        }
                        else
                        {
                            if (!prj_work.is_accept)
                            {
                                prj_work.is_accept = true;
                                prj_work.is_plan = true;
                                prj_work.date_beg = today;
                                prj_work.date_end = today;
                                prj_work.date_time_beg = today;
                                prj_work.date_time_end = today;
                                prj_work.upd_date = now;
                                prj_work.upd_user = user;
                                prj_work.upd_num = prj_work.upd_num + 1;

                                ok = true;
                            }
                        }
                        break;
                        #endregion
                    case Enums.PrjTaskActionEnum.COMPLETE_AND_FORWARD:
                    case Enums.PrjTaskActionEnum.COMPLETE_AND_RETURN:
                    case Enums.PrjTaskActionEnum.COMPLETE_AND_CLOSE:
                        #region
                        curr_user_id = currUser.CabUserId;
                        stateTypeEnum = Enums.PrjTaskUserStateTypeEnum.COMPLETED;
                        //
                        prj_work = dbContext.prj_work
                            .Where(ss => ss.task_id == prj_task.task_id && ss.exec_user_id == curr_user_id && ((ss.state_type_id == (int)Enums.PrjWorkStateTypeEnum.ACTIVE) || (ss.state_type_id == (int)Enums.PrjWorkStateTypeEnum.RETURNED)))
                            .OrderByDescending(ss => ss.work_num).FirstOrDefault();
                        if (prj_work != null)
                        {
                            prj_work.state_type_id = (int)Enums.PrjWorkStateTypeEnum.DONE;
                            prj_work.is_accept = true;
                            prj_work.is_plan = true;
                            prj_work.date_send = prj_work.date_send.HasValue ? prj_work.date_send : today;
                            prj_work.date_beg = prj_work.date_beg.HasValue ? prj_work.date_beg : today;
                            prj_work.date_time_beg = prj_work.date_time_beg.HasValue ? prj_work.date_time_beg : today;
                            prj_work.date_end = today;
                            prj_work.date_time_end = today;
                            prj_work.upd_date = now;
                            prj_work.upd_user = user;
                            prj_work.upd_num = prj_work.upd_num + 1;
                            //

                            if (taskActionEnum != Enums.PrjTaskActionEnum.COMPLETE_AND_CLOSE)
                            {
                                Enums.PrjWorkStateTypeEnum stateTypeForNextWork = taskActionEnum == Enums.PrjTaskActionEnum.COMPLETE_AND_FORWARD ? Enums.PrjWorkStateTypeEnum.ACTIVE : Enums.PrjWorkStateTypeEnum.RETURNED;

                                if (curr_work_type_id.GetValueOrDefault(0) > 0)
                                {
                                    workTypeEnum = (Enums.PrjWorkTypeEnum)curr_work_type_id;
                                }
                                else
                                {
                                    cab_user = dbContext.cab_user.Where(ss => ss.user_id == user_id).FirstOrDefault();
                                    if ((cab_user != null) && (cab_user.crm_user_role_id.GetValueOrDefault(0) > 0))
                                    {
                                        switch ((int)cab_user.crm_user_role_id)
                                        {
                                            case (int)Enums.CabUserRole.DEVELOPER:
                                                workTypeEnum = Enums.PrjWorkTypeEnum.PROGRAMM;
                                                break;
                                            case (int)Enums.CabUserRole.TESTER:
                                                workTypeEnum = Enums.PrjWorkTypeEnum.TEST;
                                                break;
                                            case (int)Enums.CabUserRole.SERVICE:
                                                workTypeEnum = Enums.PrjWorkTypeEnum.TEST;
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }

                                prj_work = dbContext.prj_work
                                    .Where(ss => ss.task_id == prj_task.task_id && ss.exec_user_id == user_id && ss.state_type_id == (int)stateTypeForNextWork && ss.work_type_id == (int)workTypeEnum)
                                    .OrderByDescending(ss => ss.work_num)
                                    .FirstOrDefault();
                                if (prj_work == null)
                                {
                                    prj_work_last = dbContext.prj_work.Where(ss => ss.task_id == prj_task.task_id).OrderByDescending(ss => ss.work_num).FirstOrDefault();
                                    //
                                    prj_work = new prj_work();
                                    prj_work.task_id = prj_task.task_id;
                                    prj_work.claim_id = prj_task.claim_id;
                                    prj_work.work_num = prj_work_last == null ? 1 : prj_work_last.work_num + 1;
                                    prj_work.work_type_id = (int)workTypeEnum;
                                    prj_work.exec_user_id = user_id.GetValueOrDefault(0);
                                    prj_work.state_type_id = (int)stateTypeForNextWork;
                                    prj_work.crt_date = now;
                                    prj_work.crt_user = user;
                                    prj_work.upd_date = now;
                                    prj_work.upd_user = user;
                                    prj_work.upd_num = 0;
                                    dbContext.prj_work.Add(prj_work);
                                }
                            }

                            if (!String.IsNullOrWhiteSpace(comment))
                            {
                                prj_mess = new prj_mess();
                                prj_mess.mess = comment;
                                prj_mess.user_id = curr_user_id.GetValueOrDefault(0);
                                prj_mess.prj_id = null;
                                prj_mess.claim_id = prj_task.claim_id;
                                prj_mess.task_id = prj_task.task_id;
                                prj_mess.work_id = null;
                                prj_mess.crt_date = now;
                                prj_mess.crt_user = user;
                                prj_mess.upd_date = now;
                                prj_mess.upd_user = user;
                                prj_mess.upd_num = 1;
                                dbContext.prj_mess.Add(prj_mess);

                                ok = true;
                            }

                            ok = true;
                        }
                        break;
                        #endregion
                    case Enums.PrjTaskActionEnum.CANCEL:
                        #region
                        curr_user_id = currUser.CabUserId;
                        stateTypeEnum = Enums.PrjTaskUserStateTypeEnum.CANCELED;
                        //
                        prj_work = dbContext.prj_work
                            .Where(ss => ss.task_id == prj_task.task_id && ss.exec_user_id == curr_user_id && ((ss.state_type_id == (int)Enums.PrjWorkStateTypeEnum.ACTIVE) || (ss.state_type_id == (int)Enums.PrjWorkStateTypeEnum.RETURNED)))
                            .OrderByDescending(ss => ss.work_num).FirstOrDefault();
                        if (prj_work != null)
                        {
                            prj_work.state_type_id = (int)Enums.PrjWorkStateTypeEnum.CANCELED;
                            prj_work.is_accept = true;
                            prj_work.is_plan = true;
                            prj_work.date_end = today;
                            prj_work.date_time_end = today;
                            prj_work.upd_date = now;
                            prj_work.upd_user = user;
                            prj_work.upd_num = prj_work.upd_num + 1;
                            //

                            if (!String.IsNullOrWhiteSpace(comment))
                            {
                                prj_mess = new prj_mess();
                                prj_mess.mess = comment;
                                prj_mess.user_id = curr_user_id.GetValueOrDefault(0);
                                prj_mess.prj_id = null;
                                prj_mess.claim_id = prj_task.claim_id;
                                prj_mess.task_id = prj_task.task_id;
                                prj_mess.work_id = null;
                                prj_mess.crt_date = now;
                                prj_mess.crt_user = user;
                                prj_mess.upd_date = now;
                                prj_mess.upd_user = user;
                                prj_mess.upd_num = 1;
                                dbContext.prj_mess.Add(prj_mess);
                            }

                            ok = true;
                        }
                        break;
                        #endregion
                    default:
                        modelState.AddModelError("", "Неподдерживаемый тип операции [" + action_id.GetValueOrDefault(0).ToString() + "]");
                        return false;
                }

                if (!ok)
                {
                    return true;
                }

                prj_claim.upd_date = now;
                prj_claim.upd_user = user;
                prj_claim.upd_num = prj_claim.upd_num + 1;

                prj_task.upd_date = now;
                prj_task.upd_user = user;
                prj_task.upd_num = prj_task.upd_num + 1;                

                dbContext.SaveChanges();

                updateTaskUserState(prj_task.task_id, (int)stateTypeEnum, curr_user_id.GetValueOrDefault(0), curr_from_user_id, comment);                    
                switch (taskActionEnum)
                {
                    case Enums.PrjTaskActionEnum.COMPLETE_AND_FORWARD:
                    case Enums.PrjTaskActionEnum.COMPLETE_AND_RETURN:
                        stateTypeEnum = Enums.PrjTaskUserStateTypeEnum.SENT_TO_WORK;
                        updateTaskUserState(prj_task.task_id, (int)stateTypeEnum, user_id.GetValueOrDefault(0), currUser.CabUserId, comment);
                        break;
                    default:
                        break;
                }

                // !!!
                // new
                vw_prj_claim vw_prj_claim = dbContext.vw_prj_claim.Where(ss => ss.claim_id == prj_task.claim_id).FirstOrDefault();
                updateClaimVersion(vw_prj_claim.claim_id, vw_prj_claim.is_active_state, vw_prj_claim.is_version_fixed);

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region UpdateTaskUserState

        private void updateTaskUserState(int task_id, int state_type_id, int user_id, int? from_user_id, string comment)
        {
            if (claimService == null)
                claimService = DependencyResolver.Current.GetService<IPrjClaimService>();
            if (claimService == null)
                throw new Exception("claimService == null");
            //
            claimService.UpdateTaskUserState(task_id, state_type_id, user_id, from_user_id, comment, true);
        }

        #endregion

        #region UpdateClaimVersion

        private void updateClaimVersion(int claim_id, bool is_active_state, bool is_version_fixed)
        {
            if (claimService == null)
                claimService = DependencyResolver.Current.GetService<IPrjClaimService>();
            if (claimService == null)
                throw new Exception("claimService == null");
            //
            claimService.UpdateClaimVersion(claim_id, is_active_state, is_version_fixed);
        }

        #endregion
    }
}
