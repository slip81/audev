﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimTypeService : IAuBaseService
    {
        IQueryable<PrjClaimTypeViewModel> GetList_forEdit();
    }

    public class PrjClaimTypeService : AuBaseService, IPrjClaimTypeService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_claim_type
                .Where(ss => ss.claim_type_id == id)
                .ProjectTo<PrjClaimTypeViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_claim_type.ProjectTo<PrjClaimTypeViewModel>();
        }

        public IQueryable<PrjClaimTypeViewModel> GetList_forEdit()
        {
            return dbContext.prj_claim_type.Where(ss => ss.claim_type_id > 0).ProjectTo<PrjClaimTypeViewModel>();
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimTypeViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            PrjClaimTypeViewModel itemEdit = (PrjClaimTypeViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var prj_claim_type = dbContext.prj_claim_type.Where(ss => ss.claim_type_id == itemEdit.claim_type_id).FirstOrDefault();
            if (prj_claim_type == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.claim_type_id.ToString());
                return null;
            }

            PrjClaimTypeViewModel oldModel = new PrjClaimTypeViewModel();
            ModelMapper.Map<prj_claim_type, PrjClaimTypeViewModel>(prj_claim_type, oldModel);
            modelBeforeChanges = oldModel;

            prj_claim_type.rate = itemEdit.rate;
            dbContext.SaveChanges();

            return xGetItem(prj_claim_type.claim_type_id);
        }
    }
}
