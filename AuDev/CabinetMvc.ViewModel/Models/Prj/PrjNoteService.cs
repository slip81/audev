﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text.RegularExpressions;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjNoteService : IAuBaseService
    {
        PrjNoteViewModel Insert_forGroup(int user_id, int? group_id, string note_text, ModelStateDictionary modelState);
        PrjNoteViewModel Update_forGroup(int user_id, int? group_id, int? note_id, string note_text, ModelStateDictionary modelState);
        bool Delete_forGroup(int user_id, int? group_id, int? note_id, ModelStateDictionary modelState);        
    }

    public class PrjNoteService : AuBaseService, IPrjNoteService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_prj_note.Where(ss => ss.note_id == id).ProjectTo<PrjNoteViewModel>().FirstOrDefault();
        }

        public PrjNoteViewModel Insert_forGroup(int user_id, int? group_id, string note_text, ModelStateDictionary modelState)
        {
            try
            {
                if (user_id != currUser.CabUserId)
                {
                    modelState.AddModelError("", "Создать новую запись в группе можно только для себя");
                    return null;
                }
                if (String.IsNullOrWhiteSpace(note_text))
                {
                    modelState.AddModelError("", "Не задан текст записи");
                    return null;
                }
                prj_user_group prj_user_group = dbContext.prj_user_group.Where(ss => ss.group_id == group_id).FirstOrDefault();
                if (prj_user_group == null)
                {
                    modelState.AddModelError("", "Не найдена группа с кодом " + group_id.GetValueOrDefault(0).ToString());
                    return null;
                }

                prj_note prj_note = new prj_note();
                prj_note.note_text = note_text;
                prj_note.note_name = note_text;
                dbContext.prj_note.Add(prj_note);

                prj_user_group_item prj_user_group_item_last = dbContext.prj_user_group_item.Where(ss => ss.group_id == group_id).OrderByDescending(ss => ss.ord).FirstOrDefault();

                prj_user_group_item prj_user_group_item = new prj_user_group_item();
                prj_user_group_item.group_id = group_id.GetValueOrDefault(0);
                prj_user_group_item.item_type_id = (int)Enums.PrjUserGroupItemTypeEnum.NOTE;
                prj_user_group_item.ord = prj_user_group_item_last != null ? prj_user_group_item_last.ord + 1 : 1;                
                prj_user_group_item.prj_note = prj_note;
                dbContext.prj_user_group_item.Add(prj_user_group_item);

                dbContext.SaveChanges();

                return (PrjNoteViewModel)GetItem(prj_note.note_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public PrjNoteViewModel Update_forGroup(int user_id, int? group_id, int? note_id, string note_text, ModelStateDictionary modelState)
        {
            try
            {
                if (user_id != currUser.CabUserId)
                {
                    modelState.AddModelError("", "Изменить запись в группе можно только для себя");
                    return null;
                }
                if (String.IsNullOrWhiteSpace(note_text))
                {
                    modelState.AddModelError("", "Не задан текст записи");
                    return null;
                }
                prj_user_group prj_user_group = dbContext.prj_user_group.Where(ss => ss.group_id == group_id).FirstOrDefault();
                if (prj_user_group == null)
                {
                    modelState.AddModelError("", "Не найдена группа с кодом " + group_id.GetValueOrDefault(0).ToString());
                    return null;
                }

                prj_note prj_note = dbContext.prj_note.Where(ss => ss.note_id == note_id).FirstOrDefault();
                if (prj_note == null)
                {
                    modelState.AddModelError("", "Не найдена запись с кодом " + note_id.GetValueOrDefault(0).ToString());
                    return null;
                }
                
                prj_note.note_text = note_text;
                prj_note.note_name = note_text;

                dbContext.SaveChanges();

                return (PrjNoteViewModel)GetItem(prj_note.note_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public bool Delete_forGroup(int user_id, int? group_id, int? note_id, ModelStateDictionary modelState) 
        {
            try
            {
                if (user_id != currUser.CabUserId)
                {
                    modelState.AddModelError("", "Удалить запись в группе можно только для себя");
                    return false;

                }
                prj_user_group prj_user_group = dbContext.prj_user_group.Where(ss => ss.group_id == group_id).FirstOrDefault();
                if (prj_user_group == null)
                {
                    modelState.AddModelError("", "Не найдена группа с кодом " + group_id.GetValueOrDefault(0).ToString());
                    return false;
                }

                prj_note prj_note = dbContext.prj_note.Where(ss => ss.note_id == note_id).FirstOrDefault();
                if (prj_note == null)
                {
                    modelState.AddModelError("", "Не найдена запись с кодом " + note_id.GetValueOrDefault(0).ToString());
                    return false;
                }

                dbContext.prj_user_group_item.RemoveRange(dbContext.prj_user_group_item.Where(ss => ss.note_id == note_id));
                dbContext.prj_note.Remove(prj_note);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }
    }
}