﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjBriefIntervalTypeService : IAuBaseService
    {
        //
    }

    public class PrjBriefIntervalTypeService : AuBaseService, IPrjBriefIntervalTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_brief_interval_type.ProjectTo<PrjBriefIntervalTypeViewModel>();
        }
    }
}
