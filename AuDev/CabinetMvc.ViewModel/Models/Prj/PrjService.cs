﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjService : IAuBaseService
    {
        IQueryable<PrjViewModel> GetList_byFilter(int state_type_id, string search);
        bool DeletePrj(int prj_id, ModelStateDictionary modelState);
        bool RestorePrj(int prj_id, ModelStateDictionary modelState);
    }

    public class PrjService : AuBaseService, IPrjService
    {
        IPrjLogService logService;

        #region Get

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_prj
                .Where(ss => ss.prj_id == id)
                .ProjectTo<PrjViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_prj.Where(ss => !ss.is_archive).ProjectTo<PrjViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_prj
                .Where(ss => !ss.is_archive && (((ss.state_type_id == parent_id) && (parent_id > 0)) || (parent_id <= 0)))
                .ProjectTo<PrjViewModel>();
        }

        public IQueryable<PrjViewModel> GetList_byFilter(int state_type_id, string search)
        {
            var query = dbContext.vw_prj.Where(ss => ss.prj_id > 0 && !ss.is_archive);
            if (state_type_id > 0)
                query = query.Where(ss => ss.state_type_id == state_type_id);
            if (!String.IsNullOrWhiteSpace(search))
                query = query.Where(ss => ss.prj_name.Trim().ToLower().Contains(search.Trim().ToLower())
                ||
                ss.state_type_name.Trim().ToLower().Contains(search.Trim().ToLower())
                );
            return query
                .ProjectTo<PrjViewModel>();
        }

        #endregion

        #region Insert

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjViewModel itemAdd = (PrjViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты группы");
                return null;
            }

            prj prj_existing = dbContext.prj.Where(ss => ss.prj_id != itemAdd.prj_id && ss.prj_name.Trim().ToLower().Equals(itemAdd.prj_name.Trim().ToLower())).FirstOrDefault();
            if (prj_existing != null)
            {
                modelState.AddModelError("", "Уже есть группа с таким названием");
                return null;
            }
            
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string user = currUser.CabUserName;

            itemAdd.prj_name = itemAdd.prj_name.Trim();
            prj prj = new prj();
            ModelMapper.Map<PrjViewModel, prj>(itemAdd, prj);
            prj.project_id = itemAdd.CrmProject == null ? null : (int?)itemAdd.CrmProject.project_id;
            prj.is_archive = false;
            prj.curr_state_id = null;
            prj.crt_date = now;
            prj.crt_user = user;
            prj.upd_date = now;
            prj.upd_user = user;
            prj.upd_num = 0;

            dbContext.prj.Add(prj);

            prj_state prj_state = new prj_state();
            prj_state.prj = prj;
            //prj_state.state_type_id = itemAdd.state_type_id.GetValueOrDefault(1);
            prj_state.state_type_id = itemAdd.PrjStateType == null ? 1 : itemAdd.PrjStateType.state_type_id;
            prj_state.date_beg = today;
            prj_state.date_end = null;
            prj_state.crt_date = now;
            prj_state.crt_user = user;            

            dbContext.prj_state.Add(prj_state);

            dbContext.SaveChanges();

            int curr_state_id = dbContext.prj_state.Where(ss => ss.prj_id == prj.prj_id).Select(ss => ss.state_id).FirstOrDefault();
            prj.curr_state_id = curr_state_id;

            dbContext.SaveChanges();

            return xGetItem(prj.prj_id);
        }

        #endregion

        #region Update

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjViewModel itemEdit = (PrjViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры группы");
                return null;
            }

            prj prj = dbContext.prj.Where(ss => ss.prj_id == itemEdit.prj_id).FirstOrDefault();
            if (prj == null)
            {
                modelState.AddModelError("", "Не найдены параметры группы");
                return null;
            }

            prj prj_existing = dbContext.prj.Where(ss => ss.prj_id != itemEdit.prj_id && ss.prj_name.Trim().ToLower().Equals(itemEdit.prj_name.Trim().ToLower())).FirstOrDefault();
            if (prj_existing != null)
            {
                modelState.AddModelError("", "Уже есть группа с таким названием");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string user = currUser.CabUserName;

            PrjViewModel oldModel = new PrjViewModel();
            ModelMapper.Map<prj, PrjViewModel>(prj, oldModel);
            modelBeforeChanges = oldModel;

            int? curr_state_id = prj.curr_state_id;
            int? new_state_type_id = itemEdit.PrjStateType == null ? null : (int?)itemEdit.PrjStateType.state_type_id;
            prj_state prj_state_existing = dbContext.prj_state.Where(ss => ss.prj_id == prj.prj_id && !ss.date_end.HasValue).OrderByDescending(ss => ss.state_id).FirstOrDefault();
            if ((prj_state_existing == null) || ((prj_state_existing != null) && (prj_state_existing.state_type_id != new_state_type_id)))
            {
                if (prj_state_existing != null)
                    prj_state_existing.date_end = today;
                prj_state prj_state = new prj_state();
                prj_state.prj = prj;
                prj_state.state_type_id = new_state_type_id.GetValueOrDefault(1);
                prj_state.date_beg = today;
                prj_state.date_end = null;
                prj_state.crt_date = now;
                prj_state.crt_user = user;
                dbContext.prj_state.Add(prj_state);
                dbContext.SaveChanges();
                curr_state_id = prj_state.state_id;
            }

            prj.prj_name = itemEdit.prj_name.Trim();
            prj.project_id = itemEdit.CrmProject == null ? null : (int?)itemEdit.CrmProject.project_id;
            prj.date_beg = itemEdit.date_beg;
            prj.date_end = itemEdit.date_end;
            prj.curr_state_id = curr_state_id;
            prj.upd_date = now;
            prj.upd_user = user;
            prj.upd_num = prj.upd_num + 1;

            dbContext.SaveChanges();

            return xGetItem(prj.prj_id);
        }

        #endregion

        #region Delete

        public bool DeletePrj(int prj_id, ModelStateDictionary modelState)
        {
            try
            {
                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;
                prj prj = dbContext.prj.Where(ss => ss.prj_id == prj_id && !ss.is_archive).FirstOrDefault();
                if (prj != null)
                {

                    prj_claim_prj prj_claim_prj = dbContext.prj_claim_prj.Where(ss => ss.prj_id == prj_id).FirstOrDefault();
                    if (prj_claim_prj != null)
                    {
                        modelState.AddModelError("", "Есть задачи с такой группой");
                        return false;
                    }

                    prj.is_archive = true;
                    prj.upd_date = now;
                    prj.upd_user = user;
                    prj.upd_num = prj.upd_num + 1;
                    dbContext.SaveChanges();

                    string logMess = user + " " + ("удалил").ToSex(currUser.IsMale) + " группу " + prj.prj_name;
                    ToPrjLog(now, logMess, prj_id, null, null, null);
                }
                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region Restore

        public bool RestorePrj(int prj_id, ModelStateDictionary modelState)
        {
            try
            {
                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;
                prj prj = dbContext.prj.Where(ss => ss.prj_id == prj_id && ss.is_archive).FirstOrDefault();
                if (prj != null)
                {
                    prj.is_archive = false;
                    prj.upd_date = now;
                    prj.upd_user = user;
                    prj.upd_num = prj.upd_num + 1;
                    dbContext.SaveChanges();

                    string logMess = user + " " + ("восстановил").ToSex(currUser.IsMale) + " группу " + prj.prj_name;
                    ToPrjLog(now, logMess, prj_id, null, null, null);
                }
                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #region Log

        private void ToPrjLog(DateTime crt_date, string mess, int? prj_id, int? claim_id, int? task_id, int? work_id)
        {
            if (logService == null)
                logService = DependencyResolver.Current.GetService<IPrjLogService>();
            if (logService == null)
                throw new Exception("logService == null");
            //
            logService.InsertLogPrj(crt_date, mess, prj_id, claim_id, task_id, work_id);
        }

        #endregion

        #endregion

    }
}
