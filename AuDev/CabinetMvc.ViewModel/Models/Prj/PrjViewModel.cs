﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Crm;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjViewModel : AuBaseViewModel
    {

        public PrjViewModel()
            : base(Enums.MainObjectType.PRJ)
        {
            //vw_prj
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int prj_id { get; set; }

        [Display(Name = "Название")]
        public string prj_name { get; set; }

        [Display(Name = "Дата начала")]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "Дата окончания")]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> curr_state_id { get; set; }

        [Display(Name = "Создан")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменен")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "Задач всего")]
        public int claim_cnt { get; set; }

        [Display(Name = "Неразобрано")]
        public int new_claim_cnt { get; set; }

        [Display(Name = "Активно")]
        public int active_claim_cnt { get; set; }

        [Display(Name = "Выполнено")]
        public int done_claim_cnt { get; set; }

        [Display(Name = "Отменено")]
        public int canceled_claim_cnt { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> state_type_id { get; set; }

        [Display(Name = "Статус")]
        public string state_type_name { get; set; }

        [Display(Name = "Статус")]
        [JsonIgnore()]
        public string state_type_name_templ { get; set; }

        [Display(Name = "Утвержденных")]
        public int claim_approved_cnt { get; set; }

        [Display(Name = "Новых")]
        public int claim_new_cnt { get; set; }

        [Display(Name = "Группа удалена")]
        public bool is_archive { get; set; }

        [Display(Name = "Проект")]
        public Nullable<int> project_id { get; set; }

        [Display(Name = "Проект")]
        public string project_name { get; set; }

        [Display(Name = "Статус")]
        [UIHint("PrjStateTypeCombo")]
        public PrjStateTypeViewModel PrjStateType { get; set; }

        [Display(Name = "Проект")]
        [UIHint("CrmProjectCombo")]
        public CrmProjectViewModel CrmProject { get; set; }
    }
}