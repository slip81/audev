﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjTaskViewModel : AuBaseViewModel
    {

        public PrjTaskViewModel()
            : base(Enums.MainObjectType.PRJ_TASK)
        {
            //vw_prj_task
        }

        // db
        
        [Key()]
        [Display(Name = "Код")]
        public int task_id { get; set; }

        [Display(Name = "Задача")]
        public int claim_id { get; set; }

        [Display(Name = "Номер задания")]
        public int task_num { get; set; }

        [Display(Name = "Формулировка")]
        public string task_text { get; set; }

        [Display(Name = "Плановая дата")]
        public Nullable<System.DateTime> date_plan { get; set; }

        [Display(Name = "Фактическая дата")]
        public Nullable<System.DateTime> date_fact { get; set; }

        [Display(Name = "Создана")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменена")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "Номер изменения")]
        public int upd_num { get; set; }

        [Display(Name = "old_task_id")]
        public Nullable<int> old_task_id { get; set; }

        [Display(Name = "old_subtask_id")]
        public Nullable<int> old_subtask_id { get; set; }

        [Display(Name = "Задача")]
        public string claim_num { get; set; }

        [Display(Name = "Название задачи")]
        public string claim_name { get; set; }

        [Display(Name = "Текст задачи")]
        public string claim_text { get; set; }

        [Display(Name = "Проект")]
        public int claim_project_id { get; set; } 

        [Display(Name = "Модуль")]
        public int claim_module_id { get; set; }

        [Display(Name = "Раздел модуля")]
        public int claim_module_part_id { get; set; }

        [Display(Name = "Версия проекта")]
        public int claim_project_version_id { get; set; }

        [Display(Name = "Источник")]
        public int claim_client_id { get; set; }

        [Display(Name = "Приоритет")]        
        public int claim_priority_id { get; set; }

        [Display(Name = "Ответственный")]
        public int claim_resp_user_id { get; set; }

        [Display(Name = "Плановая дата задачи")]
        public Nullable<System.DateTime> claim_date_plan { get; set; }

        [Display(Name = "Фактическая дата задачи")]
        public Nullable<System.DateTime> claim_date_fact { get; set; }

        [Display(Name = "Версия исправления")]
        public Nullable<int> claim_project_repair_version_id { get; set; }

        [Display(Name = "В архиве")]
        public bool claim_is_archive { get; set; }

        [Display(Name = "Контроль дат")]
        public bool claim_is_control { get; set; }

        [Display(Name = "Проект")]
        public string claim_project_name { get; set; }

        [Display(Name = "Модуль")]
        public string claim_module_name { get; set; }

        [Display(Name = "Раздел модуля")]
        public string claim_module_part_name { get; set; }

        [Display(Name = "Версия проекта")]
        public string claim_project_version_name { get; set; }

        [Display(Name = "Источник")]
        public string claim_client_name { get; set; }

        [Display(Name = "Приоритет")]        
        public string claim_priority_name { get; set; }

        [Display(Name = "Ответственный")]
        public string claim_resp_user_name { get; set; }

        [Display(Name = "Версия исправления")]
        public string claim_project_repair_version_name { get; set; }

        [Display(Name = "Статус задачи")]
        public int claim_state_type_id { get; set; }

        [Display(Name = "Статус задачи")]
        public string claim_state_type_name { get; set; }

        [Display(Name = "Статус задачи")]
        public string claim_state_type_templ { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> task_state_type_id { get; set; }

        [Display(Name = "Работ всего")]
        public int work_cnt { get; set; }

        [Display(Name = "Активно")]
        public int active_work_cnt { get; set; }

        [Display(Name = "Выполнено")]
        public int done_work_cnt { get; set; }

        [Display(Name = "Отменено")]
        public int canceled_work_cnt { get; set; }

        [Display(Name = "Статус")]
        public string task_state_type_name { get; set; }

        [Display(Name = "Статус")]
        public string task_state_type_templ { get; set; }

        [Display(Name = "Статус [old]")]
        public Nullable<int> old_state_id { get; set; }

        [Display(Name = "Статус [old]")]
        public string old_state_name { get; set; }

        [Display(Name = "Стадия")]
        public Nullable<int> active_work_type_id { get; set; }

        [Display(Name = "Стадия")]
        public string active_work_type_name { get; set; }

        [Display(Name = "Просрочено")]
        public bool is_overdue { get; set; }

        [Display(Name = "Тип")]
        public int claim_type_id { get; set; }

        [Display(Name = "Этап")]
        public int claim_stage_id { get; set; }

        [Display(Name = "Тип")]
        public string claim_type_name { get; set; }

        [Display(Name = "Этап")]
        public string claim_stage_name { get; set; }

        [Display(Name = "Комментарии")]
        public Nullable<int> task_mess_cnt { get; set; }

        [Display(Name = "Группы")]
        public string prj_id_list { get; set; }

        [Display(Name = "Группы")]
        public string prj_name_list { get; set; }

        // additional

        [JsonIgnore()]
        public bool is_new { get; set; }

        [JsonIgnore()]
        [Display(Name = "Формулировка")]
        public string task_text_short { get { return this.task_text.CutToLengthWithDots(200); } }

        [Display(Name = "Статус")]
        public int? user_state_type_id { get; set; }

        [Display(Name = "Статус")]
        public string user_state_type_name { get; set; }

        [Display(Name = "Статус")]
        public string user_state_type_templ 
        { 
            get 
            { 
                switch (user_state_type_id)
                {
                    case (int)Enums.PrjTaskUserStateTypeEnum.SENT_TO_WORK:
                        return "<span class='k-sprite cab-png cab-png-send-task' title='Передано в работу'></span>"; 
                    case (int)Enums.PrjTaskUserStateTypeEnum.RECEIVED_TO_WORK:
                        return "<span class='k-sprite cab-png cab-png-receive-task' title='Принято в работу'></span>";
                    case (int)Enums.PrjTaskUserStateTypeEnum.COMPLETED:
                        return "<span class='k-sprite cab-png cab-png-done-task' title='Выполнено'></span>";
                    case (int)Enums.PrjTaskUserStateTypeEnum.CANCELED:
                        return "<span class='k-sprite cab-png cab-png-cancel-task' title='Отменено'></span>"; 
                    default:
                        return ""; 
                }                
            } 
        }

        [Display(Name = "Кто")]
        public int? user_state_user_id { get; set; }

        [Display(Name = "Кто")]
        public string user_state_user_name { get; set; }

        [Display(Name = "От кого")]
        public int? user_state_from_user_id { get; set; }

        [Display(Name = "От кого")]
        public string user_state_from_user_name { get; set; }

        [Display(Name = "Комментарий")]
        public string user_state_mess { get; set; }

        [Display(Name = "Когда")]
        public DateTime? user_state_crt_date { get; set; }

    }
}