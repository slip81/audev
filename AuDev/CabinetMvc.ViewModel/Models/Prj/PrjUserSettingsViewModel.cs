﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjUserSettingsViewModel : AuBaseViewModel
    {

        public PrjUserSettingsViewModel()
            : base(Enums.MainObjectType.PRJ_USER_SETTINGS)
        {
            //prj_user_settings
            isParsed = false;
        }
        
        // db
        
        [Key()]
        [Display(Name = "Код")]
        public int setting_id { get; set; }

        [Display(Name = "Навзвание")]
        public string setting_name { get; set; }

        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Display(Name = "Страница")]
        public int page_type_id { get; set; }

        [Display(Name = "Настройки")]
        public string settings { get; set; }

        [Display(Name = "Настройки грида")]
        public string settings_grid { get; set; }

        [Display(Name = "Текущая")]
        public bool is_selected { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "tag")]
        public string tag { get; set; }

        // additional

        [JsonIgnore()]
        public Dictionary<string, string> prjUserSettingsList
        {
            get
            {
                if (!isParsed)
                {
                    isParsed = true;
                    List<PrjUserSettings> settings_deser = String.IsNullOrEmpty(settings) ? null : JsonConvert.DeserializeObject<List<PrjUserSettings>>(settings);
                    _prjUserSettingsList = settings_deser == null ? null : settings_deser.ToDictionary(ss => ss.id, ss => ss.val);
                }
                return _prjUserSettingsList;
            }
            set
            {
                _prjUserSettingsList = value;
            }
        }

        private Dictionary<string, string> _prjUserSettingsList;

        private bool isParsed;

        [Display(Name = "Настройки грида")]
        public string settings_grid_json { get { return String.IsNullOrWhiteSpace(settings_grid) ? null : JsonConvert.SerializeObject(settings_grid); } }
    }

    public class PrjUserSettings
    {
        public string id { get; set; }
        public string val { get; set; }
    }
}