﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjLogViewModel : AuBaseViewModel
    {

        public PrjLogViewModel()
            : base(Enums.MainObjectType.PRJ_LOG)
        {
            //vw_prj_log
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int log_id { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }

        [Display(Name = "Проект")]
        public Nullable<int> prj_id { get; set; }

        [Display(Name = "Задача")]
        public Nullable<int> claim_id { get; set; }

        [Display(Name = "Задание")]
        public Nullable<int> task_id { get; set; }

        [Display(Name = "Работа")]
        public Nullable<int> work_id { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Задача")]
        public string claim_num { get; set; }

        [Display(Name = "Задание")]
        public Nullable<int> task_num { get; set; }

        [Display(Name = "Работа")]
        public Nullable<int> work_num { get; set; }        

        [Display(Name = "Задача удалена")]
        public Nullable<bool> claim_is_archive { get; set; }

        [Display(Name = "Проект удален")]
        public Nullable<bool> prj_is_archive { get; set; }        
    }
}