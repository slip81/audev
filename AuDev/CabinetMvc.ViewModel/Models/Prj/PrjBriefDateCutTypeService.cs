﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjBriefDateCutTypeService : IAuBaseService
    {
        //
    }

    public class PrjBriefDateCutTypeService : AuBaseService, IPrjBriefDateCutTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_brief_date_cut_type.ProjectTo<PrjBriefDateCutTypeViewModel>();
        }
    }
}
