﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjWorkTypeService : IAuBaseService
    {
        IQueryable<PrjWorkTypeViewModel> GetList_Default();
    }

    public class PrjWorkTypeService : AuBaseService, IPrjWorkTypeService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_work_type
                .Where(ss => ss.work_type_id == id)
                .ProjectTo<PrjWorkTypeViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_work_type.OrderBy(ss => ss.work_type_id).ProjectTo<PrjWorkTypeViewModel>();
        }

        public IQueryable<PrjWorkTypeViewModel> GetList_Default()
        {
            return dbContext.vw_prj_work_default
                .Where(ss => ss.user_id == currUser.CabUserId)
                .Select(ss => new PrjWorkTypeViewModel()
                {
                    work_type_id = ss.work_type_id,
                    is_default = ss.is_default,
                    is_accept_default = ss.is_accept,
                    work_type_name = ss.work_type_name,
                    exec_user_id = ss.exec_user_id,
                });
        }

    }
}