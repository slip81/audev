﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjTaskUserStateViewModel : AuBaseViewModel
    {

        public PrjTaskUserStateViewModel()
            : base()
        {
            // сервиса нет, этот класс используется в методе котроллера PrjClaimEditPartial для передачи JSON-объекта с клиента
        }
        
        [Display(Name = "Задание")]
        public int task_num { get; set; }

        [Display(Name = "Статус")]
        public int? user_state_type_id { get; set; }

        [Display(Name = "Кто")]
        public int? user_state_user_id { get; set; }

        [Display(Name = "Когда")]
        public DateTime? user_state_crt_date { get; set; }
    }
}