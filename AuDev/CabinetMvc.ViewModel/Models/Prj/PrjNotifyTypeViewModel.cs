﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjNotifyTypeViewModel : AuBaseViewModel
    {

        public PrjNotifyTypeViewModel()
            : base(Enums.MainObjectType.PRJ_NOTIFY_TYPE)
        {
            //prj_notify_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int notify_type_id { get; set; }

        [Display(Name = "Название")]
        public string notify_type_name { get; set; }

        [Display(Name = "Минуты")]
        public Nullable<int> minute_count { get; set; }
    }
}