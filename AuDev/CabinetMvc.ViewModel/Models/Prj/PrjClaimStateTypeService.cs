﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimStateTypeService : IAuBaseService
    {
        //
    }

    public class PrjClaimStateTypeService : AuBaseService, IPrjClaimStateTypeService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_claim_state_type
                .Where(ss => ss.state_type_id == id)
                .ProjectTo<PrjClaimStateTypeViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_claim_state_type.ProjectTo<PrjClaimStateTypeViewModel>();
        }
    }
}
