﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjWorkViewModel : AuBaseViewModel
    {

        public PrjWorkViewModel()
            : base(Enums.MainObjectType.PRJ_WORK)
        {
            //vw_prj_work
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int work_id { get; set; }

        [Display(Name = "Задание")]
        public int task_id { get; set; }

        [Display(Name = "Задача")]
        public int claim_id { get; set; }

        [Display(Name = "Номер работы")]
        public int work_num { get; set; }

        [Display(Name = "Тип")]
        public int work_type_id { get; set; }

        [Display(Name = "Исполнитель")]
        public int exec_user_id { get; set; }

        [Display(Name = "Статус")]
        public int state_type_id { get; set; }

        [Display(Name = "В календаре")]
        public bool is_plan { get; set; }

        [Display(Name = "Когда принято")]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "Когда выполнено")]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "Принято")]
        public bool is_accept { get; set; }

        [Display(Name = "Создана")]
        public Nullable<System.DateTime> crt_date { get; set; }
        
        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменена")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "Номер изменения")]
        public int upd_num { get; set; }

        [Display(Name = "ТЗ план")]
        public Nullable<decimal> cost_plan { get; set; }

        [Display(Name = "ТЗ факт")]
        public Nullable<decimal> cost_fact { get; set; }

        [Display(Name = "Комментарий")]
        public string mess { get; set; }

        [Display(Name = "old_task_id")]
        public Nullable<int> old_task_id { get; set; }

        [Display(Name = "old_subtask_id")]
        public Nullable<int> old_subtask_id { get; set; }

        [Display(Name = "Тип")]
        public string work_type_name { get; set; }

        [Display(Name = "Исполнитель")]
        public string exec_user_name { get; set; }
        
        [Display(Name = "Статус")]
        public string state_type_name { get; set; }

        [Display(Name = "Статус")]
        public string state_type_templ { get; set; }

        [Display(Name = "Задание")]
        public int task_num { get; set; }

        [Display(Name = "Текст задания")]
        public string task_text { get; set; }

        [Display(Name = "Статус задания")]
        public int task_state_type_id { get; set; }

        [Display(Name = "Плановая дата задания")]
        public Nullable<System.DateTime> task_date_plan { get; set; }

        [Display(Name = "Фактическая дата задания")]
        public Nullable<System.DateTime> task_date_fact { get; set; }

        [Display(Name = "Статус задания")]
        public string task_state_type_name { get; set; }

        [Display(Name = "Статус задания")]
        public string task_state_type_templ { get; set; }

        [Display(Name = "Задача")]
        public string claim_num { get; set; }

        [Display(Name = "Название задачи")]
        public string claim_name { get; set; }

        [Display(Name = "Текст задачи")]
        public string claim_text { get; set; }

        [Display(Name = "Проект")]
        public int claim_project_id { get; set; }

        [Display(Name = "Модуль")]
        public int claim_module_id { get; set; }

        [Display(Name = "Раздел модуля")]
        public int claim_module_part_id { get; set; }

        [Display(Name = "Версия проекта")]
        public int claim_project_version_id { get; set; }

        [Display(Name = "Источник")]
        public int claim_client_id { get; set; }

        [Display(Name = "Приоритет")]
        public int claim_priority_id { get; set; }

        [Display(Name = "Ответственный")]
        public int claim_resp_user_id { get; set; }

        [Display(Name = "Плановая дата задачи")]
        public Nullable<System.DateTime> claim_date_plan { get; set; }

        [Display(Name = "Фактическая дата задачи")]
        public Nullable<System.DateTime> claim_date_fact { get; set; }

        [Display(Name = "Версия исправления")]
        public Nullable<int> claim_project_repair_version_id { get; set; }

        [Display(Name = "В архиве")]
        public bool claim_is_archive { get; set; }

        [Display(Name = "Контроль дат")]
        public bool claim_is_control { get; set; }

        [Display(Name = "Проект")]
        public string claim_project_name { get; set; }

        [Display(Name = "Модуль")]
        public string claim_module_name { get; set; }

        [Display(Name = "Раздел модуля")]
        public string claim_module_part_name { get; set; }

        [Display(Name = "Версия проекта")]
        public string claim_project_version_name { get; set; }

        [Display(Name = "Источник")]
        public string claim_client_name { get; set; }

        [Display(Name = "Приоритет")]
        public string claim_priority_name { get; set; }

        [Display(Name = "Ответственный")]
        public string claim_resp_user_name { get; set; }

        [Display(Name = "Версия исправления")]
        public string claim_project_repair_version_name { get; set; }

        [Display(Name = "Статус задачи")]
        public int claim_state_type_id { get; set; }

        [Display(Name = "Статус задачи")]
        public string claim_state_type_name { get; set; }

        [Display(Name = "Статус задачи")]
        public string claim_state_type_templ { get; set; }

        [Display(Name = "Просрочено")]
        public bool is_overdue { get; set; }

        [Display(Name = "В календаре с")]
        public Nullable<System.DateTime> date_time_beg { get; set; }

        [Display(Name = "В календаре по")]
        public Nullable<System.DateTime> date_time_end { get; set; }

        [Display(Name = "На весь день")]
        public bool is_all_day { get; set; }

        [Display(Name = "Тип")]
        public int claim_type_id { get; set; }

        [Display(Name = "Этап")]
        public int claim_stage_id { get; set; }

        [Display(Name = "Тип")]
        public string claim_type_name { get; set; }

        [Display(Name = "Этап")]
        public string claim_stage_name { get; set; }

        [Display(Name = "Статус сводки")]
        public Nullable<int> brief_state_type_id { get; set; }

        [Display(Name = "Статус сводки")]
        public string brief_state_type_name { get; set; }

        [Display(Name = "Когда передано")]
        public Nullable<System.DateTime> date_send { get; set; }

        [Display(Name = "Группы")]
        public string prj_id_list { get; set; }

        [Display(Name = "Группы")]
        public string prj_name_list { get; set; }

        // additional

        [JsonIgnore()]
        public bool is_new { get; set; }

        [JsonIgnore()]
        [Display(Name = "Задание")]
        public string task_text_short { get { return this.task_text.CutToLengthWithDots(200); } }

    }
}