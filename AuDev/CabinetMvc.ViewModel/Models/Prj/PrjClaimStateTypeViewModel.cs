﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimStateTypeViewModel : AuBaseViewModel
    {

        public PrjClaimStateTypeViewModel()
            : base(Enums.MainObjectType.PRJ_CLAIM_STATE_TYPE)
        {
            //prj_claim_state_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int state_type_id { get; set; }

        [Display(Name = "Название")]
        public string state_type_name { get; set; }

        [Display(Name = "Шаблон")]
        public string templ { get; set; }

        [Display(Name = "Статус сводки")]
        public Nullable<int> brief_state_type_id { get; set; }
    }
}