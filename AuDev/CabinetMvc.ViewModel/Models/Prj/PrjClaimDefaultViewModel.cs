﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimDefaultViewModel : AuBaseViewModel
    {

        public PrjClaimDefaultViewModel()
            : base(Enums.MainObjectType.PRJ_CLAIM_DEFAULT)
        {
            //prj_claim_default
        }
        
        // db

        [Key()]
        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Display(Name = "Проект")]
        public int prj_id { get; set; }

        [Display(Name = "Программа")]
        public int project_id { get; set; }

        [Display(Name = "Модуль")]
        public int module_id { get; set; }

        [Display(Name = "Раздел модуля")]
        public int module_part_id { get; set; }

        [Display(Name = "Версия модуля")]
        public int module_version_id { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Приоритет")]
        public int priority_id { get; set; }

        [Display(Name = "Ответственный")]
        public int resp_user_id { get; set; }

        [Display(Name = "Версия исправления")]
        public Nullable<int> repair_version_id { get; set; }
    }
}