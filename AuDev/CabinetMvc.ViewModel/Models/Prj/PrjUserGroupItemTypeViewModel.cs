﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjUserGroupItemTypeViewModel : AuBaseViewModel
    {

        public PrjUserGroupItemTypeViewModel()
            : base()
        {
            //prj_user_group_item_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int item_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string item_type_name { get; set; }
    }
}