﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimStageService : IAuBaseService
    {
        //GetList_Edit
        IQueryable<PrjClaimStageViewModel> GetList_Edit();
    }

    public class PrjClaimStageService : AuBaseService, IPrjClaimStageService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_claim_stage
                .Where(ss => ss.claim_stage_id == id)
                .ProjectTo<PrjClaimStageViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_claim_stage.ProjectTo<PrjClaimStageViewModel>();
        }

        public IQueryable<PrjClaimStageViewModel> GetList_Edit()
        {
            return dbContext.prj_claim_stage.Where(ss => ss.claim_stage_id > 0).ProjectTo<PrjClaimStageViewModel>();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimStageViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            PrjClaimStageViewModel itemAdd = (PrjClaimStageViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            prj_claim_stage prj_claim_stage_last = dbContext.prj_claim_stage.OrderByDescending(ss => ss.claim_stage_id).FirstOrDefault();

            prj_claim_stage prj_claim_stage = new prj_claim_stage();
            prj_claim_stage.claim_stage_id = prj_claim_stage_last != null ? prj_claim_stage_last.claim_stage_id + 1 : 1;
            prj_claim_stage.claim_stage_name = itemAdd.claim_stage_name;
            dbContext.prj_claim_stage.Add(prj_claim_stage);
            dbContext.SaveChanges();

            return xGetItem(prj_claim_stage.claim_stage_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimStageViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            PrjClaimStageViewModel itemEdit = (PrjClaimStageViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var prj_claim_stage = dbContext.prj_claim_stage.Where(ss => ss.claim_stage_id == itemEdit.claim_stage_id).FirstOrDefault();
            if (prj_claim_stage == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.claim_stage_id.ToString());
                return null;
            }

            PrjClaimStageViewModel oldModel = new PrjClaimStageViewModel();
            ModelMapper.Map<prj_claim_stage, PrjClaimStageViewModel>(prj_claim_stage, oldModel);
            modelBeforeChanges = oldModel;

            prj_claim_stage.claim_stage_name = itemEdit.claim_stage_name;
            dbContext.SaveChanges();

            return xGetItem(prj_claim_stage.claim_stage_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimStageViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            PrjClaimStageViewModel itemDel = (PrjClaimStageViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return false;
            }

            var prj_claim_stage = dbContext.prj_claim_stage.Where(ss => ss.claim_stage_id == itemDel.claim_stage_id).FirstOrDefault();
            if (prj_claim_stage == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemDel.claim_stage_id.ToString());
                return false;
            }

            var prj_claim = dbContext.prj_claim.Where(ss => ss.claim_stage_id == prj_claim_stage.claim_stage_id).FirstOrDefault();
            if (prj_claim != null)
            {
                modelState.AddModelError("", "Есть задачи с этим этапом");
                return false;
            }

            dbContext.prj_claim_stage.Remove(prj_claim_stage);
            dbContext.SaveChanges();

            return true;
        }
    }
}
