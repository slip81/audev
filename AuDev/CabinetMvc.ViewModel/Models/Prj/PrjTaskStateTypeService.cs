﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjTaskStateTypeService : IAuBaseService
    {
        //
    }

    public class PrjTaskStateTypeService : AuBaseService, IPrjTaskStateTypeService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_task_state_type
                .Where(ss => ss.state_type_id == id)
                .ProjectTo<PrjTaskStateTypeViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_task_state_type.ProjectTo<PrjTaskStateTypeViewModel>();
        }
    }
}
