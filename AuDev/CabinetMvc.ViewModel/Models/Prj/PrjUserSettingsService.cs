﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjUserSettingsService : IAuBaseService
    {
        //PrjUserSettingsViewModel UpdateSettings(int page_type_id, string settings, int? num, string name, ModelStateDictionary modelState);
        PrjUserSettingsViewModel UpdateSettings(int page_type_id, int mode, int? setting_id, string setting_name, string settings, string settings_grid,
            string tag, ModelStateDictionary modelState);
        bool ChangeSettings(int? old_setting_id, int? new_setting_id, ModelStateDictionary modelState);
        List<PrjUserSettingsViewModel> GetList_byUser(int user_id);
        IQueryable<PrjUserSettingsViewModel> GetList_byPage(int user_id, int page_type_id);
    }

    public class PrjUserSettingsService : AuBaseService, IPrjUserSettingsService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_user_settings.Where(ss => ss.setting_id == id).ProjectTo<PrjUserSettingsViewModel>().FirstOrDefault();
        }

        public List<PrjUserSettingsViewModel> GetList_byUser(int user_id)
        {
            return dbContext.prj_user_settings
                .Where(ss => ss.user_id == user_id)
                .ProjectTo<PrjUserSettingsViewModel>()
                .ToList()
                ;
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.prj_user_settings
                .Where(ss => ss.user_id == parent_id)
                .ProjectTo<PrjUserSettingsViewModel>()                
                ;
        }

        public IQueryable<PrjUserSettingsViewModel> GetList_byPage(int user_id, int page_type_id)
        {
            return dbContext.prj_user_settings
                .Where(ss => ss.user_id == user_id && ss.page_type_id == page_type_id)
                .ProjectTo<PrjUserSettingsViewModel>()
                ;
        }


        public PrjUserSettingsViewModel UpdateSettings(int page_type_id, int mode, int? setting_id, string setting_name, string settings, string settings_grid, 
            string tag, ModelStateDictionary modelState)
        {
            try
            {
                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;

                int user_id = currUser.CabUserId;

                prj_user_settings prj_user_settings = null;

                prj_user_settings prj_user_settings_selected = dbContext.prj_user_settings
                    .Where(ss => ss.setting_id != setting_id && ss.user_id == user_id && ss.page_type_id == page_type_id && ss.is_selected)
                    .FirstOrDefault();
                if (prj_user_settings_selected != null)
                {
                    prj_user_settings_selected.is_selected = false;
                    prj_user_settings_selected.upd_date = now;
                    prj_user_settings_selected.upd_user = user;
                }
                switch ((Enums.PrjUserSettingsSaveModeEnum)mode)
                {
                    case Enums.PrjUserSettingsSaveModeEnum.NEW_SETTING:
                        prj_user_settings = new prj_user_settings();
                        prj_user_settings.user_id = user_id;
                        prj_user_settings.page_type_id = page_type_id;
                        prj_user_settings.settings = settings;
                        prj_user_settings.settings_grid = settings_grid;
                        prj_user_settings.setting_name = setting_name;
                        prj_user_settings.is_selected = true;
                        prj_user_settings.tag = tag;
                        prj_user_settings.crt_date = now;
                        prj_user_settings.crt_user = user;
                        prj_user_settings.upd_date = now;
                        prj_user_settings.upd_user = user;
                        dbContext.prj_user_settings.Add(prj_user_settings);
                        break;
                    case Enums.PrjUserSettingsSaveModeEnum.REWRITE_CURRENT:
                    case Enums.PrjUserSettingsSaveModeEnum.REWRITE_OTHER:
                        prj_user_settings = dbContext.prj_user_settings.Where(ss => ss.setting_id == setting_id).FirstOrDefault();
                        if (prj_user_settings == null)
                        {
                            modelState.AddModelError("", "Не найдена конфигурация с кодом " + setting_id.GetValueOrDefault(0).ToString());
                            return null;
                        }
                        //
                        List<PrjUserSettings> curr_settings = prj_user_settings.settings != null ? JsonConvert.DeserializeObject<List<PrjUserSettings>>(prj_user_settings.settings) : new List<PrjUserSettings>();
                        List<PrjUserSettings> new_settings = settings != null ? JsonConvert.DeserializeObject<List<PrjUserSettings>>(settings) : new List<PrjUserSettings>();
                        List<PrjUserSettings> added_settings = new List<PrjUserSettings>();
                        foreach (var new_setting in new_settings)
                        {
                            var curr_setting = curr_settings.Where(ss => ss.id.Trim().ToLower().Equals(new_setting.id.Trim().ToLower())).FirstOrDefault();
                            if (curr_setting != null)
                            {
                                curr_setting.val = new_setting.val;
                            }
                            else
                            {
                                added_settings.Add(new_setting);
                            }
                        }
                        curr_settings.AddRange(added_settings);
                        //
                        prj_user_settings.settings = JsonConvert.SerializeObject(curr_settings);
                        //
                        prj_user_settings.settings_grid = settings_grid;
                        if ((Enums.PrjUserSettingsSaveModeEnum)mode == Enums.PrjUserSettingsSaveModeEnum.REWRITE_CURRENT)
                            prj_user_settings.setting_name = setting_name;
                        prj_user_settings.is_selected = true;
                        prj_user_settings.tag = tag;
                        prj_user_settings.upd_date = now;
                        prj_user_settings.upd_user = user;
                        break;
                    default:
                        break;
                }

                dbContext.SaveChanges();

                return (PrjUserSettingsViewModel)GetItem(prj_user_settings.setting_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public bool ChangeSettings(int? old_setting_id, int? new_setting_id, ModelStateDictionary modelState)
        {
            try
            {
                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;

                if (old_setting_id.GetValueOrDefault(0) == new_setting_id.GetValueOrDefault(0))
                    return true;

                prj_user_settings prj_user_settings_old = null;
                prj_user_settings prj_user_settings_new = null;
                if (old_setting_id.GetValueOrDefault(0) > 0)
                {
                    prj_user_settings_old = dbContext.prj_user_settings.Where(ss => ss.setting_id == old_setting_id).FirstOrDefault();
                    if (prj_user_settings_old != null)
                    {
                        prj_user_settings_old.is_selected = false;
                        prj_user_settings_old.upd_date = now;
                        prj_user_settings_old.upd_user = user;
                    }
                }
                if (new_setting_id.GetValueOrDefault(0) > 0)
                {
                    prj_user_settings_new = dbContext.prj_user_settings.Where(ss => ss.setting_id == new_setting_id).FirstOrDefault();
                    if (prj_user_settings_new != null)
                    {
                        prj_user_settings_new.is_selected = true;
                        prj_user_settings_new.upd_date = now;
                        prj_user_settings_new.upd_user = user;
                    }
                }

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

    }
}
