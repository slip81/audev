﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjBriefDateCutTypeViewModel : AuBaseViewModel
    {

        public PrjBriefDateCutTypeViewModel()
            : base()
        {
            //prj_brief_date_cut_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int date_cut_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string date_cut_type_name { get; set; }
    }
}