﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjDefaultViewModel : AuBaseViewModel
    {

        public PrjDefaultViewModel()
            : base(Enums.MainObjectType.PRJ_DEFAULT)
        {
            //prj_default
        }
        
        // db

        [Key()]
        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Display(Name = "Стартовая страница")]
        public string start_page { get; set; }
    }
}