﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using CabinetMvc.ViewModel.Util;
using System.Net;
using AuDev.Common.Network;
using System.Text.RegularExpressions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjNotifyService : IAuBaseService
    {
        List<PrjNotifyViewModel> GetPrjNotifyList_forSend();
    }

    public class PrjNotifyService : AuBaseService, IPrjNotifyService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_notify
                .Where(ss => ss.notify_id == id)
                .ProjectTo<PrjNotifyViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.prj_notify
                .Where(ss => ss.event_id == parent_id)
                .ProjectTo<PrjNotifyViewModel>();
        }

        public List<PrjNotifyViewModel> GetPrjNotifyList_forSend()
        {
            int userId = currUser.CabUserId;
            DateTime now = DateTime.Now;

            List<PrjNotifyViewModel> res = dbContext.prj_notify
                .Where(ss => ss.target_user_id == userId && ss.need_send && ss.send_date <= now)
                .OrderBy(ss => ss.send_date)
                // !!!
                .Take(3)
                .AsEnumerable()
                .Select(ss => new PrjNotifyViewModel()
                {
                    claim_id = ss.claim_id,
                    event_id = ss.event_id,
                    message = ss.message,
                    need_send = ss.need_send,
                    notify_id = ss.notify_id,
                    send_date = ss.send_date,
                    sent_cnt = ss.sent_cnt,
                    sent_date = ss.sent_date,
                    target_user_id = ss.target_user_id,
                    title = ss.title,
                })
                .ToList();
            
            prj_notify prj_notify = null;
            if (res != null)
            {
                foreach (var resItem in res)
                {
                    prj_notify = dbContext.prj_notify.Where(ss => ss.notify_id == resItem.notify_id).FirstOrDefault();
                    if (prj_notify == null)
                        continue;
                    prj_notify.need_send = false;
                    prj_notify.sent_cnt = prj_notify.sent_cnt + 1;
                    prj_notify.sent_date = now;
                    dbContext.SaveChanges();
                }
            }
            return res;
        }
    }
}