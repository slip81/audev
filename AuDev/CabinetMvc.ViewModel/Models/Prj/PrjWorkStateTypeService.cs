﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjWorkStateTypeService : IAuBaseService
    {
        IQueryable<PrjWorkStateTypeViewModel> GetList_Active();
    }

    public class PrjWorkStateTypeService : AuBaseService, IPrjWorkStateTypeService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_work_state_type
                .Where(ss => ss.state_type_id == id)
                .ProjectTo<PrjWorkStateTypeViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_work_state_type.ProjectTo<PrjWorkStateTypeViewModel>();
        }

        public IQueryable<PrjWorkStateTypeViewModel> GetList_Active()
        {
            return dbContext.prj_work_state_type
                .Where(ss => ss.done_or_canceled == 0)
                .ProjectTo<PrjWorkStateTypeViewModel>();
        }
    }
}
