﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjProjectVersionViewModel : AuBaseViewModel
    {

        public PrjProjectVersionViewModel()
            : base(Enums.MainObjectType.PRJ_PROJECT_VERSION)
        {
            //vw_prj_project_version
        }

        // db
        
        [Key()]
        [Display(Name = "Код")]
        public int version_id { get; set; }

        [Display(Name = "Проект")]
        public int project_id { get; set; }

        [Display(Name = "Тип")]
        public int version_type_id { get; set; }

        [Display(Name = "Версия")]
        public string version_name { get; set; }

        [Display(Name = "Значение")]
        public string version_value { get; set; }

        [Display(Name = "Проект")]
        public string project_name { get; set; }

        [Display(Name = "Название")]
        public string version_type_name { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }

    }
}