﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjWorkDefaultViewModel : AuBaseViewModel
    {

        public PrjWorkDefaultViewModel()
            : base(Enums.MainObjectType.PRJ_WORK_DEFAULT)
        {
            //vw_prj_work_default
        }
                
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Key()]
        [Display(Name = "Тип работы")]
        public int work_type_id { get; set; }

        [Display(Name = "Акцепт")]
        public bool is_accept { get; set; }

        [Display(Name = "Исполнитель")]
        public int exec_user_id { get; set; }
        
        [Display(Name = "is_default")]
        public bool is_default { get; set; }

        [Display(Name = "work_type_name")]
        public string work_type_name { get; set; }

        [Display(Name = "exec_user_name")]
        public string exec_user_name { get; set; }

    }
}