﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjFileViewModel : AuBaseViewModel
    {

        public PrjFileViewModel()
            : base(Enums.MainObjectType.PRJ_FILE)
        {
            //vw_prj_file
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int file_id { get; set; }

        [Display(Name = "Название")]
        public string file_name { get; set; }

        [Display(Name = "Ссылка")]
        public string file_link { get; set; }

        [Display(Name = "Путь")]
        public string file_path { get; set; }

        [Display(Name = "Проект")]
        public Nullable<int> prj_id { get; set; }

        [Display(Name = "Задача")]
        public Nullable<int> claim_id { get; set; }

        [Display(Name = "Задание")]
        public Nullable<int> task_id { get; set; }

        [Display(Name = "Работа")]
        public Nullable<int> work_id { get; set; }

        [Display(Name = "Создан")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменен")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "upd_num")]
        public int upd_num { get; set; }

        [Display(Name = "file_name_url")]
        public string file_name_url { get; set; }

        [Display(Name = "claim_num")]
        public string claim_num { get; set; }

        // additional

        [JsonIgnore()]
        public bool is_new { get; set; }
    }

    public class PrjFileParam
    {
        public PrjFileParam()
        {
            MaxFileSize = "10485760"; // 10 Mb
            AllowedExtensions = new List<string>() { 
            ".jpg", 
            ".jpeg",
            ".png",
            ".txt",
            ".xml",
            ".doc",
            ".docx",
            ".sql",
            ".xls",
            ".xlsx",
            ".7z",
            ".zip",
            ".rar",
            ".pdf",
            ".avi",
            ".mpeg",
            ".mpg",
            ".divx",
            ".mkv",
            ".bmp",
            ".epf",
            ".erf",
        };
        }

        public string MaxFileSize { get; set; }
        public List<string> AllowedExtensions { get; set; }
    }
}