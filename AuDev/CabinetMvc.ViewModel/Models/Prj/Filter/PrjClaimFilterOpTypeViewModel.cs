﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Crm;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimFilterOpTypeViewModel : AuBaseViewModel
    {

        public PrjClaimFilterOpTypeViewModel()
            : base()
        {
            //prj_claim_filter_op_type
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int op_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string op_type_name { get; set; }

        [Display(Name = "Наименование")]
        public string op_type_name_rus { get; set; }

        [Display(Name = "Символ")]
        public string op_type_symbol { get; set; }
    }
}