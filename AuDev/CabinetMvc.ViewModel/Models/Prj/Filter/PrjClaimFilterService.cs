﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimFilterService : IAuBaseService
    {
        PrjClaimFilterViewModel GetItem_Full(int? filter_id);        
        string GetFilterRaw(int? filter_id);
    }

    public class PrjClaimFilterService : AuBaseService, IPrjClaimFilterService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            var res = dbContext.prj_claim_filter
                .Where(ss => ((ss.user_id == parent_id) || (!ss.user_id.HasValue)) && ss.is_active)
                .ProjectTo<PrjClaimFilterViewModel>()
                .OrderBy(ss => ss.filter_name)
                .ToList();
            res.Add(new PrjClaimFilterViewModel() { filter_id = -1, filter_name = "создать фильтр..." });
            return res.AsQueryable();

            /*
            return dbContext.prj_claim_filter
                .Where(ss => ss.user_id == parent_id || !ss.user_id.HasValue)
                .ProjectTo<PrjClaimFilterViewModel>();
            */
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.prj_claim_filter
                .Where(ss => ss.filter_id == id)
                .ProjectTo<PrjClaimFilterViewModel>()
                .FirstOrDefault();
        }

        public PrjClaimFilterViewModel GetItem_Full(int? filter_id)
        {
            PrjClaimFilterViewModel res = null;
            if (filter_id.GetValueOrDefault(0) > 0)
            {
                res = (PrjClaimFilterViewModel)xGetItem((int)filter_id);
                if (res != null)
                {
                    res.filter_items = dbContext.vw_prj_claim_filter_item.Where(ss => ss.filter_id == (int)filter_id)
                        .ProjectTo<PrjClaimFilterItemViewModel>();
                }
            }
            else
            {
                res = new PrjClaimFilterViewModel() { is_active = true, user_id = currUser.CabUserId };
                res.filter_items = dbContext.vw_prj_claim_filter_item.Where(ss => !ss.filter_id.HasValue)
                    .ProjectTo<PrjClaimFilterItemViewModel>();
            }
            return res;
        }

        public string GetFilterRaw(int? filter_id)
        {
            return dbContext.prj_claim_filter.Where(ss => ss.filter_id == filter_id).Select(ss => ss.filter_raw).FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimFilterViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjClaimFilterViewModel itemAdd = (PrjClaimFilterViewModel)item;
            if ((itemAdd == null) || (itemAdd.filter_items_raw == null) || (itemAdd.filter_items_raw.Count() <= 0))
            {
                modelState.AddModelError("", "Не заданы атрибуты фильтра");
                return null;
            }

            if (String.IsNullOrWhiteSpace(itemAdd.filter_name))
            {
                modelState.AddModelError("", "Не задано наименование фильтра");
                return null;
            }

            var query = dbContext.prj_claim_filter.Where(ss => ss.is_active && ss.filter_name.Trim().ToLower().Equals(itemAdd.filter_name.Trim().ToLower()));
            if (itemAdd.user_id.GetValueOrDefault(0) > 0)
                query = query.Where(ss => ss.user_id == itemAdd.user_id);
            prj_claim_filter prj_claim_filter_existing = query.FirstOrDefault();
            if (prj_claim_filter_existing != null)
            {
                modelState.AddModelError("", "Уже есть фильтр с таким названием");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string user = currUser.CabUserName;

            prj_claim_filter prj_claim_filter = new prj_claim_filter();
            prj_claim_filter.filter_name = itemAdd.filter_name;
            prj_claim_filter.user_id = itemAdd.user_id;
            prj_claim_filter.is_active = itemAdd.is_active;
            prj_claim_filter.crt_date = now;
            prj_claim_filter.crt_user = user;
            prj_claim_filter.upd_date = now;
            prj_claim_filter.upd_user = user;

            dbContext.prj_claim_filter.Add(prj_claim_filter);

            List<prj_claim_field> prj_claim_field_list = dbContext.prj_claim_field.ToList();
            List<prj_claim_filter_op_type> prj_claim_filter_op_type_list = dbContext.prj_claim_filter_op_type.ToList();

            bool somethingChanged = false;
            prj_claim_filter_item prj_claim_filter_item = null;
            bool isFixed = true;
            prj_claim_field prj_claim_field = null;
            prj_claim_filter_op_type prj_claim_filter_op_type = null;
            List<string> field_name_value_list = null;
            List<string> field_name_value_splitted = null;
            string field_op_type = null;
            KendoFilterMain filter = null;
            List<KendoFilterItem> kendoFilterItemList = new List<KendoFilterItem>();
            KendoFilterItem kendoFilterItem = null;
            KendoFilterItemValue kendoFilterItemValue = null;
            foreach (var filter_item in itemAdd.filter_items_raw)
            {
                var is_field_in_filter = filter_item.filter_values.Where(ss => ss.field_name == "is_field_in_filter").FirstOrDefault();
                if ((is_field_in_filter == null) || (is_field_in_filter.field_text_value == null) || (Convert.ToBoolean(is_field_in_filter.field_text_value) != true))
                    continue;
                prj_claim_filter_item = new prj_claim_filter_item();
                prj_claim_filter_item.prj_claim_filter = prj_claim_filter;
                prj_claim_filter_item.field_id = Convert.ToInt32(filter_item.field_id);
                prj_claim_filter_item.op_type_id = Convert.ToInt32(filter_item.filter_values.Where(ss => ss.field_name == "op_type_id").Select(ss => ss.field_text_value).FirstOrDefault());

                field_name_value_list = new List<string>();
                var field_name_values = filter_item.filter_values.Where(ss => ss.field_name == "field_name_value").ToList();
                if ((field_name_values != null) && (field_name_values.Count > 0))
                {
                    foreach (var field_name_values_item in field_name_values)
                    {
                        field_name_value_list.Add(field_name_values_item.field_text_value);
                    }
                }
                prj_claim_filter_item.field_name_value = string.Join(";", field_name_value_list);

                //prj_claim_filter_item.field_name_value = filter_item.filter_values.Where(ss => ss.field_name == "field_name_value").Select(ss => ss.field_text_value).FirstOrDefault();

                prj_claim_filter_item.is_value_fixed = Convert.ToBoolean(filter_item.filter_values.Where(ss => ss.field_name == "is_value_fixed").Select(ss => ss.field_text_value).FirstOrDefault());

                prj_claim_filter_item.crt_date = now;
                prj_claim_filter_item.crt_user = user;
                prj_claim_filter_item.upd_date = now;
                prj_claim_filter_item.upd_user = user;

                prj_claim_field = prj_claim_field_list.Where(ss => ss.field_id == prj_claim_filter_item.field_id).FirstOrDefault();
                if (prj_claim_field == null)
                    continue;
                prj_claim_filter_op_type = prj_claim_filter_op_type_list.Where(ss => ss.op_type_id == prj_claim_filter_item.op_type_id).FirstOrDefault();
                if (prj_claim_filter_op_type == null)
                    continue;

                switch ((Enums.PrjClaimFieldTypeEnum)prj_claim_field.field_type_id)
                {
                    case Enums.PrjClaimFieldTypeEnum.REF:
                        if (String.IsNullOrWhiteSpace(prj_claim_filter_item.field_name_value))
                            break;
                        field_name_value_splitted = prj_claim_filter_item.field_name_value.Split(';').ToList();
                        if ((field_name_value_splitted == null) || (field_name_value_splitted.Count <= 0))
                            break;

                        field_op_type = prj_claim_filter_op_type.op_type_name;

                        kendoFilterItem = new KendoFilterItem()
                        {
                            filters = new List<KendoFilterItemValue>(),
                            logic = "or",
                        };
                        foreach (var field_name_value_splitted_item in field_name_value_splitted)
                        {
                            kendoFilterItemValue = new KendoFilterItemValue();
                            kendoFilterItemValue.Field = prj_claim_field.field_name;
                            kendoFilterItemValue.Operator = field_op_type;
                            kendoFilterItemValue.Value = field_name_value_splitted_item;
                            kendoFilterItem.filters.Add(kendoFilterItemValue);
                        }
                        kendoFilterItemList.Add(kendoFilterItem);
                        break;
                    case Enums.PrjClaimFieldTypeEnum.DATE:
                    case Enums.PrjClaimFieldTypeEnum.STRING:                    
                        if (String.IsNullOrWhiteSpace(prj_claim_filter_item.field_name_value))
                            break;
                        field_op_type = prj_claim_filter_op_type.op_type_name;

                        kendoFilterItemList.Add(
                            new KendoFilterItem()
                            {
                                filters = new List<KendoFilterItemValue>() { new KendoFilterItemValue()
                                                        {
                                                            Field = prj_claim_field.field_name,
                                                            Operator = field_op_type,
                                                            Value = prj_claim_filter_item.field_name_value,
                                                        }},
                                logic = "or",
                            });
                        break;
                    case Enums.PrjClaimFieldTypeEnum.NUMERIC:
                        if (String.IsNullOrWhiteSpace(prj_claim_filter_item.field_name_value))
                            break;
                        field_op_type = prj_claim_filter_op_type.op_type_name;

                        kendoFilterItemList.Add(
                            new KendoFilterItem()
                            {
                                filters = new List<KendoFilterItemValue>() { new KendoFilterItemValue()
                                                        {
                                                            Field = prj_claim_field.field_name,
                                                            Operator = field_op_type,
                                                            Value = prj_claim_filter_item.field_name_value.Replace(',','.'),
                                                        }},
                                logic = "or",
                            });
                        break;
                    case Enums.PrjClaimFieldTypeEnum.BOOLEAN:
                        if (String.IsNullOrWhiteSpace(prj_claim_filter_item.field_name_value))
                            break;
                        field_op_type = prj_claim_filter_op_type.op_type_name;

                        kendoFilterItemList.Add(
                            new KendoFilterItem()
                            {
                                filters = new List<KendoFilterItemValue>() { new KendoFilterItemValue()
                                                        {
                                                            Field = prj_claim_field.field_name,
                                                            Operator = field_op_type,
                                                            Value = prj_claim_filter_item.field_name_value == "1" ? "true" : "false",
                                                        }},
                                logic = "or",
                            });
                        break;
                    default:
                        break;
                }

                dbContext.prj_claim_filter_item.Add(prj_claim_filter_item);

                if (!prj_claim_filter_item.is_value_fixed)
                    isFixed = false;

                somethingChanged = true;
            }

            if (!somethingChanged)
            {
                modelState.AddModelError("", "Не задан ни один активный фильтр");
                return null;
            }            

            prj_claim_filter.is_fixed = isFixed;

            if (kendoFilterItemList.Count > 0)
            {
                filter = new KendoFilterMain()
                {
                    logic = "and",
                    filters = kendoFilterItemList,
                };
                prj_claim_filter.filter_raw = JsonConvert.SerializeObject(filter);
            }

            dbContext.SaveChanges();

            return xGetItem(prj_claim_filter.filter_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimFilterViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            PrjClaimFilterViewModel itemEdit = (PrjClaimFilterViewModel)item;
            if ((itemEdit == null) || (itemEdit.filter_items_raw == null) || (itemEdit.filter_items_raw.Count() <= 0))
            {
                modelState.AddModelError("", "Не заданы атрибуты фильтра");
                return null;
            }

            if (String.IsNullOrWhiteSpace(itemEdit.filter_name))
            {
                modelState.AddModelError("", "Не задано наименование фильтра");
                return null;
            }

            var query = dbContext.prj_claim_filter.Where(ss => ss.is_active && ss.filter_id != itemEdit.filter_id && ss.filter_name.Trim().ToLower().Equals(itemEdit.filter_name.Trim().ToLower()));
            if (itemEdit.user_id.GetValueOrDefault(0) > 0)
                query = query.Where(ss => ss.user_id == itemEdit.user_id);
            prj_claim_filter prj_claim_filter_existing = query.FirstOrDefault();
            if (prj_claim_filter_existing != null)
            {
                modelState.AddModelError("", "Уже есть фильтр с таким названием");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string user = currUser.CabUserName;

            prj_claim_filter prj_claim_filter = dbContext.prj_claim_filter.Where(ss => ss.filter_id == itemEdit.filter_id).FirstOrDefault();
            if (prj_claim_filter == null)
            {
                modelState.AddModelError("", "Не найден фильтр с кодом " + itemEdit.filter_id.ToString());
                return null;
            }

            prj_claim_filter.filter_name = itemEdit.filter_name;
            prj_claim_filter.user_id = itemEdit.user_id;
            prj_claim_filter.is_active = itemEdit.is_active;
            prj_claim_filter.upd_date = now;
            prj_claim_filter.upd_user = user;

            dbContext.prj_claim_filter_item.RemoveRange(dbContext.prj_claim_filter_item.Where(ss => ss.filter_id == itemEdit.filter_id));

            List<prj_claim_field> prj_claim_field_list = dbContext.prj_claim_field.ToList();
            List<prj_claim_filter_op_type> prj_claim_filter_op_type_list = dbContext.prj_claim_filter_op_type.ToList();

            bool somethingChanged = false;
            prj_claim_filter_item prj_claim_filter_item = null;
            bool isFixed = true;
            prj_claim_field prj_claim_field = null;
            prj_claim_filter_op_type prj_claim_filter_op_type = null;
            List<string> field_name_value_list = null;
            List<string> field_name_value_splitted = null;
            string field_op_type = null;
            KendoFilterMain filter = null;
            List<KendoFilterItem> kendoFilterItemList = new List<KendoFilterItem>();
            KendoFilterItem kendoFilterItem = null;
            KendoFilterItemValue kendoFilterItemValue = null;
            foreach (var filter_item in itemEdit.filter_items_raw)
            {
                var is_field_in_filter = filter_item.filter_values.Where(ss => ss.field_name == "is_field_in_filter").FirstOrDefault();
                if ((is_field_in_filter == null) || (is_field_in_filter.field_text_value == null) || (Convert.ToBoolean(is_field_in_filter.field_text_value) != true))
                    continue;
                prj_claim_filter_item = new prj_claim_filter_item();
                prj_claim_filter_item.prj_claim_filter = prj_claim_filter;
                prj_claim_filter_item.field_id = Convert.ToInt32(filter_item.field_id);
                prj_claim_filter_item.op_type_id = Convert.ToInt32(filter_item.filter_values.Where(ss => ss.field_name == "op_type_id").Select(ss => ss.field_text_value).FirstOrDefault());

                field_name_value_list = new List<string>();
                var field_name_values = filter_item.filter_values.Where(ss => ss.field_name == "field_name_value").ToList();
                if ((field_name_values != null) && (field_name_values.Count > 0))
                {
                    foreach (var field_name_values_item in field_name_values)
                    {
                        field_name_value_list.Add(field_name_values_item.field_text_value);
                    }
                }
                prj_claim_filter_item.field_name_value = string.Join(";", field_name_value_list);

                //prj_claim_filter_item.field_name_value = filter_item.filter_values.Where(ss => ss.field_name == "field_name_value").Select(ss => ss.field_text_value).FirstOrDefault();

                var is_value_fixed_value = filter_item.filter_values.Where(ss => ss.field_name == "is_value_fixed").Select(ss => ss.field_text_value).FirstOrDefault();
                if (!String.IsNullOrWhiteSpace(is_value_fixed_value))
                {
                    prj_claim_filter_item.is_value_fixed = Convert.ToBoolean(is_value_fixed_value);
                }

                prj_claim_filter_item.crt_date = now;
                prj_claim_filter_item.crt_user = user;
                prj_claim_filter_item.upd_date = now;
                prj_claim_filter_item.upd_user = user;

                prj_claim_field = prj_claim_field_list.Where(ss => ss.field_id == prj_claim_filter_item.field_id).FirstOrDefault();
                if (prj_claim_field == null)
                    continue;
                prj_claim_filter_op_type = prj_claim_filter_op_type_list.Where(ss => ss.op_type_id == prj_claim_filter_item.op_type_id).FirstOrDefault();
                if (prj_claim_filter_op_type == null)
                    continue;

                switch ((Enums.PrjClaimFieldTypeEnum)prj_claim_field.field_type_id)
                {
                    case Enums.PrjClaimFieldTypeEnum.REF:
                        if (String.IsNullOrWhiteSpace(prj_claim_filter_item.field_name_value))
                            break;
                        field_name_value_splitted = prj_claim_filter_item.field_name_value.Split(';').ToList();
                        if ((field_name_value_splitted == null) || (field_name_value_splitted.Count <= 0))
                            break;

                        field_op_type = prj_claim_filter_op_type.op_type_name;

                        kendoFilterItem = new KendoFilterItem() {
                            filters = new List<KendoFilterItemValue>(),
                            logic = "or",
                        };
                        foreach (var field_name_value_splitted_item in field_name_value_splitted)
                        {
                            kendoFilterItemValue = new KendoFilterItemValue();
                            kendoFilterItemValue.Field = prj_claim_field.field_name;
                            kendoFilterItemValue.Operator = field_op_type;
                            kendoFilterItemValue.Value = field_name_value_splitted_item;                                                        
                            kendoFilterItem.filters.Add(kendoFilterItemValue);
                        }
                        kendoFilterItemList.Add(kendoFilterItem);
                        break;
                    case Enums.PrjClaimFieldTypeEnum.DATE:
                    case Enums.PrjClaimFieldTypeEnum.STRING:
                        if (String.IsNullOrWhiteSpace(prj_claim_filter_item.field_name_value))
                            break;
                        field_op_type = prj_claim_filter_op_type.op_type_name;

                        kendoFilterItemList.Add(
                            new KendoFilterItem()
                            {
                                filters = new List<KendoFilterItemValue>() { new KendoFilterItemValue()
                                                        {
                                                            Field = prj_claim_field.field_name,
                                                            Operator = field_op_type,
                                                            Value = prj_claim_filter_item.field_name_value,
                                                        }},
                                logic = "or",
                            });
                        break;
                    case Enums.PrjClaimFieldTypeEnum.NUMERIC:
                        if (String.IsNullOrWhiteSpace(prj_claim_filter_item.field_name_value))
                            break;
                        field_op_type = prj_claim_filter_op_type.op_type_name;

                        kendoFilterItemList.Add(
                            new KendoFilterItem()
                            {
                                filters = new List<KendoFilterItemValue>() { new KendoFilterItemValue()
                                                        {
                                                            Field = prj_claim_field.field_name,
                                                            Operator = field_op_type,
                                                            Value = prj_claim_filter_item.field_name_value.Replace(',','.'),
                                                        }},
                                logic = "or",
                            });
                        break;
                    case Enums.PrjClaimFieldTypeEnum.BOOLEAN:
                        if (String.IsNullOrWhiteSpace(prj_claim_filter_item.field_name_value))
                            break;
                        field_op_type = prj_claim_filter_op_type.op_type_name;

                        kendoFilterItemList.Add(
                            new KendoFilterItem()
                            {
                                filters = new List<KendoFilterItemValue>() { new KendoFilterItemValue()
                                                        {
                                                            Field = prj_claim_field.field_name,
                                                            Operator = field_op_type,
                                                            Value = prj_claim_filter_item.field_name_value == "1" ? "true" : "false",
                                                        }},
                                logic = "or",
                            });
                        break;
                    default:
                        break;
                }

                dbContext.prj_claim_filter_item.Add(prj_claim_filter_item);

                if (!prj_claim_filter_item.is_value_fixed)
                    isFixed = false;

                somethingChanged = true;
            }

            if (!somethingChanged)
            {
                modelState.AddModelError("", "Не задан ни один активный фильтр");
                return null;
            }

            prj_claim_filter.is_fixed = isFixed;

            if (kendoFilterItemList.Count > 0)
            {
                filter = new KendoFilterMain()
                {
                    logic = "and",
                    filters = kendoFilterItemList,
                };
                prj_claim_filter.filter_raw = JsonConvert.SerializeObject(filter);
            }

            dbContext.SaveChanges();

            return xGetItem(prj_claim_filter.filter_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is PrjClaimFilterViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            PrjClaimFilterViewModel itemDel = (PrjClaimFilterViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты фильтра");
                return false;
            }

            prj_claim_filter prj_claim_filter = dbContext.prj_claim_filter.Where(ss => ss.filter_id == itemDel.filter_id).FirstOrDefault();
            if (prj_claim_filter == null)
            {
                modelState.AddModelError("", "Не найден фильтр с кодом " + itemDel.filter_id.ToString());
                return false;
            }

            dbContext.prj_claim_filter_item.RemoveRange(dbContext.prj_claim_filter_item.Where(ss => ss.filter_id == prj_claim_filter.filter_id));
            dbContext.prj_claim_filter.Remove(prj_claim_filter);
            dbContext.SaveChanges();

            return true;
        }
        
    }
}