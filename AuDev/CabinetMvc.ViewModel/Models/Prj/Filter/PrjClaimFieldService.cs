﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimFieldService : IAuBaseService
    {
        //
    }

    public class PrjClaimFieldService : AuBaseService, IPrjClaimFieldService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_claim_field.ProjectTo<PrjClaimFieldViewModel>();
        }
    }
}