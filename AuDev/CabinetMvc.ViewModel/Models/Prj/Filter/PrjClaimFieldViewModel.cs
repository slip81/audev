﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Crm;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimFieldViewModel : AuBaseViewModel
    {

        public PrjClaimFieldViewModel()
            : base()
        {
            //prj_claim_field
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int field_id { get; set; }

        [Display(Name = "Наименование")]
        public string field_name { get; set; }

        [Display(Name = "Наименование")]
        public string field_name_rus { get; set; }

        [Display(Name = "Тип")]
        public int field_type_id { get; set; }

        [Display(Name = "Порядок")]
        public int ord { get; set; }

        [Display(Name = "Кэш")]
        public Nullable<int> cache_id { get; set; }
    }
}