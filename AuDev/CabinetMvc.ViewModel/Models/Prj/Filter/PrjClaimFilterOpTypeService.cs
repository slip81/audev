﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimFilterOpTypeService : IAuBaseService
    {
        //
    }

    public class PrjClaimFilterOpTypeService : AuBaseService, IPrjClaimFilterOpTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_claim_filter_op_type.ProjectTo<PrjClaimFilterOpTypeViewModel>();
        }
    }
}