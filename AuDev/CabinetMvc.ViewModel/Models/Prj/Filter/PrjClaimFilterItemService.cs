﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjClaimFilterItemService : IAuBaseService
    {
        IQueryable<PrjClaimFilterItemViewModel> GetList_byFilter(int? filter_id);
    }

    public class PrjClaimFilterItemService : AuBaseService, IPrjClaimFilterItemService
    {
        public IQueryable<PrjClaimFilterItemViewModel> GetList_byFilter(int? filter_id)
        {
            return dbContext.vw_prj_claim_filter_item
                .Where(ss => ss.filter_id == filter_id)
                .ProjectTo<PrjClaimFilterItemViewModel>();
        }

    }
}