﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Crm;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimFilterItemViewModel : AuBaseViewModel
    {

        public PrjClaimFilterItemViewModel()
            : base()
        {
            //vw_prj_claim_filter_item
        }
                
        [Key()]
        [Display(Name = "Код поля")]
        public int field_id { get; set; }

        [Display(Name = "Наименование поля")]
        public string field_name { get; set; }

        [Display(Name = "Наименование поля")]
        public string field_name_rus { get; set; }

        [Display(Name = "Тип поля")]
        public int field_type_id { get; set; }

        [Display(Name = "Порядок поля")]
        public int ord { get; set; }

        [Display(Name = "Кэш")]
        public Nullable<int> cache_id { get; set; }

        [Display(Name = "Тип поля")]
        public string field_type_name { get; set; }

        [Display(Name = "Тип поля")]
        public string field_type_name_rus { get; set; }

        [Display(Name = "Поле в фильтре")]
        public bool is_field_in_filter { get; set; }

        [Display(Name = "Код фильтра")]
        public Nullable<int> filter_id { get; set; }

        [Display(Name = "Тип операции")]
        public Nullable<int> op_type_id { get; set; }

        [Display(Name = "Значение поля (код)")]
        public string field_id_value { get; set; }

        [Display(Name = "Значение поля (текс)")]
        public string field_name_value { get; set; }

        [Display(Name = "Значение фиксировано")]
        public Nullable<bool> is_value_fixed { get; set; }

        [Display(Name = "Наименование фильтра")]
        public string filter_name { get; set; }

        [Display(Name = "Владелец фильтра")]
        public Nullable<int> user_id { get; set; }

        [Display(Name = "Фильтр активен")]
        public Nullable<bool> is_active { get; set; }

        [Display(Name = "Владелец фильтра")]
        public string user_name { get; set; }

        [Display(Name = "Тип операции")]
        public string op_type_name { get; set; }

        [Display(Name = "Тип операции")]
        public string op_type_name_rus { get; set; }

        [Display(Name = "Тип операции")]
        public string op_type_symbol { get; set; }

        [Display(Name = "Кэш")]
        public string cache_name { get; set; }

        [Display(Name = "Кэш (поле со значением)")]
        public string cache_value_field { get; set; }

        [Display(Name = "Кэш (поле с текстом)")]
        public string cache_text_field { get; set; }
    }
}