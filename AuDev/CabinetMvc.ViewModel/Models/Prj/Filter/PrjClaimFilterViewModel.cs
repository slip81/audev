﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Crm;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimFilterViewModel : AuBaseViewModel
    {

        public PrjClaimFilterViewModel()
            : base(Enums.MainObjectType.PRJ_CLAIM_FILTER)
        {
            //prj_claim_filter
        }

        [Key()]
        [Display(Name = "Код")]
        public int filter_id { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Не задано наименование")]
        public string filter_name { get; set; }

        [Display(Name = "Владелец")]
        public Nullable<int> user_id { get; set; }

        [Display(Name = "Активен")]
        public bool is_active { get; set; }

        [Display(Name = "Когда создан")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Кем создан")]
        public string crt_user { get; set; }

        [Display(Name = "Когда изменен")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Кем изменен")]
        public string upd_user { get; set; }

        [Display(Name = "Фиксировано")]
        public bool is_fixed { get; set; }

        //public string filter_raw { get; set; }

        // addtitional
        [JsonIgnore()]
        public IEnumerable<PrjClaimFilterItemViewModel> filter_items { get; set; }
        
        [JsonIgnore()]
        public IEnumerable<PrjClaimFilterItem> filter_items_raw { get; set; }
    }

    public class PrjClaimFilterItem
    {
        public PrjClaimFilterItem()
        {
            ///
        }

        public string field_id { get; set; }
        public List<PrjClaimFilterValue> filter_values { get; set; }
    }

    public class PrjClaimFilterValue
    {
        public PrjClaimFilterValue()
        {
            ///
        }
        
        public string field_name { get; set; }
        public string field_id_value { get; set; }
        public string field_text_value { get; set; }
    }
}
