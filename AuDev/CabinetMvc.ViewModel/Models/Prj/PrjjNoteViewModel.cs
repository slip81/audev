﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjNoteViewModel : AuBaseViewModel
    {

        public PrjNoteViewModel()
            : base(Enums.MainObjectType.PRJ_NOTE)
        {
            //vw_prj_note
        }       

        [Key()]
        [Display(Name = "Код")]
        public int note_id { get; set; }

        [Display(Name = "Заголовок")]
        public string note_name { get; set; }

        [Display(Name = "Текст")]
        public string note_text { get; set; }

        [Display(Name = "№")]
        public string note_num { get; set; }
    }
}