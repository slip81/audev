﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjUserGroupItemTypeService : IAuBaseService
    {
        //
    }

    public class PrjUserGroupItemTypeService : AuBaseService, IPrjUserGroupItemTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_user_group_item_type.ProjectTo<PrjUserGroupItemTypeViewModel>();
        }
    }
}
