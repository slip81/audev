﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjNotifyViewModel : AuBaseViewModel
    {

        public PrjNotifyViewModel()
            : base(Enums.MainObjectType.PRJ_NOTIFY)
        {
            //prj_notify
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int notify_id { get; set; }

        [Display(Name = "Получатель")]
        public Nullable<int> target_user_id { get; set; }

        [Display(Name = "Событие")]
        public Nullable<int> event_id { get; set; }

        [Display(Name = "Задача")]
        public Nullable<int> claim_id { get; set; }

        [Display(Name = "Заголовок")]
        public string title { get; set; }

        [Display(Name = "Сообщение")]
        public string message { get; set; }

        [Display(Name = "Нужно отправлять")]
        public bool need_send { get; set; }

        [Display(Name = "Когда отправлять")]
        public Nullable<System.DateTime> send_date { get; set; }

        [Display(Name = "Сколько раз отправлено")]
        public int sent_cnt { get; set; }

        [Display(Name = "Когда отправлено")]
        public Nullable<System.DateTime> sent_date { get; set; }

        [Display(Name = "Когда создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Кем создано")]
        public string crt_user { get; set; }
    }
}