﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using CabinetMvc.ViewModel.Util;
using System.Net;
using AuDev.Common.Network;
using System.Text.RegularExpressions;

namespace CabinetMvc.ViewModel.Prj
{
    public interface IPrjFileService : IAuBaseService
    {
        PrjFileParam GetPrjFileParam();
        bool UploadFile(HttpPostedFileBase file, int claim_id, ModelStateDictionary modelState);
        bool RemoveFile(string fileNames, int claim_id, ModelStateDictionary modelState);
    }

    public class PrjFileService : AuBaseService, IPrjFileService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_prj_file.Where(ss => ss.file_id == id).ProjectTo<PrjFileViewModel>().FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_prj_file.ProjectTo<PrjFileViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_prj_file
                .Where(ss => ss.claim_id == parent_id)
                .ProjectTo<PrjFileViewModel>();
        }

        public PrjFileParam GetPrjFileParam()
        {
            return new PrjFileParam();
        }

        public bool UploadFile(HttpPostedFileBase file, int claim_id, ModelStateDictionary modelState)
        {
            try
            {
                PrjFileParam prjFileParam = GetPrjFileParam();

                if (file.ContentLength > int.Parse(prjFileParam.MaxFileSize))
                {
                    modelState.AddModelError("", "Файл превышает максимально допустимый размер (5 Мб)");
                    return false;
                }

                string fileExt = Path.GetExtension(file.FileName);
                if (!prjFileParam.AllowedExtensions.Contains(fileExt.ToLower().Trim()))
                {
                    modelState.AddModelError("", "Недопустимый тип файла (" + fileExt + ")");
                    return false;
                }

                DateTime now = DateTime.Now;
                string userName = currUser.CabUserName;
                
                string filePath_orig = ViewModelUtil.GetCrmFileStoragePath();
                string filePath = Path.Combine(filePath_orig, claim_id.ToString());
                string fileName = Path.GetFileName(file.FileName);
                
                var res = GlobalUtil.UploadFile(file, filePath_orig, filePath, fileName);
                if (!res.Item1)
                {
                    modelState.AddModelError("", "Ошибка при загрузхе файла");
                    return false;
                }
                
                prj_file prj_file = new prj_file();
                prj_file.claim_id = claim_id;
                prj_file.crt_date = now;
                prj_file.crt_user = userName;
                //prj_file.file_link = ViewModelUtil.GetCrmFileStorageLink() + claim_id.ToString() + "/" + fileName;                
                prj_file.file_name = fileName;
                prj_file.file_path = filePath;
                prj_file.upd_num = 1;

                dbContext.prj_file.Add(prj_file);
                dbContext.SaveChanges();
                
                // !!!
                prj_file.file_link = ViewModelUtil.GetCrmFileStorageLink() + prj_file.file_id.ToString();
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        public bool RemoveFile(string fileNames, int claim_id, ModelStateDictionary modelState)
        {
            try
            {
                if (String.IsNullOrEmpty(fileNames))
                {
                    return true;
                }

                prj_claim prj_claim = dbContext.prj_claim.Where(ss => ss.claim_id == claim_id).FirstOrDefault();
                if (prj_claim == null)
                {
                    return true;
                }

                var aContents = Regex.Matches(fileNames.Trim().ToLower(), @"<a [^>]*>(.*?)</a>").Cast<Match>().Select(m => m.Groups[1].Value).FirstOrDefault();                
                if (String.IsNullOrWhiteSpace(aContents))
                {
                    return true;
                }

                prj_file prj_file = dbContext.prj_file.Where(ss => ss.claim_id == claim_id && ss.file_name.Trim().ToLower().Equals(aContents)).FirstOrDefault();
                if (prj_file == null)
                {
                    return true;
                }

                // !!!                
                var res = GlobalUtil.DeleteFile(ViewModelUtil.GetCrmFileStoragePath(), Path.Combine(prj_file.file_path, prj_file.file_name));
                if (!res)
                {
                    //
                }

                dbContext.prj_file.Remove(prj_file);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }        
    }
}
