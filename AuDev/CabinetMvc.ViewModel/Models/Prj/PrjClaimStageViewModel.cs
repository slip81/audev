﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimStageViewModel : AuBaseViewModel
    {

        public PrjClaimStageViewModel()
            : base()
        {
            //prj_claim_stage
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int claim_stage_id { get; set; }

        [Display(Name = "Наименование")]
        public string claim_stage_name { get; set; }
    }
}