﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Prj
{
    public class PrjClaimViewModel : AuBaseViewModel
    {

        public PrjClaimViewModel()
            : base(Enums.MainObjectType.PRJ_CLAIM)
        {
            //vw_prj_claim
        }
        
        // db

        [Key()]
        [Display(Name = "Код")]
        public int claim_id { get; set; }

        [Display(Name = "Номер")]
        public string claim_num { get; set; }

        [Display(Name = "Название")]
        public string claim_name { get; set; }

        [Display(Name = "Формулировка")]
        public string claim_text { get; set; }

        [Display(Name = "Проект")]
        public int project_id { get; set; }

        [Display(Name = "Модуль")]
        public int module_id { get; set; }

        [Display(Name = "Раздел модуля")]
        public int module_part_id { get; set; }

        [Display(Name = "Версия проекта")]
        public int project_version_id { get; set; }

        [Display(Name = "Источник")]
        public int client_id { get; set; }

        [Display(Name = "Приоритет")]
        public int priority_id { get; set; }

        [Display(Name = "Ответственный")]
        public int resp_user_id { get; set; }

        [Display(Name = "Плановая дата")]
        public Nullable<System.DateTime> date_plan { get; set; }

        [Display(Name = "Фактическая дата")]
        public Nullable<System.DateTime> date_fact { get; set; }

        [Display(Name = "Версия исправления")]
        public Nullable<int> project_repair_version_id { get; set; }

        [Display(Name = "В архиве")]
        public bool is_archive { get; set; }

        [Display(Name = "Создана")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменена")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "Номер изменения")]
        public int upd_num { get; set; }

        [Display(Name = "Контроль дат")]
        public bool is_control { get; set; }

        [Display(Name = "old_task_id")]
        public Nullable<int> old_task_id { get; set; }

        [Display(Name = "Проект")]
        public string project_name { get; set; }

        [Display(Name = "Модуль")]
        public string module_name { get; set; }

        [Display(Name = "Раздел модуля")]
        public string module_part_name { get; set; }

        [Display(Name = "Версия проекта")]
        public string project_version_name { get; set; }

        [Display(Name = "Источник")]
        public string client_name { get; set; }

        [Display(Name = "Приоритет")]
        public string priority_name { get; set; }
        
        [Display(Name = "Ответственный")]
        public string resp_user_name { get; set; }

        [Display(Name = "Версия исправления")]
        public string project_repair_version_name { get; set; }

        [Display(Name = "Заданий всего")]
        public Nullable<int> task_cnt { get; set; }

        [Display(Name = "Неразобрано")]
        public Nullable<int> new_task_cnt { get; set; }

        [Display(Name = "Активно")]
        public Nullable<int> active_task_cnt { get; set; }

        [Display(Name = "Выполнено")]
        public Nullable<int> done_task_cnt { get; set; }

        [Display(Name = "Отменено")]
        public Nullable<int> canceled_task_cnt { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> claim_state_type_id { get; set; }

        [Display(Name = "Статус")]
        public string claim_state_type_name { get; set; }

        [Display(Name = "Статус")]
        [JsonIgnore()]
        public string claim_state_type_templ { get; set; }

        [Display(Name = "Группа [old]")]
        public Nullable<int> old_group_id { get; set; }

        [Display(Name = "Группа [old]")]
        public string old_group_name { get; set; }

        [Display(Name = "Статус [old]")]
        public Nullable<int> old_state_id { get; set; }

        [Display(Name = "Статус [old]")]
        public string old_state_name { get; set; }

        [Display(Name = "Стадия")]
        public Nullable<int> active_work_type_id { get; set; }

        [Display(Name = "Стадия")]
        public string active_work_type_name { get; set; }

        [Display(Name = "Просрочено")]
        public bool is_overdue { get; set; }

        [Display(Name = "И")]
        public bool is_favor { get; set; }

        [Display(Name = "Статус сводки")]
        public Nullable<int> brief_state_type_id { get; set; }

        [Display(Name = "Статус сводки")]
        public string brief_state_type_name { get; set; }

        [Display(Name = "Исполнители")]
        public string exec_user_name_list { get; set; }

        [Display(Name = "Тип")]
        public int claim_type_id { get; set; }

        [Display(Name = "Этап")]
        public int claim_stage_id { get; set; }

        [Display(Name = "Тип")]
        public string claim_type_name { get; set; }

        [Display(Name = "Этап")]
        public string claim_stage_name { get; set; }

        [Display(Name = "Комментарии")]
        public Nullable<int> mess_cnt { get; set; }

        [Display(Name = "priority_rate")]
        public Nullable<decimal> priority_rate { get; set; }

        [Display(Name = "claim_type_rate")]
        public Nullable<decimal> claim_type_rate { get; set; }

        [Display(Name = "Рейтинг")]
        public Nullable<decimal> summary_rate { get; set; }

        [Display(Name = "Активно")]
        public bool is_active_state { get; set; }

        [Display(Name = "Группы")]
        public string prj_id_list { get; set; }

        [Display(Name = "Группы")]
        public string prj_name_list { get; set; }

        [Display(Name = "Версия")]
        public int version_id { get; set; }

        [Display(Name = "is_version_fixed")]
        public bool is_version_fixed { get; set; }

        [Display(Name = "Версия")]
        public string version_name { get; set; }

        [Display(Name = "Номер версии")]
        public string version_value { get; set; }

        [Display(Name = "Версия (полная)")]
        public string version_value_full { get; set; }

        [Display(Name = "Тип версии")]
        public int version_type_id { get; set; }

        [Display(Name = "Тип версии")]
        public string version_type_name { get; set; }

        [Display(Name = "Когда удалена")]
        public Nullable<System.DateTime> arc_date { get; set; }

        [Display(Name = "Кем удалена")]
        public string arc_user { get; set; }

        // additional

        [JsonIgnore()]
        public bool is_new { get; set; }

        [JsonIgnore()]
        public int? create_claim_from_task { get; set; }

        [JsonIgnore()]
        public string create_claim_name { get; set; }

        [JsonIgnore()]
        public string create_claim_text { get; set; }

        [JsonIgnore()]
        public IEnumerable<PrjMessViewModel> claim_mess_list { get; set; }

        [JsonIgnore()]
        public IEnumerable<PrjMessViewModel> claim_task_mess_list { get; set; }

        [JsonIgnore()]
        public IEnumerable<PrjTaskViewModel> claim_task_list { get; set; }

        [JsonIgnore()]
        public IEnumerable<PrjWorkViewModel> claim_work_list { get; set; }

        [JsonIgnore()]
        public IEnumerable<PrjFileViewModel> claim_file_list { get; set; }

        [JsonIgnore()]
        [Display(Name = "Решение")]        
        public string claim_text_solution { get; set; }

        [JsonIgnore()]
        [Display(Name = "Воспроизведение")]
        public string claim_text_reprod { get; set; }

        [JsonIgnore()]
        [Display(Name = "Клиент")]
        public string claim_text_client { get; set; }

        [Display(Name = "Группы")]
        public List<string> prj_id_list_AsList { get; set; }

    }
}