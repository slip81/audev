﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Auth
{
    public interface IAuthUserPermService : IAuBaseService
    {
        IQueryable<AuthUserPermViewModel> GetSectionList_byUser(int user_id, int role_id, int group_id);
        IQueryable<AuthUserPermViewModel> GetPartList_byUserAndSection(int user_id, int section_id, int role_id, int group_id);
        bool ApplyChanges(List<CabinetMvc.ViewModel.Auth.AuthUserPermViewModel>[] permList, int role_id, int user_id, ModelStateDictionary modelState);
    }

    public class AuthUserPermService : AuBaseService, IAuthUserPermService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_auth_user_perm
                .ProjectTo<AuthUserPermViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_auth_user_perm
                .Where(ss => ss.user_id == parent_id)
                .ProjectTo<AuthUserPermViewModel>();
        }




        public IQueryable<AuthUserPermViewModel> GetSectionList_byUser(int user_id, int role_id, int group_id)
        {
            if (role_id > 0)
            {
                return dbContext.vw_auth_role_section_perm
                    .Where(ss => ss.role_id == role_id && ss.role_group_id == group_id)
                    .Select(ss => new AuthUserPermViewModel()
                    {
                        section_id = ss.section_id,
                        section_name = ss.section_name,
                        user_id = user_id,
                        allow_edit = ss.allow_edit_section == 1,
                        allow_view = ss.allow_view_section == 1,
                    })
                    .Distinct();
            }
            else
            {
                return dbContext.vw_auth_user_section_perm
                    .Where(ss => ss.user_id == user_id && ss.role_group_id == group_id)
                    .Select(ss => new AuthUserPermViewModel()
                    {
                        section_id = ss.section_id,
                        section_name = ss.section_name,
                        user_id = user_id,
                        allow_edit = ss.allow_edit_section == 1,
                        allow_view = ss.allow_view_section == 1,
                    })
                    .Distinct();
            }

        }

        public IQueryable<AuthUserPermViewModel> GetPartList_byUserAndSection(int user_id, int section_id, int role_id, int group_id)
        {
            if (role_id > 0)
            {
                return dbContext.vw_auth_role_perm
                    .Where(ss => ss.role_id == role_id && ss.section_id == section_id && ss.part_id.HasValue && ss.role_group_id == group_id)
                    .Select(ss => new AuthUserPermViewModel()
                    {
                        section_id = ss.section_id,
                        section_name = ss.section_name,
                        user_id = user_id,
                        allow_edit = ss.allow_edit,
                        allow_view = ss.allow_view,
                        id = ss.id,
                        part_id = ss.part_id,
                        part_name = ss.part_name,
                        role_id = ss.role_id,
                        role_name = ss.role_name,
                        role_perm = true,
                        user_perm = false,                       
                    });
            }
            else
            {
                return dbContext.vw_auth_user_perm
                    .Where(ss => ss.user_id == user_id && ss.section_id == section_id && ss.part_id.HasValue && ss.role_group_id == group_id)
                    .ProjectTo<AuthUserPermViewModel>();
            }
        }

        public bool ApplyChanges(List<CabinetMvc.ViewModel.Auth.AuthUserPermViewModel>[] permList,
            int role_id,
            int user_id,
            ModelStateDictionary modelState)
        {         
            List<auth_user_perm> existing_perm_list = null;

            var existing_user_role = dbContext.auth_user_role.Where(ss => ss.user_id == user_id).FirstOrDefault();
            if (existing_user_role == null)
            {
                existing_user_role = new auth_user_role();
                existing_user_role.role_id = role_id;
                existing_user_role.user_id = (int)user_id;
                dbContext.auth_user_role.Add(existing_user_role);
            }
            else
            {
                if (existing_user_role.role_id != role_id)
                {
                    existing_user_role.role_id = role_id;
                    dbContext.auth_user_perm.RemoveRange(dbContext.auth_user_perm.Where(ss => ss.user_id == (int)user_id));
                    dbContext.SaveChanges();
                }
            }

            existing_perm_list = dbContext.auth_user_perm.Where(ss => ss.user_id == user_id).ToList();
            if (existing_perm_list == null)
                existing_perm_list = new List<auth_user_perm>();

            if (permList != null)
            {
                foreach (var permSectionList in permList)
                {
                    foreach (var perm in permSectionList)
                    {
                        var existing_perm = existing_perm_list.Where(ss => ss.part_id == perm.part_id).FirstOrDefault();
                        if (existing_perm != null)
                        {
                            existing_perm.allow_edit = perm.allow_edit;
                            existing_perm.allow_view = perm.allow_view;
                        }
                        else
                        {
                            existing_perm = new auth_user_perm();
                            existing_perm.allow_edit = perm.allow_edit;
                            existing_perm.allow_view = perm.allow_view;
                            existing_perm.part_id = (int)perm.part_id;
                            existing_perm.user_id = (int)user_id;
                            dbContext.auth_user_perm.Add(existing_perm);
                        }
                    }
                }
            }

            // !!!
            // todo:

            /*
            var auth_user_perm_for_del_list = dbContext.vw_auth_user_perm.Where(ss => ss.user_id == user_id && ss.role_id != role_id).ToList();
            if (auth_user_perm_for_del_list != null)
            {
                
            }
            */


            dbContext.SaveChanges();

            return true;
        }

    }
}
