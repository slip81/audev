﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Auth
{
    public class AuthGroupViewModel : AuBaseViewModel
    {

        public AuthGroupViewModel()
            : base(Enums.MainObjectType.AUTH_GROUP)
        {            
            //auth_group
        }

        [Key()]
        [Display(Name = "Код")]
        public int group_id { get; set; }

        [Display(Name = "Наименование")]
        public string group_name { get; set; }
    }
}

