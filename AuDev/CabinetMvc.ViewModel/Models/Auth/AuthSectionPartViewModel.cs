﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Auth
{
    public class AuthSectionPartViewModel : AuBaseViewModel
    {

        public AuthSectionPartViewModel()
            : base(Enums.MainObjectType.AUTH_SECTION_PART)
        {            
            //auth_section_part
        }

        [Key()]
        [Display(Name = "Код")]
        public int part_id { get; set; }

        [Display(Name = "Название")]
        public string part_name { get; set; }

        [Display(Name = "Секция")]
        public int section_id { get; set; }

        [Display(Name = "Группа")]
        public Nullable<int> group_id { get; set; }
    }
}

