﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Auth
{
    public interface IAuthUserRoleService : IAuBaseService
    {
        //
    }

    public class AuthUserRoleService : AuBaseService, IAuthUserRoleService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_auth_user_role
                .ProjectTo<AuthUserRoleViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_auth_user_role
                .Where(ss => ss.group_id == parent_id)
                .ProjectTo<AuthUserRoleViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_auth_user_role
                .Where(ss => ss.user_id == id)
                .ProjectTo<AuthUserRoleViewModel>()
                .FirstOrDefault();
        }
    }
}
