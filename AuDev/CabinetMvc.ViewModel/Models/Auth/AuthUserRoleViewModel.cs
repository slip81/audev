﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Auth
{
    public class AuthUserRoleViewModel : AuBaseViewModel
    {

        public AuthUserRoleViewModel()
            : base(Enums.MainObjectType.AUTH_USER_ROLE)
        {            
            //vw_auth_user_role
        }

        [Key()]
        [Display(Name = "Код")]
        public int user_id { get; set; }

        [Display(Name = "Имя")]
        public string user_name { get; set; }

        [Display(Name = "Полное имя")]
        public string user_name_full { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Роль")]
        public Nullable<int> role_id { get; set; }

        [Display(Name = "Роль")]
        public string role_name { get; set; }

        [Display(Name = "Группа")]
        public Nullable<int> group_id { get; set; }

        [Display(Name = "Логин")]
        public string user_login { get; set; }

        [Display(Name = "Суперадмин")]
        public Nullable<bool> is_superadmin { get; set; }

        [Display(Name = "Полное имя")]
        public string full_name { get; set; }

        [Display(Name = "Электронная почта")]
        public string email { get; set; }

        [Display(Name = "В задачах")]
        public Nullable<bool> is_crm_user { get; set; }

        [Display(Name = "В календаре")]
        public Nullable<bool> is_executer { get; set; }

        [Display(Name = "Роль в задачах")]
        public Nullable<int> crm_user_role_id { get; set; }

        [Display(Name = "Номер ICQ")]
        public string icq { get; set; }

        [Display(Name = "Активен")]
        public Nullable<bool> is_active { get; set; }

        [Display(Name = "Роль в задачах")]
        public string crm_user_role_name { get; set; }

        [Display(Name = "Уведомлять о новых задачах")]
        public Nullable<bool> is_notify_prj { get; set; }
    }
}

