﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Auth
{
    public interface IAuthGroupService : IAuBaseService
    {
        //
    }

    public class AuthGroupService : AuBaseService, IAuthGroupService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.auth_group
                .ProjectTo<AuthGroupViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.auth_group
                .Where(ss => ss.group_id == id)
                .ProjectTo<AuthGroupViewModel>()
                .FirstOrDefault();
        }
    }
}
