﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Auth
{
    public interface IAuthSectionPartService : IAuBaseService
    {
        //
    }

    public class AuthSectionPartService : AuBaseService, IAuthSectionPartService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.auth_section_part
                .ProjectTo<AuthSectionPartViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.auth_section_part
                .Where(ss => ss.part_id == id)
                .ProjectTo<AuthSectionPartViewModel>()
                .FirstOrDefault();
        }
    }
}
