﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Auth
{
    public interface IAuthSectionService : IAuBaseService
    {
        //
    }

    public class AuthSectionService : AuBaseService, IAuthSectionService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.auth_section
                .ProjectTo<AuthSectionViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.auth_section
                .Where(ss => ss.section_id == id)
                .ProjectTo<AuthSectionViewModel>()
                .FirstOrDefault();
        }
    }
}
