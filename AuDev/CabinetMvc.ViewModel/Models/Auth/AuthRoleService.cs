﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Auth
{
    public interface IAuthRoleService : IAuBaseService
    {        
        IQueryable<AuthRoleViewModel> GetList_byGroup(int group_id);
        AuthRoleViewModel GetItem_byName(string role_name);
    }

    public class AuthRoleService : AuBaseService, IAuthRoleService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_auth_role
                .ProjectTo<AuthRoleViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.auth_role
                .Where(ss => ss.role_id == id)
                .ProjectTo<AuthRoleViewModel>()
                .FirstOrDefault();
        }

        public AuthRoleViewModel GetItem_byName(string role_name)
        {
            return dbContext.auth_role
                .Where(ss => ss.role_name.Trim().ToLower().Equals(role_name.Trim().ToLower()))
                .ProjectTo<AuthRoleViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            bool isGroup1 = parent_id == 1;
            bool isGroup2 = parent_id == 2;
            return dbContext.vw_auth_role
                .Where(ss => ((ss.is_group1 == true) && (parent_id == 1)) || ((ss.is_group2 == true) && (parent_id == 2)))
                .ProjectTo<AuthRoleViewModel>();
        }

        public IQueryable<AuthRoleViewModel> GetList_byGroup(int group_id)
        {
            return (from t1 in dbContext.auth_role
                   from t2 in dbContext.auth_role_group
                   where t1.role_id == t2.role_id
                   && t2.group_id == group_id
                   select t1)
                   .ProjectTo<AuthRoleViewModel>();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is AuthRoleViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            AuthRoleViewModel itemAdd = (AuthRoleViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты роли");
                return null;
            }

            var existing_role = dbContext.auth_role.Where(ss => ss.role_name.Trim().ToLower().Equals(itemAdd.role_name.Trim().ToLower())).FirstOrDefault();
            if (existing_role != null)
            {
                modelState.AddModelError("", "Уже есть роль " + itemAdd.role_name.Trim());
                return null;
            }

            int next_role_id = dbContext.auth_role.Max(ss => ss.role_id);
            next_role_id = next_role_id + 1;

            auth_role auth_role = new auth_role();
            auth_role.role_id = next_role_id;
            auth_role.role_name = itemAdd.role_name;
            dbContext.auth_role.Add(auth_role);

            if (itemAdd.is_group1)
            {
                auth_role_group auth_role_group1 = new auth_role_group();
                auth_role_group1.group_id = 1;
                auth_role_group1.role_id = next_role_id;
                dbContext.auth_role_group.Add(auth_role_group1);                
            }
            if (itemAdd.is_group2)
            {
                auth_role_group auth_role_group2 = new auth_role_group();
                auth_role_group2.group_id = 2;
                auth_role_group2.role_id = next_role_id;
                dbContext.auth_role_group.Add(auth_role_group2);  
            }
            
            dbContext.SaveChanges();

            AuthRoleViewModel res = dbContext.vw_auth_role
                .Where(ss => ss.role_name.Trim().ToLower().Equals(itemAdd.role_name.Trim().ToLower()))
                .ProjectTo<AuthRoleViewModel>()
                .FirstOrDefault();            
            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is AuthRoleViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            AuthRoleViewModel itemEdit = (AuthRoleViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты роли");
                return null;
            }

            var existing_role = dbContext.auth_role.Where(ss => ss.role_id != itemEdit.role_id && ss.role_name.Trim().ToLower().Equals(itemEdit.role_name.Trim().ToLower())).FirstOrDefault();
            if (existing_role != null)
            {
                modelState.AddModelError("", "Уже есть роль " + itemEdit.role_name.Trim());
                return null;
            }

            existing_role = dbContext.auth_role.Where(ss => ss.role_id == itemEdit.role_id).FirstOrDefault();
            if (existing_role == null)
            {
                modelState.AddModelError("", "Не найдена роль " + itemEdit.role_id.ToString());
                return null;
            }

            existing_role.role_name = itemEdit.role_name;

            var role_group1 = dbContext.auth_role_group.Where(ss => ss.role_id == itemEdit.role_id && ss.group_id == 1).FirstOrDefault();
            var role_group2 = dbContext.auth_role_group.Where(ss => ss.role_id == itemEdit.role_id && ss.group_id == 2).FirstOrDefault();
            
            int next_role_id = dbContext.auth_role.Max(ss => ss.role_id);
            next_role_id = next_role_id + 1;
            
            if ((itemEdit.is_group1) && (role_group1 == null))
            {
                auth_role_group auth_role_group1 = new auth_role_group();
                auth_role_group1.group_id = 1;
                auth_role_group1.role_id = next_role_id;
                dbContext.auth_role_group.Add(auth_role_group1);                
            }

            if ((!itemEdit.is_group1) && (role_group1 != null))
            {
                dbContext.auth_role_group.Remove(role_group1);
            }

            if ((itemEdit.is_group2) && (role_group2 == null))
            {
                auth_role_group auth_role_group2 = new auth_role_group();
                auth_role_group2.group_id = 2;
                auth_role_group2.role_id = next_role_id;
                dbContext.auth_role_group.Add(auth_role_group2);
            }

            if ((!itemEdit.is_group2) && (role_group2 != null))
            {
                dbContext.auth_role_group.Remove(role_group2);
            }

            dbContext.SaveChanges();

            AuthRoleViewModel res = dbContext.vw_auth_role
                .Where(ss => ss.role_name.Trim().ToLower().Equals(itemEdit.role_name.Trim().ToLower()))
                .ProjectTo<AuthRoleViewModel>()
                .FirstOrDefault();
            return res;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is AuthRoleViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            AuthRoleViewModel itemDel = (AuthRoleViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты роли");
                return false;
            }

            List<auth_role> auth_role_list = dbContext.auth_role.Where(ss => ss.role_name.Trim().ToLower().Equals(itemDel.role_name.Trim().ToLower())).ToList();
            if (auth_role_list == null)
            {
                modelState.AddModelError("", "Не найдена роль " + itemDel.role_name.Trim());
                return false;
            }

            List<int> auth_role_id_list = auth_role_list.Select(ss => ss.role_id).ToList();

            var existing_auth_user = dbContext.auth_user_role.Where(ss => auth_role_id_list.Contains(ss.role_id)).FirstOrDefault();
            if (existing_auth_user != null)
            {
                modelState.AddModelError("", "Есть пользователи данной роли");
                return false;
            }

            dbContext.auth_role.RemoveRange(auth_role_list);
            dbContext.SaveChanges();

            return true;
        }
    }
}
