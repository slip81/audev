﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Auth
{
    public class AuthSectionViewModel : AuBaseViewModel
    {

        public AuthSectionViewModel()
            : base(Enums.MainObjectType.AUTH_SECTION)
        {            
            //auth_section
        }

        [Key()]
        [Display(Name = "Код")]
        public int section_id { get; set; }

        [Display(Name = "Наименование")]
        public string section_name { get; set; }

        [Display(Name = "Группа")]
        public Nullable<int> group_id { get; set; }
    }
}

