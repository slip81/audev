﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Auth
{
    public interface IAuthRoleGroupService : IAuBaseService
    {        
        //
    }

    public class AuthRoleGroupService : AuBaseService, IAuthRoleGroupService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.auth_role_group
                .ProjectTo<AuthRoleGroupViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.auth_role_group
                .Where(ss => ss.item_id == id)
                .ProjectTo<AuthRoleGroupViewModel>()
                .FirstOrDefault();
        }


        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.auth_role_group
                .Where(ss => ss.role_id == parent_id)
                .ProjectTo<AuthRoleGroupViewModel>();
        }
    }
}
