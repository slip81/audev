﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Auth
{
    public class AuthRoleViewModel : AuBaseViewModel
    {

        public AuthRoleViewModel()
            : base(Enums.MainObjectType.AUTH_ROLE)
        {            
            //auth_role
            //vw_auth_role
        }

        [Key()]
        [Display(Name = "Код")]
        public int role_id { get; set; }

        [Display(Name = "Наименование")]
        public string role_name { get; set; }

        [Display(Name = "Для сотрудников")]
        public bool is_group1 { get; set; }

        [Display(Name = "Для клиентов")]
        public bool is_group2 { get; set; }
    }
}

