﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Auth
{
    public interface IAuthRolePermService : IAuBaseService
    {
        IQueryable<AuthRolePermViewModel> GetSectionList_byRole(int role_id, int group_id);
        IQueryable<AuthRolePermViewModel> GetPartList_byRoleAndSection(int role_id, int section_id, int group_id);
        bool ApplyChanges(List<CabinetMvc.ViewModel.Auth.AuthRolePermViewModel>[] permList, int role_id, ModelStateDictionary modelState);        
    }

    public class AuthRolePermService : AuBaseService, IAuthRolePermService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_auth_role_perm
                .ProjectTo<AuthRolePermViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_auth_role_perm
                .Where(ss => ss.role_id == parent_id)
                .ProjectTo<AuthRolePermViewModel>();
        }

        public IQueryable<AuthRolePermViewModel> GetSectionList_byRole(int role_id, int group_id)
        {
            return dbContext.vw_auth_role_section_perm
                .Where(ss => ss.role_id == role_id && ss.role_group_id == group_id)
                .Select(ss => new AuthRolePermViewModel()
                {
                    section_id = ss.section_id,
                    section_name = ss.section_name,
                    role_id = ss.role_id,                    
                    allow_edit = ss.allow_edit_section == 1,
                    allow_view = ss.allow_view_section == 1,
                })
                .Distinct();
                
        }

        public IQueryable<AuthRolePermViewModel> GetPartList_byRoleAndSection(int role_id, int section_id, int group_id)
        {
            return dbContext.vw_auth_role_perm
                .Where(ss => ss.role_id == role_id && ss.section_id == section_id && ss.part_id.HasValue && ss.role_group_id == group_id)
                .ProjectTo<AuthRolePermViewModel>();
        }

        public bool ApplyChanges(List<CabinetMvc.ViewModel.Auth.AuthRolePermViewModel>[] permList,
            int role_id,
            ModelStateDictionary modelState)
        {
            List<auth_role_perm> existing_perm_list = null;

            existing_perm_list = dbContext.auth_role_perm.Where(ss => ss.role_id == role_id).ToList();
            if (existing_perm_list == null)
                existing_perm_list = new List<auth_role_perm>();

            if (permList != null)
            {
                foreach (var permSectionList in permList)
                {
                    foreach (var perm in permSectionList)
                    {
                        var existing_perm = existing_perm_list.Where(ss => ss.part_id == perm.part_id).FirstOrDefault();
                        if (existing_perm != null)
                        {
                            existing_perm.allow_edit = perm.allow_edit;
                            existing_perm.allow_view = perm.allow_view;
                        }
                        else
                        {
                            existing_perm = new auth_role_perm();
                            existing_perm.allow_edit = perm.allow_edit;
                            existing_perm.allow_view = perm.allow_view;
                            existing_perm.part_id = (int)perm.part_id;
                            existing_perm.role_id = (int)role_id;
                            dbContext.auth_role_perm.Add(existing_perm);
                        }
                    }
                }
            }

            dbContext.SaveChanges();            

            return true;
        }

    }
}
