﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Auth
{
    public class AuthRolePermViewModel : AuBaseViewModel
    {

        public AuthRolePermViewModel()
            : base(Enums.MainObjectType.AUTH_ROLE_PERM)
        {            
            //vw_auth_role_perm
        }

        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "Секция")]
        public int section_id { get; set; }

        [Display(Name = "Секция")]
        public string section_name { get; set; }

        [Display(Name = "Раздел")]
        public Nullable<int> part_id { get; set; }

        [Display(Name = "Раздел")]
        public string part_name { get; set; }

        [Display(Name = "Роль")]
        public Nullable<int> role_id { get; set; }

        [Display(Name = "Роль")]
        public string role_name { get; set; }

        [Display(Name = "Разрешен просмотр")]
        public bool allow_view { get; set; }

        [Display(Name = "Разрешено изменение")]
        public bool allow_edit { get; set; }

        [Display(Name = "Группа")]
        public Nullable<int> role_group_id { get; set; }
    }
}

