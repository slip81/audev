﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Stock
{
    public class StockLogViewModel : AuBaseViewModel
    {

        public StockLogViewModel()
            : base()
        {           
            //vw_stock_log
        }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Key()]
        [Display(Name = "Код")]
        public int sales_id { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }

        [Display(Name = "Последняя выгрузка")]
        public Nullable<System.DateTime> last_upl_date { get; set; }

        [Display(Name = "Последняя загрузка")]
        public Nullable<System.DateTime> last_downl_date { get; set; }

        [Display(Name = "Последнее удаление")]
        public Nullable<System.DateTime> last_del_date { get; set; }
    }

}