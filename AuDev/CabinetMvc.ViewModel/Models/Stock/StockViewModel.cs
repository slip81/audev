﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Stock
{
    public class StockViewModel : AuBaseViewModel
    {

        public StockViewModel()
            : base(Enums.MainObjectType.STOCK)
        {           
            //vw_stock_row
        } 

        [Key()]
        [Display(Name = "Код")]
        public int row_id { get; set; }

        [Display(Name = "Отделение")]
        public int stock_id { get; set; }

        [Display(Name = "Пакет")]
        public Nullable<int> batch_id { get; set; }
        
        [Display(Name = "Артикул")]        
        public int artikul { get; set; }

        [Display(Name = "Код наличия")]
        public int row_stock_id { get; set; }

        [Display(Name = "Код ЕСН")]
        public Nullable<int> esn_id { get; set; }

        [Display(Name = "Код препарата")]
        public Nullable<int> prep_id { get; set; }

        [Display(Name = "Наименование")]
        public string prep_name { get; set; }

        [Display(Name = "Изготовитель")]
        public Nullable<int> firm_id { get; set; }

        [Display(Name = "Изготовитель")]
        public string firm_name { get; set; }

        [Display(Name = "Страна")]
        public string country_name { get; set; }

        [Display(Name = "Распаковано")]
        public Nullable<int> unpacked_cnt { get; set; }

        [Display(Name = "Количество")]
        public Nullable<decimal> all_cnt { get; set; }

        [Display(Name = "Цена розничная")]
        public Nullable<decimal> price { get; set; }

        [Display(Name = "Срок годности")]
        public Nullable<System.DateTime> valid_date { get; set; }

        [Display(Name = "Серии")]
        public string series { get; set; }

        [Display(Name = "ЖНВЛП")]
        public bool is_vital { get; set; }

        [Display(Name = "Штрих-код")]
        public string barcode { get; set; }

        [Display(Name = "Цена изготовителя")]
        public Nullable<decimal> price_firm { get; set; }

        [Display(Name = "Цена госреестра")]
        public Nullable<decimal> price_gr { get; set; }

        [Display(Name = "% опт. наценки")]
        public Nullable<decimal> percent_gross { get; set; }

        [Display(Name = "Цена оптовая")]
        public Nullable<decimal> price_gross { get; set; }

        [Display(Name = "Сумма оптовая")]
        public Nullable<decimal> sum_gross { get; set; }

        [Display(Name = "% опт. НДС")]
        public Nullable<decimal> percent_nds_gross { get; set; }

        [Display(Name = "Цена оптовая с НДС")]
        public Nullable<decimal> price_nds_gross { get; set; }

        [Display(Name = "Сумма оптовая с НДС")]
        public Nullable<decimal> sum_nds_gross { get; set; }

        [Display(Name = "% розн. наценки")]
        public Nullable<decimal> percent { get; set; }

        [Display(Name = "Сумма розничная")]
        public Nullable<decimal> sum { get; set; }

        [Display(Name = "Поставщик")]
        public string supplier_name { get; set; }

        [Display(Name = "№ док-та поставщика")]
        public string supplier_doc_num { get; set; }

        [Display(Name = "Дата госреестра")]
        public Nullable<System.DateTime> gr_date { get; set; }

        [Display(Name = "Группа")]
        public string farm_group_name { get; set; }

        [Display(Name = "Дата док-та поставщика")]
        public Nullable<System.DateTime> supplier_doc_date { get; set; }

        [Display(Name = "Сумма прибыли за упаковку")]
        public Nullable<decimal> profit { get; set; }

        [Display(Name = "Примечание")]
        public string mess { get; set; }

        [Display(Name = "Статус")]
        public short state { get; set; }

        [Display(Name = "Когда создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Кем создано")]
        public string crt_user { get; set; }
        
        [Display(Name = "Когда изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Кем изменено")]
        public string upd_user { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Торговая точка")]
        public int sales_id { get; set; }

        [Display(Name = "Отделение")]
        public int depart_id { get; set; }

        [Display(Name = "Отделение")]
        public string depart_name { get; set; }

        [Display(Name = "Аптека")]
        public string pharmacy_name { get; set; }

        [Display(Name = "Пакет")]
        public int batch_num { get; set; }

        [Display(Name = "Частей в пакете")]
        public Nullable<int> part_cnt { get; set; }

        [Display(Name = "Номер части пакета")]
        public Nullable<int> part_num { get; set; }

        [Display(Name = "Строк в пакете")]
        public Nullable<int> row_cnt { get; set; }

        [Display(Name = "Пакет создан")]
        public Nullable<System.DateTime> batch_crt_date { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }

        [Display(Name = "Адрес")]
        public string depart_address { get; set; }
    }

}