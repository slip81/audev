﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Wa;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Stock
{
    public interface IStockLogService : IAuBaseService
    {
        IQueryable<StockLogViewModel> GetList_bySales(int? client_id, int? sales_id, int? day_cnt);
    }

    public class StockLogService : AuBaseService, IStockLogService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_stock_log
                .Where(ss => ss.sales_id == parent_id
                )
                .ProjectTo<StockLogViewModel>();
        }

        public IQueryable<StockLogViewModel> GetList_bySales(int? client_id, int? sales_id, int? day_cnt)
        {
            var query = dbContext.vw_stock_log
                .Where(ss => ((ss.client_id == client_id) || (!client_id.HasValue))
                    && ((ss.sales_id == sales_id) || (!sales_id.HasValue))
                    );
            DateTime dateCut = DateTime.Today;
            if ((day_cnt.HasValue) && (day_cnt.GetValueOrDefault(0) >= 0))
            {
                dateCut = dateCut.AddDays(-day_cnt.GetValueOrDefault(0));
                query = query.Where(ss => !ss.last_downl_date.HasValue || !ss.last_upl_date.HasValue || ss.last_downl_date <= dateCut || ss.last_upl_date <= dateCut);
            }
            return query
                .ProjectTo<StockLogViewModel>();
        }
    }
}