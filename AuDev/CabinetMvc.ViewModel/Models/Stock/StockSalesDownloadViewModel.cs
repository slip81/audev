﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Stock
{
    public class StockSalesDownloadViewModel : AuBaseViewModel
    {

        public StockSalesDownloadViewModel()
            : base(Enums.MainObjectType.STOCK_SALES_DOWNLOAD)
        {           
            //vw_stock_sales_download
        }

        [Key()]
        [Display(Name = "Код")]
        public int download_id { get; set; }

        [Display(Name = "Торговая точка")]
        public int sales_id { get; set; }

        [Display(Name = "Частей в пакете")]
        public Nullable<int> part_cnt { get; set; }

        [Display(Name = "Часть пакета")]
        public Nullable<int> part_num { get; set; }

        [Display(Name = "Строк в пакете")]
        public Nullable<int> row_cnt { get; set; }

        [Display(Name = "Активен")]
        public bool is_active { get; set; }

        [Display(Name = "Загружены изменения с")]
        public Nullable<System.DateTime> download_batch_date_beg { get; set; }

        [Display(Name = "Загружены изменения по")]
        public Nullable<System.DateTime> download_batch_date_end { get; set; }

        [Display(Name = "Макс. строк в пакете")]
        public int batch_row_cnt { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }
    }

}