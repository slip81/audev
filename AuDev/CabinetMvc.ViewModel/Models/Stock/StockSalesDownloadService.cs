﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Stock
{
    public interface IStockSalesDownloadService : IAuBaseService
    {
        IQueryable<StockSalesDownloadViewModel> GetList_bySales(int? client_id, int? sales_id);
    }

    public class StockSalesDownloadService : AuBaseService, IStockSalesDownloadService
    {

        public IQueryable<StockSalesDownloadViewModel> GetList_bySales(int? client_id, int? sales_id)
        {
            return dbContext.vw_stock_sales_download
                .Where(ss => ((ss.client_id == client_id) || (!client_id.HasValue))
                    && ((ss.sales_id == sales_id) || (!sales_id.HasValue))                    
                    )
                .ProjectTo<StockSalesDownloadViewModel>();
        }        
    }
}