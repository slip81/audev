﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Stock
{
    public class StockBatchViewModel : AuBaseViewModel
    {

        public StockBatchViewModel()
            : base(Enums.MainObjectType.STOCK_BATCH)
        {           
            //vw_stock_batch
        } 

        [Key()]
        [Display(Name = "Код")]
        public int batch_id { get; set; }

        [Display(Name = "Отделение")]
        public int stock_id { get; set; }

        [Display(Name = "№ пакета")]
        public int batch_num { get; set; }

        [Display(Name = "Частей в пакете")]
        public Nullable<int> part_cnt { get; set; }

        [Display(Name = "Часть пакета")]
        public Nullable<int> part_num { get; set; }

        [Display(Name = "Строк в пакете")]
        public Nullable<int> row_cnt { get; set; }

        [Display(Name = "Активен")]
        public bool is_active { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }
        
        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }
        
        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Торговая точка")]
        public int sales_id { get; set; }

        [Display(Name = "Подразделение")]
        public int depart_id { get; set; }

        [Display(Name = "Подразделение")]
        public string depart_name { get; set; }

        [Display(Name = "Аптека")]
        public string pharmacy_name { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }
    }

}