﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Wa;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Stock
{
    public interface IStockService : IAuBaseService
    {
        IQueryable<StockViewModel> GetList_bySales(int? client_id, int? sales_id, string search);
        bool InsertTask(int sales_id, int request_type_id, ModelStateDictionary modelState);
        bool CancelTask(int task_id, ModelStateDictionary modelState);
    }

    public class StockService : AuBaseService, IStockService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_stock_row
                .Where(ss => ss.client_id == parent_id
                    && ss.state != (int)Enums.UndStockRowStateEnum.REMOVED
                    && ss.is_active == false
                )
                .ProjectTo<StockViewModel>();
        }

        public IQueryable<StockViewModel> GetList_bySales(int? client_id, int? sales_id, string search)
        {
            var query = dbContext.vw_stock_row
                .Where(ss => ((ss.client_id == client_id) || (!client_id.HasValue))
                    && ((ss.sales_id == sales_id) || (!sales_id.HasValue))
                    && (ss.state != (int)Enums.UndStockRowStateEnum.REMOVED)
                    && ss.is_active == false
                    && ss.batch_is_confirmed == true
                    );

            List<string> searchList = new List<string>();
            if (!String.IsNullOrWhiteSpace(search))
            {
                searchList = search.Split(' ').ToList();
                if (searchList.Count > 0)
                {
                    foreach (var searchListItem in searchList)
                    {
                        if (String.IsNullOrWhiteSpace(searchListItem))
                            continue;
                        query = query.Where(ss => ss.prep_name.Trim().ToLower().Contains(searchListItem.Trim().ToLower())
                            || ss.firm_name.Trim().ToLower().Contains(searchListItem.Trim().ToLower())
                            );
                    }
                }
            }
            
            return query.ProjectTo<StockViewModel>();
        }

        public bool InsertTask(int sales_id, int request_type_id, ModelStateDictionary modelState)
        {
            IWaTaskService waTaskService = DependencyResolver.Current.GetService<IWaTaskService>();
            if (waTaskService == null)
            {
                modelState.AddModelError("", "Системная ошибка ! Обратитесь к разработчикам");
                return false;
            }

            sales sales = dbContext.sales.Where(ss => ss.id == sales_id).FirstOrDefault();
            if (sales == null)
            {
                modelState.AddModelError("", "Не найдена торговая точка с кодом " + sales_id.ToString());
                return false;
            }

            wa_request_type wa_request_type = dbContext.wa_request_type.Where(ss => ss.request_type_id == request_type_id).FirstOrDefault();
            if (wa_request_type == null)
            {
                modelState.AddModelError("", "Не найден тип запроса с кодом " + request_type_id.ToString());
                return false;
            }

            int? client_id = dbContext.sales.Where(ss => ss.id == sales_id).Select(ss => ss.client_id).FirstOrDefault();
            /*
            var stocksForDel = (from t1 in dbContext.stock
                               from t2 in dbContext.sales
                               where t2.id == sales_id
                               && t1.client_id == t2.client_id
                               select t1)
                               .ToList();
            */
            var stockSalesForDel = dbContext.stock.Where(ss => ss.client_id == client_id).Select(ss => ss.sales_id).Distinct().ToList();
            if ((stockSalesForDel == null) || (stockSalesForDel.Count <= 0))
            {
                modelState.AddModelError("", "Не найдены торговые точки по клиенту для торговой точки с кодом " + sales_id.ToString());
                return false;
            }

            Enums.WaTaskStateEnum taskState = wa_request_type.is_outer ? Enums.WaTaskStateEnum.ISSUED_CLIENT : Enums.WaTaskStateEnum.ISSUED_SERVER;
            StockServiceParam stockServiceParam = new StockServiceParam() { sales_id = sales_id };

            foreach (var stockSaleForDel in stockSalesForDel)
            {
                stockServiceParam = new StockServiceParam() { sales_id = stockSaleForDel };
                WaTaskViewModel wa = new WaTaskViewModel()
                {
                    request_type_id = request_type_id,
                    task_state_id = (int)taskState,
                    client_id = client_id,
                    sales_id = stockSaleForDel,
                    task_param = JsonConvert.SerializeObject(stockServiceParam),
                };

                WaTaskViewModel res1 = (WaTaskViewModel)waTaskService.Insert(wa, modelState);
                if ((res1 == null) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при создании задания для WebAPI");
                    return false;
                }
            }


            /*
            string user = currUser.CabUserName;
            asna_user_log asna_user_log = null;
            asna_user_log = prepareLog("Создано задание " + res1.ord_client.ToString() + ": " + res1.request_type_name, DateTime.Now, user);
            asna_user_log.asna_user_id = asna_user_id;
            dbContext.asna_user_log.Add(asna_user_log);
            */

            dbContext.SaveChanges();

            return true;
        }

        public bool CancelTask(int task_id, ModelStateDictionary modelState)
        {
            IWaTaskService waTaskService = DependencyResolver.Current.GetService<IWaTaskService>();
            if (waTaskService == null)
            {
                modelState.AddModelError("", "Системная ошибка ! Обратитесь к разработчикам");
                return false;
            }

            wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
            if (wa_task == null)
            {
                modelState.AddModelError("", "Не найдено задание с кодом " + task_id.ToString());
                return false;
            }


            var res1 = waTaskService.UpdateTaskSate(task_id, (int)Enums.WaTaskStateEnum.CANCELED_SERVER, modelState);
            if ((!res1) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при отмене задания для WebAPI");
                return false;
            }
            
            /*
            string user = currUser.CabUserName;
            asna_user_log asna_user_log = null;
            asna_user_log = prepareLog("Отменено задание " + wa_task.ord_client.ToString(), DateTime.Now, user);
            asna_user_log.asna_user_id = asna_user_id;
            dbContext.asna_user_log.Add(asna_user_log);
            dbContext.SaveChanges();
            */

            return true;
        }


    }
}