﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceKitItemViewModel : AuBaseViewModel
    {

        public ServiceKitItemViewModel()
            : base(Enums.MainObjectType.SERVICE_KIT_ITEM)
        {
            //vw_service_kit_item
        }

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Набор")]
        public int service_kit_id { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Версия")]
        public Nullable<int> version_id { get; set; }

        [Display(Name = "Набор")]
        public string service_kit_name { get; set; }

        [Display(Name = "Услуга")]
        public string service_name { get; set; }

        [Display(Name = "Версия")]
        public string version_name { get; set; }

    }
}