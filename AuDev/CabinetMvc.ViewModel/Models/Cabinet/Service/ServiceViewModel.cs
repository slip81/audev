﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceViewModel : AuBaseViewModel
    {

        public ServiceViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE)
        {

        }

        public ServiceViewModel(vw_service item)
            : base(Enums.MainObjectType.CAB_SERVICE)
        {
            id = item.id;
            guid = item.guid;
            name = item.name;
            description = item.description;
            parent_id = item.parent_id;
            is_deleted = item.is_deleted == 1;
            exe_name = item.exe_name;
            is_site = item.is_site == 1;            
            price = item.price;
            need_key = item.need_key == 1;
            NeedCheckVersionLicense = item.NeedCheckVersionLicense == 1;
            have_spec = item.have_spec;
            max_workplace_cnt = item.max_workplace_cnt;
            is_service = item.is_service;
            priority = item.priority;
            group_id = item.group_id;
            group_name = item.group_name;
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "GUID")]
        public string guid { get; set; }

        [Display(Name = "Название")]
        public string name { get; set; }

        [Display(Name = "Описание")]
        public string description { get; set; }

        [Display(Name = "Родительская услуга")]        
        public Nullable<int> parent_id { get; set; }

        [Display(Name = "Удалено")]
        public Nullable<bool> is_deleted { get; set; }

        [Display(Name = "exe_name")]
        public string exe_name { get; set; }

        [Display(Name = "is_site")]
        public Nullable<bool> is_site { get; set; }

        [Display(Name = "Цена")]
        public Nullable<decimal> price { get; set; }

        [Display(Name = "need_key")]
        public Nullable<bool> need_key { get; set; }

        [Display(Name = "NeedCheckVersionLicense")]
        public Nullable<bool> NeedCheckVersionLicense { get; set; }

        [Display(Name = "С параметрами")]
        public bool have_spec { get; set; }

        [Display(Name = "Сервис")]
        public bool is_service { get; set; }

        [Display(Name = "Приоритет")]
        public Nullable<int> priority { get; set; }

        [Display(Name = "Макс. кол-во раб. мест")]
        public Nullable<int> max_workplace_cnt { get; set; }        

        [Display(Name = "Группа")]
        public Nullable<int> group_id { get; set; }

        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [Display(Name = "Версия")]
        public Nullable<int> OrderedVersionId_ReadOnly { get; set; }

        [Display(Name = "Версия")]
        public Nullable<int> OrderedVersionId { get; set; }

        [Display(Name = "Количество")]
        [UIHint("Integer")]
        [Range(0, 100, ErrorMessage="Можно заказать от 0 до 100 лицензий")]
        public Nullable<int> OrderedLicenseCount { get; set; }   

    }
}