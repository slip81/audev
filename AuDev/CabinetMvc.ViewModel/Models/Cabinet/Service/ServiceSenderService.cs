﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceSenderService : IAuBaseService
    {
        ServiceSenderViewModel GeItem_byService(int workplace_id, int service_id);
    }

    public class ServiceSenderService : AuBaseService, IServiceSenderService
    {
        public ServiceSenderViewModel GeItem_byService(int workplace_id, int service_id)
        {
            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;

            DateTime today = DateTime.Today;

            ServiceSenderViewModel res = null;

            switch (servEnum)
            {
                case Enums.ClientServiceEnum.SENDER_DEFECT:
                case Enums.ClientServiceEnum.SENDER_GRLS:
                    res = dbContext.service_sender
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id)
                        .ProjectTo<ServiceSenderViewModel>()
                        .FirstOrDefault()
                        ;
                    if (res == null)
                    {
                        string sales_email = (from t1 in dbContext.vw_workplace
                                             from t2 in dbContext.sales
                                             where t1.workplace_id == workplace_id
                                             && t1.sales_id == t2.id
                                             select t2.email
                                            )
                                            .FirstOrDefault();
                        service_sender service_sender = new service_sender();
                        service_sender.date_beg = today;
                        service_sender.date_end = null;
                        service_sender.interval_in_days = 3;
                        service_sender.is_disabled = false;
                        service_sender.email = sales_email;
                        service_sender.service_id = service_id;
                        service_sender.workplace_id = workplace_id;
                        service_sender.schedule_type = (int)Enums.ScheduleTypeEnum.INTERVAL_DAILY;
                        service_sender.schedule_weekly = "";
                        service_sender.schedule_monthly = "";
                        dbContext.service_sender.Add(service_sender);
                        dbContext.SaveChanges();
                        res = new ServiceSenderViewModel();
                        ModelMapper.Map<service_sender, ServiceSenderViewModel>(service_sender, res);
                    }
                    break;
                default:
                    break;
            }

            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceSenderViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ServiceSenderViewModel itemEdit = (ServiceSenderViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры услуги");
                return null;
            }

            service_sender service_sender = dbContext.service_sender.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (service_sender == null)
            {
                modelState.AddModelError("", "Не найдены параметры услуги");
                return null;
            }

            ServiceSenderViewModel oldModel = new ServiceSenderViewModel();
            ModelMapper.Map<service_sender, ServiceSenderViewModel>(service_sender, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<ServiceSenderViewModel, service_sender>(itemEdit, service_sender);

            dbContext.SaveChanges();

            ServiceSenderViewModel res = new ServiceSenderViewModel();
            ModelMapper.Map<service_sender, ServiceSenderViewModel>(service_sender, res);

            return res;            
        }

    }   
}
