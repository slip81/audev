﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceSenderLogViewModel :  AuBaseViewModel
    {

        public ServiceSenderLogViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE_LOG_SENDER)
        {           
            //service_sender_log
        } 

        [Key()]
        [Display(Name = "Код")]
        public int log_id { get; set; }

        [Display(Name = "Раб. место")]
        public int workplace_id { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Дата-время")]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }
    }
}