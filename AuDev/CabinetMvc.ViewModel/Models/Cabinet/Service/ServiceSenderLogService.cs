﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceSenderLogService : IAuBaseService
    {
        IQueryable<ServiceSenderLogViewModel> GeList_byService(int workplace_id, int service_id);
        ServiceSenderLogViewModel GetItem_Last(int client_id, int service_id);
    }

    public class ServiceSenderLogService : AuBaseService, IServiceSenderLogService
    {
        public IQueryable<ServiceSenderLogViewModel> GeList_byService(int workplace_id, int service_id)
        {
            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;

            switch (servEnum)
            {
                case Enums.ClientServiceEnum.SENDER_DEFECT:
                case Enums.ClientServiceEnum.SENDER_GRLS:
                    return dbContext.service_sender_log
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id)
                        .ProjectTo<ServiceSenderLogViewModel>()
                        ;
                default:
                    return dbContext.service_sender_log
                        .Where(ss => 0 == 1)
                        .ProjectTo<ServiceSenderLogViewModel>()
                        ;
            }

        }

        public ServiceSenderLogViewModel GetItem_Last(int client_id, int service_id)
        {
            return (from t1 in dbContext.vw_workplace
                    from t2 in dbContext.service_sender_log
                    where t1.client_id == client_id
                    && t1.workplace_id == t2.workplace_id
                    && t2.service_id == service_id
                    select t2)
                    .OrderByDescending(ss => ss.date_beg)
                    .ProjectTo<ServiceSenderLogViewModel>()
                    .FirstOrDefault()
                    ;
        }
    }
}