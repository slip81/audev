﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceCvaViewModel :  AuBaseViewModel
    {

        public ServiceCvaViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE_CVA)
        {           
            //service_cva
        } 

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Адрес отправки")]
        [Required(ErrorMessage = "Не задан адрес")]
        public string target_url { get; set; }

        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Не задан логин")]
        public string pharm_login { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public string pharm_pwd { get; set; }

        [Display(Name = "Мин. интервал отправки (часы)")]
        [Range(1, int.MaxValue)]
        public Nullable<int> send_interval { get; set; }

        [Display(Name = "Код аптеки в учетной системе")]
        [Required(ErrorMessage = "Не задан код аптеки")]
        public string pharm_code { get; set; }

        [Display(Name = "Работает ли аптека")]
        public short active { get; set; }

        //[Display(Name = "info_internet_trade")]
        //public string info_internet_trade { get; set; }

        [Display(Name = "Наименование аптеки")]
        public string info_name { get; set; }

        [Display(Name = "Принимаются ли к оплате карты VISA, MasterCard и на каких условиях ")]
        public string info_bank_cards { get; set; }

        [Display(Name = "Телефон для связи")]
        public string info_sale_phone { get; set; }

        [Display(Name = "Дополнительная информация")]
        public string info_additional_info { get; set; }

        [Display(Name = "Адрес сайта аптеки")]
        public string info_www { get; set; }

        [Display(Name = "Наименование населенного пункта")]
        public string info_city { get; set; }

        [Display(Name = "Наименование района населенного пункта ")]
        public string info_city_area { get; set; }

        [Display(Name = "Адрес  аптеки в населенном пункте")]
        public string info_address { get; set; }

        [Display(Name = "Время  работы аптеки ")]
        public string info_sale_shedule { get; set; }

        [Display(Name = "Координаты аптеки (широта, долгота)")]
        public string info_map_gps { get; set; }

        [Display(Name = "Наименование аптеки, отображаемое на карте")]
        public string info_map_caption { get; set; }

        [Display(Name = "Клиент")]
        public int? ClientId { get; set; }
    }

}