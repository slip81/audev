﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceKitViewModel : AuBaseViewModel
    {

        public ServiceKitViewModel()
            : base(Enums.MainObjectType.SERVICE_KIT)
        {
            //service_kit
        }

        [Key()]
        [Display(Name = "Код")]
        public int service_kit_id { get; set; }

        [Display(Name = "Название набора")]
        [Required(ErrorMessage = "Не задано название набора")]
        public string service_kit_name { get; set; }

    }
}