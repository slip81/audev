﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceCvaService : IAuBaseService
    {
        ServiceCvaViewModel GeItem_byService(int workplace_id, int service_id);
    }

    public class ServiceCvaService : AuBaseService, IServiceCvaService
    {
        public ServiceCvaViewModel GeItem_byService(int workplace_id, int service_id)
        {
            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;

            DateTime today = DateTime.Today;

            ServiceCvaViewModel res = null;

            switch (servEnum)
            {
                case Enums.ClientServiceEnum.CVA:                
                    res = dbContext.service_cva
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id)
                        .ProjectTo<ServiceCvaViewModel>()
                        .FirstOrDefault()
                        ;
                    if (res == null)
                    {
                        service_cva service_cva = new service_cva();
                        service_cva.active = 1;
                        service_cva.send_interval = 12;
                        service_cva.service_id = service_id;
                        service_cva.workplace_id = workplace_id;
                        dbContext.service_cva.Add(service_cva);
                        dbContext.SaveChanges();
                        res = new ServiceCvaViewModel();
                        ModelMapper.Map<service_cva, ServiceCvaViewModel>(service_cva, res);
                    }
                    break;
                default:
                    break;
            }

            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceCvaViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ServiceCvaViewModel itemEdit = (ServiceCvaViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры услуги");
                return null;
            }

            service_cva service_cva = dbContext.service_cva.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (service_cva == null)
            {
                modelState.AddModelError("", "Не найдены параметры услуги");
                return null;
            }

            ServiceCvaViewModel oldModel = new ServiceCvaViewModel();
            ModelMapper.Map<service_cva, ServiceCvaViewModel>(service_cva, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<ServiceCvaViewModel, service_cva>(itemEdit, service_cva);

            dbContext.SaveChanges();

            ServiceCvaViewModel res = new ServiceCvaViewModel();
            ModelMapper.Map<service_cva, ServiceCvaViewModel>(service_cva, res);

            return res;            
        }

    }   
}
