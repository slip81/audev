﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceDefectService : IAuBaseService
    {
        ServiceDefectViewModel GeItem_byService(int workplace_id, int service_id);
    }

    public class ServiceDefectService : AuBaseService, IServiceDefectService
    {
        public ServiceDefectViewModel GeItem_byService(int workplace_id, int service_id)
        {
            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;

            DateTime today = DateTime.Today;

            ServiceDefectViewModel res = null;

            switch (servEnum)
            {
                case Enums.ClientServiceEnum.DEFECT:
                    res = dbContext.service_defect
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id)
                        .ProjectTo<ServiceDefectViewModel>()
                        .FirstOrDefault()
                        ;
                    if (res == null)
                    {
                        service_defect service_defect = new service_defect();
                        service_defect.workplace_id = workplace_id;
                        service_defect.service_id = service_id;
                        service_defect.is_allow_download = false;
                        dbContext.service_defect.Add(service_defect);
                        dbContext.SaveChanges();
                        res = new ServiceDefectViewModel();
                        ModelMapper.Map<service_defect, ServiceDefectViewModel>(service_defect, res);
                    }
                    break;
                default:
                    break;
            }

            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceDefectViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ServiceDefectViewModel itemEdit = (ServiceDefectViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры услуги");
                return null;
            }

            service_defect service_defect = dbContext.service_defect.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (service_defect == null)
            {
                modelState.AddModelError("", "Не найдены параметры услуги");
                return null;
            }

            ServiceDefectViewModel oldModel = new ServiceDefectViewModel();
            ModelMapper.Map<service_defect, ServiceDefectViewModel>(service_defect, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<ServiceDefectViewModel, service_defect>(itemEdit, service_defect);

            dbContext.SaveChanges();

            ServiceDefectViewModel res = new ServiceDefectViewModel();
            ModelMapper.Map<service_defect, ServiceDefectViewModel>(service_defect, res);

            return res;            
        }

    }   
}
