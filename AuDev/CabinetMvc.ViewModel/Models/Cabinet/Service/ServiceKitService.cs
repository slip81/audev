﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceKitService : IAuBaseService
    {
        bool ServiceKitAdd(int service_kit_id, string service_kit_name, IEnumerable<ServiceViewModel> serviceList, ModelStateDictionary modelState);
    }

    public class ServiceKitService : AuBaseService, IServiceKitService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.service_kit
                .ProjectTo<ServiceKitViewModel>();
        }
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.service_kit
                .Where(ss => ss.service_kit_id == id)
                .ProjectTo<ServiceKitViewModel>()
                .FirstOrDefault();
        }

        public bool ServiceKitAdd(int service_kit_id, string service_kit_name, IEnumerable<ServiceViewModel> serviceList, ModelStateDictionary modelState)
        {
            if ((serviceList == null) || (serviceList.Count() <= 0))
            {
                modelState.AddModelError("", "Не заданы услуги");
                return false;
            }

            service_kit service_kit = null;
            bool isNew = false;
            if (service_kit_id <= 0)
            {
                service_kit = new service_kit();
                service_kit.service_kit_name = service_kit_name;
                dbContext.service_kit.Add(service_kit);
                isNew = true;
            }
            else
            {
                service_kit = dbContext.service_kit.Where(ss => ss.service_kit_id == service_kit_id).FirstOrDefault();
                if (service_kit == null)
                {
                    modelState.AddModelError("", "Не найден набор с кодом " + service_kit_id.ToString());
                    return false;
                }
                service_kit.service_kit_name = service_kit_name;
            }
            
            if (!isNew)
            {
                dbContext.service_kit_item.RemoveRange(dbContext.service_kit_item.Where(ss => ss.service_kit_id == service_kit.service_kit_id));
            }

            foreach (var service in serviceList)
            {
                service_kit_item service_kit_item = new service_kit_item();
                service_kit_item.service_kit = service_kit;
                service_kit_item.service_id = service.id;
                dbContext.service_kit_item.Add(service_kit_item);
            }

            dbContext.SaveChanges();

            return true;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceKitViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            ServiceKitViewModel itemDel = (ServiceKitViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты набора");
                return false;
            }

            service_kit service_kit = dbContext.service_kit.Where(ss => ss.service_kit_id == itemDel.service_kit_id).FirstOrDefault();
            if (service_kit == null)
            {
                modelState.AddModelError("", "Не найден набор с кодом " + itemDel.service_kit_id);
                return false;
            }

            dbContext.service_kit_item.RemoveRange(dbContext.service_kit_item.Where(ss => ss.service_kit_id == itemDel.service_kit_id));
            dbContext.service_kit.Remove(service_kit);

            dbContext.SaveChanges();

            return true;
        }
    }
}
 