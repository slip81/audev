﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceDefectViewModel :  AuBaseViewModel
    {

        public ServiceDefectViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE_DEFECT)
        {           
            //service_defect
        } 

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Разрешено скачивания ZIP-файла")]
        public bool is_allow_download { get; set; }

        [Display(Name = "Клиент")]
        public int? ClientId { get; set; }
    }

}