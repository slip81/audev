﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceGroupService : IAuBaseService
    {
        //
    }

    public class ServiceGroupService : AuBaseService, IServiceGroupService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.service_group
                .ProjectTo<ServiceGroupViewModel>()
                ;                
        }

    }
}
