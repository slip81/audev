﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceStockViewModel : AuBaseViewModel
    {

        public ServiceStockViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE_STOCK)
        {           
            //service_stock
        } 

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }
        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }
        [Display(Name = "Код услуги")]
        public int service_id { get; set; }
        [Display(Name = "Макс. кол-во строк в пакете")]
        public int batch_row_cnt { get; set; }

        [Display(Name = "Клиент")]
        public int? ClientId { get; set; }
    }

}