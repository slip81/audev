﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceEsn2ViewModel :  AuBaseViewModel
    {

        public ServiceEsn2ViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE_ESN2)
        {           
            //service_esn2
        } 

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Имя компьютера")]
        public string comp_name { get; set; }

        //[Display(Name = "Клиент")]
        //public int? ClientId { get; set; }
    }

}