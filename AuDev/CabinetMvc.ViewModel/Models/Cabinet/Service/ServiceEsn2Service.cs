﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceEsn2Service : IAuBaseService
    {
        List<ServiceEsn2ViewModel> GeItems_byService(int workplace_id, int service_id);
    }

    public class ServiceEsn2Service : AuBaseService, IServiceEsn2Service
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.service_esn2.Where(ss => ss.item_id == id).ProjectTo<ServiceEsn2ViewModel>().FirstOrDefault();
        }

        public List<ServiceEsn2ViewModel> GeItems_byService(int workplace_id, int service_id)
        {
            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;

            DateTime today = DateTime.Today;

            List<ServiceEsn2ViewModel> res = null;

            switch (servEnum)
            {
                case Enums.ClientServiceEnum.UND:
                    res = dbContext.service_esn2
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id)
                        .ProjectTo<ServiceEsn2ViewModel>()
                        .ToList();
                    break;
                default:
                    break;
            }

            return res;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceEsn2ViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ServiceEsn2ViewModel itemEdit = (ServiceEsn2ViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры услуги");
                return null;
            }

            service_esn2 service_esn2 = new service_esn2();
            ModelMapper.Map<ServiceEsn2ViewModel, service_esn2 >(itemEdit, service_esn2);
            dbContext.service_esn2.Add(service_esn2);
            dbContext.SaveChanges();

            return xGetItem(service_esn2.item_id);         
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceEsn2ViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            ServiceEsn2ViewModel itemDel = (ServiceEsn2ViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры услуги");
                return false;
            }

            service_esn2 service_esn2 = dbContext.service_esn2.Where(ss => ss.item_id == itemDel.item_id).FirstOrDefault();
            if (service_esn2 == null)
            {
                modelState.AddModelError("", "Не найдены параметры услуги");
                return false;
            }
            
            dbContext.service_esn2.Remove(service_esn2);
            dbContext.SaveChanges();

            return true;
        }

    }   
}
