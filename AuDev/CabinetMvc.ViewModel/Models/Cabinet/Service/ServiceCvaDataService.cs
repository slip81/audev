﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceCvaDataService : IAuBaseService
    {
        string GetItem_Data(int id);
        IQueryable<ServiceCvaDataViewModel> GetList_byClientOrSales(int client_id, int sales_id);
    }

    public class ServiceCvaDataService : AuBaseService, IServiceCvaDataService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_service_cva_data
                .Where(ss => (((parent_id > 0) && (ss.client_id == parent_id)) || (parent_id <= 0)))
                .ProjectTo<ServiceCvaDataViewModel>()
                ;
        }

        public IQueryable<ServiceCvaDataViewModel> GetList_byClientOrSales(int client_id, int sales_id)
        {
            return dbContext.vw_service_cva_data
                .Where(ss => (                    
                    (((client_id > 0) && (ss.client_id == client_id)) || (client_id <= 0))
                    &&
                    (((sales_id > 0) && (ss.sales_id == sales_id)) || (sales_id <= 0))
                    )
                    )
                .ProjectTo<ServiceCvaDataViewModel>()
                ;
        }

        public string GetItem_Data(int id)
        {
            return dbContext.service_cva_data_ext.Where(ss => ss.item_id == id)    
                .Select(ss => ss.cva_full_data)
                .FirstOrDefault();
        }
    }   
}
