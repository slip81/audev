﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceGroupViewModel : AuBaseViewModel
    {

        public ServiceGroupViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE_GROUP)
        {
            //service_group
        }

        
        [Key()]
        [Display(Name = "Код")]
        public int group_id { get; set; }

        [Display(Name = "Наименование")]
        public string group_name { get; set; }

    }
}