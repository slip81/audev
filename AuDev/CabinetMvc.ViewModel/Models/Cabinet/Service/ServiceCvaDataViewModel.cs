﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceCvaDataViewModel :  AuBaseViewModel
    {

        public ServiceCvaDataViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE_CVA_DATA)
        {   
            //service_cva_data
            //vw_service_cva_data
        } 

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Данные")]
        public string cva_data { get; set; }

        [Display(Name = "Консолидированные данные")]
        public string cva_cons_data { get; set; }

        [Display(Name = "Полные данные")]
        public string cva_full_data { get; set; }

        [Display(Name = "Номер пакета")]        
        public Nullable<int> batch_num { get; set; }

        [Display(Name = "Дата создания пакета")]
        public Nullable<System.DateTime> batch_date { get; set; }

        [Display(Name = "Статус пакета")]
        public int batch_state { get; set; }

        [Display(Name = "Дата попытки отправки")]
        public Nullable<System.DateTime> sending_date { get; set; }

        [Display(Name = "Дата успешной отправки")]
        public Nullable<System.DateTime> sent_date { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Адрес")]
        public int sales_id { get; set; }

        [Display(Name = "Адрес")]
        public string sales_address { get; set; }

        [Display(Name = "Рабочее место")]
        public string workplace_description { get; set; }

        [Display(Name = "Размер пакета")]
        public string batch_size { get; set; }

        [Display(Name = "Формат")]
        public Nullable<int> shortformat { get; set; }

        [Display(Name = "Статус пакета")]
        public string batch_state_name { get { return batch_state.EnumName<Enums.CvaSendStateEnum>(); } }

        [Display(Name = "Формат")]
        public string shortformat_str 
        { 
            get 
            {
                if (shortformat == 1)
                    return "Краткий";
                else if (shortformat == 0)
                    return "Полный";
                else return "";
            } 
        }

    }

}