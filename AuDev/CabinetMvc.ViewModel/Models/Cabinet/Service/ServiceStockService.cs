﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceStockService : IAuBaseService
    {
        ServiceStockViewModel GeItem_byService(int workplace_id, int service_id);
    }

    public class ServiceStockService : AuBaseService, IServiceStockService
    {
        public ServiceStockViewModel GeItem_byService(int workplace_id, int service_id)
        {
            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;

            DateTime today = DateTime.Today;

            ServiceStockViewModel res = null;

            switch (servEnum)
            {
                case Enums.ClientServiceEnum.STOCK:                
                    res = dbContext.service_stock
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id)
                        .ProjectTo<ServiceStockViewModel>()
                        .FirstOrDefault()
                        ;
                    if (res == null)
                    {
                        service_stock service_stock = new service_stock();
                        service_stock.service_id = service_id;
                        service_stock.workplace_id = workplace_id;
                        service_stock.batch_row_cnt = 100;
                        dbContext.service_stock.Add(service_stock);
                        dbContext.SaveChanges();
                        res = new ServiceStockViewModel();
                        ModelMapper.Map<service_stock, ServiceStockViewModel>(service_stock, res);
                    }
                    break;
                default:
                    break;
            }

            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceStockViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ServiceStockViewModel itemEdit = (ServiceStockViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры услуги");
                return null;
            }

            service_stock service_stock = dbContext.service_stock.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (service_stock == null)
            {
                modelState.AddModelError("", "Не найдены параметры услуги");
                return null;
            }

            ServiceStockViewModel oldModel = new ServiceStockViewModel();
            ModelMapper.Map<service_stock, ServiceStockViewModel>(service_stock, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<ServiceStockViewModel, service_stock>(itemEdit, service_stock);

            dbContext.SaveChanges();

            ServiceStockViewModel res = new ServiceStockViewModel();
            ModelMapper.Map<service_stock, ServiceStockViewModel>(service_stock, res);

            return res;            
        }
    }   
}
