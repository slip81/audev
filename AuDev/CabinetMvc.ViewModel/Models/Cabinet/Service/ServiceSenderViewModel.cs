﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ServiceSenderViewModel :  AuBaseViewModel
    {

        public ServiceSenderViewModel()
            : base(Enums.MainObjectType.CAB_SERVICE_SENDER)
        {           
            //service_sender
        } 

        [Key()]
        [Display(Name = "Код")]        
        public int item_id { get; set; }

        [Display(Name = "Раб. место")]
        public int workplace_id { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Отключено")]
        public bool is_disabled { get; set; }

        [Display(Name = "Дата начала действия")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "Дата окончания действия")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "периодичность (дни)")]
        public int interval_in_days { get; set; }

        [Display(Name = "Электронная почта")]
        [Required(ErrorMessage = "Не задана электронная почта")]
        [DataType((DataType.EmailAddress))]
        public string email { get; set; }

        [Display(Name = "Недельное расписание")]
        public string schedule_weekly { get; set; }

        [Display(Name = "Месячное расписание")]
        public string schedule_monthly { get; set; }

        [Display(Name = "Тип расписания")]
        public int schedule_type { get; set; }

        [Display(Name = "Клиент")]
        public int? ClientId { get; set; }
    }

    public class ScheduleBase
    {
        public int DayNum { get; set; }
        public bool IsActive { get; set; }
    }

    public class ScheduleWeekly : ScheduleBase
    {
        //
    }

    public class ScheduleMonthly : ScheduleBase
    {
        //
    }
}