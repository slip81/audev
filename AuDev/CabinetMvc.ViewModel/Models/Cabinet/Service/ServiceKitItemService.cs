﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceKitItemService : IAuBaseService
    {
        //
    }

    public class ServiceKitItemService : AuBaseService, IServiceKitItemService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.service_kit_item
                .Where(ss => ss.service_kit_id == parent_id)
                .ProjectTo<ServiceKitItemViewModel>();
        }
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.service_kit_item
                .Where(ss => ss.item_id == id)
                .ProjectTo<ServiceKitItemViewModel>()
                .FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceKitItemViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ServiceKitItemViewModel itemAdd = (ServiceKitItemViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты услуги");
                return null;
            }

            service_kit_item service_kit_item_existing = dbContext.service_kit_item.Where(ss => ss.service_kit_id == itemAdd.service_id && ss.service_id == itemAdd.service_id).FirstOrDefault();
            if (service_kit_item_existing != null)
            {
                modelState.AddModelError("", "Уже есть в наборе такая услуга");
                return null;
            }

            service_kit_item service_kit_item = new service_kit_item();
            ModelMapper.Map<ServiceKitItemViewModel, service_kit_item>(itemAdd, service_kit_item);

            dbContext.service_kit_item.Add(service_kit_item);
            dbContext.SaveChanges();

            return GetItem(service_kit_item.item_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceKitItemViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ServiceKitItemViewModel itemEdit = (ServiceKitItemViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты услуги");
                return null;
            }

            service_kit_item service_kit_item = dbContext.service_kit_item.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (service_kit_item == null)
            {
                modelState.AddModelError("", "Не найден элемент набора с кодом " + itemEdit.item_id);
                return null;
            }
            service_kit_item service_kit_item_existing = dbContext.service_kit_item.Where(ss => ss.item_id != itemEdit.item_id && ss.service_kit_id == itemEdit.service_id && ss.service_id == itemEdit.service_id).FirstOrDefault();
            if (service_kit_item_existing != null)
            {
                modelState.AddModelError("", "Уже есть в наборе такая услуга");
                return null;
            }

            ServiceKitItemViewModel oldModel = new ServiceKitItemViewModel();
            ModelMapper.Map<service_kit_item, ServiceKitItemViewModel>(service_kit_item, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<ServiceKitItemViewModel, service_kit_item>(itemEdit, service_kit_item);
            dbContext.SaveChanges();

            return itemEdit;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceKitItemViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            ServiceKitItemViewModel itemDel = (ServiceKitItemViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты услуги");
                return false;
            }

            service_kit_item service_kit_item = dbContext.service_kit_item.Where(ss => ss.item_id == itemDel.item_id).FirstOrDefault();
            if (service_kit_item == null)
            {
                modelState.AddModelError("", "Не найден элемент набора с кодом " + itemDel.item_id);
                return false;
            }
            
            dbContext.service_kit_item.Remove(service_kit_item);

            dbContext.SaveChanges();

            return true;
        }

    }
}
