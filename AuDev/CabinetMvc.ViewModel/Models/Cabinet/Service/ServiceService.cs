﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IServiceService : IAuBaseService
    {
        List<ServiceViewModel> GetList_notInKit(int service_kit_id);
        List<ServiceViewModel> GetList_inKit(int service_kit_id);
    }

    public class ServiceService : AuBaseService, IServiceService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_service
                .Select(ss => new ServiceViewModel()
                {
                    id = ss.id,
                    guid = ss.guid,
                    name = ss.name,
                    description = ss.description,
                    parent_id = ss.parent_id,
                    is_deleted = ss.is_deleted == 1,
                    exe_name = ss.exe_name,
                    is_site = ss.is_site == 1,                    
                    price = ss.price,
                    need_key = ss.need_key == 1,
                    NeedCheckVersionLicense = ss.NeedCheckVersionLicense == 1,
                    have_spec = ss.have_spec,
                    max_workplace_cnt = ss.max_workplace_cnt,
                    is_service = ss.is_service,
                    priority = ss.priority,
                    group_id = ss.group_id,
                    group_name = ss.group_name,
                });
                //.ProjectTo<ServiceViewModel>();
        }

        public List<ServiceViewModel> GetList_notInKit(int service_kit_id)
        {
            if (service_kit_id > 0)
            {
                return dbContext.vw_service
                    .Where(ss => !dbContext.service_kit_item.Where(tt => tt.service_kit_id == service_kit_id && ss.id == tt.service_kit_id).Any())
                    .Select(ss => new ServiceViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        name = ss.name,
                        description = ss.description,
                        parent_id = ss.parent_id,
                        is_deleted = ss.is_deleted == 1,
                        exe_name = ss.exe_name,
                        is_site = ss.is_site == 1,
                        price = ss.price,
                        need_key = ss.need_key == 1,
                        NeedCheckVersionLicense = ss.NeedCheckVersionLicense == 1,
                        have_spec = ss.have_spec,
                        max_workplace_cnt = ss.max_workplace_cnt,
                        is_service = ss.is_service,
                        priority = ss.priority,
                        group_id = ss.group_id,
                        group_name = ss.group_name,
                    })
                    .ToList();
            }
            else
            {
                return dbContext.vw_service                    
                    .Select(ss => new ServiceViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        name = ss.name,
                        description = ss.description,
                        parent_id = ss.parent_id,
                        is_deleted = ss.is_deleted == 1,
                        exe_name = ss.exe_name,
                        is_site = ss.is_site == 1,
                        price = ss.price,
                        need_key = ss.need_key == 1,
                        NeedCheckVersionLicense = ss.NeedCheckVersionLicense == 1,
                        have_spec = ss.have_spec,
                        max_workplace_cnt = ss.max_workplace_cnt,
                        is_service = ss.is_service,
                        priority = ss.priority,
                        group_id = ss.group_id,
                        group_name = ss.group_name,
                    })
                    .ToList();
            }

        }

        public List<ServiceViewModel> GetList_inKit(int service_kit_id)
        {
            if (service_kit_id > 0)
            {
                return (from t1 in dbContext.vw_service
                        from t2 in dbContext.service_kit_item
                        where t1.id == t2.service_id
                        && t2.service_kit_id == service_kit_id
                        select new ServiceViewModel()
                     {
                         id = t1.id,
                         guid = t1.guid,
                         name = t1.name,
                         description = t1.description,
                         parent_id = t1.parent_id,
                         is_deleted = t1.is_deleted == 1,
                         exe_name = t1.exe_name,
                         is_site = t1.is_site == 1,
                         price = t1.price,
                         need_key = t1.need_key == 1,
                         NeedCheckVersionLicense = t1.NeedCheckVersionLicense == 1,
                         have_spec = t1.have_spec,
                         max_workplace_cnt = t1.max_workplace_cnt,
                         is_service = t1.is_service,
                         priority = t1.priority,
                         group_id = t1.group_id,
                         group_name = t1.group_name,
                     })
                    .ToList();
            }
            else
            {
                return new List<ServiceViewModel>();
            }

        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ServiceViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ServiceViewModel itemEdit = (ServiceViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры услуги");
                return null;
            }

            vw_service vw_service = dbContext.vw_service.Where(ss => ss.id == itemEdit.id).FirstOrDefault();
            if (vw_service == null)
            {
                modelState.AddModelError("", "Не найдены параметры услуги");
                return null;
            }            

            modelBeforeChanges = new ServiceViewModel(vw_service);

            service service = dbContext.service.Where(ss => ss.id == itemEdit.id).FirstOrDefault();
            service.description = itemEdit.description;
            service.name = itemEdit.name;
            service.priority = itemEdit.priority;
            if (service.max_workplace_cnt != itemEdit.max_workplace_cnt)
            {
                var clientServiceParams = dbContext.client_service_param.Where(ss => ss.service_id == itemEdit.id).ToList();
                if (clientServiceParams != null)
                {
                    foreach (var clientServiceParam in clientServiceParams)
                    {
                        clientServiceParam.max_workplace_cnt = itemEdit.max_workplace_cnt;
                    }
                }
            }
            service.max_workplace_cnt = itemEdit.max_workplace_cnt;
            dbContext.SaveChanges();

            vw_service = dbContext.vw_service.AsNoTracking().Where(ss => ss.id == itemEdit.id).FirstOrDefault();
            return new ServiceViewModel(vw_service);
        }

    }
}
