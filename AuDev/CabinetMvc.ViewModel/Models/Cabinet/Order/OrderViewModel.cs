﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class OrderViewModel : AuBaseViewModel
    {

        public OrderViewModel()
            : base(Enums.MainObjectType.CAB_ORDER)
        {
            //
        }

        public OrderViewModel(order item)
            : base(Enums.MainObjectType.CAB_ORDER)
        {
            id = item.id;
            guid = item.guid;
            client_id = item.client_id;            
            created_on = item.created_on;
            created_by = item.created_by;
            updated_on = item.updated_on;
            updated_by = item.updated_by;
            is_deleted = item.is_deleted;
            commentary = item.commentary;
            commentary_manager = item.commentary_manager;
            order_status = item.order_status;
            is_conformed = item.is_conformed;
            active_status = item.active_status;
            order_name = item.order_name;
            active_status_str = item.active_status == 1 ? "Не активирована" : (item.active_status == 2 ? "Частично активирована" : "Полностью активирована");
            client_name = (item.client == null ? "" : item.client.full_name);
            is_conformed_str = item.is_conformed == 1 ? "Да" : "Нет";
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "GUID")]
        public string guid { get; set; }

        [Display(Name = "Клиент")]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "Когда создана")]
        public System.DateTime created_on { get; set; }

        [Display(Name = "Кем создана")]
        public string created_by { get; set; }

        [Display(Name = "Когда изменена")]
        public Nullable<System.DateTime> updated_on { get; set; }

        [Display(Name = "Кем изменена")]
        public string updated_by { get; set; }

        [Display(Name = "is_deleted")]
        public Nullable<short> is_deleted { get; set; }

        [Display(Name = "Комментарий")]
        public string commentary { get; set; }

        [Display(Name = "Комментарий сотрудника")]
        public string commentary_manager { get; set; }

        //order_status: 1, 2
        //2 - 'Автоматически созданная заявка'
        [Display(Name = "Статус")]
        public Nullable<int> order_status { get; set; }

        [Display(Name = "Подтверждена")]
        public Nullable<short> is_conformed { get; set; }

        [Display(Name = "Наименование")]
        public string order_name { get; set; }

        // active_status: 1, 2, 3
        // 1 - 'Не активирована'
        // 2 - 'Частично активирована'
        // 3 - 'Полностью активирована'
        [Display(Name = "Активация")]
        public Nullable<int> active_status { get; set; }

        [Display(Name = "Активация")]
        public string active_status_str { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Согласована")]
        public string is_conformed_str { get; set; }

        [JsonIgnore]
        public IEnumerable<ServiceViewModel> ServiceList { get; set; }

        [Display(Name = "NeedActivate")]
        public bool NeedActivate { get; set; }
    }
}