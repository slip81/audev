﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class QuantityViewModel
    {
        [Display(Name = "QuantityId")]
        public int QuantityId { get; set; }

        [Display(Name = "QuantityName")]
        public string QuantityName { get; set; }

    }
   
}