﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IOrderService : IAuBaseService
    {
        AuBaseViewModel Confirm(int order_id, ModelStateDictionary modelState);
        AuBaseViewModel Activate(IEnumerable<OrderItemViewModel> order_data, string commentary, string commentary_manager, ModelStateDictionary modelState);
        IQueryable<OrderViewModel> GetList_Active(int client_id);
        IQueryable<OrderViewModel> GetList_Unactive(int client_id);
    }

    public class OrderService : AuBaseService, IOrderService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.order.Where(ss => ss.is_deleted != 1)                
                    .Select(ss => new OrderViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        client_id = ss.client_id,                        
                        created_on = ss.created_on,
                        created_by = ss.created_by,
                        updated_on = ss.updated_on,
                        updated_by = ss.updated_by,
                        is_deleted = ss.is_deleted,
                        commentary = ss.commentary,
                        commentary_manager = ss.commentary_manager,
                        order_status = ss.order_status,
                        is_conformed = ss.is_conformed,
                        active_status = ss.active_status,
                        order_name = ss.order_name,
                        active_status_str = ss.active_status == 1 ? "Не активирована" : (ss.active_status == 2 ? "Частично активирована" : "Полностью активирована"),
                        client_name = ss.client.full_name,
                        is_conformed_str = ss.is_conformed == 1 ? "Да" : "Нет",
                    });
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.order.Where(ss => ss.client_id == parent_id && ss.is_deleted != 1)
                    .Select(ss => new OrderViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        client_id = ss.client_id,                        
                        created_on = ss.created_on,
                        created_by = ss.created_by,
                        updated_on = ss.updated_on,
                        updated_by = ss.updated_by,
                        is_deleted = ss.is_deleted,
                        commentary = ss.commentary,
                        commentary_manager = ss.commentary_manager,
                        order_status = ss.order_status,
                        is_conformed = ss.is_conformed,
                        active_status = ss.active_status,
                        order_name = ss.order_name,
                        active_status_str = ss.active_status == 1 ? "Не активирована" : (ss.active_status == 2 ? "Частично активирована" : "Полностью активирована"),
                        client_name = ss.client.full_name,
                        is_conformed_str = ss.is_conformed == 1 ? "Да" : "Нет",
                    });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.order.Where(ss => ss.id == id && ss.is_deleted != 1)
                    .Select(ss => new OrderViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        client_id = ss.client_id,                        
                        created_on = ss.created_on,
                        created_by = ss.created_by,
                        updated_on = ss.updated_on,
                        updated_by = ss.updated_by,
                        is_deleted = ss.is_deleted,
                        commentary = ss.commentary,
                        commentary_manager = ss.commentary_manager,
                        order_status = ss.order_status,
                        is_conformed = ss.is_conformed,
                        active_status = ss.active_status,
                        order_name = ss.order_name,
                        active_status_str = ss.active_status == 1 ? "Не активирована" : (ss.active_status == 2 ? "Частично активирована" : "Полностью активирована"),
                    })
                    .FirstOrDefault();
        }

        public IQueryable<OrderViewModel> GetList_Active(int client_id)
        {
            return dbContext.order.Where(ss => ss.client_id == client_id && ss.is_deleted != 1 && ((ss.active_status == 1) || (ss.active_status == 2)))
                    .Select(ss => new OrderViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        client_id = ss.client_id,                        
                        created_on = ss.created_on,
                        created_by = ss.created_by,
                        updated_on = ss.updated_on,
                        updated_by = ss.updated_by,
                        is_deleted = ss.is_deleted,
                        commentary = ss.commentary,
                        commentary_manager = ss.commentary_manager,
                        order_status = ss.order_status,
                        is_conformed = ss.is_conformed,
                        active_status = ss.active_status,
                        order_name = ss.order_name,
                        active_status_str = ss.active_status == 1 ? "Не активирована" : (ss.active_status == 2 ? "Частично активирована" : "Полностью активирована"),
                        client_name = ss.client.full_name,
                        is_conformed_str = ss.is_conformed == 1 ? "Да" : "Нет",
                    });
        }

        public IQueryable<OrderViewModel> GetList_Unactive(int client_id)
        {
            return dbContext.order.Where(ss => ss.client_id == client_id && ss.is_deleted != 1 && ss.active_status == 3)
                    .Select(ss => new OrderViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        client_id = ss.client_id,                        
                        created_on = ss.created_on,
                        created_by = ss.created_by,
                        updated_on = ss.updated_on,
                        updated_by = ss.updated_by,
                        is_deleted = ss.is_deleted,
                        commentary = ss.commentary,
                        commentary_manager = ss.commentary_manager,
                        order_status = ss.order_status,
                        is_conformed = ss.is_conformed,
                        active_status = ss.active_status,
                        order_name = ss.order_name,
                        active_status_str = ss.active_status == 1 ? "Не активирована" : (ss.active_status == 2 ? "Частично активирована" : "Полностью активирована"),
                        client_name = ss.client.full_name,
                        is_conformed_str = ss.is_conformed == 1 ? "Да" : "Нет",
                    });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is OrderViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            OrderViewModel orderAdd = (OrderViewModel)item;
            if (orderAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты заявки");
                return null;
            }

            if (orderAdd.ServiceList == null)
            {
                modelState.AddModelError("", "Не заданы услуги");
                return null;
            }

            var orderedServices = orderAdd.ServiceList.Where(ss => ss.OrderedLicenseCount > 0);
            if ((orderedServices == null) || (orderedServices.Count() <= 0))
            {
                modelState.AddModelError("", "Не заданы услуги");
                return null;
            }

            order order = new order();
            order.is_deleted = 0;
            //order.is_conformed = 0;
            order.is_conformed = 1;
            order.guid = Guid.NewGuid().ToString();
            order.active_status = 1;            
            order.client_id = orderAdd.client_id;
            order.commentary = orderAdd.commentary;
            order.commentary_manager = orderAdd.commentary_manager;
            order.created_by = currUser.UserName;
            order.created_on = DateTime.Now;
            order.order_status = 1;
            dbContext.order.Add(order);

            string order_name = "";
            bool isFirst = true;
            int cnt = 0;
            List<int> ordered_service_id_list = new List<int>();
            var service_list = dbContext.service.Where(ss => ss.is_deleted != 1 && ss.parent_id.HasValue).ToList();
            foreach (var serv in orderedServices)
            {
                order_item order_item = new order_item();
                order_item.guid = Guid.NewGuid().ToString();
                order_item.is_deleted = 0;
                order_item.order = order;
                order_item.quantity = serv.OrderedLicenseCount;
                order_item.service_id = serv.id;                
                int version_id = serv.OrderedVersionId.GetValueOrDefault(0);
                if (version_id <= 0)
                {
                    version_id = dbContext.version.Where(ss => ss.service_id == serv.id).OrderByDescending(ss => ss.id).Select(ss => ss.id).FirstOrDefault();
                }
                order_item.version_id = version_id;                
                
                dbContext.order_item.Add(order_item);

                if (cnt < 3)
                {
                    if (!isFirst)
                        order_name += ", ";
                    order_name += service_list.Where(ss => ss.id == order_item.service_id).Select(ss => ss.name).FirstOrDefault();
                }

                if (ordered_service_id_list.Where(ss => ss == (int)order_item.service_id).Count() <= 0)
                {
                    var service_cnt = dbContext.client_service.Where(ss => ss.client_id == orderAdd.client_id && ss.service_id == order_item.service_id).Count();
                    if (service_cnt <= 0)
                    {
                        client_service client_service = new client_service();
                        client_service.client_id = (int)orderAdd.client_id;
                        client_service.service_id = (int)order_item.service_id;
                        client_service.state = (int)Enums.ClientServiceStateEnum.ORDERED;
                        dbContext.client_service.Add(client_service);
                        ordered_service_id_list.Add((int)order_item.service_id);
                    }
                }

                isFirst = false;
                cnt++;
            }

            if (cnt > 3)
                order_name += " и еще " + (cnt - 3).ToString();
            order.order_name = order_name;

            dbContext.SaveChanges();

            int id = order.id;
            var order_db = dbContext.order.Where(ss => ss.id == id).FirstOrDefault();
            OrderViewModel res = new OrderViewModel(order_db);
            
            if (orderAdd.NeedActivate)
            {
                var order_items = dbContext.order_item.Where(ss => ss.order_id == id && ss.quantity.HasValue && ss.is_deleted != 1)
                    .Select(ss => new OrderItemViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        service_id = ss.service_id,
                        service_name = ss.service != null ? ss.service.name : "",
                        version_id = ss.version_id,
                        version_name = ss.version != null ? ss.version.name : "",
                        order_id = ss.order_id,
                        quantity = ss.quantity,
                        is_deleted = ss.is_deleted,
                        additional_data = ss.additional_data,
                        QuantityForActivation = ss.quantity,
                        QuantityForActivationVM = new QuantityViewModel() { QuantityId = (int)ss.quantity, QuantityName = "0", },
                        //}).AsEnumerable();
                    }).ToList();
            return Activate(order_items, res.commentary, res.commentary_manager, modelState);
            }
            else
            {
                return res;
            }
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is OrderViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            OrderViewModel orderEdit = (OrderViewModel)item;
            if (orderEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты заявки");
                return null;
            }

            order order = dbContext.order.Where(ss => ss.id == orderEdit.id && ss.is_deleted != 1).FirstOrDefault();
            if (order == null)
            {
                modelState.AddModelError("", "Не найдена заявка с кодом " + orderEdit.id);
                return null;
            }
            modelBeforeChanges = new OrderViewModel(order);

            order.commentary = orderEdit.commentary;
            order.commentary_manager = orderEdit.commentary_manager;
            order.updated_by = currUser.UserName;
            order.updated_on = DateTime.Now;

            dbContext.SaveChanges();

            var order_db = dbContext.order.Where(ss => ss.id == order.id).FirstOrDefault();
            return new OrderViewModel(order_db);
        }

        public AuBaseViewModel Confirm(int order_id, ModelStateDictionary modelState)
        {
            order order = dbContext.order.Where(ss => ss.id == order_id && ss.is_deleted != 1).FirstOrDefault();
            if (order == null)
            {
                modelState.AddModelError("", "Не найдена заявка с кодом " + order_id);
                return null;
            }

            if (order.is_conformed == 1)
            {
                modelState.AddModelError("", "Заявка с кодом " + order_id + " уже согласована");
                return null;
            }

            order.is_conformed = 1;
            order.updated_by = currUser.UserName;
            order.updated_on = DateTime.Now;

            dbContext.SaveChanges();

            var order_db = dbContext.order.Where(ss => ss.id == order.id).FirstOrDefault();
            return new OrderViewModel(order_db);
        }

        public AuBaseViewModel Activate(IEnumerable<OrderItemViewModel> order_data, string commentary, string commentary_manager, ModelStateDictionary modelState)
        {
            int order_id = order_data.FirstOrDefault().order_id.GetValueOrDefault(0);
            order order = dbContext.order.Where(ss => ss.id == order_id && ss.is_deleted != 1).FirstOrDefault();
            if (order == null)
            {
                modelState.AddModelError("", "Не найдена заявка с кодом " + order_id);
                return null;
            }

            if (order.active_status.GetValueOrDefault(0) > 1)
            {
                modelState.AddModelError("", "Заявка с кодом " + order_id + " уже активирована");
                return null;
            }

            DateTime now = DateTime.Now;
            int active_status_new = 1;
            int lic_num = 1;
            foreach (var item in order_data)
            {
                //order_item order_item = context.order_item.Where(ss => ss.id == item.id && item.QuantityForActivation > 0).FirstOrDefault();
                order_item order_item = dbContext.order_item.Where(ss => ss.id == item.id && item.QuantityForActivationVM.QuantityId > 0).FirstOrDefault();
                if (order_item != null)
                {
                    lic_num = 1;
                    while (lic_num <= item.QuantityForActivationVM.QuantityId)
                    {
                        license license = new license();
                        license.created_by = currUser.UserName;
                        license.created_on = now;
                        license.dt_end = item.LicenseDateEnd;
                        license.dt_start = item.LicenseDateBeg;
                        license.guid = Guid.NewGuid().ToString();
                        license.is_deleted = 0;
                        license.order_item_id = item.id;
                        dbContext.license.Add(license);
                        active_status_new = 3;
                        lic_num++;
                    }
                }
            }

            order.active_status = active_status_new;
            order.updated_by = currUser.UserName;
            order.updated_on = DateTime.Now;
            order.commentary = commentary;
            order.commentary_manager = commentary_manager;

            dbContext.SaveChanges();

            var order_db = dbContext.order.Where(ss => ss.id == order.id).FirstOrDefault();
            return new OrderViewModel(order_db);
        }
    }
}
