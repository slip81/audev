﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IOrderItemService : IAuBaseService
    {
        //
    }

    public class OrderItemService : AuBaseService, IOrderItemService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            var list = dbContext.order_item.Where(ss => ss.order_id == parent_id && ss.is_deleted != 1).ToList();
            return list.Select(ss => new OrderItemViewModel()
                    {
                        id = ss.id,
                        guid = ss.guid,
                        service_id = ss.service_id,
                        service_name = ss.service != null ? ss.service.name : "",
                        version_id = ss.version_id,
                        version_name = ss.version != null ? ss.version.name : "",
                        order_id = ss.order_id,
                        quantity = ss.quantity,
                        is_deleted = ss.is_deleted,
                        additional_data = ss.additional_data,
                        QuantityForActivation = ss.quantity,
                        //QuantityForActivationVM = new QuantityViewModel() { QuantityId = 0, QuantityName = "0", },
                        QuantityForActivationVM = new QuantityViewModel() { QuantityId = (int)ss.quantity, QuantityName = ss.quantity.ToString(), },
                    })
                    .AsQueryable();            
        }
    }
}
