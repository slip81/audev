﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class OrderItemViewModel : AuBaseViewModel
    {

        public OrderItemViewModel()
            : base(Enums.MainObjectType.CAB_ORDER_ITEM)
        {
            QuantityForActivationVM = new QuantityViewModel() { QuantityId = 0, QuantityName = "0", };
        }

        public OrderItemViewModel(order_item item)
            : base(Enums.MainObjectType.CAB_ORDER_ITEM)
        {
            id = item.id;
            guid = item.guid;
            service_id = item.service_id;
            service_name = item.service != null ? item.service.name : "";
            version_id = item.version_id;
            version_name = item.version != null ? item.version.name : "";
            order_id = item.order_id;
            quantity = item.quantity;
            is_deleted = item.is_deleted;
            additional_data = item.additional_data;
            QuantityForActivation = item.quantity;
            //QuantityForActivationVM = new QuantityViewModel() { QuantityId = 0, QuantityName = "0", };
            QuantityForActivationVM = new QuantityViewModel() { QuantityId = (int)item.quantity, QuantityName = item.quantity.ToString(), };
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "GUID")]
        public string guid { get; set; }

        [Display(Name = "Услуга")]
        public Nullable<int> service_id { get; set; }

        [Display(Name = "Услуга")]
        public string service_name { get; set; }

        [Display(Name = "Версия")]
        public Nullable<int> version_id { get; set; }

        [Display(Name = "Версия")]
        public string version_name { get; set; }

        [Display(Name = "Код заявки")]
        public Nullable<int> order_id { get; set; }

        [Display(Name = "Количество")]
        public Nullable<int> quantity { get; set; }

        [Display(Name = "is_deleted")]
        public Nullable<short> is_deleted { get; set; }

        [Display(Name = "additional_data")]
        public string additional_data { get; set; }

        [Display(Name = "Количество для активации")]
        [DataType("Integer")]
        [Range(0, int.MaxValue)]
        public Nullable<int> QuantityForActivation { get; set; }

        [UIHint("QuantityEditor")]
        [Display(Name = "Количество для активации")]
        public QuantityViewModel QuantityForActivationVM { get; set; }

        [Display(Name = "Дата начала действия лицензии")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> LicenseDateBeg { get; set; }

        [Display(Name = "Дата окончания действия лицензии")]
        [DataType(DataType.Date)]
        public Nullable<DateTime> LicenseDateEnd { get; set; }
    }
}