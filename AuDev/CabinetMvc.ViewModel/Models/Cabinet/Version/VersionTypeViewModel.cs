﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class VersionTypeViewModel : AuBaseViewModel
    {

        public VersionTypeViewModel()
            : base()
        {
            //prj_version_type
        }

        [Key()]
        [Display(Name = "Код")]
        public int version_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string version_type_name { get; set; }
    }
}