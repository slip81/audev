﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class VersionMainViewModel : AuBaseViewModel
    {

        public VersionMainViewModel()
            : base()
        {
            //vw_version_main
        }

        [Key()]
        [Display(Name = "Код")]
        public int version_main_id { get; set; }

        [Display(Name = "Название")]
        public string version_main_name { get; set; }

        [Display(Name = "Проект")]
        public Nullable<int> project_id { get; set; }

        [Display(Name = "Тип")]
        public int version_type { get; set; }

    }
}