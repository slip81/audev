﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel.Cabinet
{

    public interface IVersionMainService : IAuBaseService
    {
        //
    }

    public class VersionMainService : AuBaseService, IVersionMainService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_version_main
                .ProjectTo<VersionMainViewModel>()
                .OrderByDescending(ss => ss.version_main_id);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_version_main
                .Where(ss => ((!ss.project_id.HasValue) || (ss.project_id == parent_id)))
                .ProjectTo<VersionMainViewModel>()
                ;                
        }
    }
}