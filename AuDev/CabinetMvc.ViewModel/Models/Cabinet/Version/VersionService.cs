﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel.Cabinet
{

    public interface IVersionService : IAuBaseService
    {
        IQueryable<VersionViewModel> GetList_Sprav();
        bool VersionBatchAdd(string version_num, int? group_id, ModelStateDictionary modelState);
        bool VersionBatchDel(string version_num, int? group_id, ModelStateDictionary modelState);
    }

    public class VersionService : AuBaseService, IVersionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.version.Where(ss => ss.is_deleted != 1)
                .ProjectTo<VersionViewModel>()
                .OrderByDescending(ss => ss.id);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.version
                .Where(ss => ss.is_deleted != 1 && ss.service_id == parent_id)
                .ProjectTo<VersionViewModel>()
                .OrderByDescending(ss => ss.id);
        }

        public IQueryable<VersionViewModel> GetList_Sprav()
        {
            return from t1 in dbContext.version
                   from t2 in dbContext.service
                   where t1.service_id == t2.id
                   && t2.is_deleted != 1
                   select new VersionViewModel()
                   {
                       id = t1.id,                       
                       name = t1.name,
                       description = t1.description,
                       service_id = t1.service_id,                       
                   }
                   ;
        }

        public bool VersionBatchAdd(string version_num, int? group_id, ModelStateDictionary modelState)
        {
            try
            {
                if (String.IsNullOrEmpty(version_num))
                {
                    modelState.AddModelError("error", "Не задан номер версии");
                    return false;
                }

                List<vw_service> service_list = dbContext.vw_service
                    .Where(ss => 
                        ss.group_id == group_id
                        && !dbContext.version.Where(tt => tt.is_deleted != 1 && tt.service_id == ss.id && tt.name.Trim().ToLower().Equals(version_num.Trim().ToLower())).Any()
                        )
                    .ToList();
                if (service_list != null)
                {
                    foreach (var service in service_list)
                    {
                        VersionViewModel verVM = new VersionViewModel();
                        verVM.is_deleted = false;
                        verVM.name = version_num.Trim().ToLower();
                        verVM.service_id = service.id;
                        Insert(verVM, modelState);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                string mess = GlobalUtil.ExceptionInfo(ex);
                modelState.AddModelError("", mess);
                //ToLog(mess, (int)Enums.LogEventType.ERROR);
                return false;
            }
        }

        public bool VersionBatchDel(string version_num, int? group_id, ModelStateDictionary modelState)
        {
            try
            {
                if (String.IsNullOrEmpty(version_num))
                {
                    modelState.AddModelError("error", "Не задан номер версии");
                    return false;
                }

                List<version> version_list = (from t1 in dbContext.vw_service
                                              from t2 in dbContext.version
                                              where t1.id == t2.service_id
                                              && t1.group_id == group_id
                                              && t2.is_deleted != 1 && t2.name.Trim().ToLower().Equals(version_num.Trim().ToLower())
                                              select t2)
                                              .ToList();
                if (version_list != null)
                {
                    foreach (var version in version_list)
                    {
                        VersionViewModel verVM = new VersionViewModel();
                        verVM.id = version.id;
                        verVM.service_id = version.service_id;
                        try
                        {
                            Delete(verVM, modelState);
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                string mess = GlobalUtil.ExceptionInfo(ex);
                modelState.AddModelError("", mess);
                //ToLog(mess, (int)Enums.LogEventType.ERROR);
                return false;
            }
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            VersionViewModel itemAdd = (VersionViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты версии");
                return null;
            }

            DateTime now = DateTime.Now;
            
            itemAdd.guid = System.Guid.NewGuid().ToString();
            itemAdd.description = itemAdd.name;

            version version = new version();
            ModelMapper.Map<VersionViewModel, version>(itemAdd, version);

            dbContext.version.Add(version);
            dbContext.SaveChanges();

            VersionViewModel res = new VersionViewModel();
            ModelMapper.Map<version, VersionViewModel>(version, res);

            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            VersionViewModel itemEdit = (VersionViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры версии");
                return null;
            }

            version version = dbContext.version.Where(ss => ss.id == itemEdit.id).FirstOrDefault();
            if (version == null)
            {
                modelState.AddModelError("", "Не найдены параметры версии");
                return null;
            }

            VersionViewModel oldModel = new VersionViewModel();
            ModelMapper.Map<version, VersionViewModel>(version, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<VersionViewModel, version>(itemEdit, version);

            dbContext.SaveChanges();

            VersionViewModel res = new VersionViewModel();
            ModelMapper.Map<version, VersionViewModel>(version, res);

            return res;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            VersionViewModel itemDel = (VersionViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры версии");
                return false;
            }

            version version = dbContext.version.Where(ss => ss.id == itemDel.id).FirstOrDefault();
            if (version == null)
            {
                modelState.AddModelError("", "Не найдены параметры версии");
                return false;
            }


            var existing = dbContext.order_item.Where(ss => ss.version_id == itemDel.id).FirstOrDefault();
            if (existing != null)
            {
                modelState.AddModelError("", "По данной версии существуют заявки");
                return false;
            }

            dbContext.version.Remove(version);
            dbContext.SaveChanges();

            return true;
        }

    }
}
