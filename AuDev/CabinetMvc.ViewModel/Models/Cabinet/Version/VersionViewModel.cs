﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class VersionViewModel : AuBaseViewModel
    {

        public VersionViewModel()
            : base(Enums.MainObjectType.CAB_VERSION)
        {
            //version
        }

        /*
        public int id { get; set; }
        public string guid { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int service_id { get; set; }
        public Nullable<short> is_deleted { get; set; }
        public Nullable<int> file_size { get; set; }
        public Nullable<int> file_path_id { get; set; }
        */

        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "guid")]
        public string guid { get; set; }

        [Display(Name = "Название")]
        public string name { get; set; }

        [Display(Name = "Описание")]
        public string description { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "file_size")]
        public Nullable<int> file_size { get; set; }

        [Display(Name = "file_path_id")]
        public Nullable<int> file_path_id { get; set; }
    }
}