﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel.Cabinet
{

    public interface IVersionTypeService : IAuBaseService
    {
        //
    }

    public class VersionTypeService : AuBaseService, IVersionTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.prj_version_type
                .ProjectTo<VersionTypeViewModel>()
                .OrderBy(ss => ss.version_type_id);
        }
    }
}