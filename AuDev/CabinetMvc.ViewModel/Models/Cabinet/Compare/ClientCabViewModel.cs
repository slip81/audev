﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientCabViewModel : AuBaseViewModel
    {

        public ClientCabViewModel()
            : base(Enums.MainObjectType.COMPARE_CLIENT_CAB)
        {            
            //vw_client_compare_cabinet
        }

        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }
        
        [Display(Name = "Город")]
        public Nullable<int> city_id { get; set; }

        [Display(Name = "Наименование")]
        public string full_name { get; set; }

        [Display(Name = "ИНН")]
        public string inn { get; set; }

        [Display(Name = "КПП")]
        public string kpp { get; set; }

        [Display(Name = "Юр. адрес")]
        public string legal_address { get; set; }

        [Display(Name = "Физ. адрес")]
        public string absolute_address { get; set; }

        [Display(Name = "БИК")]
        public string bik { get; set; }

        [Display(Name = "Телефон")]        
        public string phone { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Контакт")]
        public string contact_person { get; set; }

        [Display(Name = "Индекс")]
        public string zip_code { get; set; }

        [Display(Name = "post")]
        public string post { get; set; }

        [Display(Name = "Код в CRM")]
        public Nullable<int> sm_id { get; set; }

        [Display(Name = "Город")]
        public string city_name { get; set; }

        [Display(Name = "Регион")]
        public string region_name { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> state_id { get; set; }

        [Display(Name = "Статус")]
        public string state_name { get; set; }

        [Display(Name = "Организация (CRM)")]
        public string crm_name_company_full { get; set; }

        [Display(Name = "Регион (CRM)")]
        public string crm_name_region { get; set; }

        [Display(Name = "Город (CRM)")]
        public string crm_name_city { get; set; }

        [Display(Name = "Адрес (CRM)")]
        public string crm_address { get; set; }

        [Display(Name = "Телефон (CRM)")]
        public string crm_phone { get; set; }

        [Display(Name = "Email (CRM)")]
        public string crm_email { get; set; }

        [Display(Name = "ИНН (CRM)")]
        public string crm_inn { get; set; }

        [Display(Name = "КПП (CRM)")]
        public string crm_kpp { get; set; }

        [Display(Name = "Контакт (CRM)")]
        public string crm_face_fio { get; set; }

        [Display(Name = "Адрес (CRM)")]
        public string crm_p_address { get; set; }

        [Display(Name = "UnmateBtn")]
        [JsonIgnore()]
        public string UnmateBtn
        {
            get
            {
                return "<button class='k-button k-button-icontext' title='Убрать стыковку' onclick='compare.onUnmateBtnClick(" + id.ToString() + ")'><span class='k-icon k-delete'></span></button>";
            }
        }

        [Display(Name = "row_class")]
        public string row_class
        {
            get
            {
                return sm_id.HasValue ? "k-success-colored" : "";
            }
            set
            {

            }
        }
    }
}

