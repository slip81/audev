﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientCrmService : IAuBaseService
    {
        IQueryable<ClientCrmViewModel> GetList_byFilter(int group);
        List<SelectListItem> GetStateList();
    }

    public class ClientCrmService : AuBaseService, IClientCrmService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_client_compare_crm
                .ProjectTo<ClientCrmViewModel>();
        }

        public IQueryable<ClientCrmViewModel> GetList_byFilter(int group)
        {
            /*
             group:
              0 - все
              1 - несостыкованные
              2 - состыкованные
            */
            return dbContext.vw_client_compare_crm
                .Where(ss => (
                    (group == 0)
                    // !!!
                    // todo
                    ||
                    (group == 1) && (!dbContext.vw_client_compare_cabinet.Where(tt => tt.sm_id == ss.id_company).Any())
                    ||
                    (group == 2) && (dbContext.vw_client_compare_cabinet.Where(tt => tt.sm_id == ss.id_company).Any()))
                )
                .ProjectTo<ClientCrmViewModel>();
        }

        public List<SelectListItem> GetStateList()
        {
            return dbContext.client_state.AsEnumerable()
                .Select(ss => new SelectListItem()
            {
                Text = ss.state_name,
                Value = ss.state_id.ToString(),
            })
            .OrderBy(ss => ss.Text)
            .ToList();
        }
    }
}
