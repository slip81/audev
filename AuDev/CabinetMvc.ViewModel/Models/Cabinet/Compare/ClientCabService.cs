﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientCabService : IAuBaseService
    {
        IQueryable<ClientCabViewModel> GetList_byFilter(int group);
        bool UpdateCabinetClientFromCrmClient(IEnumerable<ClientCabViewModel> items);
    }

    public class ClientCabService : AuBaseService, IClientCabService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_client_compare_cabinet
                .ProjectTo<ClientCabViewModel>();
        }
        
        public IQueryable<ClientCabViewModel> GetList_byFilter(int group)
        {
            /*
             group:
              0 - все
              1 - несостыкованные
              2 - состыкованные
            */
            return dbContext.vw_client_compare_cabinet
                .Where(ss => (
                    (group == 0)
                    ||
                    (group == 1) && (!ss.sm_id.HasValue)
                    ||
                    (group == 2) && (ss.sm_id.HasValue)
                    )
                )
                .ProjectTo<ClientCabViewModel>();
        }

        public bool UpdateCabinetClientFromCrmClient(IEnumerable<ClientCabViewModel> items)
        {
            try
            {
                if (items == null)
                    return true;
                int res = 0;
                foreach (var item in items)
                {
                    string cabinet_client_id = item.id.ToString();
                    string crm_client_id = item.sm_id.HasValue ? item.sm_id.ToString() : "null";
                    res = dbContext.Database.SqlQuery<int>("select * from cabinet.update_cabinet_client_from_crm_client(" + cabinet_client_id + ", " + crm_client_id + ")").FirstOrDefault();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
