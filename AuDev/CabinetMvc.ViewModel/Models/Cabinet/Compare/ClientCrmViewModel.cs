﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientCrmViewModel : AuBaseViewModel
    {

        public ClientCrmViewModel()
            : base(Enums.MainObjectType.COMPARE_CLIENT_CRM)
        {            
            //vw_client_compare_crm
        }

        [Key()]
        [Display(Name = "Код")]
        public int id_company { get; set; }

        [Display(Name = "Наименование")]
        public string name_company_full { get; set; }

        [Display(Name = "Регион")]
        public string name_region { get; set; }

        [Display(Name = "Город")]
        public string name_city { get; set; }

        [Display(Name = "Адрес")]
        public string address { get; set; }

        [Display(Name = "Телефон")]
        public string phone { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "ИНН")]
        public string inn { get; set; }

        [Display(Name = "КПП")]
        public string kpp { get; set; }

        [Display(Name = "Контакт")]
        public string face_fio { get; set; }

        [Display(Name = "Почтовый адрес")]
        public string p_address { get; set; }

        [Display(Name = "id_client")]
        public Nullable<int> id_client { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> id_state { get; set; }

        [Display(Name = "Статус")]
        public string name_state { get; set; }

        [Display(Name = "cabinet_client_id")]
        public Nullable<int> cabinet_client_id { get; set; }

        [Display(Name = "CreateBtn")]
        [JsonIgnore()]
        public string CreateBtn
        {
            get
            {
                //return "<button class='k-button k-button-icontext' title='Убрать стыковку'><span class='k-icon k-delete'></span></button>";
                //return "<button class='k-button k-button-icontext' title='Создать клиента' onclick='compare.onCreateBtnClick(" + id_company.ToString() + ")'><span class='k-icon k-plus'></span></button>";
                //return "<span class='k-icon k-delete'></span>";
                return "";
            }
        }

        [Display(Name = "row_class")]
        public string row_class {
            get
            {
                return cabinet_client_id.HasValue ? "k-success-colored" : "";
            }
            set
            {

            }
        }
    }
}

