﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SalesViewModel : AuBaseViewModel
    {

        public SalesViewModel()
            : base(Enums.MainObjectType.CAB_SALES)
        {
            //
            //vw_sales
        }

        [Key()]
        [Display(Name = "Код")]
        public int sales_id { get; set; }

        [Display(Name = "Адрес")]
        public string name { get; set; }

        [Display(Name = "Клиент")]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "Город")]
        public Nullable<int> city_id { get; set; }

        [Display(Name = "Телефон")]
        public string cell { get; set; }

        [Display(Name = "Контактное лицо")]
        public string contact_person { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Не задан адрес")]
        public string adress { get; set; }

        [Display(Name = "is_deleted")]
        public Nullable<short> is_deleted { get; set; }

        [Display(Name = "Код SM_ID")]
        public Nullable<int> sm_id { get; set; }

        [Display(Name = "Электронная почта")]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [Display(Name = "user_id")]
        public Nullable<int> user_id { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Город")]
        public string city_name { get; set; }

        [Display(Name = "Регион")]
        public Nullable<int> region_id { get; set; }

        [Display(Name = "Регион")]
        public string region_name { get; set; }

        [Display(Name = "Основной логин")]
        public string user_name { get; set; }

        [Display(Name = "RegionName_Selected")]
        [Required(ErrorMessage = "Не задан регион")]
        public string RegionName_Selected { get; set; }

        [Display(Name = "CityName_Selected")]
        [Required(ErrorMessage = "Не задан город")]
        public string CityName_Selected { get; set; }

        [Display(Name = "RegionName_Changed")]
        public bool RegionName_Changed { get; set; }

        [Display(Name = "CityName_Changed")]
        public bool CityName_Changed { get; set; }

        [Display(Name = "Зона региона")]
        public Nullable<int> region_zone_id { get; set; }

        [Display(Name = "Система налогообложения")]
        public Nullable<int> tax_system_type_id { get; set; }
    }
}