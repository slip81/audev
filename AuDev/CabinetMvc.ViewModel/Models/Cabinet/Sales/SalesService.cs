﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISalesService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_Sprav(long? client_id);
        IQueryable<SalesViewModel> GetList_Cva(long? client_id);
        IQueryable<SalesViewModel> GetList_Stock(long? client_id);
        IQueryable<SalesViewModel> GetList_Asna(int? client_id);
        Tuple<bool, int?, int?> Find(int client_id, string workplace, string key);
    }

    public class SalesService : AuBaseService, ISalesService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_sales.Where(ss => ss.client_id == parent_id)
                .ProjectTo<SalesViewModel>();

            /*
            return (from ss in dbContext.sales
                    from u in dbContext.my_aspnet_users
                      where ss.user_id == u.id
                      && ss.client_id == parent_id 
                      && ss.is_deleted != 1
                      select new SalesViewModel()
                      {
                        id = ss.id,
                        name = ss.name,
                        guid = ss.guid,
                        client_id = ss.client_id,
                        city_id = ss.city_id,
                        cell = ss.cell,
                        contact_person = ss.contact_person,
                        work_mode = ss.work_mode,
                        adress = ss.adress,
                        is_deleted = ss.is_deleted == 1,
                        sm_id = ss.sm_id,
                        shedule_id = ss.shedule_id,
                        district_id = ss.district_id,
                        street_id = ss.street_id,
                        pharmacy_type_id = ss.pharmacy_type_id,
                        email = ss.email,
                        site = ss.site,
                        location_map = ss.location_map,
                        is_published = ss.is_published == 1,
                        image_path = ss.image_path,
                        map_code = ss.map_code,
                        map_image_path = ss.map_image_path,
                        hardware_id = ss.hardware_id,
                        prep_limit = ss.prep_limit,
                        sales_role = ss.sales_role,
                        is_in_summary_order = ss.is_in_summary_order == 1,
                        summary_order_settings_id = ss.summary_order_settings_id,
                        user_id = ss.user_id,
                        city_name = ss.city != null ? ss.city.name : "",
                        region_name = (ss.city != null && ss.city.region != null) ? ss.city.region.name : "",                    
                        user_name = u.name,
                      });
            */
    
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_sales.Where(ss => ss.sales_id == id)
                .ProjectTo<SalesViewModel>()
                .FirstOrDefault();
            /*
            return (from ss in dbContext.sales
                    from u in dbContext.my_aspnet_users
                    where ss.user_id == u.id
                    && ss.id == id
                    && ss.is_deleted != 1
                    select new SalesViewModel()
                    {
                        id = ss.id,
                        name = ss.name,
                        guid = ss.guid,
                        client_id = ss.client_id,
                        city_id = ss.city_id,
                        cell = ss.cell,
                        contact_person = ss.contact_person,
                        work_mode = ss.work_mode,
                        adress = ss.adress,
                        is_deleted = ss.is_deleted == 1,
                        sm_id = ss.sm_id,
                        shedule_id = ss.shedule_id,
                        district_id = ss.district_id,
                        street_id = ss.street_id,
                        pharmacy_type_id = ss.pharmacy_type_id,
                        email = ss.email,
                        site = ss.site,
                        location_map = ss.location_map,
                        is_published = ss.is_published == 1,
                        image_path = ss.image_path,
                        map_code = ss.map_code,
                        map_image_path = ss.map_image_path,
                        hardware_id = ss.hardware_id,
                        prep_limit = ss.prep_limit,
                        sales_role = ss.sales_role,
                        is_in_summary_order = ss.is_in_summary_order == 1,
                        summary_order_settings_id = ss.summary_order_settings_id,
                        user_id = ss.user_id,
                        city_name = ss.city != null ? ss.city.name : "",
                        region_name = (ss.city != null && ss.city.region != null) ? ss.city.region.name : "",
                        user_name = u.name,
                        UserPwd = "***",
                        UserPwdConfirm = "***",
                    })
                    .FirstOrDefault();
            */
        }

        public IQueryable<AuBaseViewModel> GetList_Sprav(long? client_id)
        {
            return (from ss in dbContext.vw_sales
                    where ss.client_id == client_id                    
                    select new SalesViewModel()
                    {
                        sales_id = ss.sales_id,
                        adress = ss.adress,
                    });
        }

        public IQueryable<SalesViewModel> GetList_Cva(long? client_id)
        {
            return (from ss in dbContext.vw_sales
                    from t2 in dbContext.workplace
                    from t3 in dbContext.service_cva
                    where ss.client_id == client_id
                    && ss.sales_id == t2.sales_id
                    && t2.id == t3.workplace_id                    
                    select new SalesViewModel()
                    {
                        sales_id = ss.sales_id,
                        adress = ss.adress,
                    })
                    .Distinct();
        }

        public IQueryable<SalesViewModel> GetList_Stock(long? client_id)
        {
            return (from ss in dbContext.vw_sales
                    from t2 in dbContext.workplace
                    from t3 in dbContext.service_stock
                    where ss.client_id == client_id
                    && ss.sales_id == t2.sales_id
                    && t2.id == t3.workplace_id
                    select new SalesViewModel()
                    {
                        client_id = ss.client_id,
                        sales_id = ss.sales_id,
                        adress = ss.adress,
                    })
                    .Distinct();
        }

        public IQueryable<SalesViewModel> GetList_Asna(int? client_id)
        {
            return dbContext.vw_asna_user
                .Where(ss => ss.client_id == client_id && !ss.is_deleted)
                .Select(ss => new SalesViewModel()
                {
                    client_id = ss.client_id,
                    sales_id = ss.sales_id,
                    adress = ss.sales_name,
                });
        }

        public Tuple<bool, int?, int?> Find(int client_id, string workplace, string key)
        {
            if ((String.IsNullOrEmpty(workplace)) && (String.IsNullOrEmpty(key)))
                return new Tuple<bool, int?, int?>(false, null, null);

            bool workplace_defined = !String.IsNullOrEmpty(workplace);
            bool key_defined = !String.IsNullOrEmpty(key);

            int? sales_id1 = null;
            int? workplace_id1 = null;
            int? sales_id2 = null;
            int? workplace_id2 = null;

            if (workplace_defined)
            {
                vw_workplace wp = dbContext.vw_workplace
                    .Where(ss => ss.client_id == client_id && ss.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower()))
                    .FirstOrDefault();
                if (wp != null)
                {
                    sales_id1 = wp.sales_id;
                    workplace_id1 = wp.workplace_id;
                }
            }            

            if ((workplace_defined) && (workplace_id1.GetValueOrDefault(0) <= 0))
                return new Tuple<bool, int?, int?>(false, null, null);

            if (key_defined)
            {
                vw_client_service cs = dbContext.vw_client_service
                    .Where(ss => //((workplace_defined && ss.workplace_registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())) || (!workplace_defined))
                        (ss.client_id == client_id)
                        && ((workplace_id1 != null && ss.workplace_id == workplace_id1) || (workplace_id1 == null))
                        && ((key_defined && ss.activation_key != null && ss.activation_key.Trim().ToLower().Equals(key.Trim().ToLower())) || (!key_defined))
                    )
                    .OrderBy(ss => ss.sales_id)
                    .FirstOrDefault();
                if (cs != null)
                {
                    sales_id2 = cs.sales_id;
                    workplace_id2 = cs.workplace_id;
                }
            }

            if ((key_defined) && (workplace_id2.GetValueOrDefault(0) <= 0))
                return new Tuple<bool, int?, int?>(false, null, null);

            sales_id2 = sales_id2.GetValueOrDefault(0) <= 0 ? sales_id1 : sales_id2;
            workplace_id2 = workplace_id2.GetValueOrDefault(0) <= 0 ? workplace_id1 : workplace_id2;

            return new Tuple<bool, int?, int?>(true, sales_id2, workplace_id2);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SalesViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            SalesViewModel salesAdd = (SalesViewModel)item;
            if (salesAdd == null)              
            {
                modelState.AddModelError("", "Не заданы атрибуты точки");
                return null;
            }

            client client = dbContext.client.Where(ss => ss.id == salesAdd.client_id).FirstOrDefault();
            if (client == null)
            {
                modelState.AddModelError("", "Не найден клиент с кодом " + salesAdd.client_id.ToString());
                return null;
            }

            DateTime now = DateTime.Now;

            /*
            my_aspnet_users user_existing = dbContext.my_aspnet_users.Where(ss => ss.name == salesAdd.user_name).FirstOrDefault();
            if (user_existing != null)
            {
                modelState.AddModelError("", "Уже существует такой логин");
                return null;
            }

            
            my_aspnet_users user = new my_aspnet_users();
            user.applicationId = 1;
            user.isAnonymous = 0;
            user.lastActivityDate = now;
            user.name = salesAdd.user_name;

            dbContext.my_aspnet_users.Add(user);
            // т.к. нету FK между my_aspnet_membership и my_aspnet_users
            dbContext.SaveChanges();

            EncrHelper2 encr = new EncrHelper2();
            my_aspnet_membership member = new my_aspnet_membership();
            member.userId = user.id;
            member.Email = salesAdd.email;
            member.Password = encr._SetHashValue(salesAdd.UserPwd);
            member.PasswordKey = member.Password;
            member.PasswordFormat = 2;
            member.IsApproved = 1;

            dbContext.my_aspnet_membership.Add(member);
            */

            int curr_region_id = 0;
            int curr_city_id = 0;
            region region = null;
            city city = null;
            if (salesAdd.RegionName_Changed)
            {
                region = dbContext.region.Where(ss => ss.name.Trim().ToLower().Equals(salesAdd.RegionName_Selected.Trim().ToLower())).FirstOrDefault();
                if (region == null)
                {
                    region = new region();
                    region.guid = Guid.NewGuid().ToString();
                    region.name = salesAdd.RegionName_Selected;
                    region.is_deleted = 0;
                    dbContext.region.Add(region);
                    dbContext.SaveChanges();
                    curr_region_id = region.id;
                }
                curr_region_id = region.id;
            }
            else
            {
                curr_region_id = (int)salesAdd.region_id;
            }

            if ((salesAdd.RegionName_Changed) || (salesAdd.CityName_Changed))
            {
                city = dbContext.city.Where(ss => ss.region_id == curr_region_id && ss.name.Trim().ToLower().Equals(salesAdd.CityName_Selected.Trim().ToLower())).FirstOrDefault();
                if (city == null)
                {
                    city = new city();
                    city.guid = Guid.NewGuid().ToString();
                    city.name = salesAdd.CityName_Selected;
                    city.is_deleted = 0;
                    city.region_id = curr_region_id;
                    dbContext.city.Add(city);
                    dbContext.SaveChanges();
                }
                curr_city_id = city.id;
            }
            else
            {
                curr_city_id = (int)salesAdd.city_id;
            }

            salesAdd.name = salesAdd.adress;
            salesAdd.city_id = curr_city_id;
            salesAdd.is_deleted = 0;
            sales sales = new sales();
            ModelMapper.Map<SalesViewModel, sales>(salesAdd, sales);

            /*            
            sales.adress = salesAdd.adress;
            sales.brand = salesAdd.brand;
            sales.cell = salesAdd.cell;
            sales.city_id = salesAdd.city_id;
            sales.client_id = salesAdd.client_id;
            sales.contact_person = salesAdd.contact_person;
            sales.district_id = salesAdd.district_id;
            sales.email = salesAdd.email;
            sales.guid = Guid.NewGuid().ToString();
            sales.hardware_id = salesAdd.hardware_id;
            sales.image_path = salesAdd.image_path;
            sales.is_deleted = 0;
            sales.is_in_summary_order = salesAdd.is_in_summary_order == true ? (short)1 : (short)0;
            sales.is_published = salesAdd.is_published == true ? (short)1 : (short)0;
            sales.location_map = salesAdd.location_map;
            sales.map_code = salesAdd.map_code;
            sales.map_image_path = salesAdd.map_image_path;
            sales.name = salesAdd.name;
            sales.pharmacy_type_id = salesAdd.pharmacy_type_id;
            sales.prep_limit = salesAdd.prep_limit;
            sales.sales_role = salesAdd.sales_role;
            sales.shedule_id = salesAdd.shedule_id;
            sales.site = salesAdd.site;
            sales.sm_id = salesAdd.sm_id;
            sales.street_id = salesAdd.street_id;
            sales.summary_order_settings_id = salesAdd.summary_order_settings_id;
            sales.user_id = user.id;            
            sales.work_mode = salesAdd.work_mode;
            */
            
            dbContext.sales.Add(sales);
            dbContext.SaveChanges();

            vw_sales vw_sales = dbContext.vw_sales.Where(ss => ss.sales_id == sales.id).FirstOrDefault();

            SalesViewModel res = new SalesViewModel();
            ModelMapper.Map<vw_sales, SalesViewModel>(vw_sales, res);
            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SalesViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            SalesViewModel salesEdit = (SalesViewModel)item;
            if (salesEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты точки");
                return null;
            }

            var salesOld = dbContext.sales.Where(ss => ss.id == salesEdit.sales_id).FirstOrDefault();
            if (salesOld == null)
            {
                modelState.AddModelError("", "Не найдена точка с кодом " + salesEdit.sales_id.ToString());
                return null;
            }

            SalesViewModel modelOld = new SalesViewModel();
            ModelMapper.Map<sales, SalesViewModel>(salesOld, modelOld);
            modelBeforeChanges = modelOld;

            int curr_region_id = 0;
            int curr_city_id = 0;
            region region = null;
            city city = null;
            if (salesEdit.RegionName_Changed)
            {
                region = dbContext.region.Where(ss => ss.name.Trim().ToLower().Equals(salesEdit.RegionName_Selected.Trim().ToLower())).FirstOrDefault();
                if (region == null)
                {
                    region = new region();
                    region.guid = Guid.NewGuid().ToString();
                    region.name = salesEdit.RegionName_Selected;
                    region.is_deleted = 0;
                    dbContext.region.Add(region);
                    dbContext.SaveChanges();
                    curr_region_id = region.id;
                }
                curr_region_id = region.id;
            }
            else
            {
                curr_region_id = (int)salesEdit.region_id;
            }

            if ((salesEdit.RegionName_Changed) || (salesEdit.CityName_Changed))
            {
                city = dbContext.city.Where(ss => ss.region_id == curr_region_id && ss.name.Trim().ToLower().Equals(salesEdit.CityName_Selected.Trim().ToLower())).FirstOrDefault();
                if (city == null)
                {
                    city = new city();
                    city.guid = Guid.NewGuid().ToString();
                    city.name = salesEdit.CityName_Selected;
                    city.is_deleted = 0;
                    city.region_id = curr_region_id;
                    dbContext.city.Add(city);
                    dbContext.SaveChanges();
                }
                curr_city_id = city.id;
            }
            else
            {
                curr_city_id = (int)salesEdit.city_id;
            }

            salesEdit.name = salesEdit.adress;
            salesEdit.city_id = curr_city_id;
            salesEdit.is_deleted = 0;

            sales sales = dbContext.sales.Where(ss => ss.id == salesEdit.sales_id).FirstOrDefault();
            ModelMapper.Map<SalesViewModel, sales>(salesEdit, sales);

            /*
            sales.adress = salesEdit.adress;
            sales.brand = salesEdit.brand;
            sales.cell = salesEdit.cell;
            sales.city_id = salesEdit.city_id;
            sales.client_id = salesEdit.client_id;
            sales.contact_person = salesEdit.contact_person;
            sales.district_id = salesEdit.district_id;
            sales.email = salesEdit.email;
            sales.hardware_id = salesEdit.hardware_id;
            sales.image_path = salesEdit.image_path;
            sales.is_in_summary_order = salesEdit.is_in_summary_order == true ? (short)1 : (short)0;
            sales.is_published = salesEdit.is_published == true ? (short)1 : (short)0;
            sales.location_map = salesEdit.location_map;
            sales.map_code = salesEdit.map_code;
            sales.map_image_path = salesEdit.map_image_path;
            sales.name = salesEdit.name;
            sales.pharmacy_type_id = salesEdit.pharmacy_type_id;
            sales.prep_limit = salesEdit.prep_limit;
            sales.sales_role = salesEdit.sales_role;
            sales.shedule_id = salesEdit.shedule_id;
            sales.site = salesEdit.site;
            sales.sm_id = salesEdit.sm_id;
            sales.street_id = salesEdit.street_id;
            sales.summary_order_settings_id = salesEdit.summary_order_settings_id;
            sales.user_id = salesEdit.user_id;
            sales.work_mode = salesEdit.work_mode;
            */

            dbContext.SaveChanges();

            SalesViewModel res = new SalesViewModel();
            vw_sales vw_sales = dbContext.vw_sales.Where(ss => ss.sales_id == sales.id).FirstOrDefault();
            ModelMapper.Map<vw_sales, SalesViewModel>(vw_sales, res);
            return res;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SalesViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            
            long id = ((SalesViewModel)item).sales_id;
            sales sales = dbContext.sales.Where(ss => ss.id == id).FirstOrDefault();
            if (sales == null)
            {
                modelState.AddModelError("serv_result", "Точка не найдена");
                return false;
            }

            // !!!
            // remove child objects

            dbContext.sales.Remove(sales);
            dbContext.SaveChanges();

            return true;
        }
    }   
}

