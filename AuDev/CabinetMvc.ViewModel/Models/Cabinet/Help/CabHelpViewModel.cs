﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class CabHelpViewModel : AuBaseViewModel
    {

        public CabHelpViewModel()
            : base(Enums.MainObjectType.CAB_HELP)
        {            
            //cab_help
        }

        [Key()]
        [Display(Name = "Код")]
        public int help_id { get; set; }

        [Display(Name = "Название")]
        public string help_name { get; set; }

        [Display(Name = "Родитель")]
        public Nullable<int> parent_id { get; set; }

        [Display(Name = "Уровень")]
        public int help_level { get; set; }

        [Display(Name = "Адрес")]
        public string url { get; set; }

        [Display(Name = "Иконка")]
        public string img { get; set; }

        [Display(Name = "Тэги")]
        public string tag { get; set; }

        [Display(Name = "Сортировка")]
        public Nullable<int> sort { get; set; }

        // children

        [Display(Name = "ChildrenList")]
        [JsonIgnore()]
        public IEnumerable<CabHelpViewModel> ChildrenList { get; set; }

        // TreeViewModel         

        [Display(Name = "id")]
        public int? id { get; set; }

        [Display(Name = "hasChildren")]
        public bool hasChildren { get; set; }

    }

    public class CabHelpSimpleViewModel
    {
        [Key()]
        [Display(Name = "Код")]
        public int help_id { get; set; }
    }
}

