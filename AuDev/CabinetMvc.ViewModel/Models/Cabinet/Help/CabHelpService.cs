﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.ComponentModel.DataAnnotations;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
#endregion

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ICabHelpService : IAuBaseService
    {
        IEnumerable<CabHelpViewModel> GetTreeList();
        List<string> GetTagList();
        List<CabHelpSimpleViewModel> Find(string item_name);
    }

    public class CabHelpService : AuBaseService, ICabHelpService
    {
        public IEnumerable<CabHelpViewModel> GetTreeList()
        {
            List<CabHelpViewModel> mainList = dbContext.cab_help
                .Where(ss => ss.help_level == 0)
                .Select(ss => new CabHelpViewModel()
                {
                    help_id = ss.help_id,
                    help_name = ss.help_name,
                    help_level = ss.help_level,
                    parent_id = ss.parent_id,
                    url = ss.url,
                    img = ss.img,
                    tag = ss.tag,
                    sort = ss.sort,
                    id = ss.help_id,
                    hasChildren = ss.cab_help1.Any(),
                }
                )
                .OrderBy(ss => ss.sort)
                .ToList();

            if ((mainList == null) || (mainList.Count <= 0))
                return mainList;

            foreach (var mainListItem in mainList)
            {
                mainListItem.ChildrenList = getChildrenList(mainListItem.help_id);
            }

            return mainList.OrderBy(ss => ss.help_level).ThenBy(ss => ss.sort);
        }

        private IEnumerable<CabHelpViewModel> getChildrenList(int help_id)
        {
            var childList = dbContext.cab_help
                .Where(ss => ss.parent_id == help_id)
                .Select(ss => new CabHelpViewModel()
                {
                    help_id = ss.help_id,
                    help_name = ss.help_name,
                    help_level = ss.help_level,
                    parent_id = ss.parent_id,
                    url = ss.url,
                    img = ss.img,
                    tag = ss.tag,
                    sort = ss.sort,
                    id = ss.help_id,
                    hasChildren = ss.cab_help1.Any(),
                }
                )
                .OrderBy(ss => ss.sort)
                .ToList();
            if ((childList != null) && (childList.Count > 0))
            {
                foreach (var childListItem in childList)
                {
                    childListItem.ChildrenList = getChildrenList(childListItem.help_id);
                }
            }
            return childList;
        }

        public List<string> GetTagList()
        {
            return dbContext.vw_cab_help_tag.Select(ss => ss.tag).Distinct().OrderBy(ss => ss).ToList();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.cab_help.Where(ss => ss.help_id == id)
                .Select(ss => new CabHelpViewModel()
                {
                    help_id = ss.help_id,
                    help_name = ss.help_name,
                    help_level = ss.help_level,
                    parent_id = ss.parent_id,
                    url = ss.url,
                    img = ss.img,
                    tag = ss.tag,
                    sort = ss.sort,
                    id = ss.help_id,
                    hasChildren = ss.cab_help1.Any(),
                })
                .FirstOrDefault();
        }

        public List<CabHelpSimpleViewModel> Find(string item_name)
        {
            if (String.IsNullOrEmpty(item_name))
                return null;

            List<CabHelpSimpleViewModel> res = new List<CabHelpSimpleViewModel>();

            res.AddRange(dbContext.cab_help
                    .Where(ss => ss.tag.Trim().ToLower().Contains(item_name.Trim().ToLower()))
                    .Select(ss => new CabHelpSimpleViewModel()
                    {
                        help_id = ss.help_id,
                    })
                    .ToList()
                    );

            return res.OrderBy(ss => ss.help_id).ToList();
        }
    }
}
