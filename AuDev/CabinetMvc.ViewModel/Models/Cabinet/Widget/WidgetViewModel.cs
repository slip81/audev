﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class WidgetViewModel : AuBaseViewModel
    {

        public WidgetViewModel()
            : base(Enums.MainObjectType.CAB_WIDGET)
        {
            //widget
        }

        [Key()]
        [Display(Name = "Код")]
        public int widget_id { get; set; }

        [Display(Name = "Название")]
        public string widget_name { get; set; }

        [Display(Name = "Описание")]
        public string widget_descr { get; set; }

        [Display(Name = "Код html-елемента")]
        public string widget_element_id { get; set; }

        [Display(Name = "Заголовок")]
        public string widget_header { get; set; }

        [Display(Name = "Контнет")]
        public string widget_content { get; set; }

        [Display(Name = "Порядковый номер")]
        public Nullable<int> sort_num { get; set; }

        [Display(Name = "Активен")]
        public bool is_active { get; set; }

        [Display(Name = "Услуга")]
        public Nullable<int> service_id { get; set; }
    }
}

