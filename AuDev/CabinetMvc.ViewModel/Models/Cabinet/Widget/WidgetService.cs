﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IWidgetService : IAuBaseService
    {
        //
    }

    public class WidgetService : AuBaseService, IWidgetService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.widget
                .ProjectTo<WidgetViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.widget.Where(ss => ss.service_id == parent_id)
                .ProjectTo<WidgetViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.widget.Where(ss => ss.widget_id == id)
                .ProjectTo<WidgetViewModel>()
                .FirstOrDefault();
        }

    }
}
