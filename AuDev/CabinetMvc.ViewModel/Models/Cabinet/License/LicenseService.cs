﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ILicenseService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_byClientAndService(int client_id, int service_id);
    }

    public class LicenseService : AuBaseService, ILicenseService
    {
        public IQueryable<AuBaseViewModel> GetList_byClientAndService(int client_id, int service_id)
        {
            return dbContext.vw_license.Where(ss => ss.client_id == client_id && ss.service_id == service_id).Select(ss => new LicenseViewModel()
                {
                    client_id = ss.client_id,
                    client_full_name = ss.client_full_name,
                    sales_id = ss.sales_id,
                    sales_name = ss.sales_name,
                    workplace_id = ss.workplace_id,
                    workplace_reg_key = ss.workplace_reg_key,
                    workplace_descr = ss.workplace_descr,
                    order_id = ss.order_id,
                    order_status = ss.order_status,
                    order_is_confirmed = ss.order_is_confirmed == 1,
                    order_active_status = ss.order_active_status,
                    version_id = ss.version_id,
                    version_name = ss.version_name,
                    service_id = ss.service_id,
                    service_name = ss.service_name,
                    order_item_id = ss.order_item_id,
                    order_item_quantity = ss.order_item_quantity,
                    service_registration_id = ss.service_registration_id,
                    license_id = ss.license_id,
                    license_created_on = ss.license_created_on,
                    license_date_start = ss.license_date_start,
                    license_date_end = ss.license_date_end,
                    sales_address = ss.sales_address,
                }
                );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var lic = dbContext.vw_license.Where(ss => ss.license_id == id).FirstOrDefault();
            return lic == null ? null : new LicenseViewModel(lic);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is LicenseViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            var lic = (LicenseViewModel)item;

            var db_lic = (from l in dbContext.license
                          where l.id == lic.license_id
                          select l)
                       .FirstOrDefault();

            if (db_lic == null)
            {
                modelState.AddModelError("", "Не найдена лицензия с кодом " + lic.license_id.ToString());
                return null;
            }

            modelBeforeChanges = new LicenseViewModel(db_lic);

            db_lic.dt_start = lic.license_date_start.HasValue ? (DateTime?)(((DateTime)lic.license_date_start).Date) : null;
            db_lic.dt_end = lic.license_date_end.HasValue ? (DateTime?)(((DateTime)lic.license_date_end).Date) : null;
            if ((db_lic.dt_start.HasValue) && (db_lic.dt_end.HasValue) && (db_lic.dt_start > db_lic.dt_end))
            {
                modelState.AddModelError("", "Дата начала не может быть больше даты окончания");
                return null;
            }

            dbContext.SaveChanges();

            var db_lic_vw = dbContext.vw_license.Where(ss => ss.license_id == lic.license_id).FirstOrDefault();

            //var logger = new LogService();
            //logger.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, "Редактирование лицензии", Util.UserManager.CurrUser.SessionId, Util.UserManager.CurrUser.UserName, null, (long)Enums.LogEventType.CABINET, lic.license_id);
            
            return new LicenseViewModel(db_lic_vw);
        }

    }
}
