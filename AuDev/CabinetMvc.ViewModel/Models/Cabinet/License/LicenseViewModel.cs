﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class LicenseViewModel : AuBaseViewModel
    {

        public LicenseViewModel()
            : base(Enums.MainObjectType.CAB_LICENSE)
        {
            //
        }

        public LicenseViewModel(license item)
            : base(Enums.MainObjectType.CAB_LICENSE)
        {
            license_created_on = item.created_on;
            license_date_end = item.dt_end;
            license_date_start = item.dt_start;
            license_id = item.id;
            order_item_id = item.order_item_id.HasValue ? (int)item.order_item_id : 0;
            service_registration_id = item.service_registration_id.HasValue ? (int)item.service_registration_id : 0;
        }

        public LicenseViewModel(vw_license item)
            : base(Enums.MainObjectType.CAB_LICENSE)
        {
            if (item == null)
                return;

            client_id = item.client_id;
            client_full_name = item.client_full_name;
            sales_id = item.sales_id;
            sales_name = item.sales_name;
            workplace_id = item.workplace_id;
            workplace_reg_key = item.workplace_reg_key;
            workplace_descr = item.workplace_descr;
            order_id = item.order_id;
            order_status = item.order_status;
            order_is_confirmed = item.order_is_confirmed == 1;
            order_active_status = item.order_active_status;
            version_id = item.version_id;
            version_name = item.version_name;
            service_id = item.service_id;
            service_name = item.service_name;
            order_item_id = item.order_item_id;
            order_item_quantity = item.order_item_quantity;
            service_registration_id = item.service_registration_id;
            license_id = item.license_id;
            license_created_on = item.license_created_on;
            license_date_start = item.license_date_start;            
            license_date_end = item.license_date_end;
            sales_address = item.sales_address;
        }

        [Key()]
        [Display(Name = "Код")]
        public int license_id { get; set; }
        
        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Клиент")]
        public string client_full_name { get; set; }

        [Display(Name = "Точка")]
        public int sales_id { get; set; }

        [Display(Name = "Точка")]
        public string sales_name { get; set; }

        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }

        [Display(Name = "Рабочее место")]
        public string workplace_reg_key { get; set; }

        [Display(Name = "Описание рабочего места")]
        public string workplace_descr { get; set; }

        [Display(Name = "order_id")]
        public int order_id { get; set; }

        [Display(Name = "order_status")]
        public Nullable<int> order_status { get; set; }

        [Display(Name = "order_is_confirmed")]
        public Nullable<bool> order_is_confirmed { get; set; }

        [Display(Name = "order_active_status")]
        public Nullable<int> order_active_status { get; set; }

        [Display(Name = "Версия")]
        public int version_id { get; set; }

        [Display(Name = "Версия")]
        public string version_name { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Услуга")]
        public string service_name { get; set; }

        [Display(Name = "order_item_id")]
        public int order_item_id { get; set; }

        [Display(Name = "order_item_quantity")]
        public Nullable<int> order_item_quantity { get; set; }

        [Display(Name = "service_registration_id")]
        public int service_registration_id { get; set; }

        [Display(Name = "Сооздана")]
        public System.DateTime license_created_on { get; set; }

        [Display(Name = "Дествует с")]
        public Nullable<System.DateTime> license_date_start { get; set; }

        [Display(Name = "Действует по")]
        public Nullable<System.DateTime> license_date_end { get; set; }

        [Display(Name = "Точка")]
        public string sales_address { get; set; }
    }
}