﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class DeliveryViewModel : AuBaseViewModel
    {

        public DeliveryViewModel()
            : base(Enums.MainObjectType.CAB_DELIVERY)
        {            
            //delivery
        }

        [Key()]
        [Display(Name = "Код")]
        public int delivery_id { get; set; }
        
        [Display(Name = "Шаблон рассылки")]
        [Required(ErrorMessage = "Не задан шаблон")]
        public int template_id { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор")]
        public string crt_user { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> state { get; set; }

        [Display(Name = "Дата статуса")]
        public Nullable<System.DateTime> state_date { get; set; }

        [Display(Name = "Всего строк")]
        public Nullable<int> detail_cnt { get; set; }

        [Display(Name = "Обработано")]
        public Nullable<int> processed_cnt { get; set; }

        [Display(Name = "Успешных")]
        public Nullable<int> success_cnt { get; set; }

        [Display(Name = "Ошибочных")]
        public Nullable<int> error_cnt { get; set; }

        [Display(Name = "Комментарий")]
        public string mess { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Не задано наименование")]
        public string descr { get; set; }

        [Display(Name = "need_stop")]
        public bool need_stop { get; set; }

        [Display(Name = "Номер запуска")]
        public int num { get; set; }

        [Display(Name = "Шаблон рассылки")]
        public string template_name { get; set; }

        [Display(Name = "Тема шаблона")]
        public string template_subject { get; set; }

        [Display(Name = "Текст шаблона")]
        public string template_body { get; set; }

        [Display(Name = "Статус")]
        public string state_name { get { return state.EnumName<Enums.DeliveryState>(); } }

        public IEnumerable<ClientViewModel> ClientList { get; set; }

        [Display(Name = "andStart")]
        public bool andStart { get; set; }

        [Display(Name = "DeliveryBtn")]
        [JsonIgnore()]
        public string DeliveryBtn
        {
            get
            {
                switch (state)
                {
                    case (int)Enums.DeliveryState.READY:
                    case (int)Enums.DeliveryState.DONE:
                        return "<button class='k-button k-button-icontext' title='Запустить рассылку' onclick='deliveryList.onStartDeliveryBtnClick(" + delivery_id.ToString() + ")'><span class='k-icon k-i-arrow-e'></span>Запустить</button>";
                    case (int)Enums.DeliveryState.IN_PROGRESS:
                        return "<button class='k-button k-button-icontext' title='Прервать рассылку' onclick='deliveryList.onStopDeliveryBtnClick(" + delivery_id.ToString() + ")'><span class='k-icon k-i-cancel'></span>Прервать</button>";                    
                    default:
                        return "";
                }
            }
        }
    }
}

