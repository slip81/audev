﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IDeliveryDetailService : IAuBaseService
    {
        IEnumerable<ClientViewModel> GetList_asClientList(int delivery_id);
    }

    public class DeliveryDetailService : AuBaseService, IDeliveryDetailService
    {

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.delivery_detail
                .Where(ss => ss.delivery_id == parent_id)
                .ProjectTo<DeliveryDetailViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.delivery_detail
                .Where(ss => ss.detail_id == id)
                .ProjectTo<DeliveryDetailViewModel>()
                .FirstOrDefault()
                ;
        }

        public IEnumerable<ClientViewModel> GetList_asClientList(int delivery_id)
        {
            return from t1 in dbContext.delivery_detail
                   from t2 in dbContext.allclients
                   where t1.delivery_id == delivery_id
                   && t1.client_id == t2.id
                   select new ClientViewModel()
                   {
                       id = t2.id,
                       client_name = t2.client_name,
                       site = t2.site,
                   };
        }

    }
}
