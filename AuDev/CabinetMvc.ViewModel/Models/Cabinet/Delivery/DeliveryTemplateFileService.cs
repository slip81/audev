﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IDeliveryTemplateFileService : IAuBaseService
    {
        DeliveryTemplateFileViewModel UploadFile(DeliveryTemplateFileViewModel itemAdd, HttpPostedFileBase file, ModelStateDictionary modelState);
        bool RemoveFile(string fileNames, int template_id, ModelStateDictionary modelState);
    }

    public class DeliveryTemplateFileService : AuBaseService, IDeliveryTemplateFileService
    {

        List<string> allowedExtentions = new List<string>() { 
            ".jpg", 
            ".jpeg",
            ".png",
            ".txt",
            ".xml",
            ".doc",
            ".docx",            
            ".xls",
            ".xlsx",
            ".7z",
            ".zip",
            ".rar",
            ".pdf",
        };

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.delivery_template_file
                .Where(ss => ss.template_id == parent_id)
                .ProjectTo<DeliveryTemplateFileViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.delivery_template_file
                .Where(ss => ss.file_id == id)
                .ProjectTo<DeliveryTemplateFileViewModel>()
                .FirstOrDefault()
                ;
        }
        
        public DeliveryTemplateFileViewModel UploadFile(DeliveryTemplateFileViewModel itemAdd, HttpPostedFileBase file, ModelStateDictionary modelState)
        {
            try
            {
                if (itemAdd == null)
                {
                    modelState.AddModelError("", "Не заданы атрибуты файла");
                    return null;
                }

                if (file.ContentLength > 5000000)
                {
                    modelState.AddModelError("", "Файл превышает максимально допустимый размер (5 Мб)");
                    return null;
                }

                string fileExt = Path.GetExtension(file.FileName);
                if (!allowedExtentions.Contains(fileExt.ToLower().Trim()))
                {
                    modelState.AddModelError("", "Недопустимый тип файла (" + fileExt + ")");
                    return null;
                }

                delivery_template_file delivery_template_file = new delivery_template_file();
                ModelMapper.Map<DeliveryTemplateFileViewModel, delivery_template_file>(itemAdd, delivery_template_file);

                MemoryStream target = new MemoryStream();
                file.InputStream.CopyTo(target);
                delivery_template_file.file_content = target.ToArray();
                
                dbContext.delivery_template_file.Add(delivery_template_file);
                dbContext.SaveChanges();

                DeliveryTemplateFileViewModel res = new DeliveryTemplateFileViewModel();
                ModelMapper.Map<delivery_template_file, DeliveryTemplateFileViewModel>(delivery_template_file, res);
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public bool RemoveFile(string fileNames, int template_id, ModelStateDictionary modelState)
        {
            try
            {
                if (String.IsNullOrEmpty(fileNames))
                {
                    return true;
                }

                delivery_template delivery_template = dbContext.delivery_template.Where(ss => ss.template_id == template_id).FirstOrDefault();
                if (delivery_template == null)
                {
                    return true;
                }

                delivery_template_file delivery_template_file = dbContext.delivery_template_file.Where(ss => ss.template_id == template_id && ss.file_name.Trim().ToLower().Equals(fileNames.Trim().ToLower())).FirstOrDefault();
                if (delivery_template_file == null)
                {
                    return true;
                }

                dbContext.delivery_template_file.Remove(delivery_template_file);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

    }
}
