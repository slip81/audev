﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class DeliveryTemplateGroupViewModel : AuBaseViewModel
    {

        public DeliveryTemplateGroupViewModel()
            : base(Enums.MainObjectType.CAB_DELIVERY_TEMPLATE_GROUP)
        {            
            //delivery_template_group
        }

        [Key()]
        [Display(Name = "Код")]
        public int group_id { get; set; }

        [Display(Name = "Наименование")]
        public string group_name { get; set; }
    }
}

