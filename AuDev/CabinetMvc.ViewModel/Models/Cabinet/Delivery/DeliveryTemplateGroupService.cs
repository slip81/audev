﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IDeliveryTemplateGroupService : IAuBaseService
    {
        //
    }

    public class DeliveryTemplateGroupService : AuBaseService, IDeliveryTemplateGroupService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.delivery_template_group
                .ProjectTo<DeliveryTemplateGroupViewModel>();
        }

    }
}
