﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class DeliveryDetailViewModel : AuBaseViewModel
    {

        public DeliveryDetailViewModel()
            : base(Enums.MainObjectType.CAB_DELIVERY_DETAIL)
        {            
            //delivery_detail
        }

        [Key()]
        [Display(Name = "Код")]
        public int detail_id { get; set; }

        [Display(Name = "Рассылка")]
        public int delivery_id { get; set; }

        [Display(Name = "Клиент")]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> state { get; set; }

        [Display(Name = "Дата статуса")]
        public Nullable<System.DateTime> state_date { get; set; }

        [Display(Name = "Адрес")]
        public string address { get; set; }

        [Display(Name = "Тема письма")]
        public string subject { get; set; }

        [Display(Name = "Текст письма")]
        public string body { get; set; }

        [Display(Name = "Комментарий")]
        public string mess { get; set; }

        [Display(Name = "Статус")]
        public string state_name { get { return state.EnumName<Enums.DeliveryState>(); } }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }
    }
}

