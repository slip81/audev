﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IDeliveryService : IAuBaseService
    {
        bool StartDelivery(int delivery_id, ModelStateDictionary modelState);
        bool StopDelivery(int delivery_id, ModelStateDictionary modelState);
    }

    public class DeliveryService : AuBaseService, IDeliveryService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.delivery
                .ProjectTo<DeliveryViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.delivery
                .Where(ss => ss.delivery_id == id)
                .ProjectTo<DeliveryViewModel>()
                .FirstOrDefault()
                ;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is DeliveryViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            DeliveryViewModel itemAdd = (DeliveryViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты рассылки");
                return null;
            }

            if ((itemAdd.ClientList == null) || (itemAdd.ClientList.Count() <= 0))
            {
                modelState.AddModelError("", "Не заданы клиенты для рассылки");
                return null;
            }

            delivery_template delivery_template = dbContext.delivery_template.Where(ss => ss.template_id == itemAdd.template_id).FirstOrDefault();
            if (delivery_template == null)
            {
                modelState.AddModelError("", "Не найден шаблон для рассылки");
                return null;
            }

            DateTime now = DateTime.Now;
            itemAdd.crt_date = now;
            itemAdd.crt_user = currUser.CabUserName;
            itemAdd.detail_cnt = itemAdd.ClientList.Count();
            itemAdd.error_cnt = 0;
            itemAdd.state = (int)Enums.DeliveryState.READY;
            itemAdd.state_date = now;
            itemAdd.success_cnt = 0;
            itemAdd.processed_cnt = 0;                        
            delivery delivery = new delivery();
            ModelMapper.Map<DeliveryViewModel, delivery>(itemAdd, delivery);

            dbContext.delivery.Add(delivery);
            
            foreach (var detail in itemAdd.ClientList)
            {
                delivery_detail delivery_detail = new delivery_detail();
                delivery_detail.address = detail.site;
                delivery_detail.body = delivery_template.body;
                delivery_detail.client_id = detail.id;
                delivery_detail.delivery = delivery;
                delivery_detail.mess = null;
                delivery_detail.state = (int)Enums.DeliveryState.READY;
                delivery_detail.state_date = now;
                delivery_detail.subject = delivery_template.subject;
                dbContext.delivery_detail.Add(delivery_detail);                
            }

            dbContext.SaveChanges();

            if (itemAdd.andStart)
            {
                StartDelivery(delivery.delivery_id, modelState);
            }

            DeliveryViewModel res = new DeliveryViewModel();
            ModelMapper.Map<delivery, DeliveryViewModel>(delivery, res);
            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is DeliveryViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            DeliveryViewModel itemEdit = (DeliveryViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры рассылки");
                return null;
            }

            delivery delivery = dbContext.delivery.Where(ss => ss.delivery_id == itemEdit.delivery_id).FirstOrDefault();
            if (delivery == null)
            {
                modelState.AddModelError("", "Не найдены параметры рассылки");
                return null;
            }

            DateTime now = DateTime.Now;

            DeliveryViewModel oldModel = new DeliveryViewModel();
            ModelMapper.Map<delivery, DeliveryViewModel>(delivery, oldModel);
            modelBeforeChanges = oldModel;

            //ModelMapper.Map<DeliveryViewModel, delivery>(itemEdit, delivery);
            delivery.template_id = itemEdit.template_id;
            delivery.descr = itemEdit.descr;

            delivery_template delivery_template = dbContext.delivery_template.Where(ss => ss.template_id == itemEdit.template_id).FirstOrDefault();
            List<delivery_detail> delivery_detail_list = dbContext.delivery_detail.Where(ss => ss.delivery_id == itemEdit.delivery_id).ToList();
            bool detailsChanged = false;
                        
            // 1. сначала удаляем из delivery_detail_list строки, которых нет в itemEdit.ClientList
            List<delivery_detail> delivery_detail_list_for_del = null;
            if ((delivery_detail_list != null) && (delivery_detail_list.Count > 0))
            {
                if ((itemEdit.ClientList == null) || (itemEdit.ClientList.Count() <= 0))
                {
                    delivery_detail_list_for_del = delivery_detail_list;                    
                }
                else
                {
                    delivery_detail_list_for_del = delivery_detail_list.Where(ss => !itemEdit.ClientList.Where(tt => tt.id == ss.client_id).Any()).ToList();
                }
            }

            if (delivery_detail_list_for_del != null)
            {             
                dbContext.delivery_detail.RemoveRange(delivery_detail_list_for_del);
                detailsChanged = true;
            }

            // 2. потом добавляем в delivery_detail_list строки, которых там нет и которые есть в itemEdit.ClientList
            List<delivery_detail> delivery_detail_list_for_add = null;
            if ((delivery_detail_list != null) && (delivery_detail_list.Count > 0))
            {
                if ((itemEdit.ClientList != null) && (itemEdit.ClientList.Count() > 0))
                {
                    delivery_detail_list_for_add = itemEdit.ClientList.Where(tt => !delivery_detail_list.Where(ss => ss.client_id == tt.id).Any())
                        .Select(ss => new delivery_detail
                        {
                            address = ss.site,
                            body = delivery_template.body,
                            client_id = ss.id,
                            delivery_id = itemEdit.delivery_id,
                            mess = null,
                            state = (int)Enums.DeliveryState.READY,
                            state_date = now,
                            subject = delivery_template.subject,
                        })
                        .ToList();
                }                
            }
            else
            {
                if ((itemEdit.ClientList != null) && (itemEdit.ClientList.Count() > 0))
                {
                    delivery_detail_list_for_add = itemEdit.ClientList.Select(ss => new delivery_detail
                    {
                        address = ss.site,
                        body = delivery_template.body,
                        client_id = ss.id,
                        delivery_id = itemEdit.delivery_id,
                        mess = null,
                        state = (int)Enums.DeliveryState.READY,
                        state_date = now,
                        subject = delivery_template.subject,
                    }).ToList();
                }
            }

            if (delivery_detail_list_for_add != null)
            {
                dbContext.delivery_detail.AddRange(delivery_detail_list_for_add);
                detailsChanged = true;
            }

            dbContext.SaveChanges();

            if (detailsChanged)
            {
                delivery.error_cnt = 0;
                delivery.processed_cnt = 0;
                delivery.success_cnt = 0;
                delivery.detail_cnt = dbContext.delivery_detail.Where(ss => ss.delivery_id == itemEdit.delivery_id).Count();
                dbContext.SaveChanges();
            }

            DeliveryViewModel res = new DeliveryViewModel();
            ModelMapper.Map<delivery, DeliveryViewModel>(delivery, res);

            return res;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is DeliveryViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            DeliveryViewModel itemDel = (DeliveryViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты шаблона");
                return false;
            }

            delivery delivery = dbContext.delivery.Where(ss => ss.delivery_id == itemDel.delivery_id).FirstOrDefault();
            if (delivery == null)
            {
                modelState.AddModelError("", "Не найдена рассылка с кодом " + itemDel.delivery_id);
                return false;
            }

            if (delivery.state == (int)Enums.DeliveryState.IN_PROGRESS)
            {
                modelState.AddModelError("", "Нельза удалить запущенную рассылку");
                return false;
            }

            dbContext.delivery_detail.RemoveRange(dbContext.delivery_detail.Where(ss => ss.delivery_id == itemDel.delivery_id));
            dbContext.delivery.Remove(delivery);

            dbContext.SaveChanges();

            return true;
        }

        public bool StartDelivery(int delivery_id, ModelStateDictionary modelState)
        {
            int curr_delivery_id = delivery_id;

            using (var currContext = new AuMainDb())
            {                
                delivery delivery = currContext.delivery.Where(ss => ss.delivery_id == delivery_id).FirstOrDefault();
                if (delivery == null)
                {
                    ToLog("Ошибка при запуске рассылки", (long)Enums.LogEventType.ERROR, null, null, "Не найдена рассылка с кодом " + delivery_id.ToString());
                    return false;
                }

                if (delivery.state == (int)Enums.DeliveryState.IN_PROGRESS)
                {
                    ToLog("Ошибка при запуске рассылки", (long)Enums.LogEventType.ERROR, null, null, "Рассылка с кодом " + delivery_id.ToString() + " уже запущена");
                    return false;
                }

                if (delivery.state == (int)Enums.DeliveryState.DONE)
                {
                    DeliveryViewModel copyDelivery = new DeliveryViewModel()
                    {
                        crt_date = DateTime.Now,
                        crt_user = currUser.CabUserName,
                        descr = delivery.descr,
                        template_id = delivery.template_id,                        
                        num = delivery.num,
                        andStart = false,
                        ClientList = dbContext.delivery_detail.Where(ss => ss.delivery_id == delivery.delivery_id)
                        .Select(ss => new ClientViewModel()
                        {
                            id = (int)ss.client_id,
                            site = ss.address,
                        }),
                    };

                    DeliveryViewModel newDelivery = (DeliveryViewModel)Insert(copyDelivery, modelState);
                    curr_delivery_id = newDelivery.delivery_id;                    
                }
            }

            Task task = new Task(() =>
            {
                xStartDelivery(curr_delivery_id, modelState);
            }
            );
            task.Start();

            return true;
        }

        private bool xStartDelivery(int delivery_id, ModelStateDictionary modelState)
        {
            using (var currContext = new AuMainDb())
            {
                delivery delivery = currContext.delivery.Where(ss => ss.delivery_id == delivery_id).FirstOrDefault();

                List<delivery_detail> delivery_detail_list = currContext.delivery_detail.Where(ss => ss.delivery_id == delivery_id).ToList();
                if ((delivery_detail_list == null) || (delivery_detail_list.Count <= 0))
                {
                    delivery.state = (int)Enums.DeliveryState.DONE;
                    delivery.state_date = DateTime.Now;
                    delivery.mess = "Список клиентов пуст";
                    currContext.SaveChanges();
                    return true;
                }

                delivery.num = delivery.num + 1;                
                delivery.state = (int)Enums.DeliveryState.IN_PROGRESS;
                delivery.state_date = DateTime.Now;
                currContext.SaveChanges();

                int processed_cnt = 0;
                int success_cnt = 0;
                int error_cnt = 0;
                foreach (var delivery_detail in delivery_detail_list)
                {
                    // всегда ждем 3 сек
                    Thread.Sleep(3000);

                    var curr_need_stop = currContext.delivery.Where(ss => ss.delivery_id == delivery_id).Select(ss => ss.need_stop).FirstOrDefault();
                    if (curr_need_stop)
                    {
                        delivery.processed_cnt = processed_cnt;
                        delivery.error_cnt = error_cnt;
                        delivery.success_cnt = success_cnt;
                        delivery.state = (int)Enums.DeliveryState.DONE;
                        delivery.state_date = DateTime.Now;
                        currContext.SaveChanges();
                        return true;
                    }

                    try
                    {
                        processed_cnt++;

                        if (String.IsNullOrEmpty(delivery_detail.address))
                        {
                            error_cnt++;
                            delivery.processed_cnt = processed_cnt;
                            delivery.error_cnt = error_cnt;
                            delivery_detail.mess = "Не задан адрес для отправки";
                            delivery_detail.state = (int)Enums.DeliveryState.ERROR;
                            currContext.SaveChanges();
                            continue;
                        }

                        if (!(new EmailAddressAttribute().IsValid(delivery_detail.address.Trim())))
                        {
                            error_cnt++;
                            delivery.processed_cnt = processed_cnt;
                            delivery.error_cnt = error_cnt;
                            delivery_detail.mess = "Некорректный адрес для отправки";
                            delivery_detail.state = (int)Enums.DeliveryState.ERROR;
                            currContext.SaveChanges();
                            continue;
                        }


                        if (String.IsNullOrEmpty(delivery_detail.subject))
                        {
                            error_cnt++;
                            delivery.processed_cnt = processed_cnt;
                            delivery.error_cnt = error_cnt;
                            delivery_detail.mess = "Не задана тема письма";
                            delivery_detail.state = (int)Enums.DeliveryState.ERROR;
                            currContext.SaveChanges();
                            continue;
                        }

                        if (String.IsNullOrEmpty(delivery_detail.body))
                        {
                            error_cnt++;
                            delivery.processed_cnt = processed_cnt;
                            delivery.error_cnt = error_cnt;
                            delivery_detail.mess = "Не задан текст письма";
                            delivery_detail.state = (int)Enums.DeliveryState.ERROR;
                            currContext.SaveChanges();
                            continue;
                        }

                        GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", delivery_detail.address, delivery_detail.subject, delivery_detail.body, null, true);

                        success_cnt++;
                        delivery.processed_cnt = processed_cnt;
                        delivery.success_cnt = success_cnt;
                        delivery_detail.mess = "Отправлено";
                        delivery_detail.state = (int)Enums.DeliveryState.DONE;
                        currContext.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        error_cnt++;
                        delivery.processed_cnt = processed_cnt;
                        delivery.error_cnt = error_cnt;
                        delivery_detail.mess = GlobalUtil.ExceptionInfo(ex);
                        delivery_detail.state = (int)Enums.DeliveryState.ERROR;
                        currContext.SaveChanges();
                    }
                }

                delivery.state = (int)Enums.DeliveryState.DONE;
                delivery.state_date = DateTime.Now;
                currContext.SaveChanges();

                return true;
            }
        }

        public bool StopDelivery(int delivery_id, ModelStateDictionary modelState)
        {
            delivery delivery = dbContext.delivery.Where(ss => ss.delivery_id == delivery_id).FirstOrDefault();
            if (delivery == null)
            {
                modelState.AddModelError("", "Не найдена рассылка с кодом " + delivery_id.ToString());
                return false;
            }

            if (delivery.state != (int)Enums.DeliveryState.IN_PROGRESS)
            {
                modelState.AddModelError("", "Рассылка с кодом " + delivery_id.ToString() + " не запущена");
                return false;
            }

            delivery.need_stop = true;
            delivery.state = (int)Enums.DeliveryState.DONE;
            delivery.state_date = DateTime.Now;
            dbContext.SaveChanges();

            return true;
        }
    }
}
