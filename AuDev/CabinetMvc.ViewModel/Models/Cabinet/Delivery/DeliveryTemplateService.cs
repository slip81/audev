﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IDeliveryTemplateService : IAuBaseService
    {
        //
    }

    public class DeliveryTemplateService : AuBaseService, IDeliveryTemplateService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.delivery_template
                .ProjectTo<DeliveryTemplateViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.delivery_template
                .Where(ss => ss.template_id == id)
                .ProjectTo<DeliveryTemplateViewModel>()
                .FirstOrDefault()
                ;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is DeliveryTemplateViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            DeliveryTemplateViewModel itemAdd = (DeliveryTemplateViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты шаблона");
                return null;
            }

            DateTime now = DateTime.Now;

            int? curr_group_id = null;
            delivery_template_group delivery_template_group = null;
            if (itemAdd.Group_Changed)
            {
                if (!String.IsNullOrEmpty(itemAdd.Group_Selected))
                {
                    delivery_template_group = dbContext.delivery_template_group.Where(ss => ss.group_name.Trim().ToLower().Equals(itemAdd.Group_Selected.Trim().ToLower())).FirstOrDefault();
                    if (delivery_template_group == null)
                    {
                        delivery_template_group = new delivery_template_group();
                        delivery_template_group.group_name = itemAdd.Group_Selected;
                        dbContext.delivery_template_group.Add(delivery_template_group);
                        dbContext.SaveChanges();
                        curr_group_id = delivery_template_group.group_id;
                    }
                    curr_group_id = delivery_template_group.group_id;
                }
                else
                {
                    curr_group_id = null;
                }
            }
            else
            {
                curr_group_id = itemAdd.group_id;
            }

            itemAdd.group_id = curr_group_id;
            delivery_template delivery_template = new delivery_template();
            ModelMapper.Map<DeliveryTemplateViewModel, delivery_template>(itemAdd, delivery_template);

            dbContext.delivery_template.Add(delivery_template);
            dbContext.SaveChanges();

            DeliveryTemplateViewModel res = new DeliveryTemplateViewModel();
            ModelMapper.Map<delivery_template, DeliveryTemplateViewModel>(delivery_template, res);
            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is DeliveryTemplateViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            DeliveryTemplateViewModel itemEdit = (DeliveryTemplateViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры шаблона");
                return null;
            }

            delivery_template delivery_template = dbContext.delivery_template.Where(ss => ss.template_id == itemEdit.template_id).FirstOrDefault();
            if (delivery_template == null)
            {
                modelState.AddModelError("", "Не найдены параметры шаблона");
                return null;
            }

            DeliveryTemplateViewModel oldModel = new DeliveryTemplateViewModel();
            ModelMapper.Map<delivery_template, DeliveryTemplateViewModel>(delivery_template, oldModel);
            modelBeforeChanges = oldModel;

            int? curr_group_id = null;
            delivery_template_group delivery_template_group = null;
            if (itemEdit.Group_Changed)
            {
                if (!String.IsNullOrEmpty(itemEdit.Group_Selected))
                {
                    delivery_template_group = dbContext.delivery_template_group.Where(ss => ss.group_name.Trim().ToLower().Equals(itemEdit.Group_Selected.Trim().ToLower())).FirstOrDefault();
                    if (delivery_template_group == null)
                    {
                        delivery_template_group = new delivery_template_group();
                        delivery_template_group.group_name = itemEdit.Group_Selected;
                        dbContext.delivery_template_group.Add(delivery_template_group);
                        dbContext.SaveChanges();
                        curr_group_id = delivery_template_group.group_id;
                    }
                    curr_group_id = delivery_template_group.group_id;
                }
                else
                {
                    curr_group_id = null;
                }
            }
            else
            {
                curr_group_id = itemEdit.group_id;
            }


            itemEdit.group_id = curr_group_id;
            ModelMapper.Map<DeliveryTemplateViewModel, delivery_template>(itemEdit, delivery_template);

            dbContext.SaveChanges();

            DeliveryTemplateViewModel res = new DeliveryTemplateViewModel();
            ModelMapper.Map<delivery_template, DeliveryTemplateViewModel>(delivery_template, res);

            return res;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is DeliveryTemplateViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            DeliveryTemplateViewModel itemDel = (DeliveryTemplateViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты шаблона");
                return false;
            }

            delivery_template delivery_template = dbContext.delivery_template.Where(ss => ss.template_id == itemDel.template_id).FirstOrDefault();
            if (delivery_template == null)
            {
                modelState.AddModelError("", "Не найден шаблон с кодом " + itemDel.template_id);
                return false;
            }

            var existing = dbContext.delivery.Where(ss => ss.template_id == itemDel.template_id).FirstOrDefault();
            if (existing != null)
            {
                modelState.AddModelError("", "Есть рассылки с этим шаблоном");
                return false;
            }

            dbContext.delivery_template.Remove(delivery_template);

            dbContext.SaveChanges();

            return true;
        }

    }
}
