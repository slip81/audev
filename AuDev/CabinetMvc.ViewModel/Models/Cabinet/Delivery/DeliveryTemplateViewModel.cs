﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class DeliveryTemplateViewModel : AuBaseViewModel
    {

        public DeliveryTemplateViewModel()
            : base(Enums.MainObjectType.CAB_DELIVERY_TEMPLATE)
        {            
            //delivery_template
        }

        [Key()]
        [Display(Name = "Код")]
        public int template_id { get; set; }

        [Display(Name = "Группа")]
        public Nullable<int> group_id { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Не задано наименование")]
        public string template_name { get; set; }

        [Display(Name = "Тема")]
        [Required(ErrorMessage = "Не задана тема")]
        public string subject { get; set; }

        [Display(Name = "Текст")]
        [Required(ErrorMessage = "Не задан текст")]
        public string body { get; set; }

        [Display(Name = "Группа")]        
        public string Group_Selected { get; set; }

        [Display(Name = "Group_Changed")]
        public bool Group_Changed { get; set; }

        [Display(Name = "files")]
        [JsonIgnore()]
        public IEnumerable<DeliveryTemplateFileViewModel> files { get; set; }
    }
}

