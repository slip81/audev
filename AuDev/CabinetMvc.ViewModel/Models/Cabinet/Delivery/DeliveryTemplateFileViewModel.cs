﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class DeliveryTemplateFileViewModel : AuBaseViewModel
    {

        public DeliveryTemplateFileViewModel()
            : base(Enums.MainObjectType.CAB_DELIVERY_TEMPLATE_FILE)
        {            
            //delivery_template_file
        }

        [Key()]
        [Display(Name = "Код")]
        public int file_id { get; set; }

        [Display(Name = "Шаблон")]
        public int template_id { get; set; }

        [Display(Name = "Имя")]
        public string file_name { get; set; }

        [Display(Name = "Размер")]
        public long file_size { get; set; }

        [Display(Name = "Тип")]
        public string file_ext { get; set; }

        //[Display(Name = "Файл")]
        //[JsonIgnore()]
        //public byte[] file_content { get; set; }
    }
}

