﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class WorkplaceViewModel :  AuBaseViewModel
    {

        public WorkplaceViewModel()
            : base(Enums.MainObjectType.CAB_WORKPLACE)
        {
           //
        }

        public WorkplaceViewModel(workplace item)
            : base(Enums.MainObjectType.CAB_WORKPLACE)
        {            
            id = item.id;
            guid = item.guid;
            registration_key = item.registration_key;
            description = item.description;
            sales_id = item.sales_id;
            is_deleted = item.is_deleted == 1;
            //account = item.account;
            WorkplaceFullName = item.description + " [" + item.registration_key + "]";
            ReregReason = "";
        }

        public WorkplaceViewModel(vw_workplace item)
            : base(Enums.MainObjectType.CAB_WORKPLACE)
        {
            id = item.workplace_id;
            registration_key = item.registration_key;
            description = item.description;
            sales_id = item.sales_id;
            sales_name = item.sales_name;
            sales_address = item.sales_address;
            client_id = item.client_id;
            client_name = item.client_name;
            user_name = item.user_name;
            WorkplaceFullName = item.description + " [" + item.registration_key + "]";
            SalesFullName = item.sales_address + " [" + item.sales_name + "]";
            ReregReason = "";
        }
        
        [Key()]
        [Display(Name = "Код")]
        [ScaffoldColumn(false)]
        public int id { get; set; }

        [Display(Name = "GUID")]
        [ScaffoldColumn(false)]
        public string guid { get; set; }

        [Display(Name = "Железный ключ")]
        [Required(ErrorMessage = "Не задан ключ")]
        [UIHint("WorkplaceRegKey")]
        public string registration_key { get; set; }

        [Display(Name = "Описание места")]
        [Required(ErrorMessage = "Не задано описание")]
        public string description { get; set; }

        [Display(Name = "Код точки")]
        [ScaffoldColumn(false)]
        public Nullable<int> sales_id { get; set; }

        [Display(Name = "Точка")]
        [ScaffoldColumn(false)]
        public string sales_name { get; set; }

        [Display(Name = "Адрес")]
        [ScaffoldColumn(false)]
        public string sales_address { get; set; }

        [Display(Name = "is_deleted")]
        [ScaffoldColumn(false)]
        public Nullable<bool> is_deleted { get; set; }

        //[Display(Name = "account")]
        //public string account { get; set; }

        [Display(Name = "Код клиента")]
        [ScaffoldColumn(false)]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "Клиент")]
        [ScaffoldColumn(false)]
        public string client_name { get; set; }

        [Display(Name = "Логин")]
        [ScaffoldColumn(false)]
        public string user_name { get; set; }

        [Display(Name = "Рабочее место")]
        [ScaffoldColumn(false)]
        public string WorkplaceFullName { get; set; }
        
        [Display(Name = "Торговая точка")]
        [ScaffoldColumn(false)]
        public string SalesFullName { get; set; }

        [Display(Name = "Причина")]
        [ScaffoldColumn(false)]
        public string ReregReason { get; set; }  

        [Display(Name = "Услуги")]
        [ScaffoldColumn(false)]
        public string AddServiceButton
        { 
            get
            { 
                return "<a href=/ExchangeRegFromCabClient?reg_type=1&w_id=" + this.id.ToString() + " class='k-button'>Обмен с партнерами</a>"
                    //+ "<br />"
                    + "<a href=/ExchangeRegFromCabClient?reg_type=2&&w_id=" + this.id.ToString() + " class='k-button'>Накладные по email</a>"
                    ;
            }
        }
    }
}