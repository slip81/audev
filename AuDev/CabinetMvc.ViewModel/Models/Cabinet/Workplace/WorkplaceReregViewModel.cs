﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class WorkplaceReregViewModel :  AuBaseViewModel
    {

        public WorkplaceReregViewModel()
            : base(Enums.MainObjectType.CAB_WORKPLACE_REREG)
        {
            //
        }

        public WorkplaceReregViewModel(reregistration_order item)
            : base(Enums.MainObjectType.CAB_WORKPLACE_REREG)
        {
            rereg_id = item.id;
            person = item.person;
            reason = item.reason;
            manager_activation = item.manager_activation;
            manager_activation_bool = item.manager_activation == 1;
            created_on = item.created_on;
            created_by = item.created_by;
            updated_on = item.updated_on;
            updated_by = item.updated_by;
            is_deleted = item.is_deleted;
            is_used = item.is_used;
            is_used_bool = item.is_used == 1;
            workplace_id = item.workplace_id;
        }

        public WorkplaceReregViewModel(vw_workplace_rereg item)
            : base(Enums.MainObjectType.CAB_WORKPLACE_REREG)
        {            
                rereg_id = item.rereg_id;
                person = item.person;
                reason = item.reason;
                manager_activation = item.manager_activation;
                manager_activation_bool = item.manager_activation == 1;
                created_on = item.created_on;
                created_by = item.created_by;
                updated_on = item.updated_on;
                updated_by = item.updated_by;
                is_deleted = item.is_deleted;
                is_used = item.is_used;
                is_used_bool = item.is_used == 1;
                workplace_id = item.workplace_id;
                registration_key = item.registration_key;
                description = item.description;
                sales_id = item.sales_id;
                sales_name = item.sales_name;
                sales_address = item.sales_address;
                client_id = item.client_id;
                client_name = item.client_name;
        }

        
        [Key()]
        [Display(Name="Код")]
        public int rereg_id { get; set; }

        [Display(Name = "Ответственный")]
        public string person { get; set; }

        [Display(Name = "Причина")]
        public string reason { get; set; }

        [Display(Name = "Активировано")]
        public Nullable<short> manager_activation { get; set; }

        [Display(Name = "Активировано")]
        public bool manager_activation_bool { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> created_on { get; set; }

        [Display(Name = "created_by")]
        public string created_by { get; set; }

        [Display(Name = "updated_on")]
        public Nullable<System.DateTime> updated_on { get; set; }

        [Display(Name = "updated_by")]
        public string updated_by { get; set; }

        [Display(Name = "is_deleted")]
        public Nullable<short> is_deleted { get; set; }

        [Display(Name = "Использовано")]
        public Nullable<short> is_used { get; set; }

        [Display(Name = "Использовано")]
        public bool is_used_bool { get; set; }

        [Display(Name = "Рабочее место")]
        public Nullable<int> workplace_id { get; set; }

        [Display(Name = "Ключ регистрации")]
        public string registration_key { get; set; }

        [Display(Name = "Описание")]
        public string description { get; set; }

        [Display(Name = "Торговая точка")]
        public Nullable<int> sales_id { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }

        [Display(Name = "Адрес")]
        public string sales_address { get; set; }

        [Display(Name = "Организация")]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }
    }
}