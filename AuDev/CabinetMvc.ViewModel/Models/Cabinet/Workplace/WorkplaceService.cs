﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IWorkplaceService : IAuBaseService
    {
        WorkplaceViewModel GetItem_VwWorkplace(int id);
        WorkplaceViewModel Rereg(int workplace_id, string person, string reason, ModelStateDictionary modelState);
        IQueryable<WorkplaceViewModel> GetList_Sprav(long? client_id);
        IQueryable<WorkplaceViewModel> GetList_byClient(int client_id);
    }

    public class WorkplaceService : AuBaseService, IWorkplaceService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            //UrlHelper u = new UrlHelper();
            return dbContext.vw_workplace.Where(ss => ss.sales_id == parent_id )
                .OrderByDescending(ss => ss.workplace_id)
                .Select(ss => new WorkplaceViewModel()
                {
                    id = ss.workplace_id,
                    //guid = ss.guid,
                    registration_key = ss.registration_key,
                    description = ss.description,
                    sales_id = ss.sales_id,
                    //is_deleted = ss.is_deleted == 1,
                    //account = ss.,     
                    WorkplaceFullName = ss.description + " [" + ss.registration_key + "]",
                    SalesFullName = ss.sales_address + " [" + ss.sales_name + "]",
                    //AddServiceButton = "<a href='" + u.Action("ExchangeUserRegAdd", "Exchange", new { sales_id = ss.sales_id, w_id = ss.id }) + "' class='k-button'>Добавить услугу</a>",
                });
        }

        public IQueryable<WorkplaceViewModel> GetList_byClient(int client_id)
        {            
            return dbContext.vw_workplace
                .Where(ss => ss.client_id == client_id)
                .OrderByDescending(ss => ss.workplace_id)
                .Select(ss => new WorkplaceViewModel()
                {
                    id = ss.workplace_id,                    
                    registration_key = ss.registration_key,
                    description = ss.description,
                    sales_id = ss.sales_id,
                    WorkplaceFullName = ss.description + " [" + ss.registration_key + "]",
                    SalesFullName = ss.sales_address + " [" + ss.sales_name + "]",                    
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.workplace.Where(ss => ss.id == id).Select(ss => new WorkplaceViewModel()
            {
                id = ss.id,
                guid = ss.guid,
                registration_key = ss.registration_key,
                description = ss.description,
                sales_id = ss.sales_id,
                is_deleted = ss.is_deleted == 1,
                //account = ss.account,
                WorkplaceFullName = ss.description + " [" + ss.registration_key + "]",
                //AddServiceButton = "",
            })
            .FirstOrDefault();
        }

        public WorkplaceViewModel GetItem_VwWorkplace(int id)
        {
            return dbContext.vw_workplace.Where(ss => ss.workplace_id == id)
                .Select(ss => new WorkplaceViewModel()
                {
                    id = ss.workplace_id,                 
                    registration_key = ss.registration_key,
                    description = ss.description,
                    sales_id = ss.sales_id,
                    sales_address = ss.sales_address,
                    sales_name = ss.sales_name,
                    client_id = ss.client_id,
                    client_name = ss.client_name,
                    user_name = ss.user_name,
                    WorkplaceFullName = ss.description + " [" + ss.registration_key + "]",
                    SalesFullName = ss.sales_address + " [" + ss.sales_name + "]",
                })
            .FirstOrDefault();
        }

        public IQueryable<WorkplaceViewModel> GetList_Sprav(long? client_id)
        {
            return dbContext.vw_workplace
                .Where(ss => ss.client_id == client_id)
                .OrderBy(ss => ss.workplace_id)
                .Select(ss => new WorkplaceViewModel()
            {
                id = ss.workplace_id,
                sales_id = ss.sales_id,
                WorkplaceFullName = ss.description + " [" + ss.registration_key + "]",                
            });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is WorkplaceViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            WorkplaceViewModel itemAdd = (WorkplaceViewModel)item;
            if (
                (itemAdd == null)
                ||
                (String.IsNullOrEmpty(itemAdd.description))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            workplace workplace = new workplace();
            //workplace.account = wpAdd.account;
            workplace.description = itemAdd.description;
            workplace.guid = Guid.NewGuid().ToString();
            workplace.is_deleted = 0;
            workplace.registration_key = itemAdd.registration_key;
            workplace.sales_id = itemAdd.sales_id;
            dbContext.workplace.Add(workplace);
            dbContext.SaveChanges();

            int id = workplace.id;

            var workplace_db = dbContext.workplace.Where(ss => ss.id == id).FirstOrDefault();
            return new WorkplaceViewModel(workplace_db);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is WorkplaceViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            WorkplaceViewModel itemEdit = (WorkplaceViewModel)item;
            if (
                (itemEdit == null)
                ||
                (String.IsNullOrEmpty(itemEdit.description))
                ||
                (String.IsNullOrEmpty(itemEdit.registration_key))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            workplace workplace = dbContext.workplace.Where(ss => ss.id == itemEdit.id && ss.is_deleted != 1).FirstOrDefault();
            if (workplace == null)
            {
                modelState.AddModelError("", "Не найдено рабочее место с кодом " + itemEdit.id.ToString());
                return null;
            }
            modelBeforeChanges = new WorkplaceViewModel(workplace);

            reregistration_order order_existing = dbContext.reregistration_order.Where(ss => ss.workplace_id == itemEdit.id && ss.is_deleted != 1 && ss.manager_activation != 1).FirstOrDefault();
            if (order_existing != null)
            {
                modelState.AddModelError("", "Уже есть заявка на перерегистрацию данного рабочего места");
                return null;
            }

            reregistration_order order = new reregistration_order();
            order.created_by = currUser.UserName;
            order.created_on = DateTime.Now;
            order.guid = Guid.NewGuid().ToString();
            order.is_deleted = 0;
            order.is_used = 1;
            order.manager_activation = 1;
            order.person = currUser.UserName;
            order.reason = itemEdit.ReregReason;
            order.workplace_id = itemEdit.id;

            dbContext.reregistration_order.Add(order);

            workplace.description = itemEdit.description;
            workplace.registration_key = itemEdit.registration_key;

            // !!!
            // todo
            // перегенирация ключей активации
            var regList = dbContext.service_registration.Where(ss => ss.workplace_id == workplace.id && ss.license_id.HasValue).ToList();
            foreach (var reg in regList)
            {
                string activation_key = "";
                bool needKey = reg.license1.order_item.service.need_key == 1;
                if (needKey)
                {
                    string registration_key = dbContext.workplace.Where(ss => ss.id == workplace.id).FirstOrDefault().registration_key;
                    string version_name = reg.license1.order_item.version.name;
                    string exe_name = reg.license1.order_item.service.exe_name;
                    int? file_size = reg.license1.order_item.version.file_size;
                    string hash = String.Format("{0};{1};{2};{3};{4}", registration_key, version_name, (int)Math.Round(((double)file_size) / 10000, 0), exe_name, String.Empty);
                    activation_key = GlobalUtil.CheckCode(version_name, exe_name, hash);
                }
                else
                {
                    activation_key = "";
                }


                reg.activation_key = activation_key;
            }

            dbContext.SaveChanges();

            var workplace_db = dbContext.workplace.Where(ss => ss.id == itemEdit.id).FirstOrDefault();
            return new WorkplaceViewModel(workplace_db);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is WorkplaceViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            long id = ((WorkplaceViewModel)item).id;
            workplace workplace = dbContext.workplace.Where(ss => ss.id == id).FirstOrDefault();
            if (workplace == null)
            {
                modelState.AddModelError("", "Рабочее место не найдено");
                return false;
            }

            var exist1 = dbContext.service_cva.Where(ss => ss.workplace_id == workplace.id).FirstOrDefault();
            if (exist1 != null)
            {
                modelState.AddModelError("", "На рабочем месте есть настройки услуги ЦВА");
                return false;
            }

            var exist2 = dbContext.service_defect.Where(ss => ss.workplace_id == workplace.id).FirstOrDefault();
            if (exist2 != null)
            {
                modelState.AddModelError("", "На рабочем месте есть настройки услуги Закачка брака");
                return false;
            }

            var exist3 = dbContext.service_sender.Where(ss => ss.workplace_id == workplace.id).FirstOrDefault();
            if (exist3 != null)
            {
                modelState.AddModelError("", "На рабочем месте есть настройки услуги Брак (Госреестр) на почту");
                return false;
            }

            var exist4 = dbContext.service_stock.Where(ss => ss.workplace_id == workplace.id).FirstOrDefault();
            if (exist4 != null)
            {
                modelState.AddModelError("", "На рабочем месте есть настройки услуги Сводное наличие");
                return false;
            }

            dbContext.workplace.Remove(workplace);
            dbContext.SaveChanges();

            return true;
        }

        public WorkplaceViewModel Rereg(int workplace_id, string person, string reason, ModelStateDictionary modelState)
        {

            workplace workplace = dbContext.workplace.Where(ss => ss.id == workplace_id && ss.is_deleted != 1).FirstOrDefault();
            if (workplace == null)
            {
                modelState.AddModelError("", "Не найдено рабочее место с кодом " + workplace_id.ToString());
                return null;
            }

            reregistration_order order = new reregistration_order();
            order.created_by = currUser.UserName;
            order.created_on = DateTime.Now;
            order.guid = Guid.NewGuid().ToString();
            order.is_deleted = 0;
            order.is_used = 0;
            order.manager_activation = 0;
            order.person = person;
            order.reason = reason;
            order.workplace_id = workplace_id;

            dbContext.reregistration_order.Add(order);

            dbContext.SaveChanges();

            var workplace_db = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
            return new WorkplaceViewModel(workplace_db);
        }
    }   
}

