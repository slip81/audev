﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IWorkplaceReregService : IAuBaseService
    {
        //
    }

    public class WorkplaceReregService : AuBaseService, IWorkplaceReregService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_workplace_rereg
                .Where(ss => ss.is_deleted != 1)
                .Select(ss => new WorkplaceReregViewModel()
                {
                    rereg_id = ss.rereg_id,
                    person = ss.person,
                    reason = ss.reason,
                    manager_activation = ss.manager_activation,
                    manager_activation_bool = ss.manager_activation == 1,
                    created_on = ss.created_on,
                    created_by = ss.created_by,
                    updated_on = ss.updated_on,
                    updated_by = ss.updated_by,
                    is_deleted = ss.is_deleted,
                    is_used = ss.is_used,
                    is_used_bool = ss.is_used == 1,
                    workplace_id = ss.workplace_id,
                    registration_key = ss.registration_key,
                    description = ss.description,
                    sales_id = ss.sales_id,
                    sales_name = ss.sales_name,
                    sales_address = ss.sales_address,
                    client_id = ss.client_id,
                    client_name = ss.client_name,
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {            
            return dbContext.vw_workplace_rereg
                .Where(ss => ss.client_id == parent_id && ss.is_deleted != 1)
                .Select(ss => new WorkplaceReregViewModel()
                {
                    rereg_id = ss.rereg_id,
                    person = ss.person,
                    reason = ss.reason,
                    manager_activation = ss.manager_activation,
                    manager_activation_bool = ss.manager_activation == 1,
                    created_on = ss.created_on,
                    created_by = ss.created_by,
                    updated_on = ss.updated_on,
                    updated_by = ss.updated_by,
                    is_deleted = ss.is_deleted,
                    is_used = ss.is_used,
                    is_used_bool = ss.is_used == 1,
                    workplace_id = ss.workplace_id,
                    registration_key = ss.registration_key,
                    description = ss.description,
                    sales_id = ss.sales_id,
                    sales_name = ss.sales_name,
                    sales_address = ss.sales_address,
                    client_id = ss.client_id,
                    client_name = ss.client_name,                    
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_workplace_rereg
                .Where(ss => ss.rereg_id == id && ss.is_deleted != 1)
                .Select(ss => new WorkplaceReregViewModel()
                {
                    rereg_id = ss.rereg_id,
                    person = ss.person,
                    reason = ss.reason,
                    manager_activation = ss.manager_activation,
                    manager_activation_bool = ss.manager_activation == 1,
                    created_on = ss.created_on,
                    created_by = ss.created_by,
                    updated_on = ss.updated_on,
                    updated_by = ss.updated_by,
                    is_deleted = ss.is_deleted,
                    is_used = ss.is_used,
                    is_used_bool = ss.is_used == 1,
                    workplace_id = ss.workplace_id,
                    registration_key = ss.registration_key,
                    description = ss.description,
                    sales_id = ss.sales_id,
                    sales_name = ss.sales_name,
                    sales_address = ss.sales_address,
                    client_id = ss.client_id,
                    client_name = ss.client_name,
                })
                .FirstOrDefault();
        }
        

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is WorkplaceReregViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            WorkplaceReregViewModel itemAdd = (WorkplaceReregViewModel)item;
            if (
                (itemAdd == null)
                ||
                (String.IsNullOrEmpty(itemAdd.registration_key))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            workplace workplace = dbContext.workplace.Where(ss => ss.id == itemAdd.workplace_id && ss.is_deleted != 1).FirstOrDefault();
            if (workplace == null)
            {
                modelState.AddModelError("", "Не найдено рабочее место с кодом " + itemAdd.workplace_id.ToString());
                return null;
            }

            reregistration_order order_existing = dbContext.reregistration_order.Where(ss => ss.workplace_id == itemAdd.workplace_id && ss.is_deleted != 1 && ss.manager_activation != 1).FirstOrDefault();
            if (order_existing != null)
            {
                modelState.AddModelError("", "Уже есть заявка на перерегистрацию данного рабочего места");
                return null;
            }

            reregistration_order order = new reregistration_order();
            order.created_by = currUser.UserName;
            order.created_on = DateTime.Now;
            order.guid = Guid.NewGuid().ToString();
            order.is_deleted = 0;
            order.is_used = 1;
            order.manager_activation = 1;
            order.person = currUser.UserName;
            order.reason = itemAdd.reason;
            order.workplace_id = itemAdd.workplace_id;

            dbContext.reregistration_order.Add(order);

            workplace.description = itemAdd.description;
            workplace.registration_key = itemAdd.registration_key;            

            dbContext.SaveChanges();

            var rereg_db = dbContext.vw_workplace_rereg.Where(ss => ss.rereg_id == order.id).FirstOrDefault();
            return new WorkplaceReregViewModel(rereg_db);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is WorkplaceReregViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            WorkplaceReregViewModel itemEdit = (WorkplaceReregViewModel)item;
            if (itemEdit == null) 
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            reregistration_order order = dbContext.reregistration_order.Where(ss => ss.id == itemEdit.rereg_id && ss.is_deleted != 1).FirstOrDefault();
            if (order == null)
            {
                modelState.AddModelError("", "Не найдена перерегистрация с кодом " + itemEdit.rereg_id.ToString());
                return null;
            }
            modelBeforeChanges = new WorkplaceReregViewModel(order);

            order.manager_activation = itemEdit.manager_activation_bool ? (short)1 : (short)0;
            order.updated_by = currUser.UserName;
            order.updated_on = DateTime.Now;

            dbContext.SaveChanges();

            var rereg_db = dbContext.vw_workplace_rereg.Where(ss => ss.rereg_id == order.id).FirstOrDefault();
            return new WorkplaceReregViewModel(rereg_db);
        }

    }   
}
