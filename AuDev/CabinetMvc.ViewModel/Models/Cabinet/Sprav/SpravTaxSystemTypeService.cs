﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravTaxSystemTypeService : IAuBaseService
    {
        //
    }

    public class SpravTaxSystemTypeService : AuBaseService, ISpravTaxSystemTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.tax_system_type.ProjectTo<SpravTaxSystemTypeViewModel>();
        }
    }
}