﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravStreetService : IAuBaseService
    {
        //
    }

    public class SpravStreetService : AuBaseService, ISpravStreetService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.street.Where(ss => ss.is_deleted != 1).OrderBy(ss => ss.name).Select(ss => new SpravStreetViewModel()
            {
                id = ss.id,
                name = ss.name,
                district_id = ss.district_id,                
            }
            );
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.street.Where(ss => ss.is_deleted != 1 && ss.district_id == parent_id).OrderBy(ss => ss.name).Select(ss => new SpravStreetViewModel()
            {
                id = ss.id,
                name = ss.name,
                district_id = ss.district_id,
            }
            );
        }

    }
}