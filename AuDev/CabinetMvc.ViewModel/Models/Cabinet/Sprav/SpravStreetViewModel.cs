﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SpravStreetViewModel : AuBaseViewModel
    {
        public SpravStreetViewModel()
            :base()
        {
            //
        }

        public int id { get; set; }        
        public string name { get; set; }
        public Nullable<int> district_id { get; set; }
    }
}