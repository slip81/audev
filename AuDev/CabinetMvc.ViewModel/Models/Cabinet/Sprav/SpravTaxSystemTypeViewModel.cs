﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SpravTaxSystemTypeViewModel : AuBaseViewModel
    {
        public SpravTaxSystemTypeViewModel()
            :base()
        {
            //
        }

        public int system_type_id { get; set; }
        public string system_type_name { get; set; }        
    }
}