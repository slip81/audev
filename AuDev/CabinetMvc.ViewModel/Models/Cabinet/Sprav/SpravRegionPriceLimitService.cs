﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravRegionPriceLimitService : IAuBaseService
    {
        bool Delete_byRegion(int region_id, ModelStateDictionary modelState);
    }

    public class SpravRegionPriceLimitService : AuBaseService, ISpravRegionPriceLimitService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_region_price_limit
                .Where(ss => ss.item_id == id)
                .ProjectTo<SpravRegionPriceLimitViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_region_price_limit
                .Where(ss => ss.region_id == parent_id)
                .ProjectTo<SpravRegionPriceLimitViewModel>()
                ;
        }


        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SpravRegionPriceLimitViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            SpravRegionPriceLimitViewModel itemEdit = (SpravRegionPriceLimitViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты наценки");
                return null;
            }

            region_price_limit region_price_limit = null;
            if (itemEdit.is_price_limit_exists)
            {
                region_price_limit = dbContext.region_price_limit.Where(ss => ss.region_id == itemEdit.region_id && ss.limit_type_id == itemEdit.limit_type_id).FirstOrDefault();
                region_price_limit.percent_value = itemEdit.percent_value;
            }
            else
            {
                region_price_limit = new region_price_limit();
                region_price_limit.region_id = itemEdit.region_id;
                region_price_limit.limit_type_id = itemEdit.limit_type_id;
                region_price_limit.percent_value = itemEdit.percent_value;
                dbContext.region_price_limit.Add(region_price_limit);
            }

            dbContext.SaveChanges();

            return xGetItem(region_price_limit.item_id);
        }

        public bool Delete_byRegion(int region_id, ModelStateDictionary modelState)
        {
            try
            {
                region region = dbContext.region.Where(ss => ss.id == region_id && ss.is_deleted != 1).FirstOrDefault();
                if (region == null)
                {
                    modelState.AddModelError("", "Не найден регион с кодом " + region_id.ToString());
                    return false;
                }

                dbContext.region_price_limit.RemoveRange(dbContext.region_price_limit.Where(ss => ss.region_id == region_id));
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }
    }
}