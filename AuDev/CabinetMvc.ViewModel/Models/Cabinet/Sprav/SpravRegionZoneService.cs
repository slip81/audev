﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravRegionZoneService : IAuBaseService
    {
        //
    }

    public class SpravRegionZoneService : AuBaseService, ISpravRegionZoneService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_region_zone
                .Where(ss => ss.zone_id == id && ss.is_deleted != 1)
                .ProjectTo<SpravRegionZoneViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_region_zone
                .Where(ss => ss.region_id == parent_id && ss.is_deleted != 1)
                .ProjectTo<SpravRegionZoneViewModel>();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SpravRegionZoneViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            SpravRegionZoneViewModel itemAdd = (SpravRegionZoneViewModel)item;
            if ((itemAdd == null) || (String.IsNullOrWhiteSpace(itemAdd.zone_name)))
            {
                modelState.AddModelError("", "Не заданы атрибуты зоны");
                return null;
            }

            region region = dbContext.region.Where(ss => ss.id == itemAdd.region_id && ss.is_deleted != 1).FirstOrDefault();
            if (region == null)
            {
                modelState.AddModelError("", "Не найден регион с кодом " + itemAdd.region_id.ToString());
                return null;
            }

            region_zone region_zone_existing = dbContext.region_zone
                .Where(ss => ss.zone_id != itemAdd.zone_id && ss.region_id == itemAdd.region_id && ss.is_deleted != 1 && ss.zone_name != null && ss.zone_name.Trim().ToLower().Equals(itemAdd.zone_name.Trim().ToLower()))
                .FirstOrDefault();
            if (region_zone_existing != null)
            {
                modelState.AddModelError("", "Уже есть зона с таким наименованием");
                return null;
            }


            region_zone region_zone = new region_zone();
            region_zone.region_id = itemAdd.region_id;
            region_zone.zone_name = itemAdd.zone_name;
            region_zone.is_deleted = 0;
            dbContext.region_zone.Add(region_zone);

            dbContext.SaveChanges();

            return xGetItem(region_zone.zone_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SpravRegionZoneViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            SpravRegionZoneViewModel itemEdit = (SpravRegionZoneViewModel)item;
            if ((itemEdit == null) || (String.IsNullOrWhiteSpace(itemEdit.zone_name)))
            {
                modelState.AddModelError("", "Не заданы атрибуты зоны");
                return null;
            }

            region_zone region_zone_existing = dbContext.region_zone.Where(ss => ss.zone_id != itemEdit.zone_id && ss.region_id == itemEdit.region_id && ss.is_deleted != 1 && ss.zone_name != null && ss.zone_name.Trim().ToLower().Equals(itemEdit.zone_name.Trim().ToLower())).FirstOrDefault();
            if (region_zone_existing != null)
            {
                modelState.AddModelError("", "Уже есть зона с таким наименованием");
                return null;
            }

            region region = dbContext.region.Where(ss => ss.id == itemEdit.region_id && ss.is_deleted != 1).FirstOrDefault();
            if (region == null)
            {
                modelState.AddModelError("", "Не найден регион с кодом " + itemEdit.region_id.ToString());
                return null;
            }

            region_zone region_zone = dbContext.region_zone.Where(ss => ss.zone_id == itemEdit.zone_id && ss.is_deleted != 1).FirstOrDefault();
            if (region_zone == null)
            {
                modelState.AddModelError("", "Не найдена зона с кодом " + itemEdit.zone_id.ToString());
                return null;
            }

            modelBeforeChanges = new SpravRegionZoneViewModel() { zone_id = region_zone.zone_id, zone_name = region_zone.zone_name, region_id = region_zone.region_id };

            region_zone.zone_name = itemEdit.zone_name;
            dbContext.SaveChanges();

            return xGetItem(region_zone.zone_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SpravRegionZoneViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            SpravRegionZoneViewModel itemDel = (SpravRegionZoneViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры зоны");
                return false;
            }

            region_zone region_zone = dbContext.region_zone.Where(ss => ss.region_id == itemDel.region_id && ss.is_deleted != 1).FirstOrDefault();
            if (region_zone == null)
            {
                modelState.AddModelError("", "Не найдена зона с кодом " + itemDel.region_id.ToString());
                return false;
            }

            dbContext.region_zone.Remove(region_zone);
            dbContext.SaveChanges();

            return true;
        }

    }
}