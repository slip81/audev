﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SpravDistrictViewModel : AuBaseViewModel
    {
        public SpravDistrictViewModel()
            :base()
        {
            //
        }

        public int id { get; set; }        
        public string name { get; set; }
        public Nullable<int> city_id { get; set; }
    }
}