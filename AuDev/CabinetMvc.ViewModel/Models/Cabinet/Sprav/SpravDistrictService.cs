﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravDistrictService : IAuBaseService
    {
        //
    }

    public class SpravDistrictService : AuBaseService, ISpravDistrictService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.district.Where(ss => ss.is_deleted != 1).OrderBy(ss => ss.name).Select(ss => new SpravDistrictViewModel()
            {
                id = ss.id,
                name = ss.name,
                city_id = ss.city_id,
            }
            );
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.district.Where(ss => ss.is_deleted != 1 && ss.city_id == parent_id).OrderBy(ss => ss.name).Select(ss => new SpravDistrictViewModel()
            {
                id = ss.id,
                name = ss.name,
                city_id = ss.city_id,
            }
            );
        }

    }
}