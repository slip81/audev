﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravCityService : IAuBaseService
    {
        //
    }

    public class SpravCityService : AuBaseService, ISpravCityService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.city.Where(ss => ss.is_deleted != 1).OrderBy(ss => ss.name).Select(ss => new SpravCityViewModel()
            {
                id = ss.id,
                name = ss.name,
                region_id = ss.region_id,
            }
            );
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.city.Where(ss => ss.is_deleted != 1 && ss.region_id == parent_id).OrderBy(ss => ss.name).Select(ss => new SpravCityViewModel()
            {
                id = ss.id,
                name = ss.name,
                region_id = ss.region_id,
            }
            );
        }

    }
}