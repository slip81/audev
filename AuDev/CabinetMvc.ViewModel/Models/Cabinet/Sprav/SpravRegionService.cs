﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravRegionService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_byIdList(List<int> idList);
    }

    public class SpravRegionService : AuBaseService, ISpravRegionService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_region
                .Where(ss => ss.region_id == id && ss.is_deleted != 1)
                .Select(ss => new SpravRegionViewModel()
            {
                id = ss.region_id,
                name = ss.region_name,
                is_price_limit_exists = ss.is_price_limit_exists,
            }
            ).FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_region
                .Where(ss => ss.is_deleted != 1)
                .OrderBy(ss => ss.region_name)
                .Select(ss => new SpravRegionViewModel()
            {
                id = ss.region_id,
                name = ss.region_name,
                is_price_limit_exists = ss.is_price_limit_exists,
            }
            );
        }

        public IQueryable<AuBaseViewModel> GetList_byIdList(List<int> idList)
        {
            return dbContext.region.Where(ss => ss.is_deleted != 1 && idList.Contains(ss.id))
                .OrderBy(ss => ss.name).Select(ss => new SpravRegionViewModel()
            {
                id = ss.id,
                name = ss.name,
            }
            );
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SpravRegionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            SpravRegionViewModel itemAdd = (SpravRegionViewModel)item;
            if ((itemAdd == null) || (String.IsNullOrWhiteSpace(itemAdd.name)))
            {
                modelState.AddModelError("", "Не заданы атрибуты региона");
                return null;
            }

            region region_existing = dbContext.region
                .Where(ss => ss.id != itemAdd.id && ss.is_deleted != 1 && ss.name != null && ss.name.Trim().ToLower().Equals(itemAdd.name.Trim().ToLower()))
                .FirstOrDefault();
            if (region_existing != null)
            {
                modelState.AddModelError("", "Уже есть регион с таким наименованием");
                return null;
            }


            region region = new region();
            region.name = itemAdd.name;
            region.guid = new Guid().ToString();            
            region.is_deleted = 0;
            dbContext.region.Add(region);

            dbContext.SaveChanges();

            return xGetItem(region.id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SpravRegionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            SpravRegionViewModel itemEdit = (SpravRegionViewModel)item;
            if ((itemEdit == null) || (String.IsNullOrWhiteSpace(itemEdit.name)))
            {
                modelState.AddModelError("", "Не заданы атрибуты региона");
                return null;
            }

            region region_existing = dbContext.region.Where(ss => ss.id != itemEdit.id && ss.is_deleted != 1 && ss.name != null && ss.name.Trim().ToLower().Equals(itemEdit.name.Trim().ToLower())).FirstOrDefault();
            if (region_existing != null)
            {
                modelState.AddModelError("", "Уже есть регион с таким наименованием");
                return null;
            }

            region region = dbContext.region.Where(ss => ss.id == itemEdit.id && ss.is_deleted != 1).FirstOrDefault();
            if (region == null)
            {
                modelState.AddModelError("", "Не найден регион с кодом " + itemEdit.id.ToString());
                return null;
            }
  
            modelBeforeChanges = new SpravRegionViewModel() { id = region.id, name = region.name, };

            region.name = itemEdit.name;
            dbContext.SaveChanges();

            return xGetItem(region.id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SpravRegionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            SpravRegionViewModel itemDel = (SpravRegionViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры региона");
                return false;
            }

            region region = dbContext.region.Where(ss => ss.id == itemDel.id && ss.is_deleted != 1).FirstOrDefault();
            if (region == null)
            {
                modelState.AddModelError("", "Не найден регион с кодом " + itemDel.id.ToString());
                return false;
            }

            city city = dbContext.city.Where(ss => ss.region_id == itemDel.id).FirstOrDefault();
            if (city != null)
            {
                modelState.AddModelError("", "По данному региону есть города");
                return false;
            }

            dbContext.region_zone.RemoveRange(dbContext.region_zone.Where(ss => ss.region_id == itemDel.id));
            dbContext.region.Remove(region);
            dbContext.SaveChanges();

            return true;            
        }
    }
}