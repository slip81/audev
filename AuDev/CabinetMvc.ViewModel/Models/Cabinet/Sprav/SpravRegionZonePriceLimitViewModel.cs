﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SpravRegionZonePriceLimitViewModel : AuBaseViewModel
    {
        public SpravRegionZonePriceLimitViewModel()
            :base()
        {
            //vw_region_zone_price_limit
        }

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }
        
        [Display(Name = "Зона")]
        public int zone_id { get; set; }

        [Display(Name = "Тип наценки")]
        public int limit_type_id { get; set; }

        [Display(Name = "Тип наценки")]
        public string limit_type_name { get; set; }

        [Display(Name = "Процент наценки")]
        public Nullable<decimal> percent_value { get; set; }

        [Display(Name = "Есть наценка")]
        public bool is_price_limit_exists { get; set; }
    }
}