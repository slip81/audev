﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SpravRegionViewModel :  AuBaseViewModel
    {
        public SpravRegionViewModel()
            :base()
        {
            //
        }

        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Не задано наименование")]
        public string name { get; set; }

        [Display(Name = "Есть наценки")]
        public bool is_price_limit_exists { get; set; }

        [Display(Name = "ChargeParamBtn")]
        [JsonIgnore()]
        public string ChargeParamBtn
        {
            get
            {
                string btnClass = is_price_limit_exists ? "k-edit" : "k-i-plus";
                string spanClass = is_price_limit_exists ? "text-primary" : "text-success";
                return "<button class='k-button k-button-icontext' type='button' title='Наценки на ЖВ' onclick='regionList.onChargeParamBtnClick(" + id.ToString() + ")'><span class='span-charge-param-icon k-icon " + btnClass + "'></span><span class='span-charge-param " + spanClass + "'>Наценки на ЖВ</span></button>";
            }
        }
    }
}