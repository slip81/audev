﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravProjectService : IAuBaseService
    {
        //
    }

    public class SpravProjectService : AuBaseService, ISpravProjectService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.service.Where(ss => ss.is_deleted != 1 && !ss.parent_id.HasValue).OrderBy(ss => ss.name).Select(ss => new SpravProjectViewModel()
            {
                id = ss.id,                
                name = ss.name,
            }
            );
        }
    }
}