﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SpravProjectViewModel :  AuBaseViewModel
    {
        public SpravProjectViewModel()
            :base()
        {
            //
        }

        public int id { get; set; }
        public string guid { get; set; }
        public string name { get; set; }
    }
}