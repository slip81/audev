﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface ISpravRegionZonePriceLimitService : IAuBaseService
    {
        bool Delete_byZone(int zone_id, ModelStateDictionary modelState);
    }

    public class SpravRegionZonePriceLimitService : AuBaseService, ISpravRegionZonePriceLimitService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_region_zone_price_limit
                .Where(ss => ss.item_id == id)
                .ProjectTo<SpravRegionZonePriceLimitViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_region_zone_price_limit
                .Where(ss => ss.zone_id == parent_id)
                .ProjectTo<SpravRegionZonePriceLimitViewModel>()
                ;
        }


        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is SpravRegionZonePriceLimitViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            SpravRegionZonePriceLimitViewModel itemEdit = (SpravRegionZonePriceLimitViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты наценки");
                return null;
            }

            region_zone_price_limit region_zone_price_limit = null;
            if (itemEdit.is_price_limit_exists)
            {
                region_zone_price_limit = dbContext.region_zone_price_limit.Where(ss => ss.zone_id == itemEdit.zone_id && ss.limit_type_id == itemEdit.limit_type_id).FirstOrDefault();
                region_zone_price_limit.percent_value = itemEdit.percent_value;
            }
            else
            {
                region_zone_price_limit = new region_zone_price_limit();
                region_zone_price_limit.zone_id = itemEdit.zone_id;
                region_zone_price_limit.limit_type_id = itemEdit.limit_type_id;
                region_zone_price_limit.percent_value = itemEdit.percent_value;
                dbContext.region_zone_price_limit.Add(region_zone_price_limit);
            }

            dbContext.SaveChanges();

            return xGetItem(region_zone_price_limit.item_id);
        }

        public bool Delete_byZone(int zone_id, ModelStateDictionary modelState)
        {
            try
            {
                region_zone region_zone = dbContext.region_zone.Where(ss => ss.zone_id == zone_id && ss.is_deleted != 1).FirstOrDefault();
                if (region_zone == null)
                {
                    modelState.AddModelError("", "Не найдена зона с кодом " + zone_id.ToString());
                    return false;
                }

                dbContext.region_zone_price_limit.RemoveRange(dbContext.region_zone_price_limit.Where(ss => ss.zone_id == zone_id));
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }
    }
}