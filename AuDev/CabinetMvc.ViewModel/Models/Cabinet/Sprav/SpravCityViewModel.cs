﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SpravCityViewModel : AuBaseViewModel
    {
        public SpravCityViewModel()
            :base()
        {
            //
        }

        public int id { get; set; }        
        public string name { get; set; }
        public Nullable<int> region_id { get; set; }
    }
}