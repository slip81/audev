﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class SpravRegionZoneViewModel : AuBaseViewModel
    {
        public SpravRegionZoneViewModel()
            :base()
        {
            //vw_region_zone
        }

        [Display(Name = "Код")]
        public int zone_id { get; set; }

        [Display(Name = "Наименование")]
        public string zone_name { get; set; }

        [Display(Name = "Регион")]
        public int region_id { get; set; }

        [Display(Name = "Регион")]
        public string region_name { get; set; }

        [Display(Name = "is_deleted")]
        public Nullable<short> is_deleted { get; set; }

        [Display(Name = "Есть наценки")]
        public bool is_price_limit_exists { get; set; }

        [Display(Name = "ZoneChargeParamBtn")]
        [JsonIgnore()]
        public string ZoneChargeParamBtn
        {
            get
            {
                string btnClass = is_price_limit_exists ? "k-edit" : "k-i-plus";
                string spanClass = is_price_limit_exists ? "text-primary" : "text-success";
                return "<button class='k-button k-button-icontext' type='button' title='Наценки на ЖВ' onclick='regionList.onZoneChargeParamBtnClick(" + region_id.ToString() + "," + zone_id.ToString() + ")'><span class='span-zone-charge-param-icon k-icon " + btnClass + "'></span><span class='span-zone-charge-param " + spanClass + "'>Наценки на ЖВ</span></button>";
            }
        }
    }
}