﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientServiceUnregService : IAuBaseService
    {
        bool DeleteList(IEnumerable<ClientServiceUnregViewModel> servListForDel, ModelStateDictionary modelState);
    }

    public class ClientServiceUnregService : AuBaseService, IClientServiceUnregService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return from t1 in dbContext.vw_client_service_unregistered
                   where t1.client_id == parent_id
                   //where t1.client_id == -1
                   group t1 by new { t1.client_id, t1.service_id, t1.version_id, t1.service_name, t1.version_name, t1.service_description }
                       into grp
                       select new ClientServiceUnregViewModel
                       {
                           id = 0,
                           client_id = grp.Key.client_id,
                           service_id = grp.Key.service_id,
                           service_name = grp.Key.service_name,
                           service_description = grp.Key.service_description,
                           version_id = grp.Key.version_id,
                           version_name = grp.Key.version_name,
                           ServiceCnt = grp.Select(x => x.id).Count(),                           
                       };
        }

        public bool DeleteList(IEnumerable<ClientServiceUnregViewModel> servListForDel, ModelStateDictionary modelState)
        {
            if ((servListForDel == null) || (servListForDel.Count() <= 0))
            {
                modelState.AddModelError("", "Список для удаления пуст");
                return false;
            }

            var forDelList = servListForDel.Where(ss => ss.ForDel).ToList();
            if ((forDelList == null) || (forDelList.Count() <= 0))
            {
                modelState.AddModelError("", "Список для удаления пуст");
                return false;
            }

            foreach (var forDel in forDelList)
            {
                var itemsForDel = from t1 in dbContext.vw_client_service_unregistered
                                  from t2 in dbContext.license
                                  where t1.client_id == forDel.client_id && t1.service_id == forDel.service_id && t1.version_id == forDel.version_id 
                                  && t1.license_id == t2.id
                                  select t2;
                dbContext.license.RemoveRange(itemsForDel);

                /*
                var itemForDel = dbContext.vw_client_service_unregistered.Where(ss => ss.client_id == forDel.client_id && ss.service_id == forDel.service_id && ss.version_id == forDel.version_id).FirstOrDefault();
                if (itemForDel != null)
                {
                    dbContext.license.RemoveRange(dbContext.license.Where(ss => ss.id == itemForDel.license_id));
                }
                */
            }

            dbContext.SaveChanges();

            return true;
        }

    }

}
