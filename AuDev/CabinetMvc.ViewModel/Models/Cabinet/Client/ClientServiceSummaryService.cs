﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientServiceSummaryService : IAuBaseService
    {
        //
    }

    public class ClientServiceSummaryService : AuBaseService, IClientServiceSummaryService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_client_service_summary.Where(ss => ss.client_id == parent_id)
                .ProjectTo<ClientServiceSummaryViewModel>();
        }

    }
}
