﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientServiceViewModel : AuBaseViewModel
    {

        public ClientServiceViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT_SERVICE)
        {
            //
        }

        public ClientServiceViewModel(vw_client_service item)
            : base(Enums.MainObjectType.CAB_CLIENT_SERVICE)
        {            
            id = item.id;
            client_id = item.client_id;
            client_name = item.client_name;
            sales_id = item.sales_id;
            sales_name = item.sales_name;
            sales_address = item.sales_address;
            workplace_id = item.workplace_id;
            workplace_registration_key = item.workplace_registration_key;
            workplace_description = item.workplace_description;
            service_id = item.service_id;
            service_name = item.service_name;
            service_description = item.service_description;
            version_id = item.version_id;
            version_name = item.version_name;
            is_service = item.is_service;
            service_priority = item.service_priority;
            activation_key = item.activation_key;
            have_spec = item.have_spec;
            max_workplace_cnt = item.max_workplace_cnt;
            ServiceCnt = 0;
        }
     
        [Key()]
        [Display(Name = "Код")]
        public long id { get; set; }

        [Display(Name = "Организация")]
        public int client_id { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }

        [Display(Name = "Торговая точка")]
        public int sales_id { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }

        [Display(Name = "Адрес")]
        public string sales_address { get; set; }

        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }

        [Display(Name = "Ключ регистрации")]
        public string workplace_registration_key { get; set; }

        [Display(Name = "Рабочее место")]
        public string workplace_description { get; set; }

        [Display(Name = "Услуга")]
        public Nullable<int> service_id { get; set; }

        [Display(Name = "Услуга")]
        public string service_name { get; set; }
        
        [Display(Name = "Описание услуги")]
        public string service_description { get; set; }

        [Display(Name = "Версия")]
        public Nullable<int> version_id { get; set; }

        [Display(Name = "Версия")]
        public string version_name { get; set; }

        [Display(Name = "Сервис")]
        public bool is_service { get; set; }

        [Display(Name = "Приоритет")]
        public Nullable<int> service_priority { get; set; }

        [Display(Name = "Количество")]
        public Nullable<int> ServiceCnt { get; set; }

        [Display(Name = "Ключ активации")]
        public string activation_key { get; set; }
        
        [Display(Name = "Установлено")]
        public int is_registered { get; set; }

        [Display(Name = "С параметрами")]
        public Nullable<bool> have_spec { get; set; }

        [Display(Name = "Макс. кол-во раб. мест")]
        public Nullable<int> max_workplace_cnt { get; set; }

        [Display(Name = "Убрать")]
        [JsonIgnore()]
        public string DelServiceLink
        {
            get
            {
                //return (is_registered == 0 ? "" : "<a href='/' class='k-button btn-apply-service' onclick='clientEdit.onDelServiceClick(" + workplace_id.ToString() + "," + version_id.ToString() + "); return false'>Убрать</a>");
                return (is_registered == 0 ? "" : "<button class='k-button k-button-icontext' title='Убрать услугу' onclick='clientEdit.onDelServiceClick(" + workplace_id.ToString() + "," + version_id.ToString() + "); return false'><span class='k-icon k-i-close'></span></button>");
            }
        }

        [Display(Name = "Установить")]
        [JsonIgnore()]
        public string ApplyServiceLink
        {
            get
            {
                //return (is_registered == 1 ? "" : "<a href='/' class='k-button btn-apply-service' onclick='clientEdit.onApplyServiceClick(" + workplace_id.ToString() + "," + version_id.ToString() + "); return false'>Установить</a>");
                return (is_registered == 1 ? "" : "<button class='k-button k-button-icontext' title='Установить услугу' onclick='clientEdit.onApplyServiceClick(" + workplace_id.ToString() + "," + version_id.ToString() + "); return false'><span class='k-icon k-i-tick'></span></button>");
            }
        }

        [Display(Name = "Установить")]
        [JsonIgnore()]
        public string ApplyServiceChb
        {
            get
            {                
                return (is_registered == 1 ? "" : "<input type='checkbox' title='Отметить услугу для установки' class='chb-apply-service'/>");
            }
        }

    }
}

