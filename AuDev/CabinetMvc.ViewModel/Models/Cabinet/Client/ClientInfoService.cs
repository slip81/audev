﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientInfoService : IAuBaseService
    {
        //
    }

    public class ClientInfoService : AuBaseService, IClientInfoService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_client_info.Where(ss => ss.client_id == id)
                .ProjectTo<ClientInfoViewModel>()
                .FirstOrDefault();
        }
    }
}
