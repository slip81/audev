﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientServiceSummaryViewModel : AuBaseViewModel
    {

        public ClientServiceSummaryViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT_SERVICE_SUMMARY)
        {
            //vw_client_service_summary
        }
        
        //[Key()]
        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Key()]
        [Display(Name = "Услуга")]
        public Nullable<int> service_id { get; set; }

        [Display(Name = "Установлено в точках")]
        public Nullable<long> sales_cnt { get; set; }
    }
}

