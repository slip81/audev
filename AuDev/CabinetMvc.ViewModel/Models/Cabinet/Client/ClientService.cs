﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_Sprav();
        AuBaseViewModel RegisterClient(AuBaseViewModel item, ModelStateDictionary modelState);
        Tuple<bool, string> ClientActivate(int client_id, ModelStateDictionary modelState);        
        IQueryable<ClientViewModel> GetList_byFilter(string client_name, bool is_jur, bool is_phis, string wp_key, string reg_key, int? service_id);
        ClientViewModel GetItem_Short(int client_id);
        IQueryable<ClientViewModel> GetList_byService(int service_id);
        IQueryable<ClientViewModel> GetList_Asna();
    }

    public class ClientService : AuBaseService, IClientService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.allclients
                .Select(ss => new ClientViewModel()
                {
                    city_id = ss.city_id,
                    city_name = ss.city_name,
                    client_name = ss.client_name,
                    created_by = ss.created_by,
                    id = ss.id,
                    is_activated = ss.is_activated == 1,
                    last_activity_date = ss.last_activity_date,
                    phone = ss.phone,
                    reg_date = ss.reg_date,
                    region_id = ss.region_id,
                    region_name = ss.region_name,
                    sales_count = ss.sales_count,
                    user_id = ss.user_id,
                    user_name = ss.user_name,
                    is_phis = ss.is_phis == 1,
                    is_phis_str = ss.is_phis == 1 ? "Да" : "Нет",
                    is_activated_str = ss.is_activated == 1 ? "Да" : "Нет",
                    contact_person = ss.contact_person,
                    business_id = ss.business_id,
                    has_business_id = String.IsNullOrEmpty(ss.business_id) ? "" : "ДС",
                    inn = ss.inn,
                    kpp = ss.kpp,
                    legal_address = ss.legal_address,
                    absolute_address = ss.absolute_address,
                    site = ss.site,
                    sm_id = ss.sm_id,
                    state_id = ss.state_id,
                    state_name = ss.state_name,
                    region_zone_id = ss.region_zone_id,
                    tax_system_type_id = ss.tax_system_type_id,
                }
                );
        }

        public IQueryable<AuBaseViewModel> GetList_Sprav()
        {
            return dbContext.allclients
                .Where(ss => ss.is_phis != 1)
                .Select(ss => new ClientViewModel()
                {
                    client_name = ss.client_name,
                    id = ss.id,
                    business_id = ss.business_id,
                    has_business_id = String.IsNullOrEmpty(ss.business_id) ? "" : "ДС",
                    site = ss.site,
                    RegionName_Selected = "-",
                    CityName_Selected = "-",
                }
                );
        }


        public IQueryable<ClientViewModel> GetList_byFilter(string client_name, bool is_jur, bool is_phis, string wp_key, string reg_key, int? service_id)
        {
            IQueryable<allclients> cl = dbContext.allclients
                .Where(ss => 1 == 1
                    //&& ((String.IsNullOrWhiteSpace(client_name)) || ((!String.IsNullOrWhiteSpace(client_name)) && (ss.client_name.Trim().ToLower().Contains(client_name.Trim().ToLower()))))                    
                    && ((is_jur) || ((!is_jur) && (ss.is_phis == 1)))
                    && ((is_phis) || ((!is_phis) && (ss.is_phis != 1)))
                );

            if (!String.IsNullOrWhiteSpace(client_name))
            {
                cl = cl.Where(ss => ss.client_name.Trim().ToLower().Contains(client_name.Trim().ToLower()));
            }

            if (!String.IsNullOrWhiteSpace(wp_key))
            {
                cl = from t1 in cl
                     from t2 in dbContext.vw_workplace
                     where t1.id == t2.client_id
                     && t2.registration_key.Trim().ToLower().Equals(wp_key.Trim().ToLower())
                     select t1;
            }

            if (!String.IsNullOrWhiteSpace(reg_key))
            {                
                cl = from t1 in cl    
                     from t2 in dbContext.vw_client_service
                     where t1.id == t2.client_id
                     && t2.activation_key.Trim().ToLower().Equals(reg_key.Trim().ToLower())                     
                     select t1;
            }

            if (service_id.GetValueOrDefault(0) > 0)
            {                
                cl = from t1 in cl
                     from t2 in dbContext.vw_client_service
                     where t1.id == t2.client_id                     
                     && t2.service_id == (int)service_id
                     select t1;
            }
            
            return cl
                .Distinct()
                .Select(ss => new ClientViewModel()
            {
                city_id = ss.city_id,
                city_name = ss.city_name,
                client_name = ss.client_name,
                created_by = ss.created_by,
                id = ss.id,
                is_activated = ss.is_activated == 1,
                last_activity_date = ss.last_activity_date,
                phone = ss.phone,
                reg_date = ss.reg_date,
                region_id = ss.region_id,
                region_name = ss.region_name,
                sales_count = ss.sales_count,
                user_id = ss.user_id,
                user_name = ss.user_name,
                is_phis = ss.is_phis == 1,
                is_phis_str = ss.is_phis == 1 ? "Да" : "Нет",
                is_activated_str = ss.is_activated == 1 ? "Да" : "Нет",
                contact_person = ss.contact_person,
                business_id = ss.business_id,
                has_business_id = String.IsNullOrEmpty(ss.business_id) ? "" : "ДС",
                inn = ss.inn,
                kpp = ss.kpp,
                legal_address = ss.legal_address,
                absolute_address = ss.absolute_address,
                site = ss.site,
                sm_id = ss.sm_id,
                state_id = ss.state_id,
                state_name = ss.state_name,
                region_zone_id = ss.region_zone_id,
                tax_system_type_id = ss.tax_system_type_id,
            });
        }

        public IQueryable<ClientViewModel> GetList_byService(int service_id)
        {
            if (currUser.IsAdmin)
            {
                return (from t1 in dbContext.allclients
                        from t2 in dbContext.vw_client_service
                        where t1.id == t2.client_id
                        && t2.service_id == service_id
                        select new ClientViewModel()
                        {                            
                            id = t1.id,
                            client_name = t1.client_name,
                        })
                       .Distinct()
                       .OrderBy(ss => ss.client_name);
            }
            else
            {
                List<ClientViewModel> res = new List<ClientViewModel>() 
                { 
                    new ClientViewModel() 
                    {
                        id = (int)currUser.CabClientId,
                        client_name = currUser.clientInfo.client_name,                        
                    }
                };
                
                return res.AsQueryable();
            }
        }

        public IQueryable<ClientViewModel> GetList_Asna()
        {
            return dbContext.vw_asna_client
                .Select(ss => new ClientViewModel()
                    {
                        id = ss.client_id,
                        client_name = ss.client_name,
                    });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {            
            var client = dbContext.allclients.Where(ss => ss.id == id).FirstOrDefault();            
            //if (client == null) return null;

            ClientViewModel res = client == null ? new ClientViewModel() : new ClientViewModel(client);

            return res;
        }

        public ClientViewModel GetItem_Short(int client_id)
        {
            return dbContext.allclients.Where(ss => ss.id == client_id)
                .Select(ss => new ClientViewModel()
                {
                    id = ss.id,
                    client_name = ss.client_name,
                    city_id = ss.city_id,
                    region_id = ss.region_id,
                    region_zone_id = ss.region_zone_id,
                    tax_system_type_id = ss.tax_system_type_id,
                }                
                )
                .FirstOrDefault();
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ClientViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ClientViewModel clientEdit = (ClientViewModel)item;
            if (
                (clientEdit == null)
                ||
                (String.IsNullOrEmpty(clientEdit.client_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var client_old = dbContext.client.Where(ss => ss.id == clientEdit.id).FirstOrDefault();
            if (client_old == null)
            {
                modelState.AddModelError("", "Не найден клиент с кодом " + clientEdit.id.ToString());
                return null;
            }

            allclients existing_client = dbContext.allclients.Where(ss => ss.id != clientEdit.id && ss.client_name.Trim().ToLower().Equals(clientEdit.client_name.Trim().ToLower())).FirstOrDefault();
            if (existing_client != null)
            {
                modelState.AddModelError("", "Уже есть клиент с таким наименованием");
                return null;
            }

            modelBeforeChanges = new ClientViewModel(client_old);

            int curr_region_id = 0;
            int curr_city_id = 0;
            region region = null;
            city city = null;
            if (clientEdit.RegionName_Changed)
            {
                region = dbContext.region.Where(ss => ss.name.Trim().ToLower().Equals(clientEdit.RegionName_Selected.Trim().ToLower())).FirstOrDefault();
                if (region == null)
                {
                    region = new region();
                    region.guid = Guid.NewGuid().ToString();
                    region.name = clientEdit.RegionName_Selected;
                    region.is_deleted = 0;
                    dbContext.region.Add(region);
                    dbContext.SaveChanges();
                    curr_region_id = region.id;
                }
                curr_region_id = region.id;
            }
            else
            {
                curr_region_id = (int)clientEdit.region_id;
            }

            if ((clientEdit.RegionName_Changed) || (clientEdit.CityName_Changed))
            {
                city = dbContext.city.Where(ss => ss.region_id == curr_region_id && ss.name.Trim().ToLower().Equals(clientEdit.CityName_Selected.Trim().ToLower())).FirstOrDefault();
                if (city == null)
                {
                    city = new city();
                    city.guid = Guid.NewGuid().ToString();
                    city.name = clientEdit.CityName_Selected;
                    city.is_deleted = 0;
                    city.region_id = curr_region_id;
                    dbContext.city.Add(city);
                    dbContext.SaveChanges();                    
                }
                curr_city_id = city.id;
            }
            else
            {
                curr_city_id = (int)clientEdit.city_id;
            }

            var client = dbContext.client.Where(ss => ss.id == clientEdit.id).FirstOrDefault();

            client.absolute_address = clientEdit.absolute_address;
            client.activated_by = clientEdit.activated_by;
            client.activated_on = clientEdit.activated_on;
            client.balance = clientEdit.balance;
            client.bank = clientEdit.bank;
            client.bik = clientEdit.bik;            
            client.cell = clientEdit.phone;
            client.city_bank = clientEdit.city_bank;
            client.client_role = clientEdit.client_role;
            client.contact_person = clientEdit.contact_person;            
            client.dt_birth = clientEdit.dt_birth;
            client.dt_sm_sync = clientEdit.dt_birth;
            client.foundation_document = clientEdit.foundation_document;            
            client.hardware_id = clientEdit.hardware_id;
            client.inn = clientEdit.inn;
            client.kpp = clientEdit.kpp;
            client.is_in_summary_order = clientEdit.is_in_summary_order == true ? (short)1 : (short)0;
            client.is_moderated = clientEdit.is_moderated == true ? (short)1 : (short)0;
            client.is_phis = clientEdit.is_phis == true ? (short)1 : (short)0;
            client.kpp = clientEdit.kpp;
            client.legal_address = clientEdit.legal_address;
            client.license_date = clientEdit.license_date;
            client.license_number = clientEdit.license_number;
            client.loro_account = clientEdit.loro_account;
            client.moderated_by = clientEdit.moderated_by;
            client.moderated_on = clientEdit.moderated_on;
            client.full_name = clientEdit.client_name;
            client.name = clientEdit.client_name;
            client.person_genitive_case = clientEdit.person_genitive_case;
            client.person_nominative_case = clientEdit.person_nominative_case;
            client.post = clientEdit.post;
            client.profession_name = clientEdit.profession_name;
            client.send_notification = clientEdit.send_notification == true ? (short)1 : (short)0;
            client.settlement_account = clientEdit.settlement_account;
            client.site = clientEdit.site;
            client.sm_id = clientEdit.sm_id;
            client.updated_by = currUser.UserName;
            client.updated_on = DateTime.Now;
            client.zip_code = clientEdit.zip_code;
            client.city_id = curr_city_id;
            client.region_zone_id = clientEdit.region_zone_id;
            client.tax_system_type_id = clientEdit.tax_system_type_id;
            dbContext.SaveChanges();

            var client_db = dbContext.client.Where(ss => ss.id == clientEdit.id).FirstOrDefault();
            return new ClientViewModel(client_db);            
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ClientViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ClientViewModel clientAdd = (ClientViewModel)item;
            if (clientAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            allclients existing_client = dbContext.allclients.Where(ss => ss.client_name.Trim().ToLower().Equals(clientAdd.client_name.Trim().ToLower())).FirstOrDefault();
            if (existing_client != null)
            {
                modelState.AddModelError("", "Уже есть клиент с таким наименованием");
                return null;
            }

            DateTime now = DateTime.Now;

            int? curr_region_id = 0;
            int? curr_city_id = 0;
            region region = null;
            city city = null;
            if (clientAdd.RegionName_Changed)
            {
                region = dbContext.region.Where(ss => ss.name.Trim().ToLower().Equals(clientAdd.RegionName_Selected.Trim().ToLower())).FirstOrDefault();
                if (region == null)
                {
                    region = new region();
                    region.guid = Guid.NewGuid().ToString();
                    region.name = clientAdd.RegionName_Selected;
                    region.is_deleted = 0;
                    dbContext.region.Add(region);
                    dbContext.SaveChanges();
                    curr_region_id = region.id;
                }
                curr_region_id = region.id;
            }
            else
            {
                curr_region_id = clientAdd.region_id;
            }

            if ((clientAdd.RegionName_Changed) || (clientAdd.CityName_Changed))
            {
                city = dbContext.city.Where(ss => ss.region_id == curr_region_id && ss.name.Trim().ToLower().Equals(clientAdd.CityName_Selected.Trim().ToLower())).FirstOrDefault();
                if (city == null)
                {
                    city = new city();
                    city.guid = Guid.NewGuid().ToString();
                    city.name = clientAdd.CityName_Selected;
                    city.is_deleted = 0;
                    city.region_id = curr_region_id;
                    dbContext.city.Add(city);
                    dbContext.SaveChanges();
                }
                curr_city_id = city.id;
            }
            else
            {
                curr_city_id = clientAdd.city_id;
            }

            client client = new client();
            client.absolute_address = clientAdd.absolute_address;
            client.activated_by = currUser.UserName;
            client.activated_on = now;
            client.balance = clientAdd.balance;
            client.bank = clientAdd.bank;
            client.bik = clientAdd.bik;
            client.cell = clientAdd.phone;
            client.city_bank = clientAdd.city_bank;
            client.client_role = clientAdd.client_role;
            client.contact_person = clientAdd.contact_person;
            client.dt_birth = clientAdd.dt_birth;
            client.dt_sm_sync = clientAdd.dt_birth;
            client.foundation_document = clientAdd.foundation_document;
            client.hardware_id = clientAdd.hardware_id;
            client.inn = clientAdd.inn;
            client.kpp = clientAdd.kpp;
            client.is_in_summary_order = clientAdd.is_in_summary_order == true ? (short)1 : (short)0;
            client.is_moderated = clientAdd.is_moderated == true ? (short)1 : (short)0;
            client.is_phis = 0;
            client.kpp = clientAdd.kpp;
            client.legal_address = clientAdd.legal_address;
            client.license_date = clientAdd.license_date;
            client.license_number = clientAdd.license_number;
            client.loro_account = clientAdd.loro_account;
            client.moderated_by = clientAdd.moderated_by;
            client.moderated_on = clientAdd.moderated_on;
            client.full_name = clientAdd.client_name;
            client.name = clientAdd.client_name;
            client.person_genitive_case = clientAdd.person_genitive_case;
            client.person_nominative_case = clientAdd.person_nominative_case;
            client.post = clientAdd.post;
            client.profession_name = clientAdd.profession_name;
            client.send_notification = clientAdd.send_notification == true ? (short)1 : (short)0;
            client.settlement_account = clientAdd.settlement_account;
            client.site = clientAdd.site;
            client.sm_id = clientAdd.sm_id;
            client.created_by = currUser.UserName;
            client.created_on = now;
            client.zip_code = clientAdd.zip_code;
            //client.city_id = clientAdd.city_id;
            client.city_id = curr_city_id;
            client.region_zone_id = clientAdd.region_zone_id;
            client.tax_system_type_id = clientAdd.tax_system_type_id;  
            client.is_deleted = 0;
            client.guid = Guid.NewGuid().ToString();

            dbContext.client.Add(client);
            dbContext.SaveChanges();

            int id = client.id;

            var client_db = dbContext.allclients.Where(ss => ss.id == id).FirstOrDefault();
            return new ClientViewModel(client_db);            
        }

        public AuBaseViewModel RegisterClient(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ClientViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ClientViewModel clientAdd = (ClientViewModel)item;
            if (
                (clientAdd == null)
                ||
                (String.IsNullOrEmpty(clientAdd.client_name))
                ||
                (String.IsNullOrEmpty(clientAdd.UserLogin))
                ||
                (String.IsNullOrEmpty(clientAdd.UserPwd))
                ||
                (String.IsNullOrEmpty(clientAdd.UserPwdConfirm))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            if (!(clientAdd.UserPwd.Equals(clientAdd.UserPwdConfirm)))
            {
                modelState.AddModelError("", "Не совпадают пароли");
                return null;
            }

            client client = new client();
            client.absolute_address = clientAdd.absolute_address;
            client.activated_by = clientAdd.activated_by;
            client.activated_on = clientAdd.activated_on;
            client.balance = clientAdd.balance;
            client.bank = clientAdd.bank;
            client.bik = clientAdd.bik;
            client.cell = clientAdd.phone;
            client.city_bank = clientAdd.city_bank;
            client.client_role = clientAdd.client_role;
            client.contact_person = clientAdd.contact_person;            
            client.dt_birth = clientAdd.dt_birth;
            client.dt_sm_sync = clientAdd.dt_birth;
            client.foundation_document = clientAdd.foundation_document;
            client.hardware_id = clientAdd.hardware_id;
            client.inn = clientAdd.inn;
            client.kpp = clientAdd.kpp;
            client.is_in_summary_order = clientAdd.is_in_summary_order == true ? (short)1 : (short)0;
            client.is_moderated = clientAdd.is_moderated == true ? (short)1 : (short)0;
            client.is_phis = clientAdd.is_phis == true ? (short)1 : (short)0;
            client.kpp = clientAdd.kpp;
            client.legal_address = clientAdd.legal_address;
            client.license_date = clientAdd.license_date;
            client.license_number = clientAdd.license_number;
            client.loro_account = clientAdd.loro_account;
            client.moderated_by = clientAdd.moderated_by;
            client.moderated_on = clientAdd.moderated_on;
            client.full_name = clientAdd.client_name;
            client.name = clientAdd.client_name;
            client.person_genitive_case = clientAdd.person_genitive_case;
            client.person_nominative_case = clientAdd.person_nominative_case;
            client.post = clientAdd.post;
            client.profession_name = clientAdd.profession_name;
            client.send_notification = clientAdd.send_notification == true ? (short)1 : (short)0;
            client.settlement_account = clientAdd.settlement_account;
            client.site = clientAdd.site;
            client.sm_id = clientAdd.sm_id;
            client.created_by = currUser.UserName;
            client.created_on = DateTime.Now;
            client.zip_code = clientAdd.zip_code;
            client.city_id = clientAdd.city_id;
            client.is_deleted = 0;
            client.guid = Guid.NewGuid().ToString();

            dbContext.client.Add(client);
            dbContext.SaveChanges();

            int id = client.id;

            var client_db = dbContext.allclients.Where(ss => ss.id == id).FirstOrDefault();
            return new ClientViewModel(client_db);
        }

        public Tuple<bool, string> ClientActivate(int client_id, ModelStateDictionary modelState)
        {
            client client = dbContext.client.Where(ss => ss.id == client_id && ss.is_deleted != 1).FirstOrDefault();
            if (client == null)
            {
                modelState.AddModelError("", "Не найден клиент с кодом " + client_id.ToString());
                return null;
            }

            sales sales = dbContext.sales.Where(ss => ss.client_id == client_id).FirstOrDefault();
            if (sales == null)
            {
                modelState.AddModelError("", "Не найдено ни одной торговой точки клиента с кодом " + client_id.ToString());
                return null;
            }
            /*
                t2."lastActivityDate" AS last_activity_date,    
                t3."IsApproved" AS is_activated,            
                 LEFT JOIN cabinet.my_aspnet_users t2 ON t1.user_id = t2.id
                 LEFT JOIN cabinet.my_aspnet_membership t3 ON t2.id = t3."userId"
            */
            var member = dbContext.my_aspnet_membership.Where(ss => ss.userId == sales.user_id && ss.IsApproved != 1).FirstOrDefault();
            if (member == null)
            {
                modelState.AddModelError("", "Не найдена неактивированная учетная запись клиента с кодом " + client_id.ToString());
                return null;
            }

            DateTime now = DateTime.Now;

            member.IsApproved = 1;

            client.activated_on = now;
            client.activated_by = currUser.UserName;

            // !!!
            // todo: send email to client about activation

            dbContext.SaveChanges();

            return new Tuple<bool, string>(true, "");
        }

    }
}
