﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientMaxVersionService : IAuBaseService
    {
        //
    }

    public class ClientMaxVersionService : AuBaseService, IClientMaxVersionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_client_max_version
                .ProjectTo<ClientMaxVersionViewModel>()
                ;
        }
    }
}
