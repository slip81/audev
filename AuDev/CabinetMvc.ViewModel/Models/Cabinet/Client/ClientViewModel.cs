﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientViewModel : AuBaseViewModel
    {

        public ClientViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT)
        {
            //
        }

        public ClientViewModel(allclients client)
            : base(Enums.MainObjectType.CAB_CLIENT)
        {            
            city_id = client.city_id;
            city_name = client.city_name;
            client_name = client.client_name;
            created_by = client.created_by;
            id = client.id;
            is_activated = client.is_activated == 1;
            is_activated_str = is_activated == true ? "Да" : "Нет";
            last_activity_date = client.last_activity_date;
            phone = client.phone;
            reg_date = client.reg_date;
            region_id = client.region_id;
            region_name = client.region_name;
            sales_count = client.sales_count;
            user_id = client.user_id;
            user_name = client.user_name;
            is_phis = client.is_phis == 1;
            is_phis_str = is_phis == true ? "Да" : "Нет";
            is_moderated = client.is_moderated == 1;
            is_moderated_str = is_moderated == true ? "Да" : "Нет";
            contact_person = client.contact_person;
            business_id = client.business_id;
            has_business_id = String.IsNullOrEmpty(client.business_id) ? "" : "ДС";
            inn = client.inn;
            legal_address = client.legal_address;
            absolute_address = client.absolute_address;
            site = client.site;
            kpp = client.kpp;
            state_id = client.state_id;
            state_name = client.state_name;
            sm_id = client.sm_id;
            region_zone_id = client.region_zone_id;
            tax_system_type_id = client.tax_system_type_id;
        }

        public ClientViewModel(client client)
            : base(Enums.MainObjectType.CAB_CLIENT)
        {
            id = client.id;
            absolute_address = client.absolute_address;
            activated_by = client.activated_by;
            activated_on = client.activated_on;
            balance = client.balance;
            bank = client.bank;
            bik = client.bik;            
            phone = client.cell;
            city_bank = client.city_bank;
            client_role = client.client_role;
            contact_person = client.contact_person;
            created_on = client.created_on;
            dt_birth = client.dt_birth;
            dt_sm_sync = client.dt_birth;
            foundation_document = client.foundation_document;            
            guid = client.guid;
            hardware_id = client.hardware_id;
            inn = client.inn;
            is_deleted = client.is_deleted == 1;
            is_in_summary_order = client.is_in_summary_order == 1;
            is_moderated = client.is_moderated == 1;
            is_moderated_str = is_moderated == true ? "Да" : "Нет";
            is_phis = client.is_phis == 1;
            kpp = client.kpp;
            legal_address = client.legal_address;
            license_date = client.license_date;
            license_number = client.license_number;
            loro_account = client.loro_account;
            moderated_by = client.moderated_by;
            moderated_on = client.moderated_on;
            client_name = client.full_name;            
            person_genitive_case = client.person_genitive_case;
            person_nominative_case = client.person_nominative_case;
            post = client.post;            
            profession_name = client.profession_name;
            send_notification = client.send_notification == 1;
            settlement_account = client.settlement_account;
            site = client.site;
            sm_id = client.sm_id;
            updated_by = client.updated_by;
            updated_on = client.updated_on;
            zip_code = client.zip_code;            
            city_id = client.city_id;
            created_by = client.created_by;
            is_phis_str = is_phis == true ? "Да" : "Нет";
            is_activated_str = is_activated == true ? "Да" : "Нет";
            state_id = client.state_id;
            state_name = "";
            region_zone_id = client.region_zone_id;
            tax_system_type_id = client.tax_system_type_id;
            //this.sales_list = client.sales != null ? client.sales.Where(ss => ss.is_deleted != 1).Select(ss => new SalesViewModel(ss)) : null;
        }
        

        // from vw        
        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "Название организации")]
        [Required(ErrorMessage = "Не задано название организации")]
        public string client_name { get; set; }

        [Display(Name = "Телефон")]
        [DataType((DataType.PhoneNumber))]
        public string phone { get; set; }

        [Display(Name = "Код логина")]
        public Nullable<int> user_id { get; set; }

        [Display(Name = "Логин")]
        public string user_name { get; set; }

        [Display(Name = "Город")]        
        public Nullable<int> city_id { get; set; }

        [Display(Name = "Город")]
        public string city_name { get; set; }

        [Display(Name = "Регион")]        
        public Nullable<int> region_id { get; set; }

        [Display(Name = "Регион")]
        public string region_name { get; set; }

        [Display(Name = "Дата регистрации")]
        public System.DateTime reg_date { get; set; }

        [Display(Name = "Дата посл. активности")]        
        public Nullable<System.DateTime> last_activity_date { get; set; }

        [Display(Name = "Создал")]
        public string created_by { get; set; }

        [Display(Name = "Активирован")]
        public Nullable<bool> is_activated { get; set; }

        [Display(Name = "Активирован")]
        public string is_activated_str { get; set; }

        [Display(Name = "Кол-во точек")]
        public Nullable<long> sales_count { get; set; }
        
        // from table
        [Display(Name = "GUID")]
        public string guid { get; set; }

        [Display(Name = "ИНН")]
        [Required(ErrorMessage = "Не задан ИНН")]
        public string inn { get; set; }

        [Display(Name = "КПП")]
        [Required(ErrorMessage = "Не задан КПП")]
        public string kpp { get; set; }

        [Display(Name = "Юр адрес")]
        [Required(ErrorMessage = "Не задан юр. адрес")]
        public string legal_address { get; set; }
        
        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Не задан адрес")]
        public string absolute_address { get; set; }

        [Display(Name = "foundation_document")]
        public string foundation_document { get; set; }

        [Display(Name = "bank")]
        public string bank { get; set; }

        [Display(Name = "city_bank")]
        public string city_bank { get; set; }

        [Display(Name = "БИК")]
        public string bik { get; set; }

        [Display(Name = "loro_account")]
        public string loro_account { get; set; }

        [Display(Name = "settlement_account")]
        public string settlement_account { get; set; }

        [Display(Name = "Контактное лицо")]
        public string contact_person { get; set; }

        [Display(Name = "Удален")]
        public Nullable<bool> is_deleted { get; set; }

        [Display(Name = "zip_code")]
        public string zip_code { get; set; }

        [Display(Name = "post")]
        public string post { get; set; }

        [Display(Name = "person_nominative_case")]
        public string person_nominative_case { get; set; }

        [Display(Name = "person_genitive_case")]
        public string person_genitive_case { get; set; }

        [Display(Name = "Дата содздания")]
        public System.DateTime created_on { get; set; }

        [Display(Name = "Дата изменения")]
        public Nullable<System.DateTime> updated_on { get; set; }

        [Display(Name = "Изменил")]
        public string updated_by { get; set; }

        [Display(Name = "license_number")]
        public string license_number { get; set; }

        [Display(Name = "license_date")]
        public string license_date { get; set; }

        [Display(Name = "SM_ID")]
        public Nullable<int> sm_id { get; set; }

        [Display(Name = "Дата активации")]
        public Nullable<System.DateTime> activated_on { get; set; }

        [Display(Name = "Активировал")]
        public string activated_by { get; set; }

        [Display(Name = "Электронная почта")]
        [DataType((DataType.EmailAddress))]
        public string site { get; set; }

        [Display(Name = "client_role")]
        public Nullable<int> client_role { get; set; }

        [Display(Name = "dt_sm_sync")]
        public Nullable<System.DateTime> dt_sm_sync { get; set; }

        [Display(Name = "balance")]
        public Nullable<decimal> balance { get; set; }

        [Display(Name = "is_in_summary_order")]
        public Nullable<bool> is_in_summary_order { get; set; }

        [Display(Name = "hardware_id")]
        public string hardware_id { get; set; }

        [Display(Name = "Физ. лицо")]
        public Nullable<bool> is_phis { get; set; }

        [Display(Name = "Физ. лицо")]
        public string is_phis_str { get; set; }

        [Display(Name = "На модерации")]
        public Nullable<bool> is_moderated { get; set; }

        [Display(Name = "На модерации")]
        public string is_moderated_str { get; set; }

        [Display(Name = "moderated_by")]
        public string moderated_by { get; set; }

        [Display(Name = "moderated_on")]
        public Nullable<System.DateTime> moderated_on { get; set; }

        [Display(Name = "send_notification")]
        public Nullable<bool> send_notification { get; set; }

        [Display(Name = "Дата рождения")]
        public Nullable<System.DateTime> dt_birth { get; set; }

        [Display(Name = "profession_name")]
        public string profession_name { get; set; }

        [Display(Name = "Код организации ДС")]
        public string business_id { get; set; }

        [Display(Name = "Есть на ДС")]
        public string has_business_id { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> state_id { get; set; }

        [Display(Name = "Статус")]
        public string state_name { get; set; }

        [Display(Name = "Электронная почта")]        
        public string UserEmail { get; set; }

        [Display(Name = "Логин")]
        public string UserLogin { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]        
        public string UserPwd { get; set; }

        [Display(Name = "Подтверждение пароля")]
        [DataType(DataType.Password)]        
        public string UserPwdConfirm { get; set; }

        [Display(Name = "RegionName_Selected")]
        [Required(ErrorMessage = "Не задан регион")]
        public string RegionName_Selected { get; set; }

        [Display(Name = "CityName_Selected")]
        [Required(ErrorMessage = "Не задан город")]
        public string CityName_Selected { get; set; }

        [Display(Name = "RegionName_Changed")]        
        public bool RegionName_Changed { get; set; }

        [Display(Name = "CityName_Changed")]
        public bool CityName_Changed { get; set; }

        [Display(Name = "Код")]
        public int client_id { get { return this.id; } }

        [Display(Name = "Зона региона")]
        public Nullable<int> region_zone_id { get; set; }

        [Display(Name = "Система налогообложения")]
        public Nullable<int> tax_system_type_id { get; set; }

        // children        

        [JsonIgnore()]
        public IEnumerable<SpravProjectViewModel> project_list { get; set; }
    }
}

