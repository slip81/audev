﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientUserWidgetService : IAuBaseService
    {
        IQueryable<ClientUserWidgetViewModel> GetList_Avialable();
        IQueryable<ClientUserWidgetViewModel> GetList_Installed();
        bool SetWidget(string id, int? oldIndex, int? newIndex);
        bool UpdateWidget(string id, string param);
    }

    public class ClientUserWidgetService : AuBaseService, IClientUserWidgetService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.client_user_widget.Where(ss => ss.user_id == parent_id)
                .ProjectTo<ClientUserWidgetViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.client_user_widget.Where(ss => ss.item_id == id)
                .ProjectTo<ClientUserWidgetViewModel>()
                .FirstOrDefault();
        }

        public IQueryable<ClientUserWidgetViewModel> GetList_Avialable()
        {
            var user_id = currUser.CabUserId;

            return from t1 in dbContext.widget
                      from t2 in dbContext.client_user
                      from t3 in dbContext.client_service.Where(ss => t2.client_id == ss.client_id && t1.service_id == ss.service_id).DefaultIfEmpty()
                      where t2.user_id == user_id
                      //&& t2.client_id == t3.client_id
                      //&& t3.service_id == t1.service_id
                      && !dbContext.vw_client_user_widget.Where(tt => tt.user_id == user_id && tt.is_installed && tt.is_active && t1.widget_id == tt.widget_id).Any()
                      select  new ClientUserWidgetViewModel()
                        {
                            is_active = t1.is_active,
                            is_installed = false,
                            service_id = t1.service_id,
                            sort_num = t1.sort_num,
                            state = t3.service_id > 0 ? (int?)t3.state : null,
                            user_id = user_id,
                            widget_content = t1.widget_content,
                            widget_descr = t1.widget_descr,
                            widget_element_id = t1.widget_element_id,
                            widget_header = t1.widget_header,
                            widget_id = t1.widget_id,
                            widget_name = t1.widget_name,                   
                            param = "",
                        };
            /*
            return dbContext.widget.Where(ss => ss.is_active && !dbContext.vw_client_user_widget.Where(tt => tt.user_id == user_id && tt.is_installed && tt.is_active && ss.widget_id == tt.widget_id).Any())
                .Select(ss => new ClientUserWidgetViewModel()
                {
                    is_active = ss.is_active,
                    is_installed = false,
                    service_id = ss.service_id,
                    sort_num = ss.sort_num,
                    state = null,
                    user_id = user_id,
                    widget_content = ss.widget_content,
                    widget_descr = ss.widget_descr,
                    widget_element_id = ss.widget_element_id,
                    widget_header = ss.widget_header,
                    widget_id = ss.widget_id,
                    widget_name = ss.widget_name,                   
                });
            */
               
        }

        public IQueryable<ClientUserWidgetViewModel> GetList_Installed()
        {
            var user_id = currUser.CabUserId;
            return dbContext.vw_client_user_widget
                .Where(ss => ss.user_id == user_id && ss.is_installed)
                .ProjectTo<ClientUserWidgetViewModel>();
        }

        public bool SetWidget(string id, int? oldIndex, int? newIndex)
        {
            var user_id = currUser.CabUserId;                        
            widget widget = null;
            int widget_id = 0;

            widget = dbContext.widget.Where(ss => ss.widget_element_id == id).FirstOrDefault();
            widget_id = widget.widget_id;

            client_user_widget client_user_widget = dbContext.client_user_widget.Where(ss => ss.user_id == user_id && ss.widget_id == widget_id).FirstOrDefault();

            // убрали виджет
            if ((oldIndex.GetValueOrDefault(0) >= 0) && (newIndex.GetValueOrDefault(0) < 0))
            {                
                dbContext.client_user_widget.Remove(client_user_widget);
                var otherWidgets = dbContext.client_user_widget.Where(ss => ss.sort_num >= client_user_widget.sort_num).ToList();
                if (otherWidgets != null)
                {
                    foreach (var otherWidget in otherWidgets)
                    {
                        otherWidget.sort_num = otherWidget.sort_num - 1;
                    }
                }
                dbContext.SaveChanges();
            }
            // поставили виджет
            else if ((oldIndex.GetValueOrDefault(0) < 0) && (newIndex.GetValueOrDefault(0) >= 0))
            {                
                if (client_user_widget == null)
                {
                    client_user_widget = new client_user_widget();
                    client_user_widget.widget_id = widget_id;
                    client_user_widget.user_id = user_id;
                    client_user_widget.sort_num = (int)newIndex;
                    dbContext.client_user_widget.Add(client_user_widget);
                    var otherWidgets = dbContext.client_user_widget.Where(ss => ss.sort_num >= client_user_widget.sort_num).ToList();
                    if (otherWidgets != null)
                    {
                        foreach (var otherWidget in otherWidgets)
                        {
                            otherWidget.sort_num = otherWidget.sort_num + 1;
                        }
                    }
                    dbContext.SaveChanges();
                }
            }
            // сменили порядок установленных виджетов
            else if ((oldIndex.GetValueOrDefault(0) >= 0) && (newIndex.GetValueOrDefault(0) >= 0))
            {                
                if (client_user_widget != null)
                {                    
                    // перетащали сверху вниз
                    if (newIndex > oldIndex)
                    {
                        var otherWidgets = dbContext.client_user_widget.Where(ss => ss.sort_num >= (int)oldIndex && ss.sort_num <= (int)newIndex).ToList();
                        if (otherWidgets != null)
                        {
                            foreach (var otherWidget in otherWidgets)
                            {
                                otherWidget.sort_num = otherWidget.sort_num - 1;
                            }
                        }
                    }
                    // перетащали снизу вверх
                    else
                    {
                        var otherWidgets = dbContext.client_user_widget.Where(ss => ss.sort_num < (int)oldIndex && ss.sort_num >= (int)newIndex).ToList();
                        if (otherWidgets != null)
                        {
                            foreach (var otherWidget in otherWidgets)
                            {
                                otherWidget.sort_num = otherWidget.sort_num + 1;
                            }
                        }
                    }
                    client_user_widget.sort_num = (int)newIndex;
                    dbContext.SaveChanges();
                }
            }
            return true;
        }

        public bool UpdateWidget(string id, string param)
        {
            var user_id = currUser.CabUserId;
            widget widget = dbContext.widget.Where(ss => ss.widget_element_id == id).FirstOrDefault();
            client_user_widget client_user_widget = dbContext.client_user_widget.Where(ss => ss.user_id == user_id && ss.widget_id == widget.widget_id).FirstOrDefault();
            client_user_widget.param = param;
            dbContext.SaveChanges();
            return true;
        }

    }
}
