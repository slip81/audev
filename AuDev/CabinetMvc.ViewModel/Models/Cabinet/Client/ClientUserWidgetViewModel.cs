﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientUserWidgetViewModel : AuBaseViewModel
    {

        public ClientUserWidgetViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT_USER_WIDGET)
        {
            //vw_client_user_widget
        }

        [Key()]
        [Display(Name = "Виджет")]
        public int widget_id { get; set; }
        
        [Display(Name = "Виджет")]
        public string widget_name { get; set; }

        [Display(Name = "Описание виджета")]
        public string widget_descr { get; set; }

        [Display(Name = "Код элемента с виджетом")]
        public string widget_element_id { get; set; }

        [Display(Name = "Заголовок виджета")]
        public string widget_header { get; set; }

        [Display(Name = "Контент виджета")]
        public string widget_content { get; set; }

        [Display(Name = "Порядковый номер")]
        public Nullable<int> sort_num { get; set; }

        [Display(Name = "Виджет активен")]
        public bool is_active { get; set; }

        [Display(Name = "Услуга")]
        public Nullable<int> service_id { get; set; }

        [Display(Name = "Пользователь")]
        public Nullable<int> user_id { get; set; }

        [Display(Name = "Виджет установлен")]
        public bool is_installed { get; set; }

        [Display(Name = "Клиент")]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "Торговая точка")]
        public Nullable<int> sales_id { get; set; }

        [Display(Name = "Статус услуги (0 - установлена, 1 - заказана)")]
        public Nullable<int> state { get; set; }

        [Display(Name = "Параметры виджета")]
        public string param { get; set; }
    }

    public class DiscountWidgetParam 
    {
        public bool IsVisible_Graph1 { get; set; }
        public bool IsVisible_Graph2 { get; set; }
    }
}

