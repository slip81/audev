﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Hosting;
using System.Diagnostics;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientServiceService : IAuBaseService
    {
        IQueryable<ClientServiceViewModel> GetList_Service(int client_id);
        IQueryable<ClientServiceViewModel> GetList_Workplace(int client_id, int service_id);
        IQueryable<ClientServiceViewModel> GetList_RegAndUnreg_byWorkplaceId(int client_id, int workplace_id);        
        Tuple<bool, string> ApplyServiceToWorkplace(int client_id, int workplace_id, int service_id, int version_id);
        Tuple<bool, string> RemoveServiceFromWorkplace(int client_id, int workplace_id, int service_id, int version_id);
        Tuple<bool, string> ApplyServiceListToWorkplace(int client_id, int? workplace_id, List<VersionViewModel> serviceList);
    }

    public class ClientServiceService : AuBaseService, IClientServiceService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_client_service.Where(ss => ss.client_id == parent_id)
                .Select(ss => new ClientServiceViewModel()
                {
                    client_id = ss.client_id,
                    service_id = ss.service_id,
                    service_name = ss.service_name,
                    service_description = ss.service_description,
                    is_service = ss.is_service,
                    service_priority = ss.service_priority,
                    activation_key = ss.activation_key,
                    have_spec = ss.have_spec,
                    max_workplace_cnt = ss.max_workplace_cnt,
                }
                )
                .OrderBy(ss => ss.service_priority)
                .Distinct();
        }

        public IQueryable<ClientServiceViewModel> GetList_Service(int client_id)
        {            
            return from t1 in dbContext.vw_client_service
                   where t1.client_id == client_id
                   group t1 by new { t1.service_id, t1.service_name, t1.service_description, t1.is_service, t1.service_priority, /*t1.activation_key*/ }
                   into grp
                       select new ClientServiceViewModel
                       {
                           client_id = client_id,
                           service_id = grp.Key.service_id,
                           service_name = grp.Key.service_name,
                           service_description = grp.Key.service_description,
                           is_service = grp.Key.is_service,
                           service_priority = grp.Key.service_priority,
                           //activation_key = grp.Key.activation_key,
                           ServiceCnt = grp.Select(x => x.workplace_id).Distinct().Count()
                       };
        }

        public IQueryable<ClientServiceViewModel> GetList_Workplace(int client_id, int service_id)
        {
            return (from t1 in dbContext.vw_client_service
                   where t1.client_id == client_id && t1.service_id == service_id
                    select new { t1.sales_id, t1.sales_name, t1.sales_address, t1.workplace_id, t1.workplace_registration_key, t1.workplace_description, /*t1.activation_key,*/ }
                   )
                   .Distinct()
                   .Select(ss => new ClientServiceViewModel
                       {
                           client_id = client_id,
                           service_id = service_id,
                           sales_id = ss.sales_id,
                           sales_name = ss.sales_name,
                           sales_address = ss.sales_address,
                           workplace_id = ss.workplace_id,
                           workplace_registration_key = ss.workplace_registration_key,
                           workplace_description = ss.workplace_description,
                           //activation_key = ss.activation_key,
                       });
        }

        public IQueryable<ClientServiceViewModel> GetList_RegAndUnreg_byWorkplaceId(int client_id, int workplace_id)
        {
            List<int> installed_version_id_list = dbContext.vw_client_service_registered
                .Where(ss => ss.client_id == client_id && ss.workplace_id == workplace_id && ss.version_id.HasValue)
                .Select(ss => (int)ss.version_id).Distinct().ToList();

            return dbContext.vw_client_service_reg_an_unreg
                    .Where(ss => ss.client_id == client_id 
                    && ((ss.workplace_id == workplace_id) || (!ss.workplace_id.HasValue))
                    && ((ss.is_registered == 1) || ((ss.is_registered == 0) && (ss.version_id.HasValue) && (!installed_version_id_list.Contains((int)ss.version_id))))
                    )
                   .Select(ss => new ClientServiceViewModel
                   {
                       client_id = client_id,
                       workplace_id = workplace_id,
                       service_id = ss.service_id,
                       version_id = ss.version_id,
                       service_name = ss.service_name,
                       version_name = ss.version_name,
                       is_registered = ss.is_registered,
                       activation_key = ss.activation_key,
                       have_spec = ss.have_spec,
                       max_workplace_cnt = ss.max_workplace_cnt,
                   });

        }

        public Tuple<bool, string> ApplyServiceToWorkplace(int client_id, int workplace_id, int service_id, int version_id)
        {
            var lic = (from t1 in dbContext.order
                       from t2 in dbContext.order_item
                       from t3 in dbContext.license
                       where t1.id == t2.order_id
                       && t2.id == t3.order_item_id
                       && t1.client_id == client_id
                       && t2.service_id == service_id
                       && t2.version_id == version_id
                       && !t3.service_registration_id.HasValue
                       select t3)
                      .FirstOrDefault();
            if (lic == null)
            {
                return new Tuple<bool, string>(false, "Не найдена лицензия на услугу " + service_id.ToString() + " у клиента " + client_id.ToString());
            }

            int? max_workplace_cnt = null;
            vw_client_service_param vw_client_service_param = dbContext.vw_client_service_param.Where(ss => ss.client_id == client_id && ss.service_id == service_id).FirstOrDefault();
            if (vw_client_service_param != null)
            {
                max_workplace_cnt = vw_client_service_param.max_workplace_cnt;
            }

            if ((max_workplace_cnt.HasValue) && (max_workplace_cnt >= 0))
            {
                var sales_id = dbContext.vw_workplace.Where(ss => ss.workplace_id == workplace_id).Select(ss => ss.sales_id).FirstOrDefault();
                var curr_workplace_cnt = dbContext.vw_client_service.Where(ss => ss.sales_id == sales_id && ss.service_id == service_id).Select(ss => ss.workplace_id).Distinct().Count();
                if (curr_workplace_cnt >= max_workplace_cnt)
                {
                    return new Tuple<bool, string>(false, "Превышено максимальное количество рабочих мест с данной услугой");
                }
            }

            string activation_key = "";
            bool needKey = lic.order_item.service.need_key == 1;
            if (needKey)
            {
                string registration_key = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault().registration_key;
                string version_name = lic.order_item.version.name;
                string exe_name = lic.order_item.service.exe_name;
                int? file_size = lic.order_item.version.file_size;
                string hash = String.Format("{0};{1};{2};{3};{4}", registration_key, version_name, (int)Math.Round(((double)file_size) / 10000, 0), exe_name, String.Empty);
                activation_key = GlobalUtil.CheckCode(version_name, exe_name, hash);
            }
            else
            {
                activation_key = "";
            }

            service_registration reg = new service_registration();
            reg.activation_key = activation_key;
            reg.created_by = currUser.UserName;
            reg.created_on = DateTime.Now;
            reg.guid = Guid.NewGuid().ToString();
            reg.is_deleted = 0;
            reg.license_id = lic.id;
            reg.workplace_id = workplace_id;
            dbContext.service_registration.Add(reg);
            
            lic.service_registration = reg;

            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;

            DateTime today = DateTime.Today;                
            bool specExists = true;
            switch (servEnum)
            {
                case Enums.ClientServiceEnum.SENDER_DEFECT:
                case Enums.ClientServiceEnum.SENDER_GRLS:
                    string email = (from t1 in dbContext.vw_workplace
                                    from t2 in dbContext.sales
                                    where t1.workplace_id == workplace_id
                                    && t1.sales_id == t2.id
                                    select t2.email)
                                   .FirstOrDefault();
                    service_sender service_sender = dbContext.service_sender
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id).FirstOrDefault();
                    if (service_sender == null)
                    {
                        specExists = false;
                        service_sender = new service_sender();                        
                    }
                    service_sender.date_beg = today;
                    service_sender.date_end = null;
                    service_sender.email = email;
                    service_sender.interval_in_days = 3;
                    service_sender.is_disabled = false;
                    service_sender.schedule_monthly = "";
                    service_sender.schedule_weekly = "";
                    service_sender.schedule_type = (int)Enums.ScheduleTypeEnum.INTERVAL_DAILY;
                    service_sender.service_id = service_id;
                    service_sender.workplace_id = workplace_id;
                    if (!specExists)
                        dbContext.service_sender.Add(service_sender);
                    break;
                case Enums.ClientServiceEnum.CVA:
                    service_cva service_cva = dbContext.service_cva
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id).FirstOrDefault();
                    if (service_cva == null)
                    {
                        specExists = false;
                        service_cva = new service_cva();                        
                    }
                    service_cva.service_id = service_id;
                    service_cva.workplace_id = workplace_id;
                    service_cva.active = 1;
                    service_cva.send_interval = 12;
                    if (!specExists)
                        dbContext.service_cva.Add(service_cva);
                    break;
                case Enums.ClientServiceEnum.DEFECT:
                    service_defect service_defect = dbContext.service_defect
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id).FirstOrDefault();
                    if (service_defect == null)
                    {
                        specExists = false;
                        service_defect = new service_defect();                        
                    }
                    service_defect.service_id = service_id;
                    service_defect.workplace_id = workplace_id;
                    service_defect.is_allow_download = false;
                    if (!specExists)
                        dbContext.service_defect.Add(service_defect);
                    break;
                case Enums.ClientServiceEnum.STOCK:
                    service_stock service_stock = dbContext.service_stock
                        .Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id).FirstOrDefault();
                    if (service_stock == null)
                    {
                        specExists = false;
                        service_stock = new service_stock();
                    }
                    service_stock.service_id = service_id;
                    service_stock.workplace_id = workplace_id;
                    service_stock.batch_row_cnt = 100;                    
                    if (!specExists)
                        dbContext.service_stock.Add(service_stock);
                    break;
                default:
                    break;
            }

            var installed_service_cnt = dbContext.client_service.Where(ss => ss.client_id == client_id && ss.service_id == service_id && ss.state == (int)Enums.ClientServiceStateEnum.INSTALLED).Count();
            if (installed_service_cnt <= 0)
            {
                client_service client_service = new client_service();
                client_service.client_id = client_id;
                client_service.service_id = service_id;
                client_service.state = (int)Enums.ClientServiceStateEnum.INSTALLED;
                dbContext.client_service.RemoveRange(dbContext.client_service.Where(ss => ss.client_id == client_id && ss.service_id == service_id));
                dbContext.client_service.Add(client_service);
            }

            dbContext.SaveChanges();
            
            return new Tuple<bool, string>(true, "");
        }

        public Tuple<bool, string> RemoveServiceFromWorkplace(int client_id, int workplace_id, int service_id, int version_id)
        {
            var lic = (from t1 in dbContext.order
                       from t2 in dbContext.order_item
                       from t3 in dbContext.license
                       from t4 in dbContext.service_registration
                       where t1.id == t2.order_id
                       && t2.id == t3.order_item_id
                       && t1.client_id == client_id
                       && t2.service_id == service_id
                       && t2.version_id == version_id
                       && t3.service_registration_id == t4.id
                       && t4.workplace_id == workplace_id
                       select t3)
                      .FirstOrDefault();
            if (lic == null)
            {
                return new Tuple<bool, string>(false, "Не найдена лицензия на услугу " + service_id.ToString() + " у клиента " + client_id.ToString());
            }

            service_registration reg = dbContext.service_registration.Where(ss => ss.id == lic.service_registration_id).FirstOrDefault();
            if (reg == null)
            {
                return new Tuple<bool, string>(false, "Не найдена регистрация на услугу " + service_id.ToString() + " у клиента " + client_id.ToString());
            }

            lic.service_registration_id = null;
            dbContext.service_registration.Remove(reg);


            Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(service_id.ToString(), out servEnum);
            if (!parseOk)
                servEnum = Enums.ClientServiceEnum.NONE;

            switch (servEnum)
            {
                case Enums.ClientServiceEnum.SENDER_DEFECT:
                case Enums.ClientServiceEnum.SENDER_GRLS:
                    dbContext.service_sender.RemoveRange(dbContext.service_sender.Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id));
                    break;
                case Enums.ClientServiceEnum.CVA:
                    dbContext.service_cva.RemoveRange(dbContext.service_cva.Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id));
                    break;
                case Enums.ClientServiceEnum.DEFECT:
                    dbContext.service_defect.RemoveRange(dbContext.service_defect.Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id));
                    break;
                case Enums.ClientServiceEnum.STOCK:
                    dbContext.service_stock.RemoveRange(dbContext.service_stock.Where(ss => ss.workplace_id == workplace_id && ss.service_id == service_id));
                    break;
                default:
                    break;
            }

            var remaining_service_cnt = dbContext.vw_client_service.Where(ss => ss.client_id == client_id && ss.service_id == service_id).Count();
            if (remaining_service_cnt <= 1)
            {
                // больше у клиента такой услуги нет
                dbContext.client_service.RemoveRange(dbContext.client_service.Where(ss => ss.client_id == client_id && ss.service_id == service_id && ss.state == (int)Enums.ClientServiceStateEnum.INSTALLED));
            }

            dbContext.SaveChanges();

            return new Tuple<bool, string>(true, "");
        }

        public Tuple<bool, string> ApplyServiceListToWorkplace(int client_id, int? workplace_id, List<VersionViewModel> serviceList)
        {
            if ((workplace_id.GetValueOrDefault(0) <= 0) || (serviceList == null) || (serviceList.Count <= 0))
            {
                return new Tuple<bool, string>(false, "Не выбраны услуги");
            }

            foreach (var service in serviceList)
            {
                var res = ApplyServiceToWorkplace(client_id, (int)workplace_id, service.service_id, service.id);
                if (!res.Item1)
                    return new Tuple<bool, string>(false, res.Item2);
            }

            return new Tuple<bool, string>(true, "");
        }
    }
}
