﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientServiceUnregViewModel : AuBaseViewModel
    {

        public ClientServiceUnregViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT_SERVICE_UNREG)
        {
            //
        }

        public ClientServiceUnregViewModel(vw_client_service_unregistered item)
            : base(Enums.MainObjectType.CAB_CLIENT_SERVICE_UNREG)
        {
            id = item.id;
            client_id = item.client_id;
            service_id = item.service_id;
            version_id = item.version_id;
            service_name = item.service_name;
            version_name = item.version_name;
            service_description = item.service_description;
            license_id = item.license_id;
            order_item_id = item.order_item_id;
            order_id = item.order_id;
            max_workplace_cnt = item.max_workplace_cnt;
            ServiceCnt = 0;
            ForDel = false;
        }
     
        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "Организация")]
        public int client_id { get; set; }

        [Display(Name = "Код услуги")]
        public int service_id { get; set; }

        [Display(Name = "Код версии")]
        public Nullable<int> version_id { get; set; }

        [Display(Name = "Услуга")]
        public string service_name { get; set; }

        [Display(Name = "Версия")]
        public string version_name { get; set; }

        [Display(Name = "Описание услуги")]
        public string service_description { get; set; }

        [Display(Name = "license_id")]
        public Nullable<int> license_id { get; set; }

        [Display(Name = "order_item_id")]
        public Nullable<int> order_item_id { get; set; }

        [Display(Name = "order_id")]
        public Nullable<int> order_id { get; set; }

        [Display(Name = "Макс. кол-во раб. мест")]
        public Nullable<int> max_workplace_cnt { get; set; }

        [Display(Name = "Количество")]
        public Nullable<int> ServiceCnt { get; set; }

        [Display(Name = "К удалению")]
        public bool ForDel { get; set; }

    }
}

