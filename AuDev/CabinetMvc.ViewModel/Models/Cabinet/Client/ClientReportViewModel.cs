﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientReportViewModel : AuBaseViewModel
    {

        public ClientReportViewModel()
            : base()
        {
            //vw_client_report1
        }

        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "Название")]
        public string client_name { get; set; }

        [Display(Name = "Кол-во ТТ")]
        public long sales_count { get; set; }

        [Display(Name = "Область")]
        public string region_name { get; set; }

        [Display(Name = "Город")]
        public string city_name { get; set; }

        [Display(Name = "Версия (мин)")]
        public string version_min { get; set; }
       
        [Display(Name = "Версия (макс)")]
        public string version_max { get; set; }

        [Display(Name = "Дата регистрации (мин)")]
        public Nullable<System.DateTime> date_reg_min { get; set; }

        [Display(Name = "Дата регистрации (макс)")]
        public Nullable<System.DateTime> date_reg_max { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }

        [Display(Name = "Тип строки")]
        public int kind { get; set; }

    }
}