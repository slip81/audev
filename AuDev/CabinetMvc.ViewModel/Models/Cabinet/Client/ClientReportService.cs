﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientReportService : IAuBaseService
    {
        List<ClientReportViewModel> GetList_byFilter(bool is_client_only, string service_name);
    }

    public class ClientReportService : AuBaseService, IClientReportService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_client_report1
                .Where(ss => ss.kind == 1)
                .ProjectTo<ClientReportViewModel>();
        }

        public List<ClientReportViewModel> GetList_byFilter(bool is_client_only, string service_name)
        {
            var res = dbContext.Database.SqlQuery<ClientReportViewModel>("select * from cabinet.report_client_version1('" + service_name + "', " + (is_client_only ? "1" : "0") + ")").ToList();            
            return res;
            
            /*
            int kind = is_client_only ? 1 : 2;
            return dbContext.vw_client_report1
                .Where(ss => ss.kind == kind)
                .ProjectTo<ClientReportViewModel>();
            */
        }
    }
}