﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientServiceParamViewModel : AuBaseViewModel
    {
        public ClientServiceParamViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT_SERVICE_PARAM)
        {
            //vw_client_service_param
        }
     
        [Key()]
        [Display(Name = "Код")]
        public int param_id { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Услуга")]
        public int service_id { get; set; }

        [Display(Name = "Макс. кол-во раб. мест")]
        public Nullable<int> max_workplace_cnt { get; set; }

        [Display(Name = "Услуга")]
        public string service_name { get; set; }
    }
}

