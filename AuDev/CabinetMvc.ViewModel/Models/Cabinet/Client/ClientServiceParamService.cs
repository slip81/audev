﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Hosting;
using System.Diagnostics;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientServiceParamService : IAuBaseService
    {
        //
    }

    public class ClientServiceParamService : AuBaseService, IClientServiceParamService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_client_service_param
                .Where(ss => ss.client_id == parent_id)
                .ProjectTo<ClientServiceParamViewModel>()
                ;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ClientServiceParamViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ClientServiceParamViewModel itemEdit = (ClientServiceParamViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры услуги");
                return null;
            }

            vw_client_service_param vw_client_service_param = dbContext.vw_client_service_param.Where(ss => ss.client_id == itemEdit.client_id && ss.service_id == itemEdit.service_id).FirstOrDefault();
            if (vw_client_service_param == null)
            {
                modelState.AddModelError("", "Не найдены параметры услуги");
                return null;
            }

            ClientServiceParamViewModel oldModel = new ClientServiceParamViewModel();
            ModelMapper.Map<vw_client_service_param, ClientServiceParamViewModel>(vw_client_service_param, oldModel);
            modelBeforeChanges = oldModel;

            bool paramExists = true;
            client_service_param client_service_param = dbContext.client_service_param.Where(ss => ss.client_id == itemEdit.client_id && ss.service_id == itemEdit.service_id).FirstOrDefault();
            if (client_service_param == null)
            {
                paramExists = false;
                client_service_param = new client_service_param();
            }

            ModelMapper.Map<ClientServiceParamViewModel, client_service_param>(itemEdit, client_service_param);
            if (!paramExists)
            {
                dbContext.client_service_param.Add(client_service_param);
            }
            dbContext.SaveChanges();

            ClientServiceParamViewModel res = new ClientServiceParamViewModel();
            ModelMapper.Map<client_service_param, ClientServiceParamViewModel>(client_service_param, res);

            return res;
        }

    }
}
