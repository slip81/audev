﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientInfoViewModel : AuBaseViewModel
    {

        public ClientInfoViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT_INFO)
        {
            //vw_client_info
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int client_id { get; set; }

        [Display(Name = "Наименование")]
        public string client_name { get; set; }

        [Display(Name = "Адрес")]
        public string client_address { get; set; }

        [Display(Name = "Телефон")]
        public string client_phone { get; set; }

        [Display(Name = "Контакт")]
        public string client_contact { get; set; }

        [Display(Name = "Торговых точек")]
        public Nullable<long> sales_count { get; set; }

        [Display(Name = "Услуг")]
        public Nullable<long> installed_service_cnt { get; set; }

        [Display(Name = "Заказанных услуг")]
        public Nullable<long> ordered_service_cnt { get; set; }
    }
}

