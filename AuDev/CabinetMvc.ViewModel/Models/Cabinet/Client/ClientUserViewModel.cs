﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientUserViewModel : AuBaseViewModel
    {

        public ClientUserViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT_LOGIN)
        {
            //vw_client_user
        }

        [Key()]
        [Display(Name = "Код")]
        public int user_id { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Торговая точка")]
        public Nullable<int> sales_id { get; set; }

        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Не задано имя")]
        public string user_name { get; set; }

        [Display(Name = "Полное имя")]
        public string user_name_full { get; set; }
        
        [Display(Name = "Основной логин для ТТ")]
        public bool is_main_for_sales { get; set; }

        [Display(Name = "Сменить пароль")]
        public bool need_change_pwd { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_address { get; set; }

        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Не задан логин")]
        public string user_login { get; set; }

        [Display(Name = "Электронная почта")]
        [Required(ErrorMessage = "Не задана электронная почта")]
        public string user_email { get; set; }

        [Display(Name = "Активен")]
        public Nullable<short> is_active { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public string user_pwd { get; set; }

        [Display(Name = "Привязан к торговой точке")]
        public bool is_for_sales { get; set; }

        [Display(Name = "Роль")]
        public Nullable<int> role_id { get; set; }

        [Display(Name = "Роль")]
        public string role_name { get; set; }

        [Display(Name = "is_admin")]
        public bool is_admin { get; set; }

        [Display(Name = "is_superadmin")]
        public bool is_superadmin { get; set; }

    }
}

