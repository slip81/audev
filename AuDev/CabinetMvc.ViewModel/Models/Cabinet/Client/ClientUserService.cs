﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Cabinet
{
    public interface IClientUserService : IAuBaseService
    {
        //
    }

    public class ClientUserService : AuBaseService, IClientUserService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            int client_id = (int)parent_id;
            //int client_id = 5191;
            return dbContext.vw_client_user.Where(ss => ss.client_id == client_id)
                .ProjectTo<ClientUserViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_client_user.Where(ss => ss.user_id == id)
                .ProjectTo<ClientUserViewModel>()
                .FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ClientUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ClientUserViewModel itemAdd = (ClientUserViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return null;
            }

            DateTime now = DateTime.Now;

            my_aspnet_users user_existing = dbContext.my_aspnet_users.Where(ss => ss.name == itemAdd.user_login).FirstOrDefault();
            if (user_existing != null)
            {
                modelState.AddModelError("", "Уже существует такой логин");
                return null;
            }

            int user_id = itemAdd.user_id > 0 ? itemAdd.user_id : dbContext.my_aspnet_users.OrderByDescending(ss => ss.id).Select(ss => ss.id).FirstOrDefault() + 1;
            my_aspnet_users user = new my_aspnet_users();
            user.applicationId = 1;
            user.isAnonymous = 0;
            user.lastActivityDate = now;
            user.name = itemAdd.user_login;
            user.id = user_id;
            dbContext.my_aspnet_users.Add(user);
            // т.к. нету FK между my_aspnet_membership и my_aspnet_users
            dbContext.SaveChanges();         

            EncrHelper2 encr = new EncrHelper2();
            my_aspnet_membership member = new my_aspnet_membership();
            member.userId = user.id;
            member.Email = itemAdd.user_email;
            member.Password = encr._SetHashValue(itemAdd.user_pwd);
            member.PasswordKey = member.Password;
            member.PasswordFormat = 2;
            member.IsApproved = 1;

            dbContext.my_aspnet_membership.Add(member);

            sales sales = dbContext.sales.Where(ss => ss.id == itemAdd.sales_id).FirstOrDefault();

            itemAdd.user_id = user.id;
            if (!itemAdd.is_for_sales)
                itemAdd.sales_id = null;
            if ((itemAdd.is_main_for_sales) && (itemAdd.sales_id.HasValue))
            {
                if (sales == null)
                {
                    modelState.AddModelError("", "Не найдена ТТ с кодом " + itemAdd.sales_id.ToString());
                    return null;
                }

                sales.user_id = itemAdd.user_id;

                var client_user_list = dbContext.client_user.Where(ss => ss.sales_id == itemAdd.sales_id && ss.user_id != itemAdd.user_id && ss.is_main_for_sales);
                foreach (var cu in client_user_list)
                {
                    cu.is_main_for_sales = false;
                }
            }

            if (itemAdd.role_id.HasValue)
            {
                auth_user_role auth_user_role = new auth_user_role();
                auth_user_role.role_id = (int)itemAdd.role_id;
                auth_user_role.user_id = user.id;
                dbContext.auth_user_role.Add(auth_user_role);
            }

            client_user client_user = new client_user();
            ModelMapper.Map<ClientUserViewModel, client_user>(itemAdd, client_user);

            dbContext.client_user.Add(client_user);
            dbContext.SaveChanges();

            ClientUserViewModel res = new ClientUserViewModel();
            ModelMapper.Map<client_user, ClientUserViewModel>(client_user, res);
            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ClientUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ClientUserViewModel itemEdit = (ClientUserViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return null;
            }

            client_user client_user = dbContext.client_user.Where(ss => ss.user_id == itemEdit.user_id).FirstOrDefault();
            if (client_user == null)
            {
                modelState.AddModelError("", "Не найден пользователь с кодом " + itemEdit.user_id);
                return null;
            }

            ClientUserViewModel oldModel = new ClientUserViewModel();
            ModelMapper.Map<client_user, ClientUserViewModel>(client_user, oldModel);
            modelBeforeChanges = oldModel;

            my_aspnet_membership member = dbContext.my_aspnet_membership.Where(ss => ss.userId == itemEdit.user_id).FirstOrDefault();
            if (member != null)
            {
                member.Email = itemEdit.user_email;
            }

            sales sales = dbContext.sales.Where(ss => ss.id == itemEdit.sales_id).FirstOrDefault();
            
            if (!itemEdit.is_for_sales)
                itemEdit.sales_id = null;
            if ((itemEdit.is_main_for_sales) && (itemEdit.sales_id.HasValue))
            {
                if (sales == null)
                {
                    modelState.AddModelError("", "Не найдена ТТ с кодом " + itemEdit.sales_id.ToString());
                    return null;
                }

                sales.user_id = itemEdit.user_id;
                var client_user_list = dbContext.client_user.Where(ss => ss.sales_id == itemEdit.sales_id && ss.user_id != itemEdit.user_id && ss.is_main_for_sales);
                foreach (var cu in client_user_list)
                {
                    cu.is_main_for_sales = false;
                }
            }
            else
            {
                if ((sales != null) && (sales.user_id == itemEdit.user_id))
                    sales.user_id = null;
            }

            auth_user_role curr_auth_user_role = dbContext.auth_user_role.Where(ss => ss.user_id == itemEdit.user_id).FirstOrDefault();
            if (curr_auth_user_role != null)
            {
                if (itemEdit.role_id != curr_auth_user_role.role_id)
                {
                    if (itemEdit.role_id.HasValue)
                    {
                        curr_auth_user_role.role_id = (int)itemEdit.role_id;
                    }
                    else
                    {
                        dbContext.auth_user_role.Remove(curr_auth_user_role);
                    }
                }
            }
            else
            {
                auth_user_role auth_user_role = new auth_user_role();
                auth_user_role.role_id = (int)itemEdit.role_id;
                auth_user_role.user_id = itemEdit.user_id;
                dbContext.auth_user_role.Add(auth_user_role);
            }

            ModelMapper.Map<ClientUserViewModel, client_user>(itemEdit, client_user);            
            dbContext.SaveChanges();

            return itemEdit;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ClientUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            ClientUserViewModel itemDel = (ClientUserViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return false;
            }

            client_user client_user = dbContext.client_user.Where(ss => ss.user_id == itemDel.user_id).FirstOrDefault();
            if (client_user == null)
            {
                modelState.AddModelError("", "Не найден пользователь с кодом " + itemDel.user_id);
                return false;
            }

            dbContext.auth_user_role.RemoveRange(dbContext.auth_user_role.Where(ss => ss.user_id == itemDel.user_id));
            dbContext.my_aspnet_membership.RemoveRange(dbContext.my_aspnet_membership.Where(ss => ss.userId == itemDel.user_id));
            dbContext.my_aspnet_users.Remove(dbContext.my_aspnet_users.Where(ss => ss.id == itemDel.user_id).FirstOrDefault());
            dbContext.client_user.Remove(client_user);
        
            dbContext.SaveChanges();

            return true;
        }
    }
}
