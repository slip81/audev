﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Cabinet
{
    public class ClientMaxVersionViewModel : AuBaseViewModel
    {
        public ClientMaxVersionViewModel()
            : base(Enums.MainObjectType.CAB_CLIENT_INFO)
        {
            //vw_client_max_version
        }
        
        [Key()]
        public int adr { get; set; }
        public Nullable<int> id { get; set; }
        public string name { get; set; }
        public string inn { get; set; }
        public string kpp { get; set; }
        public Nullable<int> sales_id { get; set; }
        public string adress { get; set; }
        public Nullable<int> workplace_id { get; set; }
        public string description { get; set; }
        public string registration_key { get; set; }
        public string service_name { get; set; }
        public string version_name { get; set; }
    }
}

