﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.User
{
    public class UserInfo
    {
        public bool is_authenticated { get; set; }
        public string login_name { get; set; }
        public string role_name { get; set; }
        public long? role_id { get; set; }        
        public long? business_id { get; set; }
        public long? cab_client_id { get; set; }
        public int? user_id { get; set; }
    }
}