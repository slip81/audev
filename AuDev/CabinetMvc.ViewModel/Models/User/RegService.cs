﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.Auth;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.User
{
    public interface IRegService
    {
        IQueryable<RegViewModel> GetList(int scope);
        IQueryable<RegViewModel> GetList_Active();
        RegViewModel GetItem(int reg_id);
        RegViewModel Insert(AuBaseViewModel item, ModelStateDictionary modelState);
        int Update(AuBaseViewModel item, ModelStateDictionary modelState);
    }

    public class RegService : IRegService
    {                
        AuMainDb dbContext;
        private AutoMapper.IMapper modelMapper { get; set; }

        public RegService()
        {
            dbContext = new AuMainDb();                       
            //
            modelMapper = DependencyResolver.Current.GetService<AutoMapper.IMapper>();
        }
        
        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        public IQueryable<RegViewModel> GetList(int scope)
        {
            return dbContext.reg                
                .Where(ss => ((scope >= 0) && (ss.state == scope)) || (scope < 0))
                .ProjectTo<RegViewModel>()
                ;
        }

        public IQueryable<RegViewModel> GetList_Active()
        {
            return dbContext.reg
                .Where(ss => ss.state == 0)
                .ProjectTo<RegViewModel>()
                ;
        }

        public RegViewModel GetItem(int reg_id)
        {
            return dbContext.reg
                .Where(ss => ss.reg_id == reg_id)
                .ProjectTo<RegViewModel>()
                .FirstOrDefault()
                ;
        }

        public RegViewModel Insert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is RegViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            RegViewModel itemAdd = (RegViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return null;
            }

            DateTime now = DateTime.Now;

            /*
            my_aspnet_users user_existing = dbContext.my_aspnet_users.Where(ss => ss.name == itemAdd.login).FirstOrDefault();
            if (user_existing != null)
            {
                modelState.AddModelError("", "Уже существует такой логин");
                return null;
            }

            EncrHelper2 encr = new EncrHelper2();
            itemAdd.pwd = encr._SetHashValue(itemAdd.pwd);
            itemAdd.crt_date = now;
            itemAdd.state = 0;
            itemAdd.ip_address = HttpContext.Current.Request.UserHostAddress;                

            my_aspnet_users user = new my_aspnet_users();
            user.applicationId = 1;
            user.isAnonymous = 0;
            user.lastActivityDate = now;
            user.name = itemAdd.login;

            dbContext.my_aspnet_users.Add(user);            
            dbContext.SaveChanges();
            
            
            my_aspnet_membership member = new my_aspnet_membership();
            member.userId = user.id;
            member.Email = itemAdd.email;
            member.Password = itemAdd.pwd;
            member.PasswordKey = member.Password;
            member.PasswordFormat = 2;
            member.IsApproved = 0;

            dbContext.my_aspnet_membership.Add(member);
            */
            itemAdd.pwd = "";
            itemAdd.login = "";

            reg reg = new reg();
            modelMapper.Map<RegViewModel, reg>(itemAdd, reg);

            dbContext.reg.Add(reg);
            dbContext.SaveChanges();

            RegViewModel res = new RegViewModel();
            modelMapper.Map<reg, RegViewModel>(reg, res);

            if (!(System.Diagnostics.Debugger.IsAttached))
            {
                // уведомление в сервис
                string mess1 = "Организация: " + itemAdd.org_name;
                mess1 += System.Environment.NewLine;
                mess1 += "Контакт: " + itemAdd.fio;
                mess1 += System.Environment.NewLine;
                mess1 += "Электронная почта: " + itemAdd.email;
                mess1 += System.Environment.NewLine;
                mess1 += "Телефон: " + (String.IsNullOrEmpty(itemAdd.phone) ? "-" : itemAdd.phone);
                mess1 += System.Environment.NewLine;
                mess1 += "Адрес: " + (String.IsNullOrEmpty(itemAdd.org_address) ? "-" : itemAdd.org_address);
                mess1 += System.Environment.NewLine;
                mess1 += "Ссылка на активацию: ";
                mess1 += System.Environment.NewLine;
                mess1 += @"https://cab.aptekaural.ru/RegEdit?reg_id=" + reg.reg_id;

                GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", "support@aptekaural.ru", "Новая заявка на регистрацию в ЛК [" + itemAdd.org_name + "]", mess1, null);

                // уведомление мне
                GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", "pavlov.ms.81@gmail.com", "Новая заявка на регистрацию в ЛК [" + itemAdd.org_name + "]", mess1, null);

                // уведомление клиенту
                if (!String.IsNullOrEmpty(itemAdd.email))
                {
                    string mess2 = "Регистрация успешно завершена ! Уведомление об активации аккаунта после проверки менеджером будет выслано на указанный при регистрации электронный адрес";
                    GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", itemAdd.email.Trim().ToLower(), "Заявка на регистрацию в Личном Кабинете", mess2, null);
                }
            }

            return res;
        }

        public int Update(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is RegViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return -1;
            }

            RegViewModel itemAdd = (RegViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты заявки");
                return -1;
            }

            if ((itemAdd.state < 1) || (itemAdd.state > 2))
            {
                modelState.AddModelError("", "Некорректный статус заявки");
                return -1;
            }            

            reg reg = dbContext.reg.Where(ss => ss.reg_id == itemAdd.reg_id).FirstOrDefault();
            if (reg == null)
            {
                modelState.AddModelError("", "Не найдена заявка с кодом " + itemAdd.reg_id.ToString());
                return -1;
            }

            DateTime now = DateTime.Now;

            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            var currUser = ((auth == null) || (auth.CurrentUser == null)) ? null : ((IUserProvider)auth.CurrentUser.Identity).User;
            reg.state = itemAdd.state;
            reg.upd_date = now;
            reg.upd_user = currUser == null ? null : currUser.CabUserName;

            if (itemAdd.state == 2)
            {
                dbContext.SaveChanges();
                return 0;
            }

            my_aspnet_users user_existing = dbContext.my_aspnet_users.Where(ss => ss.name.Trim().ToLower().Equals(itemAdd.login.Trim().ToLower())).FirstOrDefault();
            if (user_existing != null)
            {
                modelState.AddModelError("", "Уже существует такой логин");
                return -1;
            }

            ClientViewModel clientVM = new Cabinet.ClientViewModel()
            {
                client_name = itemAdd.org_name,
                absolute_address = itemAdd.org_address,
                legal_address = itemAdd.org_address,
                site = itemAdd.email,
                contact_person = itemAdd.fio,
                phone = itemAdd.phone,
                inn = "-",
                kpp = "-",
            };

            var clientService = DependencyResolver.Current.GetService<IClientService>();
            var res = clientService.Insert(clientVM, modelState);
            if (res == null)
            {
                modelState.AddModelError("", "Ошибка при создании клиента");
                return -1;
            }
            
            clientVM = (ClientViewModel)res;

            string pwd_not_encoded = itemAdd.pwd;
            EncrHelper2 encr = new EncrHelper2();
            itemAdd.pwd = encr._SetHashValue(pwd_not_encoded);

            int user_id = dbContext.my_aspnet_users.OrderByDescending(ss => ss.id).Select(ss => ss.id).FirstOrDefault() + 1;
            my_aspnet_users user = new my_aspnet_users();
            user.applicationId = 1;
            user.isAnonymous = 0;
            user.lastActivityDate = now;
            user.name = itemAdd.login;
            user.id = user_id;

            dbContext.my_aspnet_users.Add(user);
            dbContext.SaveChanges();

            my_aspnet_membership member = new my_aspnet_membership();
            member.userId = user.id;
            member.Email = itemAdd.email;
            member.Password = itemAdd.pwd;
            member.PasswordKey = member.Password;
            member.PasswordFormat = 2;
            member.IsApproved = 1;

            dbContext.my_aspnet_membership.Add(member);

            client_user client_user = new client_user();
            
            client_user.client_id = clientVM.id;
            client_user.is_main_for_sales = false;
            client_user.need_change_pwd = false;
            client_user.sales_id = null;
            client_user.user_id = user.id;
            client_user.user_name = itemAdd.login;
            client_user.user_name_full = itemAdd.fio;

            dbContext.client_user.Add(client_user);
            dbContext.SaveChanges();

            if (!(System.Diagnostics.Debugger.IsAttached))
            {
                // уведомление клиенту
                if (!String.IsNullOrEmpty(itemAdd.email))
                {
                    //string mess2 = "Регистрация успешно завершена ! Уведомление об активации аккаунта после проверки менеджером будет выслано на указанный при регистрации электронный адрес";
                    //string mess2 = "Ваш аккаунт в Личном Кабинете активирован ! Теперь вы можете авторизоваться с указаными при регистрации учетными данными по ссылке: " + @"http://cab.aptekaural.ru/";
                    string mess2 = "Ваш аккаунт в Личном Кабинете активирован ! Теперь вы можете авторизоваться по ссылке: " + @"https://cab.aptekaural.ru/";
                    mess2 += System.Environment.NewLine;
                    mess2 += "Логин: " + itemAdd.login.Trim();
                    mess2 += System.Environment.NewLine;
                    mess2 += "Пароль: " + pwd_not_encoded.Trim();
                    GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", itemAdd.email.Trim().ToLower(), "Аккаунт в Личном Кабинете активирован", mess2, null);
                }
            }

            return clientVM.id;
        }
    }

}
