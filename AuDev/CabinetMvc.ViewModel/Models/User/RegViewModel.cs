﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.User
{
    public class RegViewModel : AuBaseViewModel
    {

        public RegViewModel()
            : base(Enums.MainObjectType.CAB_REG)
        {
            //reg
        }

        [Key()]
        [Display(Name = "Код")]
        public int reg_id { get; set; }

        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Не задан логин")]
        public string login { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public string pwd { get; set; }

        [Display(Name = "Эл. почта")]
        [Required(ErrorMessage = "Не задана электронная почта")]
        public string email { get; set; }

        [Display(Name = "Организация")]
        [Required(ErrorMessage = "Не задано название организации")]
        public string org_name { get; set; }

        [Display(Name = "Адрес организации")]
        public string org_address { get; set; }

        [Display(Name = "ФИО")]
        [Required(ErrorMessage = "Не задано ФИО")]
        public string fio { get; set; }

        [Display(Name = "Телефон")]
        public string phone { get; set; }

        [Display(Name = "Статус")]
        public int state { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "IP-адрес")]
        public string ip_address { get; set; }

        [Display(Name = "Дата обработки")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Кто обработал")]
        public string upd_user { get; set; }

        public string state_name 
        { 
            get 
            {
                if (state == 0) 
                {
                    return "Активна";
                }
                else if (state == 1)
                {
                    return "Одобрена";
                }
                else
                {
                    return "Отклонена";
                }
            } 
        }
    }
}
