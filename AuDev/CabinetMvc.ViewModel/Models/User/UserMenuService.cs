﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.User
{
    public interface IUserMenuService : IAuBaseService
    {
        IQueryable<UserMenuViewModel> GetList_byUserName(string user_name);
    }

    public class UserMenuService : AuBaseService, IUserMenuService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.cab_user_menu
                .ProjectTo<UserMenuViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.cab_user_menu
                .Where(ss => ss.menu_id == id)
                .ProjectTo<UserMenuViewModel>()
                .FirstOrDefault();
        }

        public IQueryable<UserMenuViewModel> GetList_byUserName(string user_name)
        {
            return dbContext.cab_user_menu
                .Where(ss => ss.user_name.Trim().ToLower().Equals(user_name.Trim().ToLower()))
                .OrderBy(ss => ss.sort_num)
                .ProjectTo<UserMenuViewModel>()
                ;
        }
    
    }

}
