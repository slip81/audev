﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.User
{
    public class UserProfileViewModel : AuBaseViewModel
    {
        public UserProfileViewModel()
            : base(Enums.MainObjectType.CAB_USER_PROFILE)
        {            
            //cab_user_profile
        }

        [Key()]
        [Display(Name = "Код")]
        public int profile_id { get; set; }

        [Display(Name = "Имя пользователя")]
        public string user_name { get; set; }

        [Display(Name = "Стартовая страница")]
        public string start_page { get; set; }

        [Display(Name = "Стартовая страница")]
        [JsonIgnore()]
        public string start_page_title { get; set; }

        [Display(Name = "Аватар")]
        public string avatar { get; set; }

        [JsonIgnore()]
        public IEnumerable<CabinetMvc.ViewModel.User.UserMenuViewModel> userMenuList { get; set; }

    }
}