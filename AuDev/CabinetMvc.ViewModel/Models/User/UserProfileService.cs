﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.User
{
    public interface IUserProfileService : IAuBaseService
    {
        UserProfileViewModel GetItem_byUserName(string user_name);
    }

    public class UserProfileService : AuBaseService, IUserProfileService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.cab_user_profile
                .ProjectTo<UserProfileViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.cab_user_profile
                .Where(ss => ss.profile_id == id)
                .ProjectTo<UserProfileViewModel>()
                .FirstOrDefault();
        }

        public UserProfileViewModel GetItem_byUserName(string user_name)
        {
            return dbContext.cab_user_profile
                .Where(ss => ss.user_name.Trim().ToLower().Equals(user_name.Trim().ToLower()))
                .ProjectTo<UserProfileViewModel>()
                .FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UserProfileViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            UserProfileViewModel itemAdd = (UserProfileViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            cab_user_profile cab_user_profile = new cab_user_profile();
            ModelMapper.Map<UserProfileViewModel, cab_user_profile>(itemAdd, cab_user_profile);

            dbContext.cab_user_profile.Add(cab_user_profile);

            int sort_num = 1;
            if (itemAdd.userMenuList != null)
            {
                foreach (var userMenu in itemAdd.userMenuList)
                {
                    cab_user_menu cab_user_menu = new cab_user_menu();
                    cab_user_menu.action = userMenu.action;
                    cab_user_menu.controller = userMenu.controller;
                    cab_user_menu.img = userMenu.img;                    
                    cab_user_menu.sort_num = sort_num;
                    cab_user_menu.title = userMenu.title;
                    cab_user_menu.user_name = itemAdd.user_name;
                    dbContext.cab_user_menu.Add(cab_user_menu);
                    sort_num++;
                }
            }

            dbContext.SaveChanges();

            UserProfileViewModel res = new UserProfileViewModel();
            ModelMapper.Map<cab_user_profile, UserProfileViewModel>(cab_user_profile, res);
            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UserProfileViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            UserProfileViewModel itemEdit = (UserProfileViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var cab_user_profile = dbContext.cab_user_profile.Where(ss => ss.profile_id == itemEdit.profile_id).FirstOrDefault();
            if (cab_user_profile == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.profile_id.ToString());
                return null;
            }
            
            ModelMapper.Map<cab_user_profile, UserProfileViewModel>(cab_user_profile, (UserProfileViewModel)modelBeforeChanges);
            ModelMapper.Map<UserProfileViewModel, cab_user_profile>(itemEdit, cab_user_profile);

            int sort_num = 1;
            dbContext.cab_user_menu.RemoveRange(dbContext.cab_user_menu.Where(ss => ss.user_name.Trim().ToLower().Equals(itemEdit.user_name.Trim().ToLower())));
            if (itemEdit.userMenuList != null)
            {
                dbContext.cab_user_menu.RemoveRange(dbContext.cab_user_menu.Where(ss => ss.user_name.Trim().ToLower().Equals(itemEdit.user_name.Trim().ToLower())));
                foreach (var userMenu in itemEdit.userMenuList)
                {
                    cab_user_menu cab_user_menu = new cab_user_menu();
                    cab_user_menu.action = userMenu.action;
                    cab_user_menu.controller = userMenu.controller;
                    cab_user_menu.img = userMenu.img;
                    //cab_user_menu.sort_num = userMenu.sort_num;
                    cab_user_menu.sort_num = sort_num;
                    cab_user_menu.title = userMenu.title;
                    cab_user_menu.user_name = itemEdit.user_name;
                    dbContext.cab_user_menu.Add(cab_user_menu);
                    sort_num++;
                }
            }

            dbContext.SaveChanges();

            UserProfileViewModel res = new UserProfileViewModel();
            ModelMapper.Map<cab_user_profile, UserProfileViewModel>(cab_user_profile, res);
            return res;
        }
        
    }

}
