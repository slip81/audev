﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using CabinetMvc.ViewModel;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.User
{
    public interface IUserService
    {
        UserViewModel GetUser(string user_name, long business_id, long role_id, long cab_client_id);
        UserViewModel Login(string userName, string password);        
    }

    public class UserService : IDisposable, IUserService
    {
        protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        AuMainDb dbContext;
        private AutoMapper.IMapper modelMapper { get; set; }

        public UserService()
        {
            dbContext = new AuMainDb();                       
            //
            modelMapper = DependencyResolver.Current.GetService<AutoMapper.IMapper>();
            //
            CreateUserRoles();
        }
        
        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        private Dictionary<long, string> UserRoles { get; set; }
        private void CreateUserRoles()
        {
            UserRoles = new Dictionary<long, string>();
            UserRoles.Add(101, "Administrators");
            UserRoles.Add(102, "Testers");
            UserRoles.Add(103, "Users");
            UserRoles.Add(104, "Superadmin");
        }

        private UserInfo Authenticate(string user_name, string user_pwd)
        {
            //logger.Info("Authenticate start");

            CreateUserRoles();

            EncrHelper2 encr = new EncrHelper2();

            int step_num = 0;

            step_num = 1;
            DirectoryEntry de = new DirectoryEntry();

            bool isAu = false;
            string currRoleName = "";
            long currRoleId = 0;

            DirectorySearcher deSearch;
            SearchResult result;

            DirectoryEntry de_found;
            long business_id_from_user = 0;

            List<long> business_id_list = null;
            business_id_list = dbContext.business.Select(ss => ss.business_id).ToList();

            foreach (var userRole in UserRoles)
            {
                isAu = false;
                currRoleName = userRole.Value;
                currRoleId = userRole.Key;

                foreach (var business_id in business_id_list)
                {

                    de.Path = "LDAP://10.0.1.2:389/uid=" + business_id.ToString() + ",ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";
                    de.Username = "cn=" + user_name + ",uid=" + business_id.ToString() + ",ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";

                    de.Password = encr._GetHashValue(user_pwd);
                    de.AuthenticationType = AuthenticationTypes.None;

                    deSearch = new DirectorySearcher();
                    deSearch.SearchRoot = de;

                    try
                    {
                        result = deSearch.FindOne();
                        isAu = true;

                        business_id_from_user = business_id;
                        break;

                        de_found = result.GetDirectoryEntry();

                        foreach (DirectoryEntry child in de_found.Children)
                        {
                            System.DirectoryServices.PropertyCollection pc = child.Properties;
                            IDictionaryEnumerator ide = pc.GetEnumerator();
                            ide.Reset();
                            while (ide.MoveNext())
                            {
                                PropertyValueCollection pvc = ide.Entry.Value as PropertyValueCollection;
                                if ((ide.Entry.Key.ToString().ToLower().Trim().Equals("cn"))
                                    && (pvc.Value.ToString().ToLower().Trim().Equals(user_name.ToLower().Trim())))
                                {
                                    foreach (DirectoryEntry child2 in child.Children)
                                    {
                                        System.DirectoryServices.PropertyCollection pc2 = child2.Properties;
                                        IDictionaryEnumerator ide2 = pc2.GetEnumerator();
                                        ide2.Reset();
                                        while (ide2.MoveNext())
                                        {
                                            PropertyValueCollection pvc2 = ide2.Entry.Value as PropertyValueCollection;
                                            if (ide2.Entry.Key.ToString().ToLower().Trim().Equals("uid"))
                                            {
                                                business_id_from_user = Convert.ToInt64(pvc2.Value.ToString());
                                            }
                                            if (business_id_from_user > 0)
                                                break;
                                        }
                                        if (business_id_from_user > 0)
                                            break;
                                    }
                                }

                                if (business_id_from_user > 0)
                                    break;
                            }
                            if (business_id_from_user > 0)
                                break;
                        }
                    }
                    catch
                    {
                        isAu = false;
                    }

                    if (isAu)
                        break;

                }
                if (isAu)
                    break;
            }

            // !!!
            // logger.Info("Authenticate end");

            int? curr_user_id = null;
            if (isAu)
            {
                var client_user = dbContext.vw_client_user.Where(ss => ss.is_active == 1 && ss.user_login.Trim().ToLower().Equals(user_name.Trim().ToLower())).FirstOrDefault();
                if (client_user != null)
                    curr_user_id = client_user.user_id;
            }
            
            return new UserInfo()
            {
                is_authenticated = isAu,
                login_name = user_name,
                role_name = currRoleName,
                role_id = currRoleId,
                business_id = business_id_from_user,
                user_id = curr_user_id,
            };
        }

        private UserInfo Authenticate2(string user_name, string user_pwd)
        {
            // !!!
            // logger.Info("Authenticate2 start");

            EncrHelper2 encr = new EncrHelper2();
            //string pwd = encr._GetHashValue(user_pwd);
            string pwd = user_pwd;

            var user = (from t1 in dbContext.my_aspnet_users
                          from t2 in dbContext.my_aspnet_membership
                          where t1.id == t2.userId
                          && t2.IsApproved == 1
                          && t1.name == user_name
                          && t2.Password == pwd
                          && t2.PasswordFormat == 2
                          select t1
                         )
                         .FirstOrDefault();

            if (user == null)
                return new UserInfo()
                {
                    is_authenticated = false,
                    login_name = user_name,
                    role_name = "",
                    role_id = 0,
                    business_id = null,
                    cab_client_id = null,
                }
                ;
            
            vw_client_user vw_client_user = dbContext.vw_client_user.Where(ss => ss.user_id == user.id).FirstOrDefault();
            if (vw_client_user == null)
                return new UserInfo()
                {
                    is_authenticated = false,
                    login_name = user_name,
                    role_name = "",
                    role_id = 0,
                    business_id = null,
                    cab_client_id = null,
                }
                ;

            // !!!
            // logger.Info("Authenticate2 end");
            
            return new UserInfo()
            {
                is_authenticated = true,
                login_name = user_name,
                //role_name = "Users",
                //role_id = 103,
                role_name = vw_client_user.is_superadmin ? "Superadmin" : (vw_client_user.is_admin ? "Administrators" : "Users"),
                role_id = vw_client_user.is_superadmin ? (int)Enums.UserRole.SUPERADMIN : (vw_client_user.is_admin ? (int)Enums.UserRole.ADMIN : (int)Enums.UserRole.USER),
                business_id = null,
                cab_client_id = vw_client_user.client_id,
                user_id = vw_client_user.user_id,
            }
            ;

        }

        public UserViewModel GetUser(string user_name, long business_id, long role_id, long cab_client_id) 
        {
            //logger.Info("GetUser start");

            long businessId = 0;
            string businessName = "";
            business business = null;
            allclients cab_client = null;
            vw_cab_user2 cab_user = null;

            if (cab_client_id > 0)
            {
                cab_client = dbContext.allclients.Where(ss => ss.id == cab_client_id).FirstOrDefault();
                if (cab_client == null)
                    return null;
                long business_id_long = 0;
                var parse_res = long.TryParse(cab_client.business_id, out business_id_long);
                if (parse_res)
                    business = dbContext.business.Where(ss => ss.business_id == business_id_long).FirstOrDefault();
                if (business != null)
                {
                    businessId = business_id_long;
                    businessName = business.business_name;
                }
                else
                {
                    businessId = 0;
                    businessName = cab_client.client_name;
                }                    
            }
            else if (business_id > 0)
            {
                business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return null;
                businessId = business_id;
                businessName = business.business_name;
                if (business.cabinet_client_id.HasValue)
                    cab_client = dbContext.allclients.Where(ss => ss.id == business.cabinet_client_id).FirstOrDefault();
            }
            else return null;

            List<string> actions = (from cura in dbContext.cab_user_role_action
                                    from ca in dbContext.cab_action
                                    where ((cura.action_id == ca.action_id && !cura.group_id.HasValue) || (cura.group_id == ca.group_id && !cura.action_id.HasValue))
                                    && cura.user_name == user_name
                                    select ca.sys_action_name)
                                    .Distinct()
                                    .ToList();

            var cab_users = dbContext.vw_cab_user2.Where(ss => ss.user_login.Trim().ToLower().Equals(user_name.Trim().ToLower())).ToList();

            if ((cab_users != null) && (cab_users.Count > 0))
                cab_user = cab_users.FirstOrDefault();

            if ((cab_user != null) && (!cab_user.is_active))
                return null;

            //var client_user = dbContext.vw_client_user.Where(ss => ss.client_id == cab_client.id && ss.is_active == 1 && ss.user_login.Trim().ToLower().Equals(user_name.Trim().ToLower())).FirstOrDefault();
            var client_user = dbContext.vw_client_user.Where(ss => ss.is_active == 1 && ss.user_login.Trim().ToLower().Equals(user_name.Trim().ToLower())).FirstOrDefault();

            List<Enums.ClientServiceEnum> servList = new List<Enums.ClientServiceEnum>();
            var client_service_list = dbContext.client_service.Where(ss => ss.client_id == cab_client.id && ss.state == 0).ToList();
            if (client_service_list != null)
            {
                foreach (var serv in client_service_list)
                {
                    Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
                    bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(serv.service_id.ToString(), out servEnum);
                    if (parseOk)
                        servList.Add(servEnum);
                }
            }
            
            /*
            var have_defect = dbContext.vw_client_service.Where(ss => ss.client_id == cab_client.id && ss.service_id == 34).FirstOrDefault();
            var have_exchange = dbContext.exchange_client.Where(ss => ss.cabinet_client_id == cab_client.id).FirstOrDefault();
            */

            ClientInfoViewModel clientInfoVm = null;
            List<ClientServiceSummaryViewModel> serviceSummaryVm = null;
            if (cab_client != null)
            {                
                clientInfoVm = new ClientInfoViewModel();
                vw_client_info vw_client_info = dbContext.vw_client_info.Where(ss => ss.client_id == cab_client.id).FirstOrDefault();
                modelMapper.Map<vw_client_info, ClientInfoViewModel>(vw_client_info, clientInfoVm);
                //
                serviceSummaryVm = dbContext.vw_client_service_summary
                    .Where(ss => ss.client_id == cab_client.id)
                    .Select(ss => new ClientServiceSummaryViewModel()
                    {
                        client_id = ss.client_id,
                        sales_cnt = ss.sales_cnt,
                        service_id = ss.service_id,
                    })
                    .ToList();                
            }

            List<int> allowedParts_View = null;
            List<int> allowedParts_Edit = null;
            List<vw_auth_user_perm> allowedParts = null;
            if (client_user != null)
            {
                allowedParts = dbContext.vw_auth_user_perm.Where(ss => ss.part_id.HasValue && ss.user_id == client_user.user_id && ((ss.allow_view) || (ss.allow_edit))).ToList();
            }

            if (allowedParts != null)
            {
                allowedParts_View = allowedParts.Where(ss => ss.allow_view).Select(ss => (int)ss.part_id).ToList();
                allowedParts_Edit = allowedParts.Where(ss => ss.allow_edit).Select(ss => (int)ss.part_id).ToList();
            }

            /*
            int? eventOverdueCount = null;
            int? eventStrategCount = null;
            if ((cab_user != null) &&  (cab_user.is_crm_user))
            {
                eventOverdueCount = dbContext.vw_planner_event.Where(ss => ss.exec_user_id == cab_user.user_id && ss.is_overdue).Count();
                eventStrategCount = dbContext.vw_planner_event.Where(ss => ss.exec_user_id == cab_user.user_id && ss.priority_is_boss == true && ss.is_done != true).Count();
            }
            */

            UserViewModel user = new UserViewModel()
            {
                ActionList = actions == null ? null : string.Join(",", actions.ToArray()),
                ServiceList = servList,
                BusinessId = businessId,
                BusinessName = businessName,
                UserName = user_name,
                RoleId = role_id,
                //clientInfo = cab_client != null ? new Cabinet.ClientInfoViewModel(cab_client) : null,
                clientInfo = clientInfoVm,
                serviceSummary = serviceSummaryVm,
                cabUserInfo = cab_user != null ? new Adm.CabUserViewModel(cab_user, client_user == null ? null : (int?)client_user.user_id) : new Adm.CabUserViewModel(user_name, user_name, client_user == null ? null : (int?)client_user.user_id),
                AllowedParts_View = allowedParts_View,
                AllowedParts_Edit = allowedParts_Edit,
                //EventOverdueCount = eventOverdueCount,
                //EventStrategCount = eventStrategCount,
                IsMale = cab_user != null ? cab_user.is_male : true,
            }
            ;

            /*
            user.HaveDiscount = ((user.IsAdmin) || (user.BusinessId > 0));            
            user.HaveDefect = ((user.IsAdmin) || (have_defect != null));
            user.HaveExchange = ((user.IsAdmin) || (have_exchange != null));
            */
             
            //logger.Info("GetUser end");

            return user;
        }
        
        public UserViewModel Login(string userName, string password)
        {
            //logger.Info("Login start");

            EncrHelper2 encr = new EncrHelper2();

            string user_name = userName;
            string user_pwd = encr._SetHashValue(String.IsNullOrEmpty(password) ? "" : password);

            UserInfo ui = new UserInfo();

            //if (System.Diagnostics.Debugger.IsAttached)
            if (1 != 1)
            {
                if (user_name.Trim().ToLower().Equals("empty") && (user_pwd.Equals("")))
                {
                    // !!! биона
                    user_name = "d00471";

                    ui.is_authenticated = true;
                    ui.login_name = user_name;
                    ui.business_id = 939659093734327298; // биона
                    ui.role_id = (long)Enums.UserRole.USER;
                    ui.user_id = 7458;                    
                }
                else if (user_name.Trim().ToLower().Equals("eadmin") && (user_pwd.Equals("")))
                {
                    ui.is_authenticated = true;
                    ui.login_name = user_name;
                    ui.business_id = 1007661065565111774; // ооо аурит (тест админ)
                    ui.role_id = (long)Enums.UserRole.ADMIN;
                }
                else if (user_name.Trim().ToLower().Equals("esadmin") && (user_pwd.Equals("")))
                {
                    ui.is_authenticated = true;
                    ui.login_name = user_name;
                    ui.business_id = 1007661065565111774; // ооо аурит (тест супер-админ)
                    ui.role_id = (long)Enums.UserRole.SUPERADMIN;
                }

                if (!(ui.is_authenticated))
                {                    
                    ui = Authenticate2(userName, user_pwd);
                    if (!(ui.is_authenticated))
                        return null;
                }                    
            }
            else
            {
                // !!!
                // отключим аутентификацию по логинам ДС
                // ui = Authenticate(user_name, user_pwd);
                if (!(ui.is_authenticated))
                {
                    ui = Authenticate2(user_name, user_pwd);
                    if (!(ui.is_authenticated))
                        return null;
                }
            }

            allclients cab_client = null;
            business business = null;
            vw_cab_user2 cab_user = null;

            long businessId = 0;
            string businessName = "";
            if (ui.cab_client_id.HasValue)
                cab_client = dbContext.allclients.Where(ss => ss.id == ui.cab_client_id).FirstOrDefault();
            if (ui.business_id.HasValue)
            {
                business = dbContext.business.Where(ss => ss.business_id == ui.business_id).FirstOrDefault();
                if (business != null)
                {
                    businessId = business.business_id;
                    businessName = business.business_name;
                    if (business.cabinet_client_id.HasValue)
                        cab_client = dbContext.allclients.Where(ss => ss.id == business.cabinet_client_id).FirstOrDefault();
                }
            }

            List<string> actions = (from cura in dbContext.cab_user_role_action
                                    from ca in dbContext.cab_action
                                    where ((cura.action_id == ca.action_id && !cura.group_id.HasValue) || (cura.group_id == ca.group_id && !cura.action_id.HasValue))
                                    && cura.user_name == user_name
                                    select ca.sys_action_name)
                                    .Distinct()
                                    .ToList();

            if ((actions == null) || (actions.Count <= 0))
                actions = dbContext.cab_action.Select(ss => ss.sys_action_name).Distinct().ToList();

            var cab_users = dbContext.vw_cab_user2.Where(ss => ss.user_login.Trim().ToLower().Equals(user_name.Trim().ToLower())).ToList();
            if ((cab_users != null) && (cab_users.Count > 0))
                cab_user = cab_users.FirstOrDefault();

            List<Enums.ClientServiceEnum> servList = new List<Enums.ClientServiceEnum>();
            var client_service_list = dbContext.client_service.Where(ss => ss.client_id == cab_client.id && ss.state == 0).ToList();
            if (client_service_list != null)
            {
                foreach (var serv in client_service_list)
                {
                    Enums.ClientServiceEnum servEnum = Enums.ClientServiceEnum.NONE;
                    bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(serv.service_id.ToString(), out servEnum);
                    if (parseOk)
                        servList.Add(servEnum);
                }
            }

            /*
            var have_defect = dbContext.vw_client_service.Where(ss => ss.client_id == cab_client.id && ss.service_id == 34).FirstOrDefault();
            var have_exchange = dbContext.exchange_client.Where(ss => ss.cabinet_client_id == cab_client.id).FirstOrDefault();
            */

            ClientInfoViewModel clientInfoVm = null;
            List<ClientServiceSummaryViewModel> serviceSummaryVm = null;
            if (cab_client != null)
            {
                clientInfoVm = new ClientInfoViewModel();
                vw_client_info vw_client_info = dbContext.vw_client_info.Where(ss => ss.client_id == cab_client.id).FirstOrDefault();
                modelMapper.Map<vw_client_info, ClientInfoViewModel>(vw_client_info, clientInfoVm);
                //
                serviceSummaryVm = dbContext.vw_client_service_summary
                    .Where(ss => ss.client_id == cab_client.id)
                    .Select(ss => new ClientServiceSummaryViewModel()
                    {
                        client_id = ss.client_id,
                        sales_cnt = ss.sales_cnt,
                        service_id = ss.service_id,
                    })
                    .ToList();
            }

            List<int> allowedParts_View = null;
            List<int> allowedParts_Edit = null;
            List<vw_auth_user_perm> allowedParts = dbContext.vw_auth_user_perm.Where(ss => ss.part_id.HasValue && ss.user_id == ui.user_id && ((ss.allow_view) || (ss.allow_edit))).ToList();
            if (allowedParts != null)
            {
                allowedParts_View = allowedParts.Where(ss => ss.allow_view).Select(ss => (int)ss.part_id).ToList();
                allowedParts_Edit = allowedParts.Where(ss => ss.allow_edit).Select(ss => (int)ss.part_id).ToList();
            }

            /*
            int? eventOverdueCount = null;
            int? eventStrategCount = null;
            if ((cab_user != null) && (cab_user.is_crm_user))
            {
                eventOverdueCount = dbContext.vw_planner_event.Where(ss => ss.exec_user_id == cab_user.user_id && ss.is_overdue).Count();
                eventStrategCount = dbContext.vw_planner_event.Where(ss => ss.exec_user_id == cab_user.user_id && ss.priority_is_boss == true && ss.is_done != true).Count();
            }
            */

            UserViewModel user = new UserViewModel()
            {
                ActionList = actions == null ? null : string.Join(",", actions.ToArray()),
                ServiceList = servList,
                BusinessId = businessId,
                BusinessName = businessName,
                RoleId = ui.role_id,
                UserName = user_name,
                //clientInfo = cab_client != null ? new Cabinet.ClientViewModel(cab_client) : null,
                clientInfo = clientInfoVm,
                serviceSummary = serviceSummaryVm,
                cabUserInfo = cab_user != null ? new Adm.CabUserViewModel(cab_user, ui.user_id) : new Adm.CabUserViewModel(user_name, user_name, ui.user_id),
                AllowedParts_View = allowedParts_View,
                AllowedParts_Edit = allowedParts_Edit,
                //EventOverdueCount = eventOverdueCount,
                //EventStrategCount = eventStrategCount,
                IsMale = cab_user != null ? cab_user.is_male : true,
            }
            ;
            
            /*
            user.HaveDiscount = ((user.IsAdmin) || (user.BusinessId > 0));
            user.HaveDefect = ((user.IsAdmin) || (have_defect != null));
            user.HaveExchange = ((user.IsAdmin) || (have_exchange != null));
            */

            //logger.Info("Login end");
            return user;
        }

    }
}