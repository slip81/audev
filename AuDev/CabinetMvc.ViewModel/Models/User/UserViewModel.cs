﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.User
{
    public class UserViewModel
    {
        [Display(Name = "Логин:")]        
        [Required(ErrorMessage="Не задан логин")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Пароль:")]
        public string Password { get; set; }

        [Display(Name = "запомнить меня")]        
        public bool RememberMe { get; set; }

        [Display(Name = "ReturnUrl")]
        public string ReturnUrl { get; set; }

        
        public string WelcomeString 
        {
            get { return String.IsNullOrEmpty(BusinessName) ? "-" : "Добро пожаловать, " + BusinessName.Trim(); }
        }

        public long BusinessId 
        { 
            get
            {
                return businessId;
            }
            set
            {                
                if (businessId != value)
                {
                    businessId = value;
                }                                
            }
        }
        private long businessId;

        public Cabinet.ClientInfoViewModel clientInfo { get; set; }
        public List<Cabinet.ClientServiceSummaryViewModel> serviceSummary { get; set; }
        public Adm.CabUserViewModel cabUserInfo { get; set; }

        public long CabClientId
        {
            get
            {
                return clientInfo == null ? 0 : clientInfo.client_id;
            }
        }

        public int CabUserId
        {
            get
            {
                return cabUserInfo == null ? 0 : cabUserInfo.user_id;
            }
        }

        public string CabUserName
        {
            get
            {                
                return cabUserInfo == null ? "[не определен]" : cabUserInfo.user_name;
            }
        }

        public string CabUserLogin
        {
            get
            {
                return cabUserInfo == null ? "[не определен]" : cabUserInfo.user_login;
            }
        }

        public string CabUserFullName
        {
            get
            {
                return cabUserInfo == null ? "[не определен]" : cabUserInfo.full_name;
            }
        }

        public int CabUserCrmRoleId
        {
            get
            {
                return cabUserInfo == null ? 0 : cabUserInfo.crm_user_role_id.GetValueOrDefault(0);
            }
        }

        public string BusinessName
        {
            get
            {
                return businessName;
            }
            set
            {
                if (businessName != value)
                {
                    businessName = value;
                }
            }
        }
        private string businessName;
        
        public long? RoleId
        {
            get
            {
                return roleId;
            }
            set
            {                
                if (roleId != value)
                {
                    roleId = value;
                }               
            }
        }
        private long? roleId;
        
        public string RoleName
        {
            get
            {
                switch (RoleId)
                {
                    case 101:
                        return "Administrators";
                    case 102:
                        return "Testers";
                    case 103:
                        return "Users";
                    case 104:
                        return "Superadmin";
                    default:
                        return "";
                }
            }
        }
        public bool IsAdmin
        {
            get
            {
                switch (RoleId)
                {
                    case 101:
                    case 104:
                        return true;
                    default:
                        return false;
                }
            }
        }
        public bool IsSuperAdmin
        {
            get
            {
                switch (RoleId)
                {                    
                    case 104:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsBoss
        {
            get
            {
                return ((this.CabUserId == 1) || (this.CabUserId == 2));
            }
        }

        public bool IsBossAssistant
        {
            get
            {
                return ((this.IsBoss) || (this.CabUserId == 13));
            }
        }

        public bool IsMale
        {
            get
            {
                return isMale;
            }
            set
            {
                if (isMale != value)
                {
                    isMale = value;
                }
            }
        }
        private bool isMale;

        public string ActionList {get; set;}

        public bool HaveAction(string action)
        {
            if (IsAdmin)
                return true;
            return ((String.IsNullOrEmpty(ActionList)) || (String.IsNullOrEmpty(action))) ? false : ActionList.Contains(action);
        }


        // !!!
        // todo
        public bool InRoles(string roles)
        {
            if (string.IsNullOrWhiteSpace(roles)) 
            { 
                return false; 
            }
            var rolesArray = roles.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries); 
            
            foreach (var role in rolesArray) 
            { 
                // !!!
                //var hasRole = UserRoles.Any(p => string.Compare(p.Role.Code, role, true) == 0);
                var hasRole = false; 
                if (hasRole) 
                { 
                    return true; 
                } 
            } 
            return false;
        }


        public List<Enums.ClientServiceEnum> ServiceList { get; set; }
        public bool HaveService(Enums.ClientServiceEnum serv)
        {
            if (IsAdmin)
                return true;
            return ServiceList.Contains(serv);
        }

        public List<int> AllowedParts_View { get; set; }

        public List<int> AllowedParts_Edit { get; set; }

        public bool AllowedView(Enums.AuthPartEnum part)
        {
            if ((AllowedParts_View == null) || (AllowedParts_View.Count <= 0))
                return false;
            var allow = AllowedParts_View.Where(ss => ss == (int)part).FirstOrDefault();
            return allow > 0;
        }

        public bool AllowedEdit(Enums.AuthPartEnum part)
        {
            if ((AllowedParts_Edit == null) || (AllowedParts_Edit.Count <= 0))
                return false;
            var allow = AllowedParts_Edit.Where(ss => ss == (int)part).FirstOrDefault();
            return allow > 0;
        }

        /*
        public int? EventOverdueCount { get; set; }        
        public int? EventStrategCount { get; set; }
        */
    }
}