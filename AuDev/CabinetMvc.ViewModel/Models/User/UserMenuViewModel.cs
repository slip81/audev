﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.User
{
    public class UserMenuViewModel : AuBaseViewModel
    {
        public UserMenuViewModel()
            : base(Enums.MainObjectType.CAB_USER_MENU)
        {            
            //cab_user_menu
        }

        [Key()]
        [Display(Name = "Код")]
        public int menu_id { get; set; }

        [Display(Name = "Пользователь")]
        public string user_name { get; set; }

        [Display(Name = "Номер")]
        public int sort_num { get; set; }

        [Display(Name = "title")]
        public string title { get; set; }

        [Display(Name = "controller")]
        public string controller { get; set; }

        [Display(Name = "action")]
        public string action { get; set; }

        [Display(Name = "img")]
        public string img { get; set; }

        [Display(Name = "img_class")]
        public string img_class { get { return String.IsNullOrEmpty(img) ? "" : (img.EndsWith(".png") ? img.Substring(0, img.Length - 4) : img); } }

    }
}