﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Defect
{
    public interface IDefectService : IAuBaseService
    {
        bool IsDefectDownloadAllowed(string workplace);
        bool IsDefectDownloadAllowed_forClient(long client_id);
    }

    public class DefectService : AuBaseService, IDefectService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_defect.Select(ss => new DefectViewModel()
            {
                defect_id = ss.defect_id,
                comm_name = ss.comm_name,
                pack_name = ss.pack_name,
                full_name = ss.full_name,
                series = ss.series,
                producer = ss.producer,
                defect_status_id = ss.defect_status_id,
                defect_type = ss.defect_type,
                country_id = ss.country_id,
                document_id = ss.document_id,
                parent_defect_id = ss.parent_defect_id,
                defect_status_text = ss.defect_status_text,
                scope = ss.scope,
                is_drug = ss.is_drug,
                old_id = ss.old,
                created_on = ss.created_on,
                created_by = ss.created_by,
                updated_on = ss.updated_on,
                updated_by = ss.updated_by,
                defect_source_id = ss.defect_source_id,
                document_name = ss.document_name,
                document_num = ss.document_num,
                accept_date = ss.accept_date,
                create_date = ss.create_date,
                link = ss.link,
                comment = ss.comment,
                document_type_id = ss.document_type_id,
                body_size_kb = ss.body_size_kb,
                doc_created_on = ss.doc_created_on,
                doc_created_by = ss.doc_created_by,
                doc_updated_on = ss.doc_updated_on,
                doc_updated_by = ss.doc_updated_by,
                old = ss.old,
                status_name = ss.status_name,
                source_name = ss.source_name,
                country_name = ss.country_name,
            }
            );
        }

        public bool IsDefectDownloadAllowed(string workplace)
        {
            var res = (from t1 in dbContext.service_defect
                       from t2 in dbContext.workplace
                       where t1.workplace_id == t2.id
                       && t1.service_id == (int)Enums.ClientServiceEnum.DEFECT
                       && t2.is_deleted == 0
                       && t2.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                       && t1.is_allow_download
                       select t1)
                      .FirstOrDefault();

            return res != null;
        }
        
        public bool IsDefectDownloadAllowed_forClient(long client_id)
        {
            var res = (from t1 in dbContext.service_defect
                       from t2 in dbContext.workplace
                       from t3 in dbContext.sales
                       where t1.workplace_id == t2.id
                       && t2.sales_id == t3.id
                       && t1.service_id == (int)Enums.ClientServiceEnum.DEFECT
                       && t2.is_deleted == 0
                       && t3.is_deleted == 0
                       && t3.client_id == client_id
                       && t1.is_allow_download
                       select t1)
          .FirstOrDefault();

            return res != null;
        }
       
    }
}