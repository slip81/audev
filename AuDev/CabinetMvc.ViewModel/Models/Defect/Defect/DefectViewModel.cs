﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;


namespace CabinetMvc.ViewModel.Defect
{
    public class DefectViewModel : AuBaseViewModel
    {
        public DefectViewModel()
            : base(Enums.MainObjectType.DEFECT)
        {
            //
        }

        public DefectViewModel(defect item)
            : base(Enums.MainObjectType.DEFECT)
        {
            defect_id = item.defect_id;
            comm_name = item.comm_name;
            pack_name = item.pack_name;
            full_name = item.full_name;
            series = item.series;
            producer = item.producer;
            defect_status_id = item.defect_status_id;
            defect_type = item.defect_type;
            country_id = item.country_id;
            document_id = item.document_id;
            parent_defect_id = item.parent_defect_id;
            defect_status_text = item.defect_status_text;
            scope = item.scope;
            is_drug = item.is_drug;
            created_on = item.created_on;
            created_by = item.created_by;
            updated_on = item.updated_on;
            updated_by = item.updated_by;
            defect_source_id = item.defect_source_id;
        }

        public DefectViewModel(vw_defect item)
            : base(Enums.MainObjectType.DEFECT)
        {
            defect_id = item.defect_id;
            comm_name = item.comm_name;
            pack_name = item.pack_name;
            full_name = item.full_name;
            series = item.series;
            producer = item.producer;
            defect_status_id = item.defect_status_id;
            defect_type = item.defect_type;
            country_id = item.country_id;
            document_id = item.document_id;
            parent_defect_id = item.parent_defect_id;
            defect_status_text = item.defect_status_text;
            scope = item.scope;
            is_drug = item.is_drug;
            old_id = item.old;
            created_on = item.created_on;
            created_by = item.created_by;
            updated_on = item.updated_on;
            updated_by = item.updated_by;
            defect_source_id = item.defect_source_id;
            document_name = item.document_name;
            document_num = item.document_num;
            accept_date = item.accept_date;
            create_date = item.create_date;
            link = item.link;
            comment = item.comment;
            document_type_id = item.document_type_id;
            body_size_kb = item.body_size_kb;
            doc_created_on = item.doc_created_on;
            doc_created_by = item.doc_created_by;
            doc_updated_on = item.doc_updated_on;
            doc_updated_by = item.doc_updated_by;
            old = item.old;
            status_name = item.status_name;
            source_name = item.source_name;
            country_name = item.country_name;
        }

        [Key()]
        public long defect_id { get; set; }
        public string comm_name { get; set; }
        public string pack_name { get; set; }
        public string full_name { get; set; }
        public string series { get; set; }
        public string producer { get; set; }
        public Nullable<int> defect_status_id { get; set; }
        public string defect_type { get; set; }
        public Nullable<long> country_id { get; set; }
        public Nullable<long> document_id { get; set; }
        public Nullable<long> parent_defect_id { get; set; }
        public string defect_status_text { get; set; }
        public string scope { get; set; }
        public short is_drug { get; set; }
        public Nullable<int> old_id { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> updated_on { get; set; }
        public string updated_by { get; set; }
        public Nullable<int> defect_source_id { get; set; }
        public string document_name { get; set; }
        public string document_num { get; set; }
        public Nullable<System.DateTime> accept_date { get; set; }
        public Nullable<System.DateTime> create_date { get; set; }
        public string link { get; set; }
        public string comment { get; set; }
        public Nullable<int> document_type_id { get; set; }
        public Nullable<decimal> body_size_kb { get; set; }
        public Nullable<System.DateTime> doc_created_on { get; set; }
        public string doc_created_by { get; set; }
        public Nullable<System.DateTime> doc_updated_on { get; set; }
        public string doc_updated_by { get; set; }
        public Nullable<short> old { get; set; }
        public string status_name { get; set; }
        public string source_name { get; set; }
        public string country_name { get; set; }
    }
}