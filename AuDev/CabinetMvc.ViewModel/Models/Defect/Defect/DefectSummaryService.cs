﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Defect
{
    public interface IDefectSummaryService : IAuBaseService
    {
        //
    }

    public class DefectSummaryService : AuBaseService, IDefectSummaryService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            var max_date = dbContext.log_esn.Where(ss => ss.log_esn_type_id == (int)Enums.LogScope.DEFECT).OrderByDescending(ss => ss.log_id).Select(ss => ss.date_beg).FirstOrDefault();
            var cnt = dbContext.vw_defect_dbf.Count();
            var service_defect = (from t1 in dbContext.service_defect
                                  from t2 in dbContext.workplace
                                  from t3 in dbContext.sales
                                  where t1.workplace_id == t2.id
                                  && t2.sales_id == t3.id
                                  && t3.client_id == id
                                  && t1.service_id == (int)Enums.ClientServiceEnum.DEFECT
                                  && t2.is_deleted == 0
                                  && t3.is_deleted == 0
                                  select t1)
                                 .FirstOrDefault();
            return new DefectSummaryViewModel() { defect_upd_date = max_date, defect_cnt = cnt, is_allow_download = service_defect == null ? false : service_defect.is_allow_download };
        }
       
    }
}