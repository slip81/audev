﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;


namespace CabinetMvc.ViewModel.Defect
{
    public class DefectSummaryViewModel : AuBaseViewModel
    {
        public DefectSummaryViewModel()            
        {
            //
        }

        public DateTime? defect_upd_date { get; set; }
        public int defect_cnt { get; set; }
        public bool is_allow_download { get; set; }
    }
}