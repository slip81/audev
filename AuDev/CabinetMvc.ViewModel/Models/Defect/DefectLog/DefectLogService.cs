﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Defect
{
    public interface IDefectLogService : IAuBaseService
    {
        //
    }

    public class DefectLogService : AuBaseService, IDefectLogService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.log_esn.Where(ss => ss.scope == (int)Enums.LogScope.DEFECT)                
                .Select(ss => new DefectLogViewModel()
            {
                log_id = ss.log_id,
                date_beg = ss.date_beg,
                mess = ss.mess,
                user_name = ss.user_name,
                log_event_type_id = ss.log_esn_type_id,
                log_event_type_str = ss.log_esn_type.type_name,
            }
            );
        }
       
    }
}