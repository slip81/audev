﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;


namespace CabinetMvc.ViewModel.Defect
{
    public class DefectLogViewModel : AuBaseViewModel
    {
        public DefectLogViewModel()
            : base(Enums.MainObjectType.DEFECT_LOG)
        {
            //
        }

        public DefectLogViewModel(log_esn item)
            : base(Enums.MainObjectType.DEFECT_LOG)
        {
            log_id = item.log_id;
            date_beg = item.date_beg;
            mess = item.mess;
            user_name = item.user_name;
            log_event_type_id = item.log_esn_type_id;
        }

        [Key()]
        [Display(Name = "Код")]
        public long log_id { get; set; }

        [Display(Name = "Дата-время")]
        public System.DateTime date_beg { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }

        [Display(Name = "Пользователь")]
        public string user_name { get; set; }

        [Display(Name = "Тип операции")]
        public Nullable<long> log_event_type_id { get; set; }

        [Display(Name = "Тип операции")]
        public string log_event_type_str { get; set; }
    }
}