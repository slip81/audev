﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Defect
{
    public class DefectRequestViewModel : AuBaseViewModel
    {
        public DefectRequestViewModel()
            : base(Enums.MainObjectType.DEFECT)
        {
            //
        }

        public DefectRequestViewModel(request item)
            : base(Enums.MainObjectType.DEFECT_REQUEST)
        {
            request_id = item.request_id;
            login = item.login;
            workplace = item.workplace;
            version_num = item.version_num;
            request_date = item.request_date;
            state = item.state;
            record_cnt = item.record_cnt;
            request_type = item.request_type;
            commit_date = item.commit_date;
            scope = item.scope;
            scope2 = item.scope2;
        }

        [Key()]
        [Display(Name = "Код")]
        public long request_id { get; set; }

        [Display(Name = "Логин")]
        public string login { get; set; }

        [Display(Name = "Рабочее место")]
        public string workplace { get; set; }

        [Display(Name = "Номер версии")]
        public string version_num { get; set; }

        [Display(Name = "Дата запроса")]
        public Nullable<System.DateTime> request_date { get; set; }

        [Display(Name = "Статус запроса")]
        public int state { get; set; }

        [Display(Name = "Статус запроса")]
        public string state_str { get; set; }

        [ScaffoldColumn(false)]
        public Nullable<int> record_cnt { get; set; }

        [Display(Name = "Тип запроса")]        
        public int request_type { get; set; }

        [Display(Name = "Тип запроса")]
        public string request_type_str { get; set; }

        [Display(Name = "Дата подтверждения")]        
        public Nullable<System.DateTime> commit_date { get; set; }

        [Display(Name = "scope")]      
        public int scope { get; set; }

        [Display(Name = "scope2")]      
        public Nullable<int> scope2 { get; set; }
    }
}