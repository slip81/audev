﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Defect
{
    public interface IDefectRequestService : IAuBaseService
    {
        //
    }

    public class DefectRequestService : AuBaseService, IDefectRequestService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.request
                .Where(ss => ss.scope == (int)Enums.LogScope.DEFECT)
                .Select(ss => new DefectRequestViewModel()
            {
                request_id = ss.request_id,
                login = ss.login,
                workplace = ss.workplace,
                version_num = ss.version_num,
                request_date = ss.request_date,
                state = ss.state,
                state_str = ss.state == (int)Enums.RequestStateEnum.CONFIRMED ? "Подтверждено" : "Не подтверждено",
                record_cnt = ss.record_cnt,
                request_type = ss.request_type,
                request_type_str = ss.request_type == (int)Enums.RequestTypeEnum.RECORDS_ALL ? "Все записи" : "Новые записи",
                commit_date = ss.commit_date,
                scope = ss.scope,
                scope2 = ss.scope2,
            }
            );
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return from t1 in dbContext.vw_workplace
                      from t2 in dbContext.request
                      where t1.user_name != null && t1.user_name.Trim().ToLower().Equals(t2.login.Trim().ToLower())
                      && t1.registration_key != null && t1.registration_key.Trim().ToLower().Equals(t2.workplace.Trim().ToLower())
                      && t1.client_id == parent_id
                      select new DefectRequestViewModel()
                    {
                        request_id = t2.request_id,
                        login = t2.login,
                        workplace = t2.workplace,
                        version_num = t2.version_num,
                        request_date = t2.request_date,
                        state = t2.state,
                        state_str = t2.state == (int)Enums.RequestStateEnum.CONFIRMED ? "Подтверждено" : "Не подтверждено",
                        record_cnt = t2.record_cnt,
                        request_type = t2.request_type,
                        request_type_str = t2.request_type == (int)Enums.RequestTypeEnum.RECORDS_ALL ? "Все записи" : "Новые записи",
                        commit_date = t2.commit_date,
                        scope = t2.scope,
                        scope2 = t2.scope2,
                    };            
        }
       
    }
}