﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.ComponentModel.DataAnnotations;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Sys
{
    public interface ICabGridColumnService : IAuBaseService
    {
        IQueryable<CabGridColumnViewModel> GetList_Color(int grid_id);
        List<CabGridColumnViewModel> GetList_withSys(int grid_id);
    }

    public class CabGridColumnService : AuBaseService, ICabGridColumnService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.cab_grid_column
                .Select(ss => new CabGridColumnViewModel
                {
                    column_id = ss.column_id,
                    grid_id = ss.grid_id,
                    column_name = ss.column_name,
                    column_name_rus = ss.column_name_rus,
                    format = ss.format,
                    templ = ss.templ,
                    title = ss.title,
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.cab_grid_column
                .Where(ss => ss.grid_id == parent_id)
                .Select(ss => new CabGridColumnViewModel
                {
                    column_id = ss.column_id,
                    grid_id = ss.grid_id,
                    column_name = ss.column_name,
                    column_name_rus = ss.column_name_rus,
                    format = ss.format,
                    templ = ss.templ,
                    title = ss.title,
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.cab_grid_column
                .Where(ss => ss.column_id == id)
                .Select(ss => new CabGridColumnViewModel
                {
                    column_id = ss.column_id,
                    grid_id = ss.grid_id,
                    column_name = ss.column_name,
                    column_name_rus = ss.column_name_rus,
                    format = ss.format,
                    templ = ss.templ,
                    title = ss.title,
                })
                .FirstOrDefault();
        }

        public IQueryable<CabGridColumnViewModel> GetList_Color(int grid_id)
        {
            return dbContext.cab_grid_column
            .Where(ss => ss.grid_id == grid_id
                && (
                (ss.column_id == (int)Enums.CabGridColumn_TaskList.state_name) 
                || (ss.column_id == (int)Enums.CabGridColumn_TaskList.project_name) 
                || (ss.column_id == (int)Enums.CabGridColumn_TaskList.priority_name)
                || (ss.column_id == (int)Enums.CabGridColumn_TaskList.exec_user_name)
                || (ss.column_id == (int)Enums.CabGridColumn_TaskList.owner_user_name)
                || (ss.column_id == (int)Enums.CabGridColumn_TaskList.assigned_user_name))
                )
            .Select(ss => new CabGridColumnViewModel
            {
                column_id = ss.column_id,
                grid_id = ss.grid_id,
                column_name = ss.column_name,
                column_name_rus = ss.column_name_rus,
                format = ss.format,
                templ = ss.templ,
                title = ss.title,
            });
        }

        public List<CabGridColumnViewModel> GetList_withSys(int grid_id)
        {
            if (grid_id != (int)Enums.CabGrid.TaskList)
                return null;

            List<CabGridColumnViewModel> result = new List<CabGridColumnViewModel>();

            List<CabGridColumnViewModel> res1 = dbContext.cab_grid_column
                .Where(ss => ss.grid_id == grid_id)
                .Select(ss => new CabGridColumnViewModel
                {
                    column_id = ss.column_id,
                    grid_id = ss.grid_id,
                    column_name = ss.column_name,
                    column_name_rus = ss.column_name_rus,
                })
                .ToList();
            
            result.AddRange(res1);

            List<CabGridColumnViewModel> res2 = new List<CabGridColumnViewModel>();
            var sysValues = GlobalUtil.GetEnumValues<Enums.CabGrid>().ToList();
            foreach (var sysValue in sysValues)
            {
                var attrName = sysValue.GetAttribute<DisplayAttribute>().Name;
                var attrValue = int.Parse(sysValue.ToString());
                res2.Add(new CabGridColumnViewModel() 
                { 
                    column_id = attrValue, 
                    grid_id = grid_id,
                    column_name = attrName,
                    column_name_rus = attrName,
                });
            }

            result.AddRange(res2);

            return result.OrderBy(ss => ss.column_name_rus).ToList();
        }
        
    }

}
