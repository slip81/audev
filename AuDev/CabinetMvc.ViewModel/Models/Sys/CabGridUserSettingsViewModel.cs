﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Sys
{
    public class CabGridUserSettingsViewModel : AuBaseViewModel
    {
        public CabGridUserSettingsViewModel()
            : base(Enums.MainObjectType.CAB_GRID_USER_SETTINGS)
        {
            //
        }

        public CabGridUserSettingsViewModel(cab_grid_user_settings item)
            : base(Enums.MainObjectType.CAB_GRID_USER_SETTINGS)
        {
            item_id = item.item_id;
            grid_id = item.grid_id;
            user_id = item.user_id;
            sort_options = item.sort_options;
            filter_options = item.filter_options;
            back_color_column_id = item.back_color_column_id;
            page_size = item.page_size;
            columns_options = item.columns_options;
            grid_options = item.grid_options;
        }

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "grid_id")]
        public int grid_id { get; set; }

        [Display(Name = "user_id")]
        public int user_id { get; set; }

        [Display(Name = "sort_options")]
        public string sort_options { get; set; }

        [Display(Name = "sort_options")]
        public string filter_options { get; set; }

        [Display(Name = "back_color_column_id")]
        public Nullable<int> back_color_column_id { get; set; }

        [Display(Name = "back_color_column_id")]
        public Nullable<int> page_size { get; set; }

        [Display(Name = "columns_options")]
        public string columns_options { get; set; }

        [Display(Name = "grid_options")]
        public string grid_options { get; set; }
    }

    public class KendoGridColumn
    {
        public string field { get; set; }
        public string width { get; set; }
    }
}