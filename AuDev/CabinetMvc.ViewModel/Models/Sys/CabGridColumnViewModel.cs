﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Sys
{
    public class CabGridColumnViewModel : AuBaseViewModel
    {
        public CabGridColumnViewModel()
            : base(Enums.MainObjectType.CAB_GRID_COLUMN)
        {
            //
        }

        public CabGridColumnViewModel(cab_grid_column item)
            : base(Enums.MainObjectType.CAB_GRID_COLUMN)
        {
            column_id = item.column_id;
            grid_id = item.grid_id;
            column_name = item.column_name;
            column_name_rus = item.column_name_rus;
            format = item.format;
            templ = item.templ;
            title = item.title;
        }

        [Key()]
        [Display(Name="Код")]
        public int column_id { get; set; }

        [Display(Name = "Грид")]
        public int grid_id { get; set; }

        [Display(Name = "Колонка")]
        public string column_name { get; set; }

        [Display(Name = "Колонка (рус)")]
        public string column_name_rus { get; set; }

        [Display(Name = "Формат")]
        public string format { get; set; }

        [Display(Name = "Шаблон")]
        public string templ { get; set; }

        [Display(Name = "Хинт")]
        public string title { get; set; }
    }
}