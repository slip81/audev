﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Sys
{
    public class CabGridViewModel : AuBaseViewModel
    {
        public CabGridViewModel()
            : base(Enums.MainObjectType.CAB_GRID)
        {
            //
        }

        public CabGridViewModel(cab_grid item)
            : base(Enums.MainObjectType.CAB_GRID)
        {
            grid_id = item.grid_id;
            grid_name = item.grid_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public int grid_id { get; set; }

        [Display(Name = "Название")]
        public string grid_name { get; set; }
    }
}