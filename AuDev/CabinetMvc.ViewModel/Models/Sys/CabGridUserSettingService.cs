﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Diagnostics;
using System.Data.Entity.Validation;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using CabinetMvc.ViewModel.Crm;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Sys
{
    public interface ICabGridUserSettingService : IAuBaseService
    {
        //CabGridUserSettingsViewModel UpdateSettings(int grid_id, string sort_options, string filter_options, int? page_size, List<KendoGridColumn> columns_options);
        CabGridUserSettingsViewModel UpdateSettings(int grid_id, string sort_options, string filter_options, int? page_size, string columns_options);
        string GetGridOptions(int grid_id);
        bool SaveGridOptions(int grid_id, string grid_options);
    }

    public class CabGridUserSettingService : AuBaseService, ICabGridUserSettingService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {            
            return dbContext.cab_grid_user_settings
                .Where(ss => ss.grid_id == id && ss.user_id == currUser.CabUserId)
                .Select(ss => new CabGridUserSettingsViewModel
                {
                    item_id = ss.item_id,
                    grid_id = ss.grid_id,
                    user_id = ss.user_id,
                    sort_options = ss.sort_options,
                    filter_options = ss.filter_options,
                    back_color_column_id = ss.back_color_column_id,
                    page_size = ss.page_size,
                    columns_options = ss.columns_options,
                    grid_options = ss.grid_options,
                })
                .FirstOrDefault();
        }

        //public CabGridUserSettingsViewModel UpdateSettings(int grid_id, string sort_options, string filter_options, int? page_size, List<KendoGridColumn> columns_options)
        public CabGridUserSettingsViewModel UpdateSettings(int grid_id, string sort_options, string filter_options, int? page_size, string columns_options)
        {
            //string columns_options_str = columns_options.ToString();

            var cab_grid_user_settings = dbContext.cab_grid_user_settings.Where(ss => ss.grid_id == grid_id && ss.user_id == currUser.CabUserId).FirstOrDefault();
            if (cab_grid_user_settings == null)
            {
                cab_grid_user_settings = new cab_grid_user_settings();
                cab_grid_user_settings.grid_id = grid_id;
                cab_grid_user_settings.user_id = currUser.CabUserId;
                cab_grid_user_settings.sort_options = sort_options;
                cab_grid_user_settings.filter_options = filter_options;
                cab_grid_user_settings.page_size = page_size;
                //cab_grid_user_settings.columns_options = columns_options_str;
                cab_grid_user_settings.columns_options = columns_options;
                dbContext.cab_grid_user_settings.Add(cab_grid_user_settings);
            }
            else
            {
                cab_grid_user_settings.sort_options = sort_options;
                cab_grid_user_settings.filter_options = filter_options;
                cab_grid_user_settings.page_size = page_size;
                //cab_grid_user_settings.columns_options = columns_options_str;
                cab_grid_user_settings.columns_options = columns_options;
            }

            var err = "";
            try
            {
                dbContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        err += string.Format("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }

                err += "; end";
            }

            return new CabGridUserSettingsViewModel(cab_grid_user_settings);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CabGridUserSettingsViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CabGridUserSettingsViewModel)item).item_id;

            var cab_grid_user_settings = dbContext.cab_grid_user_settings.Where(ss => ss.item_id == id).FirstOrDefault();
            if (cab_grid_user_settings == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            dbContext.cab_grid_user_settings.Remove(cab_grid_user_settings);
            dbContext.SaveChanges();

            return true;
        }

        public string GetGridOptions(int grid_id)
        {
            return dbContext.cab_grid_user_settings
                .Where(ss => ss.grid_id == grid_id && ss.user_id == currUser.CabUserId)
                .Select(ss => ss.grid_options)
                .FirstOrDefault();
        }
        
        public bool SaveGridOptions
            (int grid_id, string grid_options)
        {
            cab_grid_user_settings cab_grid_user_settings = dbContext.cab_grid_user_settings
                .Where(ss => ss.grid_id == grid_id && ss.user_id == currUser.CabUserId)                
                .FirstOrDefault();
            if (cab_grid_user_settings == null)
            {
                cab_grid_user_settings = new cab_grid_user_settings();
                cab_grid_user_settings.grid_id = grid_id;
                cab_grid_user_settings.user_id = currUser.CabUserId;
                cab_grid_user_settings.grid_options = grid_options;
                dbContext.cab_grid_user_settings.Add(cab_grid_user_settings);
            }
            else
            {
                cab_grid_user_settings.grid_options = grid_options;
            }

            dbContext.SaveChanges();
            return true;
        }
    }

}
