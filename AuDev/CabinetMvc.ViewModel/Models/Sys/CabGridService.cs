﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Sys
{
    public interface ICabGridService : IAuBaseService
    {
        //
    }

    public class CabGridService : AuBaseService, ICabGridService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.cab_grid
                .Select(ss => new CabGridViewModel
                {
                    grid_id = ss.grid_id,
                    grid_name = ss.grid_name,
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.cab_grid
                .Where(ss => ss.grid_id == id)
                .Select(ss => new CabGridViewModel
                {
                    grid_id = ss.grid_id,
                    grid_name = ss.grid_name,
                })
                .FirstOrDefault();
        }
        
    }

}
