﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Sys
{
    public class CabGridColumnUserSettingsViewModel : AuBaseViewModel
    {
        public CabGridColumnUserSettingsViewModel()
            : base(Enums.MainObjectType.CAB_GRID_COLUMN_USER_SETTINGS)
        {
            //
        }

        public CabGridColumnUserSettingsViewModel(cab_grid_column_user_settings item)
            : base(Enums.MainObjectType.CAB_GRID_COLUMN_USER_SETTINGS)
        {
            item_id = item.item_id;
            column_id = item.column_id;
            user_id = item.user_id;
            column_num = item.column_num;
            is_visible = item.is_visible;
            width = item.width;
            format = "";
            templ = "";
            is_filterable = item.is_filterable;
            is_sortable = item.is_sortable;
            column_index = 0;
        }

        public CabGridColumnUserSettingsViewModel(vw_cab_grid_column_user_settings item)
            : base(Enums.MainObjectType.CAB_GRID_COLUMN_USER_SETTINGS)
        {
            item_id = item.item_id;
            column_id = item.column_id;
            user_id = item.user_id; 
            column_num = item.column_num;
            is_visible = item.is_visible;
            width = item.width;
            format = item.format;
            templ = item.templ;
            is_filterable = item.is_filterable;
            is_sortable = item.is_sortable;
            column_index = item.column_index;
        }

        [Key()]
        [Display(Name="Код")]
        public int item_id { get; set; }

        [Display(Name = "Колонка")]
        public int column_id { get; set; }

        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Display(Name = "Номер колонки")]
        public Nullable<int> column_num { get; set; }

        [Display(Name = "Видимость")]
        public bool is_visible { get; set; }

        [Display(Name = "Ширина")]
        public Nullable<int> width { get; set; }

        [Display(Name = "Формат")]
        public string format { get; set; }

        [Display(Name = "Шаблон")]
        public string templ { get; set; }

        [Display(Name = "is_filterable")]
        public bool is_filterable { get; set; }

        [Display(Name = "is_sortable")]
        public bool is_sortable { get; set; }

        [Display(Name = "Грид")]
        public int grid_id { get; set; }

        [Display(Name = "Грид")]
        public string grid_name { get; set; }

        [Display(Name = "Колонка")]
        public string column_name { get; set; }

        [Display(Name = "Колонка (рус)")]
        public string column_name_rus { get; set; }

        [Display(Name = "column_index")]
        public int column_index { get; set; }

        [Display(Name = "title")]
        public string title { get; set; }
    }
}