﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Sys
{
    public interface ICabGridColumnUserSettingsService : IAuBaseService
    {
        bool UpdateSettings(IEnumerable<CabGridColumnUserSettingsViewModel> settings, int? color_column_id, ModelStateDictionary modelState);
    }

    public class CabGridColumnUserSettingsService : AuBaseService, ICabGridColumnUserSettingsService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            var currUserId = currUser.CabUserId;
            var res = (from t1 in dbContext.vw_cab_grid_column_user_settings
                       from t2 in dbContext.cab_grid_column
                       where t1.column_id == t2.column_id
                       && t2.grid_id == parent_id
                       && t1.user_id == currUserId
                       select new CabGridColumnUserSettingsViewModel
                         {
                             item_id = t1.item_id,
                             column_id = t2.column_id,
                             user_id = t1.user_id,
                             column_num = t1.column_num,
                             is_visible = t1.is_visible,
                             width = t1.width,
                             format = t2.format,
                             templ = t2.templ,
                             is_filterable = t1.is_filterable,
                             is_sortable = t1.is_sortable,
                             /*
                             grid_id = t1.cab_grid_column.grid_id,
                             grid_name = t1.cab_grid_column.cab_grid.grid_name,
                             column_name = t1.cab_grid_column.column_name,
                             column_name_rus = t1.cab_grid_column.column_name_rus,
                             */
                             grid_id = t2.grid_id,
                             grid_name = t2.cab_grid.grid_name,
                             column_name = t2.column_name,
                             column_name_rus = t2.column_name_rus,
                             column_index = t1.column_index,
                             title = t2.title,
                         })
                        .OrderBy(ss => ss.column_num).ThenBy(ss => ss.column_id);

            if ((res == null) || (res.Count() <= 0))
            {
                int i = 1;
                var grid_columns = dbContext.cab_grid_column.Where(ss => ss.grid_id == parent_id).OrderBy(ss => ss.column_id).ToList();
                if ((grid_columns != null) && (grid_columns.Count > 0))
                {
                    foreach (var col in grid_columns)
                    {
                        cab_grid_column_user_settings sett = new cab_grid_column_user_settings();
                        sett.column_id = col.column_id;
                        sett.column_num = i;                        
                        sett.is_filterable = true;
                        sett.is_sortable = true;
                        sett.is_visible = true;
                        sett.user_id = currUserId;
                        sett.width = 200;
                        dbContext.cab_grid_column_user_settings.Add(sett);
                        i++;
                    }
                    dbContext.SaveChanges();

                    res = (from t1 in dbContext.vw_cab_grid_column_user_settings
                           from t2 in dbContext.cab_grid_column
                           where t1.column_id == t2.column_id
                           && t2.grid_id == parent_id
                           && t1.user_id == currUserId
                           select new CabGridColumnUserSettingsViewModel
                           {
                               item_id = t1.item_id,
                               column_id = t2.column_id,
                               user_id = t1.user_id,
                               column_num = t1.column_num,
                               is_visible = t1.is_visible,
                               width = t1.width,
                               format = t2.format,
                               templ = t2.templ,
                               is_filterable = t1.is_filterable,
                               is_sortable = t1.is_sortable,
                               /*
                               grid_id = t1.cab_grid_column.grid_id,
                               grid_name = t1.cab_grid_column.cab_grid.grid_name,
                               column_name = t1.cab_grid_column.column_name,
                               column_name_rus = t1.cab_grid_column.column_name_rus,
                               */
                               grid_id = t2.grid_id,
                               grid_name = t2.cab_grid.grid_name,
                               column_name = t2.column_name,
                               column_name_rus = t2.column_name_rus,
                               column_index = t1.column_index,
                               title = t2.title,
                           })
                        .OrderBy(ss => ss.column_num).ThenBy(ss => ss.column_id);
                }
            }

            return res;
        }

        public bool UpdateSettings(IEnumerable<CabGridColumnUserSettingsViewModel> settings, int? color_column_id, ModelStateDictionary modelState)
        {
            if ((settings == null) || (settings.Count() <= 0))
            {
                modelState.AddModelError("", "Список колонок пуст");
                return false;
            }
            var i = 1;
            bool settingExists = true;
            var first = settings.FirstOrDefault();
            var existing = dbContext.cab_grid_column_user_settings.Where(ss => ss.cab_grid_column.grid_id == first.grid_id && ss.user_id == first.user_id).ToList();
            foreach (var new_item in settings)
            {
                settingExists = true;
                var existing_item = existing.Where(ss => ss.column_id == new_item.column_id).FirstOrDefault();
                if (existing_item == null)
                {
                    settingExists = false;
                    existing_item = new cab_grid_column_user_settings();
                    existing_item.column_id = new_item.column_id;
                    existing_item.user_id = first.user_id;
                }

                //item.column_num = new_item.column_num;
                existing_item.column_num = i;                
                existing_item.is_filterable = new_item.is_filterable;
                existing_item.is_sortable = new_item.is_sortable;
                existing_item.width = new_item.width;
                existing_item.is_visible = new_item.is_visible;
                i++;
                if (!settingExists)
                {
                    dbContext.cab_grid_column_user_settings.Add(existing_item);
                }
            }

            var existing_grid = dbContext.cab_grid_user_settings.Where(ss => ss.grid_id == first.grid_id && ss.user_id == first.user_id).FirstOrDefault();
            if (existing_grid != null)
            {
                existing_grid.back_color_column_id = color_column_id;
            }

            dbContext.SaveChanges();

            return true;
        }
        
    }

}
