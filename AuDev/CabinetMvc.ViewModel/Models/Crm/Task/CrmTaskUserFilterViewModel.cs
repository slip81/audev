﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmTaskUserFilterViewModel : AuBaseViewModel
    {
        public CrmTaskUserFilterViewModel()
            : base(Enums.MainObjectType.CRM_TASK_USER_FILTER)
        {
            //
        }

        public CrmTaskUserFilterViewModel(crm_task_user_filter item)
            : base(Enums.MainObjectType.CRM_TASK_USER_FILTER)
        {
            item_id = item.item_id;
            user_id = item.user_id;
            grid_id = item.grid_id;
            raw_filter = item.raw_filter;
            project_filter = item.project_filter;
            client_filter = item.client_filter;
            priority_filter = item.priority_filter;
            state_filter = item.state_filter;
            exec_user_filter = item.exec_user_filter;
            owner_user_filter = item.owner_user_filter;
            assigned_user_filter = item.assigned_user_filter;
            crt_date1_filter = item.crt_date1_filter;
            crt_date2_filter = item.crt_date2_filter;
            required_date1_filter = item.required_date1_filter;
            required_date2_filter = item.required_date2_filter;
            repair_date1_filter = item.repair_date1_filter;
            repair_date2_filter = item.repair_date2_filter;
            curr_group_id = item.curr_group_id;
            fin_cost_filter = (double?)item.fin_cost_filter;
            is_moderated_filter = item.is_moderated_filter;
            batch_filter = item.batch_filter;
        }

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "user_id")]
        public int user_id { get; set; }

        [Display(Name = "grid_id")]
        public int grid_id { get; set; }

        [Display(Name = "raw_filter")]
        public string raw_filter { get; set; }

        [Display(Name = "project_filter")]
        public string project_filter { get; set; }

        [Display(Name = "client_filter")]
        public string client_filter { get; set; }

        [Display(Name = "priority_filter")]
        public string priority_filter { get; set; }

        [Display(Name = "state_filter")]
        public string state_filter { get; set; }

        [Display(Name = "exec_user_filter")]
        public string exec_user_filter { get; set; }

        [Display(Name = "owner_user_filter")]
        public string owner_user_filter { get; set; }

        [Display(Name = "assigned_user_filter")]
        public string assigned_user_filter { get; set; }

        [Display(Name = "crt_date1_filter")]
        public Nullable<System.DateTime> crt_date1_filter { get; set; }

        [Display(Name = "crt_date2_filter")]
        public Nullable<System.DateTime> crt_date2_filter { get; set; }

        [Display(Name = "required_date1_filter")]
        public Nullable<System.DateTime> required_date1_filter { get; set; }

        [Display(Name = "required_date2_filter")]
        public Nullable<System.DateTime> required_date2_filter { get; set; }

        [Display(Name = "repair_date1_filter")]
        public Nullable<System.DateTime> repair_date1_filter { get; set; }

        [Display(Name = "repair_date2_filter")]
        public Nullable<System.DateTime> repair_date2_filter { get; set; }

        [Display(Name = "is_moderated_filter")]
        public bool is_moderated_filter { get; set; }

        [Display(Name = "curr_group_id")]
        public Nullable<int> curr_group_id { get; set; }

        [Display(Name = "fin_cost_filter")]
        public Nullable<double> fin_cost_filter { get; set; }

        [Display(Name = "batch_filter")]
        public string batch_filter { get; set; }
        
        public List<int> projectFilterList { get; set; }

        public List<int> clientFilterList { get; set; }

        public List<int> priorityFilterList { get; set; }

        public List<int> stateFilterList { get; set; }

        public List<int> execUserFilterList { get; set; }

        public List<int> ownerUserFilterList { get; set; }

        public List<int> assignedUserFilterList { get; set; }

        public List<int> batchFilterList { get; set; }
    }
}

