﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmTaskNoteService : IAuBaseService
    {
        //
    }

    public class CrmTaskNoteService : AuBaseService, ICrmTaskNoteService
    {
        ICrmLogService logService;        

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_crm_task_note
                .Where(ss => ss.task_id == parent_id)
                .Select(ss => new CrmTaskNoteViewModel
                {
                    note_id = ss.note_id,
                    task_id = ss.task_id,
                    subtask_id = ss.subtask_id,
                    crt_date = ss.crt_date,
                    owner_user_id = ss.owner_user_id,
                    note_text = ss.note_text,
                    file_name = ss.file_name,
                    is_file_exists = ss.is_file_exists,
                    user_name = ss.user_name,   
                    note_num = ss.note_num,
                });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmTaskNoteViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmTaskNoteViewModel itemAdd = (CrmTaskNoteViewModel)item;
            if ((itemAdd == null) || (String.IsNullOrEmpty(itemAdd.note_text)))
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_task crm_task = dbContext.crm_task.Where(ss => ss.task_id == itemAdd.task_id).FirstOrDefault();
            if (crm_task == null)
            {
                modelState.AddModelError("", "Не найдена задача с кодом " + itemAdd.task_id.ToString());
                return null;
            }

            vw_crm_task crm_task_db = dbContext.vw_crm_task.Where(ss => ss.task_id == itemAdd.task_id && ss.view_user_id == currUser.CabUserId).FirstOrDefault();

            itemAdd.note_text = GlobalUtil.Linkify(itemAdd.note_text);
            DateTime now = DateTime.Now;            
            crm_task_note crm_task_note = new crm_task_note();
            crm_task_note.crt_date = now;
            crm_task_note.file_name = null;
            crm_task_note.note_text = itemAdd.note_text;
            crm_task_note.owner_user_id = currUser.CabUserId;
            crm_task_note.task_id = itemAdd.task_id;

            dbContext.crm_task_note.Add(crm_task_note);

            crm_task.upd_date = DateTime.Now;
            crm_task.upd_user = currUser.CabUserName;
            crm_task.upd_num = crm_task.upd_num + 1;

            dbContext.SaveChanges();

            var crmTaskService = DependencyResolver.Current.GetService<ICrmTaskService>();
            List<int> watchedColumns = crmTaskService.GetNotifyIcqWatchedColumns();

            // notify
            string mess_notify = "";
            notify notify = null;

            if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.notes_text))
            {
                mess_notify = "Изменена задача:";
                mess_notify += System.Environment.NewLine;
                mess_notify += crmTaskService.GetNotifyIcqContent(crm_task_db);
                mess_notify += "Изменения:";
                mess_notify += System.Environment.NewLine;
                mess_notify += "Добавление комментария";
                mess_notify += System.Environment.NewLine;
                mess_notify += "Автор изменений: " + currUser.CabUserName;
                mess_notify += System.Environment.NewLine;
                mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
                mess_notify += System.Environment.NewLine;

                List<int> notifyTargetList = dbContext.vw_crm_notify_param_user.Where(ss =>
                    ((ss.changer_user_id == crm_task.assigned_user_id) && (ss.is_owner == true) && (ss.user_id != currUser.CabUserId))
                    ||
                    ((ss.changer_user_id == crm_task.exec_user_id) && (ss.is_executer == true) && (ss.user_id != currUser.CabUserId))
                    )
                    .Select(ss => ss.user_id)
                    .Distinct()
                    .ToList();
                if ((notifyTargetList != null) && (notifyTargetList.Count > 0))
                {
                    foreach (var notifyTarget in notifyTargetList)
                    {
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                        dbContext.notify.Add(notify);
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.EMAIL;
                        dbContext.notify.Add(notify);
                    }
                }
            }

            /*
            // уведомление ответственному
            if ((crm_task.assigned_user_id > 0) && (crm_task.assigned_user_id != currUser.CabUserId))
            {
                notify = new notify();
                notify.crt_date = now;
                notify.message = mess_notify;
                notify.message_sent = false;
                notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                notify.sent_date = null;
                notify.target_user_id = crm_task.assigned_user_id;
                notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                dbContext.notify.Add(notify);
            }
            // уведомление исполнителю
            if ((crm_task.exec_user_id > 0) && (crm_task.exec_user_id != crm_task.assigned_user_id) && (crm_task.exec_user_id != currUser.CabUserId))
            {
                notify = new notify();
                notify.crt_date = now;
                notify.message = mess_notify;
                notify.message_sent = false;
                notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                notify.sent_date = null;
                notify.target_user_id = crm_task.exec_user_id;
                notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                dbContext.notify.Add(notify);
            }
            */

            // log
            if (logService == null)
                logService = DependencyResolver.Current.GetService<ICrmLogService>();
            if (logService == null)
                throw new Exception("logService == null");
            logService.InsertLogCrm(now, "Добавление комментария", crm_task_note.task_id);

            dbContext.SaveChanges();

            vw_crm_task_note vw_crm_task_note = dbContext.vw_crm_task_note.Where(ss => ss.note_id == crm_task_note.note_id).FirstOrDefault();

            return new CrmTaskNoteViewModel(vw_crm_task_note);
        }
    }
}
