﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmUserTaskFollowViewModel : AuBaseViewModel
    {
        public CrmUserTaskFollowViewModel()
            : base(Enums.MainObjectType.CRM_USER_TASK_FOLLOW)
        {
            //
        }

        public CrmUserTaskFollowViewModel(crm_user_task_follow item)
            : base(Enums.MainObjectType.CRM_USER_TASK_FOLLOW)
        {
            item_id = item.item_id;
            user_id = item.user_id;
            task_id = item.task_id;
        }

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Display(Name = "Задача")]
        public int task_id { get; set; }
    }
}