﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Db.Model;
using AuDev.Common.Network;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmTaskFilterService : IAuBaseService
    {
        List<SimpleCombo> GetList_Distinct();
        List<CrmTaskFilterViewModel> GetTreeList(int filter_group_id);
    }

    public class CrmTaskFilterService : AuBaseService, ICrmTaskFilterService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_crm_task_filter.ProjectTo<CrmTaskFilterViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_crm_task_filter
                .Where(ss => ss.filter_group_id == parent_id)
                .ProjectTo<CrmTaskFilterViewModel>();
        }

        public List<SimpleCombo> GetList_Distinct()
        {
            return dbContext.vw_crm_task_filter
                .Select(ss => new SimpleCombo()
                {
                    obj_id = ss.filter_group_id,
                    obj_name = ss.filter_group_name,
                }
                )
                .Distinct()
                .ToList();
        }

        public List<CrmTaskFilterViewModel> GetTreeList(int filter_group_id)
        {
            List<CrmTaskFilterViewModel> result = new List<CrmTaskFilterViewModel>();
            result.Add(new CrmTaskFilterViewModel() 
            { 
                adr = 0,
                cnt = 0,
                filter_group_id = filter_group_id,
                filter_group_name = filter_group_id.ToString(),
                HasChildren = true,
                ChildrenList = dbContext.vw_crm_task_filter.Where(ss => ss.filter_group_id == filter_group_id).ProjectTo<CrmTaskFilterViewModel>().ToList(),
            });
            return result;
        }
    }
}