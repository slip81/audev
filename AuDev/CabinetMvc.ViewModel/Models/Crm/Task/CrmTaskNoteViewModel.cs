﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmTaskNoteViewModel : AuBaseViewModel
    {
        public CrmTaskNoteViewModel()
            : base(Enums.MainObjectType.CRM_TASK_NOTE)
        {
            //
        }

        public CrmTaskNoteViewModel(vw_crm_task_note item)
            : base(Enums.MainObjectType.CRM_TASK_NOTE)
        {
            note_id = item.note_id;
            task_id = item.task_id;
            subtask_id = item.subtask_id;
            crt_date = item.crt_date;
            owner_user_id = item.owner_user_id;
            note_text = item.note_text;
            file_name = item.file_name;
            is_file_exists = item.is_file_exists;
            user_name = item.user_name;
            note_num = item.note_num;
        }

        [Key()]
        [Display(Name = "Код")]
        public int note_id { get; set; }

        [Display(Name = "Задача")]
        public int task_id { get; set; }

        [Display(Name = "Подзадача")]
        public Nullable<int> subtask_id { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор")]
        public int owner_user_id { get; set; }

        [Display(Name = "Текст")]
        [AllowHtml()]
        [UIHint("TextArea")]
        public string note_text { get; set; }

        [Display(Name = "Файл")]
        public string file_name { get; set; }

        [Display(Name = "Файл есть")]
        public bool is_file_exists { get; set; }

        [Display(Name = "Автор")]
        public string user_name { get; set; }

        [Display(Name = "Номер")]
        public int note_num { get; set; }
    }
}