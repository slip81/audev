﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Storage;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;
#endregion

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmTaskService : IAuBaseService
    {
        IQueryable<CrmTaskViewModel> GetList_byGroup(int group_id, string phrase);
        IQueryable<CrmTaskViewModel> GetList_byUser(int user_id, int role_id, string phrase);
        IQueryable<CrmTaskViewModel> GetList_Arc();
        IQueryable<CrmTaskViewModel> GetShortList_ForExecUser(string num, int exec_user_id);
        IQueryable<SimpleCombo> GetNumList(string num);
        IQueryable<CrmTaskViewModel> GetList_forPlannerExport(DateTime date_beg, DateTime date_end, int? exec_user_id);
        bool TaskOperationExecute(string task_id_list, int operation_id, int? batch_id, ModelStateDictionary modelState);
        bool TaskRelAdd(int parent_task_id, string child_task_num, int rel_type_id, ModelStateDictionary modelState);
        bool TaskRelDel(int parent_task_id, int child_task_id, int rel_type_id, ModelStateDictionary modelState);
        string GetTaskText(int task_id);        
        Tuple<bool, string, DateTime?> TaskSaveBefore(int task_id, int task_upd_num);
        AuBaseViewModel Insert_Story(long story_id, ModelStateDictionary modelState);
        string GetNotifyIcqContent(vw_crm_task crm_task);
        List<int> GetNotifyIcqWatchedColumns();        
    }

    public class CrmTaskService : AuBaseService, ICrmTaskService
    {
        ICrmLogService logService;

        #region Get

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_crm_task.Where(ss => ss.view_user_id == currUser.CabUserId)
                .Select(ss => new CrmTaskViewModel
                {
                    task_id = ss.task_id,
                    task_name = ss.task_name,
                    task_text = ss.task_text,
                    project_id = ss.project_id,
                    module_id = ss.module_id,
                    module_part_id = ss.module_part_id,
                    module_version_id = ss.module_version_id,
                    client_id = ss.client_id,
                    priority_id = ss.priority_id,
                    state_id = ss.state_id,
                    owner_user_id = ss.owner_user_id,
                    exec_user_id = ss.exec_user_id,
                    task_num = ss.task_num,
                    crt_date = ss.crt_date,
                    required_date = ss.required_date,
                    repair_date = ss.repair_date,
                    repair_version_id = ss.repair_version_id,
                    group_id = ss.group_id,                    
                    prepared_user_id = ss.prepared_user_id,
                    fixed_user_id = ss.fixed_user_id,
                    approved_user_id = ss.approved_user_id,
                    assigned_user_id = ss.assigned_user_id,
                    is_archive = false,
                    rel_child_cnt = ss.rel_child_cnt,
                    rel_parent_cnt = ss.rel_parent_cnt,
                    is_fin = ss.is_fin,
                    is_fin_name = ss.is_fin_name,
                    fin_cost = ss.fin_cost,
                    is_event = ss.is_event,
                    is_event_name = ss.is_event_name,
                    event_user_id = ss.event_user_id,
                    upd_date = ss.upd_date,
                    upd_num = ss.upd_num,
                    upd_user = ss.upd_user,
                    is_event_for_exec = (ss.event_user_id == ss.exec_user_id) && (ss.is_event),
                    view_user_id = ss.view_user_id,
                    state_fore_color = ss.state_fore_color,
                    project_fore_color = ss.project_fore_color,
                    priority_fore_color = ss.priority_fore_color,
                    moderator_user_id = ss.moderator_user_id,
                    moderator_user_name = ss.moderator_user_name,
                    storage_folder_id = ss.storage_folder_id,
                    storage_folder_name = ss.storage_folder_name,
                    is_in_storage = ss.is_in_storage,
                    is_in_storage_name = ss.is_in_storage_name,
                    IsModerated = ss.moderator_user_id > 0,

                    project_name = ss.project_name,
                    module_name = ss.module_name,
                    module_part_name = ss.module_part_name,
                    module_version_name = ss.module_version_name,
                    client_name = ss.client_name,
                    priority_name = ss.priority_name,
                    state_name = ss.state_name,
                    owner_user_name = ss.owner_user_name,
                    exec_user_name = ss.exec_user_name,
                    repair_version_name = ss.repair_version_name,
                    task_num_int = ss.task_num_int,
                    prepared_user_name = ss.prepared_user_name,
                    fixed_user_name = ss.fixed_user_name,
                    approved_user_name = ss.approved_user_name,
                    assigned_user_name = ss.assigned_user_name,
                    group_name = ss.group_name,
                    task_text_substring = ss.task_text_substring,
                    subtasks_text = ss.subtasks_text,
                    subtasks_task_id = ss.subtasks_task_id,
                    subtasks_count = ss.subtasks_count,
                    notes_text = ss.notes_text,
                    notes_task_id = ss.notes_task_id,
                    notes_count = ss.notes_count,
                    batch_id = ss.batch_id,
                    batch_name = ss.batch_name,
                    priority_is_boss = ss.priority_is_boss,
                    state_for_event = ss.state_for_event,
                    event_date_beg = ss.event_date_beg,
                    event_date_end = ss.event_date_end,
                    is_control = ss.is_control,
                    required_date_orig = ss.required_date_orig,
                    repair_date_orig = ss.repair_date_orig,
                    event_date_beg_orig = ss.event_date_beg_orig,
                    event_date_end_orig = ss.event_date_end_orig,
                    control_crt_date_orig = ss.control_crt_date_orig,
                    control_crt_user_orig = ss.control_crt_user_orig,
                    required_date_curr = ss.required_date_curr,
                    repair_date_curr = ss.repair_date_curr,
                    event_date_beg_curr = ss.event_date_beg_curr,
                    event_date_end_curr = ss.event_date_end_curr,
                    control_crt_date_curr = ss.control_crt_date_curr,
                    control_crt_user_curr = ss.control_crt_user_curr,
                    cost_plan = ss.cost_plan,
                    cost_fact = ss.cost_fact,
                });
        }

        public IQueryable<CrmTaskViewModel> GetList_byUser(int user_id, int role_id, string phrase)
        {
            var roleEnum = (Enums.CrmUserTaskRole)role_id;
            //var crm_user_id = currUser.CabCrmUserId;
            var res = (from t1 in dbContext.vw_crm_task
                       from t2 in dbContext.crm_user_task_follow.Where(ss => ss.task_id == t1.task_id && ss.user_id == user_id).DefaultIfEmpty()
                       from t3 in dbContext.crm_project_user.Where(ss => ss.project_id == t1.project_id && ss.user_id == user_id).DefaultIfEmpty()
                       where (t1.group_id == (int)Enums.CrmGroup.APPROVED && t1.view_user_id == currUser.CabUserId)
                       select new { vw_crm_task = t1, is_followed = ((t2 == null) ? false : true), is_in_project = ((t3 == null) ? false : true), })
                      .Where(ss => (
                      ((roleEnum == Enums.CrmUserTaskRole.ALL) && ((ss.vw_crm_task.exec_user_id == user_id) || (ss.vw_crm_task.assigned_user_id == user_id) || (ss.is_followed) || (ss.is_in_project)))
                      ||
                      ((roleEnum == Enums.CrmUserTaskRole.MY_EXEC) && (ss.vw_crm_task.exec_user_id == user_id))
                      ||
                      ((roleEnum == Enums.CrmUserTaskRole.MY_ASSIGNED) && (ss.vw_crm_task.assigned_user_id == user_id))
                      ||
                      ((roleEnum == Enums.CrmUserTaskRole.MY_FOLLOW) && (ss.is_followed))
                      ||
                      ((roleEnum == Enums.CrmUserTaskRole.MY_PROJECT) && (ss.is_in_project) && (ss.vw_crm_task.exec_user_id != user_id) && (ss.vw_crm_task.assigned_user_id != user_id))
                      )
                      )
                      .Select(ss => new CrmTaskViewModel
                                      {
                                          task_id = ss.vw_crm_task.task_id,
                                          task_name = ss.vw_crm_task.task_name,
                                          task_text = ss.vw_crm_task.task_text,
                                          project_id = ss.vw_crm_task.project_id,
                                          module_id = ss.vw_crm_task.module_id,
                                          module_part_id = ss.vw_crm_task.module_part_id,
                                          module_version_id = ss.vw_crm_task.module_version_id,
                                          client_id = ss.vw_crm_task.client_id,
                                          priority_id = ss.vw_crm_task.priority_id,
                                          state_id = ss.vw_crm_task.state_id,
                                          owner_user_id = ss.vw_crm_task.owner_user_id,
                                          exec_user_id = ss.vw_crm_task.exec_user_id,
                                          task_num = ss.vw_crm_task.task_num,
                                          crt_date = ss.vw_crm_task.crt_date,
                                          required_date = ss.vw_crm_task.required_date,
                                          repair_date = ss.vw_crm_task.repair_date,
                                          repair_version_id = ss.vw_crm_task.repair_version_id,
                                          group_id = ss.vw_crm_task.group_id,                                          
                                          prepared_user_id = ss.vw_crm_task.prepared_user_id,
                                          fixed_user_id = ss.vw_crm_task.fixed_user_id,
                                          approved_user_id = ss.vw_crm_task.approved_user_id,
                                          assigned_user_id = ss.vw_crm_task.assigned_user_id,
                                          is_archive = false,
                                          rel_child_cnt = ss.vw_crm_task.rel_child_cnt,
                                          rel_parent_cnt = ss.vw_crm_task.rel_parent_cnt,
                                          is_fin = ss.vw_crm_task.is_fin,
                                          is_fin_name = ss.vw_crm_task.is_fin_name,
                                          fin_cost = ss.vw_crm_task.fin_cost,
                                          is_event = ss.vw_crm_task.is_event,
                                          is_event_name = ss.vw_crm_task.is_event_name,
                                          event_user_id = ss.vw_crm_task.event_user_id,
                                          upd_date = ss.vw_crm_task.upd_date,
                                          upd_num = ss.vw_crm_task.upd_num,
                                          upd_user = ss.vw_crm_task.upd_user,
                                          is_event_for_exec = (ss.vw_crm_task.event_user_id == ss.vw_crm_task.exec_user_id) && (ss.vw_crm_task.is_event),
                                          view_user_id = ss.vw_crm_task.view_user_id,
                                          state_fore_color = ss.vw_crm_task.state_fore_color,
                                          project_fore_color = ss.vw_crm_task.project_fore_color,
                                          priority_fore_color = ss.vw_crm_task.priority_fore_color,
                                          moderator_user_id = ss.vw_crm_task.moderator_user_id,
                                          moderator_user_name = ss.vw_crm_task.moderator_user_name,
                                          storage_folder_id = ss.vw_crm_task.storage_folder_id,
                                          storage_folder_name = ss.vw_crm_task.storage_folder_name,
                                          is_in_storage = ss.vw_crm_task.is_in_storage,
                                          is_in_storage_name = ss.vw_crm_task.is_in_storage_name,
                                          IsModerated = ss.vw_crm_task.moderator_user_id > 0,

                                          project_name = ss.vw_crm_task.project_name,
                                          module_name = ss.vw_crm_task.module_name,
                                          module_part_name = ss.vw_crm_task.module_part_name,
                                          module_version_name = ss.vw_crm_task.module_version_name,
                                          client_name = ss.vw_crm_task.client_name,
                                          priority_name = ss.vw_crm_task.priority_name,
                                          state_name = ss.vw_crm_task.state_name,
                                          owner_user_name = ss.vw_crm_task.owner_user_name,
                                          exec_user_name = ss.vw_crm_task.exec_user_name,
                                          repair_version_name = ss.vw_crm_task.repair_version_name,
                                          task_num_int = ss.vw_crm_task.task_num_int,
                                          prepared_user_name = ss.vw_crm_task.prepared_user_name,
                                          fixed_user_name = ss.vw_crm_task.fixed_user_name,
                                          approved_user_name = ss.vw_crm_task.approved_user_name,
                                          assigned_user_name = ss.vw_crm_task.assigned_user_name,
                                          group_name = ss.vw_crm_task.group_name,
                                          task_text_substring = ss.vw_crm_task.task_text_substring,
                                          subtasks_text = ss.vw_crm_task.subtasks_text,
                                          subtasks_task_id = ss.vw_crm_task.subtasks_task_id,
                                          subtasks_count = ss.vw_crm_task.subtasks_count,
                                          notes_text = ss.vw_crm_task.notes_text,
                                          notes_task_id = ss.vw_crm_task.notes_task_id,
                                          notes_count = ss.vw_crm_task.notes_count,
                                          batch_id = ss.vw_crm_task.batch_id,
                                          batch_name = ss.vw_crm_task.batch_name,
                                          priority_is_boss = ss.vw_crm_task.priority_is_boss,
                                          state_for_event = ss.vw_crm_task.state_for_event,
                                          event_date_beg = ss.vw_crm_task.event_date_beg,
                                          event_date_end = ss.vw_crm_task.event_date_end,
                                          is_control = ss.vw_crm_task.is_control,
                                          required_date_orig = ss.vw_crm_task.required_date_orig,
                                          repair_date_orig = ss.vw_crm_task.repair_date_orig,
                                          event_date_beg_orig = ss.vw_crm_task.event_date_beg_orig,
                                          event_date_end_orig = ss.vw_crm_task.event_date_end_orig,
                                          control_crt_date_orig = ss.vw_crm_task.control_crt_date_orig,
                                          control_crt_user_orig = ss.vw_crm_task.control_crt_user_orig,
                                          required_date_curr = ss.vw_crm_task.required_date_curr,
                                          repair_date_curr = ss.vw_crm_task.repair_date_curr,
                                          event_date_beg_curr = ss.vw_crm_task.event_date_beg_curr,
                                          event_date_end_curr = ss.vw_crm_task.event_date_end_curr,
                                          control_crt_date_curr = ss.vw_crm_task.control_crt_date_curr,
                                          control_crt_user_curr = ss.vw_crm_task.control_crt_user_curr,
                                          cost_plan = ss.vw_crm_task.cost_plan,
                                          cost_fact = ss.vw_crm_task.cost_fact,
                                      });

            return res;

        }

        public IQueryable<CrmTaskViewModel> GetList_byGroup(int group_id, string phrase)
        {
            if (!String.IsNullOrEmpty(phrase))
            {
                // кратко, подробно, номер, комментарии, подзадачи

                List<int> res0 = new List<int> { 0 };

                var res1 = from t1 in dbContext.crm_task
                           where (((t1.group_id == group_id) && (group_id >= 0)) || (group_id < 0))
                           && ((t1.task_name.Trim().ToLower().Contains(phrase.Trim().ToLower())) || (t1.task_text.Trim().ToLower().Contains(phrase.Trim().ToLower())) || (t1.task_num.Trim().ToLower().Contains(phrase.Trim().ToLower())))
                           select t1.task_id;

                var res2 = from t1 in dbContext.crm_task
                           from t2 in dbContext.crm_task_note
                           where t1.task_id == t2.task_id
                           && (((t1.group_id == group_id) && (group_id >= 0)) || (group_id < 0))
                           && (t2.note_text.Trim().ToLower().Contains(phrase.Trim().ToLower()))
                           select t1.task_id;

                var res3 = from t1 in dbContext.crm_task
                           from t2 in dbContext.crm_subtask
                           where t1.task_id == t2.task_id
                           && (((t1.group_id == group_id) && (group_id >= 0)) || (group_id < 0))
                           && (t2.subtask_text.Trim().ToLower().Contains(phrase.Trim().ToLower()))
                           select t1.task_id;

                var res_full = res0.Union(res1.Union(res2).Union(res3));

                return from ss in dbContext.vw_crm_task
                       from t2 in res_full
                       where ss.task_id == t2
                       && (((ss.group_id == group_id) && (group_id >= 0)) || (group_id < 0)) && ss.view_user_id == currUser.CabUserId
                       select new CrmTaskViewModel
                 {
                     task_id = ss.task_id,
                     task_name = ss.task_name,
                     task_text = ss.task_text,
                     project_id = ss.project_id,
                     module_id = ss.module_id,
                     module_part_id = ss.module_part_id,
                     module_version_id = ss.module_version_id,
                     client_id = ss.client_id,
                     priority_id = ss.priority_id,
                     state_id = ss.state_id,
                     owner_user_id = ss.owner_user_id,
                     exec_user_id = ss.exec_user_id,
                     task_num = ss.task_num,
                     crt_date = ss.crt_date,
                     required_date = ss.required_date,
                     repair_date = ss.repair_date,
                     repair_version_id = ss.repair_version_id,
                     group_id = ss.group_id,                     
                     prepared_user_id = ss.prepared_user_id,
                     fixed_user_id = ss.fixed_user_id,
                     approved_user_id = ss.approved_user_id,
                     assigned_user_id = ss.assigned_user_id,
                     is_archive = false,
                     rel_child_cnt = ss.rel_child_cnt,
                     rel_parent_cnt = ss.rel_parent_cnt,
                     is_fin = ss.is_fin,
                     is_fin_name = ss.is_fin_name,
                     fin_cost = ss.fin_cost,
                     is_event = ss.is_event,
                     is_event_name = ss.is_event_name,
                     event_user_id = ss.event_user_id,
                     upd_date = ss.upd_date,
                     upd_num = ss.upd_num,
                     upd_user = ss.upd_user,
                     is_event_for_exec = (ss.event_user_id == ss.exec_user_id) && (ss.is_event),
                     view_user_id = ss.view_user_id,
                     state_fore_color = ss.state_fore_color,
                     project_fore_color = ss.project_fore_color,
                     priority_fore_color = ss.priority_fore_color,
                     moderator_user_id = ss.moderator_user_id,
                     moderator_user_name = ss.moderator_user_name,
                     storage_folder_id = ss.storage_folder_id,
                     storage_folder_name = ss.storage_folder_name,
                     is_in_storage = ss.is_in_storage,
                     is_in_storage_name = ss.is_in_storage_name,
                     IsModerated = ss.moderator_user_id > 0,

                     project_name = ss.project_name,
                     module_name = ss.module_name,
                     module_part_name = ss.module_part_name,
                     module_version_name = ss.module_version_name,
                     client_name = ss.client_name,
                     priority_name = ss.priority_name,
                     state_name = ss.state_name,
                     owner_user_name = ss.owner_user_name,
                     exec_user_name = ss.exec_user_name,
                     repair_version_name = ss.repair_version_name,
                     task_num_int = ss.task_num_int,
                     prepared_user_name = ss.prepared_user_name,
                     fixed_user_name = ss.fixed_user_name,
                     approved_user_name = ss.approved_user_name,
                     assigned_user_name = ss.assigned_user_name,
                     group_name = ss.group_name,
                     task_text_substring = ss.task_text_substring,
                     subtasks_text = ss.subtasks_text,
                     subtasks_task_id = ss.subtasks_task_id,
                     subtasks_count = ss.subtasks_count,
                     notes_text = ss.notes_text,
                     notes_task_id = ss.notes_task_id,
                     notes_count = ss.notes_count,
                     batch_id = ss.batch_id,
                     batch_name = ss.batch_name,
                     priority_is_boss = ss.priority_is_boss,
                     state_for_event = ss.state_for_event,
                     event_date_beg = ss.event_date_beg,
                     event_date_end = ss.event_date_end,
                     is_control = ss.is_control,
                     required_date_orig = ss.required_date_orig,
                     repair_date_orig = ss.repair_date_orig,
                     event_date_beg_orig = ss.event_date_beg_orig,
                     event_date_end_orig = ss.event_date_end_orig,
                     control_crt_date_orig = ss.control_crt_date_orig,
                     control_crt_user_orig = ss.control_crt_user_orig,
                     required_date_curr = ss.required_date_curr,
                     repair_date_curr = ss.repair_date_curr,
                     event_date_beg_curr = ss.event_date_beg_curr,
                     event_date_end_curr = ss.event_date_end_curr,
                     control_crt_date_curr = ss.control_crt_date_curr,
                     control_crt_user_curr = ss.control_crt_user_curr,
                     cost_plan = ss.cost_plan,
                     cost_fact = ss.cost_fact,
                 };
            }

            return dbContext.vw_crm_task
                .Where(ss => (((ss.group_id == group_id) && (group_id >= 0)) || (group_id < 0)) && ss.view_user_id == currUser.CabUserId)
                .Select(ss => new CrmTaskViewModel
                {
                    task_id = ss.task_id,
                    task_name = ss.task_name,
                    task_text = ss.task_text,
                    project_id = ss.project_id,
                    module_id = ss.module_id,
                    module_part_id = ss.module_part_id,
                    module_version_id = ss.module_version_id,
                    client_id = ss.client_id,
                    priority_id = ss.priority_id,
                    state_id = ss.state_id,
                    owner_user_id = ss.owner_user_id,
                    exec_user_id = ss.exec_user_id,
                    task_num = ss.task_num,
                    crt_date = ss.crt_date,
                    required_date = ss.required_date,
                    repair_date = ss.repair_date,
                    repair_version_id = ss.repair_version_id,
                    group_id = ss.group_id,                    
                    prepared_user_id = ss.prepared_user_id,
                    fixed_user_id = ss.fixed_user_id,
                    approved_user_id = ss.approved_user_id,
                    assigned_user_id = ss.assigned_user_id,
                    is_archive = false,
                    rel_child_cnt = ss.rel_child_cnt,
                    rel_parent_cnt = ss.rel_parent_cnt,
                    is_fin = ss.is_fin,
                    is_fin_name = ss.is_fin_name,
                    fin_cost = ss.fin_cost,
                    is_event = ss.is_event,
                    is_event_name = ss.is_event_name,
                    event_user_id = ss.event_user_id,
                    upd_date = ss.upd_date,
                    upd_num = ss.upd_num,
                    upd_user = ss.upd_user,
                    is_event_for_exec = (ss.event_user_id == ss.exec_user_id) && (ss.is_event),
                    view_user_id = ss.view_user_id,
                    state_fore_color = ss.state_fore_color,
                    project_fore_color = ss.project_fore_color,
                    priority_fore_color = ss.priority_fore_color,
                    moderator_user_id = ss.moderator_user_id,
                    moderator_user_name = ss.moderator_user_name,
                    storage_folder_id = ss.storage_folder_id,
                    storage_folder_name = ss.storage_folder_name,
                    is_in_storage = ss.is_in_storage,
                    is_in_storage_name = ss.is_in_storage_name,
                    IsModerated = ss.moderator_user_id > 0,

                    project_name = ss.project_name,
                    module_name = ss.module_name,
                    module_part_name = ss.module_part_name,
                    module_version_name = ss.module_version_name,
                    client_name = ss.client_name,
                    priority_name = ss.priority_name,
                    state_name = ss.state_name,
                    owner_user_name = ss.owner_user_name,
                    exec_user_name = ss.exec_user_name,
                    repair_version_name = ss.repair_version_name,
                    task_num_int = ss.task_num_int,
                    prepared_user_name = ss.prepared_user_name,
                    fixed_user_name = ss.fixed_user_name,
                    approved_user_name = ss.approved_user_name,
                    assigned_user_name = ss.assigned_user_name,
                    group_name = ss.group_name,
                    task_text_substring = ss.task_text_substring,
                    subtasks_text = ss.subtasks_text,
                    subtasks_task_id = ss.subtasks_task_id,
                    subtasks_count = ss.subtasks_count,
                    notes_text = ss.notes_text,
                    notes_task_id = ss.notes_task_id,
                    notes_count = ss.notes_count,
                    batch_id = ss.batch_id,
                    batch_name = ss.batch_name,
                    priority_is_boss = ss.priority_is_boss,
                    state_for_event = ss.state_for_event,
                    event_date_beg = ss.event_date_beg,
                    event_date_end = ss.event_date_end,
                    is_control = ss.is_control,
                    required_date_orig = ss.required_date_orig,
                    repair_date_orig = ss.repair_date_orig,
                    event_date_beg_orig = ss.event_date_beg_orig,
                    event_date_end_orig = ss.event_date_end_orig,
                    control_crt_date_orig = ss.control_crt_date_orig,
                    control_crt_user_orig = ss.control_crt_user_orig,
                    required_date_curr = ss.required_date_curr,
                    repair_date_curr = ss.repair_date_curr,
                    event_date_beg_curr = ss.event_date_beg_curr,
                    event_date_end_curr = ss.event_date_end_curr,
                    control_crt_date_curr = ss.control_crt_date_curr,
                    control_crt_user_curr = ss.control_crt_user_curr,
                    cost_plan = ss.cost_plan,
                    cost_fact = ss.cost_fact,
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.vw_crm_task
                .Where(ss => ss.task_id == id && ss.view_user_id == currUser.CabUserId)
                .Select(ss => new CrmTaskViewModel
                {
                    task_id = ss.task_id,
                    task_name = ss.task_name,
                    task_text = ss.task_text,
                    project_id = ss.project_id,
                    module_id = ss.module_id,
                    module_part_id = ss.module_part_id,
                    module_version_id = ss.module_version_id,
                    client_id = ss.client_id,
                    priority_id = ss.priority_id,
                    state_id = ss.state_id,
                    owner_user_id = ss.owner_user_id,
                    exec_user_id = ss.exec_user_id,
                    task_num = ss.task_num,
                    crt_date = ss.crt_date,
                    required_date = ss.required_date,
                    repair_date = ss.repair_date,
                    repair_version_id = ss.repair_version_id,
                    group_id = ss.group_id,                    
                    prepared_user_id = ss.prepared_user_id,
                    fixed_user_id = ss.fixed_user_id,
                    approved_user_id = ss.approved_user_id,
                    assigned_user_id = ss.assigned_user_id,
                    is_archive = false,
                    rel_child_cnt = ss.rel_child_cnt,
                    rel_parent_cnt = ss.rel_parent_cnt,
                    is_fin = ss.is_fin,
                    is_fin_name = ss.is_fin_name,
                    fin_cost = ss.fin_cost,
                    is_event = ss.is_event,
                    is_event_name = ss.is_event_name,
                    event_user_id = ss.event_user_id,
                    upd_date = ss.upd_date,
                    upd_num = ss.upd_num,
                    upd_user = ss.upd_user,
                    is_event_for_exec = (ss.event_user_id == ss.exec_user_id) && (ss.is_event),
                    view_user_id = ss.view_user_id,
                    state_fore_color = ss.state_fore_color,
                    project_fore_color = ss.project_fore_color,
                    priority_fore_color = ss.priority_fore_color,
                    moderator_user_id = ss.moderator_user_id,
                    moderator_user_name = ss.moderator_user_name,
                    storage_folder_id = ss.storage_folder_id,
                    storage_folder_name = ss.storage_folder_name,
                    is_in_storage = ss.is_in_storage,
                    is_in_storage_name = ss.is_in_storage_name,
                    IsModerated = ss.moderator_user_id > 0,

                    project_name = ss.priority_name,
                    module_name = ss.module_name,
                    module_part_name = ss.module_part_name,
                    module_version_name = ss.module_version_name,
                    client_name = ss.client_name,
                    priority_name = ss.priority_name,
                    state_name = ss.state_name,
                    owner_user_name = ss.owner_user_name,
                    exec_user_name = ss.exec_user_name,
                    repair_version_name = ss.repair_version_name,
                    task_num_int = ss.task_num_int,
                    prepared_user_name = ss.prepared_user_name,
                    fixed_user_name = ss.fixed_user_name,
                    approved_user_name = ss.approved_user_name,
                    assigned_user_name = ss.assigned_user_name,
                    group_name = ss.group_name,
                    task_text_substring = ss.task_text_substring,
                    subtasks_text = ss.subtasks_text,
                    subtasks_task_id = ss.subtasks_task_id,
                    subtasks_count = ss.subtasks_count,
                    notes_text = ss.notes_text,
                    notes_task_id = ss.notes_task_id,
                    notes_count = ss.notes_count,
                    batch_id = ss.batch_id,
                    batch_name = ss.batch_name,
                    priority_is_boss = ss.priority_is_boss,
                    state_for_event = ss.state_for_event,
                    event_date_beg = ss.event_date_beg,
                    event_date_end = ss.event_date_end,
                    is_control = ss.is_control,
                    required_date_orig = ss.required_date_orig,
                    repair_date_orig = ss.repair_date_orig,
                    event_date_beg_orig = ss.event_date_beg_orig,
                    event_date_end_orig = ss.event_date_end_orig,
                    control_crt_date_orig = ss.control_crt_date_orig,
                    control_crt_user_orig = ss.control_crt_user_orig,
                    required_date_curr = ss.required_date_curr,
                    repair_date_curr = ss.repair_date_curr,
                    event_date_beg_curr = ss.event_date_beg_curr,
                    event_date_end_curr = ss.event_date_end_curr,
                    control_crt_date_curr = ss.control_crt_date_curr,
                    control_crt_user_curr = ss.control_crt_user_curr,
                    cost_plan = ss.cost_plan,
                    cost_fact = ss.cost_fact,
                })
                .FirstOrDefault();

            if (res == null)
                return res;

            res.notes = dbContext.vw_crm_task_note
                .Where(ss => ss.task_id == res.task_id)
                .Select(ss => new CrmTaskNoteViewModel()
            {
                note_id = ss.note_id,
                task_id = ss.task_id,
                subtask_id = ss.subtask_id,
                crt_date = ss.crt_date,
                owner_user_id = ss.owner_user_id,
                note_text = ss.note_text,
                file_name = ss.file_name,
                is_file_exists = ss.is_file_exists,
                user_name = ss.user_name,
                note_num = ss.note_num,
            }
            );

            IEnumerable<crm_state> crm_state_list = dbContext.crm_state;
            IEnumerable<cab_user> cab_user_list = dbContext.cab_user.Where(ss => ss.is_active && ss.is_crm_user);

            res.subtasks = dbContext.crm_subtask
                .Where(ss => ss.task_id == res.task_id)
                .Select(ss => new CrmSubtaskViewModel()
                {
                    subtask_id = ss.subtask_id,
                    task_id = ss.task_id,
                    subtask_text = ss.subtask_text,
                    state_id = ss.state_id,
                    owner_user_id = ss.owner_user_id,
                    crt_date = ss.crt_date,
                    repair_date = ss.repair_date,
                    task_num = ss.task_num,
                    upd_date = ss.upd_date,
                    owner_user_name = ss.cab_user1.user_name,
                    file_id = ss.file_id,
                    file_name = ss.crm_task_file == null ? "" : ss.crm_task_file.file_name,
                    IsChecked = ss.state_id > 0,
                    IsNew = false,
                    StateList = crm_state_list.Select(tt => new CrmStateViewModel()
                        {
                            state_id = tt.state_id,
                            state_name = tt.state_name,
                        }
                        ),
                    state_name_db = ss.crm_state.state_name,
                    State = new CrmStateViewModel() { state_id = ss.state_id, state_name = ss.crm_state.state_name, },

                    ExecUserList = cab_user_list.Select(tt => new CabUserViewModel()
                    {
                        user_id = tt.user_id,
                        user_name = tt.user_name,
                    }
                    ),
                    exec_user_name_db = ss.cab_user.user_name,
                    ExecUser = new CabUserViewModel() { user_id = ss.exec_user_id, user_name = ss.cab_user.user_name, },

                    TaskFile = new CrmTaskFileViewModel() { file_id = (ss.file_id != null ? (int)ss.file_id : 0), file_name = ss.crm_task_file.file_name },
                }
            );

            res.logs = dbContext.vw_log_crm
                .Where(ss => ss.task_id == res.task_id)
                .Select(ss => new CrmLogViewModel()
                {
                    log_id = ss.log_id,
                    date_beg = ss.date_beg,
                    date_end = ss.date_end,
                    user_id = ss.user_id,
                    mess = ss.mess,
                    task_id = ss.task_id,
                    user_name = ss.user_name,
                    full_name = ss.full_name,
                    task_num = ss.task_num,
                    task_name = ss.task_name,
                }
            )
            .ToList();

            res.files = dbContext.crm_task_file
                .Where(ss => ss.task_id == res.task_id)
                .ProjectTo<CrmTaskFileViewModel>();

            return res;
        }

        public IQueryable<CrmTaskViewModel> GetList_Arc()
        {
            return dbContext.vw_crm_task_arc
                .Select(ss => new CrmTaskViewModel
                {
                    task_id = ss.task_id,
                    task_name = ss.task_name,
                    task_text = ss.task_text,
                    project_id = ss.project_id,
                    module_id = ss.module_id,
                    module_part_id = ss.module_part_id,
                    module_version_id = ss.module_version_id,
                    client_id = ss.client_id,
                    priority_id = ss.priority_id,
                    state_id = ss.state_id,
                    owner_user_id = ss.owner_user_id,
                    exec_user_id = ss.exec_user_id,
                    task_num = ss.task_num,
                    crt_date = ss.crt_date,
                    required_date = ss.required_date,
                    repair_date = ss.repair_date,
                    repair_version_id = ss.repair_version_id,
                    group_id = ss.group_id,                    
                    prepared_user_id = ss.prepared_user_id,
                    fixed_user_id = ss.fixed_user_id,
                    approved_user_id = ss.approved_user_id,
                    assigned_user_id = ss.assigned_user_id,
                    is_archive = true,

                    project_name = ss.project_name,
                    module_name = ss.module_name,
                    module_part_name = ss.module_part_name,
                    module_version_name = ss.module_version_name,
                    client_name = ss.client_name,
                    priority_name = ss.priority_name,
                    state_name = ss.state_name,
                    owner_user_name = ss.owner_user_name,
                    exec_user_name = ss.exec_user_name,
                    repair_version_name = ss.repair_version_name,
                    task_num_int = ss.task_num_int,
                    prepared_user_name = ss.prepared_user_name,
                    fixed_user_name = ss.fixed_user_name,
                    approved_user_name = ss.approved_user_name,
                    assigned_user_name = ss.assigned_user_name,
                    group_name = ss.group_name,                    
                });
        }

        public IQueryable<SimpleCombo> GetNumList(string num)
        {
            //var task_num_search = ("AU-" + num).ToLower();
            var task_num_search = num.ToLower();
            return dbContext.vw_crm_task
                //.Where(ss => ss.task_num.ToLower().StartsWith(task_num_search))
                .Where(ss => ss.view_user_id == currUser.CabUserId && ss.task_num.ToLower().Contains(task_num_search))
                .OrderByDescending(ss => ss.task_num)
                .Select(ss => new SimpleCombo() { obj_id = ss.task_id, obj_name = ss.task_num, })
                ;
        }

        public IQueryable<CrmTaskViewModel> GetShortList_ForExecUser(string num, int exec_user_id)
        {
            var task_num_search = num.ToLower();
            var curr_cab_user_id = currUser.CabUserId;
            return dbContext.vw_crm_task
                .Where(ss => /*ss.exec_user_id == exec_user_id &&*/
                    ss.view_user_id == curr_cab_user_id && ss.task_num.ToLower().Contains(task_num_search))
                .OrderByDescending(ss => ss.task_num)
                .Select(ss => new CrmTaskViewModel()
                {
                    task_id = ss.task_id,
                    task_num = ss.task_num,
                    task_name = ss.task_name,
                    task_text = ss.task_text,
                })
                ;
        }

        public string GetTaskText(int task_id)
        {
            return dbContext.crm_task.Where(ss => ss.task_id == task_id).Select(ss => ss.task_text).FirstOrDefault();
        }

        public IQueryable<CrmTaskViewModel> GetList_forPlannerExport(DateTime date_beg, DateTime date_end, int? exec_user_id)
        {
            int curr_user_id = currUser.CabUserId;
            DateTime date_end_actual = date_end.AddDays(1);

            return from ss in dbContext.vw_crm_task
                         from t2 in dbContext.vw_planner_event
                    .Where(t2 =>
                        ((date_beg >= t2.date_beg && date_beg <= t2.date_end) || (date_end_actual >= t2.date_beg && date_end_actual <= t2.date_end) || (t2.date_beg >= date_beg && t2.date_beg < date_end_actual) || (t2.date_end >= date_beg && t2.date_end < date_end_actual))
                        && (
                        ((exec_user_id == curr_user_id) && (t2.exec_user_id == exec_user_id))
                        || ((exec_user_id != curr_user_id) && (t2.exec_user_id == exec_user_id) && (t2.is_public))
                        || ((exec_user_id == null) && (t2.is_public))
                        || ((exec_user_id == null) && (t2.exec_user_id == curr_user_id))
                        )
                        )
                         where ss.task_id == t2.task_id
                         && ss.view_user_id == curr_user_id
                         select new CrmTaskViewModel
                 {
                     task_id = ss.task_id,
                     task_name = ss.task_name,
                     task_text = ss.task_text,
                     project_id = ss.project_id,
                     module_id = ss.module_id,
                     module_part_id = ss.module_part_id,
                     module_version_id = ss.module_version_id,
                     client_id = ss.client_id,
                     priority_id = ss.priority_id,
                     state_id = ss.state_id,
                     owner_user_id = ss.owner_user_id,
                     exec_user_id = ss.exec_user_id,
                     task_num = ss.task_num,
                     crt_date = ss.crt_date,
                     required_date = ss.required_date,
                     repair_date = ss.repair_date,
                     repair_version_id = ss.repair_version_id,
                     group_id = ss.group_id,                     
                     prepared_user_id = ss.prepared_user_id,
                     fixed_user_id = ss.fixed_user_id,
                     approved_user_id = ss.approved_user_id,
                     assigned_user_id = ss.assigned_user_id,
                     is_archive = false,
                     rel_child_cnt = ss.rel_child_cnt,
                     rel_parent_cnt = ss.rel_parent_cnt,
                     is_fin = ss.is_fin,
                     is_fin_name = ss.is_fin_name,
                     fin_cost = ss.fin_cost,
                     is_event = ss.is_event,
                     is_event_name = ss.is_event_name,
                     event_user_id = ss.event_user_id,
                     upd_date = ss.upd_date,
                     upd_num = ss.upd_num,
                     upd_user = ss.upd_user,
                     is_event_for_exec = (ss.event_user_id == ss.exec_user_id) && (ss.is_event),
                     view_user_id = ss.view_user_id,
                     state_fore_color = ss.state_fore_color,
                     project_fore_color = ss.project_fore_color,
                     priority_fore_color = ss.priority_fore_color,
                     moderator_user_id = ss.moderator_user_id,
                     moderator_user_name = ss.moderator_user_name,
                     storage_folder_id = ss.storage_folder_id,
                     storage_folder_name = ss.storage_folder_name,
                     is_in_storage = ss.is_in_storage,
                     is_in_storage_name = ss.is_in_storage_name,
                     IsModerated = ss.moderator_user_id > 0,

                     project_name = ss.project_name,
                     module_name = ss.module_name,
                     module_part_name = ss.module_part_name,
                     module_version_name = ss.module_version_name,
                     client_name = ss.client_name,
                     priority_name = ss.priority_name,
                     state_name = ss.state_name,
                     owner_user_name = ss.owner_user_name,
                     exec_user_name = ss.exec_user_name,
                     repair_version_name = ss.repair_version_name,
                     task_num_int = ss.task_num_int,
                     prepared_user_name = ss.prepared_user_name,
                     fixed_user_name = ss.fixed_user_name,
                     approved_user_name = ss.approved_user_name,
                     assigned_user_name = ss.assigned_user_name,
                     group_name = ss.group_name,
                     task_text_substring = ss.task_text_substring,
                     subtasks_text = ss.subtasks_text,
                     subtasks_task_id = ss.subtasks_task_id,
                     subtasks_count = ss.subtasks_count,
                     notes_text = ss.notes_text,
                     notes_task_id = ss.notes_task_id,
                     notes_count = ss.notes_count,
                     batch_id = ss.batch_id,
                     batch_name = ss.batch_name,
                     priority_is_boss = ss.priority_is_boss,
                     state_for_event = ss.state_for_event,
                     event_date_beg = ss.event_date_beg,
                     event_date_end = ss.event_date_end,
                     is_control = ss.is_control,
                     required_date_orig = ss.required_date_orig,
                     repair_date_orig = ss.repair_date_orig,
                     event_date_beg_orig = ss.event_date_beg_orig,
                     event_date_end_orig = ss.event_date_end_orig,
                     control_crt_date_orig = ss.control_crt_date_orig,
                     control_crt_user_orig = ss.control_crt_user_orig,
                     required_date_curr = ss.required_date_curr,
                     repair_date_curr = ss.repair_date_curr,
                     event_date_beg_curr = ss.event_date_beg_curr,
                     event_date_end_curr = ss.event_date_end_curr,
                     control_crt_date_curr = ss.control_crt_date_curr,
                     control_crt_user_curr = ss.control_crt_user_curr,
                     cost_plan = ss.cost_plan,
                     cost_fact = ss.cost_fact,
                 };

        }

        #endregion

        #region Edit

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmTaskViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CrmTaskViewModel itemAdd = (CrmTaskViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты задачи");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            itemAdd.is_control = currUser.IsBoss ? itemAdd.is_control : false;

            var task_max = dbContext.vw_crm_task.Where(ss => ss.view_user_id == currUser.CabUserId).OrderByDescending(ss => ss.task_num_int).FirstOrDefault();
            int task_num_int = task_max != null ? task_max.task_num_int + 1 : 1;

            crm_task crm_task = new crm_task();
            crm_task.client_id = itemAdd.client_id;
            crm_task.crt_date = now;
            crm_task.exec_user_id = itemAdd.exec_user_id;
            crm_task.module_id = itemAdd.module_id;
            //crm_task.module_part_id = itemAdd.module_part_id;
            crm_task.module_part_id = 0;
            crm_task.module_version_id = itemAdd.module_version_id;
            crm_task.owner_user_id = currUser.CabUserId;
            crm_task.priority_id = itemAdd.priority_id;
            crm_task.project_id = itemAdd.project_id;
            crm_task.repair_date = itemAdd.repair_date;
            crm_task.repair_version_id = itemAdd.repair_version_id.GetValueOrDefault(0);
            crm_task.required_date = itemAdd.required_date;
            crm_task.state_id = itemAdd.state_id;
            crm_task.task_name = itemAdd.task_name;
            crm_task.task_num = "AU-" + task_num_int.ToExactLength(4, "0");
            crm_task.task_text = itemAdd.task_text;
            crm_task.group_id = itemAdd.group_id;            
            crm_task.prepared_user_id = 0;
            crm_task.fixed_user_id = 0;
            crm_task.approved_user_id = 0;
            crm_task.assigned_user_id = itemAdd.assigned_user_id;
            crm_task.is_fin = itemAdd.fin_cost.GetValueOrDefault(0) > 0 ? true : false;
            crm_task.fin_cost = itemAdd.fin_cost.GetValueOrDefault(0) > 0 ? itemAdd.fin_cost : null;
            //crm_task.is_event = itemAdd.is_event;
            crm_task.moderator_user_id = itemAdd.IsModerated ? currUser.CabUserId : 0;
            crm_task.storage_folder_id = itemAdd.storage_folder_id;
            crm_task.batch_id = itemAdd.batch_id;
            crm_task.cost_plan = itemAdd.cost_plan;
            crm_task.cost_fact = itemAdd.cost_fact;

            crm_task.upd_date = now;
            crm_task.upd_user = currUser.CabUserName;
            crm_task.upd_num = 0;

            crm_task.is_control = itemAdd.is_control;
                
            dbContext.crm_task.Add(crm_task);

            bool is_add_event_ok = false;
            DateTime event_date_beg = today;
            DateTime event_date_end = today;
            planner_event planner_event = null;
            planner_task_event planner_task_event = null;
            crm_task_control crm_task_control = new crm_task_control();

            if (itemAdd.is_event)
            {
                is_add_event_ok = true;
                if (itemAdd.exec_user_id <= 0)
                    is_add_event_ok = false;
                if (is_add_event_ok)
                {
                    event_date_beg = itemAdd.event_date_beg.HasValue ? (DateTime)itemAdd.event_date_beg : (itemAdd.required_date.HasValue ? (DateTime)itemAdd.required_date : today);
                    event_date_end = itemAdd.event_date_end.HasValue ? (DateTime)itemAdd.event_date_end : event_date_beg;
                    event_date_end = event_date_end < event_date_beg ? event_date_beg : event_date_end;
                    planner_event = new planner_event();
                    planner_event.description = itemAdd.task_text;
                    planner_event.date_end = event_date_end;
                    planner_event.date_beg = event_date_beg;
                    planner_event.is_all_day = true;
                    planner_event.title = itemAdd.task_name;
                    planner_event.exec_user_id = itemAdd.exec_user_id;
                    planner_event.is_public = true;
                    planner_event.event_priority_id = (int)Enums.PlannerEventPriorityEnum.LEVEL0;
                    planner_event.rate = null;
                    planner_event.crt_date = now;
                    planner_event.crt_user = currUser.UserName;
                    planner_event.upd_date = now;
                    planner_event.upd_user = currUser.UserName;
                    planner_event.crm_task = crm_task;
                    planner_event.event_state_id = itemAdd.state_id;
                    planner_event.progress = null;
                    planner_event.result = null;
                    planner_event.event_group_id = (int)Enums.CrmEventGroupEnum.NORMAL;
                    planner_event.is_public_global = false;
                    dbContext.planner_event.Add(planner_event);
                    planner_task_event = new planner_task_event();
                    planner_task_event.crm_task = crm_task;
                    planner_task_event.planner_event = planner_event;
                    planner_task_event.is_actual = true;
                    dbContext.planner_task_event.Add(planner_task_event);
                }
            }

            if (itemAdd.is_control)
            {
                crm_task_control = new crm_task_control();
                crm_task_control.crm_task = crm_task;
                crm_task_control.is_orig = true;
                crm_task_control.required_date = itemAdd.required_date;
                crm_task_control.repair_date = itemAdd.repair_date;
                crm_task_control.event_date_beg = is_add_event_ok ? (DateTime?)event_date_beg : null;
                crm_task_control.event_date_end = is_add_event_ok ? (DateTime?)event_date_end : null;
                crm_task_control.crt_date = now;
                crm_task_control.crt_user = currUser.UserName;
                dbContext.crm_task_control.Add(crm_task_control);
                crm_task_control = new crm_task_control();
                crm_task_control.crm_task = crm_task;
                crm_task_control.is_orig = false;
                crm_task_control.required_date = itemAdd.required_date;
                crm_task_control.repair_date = itemAdd.repair_date;
                crm_task_control.event_date_beg = is_add_event_ok ? (DateTime?)event_date_beg : null;
                crm_task_control.event_date_end = is_add_event_ok ? (DateTime?)event_date_end : null;
                crm_task_control.crt_date = now;
                crm_task_control.crt_user = currUser.UserName;
                dbContext.crm_task_control.Add(crm_task_control);
            }

            List<string> log_mess_list1 = new List<string>();
            List<string> log_mess_list2 = new List<string>();
            if (itemAdd.notes != null)
            {
                foreach (var note in itemAdd.notes.Where(ss => ss.note_text != null).Reverse())
                {
                    note.note_text = GlobalUtil.Linkify(note.note_text);
                    crm_task_note crm_task_note = new crm_task_note();
                    crm_task_note.crt_date = now;
                    crm_task_note.file_name = note.file_name;
                    crm_task_note.note_text = note.note_text;
                    crm_task_note.owner_user_id = note.owner_user_id;
                    crm_task_note.subtask_id = null;
                    crm_task_note.crm_task = crm_task;
                    dbContext.crm_task_note.Add(crm_task_note);
                    log_mess_list1.Add("Добавление комментария № " + crm_task_note.note_id.ToString());
                }
            }

            if (itemAdd.subtasks != null)
            {
                foreach (var subtask in itemAdd.subtasks.Reverse())
                {
                    subtask.subtask_text = GlobalUtil.Linkify(subtask.subtask_text);
                    crm_subtask crm_subtask = new crm_subtask();
                    crm_subtask.crt_date = now;
                    //crm_subtask.owner_user_id = crm_task.owner_user_id;
                    crm_subtask.owner_user_id = currUser.CabUserId;
                    crm_subtask.subtask_text = subtask.subtask_text;
                    //crm_subtask.state_id = subtask.IsChecked ? 1 : 0;
                    crm_subtask.state_id = subtask.state_id;
                    //crm_subtask.repair_date = subtask.IsChecked ? (DateTime?)now : null;
                    crm_subtask.repair_date = null;
                    crm_subtask.task_num = subtask.task_num;
                    crm_subtask.upd_date = null;
                    crm_subtask.file_id = subtask.file_id;
                    crm_subtask.exec_user_id = crm_task.exec_user_id;
                    crm_subtask.crm_task = crm_task;
                    dbContext.crm_subtask.Add(crm_subtask);
                    log_mess_list2.Add("Добавление подзадачи № " + crm_subtask.task_num.ToString());
                }
            }
            
            dbContext.SaveChanges();

            ToLog(now, "Создана задача " + crm_task.task_num, crm_task.task_id);

            if (is_add_event_ok)
                ToLog(now, "Задача поставлена в календарь c " + event_date_beg.ToString("dd.MM.yyyy") + " по " + event_date_end.ToString("dd.MM.yyyy"), crm_task.task_id);

            if (log_mess_list1.Count > 0)
            {
                foreach (var log_mess in log_mess_list1)
                {
                    ToLog(now, log_mess, crm_task.task_id);
                }
            }

            if (log_mess_list2.Count > 0)
            {
                foreach (var log_mess in log_mess_list2)
                {
                    ToLog(now, log_mess, crm_task.task_id);
                }
            }

            var crm_task_db = dbContext.vw_crm_task.Where(ss => ss.task_id == crm_task.task_id && ss.view_user_id == currUser.CabUserId).FirstOrDefault();
            
            // notify            
            string mess_notify = "Создана новая задача:";
            mess_notify += System.Environment.NewLine;
                        
            mess_notify += GetNotifyIcqContent(crm_task_db);

            mess_notify += "Автор изменений: " + currUser.CabUserName;
            mess_notify += System.Environment.NewLine;
            mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
            mess_notify += System.Environment.NewLine;
            
            List<int> notifyTargetList_ICQ = dbContext.vw_crm_notify_param_user.Where(ss =>
                (
                ((ss.changer_user_id == crm_task.assigned_user_id) && (ss.is_owner == true) && (ss.user_id != currUser.CabUserId))
                ||
                ((ss.changer_user_id == crm_task.exec_user_id) && (ss.is_executer == true) && (ss.user_id != currUser.CabUserId))
                )
                )
                .Select(ss => ss.user_id)
                .Distinct()
                .ToList();

            if ((notifyTargetList_ICQ != null) && (notifyTargetList_ICQ.Count > 0))
            {
                foreach (var notifyTarget in notifyTargetList_ICQ)
                {
                    notify notify = new notify();
                    notify.crt_date = now;
                    notify.message = mess_notify;
                    notify.message_sent = false;
                    notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                    notify.sent_date = null;
                    notify.target_user_id = notifyTarget;
                    notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                    dbContext.notify.Add(notify);
                    notify = new notify();
                    notify.crt_date = now;
                    notify.message = mess_notify;
                    notify.message_sent = false;
                    notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                    notify.sent_date = null;
                    notify.target_user_id = notifyTarget;
                    notify.transport_type = (int)Enums.NotifyTransportTypeEnum.EMAIL;
                    dbContext.notify.Add(notify);
                }
                dbContext.SaveChanges();
            }

            return new CrmTaskViewModel(crm_task_db);
        }
     
        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmTaskViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CrmTaskViewModel itemEdit = (CrmTaskViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты задачи");
                return null;
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string mess_log = "";
            string mess_notify = "";            
            string mess_notify_detail = "";

            crm_task crm_task = dbContext.crm_task.Where(ss => ss.task_id == itemEdit.task_id).FirstOrDefault();
            if (crm_task == null)
            {
                modelState.AddModelError("", "Не найдена задача с кодом " + itemEdit.task_id.ToString());
                return null;
            }

            modelBeforeChanges = new CrmTaskViewModel(crm_task);

            vw_crm_task vw_crm_task = dbContext.vw_crm_task.Where(ss => ss.task_id == itemEdit.task_id && ss.view_user_id == currUser.CabUserId).FirstOrDefault();

            List<string> log_mess_list1 = new List<string>();
            List<string> log_mess_list2 = new List<string>();
            List<string> log_mess_list3 = new List<string>();

            crm_client crm_client = null;
            List<cab_user> cab_user_list = null;
            cab_user cab_user = null;
            List<crm_state> crm_state_list = null;
            crm_state crm_state = null;
            crm_module crm_module = null;
            List<crm_module_version> crm_module_version_list = null;
            crm_module_version crm_module_version = null;
            crm_priority crm_priority = null;
            crm_project crm_project = null;
            crm_group crm_group = null;
            crm_batch crm_batch = null;
            planner_event planner_event = null;
            planner_task_event planner_task_event = null;
            crm_task_control crm_task_control = null;
            DateTime event_date_beg = DateTime.Today;
            DateTime event_date_end = DateTime.Today;

            List<int> watchedColumns = GetNotifyIcqWatchedColumns();

            itemEdit.moderator_user_id = itemEdit.IsModerated ? currUser.CabUserId : 0;
            itemEdit.is_control = currUser.IsBoss ? itemEdit.is_control : crm_task.is_control;
             
            // 1) если задачи не было в календаре и itemEdit.is_event = true - ставим ее в календарь
            // 2) если задача была в календаре и itemEdit.is_event = false - убираем ее из календаря, но только по совпадению planner_event.exec_user_id = crm_task.exec_user_id, обновляем is_actual для этой task_id
            // 3) если задача была в календаре и itemEdit.is_event = true:
            // 3.1) если сменился исполнитель - создаем еще одну задачу в календаре с planner_event.exec_user_id = itemEdit.exec_user_id, ставим ей is_actual = true
            // 3.2) если сменились даты "с-по в календаре" - обновляем даты у planner_event где planner_event.exec_user_id = crm_task.exec_user_id            

            event_date_beg = itemEdit.event_date_beg.HasValue ? (DateTime)itemEdit.event_date_beg : (itemEdit.required_date.HasValue ? (DateTime)itemEdit.required_date : today);
            event_date_end = itemEdit.event_date_end.HasValue ? (DateTime)itemEdit.event_date_end : event_date_beg;
            event_date_end = event_date_end < event_date_beg ? event_date_beg : event_date_end;
            if ((!vw_crm_task.is_event) && (itemEdit.is_event))
            {
                planner_event = new planner_event();
                planner_event.description = itemEdit.task_text;
                planner_event.date_end = event_date_end;
                planner_event.date_beg = event_date_beg;
                planner_event.is_all_day = true;
                planner_event.title = itemEdit.task_name;
                planner_event.exec_user_id = itemEdit.exec_user_id;
                planner_event.is_public = true;
                planner_event.event_priority_id = (int)Enums.PlannerEventPriorityEnum.LEVEL0;
                planner_event.rate = null;
                planner_event.crt_date = now;
                planner_event.crt_user = currUser.UserName;
                planner_event.upd_date = now;
                planner_event.upd_user = currUser.UserName;
                planner_event.crm_task = crm_task;
                planner_event.event_state_id = itemEdit.state_id;
                planner_event.progress = null;
                planner_event.result = null;
                planner_event.event_group_id = (int)Enums.CrmEventGroupEnum.NORMAL;
                planner_event.is_public_global = false;
                dbContext.planner_event.Add(planner_event);
                planner_task_event = new planner_task_event();
                planner_task_event.crm_task = crm_task;
                planner_task_event.planner_event = planner_event;
                planner_task_event.is_actual = true;
                dbContext.planner_task_event.Add(planner_task_event);

                if (cab_user_list == null)
                    cab_user_list = dbContext.cab_user.ToList();
                cab_user = cab_user_list.Where(ss => ss.user_id == planner_event.exec_user_id).FirstOrDefault();
                mess_log = "Отправка в календарь для " + cab_user.user_name + " на " + event_date_beg.ToString("dd.MM.yyyy") + (event_date_beg != event_date_end ? (" - " + event_date_end.ToString("dd.MM.yyyy")) : "");
                log_mess_list1.Add(mess_log);
            }
            else if ((vw_crm_task.is_event) && (!itemEdit.is_event))
            {
                if (cab_user_list == null)
                    cab_user_list = dbContext.cab_user.ToList();
                cab_user = cab_user_list.Where(ss => ss.user_id == crm_task.exec_user_id).FirstOrDefault();
                dbContext.planner_task_event.RemoveRange(from t1 in dbContext.planner_task_event
                                                         from t2 in dbContext.planner_event
                                                         where t1.event_id == t2.event_id
                                                         && t2.task_id == crm_task.task_id && t2.exec_user_id == crm_task.exec_user_id
                                                         select t1
                                                         );
                dbContext.planner_event.RemoveRange(dbContext.planner_event.Where(ss => ss.task_id == crm_task.task_id && ss.exec_user_id == crm_task.exec_user_id));
                mess_log = "Удаление из календаря у " + cab_user.user_name;
                log_mess_list1.Add(mess_log);
            }
            else if ((vw_crm_task.is_event) && (itemEdit.is_event))
            {
                if ((crm_task.exec_user_id != itemEdit.exec_user_id) && (itemEdit.exec_user_id > 0))
                {
                    planner_event = new planner_event();
                    planner_event.description = itemEdit.task_text;
                    planner_event.date_end = event_date_end;
                    planner_event.date_beg = event_date_beg;
                    planner_event.is_all_day = true;
                    planner_event.title = itemEdit.task_name;
                    planner_event.exec_user_id = itemEdit.exec_user_id;
                    planner_event.is_public = true;
                    planner_event.event_priority_id = (int)Enums.PlannerEventPriorityEnum.LEVEL0;
                    planner_event.rate = null;
                    planner_event.crt_date = now;
                    planner_event.crt_user = currUser.UserName;
                    planner_event.upd_date = now;
                    planner_event.upd_user = currUser.UserName;
                    planner_event.crm_task = crm_task;
                    planner_event.event_state_id = itemEdit.state_id;
                    planner_event.progress = null;
                    planner_event.result = null;
                    planner_event.event_group_id = (int)Enums.CrmEventGroupEnum.NORMAL;
                    planner_event.is_public_global = false;
                    dbContext.planner_event.Add(planner_event);
                    planner_task_event = new planner_task_event();
                    planner_task_event.crm_task = crm_task;
                    planner_task_event.planner_event = planner_event;
                    planner_task_event.is_actual = true;
                    dbContext.planner_task_event.Add(planner_task_event);
                    if (cab_user_list == null)
                        cab_user_list = dbContext.cab_user.ToList();
                    cab_user = cab_user_list.Where(ss => ss.user_id == planner_event.exec_user_id).FirstOrDefault();
                    mess_log = "Отправка в календарь для " + cab_user.user_name + " на " + event_date_beg.ToString("dd.MM.yyyy") + (event_date_beg != event_date_end ? (" - " + event_date_end.ToString("dd.MM.yyyy")) : "");
                    log_mess_list1.Add(mess_log);
                }
                else if ((vw_crm_task.event_date_beg != itemEdit.event_date_beg) || (vw_crm_task.event_date_end != itemEdit.event_date_end))
                {
                    planner_event = (from t1 in dbContext.planner_event
                                     from t2 in dbContext.planner_task_event
                                     where t1.event_id == t2.event_id
                                     && t1.task_id == itemEdit.task_id
                                     && t1.exec_user_id == itemEdit.exec_user_id
                                     && t2.is_actual
                                     select t1)
                                     .FirstOrDefault();
                    if (planner_event == null)
                    {
                        planner_event = (from t1 in dbContext.planner_event
                                         from t2 in dbContext.planner_task_event
                                         where t1.event_id == t2.event_id
                                         && t1.task_id == itemEdit.task_id
                                         //&& t1.exec_user_id == itemEdit.exec_user_id                                         
                                         && t2.is_actual
                                         select t1)
                                         .FirstOrDefault();
                    }
                    if (planner_event == null)
                    {
                        planner_event = (from t1 in dbContext.planner_event
                                         from t2 in dbContext.planner_task_event
                                         where t1.event_id == t2.event_id
                                         && t1.task_id == itemEdit.task_id
                                         //&& t1.exec_user_id == itemEdit.exec_user_id                                         
                                         //&& t2.is_actual
                                         select t1)
                                         .FirstOrDefault();
                    }
                    if (planner_event != null)
                    {
                        if (cab_user_list == null)
                            cab_user_list = dbContext.cab_user.ToList();
                        cab_user = cab_user_list.Where(ss => ss.user_id == planner_event.exec_user_id).FirstOrDefault();
                        mess_log = "Смена дат в календаре для " + cab_user.user_name + " с " + planner_event.date_beg.ToString("dd.MM.yyyy") + (planner_event.date_beg != planner_event.date_end ? (" - " + planner_event.date_end.ToString("dd.MM.yyyy")) : "")
                            + " на "
                            + event_date_beg.ToString("dd.MM.yyyy") + (event_date_beg != event_date_end ? (" - " + event_date_end.ToString("dd.MM.yyyy")) : "")
                            ;
                        log_mess_list1.Add(mess_log);

                        planner_event.date_beg = event_date_beg;
                        planner_event.date_end = event_date_end;
                    }
                }
            }

            if ((!crm_task.is_control) && (itemEdit.is_control))
            {
                crm_task_control = new crm_task_control();
                crm_task_control.crm_task = crm_task;
                crm_task_control.is_orig = true;
                crm_task_control.required_date = itemEdit.required_date;
                crm_task_control.repair_date = itemEdit.repair_date;
                crm_task_control.event_date_beg = itemEdit.is_event ? (DateTime?)event_date_beg : null;
                crm_task_control.event_date_end = itemEdit.is_event ? (DateTime?)event_date_end : null;
                crm_task_control.crt_date = now;
                crm_task_control.crt_user = currUser.UserName;
                dbContext.crm_task_control.Add(crm_task_control);
                crm_task_control = new crm_task_control();
                crm_task_control.crm_task = crm_task;
                crm_task_control.is_orig = false;
                crm_task_control.required_date = itemEdit.required_date;
                crm_task_control.repair_date = itemEdit.repair_date;
                crm_task_control.event_date_beg = itemEdit.is_event ? (DateTime?)event_date_beg : null;
                crm_task_control.event_date_end = itemEdit.is_event ? (DateTime?)event_date_end : null;
                crm_task_control.crt_date = now;
                crm_task_control.crt_user = currUser.UserName;
                dbContext.crm_task_control.Add(crm_task_control);
            }
            else if ((crm_task.is_control) && (!itemEdit.is_control))
            {
                dbContext.crm_task_control.RemoveRange(dbContext.crm_task_control.Where(ss => ss.task_id == itemEdit.task_id));
            }
            else if ((crm_task.is_control) && (itemEdit.is_control))
            {
                if ((crm_task.required_date != itemEdit.required_date) || (crm_task.repair_date != itemEdit.repair_date) 
                    || (vw_crm_task.event_date_beg != itemEdit.event_date_beg) || (vw_crm_task.event_date_end != itemEdit.event_date_end)
                    || ((vw_crm_task.is_event) && (!itemEdit.is_event))
                    )
                {
                    crm_task_control = dbContext.crm_task_control.Where(ss => ss.task_id == itemEdit.task_id && !ss.is_orig).FirstOrDefault();
                    if (crm_task_control != null)
                    {
                        if ((vw_crm_task.is_event) && (!itemEdit.is_event))
                        {
                            crm_task_control.event_date_beg = null;
                            crm_task_control.event_date_end = null;
                        }
                        else if ((vw_crm_task.event_date_beg != itemEdit.event_date_beg) || (vw_crm_task.event_date_end != itemEdit.event_date_end))
                        {
                            crm_task_control.event_date_beg = itemEdit.event_date_beg;
                            crm_task_control.event_date_end = itemEdit.event_date_end;
                        }

                        if (crm_task.repair_date != itemEdit.repair_date)
                        {
                            crm_task_control.repair_date = itemEdit.repair_date;
                        }

                        if (crm_task.required_date != itemEdit.required_date)
                        {
                            crm_task_control.required_date = itemEdit.required_date;
                        }

                        crm_task_control.crt_date = now;
                        crm_task_control.crt_user = currUser.UserName;
                    }
                }
            }

            if (crm_task.task_name != itemEdit.task_name)
            {
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.task_name))
                {
                    mess_notify_detail += "Смена заголовка задачи";
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add("Смена заголовка с [" + crm_task.task_name + "] на [" + itemEdit.task_name + "]");
                crm_task.task_name = itemEdit.task_name;                
            }
            if (crm_task.task_text != itemEdit.task_text)
            {
                mess_log = "Смена текста задачи";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.task_text))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.task_text = itemEdit.task_text;                
            }
            if (crm_task.client_id != itemEdit.client_id)
            {
                if (crm_client == null)
                    crm_client = dbContext.crm_client.Where(ss => ss.client_id == itemEdit.client_id).FirstOrDefault();
                mess_log = "Смена клиента с [" + vw_crm_task.client_name.ToString() + "] на [" + (crm_client != null ? crm_client.client_name.ToString() : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.client_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.client_id = itemEdit.client_id;                
            }

            if (crm_task.exec_user_id != itemEdit.exec_user_id)
            {
                if (cab_user_list == null)
                    cab_user_list = dbContext.cab_user.ToList();
                cab_user = cab_user_list.Where(ss => ss.user_id == itemEdit.exec_user_id).FirstOrDefault();
                mess_log = "Смена исполнителя с [" + vw_crm_task.exec_user_name.ToString() + "] на [" + (cab_user != null ? cab_user.user_name : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.exec_user_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.exec_user_id = itemEdit.exec_user_id;
            }
            if (crm_task.module_id != itemEdit.module_id)
            {
                if (crm_module == null)
                    crm_module = dbContext.crm_module.Where(ss => ss.module_id == itemEdit.module_id).FirstOrDefault();
                mess_log = "Смена модуля с [" + vw_crm_task.module_name.ToString() + "] на [" + (crm_module != null ? crm_module.module_name : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.module_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.module_id = itemEdit.module_id;
            }
            if (crm_task.module_version_id != itemEdit.module_version_id)
            {
                if (crm_module_version_list == null)
                    crm_module_version_list = dbContext.crm_module_version.ToList();
                crm_module_version = crm_module_version_list.Where(ss => ss.module_version_id == itemEdit.module_version_id).FirstOrDefault();
                mess_log = "Смена версии модуля с [" + vw_crm_task.module_version_name.ToString() + "] на [" + (crm_module_version != null ? crm_module_version.module_version_name : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.module_version_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.module_version_id = itemEdit.module_version_id;
            }
            if (crm_task.priority_id != itemEdit.priority_id)
            {
                if (crm_priority == null)
                    crm_priority = dbContext.crm_priority.Where(ss => ss.priority_id == itemEdit.priority_id).FirstOrDefault();
                mess_log = "Смена приоритета с [" + vw_crm_task.priority_name.ToString() + "] на [" + (crm_priority != null ? crm_priority.priority_name : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.priority_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.priority_id = itemEdit.priority_id;                
            }
            if (crm_task.project_id != itemEdit.project_id)
            {
                if (crm_project == null)
                    crm_project = dbContext.crm_project.Where(ss => ss.project_id == itemEdit.priority_id).FirstOrDefault();
                mess_log = "Смена программы с [" + vw_crm_task.project_name.ToString() + "] на [" + (crm_project != null ? crm_project.project_name : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.project_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.project_id = itemEdit.project_id;                
            }
            if (crm_task.repair_date != itemEdit.repair_date)
            {
                mess_log = "Смена даты исполнения с [" + crm_task.repair_date.ToString() + "] на [" + (itemEdit.repair_date.HasValue ? ((DateTime)itemEdit.repair_date).ToString("dd.MM.yyyy") : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.repair_date))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.repair_date = itemEdit.repair_date;
            }
            if (crm_task.required_date != itemEdit.required_date)
            {
                mess_log = "Смена плановой даты с [" + crm_task.required_date.ToString() + "] на [" + (itemEdit.required_date.HasValue ? ((DateTime)itemEdit.required_date).ToString("dd.MM.yyyy") : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.required_date))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.required_date = itemEdit.required_date;
            }
            if (crm_task.state_id != itemEdit.state_id)
            {
                if (crm_state_list == null)
                    crm_state_list = dbContext.crm_state.ToList();
                crm_state = crm_state_list.Where(ss => ss.state_id == itemEdit.state_id).FirstOrDefault();
                mess_log = "Смена статуса с [" + vw_crm_task.state_name.ToString() + "] на [" + (crm_state != null ? crm_state.state_name : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.state_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.state_id = itemEdit.state_id;                
            }
            if (crm_task.assigned_user_id != itemEdit.assigned_user_id)
            {
                if (cab_user_list == null)
                    cab_user_list = dbContext.cab_user.ToList();
                cab_user = cab_user_list.Where(ss => ss.user_id == itemEdit.assigned_user_id).FirstOrDefault();
                mess_log = "Смена ответственного с [" + vw_crm_task.assigned_user_name.ToString() + "] на [" + (cab_user != null ? cab_user.user_name : "-" ) + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.assigned_user_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.assigned_user_id = itemEdit.assigned_user_id;
            }
            if (crm_task.group_id != itemEdit.group_id)
            {
                if (crm_group == null)
                    crm_group = dbContext.crm_group.Where(ss => ss.group_id == itemEdit.group_id).FirstOrDefault();
                mess_log = "Смена группы с [" + vw_crm_task.group_name.ToString() + "] на [" + (crm_group != null ? crm_group.group_name : "-") + "]";
                //if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.SYS_group_id))
                if (1 == 1)
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.group_id = itemEdit.group_id;
            }
            if (crm_task.repair_version_id != itemEdit.repair_version_id)
            {
                if (crm_module_version_list == null)
                    crm_module_version_list = dbContext.crm_module_version.ToList();
                crm_module_version = crm_module_version_list.Where(ss => ss.module_version_id == itemEdit.repair_version_id).FirstOrDefault();
                mess_log = "Смена версии исправления с [" + (String.IsNullOrEmpty(vw_crm_task.repair_version_name) ? "-" : vw_crm_task.repair_version_name.ToString()) + "] на [" + (crm_module_version != null ? crm_module_version.module_version_name : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.repair_version_name))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.repair_version_id = itemEdit.repair_version_id;
            }
            if (crm_task.fin_cost.GetValueOrDefault(0) != itemEdit.fin_cost.GetValueOrDefault(0))
            {
                mess_log = "Смена цены с [" + vw_crm_task.fin_cost.ToString() + "] на [" + itemEdit.fin_cost.ToString() + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.fin_cost))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.fin_cost = itemEdit.fin_cost.GetValueOrDefault(0) > 0 ? itemEdit.fin_cost : null;
                crm_task.is_fin = itemEdit.fin_cost.GetValueOrDefault(0) > 0 ? true : false;
            }

            bool is_storage_folder_changed = false;
            int? storage_folder_id_old = null;
            if (crm_task.storage_folder_id != itemEdit.storage_folder_id)
            {
                is_storage_folder_changed = true;
                storage_folder_id_old = crm_task.storage_folder_id;
                string storage_folder_name_new = "-";
                if (itemEdit.storage_folder_id.GetValueOrDefault(0) > 0)
                    storage_folder_name_new = dbContext.storage_folder.FirstOrDefault(ss => ss.folder_id == itemEdit.storage_folder_id).folder_name;
                mess_log = "Смена папки хранилища с [" + (String.IsNullOrEmpty(vw_crm_task.storage_folder_name) ? "-" : vw_crm_task.storage_folder_name)
                    + "] на [" + storage_folder_name_new + "]";
                log_mess_list1.Add(mess_log);
                crm_task.storage_folder_id = itemEdit.storage_folder_id;
            }

            if (crm_task.batch_id != itemEdit.batch_id)
            {
                if (crm_batch == null)
                    crm_batch = dbContext.crm_batch.Where(ss => ss.batch_id == itemEdit.batch_id).FirstOrDefault();
                mess_log = "Смена проекта с [" + vw_crm_task.batch_name.ToString() + "] на [" + (crm_batch != null ? crm_batch.batch_name : "-") + "]";
                //if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.SYS_group_id))
                if (1 == 1)
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.batch_id = itemEdit.batch_id;
            }

            if ((crm_task.moderator_user_id <= 0) && (crm_task.moderator_user_id != itemEdit.moderator_user_id))
            {
                mess_log = "Поставлена модерация";
                //if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.SYS_moderator_user_id))
                if (1 == 1)
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.moderator_user_id = itemEdit.moderator_user_id;
            }
            else if ((crm_task.moderator_user_id > 0) && (itemEdit.moderator_user_id <= 0))
            {
                mess_log = "Убрана модерация";
                //if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.SYS_moderator_user_id))
                if (1 == 1)
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.moderator_user_id = itemEdit.moderator_user_id;
            }

            if (crm_task.is_control != itemEdit.is_control)
            {
                mess_log = itemEdit.is_control ? "Поставлен контроль дат" : "Убран контроль дат";
                //if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.SYS_moderator_user_id))
                if (1 == 1)
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.is_control = itemEdit.is_control;
            }

            if (crm_task.cost_plan != itemEdit.cost_plan)
            {
                mess_log = "Смена ТЗ план с [" + (crm_task.cost_plan.HasValue ? crm_task.cost_plan.ToString() : "-")
                    + "] на [" + (itemEdit.cost_plan.HasValue ? itemEdit.cost_plan.ToString() : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.cost_plan))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.cost_plan = itemEdit.cost_plan;
            }

            if (crm_task.cost_fact != itemEdit.cost_fact)
            {
                mess_log = "Смена ТЗ факт с [" + (crm_task.cost_fact.HasValue ? crm_task.cost_fact.ToString() : "-")
                    + "] на [" + (itemEdit.cost_fact.HasValue ? itemEdit.cost_fact.ToString() : "-") + "]";
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.cost_fact))
                {
                    mess_notify_detail += mess_log;
                    mess_notify_detail += System.Environment.NewLine;
                }
                log_mess_list1.Add(mess_log);
                crm_task.cost_fact = itemEdit.cost_fact;
            }

            crm_task.upd_date = now;
            crm_task.upd_num = crm_task.upd_num + 1;
            crm_task.upd_user = currUser.CabUserName;
            crm_task.module_part_id = 0;            

            bool notesChanged = false;
            bool notesAdded = false;
            if (itemEdit.notes != null)
            {
                var notes_existing = dbContext.crm_task_note.Where(ss => ss.task_id == crm_task.task_id).ToList();
                foreach (var note in itemEdit.notes.Where(ss => ss.note_text != null).Reverse())
                {
                    note.note_text = GlobalUtil.Linkify(note.note_text);
                    crm_task_note note_existing = null;
                    if (notes_existing != null)
                    {
                        note_existing = notes_existing.Where(ss => ss.note_id == note.note_id).FirstOrDefault();
                    }

                    if (note_existing != null)
                    {
                        note_existing.file_name = note.file_name;
                        if (note_existing.note_text != note.note_text)
                        {
                            mess_log = "Изменение комментария № " + note_existing.note_id.ToString();
                            log_mess_list2.Add(mess_log);
                            note_existing.note_text = note.note_text;
                            notesChanged = true;
                        }
                    }
                    else
                    {
                        crm_task_note crm_task_note = new crm_task_note();
                        crm_task_note.crt_date = now;
                        crm_task_note.file_name = note.file_name;
                        crm_task_note.note_text = note.note_text;
                        crm_task_note.owner_user_id = note.owner_user_id;
                        crm_task_note.subtask_id = null;
                        crm_task_note.crm_task = crm_task;
                        dbContext.crm_task_note.Add(crm_task_note);
                        mess_log = "Добавление комментария № " + crm_task_note.note_id.ToString();
                        log_mess_list2.Add(mess_log);
                        notesAdded = true;
                    }
                }
            }
            if (notesChanged)
            {
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.notes_text))                
                {
                    mess_notify_detail += "Изменение комментариев";
                    mess_notify_detail += System.Environment.NewLine;
                }
            }
            if (notesAdded)
            {
                if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.notes_text))
                {
                    mess_notify_detail += "Добавление новых комментариев";
                    mess_notify_detail += System.Environment.NewLine;
                }
            }
            
            var subtask_max = dbContext.crm_subtask.Where(ss => ss.task_id == crm_task.task_id).OrderByDescending(ss => ss.task_num).FirstOrDefault();
            var subtask_max_num = subtask_max == null ? 0 : subtask_max.task_num;

            if (itemEdit.subtasks != null)
            {
                var subtasks_existing = dbContext.crm_subtask.Where(ss => ss.task_id == crm_task.task_id).ToList();
                foreach (var subtask in itemEdit.subtasks.Where(ss => ss.IsNew).Reverse())
                {
                    subtask.subtask_text = GlobalUtil.Linkify(subtask.subtask_text);
                    crm_subtask subtask_existing = null;
                    if (subtasks_existing != null)
                    {
                        subtask_existing = subtasks_existing.Where(ss => ss.subtask_id == subtask.subtask_id).FirstOrDefault();
                    }

                    if (subtask_existing != null)
                    {
                        if (subtask_existing.subtask_text != subtask.subtask_text)
                        {
                            mess_log = "Смена текста подзадачи № " + subtask_existing.task_num.ToString();
                            if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.subtasks_text))                            
                            {
                                mess_notify_detail += mess_log;
                                mess_notify_detail += System.Environment.NewLine;                                
                            }
                            log_mess_list3.Add(mess_log);
                            subtask_existing.subtask_text = subtask.subtask_text;
                            subtask_existing.upd_date = now;
                        }

                        var new_state_id = subtask.state_id;
                        if (subtask_existing.state_id != new_state_id)
                        {
                            if (crm_state_list == null)
                                crm_state_list = dbContext.crm_state.ToList();
                            mess_log = "Смена статуса подзадачи № " + subtask_existing.task_num.ToString()
                                + " с [" + crm_state_list.Where(ss => ss.state_id == subtask_existing.state_id).FirstOrDefault().state_name
                                + "] на [" + crm_state_list.Where(ss => ss.state_id == new_state_id).FirstOrDefault().state_name + "]"
                                ;
                            if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.subtasks_text))
                            {
                                mess_notify_detail += mess_log;
                                mess_notify_detail += System.Environment.NewLine;
                            }
                            log_mess_list3.Add(mess_log);
                            subtask_existing.state_id = new_state_id;
                            subtask_existing.upd_date = now;
                        }

                        if (subtask_existing.file_id != subtask.file_id)
                        {
                            mess_log = "Смена файла подзадачи № " + subtask_existing.task_num.ToString()
                                + " c [" + (subtask_existing.file_id.HasValue ? subtask_existing.file_id.ToString() : "-") + "]"
                                + " на [" + (subtask.file_id.HasValue ? subtask.file_id.ToString() : "-") + "]"
                                ;
                            if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.subtasks_text))                            
                            {
                                mess_notify_detail += mess_log;
                                mess_notify_detail += System.Environment.NewLine;
                            }
                            log_mess_list3.Add(mess_log);
                            subtask_existing.file_id = subtask.file_id;
                            subtask_existing.upd_date = now;
                        }

                        /*
                        var new_exec_user_id = subtask.exec_user_id;
                        if (subtask_existing.exec_user_id != new_exec_user_id)
                        {
                            if (cab_user_list == null)
                                cab_user_list = dbContext.cab_user.ToList();
                            mess_log = "Смена исполнителя подзадачи № " + subtask_existing.task_num.ToString()
                                + " с [" + cab_user_list.Where(ss => ss.user_id == subtask_existing.exec_user_id).FirstOrDefault().user_name
                                + "] на [" + cab_user_list.Where(ss => ss.user_id == new_exec_user_id).FirstOrDefault().user_name + "]"
                                ;
                            mess_notify_detail += mess_log;
                            mess_notify_detail += System.Environment.NewLine;
                            log_mess_list3.Add(mess_log);
                            subtask_existing.exec_user_id = new_exec_user_id;
                            subtask_existing.upd_date = now;
                        }
                        */
                    }
                    else
                    {
                        if (crm_state_list == null)
                            crm_state_list = dbContext.crm_state.ToList();

                        subtask_max_num += 1;
                        //
                        crm_subtask crm_subtask = new crm_subtask();
                        crm_subtask.crt_date = now;
                        crm_subtask.owner_user_id = currUser.CabUserId;
                        crm_subtask.subtask_text = subtask.subtask_text;
                        crm_subtask.state_id = subtask.state_id;
                        crm_subtask.task_num = subtask_max_num;
                        crm_subtask.upd_date = null;
                        crm_subtask.file_id = subtask.file_id;
                        crm_subtask.exec_user_id = crm_subtask.owner_user_id;
                        crm_subtask.crm_task = crm_task;
                        dbContext.crm_subtask.Add(crm_subtask);
                        mess_log = "Добавление подзадачи № " + crm_subtask.task_num.ToString();
                        if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.subtasks_text))                        
                        {
                            mess_notify_detail += mess_log;
                            mess_notify_detail += System.Environment.NewLine;
                        }
                        log_mess_list3.Add(mess_log);
                        //                            
                    }
                }
            }

            var storageFolderService = DependencyResolver.Current.GetService<IStorageFolderService>();
            if (is_storage_folder_changed)
            {
                if (itemEdit.storage_folder_id.GetValueOrDefault(0) > 0)
                    storageFolderService.AddTaskToFolder(itemEdit.task_id, itemEdit.task_num, (int)itemEdit.storage_folder_id, storage_folder_id_old, modelState);
                else
                    storageFolderService.RemoveTaskFromFolder(itemEdit.task_id, itemEdit.task_num, storage_folder_id_old, modelState);
            }

            dbContext.SaveChanges();

            if (log_mess_list1.Count > 0)
            {
                foreach (var log_mess in log_mess_list1)
                {
                    ToLog(now, log_mess, crm_task.task_id);
                }
            }

            if (log_mess_list2.Count > 0)
            {
                foreach (var log_mess in log_mess_list2)
                {
                    ToLog(now, log_mess, crm_task.task_id);
                }
            }

            if (log_mess_list3.Count > 0)
            {
                foreach (var log_mess in log_mess_list3)
                {
                    ToLog(now, log_mess, crm_task.task_id);
                }
            }

            var crm_task_db = dbContext.vw_crm_task.AsNoTracking().Where(ss => ss.task_id == crm_task.task_id && ss.view_user_id == currUser.CabUserId).FirstOrDefault();
            
            if (!String.IsNullOrEmpty(mess_notify_detail))
            {
                /*
                    mess_notify += "Задача " + crm_task.task_num + ": " + crm_task.task_name.CutToLengthWithDots(50);
                    mess_notify += System.Environment.NewLine;
                    mess_notify += mess_notify_detail;
                */

                // notify            
                mess_notify = "Изменена задача:";
                mess_notify += System.Environment.NewLine;
                mess_notify += GetNotifyIcqContent(crm_task_db);
                mess_notify += "Изменения:";
                mess_notify += System.Environment.NewLine;
                mess_notify += mess_notify_detail;

                mess_notify += "Автор изменений: " + currUser.CabUserName;
                mess_notify += System.Environment.NewLine;
                mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
                mess_notify += System.Environment.NewLine;

                List<int> notifyTargetList = dbContext.vw_crm_notify_param_user.Where(ss =>
                    ((ss.changer_user_id == crm_task.assigned_user_id) && (ss.is_owner == true) && (ss.user_id != currUser.CabUserId))
                    ||
                    ((ss.changer_user_id == crm_task.exec_user_id) && (ss.is_executer == true) && (ss.user_id != currUser.CabUserId))
                    )
                    .Select(ss => ss.user_id)
                    .Distinct()
                    .ToList();
                if ((notifyTargetList != null) && (notifyTargetList.Count > 0))
                {
                    foreach (var notifyTarget in notifyTargetList)
                    {
                        notify notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                        dbContext.notify.Add(notify);
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.EMAIL;
                        dbContext.notify.Add(notify);
                    }

                    dbContext.SaveChanges();
                }
            }

            return new CrmTaskViewModel(crm_task_db);
        }

        public Tuple<bool, string, DateTime?> TaskSaveBefore(int task_id, int task_upd_num)
        {
            var upd_last = dbContext.crm_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
            
            // не хватает upd_user
            if ((upd_last != null) && (upd_last.upd_num != task_upd_num))
                return new Tuple<bool, string, DateTime?>(false, upd_last.upd_user, (DateTime)(upd_last.upd_date));

            return new Tuple<bool, string, DateTime?>(true, "", null);
        }

        #endregion

        #region TaskOperationExecute

        public bool TaskOperationExecute(string task_id_list, int operation_id, int? batch_id, ModelStateDictionary modelState)
        {
            if (String.IsNullOrEmpty(task_id_list))
            {
                modelState.AddModelError("", "Не указан список задач");
                return false;
            }

            Enums.CrmTaskOperation crmOp = Enums.CrmTaskOperation.TO_FOLLOW;
            var parseRes = Enum.TryParse<Enums.CrmTaskOperation>(operation_id.ToString(), out crmOp);
            if (!parseRes)
            {
                modelState.AddModelError("", "Неизвестный тип операции");
                return false;
            }

            if ((crmOp == Enums.CrmTaskOperation.TO_BATCH) && (!currUser.IsBoss))
            {
                modelState.AddModelError("", "Нет прав на данную операцию");
                return false;
            }

            DateTime now = DateTime.Now;
            log_crm log_crm = null;
            int old_group_id = 0;
            List<log_crm> log_crm_list = new List<log_crm>();
            string mess = "";
            string mess_notify = "";
            string mess_notify_detail = "";
            string mess_notify_detail_group = "";
            var cab_user_id = currUser.CabUserId;
            List<crm_batch> crm_batch_list = null;

            string task_id_list_formatted = "";
            List<int> task_id_list_int = new List<int>();
            try
            {
                task_id_list_formatted = task_id_list.EndsWith(",") ? (task_id_list.Remove(task_id_list.Length - 1)) : task_id_list;
                task_id_list_int.AddRange(task_id_list_formatted.Split(',').Select(int.Parse).AsEnumerable());
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }

            var tasks = dbContext.crm_task.Where(ss => task_id_list_int.Contains(ss.task_id)).ToList();
            var followed_tasks = (from t1 in dbContext.crm_task
                                  from t2 in dbContext.crm_user_task_follow
                                  where t1.task_id == t2.task_id
                                  && t2.user_id == cab_user_id
                                  select t2)
                                 .ToList();

            if ((tasks != null) && (tasks.Count > 0))
            {
                foreach (var task in tasks)
                {
                    switch (crmOp)
                    {
                        case Enums.CrmTaskOperation.TO_BUCKET:
                        case Enums.CrmTaskOperation.TO_PREPARED:
                        case Enums.CrmTaskOperation.TO_FIXED:
                        case Enums.CrmTaskOperation.TO_APPROVED:
                        case Enums.CrmTaskOperation.TO_CLOSED:
                        case Enums.CrmTaskOperation.TO_VERSION:
                            old_group_id = task.group_id;

                            task.group_id = operation_id;
                            task.upd_date = now;
                            task.upd_num = task.upd_num + 1;
                            task.upd_user = currUser.CabUserName;

                            string mess2 = "";
                            if (task.is_archive)
                            {
                                task.is_archive = false;
                                mess2 = " [Архив]";
                            }
                            mess_notify_detail_group = " в [" + operation_id.EnumName<Enums.CrmGroup>() + "]";
                            mess = "Перенос из [" + old_group_id.EnumName<Enums.CrmGroup>() + "]" + mess2 + " в [" + operation_id.EnumName<Enums.CrmGroup>() + "]";
                            switch (crmOp)
                            {
                                case Enums.CrmTaskOperation.TO_BUCKET:
                                    //task.prepared_user_id = 0;
                                    //task.fixed_user_id = 0;
                                    //task.approved_user_id = 0;                                    
                                    break;
                                case Enums.CrmTaskOperation.TO_PREPARED:
                                    task.prepared_user_id = currUser.CabUserId;
                                    //task.fixed_user_id = 0;
                                    //task.approved_user_id = 0;
                                    break;
                                case Enums.CrmTaskOperation.TO_FIXED:
                                    task.fixed_user_id = currUser.CabUserId;
                                    //task.approved_user_id = 0;
                                    break;
                                case Enums.CrmTaskOperation.TO_APPROVED:
                                    task.approved_user_id = currUser.CabUserId;
                                    break;
                                case Enums.CrmTaskOperation.TO_CLOSED:
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case Enums.CrmTaskOperation.TO_ARCHIVE:
                            task.is_archive = true;
                            task.upd_date = now;

                            dbContext.planner_task_event.RemoveRange(dbContext.planner_task_event.Where(ss => ss.task_id == task.task_id));
                            dbContext.planner_event.RemoveRange(dbContext.planner_event.Where(ss => ss.task_id == task.task_id));

                            mess_notify_detail_group = " в [Архив]";
                            mess = "Перенос из [" + task.group_id.EnumName<Enums.CrmGroup>() + "] в [Архив]";
                            break;
                        case Enums.CrmTaskOperation.FROM_ARCHIVE:
                            task.is_archive = false;
                            task.upd_date = now;

                            mess_notify_detail_group = " из [Архива]";
                            mess = "Перенос из [Архив] в [" + task.group_id.EnumName<Enums.CrmGroup>() + "]";
                            break;
                        case Enums.CrmTaskOperation.TO_FOLLOW:
                            if ((followed_tasks != null) && (followed_tasks.Count > 0))
                            {
                                var existing_followed_task = followed_tasks.Where(ss => ss.task_id == task.task_id).FirstOrDefault();
                                if (existing_followed_task != null)
                                    continue;
                            }

                            crm_user_task_follow crm_user_task_follow = new crm_user_task_follow();
                            crm_user_task_follow.task_id = task.task_id;
                            crm_user_task_follow.user_id = cab_user_id;
                            dbContext.crm_user_task_follow.Add(crm_user_task_follow);

                            mess = "Установлен контроль";

                            break;
                        case Enums.CrmTaskOperation.TO_UNFOLLOW:
                            if ((followed_tasks != null) && (followed_tasks.Count > 0))
                            {
                                var existing_followed_task = followed_tasks.Where(ss => ss.task_id == task.task_id).FirstOrDefault();
                                if (existing_followed_task == null)
                                    continue;
                                dbContext.crm_user_task_follow.Remove(existing_followed_task);

                                mess = "Снят контроль";
                            }
                            break;
                        case Enums.CrmTaskOperation.TO_BATCH:                            
                            if (crm_batch_list == null)
                                crm_batch_list = dbContext.crm_batch.ToList();
                            var batch_name_prev = crm_batch_list.Where(ss => ss.batch_id == task.batch_id).Select(ss => ss.batch_name).FirstOrDefault();
                            var batch_name = crm_batch_list.Where(ss => ss.batch_id == batch_id).Select(ss => ss.batch_name).FirstOrDefault();

                            task.batch_id = batch_id.GetValueOrDefault(0);
                            task.upd_date = now;
                            
                            mess_notify_detail_group = " в [Проект]";
                            mess = "Перенос из [" + batch_name_prev + "] в [" + batch_name + "]";
                            break;
                        default:
                            break;
                    }
                    //
                    log_crm = new log_crm();
                    log_crm.date_beg = now;
                    log_crm.date_end = now;
                    log_crm.mess = mess;
                    log_crm.task_id = task.task_id;
                    log_crm.user_id = cab_user_id;
                    log_crm_list.Add(log_crm);
                }

                //mess_notify_detail += ("Перенос задач " + "").CutToLengthWithDots(200) + mess_notify_detail_group;

                dbContext.log_crm.AddRange(log_crm_list);
                dbContext.SaveChanges();
            }

            return true;
        }

        #endregion

        #region TaskRel

        public bool TaskRelAdd(int parent_task_id, string child_task_num, int rel_type_id, ModelStateDictionary modelState)
        {
            if (parent_task_id <= 0)
            {
                modelState.AddModelError("", "Не задана родительская задача");
                return false;
            }

            if (String.IsNullOrEmpty(child_task_num))
            {
                modelState.AddModelError("", "Не задана дочерняя задача");
                return false;
            }

            if (rel_type_id <= 0)
            {
                modelState.AddModelError("", "Не задан тип связи");
                return false;
            }

            var parent_task = dbContext.crm_task.Where(ss => ss.task_id == parent_task_id).FirstOrDefault();
            if (parent_task == null)
            {
                modelState.AddModelError("", "Не найдена родительская задача");
                return false;
            }

            var vw_child_task = dbContext.vw_crm_task.Where(ss => ss.view_user_id == currUser.CabUserId && ss.task_num.ToLower() == child_task_num.ToLower()).FirstOrDefault();
            if (vw_child_task == null)
            {
                modelState.AddModelError("", "Не найдена дочерняя задача");
                return false;
            }
            var child_task = dbContext.crm_task.Where(ss => ss.task_id == vw_child_task.task_id).FirstOrDefault();

            if (parent_task.task_id == child_task.task_id)
            {
                modelState.AddModelError("", "Нельзя связать задачу саму с собой");
                return false;
            }

            var existing_rel = dbContext.crm_task_rel.Where(ss => ss.task_id == parent_task.task_id && ss.child_task_id == child_task.task_id).FirstOrDefault();
            if (existing_rel != null)
            {
                modelState.AddModelError("", "Уже есть связь между данными задачами");
                return false;
            }

            crm_task_rel crm_task_rel = new crm_task_rel();
            crm_task_rel.child_task_id = child_task.task_id;
            crm_task_rel.rel_type_id = rel_type_id;
            crm_task_rel.task_id = parent_task.task_id;
            dbContext.crm_task_rel.Add(crm_task_rel);

            parent_task.rel_child_cnt = parent_task.rel_child_cnt + 1;
            child_task.rel_parent_cnt = child_task.rel_parent_cnt + 1;

            dbContext.SaveChanges();

            return true;
        }

        public bool TaskRelDel(int parent_task_id, int child_task_id, int rel_type_id, ModelStateDictionary modelState)
        {
            if (parent_task_id <= 0)
            {
                modelState.AddModelError("", "Не задана родительская задача");
                return false;
            }

            if (child_task_id <= 0)
            {
                modelState.AddModelError("", "Не задана дочерняя задача");
                return false;
            }

            if (rel_type_id <= 0)
            {
                modelState.AddModelError("", "Не задан тип связи");
                return false;
            }

            var parent_task = dbContext.crm_task.Where(ss => ss.task_id == parent_task_id).FirstOrDefault();
            if (parent_task == null)
            {
                modelState.AddModelError("", "Не найдена родительская задача");
                return false;
            }

            var child_task = dbContext.crm_task.Where(ss => ss.task_id == child_task_id).FirstOrDefault();
            if (child_task == null)
            {
                modelState.AddModelError("", "Не найдена дочерняя задача");
                return false;
            }

            var existing_rel = dbContext.crm_task_rel.Where(ss => ss.task_id == parent_task_id && ss.child_task_id == child_task_id && ss.rel_type_id == rel_type_id).FirstOrDefault();
            if (existing_rel == null)
            {
                modelState.AddModelError("", "Не найдена связь между данными задачами");
                return false;
            }

            dbContext.crm_task_rel.Remove(existing_rel);

            parent_task.rel_child_cnt = parent_task.rel_child_cnt - 1;
            child_task.rel_parent_cnt = child_task.rel_parent_cnt - 1;
            if (parent_task.rel_child_cnt < 0)
                parent_task.rel_child_cnt = 0;
            if (child_task.rel_parent_cnt < 0)
                child_task.rel_parent_cnt = 0;

            dbContext.SaveChanges();

            return true;
        }

        #endregion

        #region Story

        public AuBaseViewModel Insert_Story(long story_id, ModelStateDictionary modelState)
        {
            story story = dbContext.story.Where(ss => ss.story_id == story_id).FirstOrDefault();
            if (story == null)
            {
                modelState.AddModelError("", "Не найден вопрос с кодом " + story_id.ToString());
                return null;
            }
            crm_client crm_client = dbContext.crm_client.Where(ss => ss.client_name.Trim().ToLower().Equals(story.org_name.Trim().ToLower())).FirstOrDefault();
            if (crm_client == null)
            {
                crm_client = new crm_client();
                crm_client.client_name = story.org_name.Trim();
                crm_client.client_id = dbContext.crm_client.Max(ss => ss.client_id) + 1;
                dbContext.crm_client.Add(crm_client);
                dbContext.SaveChanges();
            }

            CrmTaskViewModel taskAdd = new CrmTaskViewModel()
            {
                task_name = story.story_text.CutToLengthWithDots(50),
                task_text = story.story_text,
                client_id = crm_client.client_id,
                owner_user_id = currUser.CabUserId,
                is_archive = false,
                is_event = false,
            };


            return xInsert(taskAdd, modelState);
        }

        #endregion

        #region Log

        private void ToLog(DateTime date_beg, string mess, int? task_id)
        {
            if (logService == null)
                logService = DependencyResolver.Current.GetService<ICrmLogService>();
            if (logService == null)
                throw new Exception("logService == null");
            //
            logService.InsertLogCrm(date_beg, mess, task_id);
        }
        
        #endregion

        #region Notify

        public string GetNotifyIcqContent(vw_crm_task crm_task)
        {
            List<CrmTaskNotifyInfo> notifyList = new List<CrmTaskNotifyInfo>();

            addNotifyIcqContent(notifyList, crm_task);

            if ((notifyList == null) || (notifyList.Count <= 0))
                return "";

            string result = "";
            foreach (var notify in notifyList)
            {
                result += notify.column_name + ": " + notify.value.CutToLengthWithDots(50);
                result += System.Environment.NewLine;
            }
            return result;
        }

        private void addNotifyIcqContent(List<CrmTaskNotifyInfo> notifyList, vw_crm_task crm_task)
        {
            var contentColumns = (from t1 in dbContext.crm_notify
                                  from t2 in dbContext.crm_notify_param_content
                                  from t3 in dbContext.vw_cab_grid_column_user_settings
                                  where t1.notify_id == t2.notify_id
                                  && t2.column_id == t3.column_id
                                  && t1.user_id == t3.user_id
                                  && t1.user_id == currUser.CabUserId
                                  && t1.is_active_icq
                                  && t2.transport_type == (int)Enums.NotifyTransportTypeEnum.ICQ
                                  && t2.is_active                                                  
                                  select t3)
                                  .ToList();
            if ((contentColumns != null) && (contentColumns.Count > 0))
            {
                foreach (var contentColumn in contentColumns.OrderBy(ss => !ss.is_visible).ThenBy(ss => ss.column_index))
                {
                    var taskAttrValue = getCrmTaskAttrValue_byColumnId(crm_task, contentColumn);
                    if (taskAttrValue != null)
                        notifyList.Add(taskAttrValue);
                }
            }
        }

        private CrmTaskNotifyInfo getCrmTaskAttrValue_byColumnId(vw_crm_task crm_task, vw_cab_grid_column_user_settings column)
        {
            Enums.CabGridColumn_TaskList col = Enums.CabGridColumn_TaskList.task_id;
            bool parseOk = Enum.TryParse<Enums.CabGridColumn_TaskList>(column.column_id.ToString(), out col);
            if (!parseOk)
                return null;

            CrmTaskNotifyInfo result = new CrmTaskNotifyInfo(column.column_id, column.column_name_rus, "");

            switch (col)
            {
                #region
                case Enums.CabGridColumn_TaskList.assigned_user_name:
                    result.value = crm_task.assigned_user_name;
                    break;
                case Enums.CabGridColumn_TaskList.client_name:
                    result.value = crm_task.client_name;
                    break;
                case Enums.CabGridColumn_TaskList.crt_date:
                    result.value = crm_task.crt_date.HasValue ? ((DateTime)crm_task.crt_date).ToString("dd.MM.yyyy HH:mm:ss") : "";
                    break;
                case Enums.CabGridColumn_TaskList.exec_user_name:
                    result.value = crm_task.exec_user_name;
                    break;
                case Enums.CabGridColumn_TaskList.fin_cost:
                    result.value = crm_task.fin_cost.HasValue ? crm_task.fin_cost.ToString() : "";
                    break;
                case Enums.CabGridColumn_TaskList.is_fin_name:
                    result.value = crm_task.is_fin_name;
                    break;
                case Enums.CabGridColumn_TaskList.module_name:
                    result.value = crm_task.module_name;
                    break;
                case Enums.CabGridColumn_TaskList.module_version_name:
                    result.value = crm_task.module_version_name;
                    break;
                case Enums.CabGridColumn_TaskList.owner_user_name:
                    result.value = crm_task.owner_user_name;
                    break;
                case Enums.CabGridColumn_TaskList.priority_name:
                    result.value = crm_task.priority_name;
                    break;
                case Enums.CabGridColumn_TaskList.project_name:
                    result.value = crm_task.project_name;
                    break;
                case Enums.CabGridColumn_TaskList.repair_date:
                    result.value = crm_task.repair_date.HasValue ? ((DateTime)crm_task.repair_date).ToString("dd.MM.yyyy") : "";
                    break;
                case Enums.CabGridColumn_TaskList.repair_version_name:
                    result.value = crm_task.repair_version_name;
                    break;
                case Enums.CabGridColumn_TaskList.required_date:
                    result.value = crm_task.required_date.HasValue ? ((DateTime)crm_task.required_date).ToString("dd.MM.yyyy") : "";
                    break;
                case Enums.CabGridColumn_TaskList.state_name:
                    result.value = crm_task.state_name;
                    break;
                case Enums.CabGridColumn_TaskList.task_id:
                    result.value = crm_task.task_id.ToString();
                    break;
                case Enums.CabGridColumn_TaskList.task_name:
                    result.value = crm_task.task_name;
                    break;
                case Enums.CabGridColumn_TaskList.task_num:
                    result.value = crm_task.task_num;
                    break;
                case Enums.CabGridColumn_TaskList.task_text:
                    result.value = crm_task.task_text;
                    break;
                case Enums.CabGridColumn_TaskList.upd_date:
                    result.value = crm_task.upd_date.HasValue ? ((DateTime)crm_task.upd_date).ToString("dd.MM.yyyy HH:mm:ss") : "";
                    break;
                default:
                    result = null;
                    break;
                #endregion
            }

            return result;
        }

        public List<int> GetNotifyIcqWatchedColumns()
        {
            return (from t1 in dbContext.crm_notify
                    from t2 in dbContext.crm_notify_param_attr                                  
                    where t1.notify_id == t2.notify_id
                    && t1.user_id == currUser.CabUserId
                    && t1.is_active_icq
                    && t2.transport_type == (int)Enums.NotifyTransportTypeEnum.ICQ
                    && t2.is_active
                    select t2.column_id)
                    .ToList();
        }

        #endregion        

    }
}
