﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmSubtaskViewModel : AuBaseViewModel
    {
        public CrmSubtaskViewModel()
            : base(Enums.MainObjectType.CRM_SUBTASK)
        {
            //
        }

        public CrmSubtaskViewModel(crm_subtask item)
            : base(Enums.MainObjectType.CRM_SUBTASK)
        {
            subtask_id = item.subtask_id;
            task_id = item.task_id;
            subtask_text = item.subtask_text;
            state_id = item.state_id;
            owner_user_id = item.owner_user_id;           
            crt_date = item.crt_date;            
            repair_date = item.repair_date;
            task_num = item.task_num;
            upd_date = item.upd_date;                
            IsChecked = false;
            IsNew = true;
            file_id = item.file_id;
            exec_user_id = item.exec_user_id;
        }

        [Key()]
        [Display(Name = "Код")]
        public int subtask_id { get; set; }

        [Display(Name = "Главная задача")]
        public int? task_id { get; set; }

        [Display(Name = "Описание")]
        [UIHint("TextArea")]
        [AllowHtml()]
        public string subtask_text { get; set; }

        [Display(Name = "Статус")]
        public int state_id { get; set; }

        [Display(Name = "Статус")]
        public int state_id_ReadOnly { get; set; }

        [Display(Name = "Автор")]
        public int owner_user_id { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Дата исполнения")]
        public Nullable<System.DateTime> repair_date { get; set; }

        [Display(Name = "№")]
        public int task_num { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Исполнитель")]
        public int exec_user_id { get; set; }

        //[Display(Name = "Исполнитель")]
        //public string exec_user_name { get; set; }

        [Display(Name = "Автор")]
        public string owner_user_name { get; set; }

        [Display(Name = "Файл")]
        public Nullable<int> file_id { get; set; }

        [Display(Name = "Файл")]
        public string file_name { get; set; }

        [Display(Name = "Файл")]
        public int file_id_ReadOnly { get; set; }

        [Display(Name = "Исполнитель")]
        public int exec_user_id_ReadOnly { get; set; }

        [Display(Name = "IsChecked")]
        public bool IsChecked { get; set; }

        [Display(Name = "IsNew")]
        public bool IsNew { get; set; }

        //[Display(Name = "State")]
        [JsonIgnore]
        public IEnumerable<CrmStateViewModel> StateList { get; set; }

        [Display(Name = "Статус")]
        public string state_name { get { return (StateList == null ? "StateList == null" : StateList.Where(ss => ss.state_id == this.state_id).Select(ss => ss.state_name).FirstOrDefault()); } }

        [Display(Name = "Статус")]
        public string state_name_db { get; set; }

        [Display(Name = "Статус")]
        [UIHint("StateCombo")]
        public CrmStateViewModel State { get; set; }

        [Display(Name = "Файл")]
        [UIHint("TaskFileCombo")]
        public CrmTaskFileViewModel TaskFile { get; set; }

        [JsonIgnore]
        public IEnumerable<CabUserViewModel> ExecUserList { get; set; }

        [Display(Name = "Исполнитель")]
        public string exec_user_name { get { return (ExecUserList == null ? "ExecUserList == null" : ExecUserList.Where(ss => ss.user_id == this.exec_user_id).Select(ss => ss.user_name).FirstOrDefault()); } }

        public string exec_user_name_db { get; set; }

        [Display(Name = "Исполнитель")]
        [UIHint("ExecUserCombo")]
        public CabUserViewModel ExecUser { get; set; }

    }
}