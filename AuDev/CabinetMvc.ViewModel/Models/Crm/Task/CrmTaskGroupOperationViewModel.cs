﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmTaskGroupOperationViewModel : AuBaseViewModel
    {
        public CrmTaskGroupOperationViewModel()
            : base(Enums.MainObjectType.CRM_TASK_GROUP_OPERATION)
        {
            //
        }

        public CrmTaskGroupOperationViewModel(crm_group_operation item)
            : base(Enums.MainObjectType.CRM_TASK_GROUP_OPERATION)
        {
            item_id = item.item_id;
            group_id = item.group_id;
            operation_id = item.operation_id;
            operation_name = item.operation_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Группа")]
        public int group_id { get; set; }

        [Display(Name = "Код операции")]
        public int operation_id { get; set; }

        [Display(Name = "Операция")]
        public string operation_name { get; set; }
    }
}