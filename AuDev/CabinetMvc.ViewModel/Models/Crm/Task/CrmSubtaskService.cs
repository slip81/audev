﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmSubtaskService : IAuBaseService
    {
        //
    }

    public class CrmSubtaskService : AuBaseService, ICrmSubtaskService
    {
        ICrmLogService logService;        

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_subtask
                .Where(ss => ss.task_id == parent_id)
                .Select(ss => new CrmSubtaskViewModel
                {
                    subtask_id = ss.subtask_id,
                    task_id = ss.task_id,
                    subtask_text = ss.subtask_text,
                    state_id = ss.state_id,
                    owner_user_id = ss.owner_user_id,
                    crt_date = ss.crt_date,
                    repair_date = ss.repair_date,
                    task_num = ss.task_num,
                    upd_date = ss.upd_date,
                    IsChecked = ss.state_id > 0,
                    IsNew = false,
                    owner_user_name = ss.cab_user1.user_name,
                    file_id = ss.file_id,
                    file_name = ss.crm_task_file != null ? ss.crm_task_file.file_name : "",
                    exec_user_id = ss.exec_user_id,
                    exec_user_name_db = ss.cab_user.user_name,
                    ExecUser = new CabUserViewModel () { user_id = ss.exec_user_id, user_name = ss.cab_user.user_name, },
                    state_name_db = ss.crm_state.state_name,
                    State = new CrmStateViewModel() { state_id = ss.state_id, state_name = ss.crm_state.state_name, },
                    TaskFile = new CrmTaskFileViewModel() { file_id = (ss.file_id != null ? (int)ss.file_id : 0), file_name = ss.crm_task_file.file_name },
                });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmSubtaskViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmSubtaskViewModel itemAdd = (CrmSubtaskViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_task crm_task = dbContext.crm_task.Where(ss => ss.task_id == itemAdd.task_id).FirstOrDefault();
            if (crm_task == null)
            {
                modelState.AddModelError("", "Не найдена задача с кодом " + itemAdd.task_id.ToString());
                return null;
            }

            vw_crm_task crm_task_db = dbContext.vw_crm_task.Where(ss => ss.task_id == itemAdd.task_id && ss.view_user_id == currUser.CabUserId).FirstOrDefault();

            itemAdd.subtask_text = GlobalUtil.Linkify(itemAdd.subtask_text);
            DateTime now = DateTime.Now;
            crm_subtask crm_subtask_last = dbContext.crm_subtask.Where(ss => ss.task_id == itemAdd.task_id).OrderByDescending(ss => ss.task_num).FirstOrDefault();
            int task_num = crm_subtask_last != null ? crm_subtask_last.task_num + 1 : 1;
            crm_subtask crm_subtask = new crm_subtask();
            crm_subtask.crt_date = now;
            crm_subtask.file_id = itemAdd.TaskFile != null ? (int?)itemAdd.TaskFile.file_id : null;
            crm_subtask.owner_user_id = currUser.CabUserId;
            crm_subtask.repair_date = itemAdd.repair_date;
            crm_subtask.state_id = itemAdd.State != null ? itemAdd.State.state_id : 0;
            crm_subtask.subtask_text = itemAdd.subtask_text;
            crm_subtask.task_id = (int)itemAdd.task_id;
            crm_subtask.task_num = task_num;
            crm_subtask.upd_date = null;
            crm_subtask.exec_user_id = itemAdd.ExecUser != null ? itemAdd.ExecUser.user_id : 0;

            dbContext.crm_subtask.Add(crm_subtask);

            crm_task.upd_date = now;
            crm_task.upd_user = currUser.CabUserName;
            crm_task.upd_num = crm_task.upd_num + 1;

            var crmTaskService = DependencyResolver.Current.GetService<ICrmTaskService>();
            List<int> watchedColumns = crmTaskService.GetNotifyIcqWatchedColumns();

            // notify
            string mess_notify = "";
            notify notify = null;

            if (watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.subtasks_text))
            {
                mess_notify = "Изменена задача:";
                mess_notify += System.Environment.NewLine;
                mess_notify += crmTaskService.GetNotifyIcqContent(crm_task_db);
                mess_notify += "Изменения:";
                mess_notify += System.Environment.NewLine;
                mess_notify += "Добавление подзадачи № " + crm_subtask.task_num.ToString();
                mess_notify += System.Environment.NewLine;
                mess_notify += "Автор изменений: " + currUser.CabUserName;
                mess_notify += System.Environment.NewLine;
                mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
                mess_notify += System.Environment.NewLine;

                List<int> notifyTargetList = dbContext.vw_crm_notify_param_user.Where(ss =>
                    ((ss.changer_user_id == crm_task.assigned_user_id) && (ss.is_owner == true) && (ss.user_id != currUser.CabUserId))
                    ||
                    ((ss.changer_user_id == crm_task.exec_user_id) && (ss.is_executer == true) && (ss.user_id != currUser.CabUserId))
                    )
                    .Select(ss => ss.user_id)
                    .Distinct()
                    .ToList();
                if ((notifyTargetList != null) && (notifyTargetList.Count > 0))
                {
                    foreach (var notifyTarget in notifyTargetList)
                    {
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                        dbContext.notify.Add(notify);
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.EMAIL;
                        dbContext.notify.Add(notify);
                    }
                }
            }

            /*
            // уведомление ответственному
            if ((crm_task.assigned_user_id > 0) && (crm_task.assigned_user_id != currUser.CabUserId))
            {
                notify = new notify();
                notify.crt_date = now;
                notify.message = mess_notify;
                notify.message_sent = false;
                notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                notify.sent_date = null;
                notify.target_user_id = crm_task.assigned_user_id;
                notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                dbContext.notify.Add(notify);
            }
            // уведомление исполнителю
            if ((crm_task.exec_user_id > 0) && (crm_task.exec_user_id != crm_task.assigned_user_id) && (crm_task.exec_user_id != currUser.CabUserId))
            {
                notify = new notify();
                notify.crt_date = now;
                notify.message = mess_notify;
                notify.message_sent = false;
                notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                notify.sent_date = null;
                notify.target_user_id = crm_task.exec_user_id;
                notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                dbContext.notify.Add(notify);
            }
            */

            // log
            if (logService == null)
                logService = DependencyResolver.Current.GetService<ICrmLogService>();
            if (logService == null)
                throw new Exception("logService == null");
            logService.InsertLogCrm(now, "Добавление подзадачи № " + crm_subtask.task_num.ToString(), crm_subtask.task_id);

            dbContext.SaveChanges();

            return new CrmSubtaskViewModel(crm_subtask);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmSubtaskViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmSubtaskViewModel itemEdit = (CrmSubtaskViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_subtask = dbContext.crm_subtask.Where(ss => ss.subtask_id == itemEdit.subtask_id).FirstOrDefault();
            if (crm_subtask == null)
            {
                modelState.AddModelError("", "Не найдена подзадача с кодом " + itemEdit.subtask_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmSubtaskViewModel(crm_subtask);

            crm_task crm_task = dbContext.crm_task.Where(ss => ss.task_id == crm_subtask.task_id).FirstOrDefault();
            if (crm_task == null)
            {
                modelState.AddModelError("", "Не найдена задача с кодом " + crm_subtask.task_id.ToString());
                return null;
            }

            vw_crm_task crm_task_db = dbContext.vw_crm_task.Where(ss => ss.task_id == crm_subtask.task_id && ss.view_user_id == currUser.CabUserId).FirstOrDefault();

            itemEdit.subtask_text = GlobalUtil.Linkify(itemEdit.subtask_text);
            string mess_log = "";
            string mess_notify = "";
            string mess_notify_detail = "";
            List<string> log_mess_list3 = new List<string>();
            if (crm_subtask.subtask_text != itemEdit.subtask_text)
            {
                mess_log = "Смена текста подзадачи № " + crm_subtask.task_num.ToString();
                mess_notify_detail += mess_log;
                mess_notify_detail += System.Environment.NewLine;
                log_mess_list3.Add(mess_log);
            }

            var new_state_id = itemEdit.State != null ? itemEdit.State.state_id : 0;
            if (crm_subtask.state_id != new_state_id)
            {
                List<crm_state> crm_state_list = dbContext.crm_state.ToList();
                mess_log = "Смена статуса подзадачи № " + crm_subtask.task_num.ToString()
                    + " с [" + crm_state_list.Where(ss => ss.state_id == crm_subtask.state_id).FirstOrDefault().state_name
                    + "] на [" + crm_state_list.Where(ss => ss.state_id == new_state_id).FirstOrDefault().state_name + "]"
                    ;
                mess_notify_detail += mess_log;
                mess_notify_detail += System.Environment.NewLine;
                log_mess_list3.Add(mess_log);
            }

            var new_file_id = (((itemEdit.TaskFile != null) && (itemEdit.TaskFile.file_id > 0)) ? (int?)itemEdit.TaskFile.file_id : null);
            if (crm_subtask.file_id != new_file_id)
            {
                mess_log = "Смена файла подзадачи № " + crm_subtask.task_num.ToString()
                    + " c [" + (crm_subtask.file_id.HasValue ? crm_subtask.file_id.ToString() : "-") + "]"
                    + " на [" + (new_file_id.HasValue ? new_file_id.ToString() : "-") + "]"
                    ;
                mess_notify_detail += mess_log;
                mess_notify_detail += System.Environment.NewLine;
                log_mess_list3.Add(mess_log);
            }

            var new_exec_user_id = itemEdit.ExecUser != null ? itemEdit.ExecUser.user_id: 0;
            if (crm_subtask.exec_user_id != new_exec_user_id)
            {
                List<cab_user> cab_user_list = dbContext.cab_user.ToList();
                mess_log = "Смена исполнителя подзадачи № " + crm_subtask.task_num.ToString()
                    + " с [" + cab_user_list.Where(ss => ss.user_id == crm_subtask.exec_user_id).FirstOrDefault().user_name
                    + "] на [" + cab_user_list.Where(ss => ss.user_id == new_exec_user_id).FirstOrDefault().user_name + "]"
                    ;
                mess_notify_detail += mess_log;
                mess_notify_detail += System.Environment.NewLine;
                log_mess_list3.Add(mess_log);
            }

            DateTime now = DateTime.Now;
            crm_subtask.file_id = ((itemEdit.TaskFile != null) && (itemEdit.TaskFile.file_id > 0)) ? (int?)itemEdit.TaskFile.file_id : null;
            crm_subtask.repair_date = itemEdit.repair_date;
            crm_subtask.state_id = itemEdit.State != null ? itemEdit.State.state_id : 0;
            crm_subtask.exec_user_id = itemEdit.ExecUser != null ? itemEdit.ExecUser.user_id: 0;
            crm_subtask.subtask_text = itemEdit.subtask_text;
            crm_subtask.upd_date = now;

            crm_task.upd_date = now;
            crm_task.upd_user = currUser.CabUserName;
            crm_task.upd_num = crm_task.upd_num + 1;

            dbContext.SaveChanges();

            // notify
            notify notify = null;

            var crmTaskService = DependencyResolver.Current.GetService<ICrmTaskService>();
            List<int> watchedColumns = crmTaskService.GetNotifyIcqWatchedColumns();

            if ((watchedColumns.Contains((int)Enums.CabGridColumn_TaskList.subtasks_text)) && (!String.IsNullOrEmpty(mess_notify_detail)))
            {
                //mess_notify += "Задача " + crm_task.task_num + ": " + crm_task.task_name.CutToLengthWithDots(50);
                mess_notify = "Изменена задача:";
                mess_notify += System.Environment.NewLine;                
                mess_notify += crmTaskService.GetNotifyIcqContent(crm_task_db);
                mess_notify += "Изменения:";
                mess_notify += System.Environment.NewLine;
                mess_notify += mess_notify_detail;                
                mess_notify += "Автор изменений: " + currUser.CabUserName;
                mess_notify += System.Environment.NewLine;
                mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
                mess_notify += System.Environment.NewLine;

                List<int> notifyTargetList = dbContext.vw_crm_notify_param_user.Where(ss =>
                    ((ss.changer_user_id == crm_task.assigned_user_id) && (ss.is_owner == true) && (ss.user_id != currUser.CabUserId))
                    ||
                    ((ss.changer_user_id == crm_task.exec_user_id) && (ss.is_executer == true) && (ss.user_id != currUser.CabUserId))
                    )
                    .Select(ss => ss.user_id)
                    .Distinct()
                    .ToList();
                if ((notifyTargetList != null) && (notifyTargetList.Count > 0))
                {
                    foreach (var notifyTarget in notifyTargetList)
                    {
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                        dbContext.notify.Add(notify);
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.EMAIL;
                        dbContext.notify.Add(notify);
                    }
                }

                /*
                // уведомление ответственному
                if ((crm_task.assigned_user_id > 0) && (crm_task.assigned_user_id != currUser.CabUserId))
                {
                    notify = new notify();
                    notify.crt_date = now;
                    notify.message = mess_notify;
                    notify.message_sent = false;
                    notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                    notify.sent_date = null;
                    notify.target_user_id = crm_task.assigned_user_id;
                    notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                    dbContext.notify.Add(notify);
                }
                // уведомление исполнителю
                if ((crm_task.exec_user_id > 0) && (crm_task.exec_user_id != crm_task.assigned_user_id) && (crm_task.exec_user_id != currUser.CabUserId))
                {
                    notify = new notify();
                    notify.crt_date = now;
                    notify.message = mess_notify;
                    notify.message_sent = false;
                    notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                    notify.sent_date = null;
                    notify.target_user_id = crm_task.exec_user_id;
                    notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                    dbContext.notify.Add(notify);
                }
                */
            }

            // log

            if (logService == null)
                logService = DependencyResolver.Current.GetService<ICrmLogService>();
            if (logService == null)
                throw new Exception("logService == null");

            if (log_mess_list3.Count > 0)
            {
                foreach (var log_mess in log_mess_list3)
                {
                    logService.InsertLogCrm(now, log_mess, crm_subtask.task_id);
                }
            }
            
            dbContext.SaveChanges();

            return new CrmSubtaskViewModel(crm_subtask);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmSubtaskViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            long id = ((CrmSubtaskViewModel)item).subtask_id;

            var crm_subtask = dbContext.crm_subtask.Where(ss => ss.subtask_id == id).FirstOrDefault();
            if (crm_subtask == null)
            {
                modelState.AddModelError("serv_result", "Подзадача не найдена");
                return false;
            }

            crm_task crm_task = dbContext.crm_task.Where(ss => ss.task_id == crm_subtask.task_id).FirstOrDefault();
            crm_task.upd_date = DateTime.Now;
            crm_task.upd_user = currUser.CabUserName;
            crm_task.upd_num = crm_task.upd_num + 1;

            dbContext.crm_subtask.Remove(crm_subtask);
            dbContext.SaveChanges();

            return true;
        }
    }
}
