﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmUserTaskFollowService : IAuBaseService
    {
        //
    }

    public class CrmUserTaskFollowService : AuBaseService, ICrmUserTaskFollowService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_user_task_follow
                .Where(ss => ss.user_id == parent_id)
                .Select(ss => new CrmUserTaskFollowViewModel
                {
                    item_id = ss.item_id,
                    user_id = ss.user_id,
                    task_id = ss.task_id,
                });
        }
    }
}
