﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Db.Model;
using AuDev.Common.Network;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmTaskFileService : IAuBaseService
    {
        CrmTaskFileViewModel UploadFile(HttpPostedFileBase postedFile, int task_id, ModelStateDictionary modelState);
        bool RemoveFile(int file_id, ModelStateDictionary modelState);
    }

    public class CrmTaskFileService : AuBaseService, ICrmTaskFileService
    {
        ICrmLogService logService;

        List<string> allowedExtentions = new List<string>() { 
            ".jpg", 
            ".jpeg",
            ".png",
            ".txt",
            ".xml",
            ".doc",
            ".docx",
            ".sql",
            ".xls",
            ".xlsx",
            ".7z",
            ".zip",
            ".rar",
            ".pdf",
            ".avi",
            ".mpeg",
            ".mpg",
            ".divx",
            ".mkv",
        };

        private void toLog(DateTime date_beg, string mess, int? task_id)
        {
            if (logService == null)
                logService = DependencyResolver.Current.GetService<ICrmLogService>();
            if (logService == null)
                throw new Exception("logService == null");
            //
            logService.InsertLogCrm(date_beg, mess, task_id);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_task_file
                .Where(ss => ss.task_id == parent_id)
                .ProjectTo<CrmTaskFileViewModel>();
        }

        public CrmTaskFileViewModel UploadFile(HttpPostedFileBase postedFile, int task_id, ModelStateDictionary modelState)
        {
            if (postedFile == null)
            {
                modelState.AddModelError("", "Не указан файл");
                return null;
            }
            
            if (postedFile.ContentLength > 5000000)            
            {
                modelState.AddModelError("", "Файл превышает максимально допустимый размер (5 Мб)");
                return null;
            }

            string fileExt = Path.GetExtension(postedFile.FileName);
            if (!allowedExtentions.Contains(fileExt.ToLower().Trim()))
            {
                modelState.AddModelError("", "Недопустимый тип файла (" + fileExt + ")");
                return null;
            }

            crm_task crm_task = dbContext.crm_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
            if (crm_task == null)
            {
                modelState.AddModelError("", "Не найдена задача с кодом " + task_id.ToString());
                return null;
            }

            string filePath = @"C:\Release\Cabinet\File\Crm\" + task_id.ToString();

            string filePath2_orig = ViewModelUtil.GetCrmFileStoragePath();
            string filePath2 = filePath2_orig + @"\" + task_id.ToString();

            string fileName = Path.GetFileName(postedFile.FileName);
            string physicalPath = Path.Combine(filePath2, fileName);
            //if (!System.Diagnostics.Debugger.IsAttached)
            if (1 == 1)
            {
                try
                {
                    // !!!
                    NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");
                    if (!System.Diagnostics.Debugger.IsAttached)
                    {
                        using (new NetworkConnection(filePath2_orig, writeCredentials))
                        {                            
                            if (!System.IO.Directory.Exists(filePath2))
                            {
                                System.IO.Directory.CreateDirectory(filePath2);
                            }
                            postedFile.SaveAs(physicalPath);
                        }
                    }
                    else
                    {
                        if (!System.IO.Directory.Exists(filePath2))
                        {
                            System.IO.Directory.CreateDirectory(filePath2);
                        }
                        postedFile.SaveAs(physicalPath);
                    }
                }
                catch (Exception ex)
                {
                    string ex_info = GlobalUtil.ExceptionInfo(ex);
                    ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(new CrmTaskFileViewModel()), null, ex_info);
                    modelState.AddModelError("", "Ошибка: " + ex_info);
                    return null;
                }
            }

            DateTime now = DateTime.Now;
            crm_task_file crm_task_file = new crm_task_file();
            crm_task_file.crt_date = now;
            crm_task_file.file_name = Path.GetFileName(postedFile.FileName);
            crm_task_file.file_path = ViewModelUtil.GetCrmFileStorageLink() + task_id.ToString() + @"/" + crm_task_file.file_name;
            crm_task_file.owner_user_id = currUser.CabUserId;
            crm_task_file.subtask_id = null;
            crm_task_file.task_id = task_id;
            crm_task_file.physical_path = ViewModelUtil.GetCrmFileStoragePath() + @"\" + task_id.ToString();
            //physical_path
            dbContext.crm_task_file.Add(crm_task_file);

            /*
            crm_task.upd_date = DateTime.Now;
            crm_task.upd_user = currUser.CabUserName;
            crm_task.upd_num = crm_task.upd_num + 1;
            */

            toLog(now, "Добавлен файл " + crm_task_file.file_name, task_id);

            dbContext.SaveChanges();

            CrmTaskFileViewModel res = new CrmTaskFileViewModel();
            ModelMapper.Map<crm_task_file, CrmTaskFileViewModel>(crm_task_file, res);
            return res;
        }

        public bool RemoveFile(int file_id, ModelStateDictionary modelState)
        {
            crm_task_file crm_task_file = dbContext.crm_task_file.Where(ss => ss.file_id == file_id).FirstOrDefault();
            if (crm_task_file == null)
            {
                modelState.AddModelError("","Не найден файл с кодом " + file_id.ToString());
                return false;
            }
            
            DateTime now = DateTime.Now;
            int task_id = crm_task_file.task_id;
            string file_name = crm_task_file.file_name;

            crm_task crm_task = dbContext.crm_task.Where(ss => ss.task_id == crm_task_file.task_id).FirstOrDefault();
            if (crm_task == null)
            {
                modelState.AddModelError("", "Не найдена задача с кодом " + crm_task_file.task_id.ToString());
                return false;
            }

            var existing = dbContext.crm_subtask.Where(ss => ss.file_id == crm_task_file.file_id).FirstOrDefault();
            if (existing != null)
            {
                modelState.AddModelError("", "Файл привязан к подзадаче " + existing.task_num.ToString());
                return false;
            }

            if (!System.Diagnostics.Debugger.IsAttached)
            {
                try
                {
                    string filePath2_orig = ViewModelUtil.GetCrmFileStoragePath();
                    string filePath2 = filePath2_orig + @"\" + task_id.ToString();
                    string physicalPath = Path.Combine(filePath2, file_name);

                    // !!!
                    NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");
                    if (!System.Diagnostics.Debugger.IsAttached)
                    {
                        using (new NetworkConnection(filePath2_orig, writeCredentials))
                        {
                            if (File.Exists(physicalPath))
                            {
                                System.IO.File.Delete(physicalPath);
                            }
                        }
                    }
                    else
                    {
                        if (File.Exists(physicalPath))
                        {
                            System.IO.File.Delete(physicalPath);
                        }
                    }
                }
                catch (Exception ex)
                {
                    CrmTaskFileViewModel errModel = new CrmTaskFileViewModel();
                    ModelMapper.Map<crm_task_file, CrmTaskFileViewModel>(crm_task_file, errModel);
                    ToLog("Ошибка при удалении", (long)Enums.LogEventType.CABINET, new LogObject(errModel), null, GlobalUtil.ExceptionInfo(ex));
                }
            }

            dbContext.crm_task_file.Remove(crm_task_file);

            /*
            crm_task.upd_date = DateTime.Now;
            crm_task.upd_user = currUser.CabUserName;
            crm_task.upd_num = crm_task.upd_num + 1;
            */

            dbContext.SaveChanges();

            toLog(now, "Удален файл " + file_name, task_id);

            return true;
        }
    }
}
