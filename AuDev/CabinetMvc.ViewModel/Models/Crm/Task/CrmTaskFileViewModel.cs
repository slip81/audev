﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmTaskFileViewModel : AuBaseViewModel
    {
        public CrmTaskFileViewModel()
            : base(Enums.MainObjectType.CRM_TASK_FILE)
        {
            //crm_task_file
        }


        [Key()]
        [Display(Name = "Код")]
        public int file_id { get; set; }

        [Display(Name = "Задача")]
        public int task_id { get; set; }

        [Display(Name = "Подзадача")]
        public Nullable<int> subtask_id { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]
        public int owner_user_id { get; set; }

        [Display(Name = "Имя файла")]
        public string file_name { get; set; }

        [Display(Name = "Ссылка")]
        public string file_path { get; set; }

        [Display(Name = "Путь")]
        public string physical_path { get; set; }

        [Display(Name = " ")]
        [JsonIgnore()]
        public string TaskFileRemoveBtn
        {
            get
            {
                return "<button type='button' class='k-button k-button-icontext' title='Удалить файл' onclick='onTaskFileRemoveBtnClick(" + file_id.ToString() + ")'><span class='k-icon k-i-close'></span></button>";
            }
        }
    }
}