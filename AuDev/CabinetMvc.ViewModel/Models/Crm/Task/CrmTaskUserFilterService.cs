﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmTaskUserFilterService : IAuBaseService
    {
        bool UpdateFilter(CrmTaskUserFilterViewModel model, ModelStateDictionary modelState);
    }

    public class CrmTaskUserFilterService : AuBaseService, ICrmTaskUserFilterService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            var currUserId = currUser.CabUserId;
            var res = dbContext.crm_task_user_filter.Where(ss => ss.grid_id == id && ss.user_id == currUserId)
                .Select(ss => new CrmTaskUserFilterViewModel
                {
                    item_id = ss.item_id,
                    user_id = ss.user_id,
                    grid_id = ss.grid_id,
                    raw_filter = ss.raw_filter,
                    project_filter = ss.project_filter,
                    client_filter = ss.client_filter,
                    priority_filter = ss.priority_filter,
                    state_filter = ss.state_filter,
                    exec_user_filter = ss.exec_user_filter,
                    owner_user_filter = ss.owner_user_filter,                    
                    assigned_user_filter = ss.assigned_user_filter,
                    crt_date1_filter = ss.crt_date1_filter,
                    crt_date2_filter = ss.crt_date2_filter,
                    required_date1_filter = ss.required_date1_filter,
                    required_date2_filter = ss.required_date2_filter,
                    repair_date1_filter = ss.repair_date1_filter,
                    repair_date2_filter = ss.repair_date2_filter,
                    curr_group_id = ss.curr_group_id,
                    fin_cost_filter = (double?)ss.fin_cost_filter,
                    is_moderated_filter = ss.is_moderated_filter,
                    batch_filter = ss.batch_filter,
                })
                .FirstOrDefault();
            
            if (res == null)
            {
                crm_task_user_filter filt = new crm_task_user_filter();
                filt.client_filter = "";
                filt.exec_user_filter = "";
                filt.grid_id = (int)id;
                filt.owner_user_filter = "";
                filt.priority_filter = "";
                filt.project_filter = "";
                filt.raw_filter = "";
                filt.state_filter = "";
                filt.assigned_user_filter = "";
                filt.user_id = currUserId;
                filt.fin_cost_filter = null;
                filt.is_moderated_filter = false;
                filt.batch_filter = "";
                dbContext.crm_task_user_filter.Add(filt);
                dbContext.SaveChanges();

                res = dbContext.crm_task_user_filter.Where(ss => ss.grid_id == id && ss.user_id == currUserId)
                                .Select(ss => new CrmTaskUserFilterViewModel
                                {
                                    item_id = ss.item_id,
                                    user_id = ss.user_id,
                                    grid_id = ss.grid_id,
                                    raw_filter = ss.raw_filter,
                                    project_filter = ss.project_filter,
                                    client_filter = ss.client_filter,
                                    priority_filter = ss.priority_filter,
                                    state_filter = ss.state_filter,
                                    exec_user_filter = ss.exec_user_filter,
                                    owner_user_filter = ss.owner_user_filter,
                                    assigned_user_filter = ss.assigned_user_filter,
                                    crt_date1_filter = ss.crt_date1_filter,
                                    crt_date2_filter = ss.crt_date2_filter,
                                    required_date1_filter = ss.required_date1_filter,
                                    required_date2_filter = ss.required_date2_filter,
                                    repair_date1_filter = ss.repair_date1_filter,
                                    repair_date2_filter = ss.repair_date2_filter,
                                    curr_group_id = ss.curr_group_id,
                                    fin_cost_filter = (double?)ss.fin_cost_filter,
                                    is_moderated_filter = ss.is_moderated_filter,
                                    batch_filter = ss.batch_filter,
                                })
                                .FirstOrDefault();
            }

            string list_formatted = "";
            List<int> clientFilterList = new List<int>();
            try
            {
                list_formatted = res.client_filter.EndsWith(",") ? (res.client_filter.Remove(res.client_filter.Length - 1)) : res.client_filter;
                clientFilterList.AddRange(list_formatted.Split(',').Select(int.Parse).AsEnumerable());
                res.clientFilterList = new List<int>(clientFilterList);
            }
            catch (Exception ex)
            {
                res.clientFilterList = new List<int>();
            }
            List<int> execUserFilterList = new List<int>();
            try
            {
                list_formatted = res.exec_user_filter.EndsWith(",") ? (res.exec_user_filter.Remove(res.exec_user_filter.Length - 1)) : res.exec_user_filter;
                execUserFilterList.AddRange(list_formatted.Split(',').Select(int.Parse).AsEnumerable());
                res.execUserFilterList = new List<int>(execUserFilterList);
            }
            catch (Exception ex)
            {
                res.execUserFilterList = new List<int>();
            }
            List<int> ownerUserFilterList = new List<int>();
            try
            {
                list_formatted = res.owner_user_filter.EndsWith(",") ? (res.owner_user_filter.Remove(res.owner_user_filter.Length - 1)) : res.owner_user_filter;
                ownerUserFilterList.AddRange(list_formatted.Split(',').Select(int.Parse).AsEnumerable());
                res.ownerUserFilterList = new List<int>(ownerUserFilterList);
            }
            catch (Exception ex)
            {
                res.ownerUserFilterList = new List<int>();
            }
            List<int> priorityFilterList = new List<int>();
            try
            {
                list_formatted = res.priority_filter.EndsWith(",") ? (res.priority_filter.Remove(res.priority_filter.Length - 1)) : res.priority_filter;
                priorityFilterList.AddRange(list_formatted.Split(',').Select(int.Parse).AsEnumerable());
                res.priorityFilterList = new List<int>(priorityFilterList);
            }
            catch (Exception ex)
            {
                res.priorityFilterList = new List<int>();
            }
            List<int> projectFilterList = new List<int>();
            try
            {
                list_formatted = res.project_filter.EndsWith(",") ? (res.project_filter.Remove(res.project_filter.Length - 1)) : res.project_filter;
                projectFilterList.AddRange(list_formatted.Split(',').Select(int.Parse).AsEnumerable());
                res.projectFilterList = new List<int>(projectFilterList);
            }
            catch (Exception ex)
            {
                res.projectFilterList = new List<int>();
            }
            List<int> stateFilterList = new List<int>();
            try
            {
                list_formatted = res.state_filter.EndsWith(",") ? (res.state_filter.Remove(res.state_filter.Length - 1)) : res.state_filter;
                stateFilterList.AddRange(list_formatted.Split(',').Select(int.Parse).AsEnumerable());
                res.stateFilterList = new List<int>(stateFilterList);
            }
            catch (Exception ex)
            {
                res.stateFilterList = new List<int>();
            }
            List<int> assignedUserFilterList = new List<int>();
            try
            {
                list_formatted = res.assigned_user_filter.EndsWith(",") ? (res.assigned_user_filter.Remove(res.assigned_user_filter.Length - 1)) : res.assigned_user_filter;
                assignedUserFilterList.AddRange(list_formatted.Split(',').Select(int.Parse).AsEnumerable());
                res.assignedUserFilterList = new List<int>(assignedUserFilterList);
            }
            catch (Exception ex)
            {
                res.assignedUserFilterList = new List<int>();
            }
            List<int> batchFilterList = new List<int>();
            try
            {
                list_formatted = res.batch_filter.EndsWith(",") ? (res.batch_filter.Remove(res.batch_filter.Length - 1)) : res.batch_filter;
                batchFilterList.AddRange(list_formatted.Split(',').Select(int.Parse).AsEnumerable());
                res.batchFilterList = new List<int>(batchFilterList);
            }
            catch (Exception ex)
            {
                res.batchFilterList = new List<int>();
            }

            return res;
        }

        public bool UpdateFilter(CrmTaskUserFilterViewModel model, ModelStateDictionary modelState)
        {
            if (model == null)
            {
                modelState.AddModelError("", "Не заданы аттрибуты");
                return false;
            }

            int grid_id = model.grid_id;

            var currUserId = currUser.CabUserId;
            var filt = dbContext.crm_task_user_filter.Where(ss => ss.grid_id == grid_id && ss.user_id == currUserId).FirstOrDefault();
            if (filt == null)
            {
                filt = new crm_task_user_filter();
                filt.user_id = currUserId;
                filt.grid_id = grid_id;
                filt.raw_filter = model.raw_filter;
                filt.client_filter = model.client_filter;
                filt.priority_filter = model.priority_filter;
                filt.project_filter = model.project_filter;
                filt.owner_user_filter = model.owner_user_filter;
                filt.exec_user_filter = model.exec_user_filter;
                filt.state_filter = model.state_filter;
                filt.assigned_user_filter = model.assigned_user_filter;
                filt.crt_date1_filter = model.crt_date1_filter;
                filt.crt_date2_filter = model.crt_date2_filter;
                filt.required_date1_filter = model.required_date1_filter;
                filt.required_date2_filter = model.required_date2_filter;
                filt.repair_date1_filter = model.repair_date1_filter;
                filt.repair_date2_filter = model.repair_date2_filter;
                filt.curr_group_id = model.curr_group_id;
                filt.fin_cost_filter = (decimal?)model.fin_cost_filter;
                filt.is_moderated_filter = model.is_moderated_filter;
                filt.batch_filter = model.batch_filter;
                dbContext.crm_task_user_filter.Add(filt);
            }
            else
            {
                filt.raw_filter = model.raw_filter;
                filt.client_filter = model.client_filter;
                filt.priority_filter = model.priority_filter;
                filt.project_filter = model.project_filter;
                filt.owner_user_filter = model.owner_user_filter;
                filt.exec_user_filter = model.exec_user_filter;
                filt.assigned_user_filter = model.assigned_user_filter;
                filt.state_filter = model.state_filter;
                filt.crt_date1_filter = model.crt_date1_filter;
                filt.crt_date2_filter = model.crt_date2_filter;
                filt.required_date1_filter = model.required_date1_filter;
                filt.required_date2_filter = model.required_date2_filter;
                filt.repair_date1_filter = model.repair_date1_filter;
                filt.repair_date2_filter = model.repair_date2_filter;
                filt.curr_group_id = model.curr_group_id;
                filt.fin_cost_filter = (decimal?)model.fin_cost_filter;
                filt.is_moderated_filter = model.is_moderated_filter;
                filt.batch_filter = model.batch_filter;
            }
            
            dbContext.SaveChanges();            

            return true;
        }
    }

}
