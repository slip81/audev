﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmGroupViewModel : AuBaseViewModel
    {
        public CrmGroupViewModel()
            : base(Enums.MainObjectType.CRM_GROUP)
        {
            //
        }

        public CrmGroupViewModel(crm_group item)
            : base(Enums.MainObjectType.CRM_GROUP)
        {            
            group_id = item.group_id;
            group_name = item.group_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public int group_id { get; set; }

        [Display(Name = "Наименование")]
        public string group_name { get; set; }
    }
}