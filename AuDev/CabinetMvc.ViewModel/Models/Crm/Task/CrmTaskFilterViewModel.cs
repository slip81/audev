﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmTaskFilterViewModel : AuBaseViewModel
    {
        public CrmTaskFilterViewModel()
            : base(Enums.MainObjectType.CRM_TASK_FILTER)
        {
            //vw_crm_task_filter
        }


        [Key()]
        [Display(Name = "Код")]
        public int adr { get; set; }

        [Display(Name = "filter_group_id")]
        public int filter_group_id { get; set; }

        [Display(Name = "filter_group_name")]
        public string filter_group_name { get; set; }

        [Display(Name = "filter_value_id")]
        public Nullable<int> filter_value_id { get; set; }

        [Display(Name = "filter_value_name")]
        public string filter_value_name { get; set; }

        [Display(Name = "cnt")]
        public Nullable<int> cnt { get; set; }

        // children

        [Display(Name = "ChildrenList")]
        [JsonIgnore()]
        public IEnumerable<CrmTaskFilterViewModel> ChildrenList { get; set; }

        [Display(Name = "ChildrenList")]
        [JsonIgnore()]
        public bool HasChildren { get; set; }        
    }
}