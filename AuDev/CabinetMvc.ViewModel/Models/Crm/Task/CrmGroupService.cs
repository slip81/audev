﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmGroupService : IAuBaseService
    {
        //
    }

    public class CrmGroupService : AuBaseService, ICrmGroupService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_group
                .Where(ss => ss.group_id >= 0)
                .Select(ss => new CrmGroupViewModel
                {
                    group_id = ss.group_id,
                    group_name = ss.group_name,
                })
                .OrderBy(ss => ss.group_id);
        }
    }
}
