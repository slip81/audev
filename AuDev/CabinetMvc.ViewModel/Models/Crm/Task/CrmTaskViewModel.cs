﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmTaskViewModel : AuBaseViewModel
    {
        public CrmTaskViewModel()
            : base(Enums.MainObjectType.CRM_TASK)
        {
            task_id = 0;
            task_name = "";
            task_text = "";
            project_id = 0;
            module_id = 0;
            module_part_id = 0;
            module_version_id = 0;
            client_id = 0;
            priority_id = 0;
            state_id = 0;
            owner_user_id = 0;
            exec_user_id = 0;
            task_num = "";
            crt_date = null;
            required_date = null;
            repair_date = null;
            repair_version_id = 0;
            group_id = 0;            
            prepared_user_id = 0;
            fixed_user_id = 0;
            approved_user_id = 0;
            assigned_user_id = 0;
            rel_child_cnt = 0;
            rel_parent_cnt = 0;
            is_fin = false;
            is_fin_name = "";
            fin_cost = 0;
            is_event = false;
            is_event_name = "";
            event_user_id = 0;
            upd_date = null;
            upd_num = 0;
            upd_user = "";
            is_event_for_exec = false;
            view_user_id = 0;
            state_fore_color = "";
            project_fore_color = "";
            priority_fore_color = "";
            moderator_user_id = 0;

            project_name = "";
            module_name = "";
            module_part_name = "";
            module_version_name = "";
            client_name = "";
            priority_name = "";
            state_name = "";
            owner_user_name = "";
            exec_user_name = "";
            repair_version_name = "";
            task_num_int = 0;
            prepared_user_name = "";
            fixed_user_name = "";
            approved_user_name = "";
            assigned_user_name = "";
            group_name = "";
            is_archive = false;
            moderator_user_name = "";
            storage_folder_id = null;
            storage_folder_name = "";
            is_in_storage = false;
            is_in_storage_name = "";
            task_text_substring = "";
            subtasks_text = "";
            subtasks_task_id = null;
            notes_text = "";
            notes_task_id = null;
            batch_id = 0;
            batch_name = "";
            priority_is_boss = false;
            state_for_event = false;
            event_date_beg = null;
            event_date_end = null;
            is_control = false;
            required_date_orig = null;
            repair_date_orig = null;
            event_date_beg_orig = null;
            event_date_end_orig = null;
            control_crt_date_orig = null;
            control_crt_user_orig = null;
            required_date_curr = null;
            repair_date_curr = null;
            event_date_beg_curr = null;
            event_date_end_curr = null;
            control_crt_date_curr = null;
            control_crt_user_curr = null;
            cost_plan = null;
            cost_fact = null;
            IsModerated = false;
        }

        public CrmTaskViewModel(vw_crm_task item)
            : base(Enums.MainObjectType.CRM_TASK)
        {
            task_id = item.task_id;
            task_name = item.task_name;
            task_text = item.task_text;
            project_id = item.project_id;
            module_id = item.module_id;
            module_part_id = item.module_part_id;
            module_version_id = item.module_version_id;
            client_id = item.client_id;
            priority_id = item.priority_id;
            state_id = item.state_id;
            owner_user_id = item.owner_user_id;
            exec_user_id = item.exec_user_id;
            task_num = item.task_num;
            crt_date = item.crt_date;
            required_date = item.required_date;
            repair_date = item.repair_date;
            repair_version_id = item.repair_version_id;
            group_id = item.group_id;            
            prepared_user_id = item.prepared_user_id;
            fixed_user_id = item.fixed_user_id;
            approved_user_id = item.approved_user_id;
            assigned_user_id = item.assigned_user_id;
            rel_child_cnt = item.rel_child_cnt;
            rel_parent_cnt = item.rel_parent_cnt;
            is_fin = item.is_fin;
            is_fin_name = item.is_fin_name;
            fin_cost = item.fin_cost;
            is_event = item.is_event;
            is_event_name = item.is_event_name;
            event_user_id = item.event_user_id;
            upd_date = item.upd_date;
            upd_num = item.upd_num;
            upd_user = item.upd_user;
            is_event_for_exec = (item.event_user_id == item.exec_user_id) && (item.is_event);
            view_user_id = item.view_user_id;
            state_fore_color = item.state_fore_color;
            project_fore_color = item.project_fore_color;
            priority_fore_color = item.priority_fore_color;
            moderator_user_id = item.moderator_user_id;
            storage_folder_id = item.storage_folder_id;
            storage_folder_name = item.storage_folder_name;
            is_in_storage = item.is_in_storage;
            is_in_storage_name = item.is_in_storage_name;
            IsModerated = item.moderator_user_id > 0;

            project_name = item.priority_name;
            module_name = item.module_name;
            module_part_name = item.module_part_name;
            module_version_name = item.module_version_name;
            client_name = item.client_name;
            priority_name = item.priority_name;
            state_name = item.state_name;
            owner_user_name = item.owner_user_name;
            exec_user_name = item.exec_user_name;
            repair_version_name = item.repair_version_name;
            task_num_int = item.task_num_int;
            prepared_user_name = item.prepared_user_name;
            fixed_user_name = item.fixed_user_name;
            approved_user_name = item.approved_user_name;
            assigned_user_name = item.assigned_user_name;
            group_name = item.group_name;
            is_archive = false;
            moderator_user_name = item.moderator_user_name;
            task_text_substring = item.task_text_substring;
            subtasks_text = item.subtasks_text;
            subtasks_task_id = item.subtasks_task_id;
            notes_text = item.notes_text;
            notes_task_id = item.notes_task_id;
            batch_id = item.batch_id;
            batch_name = item.batch_name;
            priority_is_boss = item.priority_is_boss;
            state_for_event = item.state_for_event;
            event_date_beg = item.event_date_beg;
            event_date_end = item.event_date_end;
            is_control = item.is_control;
            required_date_orig = item.required_date_orig;
            repair_date_orig = item.repair_date_orig;
            event_date_beg_orig = item.event_date_beg_orig;
            event_date_end_orig = item.event_date_end_orig;
            control_crt_date_orig = item.control_crt_date_orig;
            control_crt_user_orig = item.control_crt_user_orig;
            required_date_curr = item.required_date_curr;
            repair_date_curr = item.repair_date_curr;
            event_date_beg_curr = item.event_date_beg_curr;
            event_date_end_curr = item.event_date_end_curr;
            control_crt_date_curr = item.control_crt_date_curr;
            control_crt_user_curr = item.control_crt_user_curr;
            cost_plan = item.cost_plan;
            cost_fact = item.cost_fact;
        }

        public CrmTaskViewModel(crm_task item)
            : base(Enums.MainObjectType.CRM_TASK)
        {
            task_id = item.task_id;
            task_name = item.task_name;
            task_text = item.task_text;
            project_id = item.project_id;
            module_id = item.module_id;
            module_part_id = item.module_part_id;
            module_version_id = item.module_version_id;
            client_id = item.client_id;
            priority_id = item.priority_id;
            state_id = item.state_id;
            owner_user_id = item.owner_user_id;
            exec_user_id = item.exec_user_id;
            task_num = item.task_num;
            crt_date = item.crt_date;
            required_date = item.required_date;
            repair_date = item.repair_date;
            repair_version_id = item.repair_version_id;
            group_id = item.group_id;            
            prepared_user_id = item.prepared_user_id;
            fixed_user_id = item.fixed_user_id;
            approved_user_id = item.approved_user_id;
            assigned_user_id = item.assigned_user_id;
            is_archive = item.is_archive;
            rel_child_cnt = item.rel_child_cnt;
            rel_parent_cnt = item.rel_parent_cnt;
            is_fin = item.is_fin;            
            fin_cost = item.fin_cost;
            is_event = false;
            event_user_id = 0;
            upd_date = item.upd_date;
            upd_num = item.upd_num;
            upd_user = item.upd_user;
            is_event_for_exec = false;
            moderator_user_id = item.moderator_user_id;
            storage_folder_id = item.storage_folder_id;
            storage_folder_name = "";
            is_in_storage = false;
            is_in_storage_name = "";
            task_text_substring = "";
            subtasks_text = "";
            subtasks_task_id = null;
            notes_text = "";
            notes_task_id = null;
            batch_id = item.batch_id;
            batch_name = "";
            priority_is_boss = false;
            state_for_event = false;
            event_date_beg = null;
            event_date_end = null;
            is_control = item.is_control;
            required_date_orig = null;
            repair_date_orig = null;
            event_date_beg_orig = null;
            event_date_end_orig = null;
            control_crt_date_orig = null;
            control_crt_user_orig = null;
            required_date_curr = null;
            repair_date_curr = null;
            event_date_beg_curr = null;
            event_date_end_curr = null;
            control_crt_date_curr = null;
            control_crt_user_curr = null;
            cost_plan = item.cost_plan;
            cost_fact = item.cost_fact;

            IsModerated = item.moderator_user_id > 0;
        }

        [Key()]
        [Display(Name = "Код")]
        public int task_id { get; set; }

        [Display(Name = "Кратко")]
        [AllowHtml()]
        public string task_name { get; set; }

        [Display(Name = "Подробно")]
        [Required(ErrorMessage = "Не задан текст задачи")]
        [UIHint("Text")]
        [AllowHtml()]
        public string task_text { get; set; }

        [Display(Name = "Программа")]
        public int project_id { get; set; }

        [Display(Name = "Модуль")]
        public int module_id { get; set; }

        [Display(Name = "Раздел модуля")]
        public int module_part_id { get; set; }

        [Display(Name = "Версия обнаружения")]
        public int module_version_id { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Приоритет")]
        public int priority_id { get; set; }

        [Display(Name = "Статус")]
        public int state_id { get; set; }

        [Display(Name = "Автор")]
        public int owner_user_id { get; set; }

        [Display(Name = "Исполнитель")]
        public int exec_user_id { get; set; }

        [Display(Name = "Номер")]
        public string task_num { get; set; }

        [Display(Name = "Дата создания")]
        [DataType(DataType.DateTime)]        
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Плановая дата")]
        [DataType(DataType.DateTime)]
        public Nullable<System.DateTime> required_date { get; set; }

        [Display(Name = "Фактическая дата")]
        [DataType(DataType.DateTime)]
        public Nullable<System.DateTime> repair_date { get; set; }

        [Display(Name = "Версия исполнения")]
        public Nullable<int> repair_version_id { get; set; }

        [Display(Name = "Архивная")]
        public bool is_archive { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Модуль")]
        public string module_name { get; set; }

        [Display(Name = "Раздел модуля")]
        public string module_part_name { get; set; }

        [Display(Name = "Версия обнаружения")]
        public string module_version_name { get; set; }

        [Display(Name = "Приоритет")]
        public string priority_name { get; set; }

        [Display(Name = "Программа")]
        public string project_name { get; set; }

        [Display(Name = "Статус")]
        public string state_name { get; set; }

        [Display(Name = "Автор")]
        public string owner_user_name { get; set; }

        [Display(Name = "Исполнитель")]
        public string exec_user_name { get; set; }

        [Display(Name = "Версия исполнения")]
        public string repair_version_name { get; set; }

        [Display(Name = "Номер")]
        public int task_num_int { get; set; }

        [Display(Name = "Группа")]
        public int group_id { get; set; }

        [Display(Name = "Обработал")]           
        public int prepared_user_id { get; set; }

        [Display(Name = "К утверждению")]           
        public int fixed_user_id { get; set; }

        [Display(Name = "Утвердил")]           
        public int approved_user_id { get; set; }

        [Display(Name = "Обработал")]           
        public string prepared_user_name { get; set; }

        [Display(Name = "К утверждению")]           
        public string fixed_user_name { get; set; }

        [Display(Name = "Утвердил")]
        public string approved_user_name { get; set; }

        [Display(Name = "Ответственный")]
        public int assigned_user_id { get; set; }

        [Display(Name = "Ответственный")]
        public string assigned_user_name { get; set; }

        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [Display(Name = "Дочерние связи")]
        public int rel_child_cnt { get; set; }

        [Display(Name = "Родительские связи")]
        public int rel_parent_cnt { get; set; }

        [Display(Name = "Фин. знач")]
        public bool is_fin { get; set; }

        [Display(Name = "Фин. знач")]
        public string is_fin_name { get; set; }

        [Display(Name = "Цена")]
        public Nullable<decimal> fin_cost { get; set; }

        [Display(Name = "В календаре")]
        public bool is_event { get; set; }

        [Display(Name = "В календаре")]
        public string is_event_name { get; set; }

        [Display(Name = "event_user_id")]
        public Nullable<int> event_user_id { get; set; }

        [Display(Name = "Дата изменения")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Номер изменения")]
        public int upd_num { get; set; }

        [Display(Name = "Автор изменения")]
        public string upd_user { get; set; }

        [Display(Name = "В календаре для исполнителя")]
        public bool is_event_for_exec { get; set; }

        [Display(Name = "view_user_id")]
        public Nullable<int> view_user_id { get; set; }

        [Display(Name = "state_fore_color")]
        public string state_fore_color { get; set; }

        [Display(Name = "project_fore_color")]
        public string project_fore_color { get; set; }

        [Display(Name = "priority_fore_color")]
        public string priority_fore_color { get; set; }

        [Display(Name = "Модерация")]
        public int moderator_user_id { get; set; }

        [Display(Name = "Модерация")]
        public string moderator_user_name { get; set; }

        [Display(Name = "Папка")]
        public Nullable<int> storage_folder_id { get; set; }

        [Display(Name = "Папка")]
        public string storage_folder_name { get; set; }

        [Display(Name = "В хранилище")]
        public bool is_in_storage { get; set; }

        [Display(Name = "В хранилище")]
        public string is_in_storage_name { get; set; }

        [Display(Name = "Подробно")]
        [JsonIgnore()]
        public string task_text_substring { get; set; }

        [Display(Name = "Подзадачи")]
        [JsonIgnore()]
        public string subtasks_text { get; set; }

        [Display(Name = "Есть подзадачи")]
        [JsonIgnore()]
        public Nullable<int> subtasks_task_id { get; set; }

        [Display(Name = "Подзадач")]
        [JsonIgnore()]
        public Nullable<int> subtasks_count { get; set; }

        [Display(Name = "Комментарии")]
        [JsonIgnore()]
        public string notes_text { get; set; }

        [Display(Name = "Есть комментарии")]
        [JsonIgnore()]
        public Nullable<int> notes_task_id { get; set; }

        [Display(Name = "Комментариев")]
        [JsonIgnore()]
        public Nullable<int> notes_count { get; set; }

        [Display(Name = "Проект")]
        public int batch_id { get; set; }

        [Display(Name = "Проект")]
        [JsonIgnore()]
        public string batch_name { get; set; }

        [Display(Name = "Приоритет руководителя")]
        [JsonIgnore()]
        public Nullable<bool> priority_is_boss { get; set; }

        [Display(Name = "Статус для событий")]
        [JsonIgnore()]
        public Nullable<bool> state_for_event { get; set; }

        [Display(Name = "В календаре с")]
        [JsonIgnore()]
        public Nullable<System.DateTime> event_date_beg { get; set; }

        [Display(Name = "В календаре по")]
        [JsonIgnore()]
        public Nullable<System.DateTime> event_date_end { get; set; }

        [Display(Name = "Контроль дат")]
        public bool is_control { get; set; }

        [Display(Name = "required_date_orig")]
        public Nullable<System.DateTime> required_date_orig { get; set; }

        [Display(Name = "repair_date_orig")]
        public Nullable<System.DateTime> repair_date_orig { get; set; }

        [Display(Name = "event_date_beg_orig")]
        public Nullable<System.DateTime> event_date_beg_orig { get; set; }

        [Display(Name = "event_date_end_orig")]
        public Nullable<System.DateTime> event_date_end_orig { get; set; }

        [Display(Name = "control_crt_date_orig")]
        public Nullable<System.DateTime> control_crt_date_orig { get; set; }

        [Display(Name = "control_crt_user_orig")]
        public string control_crt_user_orig { get; set; }

        [Display(Name = "required_date_curr")]
        public Nullable<System.DateTime> required_date_curr { get; set; }

        [Display(Name = "repair_date_curr")]
        public Nullable<System.DateTime> repair_date_curr { get; set; }

        [Display(Name = "event_date_beg_curr")]
        public Nullable<System.DateTime> event_date_beg_curr { get; set; }

        [Display(Name = "event_date_end_curr")]
        public Nullable<System.DateTime> event_date_end_curr { get; set; }

        [Display(Name = "control_crt_date_curr")]
        public Nullable<System.DateTime> control_crt_date_curr { get; set; }

        [Display(Name = "control_crt_user_curr")]
        public string control_crt_user_curr { get; set; }

        [Display(Name = "ТЗ план")]
        public Nullable<decimal> cost_plan { get; set; }

        [Display(Name = "ТЗ факт")]
        public Nullable<decimal> cost_fact { get; set; }

        [Display(Name = "IsChecked")]
        public bool IsChecked { get; set; }

        [Display(Name = "IsModerated")]
        public bool IsModerated { get; set; }

        [Display(Name = "Создал")]
        public string OwnerInfo { get { return owner_user_id <= 0 ? "" : ("Создал: " + owner_user_name); } }

        [Display(Name = "Обработал")]
        public string PreparedInfo { get { return prepared_user_id <= 0 ? "" : ("Обработал: " + prepared_user_name); } }

        [Display(Name = "Одобрил")]
        public string FixedInfo { get { return fixed_user_id <= 0 ? "" : ("Одобрил: " + fixed_user_name);  } }

        [Display(Name = "Утвердил")]
        public string ApprovedInfo { get { return approved_user_id <= 0 ? "" : ("Утвердил: " + approved_user_name); } }

        [Display(Name = "История")]
        public string TaskInfo { get { return OwnerInfo + " " + PreparedInfo + " " + FixedInfo + " " + ApprovedInfo; } }

        [Display(Name = "В календаре")]
        [JsonIgnore()]
        public string EventPeriod { 
            get 
            { 
                return event_date_beg.HasValue ?
                (((DateTime)event_date_beg).ToString("dd.MM.yyyy") + (((event_date_end.HasValue) && (event_date_beg != event_date_end)) ? (" - " + ((DateTime)event_date_end).ToString("dd.MM.yyyy")) : "")) 
                    : 
                ""
                ; 
            }
        }

        [JsonIgnore()]
        public IEnumerable<CrmTaskNoteViewModel> notes { get; set; }

        [JsonIgnore()]
        public IEnumerable<CrmSubtaskViewModel> subtasks { get; set; }

        [JsonIgnore()]
        public IEnumerable<CrmLogViewModel> logs { get; set; }

        [JsonIgnore()]
        public IEnumerable<CrmTaskFileViewModel> files { get; set; }
    }

    public class CrmTaskNotifyInfo
    {
        public CrmTaskNotifyInfo()
        {
            //
        }

        public CrmTaskNotifyInfo(int _column_id, string _column_name, string _value)
        {
            column_id = _column_id;
            column_name = _column_name;
            value = _value;
        }

        public int column_id { get; set; }
        public string column_name { get; set; }
        public string value { get; set; }
    }
}