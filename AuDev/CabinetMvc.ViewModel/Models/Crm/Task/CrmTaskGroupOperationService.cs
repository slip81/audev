﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmTaskGroupOperationService : IAuBaseService
    {
        //
    }

    public class CrmTaskGroupOperationService : AuBaseService, ICrmTaskGroupOperationService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_group_operation.AsEnumerable()
                .Select(ss => new CrmTaskGroupOperationViewModel
                {
                    item_id = ss.item_id,
                    group_id = ss.group_id,
                    operation_id = ss.operation_id,
                    operation_name = ss.operation_name,
                })
                .AsQueryable()
                .OrderBy(ss => ss.operation_id);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_group_operation.Where(ss => ss.group_id == parent_id)
                .Select(ss => new CrmTaskGroupOperationViewModel
                {
                    item_id = ss.item_id,
                    group_id = ss.group_id,
                    operation_id = ss.operation_id,
                    operation_name = ss.operation_name,
                })
                .OrderBy(ss => ss.operation_id);
        }
    }
}
