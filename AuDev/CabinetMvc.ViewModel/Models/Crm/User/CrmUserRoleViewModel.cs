﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmUserRoleViewModel : AuBaseViewModel
    {
        public CrmUserRoleViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_USER_ROLE)
        {
            //
        }

        public CrmUserRoleViewModel(crm_user_role item)
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_USER_ROLE)
        {
            role_id = item.role_id;
            role_name = item.role_name;
            add_days_date_plan = item.add_days_date_plan;
            ignore_date_plan = item.ignore_date_plan;
        }

        [Key]
        [Display(Name = "Код")]
        public int role_id { get; set; }

        [Display(Name = "Наименование")]
        public string role_name { get; set; }

        [Display(Name = "Игнорировать плановую дату")]
        public bool ignore_date_plan { get; set; }

        [Display(Name = "Сколько дней можно добавить к плановой дате")]
        public Nullable<int> add_days_date_plan { get; set; }
    }
}