﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmUserRoleService : IAuBaseService
    {
        //
    }

    public class CrmUserRoleService : AuBaseService, ICrmUserRoleService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_user_role
                .Select(ss => new CrmUserRoleViewModel
                {
                    role_id = ss.role_id,
                    role_name = ss.role_name,
                    ignore_date_plan = ss.ignore_date_plan,
                    add_days_date_plan = ss.add_days_date_plan,
                })
                ;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmUserRoleViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmUserRoleViewModel itemAdd = (CrmUserRoleViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_user_role crm_user_role = new crm_user_role();
            crm_user_role.role_id = dbContext.crm_user_role.Max(ss => ss.role_id) + 1;
            crm_user_role.role_name = itemAdd.role_name;
            crm_user_role.ignore_date_plan = itemAdd.ignore_date_plan;
            crm_user_role.add_days_date_plan = itemAdd.add_days_date_plan;            
            dbContext.crm_user_role.Add(crm_user_role);

            dbContext.SaveChanges();

            return new CrmUserRoleViewModel(crm_user_role);            
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmUserRoleViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmUserRoleViewModel itemEdit = (CrmUserRoleViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_user_role = dbContext.crm_user_role.Where(ss => ss.role_id == itemEdit.role_id).FirstOrDefault();
            if (crm_user_role == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.role_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmUserRoleViewModel(crm_user_role);

            crm_user_role.role_name = itemEdit.role_name;
            crm_user_role.ignore_date_plan = itemEdit.ignore_date_plan;

            dbContext.SaveChanges();

            return new CrmUserRoleViewModel(crm_user_role);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmUserRoleViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmUserRoleViewModel)item).role_id;

            var crm_user_role = dbContext.crm_user_role.Where(ss => ss.role_id == id).FirstOrDefault();
            if (crm_user_role == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.cab_user.Where(ss => ss.crm_user_role_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют пользователи с ролью с кодом " + id.ToString());
                return false;
            }

            dbContext.crm_user_role_state_done.RemoveRange(dbContext.crm_user_role_state_done.Where(ss => ss.role_id == id));
            dbContext.crm_user_role.Remove(crm_user_role);
            dbContext.SaveChanges();

            return true;
        }
 
    }
}
