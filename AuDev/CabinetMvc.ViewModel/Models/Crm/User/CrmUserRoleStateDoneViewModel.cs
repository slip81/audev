﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmUserRoleStateDoneViewModel : AuBaseViewModel
    {
        public CrmUserRoleStateDoneViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_USER_ROLE_STATE_DONE)
        {
            //
        }

        public CrmUserRoleStateDoneViewModel(crm_user_role_state_done item)
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_USER_ROLE_STATE_DONE)
        {
            role_id = item.role_id;
            item_id = item.item_id;
            state_id = item.state_id;
            state_name = "";
        }

        [Key]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Роль")]
        public int role_id { get; set; }

        [Display(Name = "Статус")]
        public int state_id { get; set; }

        [Display(Name = "Статус")]
        public string state_name { get; set; }

        [Display(Name = "Статус")]
        [UIHint("CrmStateCombo")]
        public CrmStateViewModel State { get; set; }
    }
}