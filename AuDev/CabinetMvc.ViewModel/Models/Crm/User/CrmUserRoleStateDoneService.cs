﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmUserRoleStateDoneService : IAuBaseService
    {
        //
    }

    public class CrmUserRoleStateDoneService : AuBaseService, ICrmUserRoleStateDoneService
    {

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_user_role_state_done.Where(ss => ss.role_id == parent_id)
                .Select(ss => new CrmUserRoleStateDoneViewModel
                {
                    role_id = ss.role_id,
                    item_id = ss.item_id,
                    state_id = ss.state_id,
                    state_name = ss.crm_state.state_name,
                    //Ds = new ExchangePartnerViewModel() { ds_id = (long)ss.ds_id, ds_name = ss.datasource.ds_name, },
                    State = new CrmStateViewModel() { state_id = ss.state_id, state_name = ss.crm_state.state_name, fore_color = "", },
                })
                ;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmUserRoleStateDoneViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmUserRoleStateDoneViewModel itemAdd = (CrmUserRoleStateDoneViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var state_id = itemAdd.State.state_id;
            crm_user_role_state_done crm_user_role_state_done = new crm_user_role_state_done();
            crm_user_role_state_done.role_id = itemAdd.role_id;
            crm_user_role_state_done.state_id = state_id;
            dbContext.crm_user_role_state_done.Add(crm_user_role_state_done);

            dbContext.SaveChanges();

            return new CrmUserRoleStateDoneViewModel(crm_user_role_state_done);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmUserRoleStateDoneViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmUserRoleStateDoneViewModel itemEdit = (CrmUserRoleStateDoneViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_user_role_state_done = dbContext.crm_user_role_state_done.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (crm_user_role_state_done == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.item_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmUserRoleStateDoneViewModel(crm_user_role_state_done);

            var state_id = itemEdit.State.state_id;
            crm_user_role_state_done.state_id = state_id;

            dbContext.SaveChanges();

            return new CrmUserRoleStateDoneViewModel(crm_user_role_state_done);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmUserRoleStateDoneViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmUserRoleStateDoneViewModel)item).item_id;

            var crm_user_role_state_done = dbContext.crm_user_role_state_done.Where(ss => ss.item_id == id).FirstOrDefault();
            if (crm_user_role_state_done == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            dbContext.crm_user_role_state_done.Remove(crm_user_role_state_done);
            dbContext.SaveChanges();

            return true;
        }
 
    }
}
