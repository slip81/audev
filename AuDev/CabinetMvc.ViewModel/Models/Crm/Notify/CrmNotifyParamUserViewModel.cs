﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmNotifyParamUserViewModel : AuBaseViewModel
    {

        public CrmNotifyParamUserViewModel()
            : base(Enums.MainObjectType.CRM_NOTIFY_PARAM_USER)
        {            
            //vw_crm_notify_param_user
        }

        [Key()]
        [Display(Name = "Код")]
        public int adr { get; set; }

        [Display(Name = "notify_id")]
        public int notify_id { get; set; }

        [Display(Name = "Хозяин")]
        public int user_id { get; set; }

        [Display(Name = "Транспорт")]
        public Nullable<int> transport_type { get; set; }

        [Display(Name = "За кем следить")]
        public Nullable<int> changer_user_id { get; set; }

        [Display(Name = "Чьи задачи отслеживать")]
        public string user_name { get; set; }

        [Display(Name = "Пользователь = исполнитель по задаче")]
        public Nullable<bool> is_executer { get; set; }

        [Display(Name = "Пользователь = ответственный по задаче")]
        public Nullable<bool> is_owner { get; set; }

        [Display(Name = "Вкл")]
        public Nullable<bool> is_active { get; set; }
    }
}

