﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmNotifyParamContentService : IAuBaseService
    {
        IQueryable<CrmNotifyParamContentViewModel> GetItems_byUserAndTransport(int user_id, int transport_type);        
    }

    public class CrmNotifyParamContentService : AuBaseService, ICrmNotifyParamContentService
    {
        public IQueryable<CrmNotifyParamContentViewModel> GetItems_byUserAndTransport(int user_id, int transport_type)
        {
            return dbContext.vw_crm_notify_param_content
                .Where(ss => ss.user_id == user_id 
                    && ss.transport_type == transport_type
                    )
                .ProjectTo<CrmNotifyParamContentViewModel>()
                ;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmNotifyParamContentViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CrmNotifyParamContentViewModel itemEdit = (CrmNotifyParamContentViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_notify crm_notify = dbContext.crm_notify.Where(ss => ss.user_id == currUser.CabUserId).FirstOrDefault();
            if (crm_notify == null)
            {
                modelState.AddModelError("", "crm_notify == null");
                return null;
            }

            crm_notify_param_content param = dbContext.crm_notify_param_content.Where(ss => ss.column_id == itemEdit.column_id
                && ss.transport_type == itemEdit.transport_type
                && ss.notify_id == crm_notify.notify_id
                ).FirstOrDefault();

            if (param == null)
            {
                param = new crm_notify_param_content();
                param.notify_id = crm_notify.notify_id;
                param.transport_type = itemEdit.transport_type.GetValueOrDefault(0);
                param.column_id = itemEdit.column_id.GetValueOrDefault(0);
                param.is_active = itemEdit.is_active.GetValueOrDefault(false);
                dbContext.crm_notify_param_content.Add(param);
            }
            else
            {
                param.is_active = itemEdit.is_active.GetValueOrDefault(false);
            }


            dbContext.SaveChanges();

            return itemEdit;
        }                
    }
}