﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmNotifyParamUserService : IAuBaseService
    {
        IQueryable<CrmNotifyParamUserViewModel> GetItems_byUserAndTransport(int user_id, int transport_type);        
    }

    public class CrmNotifyParamUserService : AuBaseService, ICrmNotifyParamUserService
    {
        public IQueryable<CrmNotifyParamUserViewModel> GetItems_byUserAndTransport(int user_id, int transport_type)
        {
            return dbContext.vw_crm_notify_param_user
                .Where(ss => ss.user_id == user_id 
                    && ((ss.transport_type == transport_type) || (!ss.transport_type.HasValue))
                    )
                .ProjectTo<CrmNotifyParamUserViewModel>()
                ;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmNotifyParamUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CrmNotifyParamUserViewModel itemEdit = (CrmNotifyParamUserViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_notify crm_notify = dbContext.crm_notify.Where(ss => ss.user_id == currUser.CabUserId).FirstOrDefault();
            if (crm_notify == null)
            {
                modelState.AddModelError("", "crm_notify == null");
                return null;
            }

            crm_notify_param_user param = dbContext.crm_notify_param_user.Where(ss => ss.user_id == itemEdit.changer_user_id 
                && ss.transport_type == itemEdit.transport_type
                && ss.notify_id == crm_notify.notify_id
                ).FirstOrDefault();

            if (param == null)
            {
                param = new crm_notify_param_user();
                param.is_executer = itemEdit.is_executer.GetValueOrDefault(false);
                param.is_owner = itemEdit.is_owner.GetValueOrDefault(false);
                param.notify_id = crm_notify.notify_id;
                param.transport_type = itemEdit.transport_type.GetValueOrDefault(0);
                param.user_id = itemEdit.changer_user_id.GetValueOrDefault(0);
                dbContext.crm_notify_param_user.Add(param);
            }
            else
            {
                param.is_executer = itemEdit.is_executer.GetValueOrDefault(false);
                param.is_owner = itemEdit.is_owner.GetValueOrDefault(false);
            }


            dbContext.SaveChanges();

            return itemEdit;
        }                
    }
}