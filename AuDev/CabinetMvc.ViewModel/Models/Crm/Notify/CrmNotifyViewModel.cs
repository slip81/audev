﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmNotifyViewModel : AuBaseViewModel
    {

        public CrmNotifyViewModel()
            : base(Enums.MainObjectType.CRM_NOTIFY)
        {            
            //crm_notify
        }

        [Key()]
        [Display(Name = "Код")]
        public int notify_id { get; set; }

        [Display(Name = "Пользователь")]
        public int user_id { get; set; }

        [Display(Name = "Включен ICQ")]
        public bool is_active_icq { get; set; }
        
        [Display(Name = "Включен Email")]
        public bool is_active_email { get; set; }

        [Display(Name = "ICQ")]
        public string icq { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "paramTransport")]
        [JsonIgnore()]
        public IEnumerable<SelectListItem> paramTransport { get; set; }

        [Display(Name = "paramUsers_icq")]
        [JsonIgnore()]
        public IEnumerable<CrmNotifyParamUserViewModel> paramUsers_icq { get; set; }

        [Display(Name = "paramUsers_email")]
        [JsonIgnore()]
        public IEnumerable<CrmNotifyParamUserViewModel> paramUsers_email { get; set; }

        [Display(Name = "paramAttr_icq")]
        [JsonIgnore()]
        public IEnumerable<CrmNotifyParamAttrViewModel> paramAttr_icq { get; set; }

        [Display(Name = "paramAttr_email")]
        [JsonIgnore()]
        public IEnumerable<CrmNotifyParamAttrViewModel> paramAttr_email { get; set; }

        [Display(Name = "paramContent_icq")]
        [JsonIgnore()]
        public IEnumerable<CrmNotifyParamContentViewModel> paramContent_icq { get; set; }

        [Display(Name = "paramContent_email")]
        [JsonIgnore()]
        public IEnumerable<CrmNotifyParamContentViewModel> paramContent_email { get; set; }
    }
}

