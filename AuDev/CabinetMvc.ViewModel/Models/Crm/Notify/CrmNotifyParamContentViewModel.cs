﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmNotifyParamContentViewModel : AuBaseViewModel
    {

        public CrmNotifyParamContentViewModel()
            : base(Enums.MainObjectType.CRM_NOTIFY_PARAM_CONTENT)
        {            
            //vw_crm_notify_param_content
        }

        [Key()]
        [Display(Name = "Код")]
        public int adr { get; set; }

        [Display(Name = "Код")]
        public int notify_id { get; set; }

        [Display(Name = "Хозяин")]
        public int user_id { get; set; }

        [Display(Name = "Тип транспорта")]
        public Nullable<int> transport_type { get; set; }

        [Display(Name = "Атрибут")]
        public Nullable<int> column_id { get; set; }

        [Display(Name = "Атрибут")]
        public string column_name { get; set; }

        [Display(Name = "Атрибут")]
        public string column_name_rus { get; set; }

        [Display(Name = "is_active")]
        public Nullable<bool> is_active { get; set; }
    }
}

