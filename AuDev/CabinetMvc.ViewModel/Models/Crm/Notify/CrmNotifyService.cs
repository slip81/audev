﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmNotifyService : IAuBaseService
    {
        CrmNotifyViewModel CreateNew(ModelStateDictionary modelState);
        IEnumerable<SelectListItem> GetParam_Transport(int user_id, ModelStateDictionary modelState);
    }

    public class CrmNotifyService : AuBaseService, ICrmNotifyService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.crm_notify
                .Where(ss => ss.user_id == id)
                .ProjectTo<CrmNotifyViewModel>()
                .FirstOrDefault()
                ;
        }

        public IEnumerable<SelectListItem> GetParam_Transport(int user_id, ModelStateDictionary modelState)
        {
            List<SelectListItem> result = new List<SelectListItem>();

            var notify = dbContext.crm_notify.Where(ss => ss.user_id == user_id).FirstOrDefault();
            if (notify == null)
            {
                modelState.AddModelError("", "Не найдены параметры уведомлений для пользователя с кодом " + user_id.ToString());
                return result;
            }

            result.Add(new SelectListItem() { Value = ((int)Enums.NotifyTransportTypeEnum.ICQ).EnumName<Enums.NotifyTransportTypeEnum>(), Text = notify.icq, Disabled = !notify.is_active_icq });
            result.Add(new SelectListItem() { Value = ((int)Enums.NotifyTransportTypeEnum.EMAIL).EnumName<Enums.NotifyTransportTypeEnum>(), Text = notify.email, Disabled = !notify.is_active_email });
            
            return result;
        }

        public CrmNotifyViewModel CreateNew( ModelStateDictionary modelState)
        {            
            CrmNotifyViewModel res = new CrmNotifyViewModel();

            crm_notify existing_crm_notify = dbContext.crm_notify.Where(ss => ss.user_id == currUser.CabUserId).FirstOrDefault();
            if (existing_crm_notify != null)
            {
                ModelMapper.Map<crm_notify, CrmNotifyViewModel>(existing_crm_notify, res);
                return res;
            }

            cab_user cab_user = dbContext.cab_user.Where(ss => ss.user_id == currUser.CabUserId).FirstOrDefault();

            crm_notify crm_notify = new crm_notify();
            
            crm_notify.email = cab_user.email;
            crm_notify.icq = cab_user.icq;
            crm_notify.is_active_email = false;
            crm_notify.is_active_icq = false;
            crm_notify.user_id = cab_user.user_id;
            
            dbContext.crm_notify.Add(crm_notify);
            dbContext.SaveChanges();

            ModelMapper.Map<crm_notify, CrmNotifyViewModel>(crm_notify, res);
            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmNotifyViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CrmNotifyViewModel itemEdit = (CrmNotifyViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_notify crm_notify = dbContext.crm_notify.Where(ss => ss.user_id == currUser.CabUserId).FirstOrDefault();
            if (crm_notify == null)
            {
                modelState.AddModelError("", "crm_notify == null");
                return null;
            }

            crm_notify.is_active_email = itemEdit.is_active_email;
            crm_notify.is_active_icq = itemEdit.is_active_icq;
            crm_notify.email = itemEdit.email;
            crm_notify.icq = itemEdit.icq;
            dbContext.SaveChanges();

            return itemEdit;            
        }
   
    }
}