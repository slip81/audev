﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmLogService : IAuBaseService
    {
        void InsertLogCrm(DateTime date_beg, string mess, int? task_id);
    }

    public class CrmLogService : AuBaseService, ICrmLogService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_log_crm
                .Select(ss => new CrmLogViewModel
                {
                    log_id = ss.log_id,
                    date_beg = ss.date_beg,
                    date_end = ss.date_end,
                    user_id = ss.user_id,
                    mess = ss.mess,
                    task_id = ss.task_id,
                    user_name = ss.user_name,
                    full_name = ss.full_name,
                    task_num = ss.task_num,
                    task_name = ss.task_name,
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_log_crm
                .Where(ss => ss.task_id == parent_id)
                .Select(ss => new CrmLogViewModel
                {
                    log_id = ss.log_id,
                    date_beg = ss.date_beg,
                    date_end = ss.date_end,
                    user_id = ss.user_id,
                    mess = ss.mess,
                    task_id = ss.task_id,
                    user_name = ss.user_name,
                    full_name = ss.full_name,
                    task_num = ss.task_num,
                    task_name = ss.task_name,
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_log_crm
                .Where(ss => ss.log_id == id)
                .Select(ss => new CrmLogViewModel
                {
                    log_id = ss.log_id,
                    date_beg = ss.date_beg,
                    date_end = ss.date_end,
                    user_id = ss.user_id,
                    mess = ss.mess,
                    task_id = ss.task_id,
                    user_name = ss.user_name,
                    full_name = ss.full_name,
                    task_num = ss.task_num,
                    task_name = ss.task_name,
                })
                .FirstOrDefault();
        }

        public void InsertLogCrm(DateTime date_beg, string mess, int? task_id)
        {
            DateTime now = DateTime.Now;
            log_crm log_crm = new log_crm();
            log_crm.date_beg = date_beg;
            log_crm.date_end = now;
            log_crm.mess = mess;
            log_crm.task_id = task_id;
            log_crm.user_id = currUser.CabUserId;
            dbContext.log_crm.Add(log_crm);
            dbContext.SaveChanges();           
        }

    }

}
