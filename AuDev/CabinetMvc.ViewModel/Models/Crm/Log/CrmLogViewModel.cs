﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmLogViewModel : AuBaseViewModel
    {
        public CrmLogViewModel()
            : base(Enums.MainObjectType.CRM_LOG)
        {
            
        }

        public CrmLogViewModel(vw_log_crm item)
            : base(Enums.MainObjectType.CRM_LOG)
        {
            log_id = item.log_id;
            date_beg = item.date_beg;
            date_end = item.date_end;
            user_id = item.user_id;
            mess = item.mess;
            task_id = item.task_id;
            user_name = item.user_name;
            full_name = item.full_name;
            task_num = item.task_num;
            task_name = item.task_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public long log_id { get; set; }

        [Display(Name = "Дата события")]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "Дата окончания")]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "Сотрудник")]
        public Nullable<int> user_id { get; set; }

        [Display(Name = "Сообщение")]
        [AllowHtml()]
        public string mess { get; set; }

        [Display(Name = "Код задачи")]
        public Nullable<int> task_id { get; set; }

        [Display(Name = "Сотрудник")]
        public string user_name { get; set; }

        [Display(Name = "Сотрудник")]
        public string full_name { get; set; }

        [Display(Name = "№ задачи")]
        public string task_num { get; set; }

        [Display(Name = "Задача")]
        [AllowHtml()]
        public string task_name { get; set; }
    }
}