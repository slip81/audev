﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmTaskRelViewModel : AuBaseViewModel
    {
        public CrmTaskRelViewModel()
            : base(Enums.MainObjectType.CRM_TASK_REL)
        {
            //
        }

        public CrmTaskRelViewModel(vw_crm_task_rel item)
            : base(Enums.MainObjectType.CRM_TASK_REL)
        {
            item_id = item.item_id;
            task_id = item.task_id;
            child_task_num = item.child_task_num;
            child_task_id = item.child_task_id;
            rel_type_id = item.rel_type_id;
            rel_type_name = item.rel_type_name;

            task_name = item.task_name;
            task_text = item.task_text;
            project_id = item.project_id;
            module_id = item.module_id;
            module_part_id = item.module_part_id;
            module_version_id = item.module_version_id;
            client_id = item.client_id;
            priority_id = item.priority_id;
            state_id = item.state_id;
            owner_user_id = item.owner_user_id;
            exec_user_id = item.exec_user_id;
            task_num = item.task_num;
            crt_date = item.crt_date;
            required_date = item.required_date;
            repair_date = item.repair_date;
            repair_version_id = item.repair_version_id;
            group_id = item.group_id;            
            prepared_user_id = item.prepared_user_id;
            fixed_user_id = item.fixed_user_id;
            approved_user_id = item.approved_user_id;
            assigned_user_id = item.assigned_user_id;

            project_name = item.priority_name;
            module_name = item.module_name;
            module_part_name = item.module_part_name;
            module_version_name = item.module_version_name;
            client_name = item.client_name;
            priority_name = item.priority_name;
            state_name = item.state_name;
            owner_user_name = item.owner_user_name;
            exec_user_name = item.exec_user_name;
            repair_version_name = item.repair_version_name;
            task_num_int = item.task_num_int;
            prepared_user_name = item.prepared_user_name;
            fixed_user_name = item.fixed_user_name;
            approved_user_name = item.approved_user_name;
            assigned_user_name = item.assigned_user_name;
            group_name = item.group_name;
            
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        public int task_id { get; set; }
        public Nullable<int> child_task_num { get; set; }
        public int rel_type_id { get; set; }
        public string rel_type_name { get; set; }
        public int child_task_id { get; set; }
        public string task_name { get; set; }
        public string task_text { get; set; }
        public int project_id { get; set; }
        public int module_id { get; set; }
        public int module_part_id { get; set; }
        public int module_version_id { get; set; }
        public int client_id { get; set; }
        public int priority_id { get; set; }
        public int state_id { get; set; }
        public int owner_user_id { get; set; }
        public int exec_user_id { get; set; }
        public string task_num { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public Nullable<System.DateTime> required_date { get; set; }
        public Nullable<System.DateTime> repair_date { get; set; }
        public Nullable<int> repair_version_id { get; set; }
        public string client_name { get; set; }
        public string module_name { get; set; }
        public string module_part_name { get; set; }
        public string module_version_name { get; set; }
        public string priority_name { get; set; }
        public string project_name { get; set; }
        public string state_name { get; set; }
        public string owner_user_name { get; set; }
        public string exec_user_name { get; set; }
        public string repair_version_name { get; set; }
        public int task_num_int { get; set; }
        public int group_id { get; set; }        
        public int prepared_user_id { get; set; }
        public int fixed_user_id { get; set; }
        public int approved_user_id { get; set; }
        public string prepared_user_name { get; set; }
        public string fixed_user_name { get; set; }
        public string approved_user_name { get; set; }
        public int assigned_user_id { get; set; }
        public string assigned_user_name { get; set; }
        public string group_name { get; set; }
    }
}