﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmTaskRelTypeService : IAuBaseService
    {
        //
    }

    public class CrmTaskRelTypeService : AuBaseService, ICrmTaskRelTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_task_rel_type
                .Select(ss => new CrmTaskRelTypeViewModel
                {
                    type_id = ss.type_id,
                    type_name = ss.type_name,
                });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmTaskRelTypeViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmTaskRelTypeViewModel itemAdd = (CrmTaskRelTypeViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_task_rel_type crm_task_rel_type = new crm_task_rel_type();
            int type_id_new = 1;
            var crm_task_rel_type_max = dbContext.crm_task_rel_type.OrderByDescending(ss => ss.type_id).FirstOrDefault();
            if (crm_task_rel_type_max != null)
                type_id_new = crm_task_rel_type_max.type_id + 1;
            crm_task_rel_type.type_id = type_id_new;
            crm_task_rel_type.type_name = itemAdd.type_name;
            dbContext.crm_task_rel_type.Add(crm_task_rel_type);
            dbContext.SaveChanges();

            return new CrmTaskRelTypeViewModel(crm_task_rel_type);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmTaskRelTypeViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmTaskRelTypeViewModel itemEdit = (CrmTaskRelTypeViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_task_rel_type = dbContext.crm_task_rel_type.Where(ss => ss.type_id == itemEdit.type_id).FirstOrDefault();
            if (crm_task_rel_type == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.type_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmTaskRelTypeViewModel(crm_task_rel_type);

            crm_task_rel_type.type_name = itemEdit.type_name;
            dbContext.SaveChanges();

            return new CrmTaskRelTypeViewModel(crm_task_rel_type); ;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmTaskRelTypeViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmTaskRelTypeViewModel)item).type_id;

            var crm_task_rel_type = dbContext.crm_task_rel_type.Where(ss => ss.type_id == id).FirstOrDefault();
            if (crm_task_rel_type == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.crm_task_rel.Where(ss => ss.rel_type_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют связи задач с данным типом связи");
                return false;
            }

            dbContext.crm_task_rel_type.Remove(crm_task_rel_type);
            dbContext.SaveChanges();

            return true;
        }
    }
}
