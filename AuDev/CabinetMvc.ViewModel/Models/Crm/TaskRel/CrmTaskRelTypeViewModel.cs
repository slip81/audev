﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmTaskRelTypeViewModel : AuBaseViewModel
    {
        public CrmTaskRelTypeViewModel()
            : base(Enums.MainObjectType.CRM_TASK_REL_TYPE)
        {
            //
        }

        public CrmTaskRelTypeViewModel(crm_task_rel_type item)
            : base(Enums.MainObjectType.CRM_TASK_REL_TYPE)
        {
            type_id = item.type_id;
            type_name = item.type_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public int type_id { get; set; }
        [Display(Name = "Название")]
        public string type_name { get; set; }
    }
}