﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmTaskRelService : IAuBaseService
    {
        //
    }

    public class CrmTaskRelService : AuBaseService, ICrmTaskRelService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_crm_task_rel
                .Where(ss => ss.task_id == parent_id || ss.child_task_id == parent_id)
                .Select(ss => new CrmTaskRelViewModel
                {
                    item_id = ss.item_id,
                    task_id = ss.task_id,
                    child_task_num = ss.child_task_num,
                    child_task_id = ss.child_task_id,
                    rel_type_id = ss.rel_type_id,
                    rel_type_name = ss.rel_type_name,

                    task_name = ss.task_name,
                    task_text = ss.task_text,
                    project_id = ss.project_id,
                    module_id = ss.module_id,
                    module_part_id = ss.module_part_id,
                    module_version_id = ss.module_version_id,
                    client_id = ss.client_id,
                    priority_id = ss.priority_id,
                    state_id = ss.state_id,
                    owner_user_id = ss.owner_user_id,
                    exec_user_id = ss.exec_user_id,
                    task_num = ss.task_num,
                    crt_date = ss.crt_date,
                    required_date = ss.required_date,
                    repair_date = ss.repair_date,
                    repair_version_id = ss.repair_version_id,
                    group_id = ss.group_id,                    
                    prepared_user_id = ss.prepared_user_id,
                    fixed_user_id = ss.fixed_user_id,
                    approved_user_id = ss.approved_user_id,
                    assigned_user_id = ss.assigned_user_id,

                    project_name = ss.priority_name,
                    module_name = ss.module_name,
                    module_part_name = ss.module_part_name,
                    module_version_name = ss.module_version_name,
                    client_name = ss.client_name,
                    priority_name = ss.priority_name,
                    state_name = ss.state_name,
                    owner_user_name = ss.owner_user_name,
                    exec_user_name = ss.exec_user_name,
                    repair_version_name = ss.repair_version_name,
                    task_num_int = ss.task_num_int,
                    prepared_user_name = ss.prepared_user_name,
                    fixed_user_name = ss.fixed_user_name,
                    approved_user_name = ss.approved_user_name,
                    assigned_user_name = ss.assigned_user_name,
                    group_name = ss.group_name,
                });
        }

    }
}
