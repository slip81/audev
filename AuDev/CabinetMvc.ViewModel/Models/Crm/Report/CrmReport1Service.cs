﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmReport1Service : IAuBaseService
    {
        IEnumerable<CrmReport1ViewModel> GetReport(int user_id, DateTime? date_beg, DateTime? date_end, ModelStateDictionary modelState);
    }

    public class CrmReport1Service : AuBaseService, ICrmReport1Service
    {
        public IEnumerable<CrmReport1ViewModel> GetReport(int user_id, DateTime? date_beg, DateTime? date_end, ModelStateDictionary modelState)
        {
            string date_beg_str = date_beg.HasValue ? ("'" + ((DateTime)date_beg).ToString("yyyy-MM-dd") + "'") : "null";
            string date_end_str = date_end.HasValue ? ("'" + ((DateTime)date_end).ToString("yyyy-MM-dd") + "'") : "null";
            List<CrmReport1ViewModel> res = dbContext.Database.SqlQuery<CrmReport1ViewModel>("select * from cabinet.report_crm_task1(" + user_id.ToString() + ", " + date_beg_str + "," + date_end_str + ")").ToList();
            return res;
        }
    }
}