﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmReport1ViewModel : AuBaseViewModel
    {

        public CrmReport1ViewModel()
            : base(Enums.MainObjectType.CRM_REPORT1)
        {            
            //
        }

        [Key()]
        [Display(Name = "Код")]
        public long id { get; set; }

        [Display(Name = "Пользователь")]
        public int? user_id { get; set; }

        [Display(Name = "Пользователь")]
        public string user_name { get; set; }

        [Display(Name = "Дата начала")]
        public DateTime? date_beg { get; set; }

        [Display(Name = "Дата окончания")]
        public DateTime? date_end { get; set; }

        [Display(Name = "Код задачи")]
        public int? task_id { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }

        [Display(Name = "№ задачи")]
        public string task_num { get; set; }

        [Display(Name = "Кратко")]
        public string task_name { get; set; }

        [Display(Name = "Плановая дата")]
        public DateTime? required_date { get; set; }

        [Display(Name = "Фактическая дата")]
        public DateTime? repair_date { get; set; }

        [Display(Name = "Фактическая дата")]
        public string state_name { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Проект")]
        public string project_name { get; set; }

        [Display(Name = "Модуль")]
        public string module_name { get; set; }

        [Display(Name = "Версия обнаружения")]
        public string module_version_name { get; set; }

        [Display(Name = "Версия исправления")]
        public string repair_version_name { get; set; }

        [Display(Name = "Приоритет")]
        public string priority_name { get; set; }

        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [Display(Name = "Подзадачи")]
        public string subtasks_text { get; set; }

        [Display(Name = "Комментарии")]
        public string comments_text { get; set; }        
    }
}

