﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmSchedulerViewModel : AuBaseViewModel, Kendo.Mvc.UI.ISchedulerEvent
    {
        public CrmSchedulerViewModel()
            : base(Enums.MainObjectType.CRM_SCHEDULER)
        {
            
        }

        public CrmSchedulerViewModel(crm_event item)
            : base(Enums.MainObjectType.CRM_SCHEDULER)
        {
            event_id = item.event_id;
            task_id = item.task_id;
            task_name = item.event_name;
            task_text = item.event_text;
            exec_user_id = item.exec_user_id;
            is_done = item.is_done;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            is_canceled = item.is_canceled;
            date_plan = item.date_plan;
            event_progress = item.event_progress;
            event_result = item.event_result;
            task_state_id = item.task_state_id;
            date_fact = item.date_fact;
            event_group_id = item.event_group_id;
            RecurrenceID = null;
            IsDateChanged = true;
            is_overdue = false;
            //
            Description = item.event_text;
            End = item.date_end;
            EndTimezone = null;
            IsAllDay = true;
            RecurrenceException = null;
            RecurrenceRule = null;
            Start = item.date_beg;
            StartTimezone = null;
            Title = item.event_name;
            file_exists = false;
        }

        public CrmSchedulerViewModel(vw_crm_event item)
            : base(Enums.MainObjectType.CRM_SCHEDULER)
        {
            event_id = item.event_id;
            task_id = item.task_id;
            exec_user_id = item.exec_user_id;
            is_done = item.is_done;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            is_canceled = item.is_canceled;
            date_plan = item.date_plan;
            event_progress = item.event_progress;
            event_result = item.event_result;
            task_state_id = item.task_state_id;
            date_fact = item.date_fact;
            event_group_id = item.event_group_id;
            task_num = item.task_num;
            //task_name = item.task_name;
            //task_text = item.task_text;
            task_name = item.event_name;
            task_text = item.event_text;
            exec_user_name = item.exec_user_name;
            RecurrenceID = null;
            IsDateChanged = true;
            event_group_name = item.event_group_name;
            state_name = item.state_name;
            exec_user_role_id = item.exec_user_role_id;
            ignore_date_plan = item.ignore_date_plan;
            is_done_user_role = item.is_done_user_role;
            is_overdue = item.is_overdue;
            //
            assigned_user_name = item.assigned_user_name;
            owner_user_name = item.owner_user_name;
            client_name = item.client_name;
            fin_cost = item.fin_cost;
            project_name = item.project_name;
            module_name = item.module_name;
            module_version_name = item.module_version_name;
            upd_date = item.upd_date;
            priority_name = item.priority_name;
            file_exists = item.file_exists;
            //
            Description = item.event_text;
            End = item.date_end;
            EndTimezone = null;
            IsAllDay = true;
            RecurrenceException = null;
            RecurrenceRule = null;
            Start = item.date_beg;
            StartTimezone = null;
            Title = item.event_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public int event_id { get; set; }

        [Display(Name = "task_id")]
        public Nullable<int> task_id { get; set; }

        [Display(Name = "exec_user_id")]
        public Nullable<int> exec_user_id { get; set; }

        [Display(Name = "is_done")]
        public bool is_done { get; set; }

        [Display(Name = "crt_date")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "crt_user")]
        public string crt_user { get; set; }

        [Display(Name = "is_canceled")]
        public bool is_canceled { get; set; }

        [Display(Name = "date_plan")]
        public Nullable<System.DateTime> date_plan { get; set; }

        [Display(Name = "event_progress")]
        [AllowHtml()]
        public string event_progress { get; set; }

        [Display(Name = "event_result")]
        [AllowHtml()]
        public string event_result { get; set; }

        [Display(Name = "task_state_id")]
        public int task_state_id { get; set; }

        [Display(Name = "date_fact")]
        public Nullable<System.DateTime> date_fact { get; set; }

        [Display(Name = "event_group_id")]
        public Nullable<int> event_group_id { get; set; }

        [Display(Name = "task_num")]
        public string task_num { get; set; }

        [Display(Name = "task_name")]
        [AllowHtml()]
        public string task_name { get; set; }

        [Display(Name = "task_text")]
        [AllowHtml()]
        public string task_text { get; set; }

        [Display(Name = "exec_user_name")]
        public string exec_user_name { get; set; }

        [Display(Name = "RecurrenceID")]
        public Nullable<int> RecurrenceID { get; set; }

        [Display(Name = "IsDateChanged")]
        public bool IsDateChanged { get; set; }

        [Display(Name = "state_name")]
        public string state_name { get; set; }

        [Display(Name = "event_group_name")]
        public string event_group_name { get; set; }

        [Display(Name = "exec_user_role_id")]
        public Nullable<int> exec_user_role_id { get; set; }

        [Display(Name = "ignore_date_plan")]
        public Nullable<bool> ignore_date_plan { get; set; }

        [Display(Name = "is_done_user_role")]
        public bool is_done_user_role { get; set; }

        [Display(Name = "is_overdue")]
        public bool is_overdue { get; set; }

        [Display(Name = "Ответственный")]
        public string assigned_user_name { get; set; }

        [Display(Name = "Автор")]
        public string owner_user_name { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }
        
        [Display(Name = "Цена")]
        public Nullable<decimal> fin_cost { get; set; }

        [Display(Name = "Проект")]
        public string project_name { get; set; }

        [Display(Name = "Модуль")]
        public string module_name { get; set; }

        [Display(Name = "Версия модуля")]
        public string module_version_name { get; set; }

        [Display(Name = "Дата изменения")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Приоритет")]
        public string priority_name { get; set; }

        [Display(Name = "С файлами")]
        public bool file_exists { get; set; }

        // ISchedulerEvent        
        [AllowHtml()]
        public string Description { get; set; }
        public DateTime End { get; set; }
        public string EndTimezone { get; set; }
        public bool IsAllDay { get; set; }
        public string RecurrenceException { get; set; }
        public string RecurrenceRule { get; set; }
        public DateTime Start { get; set; }
        public string StartTimezone { get; set; }
        [AllowHtml()]
        public string Title { get; set; }
    }
}