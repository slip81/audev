﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmSchedulerService : IAuBaseService
    {
        IQueryable<CrmSchedulerViewModel> GetList_byFilter(bool show_done, bool show_canceled, int? user_id, bool overdue_only, bool priority_control, DateTime? date_beg, DateTime? date_end, bool canceled_only);
        CrmSchedulerViewModel GetItem_byTask(int task_id);
        IQueryable<DateTime> GetDates_OverdueTasks();
    }

    public class CrmSchedulerService : AuBaseService, ICrmSchedulerService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_crm_event
                .Select(ss => new CrmSchedulerViewModel
                {
                    event_id = ss.event_id,
                    task_id = ss.task_id,
                    exec_user_id = ss.exec_user_id,
                    is_done = ss.is_done,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    is_canceled = ss.is_canceled,
                    date_plan = ss.date_plan,
                    event_progress = ss.event_progress,
                    event_result = ss.event_result,
                    task_state_id = ss.task_state_id,
                    date_fact = ss.date_fact,
                    task_num = ss.task_num,
                    //task_name = ss.task_name,
                    //task_text = ss.task_text,
                    task_name = ss.event_name,
                    task_text = ss.event_text,
                    exec_user_name = ss.exec_user_name,
                    IsDateChanged = true,
                    event_group_id = ss.event_group_id,
                    event_group_name = ss.event_group_name,
                    state_name = ss.state_name,
                    exec_user_role_id = ss.exec_user_role_id,
                    ignore_date_plan = ss.ignore_date_plan,
                    is_done_user_role = ss.is_done_user_role,
                    is_overdue = ss.is_overdue,
                    //
                    assigned_user_name = ss.assigned_user_name,
                    owner_user_name = ss.owner_user_name,
                    client_name = ss.client_name,
                    fin_cost = ss.fin_cost,
                    project_name = ss.project_name,
                    module_name = ss.module_name,
                    module_version_name = ss.module_version_name,
                    upd_date = ss.upd_date,
                    priority_name = ss.priority_name,
                    file_exists = ss.file_exists,
                    //
                    Description = ss.event_text,
                    End = ss.date_end,
                    EndTimezone = null,
                    IsAllDay = true,
                    RecurrenceException = null,
                    RecurrenceRule = null,
                    Start = ss.date_beg,
                    StartTimezone = null,
                    Title = ss.event_name,
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_crm_event
                .Where(ss => ss.exec_user_id == parent_id)
                .Select(ss => new CrmSchedulerViewModel
                {
                    event_id = ss.event_id,
                    task_id = ss.task_id,
                    exec_user_id = ss.exec_user_id,
                    is_done = ss.is_done,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    is_canceled = ss.is_canceled,
                    date_plan = ss.date_plan,
                    event_progress = ss.event_progress,
                    event_result = ss.event_result,
                    task_state_id = ss.task_state_id,
                    date_fact = ss.date_fact,
                    task_num = ss.task_num,
                    //task_name = ss.task_name,
                    //task_text = ss.task_text,
                    task_name = ss.event_name,
                    task_text = ss.event_text,
                    exec_user_name = ss.exec_user_name,
                    IsDateChanged = true,
                    event_group_id = ss.event_group_id,
                    event_group_name = ss.event_group_name,
                    state_name = ss.state_name,
                    exec_user_role_id = ss.exec_user_role_id,
                    ignore_date_plan = ss.ignore_date_plan,
                    is_done_user_role = ss.is_done_user_role,
                    is_overdue = ss.is_overdue,
                    //
                    assigned_user_name = ss.assigned_user_name,
                    owner_user_name = ss.owner_user_name,
                    client_name = ss.client_name,
                    fin_cost = ss.fin_cost,
                    project_name = ss.project_name,
                    module_name = ss.module_name,
                    module_version_name = ss.module_version_name,
                    upd_date = ss.upd_date,
                    priority_name = ss.priority_name,
                    file_exists = ss.file_exists,
                    //
                    Description = ss.event_text,
                    End = ss.date_end,
                    EndTimezone = null,
                    IsAllDay = true,
                    RecurrenceException = null,
                    RecurrenceRule = null,
                    Start = ss.date_beg,
                    StartTimezone = null,
                    Title = ss.event_name,                   
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_crm_event
                .Where(ss => ss.event_id == id)
                .Select(ss => new CrmSchedulerViewModel
                {
                    event_id = ss.event_id,
                    task_id = ss.task_id,
                    exec_user_id = ss.exec_user_id,
                    is_done = ss.is_done,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    is_canceled = ss.is_canceled,
                    date_plan = ss.date_plan,
                    event_progress = ss.event_progress,
                    event_result = ss.event_result,
                    task_state_id = ss.task_state_id,
                    date_fact = ss.date_fact,
                    task_num = ss.task_num,
                    //task_name = ss.task_name,
                    //task_text = ss.task_text,
                    task_name = ss.event_name,
                    task_text = ss.event_text,
                    exec_user_name = ss.exec_user_name,
                    IsDateChanged = true,
                    event_group_id = ss.event_group_id,
                    event_group_name = ss.event_group_name,
                    state_name = ss.state_name,
                    exec_user_role_id = ss.exec_user_role_id,
                    ignore_date_plan = ss.ignore_date_plan,
                    is_done_user_role = ss.is_done_user_role,
                    is_overdue = ss.is_overdue,
                    //
                    assigned_user_name = ss.assigned_user_name,
                    owner_user_name = ss.owner_user_name,
                    client_name = ss.client_name,
                    fin_cost = ss.fin_cost,
                    project_name = ss.project_name,
                    module_name = ss.module_name,
                    module_version_name = ss.module_version_name,
                    upd_date = ss.upd_date,
                    priority_name = ss.priority_name,
                    file_exists = ss.file_exists,
                    //
                    Description = ss.event_text,
                    End = ss.date_end,
                    EndTimezone = null,
                    IsAllDay = true,
                    RecurrenceException = null,
                    RecurrenceRule = null,
                    Start = ss.date_beg,
                    StartTimezone = null,
                    Title = ss.event_name,                    
                })
                .FirstOrDefault();
        }

        public CrmSchedulerViewModel GetItem_byTask(int task_id)
        {
            return dbContext.vw_crm_event
                .Where(ss => ss.task_id == task_id)
                .OrderByDescending(ss => ss.event_id)
                .Select(ss => new CrmSchedulerViewModel
                {
                    event_id = ss.event_id,
                    task_id = ss.task_id,
                    exec_user_id = ss.exec_user_id,
                    is_done = ss.is_done,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    is_canceled = ss.is_canceled,
                    date_plan = ss.date_plan,
                    event_progress = ss.event_progress,
                    event_result = ss.event_result,
                    task_state_id = ss.task_state_id,
                    date_fact = ss.date_fact,
                    task_num = ss.task_num,
                    //task_name = ss.task_name,
                    //task_text = ss.task_text,
                    task_name = ss.event_name,
                    task_text = ss.event_text,
                    exec_user_name = ss.exec_user_name,
                    IsDateChanged = true,
                    event_group_id = ss.event_group_id,
                    event_group_name = ss.event_group_name,
                    state_name = ss.state_name,
                    exec_user_role_id = ss.exec_user_role_id,
                    ignore_date_plan = ss.ignore_date_plan,
                    is_done_user_role = ss.is_done_user_role,
                    is_overdue = ss.is_overdue,
                    //
                    assigned_user_name = ss.assigned_user_name,
                    owner_user_name = ss.owner_user_name,
                    client_name = ss.client_name,
                    fin_cost = ss.fin_cost,
                    project_name = ss.project_name,
                    module_name = ss.module_name,
                    module_version_name = ss.module_version_name,
                    upd_date = ss.upd_date,
                    priority_name = ss.priority_name,
                    file_exists = ss.file_exists,
                    //
                    Description = ss.event_text,
                    End = ss.date_end,
                    EndTimezone = null,
                    IsAllDay = true,
                    RecurrenceException = null,
                    RecurrenceRule = null,
                    Start = ss.date_beg,
                    StartTimezone = null,
                    Title = ss.event_name,
                })
                .FirstOrDefault();
        }

        public IQueryable<CrmSchedulerViewModel> GetList_byFilter(bool show_done, bool show_canceled, int? user_id, bool overdue_only, bool priority_control, DateTime? date_beg, DateTime? date_end, bool canceled_only)
        {
            bool is_period_specified = (date_beg.HasValue) && (date_end.HasValue);
            var user_id_int = user_id.GetValueOrDefault(-1);
            return dbContext.vw_crm_event
                .Where(ss => (1 == 1)
                    && ((show_done) || ((!show_done) && (!ss.is_done)))
                    && ((show_canceled) || ((!show_canceled) && (!ss.is_canceled)))
                    && ((user_id_int == -1) || ((user_id_int >= 0) && (ss.exec_user_id == user_id_int)))                    
                    //&& ((!overdue_only) || ((overdue_only) && (ss.date_plan.HasValue) && (ss.date_plan < DateTime.Today) && (!ss.is_done_user_role) && (!(bool)ss.ignore_date_plan)))
                    && ((!overdue_only) || ((overdue_only) && (ss.is_overdue)))
                    && ((!canceled_only) || ((canceled_only) && (ss.is_canceled)))
                    && ((!priority_control) || ((priority_control) && (ss.priority_control == true)))
                    //&& ((!date_beg.HasValue) || ((date_beg.HasValue) && (ss.date_beg >= date_beg)))
                    //&& ((!date_end.HasValue) || ((date_end.HasValue) && (ss.date_beg <= date_end)))
                    && (((is_period_specified) && ((date_beg >= ss.date_beg && date_beg <= ss.date_end) || (date_end >= ss.date_beg && date_end <= ss.date_end) || (ss.date_beg >= date_beg && ss.date_beg <= date_end) || (ss.date_end >= date_beg && ss.date_end <= date_end))) || (!is_period_specified))
                    
                )
                .Select(ss => new CrmSchedulerViewModel
                {
                    event_id = ss.event_id,
                    task_id = ss.task_id,
                    exec_user_id = ss.exec_user_id,
                    is_done = ss.is_done,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    is_canceled = ss.is_canceled,
                    date_plan = ss.date_plan,
                    event_progress = ss.event_progress,
                    event_result = ss.event_result,
                    task_state_id = ss.task_state_id,
                    date_fact = ss.date_fact,
                    task_num = ss.task_num,
                    //task_name = ss.task_name,
                    //task_text = ss.task_text,
                    task_name = ss.event_name,
                    task_text = ss.event_text,
                    exec_user_name = ss.exec_user_name,
                    IsDateChanged = true,
                    event_group_id = ss.event_group_id,
                    event_group_name = ss.event_group_name,
                    state_name = ss.state_name,
                    exec_user_role_id = ss.exec_user_role_id,
                    ignore_date_plan = ss.ignore_date_plan,
                    is_done_user_role = ss.is_done_user_role,
                    is_overdue = ss.is_overdue,
                    //
                    assigned_user_name = ss.assigned_user_name,
                    owner_user_name = ss.owner_user_name,
                    client_name = ss.client_name,
                    fin_cost = ss.fin_cost,
                    project_name = ss.project_name,
                    module_name = ss.module_name,
                    module_version_name = ss.module_version_name,
                    upd_date = ss.upd_date,
                    priority_name = ss.priority_name,
                    file_exists = ss.file_exists,
                    //
                    Description = ss.event_text,
                    End = ss.date_end,
                    EndTimezone = null,
                    IsAllDay = true,
                    RecurrenceException = null,
                    RecurrenceRule = null,
                    Start = ss.date_beg,
                    StartTimezone = null,
                    Title = ss.event_name,
                });
        }

        public IQueryable<DateTime> GetDates_OverdueTasks()
        {            
            var dates = dbContext.vw_crm_event_overdue_dates.Select(ss => ss.event_date);            
            return dates;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmSchedulerViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmSchedulerViewModel itemAdd = (CrmSchedulerViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            string mess_notify = "";

            crm_event crm_event = new crm_event();
            crm_task existing_task = null;

            if (!String.IsNullOrEmpty(itemAdd.task_num))
            {
                string task_num = itemAdd.task_num.Contains("[") ? (itemAdd.task_num.Substring(0, itemAdd.task_num.IndexOf("[")).Trim()) : itemAdd.task_num;
                existing_task = dbContext.crm_task.Where(ss => ss.task_num.Trim().ToLower().Equals(task_num.Trim().ToLower())).FirstOrDefault();
            }

            if (existing_task != null)
            {
                crm_event.event_name = existing_task.task_name;
                crm_event.event_text = existing_task.task_text;
                crm_event.date_plan = existing_task.required_date;
                crm_event.task_id = existing_task.task_id;
                crm_event.task_state_id = existing_task.state_id;             
            }
            else
            {
                crm_event.event_name = itemAdd.Description;
                crm_event.event_text = itemAdd.Description;
                crm_event.date_plan = itemAdd.date_plan;                
                crm_event.task_id = null;                
            }

            crm_event.crt_date = now;
            crm_event.crt_user = currUser.CabUserName;
            crm_event.date_beg = itemAdd.Start;
            crm_event.date_end = itemAdd.End.AddDays(-1);
            //crm_event.date_end = itemAdd.End;
            crm_event.exec_user_id = itemAdd.exec_user_id;
            crm_event.event_group_id = itemAdd.event_group_id;
            crm_event.is_done = false;
            crm_event.is_canceled = false;
            crm_event.date_fact = null;

            dbContext.crm_event.Add(crm_event);

            mess_notify += "Добавлено событие в Календаре: " + ((existing_task != null ? "[" + existing_task.task_num + "] " + crm_event.event_name : crm_event.event_name).CutToLengthWithDots(50));
            mess_notify += System.Environment.NewLine;
            mess_notify += "Дата события: " + crm_event.date_beg.ToString("dd.MM.yyyy");
            mess_notify += System.Environment.NewLine;
            if (crm_event.date_plan.HasValue)
            {
                mess_notify += "Плановая дата: " + ((DateTime)crm_event.date_plan).ToString("dd.MM.yyyy");
                mess_notify += System.Environment.NewLine;
            }
            
            mess_notify += "Автор изменений: " + currUser.CabUserName;
            mess_notify += System.Environment.NewLine;
            mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
            mess_notify += System.Environment.NewLine;

            notify notify = null;

            List<int> notifyTargetList = dbContext.vw_crm_notify_param_user.Where(ss =>
                ((ss.changer_user_id == crm_event.exec_user_id) && (ss.is_executer == true) && (ss.user_id != currUser.CabUserId))
                )
                .Select(ss => ss.user_id)
                .Distinct()
                .ToList();
            if ((notifyTargetList != null) && (notifyTargetList.Count > 0))
            {
                foreach (var notifyTarget in notifyTargetList)
                {
                    notify = new notify();
                    notify.crt_date = now;
                    notify.message = mess_notify;
                    notify.message_sent = false;
                    notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                    notify.sent_date = null;
                    notify.target_user_id = notifyTarget;
                    notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                    dbContext.notify.Add(notify);
                }
            }

            /*
            // уведомление исполнителю
            if ((crm_event.exec_user_id > 0) && (crm_event.exec_user_id != currUser.CabUserId))
            {
                notify = new notify();
                notify.crt_date = now;
                notify.message = mess_notify;
                notify.message_sent = false;
                notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                notify.sent_date = null;
                notify.target_user_id = crm_event.exec_user_id;
                dbContext.notify.Add(notify);
            }
            */

            updatePlannerEvent_forCrmEvent(crm_event, true, false);

            dbContext.SaveChanges();            

            var vw_crm_event = dbContext.vw_crm_event.Where(ss => ss.event_id == crm_event.event_id).FirstOrDefault();
            return new CrmSchedulerViewModel(vw_crm_event);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmSchedulerViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmSchedulerViewModel itemEdit = (CrmSchedulerViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_event = dbContext.crm_event.Where(ss => ss.event_id == itemEdit.event_id).FirstOrDefault();
            if (crm_event == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.event_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmSchedulerViewModel(crm_event);

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            DateTime date_end_new = today;
            string mess_notify = "";
            string mess_notify_detail = "";
            crm_task existing_task = null;

            if (itemEdit.task_id.GetValueOrDefault(0) > 0)
            {
                existing_task = dbContext.crm_task.Where(ss => ss.task_id == itemEdit.task_id).FirstOrDefault();
            }
            
            if (itemEdit.IsDateChanged)
            {
                date_end_new = itemEdit.End.AddDays(-1);
                //crm_event.date_end = itemEdit.End.AddDays(-1);
            }
            else
            {
                date_end_new = itemEdit.End;
                //crm_event.date_end = itemEdit.End;
            }
            
            if (crm_event.date_beg != itemEdit.Start)
            {
                mess_notify_detail += "Смена даты начала события с " + crm_event.date_beg.ToString("dd.MM.yyyy") + " на " + itemEdit.Start.ToString("dd.MM.yyyy");
                mess_notify_detail += System.Environment.NewLine;
                crm_event.date_beg = itemEdit.Start;
            }
            if (crm_event.date_end != date_end_new)
            {
                mess_notify_detail += "Смена даты окончания события с " + crm_event.date_end.ToString("dd.MM.yyyy") + " на " + date_end_new.ToString("dd.MM.yyyy");
                mess_notify_detail += System.Environment.NewLine;
                crm_event.date_end = date_end_new;
            }

            if (existing_task != null)
            {
                if (crm_event.event_name != existing_task.task_name)
                {
                    mess_notify_detail += "Смена заголовка события";
                    mess_notify_detail += System.Environment.NewLine;
                    crm_event.event_name = existing_task.task_name;
                }
                if (crm_event.event_text != existing_task.task_text)
                {
                    mess_notify_detail += "Смена текста события";
                    mess_notify_detail += System.Environment.NewLine;
                    crm_event.event_text = existing_task.task_text;
                }
            }
            else
            {
                if (crm_event.event_name != itemEdit.Description)
                {
                    mess_notify_detail += "Смена заголовка события";
                    mess_notify_detail += System.Environment.NewLine;
                    crm_event.event_name = itemEdit.Description;
                }
                if (crm_event.event_text != itemEdit.Description)
                {
                    mess_notify_detail += "Смена текста события";
                    mess_notify_detail += System.Environment.NewLine;
                    crm_event.event_text = itemEdit.Description;
                }
            }

            if (crm_event.event_progress != itemEdit.event_progress)
            {
                mess_notify_detail += "Смена хода выполнения события";
                mess_notify_detail += System.Environment.NewLine;
                crm_event.event_progress = itemEdit.event_progress;
            }
            if (crm_event.event_result != itemEdit.event_result)
            {
                mess_notify_detail += "Смена результата выполнения события";
                mess_notify_detail += System.Environment.NewLine;
                crm_event.event_result = itemEdit.event_result;
            }
            if (crm_event.exec_user_id != itemEdit.exec_user_id)
            {
                mess_notify_detail += "Смена исполнителя события";
                mess_notify_detail += System.Environment.NewLine;
                crm_event.exec_user_id = itemEdit.exec_user_id;
            }
            if (crm_event.event_group_id != itemEdit.event_group_id)
            {
                mess_notify_detail += "Смена приоритета события";
                mess_notify_detail += System.Environment.NewLine;
                crm_event.event_group_id = itemEdit.event_group_id;
            }

            string crm_event_curr_state = crm_event.is_done ? "Выполнено" : (crm_event.is_canceled ? "Отменено" : "Автивно");
            string crm_event_new_state = itemEdit.is_done ? "Выполнено" : (itemEdit.is_canceled ? "Отменено" : "Автивно");
            if ((crm_event.is_done != itemEdit.is_done) || (crm_event.is_canceled != itemEdit.is_canceled))
            {
                mess_notify_detail += "Смена статуса события с " + crm_event_curr_state + " на " + crm_event_new_state;              
                mess_notify_detail += System.Environment.NewLine;
                crm_event.is_done = itemEdit.is_done;
                crm_event.is_canceled = itemEdit.is_canceled;
            }

            if (crm_event.date_plan != itemEdit.date_plan)
            {
                mess_notify_detail += "Смена плановой даты события с " + (crm_event.date_plan.HasValue ? ((DateTime)crm_event.date_plan).ToString("dd.MM.yyyy") : "-") + " на " + (itemEdit.date_plan.HasValue ? ((DateTime)itemEdit.date_plan).ToString("dd.MM.yyyy") : "-");
                mess_notify_detail += System.Environment.NewLine;
                crm_event.date_plan = itemEdit.date_plan;
            }

            DateTime? date_fact_new = null;
            if (crm_event.task_id.GetValueOrDefault(0) <= 0)
            {
                if (itemEdit.is_done)
                {
                    date_fact_new = itemEdit.date_fact.HasValue ? (DateTime?)((DateTime)itemEdit.date_fact).Date : DateTime.Today.Date; ;                    
                }
                else
                {
                    date_fact_new = null;                    
                }
                if (crm_event.date_fact != date_fact_new)
                {
                    mess_notify_detail += "Смена фактической даты выполнения события с " + (crm_event.date_fact.HasValue ? ((DateTime)crm_event.date_fact).ToString("dd.MM.yyyy") : "-") + " на " + (date_fact_new.HasValue ? ((DateTime)date_fact_new).ToString("dd.MM.yyyy") : "-");
                    mess_notify_detail += System.Environment.NewLine;
                    crm_event.date_fact = date_fact_new;
                }
            }

            if (!String.IsNullOrEmpty(mess_notify_detail))
            {
                mess_notify += "Изменено событие в Календаре: " + crm_event.event_name.CutToLengthWithDots(50);
                mess_notify += System.Environment.NewLine;
                mess_notify += mess_notify_detail;
                mess_notify += "Автор изменений: " + currUser.CabUserName;
                mess_notify += System.Environment.NewLine;
                mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
                mess_notify += System.Environment.NewLine;

                notify notify = null;                
                List<int> notifyTargetList = dbContext.vw_crm_notify_param_user.Where(ss =>
                    ((ss.changer_user_id == crm_event.exec_user_id) && (ss.is_executer == true) && (ss.user_id != currUser.CabUserId))
                    )
                    .Select(ss => ss.user_id)
                    .Distinct()
                    .ToList();
                if ((notifyTargetList != null) && (notifyTargetList.Count > 0))
                {
                    foreach (var notifyTarget in notifyTargetList)
                    {
                        notify = new notify();
                        notify.crt_date = now;
                        notify.message = mess_notify;
                        notify.message_sent = false;
                        notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                        notify.sent_date = null;
                        notify.target_user_id = notifyTarget;
                        notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                        dbContext.notify.Add(notify);
                    }
                }

                /*
                // уведомление исполнителю
                if ((crm_event.exec_user_id > 0) && (crm_event.exec_user_id != currUser.CabUserId))
                {
                    notify = new notify();
                    notify.crt_date = now;
                    notify.message = mess_notify;
                    notify.message_sent = false;
                    notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                    notify.sent_date = null;
                    notify.target_user_id = crm_event.exec_user_id;
                    dbContext.notify.Add(notify);
                }
                */

            }

            updatePlannerEvent_forCrmEvent(crm_event, false, false);

            dbContext.SaveChanges();            

            var vw_crm_event = dbContext.vw_crm_event.Where(ss => ss.event_id == crm_event.event_id).FirstOrDefault();
            return new CrmSchedulerViewModel(vw_crm_event);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmSchedulerViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            long id = ((CrmSchedulerViewModel)item).event_id;
            DateTime now = DateTime.Now;
            string mess_notify = "";

            var crm_event = dbContext.crm_event.Where(ss => ss.event_id == id).FirstOrDefault();
            if (crm_event == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            if (crm_event.task_id.GetValueOrDefault(0) > 0)
            {
                var crm_task_list = dbContext.crm_task.Where(ss => ss.task_id == crm_event.task_id).ToList();
                if ((crm_task_list != null) && (crm_task_list.Count == 1))
                {
                    var crm_task = crm_task_list.FirstOrDefault();
                    //crm_task.is_event = false;
                }
            }            
            
            mess_notify += "Удалено событие в Календаре: " + crm_event.event_name.CutToLengthWithDots(50);
            mess_notify += System.Environment.NewLine;            
            mess_notify += "Автор изменений: " + currUser.CabUserName;
            mess_notify += System.Environment.NewLine;
            mess_notify += "Дата изменений: " + now.ToString("dd.MM.yyyy HH:mm:ss");
            mess_notify += System.Environment.NewLine;

            notify notify = null;
            List<int> notifyTargetList = dbContext.vw_crm_notify_param_user.Where(ss =>
                ((ss.changer_user_id == crm_event.exec_user_id) && (ss.is_executer == true) && (ss.user_id != currUser.CabUserId))
                )
                .Select(ss => ss.user_id)
                .Distinct()
                .ToList();
            if ((notifyTargetList != null) && (notifyTargetList.Count > 0))
            {
                foreach (var notifyTarget in notifyTargetList)
                {
                    notify = new notify();
                    notify.crt_date = now;
                    notify.message = mess_notify;
                    notify.message_sent = false;
                    notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                    notify.sent_date = null;
                    notify.target_user_id = notifyTarget;
                    notify.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                    dbContext.notify.Add(notify);
                }
            }

            /*
            // уведомление исполнителю
            if ((crm_event.exec_user_id > 0) && (crm_event.exec_user_id != currUser.CabUserId))
            {
                notify = new notify();
                notify.crt_date = now;
                notify.message = mess_notify;
                notify.message_sent = false;
                notify.scope = (int)Enums.NotifyScope.CRM_TASK;
                notify.sent_date = null;
                notify.target_user_id = crm_event.exec_user_id;
                dbContext.notify.Add(notify);
            }
            */

            dbContext.planner_event.RemoveRange(dbContext.planner_event.Where(ss => ss.crm_event_id == crm_event.event_id));
            dbContext.planner_task_event.RemoveRange(dbContext.planner_task_event.Where(ss => ss.event_id == crm_event.event_id));
            dbContext.crm_event.Remove(crm_event);
            dbContext.SaveChanges();

            return true;
        }

        #region updatePlannerEvent_forCrmEvent

        private void updatePlannerEvent_forCrmEvent(crm_event sourceEvent, bool createIfNotExists, bool saveChanges) 
        {
            // !!!
            return;

            if (sourceEvent == null)
                return;

            bool created = false;
            planner_event targetEvent = dbContext.planner_event.Where(ss => ss.crm_event_id == sourceEvent.event_id).FirstOrDefault();
            if (targetEvent == null)
            {
                if (createIfNotExists)
                {
                    targetEvent = new planner_event();
                    created = true;
                }
                else
                    return;
            }

            DateTime now = DateTime.Now;
            targetEvent.date_beg = sourceEvent.date_beg;
            targetEvent.date_end = sourceEvent.date_end;
            targetEvent.description = sourceEvent.event_text;
            targetEvent.event_group_id = sourceEvent.event_group_id.GetValueOrDefault(0) <= 0 ? 1 : (int)sourceEvent.event_group_id;
            targetEvent.event_state_id = sourceEvent.is_canceled ? (int)Enums.PlannerEventStateEnum.CANCELED : (sourceEvent.is_done ? (int)Enums.PlannerEventStateEnum.DONE : (int)Enums.PlannerEventStateEnum.ACTIVE);
            targetEvent.exec_user_id = sourceEvent.exec_user_id;
            targetEvent.progress = sourceEvent.event_progress;
            targetEvent.result = sourceEvent.event_result;
            targetEvent.task_id = sourceEvent.task_id;
            targetEvent.title = sourceEvent.event_name;
            targetEvent.upd_date = now;
            targetEvent.upd_user = currUser.CabUserName;

            if (created)
            {
                targetEvent.crm_event_id = sourceEvent.event_id;
                targetEvent.is_all_day = true;
                targetEvent.is_public = false;
                targetEvent.is_public_global = false;
                targetEvent.rate = 0;                
                targetEvent.crt_date = targetEvent.upd_date;
                targetEvent.crt_user = targetEvent.upd_user;
                dbContext.planner_event.Add(targetEvent);
            }
            
            if (saveChanges)
                dbContext.SaveChanges();
        }

        #endregion
    }

}
