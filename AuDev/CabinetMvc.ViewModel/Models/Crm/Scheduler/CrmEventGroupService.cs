﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmEventGroupService : IAuBaseService
    {
        //
    }

    public class CrmEventGroupService : AuBaseService, ICrmEventGroupService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_event_group
                .Select(ss => new CrmEventGroupViewModel
                {
                    group_id = ss.group_id,
                    group_name = ss.group_name,
                });
        }   
    }

}
