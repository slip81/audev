﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmEventGroupViewModel : AuBaseViewModel
    {
        public CrmEventGroupViewModel()
            : base()
        {
            
        }

        public CrmEventGroupViewModel(crm_event_group item)
            : base()
        {
            group_id = item.group_id;
            group_name = item.group_name;
        }

        [Key]
        [Display(Name = "group_id")]
        public int group_id { get; set; }

        [Display(Name = "group_name")]
        public string group_name { get; set; }
    }
}