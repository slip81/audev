﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmModulePartService : IAuBaseService
    {
        //
    }

    public class CrmModulePartService : AuBaseService, ICrmModulePartService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_module_part.AsEnumerable()
                .Select(ss => new CrmModulePartViewModel
                {
                    module_part_id = ss.module_part_id,
                    module_part_name = ss.module_part_name,
                    module_id = ss.module_id,
                })
                .AsQueryable()
                .OrderBy(ss => ss.module_part_name);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_module_part.Where(ss => ss.module_id == parent_id).AsEnumerable()
                .Select(ss => new CrmModulePartViewModel
                {
                    module_part_id = ss.module_part_id,
                    module_part_name = ss.module_part_name,
                    module_id = ss.module_id,
                })
                .AsQueryable()
                .OrderBy(ss => ss.module_part_name);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModulePartViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmModulePartViewModel itemAdd = (CrmModulePartViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_module_part crm_module_part = new crm_module_part();
            crm_module_part.module_part_id = dbContext.crm_module_part.Max(ss => ss.module_part_id) + 1;
            crm_module_part.module_part_name = itemAdd.module_part_name.Trim();
            crm_module_part.module_id = itemAdd.module_id;

            dbContext.crm_module_part.Add(crm_module_part);
            dbContext.SaveChanges();

            return new CrmModulePartViewModel() { module_part_id = crm_module_part.module_part_id, module_part_name = crm_module_part.module_part_name, module_id = crm_module_part.module_id, };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModulePartViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmModulePartViewModel itemEdit = (CrmModulePartViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_module_part = dbContext.crm_module_part.Where(ss => ss.module_part_id == itemEdit.module_part_id).FirstOrDefault();
            if (crm_module_part == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.module_part_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmModulePartViewModel() { module_part_id = crm_module_part.module_part_id, module_part_name = crm_module_part.module_part_name, module_id = crm_module_part.module_id, };

            crm_module_part.module_part_name = itemEdit.module_part_name.Trim();
            dbContext.SaveChanges();

            return new CrmModulePartViewModel() { module_part_id = crm_module_part.module_part_id, module_part_name = crm_module_part.module_part_name, module_id = crm_module_part.module_id, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModulePartViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmModulePartViewModel)item).module_part_id;

            var crm_module_part = dbContext.crm_module_part.Where(ss => ss.module_part_id == id).FirstOrDefault();
            if (crm_module_part == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.prj_claim.Where(ss => ss.module_part_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с разделом модуля с кодом " + id.ToString());
                return false;
            }

            dbContext.crm_module_part.Remove(crm_module_part);
            dbContext.SaveChanges();

            return true;
        }

    }
}
