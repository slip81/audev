﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmPriorityViewModel : AuBaseViewModel
    {
        public CrmPriorityViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_PRIORITY)
        {
            //AuDev.Common.Db.Model.crm_priority
        }

        [Display(Name="Код")]
        public int priority_id { get; set; }
        
        [Required(ErrorMessage = "Не задано наименование")]
        [Display(Name = "Название")]
        public string priority_name { get; set; }
        
        public string fore_color { get; set; }
        [UIHint("IntegerFromZero")]
        public int ord { get; set; }

        public bool is_control { get; set; }

        public bool is_boss { get; set; }

        [UIHint("NumberDecimal")]
        [Range(0, 1)]
        [Display(Name = "Кфт")]
        public Nullable<decimal> rate { get; set; }
    }
}