﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmProjectUserService : IAuBaseService
    {
        //
    }

    public class CrmProjectUserService : AuBaseService, ICrmProjectUserService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_project_user.Where(ss => ss.project_id == parent_id)
                .Select(ss => new CrmProjectUserViewModel
                {
                    item_id = ss.item_id,
                    user_id = ss.user_id,
                    project_id = ss.project_id,
                    user_name = ss.cab_user.user_name,                    
                    ProjectUser = new CabinetMvc.ViewModel.Adm.CabUserViewModel() { user_id = ss.user_id, user_name = ss.cab_user.user_name, },
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_project_user
                .Select(ss => new CrmProjectUserViewModel
                {
                    item_id = ss.item_id,
                    user_id = ss.user_id,
                    project_id = ss.project_id,
                    user_name = ss.cab_user.user_name,
                    ProjectUser = new CabinetMvc.ViewModel.Adm.CabUserViewModel() { user_id = ss.user_id, user_name = ss.cab_user.user_name, },
                });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectUserViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmProjectUserViewModel itemAdd = (CrmProjectUserViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }
            if (itemAdd.ProjectUser == null)
            {
                modelState.AddModelError("", "Не задан пользователь");
                return null;
            }

            var existing = dbContext.crm_project_user.Where(ss => ss.project_id == itemAdd.project_id && ss.user_id == itemAdd.ProjectUser.user_id).FirstOrDefault();
            if (existing != null)
            {
                modelState.AddModelError("", "Уже есть данный участник");
                return null;
            }

            crm_project_user crm_project_user = new crm_project_user();
            crm_project_user.project_id = itemAdd.project_id;
            crm_project_user.user_id = itemAdd.ProjectUser.user_id;
            dbContext.crm_project_user.Add(crm_project_user);
            dbContext.SaveChanges();

            return new CrmProjectUserViewModel() { project_id = crm_project_user.project_id, user_id = crm_project_user.user_id, ProjectUser = itemAdd.ProjectUser, };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectUserViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmProjectUserViewModel itemEdit = (CrmProjectUserViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }


            var existing = dbContext.crm_project_user.Where(ss => ss.item_id != itemEdit.item_id && ss.project_id == itemEdit.project_id && ss.user_id == itemEdit.ProjectUser.user_id).FirstOrDefault();
            if (existing != null)
            {
                modelState.AddModelError("", "Уже есть данный участник");
                return null;
            }

            var crm_project_user = dbContext.crm_project_user.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (crm_project_user == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.item_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmProjectUserViewModel() { project_id = crm_project_user.project_id, user_id = crm_project_user.user_id, ProjectUser = itemEdit.ProjectUser, };

            crm_project_user.user_id = itemEdit.ProjectUser.user_id;
            dbContext.SaveChanges();

            return new CrmProjectUserViewModel() { project_id = crm_project_user.project_id, user_id = crm_project_user.user_id, ProjectUser = itemEdit.ProjectUser, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectUserViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmProjectUserViewModel)item).item_id;

            var crm_project_user = dbContext.crm_project_user.Where(ss => ss.item_id == id).FirstOrDefault();
            if (crm_project_user == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            dbContext.crm_project_user.Remove(crm_project_user);
            dbContext.SaveChanges();

            return true;
        }
    }
}
