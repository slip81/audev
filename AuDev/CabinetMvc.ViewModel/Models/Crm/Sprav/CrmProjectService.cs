﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmProjectService : IAuBaseService
    {
        IQueryable<CrmProjectViewModel> GetList_forEdit();
        IQueryable<CrmProjectViewModel> GetList_Sprav();
    }

    public class CrmProjectService : AuBaseService, ICrmProjectService
    {
        public IQueryable<CrmProjectViewModel> GetList_forEdit()
        {
            return dbContext.vw_crm_project.Where(ss => ss.user_id == currUser.CabUserId && ss.project_id > 0)
                .Select(ss => new CrmProjectViewModel
                {
                    project_id = ss.project_id,
                    project_name = ss.project_name,
                    fore_color = ss.fore_color,
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_crm_project.Where(ss => ss.user_id == currUser.CabUserId).AsEnumerable()
                .Select(ss => new CrmProjectViewModel
                {
                    project_id = ss.project_id,
                    project_name = ss.project_name,
                    fore_color = ss.fore_color,
                })
                .AsQueryable()
                .OrderBy(ss => ss.project_name);
        }

        public IQueryable<CrmProjectViewModel> GetList_Sprav()
        {
            return dbContext.crm_project.Where(ss => ss.project_id > 0)
                .Select(ss => new CrmProjectViewModel
                {
                    project_id = ss.project_id,
                    project_name = ss.project_name,                    
                });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmProjectViewModel itemAdd = (CrmProjectViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_project crm_project = new crm_project();
            crm_project.project_id = dbContext.crm_project.Max(ss => ss.project_id) + 1;
            crm_project.project_name = itemAdd.project_name.Trim();
            dbContext.crm_project.Add(crm_project);

            crm_project_user_settings crm_project_user_settings = new crm_project_user_settings();
            crm_project_user_settings.crm_project = crm_project;
            crm_project_user_settings.user_id = currUser.CabUserId;
            crm_project_user_settings.fore_color = itemAdd.fore_color;
            dbContext.crm_project_user_settings.Add(crm_project_user_settings);

            dbContext.SaveChanges();

            return new CrmProjectViewModel() { project_id = crm_project.project_id, project_name = crm_project.project_name, };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmProjectViewModel itemEdit = (CrmProjectViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_project = dbContext.crm_project.Where(ss => ss.project_id == itemEdit.project_id).FirstOrDefault();
            if (crm_project == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.project_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmProjectViewModel() { project_id = crm_project.project_id, project_name = crm_project.project_name, };

            crm_project.project_name = itemEdit.project_name.Trim();

            var cab_user_id = currUser.CabUserId;
            crm_project_user_settings crm_project_user_settings = dbContext.crm_project_user_settings.Where(ss => ss.user_id == cab_user_id && ss.project_id == crm_project.project_id).FirstOrDefault();
            if (crm_project_user_settings != null)
            {
                crm_project_user_settings.fore_color = itemEdit.fore_color;
            }
            else
            {
                crm_project_user_settings = new crm_project_user_settings();
                crm_project_user_settings.project_id = itemEdit.project_id;
                crm_project_user_settings.user_id = cab_user_id;
                crm_project_user_settings.fore_color = itemEdit.fore_color;
                dbContext.crm_project_user_settings.Add(crm_project_user_settings);
            }

            dbContext.SaveChanges();

            return new CrmProjectViewModel() { project_id = crm_project.project_id, project_name = crm_project.project_name, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmProjectViewModel)item).project_id;

            var crm_project = dbContext.crm_project.Where(ss => ss.project_id == id).FirstOrDefault();
            if (crm_project == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.prj_claim.Where(ss => ss.project_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с проектом с кодом " + id.ToString());
                return false;
            }

            var curr_user_id = currUser.CabUserId;
            dbContext.crm_project_user_settings.RemoveRange(dbContext.crm_project_user_settings.Where(ss => ss.project_id == id));
            dbContext.crm_project.Remove(crm_project);
            dbContext.SaveChanges();

            return true;
        }
    }
}
