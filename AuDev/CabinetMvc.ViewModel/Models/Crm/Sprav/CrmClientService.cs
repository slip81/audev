﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmClientService : IAuBaseService
    {
        IQueryable<CrmClientViewModel> GetList_forEdit();
    }

    public class CrmClientService : AuBaseService, ICrmClientService
    {
        public IQueryable<CrmClientViewModel> GetList_forEdit()
        {
            return dbContext.crm_client.Where(ss => ss.client_id > 0)
                .Select(ss => new CrmClientViewModel
                {
                    client_id = ss.client_id,
                    client_name = ss.client_name,
                    cab_client_id = ss.cab_client_id,
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_client.AsEnumerable()
                .Select(ss => new CrmClientViewModel
                {
                    client_id = ss.client_id,
                    client_name = ss.client_name,
                    cab_client_id = ss.cab_client_id,
                })
                .AsQueryable()
                .OrderBy(ss => ss.client_name);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmClientViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmClientViewModel itemAdd = (CrmClientViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_client crm_client = new crm_client();
            crm_client.client_id = dbContext.crm_client.Max(ss => ss.client_id) + 1;
            crm_client.client_name = itemAdd.client_name;
            //crm_client.cab_client_id = itemAdd.client_id;
            crm_client.cab_client_id = null;

            dbContext.crm_client.Add(crm_client);
            dbContext.SaveChanges();

            return new CrmClientViewModel() { client_id = crm_client.client_id, client_name = crm_client.client_name, };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmClientViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmClientViewModel itemEdit = (CrmClientViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_client = dbContext.crm_client.Where(ss => ss.client_id == itemEdit.client_id).FirstOrDefault();
            if (crm_client == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.client_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmClientViewModel() { client_id = crm_client.client_id, client_name = crm_client.client_name, };

            crm_client.client_name = itemEdit.client_name;
            dbContext.SaveChanges();

            return new CrmClientViewModel() { client_id = crm_client.client_id, client_name = crm_client.client_name, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmClientViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmClientViewModel)item).client_id;

            var crm_client = dbContext.crm_client.Where(ss => ss.client_id == id).FirstOrDefault();
            if (crm_client == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.prj_claim.Where(ss => ss.client_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с клиентом с кодом " + id.ToString());
                return false;
            }

            dbContext.crm_client.Remove(crm_client);
            dbContext.SaveChanges();

            return true;
        }
    }
}
