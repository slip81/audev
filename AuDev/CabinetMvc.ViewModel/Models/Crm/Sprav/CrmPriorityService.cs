﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmPriorityService : IAuBaseService
    {
        IQueryable<CrmPriorityViewModel> GetList_forEdit();
    }

    public class CrmPriorityService : AuBaseService, ICrmPriorityService
    {
        public IQueryable<CrmPriorityViewModel> GetList_forEdit()
        {
            return dbContext.vw_crm_priority.Where(ss => ss.user_id == currUser.CabUserId && ss.priority_id > 0)
                .Select(ss => new CrmPriorityViewModel
                {
                    priority_id = ss.priority_id,
                    priority_name = ss.priority_name,
                    fore_color = ss.fore_color,
                    ord = ss.ord,
                    is_control = ss.is_control,
                    is_boss = ss.is_boss,
                    rate = ss.rate,
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_crm_priority
                .Where(ss => ss.user_id == currUser.CabUserId 
                    //&& ((!currUser.IsBoss && !ss.is_boss) || (currUser.IsBoss))
                    )
                .AsEnumerable()
                .Select(ss => new CrmPriorityViewModel
                {
                    priority_id = ss.priority_id,
                    priority_name = ss.priority_name,
                    fore_color = ss.fore_color,
                    ord = ss.ord,
                    is_control = ss.is_control,
                    is_boss = ss.is_boss,
                    rate = ss.rate,
                })
                .AsQueryable()
                .OrderBy(ss => ss.ord);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmPriorityViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmPriorityViewModel itemAdd = (CrmPriorityViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }
            
            crm_priority crm_priority = new crm_priority();
            crm_priority.priority_id = dbContext.crm_priority.Max(ss => ss.priority_id) + 1;
            crm_priority.priority_name = itemAdd.priority_name.Trim();
            crm_priority.ord = itemAdd.ord;
            crm_priority.is_control = itemAdd.is_control;
            crm_priority.rate = itemAdd.rate;
            dbContext.crm_priority.Add(crm_priority);

            crm_priority_user_settings crm_priority_user_settings = new crm_priority_user_settings();
            crm_priority_user_settings.crm_priority = crm_priority;
            crm_priority_user_settings.user_id = currUser.CabUserId;
            crm_priority_user_settings.fore_color = itemAdd.fore_color;
            dbContext.crm_priority_user_settings.Add(crm_priority_user_settings);

            dbContext.SaveChanges();

            return new CrmPriorityViewModel() { priority_id = crm_priority.priority_id, priority_name = crm_priority.priority_name, };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmPriorityViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmPriorityViewModel itemEdit = (CrmPriorityViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_priority = dbContext.crm_priority.Where(ss => ss.priority_id == itemEdit.priority_id).FirstOrDefault();
            if (crm_priority == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.priority_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmPriorityViewModel() { priority_id = crm_priority.priority_id, priority_name = crm_priority.priority_name, };

            crm_priority.priority_name = itemEdit.priority_name.Trim();
            crm_priority.ord = itemEdit.ord;
            crm_priority.is_control = itemEdit.is_control;
            crm_priority.rate = itemEdit.rate;

            var cab_user_id = currUser.CabUserId;
            crm_priority_user_settings crm_priority_user_settings = dbContext.crm_priority_user_settings.Where(ss => ss.user_id == cab_user_id && ss.priority_id == crm_priority.priority_id).FirstOrDefault();
            if (crm_priority_user_settings != null)
            {
                crm_priority_user_settings.fore_color = itemEdit.fore_color;
            }
            else
            {
                crm_priority_user_settings = new crm_priority_user_settings();
                crm_priority_user_settings.priority_id = itemEdit.priority_id;
                crm_priority_user_settings.user_id = cab_user_id;
                crm_priority_user_settings.fore_color = itemEdit.fore_color;
                dbContext.crm_priority_user_settings.Add(crm_priority_user_settings);
            }

            dbContext.SaveChanges();

            return new CrmPriorityViewModel() { priority_id = crm_priority.priority_id, priority_name = crm_priority.priority_name, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmPriorityViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmPriorityViewModel)item).priority_id;

            var crm_priority = dbContext.crm_priority.Where(ss => ss.priority_id == id).FirstOrDefault();
            if (crm_priority == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.prj_claim.Where(ss => ss.priority_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с приоритетом с кодом " + id.ToString());
                return false;
            }

            var curr_user_id = currUser.CabUserId;
            dbContext.crm_priority_user_settings.RemoveRange(dbContext.crm_priority_user_settings.Where(ss => ss.priority_id == id));
            dbContext.crm_priority.Remove(crm_priority);
            dbContext.SaveChanges();

            return true;
        }
    }
}
