﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmProjectVersionViewModel : AuBaseViewModel
    {
        public CrmProjectVersionViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_PROJECT_VERSION)
        {
            //
        }

        public int project_version_id { get; set; }
        public string project_version_name { get; set; }
        public int project_id { get; set; }
    }
}