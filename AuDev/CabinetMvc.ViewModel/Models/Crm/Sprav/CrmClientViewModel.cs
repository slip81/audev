﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmClientViewModel : AuBaseViewModel
    {
        public CrmClientViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_CLIENT)
        {
            //
        }
        
        public int client_id { get; set; }
        public string client_name { get; set; }
        public Nullable<int> cab_client_id { get; set; }       
    }
}