﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmBatchService : IAuBaseService
    {
        IQueryable<CrmBatchViewModel> GetList_forEdit();
    }

    public class CrmBatchService : AuBaseService, ICrmBatchService
    {
        public IQueryable<CrmBatchViewModel> GetList_forEdit()
        {
            return dbContext.crm_batch.Where(ss => ss.batch_id > 0)
                .ProjectTo<CrmBatchViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_batch.AsEnumerable()
                .Select(ss => new CrmBatchViewModel
                {
                    batch_id = ss.batch_id,
                    batch_name = ss.batch_name,
                    date_beg = ss.date_beg,
                    date_end = ss.date_end,
                })
                .AsQueryable()
                .OrderBy(ss => ss.batch_name);
        }


        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmBatchViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmBatchViewModel itemAdd = (CrmBatchViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_batch crm_batch = new crm_batch();
            crm_batch.batch_id = dbContext.crm_batch.Max(ss => ss.batch_id) + 1;
            crm_batch.batch_name = itemAdd.batch_name;
            crm_batch.date_beg = itemAdd.date_beg;
            crm_batch.date_end = itemAdd.date_end;
            dbContext.crm_batch.Add(crm_batch);

            dbContext.SaveChanges();

            return new CrmBatchViewModel() { batch_id = crm_batch.batch_id, batch_name = crm_batch.batch_name, };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmBatchViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmBatchViewModel itemEdit = (CrmBatchViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_batch = dbContext.crm_batch.Where(ss => ss.batch_id == itemEdit.batch_id).FirstOrDefault();
            if (crm_batch == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.batch_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmBatchViewModel() { batch_id = crm_batch.batch_id, batch_name = crm_batch.batch_name, };

            crm_batch.batch_name = itemEdit.batch_name;
            crm_batch.date_beg = itemEdit.date_beg;
            crm_batch.date_end = itemEdit.date_end;

            dbContext.SaveChanges();

            return new CrmBatchViewModel() { batch_id = crm_batch.batch_id, batch_name = crm_batch.batch_name, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmBatchViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmBatchViewModel)item).batch_id;

            var crm_batch = dbContext.crm_batch.Where(ss => ss.batch_id == id).FirstOrDefault();
            if (crm_batch == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.crm_task.Where(ss => ss.batch_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с проектом с кодом " + id.ToString());
                return false;
            }

            dbContext.crm_batch.Remove(crm_batch);
            dbContext.SaveChanges();

            return true;
        }
    }
}
