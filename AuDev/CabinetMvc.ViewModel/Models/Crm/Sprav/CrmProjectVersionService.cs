﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmProjectVersionService : IAuBaseService
    {
        //
    }

    public class CrmProjectVersionService : AuBaseService, ICrmProjectVersionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_project_version.AsEnumerable()
                .Select(ss => new CrmProjectVersionViewModel
                {
                    project_version_id = ss.project_version_id,
                    project_version_name = ss.project_version_name,
                    project_id = ss.project_id,
                })
                .AsQueryable()
                .OrderBy(ss => ss.project_version_name);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_project_version.Where(ss => ss.project_id == parent_id && ss.project_version_id > 0)
                .Select(ss => new CrmProjectVersionViewModel
                {
                    project_version_id = ss.project_version_id,
                    project_version_name = ss.project_version_name,
                    project_id = ss.project_id,
                });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectVersionViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmProjectVersionViewModel itemAdd = (CrmProjectVersionViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_project_version crm_project_version = new crm_project_version();            
            crm_project_version.project_version_name = itemAdd.project_version_name.Trim();
            crm_project_version.project_id = itemAdd.project_id;

            dbContext.crm_project_version.Add(crm_project_version);
            dbContext.SaveChanges();

            return new CrmProjectVersionViewModel() {
                project_version_id = crm_project_version.project_version_id,
                project_version_name = crm_project_version.project_version_name,
                project_id = crm_project_version.project_id,
            };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectVersionViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmProjectVersionViewModel itemEdit = (CrmProjectVersionViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_project_version = dbContext.crm_project_version.Where(ss => ss.project_version_id == itemEdit.project_version_id).FirstOrDefault();
            if (crm_project_version == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.project_version_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmProjectVersionViewModel() {
                project_version_id = crm_project_version.project_version_id,
                project_version_name = crm_project_version.project_version_name,
                project_id = crm_project_version.project_id,
            };

            crm_project_version.project_version_name = itemEdit.project_version_name.Trim();
            dbContext.SaveChanges();

            return new CrmProjectVersionViewModel()
            {
                project_version_id = crm_project_version.project_version_id,
                project_version_name = crm_project_version.project_version_name,
                project_id = crm_project_version.project_id,
            };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmProjectVersionViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmProjectVersionViewModel)item).project_version_id;

            var crm_project_version = dbContext.crm_project_version.Where(ss => ss.project_version_id == id).FirstOrDefault();
            if (crm_project_version == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            
            var existing1 = dbContext.prj_claim.Where(ss => ss.project_version_id == id || ss.project_repair_version_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с версией с кодом " + id.ToString());
                return false;
            }
            

            dbContext.crm_project_version.Remove(crm_project_version);
            dbContext.SaveChanges();

            return true;
        }

    }
}
