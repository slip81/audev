﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmProjectUserViewModel : AuBaseViewModel
    {
        public CrmProjectUserViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_PROJECT_USER)
        {
            //
        }

        public int item_id { get; set; }
        public int user_id { get; set; }
        public int project_id { get; set; }
        public string user_name { get; set; }

        [Display(Name = "Пользователь")]
        [UIHint("CabUserCombo")]
        public CabinetMvc.ViewModel.Adm.CabUserViewModel ProjectUser { get; set; }
    }
}