﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmModuleVersionService : IAuBaseService
    {
        //
    }

    public class CrmModuleVersionService : AuBaseService, ICrmModuleVersionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_module_version.AsEnumerable()
                .Select(ss => new CrmModuleVersionViewModel
                {
                    module_version_id = ss.module_version_id,
                    module_version_name = ss.module_version_name,
                    module_id = ss.module_id,
                })
                .AsQueryable()
                .OrderBy(ss => ss.module_version_name);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_module_version.Where(ss => ss.module_id == parent_id && ss.module_version_id > 0)
                .Select(ss => new CrmModuleVersionViewModel
                {
                    module_version_id = ss.module_version_id,
                    module_version_name = ss.module_version_name,
                    module_id = ss.module_id,
                });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModuleVersionViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmModuleVersionViewModel itemAdd = (CrmModuleVersionViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_module_version crm_module_version = new crm_module_version();
            crm_module_version.module_version_id = dbContext.crm_module_version.Max(ss => ss.module_version_id) + 1;
            crm_module_version.module_version_name = itemAdd.module_version_name.Trim();
            crm_module_version.module_id = itemAdd.module_id;

            dbContext.crm_module_version.Add(crm_module_version);
            dbContext.SaveChanges();

            return new CrmModuleVersionViewModel() { module_version_id = crm_module_version.module_version_id, module_version_name = crm_module_version.module_version_name, module_id = crm_module_version.module_id,};
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModuleVersionViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmModuleVersionViewModel itemEdit = (CrmModuleVersionViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_module_version = dbContext.crm_module_version.Where(ss => ss.module_version_id == itemEdit.module_version_id).FirstOrDefault();
            if (crm_module_version == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.module_version_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmModuleVersionViewModel() { module_version_id = crm_module_version.module_version_id, module_version_name = crm_module_version.module_version_name, module_id = crm_module_version.module_id, };

            crm_module_version.module_version_name = itemEdit.module_version_name.Trim();
            dbContext.SaveChanges();

            return new CrmModuleVersionViewModel() { module_version_id = crm_module_version.module_version_id, module_version_name = crm_module_version.module_version_name, module_id = crm_module_version.module_id, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModuleVersionViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmModuleVersionViewModel)item).module_version_id;

            var crm_module_version = dbContext.crm_module_version.Where(ss => ss.module_version_id == id).FirstOrDefault();
            if (crm_module_version == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            /*
            var existing1 = dbContext.prj_claim.Where(ss => ss.module_version_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с версией с кодом " + id.ToString());
                return false;
            }
            */

            dbContext.crm_module_version.Remove(crm_module_version);
            dbContext.SaveChanges();

            return true;
        }

    }
}
