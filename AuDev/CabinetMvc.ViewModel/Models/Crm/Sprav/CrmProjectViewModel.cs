﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmProjectViewModel : AuBaseViewModel
    {
        public CrmProjectViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_PROJECT)
        {
            //
        }

        public int project_id { get; set; }
        public string project_name { get; set; }
        public string fore_color { get; set; }
    }
}