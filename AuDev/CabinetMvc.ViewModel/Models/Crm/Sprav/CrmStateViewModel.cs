﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmStateViewModel : AuBaseViewModel
    {
        public CrmStateViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_STATE)
        {
            //            
        }

        public int state_id { get; set; }
        public string state_name { get; set; }
        public string fore_color { get; set; }
        [UIHint("IntegerFromZero")]
        public int ord { get; set; }
        public bool for_event { get; set; }
        public int done_or_canceled { get; set; }
    }
}