﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmModuleViewModel : AuBaseViewModel
    {
        public CrmModuleViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_MODULE)
        {
            //
        }

        public int module_id { get; set; }
        public string module_name { get; set; }
        public int project_id { get; set; }      
    }
}