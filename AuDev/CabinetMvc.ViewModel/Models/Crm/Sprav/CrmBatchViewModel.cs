﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmBatchViewModel : AuBaseViewModel
    {
        public CrmBatchViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_BATCH)
        {
            //
        }

        public int batch_id { get; set; }
        public string batch_name { get; set; }
        [DataType(DataType.Date)]
        public DateTime? date_beg { get; set; }
        [DataType(DataType.Date)]
        public DateTime? date_end { get; set; }
    }
}