﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmModulePartViewModel : AuBaseViewModel
    {
        public CrmModulePartViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_MODULE_PART)
        {
            //
        }

        public int module_part_id { get; set; }
        public string module_part_name { get; set; }
        public int module_id { get; set; }   
    }
}