﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmStateService : IAuBaseService
    {
        IQueryable<CrmStateViewModel> GetList_forEdit();
        IEnumerable<CrmStateViewModel> GetList_forEvent();
    }

    public class CrmStateService : AuBaseService, ICrmStateService
    {
        public IQueryable<CrmStateViewModel> GetList_forEdit()
        {
            return dbContext.vw_crm_state.Where(ss => ss.user_id == currUser.CabUserId && ss.state_id > 0)
                .Select(ss => new CrmStateViewModel
                {
                    state_id = ss.state_id,
                    state_name = ss.state_name,
                    fore_color = ss.fore_color,
                    ord = ss.ord,
                    for_event = ss.for_event,                    
                });

        }
        public IEnumerable<CrmStateViewModel> GetList_forEvent()
        {
            return dbContext.vw_crm_state.Where(ss => ss.user_id == currUser.CabUserId && ss.for_event)
                .Select(ss => new CrmStateViewModel
                {
                    state_id = ss.state_id,
                    state_name = ss.state_name,
                    fore_color = ss.fore_color,
                    ord = ss.ord,
                    for_event = ss.for_event,
                })
                .OrderBy(ss => ss.state_id)
                .AsEnumerable();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_crm_state.Where(ss => ss.user_id == currUser.CabUserId).AsEnumerable()
                .Select(ss => new CrmStateViewModel
                {
                    state_id = ss.state_id,
                    state_name = ss.state_name,
                    fore_color = ss.fore_color,
                    ord = ss.ord,
                    for_event = ss.for_event,
                })
                .AsQueryable()
                .OrderBy(ss => ss.ord);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmStateViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmStateViewModel itemAdd = (CrmStateViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_state crm_state = new crm_state();
            crm_state.state_id = dbContext.crm_state.Max(ss => ss.state_id) + 1;
            crm_state.state_name = itemAdd.state_name;
            crm_state.ord = itemAdd.ord;
            crm_state.done_or_canceled = itemAdd.done_or_canceled;
            dbContext.crm_state.Add(crm_state);

            crm_state_user_settings crm_state_user_settings = new crm_state_user_settings();
            crm_state_user_settings.crm_state = crm_state;
            crm_state_user_settings.user_id = currUser.CabUserId;
            crm_state_user_settings.fore_color = itemAdd.fore_color;
            dbContext.crm_state_user_settings.Add(crm_state_user_settings);

            dbContext.SaveChanges();

            return new CrmStateViewModel() { state_id = crm_state.state_id, state_name = crm_state.state_name, };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmStateViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmStateViewModel itemEdit = (CrmStateViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_state = dbContext.crm_state.Where(ss => ss.state_id == itemEdit.state_id).FirstOrDefault();
            if (crm_state == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.state_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmStateViewModel() { state_id = crm_state.state_id, state_name = crm_state.state_name, };

            crm_state.state_name = itemEdit.state_name;
            crm_state.ord = itemEdit.ord;
            crm_state.done_or_canceled = itemEdit.done_or_canceled;

            var cab_user_id = currUser.CabUserId;
            crm_state_user_settings crm_state_user_settings = dbContext.crm_state_user_settings.Where(ss => ss.user_id == cab_user_id && ss.state_id == crm_state.state_id).FirstOrDefault();
            if (crm_state_user_settings != null)
            {
                crm_state_user_settings.fore_color = itemEdit.fore_color;
            }
            else
            {
                crm_state_user_settings = new crm_state_user_settings();
                crm_state_user_settings.state_id = itemEdit.state_id;
                crm_state_user_settings.user_id = cab_user_id;
                crm_state_user_settings.fore_color = itemEdit.fore_color;
                dbContext.crm_state_user_settings.Add(crm_state_user_settings);
            }

            dbContext.SaveChanges();

            return new CrmStateViewModel() { state_id = crm_state.state_id, state_name = crm_state.state_name, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmStateViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmStateViewModel)item).state_id;

            var crm_state = dbContext.crm_state.Where(ss => ss.state_id == id).FirstOrDefault();
            if (crm_state == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.crm_task.Where(ss => ss.state_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с статусом с кодом " + id.ToString());
                return false;
            }

            var existing2 = dbContext.crm_subtask.Where(ss => ss.state_id == id).FirstOrDefault();
            if (existing2 != null)
            {
                modelState.AddModelError("", "Существуют подзадачи с статусом с кодом " + id.ToString());
                return false;
            }

            var curr_user_id = currUser.CabUserId;
            dbContext.crm_state_user_settings.RemoveRange(dbContext.crm_state_user_settings.Where(ss => ss.state_id == id));
            dbContext.crm_state.Remove(crm_state);
            dbContext.SaveChanges();

            return true;
        }
    }
}
