﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Crm
{
    public class CrmModuleVersionViewModel : AuBaseViewModel
    {
        public CrmModuleVersionViewModel()
            : base(AuDev.Common.Util.Enums.MainObjectType.CRM_MODULE_VERSION)
        {
            //
        }

        public int module_version_id { get; set; }
        public string module_version_name { get; set; }
        public int module_id { get; set; }
    }
}