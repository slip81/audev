﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Crm
{
    public interface ICrmModuleService : IAuBaseService
    {
        //
    }

    public class CrmModuleService : AuBaseService, ICrmModuleService
    {

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.crm_module.Where(ss => ss.project_id == parent_id && ss.module_id > 0)
                .Select(ss => new CrmModuleViewModel
                {
                    module_id = ss.module_id,
                    module_name = ss.module_name,
                    project_id = ss.project_id,
                });
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.crm_module.AsEnumerable()
                .Select(ss => new CrmModuleViewModel
                {
                    module_id = ss.module_id,
                    module_name = ss.module_name,
                    project_id = ss.project_id,
                })
                .AsQueryable()
                .OrderBy(ss => ss.module_name);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModuleViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmModuleViewModel itemAdd = (CrmModuleViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            crm_module crm_module = new crm_module();
            crm_module.module_id = dbContext.crm_module.Max(ss => ss.module_id) + 1;
            crm_module.module_name = itemAdd.module_name.Trim();
            crm_module.project_id = itemAdd.project_id;

            dbContext.crm_module.Add(crm_module);
            dbContext.SaveChanges();

            return new CrmModuleViewModel() { module_id = crm_module.module_id, module_name = crm_module.module_name, };
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModuleViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return null;
            }

            CrmModuleViewModel itemEdit = (CrmModuleViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            var crm_module = dbContext.crm_module.Where(ss => ss.module_id == itemEdit.module_id).FirstOrDefault();
            if (crm_module == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + itemEdit.module_id.ToString());
                return null;
            }
            modelBeforeChanges = new CrmModuleViewModel() { module_id = crm_module.module_id, module_name = crm_module.module_name, project_id = crm_module.project_id, };

            crm_module.module_name = itemEdit.module_name.Trim();
            dbContext.SaveChanges();

            return new CrmModuleViewModel() { module_id = crm_module.module_id, module_name = crm_module.module_name, project_id = crm_module.project_id, };
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CrmModuleViewModel))
            {
                modelState.AddModelError("", "Некорректные параметры");
                return false;
            }

            int id = ((CrmModuleViewModel)item).module_id;

            var crm_module = dbContext.crm_module.Where(ss => ss.module_id == id).FirstOrDefault();
            if (crm_module == null)
            {
                modelState.AddModelError("", "Не найдена запись с кодом " + id.ToString());
                return false;
            }

            var existing1 = dbContext.prj_claim.Where(ss => ss.module_id == id).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("", "Существуют задачи с модулем с кодом " + id.ToString());
                return false;
            }

            dbContext.crm_module_version.RemoveRange(dbContext.crm_module_version.Where(ss => ss.module_id == crm_module.module_id));
            dbContext.crm_module_part.RemoveRange(dbContext.crm_module_part.Where(ss => ss.module_id == crm_module.module_id));
            dbContext.crm_module.Remove(crm_module);
            dbContext.SaveChanges();

            return true;
        }
    }
}
