﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Exchange;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.User;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.ViewModel.Crm;
using CabinetMvc.ViewModel.Auth;
using CabinetMvc.ViewModel.Storage;
using CabinetMvc.ViewModel.Update;
using CabinetMvc.ViewModel.Prj;
using CabinetMvc.ViewModel.Stock;
using CabinetMvc.ViewModel.Asna;
using CabinetMvc.ViewModel.Wa;
using CabinetMvc.Auth;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AutoMapper;

namespace CabinetMvc.ViewModel
{

    public class AutoMapperCommon: IMapper
    {
        static AutoMapperCommon() 
        {        
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<programm_order, ProgrammOrderViewModel>();
                cfg.CreateMap<vw_programm_order, ProgrammOrderViewModel>();
                cfg.CreateMap<ProgrammOrderViewModel, programm_order>();
                //
                cfg.CreateMap<cab_user_profile, UserProfileViewModel>();
                cfg.CreateMap<UserProfileViewModel, cab_user_profile>();
                //                
                cfg.CreateMap<vw_exchange_file_log, ExchangeFileLogViewModel>();
                //                
                cfg.CreateMap<vw_exchange_file_node, ExchangeFileNodeViewModel>();
                cfg.CreateMap<exchange_file_node, ExchangeFileNodeViewModel>();
                cfg.CreateMap<ExchangeFileNodeViewModel, exchange_file_node>();                
                //
                cfg.CreateMap<exchange_file_node_item, ExchangeFileNodeItemViewModel>()
                    .ForMember(vm => vm.exchange_item_name, conf => conf.MapFrom(db => db.exchange_item.item_name));
                cfg.CreateMap<ExchangeFileNodeItemViewModel, exchange_file_node_item>();
                //
                cfg.CreateMap<datasource_exchange_item, ExchangePartnerItemViewModel>();
                cfg.CreateMap<vw_datasource_exchange_item, ExchangePartnerItemViewModel>();
                cfg.CreateMap<ExchangePartnerItemViewModel, datasource_exchange_item>();
                //
                cfg.CreateMap<datasource, ExchangePartnerViewModel>();
                cfg.CreateMap<ExchangePartnerViewModel, datasource>();
                //
                cfg.CreateMap<service_sender, ServiceSenderViewModel>();
                cfg.CreateMap<ServiceSenderViewModel, service_sender>();
                //
                cfg.CreateMap<service_sender_log, ServiceSenderLogViewModel>();
                //                
                cfg.CreateMap<sales, SalesViewModel>()
                    .ForMember(vm => vm.sales_id, conf => conf.MapFrom(db => db.id));
                cfg.CreateMap<vw_sales, SalesViewModel>();
                cfg.CreateMap<SalesViewModel, sales>()
                    .ForMember(db => db.id, conf => conf.MapFrom(vm => vm.sales_id));
                cfg.CreateMap<service_cva, ServiceCvaViewModel>();
                cfg.CreateMap<ServiceCvaViewModel, service_cva>()
                    .ForMember(db => db.info_internet_trade, conf => conf.UseValue<string>("0"));
                //
                cfg.CreateMap<service_cva_data, ServiceCvaDataViewModel>();
                cfg.CreateMap<vw_service_cva_data, ServiceCvaDataViewModel>();                
                //
                cfg.CreateMap<client_user, ClientUserViewModel>()
                    .ForMember(vm => vm.is_for_sales, conf => conf.MapFrom(db => db.sales_id.HasValue))
                    .ForMember(vm => vm.user_pwd, conf => conf.UseValue<string>("***"));
                cfg.CreateMap<vw_client_user, ClientUserViewModel>()
                    .ForMember(vm => vm.is_for_sales, conf => conf.MapFrom(db => db.sales_id.HasValue))
                    //.ForMember(vm => vm.user_pwd, conf => conf.UseValue<string>("***"))
                    ;
                cfg.CreateMap<ClientUserViewModel, client_user>();
                //
                cfg.CreateMap<widget, WidgetViewModel>();
                cfg.CreateMap<WidgetViewModel, widget>();
                //
                cfg.CreateMap<vw_client_user_widget, ClientUserWidgetViewModel>();
                cfg.CreateMap<client_user_widget, ClientUserWidgetViewModel>();                    
                cfg.CreateMap<ClientUserWidgetViewModel, client_user_widget>(); 
                //
                cfg.CreateMap<service_defect, ServiceDefectViewModel>();
                cfg.CreateMap<ServiceDefectViewModel, service_defect>();
                //
                cfg.CreateMap<vw_client_info, ClientInfoViewModel>();
                //
                cfg.CreateMap<vw_client_service_summary, ClientServiceSummaryViewModel>();
                //
                cfg.CreateMap<service_group, ServiceGroupViewModel>();
                //
                cfg.CreateMap<vw_service, ServiceViewModel>();
                //
                cfg.CreateMap<reg, RegViewModel>();
                cfg.CreateMap<RegViewModel, reg>();
                //
                cfg.CreateMap<delivery_template, DeliveryTemplateViewModel>()
                    .ForMember(vm => vm.Group_Selected, conf => conf.MapFrom(db => (db.delivery_template_group != null ? db.delivery_template_group.group_name : "")))
                    .ForMember(vm => vm.files, conf => conf.MapFrom(db => (db.delivery_template_file)))
                    ;
                cfg.CreateMap<DeliveryTemplateViewModel, delivery_template>();
                //
                cfg.CreateMap<delivery_template_group, DeliveryTemplateGroupViewModel>();
                //
                cfg.CreateMap<delivery, DeliveryViewModel>()
                    .ForMember(vm => vm.template_name, conf => conf.MapFrom(db => db.delivery_template.template_name))
                    .ForMember(vm => vm.template_subject, conf => conf.MapFrom(db => db.delivery_template.subject))
                    .ForMember(vm => vm.template_body, conf => conf.MapFrom(db => db.delivery_template.body))
                    ;
                cfg.CreateMap<DeliveryViewModel, delivery>();
                //
                cfg.CreateMap<delivery_detail, DeliveryDetailViewModel>()
                    .ForMember(vm => vm.client_name, conf => conf.MapFrom(db => (db.client != null ? db.client.name : "")))
                    ;                
                //
                cfg.CreateMap<vw_client_service_param, ClientServiceParamViewModel>();
                cfg.CreateMap<client_service_param, ClientServiceParamViewModel>();
                cfg.CreateMap<ClientServiceParamViewModel, client_service_param>()
                    .ForMember(db => db.param_id, conf => conf.Ignore())
                    ;
                //
                cfg.CreateMap<report_discount, ReportDiscountViewModel>();
                //
                cfg.CreateMap<cab_user_menu, UserMenuViewModel>();
                cfg.CreateMap<UserMenuViewModel, cab_user_menu>();
                //
                cfg.CreateMap<version, VersionViewModel>()
                    .ForMember(vm => vm.is_deleted, conf => conf.MapFrom(db => db.is_deleted == 1 ? true : false))
                    ;
                cfg.CreateMap<VersionViewModel, version>()
                    .ForMember(db => db.is_deleted, conf => conf.MapFrom(vm => vm.is_deleted ? 1 : 0))
                    .ForMember(db => db.file_path_id, conf => conf.UseValue<int?>(null))
                    .ForMember(db => db.file_size, conf => conf.UseValue<int>(0))
                    ;
                //
                cfg.CreateMap<auth_group, AuthGroupViewModel>();
                //
                cfg.CreateMap<auth_section, AuthSectionViewModel>();
                //
                cfg.CreateMap<auth_section_part, AuthSectionPartViewModel>();
                //
                cfg.CreateMap<auth_role, AuthRoleViewModel>();
                //
                cfg.CreateMap<vw_auth_role, AuthRoleViewModel>();
                //
                cfg.CreateMap<vw_auth_user_role, AuthUserRoleViewModel>();
                //
                cfg.CreateMap<vw_auth_user_perm, AuthUserPermViewModel>();
                //
                cfg.CreateMap<vw_auth_role_perm, AuthRolePermViewModel>();
                //
                cfg.CreateMap<auth_role_group, AuthRoleGroupViewModel>();
                //
                cfg.CreateMap<storage_file_type, StorageFileTypeViewModel>()
                    .ForMember(vm => vm.FileTypeGroup, conf => conf.MapFrom(db => new StorageFileTypeGroupViewModel() {group_id = db.group_id, group_name = db.storage_file_type_group.group_name, }))                    
                    .ForMember(vm => vm.group_name, conf => conf.MapFrom(db => db.storage_file_type_group.group_name))
                    .ForMember(vm => vm.img, conf => conf.MapFrom(db => db.storage_file_type_group.img))
                    ;
                cfg.CreateMap<StorageFileTypeViewModel, storage_file_type>();

                //
                cfg.CreateMap<storage_file_version, StorageFileVersionViewModel>();
                cfg.CreateMap<StorageFileVersionViewModel, storage_file_version>();
                //
                cfg.CreateMap<storage_param, StorageParamViewModel>();
                cfg.CreateMap<StorageParamViewModel, storage_param>();
                //
                cfg.CreateMap<storage_file_type_group, StorageFileTypeGroupViewModel>();                
                cfg.CreateMap<StorageFileTypeGroupViewModel, storage_file_type_group>();
                //
                cfg.CreateMap<vw_log_storage, StorageLogViewModel>();
                //
                cfg.CreateMap<storage_link, StorageLinkViewModel>();
                cfg.CreateMap<StorageLinkViewModel, storage_link>();
                //
                cfg.CreateMap<vw_storage_trash, StorageTrashViewModel>();
                //
                cfg.CreateMap<service_kit, ServiceKitViewModel>();
                cfg.CreateMap<ServiceKitViewModel, service_kit>();
                //
                cfg.CreateMap<vw_service_kit_item, ServiceKitItemViewModel>();
                cfg.CreateMap<service_kit_item, ServiceKitItemViewModel>();
                cfg.CreateMap<ServiceKitItemViewModel, service_kit_item>();
                //
                cfg.CreateMap<vw_client_compare_cabinet, ClientCabViewModel>();
                //
                cfg.CreateMap<vw_client_compare_crm, ClientCrmViewModel>();
                //
                cfg.CreateMap<delivery_template_file, DeliveryTemplateFileViewModel>();
                cfg.CreateMap<DeliveryTemplateFileViewModel, delivery_template_file>();
                //
                cfg.CreateMap<campaign, CampaignViewModel>();
                cfg.CreateMap<CampaignViewModel, campaign>();
                cfg.CreateMap<vw_campaign, CampaignViewModel>();
                cfg.CreateMap<CampaignViewModel, vw_campaign>();
                //
                cfg.CreateMap<vw_client_max_version, ClientMaxVersionViewModel>();
                //
                cfg.CreateMap<campaign_business_org, CampaignBusinessOrgViewModel>()
                    .ForMember(vm => vm.org_id_str, conf => conf.MapFrom(db => db.org_id.ToString()))
                    ;
                cfg.CreateMap<CampaignBusinessOrgViewModel, campaign_business_org>();
                cfg.CreateMap<vw_campaign_business_org, CampaignBusinessOrgViewModel>()
                    .ForMember(vm => vm.org_id_str, conf => conf.MapFrom(db => db.org_id.ToString()))
                    ;
                cfg.CreateMap<CampaignBusinessOrgViewModel, vw_campaign_business_org>();
                //
                cfg.CreateMap<update_soft, UpdateSoftViewModel>();
                cfg.CreateMap<vw_update_soft, UpdateSoftViewModel>()
                    .ForMember(vm => vm.SoftGroup,
                    conf => conf.MapFrom(db => new UpdateSoftGroupViewModel() 
                    { 
                        group_id = db.group_id, group_name = db.group_name, 
                    }))
                    ;
                cfg.CreateMap<UpdateSoftViewModel, update_soft>();
                //
                cfg.CreateMap<update_soft_version, UpdateSoftVersionViewModel>();
                cfg.CreateMap<UpdateSoftVersionViewModel, update_soft_version>();
                //
                cfg.CreateMap<update_batch, UpdateBatchViewModel>();
                cfg.CreateMap<vw_update_batch, UpdateBatchViewModel>();
                cfg.CreateMap<UpdateBatchViewModel, update_batch>();
                //
                cfg.CreateMap<update_batch_item, UpdateBatchItemViewModel>();
                cfg.CreateMap<vw_update_batch_item, UpdateBatchItemViewModel>();
                cfg.CreateMap<UpdateBatchItemViewModel, update_batch_item>();
                //
                cfg.CreateMap<prj, PrjViewModel>();                
                cfg.CreateMap<vw_prj, PrjViewModel>()
                    .ForMember(vm => vm.CrmProject,
                    conf => conf.MapFrom(db => db.project_id == null ? null : new CrmProjectViewModel()
                    {
                        project_id = (int)db.project_id,
                        project_name = db.project_name,
                    }))
                    .ForMember(vm => vm.PrjStateType,
                    conf => conf.MapFrom(db => db.state_type_id == null ? null : new PrjStateTypeViewModel()
                    {
                        state_type_id = (int)db.state_type_id,
                        state_type_name = db.state_type_name,
                    }))                    
                    ;
                cfg.CreateMap<PrjViewModel, prj>()
                    .ForMember(db => db.crm_project,conf => conf.Ignore())                    
                    ;

                //
                cfg.CreateMap<prj_claim, PrjClaimViewModel>();
                cfg.CreateMap<vw_prj_claim, PrjClaimViewModel>();
                cfg.CreateMap<PrjClaimViewModel, prj_claim>();
                //
                cfg.CreateMap<prj_state_type, PrjStateTypeViewModel>();
                cfg.CreateMap<PrjStateTypeViewModel, prj_state_type>();
                //
                cfg.CreateMap<prj_task, PrjTaskViewModel>();
                cfg.CreateMap<vw_prj_task, PrjTaskViewModel>();
                cfg.CreateMap<PrjTaskViewModel, prj_task>();
                //
                cfg.CreateMap<prj_work, PrjWorkViewModel>();
                cfg.CreateMap<vw_prj_work, PrjWorkViewModel>();
                cfg.CreateMap<PrjWorkViewModel, prj_work>();
                //
                cfg.CreateMap<prj_work_type, PrjWorkTypeViewModel>();
                cfg.CreateMap<PrjWorkTypeViewModel, prj_work_type>();
                //
                cfg.CreateMap<prj_claim_state_type, PrjClaimStateTypeViewModel>();
                cfg.CreateMap<PrjClaimStateTypeViewModel, prj_claim_state_type>();
                //
                cfg.CreateMap<prj_task_state_type, PrjTaskStateTypeViewModel>();
                cfg.CreateMap<PrjTaskStateTypeViewModel, prj_task_state_type>();
                //
                cfg.CreateMap<prj_work_state_type, PrjWorkStateTypeViewModel>();
                cfg.CreateMap<PrjWorkStateTypeViewModel, prj_work_state_type>();
                //
                cfg.CreateMap<prj_mess, PrjMessViewModel>();
                cfg.CreateMap<vw_prj_mess, PrjMessViewModel>();
                cfg.CreateMap<PrjMessViewModel, prj_mess>();
                //
                cfg.CreateMap<prj_log, PrjLogViewModel>();
                cfg.CreateMap<vw_prj_log, PrjLogViewModel>();
                cfg.CreateMap<PrjLogViewModel, prj_log>();                
                //
                cfg.CreateMap<prj_user_settings, PrjUserSettingsViewModel>();
                cfg.CreateMap<PrjUserSettingsViewModel, prj_user_settings>();
                //
                cfg.CreateMap<prj_claim_default, PrjClaimDefaultViewModel>();
                cfg.CreateMap<PrjClaimDefaultViewModel, prj_claim_default>();
                //
                cfg.CreateMap<prj_work_default, PrjWorkDefaultViewModel>();
                cfg.CreateMap<vw_prj_work_default, PrjWorkDefaultViewModel>();
                cfg.CreateMap<PrjWorkDefaultViewModel, prj_work_default>();
                //
                cfg.CreateMap<prj_default, PrjDefaultViewModel>();
                cfg.CreateMap<PrjDefaultViewModel, prj_default>();
                //
                cfg.CreateMap<vw_prj_file, PrjFileViewModel>();
                cfg.CreateMap<prj_file, PrjFileViewModel>();
                cfg.CreateMap<PrjFileViewModel, prj_file>();
                //                
                cfg.CreateMap<service_stock, ServiceStockViewModel>();
                cfg.CreateMap<ServiceStockViewModel, service_stock>();
                //
                cfg.CreateMap<vw_stock_row, StockViewModel>();
                cfg.CreateMap<stock_row, StockViewModel>();
                cfg.CreateMap<StockViewModel, stock_row>();
                //
                cfg.CreateMap<vw_stock_batch, StockBatchViewModel>();
                cfg.CreateMap<StockBatchViewModel, vw_stock_batch>();
                //
                cfg.CreateMap<vw_stock_sales_download, StockSalesDownloadViewModel>();
                cfg.CreateMap<StockSalesDownloadViewModel, vw_stock_sales_download>();
                //
                cfg.CreateMap<update_soft_group, UpdateSoftGroupViewModel>();
                cfg.CreateMap<UpdateSoftGroupViewModel, update_soft_group>();
                //
                cfg.CreateMap<prj_notify_type, PrjNotifyTypeViewModel>();
                cfg.CreateMap<PrjNotifyTypeViewModel, prj_notify_type>();
                //
                cfg.CreateMap<prj_notify, PrjNotifyViewModel>();
                cfg.CreateMap<PrjNotifyViewModel, prj_notify>();
                //
                cfg.CreateMap<vw_asna_user, AsnaUserViewModel>()
                    .ForMember(vm => vm.sch_full_start_time_type_id, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? 1 : db.sch_full_start_time_type_id))
                    .ForMember(vm => vm.sch_full_end_time_type_id, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? 1 : db.sch_full_end_time_type_id))
                    .ForMember(vm => vm.sch_full_interval_type_id, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? 1 : db.sch_full_interval_type_id))
                    .ForMember(vm => vm.sch_full_is_active, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? true : db.sch_full_is_active))
                    .ForMember(vm => vm.sch_full_is_day1, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? false : db.sch_full_is_day1))
                    .ForMember(vm => vm.sch_full_is_day2, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? false : db.sch_full_is_day2))
                    .ForMember(vm => vm.sch_full_is_day3, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? false : db.sch_full_is_day3))
                    .ForMember(vm => vm.sch_full_is_day4, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? false : db.sch_full_is_day4))
                    .ForMember(vm => vm.sch_full_is_day5, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? false : db.sch_full_is_day5))
                    .ForMember(vm => vm.sch_full_is_day6, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? false : db.sch_full_is_day6))
                    .ForMember(vm => vm.sch_full_is_day7, conf => conf.MapFrom(db => db.sch_full_schedule_id == null ? false : db.sch_full_is_day7))
                    //
                    .ForMember(vm => vm.sch_ch_start_time_type_id, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? 1 : db.sch_ch_start_time_type_id))
                    .ForMember(vm => vm.sch_ch_end_time_type_id, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? 1 : db.sch_ch_end_time_type_id))
                    .ForMember(vm => vm.sch_ch_interval_type_id, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? 1 : db.sch_ch_interval_type_id))
                    .ForMember(vm => vm.sch_ch_is_active, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? true : db.sch_ch_is_active))
                    .ForMember(vm => vm.sch_ch_is_day1, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? false : db.sch_ch_is_day1))
                    .ForMember(vm => vm.sch_ch_is_day2, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? false : db.sch_ch_is_day2))
                    .ForMember(vm => vm.sch_ch_is_day3, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? false : db.sch_ch_is_day3))
                    .ForMember(vm => vm.sch_ch_is_day4, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? false : db.sch_ch_is_day4))
                    .ForMember(vm => vm.sch_ch_is_day5, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? false : db.sch_ch_is_day5))
                    .ForMember(vm => vm.sch_ch_is_day6, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? false : db.sch_ch_is_day6))
                    .ForMember(vm => vm.sch_ch_is_day7, conf => conf.MapFrom(db => db.sch_ch_schedule_id == null ? false : db.sch_ch_is_day7))
                    ;
                cfg.CreateMap<asna_user, AsnaUserViewModel>();
                cfg.CreateMap<AsnaUserViewModel, asna_user>();
                //
                cfg.CreateMap<vw_wa_task, WaTaskViewModel>()
                    .ForMember(vm => vm.task_param, conf => conf.Ignore())
                    ;
                cfg.CreateMap<wa_task, WaTaskViewModel>()
                    .ForMember(vm => vm.task_param, conf => conf.Ignore())
                    ;
                cfg.CreateMap<WaTaskViewModel, wa_task>();
                //                
                cfg.CreateMap<wa_request_type, WaRequestTypeViewModel>();               
                cfg.CreateMap<WaRequestTypeViewModel, wa_request_type>();
                //                
                cfg.CreateMap<asna_schedule_interval_type, AsnaScheduleIntervalTypeViewModel>()
                    .ReverseMap();
                //                
                cfg.CreateMap<asna_schedule_time_type, AsnaScheduleTimeTypeViewModel>()
                    .ReverseMap();
                //
                cfg.CreateMap<asna_user_log, AsnaUserLogViewModel>();
                cfg.CreateMap<vw_asna_user_log, AsnaUserLogViewModel>();
                //
                cfg.CreateMap<asna_link_row, AsnaLinkViewModel>();
                //
                cfg.CreateMap<vw_wa_task_lc, WaTaskLcViewModel>();
                //
                cfg.CreateMap<vw_asna_stock_row, AsnaStockRowViewModel>();
                //
                cfg.CreateMap<vw_asna_stock_chain, AsnaStockChainViewModel>();
                //
                cfg.CreateMap<vw_asna_stock_row_actual, AsnaStockRowActualViewModel>();
                //
                cfg.CreateMap<asna_order_state_type, AsnaOrderStateTypeViewModel>();
                //
                cfg.CreateMap<vw_asna_order, AsnaOrderViewModel>();
                //
                cfg.CreateMap<vw_region_zone, SpravRegionZoneViewModel>();
                cfg.CreateMap<region_zone, SpravRegionZoneViewModel>().ReverseMap();
                //
                cfg.CreateMap<vw_region_price_limit, SpravRegionPriceLimitViewModel>();
                //
                cfg.CreateMap<vw_region_zone_price_limit, SpravRegionZonePriceLimitViewModel>();
                //
                cfg.CreateMap<tax_system_type, SpravTaxSystemTypeViewModel>();
                //
                cfg.CreateMap<programm_param_type, ProgrammParamTypeViewModel>();
                //
                cfg.CreateMap<programm_step, ProgrammStepViewModel>()
                    .ForMember(vm => vm.single_use_only_chb, conf => conf.MapFrom(db => db.single_use_only == 1 ? true : false))
                    ;
                cfg.CreateMap<ProgrammStepViewModel, programm_step>()
                    .ForMember(db => db.single_use_only, conf => conf.MapFrom(vm => vm.single_use_only_chb ? (short)1 : (short)0))
                    ;
                //
                cfg.CreateMap<programm_step_param, ProgrammStepParamViewModel>();
                cfg.CreateMap<vw_programm_step_param, ProgrammStepParamViewModel>();
                //
                cfg.CreateMap<programm_step_param_type, ProgrammStepParamTypeViewModel>();
                //
                cfg.CreateMap<programm_card_param_type, ProgrammCardParamTypeViewModel>();
                //
                cfg.CreateMap<programm_trans_param_type, ProgrammTransParamTypeViewModel>();
                //
                cfg.CreateMap<vw_programm_step_condition, ProgrammStepConditionViewModel>();
                //
                cfg.CreateMap<programm_step_condition_type, ProgrammStepConditionTypeViewModel>();
                //
                cfg.CreateMap<programm_step_logic_type, ProgrammStepLogicTypeViewModel>();
                //
                cfg.CreateMap<vw_programm_step_logic, ProgrammStepLogicViewModel>();
                //
                cfg.CreateMap<vw_asna_order_row, AsnaOrderRowViewModel>();
                //
                cfg.CreateMap<vw_asna_order_state, AsnaOrderStateViewModel>();
                //
                cfg.CreateMap<vw_asna_order_row_state, AsnaOrderRowStateViewModel>();
                //
                cfg.CreateMap<prj_user_group, PrjUserGroupViewModel>()
                    .ForMember(vm => vm.group_html, conf => conf.Ignore());
                //
                cfg.CreateMap<prj_brief_type, PrjBriefTypeViewModel>();
                //
                cfg.CreateMap<prj_brief_interval_type, PrjBriefIntervalTypeViewModel>();
                //
                cfg.CreateMap<prj_user_group_item_type, PrjUserGroupItemTypeViewModel>();
                //
                cfg.CreateMap<vw_prj_note, PrjNoteViewModel>().ReverseMap();
                cfg.CreateMap<PrjNoteViewModel, prj_note>();
                //
                cfg.CreateMap<vw_prj_user_group_item, PrjUserGroupItemViewModel>().ReverseMap();
                cfg.CreateMap<PrjUserGroupItemViewModel, prj_user_group_item>();
                //
                cfg.CreateMap<prj_brief_date_cut_type, PrjBriefDateCutTypeViewModel>();
                //
                cfg.CreateMap<vw_client_report1, ClientReportViewModel>();
                //
                cfg.CreateMap<prj_claim_type, PrjClaimTypeViewModel>();
                //
                cfg.CreateMap<prj_claim_stage, PrjClaimStageViewModel>();
                //
                cfg.CreateMap<prj_task_user_state_type, PrjTaskUserStateTypeViewModel>();
                //
                cfg.CreateMap<service_esn2, ServiceEsn2ViewModel>();
                cfg.CreateMap<ServiceEsn2ViewModel, service_esn2>();
                //
                cfg.CreateMap<vw_version_main, VersionMainViewModel>();
                //
                cfg.CreateMap<PrjProjectVersionViewModel, prj_project_version>().ReverseMap();
                cfg.CreateMap<vw_prj_project_version, PrjProjectVersionViewModel>();
                //
                cfg.CreateMap<prj_claim_field, PrjClaimFieldViewModel>();
                //
                cfg.CreateMap<prj_claim_filter_op_type, PrjClaimFilterOpTypeViewModel>();
                //
                cfg.CreateMap<prj_claim_filter, PrjClaimFilterViewModel>();
                //
                cfg.CreateMap<vw_prj_claim_filter_item, PrjClaimFilterItemViewModel>();
                //
                cfg.CreateMap<prj_version_type, VersionTypeViewModel>();
                //
                cfg.CreateMap<vw_prj_report1, PrjReport1ViewModel>();
                //
                cfg.CreateMap<vw_stock_log, StockLogViewModel>();
            });    
        }

        public IConfigurationProvider ConfigurationProvider { get { return Mapper.Configuration; } }

        public Func<Type, object> ServiceCtor { get { return Mapper.Configuration.ServiceCtor; } }

        
        public object Map(object source, Type sourceType, Type destinationType)
        {                        
            return Mapper.Map(source, sourceType, destinationType); 
        }

        public TDestination Map<TDestination>(object source)
        {
            return Mapper.Map<TDestination>(source);
        }

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return Mapper.Map<TSource, TDestination>(source);
        }

        public TDestination Map<TDestination>(object source, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map<TDestination>(source, opts);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return Mapper.Map<TSource, TDestination>(source, destination);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            return Mapper.Map<TSource, TDestination>(source, destination, opts);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType)
        {
            return Mapper.Map(source, destination, sourceType, destinationType);
        }

        public object Map(object source, Type sourceType, Type destinationType, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map(source, sourceType, destinationType, opts);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map(source, destination, sourceType, destinationType, opts);
        }

        public TDestination Map<TSource, TDestination>(TSource source, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            return Mapper.Map<TSource, TDestination>(source, opts);
        }
    }
}