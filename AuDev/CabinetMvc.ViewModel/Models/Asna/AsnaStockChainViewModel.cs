﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaStockChainViewModel : AuBaseViewModel
    {

        public AsnaStockChainViewModel()
            : base(Enums.MainObjectType.ASNA_STOCK_CHAIN)
        {           
            //vw_asna_stock_chain
        } 

        [Key()]
        [Display(Name = "Код")]
        public int chain_id { get; set; }

        [Display(Name = "Пользователь")]
        public int asna_user_id { get; set; }

        [Display(Name = "Номер")]
        public int chain_num { get; set; }

        [Display(Name = "Дата выгрузки остатков")]
        public Nullable<System.DateTime> stock_date { get; set; }

        [Display(Name = "Полные остатки")]
        public bool is_full { get; set; }

        [Display(Name = "Создан")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменен")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "Активно")]
        public bool is_active { get; set; }

        [Display(Name = "Когда завершено")]
        public Nullable<System.DateTime> inactive_date { get; set; }

        [Display(Name = "РМ")]
        public int workplace_id { get; set; }

        [Display(Name = "РМ")]
        public string workplace_name { get; set; }

        [Display(Name = "ТТ")]
        public int sales_id { get; set; }

        [Display(Name = "ТТ")]
        public string sales_name { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "Название")]
        public string chain_name { get; set; }
    }    

}