﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaLinkViewModel : AuBaseViewModel
    {

        public AsnaLinkViewModel()
            : base(Enums.MainObjectType.ASNA_LINK)
        {           
            //asna_link_row
        } 

        [Key()]
        [Display(Name = "Код")]
        public int row_id { get; set; }

        [Display(Name = "link_id")]
        public int link_id { get; set; }

        [Display(Name = "Код АСНА")]
        public Nullable<int> asna_nnt { get; set; }

        [Display(Name = "Код аптеки")]
        public string asna_sku { get; set; }

        [Display(Name = "Наименование")]
        public string asna_name { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "Уникально")]
        public bool is_unique { get; set; }
    }
    

}