﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaStockRowActualService : IAuBaseService
    {
        //
    }

    public class AsnaStockRowActualService : AuBaseService, IAsnaStockRowActualService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_asna_stock_row_actual
                .Where(ss => ss.sales_id == parent_id)
                .ProjectTo<AsnaStockRowActualViewModel>();
        }

    }
}