﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaScheduleTimeTypeViewModel : AuBaseViewModel
    {

        public AsnaScheduleTimeTypeViewModel()
            : base(Enums.MainObjectType.ASNA_SCHEDULE_TIME_TYPE)
        {           
            //asna_schedule_time_type
        } 

        [Key()]
        [Display(Name = "Код")]
        public int time_type_id { get; set; }

        [Display(Name = "Название")]
        public string time_type_name { get; set; }

        [Display(Name = "Значение")]
        public Nullable<int> time_type_value { get; set; }
    }

}