﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaStockRowViewModel : AuBaseViewModel
    {

        public AsnaStockRowViewModel()
            : base(Enums.MainObjectType.ASNA_STOCK_ROW)
        {           
            //vw_asna_stock_row
        } 

        [Key()]
        [Display(Name = "Код")]
        public int row_id { get; set; }

        [Display(Name = "Пользователь")]
        public int asna_user_id { get; set; }

        [Display(Name = "Пришло в пакете")]
        public Nullable<int> in_batch_id { get; set; }

        [Display(Name = "Ушло в пакете")]
        public Nullable<int> out_batch_id { get; set; }

        [Display(Name = "Артикул")]
        public int artikul { get; set; }

        [Display(Name = "row_stock_id")]
        public Nullable<int> row_stock_id { get; set; }

        [Display(Name = "Код ЕСн")]
        public Nullable<int> esn_id { get; set; }

        [Display(Name = "Код товара в аптеке")]
        public Nullable<int> prep_id { get; set; }

        [Display(Name = "Наименование товара в аптеке")]
        public string prep_name { get; set; }

        [Display(Name = "Производитель")]
        public Nullable<int> firm_id { get; set; }

        [Display(Name = "Производитель")]
        public string firm_name { get; set; }

        [Display(Name = "Штрих-код")]
        public string barcode { get; set; }

        [Display(Name = "Уникальный код партии товара")]
        public string asna_prt_id { get; set; }

        [Display(Name = "Уникальный код товара в аптеке")]
        public string asna_sku { get; set; }

        [Display(Name = "Текущее количество в партии")]
        public Nullable<decimal> asna_qnt { get; set; }

        [Display(Name = "Номер документа")]
        public string asna_doc { get; set; }

        [Display(Name = "ИНН поставщика")]
        public string asna_sup_inn { get; set; }

        [Display(Name = "asna_sup_id")]
        public string asna_sup_id { get; set; }

        [Display(Name = "Дата поставки")]
        public Nullable<System.DateTime> asna_sup_date { get; set; }

        [Display(Name = "НДС товара")]
        public Nullable<int> asna_nds { get; set; }

        [Display(Name = "Признак ЖНВЛС")]
        public bool asna_gnvls { get; set; }

        [Display(Name = "Признак Детское питание")]
        public bool asna_dp { get; set; }

        [Display(Name = "Производитель")]
        public string asna_man { get; set; }

        [Display(Name = "Серия товара")]
        public string asna_series { get; set; }

        [Display(Name = "Срок годности")]
        public Nullable<System.DateTime> asna_exp { get; set; }

        [Display(Name = "Цена производителя")]
        public Nullable<decimal> asna_prc_man { get; set; }

        [Display(Name = "Цена оптовая")]
        public Nullable<decimal> asna_prc_opt { get; set; }

        [Display(Name = "Цена оптовая с НДС")]
        public Nullable<decimal> asna_prc_opt_nds { get; set; }

        [Display(Name = "Цена реализации")]
        public Nullable<decimal> asna_prc_ret { get; set; }

        [Display(Name = "Цена госреестра")]
        public Nullable<decimal> asna_prc_reestr { get; set; }

        [Display(Name = "Максимально допустимая розничная цена для ЖНВЛС")]
        public Nullable<decimal> asna_prc_gnvls_max { get; set; }

        [Display(Name = "Сумма оптовая без НДС")]
        public Nullable<decimal> asna_sum_opt { get; set; }

        [Display(Name = "Сумма оптовая с НДС")]
        public Nullable<decimal> asna_sum_opt_nds { get; set; }

        [Display(Name = "Процент наценки для ЖВЛНС и ДП")]
        public Nullable<decimal> asna_mr_gnvls { get; set; }

        [Display(Name = "Признак товара")]
        public Nullable<int> asna_tag { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "Код аптеки по АСНА")]
        public string asna_store_id { get; set; }

        [Display(Name = "Код ЮЛ по АСНА")]        
        public string asna_legal_id { get; set; }

        [Display(Name = "Код справочника связок")]
        public Nullable<int> asna_rr_id { get; set; }

        [Display(Name = "РМ")]
        public int workplace_id { get; set; }

        [Display(Name = "РМ")]
        public string workplace_name { get; set; }

        [Display(Name = "ТТ")]
        public int sales_id { get; set; }

        [Display(Name = "ТТ")]
        public string sales_name { get; set; }

        [Display(Name = "Организация")]
        public int client_id { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }

        [Display(Name = "Цепочка")]
        public Nullable<int> chain_id { get; set; }

        [Display(Name = "Код в справочнике АСНА")]
        public Nullable<int> asna_nnt { get; set; }

        [Display(Name = "Наименование в справочнике АСНА")]
        public string asna_name { get; set; }

        [Display(Name = "Связано")]
        public bool is_linked { get; set; }

        [Display(Name = "Уникально")]
        public Nullable<bool> is_unique { get; set; }
    }    

}