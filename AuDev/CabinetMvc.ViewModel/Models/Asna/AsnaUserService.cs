﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaUserService : IAuBaseService
    {
        AsnaUserViewModel GetItem_byWorkplace(int workplace_id);
        AsnaUserViewModel GetItem_bySales(int sales_id);
        bool InsertTask(int asna_user_id, int request_type_id, ModelStateDictionary modelState);
        bool UpdateTask(int asna_user_id, int task_id, int request_type_id, ModelStateDictionary modelState);
        bool DeleteTask(int asna_user_id, int task_id, ModelStateDictionary modelState);
        bool CancelTask(int asna_user_id, int task_id, ModelStateDictionary modelState);
    }

    public class AsnaUserService : AuBaseService, IAsnaUserService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_asna_user
                .Where(ss => ss.client_id == parent_id)
                .ProjectTo<AsnaUserViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_asna_user                
                .ProjectTo<AsnaUserViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_asna_user
                .Where(ss => ss.asna_user_id == id)
                .ProjectTo<AsnaUserViewModel>()
                .FirstOrDefault();  
        }

        public AsnaUserViewModel GetItem_byWorkplace(int workplace_id)
        {
            return dbContext.vw_asna_user
                .Where(ss => ss.workplace_id == workplace_id)
                .ProjectTo<AsnaUserViewModel>()
                .FirstOrDefault();
        }

        public AsnaUserViewModel GetItem_bySales(int sales_id)
        {
            return dbContext.vw_asna_user
                .Where(ss => ss.sales_id == sales_id)
                .ProjectTo<AsnaUserViewModel>()
                .FirstOrDefault();
        }

        /*
        public AsnaScheduleViewModel CreateSchedule_byUser(int asna_user_id, int schedule_type_id)
        {
            return new AsnaScheduleViewModel()
            {
                asna_user_id = asna_user_id,
                schedule_type_id = schedule_type_id,
                is_active = true,
                start_time_type_id = 1,
                end_time_type_id = 1,
                interval_type_id = 1,
                is_day1 = true,
                is_day2 = true,
                is_day3 = true,
                is_day4 = true,
                is_day5 = true,
                is_day6 = false,
                is_day7 = false,
            };
        }
        */

        private asna_user_log prepareLog(string mess, DateTime crt_date, string crt_user)
        {
            asna_user_log asna_user_log = new asna_user_log();
            asna_user_log.mess = mess;
            asna_user_log.crt_date = crt_date;
            asna_user_log.crt_user = crt_user;
            return asna_user_log;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is AsnaUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            AsnaUserViewModel itemAdd = (AsnaUserViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return null;
            }

            vw_asna_user asna_user_existing = (from t1 in dbContext.workplace
                                               from t2 in dbContext.vw_asna_user
                                               where t1.sales_id == t2.sales_id
                                               && t1.id == itemAdd.workplace_id
                                               //&& t2.workplace_id != itemAdd.workplace_id
                                               select t2)
                                              .FirstOrDefault();
            if (asna_user_existing != null)
            {
                modelState.AddModelError("", "Уже есть пользователь на этой торговой точке");
                return null;
            }

            asna_user_existing = dbContext.vw_asna_user.Where(ss => ss.asna_user_id != itemAdd.asna_user_id && ss.asna_store_id != null && ss.asna_store_id.Trim().ToLower().Equals(itemAdd.asna_store_id.Trim().ToLower())).FirstOrDefault();
            if (asna_user_existing != null)
            {
                modelState.AddModelError("", "Уже есть пользователь с таким кодом аптеки по АСНА");
                return null;
            }

            vw_workplace vw_workplace = dbContext.vw_workplace.Where(ss => ss.workplace_id == itemAdd.workplace_id).FirstOrDefault();
            if (vw_workplace == null)
            {
                modelState.AddModelError("", "Не найдено рабочее место с кодом " + itemAdd.workplace_id.ToString());
                return null;
            }

            DateTime now = DateTime.Now;
            string user = currUser.CabUserName;

            asna_user asna_user = new asna_user();
            ModelMapper.Map<AsnaUserViewModel, asna_user>(itemAdd, asna_user);

            asna_user.is_active = true;
            asna_user.crt_date = now;
            asna_user.crt_user = user;
            asna_user.upd_date = now;
            asna_user.upd_user = user;
            asna_user.is_deleted = false;
            dbContext.asna_user.Add(asna_user);

            asna_schedule asna_schedule_full = new asna_schedule();
            asna_schedule_full.asna_user = asna_user;
            asna_schedule_full.schedule_type_id = (int)Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS;
            asna_schedule_full.is_active = itemAdd.sch_full_is_active.GetValueOrDefault(false);
            asna_schedule_full.interval_type_id = itemAdd.sch_full_interval_type_id;
            asna_schedule_full.start_time_type_id = itemAdd.sch_full_start_time_type_id;
            asna_schedule_full.end_time_type_id = itemAdd.sch_full_end_time_type_id;
            asna_schedule_full.is_day1 = itemAdd.sch_full_is_day1.GetValueOrDefault(false);
            asna_schedule_full.is_day2 = itemAdd.sch_full_is_day2.GetValueOrDefault(false);
            asna_schedule_full.is_day3 = itemAdd.sch_full_is_day3.GetValueOrDefault(false);
            asna_schedule_full.is_day4 = itemAdd.sch_full_is_day4.GetValueOrDefault(false);
            asna_schedule_full.is_day5 = itemAdd.sch_full_is_day5.GetValueOrDefault(false);
            asna_schedule_full.is_day6 = itemAdd.sch_full_is_day6.GetValueOrDefault(false);
            asna_schedule_full.is_day7 = itemAdd.sch_full_is_day7.GetValueOrDefault(false);
            asna_schedule_full.crt_date = now;
            asna_schedule_full.crt_user = user;
            asna_schedule_full.upd_date = now;
            asna_schedule_full.upd_user = user;
            asna_schedule_full.is_deleted = false;

            dbContext.asna_schedule.Add(asna_schedule_full);

            asna_schedule asna_schedule_ch = new asna_schedule();
            asna_schedule_ch.asna_user = asna_user;
            asna_schedule_ch.schedule_type_id = (int)Enums.AsnaScheduleTypeEnum.SEND_CHANGED_REMAINS;
            asna_schedule_ch.is_active = itemAdd.sch_ch_is_active.GetValueOrDefault(false);
            asna_schedule_ch.interval_type_id = itemAdd.sch_ch_interval_type_id;
            asna_schedule_ch.start_time_type_id = itemAdd.sch_ch_start_time_type_id;
            asna_schedule_ch.end_time_type_id = itemAdd.sch_ch_end_time_type_id;
            asna_schedule_ch.is_day1 = itemAdd.sch_ch_is_day1.GetValueOrDefault(false);
            asna_schedule_ch.is_day2 = itemAdd.sch_ch_is_day2.GetValueOrDefault(false);
            asna_schedule_ch.is_day3 = itemAdd.sch_ch_is_day3.GetValueOrDefault(false);
            asna_schedule_ch.is_day4 = itemAdd.sch_ch_is_day4.GetValueOrDefault(false);
            asna_schedule_ch.is_day5 = itemAdd.sch_ch_is_day5.GetValueOrDefault(false);
            asna_schedule_ch.is_day6 = itemAdd.sch_ch_is_day6.GetValueOrDefault(false);
            asna_schedule_ch.is_day7 = itemAdd.sch_ch_is_day7.GetValueOrDefault(false);
            asna_schedule_ch.crt_date = now;
            asna_schedule_ch.crt_user = user;
            asna_schedule_ch.upd_date = now;
            asna_schedule_ch.upd_user = user;
            asna_schedule_ch.is_deleted = false;

            dbContext.asna_schedule.Add(asna_schedule_ch);

            asna_user_log asna_user_log = null;

            asna_user_log = prepareLog("Создан пользователь [" + vw_workplace.client_name + "] [" + vw_workplace.sales_address + "] [" + vw_workplace.registration_key + "]", DateTime.Now, user);
            asna_user_log.asna_user = asna_user;
            dbContext.asna_user_log.Add(asna_user_log);
            if (asna_schedule_full.is_active)
            {
                asna_user_log = prepareLog("Включена отправка полных остатков", DateTime.Now.AddSeconds(1), user);
                asna_user_log.asna_user = asna_user;
                dbContext.asna_user_log.Add(asna_user_log);
            }
            if (asna_schedule_ch.is_active)
            {
                asna_user_log = prepareLog("Включена отправка изменений остатков", DateTime.Now.AddSeconds(1), user);
                asna_user_log.asna_user = asna_user;
                dbContext.asna_user_log.Add(asna_user_log);
            }

            dbContext.SaveChanges();

            return xGetItem(asna_user.asna_user_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is AsnaUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            AsnaUserViewModel itemEdit = (AsnaUserViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры пользователя");
                return null;
            }

            asna_user asna_user = dbContext.asna_user.Where(ss => ss.asna_user_id == itemEdit.asna_user_id).FirstOrDefault();
            if (asna_user == null)
            {
                modelState.AddModelError("", "Не найдены параметры пользователя c кодом " + itemEdit.asna_user_id.ToString());
                return null;
            }

            vw_workplace vw_workplace = dbContext.vw_workplace.Where(ss => ss.workplace_id == itemEdit.workplace_id).FirstOrDefault();
            if (vw_workplace == null)
            {
                modelState.AddModelError("", "Не найдено рабочее место с кодом " + itemEdit.workplace_id.ToString());
                return null;
            }

            vw_asna_user asna_user_existing = dbContext.vw_asna_user.Where(ss => ss.asna_user_id != itemEdit.asna_user_id && ss.asna_store_id != null && ss.asna_store_id.Trim().ToLower().Equals(itemEdit.asna_store_id.Trim().ToLower())).FirstOrDefault();
            if (asna_user_existing != null)
            {
                modelState.AddModelError("", "Уже есть пользователь с таким кодом аптеки по АСНА");
                return null;
            }

            DateTime now = DateTime.Now;
            string user = currUser.CabUserName;

            AsnaUserViewModel oldModel = new AsnaUserViewModel();
            ModelMapper.Map<asna_user, AsnaUserViewModel>(asna_user, oldModel);
            modelBeforeChanges = oldModel;

            asna_user_log asna_user_log = null;

            if (asna_user.is_active != itemEdit.is_active)
            {
                asna_user.is_active = itemEdit.is_active;
                //
                asna_user_log = prepareLog("Поставлен признак: Пользователь " + (itemEdit.is_active ? "активен" : "неактивен"), now, user);
                asna_user_log.asna_user = asna_user;
                dbContext.asna_user_log.Add(asna_user_log);                
            }
            
            if (asna_user.workplace_id != itemEdit.workplace_id)
            {
                asna_user.workplace_id = itemEdit.workplace_id;
                //
                asna_user_log = prepareLog("Изменено рабочее место на " + vw_workplace.registration_key, now, user);
                asna_user_log.asna_user = asna_user;
                dbContext.asna_user_log.Add(asna_user_log);
            }
            
            asna_user.asna_store_id = itemEdit.asna_store_id;
            asna_user.asna_pwd = itemEdit.asna_pwd;
            asna_user.asna_rr_id = itemEdit.asna_rr_id;

            asna_user.upd_date = now;
            asna_user.upd_user = user;

            asna_schedule asna_schedule_full = dbContext.asna_schedule.Where(ss => ss.asna_user_id == asna_user.asna_user_id && ss.schedule_type_id == (int)Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS).FirstOrDefault();
            if (asna_schedule_full == null)
            {
                asna_schedule_full = new asna_schedule();
                asna_schedule_full.asna_user = asna_user;
                asna_schedule_full.schedule_type_id = (int)Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS;
                asna_schedule_full.is_active = itemEdit.sch_full_is_active.GetValueOrDefault(false);
                asna_schedule_full.interval_type_id = itemEdit.sch_full_interval_type_id;
                asna_schedule_full.start_time_type_id = itemEdit.sch_full_start_time_type_id;
                asna_schedule_full.end_time_type_id = itemEdit.sch_full_end_time_type_id;
                asna_schedule_full.is_day1 = itemEdit.sch_full_is_day1.GetValueOrDefault(false);
                asna_schedule_full.is_day2 = itemEdit.sch_full_is_day2.GetValueOrDefault(false);
                asna_schedule_full.is_day3 = itemEdit.sch_full_is_day3.GetValueOrDefault(false);
                asna_schedule_full.is_day4 = itemEdit.sch_full_is_day4.GetValueOrDefault(false);
                asna_schedule_full.is_day5 = itemEdit.sch_full_is_day5.GetValueOrDefault(false);
                asna_schedule_full.is_day6 = itemEdit.sch_full_is_day6.GetValueOrDefault(false);
                asna_schedule_full.is_day7 = itemEdit.sch_full_is_day7.GetValueOrDefault(false);
                asna_schedule_full.crt_date = now;
                asna_schedule_full.crt_user = user;
                asna_schedule_full.upd_date = now;
                asna_schedule_full.upd_user = user;
                asna_schedule_full.is_deleted = false;
                dbContext.asna_schedule.Add(asna_schedule_full);
                //
                if (asna_schedule_full.is_active)
                {
                    asna_user_log = prepareLog("Включена отправка полных остатков", now, user);
                    asna_user_log.asna_user = asna_user;
                    dbContext.asna_user_log.Add(asna_user_log);
                }
            }
            else
            {
                if (asna_schedule_full.is_active != itemEdit.sch_full_is_active.GetValueOrDefault(false))
                {
                    asna_schedule_full.is_active = itemEdit.sch_full_is_active.GetValueOrDefault(false);
                    //
                    asna_user_log = prepareLog((itemEdit.sch_full_is_active.GetValueOrDefault(false) ? "Включена" : "Отключена") + " отправка полных остатков", now, user);
                    asna_user_log.asna_user = asna_user;
                    dbContext.asna_user_log.Add(asna_user_log);
                }                
                asna_schedule_full.interval_type_id = itemEdit.sch_full_interval_type_id;
                asna_schedule_full.start_time_type_id = itemEdit.sch_full_start_time_type_id;
                asna_schedule_full.end_time_type_id = itemEdit.sch_full_end_time_type_id;
                asna_schedule_full.is_day1 = itemEdit.sch_full_is_day1.GetValueOrDefault(false);
                asna_schedule_full.is_day2 = itemEdit.sch_full_is_day2.GetValueOrDefault(false);
                asna_schedule_full.is_day3 = itemEdit.sch_full_is_day3.GetValueOrDefault(false);
                asna_schedule_full.is_day4 = itemEdit.sch_full_is_day4.GetValueOrDefault(false);
                asna_schedule_full.is_day5 = itemEdit.sch_full_is_day5.GetValueOrDefault(false);
                asna_schedule_full.is_day6 = itemEdit.sch_full_is_day6.GetValueOrDefault(false);
                asna_schedule_full.is_day7 = itemEdit.sch_full_is_day7.GetValueOrDefault(false);
                asna_schedule_full.upd_date = now;
                asna_schedule_full.upd_user = user;
            }

            asna_schedule asna_schedule_ch = dbContext.asna_schedule.Where(ss => ss.asna_user_id == asna_user.asna_user_id && ss.schedule_type_id == (int)Enums.AsnaScheduleTypeEnum.SEND_CHANGED_REMAINS).FirstOrDefault();
            if (asna_schedule_ch == null)
            {
                asna_schedule_ch = new asna_schedule();
                asna_schedule_ch.asna_user = asna_user;
                asna_schedule_ch.schedule_type_id = (int)Enums.AsnaScheduleTypeEnum.SEND_CHANGED_REMAINS;
                asna_schedule_ch.is_active = itemEdit.sch_ch_is_active.GetValueOrDefault(false);
                asna_schedule_ch.interval_type_id = itemEdit.sch_ch_interval_type_id;
                asna_schedule_ch.start_time_type_id = itemEdit.sch_ch_start_time_type_id;
                asna_schedule_ch.end_time_type_id = itemEdit.sch_ch_end_time_type_id;
                asna_schedule_ch.is_day1 = itemEdit.sch_ch_is_day1.GetValueOrDefault(false);
                asna_schedule_ch.is_day2 = itemEdit.sch_ch_is_day2.GetValueOrDefault(false);
                asna_schedule_ch.is_day3 = itemEdit.sch_ch_is_day3.GetValueOrDefault(false);
                asna_schedule_ch.is_day4 = itemEdit.sch_ch_is_day4.GetValueOrDefault(false);
                asna_schedule_ch.is_day5 = itemEdit.sch_ch_is_day5.GetValueOrDefault(false);
                asna_schedule_ch.is_day6 = itemEdit.sch_ch_is_day6.GetValueOrDefault(false);
                asna_schedule_ch.is_day7 = itemEdit.sch_ch_is_day7.GetValueOrDefault(false);
                asna_schedule_ch.crt_date = now;
                asna_schedule_ch.crt_user = user;
                asna_schedule_ch.upd_date = now;
                asna_schedule_ch.upd_user = user;
                asna_schedule_ch.is_deleted = false;
                dbContext.asna_schedule.Add(asna_schedule_ch);
                //
                if (asna_schedule_ch.is_active)
                {
                    asna_user_log = prepareLog("Включена отправка изменений остатков", now, user);
                    asna_user_log.asna_user = asna_user;
                    dbContext.asna_user_log.Add(asna_user_log);
                }
            }
            else
            {
                if (asna_schedule_ch.is_active != itemEdit.sch_ch_is_active.GetValueOrDefault(false))
                {
                    asna_schedule_ch.is_active = itemEdit.sch_ch_is_active.GetValueOrDefault(false);
                    //
                    asna_user_log = prepareLog((itemEdit.sch_ch_is_active.GetValueOrDefault(false) ? "Включена" : "Отключена") + " отправка изменений остатков", now, user);
                    asna_user_log.asna_user = asna_user;
                    dbContext.asna_user_log.Add(asna_user_log);
                }                
                asna_schedule_ch.interval_type_id = itemEdit.sch_ch_interval_type_id;
                asna_schedule_ch.start_time_type_id = itemEdit.sch_ch_start_time_type_id;
                asna_schedule_ch.end_time_type_id = itemEdit.sch_ch_end_time_type_id;
                asna_schedule_ch.is_day1 = itemEdit.sch_ch_is_day1.GetValueOrDefault(false);
                asna_schedule_ch.is_day2 = itemEdit.sch_ch_is_day2.GetValueOrDefault(false);
                asna_schedule_ch.is_day3 = itemEdit.sch_ch_is_day3.GetValueOrDefault(false);
                asna_schedule_ch.is_day4 = itemEdit.sch_ch_is_day4.GetValueOrDefault(false);
                asna_schedule_ch.is_day5 = itemEdit.sch_ch_is_day5.GetValueOrDefault(false);
                asna_schedule_ch.is_day6 = itemEdit.sch_ch_is_day6.GetValueOrDefault(false);
                asna_schedule_ch.is_day7 = itemEdit.sch_ch_is_day7.GetValueOrDefault(false);
                asna_schedule_ch.upd_date = now;
                asna_schedule_ch.upd_user = user;
            }

            dbContext.SaveChanges();

            return xGetItem(asna_user.asna_user_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is AsnaUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            AsnaUserViewModel itemDel = (AsnaUserViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры пользователя");
                return false;
            }

            asna_user asna_user = dbContext.asna_user.Where(ss => ss.asna_user_id == itemDel.asna_user_id).FirstOrDefault();
            if (asna_user == null)
            {
                modelState.AddModelError("", "Не найдены параметры пользователя с кодом " + itemDel.asna_user_id.ToString());
                return false;
            }

            dbContext.asna_user_log.RemoveRange(dbContext.asna_user_log.Where(ss => ss.asna_user_id == asna_user.asna_user_id));
            dbContext.asna_schedule.RemoveRange(dbContext.asna_schedule.Where(ss => ss.asna_user_id == asna_user.asna_user_id));
            dbContext.asna_user.Remove(asna_user);
            dbContext.SaveChanges();

            return true;            
        }

        public bool InsertTask(int asna_user_id, int request_type_id, ModelStateDictionary modelState)
        {            
            IWaTaskService waTaskService = DependencyResolver.Current.GetService<IWaTaskService>();
            if (waTaskService == null)
            {
                modelState.AddModelError("", "Системная ошибка ! Обратитесь к разработчикам");
                return false;
            }

            vw_asna_user vw_asna_user = dbContext.vw_asna_user.Where(ss => ss.asna_user_id == asna_user_id).FirstOrDefault();
            if (vw_asna_user == null)
            {
                modelState.AddModelError("", "Не найден пользователь с кодом " + asna_user_id.ToString());
                return false;
            }

            wa_request_type wa_request_type = dbContext.wa_request_type.Where(ss => ss.request_type_id == request_type_id).FirstOrDefault();
            if (wa_request_type == null)
            {
                modelState.AddModelError("", "Не найден тип запроса с кодом " + request_type_id.ToString());
                return false;
            }

            Enums.WaTaskStateEnum taskState = wa_request_type.is_outer ? Enums.WaTaskStateEnum.ISSUED_CLIENT : Enums.WaTaskStateEnum.ISSUED_SERVER;
            AsnaServiceParam asnaServiceParam = new AsnaServiceParam() { asna_user_id = asna_user_id };
            WaTaskViewModel wa = new WaTaskViewModel()
            {
                request_type_id = request_type_id,
                task_state_id = (int)taskState,
                client_id = vw_asna_user.client_id,
                sales_id = vw_asna_user.sales_id,
                workplace_id = vw_asna_user.workplace_id,
                task_param = JsonConvert.SerializeObject(asnaServiceParam),
            };

            WaTaskViewModel res1 = (WaTaskViewModel)waTaskService.Insert(wa, modelState);
            if ((res1 == null) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при создании задания для WebAPI");
                return false;
            }

            string user = currUser.CabUserName;
            asna_user_log asna_user_log = null;
            asna_user_log = prepareLog("Создано задание " + res1.ord_client.ToString()+ ": " + res1.request_type_name, DateTime.Now, user);
            asna_user_log.asna_user_id = asna_user_id;
            dbContext.asna_user_log.Add(asna_user_log);
            dbContext.SaveChanges();

            return true;
        }

        public bool UpdateTask(int asna_user_id, int task_id, int request_type_id, ModelStateDictionary modelState)
        {
            IWaTaskService waTaskService = DependencyResolver.Current.GetService<IWaTaskService>();
            if (waTaskService == null)
            {
                modelState.AddModelError("", "Системная ошибка ! Обратитесь к разработчикам");
                return false;
            }

            vw_wa_task vw_wa_task = dbContext.vw_wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
            if (vw_wa_task == null)
            {
                modelState.AddModelError("", "Не найдено задание с кодом " + task_id.ToString());
                return false;
            }

            wa_request_type wa_request_type = dbContext.wa_request_type.Where(ss => ss.request_type_id == request_type_id).FirstOrDefault();
            if (wa_request_type == null)
            {
                modelState.AddModelError("", "Не найден тип запроса с кодом " + request_type_id.ToString());
                return false;
            }

            WaTaskViewModel wa = new WaTaskViewModel()
            {
                task_id = task_id,
                request_type_id = request_type_id,
            };

            var res1 = waTaskService.Update(wa, modelState);
            if ((res1 == null) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при изменении задания для WebAPI");
                return false;
            }

            string user = currUser.CabUserName;
            asna_user_log asna_user_log = null;
            asna_user_log = prepareLog("Изменено задание " + vw_wa_task.ord_client.ToString() + ": с " + vw_wa_task.request_type_name + " на " + wa_request_type.request_type_name, DateTime.Now, user);
            asna_user_log.asna_user_id = asna_user_id;
            dbContext.asna_user_log.Add(asna_user_log);
            dbContext.SaveChanges();

            return true;
        }

        public bool CancelTask(int asna_user_id, int task_id, ModelStateDictionary modelState)
        {
            IWaTaskService waTaskService = DependencyResolver.Current.GetService<IWaTaskService>();
            if (waTaskService == null)
            {
                modelState.AddModelError("", "Системная ошибка ! Обратитесь к разработчикам");
                return false;
            }

            wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
            if (wa_task == null)
            {
                modelState.AddModelError("", "Не найдено задание с кодом " + task_id.ToString());
                return false;
            }


            var res1 = waTaskService.UpdateTaskSate(task_id, (int)Enums.WaTaskStateEnum.CANCELED_SERVER, modelState);
            if ((!res1) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при отмене задания для WebAPI");
                return false;
            }

            string user = currUser.CabUserName;
            asna_user_log asna_user_log = null;
            asna_user_log = prepareLog("Отменено задание " + wa_task.ord_client.ToString(), DateTime.Now, user);
            asna_user_log.asna_user_id = asna_user_id;
            dbContext.asna_user_log.Add(asna_user_log);
            dbContext.SaveChanges();

            return true;
        }

        public bool DeleteTask(int asna_user_id, int task_id, ModelStateDictionary modelState)
        {
            IWaTaskService waTaskService = DependencyResolver.Current.GetService<IWaTaskService>();
            if (waTaskService == null)
            {
                modelState.AddModelError("", "Системная ошибка ! Обратитесь к разработчикам");
                return false;
            }

            vw_wa_task vw_wa_task = dbContext.vw_wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
            if (vw_wa_task == null)
            {
                modelState.AddModelError("", "Не найдено задание с кодом " + task_id.ToString());
                return false;
            }

            WaTaskViewModel wa = new WaTaskViewModel()
            {
                task_id = task_id,
                request_type_id = vw_wa_task.request_type_id,
            };

            var res1 = waTaskService.Delete(wa, modelState);
            if ((!res1) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при удалении задания для WebAPI");
                return false;
            }

            string user = currUser.CabUserName;
            asna_user_log asna_user_log = null;
            asna_user_log = prepareLog("Удалено задание " + vw_wa_task.ord_client.ToString() + ": " + vw_wa_task.request_type_name, DateTime.Now, user);
            asna_user_log.asna_user_id = asna_user_id;
            dbContext.asna_user_log.Add(asna_user_log);
            dbContext.SaveChanges();
            
            return true;
        }
    }
}