﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaUserLogService : IAuBaseService
    {
        IQueryable<AsnaUserLogViewModel> GetList_bySales(int? client_id, int? sales_id);
    }

    public class AsnaUserLogService : AuBaseService, IAsnaUserLogService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_asna_user_log
                .Where(ss => ss.asna_user_id == parent_id)
                .ProjectTo<AsnaUserLogViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_asna_user_log
                .ProjectTo<AsnaUserLogViewModel>();
        }

        public IQueryable<AsnaUserLogViewModel> GetList_bySales(int? client_id, int? sales_id)
        {
            return dbContext.vw_asna_user_log
                .Where(ss => ((ss.client_id == client_id && client_id.HasValue) || (!client_id.HasValue))
                    &&
                    ((ss.sales_id == sales_id && sales_id.HasValue) || (!sales_id.HasValue))
                    )
                .ProjectTo<AsnaUserLogViewModel>();
        }
    }
}