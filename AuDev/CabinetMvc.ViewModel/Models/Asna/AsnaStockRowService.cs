﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaStockRowService : IAuBaseService
    {
        //
    }

    public class AsnaStockRowService : AuBaseService, IAsnaStockRowService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_asna_stock_row
                .Where(ss => ss.chain_id == parent_id)
                .ProjectTo<AsnaStockRowViewModel>();
        }

    }
}