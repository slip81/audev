﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaOrderStateTypeViewModel : AuBaseViewModel
    {

        public AsnaOrderStateTypeViewModel()
            : base(Enums.MainObjectType.ASNA_ORDER_STATE_TYPE)
        {           
            //asna_order_state_type
        } 

        [Key()]
        [Display(Name = "Код")]
        public int state_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string state_type_name { get; set; }

        [Display(Name = "Формируется для заголовка")]
        public bool is_for_order { get; set; }

        [Display(Name = "Формируется для строки")]
        public bool is_for_row { get; set; }

        [Display(Name = "crt_date")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "crt_user")]
        public string crt_user { get; set; }

        [Display(Name = "upd_date")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "upd_user")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }
    }

}