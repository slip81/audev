﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaUserLogViewModel : AuBaseViewModel
    {

        public AsnaUserLogViewModel()
            : base(Enums.MainObjectType.ASNA_USER_LOG)
        {           
            //vw_asna_user_log
        } 

        [Key()]
        [Display(Name = "Код")]
        public int log_id { get; set; }

        [Display(Name = "Пользователь")]
        public int asna_user_id { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }

        [Display(Name = "Запрос")]
        public Nullable<long> request_id { get; set; }

        [Display(Name = "Цепочка")]
        public Nullable<int> chain_id { get; set; }

        [Display(Name = "Пакет (в)")]
        public Nullable<int> in_batch_id { get; set; }

        [Display(Name = "Пакет (из)")]
        public Nullable<int> out_batch_id { get; set; }

        [Display(Name = "Код")]
        public Nullable<int> info_id { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор")]
        public string crt_user { get; set; }

        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }

        [Display(Name = "Активен")]
        public bool is_active { get; set; }

        [Display(Name = "Рабочее место")]
        public string workplace_name { get; set; }

        [Display(Name = "Торговая точка")]
        public int sales_id { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }

        [Display(Name = "Организация")]
        public int client_id { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }

        [Display(Name = "Активен")]
        public string is_active_str { get; set; }
    }
    

}