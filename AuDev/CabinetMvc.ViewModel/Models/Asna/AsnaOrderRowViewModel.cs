﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaOrderRowViewModel : AuBaseViewModel
    {

        public AsnaOrderRowViewModel()
            : base(Enums.MainObjectType.ASNA_ORDER_ROW)
        {           
            //vw_asna_order_row
        } 

        [Key()]
        [Display(Name = "Код")]
        public int row_id { get; set; }

        [Display(Name = "Код заказа")]
        public int order_id { get; set; }

        [Display(Name = "Статус")]
        public Nullable<int> state_type_id { get; set; }

        [Display(Name = "Код")]
        public string asna_row_id { get; set; }

        [Display(Name = "Код заказа")]
        public string asna_order_id { get; set; }

        [Display(Name = "Тип строки")]
        public short asna_row_type { get; set; }

        [Display(Name = "Код партии")]
        public string asna_prt_id { get; set; }

        [Display(Name = "Уникальный код по АСНА")]
        public int asna_nnt { get; set; }

        [Display(Name = "Кол-во в заказе (всего)")]
        public decimal asna_qnt { get; set; }

        [Display(Name = "Цена товара")]
        public decimal asna_prc { get; set; }

        [Display(Name = "Цена товара по АСНА-Экономия")]
        public Nullable<decimal> asna_prc_dsc { get; set; }

        [Display(Name = "Признак дисконта")]
        public string asna_dsc_union { get; set; }

        [Display(Name = "Дотация для единицы товара")]
        public Nullable<decimal> asna_dtn { get; set; }

        [Display(Name = "Цена реализации со скидкой")]
        public Nullable<decimal> asna_prc_loyal { get; set; }

        [Display(Name = "Цена поставки")]
        public Nullable<decimal> asna_prc_opt_nds { get; set; }

        [Display(Name = "ИНН поставщика")]
        public string asna_supp_inn { get; set; }

        [Display(Name = "Дата поставки")]
        public Nullable<System.DateTime> asna_dlv_date { get; set; }

        [Display(Name = "Незарезервировано")]
        public Nullable<decimal> asna_qnt_unrsv { get; set; }

        [Display(Name = "Фиксированная цена")]
        public Nullable<decimal> asna_prc_fix { get; set; }

        [Display(Name = "Обновлено")]
        public Nullable<System.DateTime> asna_ts { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "Статус")]
        public string state_type_name { get; set; }
        
        [Display(Name = "Код товара")]
        public string asna_sku { get; set; }

        [Display(Name = "Наименование")]
        public string asna_name { get; set; }
    }

}