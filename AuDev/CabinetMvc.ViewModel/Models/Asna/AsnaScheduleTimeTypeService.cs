﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaScheduleTimeTypeService : IAuBaseService
    {
        //
    }

    public class AsnaScheduleTimeTypeService : AuBaseService, IAsnaScheduleTimeTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.asna_schedule_time_type
                .OrderBy(ss => ss.time_type_id)
                .ProjectTo<AsnaScheduleTimeTypeViewModel>();
        }
    }
}