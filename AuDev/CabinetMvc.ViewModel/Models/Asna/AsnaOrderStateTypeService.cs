﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaOrderStateTypeService : IAuBaseService
    {
        //
    }

    public class AsnaOrderStateTypeService : AuBaseService, IAsnaOrderStateTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.asna_order_state_type
                .ProjectTo<AsnaOrderStateTypeViewModel>();
        }
    }
}