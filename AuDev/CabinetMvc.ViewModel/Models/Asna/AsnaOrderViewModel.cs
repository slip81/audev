﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaOrderViewModel : AuBaseViewModel
    {

        public AsnaOrderViewModel()
            : base(Enums.MainObjectType.ASNA_ORDER)
        {           
            //vw_asna_order
        } 

        [Key()]
        [Display(Name = "Код")]
        public int order_id { get; set; }

        [Display(Name = "Пользователь")]
        public int asna_user_id { get; set; }

        [Display(Name = "Пользователь (для выдачи)")]
        public int issuer_asna_user_id { get; set; }

        [Display(Name = "Статус заказа")]
        public Nullable<int> state_type_id { get; set; }

        [Display(Name = "Код заказа")]
        public string asna_order_id { get; set; }

        [Display(Name = "Источник заказа")]
        public string asna_src { get; set; }

        [Display(Name = "Номер заказа")]
        public string asna_num { get; set; }

        [Display(Name = "Дата заказа")]
        public Nullable<System.DateTime> asna_date { get; set; }

        [Display(Name = "Имя покупателя")]
        public string asna_name { get; set; }

        [Display(Name = "Номер телефона")]
        public string asna_m_phone { get; set; }

        [Display(Name = "Тип оплаты")]
        public string asna_pay_type { get; set; }

        [Display(Name = "Тип оплаты")]
        public string asna_pay_type_id { get; set; }

        [Display(Name = "Дисконтная карта")]
        public string asna_d_card { get; set; }

        [Display(Name = "Признак АСНА-Экономия")]
        public short asna_ae { get; set; }

        [Display(Name = "Код совместной покупки")]
        public string asna_union_id { get; set; }

        [Display(Name = "Обновлено")]
        public Nullable<System.DateTime> asna_ts { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "РМ")]
        public int workplace_id { get; set; }

        [Display(Name = "РМ")]
        public string workplace_name { get; set; }

        [Display(Name = "ТТ")]
        public int sales_id { get; set; }

        [Display(Name = "ТТ")]
        public string sales_name { get; set; }

        [Display(Name = "Клиент")]
        public int client_id { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "РМ (для выдачи)")]
        public int issuer_workplace_id { get; set; }

        [Display(Name = "РМ (для выдачи)")]
        public string issuer_workplace_name { get; set; }

        [Display(Name = "ТТ (для выдачи)")]
        public int issuer_sales_id { get; set; }

        [Display(Name = "ТТ (для выдачи)")]
        public string issuer_sales_name { get; set; }

        [Display(Name = "Клиент (для выдачи)")]
        public int issuer_client_id { get; set; }

        [Display(Name = "Клиент (для выдачи)")]
        public string issuer_client_name { get; set; }

        [Display(Name = "Статус заказа")]
        public string state_type_name { get; set; }
    }

}