﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaScheduleIntervalTypeViewModel : AuBaseViewModel
    {

        public AsnaScheduleIntervalTypeViewModel()
            : base(Enums.MainObjectType.ASNA_SCHEDULE_INTERVAL_TYPE)
        {           
            //asna_schedule_interval_type
        } 

        [Key()]
        [Display(Name = "Код")]
        public int interval_type_id { get; set; }

        [Display(Name = "Название")]
        public string interval_type_name { get; set; }

        [Display(Name = "Значение")]
        public Nullable<int> interval_type_value { get; set; }
    }

}