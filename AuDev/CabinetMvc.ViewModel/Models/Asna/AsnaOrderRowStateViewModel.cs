﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaOrderRowStateViewModel : AuBaseViewModel
    {

        public AsnaOrderRowStateViewModel()
            : base(Enums.MainObjectType.ASNA_ORDER_ROW_STATE)
        {           
            //vw_asna_order_row_state
        } 

        [Key()]
        [Display(Name = "Код")]
        public int row_state_id { get; set; }

        [Display(Name = "Код строки")]
        public int row_id { get; set; }

        [Display(Name = "Статус")]
        public int state_type_id { get; set; }

        [Display(Name = "Код статуса")]
        public string asna_status_id { get; set; }

        [Display(Name = "Код заказа")]
        public string asna_order_id { get; set; }

        [Display(Name = "Код строки заказа")]
        public string asna_row_id { get; set; }

        [Display(Name = "Дата статуса")]
        public Nullable<System.DateTime> asna_date { get; set; }

        [Display(Name = "Дата сброса резерва")]
        public Nullable<System.DateTime> asna_rc_date { get; set; }

        [Display(Name = "Комментарий")]
        public string asna_cmnt { get; set; }

        [Display(Name = "Обновлено")]
        public Nullable<System.DateTime> asna_ts { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "Статус")]
        public string state_type_name { get; set; }
    }

}