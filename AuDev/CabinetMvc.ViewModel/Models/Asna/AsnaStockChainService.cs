﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaStockChainService : IAuBaseService
    {
        IQueryable<AsnaStockChainViewModel> GetList_byDate(int sales_id, DateTime chain_date);
        DateTime GetLastChainDate(int sales_id);
        List<DateTime> GetChainDates(int sales_id);
    }

    public class AsnaStockChainService : AuBaseService, IAsnaStockChainService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_asna_stock_chain
                .Where(ss => ss.sales_id == parent_id)
                .ProjectTo<AsnaStockChainViewModel>();
        }

        public IQueryable<AsnaStockChainViewModel> GetList_byDate(int sales_id, DateTime chain_date)
        {
            DateTime chain_date_beg = chain_date.Date;
            DateTime chain_date_end = chain_date_beg.AddDays(1);
            return dbContext.vw_asna_stock_chain
                .Where(ss => ss.sales_id == sales_id && ss.crt_date >= chain_date_beg && ss.crt_date < chain_date_end)
                .OrderByDescending(ss => ss.chain_id)
                .ProjectTo<AsnaStockChainViewModel>();
        }

        public DateTime GetLastChainDate(int sales_id)
        {
            DateTime res = DateTime.Today;

            vw_asna_stock_chain vw_asna_stock_chain = dbContext.vw_asna_stock_chain.Where(ss => ss.sales_id == sales_id && !ss.is_deleted && !ss.is_active).OrderByDescending(ss => ss.chain_id).FirstOrDefault();
            res = vw_asna_stock_chain != null ? ((DateTime)vw_asna_stock_chain.crt_date).Date : DateTime.Today;

            return res;
        }

        public List<DateTime> GetChainDates(int sales_id)
        {
            List<DateTime> result = new List<DateTime>();
            List<DateTime?> dates = dbContext.vw_asna_stock_chain.Where(ss => ss.sales_id == sales_id && !ss.is_deleted && !ss.is_active)
                .Select(ss => ss.crt_date)
                .Distinct()
                .ToList();
            if ((dates != null) || (dates.Count <= 0))
                result = dates.Where(ss => ss.HasValue).Select(ss => ((DateTime)ss).Date).ToList();
            
            //result.Add(DateTime.Today);
            //result = result.Distinct().ToList();
          
            return result;
        }

    }
}