﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaLinkService : IAuBaseService
    {
        IQueryable<AsnaLinkViewModel> GetList_bySales(int? sales_id);
    }

    public class AsnaLinkService : AuBaseService, IAsnaLinkService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return (from t1 in dbContext.asna_link
                    from t2 in dbContext.asna_link_row                    
                    where t1.link_id == t2.link_id
                    && t1.asna_user_id == parent_id                    
                    select t2)
                   .ProjectTo<AsnaLinkViewModel>();
        }

        public IQueryable<AsnaLinkViewModel> GetList_bySales(int? sales_id)
        {
            return (from t1 in dbContext.asna_link
                   from t2 in dbContext.asna_link_row
                   from t3 in dbContext.vw_asna_user
                   where t1.link_id == t2.link_id
                   && t1.asna_user_id == t3.asna_user_id
                   && t3.sales_id == sales_id
                   select t2)
                   .ProjectTo<AsnaLinkViewModel>();
        }
    }
}