﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Asna
{
    public class AsnaUserViewModel : AuBaseViewModel
    {

        public AsnaUserViewModel()
            : base(Enums.MainObjectType.ASNA)
        {           
            //vw_asna_user
        } 

        [Key()]
        [Display(Name = "Код")]
        public int asna_user_id { get; set; }

        [Display(Name = "Рабочее место")]
        public int workplace_id { get; set; }

        [Display(Name = "Активен")]
        public bool is_active { get; set; }

        [Display(Name = "Код аптеки по АСНА")]
        [Required(ErrorMessage="Не задан код аптеки по АСНА")]
        public string asna_store_id { get; set; }

        [Display(Name = "Пароль аптеки")]
        [Required(ErrorMessage = "Не задан пароль аптеки")]
        public string asna_pwd { get; set; }

        [Display(Name = "Код справочника связок")]
        [Required(ErrorMessage = "Не задан код справочника связок")]
        public Nullable<int> asna_rr_id { get; set; }

        [Display(Name = "Код ЮЛ по АСНА")]        
        public string asna_legal_id { get; set; }
        
        [JsonIgnore()]
        public string auth { get; set; }
        
        [JsonIgnore()]
        public Nullable<System.DateTime> auth_date_beg { get; set; }

        [JsonIgnore()]
        public Nullable<System.DateTime> auth_date_end { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "Рабочее место")]
        public string workplace_name { get; set; }

        [Display(Name = "Торговая точка")]
        public int sales_id { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_name { get; set; }

        [Display(Name = "Организация")]
        public int client_id { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }

        [Display(Name = "Активен")]
        public string is_active_str { get; set; }

        [Display(Name = "Логин")]
        public Nullable<int> user_id { get; set; }

        [Display(Name = "Логин")]
        public string user_name { get; set; }

        public Nullable<int> sch_full_schedule_id { get; set; }
        public Nullable<bool> sch_full_is_active { get; set; }
        public Nullable<int> sch_full_interval_type_id { get; set; }
        public Nullable<int> sch_full_start_time_type_id { get; set; }
        public Nullable<int> sch_full_end_time_type_id { get; set; }
        public Nullable<bool> sch_full_is_day1 { get; set; }
        public Nullable<bool> sch_full_is_day2 { get; set; }
        public Nullable<bool> sch_full_is_day3 { get; set; }
        public Nullable<bool> sch_full_is_day4 { get; set; }
        public Nullable<bool> sch_full_is_day5 { get; set; }
        public Nullable<bool> sch_full_is_day6 { get; set; }
        public Nullable<bool> sch_full_is_day7 { get; set; }
        public Nullable<int> sch_ch_schedule_id { get; set; }
        public Nullable<bool> sch_ch_is_active { get; set; }
        public Nullable<int> sch_ch_interval_type_id { get; set; }
        public Nullable<int> sch_ch_start_time_type_id { get; set; }
        public Nullable<int> sch_ch_end_time_type_id { get; set; }
        public Nullable<bool> sch_ch_is_day1 { get; set; }
        public Nullable<bool> sch_ch_is_day2 { get; set; }
        public Nullable<bool> sch_ch_is_day3 { get; set; }
        public Nullable<bool> sch_ch_is_day4 { get; set; }
        public Nullable<bool> sch_ch_is_day5 { get; set; }
        public Nullable<bool> sch_ch_is_day6 { get; set; }
        public Nullable<bool> sch_ch_is_day7 { get; set; }
        public Nullable<int> sch_full_start_time_type_value { get; set; }
        public Nullable<int> sch_full_end_time_type_value { get; set; }
        public Nullable<int> sch_full_interval_type_value { get; set; }
        public Nullable<int> sch_ch_start_time_type_value { get; set; }
        public Nullable<int> sch_ch_end_time_type_value { get; set; }
        public Nullable<int> sch_ch_interval_type_value { get; set; }
    }

    public class AsnaServiceParam
    {
        public AsnaServiceParam()
        {
            //
        }

        public int asna_user_id { get; set; }
    }   

}