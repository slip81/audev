﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using CabinetMvc.ViewModel.Wa;

namespace CabinetMvc.ViewModel.Asna
{
    public interface IAsnaOrderStateService : IAuBaseService
    {
        //
    }

    public class AsnaOrderStateService : AuBaseService, IAsnaOrderStateService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_asna_order_state
                .Where(ss => ss.order_id == parent_id)
                .ProjectTo<AsnaOrderStateViewModel>();
        }
    }
}