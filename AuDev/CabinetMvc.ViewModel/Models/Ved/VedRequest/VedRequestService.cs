﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Ved
{
    public interface IVedRequestService : IAuBaseService
    {
        //
    }

    public class VedRequestService : AuBaseService, IVedRequestService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.request
                .Where(ss => ss.scope == (int)Enums.LogScope.VED)
                .Select(ss => new VedRequestViewModel()
                {
                    request_id = ss.request_id,
                    login = ss.login,
                    workplace = ss.workplace,
                    version_num = ss.version_num,
                    request_date = ss.request_date,
                    state = ss.state,
                    state_str = ss.state == (int)Enums.RequestStateEnum.CONFIRMED ? "Подтверждено" : "Не подтверждено",
                    record_cnt = ss.record_cnt,
                    request_type = ss.request_type,
                    request_type_str = ss.request_type == (int)Enums.RequestTypeEnum.RECORDS_ALL ? "Все записи" : "Новые записи",
                    commit_date = ss.commit_date,
                    scope = ss.scope,
                    scope2 = ss.scope2,
                }
            );
        }

    }
}