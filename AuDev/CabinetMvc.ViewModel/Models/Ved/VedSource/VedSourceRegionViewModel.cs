﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Ved
{
    public class VedSourceRegionViewModel : AuBaseViewModel
    {
        public VedSourceRegionViewModel()
            : base(Enums.MainObjectType.VED_SOURCE_REGION)
        {
            AddMode = true;
        }

        public VedSourceRegionViewModel(ved_source_region item)
            : base(Enums.MainObjectType.VED_SOURCE_REGION)
        {
            item_id = item.item_id;
            region_id = item.region_id;
            url_site = item.url_site;
            url_file_main = item.url_file_main;
            url_file_res = item.url_file_res;
            descr = item.descr;            
            arc_type = item.arc_type;
            url_file_a_content = item.url_file_a_content;
            template_id = item.template_id;
            out_template_id = item.out_template_id;
            AddMode = false;
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Key()]
        [Display(Name = "Регион")]
        [Required(ErrorMessage = "Не указан регион")]
        public int region_id { get; set; }

        [Display(Name = "Регион")]
        public string region_name { get; set; }

        [Display(Name = "URL сайта где искать ссылку на реестр ЖВ")]
        [UIHint("Text")]
        public string url_site { get; set; }

        [Display(Name = "URL ссылки на файл реестра ЖВ")]
        [Required(ErrorMessage = "Не указан URL файла")]
        [UIHint("Text")]
        public string url_file_main { get; set; }

        [Display(Name = "Резервный URL ссылки на файл реестра ЖВ")]
        [UIHint("Text")]
        public string url_file_res { get; set; }

        [Display(Name = "Описание")]
        [UIHint("Text")]
        public string descr { get; set; }

        [Display(Name = "Тип сжатия файла")]
        [UIHint("ArcTypeCombo")]
        public int arc_type { get; set; }

        [Display(Name = "Тип сжатия файла")]
        [UIHint("Text")]
        public string arc_type_name { get { return arc_type.EnumName<Enums.ArcType>(); } }

        [Display(Name = "Порядковый номер")]
        [UIHint("IntegerFromOne")]
        public int num { get; set; }

        [Display(Name = "Текст в теге <a>")]
        [UIHint("Text")]
        public string url_file_a_content { get; set; }

        [Display(Name = "Шаблон для импорта основных строк")]
        public Nullable<long> template_id { get; set; }

        [Display(Name = "Шаблон для импорта исключенных строк")]
        public Nullable<long> out_template_id { get; set; }

        [Display(Name = "AddMode")]
        [JsonIgnore()]
        public bool AddMode { get; set; }
    }
}