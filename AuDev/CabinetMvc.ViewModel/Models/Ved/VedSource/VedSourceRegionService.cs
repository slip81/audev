﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Ved
{
    public interface IVedSourceRegionService : IAuBaseService
    {
        //
    }

    public class VedSourceRegionService : AuBaseService, IVedSourceRegionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_ved_source_region.Select(ss => new VedSourceRegionViewModel()
            {                
                url_site = ss.url_site,
                url_file_main = ss.url_file_main,
                url_file_res = ss.url_file_res,
                descr = ss.descr,                
                item_id = ss.item_id,
                region_id = (int)ss.region_id,
                arc_type = (int)ss.arc_type,
                region_name = ss.region_name,
                url_file_a_content = ss.url_file_a_content,
                template_id = ss.template_id,
                out_template_id = ss.out_template_id,
                AddMode = false,
            }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.ved_source_region.Where(ss => ss.item_id == id)
                .Select(ss => new VedSourceRegionViewModel()
                {
                    url_site = ss.url_site,
                    url_file_main = ss.url_file_main,
                    url_file_res = ss.url_file_res,
                    descr = ss.descr,
                    item_id = ss.item_id,
                    region_id = ss.region_id,
                    arc_type = ss.arc_type,
                    url_file_a_content = ss.url_file_a_content,
                    template_id = ss.template_id,
                    out_template_id = ss.out_template_id,
                    AddMode = false,
                }
                )
                .FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VedSourceRegionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            VedSourceRegionViewModel itemAdd = (VedSourceRegionViewModel)item;
            if (
                (itemAdd == null)
                ||
                (String.IsNullOrEmpty(itemAdd.url_site))
                ||
                (String.IsNullOrEmpty(itemAdd.url_file_main))
                ||
                (itemAdd.region_id <= 0)
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты источника");
                return null;
            }

            var existing = dbContext.ved_source_region.Where(ss => ss.region_id == itemAdd.region_id).FirstOrDefault();
            if (existing != null)
            {
                modelState.AddModelError("", "Уже есть источник для данного региона");
                return null;
            }

            ved_source_region ved_source_region = new ved_source_region();
            ved_source_region.url_site = itemAdd.url_site;
            ved_source_region.url_file_main = itemAdd.url_file_main;
            ved_source_region.url_file_res = itemAdd.url_file_res;
            ved_source_region.arc_type = itemAdd.arc_type;
            ved_source_region.descr = itemAdd.descr;
            ved_source_region.region_id = itemAdd.region_id;
            ved_source_region.url_file_a_content = itemAdd.url_file_a_content;
            ved_source_region.template_id = itemAdd.template_id;
            ved_source_region.out_template_id = itemAdd.out_template_id;

            dbContext.ved_source_region.Add(ved_source_region);
            dbContext.SaveChanges();

            return new VedSourceRegionViewModel(ved_source_region);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VedSourceRegionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            VedSourceRegionViewModel itemEdit = (VedSourceRegionViewModel)item;
            if (
                (itemEdit == null)
                ||
                (String.IsNullOrEmpty(itemEdit.url_site))
                ||
                (String.IsNullOrEmpty(itemEdit.url_file_main))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты источника");
                return null;
            }

            ved_source_region ved_source_region = dbContext.ved_source_region.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (ved_source_region == null)
            {
                modelState.AddModelError("", "Не найден источник с кодом " + itemEdit.item_id.ToString());
                return null;
            }

            var existing = dbContext.ved_source_region.Where(ss => ss.item_id != itemEdit.item_id && ss.region_id == itemEdit.region_id).FirstOrDefault();
            if (existing != null)
            {
                modelState.AddModelError("", "Уже есть источник для данного региона");
                return null;
            }

            modelBeforeChanges = new VedSourceRegionViewModel(ved_source_region);

            ved_source_region.url_site = itemEdit.url_site;
            ved_source_region.url_file_main = itemEdit.url_file_main;
            ved_source_region.url_file_res = itemEdit.url_file_res;
            ved_source_region.arc_type = itemEdit.arc_type;
            ved_source_region.descr = itemEdit.descr;
            ved_source_region.region_id = itemEdit.region_id;
            ved_source_region.url_file_a_content = itemEdit.url_file_a_content;
            ved_source_region.template_id = itemEdit.template_id;
            ved_source_region.out_template_id = itemEdit.out_template_id;

            dbContext.SaveChanges();

            return new VedSourceRegionViewModel(ved_source_region);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VedSourceRegionViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((VedSourceRegionViewModel)item).item_id;

            ved_source_region ved_source_region = null;

            ved_source_region = dbContext.ved_source_region.Where(ss => ss.item_id == id).FirstOrDefault();
            if (ved_source_region == null)
            {
                modelState.AddModelError("serv_result", "Исчтоник не найден");
                return false;
            }

            dbContext.ved_source_region.Remove(ved_source_region);
            dbContext.SaveChanges();

            return true;
        }

    }
}