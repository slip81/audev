﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Ved
{
    public interface IVedSourceService : IAuBaseService
    {
        AuBaseViewModel GetItem_Single();
    }

    public class VedSourceService :  AuBaseService, IVedSourceService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.ved_source.Select(ss => new VedSourceViewModel()
            {
                ved_source_id = ss.ved_source_id,
                url_site = ss.url_site,
                url_file_main = ss.url_file_main,
                url_file_res = ss.url_file_res,
                descr = ss.descr,                
                num = ss.num,
                arc_type = ss.arc_type,
                url_file_a_content = ss.url_file_a_content,
                template_id = ss.template_id,
                out_template_id = ss.out_template_id,
            }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.ved_source.Select(ss => new VedSourceViewModel()
            {
                ved_source_id = ss.ved_source_id,
                url_site = ss.url_site,
                url_file_main = ss.url_file_main,
                url_file_res = ss.url_file_res,
                descr = ss.descr,
                num = ss.num,
                arc_type = ss.arc_type,
                url_file_a_content = ss.url_file_a_content,
                template_id = ss.template_id,
                out_template_id = ss.out_template_id,
            }
            )
            .FirstOrDefault();
        }

        public AuBaseViewModel GetItem_Single()
        {
            return dbContext.ved_source.Select(ss => new VedSourceViewModel()
            {
                ved_source_id = ss.ved_source_id,
                url_site = ss.url_site,
                url_file_main = ss.url_file_main,
                url_file_res = ss.url_file_res,
                descr = ss.descr,
                num = ss.num,
                arc_type = ss.arc_type,
                url_file_a_content = ss.url_file_a_content,
                template_id = ss.template_id,
                out_template_id = ss.out_template_id,
            }
            )
            .SingleOrDefault();
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VedSourceViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            VedSourceViewModel itemEdit = (VedSourceViewModel)item;
            if (
                (itemEdit == null)
                ||
                (String.IsNullOrEmpty(itemEdit.url_site))
                ||
                (String.IsNullOrEmpty(itemEdit.url_file_main))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты источника");
                return null;
            }

            ved_source ved_source = dbContext.ved_source.Where(ss => ss.ved_source_id == itemEdit.ved_source_id).FirstOrDefault();
            if (ved_source == null)
            {
                modelState.AddModelError("", "Не найден источник с кодом " + itemEdit.ved_source_id.ToString());
                return null;
            }

            modelBeforeChanges = new VedSourceViewModel(ved_source);

            ved_source.url_site = itemEdit.url_site;
            ved_source.url_file_main = itemEdit.url_file_main;
            ved_source.url_file_res = itemEdit.url_file_res;
            ved_source.arc_type = itemEdit.arc_type;
            ved_source.descr = itemEdit.descr;
            ved_source.num = itemEdit.num;
            ved_source.url_file_a_content = itemEdit.url_file_a_content;
            ved_source.template_id = itemEdit.template_id;
            ved_source.out_template_id = itemEdit.out_template_id;

            dbContext.SaveChanges();

            return new VedSourceViewModel(ved_source);
        }

    }
}