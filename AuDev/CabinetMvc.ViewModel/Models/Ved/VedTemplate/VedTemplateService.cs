﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Ved
{
    public interface IVedTemplateService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_Sprav();
    }

    public class VedTemplateService : AuBaseService, IVedTemplateService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.ved_template.Select(ss => new VedTemplateViewModel()
            {
                template_id = ss.template_id,
                template_name = ss.template_name,
                start_row_num = ss.start_row_num,                
                list_name = ss.list_name,
            }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.ved_template
                .Where(ss => ss.template_id == id)
                .Select(ss => new VedTemplateViewModel()
            {
                template_id = ss.template_id,
                template_name = ss.template_name,
                start_row_num = ss.start_row_num,                
                list_name = ss.list_name,
            }            
            )
            .FirstOrDefault();
        }

        public IQueryable<AuBaseViewModel> GetList_Sprav()
        {
            return dbContext.ved_template.Select(ss => new VedTemplateViewModel()
            {
                template_id = ss.template_id,
                template_name = ss.template_name,
            }
            );
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VedTemplateViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            VedTemplateViewModel itemAdd = (VedTemplateViewModel)item;
            if ((itemAdd == null) || (String.IsNullOrEmpty(itemAdd.template_name)))
            {
                modelState.AddModelError("", "Не заданы атрибуты шаблона");
                return null;
            }

            if ((itemAdd.template_columns == null) || (itemAdd.template_columns.Count() <= 0))
            {
                modelState.AddModelError("", "Не заданы колонки шаблона");
                return null;
            }

            var existing = dbContext.ved_template.Where(ss => ss.template_name == itemAdd.template_name).FirstOrDefault();
            if (existing != null)
            {
                modelState.AddModelError("", "Уже есть шаблон с таким названием");
                return null;
            }

            ved_template ved_template = new ved_template();
            ved_template.list_name = itemAdd.list_name;
            ved_template.start_row_num = itemAdd.start_row_num;
            ved_template.template_name = itemAdd.template_name;
            dbContext.ved_template.Add(ved_template);
         
            foreach (var col in itemAdd.template_columns)
            {
                ved_template_column ved_template_column = new ved_template_column();
                ved_template_column.ved_template = ved_template;
                ved_template_column.column_id = col.column_id;
                ved_template_column.column_num = col.is_active ? col.column_num : 0;
                ved_template_column.is_active = col.is_active;
                ved_template_column.is_for_match = col.is_active ? col.is_for_match : false;
                dbContext.ved_template_column.Add(ved_template_column);
            }
            
            dbContext.SaveChanges();

            return new VedTemplateViewModel(ved_template);            
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VedTemplateViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            VedTemplateViewModel itemEdit = (VedTemplateViewModel)item;
            if ((itemEdit == null) || (String.IsNullOrEmpty(itemEdit.template_name)))
            {
                modelState.AddModelError("", "Не заданы атрибуты шаблона");
                return null;
            }

            if ((itemEdit.template_columns == null) || (itemEdit.template_columns.Count() <= 0))
            {
                modelState.AddModelError("", "Не заданы колонки шаблона");
                return null;
            }

            ved_template ved_template = dbContext.ved_template.Where(ss => ss.template_id == itemEdit.template_id).FirstOrDefault();
            if (ved_template == null)
            {
                modelState.AddModelError("", "Не найден шаблон с кодом " + itemEdit.template_id.ToString());
                return null;
            }

            modelBeforeChanges = new VedTemplateViewModel(ved_template);

            ved_template.list_name = itemEdit.list_name;
            ved_template.start_row_num = itemEdit.start_row_num;
            ved_template.template_name = itemEdit.template_name;

            List<ved_template_column> template_columns = dbContext.ved_template_column.Where(ss => ss.template_id == itemEdit.template_id).ToList();
            if ((template_columns == null) || (template_columns.Count <= 0))
            {
                modelState.AddModelError("", "Не найдены колонки шаблона");
                return null;
            }
            
            foreach (var col_db in template_columns)
            {
                var col_new = itemEdit.template_columns.Where(ss => ss.column_id == col_db.column_id).FirstOrDefault();
                if (col_new == null)
                    continue;
                col_db.column_num = col_new.is_active ? col_new.column_num : 0;
                col_db.is_active = col_new.is_active;
                col_db.is_for_match = col_new.is_active ? col_new.is_for_match : false;
            }

            dbContext.SaveChanges();

            return new VedTemplateViewModel(ved_template);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is VedTemplateViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((VedTemplateViewModel)item).template_id;

            ved_template ved_template = dbContext.ved_template.Where(ss => ss.template_id == id).FirstOrDefault();
            if (ved_template == null)
            {
                modelState.AddModelError("serv_result", "Шаблон не найден");
                return false;
            }

            var existing1 = dbContext.ved_source.Where(ss => ((ss.template_id == id) || (ss.out_template_id == id))).FirstOrDefault();
            if (existing1 != null)
            {
                modelState.AddModelError("serv_result", "Шаблон привязан к источнику госреестра");
                return false;
            }

            var existing2 = dbContext.vw_ved_source_region.Where(ss => ((ss.template_id == id) || (ss.out_template_id == id))).FirstOrDefault();
            if (existing2 != null)
            {
                modelState.AddModelError("serv_result", "Шаблон привязан к источнику регионального реестра " + existing2.region_name);
                return false;
            }

            dbContext.ved_template_column.RemoveRange(dbContext.ved_template_column.Where(ss => ss.template_id == id));
            dbContext.ved_template.Remove(ved_template);
            dbContext.SaveChanges();

            return true;
        }
             
    }
}