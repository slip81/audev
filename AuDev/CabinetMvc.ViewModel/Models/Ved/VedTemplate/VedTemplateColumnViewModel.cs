﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Ved
{
    public class VedTemplateColumnViewModel : AuBaseViewModel
    {
        public VedTemplateColumnViewModel()
            : base(Enums.MainObjectType.VED_TEMPLATE_COLUMN)
        {
            //       
        }

        public VedTemplateColumnViewModel(ved_template_column item)
            : base(Enums.MainObjectType.VED_TEMPLATE_COLUMN)
        {
            item_id = item.item_id;
            template_id = item.template_id;
            column_id = item.column_id;
            column_num = item.column_num;
            is_active = item.is_active;
            is_for_match = item.is_for_match;
            template_name = "";
            is_active_str = item.is_active ? "Да" : "Нет";
            is_for_match_str = item.is_for_match ? "Да" : "Нет";
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }
        [Display(Name = "Шаблон")]
        public long template_id { get; set; }
        [Display(Name = "Шаблон")]
        public string template_name { get; set; }
        [Display(Name = "Колонка")]
        [UIHint("IntegerFromOne")]
        public int column_id { get; set; }
        [Display(Name = "Колонка")]
        public string column_id_str { get { return column_id.EnumName<Enums.VedColumn>(); } }
        [Display(Name = "Номер колонки")]
        [UIHint("Integer")]
        [Range(0, int.MaxValue, ErrorMessage = "Значение вне допустимого диапазона")]
        public int column_num { get; set; }
        [Display(Name = "Есть в файле")]
        public bool is_active { get; set; }
        [Display(Name = "Есть в файле")]
        public string is_active_str { get; set; }
        [Display(Name = "Участвует в сопоставлении")]
        public bool is_for_match { get; set; }
        [Display(Name = "Участвует в сопоставлении")]
        public string is_for_match_str { get; set; }
    }
}