﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Ved
{
    public class VedTemplateViewModel : AuBaseViewModel
    {
        public VedTemplateViewModel()
            : base(Enums.MainObjectType.VED_TEMPLATE)
        {
            start_row_num = 1;
            AddMode = true;
        }

        public VedTemplateViewModel(ved_template item)
            : base(Enums.MainObjectType.VED_TEMPLATE)
        {
            template_id = item.template_id;
            template_name = item.template_name;
            start_row_num = item.start_row_num;            
            list_name = item.list_name;
            AddMode = false;
        }
        [Key()]
        [Display(Name = "Код")]
        public long template_id { get; set; }

        [Display(Name = "Название")]
        [UIHint("Text")]
        [Required(ErrorMessage = "Не задано наименование")]
        public string template_name { get; set; }

        [Display(Name = "Номер первой строки с данными (мин = 1)")]
        [Range(1, int.MaxValue, ErrorMessage = "Значение вне допустимого диапазона")]
        [UIHint("IntegerFromOne")]
        public int start_row_num { get; set; }

        [Display(Name = "Название листа с данными в Excel-файле")]
        [UIHint("Text")]
        public string list_name { get; set; }

        [Display(Name = "AddMode")]
        public bool AddMode { get; set; }

        [JsonIgnore()]
        public IEnumerable<VedTemplateColumnViewModel> template_columns { get; set; }
    }
}