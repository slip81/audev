﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Ved
{
    public interface IVedTemplateColumnService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_forNewTemplate();
    }

    public class VedTemplateColumnService : AuBaseService, IVedTemplateColumnService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.ved_template_column
                .Where(ss => ss.template_id == parent_id)
                .Select(ss => new VedTemplateColumnViewModel()
            {
                item_id = ss.item_id,
                template_id = ss.template_id,
                column_id = ss.column_id,
                column_num = ss.column_num,
                is_active = ss.is_active,
                is_for_match = ss.is_for_match,
                template_name = ss.ved_template.template_name,
                is_active_str = ss.is_active ? "Да" : "Нет",
                is_for_match_str = ss.is_for_match ? "Да" : "Нет",
            }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.ved_template_column
                .Select(ss => new VedTemplateColumnViewModel()
            {
                item_id = ss.item_id,
                template_id = ss.template_id,
                column_id = ss.column_id,
                column_num = ss.column_num,
                is_active = ss.is_active,
                is_for_match = ss.is_for_match,
                template_name = ss.ved_template.template_name,
                is_active_str = ss.is_active ? "Да" : "Нет",
                is_for_match_str = ss.is_for_match ? "Да" : "Нет",
            }            
            )
            .FirstOrDefault();
        }

        public IQueryable<AuBaseViewModel> GetList_forNewTemplate()
        {
            return dbContext.ved_column
                .Select(ss => new VedTemplateColumnViewModel()
            {
                item_id = ss.column_id,
                template_id = 0,
                column_id = ss.column_id,
                column_num = ss.column_id,
                is_active = true,
                is_for_match = false,
                template_name = "",
                is_active_str = "Да",
                is_for_match_str = "Да",
            }
            );
        }

    }
}