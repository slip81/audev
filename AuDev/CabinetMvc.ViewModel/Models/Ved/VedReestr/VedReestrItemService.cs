﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Ved
{
    public interface IVedReestrItemService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_Free(int region_id);
    }

    public class VedReestrItemService : AuBaseService, IVedReestrItemService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_ved_reestr_item.Select(ss => new VedReestrItemViewModel()
            {
                reestr_id = ss.reestr_id,
                reestr_name = ss.reestr_name,
                date_beg = ss.date_beg,
                reestr_crt_date = ss.reestr_crt_date,
                parent_id = ss.parent_id,
                item_id = ss.item_id,
                item_code = ss.item_code,
                mnn_id = ss.mnn_id,
                comm_name_id = ss.comm_name_id,
                producer_id = ss.producer_id,
                pack_name = ss.pack_name,
                pack_count = ss.pack_count,
                limit_price = ss.limit_price,
                price_for_init_pack = ss.price_for_init_pack,
                reg_num = ss.reg_num,
                price_reg_date = ss.price_reg_date,
                ean13 = ss.ean13,
                crt_date = ss.crt_date,
                mnn = ss.mnn,
                producer = ss.producer,
                comm_name = ss.comm_name,
                price_reg_ndoc = ss.price_reg_ndoc,
            }
            );
        }

        public IQueryable<AuBaseViewModel> GetList_Free(int region_id)
         {
             return dbContext.vw_ved_reestr_item
                 .Where(ss => (
                     (!dbContext.vw_ved_reestr_region_item.Where(tt => tt.region_id == region_id && tt.gos_reestr_item_id == ss.item_id).Any())
                     &&
                     (!dbContext.vw_ved_reestr_region_item_out.Where(tt => tt.region_id == region_id && tt.gos_reestr_item_id == ss.item_id).Any())
                     )
                 )
                 .Select(ss => new VedReestrItemViewModel()
             {
                 reestr_id = ss.reestr_id,
                 reestr_name = ss.reestr_name,
                 date_beg = ss.date_beg,
                 reestr_crt_date = ss.reestr_crt_date,
                 parent_id = ss.parent_id,
                 item_id = ss.item_id,
                 item_code = ss.item_code,
                 mnn_id = ss.mnn_id,
                 comm_name_id = ss.comm_name_id,
                 producer_id = ss.producer_id,
                 pack_name = ss.pack_name,
                 pack_count = ss.pack_count,
                 limit_price = ss.limit_price,
                 price_for_init_pack = ss.price_for_init_pack,
                 reg_num = ss.reg_num,
                 price_reg_date = ss.price_reg_date,
                 ean13 = ss.ean13,
                 crt_date = ss.crt_date,
                 mnn = ss.mnn,
                 producer = ss.producer,
                 comm_name = ss.comm_name,
                 price_reg_ndoc = ss.price_reg_ndoc,
             }
             );
         }

    }
}