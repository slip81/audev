﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Ved
{
    public class VedReestrItemOutViewModel : AuBaseViewModel
    {
        public VedReestrItemOutViewModel()
            : base(Enums.MainObjectType.VED_REESTR_ITEM_OUT)
        {
            //
        }

        public VedReestrItemOutViewModel(vw_ved_reestr_item_out item)
            : base(Enums.MainObjectType.VED_REESTR_ITEM_OUT)
        {
            reestr_id = item.reestr_id;
            reestr_name = item.reestr_name;
            date_beg = item.date_beg;
            reestr_crt_date = item.reestr_crt_date;
            parent_id = item.parent_id;
            item_id = item.item_id;
            item_code = item.item_code;
            mnn_id = item.mnn_id;
            comm_name_id = item.comm_name_id;
            producer_id = item.producer_id;
            pack_name = item.pack_name;
            pack_count = item.pack_count;
            limit_price = item.limit_price;
            price_for_init_pack = item.price_for_init_pack;
            reg_num = item.reg_num;
            price_reg_date = item.price_reg_date;
            ean13 = item.ean13;
            crt_date = item.crt_date;
            mnn = item.mnn;
            producer = item.producer;
            comm_name = item.comm_name;
            out_date = item.out_date;
            out_reazon = item.out_reazon;
            price_reg_ndoc = item.price_reg_ndoc;
        }
        
        [Display(Name = "Код реестра")]
        public long reestr_id { get; set; }
        [Display(Name = "Реестр")]
        public string reestr_name { get; set; }
        [Display(Name = "Дата реестра")]
        public Nullable<System.DateTime> date_beg { get; set; }
        [Display(Name = "Дата создания реестра")]
        public Nullable<System.DateTime> reestr_crt_date { get; set; }
        [Display(Name = "parent_id")]
        public Nullable<long> parent_id { get; set; }
        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }
        [Display(Name = "Код строки")]
        public long item_code { get; set; }
        [Display(Name = "МНН")]
        public Nullable<long> mnn_id { get; set; }
        [Display(Name = "Торговое наименование")]
        public Nullable<long> comm_name_id { get; set; }
        [Display(Name = "Изготовитель")]
        public Nullable<long> producer_id { get; set; }
        [Display(Name = "Упаковка")]
        public string pack_name { get; set; }
        [Display(Name = "Кол-во в упаковке")]
        public Nullable<int> pack_count { get; set; }
        [Display(Name = "Предельная цена")]
        public Nullable<decimal> limit_price { get; set; }
        [Display(Name = "Цена указана для первичной упаковки")]
        public bool price_for_init_pack { get; set; }
        [Display(Name = "№ РУ")]
        public string reg_num { get; set; }
        [Display(Name = "Дата решения")]
        public string price_reg_date { get; set; }
        [Display(Name = "Номер решения")]
        public string price_reg_ndoc { get; set; }
        [Display(Name = "EAN13")]
        public string ean13 { get; set; }
        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }
        [Display(Name = "МНН")]
        public string mnn { get; set; }
        [Display(Name = "Изготовитель")]
        public string producer { get; set; }
        [Display(Name = "Торговое наименование")]
        public string comm_name { get; set; }
        [Display(Name = "Дата исключения")]
        public Nullable<System.DateTime> out_date { get; set; }
        [Display(Name = "Причина исключения")]
        public string out_reazon { get; set; }

    }
}