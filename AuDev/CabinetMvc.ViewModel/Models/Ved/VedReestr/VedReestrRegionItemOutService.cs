﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Ved
{
    public interface IVedReestrRegionItemOutService : IAuBaseService
    {
        //
    }

    public class VedReestrRegionItemOutService : AuBaseService, IVedReestrRegionItemOutService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_ved_reestr_region_item_out.Select(ss => new VedReestrRegionItemOutViewModel()
            {
                reestr_id = ss.reestr_id,
                reestr_name = ss.reestr_name,
                date_beg = ss.date_beg,
                reestr_crt_date = ss.reestr_crt_date,
                parent_id = ss.parent_id,
                item_id = ss.item_id,
                item_code = ss.item_code,
                pack_name = ss.pack_name,
                pack_count = ss.pack_count,
                limit_price = ss.limit_price,
                price_for_init_pack = ss.price_for_init_pack,
                reg_num = ss.reg_num,
                price_reg_date = ss.price_reg_date,
                ean13 = ss.ean13,
                crt_date = ss.crt_date,
                out_date = ss.out_date,
                out_reazon = ss.out_reazon,
                mnn = ss.mnn,
                producer = ss.producer,
                comm_name = ss.comm_name,
                limit_opt_incr = ss.limit_opt_incr,
                limit_rozn_incr = ss.limit_rozn_incr,
                limit_rozn_price = ss.limit_rozn_price,
                limit_rozn_price_with_nds = ss.limit_rozn_price_with_nds,
                gos_reestr_item_id = ss.gos_reestr_item_id,
                region_id = ss.region_id,
                price_reg_ndoc = ss.price_reg_ndoc,
            }
            );
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_ved_reestr_region_item_out
                .Where(ss => ss.region_id == parent_id)
                .Select(ss => new VedReestrRegionItemOutViewModel()
            {
                reestr_id = ss.reestr_id,
                reestr_name = ss.reestr_name,
                date_beg = ss.date_beg,
                reestr_crt_date = ss.reestr_crt_date,
                parent_id = ss.parent_id,
                item_id = ss.item_id,
                item_code = ss.item_code,
                pack_name = ss.pack_name,
                pack_count = ss.pack_count,
                limit_price = ss.limit_price,
                price_for_init_pack = ss.price_for_init_pack,
                reg_num = ss.reg_num,
                price_reg_date = ss.price_reg_date,
                ean13 = ss.ean13,
                crt_date = ss.crt_date,
                out_date = ss.out_date,
                out_reazon = ss.out_reazon,
                mnn = ss.mnn,
                producer = ss.producer,
                comm_name = ss.comm_name,
                limit_opt_incr = ss.limit_opt_incr,
                limit_rozn_incr = ss.limit_rozn_incr,
                limit_rozn_price = ss.limit_rozn_price,
                limit_rozn_price_with_nds = ss.limit_rozn_price_with_nds,
                gos_reestr_item_id = ss.gos_reestr_item_id,
                region_id = ss.region_id,
                price_reg_ndoc = ss.price_reg_ndoc,
            }
            );
        }
    }
}