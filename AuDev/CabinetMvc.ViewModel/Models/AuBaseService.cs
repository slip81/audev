﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CabinetMvc.ViewModel.User;
using CabinetMvc.ViewModel.Adm;
using CabinetMvc.Auth;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel
{
    public class AuBaseService : IDisposable, IAuBaseService
    {
        protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        protected AuMainDb dbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = new AuMainDb();
                }
                return _dbContext;
            }
        }
        private AuMainDb _dbContext;               

        protected AuBaseViewModel modelBeforeChanges;

        private ILoggerService dbLogger;

        public UserViewModel currUser { get; set; }

        [Ninject.Inject]
        public AutoMapper.IMapper ModelMapper { get; set; }

        public AuBaseService()
        {                      
            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            currUser = ((auth == null) || (auth.CurrentUser == null)) ? null : ((IUserProvider)auth.CurrentUser.Identity).User;

            dbLogger = DependencyResolver.Current.GetService<ILoggerService>();
            if (dbLogger == null)
                throw new Exception("dbLogger == null");
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                //logger.Info("_dbContext.Dispose() " + GetType().Name);
                _dbContext.Dispose();
            }
        }

        public void ToLog(string mess, long? log_type_id, LogObject log1 = null, LogObject log2 = null, string mess2 = null)
        {
            dbLogger.ToLog(mess, log_type_id, log1, log2, mess2);
        }

        public AuBaseViewModel GetItem(long id)
        {
            try
            {
                return xGetItem(id);
            }
            catch (Exception ex)
            {
                // log
                return null;
            }
        }

        public IQueryable<AuBaseViewModel> GetList()
        {
            try
            {
                return xGetList();
            }
            catch (Exception ex)
            {
                // log
                return null;
            }
        }

        public IQueryable<AuBaseViewModel> GetList_byParent(long parent_id)
        {
            try
            {
                return xGetList_byParent(parent_id);
            }
            catch (Exception ex)
            {
                // log
                return null;
            }
        }


        public IQueryable<AuBaseViewModel> GetList_bySearch(string search1, string search2)
        {
            try
            {
                return xGetList_bySearch(search1, search2);
            }
            catch (Exception ex)
            {
                // log
                return null;
            }
        }

        public AuBaseViewModel Insert (AuBaseViewModel item, ModelStateDictionary modelState)
        {
            modelBeforeChanges = null;
            try
            {
                var res = xInsert(item, modelState);
                ToLog("Добавление", (long)Enums.LogEventType.CABINET, new LogObject(res != null ? res : item));
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(item), null, GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public AuBaseViewModel Update(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            modelBeforeChanges = null;
            try
            {
                var res = xUpdate(item, modelState);
                ToLog("Изменение", (long)Enums.LogEventType.CABINET, modelBeforeChanges == null ? null : new LogObject(modelBeforeChanges), new LogObject(res != null ? res : item));
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                ToLog("Ошибка при изменении", (long)Enums.LogEventType.CABINET, modelBeforeChanges == null ? null : new LogObject(modelBeforeChanges), new LogObject(item), GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public bool Delete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            try
            {
                var res = xDelete(item, modelState);
                if (res)
                    ToLog("Удаление", (long)Enums.LogEventType.CABINET, new LogObject(item));
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                ToLog("Ошибка при удалении", (long)Enums.LogEventType.CABINET, new LogObject(item), null, GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }



        protected virtual AuBaseViewModel xGetItem(long id)
        {
            throw new NotImplementedException();
        }

        protected virtual IQueryable<AuBaseViewModel> xGetList()
        {
            throw new NotImplementedException();
        }

        protected virtual IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {

            throw new NotImplementedException();
        }

        protected virtual IQueryable<AuBaseViewModel> xGetList_bySearch(string search1, string search2)
        {
            throw new NotImplementedException();
        }

        protected virtual AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }

        protected virtual AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }

        protected virtual bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            throw new NotImplementedException();
        }
    }

}
