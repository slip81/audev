﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeTaskTypeService : IAuBaseService
    {
        ExchangeTaskTypeViewModel InsertType(string type_name, ModelStateDictionary modelState);
    }

    public class ExchangeTaskTypeService : AuBaseService, IExchangeTaskTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.exchange_task_type
                .Select(ss => new ExchangeTaskTypeViewModel()
            {
                type_id = ss.type_id,
                type_name = ss.type_name,
            }
            );
        }

        public ExchangeTaskTypeViewModel InsertType(string type_name, ModelStateDictionary modelState)
        {
            if (String.IsNullOrEmpty(type_name))
            {
                modelState.AddModelError("", "Не заданы атрибуты задачи");
                return null;
            }

            int id = dbContext.exchange_task_type.OrderByDescending(ss => ss.type_id).FirstOrDefault().type_id;
            id = id + 1;
            exchange_task_type exchange_task_type = new exchange_task_type();
            exchange_task_type.type_id = id;
            exchange_task_type.type_name = type_name;
            dbContext.exchange_task_type.Add(exchange_task_type);
            dbContext.SaveChanges();

            return new ExchangeTaskTypeViewModel(exchange_task_type);
        }
    }
}