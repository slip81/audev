﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeTaskViewModel : AuBaseViewModel
    {
        public ExchangeTaskViewModel()
            : base(Enums.MainObjectType.EXCHANGE_TASK)
        {
            //      
        }

        public ExchangeTaskViewModel(exchange_reg_task item)
            : base(Enums.MainObjectType.EXCHANGE_TASK)
        {
            task_id = item.task_id;
            reg_id = item.reg_id;
            task_type_id = item.task_type_id;
            param_date_beg = item.param_date_beg;
            param_date_end = item.param_date_end;
            state = item.state;
            state_date = item.state_date;
            comment = item.comment;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            task_type_name = "";
            state_name = item.state.EnumName<Enums.ExchangeTaskState>();
            ds_id = item.ds_id;
            ds_name = "";
        }

        [Key()]
        [Display(Name = "Код")]
        public long task_id { get; set; }

        [Display(Name = "Код регистрации")]
        public long reg_id { get; set; }
        
        [Display(Name = "Тип задачи")]        
        public int task_type_id { get; set; }

        [Display(Name = "Отправлять с")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> param_date_beg { get; set; }

        [Display(Name = "Отправлять по")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> param_date_end { get; set; }

        [Display(Name = "Статус")]        
        public int state { get; set; }

        [Display(Name = "Статус")]        
        public string state_name { get; set; }

        [Display(Name = "Дата статуса")]        
        public Nullable<System.DateTime> state_date { get; set; }

        [Display(Name = "Комментарий")]        
        public string comment { get; set; }

        [Display(Name = "Дата создания")]        
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]        
        public string crt_user { get; set; }

        [Display(Name = "Тип задачи")]        
        public string task_type_name { get; set; }

        [Display(Name = "Партнер")]        
        public Nullable<long> ds_id { get; set; }

        [Display(Name = "Партнер")]        
        public string ds_name { get; set; }

        [Display(Name = "Тип задачи")]
        [UIHint("TaskTypeCombo")]
        public ExchangeTaskTypeViewModel TaskType { get; set; }

        [Display(Name = "Партнер")]
        [UIHint("DsCombo")]
        public ExchangePartnerViewModel Ds { get; set; }
    }
}