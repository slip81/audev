﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeTaskService : IAuBaseService
    {
        //InsertTask_forUser
        ExchangeTaskViewModel InsertTask_forUser(ExchangeTaskViewModel item, long user_id, ModelStateDictionary modelState);
    }

    public class ExchangeTaskService : AuBaseService, IExchangeTaskService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.exchange_reg_task
                .Select(ss => new ExchangeTaskViewModel()
            {
                task_id = ss.task_id,
                reg_id = ss.reg_id,
                task_type_id = ss.task_type_id,
                param_date_beg = ss.param_date_beg,
                param_date_end = ss.param_date_end,
                state = ss.state,
                state_date = ss.state_date,
                comment = ss.comment,
                crt_date = ss.crt_date,
                crt_user = ss.crt_user,
                task_type_name = ss.exchange_task_type.type_name,
                //state_name = ss.state.EnumName<Enums.ExchangeTaskState>(),
                state_name = ss.state == 0 ? "Выдано" : (ss.state == 1 ? "Получено" : (ss.state == 2 ? "Выполнено с ошибками" : "Выполнено без ошибок")),
                ds_id = ss.ds_id,
                ds_name = ss.datasource.ds_name,
                TaskType = new ExchangeTaskTypeViewModel() { type_id = ss.task_type_id, type_name = ss.exchange_task_type.type_name, },
                Ds = new ExchangePartnerViewModel() { ds_id = (long)ss.ds_id, ds_name = ss.datasource.ds_name, },
            }
            );
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            var reg_id = (from t1 in dbContext.exchange_user_reg
                          from t2 in dbContext.exchange_reg_partner
                          where t1.reg_id == t2.reg_id
                          && t1.user_id == parent_id
                          select t2.reg_id)
                      .FirstOrDefault();

            return dbContext.exchange_reg_task
                .Where(ss => ss.reg_id == reg_id)
                .Select(ss => new ExchangeTaskViewModel()
                {
                    task_id = ss.task_id,
                    reg_id = ss.reg_id,
                    task_type_id = ss.task_type_id,
                    param_date_beg = ss.param_date_beg,
                    param_date_end = ss.param_date_end,
                    state = ss.state,
                    state_date = ss.state_date,
                    comment = ss.comment,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    task_type_name = ss.exchange_task_type.type_name,
                    //state_name = ss.state.EnumName<Enums.ExchangeTaskState>(),
                    state_name = ss.state == 0 ? "Выдано" : (ss.state == 1 ? "Получено" : (ss.state == 2 ? "Выполнено с ошибками" : "Выполнено без ошибок")),
                    ds_id = ss.ds_id,
                    ds_name = ss.datasource.ds_name,
                    TaskType = new ExchangeTaskTypeViewModel() { type_id = ss.task_type_id, type_name = ss.exchange_task_type.type_name, },
                    Ds = new ExchangePartnerViewModel() { ds_id = (long)ss.ds_id, ds_name = ss.datasource.ds_name, },
                }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.exchange_reg_task
                .Where(ss => ss.task_id == id)
                .Select(ss => new ExchangeTaskViewModel()
                {
                    task_id = ss.task_id,
                    reg_id = ss.reg_id,
                    task_type_id = ss.task_type_id,
                    param_date_beg = ss.param_date_beg,
                    param_date_end = ss.param_date_end,
                    state = ss.state,
                    state_date = ss.state_date,
                    comment = ss.comment,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    task_type_name = ss.exchange_task_type.type_name,
                    //state_name = ss.state.EnumName<Enums.ExchangeTaskState>(),
                    state_name = ss.state == 0 ? "Выдано" : (ss.state == 1 ? "Получено" : (ss.state == 2 ? "Выполнено с ошибками" : "Выполнено без ошибок")),
                    ds_id = ss.ds_id,
                    ds_name = ss.datasource.ds_name,
                    TaskType = new ExchangeTaskTypeViewModel() { type_id = ss.task_type_id, type_name = ss.exchange_task_type.type_name, },
                    Ds = new ExchangePartnerViewModel() { ds_id = (long)ss.ds_id, ds_name = ss.datasource.ds_name, },
                }
            )
            .FirstOrDefault();
        }

        public ExchangeTaskViewModel InsertTask_forUser(ExchangeTaskViewModel item, long user_id, ModelStateDictionary modelState)
        {

            ExchangeTaskViewModel itemAdd = item;
            if ((itemAdd == null) || (itemAdd.TaskType == null) || (itemAdd.Ds == null))
            {
                modelState.AddModelError("", "Не заданы атрибуты задачи");
                return null;
            }
            DateTime now = DateTime.Now;
            int taskTypeId = itemAdd.TaskType.type_id;
            long dsId = itemAdd.Ds.ds_id;
            var regs = (from t1 in dbContext.exchange_user_reg
                        from t2 in dbContext.exchange_reg_partner
                        where t1.reg_id == t2.reg_id
                        && t1.user_id == user_id
                        //&& t2.ds_id == dsId
                        select t2
                        ).ToList();
            long regId = -1;
            if ((regs != null) && (regs.Count > 0))
            {
                var reg = regs.Where(ss => ss.ds_id == dsId).FirstOrDefault();
                if (reg == null)
                {
                    reg = regs.FirstOrDefault();
                }
                if (reg != null)
                {
                    regId = reg.reg_id;
                }
            }

            /*
            long regId = (from t1 in dbContext.exchange_user_reg
                           from t2 in dbContext.exchange_reg_partner
                           where t1.reg_id == t2.reg_id
                           && t1.user_id == user_id
                           && t2.ds_id == dsId
                           select t1.reg_id
                          )
                          .FirstOrDefault();
            */
            if ((regId <= 0) || (dsId <= 0) || (taskTypeId <= 0))
            {
                modelState.AddModelError("", "Не заданы атрибуты задачи");
                return null;
            }

            exchange_reg_task exchange_reg_task = new exchange_reg_task();
            exchange_reg_task.comment = itemAdd.comment;
            exchange_reg_task.crt_date = now;
            exchange_reg_task.crt_user = currUser.UserName;
            exchange_reg_task.param_date_beg = taskTypeId == 1 ? itemAdd.param_date_beg : null;
            exchange_reg_task.param_date_end = taskTypeId == 1 ? itemAdd.param_date_end : null;
            exchange_reg_task.reg_id = regId;
            exchange_reg_task.state = (int)Enums.ExchangeTaskState.CREATED;
            exchange_reg_task.state_date = now;
            exchange_reg_task.task_type_id = taskTypeId;
            exchange_reg_task.ds_id = dsId;

            dbContext.exchange_reg_task.Add(exchange_reg_task);
            dbContext.SaveChanges();

            return new ExchangeTaskViewModel(exchange_reg_task);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeTaskViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeTaskViewModel itemEdit = (ExchangeTaskViewModel)item;
            if ((itemEdit == null) || (itemEdit.TaskType == null) || (itemEdit.Ds == null))
            {
                modelState.AddModelError("", "Не заданы атрибуты задачи");
                return null;
            }

            exchange_reg_task exchange_reg_task = dbContext.exchange_reg_task.Where(ss => ss.task_id == itemEdit.task_id).FirstOrDefault();
            if (exchange_reg_task == null)
            {
                modelState.AddModelError("", "Не найдено задание с кодом " + itemEdit.task_id.ToString());
                return null;
            }

            modelBeforeChanges = new ExchangeTaskViewModel(exchange_reg_task);
            
            int taskTypeId = itemEdit.TaskType.type_id;
            long dsId = itemEdit.Ds.ds_id;            
            exchange_reg_task.comment = itemEdit.comment;
            exchange_reg_task.param_date_beg = taskTypeId == 1 ? itemEdit.param_date_beg : null;
            exchange_reg_task.param_date_end = taskTypeId == 1 ? itemEdit.param_date_end : null;
            exchange_reg_task.task_type_id = taskTypeId;
            exchange_reg_task.ds_id = dsId;
            
            dbContext.SaveChanges();

            return new ExchangeTaskViewModel(exchange_reg_task);
        } 

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeTaskViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((ExchangeTaskViewModel)item).task_id;

            exchange_reg_task exchange_reg_task = dbContext.exchange_reg_task.Where(ss => ss.task_id == id).FirstOrDefault();
            if (exchange_reg_task == null)
            {
                modelState.AddModelError("serv_result", "Задача не найдена");
                return false;
            }

            dbContext.exchange_reg_task.Remove(exchange_reg_task);
            dbContext.SaveChanges();
            return true;
        }
    }
}