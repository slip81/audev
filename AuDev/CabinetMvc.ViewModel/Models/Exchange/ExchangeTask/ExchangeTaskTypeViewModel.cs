﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeTaskTypeViewModel : AuBaseViewModel
    {
        public ExchangeTaskTypeViewModel()
            : base(Enums.MainObjectType.EXCHANGE_TASK_TYPE)
        {
            //
        }

        public ExchangeTaskTypeViewModel(exchange_task_type item)
            : base(Enums.MainObjectType.EXCHANGE_TASK_TYPE)
        {
            type_id = item.type_id;
            type_name = item.type_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public int type_id { get; set; }

        [Display(Name = "Наименование")]
        public string type_name { get; set; }
    }
}