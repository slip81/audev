﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeDocDataService : IAuBaseService
    {
        //
    }

    public class ExchangeDocDataService : AuBaseService, IExchangeDocDataService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.exchange_doc_data.Where(ss => ss.item_id == id).Select(ss => new ExchangeDocDataViewModel()
            {
                item_id = ss.item_id,
                data_text = ss.data_text,
                data_binary = ss.data_binary,                
            }
            )
            .FirstOrDefault();
        }
    }
}