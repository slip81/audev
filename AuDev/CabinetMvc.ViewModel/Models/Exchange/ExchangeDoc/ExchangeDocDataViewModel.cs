﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeDocDataViewModel :  AuBaseViewModel
    {
        public ExchangeDocDataViewModel()
            : base(Enums.MainObjectType.EXCHANGE_DOC_DATA)
        {
            //
        }

        public ExchangeDocDataViewModel(exchange_doc_data item)
            : base(Enums.MainObjectType.EXCHANGE_DOC_DATA)
        {
            item_id = item.item_id;
            data_text = item.data_text;            
            data_binary = item.data_binary;            
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Display(Name = "Даннные (текст)")]
        public string data_text { get; set; }

        [Display(Name = "Даннные (бинарные)")]
        public byte[] data_binary { get; set; }

        [Display(Name = "Даннные")]        
        public string DataTextFromBinary { get { return GlobalUtil.GetString(this.data_binary); } }
    }
}