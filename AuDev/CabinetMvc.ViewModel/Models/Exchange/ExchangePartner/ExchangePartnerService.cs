﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangePartnerService : IAuBaseService
    {
        //
    }

    public class ExchangePartnerService : AuBaseService, IExchangePartnerService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.datasource
                .Where(ss => ss.ds_id > 100)
                .ProjectTo<ExchangePartnerViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return (from eu in dbContext.exchange_user
                      from eur in dbContext.exchange_user_reg
                      from erp in dbContext.exchange_reg_partner
                      from ds in dbContext.datasource
                      where eu.client_id == parent_id
                      && eu.user_id == eur.user_id
                      && eur.reg_id == erp.reg_id
                      && erp.ds_id == ds.ds_id
                      && ds.ds_id > 100
                      select ds)
                      .ProjectTo<ExchangePartnerViewModel>()
                      .Distinct();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.datasource
                .Where(ss => ss.ds_id == id)
                .ProjectTo<ExchangePartnerViewModel>()
                .FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangePartnerViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangePartnerViewModel itemAdd = (ExchangePartnerViewModel)item;
            if (
                (itemAdd == null)
                ||
                (String.IsNullOrEmpty(itemAdd.ds_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты Партнера");
                return null;
            }

            long max_ds_id = 0;
            max_ds_id = dbContext.datasource.Where(ss => ss.ds_id > 100).Max(ss => ss.ds_id);
            max_ds_id = max_ds_id < 100 ? 100 : max_ds_id;

            itemAdd.ds_id = max_ds_id + 1;

            datasource datasource = new datasource();
            /*
            datasource.ds_id = max_ds_id + 1;
            datasource.ds_name = itemAdd.ds_name;
            */
            ModelMapper.Map<ExchangePartnerViewModel, datasource>(itemAdd, datasource);
            dbContext.datasource.Add(datasource);

            /*
            if (!String.IsNullOrEmpty(itemAdd.exchange_item_get_list))
            {
                var deserGetList = JsonConvert.DeserializeObject<List<ExchangePartnerItemViewModel>>(itemAdd.exchange_item_get_list);
                if ((deserGetList != null) && (deserGetList.Count > 0))
                {
                    foreach (var deserGet in deserGetList)
                    {
                        datasource_exchange_item datasource_exchange_item = new datasource_exchange_item();
                        datasource_exchange_item.ds_id = datasource.ds_id;
                        datasource_exchange_item.file_name_mask = deserGet.file_name_mask;
                        datasource_exchange_item.for_get = deserGet.for_get;
                        datasource_exchange_item.for_send = deserGet.for_send;
                        datasource_exchange_item.item_file_name = deserGet.item_file_name;
                        datasource_exchange_item.item_id = deserGet.item_id;
                        datasource_exchange_item.item_type_id = deserGet.item_type_id;
                        datasource_exchange_item.param = deserGet.param;
                        datasource_exchange_item.sql_text = deserGet.sql_text;
                        dbContext.datasource_exchange_item.Add(datasource_exchange_item);
                    }
                }
            }

            if (!String.IsNullOrEmpty(itemAdd.exchange_item_send_list))
            {
                var deserSendList = JsonConvert.DeserializeObject<List<ExchangePartnerItemViewModel>>(itemAdd.exchange_item_send_list);
                if ((deserSendList != null) && (deserSendList.Count > 0))
                {
                    foreach (var deserSend in deserSendList)
                    {
                        datasource_exchange_item datasource_exchange_item = new datasource_exchange_item();
                        datasource_exchange_item.ds_id = datasource.ds_id;
                        datasource_exchange_item.file_name_mask = deserSend.file_name_mask;
                        datasource_exchange_item.for_get = deserSend.for_get;
                        datasource_exchange_item.for_send = deserSend.for_send;
                        datasource_exchange_item.item_file_name = deserSend.item_file_name;
                        datasource_exchange_item.item_id = deserSend.item_id;
                        datasource_exchange_item.item_type_id = deserSend.item_type_id;
                        datasource_exchange_item.param = deserSend.param;
                        datasource_exchange_item.sql_text = deserSend.sql_text;
                        dbContext.datasource_exchange_item.Add(datasource_exchange_item);
                    }
                }
            }
            */
            dbContext.SaveChanges();

            ExchangePartnerViewModel res = new ExchangePartnerViewModel();
            ModelMapper.Map<datasource, ExchangePartnerViewModel>(datasource, res);
            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangePartnerViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangePartnerViewModel itemEdit = (ExchangePartnerViewModel)item;
            if (
                (itemEdit == null)
                ||
                (String.IsNullOrEmpty(itemEdit.ds_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты Партнера");
                return null;
            }

            var datasource = dbContext.datasource.Where(ss => ss.ds_id == itemEdit.ds_id).FirstOrDefault();
            if (datasource == null)
            {
                modelState.AddModelError("", "Не найден Партнер с кодом " + itemEdit.ds_id.ToString());
                return null;
            }

            var datasource_existing = dbContext.datasource.Where(ss => ss.ds_id > 100 && ss.ds_id != itemEdit.ds_id && ss.ds_name.Trim().ToLower().Equals(itemEdit.ds_name.Trim().ToLower())).FirstOrDefault();
            if (datasource_existing != null)
            {
                modelState.AddModelError("", "Уже есть Партнер с аналогичным названием");
                return null;
            }

            ExchangePartnerViewModel modelOld = new ExchangePartnerViewModel();
            ModelMapper.Map<datasource, ExchangePartnerViewModel>(datasource, modelOld);
            modelBeforeChanges = modelOld;

            ModelMapper.Map<ExchangePartnerViewModel, datasource>(itemEdit, datasource);
            //datasource.ds_name = itemEdit.ds_name;

            /*
            dbContext.datasource_exchange_item.RemoveRange(dbContext.datasource_exchange_item.Where(ss => ss.ds_id == itemEdit.ds_id));

            if (!String.IsNullOrEmpty(itemEdit.exchange_item_get_list))
            {
                var deserGetList = JsonConvert.DeserializeObject<List<ExchangePartnerItemViewModel>>(itemEdit.exchange_item_get_list);
                if ((deserGetList != null) && (deserGetList.Count > 0))
                {
                    foreach (var deserGet in deserGetList)
                    {
                        datasource_exchange_item datasource_exchange_item = new datasource_exchange_item();
                        datasource_exchange_item.ds_id = datasource.ds_id;
                        datasource_exchange_item.file_name_mask = deserGet.file_name_mask;
                        datasource_exchange_item.for_get = deserGet.for_get;
                        datasource_exchange_item.for_send = deserGet.for_send;
                        datasource_exchange_item.item_file_name = deserGet.item_file_name;
                        datasource_exchange_item.item_id = deserGet.item_id;
                        datasource_exchange_item.item_type_id = deserGet.item_type_id;
                        datasource_exchange_item.param = deserGet.param;
                        datasource_exchange_item.sql_text = deserGet.sql_text;
                        dbContext.datasource_exchange_item.Add(datasource_exchange_item);
                    }
                }
            }

            if (!String.IsNullOrEmpty(itemEdit.exchange_item_send_list))
            {
                var deserSendList = JsonConvert.DeserializeObject<List<ExchangePartnerItemViewModel>>(itemEdit.exchange_item_send_list);
                if ((deserSendList != null) && (deserSendList.Count > 0))
                {
                    foreach (var deserSend in deserSendList)
                    {
                        datasource_exchange_item datasource_exchange_item = new datasource_exchange_item();
                        datasource_exchange_item.ds_id = datasource.ds_id;
                        datasource_exchange_item.file_name_mask = deserSend.file_name_mask;
                        datasource_exchange_item.for_get = deserSend.for_get;
                        datasource_exchange_item.for_send = deserSend.for_send;
                        datasource_exchange_item.item_file_name = deserSend.item_file_name;
                        datasource_exchange_item.item_id = deserSend.item_id;
                        datasource_exchange_item.item_type_id = deserSend.item_type_id;
                        datasource_exchange_item.param = deserSend.param;
                        datasource_exchange_item.sql_text = deserSend.sql_text;
                        dbContext.datasource_exchange_item.Add(datasource_exchange_item);
                    }
                }
            }
            */

            dbContext.SaveChanges();

            //return new ExchangePartnerViewModel(datasource);            
            ExchangePartnerViewModel res = new ExchangePartnerViewModel();
            ModelMapper.Map<datasource, ExchangePartnerViewModel>(datasource, res);
            return res;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangePartnerViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((ExchangePartnerViewModel)item).ds_id;

            datasource datasource = null;

            datasource = dbContext.datasource.Where(ss => ss.ds_id == id).FirstOrDefault();
            if (datasource == null)
            {
                modelState.AddModelError("serv_result", "Партнер не найден");
                return false;
            }

            var exchange_reg_partner = dbContext.exchange_reg_partner.Where(ss => ss.ds_id == id).FirstOrDefault();
            if (exchange_reg_partner != null)
            {
                modelState.AddModelError("serv_result", "Есть регистрации на данного Партнера");
                return false;
            }

            var exchange_file_node = dbContext.exchange_file_node.Where(ss => ss.partner_id == id).FirstOrDefault();
            if (exchange_file_node != null)
            {
                modelState.AddModelError("serv_result", "Есть регистрации на данного Партнера");
                return false;
            }

            dbContext.datasource_exchange_item.RemoveRange(dbContext.datasource_exchange_item.Where(ss => ss.ds_id == id));
            dbContext.datasource.Remove(datasource);
            dbContext.SaveChanges();
            return true;
        }

    }
}