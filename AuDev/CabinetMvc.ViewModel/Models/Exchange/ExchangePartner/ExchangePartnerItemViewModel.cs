﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangePartnerItemViewModel : AuBaseViewModel
    {
        public ExchangePartnerItemViewModel()
            : base(Enums.MainObjectType.EXCHANGE_PARTNER_ITEM)
        {
            //vw_datasource_exchange_item
        }


        [Key()]
        [Display(Name = "Код")]
        public long rel_id { get; set; }

        [Display(Name = "Партнер")]
        public long ds_id { get; set; }

        [Display(Name = "Тип файла")]
        public int item_id { get; set; }

        [Display(Name = "Для отправки")]
        public bool for_send { get; set; }

        [Display(Name = "Для получения")]
        public bool for_get { get; set; }

        [Display(Name = "item_type_id")]
        public Nullable<int> item_type_id { get; set; }

        [Display(Name = "item_file_name")]
        public string item_file_name { get; set; }

        [Display(Name = "sql_text")]
        public string sql_text { get; set; }

        [Display(Name = "param")]
        public string param { get; set; }

        [Display(Name = "Имя файла (маска)")]
        public string file_name_mask { get; set; }

        [Display(Name = "Партнер")]
        public string ds_name { get; set; }

        [Display(Name = "Тип файла")]
        public string item_name { get; set; }
    }
}