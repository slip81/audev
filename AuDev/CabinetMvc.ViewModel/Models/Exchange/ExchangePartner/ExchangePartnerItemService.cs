﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangePartnerItemService : IAuBaseService
    {
        IQueryable<ExchangePartnerItemViewModel> GetList_forGet(long parent_id);
        IQueryable<ExchangePartnerItemViewModel> GetList_forSend(long parent_id);
    }

    public class ExchangePartnerItemService : AuBaseService, IExchangePartnerItemService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_datasource_exchange_item
                .Where(ss => ss.ds_id == parent_id)
                .ProjectTo<ExchangePartnerItemViewModel>();
        }

        public IQueryable<ExchangePartnerItemViewModel> GetList_forGet(long parent_id)
        {
            return dbContext.vw_datasource_exchange_item
                .Where(ss => ss.ds_id == parent_id && ss.for_get && ss.item_id > 0)
                .ProjectTo<ExchangePartnerItemViewModel>();
        }

        public IQueryable<ExchangePartnerItemViewModel> GetList_forSend(long parent_id)
        {
            return dbContext.vw_datasource_exchange_item
                .Where(ss => ss.ds_id == parent_id && ss.for_send && ss.item_id > 0)
                .ProjectTo<ExchangePartnerItemViewModel>();
        }
    }
}