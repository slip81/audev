﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangePartnerViewModel : AuBaseViewModel
    {
        public ExchangePartnerViewModel()
            : base(Enums.MainObjectType.EXCHANGE_PARTNER)
        {
            //datasource
        }

        [Key()]
        [Display(Name = "Код")]
        public long ds_id { get; set; }

        [Display(Name = "Название партнера")]
        [Required(ErrorMessage = "Не задано наименование")]
        public string ds_name { get; set; }

        /*
        [Display(Name = "exchange_item_get_list")]
        public string exchange_item_get_list { get; set; }

        [Display(Name = "exchange_item_send_list")]
        public string exchange_item_send_list { get; set; }
        */
    }
}