﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeLogDateViewModel : AuBaseViewModel
    {
        public ExchangeLogDateViewModel()
            : base()
        {
            //
        }

        [Display(Name = "date_id")]
        public Nullable<long> date_id { get; set; }

        [Display(Name = "client_id")]
        public Nullable<long> client_id { get; set; }

        [Display(Name = "user_id")]
        public Nullable<long> user_id { get; set; }

        [Display(Name = "Дата-время")]
        public string date_beg { get; set; }
    }
}