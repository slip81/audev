﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeLogDateService : IAuBaseService
    {
        IQueryable<ExchangeLogDateViewModel> GetList_ForLog(long client_id, long user_id);
    }

    public class ExchangeLogDateService : AuBaseService, IExchangeLogDateService
    {
        public IQueryable<ExchangeLogDateViewModel> GetList_ForLog(long client_id, long user_id)
        {            
            return (from l in dbContext.vw_log_esn_exchange_partner
                    where l.client_id == client_id
                    && l.user_id == user_id
                    select l)    
                    .Select(ss => new ExchangeLogDateViewModel()
                    {
                        date_id = ss.date_id,
                        client_id = ss.client_id,
                        user_id = ss.user_id,
                        date_beg = ss.date_beg_minutes,
                    })
                    .Distinct();
        }
    }
}