﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeLogViewModel : AuBaseViewModel
    {
        public ExchangeLogViewModel()
            : base(Enums.MainObjectType.EXCHANGE_LOG)
        {
            //
        }

        public ExchangeLogViewModel(log_esn item)
            : base(Enums.MainObjectType.EXCHANGE_LOG)
        {
            log_id = item.log_id;
            date_beg = item.date_beg;
            mess = item.mess;
            user_name = item.user_name;
            log_event_type_id = item.log_esn_type_id;
            obj_id = item.obj_id;
            scope = item.scope;
            date_end = item.date_end;
            mess2 = item.mess2;
        }

        [Key()]
        [Display(Name = "Код")]
        public long log_id { get; set; }

        [Display(Name = "Дата начала")]
        public System.DateTime date_beg { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }

        [Display(Name = "Пользователь")]
        public string user_name { get; set; }

        [Display(Name = "Тип операции")]
        public Nullable<long> log_event_type_id { get; set; }

        [Display(Name = "Тип операции")]
        public string log_event_type_str { get; set; }

        [Display(Name = "Данные")]
        public Nullable<long> obj_id { get; set; }
        
        [Display(Name = "scope")]
        public Nullable<int> scope { get; set; }

        [Display(Name = "Дата окончания")]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "Доп. сообщение")]
        public string mess2 { get; set; }

        [Display(Name = "Данные")]
        public string ViewDataButton
        {
            get
            {
                return this.obj_id.HasValue ? ("<a href=/ExchangeDataView?obj_id=" + this.obj_id.ToString() + " class='k-button'>Посмотреть</a>") : "";                    
            }
        }
    }
}