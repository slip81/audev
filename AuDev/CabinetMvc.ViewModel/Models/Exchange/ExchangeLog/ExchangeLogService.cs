﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeLogService : IAuBaseService
    {
        IQueryable<ExchangeLogViewModel> GetList_ForLog(long client_id, long user_id, string date_beg);
    }

    public class ExchangeLogService : AuBaseService, IExchangeLogService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.log_esn
                .Where(ss => ss.scope == (int)Enums.LogScope.EXCHANGE_PARTNER)
                .Select(ss => new ExchangeLogViewModel()
            {
                log_id = ss.log_id,
                date_beg = ss.date_beg,
                mess = ss.mess,
                user_name = ss.user_name,
                log_event_type_id = ss.log_esn_type_id,
                log_event_type_str = ss.log_esn_type.type_name,
                obj_id = ss.obj_id,
                scope = ss.scope,
                date_end = ss.date_end,
                mess2 = ss.mess2,
            }
            );
        }

        public IQueryable<ExchangeLogViewModel> GetList_ForLog(long client_id, long user_id, string date_beg)
        {
            DateTime dt = DateTime.Now;
            bool boo = false;
            string err = "";
            boo = DateTime.TryParse(date_beg, out dt);
            if (!boo)
                err = "Не удалось преобразовать " + date_beg + " в дату";

            return (from l in dbContext.vw_log_esn_exchange_partner
                    from lt in dbContext.log_esn_type
                    where l.log_esn_type_id == lt.type_id
                    && l.client_id == client_id
                    && l.user_id == user_id
                    && l.date_beg_minutes == date_beg
                    select new { l.log_id, l.date_beg, l.mess, l.user_name, l.log_esn_type_id, l.obj_id, l.scope, l.date_end, l.mess2, lt.type_name })
                    .Select(ss => new ExchangeLogViewModel()
                    {
                        log_id = ss.log_id,
                        date_beg = ss.date_beg,
                        mess = boo ? ss.mess : err,
                        user_name = ss.user_name,
                        log_event_type_id = ss.log_esn_type_id,
                        log_event_type_str = ss.type_name,
                        /*
                        log_event_type_str = ss.log_event_type_id == (long)Enums.ExchangeLogEventType.GET_EXCHANGE_XML ? "Скачивание XML" :
                            (ss.log_event_type_id == (long)Enums.ExchangeLogEventType.SEND_EXCHANGE_DATA ? "Отправка данных" :
                            (ss.log_event_type_id == (long)Enums.ExchangeLogEventType.MAKE_EXCHANGE_FILE ? "Формирование файла" : "Ошибка")
                            ),
                        */
                        obj_id = ss.obj_id,
                        scope = ss.scope,
                        date_end = ss.date_end,
                        mess2 = ss.mess2,
                    }
                    );
        }
    }
}