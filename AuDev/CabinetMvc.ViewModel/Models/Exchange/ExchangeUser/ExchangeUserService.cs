﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeUserService : IAuBaseService
    {
        ExchangeUserViewModel GetItem_byLoginAndWorkplace(string login, string workplace);
        IQueryable<ExchangeUserViewModel> GetList_ForLog(long client_id);
    }

    public class ExchangeUserService : AuBaseService, IExchangeUserService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_exchange_user
                // !!!
                .Where(ss => ss.workplace_id.HasValue)
                .Select(ss => new ExchangeUserViewModel()
            {
                user_id = ss.user_id,
                login = ss.login,
                workplace = ss.workplace,                           
                client_id = ss.exchange_client_id,
                client_name = ss.client_name,
                cabinet_sales_id = ss.sales_id,
                cabinet_workplace_id = ss.workplace_id,
                cabinet_sales_name = ss.sales_name,
                cabinet_workplace_name = ss.workplace_description,  
                task_cnt = ss.task_cnt,
                task_cnt_completed = ss.task_cnt_completed,
                task_info = ss.task_info,
            }
            );
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_exchange_user
                .Where(ss => ss.exchange_client_id == parent_id
                    // !!!
                    && ss.workplace_id.HasValue
                )
                .Select(ss => new ExchangeUserViewModel()
            {
                user_id = ss.user_id,
                login = ss.login,
                workplace = ss.workplace,
                client_id = ss.exchange_client_id,
                client_name = ss.client_name,
                cabinet_sales_id = ss.sales_id,
                cabinet_workplace_id = ss.workplace_id,
                cabinet_sales_name = ss.sales_name,
                cabinet_workplace_name = ss.workplace_description,
                task_cnt = ss.task_cnt,
                task_cnt_completed = ss.task_cnt_completed,
                task_info = ss.task_info,
            }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_exchange_user
                .Where(ss => ss.user_id == id)
                .Select(ss => new ExchangeUserViewModel()
            {
                user_id = ss.user_id,
                login = ss.login,
                workplace = ss.workplace,
                client_id = ss.exchange_client_id,
                client_name = ss.client_name,
                cabinet_sales_id = ss.sales_id,
                cabinet_workplace_id = ss.workplace_id,
                cabinet_sales_name = ss.sales_name,
                cabinet_workplace_name = ss.workplace_description,
                task_cnt = ss.task_cnt,
                task_cnt_completed = ss.task_cnt_completed,
                task_info = ss.task_info,
            }
            )
            .FirstOrDefault();
        }

        public ExchangeUserViewModel GetItem_byLoginAndWorkplace(string login, string workplace)
        {
            return dbContext.vw_exchange_user
                .Where(ss => ss.login == login && ss.workplace == workplace)
                .Select(ss => new ExchangeUserViewModel()
                {
                    user_id = ss.user_id,
                    login = ss.login,
                    workplace = ss.workplace,
                    client_id = ss.exchange_client_id,
                    client_name = ss.client_name,
                    cabinet_sales_id = ss.sales_id,
                    cabinet_workplace_id = ss.workplace_id,
                    cabinet_sales_name = ss.sales_name,
                    cabinet_workplace_name = ss.workplace_description,
                    task_cnt = ss.task_cnt,
                    task_cnt_completed = ss.task_cnt_completed,
                    task_info = ss.task_info,
                }
                ).FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeUserViewModel user_add = (ExchangeUserViewModel)item;
            if (
                (user_add == null)
                ||
                (!user_add.cabinet_sales_id.HasValue)
                ||
                (!user_add.cabinet_workplace_id.HasValue)
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return null;
            }

            exchange_user existing_exchange_user = dbContext.exchange_user.Where(ss => ss.cabinet_sales_id == user_add.cabinet_sales_id && ss.cabinet_workplace_id == user_add.cabinet_workplace_id).FirstOrDefault();
            if (existing_exchange_user != null)
            {
                modelState.AddModelError("", "Для данного рабочего места уже заведен пользователь");
                return null;
            }

            /*
            string login = (from s in dbContext.sales
                         from u in dbContext.my_aspnet_users
                         where s.user_id == u.id
                         && s.id == user_add.cabinet_sales_id
                         select u.name)
                        .FirstOrDefault()
                        ;
             */

            string login = dbContext.vw_client_user.Where(ss => ss.sales_id == user_add.cabinet_sales_id && ss.is_main_for_sales).Select(ss => ss.user_login).FirstOrDefault();
            string workplace = dbContext.workplace.Where(ss => ss.id == user_add.cabinet_workplace_id).Select(ss => ss.registration_key).FirstOrDefault();

            exchange_user exchange_user = new exchange_user();
            exchange_user.client_id = user_add.client_id;
            exchange_user.login = login;
            exchange_user.workplace = workplace;
            exchange_user.cabinet_sales_id = user_add.cabinet_sales_id;
            exchange_user.cabinet_workplace_id = user_add.cabinet_workplace_id;
            
            //exchange_user.login = user_add.login;
            //exchange_user.workplace = user_add.workplace;
            //exchange_user.version_num = user_add.version_num;            

            dbContext.exchange_user.Add(exchange_user);
            dbContext.SaveChanges();

            return new ExchangeUserViewModel(exchange_user);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeUserViewModel user_edit = (ExchangeUserViewModel)item;
            if (
                (user_edit == null)
                ||
                (!user_edit.cabinet_sales_id.HasValue)
                ||
                (!user_edit.cabinet_workplace_id.HasValue)
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return null;
            }

            var exchange_user = dbContext.exchange_user.Where(ss => ss.user_id == user_edit.user_id).FirstOrDefault();
            if (exchange_user == null)
            {
                modelState.AddModelError("", "Не найден пользователь с кодом " + user_edit.user_id.ToString());
                return null;
            }

            exchange_user existing_exchange_user = dbContext.exchange_user.Where(ss => ss.cabinet_sales_id == user_edit.cabinet_sales_id && ss.cabinet_workplace_id == user_edit.cabinet_workplace_id).FirstOrDefault();
            if (existing_exchange_user != null)
            {
                modelState.AddModelError("", "Для данного рабочего места уже заведен пользователь");
                return null;
            }

            modelBeforeChanges = new ExchangeUserViewModel(exchange_user);

            /*
            string login = (from s in dbContext.sales
                            from u in dbContext.my_aspnet_users
                            where s.user_id == u.id
                            && s.id == user_edit.cabinet_sales_id
                            select u.name)
                        .FirstOrDefault()
                        ;
            */
            string login = dbContext.vw_client_user.Where(ss => ss.sales_id == user_edit.cabinet_sales_id && ss.is_main_for_sales).Select(ss => ss.user_login).FirstOrDefault();
            string workplace = dbContext.workplace.Where(ss => ss.id == user_edit.cabinet_workplace_id).Select(ss => ss.registration_key).FirstOrDefault();

            exchange_user.login = login;
            exchange_user.workplace = workplace;
            exchange_user.cabinet_sales_id = user_edit.cabinet_sales_id;
            exchange_user.cabinet_workplace_id = user_edit.cabinet_workplace_id;

            //exchange_user.client_id = user_edit.client_id;            
            //exchange_user.login = user_edit.login;
            //exchange_user.version_num = user_edit.version_num;
            //exchange_user.workplace = user_edit.workplace;

            dbContext.SaveChanges();

            return new ExchangeUserViewModel(exchange_user);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeUserViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((ExchangeUserViewModel)item).user_id;

            exchange_user exchange_user = null;

            exchange_user = dbContext.exchange_user.Where(ss => ss.user_id == id).FirstOrDefault();
            if (exchange_user == null)
            {
                modelState.AddModelError("serv_result", "Пользователь не найден");
                return false;
            }

            var exch = dbContext.exchange.Where(ss => ss.user_id == exchange_user.user_id).FirstOrDefault();
            if (exch != null)
            {
                modelState.AddModelError("serv_result", "Есть данные обмена по этому пользователю");
                return false;
            }

            dbContext.exchange_user_reg.RemoveRange(dbContext.exchange_user_reg.Where(ss => ss.user_id == exchange_user.user_id));
            dbContext.exchange_user.Remove(exchange_user);

            dbContext.SaveChanges();
            return true;
        }

        public IQueryable<ExchangeUserViewModel> GetList_ForLog(long client_id)
        {
            return (from eu in dbContext.exchange_user
                    from l in dbContext.vw_log_esn_exchange_partner
                    where eu.client_id == l.client_id
                    && eu.client_id == client_id
                    select eu)
                      .Distinct()
                      .Select(ss => new ExchangeUserViewModel()
                      {
                          user_id = ss.user_id,
                          login = ss.login,
                          workplace = ss.workplace,
                          version_num = ss.version_num,
                          client_id = ss.client_id,
                          client_name = ss.client_id.HasValue ? ss.exchange_client.client_name : "",
                      }
            );
        }
    }
}