﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeUserViewModel : AuBaseViewModel
    {
        public ExchangeUserViewModel()
            : base(Enums.MainObjectType.EXCHANGE_USER)
        {
            //
        }

        public ExchangeUserViewModel(exchange_user item)
            : base(Enums.MainObjectType.EXCHANGE_USER)
        {
            user_id = item.user_id;
            login = item.login;
            workplace = item.workplace;
            version_num = item.version_num;
            client_id = item.client_id;
            cabinet_sales_id = item.cabinet_sales_id;
            cabinet_workplace_id = item.cabinet_workplace_id;
        }

        [Key()]
        [Display(Name = "Код")]
        public long user_id { get; set; }

        [Display(Name = "Логин")]        
        //[Required(ErrorMessage = "Не задан логин")]
        public string login { get; set; }

        [Display(Name = "Железный ключ")]
        //[Required(ErrorMessage = "Не задано рабочее место")]
        public string workplace { get; set; }

        [Display(Name = "Номер версии")]        
        public string version_num { get; set; }

        [Display(Name = "Код участника")]
        public long? client_id { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }

        [Display(Name = "Торговая точка")]
        public Nullable<long> cabinet_sales_id { get; set; }

        [Display(Name = "Торговая точка")]
        public string cabinet_sales_name { get; set; }

        [Display(Name = "Рабочее место")]
        [Required(ErrorMessage = "Не задано рабочее место")]
        public Nullable<long> cabinet_workplace_id { get; set; }

        [Display(Name = "Рабочее место")]        
        public string cabinet_workplace_name { get; set; }

        [Display(Name = "Всего заданий")]
        public Nullable<int> task_cnt { get; set; }

        [Display(Name = "Выполнено заданий")]
        public Nullable<int> task_cnt_completed { get; set; }

        [Display(Name = "Задания")]
        public string task_info { get; set; }
    }

}