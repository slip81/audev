﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using CabinetMvc.ViewModel.Util;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeRegDocViewModel : AuBaseViewModel
    {
        public ExchangeRegDocViewModel()
            : base(Enums.MainObjectType.EXCHANGE_REG_DOC)
        {
            days_to_keep_data = 30;
        }

        public ExchangeRegDocViewModel(exchange_user_reg item, exchange_reg_doc item2)
            : base(Enums.MainObjectType.EXCHANGE_REG_DOC)
        {
            item_id = item.item_id;
            user_id = item.user_id;
            reg_id = item.reg_id;
            is_active = item.is_active;
            days_to_keep_data = item2.days_to_keep_data;
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Display(Name = "Код пользователя")]
        public long user_id { get; set; }

        [Display(Name = "Код регистрации")]
        public long reg_id { get; set; }

        [Display(Name = "Регистрация активна")]
        public bool is_active { get; set; }

        [Display(Name = "Регистрация активна")]
        public string is_active_str { get; set; }

        [Display(Name = "Сколько дней хранить документы (0 - не ограничено)")]
        public Nullable<int> days_to_keep_data { get; set; }
        

    }

}