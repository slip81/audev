﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeRegDocSalesService : IAuBaseService
    {
        //
    }

    public class ExchangeRegDocSalesService : AuBaseService, IExchangeRegDocSalesService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.exchange_reg_doc_sales.Where(ss => ss.reg_id == parent_id)
                .Select(ss => new ExchangeRegDocSalesViewModel()
                {
                    item_id = ss.item_id,
                    reg_id = ss.reg_id,
                    sales_id = ss.sales_id,
                    sales_name = ss.sales_name,
                    is_active = ss.is_active,
                    is_active_str = ss.is_active ? "Да" : "Нет",
                    email_for_docs = ss.email_for_docs,
                    login_name = ss.login_name,
                    sales_address = ss.sales_address,
                }
                );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.exchange_reg_doc_sales.Where(ss => ss.item_id == id)
                .Select(ss => new ExchangeRegDocSalesViewModel()
                {
                    item_id = ss.item_id,
                    reg_id = ss.reg_id,
                    sales_id = ss.sales_id,
                    sales_name = ss.sales_name,
                    is_active = ss.is_active,
                    is_active_str = ss.is_active ? "Да" : "Нет",
                    email_for_docs = ss.email_for_docs,
                    login_name = ss.login_name,
                    sales_address = ss.sales_address,
                }
                )
                .FirstOrDefault();
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeRegDocSalesViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeRegDocSalesViewModel doc_sale_edit = (ExchangeRegDocSalesViewModel)item;
            if (doc_sale_edit == null)
            {
                modelState.AddModelError("", "Не задана точка для обмена");
                return null;
            }

            var exchange_reg_doc_sales = dbContext.exchange_reg_doc_sales.Where(ss => ss.item_id == doc_sale_edit.item_id).FirstOrDefault();
            if (exchange_reg_doc_sales == null)
            {
                modelState.AddModelError("", "Не найдена торговая точка для обмена с кодом " + doc_sale_edit.item_id.ToString());
                return null;
            }
            modelBeforeChanges = new ExchangeRegDocSalesViewModel(exchange_reg_doc_sales);
            
            exchange_reg_doc_sales.is_active = doc_sale_edit.is_active;
            exchange_reg_doc_sales.email_for_docs = doc_sale_edit.email_for_docs;

            dbContext.SaveChanges();

            return new ExchangeRegDocSalesViewModel(exchange_reg_doc_sales);
        }
    }
}