﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using CabinetMvc.ViewModel.Util;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeRegDocSalesViewModel : AuBaseViewModel
    {
        public ExchangeRegDocSalesViewModel()
            : base(Enums.MainObjectType.EXCHANGE_REG_DOC_SALES)
        {
            
        }

        public ExchangeRegDocSalesViewModel(exchange_reg_doc_sales item)
            : base(Enums.MainObjectType.EXCHANGE_REG_DOC_SALES)
        {
            item_id = item.item_id;
            reg_id = item.reg_id;
            sales_id = item.sales_id;
            sales_name = item.sales_name;
            is_active = item.is_active;
            is_active_str = item.is_active ? "Да" : "Нет";
            email_for_docs = item.email_for_docs;
            login_name = item.login_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Display(Name = "Код регистрации")]
        public long reg_id { get; set; }

        [Display(Name = "Торговая точка")]
        public Nullable<int> sales_id { get; set; }

        [Display(Name = "Торговая точка")]
        [JsonIgnore()]
        public string sales_name { get; set; }

        [Display(Name = "Используется в обмене")]
        public bool is_active { get; set; }

        [Display(Name = "Используется в обмене")]
        [JsonIgnore()]
        public string is_active_str { get; set; }

        [Display(Name = "Эл. почта для отправки накладных")]
        public string email_for_docs { get; set; }

        [Display(Name = "Логин точки")]
        [JsonIgnore()]
        public string login_name { get; set; }

        [Display(Name = "Адрес точки")]
        [JsonIgnore()]
        public string sales_address { get; set; }

    }

}