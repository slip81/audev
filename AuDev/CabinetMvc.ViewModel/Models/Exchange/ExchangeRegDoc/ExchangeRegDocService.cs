﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Cabinet;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeRegDocService : IAuBaseService
    {
        AuBaseViewModel Insert_withSales(ExchangeRegDocViewModel item, IEnumerable<SalesViewModel> sales, ModelStateDictionary modelState);
    }

    public class ExchangeRegDocService : AuBaseService, IExchangeRegDocService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return    from eur in dbContext.exchange_user_reg
                      from er in dbContext.exchange_reg
                      from erp in dbContext.exchange_reg_doc                      
                      where eur.user_id == parent_id
                      && eur.reg_id == er.reg_id
                      && er.reg_type == (int)Enums.ExchangeRegType.DOC
                      && er.reg_id == erp.reg_id                      
                      select new ExchangeRegDocViewModel()
                      {  
                          is_active = eur.is_active,
                          item_id = eur.item_id,
                          reg_id = er.reg_id,
                          user_id = eur.user_id,                          
                          is_active_str = eur.is_active ? "Да" : "Нет",
                          days_to_keep_data = erp.days_to_keep_data,
                      };
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return (from eur in dbContext.exchange_user_reg
                   from er in dbContext.exchange_reg
                   from erp in dbContext.exchange_reg_doc                   
                   where eur.item_id == id
                   && eur.reg_id == er.reg_id
                   && er.reg_type == (int)Enums.ExchangeRegType.DOC
                   && er.reg_id == erp.reg_id
                    select new ExchangeRegDocViewModel()
                   {
                       is_active = eur.is_active,
                       item_id = eur.item_id,
                       reg_id = er.reg_id,
                       user_id = eur.user_id,
                       is_active_str = eur.is_active ? "Да" : "Нет",
                       days_to_keep_data = erp.days_to_keep_data,
                   }
                   )
                   .FirstOrDefault();
        }

        public AuBaseViewModel Insert_withSales(ExchangeRegDocViewModel item, IEnumerable<SalesViewModel> sales, ModelStateDictionary modelState)
        {
            modelBeforeChanges = null;
            try
            {
                var res = xInsert_withSales(item, sales, modelState);
                ToLog("Добавление", (long)Enums.LogEventType.CABINET, new LogObject(res != null ? res : item));
                return res;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(item));
                return null;
            }
        }

        private AuBaseViewModel xInsert_withSales(ExchangeRegDocViewModel item, IEnumerable<SalesViewModel> sales, ModelStateDictionary modelState)
        {
            ExchangeRegDocViewModel reg_add = item;
            if (reg_add == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты регистрации");
                return null;
            }

            var existing_reg = (from eur in dbContext.exchange_user_reg
                                from er in dbContext.exchange_reg
                                from erp in dbContext.exchange_reg_doc
                                where eur.reg_id == er.reg_id
                                && eur.user_id == reg_add.user_id
                                && er.reg_type == (int)Enums.ExchangeRegType.DOC
                                && er.reg_id == erp.reg_id                                
                                select eur)
                                .FirstOrDefault();
            if (existing_reg != null)
            {
                modelState.AddModelError("", "На эту услугу уже есть регистрация у данного пользователя");
                return null;
            }


            exchange_reg exchange_reg = new exchange_reg();
            exchange_reg.reg_type = (int)Enums.ExchangeRegType.DOC;
            dbContext.exchange_reg.Add(exchange_reg);

            exchange_reg_doc exchange_reg_doc = new exchange_reg_doc();
            exchange_reg_doc.reg_id = exchange_reg.reg_id;
            exchange_reg_doc.days_to_keep_data = reg_add.days_to_keep_data;
            dbContext.exchange_reg_doc.Add(exchange_reg_doc);

            exchange_user_reg exchange_user_reg = new exchange_user_reg();
            exchange_user_reg.user_id = reg_add.user_id;
            exchange_user_reg.reg_id = exchange_reg.reg_id;
            exchange_user_reg.is_active = reg_add.is_active;
            dbContext.exchange_user_reg.Add(exchange_user_reg);

            if ((sales != null) && (sales.Count() > 0))
            {
                foreach (var sale in sales)
                {
                    exchange_reg_doc_sales exchange_reg_doc_sales = new exchange_reg_doc_sales();
                    exchange_reg_doc_sales.email_for_docs = "";
                    exchange_reg_doc_sales.exchange_reg_doc = exchange_reg_doc;
                    exchange_reg_doc_sales.is_active = true;
                    exchange_reg_doc_sales.sales_id = sale.sales_id;
                    exchange_reg_doc_sales.sales_name = sale.name;
                    exchange_reg_doc_sales.login_name = sale.user_name;
                    exchange_reg_doc_sales.sales_address = sale.adress;
                    dbContext.exchange_reg_doc_sales.Add(exchange_reg_doc_sales);
                }
            }


            dbContext.SaveChanges();

            return new ExchangeRegDocViewModel(exchange_user_reg, exchange_reg_doc);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeRegDocViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeRegDocViewModel reg_edit = (ExchangeRegDocViewModel)item;
            if (reg_edit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты регистрации");
                return null;
            }

            var exchange_user_reg = dbContext.exchange_user_reg.Where(ss => ss.item_id == reg_edit.item_id).FirstOrDefault();
            if (exchange_user_reg == null)
            {
                modelState.AddModelError("", "Не найдена регистрация пользователя с кодом " + reg_edit.item_id.ToString());
                return null;
            }
            
            var exchange_reg_doc = (from er in dbContext.exchange_reg
                                        from erp in dbContext.exchange_reg_doc
                                        where er.reg_id == reg_edit.reg_id
                                        && er.reg_type == (int)Enums.ExchangeRegType.DOC
                                        && er.reg_id == erp.reg_id
                                        select erp)
                                        .FirstOrDefault();
            if (exchange_reg_doc == null)
            {
                modelState.AddModelError("", "Не найдена регистрация с кодом " + reg_edit.reg_id.ToString());
                return null;
            }
            modelBeforeChanges = new ExchangeRegDocViewModel(exchange_user_reg, exchange_reg_doc);

            exchange_reg_doc.days_to_keep_data = reg_edit.days_to_keep_data;
            exchange_user_reg.is_active = reg_edit.is_active;

            dbContext.SaveChanges();

            return new ExchangeRegDocViewModel(exchange_user_reg, exchange_reg_doc);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeRegDocViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((ExchangeRegDocViewModel)item).item_id;
            long reg_id = ((ExchangeRegDocViewModel)item).reg_id;

            exchange_user_reg exchange_user_reg = null;

            exchange_user_reg = dbContext.exchange_user_reg.Where(ss => ss.item_id == id).FirstOrDefault();
            if (exchange_user_reg == null)
            {
                modelState.AddModelError("serv_result", "Регистрация не найдена");
                return false;
            }


            dbContext.exchange_user_reg.Remove(exchange_user_reg);
            dbContext.exchange_reg_doc_sales.RemoveRange(dbContext.exchange_reg_doc_sales.Where(ss => ss.reg_id == reg_id));
            dbContext.exchange_reg_doc.Remove(dbContext.exchange_reg_doc.Where(ss => ss.reg_id == reg_id).FirstOrDefault());
            dbContext.exchange_reg.Remove(dbContext.exchange_reg.Where(ss => ss.reg_id == reg_id).FirstOrDefault());

            dbContext.SaveChanges();
            return true;
        }
    }
}