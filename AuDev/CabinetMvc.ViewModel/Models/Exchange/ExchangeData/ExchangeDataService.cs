﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeDataService : IAuBaseService
    {
        //
    }

    public class ExchangeDataService : AuBaseService, IExchangeDataService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.exchange_data.Where(ss => ss.exchange_id == id).Select(ss => new ExchangeDataViewModel()
            {
                exchange_id = ss.exchange_id,
                data_text = ss.data_text,
                data_binary = ss.data_binary,                
            }
            )
            .FirstOrDefault();
        }
    }
}