﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;


namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeDataViewModel : AuBaseViewModel
    {
        public ExchangeDataViewModel()
            : base(Enums.MainObjectType.EXCHANGE_DATA)
        {
            //
        }

        public ExchangeDataViewModel(exchange_data item)
            : base(Enums.MainObjectType.EXCHANGE_DATA)
        {
            exchange_id = item.exchange_id;
            data_text = item.data_text;            
            data_binary = item.data_binary;            
        }

        [Key()]
        [Display(Name = "Код")]
        public long exchange_id { get; set; }

        [Display(Name = "Даннные (текст)")]
        public string data_text { get; set; }

        [Display(Name = "Даннные (бинарные)")]
        public byte[] data_binary { get; set; }

        [Display(Name = "Даннные")]
        public string DataTextFromBinary { get { return GlobalUtil.GetString(this.data_binary); } }
    }
}