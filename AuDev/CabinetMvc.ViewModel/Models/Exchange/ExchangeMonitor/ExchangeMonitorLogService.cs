﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeMonitorLogService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_forMonitor(long reg_id, long ds_id, int exchange_direction);
    }

    public class ExchangeMonitorLogService : AuBaseService, IExchangeMonitorLogService
    {
        public IQueryable<AuBaseViewModel> GetList_forMonitor(long reg_id, long ds_id, int exchange_direction)
        {
            return from eu in dbContext.exchange_user
                   from eur in dbContext.exchange_user_reg
                   from erp in dbContext.exchange_reg_partner
                   from el in dbContext.exchange_log
                   where eu.user_id == eur.user_id
                   && eur.is_active
                   && eur.reg_id == erp.reg_id
                   // !!!
                   // todo: сравнение по reg_id (или sales_id и workplace_id)
                   && el.login == eu.login
                   //&& (((exchange_direction == 1) && (el.date_beg > erp.upload_last_date)) || ((exchange_direction != 1) && (el.date_beg > erp.download_last_date)))
                   && eur.reg_id == reg_id
                   && erp.ds_id == ds_id
                   && el.exchange_direction == exchange_direction
                   select new ExchangeMonitorLogViewModel()
                   {
                       log_id = el.log_id,
                       date_beg = el.date_beg,
                       date_end = el.date_end,
                       reg_id = el.reg_id,
                       login = el.login,
                       exchange_direction = el.exchange_direction,
                       exchange_item_id = el.exchange_item_id,
                       ds_id = el.ds_id,
                       exchange_id = el.exchange_id,
                       error_level = el.error_level,
                       mess = el.mess,
                       mess2 = el.mess2,
                   }
                   ;     
        }

    }
}