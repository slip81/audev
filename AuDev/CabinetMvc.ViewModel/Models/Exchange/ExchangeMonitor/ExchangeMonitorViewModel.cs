﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeMonitorViewModel : AuBaseViewModel
    {
        public ExchangeMonitorViewModel()
            : base(Enums.MainObjectType.EXCHANGE_MONITOR)
        {
            //
        }

        public ExchangeMonitorViewModel(vw_exchange_monitor item)
            : base(Enums.MainObjectType.EXCHANGE_MONITOR)
        {
            id = item.id;            
            user_id = item.user_id;
            login = item.login;
            workplace = item.workplace;
            client_id = item.client_id;
            cabinet_client_id = item.cabinet_client_id;
            cabinet_sales_id = item.cabinet_sales_id;
            cabinet_workplace_id = item.cabinet_workplace_id;
            client_name = item.client_name;
            sales_name = item.sales_name;
            workplace_name = item.workplace_name;
            reg_id = item.reg_id;
            ds_id = item.ds_id;
            is_prepared = item.is_prepared;
            upload_state = item.upload_state;
            download_state = item.download_state;
            upload_err_mess = item.upload_err_mess;
            download_err_mess = item.download_err_mess;
            upload_ok_date = item.upload_ok_date;
            download_ok_date = item.download_ok_date;
            prepared_date = item.prepared_date;
            upload_last_date = item.upload_last_date;
            download_last_date = item.download_last_date;
            upload_last_mess = item.upload_last_mess;
            download_last_mess = item.download_last_mess;
            download_ok_cnt = item.download_ok_cnt;
            download_warn_cnt = item.download_warn_cnt;
            download_err_cnt = item.download_err_cnt;
            upload_ok_cnt = item.upload_ok_cnt;
            upload_warn_cnt = item.upload_warn_cnt;
            upload_err_cnt = item.upload_err_cnt;
            ds_name = item.ds_name;
            user_reg_item_id = item.user_reg_item_id;
        }


        [Key()]
        [Display(Name="Код")]
        public long id { get; set; }

        [Display(Name = "Код пользователя")]
        public long user_id { get; set; }

        [Display(Name = "Логин")]
        public string login { get; set; }

        [Display(Name = "Раб. место")]
        public string workplace { get; set; }

        [Display(Name = "Код клиента")]
        public Nullable<long> client_id { get; set; }

        [Display(Name = "Код клиента (старый ЛК)")]
        public Nullable<long> cabinet_client_id { get; set; }

        [Display(Name = "Код ТТ (старый ЛК)")]
        public Nullable<long> cabinet_sales_id { get; set; }

        [Display(Name = "Код раб. места (старый ЛК)")]
        public Nullable<long> cabinet_workplace_id { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }

        [Display(Name = "Адрес")]
        public string sales_name { get; set; }

        [Display(Name = "Раб. место")]
        public string workplace_name { get; set; }

        [Display(Name = "Код регистрации")]
        public Nullable<long> reg_id { get; set; }

        [Display(Name = "Обмен с")]
        public Nullable<long> ds_id { get; set; }

        [Display(Name = "Обмен с")]
        public string ds_name { get; set; }

        [Display(Name = "Настройки завершены")]
        public bool is_prepared { get; set; }

        [Display(Name = "Состояние отдачи")]
        public int upload_state { get; set; }

        [Display(Name = "Состояние приема")]
        public int download_state { get; set; }

        [Display(Name = "Последняя ошибка отдачи")]
        public string upload_err_mess { get; set; }

        [Display(Name = "Последняя ошибка приема")]
        public string download_err_mess { get; set; }

        [Display(Name = "Дата успешного приема")]
        public Nullable<System.DateTime> upload_ok_date { get; set; }

        [Display(Name = "Дата успешной отдачи")]
        public Nullable<System.DateTime> download_ok_date { get; set; }

        [Display(Name = "Дата завершения настроек")]
        public Nullable<System.DateTime> prepared_date { get; set; }

        [Display(Name = "Дата последнего приема")]
        public Nullable<System.DateTime> upload_last_date { get; set; }

        [Display(Name = "Дата последней отдачи")]
        public Nullable<System.DateTime> download_last_date { get; set; }

        [Display(Name = "Сообщение последнего приема")]
        public string upload_last_mess { get; set; }

        [Display(Name = "Сообщение последней отдачи")]
        public string download_last_mess { get; set; }

        [Display(Name = "download_ok_cnt")]
        public Nullable<int> download_ok_cnt { get; set; }

        [Display(Name = "download_warn_cnt")]
        public Nullable<int> download_warn_cnt { get; set; }

        [Display(Name = "download_err_cnt")]
        public Nullable<int> download_err_cnt { get; set; }

        [Display(Name = "upload_ok_cnt")]
        public Nullable<int> upload_ok_cnt { get; set; }

        [Display(Name = "upload_warn_cnt")]
        public Nullable<int> upload_warn_cnt { get; set; }

        [Display(Name = "upload_err_cnt")]
        public Nullable<int> upload_err_cnt { get; set; }

        [Display(Name = "user_reg_item_id")]
        public Nullable<long> user_reg_item_id { get; set; }

        [Display(Name = "Настройки завершены")]
        public string ImagePrepared { get { return is_prepared ? "monitor-green.png" : "monitor-red.png"; } }

        [Display(Name = "Прием")]
        public string ImageDownload { get { return is_prepared ? (download_state == 0 ? "monitor-green.png" : (download_state == 1 ? "monitor-yellow.png" : "monitor-red.png")) : "monitor-grey.png"; } }

        [Display(Name = "Отправка")]
        public string ImageUpload { get { return is_prepared ? (upload_state == 0 ? "monitor-green.png" : (upload_state == 1 ? "monitor-yellow.png" : "monitor-red.png")) : "monitor-grey.png"; } }

        [Display(Name = "Настройки завершены")]
        public string ImagePreparedHtml { get { return ImageDiv_Short(ImagePrepared, (prepared_date == null ? "настройки не завершены" : ((DateTime)prepared_date).ToString("dd.MM.yyyy HH:mm:ss"))); } }

        [Display(Name = "Прием")]
        public string ImageDownloadHtml { get { return is_prepared ? ImageDiv(ImageDownload, (download_last_date == null ? "" : ((DateTime)download_last_date).ToString("dd.MM.yyyy HH:mm:ss")), download_last_mess, download_ok_cnt, download_warn_cnt, download_err_cnt, 0) : ImageDiv_Short(ImageDownload, "настройки не завершены"); } }

        [Display(Name = "Отправка")]
        public string ImageUploadHtml { get { return is_prepared ? ImageDiv(ImageUpload, (upload_last_date == null ? "" : ((DateTime)upload_last_date).ToString("dd.MM.yyyy HH:mm:ss")), upload_last_mess, upload_ok_cnt, upload_warn_cnt, upload_err_cnt, 1) : ImageDiv_Short(ImageUpload, "настройки не завершены"); } }

        private string ImageDiv(string image_path, string text, string text2, int? ok_cnt, int? warn_cnt, int? err_cnt, int click_param)
        {

            return "<div style='display:table;'>"
                + "<div style='display:table-row; text-align:center;'>"
                + "<div class='col-md-no-padding' style='font-size: .8em;'>"
                + "<img src='/Images/New/" + image_path + "' alt='ОК' height='20' width='20' style='vertical-align:middle; cursor:pointer;' onclick='exchangeMonitor.onImageClick(this, " + click_param.ToString() + ", -1)'/>"
                + "<span>&nbsp;" + text + "</span>"
                + "</div>"
                + "</div>"
                + "<div style='display:table-row'>"
                + "<div class='col-md-no-padding' style='font-size: .8em;'>"
                + "<span title='" + text2 + "'>&nbsp;" + text2.CutToLengthWithDots(24) + "</span>"
                + "</div>"
                + "</div>"
                + "<div style='display:table-row;'>"
                + "<div class='col-md-no-padding' style='padding-top: 5px;'>"
                + "<div class='text-link' style='float:left !important; background-color:#80ff80; border: 1px solid #00cc00; cursor:pointer;' onclick='exchangeMonitor.onImageClick(this, " + click_param.ToString() + ", 0)'>" + ok_cnt.ToHtmlTextLinkExactLength(11) + "</div>"
                + "<div class='text-link' style='float:left !important; background-color:#ffff66; border: 1px solid #cccc00; cursor:pointer;' onclick='exchangeMonitor.onImageClick(this, " + click_param.ToString() + ", 1)'>" + warn_cnt.ToHtmlTextLinkExactLength(11) + "</div>"
                + "<div class='text-link' style='float:left !important; background-color:#ff6666; border: 1px solid #cc0000; cursor:pointer;' onclick='exchangeMonitor.onImageClick(this, " + click_param.ToString() + ", 2)'>" + err_cnt.ToHtmlTextLinkExactLength(11) + "</div>"                
                + "</div>"
                + "</div>"
                + "</div>"
                ;
        }

        private string ImageDiv_Short(string image_path, string text)
        {
            return "<div style='display:table;'>"
                + "<div style='display:table-row; text-align:center; '>"
                + "<div class='col-md-no-padding' style='font-size: .8em; '>"
                + "<img src='/Images/New/" + image_path + "' alt='ОК' height='20' width='20' style='vertical-align:middle;'/>"
                + (String.IsNullOrEmpty(text) ? "" : "<span>&nbsp;" + text + "</span>")
                + "</div>"
                + "</div>"
                + "</div>"
                ;
        }     

    }
}