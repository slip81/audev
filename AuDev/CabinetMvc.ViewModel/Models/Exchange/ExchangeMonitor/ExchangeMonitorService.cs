﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeMonitorService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_byClientAndPartner(long parent_id, long ds_id, bool errors_only);
    }

    public class ExchangeMonitorService : AuBaseService, IExchangeMonitorService
    {
        public IQueryable<AuBaseViewModel> GetList_byClientAndPartner(long parent_id, long ds_id, bool errors_only)
        {
            return dbContext.vw_exchange_monitor
                .Where(ss => 
                    ss.client_id == parent_id && ss.ds_id == ds_id
                    && (((errors_only) && ((ss.upload_state != 0) || (ss.download_state != 0))) || (!errors_only))
                    )
                .Select(ss => new ExchangeMonitorViewModel()
                      {
                        id = ss.id,  
                        user_id = ss.user_id,
                        login = ss.login,
                        workplace = ss.workplace,
                        client_id = ss.client_id,
                        cabinet_client_id = ss.cabinet_client_id,
                        cabinet_sales_id = ss.cabinet_sales_id,
                        cabinet_workplace_id = ss.cabinet_workplace_id,
                        client_name = ss.client_name,
                        sales_name = ss.sales_name,
                        workplace_name = ss.workplace_name,
                        reg_id = ss.reg_id,
                        ds_id = ss.ds_id,                        
                        is_prepared = ss.is_prepared,
                        upload_state = ss.upload_state,
                        download_state = ss.download_state,
                        upload_err_mess = ss.upload_err_mess,
                        download_err_mess = ss.download_err_mess,
                        upload_ok_date = ss.upload_ok_date,
                        download_ok_date = ss.download_ok_date,
                        prepared_date = ss.prepared_date,
                        upload_last_date = ss.upload_last_date,
                        download_last_date = ss.download_last_date,
                        upload_last_mess = ss.upload_last_mess,
                        download_last_mess = ss.download_last_mess,
                        download_ok_cnt = ss.download_ok_cnt,
                        download_warn_cnt = ss.download_warn_cnt,
                        download_err_cnt = ss.download_err_cnt,
                        upload_ok_cnt = ss.upload_ok_cnt,
                        upload_warn_cnt = ss.upload_warn_cnt,
                        upload_err_cnt = ss.upload_err_cnt,
                        ds_name = ss.ds_name,
                        user_reg_item_id = ss.user_reg_item_id,
                      });
        }
    }
}