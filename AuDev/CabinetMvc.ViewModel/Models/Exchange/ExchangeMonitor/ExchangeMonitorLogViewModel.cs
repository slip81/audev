﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeMonitorLogViewModel : AuBaseViewModel
    {
        public ExchangeMonitorLogViewModel()
            : base(Enums.MainObjectType.EXCHANGE_MONITOR_LOG)
        {
            //
        }

        public ExchangeMonitorLogViewModel(exchange_log item)
            : base(Enums.MainObjectType.EXCHANGE_MONITOR_LOG)
        {
            log_id = item.log_id;
            date_beg = item.date_beg;
            date_end = item.date_end;
            reg_id = item.reg_id;
            login = item.login;
            exchange_direction = item.exchange_direction;
            exchange_item_id = item.exchange_item_id;
            ds_id = item.ds_id;
            exchange_id = item.exchange_id;
            error_level = item.error_level;
            mess = item.mess;
            mess2 = item.mess2;
        }


        [Key()]
        [Display(Name = "Код")]
        public long log_id { get; set; }

        [Display(Name = "Дата-время")]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "date_end")]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "reg_id")]
        public Nullable<long> reg_id { get; set; }

        [Display(Name = "login")]
        public string login { get; set; }

        [Display(Name = "exchange_direction")]
        public int exchange_direction { get; set; }

        [Display(Name = "exchange_item_id")]
        public int exchange_item_id { get; set; }

        [Display(Name = "ds_id")]
        public Nullable<long> ds_id { get; set; }

        [Display(Name = "exchange_id")]
        public Nullable<long> exchange_id { get; set; }

        [Display(Name = "error_level")]
        public int error_level { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }

        [Display(Name = "mess2")]
        public string mess2 { get; set; }
    }
}