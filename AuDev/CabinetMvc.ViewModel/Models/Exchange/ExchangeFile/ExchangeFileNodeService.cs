﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;


namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeFileNodeService : IAuBaseService
    {
        IQueryable<ExchangeFileNodeViewModel> GetList_Sprav();
    }

    public class ExchangeFileNodeService : AuBaseService, IExchangeFileNodeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_exchange_file_node
               .ProjectTo<ExchangeFileNodeViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_exchange_file_node
                .Where(ss => ss.node_id == id)
                .ProjectTo<ExchangeFileNodeViewModel>()
                .FirstOrDefault()
                ;
        }

        public IQueryable<ExchangeFileNodeViewModel> GetList_Sprav()
        {
            if (currUser.IsAdmin)
            {
                return dbContext.vw_exchange_file_node
                    .Select(ss => new ExchangeFileNodeViewModel()
                    {
                        client_id = ss.client_id,
                        client_name = ss.client_name,
                    }
                    )
                    .Distinct();
            } 
            else
            {
                List<ExchangeFileNodeViewModel> res = new List<ExchangeFileNodeViewModel>() 
                { 
                    new ExchangeFileNodeViewModel() { client_id = (int)currUser.CabClientId, client_name = currUser.clientInfo.client_name, }
                };
                
                return res.AsQueryable();                
            }
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeFileNodeViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeFileNodeViewModel itemAdd = (ExchangeFileNodeViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты узла");
                return null;
            }

            exchange_file_node exchange_file_node_existing = dbContext.exchange_file_node.Where(ss => ss.node_name.Trim().ToLower().Equals(itemAdd.node_name.Trim().ToLower())).FirstOrDefault();
            if (exchange_file_node_existing != null)
            {
                modelState.AddModelError("", "Уже создан такой узел");
                return null;
            }

            exchange_file_node exchange_file_node = new exchange_file_node();
            ModelMapper.Map<ExchangeFileNodeViewModel, exchange_file_node>(itemAdd, exchange_file_node);
            dbContext.exchange_file_node.Add(exchange_file_node);

            if (!String.IsNullOrEmpty(itemAdd.node_item_get_list))
            {
                var deserGetList = JsonConvert.DeserializeObject<List<ExchangeFileNodeItemViewModel>>(itemAdd.node_item_get_list);
                if ((deserGetList != null) && (deserGetList.Count > 0))
                {
                    foreach (var deserGet in deserGetList)
                    {
                        exchange_file_node_item exchange_file_node_item = new exchange_file_node_item();
                        exchange_file_node_item.exchange_item_id = deserGet.exchange_item_id;
                        exchange_file_node_item.file_name_mask = deserGet.file_name_mask;
                        exchange_file_node_item.node_id = exchange_file_node.node_id;
                        dbContext.exchange_file_node_item.Add(exchange_file_node_item);
                    }
                }
            }

            if (!String.IsNullOrEmpty(itemAdd.node_item_send_list))
            {
                var deserSendList = JsonConvert.DeserializeObject<List<ExchangeFileNodeItemViewModel>>(itemAdd.node_item_send_list);
                if ((deserSendList != null) && (deserSendList.Count > 0))
                {
                    foreach (var deserSend in deserSendList)
                    {
                        exchange_file_node_item exchange_file_node_item = new exchange_file_node_item();
                        exchange_file_node_item.exchange_item_id = deserSend.exchange_item_id;
                        exchange_file_node_item.file_name_mask = deserSend.file_name_mask;
                        exchange_file_node_item.node_id = exchange_file_node.node_id;
                        dbContext.exchange_file_node_item.Add(exchange_file_node_item);
                    }
                }
            }

            dbContext.SaveChanges();

            ExchangeFileNodeViewModel res = new ExchangeFileNodeViewModel();
            ModelMapper.Map<exchange_file_node, ExchangeFileNodeViewModel>(exchange_file_node, res);

            return res;
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeFileNodeViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeFileNodeViewModel itemEdit = (ExchangeFileNodeViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты узла");
                return null;
            }

            exchange_file_node exchange_file_node_existing = dbContext.exchange_file_node.Where(ss => ss.node_id != itemEdit.node_id && ss.node_name.Trim().ToLower().Equals(itemEdit.node_name.Trim().ToLower())).FirstOrDefault();
            if (exchange_file_node_existing != null)
            {
                modelState.AddModelError("", "Уже создан такой узел");
                return null;
            }

            exchange_file_node exchange_file_node = dbContext.exchange_file_node.Where(ss => ss.node_id == itemEdit.node_id).FirstOrDefault();
            if (exchange_file_node == null)
            {
                modelState.AddModelError("", "Не найден узел " + itemEdit.node_name);
                return null;
            }

            ExchangeFileNodeViewModel oldModel = new ExchangeFileNodeViewModel();
            ModelMapper.Map<exchange_file_node, ExchangeFileNodeViewModel>(exchange_file_node, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<ExchangeFileNodeViewModel, exchange_file_node>(itemEdit, exchange_file_node);

            dbContext.exchange_file_node_item.RemoveRange(dbContext.exchange_file_node_item.Where(ss => ss.node_id == itemEdit.node_id));

            if (!String.IsNullOrEmpty(itemEdit.node_item_get_list))
            {
                var deserGetList = JsonConvert.DeserializeObject<List<ExchangeFileNodeItemViewModel>>(itemEdit.node_item_get_list);
                if ((deserGetList != null) && (deserGetList.Count > 0))
                {
                    foreach (var deserGet in deserGetList)
                    {
                        exchange_file_node_item exchange_file_node_item = new exchange_file_node_item();
                        exchange_file_node_item.exchange_item_id = deserGet.exchange_item_id;
                        exchange_file_node_item.file_name_mask = deserGet.file_name_mask;
                        exchange_file_node_item.node_id = exchange_file_node.node_id;
                        dbContext.exchange_file_node_item.Add(exchange_file_node_item);
                    }
                }
            }

            if (!String.IsNullOrEmpty(itemEdit.node_item_send_list))
            {
                var deserSendList = JsonConvert.DeserializeObject<List<ExchangeFileNodeItemViewModel>>(itemEdit.node_item_send_list);
                if ((deserSendList != null) && (deserSendList.Count > 0))
                {
                    foreach (var deserSend in deserSendList)
                    {
                        exchange_file_node_item exchange_file_node_item = new exchange_file_node_item();
                        exchange_file_node_item.exchange_item_id = deserSend.exchange_item_id;
                        exchange_file_node_item.file_name_mask = deserSend.file_name_mask;
                        exchange_file_node_item.node_id = exchange_file_node.node_id;
                        dbContext.exchange_file_node_item.Add(exchange_file_node_item);
                    }
                }
            }

            dbContext.SaveChanges();

            ExchangeFileNodeViewModel res = new ExchangeFileNodeViewModel();
            ModelMapper.Map<exchange_file_node, ExchangeFileNodeViewModel>(exchange_file_node, res);

            return res;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeFileNodeViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((ExchangeFileNodeViewModel)item).node_id;

            exchange_file_node exchange_file_node = dbContext.exchange_file_node.Where(ss => ss.node_id == id).FirstOrDefault();
            if (exchange_file_node == null)
            {
                modelState.AddModelError("serv_result", "Не найден узел");
                return false;
            }

            dbContext.exchange_file_node_item.RemoveRange(dbContext.exchange_file_node_item.Where(ss => ss.node_id == id));
            dbContext.exchange_file_node.Remove(exchange_file_node);
            dbContext.SaveChanges();
            return true;
        }

    }
}