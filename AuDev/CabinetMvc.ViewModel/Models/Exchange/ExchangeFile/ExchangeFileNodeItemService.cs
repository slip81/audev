﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeFileNodeItemService : IAuBaseService
    {
        IQueryable<ExchangeFileNodeItemViewModel> GetList_New(long ds_id, bool forSend, bool forGet);
        IQueryable<ExchangeFileNodeItemViewModel> GetList_Exisiting(int node_id, bool forSend, bool forGet);
    }

    public class ExchangeFileNodeItemService : AuBaseService, IExchangeFileNodeItemService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.exchange_file_node_item       
                .Where(ss => ss.node_id == parent_id)
                .ProjectTo<ExchangeFileNodeItemViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.exchange_file_node_item
                .Where(ss => ss.item_id == id)
                .ProjectTo<ExchangeFileNodeItemViewModel>()
                .FirstOrDefault()
                ;
        }

        public IQueryable<ExchangeFileNodeItemViewModel> GetList_New(long ds_id, bool forSend, bool forGet)
        {
            return dbContext.datasource_exchange_item
                .Where(ss => ss.ds_id == ds_id 
                    && ss.item_id > 0
                    && ss.for_get == forGet
                    && ss.for_send == forSend
                )
                .Select(ss => new ExchangeFileNodeItemViewModel(){
                    exchange_item_id = ss.item_id,
                    exchange_item_name = ss.exchange_item.item_name,
                    file_name_mask = ss.file_name_mask,
                    item_id = 0,
                    node_id = 0,
                })
                ;
        }

        public IQueryable<ExchangeFileNodeItemViewModel> GetList_Exisiting(int node_id, bool forSend, bool forGet)
        {
            return (from t1 in dbContext.exchange_file_node_item
                    from t2 in dbContext.datasource_exchange_item
                    from t3 in dbContext.exchange_file_node
                       where t1.node_id == t3.node_id
                       && t1.exchange_item_id == t2.item_id
                       && t2.ds_id == t3.partner_id
                       && t1.node_id == node_id
                       && t2.for_get == forGet
                       && t2.for_send == forSend
                       select t1)
                      .ProjectTo<ExchangeFileNodeItemViewModel>();
        }


    }
}