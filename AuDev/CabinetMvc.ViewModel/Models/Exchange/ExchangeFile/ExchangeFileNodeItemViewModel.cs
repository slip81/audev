﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeFileNodeItemViewModel : AuBaseViewModel
    {
        public ExchangeFileNodeItemViewModel()
            : base(Enums.MainObjectType.EXCHANGE_FILE_NODE_ITEM)
        {
            //exchange_file_node_item
        }

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Узел")]
        public int node_id { get; set; }

        [Display(Name = "Тип файла")]
        public int exchange_item_id { get; set; }

        [Display(Name = "Тип файла")]
        public string exchange_item_name { get; set; }

        [Display(Name = "Имя файла (маска)")]
        public string file_name_mask { get; set; }
    }
}