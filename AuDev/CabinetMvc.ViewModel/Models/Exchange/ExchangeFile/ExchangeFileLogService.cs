﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeFileLogService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_byFilter(int? ds_id, int? client_id, bool? error_only);
    }

    public class ExchangeFileLogService : AuBaseService, IExchangeFileLogService
    {
        public IQueryable<AuBaseViewModel> GetList_byFilter(int? ds_id, int? client_id, bool? error_only)
        {
            /*
            return dbContext.vw_exchange_file_log
                .Where(ss => ((ds_id.HasValue && ss.partner_id == ds_id) || (!ds_id.HasValue)) 
                    && ((client_id.HasValue && ss.client_id == client_id) || (!client_id.HasValue))
                    && ((error_only.HasValue && error_only == true && ((!ss.days_cnt.HasValue) || (ss.days_cnt > 3))) || (!error_only.HasValue) || (error_only == false))
                    )
               .ProjectTo<ExchangeFileLogViewModel>();    
            */
            var query = dbContext.vw_exchange_file_log.Where(ss => 1==1);
            if (ds_id.HasValue)
                query = query.Where(ss => ss.partner_id == ds_id);
            if (client_id.HasValue)
                query = query.Where(ss => ss.client_id == client_id);
            if ((error_only.HasValue) && (error_only == true))
                query = query.Where(ss => ((!ss.days_cnt.HasValue) || (ss.days_cnt > 3)));

            return query.ProjectTo<ExchangeFileLogViewModel>();
        }        

    }
}