﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeFileLogViewModel : AuBaseViewModel
    {

        public ExchangeFileLogViewModel()
            : base(Enums.MainObjectType.EXCHANGE_FILE_LOG)
        {
            //vw_exchange_file_log
        }

        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }

        [Display(Name = "Узел")]
        public int node_id { get; set; }

        [Display(Name = "Партнер")]
        public long partner_id { get; set; }

        [Display(Name = "Организация")]
        public int client_id { get; set; }

        [Display(Name = "Торговая точка")]
        public int sales_id { get; set; }

        [Display(Name = "Партнер")]
        public string partner_name { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_address { get; set; }

        [Display(Name = "Узел")]
        public string node_name { get; set; }

        [Display(Name = "Тип файла")]
        public int exchange_item_id { get; set; }

        [Display(Name = "Тип файла")]
        public string item_name { get; set; }

        [Display(Name = "Последний файл")]
        public string filename_last { get; set; }

        [Display(Name = "Последний файл (период)")]
        public string period_last { get; set; }

        [Display(Name = "Предпоследний файл")]
        public string filename_prev { get; set; }

        [Display(Name = "Предпоследний файл (период)")]
        public string period_prev { get; set; }

        [Display(Name = "Прошло дней")]
        public Nullable<int> days_cnt { get; set; }
    }
}