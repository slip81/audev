﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeFileNodeViewModel : AuBaseViewModel
    {
        public ExchangeFileNodeViewModel()
            : base(Enums.MainObjectType.EXCHANGE_FILE_NODE)
        {
            //exchange_file_node
            //vw_exchange_file_node
        }

        [Key()]
        [Display(Name = "Код")]
        public int node_id { get; set; }

        [Display(Name = "Партнер")]
        [Required(ErrorMessage = "Не задан партнер")]
        public long partner_id { get; set; }

        [Display(Name = "Организация")]
        [Required(ErrorMessage = "Не задана организация")]
        public int client_id { get; set; }

        [Display(Name = "Торговая точка")]
        [Required(ErrorMessage = "Не задана торговая точка")]
        public int sales_id { get; set; }

        [Display(Name = "Имя точки для обмена")]
        [Required(ErrorMessage = "Не задано имя точки")]
        public string node_name { get; set; }

        [Display(Name = "Организация")]
        public string client_name { get; set; }

        [Display(Name = "Торговая точка")]
        public string sales_address { get; set; }

        [Display(Name = "Партнер")]
        public string partner_name { get; set; }

        [Display(Name = "node_item_get_list")]        
        public string node_item_get_list { get; set; }

        [Display(Name = "node_item_send_list")]
        public string node_item_send_list { get; set; }
    }
}