﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeRegPartnerService : IAuBaseService
    {
        //
    }

    public class ExchangeRegPartnerService : AuBaseService, IExchangeRegPartnerService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return    from eur in dbContext.exchange_user_reg
                      from er in dbContext.exchange_reg
                      from erp in dbContext.exchange_reg_partner
                      from ds in dbContext.datasource
                      where eur.user_id == parent_id
                      && eur.reg_id == er.reg_id
                      && er.reg_type == (int)Enums.ExchangeRegType.PARTNER
                      && er.reg_id == erp.reg_id
                      && erp.ds_id == ds.ds_id
                      select new ExchangeRegPartnerViewModel()
                      {
                          crt_date = erp.crt_date,
                          download_url = erp.download_url,
                          ds_id = erp.ds_id,
                          ds_name = ds.ds_name,
                          format_type = erp.format_type,
                          is_active = eur.is_active,
                          item_id = eur.item_id,
                          login_name = erp.login,
                          password = erp.password,
                          reg_code = erp.reg_code,
                          reg_id = er.reg_id,
                          reg_name = erp.reg_name,
                          send_all = erp.send_all == 1 ? true : false,
                          transport_type = erp.transport_type,
                          upload_url = erp.upload_url,
                          user_id = eur.user_id,
                          send_all_str = erp.send_all == 1 ? "Да" : "Нет",
                          is_active_str = eur.is_active ? "Да" : "Нет",
                          use_sales_prefix = erp.use_sales_prefix == 1 ? true : false,
                          use_sales_prefix_str = erp.use_sales_prefix == 1 ? "Да" : "Нет",
                          download_arc_type = erp.download_arc_type,
                          upload_arc_type = erp.upload_arc_type,
                          pop3_address = erp.pop3_address,
                          pop3_port = erp.pop3_port,
                          pop3_use_ssl = erp.pop3_use_ssl,
                          upload_equals_download = erp.upload_equals_download,
                          upload_url_alias = erp.upload_url_alias,
                          days_cnt_for_mov = erp.days_cnt_for_mov,
                          upload_confirm_address = erp.upload_confirm_address,
                          is_prepared = erp.is_prepared,
                          upload_state = erp.upload_state,
                          download_state = erp.download_state,
                          upload_last_date = erp.upload_last_date,
                          download_last_date = erp.download_last_date,
                          upload_last_mess = erp.upload_last_mess,
                          download_last_mess = erp.download_last_mess,
                          prepared_date = erp.prepared_date,
                      };
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return (from eur in dbContext.exchange_user_reg
                   from er in dbContext.exchange_reg
                   from erp in dbContext.exchange_reg_partner
                   from ds in dbContext.datasource
                   where eur.item_id == id
                   && eur.reg_id == er.reg_id
                   && er.reg_type == (int)Enums.ExchangeRegType.PARTNER
                   && er.reg_id == erp.reg_id
                   && erp.ds_id == ds.ds_id
                   select new ExchangeRegPartnerViewModel()
                   {
                       crt_date = erp.crt_date,
                       download_url = erp.download_url,
                       ds_id = erp.ds_id,
                       ds_name = ds.ds_name,
                       format_type = erp.format_type,
                       is_active = eur.is_active,
                       item_id = eur.item_id,
                       login_name = erp.login,
                       password = erp.password,
                       reg_code = erp.reg_code,
                       reg_id = er.reg_id,
                       reg_name = erp.reg_name,
                       send_all = erp.send_all == 1 ? true : false,
                       transport_type = erp.transport_type,
                       upload_url = erp.upload_url,
                       user_id = eur.user_id,
                       send_all_str = erp.send_all == 1 ? "Да" : "Нет",
                       is_active_str = eur.is_active ? "Да" : "Нет",
                       use_sales_prefix = erp.use_sales_prefix == 1 ? true : false,
                       use_sales_prefix_str = erp.use_sales_prefix == 1 ? "Да" : "Нет",
                       download_arc_type = erp.download_arc_type,
                       upload_arc_type = erp.upload_arc_type,
                       pop3_address = erp.pop3_address,
                       pop3_port = erp.pop3_port,
                       pop3_use_ssl = erp.pop3_use_ssl,
                       upload_equals_download = erp.upload_equals_download,
                       upload_url_alias = erp.upload_url_alias,
                       days_cnt_for_mov = erp.days_cnt_for_mov,
                       upload_confirm_address = erp.upload_confirm_address,
                       is_prepared = erp.is_prepared,
                       upload_state = erp.upload_state,
                       download_state = erp.download_state,
                       upload_last_date = erp.upload_last_date,
                       download_last_date = erp.download_last_date,
                       upload_last_mess = erp.upload_last_mess,
                       download_last_mess = erp.download_last_mess,
                       prepared_date = erp.prepared_date,
                   }
                   )
                   .FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeRegPartnerViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeRegPartnerViewModel reg_add = (ExchangeRegPartnerViewModel)item;
            if (
                (reg_add == null)
                ||
                (reg_add.ds_id.GetValueOrDefault(0) <= 0)
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты регистрации");
                return null;
            }

            var existing_reg = (from eur in dbContext.exchange_user_reg
                                from er in dbContext.exchange_reg
                                from erp in dbContext.exchange_reg_partner
                                where eur.reg_id == er.reg_id
                                && eur.user_id == reg_add.user_id
                                && er.reg_type == (int)Enums.ExchangeRegType.PARTNER
                                && er.reg_id == erp.reg_id
                                && erp.ds_id == reg_add.ds_id
                                select eur)
                                 .FirstOrDefault();
            if (existing_reg != null)
            {
                modelState.AddModelError("", "На этого партнера уже есть регистрация у данного пользователя");
                return null;
            }

            DateTime? now = DateTime.Now;

            exchange_reg exchange_reg = new exchange_reg();
            exchange_reg.reg_type = (int)Enums.ExchangeRegType.PARTNER;
            dbContext.exchange_reg.Add(exchange_reg);

            exchange_reg_partner exchange_reg_partner = new exchange_reg_partner();
            exchange_reg_partner.reg_id = exchange_reg.reg_id;
            exchange_reg_partner.crt_date = DateTime.Now;
            exchange_reg_partner.ds_id = reg_add.ds_id;
            exchange_reg_partner.reg_code = reg_add.reg_code;
            exchange_reg_partner.reg_name = reg_add.reg_name;            
            exchange_reg_partner.format_type = reg_add.format_type;
            exchange_reg_partner.use_sales_prefix = reg_add.use_sales_prefix ? 1 : 0;
            exchange_reg_partner.upload_arc_type = reg_add.upload_arc_type;
            exchange_reg_partner.download_arc_type = reg_add.download_arc_type;
            exchange_reg_partner.send_all = reg_add.send_all ? 1 : 0;
            exchange_reg_partner.days_cnt_for_mov = reg_add.days_cnt_for_mov;
            exchange_reg_partner.upload_confirm_address = reg_add.upload_confirm_address;
            exchange_reg_partner.is_prepared = reg_add.is_prepared;
            exchange_reg_partner.prepared_date = reg_add.is_prepared ? now : null;
            
            exchange_reg_partner.transport_type = reg_add.transport_type;
            switch ((Enums.ExchangeTransport)reg_add.transport_type)
            {
                case Enums.ExchangeTransport.FTP:
                    reg_add.login_name = reg_add.login_ftp;
                    reg_add.password = reg_add.password_ftp;
                    reg_add.download_url = reg_add.download_url_ftp;
                    reg_add.upload_equals_download = false;
                    reg_add.upload_url = reg_add.upload_url_ftp;                    
                    reg_add.upload_url_alias = reg_add.upload_url_ftp;
                    reg_add.pop3_address = "";
                    reg_add.pop3_port = null;
                    reg_add.pop3_use_ssl = false;
                    break;
                case Enums.ExchangeTransport.EMAIL:
                    reg_add.login_name = "";
                    reg_add.password = reg_add.password_email;
                    reg_add.download_url = reg_add.download_url_email;
                    reg_add.upload_equals_download = reg_add.upload_equals_download_email;
                    reg_add.upload_url = reg_add.upload_equals_download_email ? reg_add.download_url_email : reg_add.upload_url_email;
                    reg_add.upload_url_alias = reg_add.upload_equals_download_email ? reg_add.download_url_email : reg_add.upload_url_alias_email;
                    reg_add.pop3_address = reg_add.pop3_address_email;
                    reg_add.pop3_port = reg_add.pop3_port_email;
                    reg_add.pop3_use_ssl = reg_add.pop3_use_ssl_email;
                    break;
                default:
                    modelState.AddModelError("", "Непредусмотренный тип транспорта " + reg_add.transport_type.ToString());
                    return null;
            }
            
            exchange_reg_partner.login = reg_add.login_name;
            exchange_reg_partner.password = reg_add.password;
            exchange_reg_partner.download_url = reg_add.download_url;
            exchange_reg_partner.upload_equals_download = reg_add.upload_equals_download;
            exchange_reg_partner.upload_url = reg_add.upload_url;
            exchange_reg_partner.upload_url_alias = reg_add.upload_url_alias;
            exchange_reg_partner.pop3_address = reg_add.pop3_address;
            exchange_reg_partner.pop3_port = reg_add.pop3_port;
            exchange_reg_partner.pop3_use_ssl = reg_add.pop3_use_ssl;

            dbContext.exchange_reg_partner.Add(exchange_reg_partner);

            exchange_user_reg exchange_user_reg = new exchange_user_reg();
            exchange_user_reg.user_id = reg_add.user_id;
            exchange_user_reg.reg_id = exchange_reg.reg_id;
            exchange_user_reg.is_active = reg_add.is_active;
            dbContext.exchange_user_reg.Add(exchange_user_reg);

            var new_items = dbContext.datasource_exchange_item
                .Where(ss => ss.ds_id == exchange_reg_partner.ds_id && ((ss.for_get) || (ss.for_send)))
                .ToList();
            if (new_items != null) 
            {
                foreach (var new_item in new_items)
                {
                    dbContext.exchange_reg_partner_item.Add(new exchange_reg_partner_item() 
                    { 
                        days_cnt_for_wait = 1,
                        exchange_item_id = new_item.item_id,
                        is_required = false,
                        is_download_to_client = true,
                        exchange_reg = exchange_reg,
                    }
                    );
                }
            }

            dbContext.SaveChanges();

            return new ExchangeRegPartnerViewModel(exchange_user_reg, exchange_reg_partner);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeRegPartnerViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeRegPartnerViewModel reg_edit = (ExchangeRegPartnerViewModel)item;
            if (
                (reg_edit == null)
                ||
                (reg_edit.ds_id.GetValueOrDefault(0) <= 0)
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты регистрации");
                return null;
            }

            var exchange_user_reg = dbContext.exchange_user_reg.Where(ss => ss.item_id == reg_edit.item_id).FirstOrDefault();
            if (exchange_user_reg == null)
            {
                modelState.AddModelError("", "Не найдена регистрация пользователя с кодом " + reg_edit.item_id.ToString());
                return null;
            }

            //var exchange_reg = dbContext.exchange_reg.Where(ss => ss.reg_id == reg_edit.reg_id).FirstOrDefault();
            var exchange_reg_partner = (from er in dbContext.exchange_reg
                                        from erp in dbContext.exchange_reg_partner
                                        where er.reg_id == reg_edit.reg_id
                                        && er.reg_type == (int)Enums.ExchangeRegType.PARTNER
                                        && er.reg_id == erp.reg_id
                                        select erp)
                                        .FirstOrDefault();
            if (exchange_reg_partner == null)
            {
                modelState.AddModelError("", "Не найдена регистрация с кодом " + reg_edit.reg_id.ToString());
                return null;
            }

            modelBeforeChanges = new ExchangeRegPartnerViewModel(exchange_user_reg, exchange_reg_partner);

            DateTime? now = DateTime.Now;
                        
            exchange_reg_partner.ds_id = reg_edit.ds_id;
            exchange_reg_partner.reg_code = reg_edit.reg_code;
            exchange_reg_partner.reg_name = reg_edit.reg_name;
            exchange_reg_partner.format_type = reg_edit.format_type;
            exchange_reg_partner.use_sales_prefix = reg_edit.use_sales_prefix ? 1 : 0;
            exchange_reg_partner.upload_arc_type = reg_edit.upload_arc_type;
            exchange_reg_partner.download_arc_type = reg_edit.download_arc_type;
            exchange_reg_partner.send_all = reg_edit.send_all ? 1 : 0;
            exchange_reg_partner.days_cnt_for_mov = reg_edit.days_cnt_for_mov;
            exchange_reg_partner.upload_confirm_address = reg_edit.upload_confirm_address;
            exchange_reg_partner.is_prepared = reg_edit.is_prepared;
            exchange_reg_partner.prepared_date = reg_edit.is_prepared ? now : null;

            exchange_reg_partner.transport_type = reg_edit.transport_type;
            switch ((Enums.ExchangeTransport)reg_edit.transport_type)
            {
                case Enums.ExchangeTransport.FTP:
                    reg_edit.login_name = reg_edit.login_ftp;
                    reg_edit.password = reg_edit.password_ftp;
                    reg_edit.download_url = reg_edit.download_url_ftp;
                    reg_edit.upload_equals_download = false;
                    reg_edit.upload_url = reg_edit.upload_url_ftp;
                    reg_edit.upload_url_alias = reg_edit.upload_url_ftp;
                    reg_edit.pop3_address = "";
                    reg_edit.pop3_port = null;
                    reg_edit.pop3_use_ssl = false;
                    break;
                case Enums.ExchangeTransport.EMAIL:
                    reg_edit.login_name = "";
                    reg_edit.password = reg_edit.password_email;
                    reg_edit.download_url = reg_edit.download_url_email;
                    reg_edit.upload_equals_download = reg_edit.upload_equals_download_email;
                    reg_edit.upload_url = reg_edit.upload_equals_download_email ? reg_edit.download_url_email : reg_edit.upload_url_email;
                    reg_edit.upload_url_alias = reg_edit.upload_equals_download_email ? reg_edit.download_url_email : reg_edit.upload_url_alias_email;
                    reg_edit.pop3_address = reg_edit.pop3_address_email;
                    reg_edit.pop3_port = reg_edit.pop3_port_email;
                    reg_edit.pop3_use_ssl = reg_edit.pop3_use_ssl_email;
                    break;
                default:
                    modelState.AddModelError("", "Непредусмотренный тип транспорта " + reg_edit.transport_type.ToString());
                    return null;
            }

            exchange_reg_partner.login = reg_edit.login_name;
            exchange_reg_partner.password = reg_edit.password;
            exchange_reg_partner.download_url = reg_edit.download_url;
            exchange_reg_partner.upload_equals_download = reg_edit.upload_equals_download;
            exchange_reg_partner.upload_url = reg_edit.upload_url;
            exchange_reg_partner.upload_url_alias = reg_edit.upload_url_alias;
            exchange_reg_partner.pop3_address = reg_edit.pop3_address;
            exchange_reg_partner.pop3_port = reg_edit.pop3_port;
            exchange_reg_partner.pop3_use_ssl = reg_edit.pop3_use_ssl;

            exchange_user_reg.is_active = reg_edit.is_active;

            dbContext.SaveChanges();

            return new ExchangeRegPartnerViewModel(exchange_user_reg, exchange_reg_partner);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeRegPartnerViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((ExchangeRegPartnerViewModel)item).item_id;
            long reg_id = ((ExchangeRegPartnerViewModel)item).reg_id;

            exchange_user_reg exchange_user_reg = null;

            exchange_user_reg = dbContext.exchange_user_reg.Where(ss => ss.item_id == id).FirstOrDefault();
            if (exchange_user_reg == null)
            {
                modelState.AddModelError("serv_result", "Регистрация не найдена");
                return false;
            }

            
            dbContext.exchange_user_reg.Remove(exchange_user_reg);
            dbContext.exchange_reg_partner_item.RemoveRange(dbContext.exchange_reg_partner_item.Where(ss => ss.reg_id == reg_id));
            dbContext.exchange_reg_partner.Remove(dbContext.exchange_reg_partner.Where(ss => ss.reg_id == reg_id).FirstOrDefault());
            dbContext.exchange_reg.Remove(dbContext.exchange_reg.Where(ss => ss.reg_id == reg_id).FirstOrDefault());

            dbContext.SaveChanges();
            return true;
        }
    }
}