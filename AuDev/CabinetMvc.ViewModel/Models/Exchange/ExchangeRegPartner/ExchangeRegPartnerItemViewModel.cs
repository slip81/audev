﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeRegPartnerItemViewModel : AuBaseViewModel
    {
        public ExchangeRegPartnerItemViewModel()
            : base(Enums.MainObjectType.EXCHANGE_REG_PARTNER_ITEM)
        {
            //
        }

        public ExchangeRegPartnerItemViewModel(exchange_reg_partner_item item)
            : base(Enums.MainObjectType.EXCHANGE_REG_PARTNER_ITEM)
        {            
            item_id = item.item_id;
            reg_id = item.reg_id;
            exchange_item_id = item.exchange_item_id;
            exchange_item_name = item.exchange_item.item_name;
            is_required = item.is_required;
            days_cnt_for_wait = item.days_cnt_for_wait;
            is_download_to_client = item.is_download_to_client;
        }
        
        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Display(Name = "Код регистрации")]
        public long reg_id { get; set; }

        [Display(Name = "Файл обмена")]
        public int exchange_item_id { get; set; }

        [Display(Name = "Файл обмена")]
        [UIHint("Text")]
        public string exchange_item_name { get; set; }

        [Display(Name = "Обязателен")]
        public bool is_required { get; set; }

        [Display(Name = "Сколько дней можно ждать")]
        [UIHint("IntegerFromOne")]
        public int days_cnt_for_wait { get; set; }

        [Display(Name = "Отдавать клиенту")]
        public bool is_download_to_client { get; set; }
    }

}