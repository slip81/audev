﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeRegPartnerItemService : IAuBaseService
    {
        //
    }

    public class ExchangeRegPartnerItemService : AuBaseService, IExchangeRegPartnerItemService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.exchange_reg_partner_item
                .Where(ss => ss.reg_id == parent_id)
                .Select(ss => new ExchangeRegPartnerItemViewModel()
                {
                    item_id = ss.item_id,
                    reg_id = ss.reg_id,
                    exchange_item_id = ss.exchange_item_id,
                    exchange_item_name = ss.exchange_item.item_name,
                    is_required = ss.is_required,
                    days_cnt_for_wait = ss.days_cnt_for_wait,
                    is_download_to_client = ss.is_download_to_client,
                }
                );
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeRegPartnerItemViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeRegPartnerItemViewModel itemEdit = (ExchangeRegPartnerItemViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты настройки файла");
                return null;
            }

            var exchange_reg_partner_item = dbContext.exchange_reg_partner_item.Where(ss => ss.item_id == itemEdit.item_id).FirstOrDefault();
            if (exchange_reg_partner_item == null)
            {
                modelState.AddModelError("", "Не найдена настройка файла " + itemEdit.item_id.ToString());
                return null;
            }

            modelBeforeChanges = new ExchangeRegPartnerItemViewModel(exchange_reg_partner_item);

            exchange_reg_partner_item.days_cnt_for_wait = itemEdit.days_cnt_for_wait;
            exchange_reg_partner_item.is_required = itemEdit.is_required;
            exchange_reg_partner_item.is_download_to_client = itemEdit.is_download_to_client;

            dbContext.SaveChanges();

            return new ExchangeRegPartnerItemViewModel(exchange_reg_partner_item);
        }
    }
}