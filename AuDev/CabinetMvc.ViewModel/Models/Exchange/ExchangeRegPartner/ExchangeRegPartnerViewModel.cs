﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using Newtonsoft.Json;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeRegPartnerViewModel : AuBaseViewModel
    {
        public ExchangeRegPartnerViewModel()
            : base(Enums.MainObjectType.EXCHANGE_REG_PARTNER)
        {
            //
            upload_equals_download = true;
            days_cnt_for_mov = 1;
            CreateCombo();
        }

        public ExchangeRegPartnerViewModel(exchange_user_reg item, exchange_reg_partner item2)
            : base(Enums.MainObjectType.EXCHANGE_REG_PARTNER)
        {            
            //
            item_id = item.item_id;
            user_id = item.user_id;
            reg_id = item.reg_id;
            is_active = item.is_active;
            //
            ds_id = item2.ds_id;
            login_name = item2.login;
            password = item2.password;
            transport_type = item2.transport_type;
            format_type = item2.format_type;
            upload_url = item2.upload_url;
            download_url = item2.download_url;
            crt_date = item2.crt_date;
            reg_code = item2.reg_code;
            reg_name = item2.reg_name;
            send_all = item2.send_all == 1 ? true : false;
            use_sales_prefix = item2.use_sales_prefix == 1 ? true : false;
            download_arc_type = item2.download_arc_type;
            upload_arc_type = item2.upload_arc_type;
            pop3_address = item2.pop3_address;
            pop3_port = item2.pop3_port;
            pop3_use_ssl = item2.pop3_use_ssl;
            upload_equals_download = item2.upload_equals_download;
            upload_url_alias = item2.upload_url_alias;
            days_cnt_for_mov = item2.days_cnt_for_mov;
            upload_confirm_address = item2.upload_confirm_address;
            is_prepared = item2.is_prepared;
            upload_state = item2.upload_state;
            download_state = item2.download_state;
            upload_last_date = item2.upload_last_date;
            download_last_date = item2.download_last_date;
            upload_last_mess = item2.upload_last_mess;
            download_last_mess = item2.download_last_mess;
            prepared_date = item2.prepared_date;
            //
            CreateCombo();
        }

        private void CreateCombo()
        {
            if (ExchangeFormatList == null)
            {
                ExchangeFormatList = new List<SimpleCombo>();
                var format_values = GlobalUtil.GetEnumValues<Enums.ExchangeFormat>();
                foreach (var format_value in format_values)
                {
                    ExchangeFormatList.Add(new SimpleCombo()
                    {
                        obj_id = (int)format_value,
                        obj_name = ((int)format_value).EnumName<Enums.ExchangeFormat>(),
                    });
                }
            }
            if (ExchangeTransportList == null)
            {
                ExchangeTransportList = new List<SimpleCombo>();
                var transport_values = GlobalUtil.GetEnumValues<Enums.ExchangeTransport>();
                foreach (var transport_value in transport_values)
                {
                    ExchangeTransportList.Add(new SimpleCombo()
                    {
                        obj_id = (int)transport_value,
                        obj_name = ((int)transport_value).EnumName<Enums.ExchangeTransport>(),
                    });
                }
            }
            if (ExchangeCompressionList == null)
            {
                ExchangeCompressionList = new List<SimpleCombo>();
                var compr_values = GlobalUtil.GetEnumValues<Enums.ArcType>();
                foreach (var compr_value in compr_values)
                {
                    ExchangeCompressionList.Add(new SimpleCombo()
                    {
                        obj_id = (int)compr_value,
                        obj_name = ((int)compr_value).EnumName<Enums.ArcType>(),
                    });
                }
            }
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Display(Name = "Код пользователя")]
        public long user_id { get; set; }

        [Display(Name = "Код регистрации")]
        public long reg_id { get; set; }

        [Display(Name = "Регистрация активна")]
        public bool is_active { get; set; }

        [Display(Name = "Регистрация активна")]
        public string is_active_str { get; set; }
        
        [Display(Name = "Регистрация для партнера")]
        [Required(ErrorMessage = "Не указан партнер")]
        public Nullable<long> ds_id { get; set; }

        [Display(Name = "Логин для доступа")]
        public string login_name { get; set; }

        [Display(Name = "Пароль для доступа")]
        [DataType(DataType.Password)]
        public string password { get; set; }

        [Display(Name = "Тип транспорта")]
        [Required(ErrorMessage = "Не указан тип транспорта")]
        public Nullable<int> transport_type { get; set; }

        [Display(Name = "Формат файлов для передачи")]
        [Required(ErrorMessage = "Не указан формат файлов передачи")]
        public Nullable<int> format_type { get; set; }

        [Display(Name = "Путь для отправки файлов Партнеру")]
        public string upload_url { get; set; }

        [Display(Name = "Путь для получения файлов от Партнера")]
        public string download_url { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Код Участника в партнерской программе")]
        [UIHint("Text")]
        public string reg_code { get; set; }

        [Display(Name = "Имя Участника в партнерской программе")]
        [UIHint("Text")]
        public string reg_name { get; set; }

        [Display(Name = "Передавать все")]
        public bool send_all { get; set; }

        [Display(Name = "Передавать все")]
        public string send_all_str { get; set; }

        [Display(Name = "Партнер")]
        public string ds_name { get; set; }

        [Display(Name = "Использовать префикс торговых точек")]
        public bool use_sales_prefix { get; set; }

        [Display(Name = "Использовать префикс торговых точек")]
        public string use_sales_prefix_str { get; set; }

        [Display(Name = "Сжатие отправляемого файла")]
        public int upload_arc_type { get; set; }

        [Display(Name = "Сжатие получаемого файла")]
        public int download_arc_type { get; set; }

        // fake fields for ftp reg for View

        [Display(Name = "Логин для доступа по FTP")]
        [UIHint("Text")]
        [JsonIgnore()]        
        public string login_ftp { get; set; }

        [Display(Name = "Пароль для доступа по FTP")]
        [DataType(DataType.Password)]
        [UIHint("Password")]
        [JsonIgnore()]
        public string password_ftp { get; set; }

        [Display(Name = "Путь к каталогу на FTP для отправки файлов Партнеру")]
        [UIHint("Text")]
        [JsonIgnore()]
        public string upload_url_ftp { get; set; }

        [Display(Name = "Путь к каталогу на FTP для получения файлов от Партнера")]
        [UIHint("Text")]
        [JsonIgnore()]
        public string download_url_ftp { get; set; }

        [Display(Name = "Адрес POP3-сервера")]
        public string pop3_address { get; set; }

        [Display(Name = "Порт POP3-сервера")]
        public Nullable<int> pop3_port { get; set; }

        [Display(Name = "Использовать шифрованное соединение (SSL)")]
        public bool pop3_use_ssl { get; set; }

        [Display(Name = "Использовать для отправки этот же адрес")]
        public bool upload_equals_download { get; set; }

        [Display(Name = "Адрес выгрузки, который будет отображаться Партнеру")]
        public string upload_url_alias { get; set; }

        [Display(Name = "Количество дней выгрузки для движения")]
        [UIHint("IntegerFromOne")]
        public int days_cnt_for_mov { get; set; }

        // fake fields for email reg for View

        // download
        [Display(Name = "Электронный адрес для получения файлов от Партнера")]
        [UIHint("Text")]
        [JsonIgnore()]
        public string download_url_email { get; set; }

        [Display(Name = "Пароль от почтового ящика")]
        [DataType(DataType.Password)]
        [UIHint("Password")]
        [JsonIgnore()]
        public string password_email { get; set; }

        [Display(Name = "Адрес POP3-сервера")]
        [UIHint("Text")]
        [JsonIgnore()]
        public string pop3_address_email { get; set; }

        [Display(Name = "Порт для соединения")]
        [UIHint("IntegerFromOne")]
        [JsonIgnore()]
        public Nullable<int> pop3_port_email { get; set; }

        [Display(Name = "Использовать шифрованное соединение (SSL)")]
        [JsonIgnore()]
        public bool pop3_use_ssl_email { get; set; }

        // upload
        [Display(Name = "Использовать для отправки этот же адрес")]
        [JsonIgnore()]
        public bool upload_equals_download_email { get; set; }

        [Display(Name = "Электронный адрес, на который будут отправляться файлы Партнеру")]
        [UIHint("Text")]
        [JsonIgnore()]
        public string upload_url_email { get; set; }

        [Display(Name = "Электронный адрес, который будет отображаться Партнеру")]
        [UIHint("Text")]
        [JsonIgnore()]
        public string upload_url_alias_email { get; set; }

        [Display(Name = "Электронный адрес, на который будет отправляться подтверждение")]
        [UIHint("Text")]
        public string upload_confirm_address { get; set; }

        [Display(Name = "Настройки завершены")]        
        public bool is_prepared { get; set; }

        [Display(Name = "Состояние отправки")]
        [UIHint("Text")]
        public int upload_state { get; set; }

        [Display(Name = "Состояние приема")]
        [UIHint("Text")]
        public int download_state { get; set; }

        [Display(Name = "Дата последней отправки")]
        public Nullable<System.DateTime> upload_last_date { get; set; }

        [Display(Name = "Дата последнего приема")]
        public Nullable<System.DateTime> download_last_date { get; set; }

        [Display(Name = "Сообщение последней отправки")]
        public string upload_last_mess { get; set; }

        [Display(Name = "Сообщение последнего приема")]
        public string download_last_mess { get; set; }

        [Display(Name = "Дата завершения настроек")]
        public Nullable<System.DateTime> prepared_date { get; set; }
    

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public List<SimpleCombo> ExchangeFormatList { get; set; }

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public List<SimpleCombo> ExchangeTransportList { get; set; }

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public List<SimpleCombo> ExchangeCompressionList { get; set; }

    }

}