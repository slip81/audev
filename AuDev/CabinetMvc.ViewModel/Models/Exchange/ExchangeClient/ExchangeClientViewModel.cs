﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public class ExchangeClientViewModel : AuBaseViewModel
    {
        public ExchangeClientViewModel()
            : base(Enums.MainObjectType.EXCHANGE_CLIENT)
        {
            //
        }

        public ExchangeClientViewModel(exchange_client item)
            : base(Enums.MainObjectType.EXCHANGE_CLIENT)
        {
            client_id = item.client_id;
            client_name = item.client_name;
            cabinet_client_id = item.cabinet_client_id;
        }

        [Key()]
        [Display(Name = "Код")]
        public long client_id { get; set; }

        [Display(Name = "Организация")]        
        public string client_name { get; set; }

        [Display(Name = "Организация")]
        [Required(ErrorMessage = "Не задана организация")]
        public Nullable<long> cabinet_client_id { get; set; }
    }
}