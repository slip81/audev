﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Exchange
{
    public interface IExchangeClientService  : IAuBaseService
    {
        ExchangeClientViewModel GetItem_byCabinetClientId(int? id);
        IQueryable<ExchangeClientViewModel> GetList_ForLog();        
    }

    public class ExchangeClientService : AuBaseService, IExchangeClientService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_exchange_client.Select(ss => new ExchangeClientViewModel()
            {
                client_id = ss.client_id,
                client_name = ss.client_name,
                cabinet_client_id = ss.cabinet_client_id,
            }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_exchange_client
                .Where(ss => ss.client_id == id)
                .Select(ss => new ExchangeClientViewModel()
            {
                client_id = ss.client_id,
                client_name = ss.client_name,
                cabinet_client_id = ss.cabinet_client_id,
            }
            )
            .FirstOrDefault();
        }

        public ExchangeClientViewModel GetItem_byCabinetClientId(int? id)
        {
            return dbContext.vw_exchange_client
                .Where(ss => ss.cabinet_client_id == id)
                .Select(ss => new ExchangeClientViewModel()
                {
                    client_id = ss.client_id,
                    client_name = ss.client_name,
                    cabinet_client_id = ss.cabinet_client_id,
                }
                )
                .FirstOrDefault();
        }

        public IQueryable<ExchangeClientViewModel> GetList_ForLog()
        {
            // !!!
            // todo
            long cabinet_client_id = -1;

            if (currUser.IsAdmin)
            {
                return (from ec in dbContext.vw_exchange_client
                        from l in dbContext.vw_log_esn_exchange_partner
                        where ec.client_id == l.client_id
                        select ec)
                          .Distinct()
                          .Select(ss => new ExchangeClientViewModel()
                          {
                              client_id = ss.client_id,
                              client_name = ss.client_name,
                              cabinet_client_id = ss.cabinet_client_id,
                          }
                );
            }
            else
            {
                return (from ec in dbContext.vw_exchange_client
                        from l in dbContext.vw_log_esn_exchange_partner
                        where ec.client_id == l.client_id
                        && ec.cabinet_client_id == cabinet_client_id
                        select ec)
                      .Distinct()
                      .Select(ss => new ExchangeClientViewModel()
                      {
                          client_id = ss.client_id,
                          client_name = ss.client_name,
                          cabinet_client_id = ss.cabinet_client_id,
                      }
                      );
            }
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeClientViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeClientViewModel client_add = (ExchangeClientViewModel)item;
            if (
                (client_add == null)
                ||
                (String.IsNullOrEmpty(client_add.client_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты участника обмена");
                return null;
            }

            exchange_client existing_exchange_client = dbContext.exchange_client.Where(ss => ss.cabinet_client_id == client_add.cabinet_client_id).FirstOrDefault();
            if (existing_exchange_client != null)
            {
                modelState.AddModelError("", "Уже создан такой участник обмена");
                return null;
            }

            exchange_client exchange_client = new exchange_client();
            exchange_client.client_name = client_add.client_name;
            exchange_client.cabinet_client_id = client_add.cabinet_client_id;

            dbContext.exchange_client.Add(exchange_client);
            dbContext.SaveChanges();

            return new ExchangeClientViewModel(exchange_client);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeClientViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ExchangeClientViewModel client_edit = (ExchangeClientViewModel)item;
            if (
                (client_edit == null)
                ||
                (String.IsNullOrEmpty(client_edit.client_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты участника обмена");
                return null;
            }

            var client = dbContext.exchange_client.Where(ss => ss.client_id == client_edit.client_id).FirstOrDefault();
            if (client == null)
            {
                modelState.AddModelError("", "Не найден участник обмена с кодом " + client_edit.client_id.ToString());
                return null;
            }
            modelBeforeChanges = new ExchangeClientViewModel(client);

            client.client_name = client_edit.client_name;
            //client.cabinet_client_id = client_edit.cabinet_client_id;

            dbContext.SaveChanges();

            return new ExchangeClientViewModel(client);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ExchangeClientViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((ExchangeClientViewModel)item).client_id;

            exchange_client exchange_client = null;

            exchange_client = dbContext.exchange_client.Where(ss => ss.client_id == id).FirstOrDefault();
            if (exchange_client == null)
            {
                modelState.AddModelError("serv_result", "Участник обмена не найден");
                return false;
            }

            var user = dbContext.exchange_user.Where(ss => ss.client_id == exchange_client.client_id).FirstOrDefault();
            if (user != null)
            {
                modelState.AddModelError("serv_result", "Есть зарегестрированные пользователи данного участника");
                return false;
            }

            dbContext.exchange_client.Remove(exchange_client);
            dbContext.SaveChanges();
            return true;
        }

    }
}