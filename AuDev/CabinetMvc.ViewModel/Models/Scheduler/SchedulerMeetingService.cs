﻿namespace CabinetMvc.ViewModel.Scheduler
{
    using Kendo.Mvc.UI;
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using CabinetMvc.ViewModel.Discount;
    using CabinetMvc.ViewModel.User;
    using AuDev.Common.Db.Model;

    public class SchedulerMeetingService : ISchedulerEventService<MeetingViewModel>
    {
        private UserViewModel currUser { get; set; }

        public SchedulerMeetingService(UserViewModel _currUser)
        {
            currUser = _currUser;
        }

        public virtual IQueryable<MeetingViewModel> GetAll()
        {            
            long business_id = currUser.BusinessId;
            using (var dbContext = new AuMainDb())
            {
                IQueryable<cab_event> cab_events = dbContext.cab_event.Where(ss => ss.business_id == business_id);

                return cab_events.ToList().Select(meeting => new MeetingViewModel
                {
                    Description = meeting.description,
                    End = (DateTime)meeting.date_end,
                    IsAllDay = (meeting.is_all_day == 1),
                    EndTimezone = meeting.end_timezone,
                    MeetingID = meeting.event_id,
                    RecurrenceException = meeting.recurrence_exception,
                    RecurrenceID = meeting.recurrence_id,
                    RecurrenceRule = meeting.recurrence_rule,
                    RoomID = meeting.event_type_id,
                    Start = (DateTime)meeting.date_beg,
                    StartTimezone = meeting.start_timezone,
                    Title = meeting.title,
                }).AsQueryable();
            }
        }

        public virtual void Insert(MeetingViewModel meeting, ModelStateDictionary modelState)
        {
            
            if (ValidateModel(meeting, modelState))
            {
                if (string.IsNullOrEmpty(meeting.Title))
                {
                    meeting.Title = "";
                }

                long business_id = currUser.BusinessId;
                using (var dbContext = new AuMainDb())
                {
                    cab_event entity = new cab_event();
                    entity.business_id = business_id;
                    entity.date_beg = meeting.Start;
                    entity.date_end = meeting.End;
                    entity.description = meeting.Description;
                    entity.end_timezone = meeting.EndTimezone;
                    //entity.event_state_id = 
                    entity.event_type_id = meeting.RoomID;
                    entity.is_all_day = meeting.IsAllDay ? (short)1 : (short)0;
                    entity.recurrence_exception = meeting.RecurrenceException;
                    entity.recurrence_id = meeting.RecurrenceID;
                    entity.recurrence_rule = meeting.RecurrenceRule;
                    entity.start_timezone = meeting.StartTimezone;
                    entity.title = meeting.Title;
                    dbContext.cab_event.Add(entity);
                    dbContext.SaveChanges();
                    meeting.MeetingID = entity.event_id;
                }
            }
            
        }

        public virtual void Update(MeetingViewModel meeting, ModelStateDictionary modelState)
        {
            
            if (ValidateModel(meeting, modelState))
            {
                if (string.IsNullOrEmpty(meeting.Title))
                {
                    meeting.Title = "";
                }

                long business_id = currUser.BusinessId;
                using (var dbContext = new AuMainDb())
                {
                    cab_event entity = dbContext.cab_event.Where(ss => ss.event_id == meeting.MeetingID && ss.business_id == business_id).FirstOrDefault();                    
                    entity.date_beg = meeting.Start;
                    entity.date_end = meeting.End;
                    entity.description = meeting.Description;
                    entity.end_timezone = meeting.EndTimezone;
                    //entity.event_state_id = 
                    entity.event_type_id = meeting.RoomID;
                    entity.is_all_day = meeting.IsAllDay ? (short)1 : (short)0;
                    entity.recurrence_exception = meeting.RecurrenceException;
                    entity.recurrence_id = meeting.RecurrenceID;
                    entity.recurrence_rule = meeting.RecurrenceRule;
                    entity.start_timezone = meeting.StartTimezone;
                    entity.title = meeting.Title;                    
                    dbContext.SaveChanges();                 
                }
            }
            
        }

        public virtual void Delete(MeetingViewModel meeting, ModelStateDictionary modelState)
        {

            long business_id = currUser.BusinessId;
            using (var dbContext = new AuMainDb())
            {
                cab_event entity = dbContext.cab_event.Where(ss => ss.event_id == meeting.MeetingID && ss.business_id == business_id).FirstOrDefault();
                dbContext.cab_event.Remove(entity);
                dbContext.SaveChanges();
            }
        }

        private bool ValidateModel(MeetingViewModel appointment, ModelStateDictionary modelState)
        {
            if (appointment.Start > appointment.End)
            {
                modelState.AddModelError("errors", "Дата окончания не должна быть меньше даты начала");
                return false;
            }

            return true;
        }

        public void Dispose()
        {
            //
        }
    }
}