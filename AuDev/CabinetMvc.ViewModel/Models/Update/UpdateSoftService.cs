﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Update
{
    public interface IUpdateSoftService : IAuBaseService
    {

    }

    public class UpdateSoftService : AuBaseService, IUpdateSoftService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_update_soft
                .Where(ss => ss.soft_id == id)
                .ProjectTo<UpdateSoftViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_update_soft.ProjectTo<UpdateSoftViewModel>();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateSoftViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            UpdateSoftViewModel itemAdd = (UpdateSoftViewModel)item;
            if ((itemAdd == null) || (itemAdd.SoftGroup == null))
            {
                modelState.AddModelError("", "Не заданы атрибуты софта");
                return null;
            }

            if (itemAdd.service_id.GetValueOrDefault(0) > 0)
            {
                update_soft update_soft_existing = dbContext.update_soft.Where(ss => ss.soft_id != itemAdd.soft_id && ss.service_id == itemAdd.service_id).FirstOrDefault();
                if (update_soft_existing != null)
                {
                    modelState.AddModelError("", "Уже есть софт, привязанный к этой услуге");
                    return null;
                }
            }

            update_soft update_soft = new update_soft();
            ModelMapper.Map<UpdateSoftViewModel, update_soft>(itemAdd, update_soft);
            update_soft.soft_guid = System.Guid.NewGuid().ToString();
            update_soft.group_id = itemAdd.SoftGroup.group_id;

            dbContext.update_soft.Add(update_soft);
            dbContext.SaveChanges();

            return xGetItem(update_soft.soft_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateSoftViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            UpdateSoftViewModel itemEdit = (UpdateSoftViewModel)item;
            if ((itemEdit == null) || (itemEdit.SoftGroup == null))
            {
                modelState.AddModelError("", "Не заданы параметры софта");
                return null;
            }

            update_soft update_soft = dbContext.update_soft.Where(ss => ss.soft_id == itemEdit.soft_id).FirstOrDefault();
            if (update_soft == null)
            {
                modelState.AddModelError("", "Не найдены параметры софта");
                return null;
            }

            if (itemEdit.service_id.GetValueOrDefault(0) > 0)
            {
                update_soft update_soft_existing = dbContext.update_soft.Where(ss => ss.soft_id != itemEdit.soft_id && ss.service_id == itemEdit.service_id).FirstOrDefault();
                if (update_soft_existing != null)
                {
                    modelState.AddModelError("", "Уже есть софт, привязанный к этой услуге");
                    return null;
                }
            }

            UpdateSoftViewModel oldModel = new UpdateSoftViewModel();
            ModelMapper.Map<update_soft, UpdateSoftViewModel>(update_soft, oldModel);
            modelBeforeChanges = oldModel;

            ModelMapper.Map<UpdateSoftViewModel, update_soft>(itemEdit, update_soft);
            update_soft.group_id = itemEdit.SoftGroup.group_id;

            dbContext.SaveChanges();

            return xGetItem(update_soft.soft_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateSoftViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            UpdateSoftViewModel itemDel = (UpdateSoftViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры софта");
                return false;
            }

            update_soft update_soft = dbContext.update_soft.Where(ss => ss.soft_id == itemDel.soft_id).FirstOrDefault();
            if (update_soft == null)
            {
                modelState.AddModelError("", "Не найдены параметры софта");
                return false;
            }

            List<update_soft_version> update_soft_version_list = dbContext.update_soft_version.Where(ss => ss.soft_id == itemDel.soft_id).ToList();
            if (update_soft_version_list != null)
            {
                foreach (var update_soft_version in update_soft_version_list)
                {
                    string file_path = update_soft_version.file_path;
                    if (!String.IsNullOrEmpty(file_path))
                    {
                        string folder = Path.GetDirectoryName(file_path);
                        if (Directory.Exists(folder))
                            Directory.Delete(folder, true);
                    }

                    string xml_file_path = update_soft_version.xml_file_path;
                    if (!String.IsNullOrEmpty(xml_file_path))
                    {
                        string folder = Path.GetDirectoryName(xml_file_path);
                        if (Directory.Exists(folder))
                            Directory.Delete(folder, true);
                    }
                }
            }

            dbContext.update_soft_version.RemoveRange(dbContext.update_soft_version.Where(ss => ss.soft_id == itemDel.soft_id));
            dbContext.update_soft.Remove(update_soft);
            dbContext.SaveChanges();

            return true;            
        }

    }
}
