﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Update
{
    public interface IUpdateBatchItemService : IAuBaseService
    {

    }

    public class UpdateBatchItemService : AuBaseService, IUpdateBatchItemService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.update_batch_item
                .Where(ss => ss.item_id == id)
                .ProjectTo<UpdateBatchItemViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_update_batch_item.Where(ss => ss.batch_id == parent_id).ProjectTo<UpdateBatchItemViewModel>();
        }

    }
}
