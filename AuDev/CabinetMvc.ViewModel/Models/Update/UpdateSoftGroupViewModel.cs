﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
#endregion

namespace CabinetMvc.ViewModel.Update
{
    public class UpdateSoftGroupViewModel : AuBaseViewModel
    {
        public UpdateSoftGroupViewModel()
            : base(Enums.MainObjectType.UPDATE_SOFT_GROUP)
        {            
            //update_soft_group
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int group_id { get; set; }

        [Display(Name = "Наименование")]
        public string group_name { get; set; }
    }
}