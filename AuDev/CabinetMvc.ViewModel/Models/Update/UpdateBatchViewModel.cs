﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Update
{
    public class UpdateBatchViewModel : AuBaseViewModel
    {

        public UpdateBatchViewModel()
            : base(Enums.MainObjectType.UPDATE_BATCH)
        {
            //vw_update_batch
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int batch_id { get; set; }

        [Display(Name = "Наименование")]
        public string batch_name { get; set; }

        [Display(Name = "Клиент")]
        [Required(ErrorMessage="Не выбрана организация")]
        public int client_id { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]
        public string crt_user { get; set; }

        [Display(Name = "Когда скачано")]
        public Nullable<System.DateTime> last_download_date { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "update_batch_item_list")]
        [JsonIgnore()]
        public string update_batch_item_list { get; set; }

    }
}