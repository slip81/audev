﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Update
{
    public interface IUpdateBatchService : IAuBaseService
    {

    }

    public class UpdateBatchService : AuBaseService, IUpdateBatchService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_update_batch
                .Where(ss => ss.batch_id == id)
                .ProjectTo<UpdateBatchViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_update_batch.ProjectTo<UpdateBatchViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_update_batch.Where(ss => ss.client_id == parent_id).ProjectTo<UpdateBatchViewModel>();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateBatchViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            UpdateBatchViewModel itemAdd = (UpdateBatchViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты пакета");
                return null;
            }

            itemAdd.batch_name = String.IsNullOrEmpty(itemAdd.batch_name) ? "[нет названия]" : itemAdd.batch_name;
            
            List<UpdateBatchItemViewModel> update_batch_item_list = JsonConvert.DeserializeObject<List<UpdateBatchItemViewModel>>(itemAdd.update_batch_item_list);

            DateTime now =  DateTime.Now;
            update_batch update_batch = new update_batch();
            ModelMapper.Map<UpdateBatchViewModel, update_batch>(itemAdd, update_batch);
            update_batch.crt_date = now;
            update_batch.crt_user = currUser.CabUserName;

            dbContext.update_batch.Add(update_batch);

            if (update_batch_item_list != null)
            {
                foreach (var update_batch_item_list_item in update_batch_item_list.Where(ss => ss.is_subscribed).ToList())
                {
                    update_batch_item update_batch_item = new update_batch_item();
                    update_batch_item.update_batch = update_batch;
                    update_batch_item.soft_id = update_batch_item_list_item.soft_id;                    
                    dbContext.update_batch_item.Add(update_batch_item);
                }
            }

            dbContext.SaveChanges();

            return xGetItem(update_batch.batch_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateBatchViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            UpdateBatchViewModel itemEdit = (UpdateBatchViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры пакета");
                return null;
            }

            update_batch update_batch = dbContext.update_batch.Where(ss => ss.batch_id == itemEdit.batch_id).FirstOrDefault();
            if (update_batch == null)
            {
                modelState.AddModelError("", "Не найдены параметры пакета");
                return null;
            }

            UpdateBatchViewModel oldModel = new UpdateBatchViewModel();
            ModelMapper.Map<update_batch, UpdateBatchViewModel>(update_batch, oldModel);
            modelBeforeChanges = oldModel;

            List<UpdateBatchItemViewModel> update_batch_item_list = JsonConvert.DeserializeObject<List<UpdateBatchItemViewModel>>(itemEdit.update_batch_item_list);
            if (update_batch_item_list == null)
                update_batch_item_list = new List<UpdateBatchItemViewModel>();
            List<update_batch_item> update_batch_item_list_existing = dbContext.update_batch_item.Where(ss => ss.batch_id == itemEdit.batch_id).ToList();
            if (update_batch_item_list_existing == null)
                update_batch_item_list_existing = new List<update_batch_item>();

            foreach (var update_batch_item_list_item in update_batch_item_list.Where(ss => ss.is_subscribed && !update_batch_item_list_existing.Where(tt => tt.soft_id == ss.soft_id).Any()).ToList())
            {
                update_batch_item update_batch_item = new update_batch_item();
                update_batch_item.update_batch = update_batch;
                update_batch_item.soft_id = update_batch_item_list_item.soft_id;                
                dbContext.update_batch_item.Add(update_batch_item);
            }

            foreach (var update_batch_item_list_item in update_batch_item_list_existing.Where(ss => !update_batch_item_list.Where(tt => tt.is_subscribed && tt.soft_id == ss.soft_id).Any()).ToList())
            {
                dbContext.update_batch_item.Remove(update_batch_item_list_item);
            }
            
            dbContext.SaveChanges();

            return xGetItem(update_batch.batch_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateBatchViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            UpdateBatchViewModel itemDel = (UpdateBatchViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры пакета");
                return false;
            }

            update_batch update_batch = dbContext.update_batch.Where(ss => ss.batch_id == itemDel.batch_id).FirstOrDefault();
            if (update_batch == null)
            {
                modelState.AddModelError("", "Не найдены параметры пакета");
                return false;
            }


            dbContext.update_batch_item.RemoveRange(dbContext.update_batch_item.Where(ss => ss.batch_id == itemDel.batch_id));
            dbContext.update_batch.Remove(update_batch);
            dbContext.SaveChanges();

            return true;            
        }

    }
}
