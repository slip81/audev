﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Update
{
    public class UpdateBatchItemViewModel : AuBaseViewModel
    {

        public UpdateBatchItemViewModel()
            : base(Enums.MainObjectType.UPDATE_BATCH_ITEM)
        {
            //vw_update_batch_item
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }

        [Display(Name = "Софт")]
        public int soft_id { get; set; }

        [Display(Name = "Софт")]
        public string soft_name { get; set; }

        [Display(Name = "Пакет")]
        public Nullable<int> batch_id { get; set; }

        [Display(Name = "Клиент")]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "Подписка")]
        public bool is_subscribed { get; set; }

        [Display(Name = "Приоритет")]
        public Nullable<int> ord { get; set; }

        [Display(Name = "Когда скачано")]
        public Nullable<System.DateTime> last_download_date { get; set; }

    }
}