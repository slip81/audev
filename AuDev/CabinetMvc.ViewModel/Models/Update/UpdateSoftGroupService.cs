﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Update
{
    public interface IUpdateSoftGroupService : IAuBaseService
    {
        //
    }

    public class UpdateSoftGroupService : AuBaseService, IUpdateSoftGroupService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.update_soft_group
                .Where(ss => ss.group_id == id)
                .ProjectTo<UpdateSoftGroupViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.update_soft_group.ProjectTo<UpdateSoftGroupViewModel>();
        }
    }
}