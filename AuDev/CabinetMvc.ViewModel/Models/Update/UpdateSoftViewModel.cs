﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
#endregion

namespace CabinetMvc.ViewModel.Update
{
    public class UpdateSoftViewModel : AuBaseViewModel
    {
        public UpdateSoftViewModel()
            : base(Enums.MainObjectType.UPDATE_SOFT)
        {            
            //vw_update_soft
        }
        
        [Key()]
        [Display(Name = "Код")]
        public int soft_id { get; set; }

        [Display(Name = "Название")]
        [Required(ErrorMessage="Не задано название")]
        public string soft_name { get; set; }

        [Display(Name = "Услуга")]
        public Nullable<int> service_id { get; set; }

        [Display(Name = "Есть доп. файлы")]
        public bool items_exists { get; set; }

        [Display(Name = "guid")]
        [ScaffoldColumn(false)]
        public string soft_guid { get; set; }

        [Display(Name = "Группа")]
        public int group_id { get; set; }

        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [Display(Name = "Группа")]
        [UIHint("SoftGroupCombo")]
        public UpdateSoftGroupViewModel SoftGroup { get; set; }
    }
}