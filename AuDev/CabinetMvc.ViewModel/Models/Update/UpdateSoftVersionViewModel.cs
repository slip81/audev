﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Update
{
    public class UpdateSoftVersionViewModel : AuBaseViewModel
    {

        public UpdateSoftVersionViewModel()
            : base(Enums.MainObjectType.UPDATE_SOFT_VERSION)
        {
            //update_soft_version
        }
        
        [Key()]
        [Display(Name = "Код")]
        [ScaffoldColumn(false)] 
        public int version_id { get; set; }

        [Display(Name = "Софт")]
        [ScaffoldColumn(false)] 
        public int soft_id { get; set; }

        [Display(Name = "Номер")]
        [Required(ErrorMessage="Не задан номер версии")]
        public string version_name { get; set; }

        [Display(Name = "Билд")]
        public string version_build { get; set; }

        [Display(Name = "Дата версии")]
        [Required(ErrorMessage = "Не задана дата версии")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> version_date { get; set; }

        [Display(Name = "Файл")]
        [UIHint("UpdateFileUpload")]
        //[Required(ErrorMessage = "Не задан файл")]
        public string file_path { get; set; }

        [Display(Name = "Имя файла")]                
        [ScaffoldColumn(false)]
        public string file_name { get; set; }

        [Display(Name = "Дата выкладки")]
        [ScaffoldColumn(false)] 
        public Nullable<System.DateTime> upload_date { get; set; }

        [Display(Name = "Автор выкладки")]
        [ScaffoldColumn(false)] 
        public string upload_user { get; set; }

        [Display(Name = "XML файл")]
        [UIHint("UpdateXmlFileUpload")]
        public string xml_file_path { get; set; }

        [Display(Name = "Имя XML файла")]
        [ScaffoldColumn(false)]
        public string xml_file_name { get; set; }

        [Display(Name = "guid")]
        [ScaffoldColumn(false)]
        public string version_guid { get; set; }

    }
}