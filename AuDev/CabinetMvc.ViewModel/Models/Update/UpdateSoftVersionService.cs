﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Update
{
    public interface IUpdateSoftVersionService : IAuBaseService
    {

    }

    public class UpdateSoftVersionService : AuBaseService, IUpdateSoftVersionService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.update_soft_version
                .Where(ss => ss.version_id == id)
                .ProjectTo<UpdateSoftVersionViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.update_soft_version
                .Where(ss => ss.soft_id == parent_id)
                .ProjectTo<UpdateSoftVersionViewModel>();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateSoftVersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            UpdateSoftVersionViewModel itemAdd = (UpdateSoftVersionViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты версии");
                return null;
            }

            update_soft_version update_soft_version_existing = dbContext.update_soft_version.Where(ss => ss.version_id != itemAdd.version_id && ss.soft_id == itemAdd.soft_id && ss.version_name == itemAdd.version_name && ss.version_build == itemAdd.version_build).FirstOrDefault();
            if (update_soft_version_existing != null)
            {
                modelState.AddModelError("", "Уже есть такая версия у этого софта");
                return null;
            }

            DateTime now = DateTime.Now;
            update_soft_version update_soft_version = new update_soft_version();
            ModelMapper.Map<UpdateSoftVersionViewModel, update_soft_version>(itemAdd, update_soft_version);
            update_soft_version.version_guid = System.Guid.NewGuid().ToString();
            update_soft_version.file_name = Path.GetFileName(update_soft_version.file_path);
            update_soft_version.xml_file_name = Path.GetFileName(update_soft_version.xml_file_path);
            update_soft_version.upload_date = now;
            update_soft_version.upload_user = currUser.UserName;

            dbContext.update_soft_version.Add(update_soft_version);
            dbContext.SaveChanges();

            return xGetItem(update_soft_version.version_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateSoftVersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            UpdateSoftVersionViewModel itemEdit = (UpdateSoftVersionViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры версии");
                return null;
            }

            update_soft_version update_soft_version = dbContext.update_soft_version.Where(ss => ss.version_id == itemEdit.version_id).FirstOrDefault();
            if (update_soft_version == null)
            {
                modelState.AddModelError("", "Не найдены параметры версии");
                return null;
            }

            update_soft_version update_soft_version_existing = dbContext.update_soft_version.Where(ss => ss.version_id != itemEdit.version_id && ss.soft_id == itemEdit.soft_id && ss.version_name == itemEdit.version_name && ss.version_build == itemEdit.version_build).FirstOrDefault();
            if (update_soft_version_existing != null)
            {
                modelState.AddModelError("", "Уже есть такая версия у этого софта");
                return null;
            }

            UpdateSoftVersionViewModel oldModel = new UpdateSoftVersionViewModel();
            ModelMapper.Map<update_soft_version, UpdateSoftVersionViewModel>(update_soft_version, oldModel);
            modelBeforeChanges = oldModel;

            DateTime now = DateTime.Now;

            DateTime? upload_date = update_soft_version.upload_date;
            string upload_user = update_soft_version.upload_user;

            string file_path = update_soft_version.file_path;
            string file_name = update_soft_version.file_name;
            if (itemEdit.file_path != update_soft_version.file_path)
            {
                file_path = itemEdit.file_path;
                file_name = Path.GetFileName(file_path);
                upload_date = now;
                upload_user = currUser.UserName;
            }

            string xml_file_path = update_soft_version.xml_file_path;
            string xml_file_name = update_soft_version.xml_file_name;
            if (itemEdit.xml_file_path != update_soft_version.xml_file_path)
            {
                xml_file_path = itemEdit.xml_file_path;
                xml_file_name = Path.GetFileName(xml_file_path);
                upload_date = now;
                upload_user = currUser.UserName;
            }

            ModelMapper.Map<UpdateSoftVersionViewModel, update_soft_version>(itemEdit, update_soft_version);

            update_soft_version.file_path = file_path;
            update_soft_version.file_name = file_name;
            update_soft_version.xml_file_path = xml_file_path;
            update_soft_version.xml_file_name = xml_file_name;
            update_soft_version.upload_date = upload_date;
            update_soft_version.upload_user = upload_user;

            dbContext.SaveChanges();

            return xGetItem(update_soft_version.version_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is UpdateSoftVersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            UpdateSoftVersionViewModel itemDel = (UpdateSoftVersionViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры версии");
                return false;
            }

            update_soft_version update_soft_version = dbContext.update_soft_version.Where(ss => ss.version_id == itemDel.version_id).FirstOrDefault();
            if (update_soft_version == null)
            {
                modelState.AddModelError("", "Не найдены параметры версии");
                return false;
            }

            string file_path = update_soft_version.file_path;
            string xml_file_path = update_soft_version.xml_file_path;

            dbContext.update_soft_version.Remove(update_soft_version);
            dbContext.SaveChanges();

            if (!String.IsNullOrEmpty(file_path))
            {
                string folder = Path.GetDirectoryName(file_path);
                if (Directory.Exists(folder))
                    Directory.Delete(folder, true);
            }

            if (!String.IsNullOrEmpty(xml_file_path))
            {
                string folder = Path.GetDirectoryName(xml_file_path);
                if (Directory.Exists(folder))
                    Directory.Delete(folder, true);
            }

            return true;            
        }

    }
}
