﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Adm
{
    public class LdapService : AuBaseService
    {
        private string username = "cn=Admin,dc=aptekaural,dc=ru";
        private string pwd = "Plyr21q";


        public Tuple<bool, string> CreateBusiness(string business_name)
        {
            if (System.Diagnostics.Debugger.IsAttached)
                return new Tuple<bool, string>(true, "");

            string uid = business_name;
            try
            {
                // create org in Users
                DirectoryEntry parentEntry = new DirectoryEntry("LDAP://10.0.1.2:389/ou=Users,dc=aptekaural,dc=ru", username, pwd, AuthenticationTypes.None);
                DirectoryEntry newUser = parentEntry.Children.Add("uid=" + uid, "account");
                //newUser.Properties["cn"].Add("123321");
                //newUser.Properties["sn"].Add("123321");
                newUser.CommitChanges();

                newUser.RefreshCache();
                //newUser.Properties["objectClass"].Add("uidObject");
                //newUser.Properties["uid"].Add("123321");
                newUser.Properties["objectClass"].Add("top");
                //newUser.Properties["userPassword"].Add("123");
                newUser.CommitChanges();

                return new Tuple<bool, string>(true, "");

            }
            catch (Exception ex)
            {
                ToLog("[CreateBusiness] " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.CABINET);
                return new Tuple<bool, string>(false, GlobalUtil.ExceptionInfo(ex));
            }
        }

        public Tuple<bool, string> CreateUser(string business_name, string user_name, string user_pwd)
        {
            if (System.Diagnostics.Debugger.IsAttached)
                return new Tuple<bool, string>(true, "");

            string uid = business_name;
            string new_user = user_name;
            string new_user_password = user_pwd;
            try
            {

                // create user
                DirectoryEntry parentEntry = new DirectoryEntry("LDAP://10.0.1.2:389/uid=" + uid + ",ou=Users,dc=aptekaural,dc=ru", username, pwd, AuthenticationTypes.None);
                DirectoryEntry newUser = parentEntry.Children.Add("cn=" + new_user, "person");
                //newUser.Properties["cn"].Add(new_user);
                newUser.Properties["sn"].Add(new_user);
                newUser.CommitChanges();

                newUser.RefreshCache();
                //newUser.Properties["objectClass"].Add("uidObject");
                //newUser.Properties["uid"].Add("123321");
                newUser.Properties["objectClass"].Add("top");
                newUser.Properties["userPassword"].Add(new_user_password);
                newUser.CommitChanges();

                return new Tuple<bool, string>(true, "");
            }
            catch (Exception ex)
            {
                ToLog("[CreateUser] " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.CABINET);
                return new Tuple<bool, string>(false, GlobalUtil.ExceptionInfo(ex));
            }
        }

        public Tuple<bool, string> CreateAdmin(string user_name, string user_pwd)
        {            
            if (System.Diagnostics.Debugger.IsAttached)
                return new Tuple<bool, string>(true, "");

            string uid = "1007661065565111774";
            string new_user = user_name;
            string new_user_password = user_pwd;
            try
            {

                // create user
                DirectoryEntry parentEntry = new DirectoryEntry("LDAP://10.0.1.2:389/uid=" + uid + ",ou=Administrators,dc=aptekaural,dc=ru", username, pwd, AuthenticationTypes.None);
                DirectoryEntry newUser = parentEntry.Children.Add("cn=" + new_user, "person");
                //newUser.Properties["cn"].Add(new_user);
                newUser.Properties["sn"].Add(new_user);
                newUser.CommitChanges();

                newUser.RefreshCache();
                //newUser.Properties["objectClass"].Add("uidObject");
                //newUser.Properties["uid"].Add("123321");
                newUser.Properties["objectClass"].Add("top");
                newUser.Properties["userPassword"].Add(new_user_password);
                newUser.CommitChanges();

                return new Tuple<bool, string>(true, "");
            }
            catch (Exception ex)
            {
                ToLog("[CreateAdmin] " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.CABINET);
                return new Tuple<bool, string>(false, GlobalUtil.ExceptionInfo(ex));
            }
        }

        public Tuple<bool, string> DeleteUser(string business_name, string user_name)
        {
            if (System.Diagnostics.Debugger.IsAttached)
                return new Tuple<bool, string>(true, "");

            string uid = business_name;
            string curr_user = user_name;
            string filter = "cn=" + curr_user;
            try
            {
                // delete user
                DirectoryEntry parentEntry = new DirectoryEntry("LDAP://10.0.1.2:389/uid=" + uid + ",ou=Users,dc=aptekaural,dc=ru", username, pwd, AuthenticationTypes.None);
                DirectoryEntry currUserEntry = parentEntry.Children.Find(filter);
                parentEntry.Children.Remove(currUserEntry);
                parentEntry.CommitChanges();
                //DirectoryEntry currUserEntry = new DirectoryEntry("LDAP://10.0.1.2:389/cn=" + curr_user  + ",uid=" + uid + ",ou=Users,dc=aptekaural,dc=ru", username, pwd, AuthenticationTypes.None);

                //MessageBox.Show(currUserEntry.Path);

                //currUserEntry.DeleteTree();
                //currUserEntry.CommitChanges();
                return new Tuple<bool, string>(true, "");
            }
            catch (Exception ex)
            {
                ToLog("[DeleteUser] " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.CABINET);
                return new Tuple<bool, string>(false, GlobalUtil.ExceptionInfo(ex));
            }
        }

        public Tuple<bool, string> ChangePassword(string business_name, string user_name, string user_new_pwd)
        {
            if (System.Diagnostics.Debugger.IsAttached)
                return new Tuple<bool, string>(true, "");

            var res1 = DeleteUser(business_name, user_name);
            if (!res1.Item1)
                return new Tuple<bool, string>(res1.Item1, res1.Item2);

            var res2 = CreateUser(business_name, user_name, user_new_pwd);
            if (!res2.Item1)
                return new Tuple<bool, string>(res2.Item1, res2.Item2);
            
            return new Tuple<bool, string>(true, "");
        }

        public Tuple<bool, string> DeleteBusiness(string business_name)
        {
            if (System.Diagnostics.Debugger.IsAttached)
                return new Tuple<bool, string>(true, "");

            string uid = business_name;
            string filter = "uid=" + uid;
            try
            {                
                DirectoryEntry parentEntry = new DirectoryEntry("LDAP://10.0.1.2:389/ou=Users,dc=aptekaural,dc=ru", username, pwd, AuthenticationTypes.None);
                DirectoryEntry currBusinessEntry = parentEntry.Children.Find(filter);

                DirectoryEntries children = currBusinessEntry.Children;
                foreach (var child in children.Cast<DirectoryEntry>())
                {
                    currBusinessEntry.Children.Remove(child);
                }
                currBusinessEntry.CommitChanges();

                parentEntry.Children.Remove(currBusinessEntry);
                parentEntry.CommitChanges();

                return new Tuple<bool, string>(true, "");
            }
            catch (Exception ex)
            {
                ToLog("[DeleteBusiness] " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.CABINET);
                return new Tuple<bool, string>(false, GlobalUtil.ExceptionInfo(ex));
            }
        }
    }
}