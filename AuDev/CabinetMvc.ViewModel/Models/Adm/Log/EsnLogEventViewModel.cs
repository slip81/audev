﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Exchange;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Adm
{
    public class EsnLogEventViewModel : AuBaseViewModel
    {
        public EsnLogEventViewModel()
        {
            //
        }

        public EsnLogEventViewModel(log_esn item)
        {
            log_id = item.log_id;
            date_beg = item.date_beg;
            mess = item.mess;
            user_name = item.user_name;
            log_event_type_id = item.log_esn_type_id;
            obj_id = item.obj_id;
            scope = item.scope;
            date_end = item.date_end;
            mess2 = item.mess2;
        }

        [Display(Name = "Код")]
        public long log_id { get; set; }
        [Display(Name = "Дата начала")]
        public System.DateTime date_beg { get; set; }
        [Display(Name = "Сообщение")]
        public string mess { get; set; }
        [Display(Name = "Пользователь")]
        public string user_name { get; set; }
        [Display(Name = "Тип события")]
        public Nullable<long> log_event_type_id { get; set; }
        [Display(Name = "Код объекта")]
        public Nullable<long> obj_id { get; set; }
        [Display(Name = "scope")]
        public Nullable<int> scope { get; set; }
        [Display(Name = "Дата окончания")]
        public Nullable<System.DateTime> date_end { get; set; }
        
        [Display(Name = "Доп. сообщение")]
        [JsonIgnore()]
        public string mess2 { get; set; }

        [Display(Name = "Тип события")]
        [JsonIgnore()]
        public string log_event_type_name { get; set; }

        [Display(Name = "Доп. сообщение")]
        public string mess2_exists { get { return String.IsNullOrWhiteSpace(mess2) ? "" : mess2.CutToLengthWithDots(100); } }
        //public string mess2_exists { get { return String.IsNullOrWhiteSpace(mess2) ? "" : "<a href='/GetLogDetailMess?log_id=" + log_id.ToString() + "' target='_blank'>подробно...</a>"; } }
        //public string mess2_exists { get { return "<a href='/GetLogDetailMess?log_id=" + log_id.ToString() + "' target='_blank'>подробно...</a>"; } }

    }
}