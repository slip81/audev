﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Adm
{
    public interface ILogCabService : IAuBaseService
    {
        //
    }

    public class LogCabService : AuBaseService, ILogCabService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.log_cab
                .Select(ss => new LogCabViewModel()
                {
                    log_id = ss.log_id,
                    date_beg = ss.date_beg,
                    mess = ss.mess,
                    user_name = ss.user_name,
                    obj_type = ss.obj_type,
                    obj_type_name = ss.obj_type_name,
                    obj1_id = ss.obj1_id,
                    obj1_value = ss.obj1_value,                    
                    obj2_id = ss.obj2_id,
                    obj2_value = ss.obj2_value,
                    mess2 = ss.mess2,
                }
                );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.log_cab.Where(ss => ss.log_id == id).FirstOrDefault();
            return res != null ? new LogCabViewModel(res) : null;
        }
        
    }
}
