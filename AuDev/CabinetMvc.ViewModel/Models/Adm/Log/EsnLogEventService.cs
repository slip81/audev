﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Exchange;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Adm
{
    public interface IEsnLogEventService : IAuBaseService
    {
        IQueryable<EsnLogEventViewModel> GetList_byScope(int scope, bool error_only);
        string GetLogDetailMess(long log_id);
    }

    public class EsnLogEventService : AuBaseService, IEsnLogEventService
    {
        public IQueryable<EsnLogEventViewModel> GetList_byScope(int scope, bool error_only)
        {
            return dbContext.log_esn
                .Where(ss => (((scope > 0) && (ss.scope == scope)) || (scope <= 0))
                    && (((error_only) && (ss.log_esn_type_id == (int)Enums.LogEsnType.ERROR)) || (!error_only))
                )
                .Select(ss => new EsnLogEventViewModel()
                {
                    log_id = ss.log_id,
                    date_beg = ss.date_beg,
                    mess = ss.mess,
                    user_name = ss.user_name,
                    log_event_type_id = ss.log_esn_type_id,
                    log_event_type_name = ss.log_esn_type.type_name,
                    obj_id = ss.obj_id,
                    scope = ss.scope,
                    date_end = ss.date_end,
                    mess2 = ss.mess2,
                }
                );
        }

        public string GetLogDetailMess(long log_id)
        {
            //return dbContext.log_esn.Where(ss => ss.log_id == log_id).Select(ss => ss.mess2).FirstOrDefault();
            return dbContext.log_esn_data.Where(ss => ss.log_id == log_id).Select(ss => ss.data_text).FirstOrDefault();
        }
    }
}
