﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Adm
{
    public interface IDiscountLogEventService : IAuBaseService
    {
        //
    }

    public class DiscountLogEventService : AuBaseService, IDiscountLogEventService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return from le in dbContext.log_event
                   from b in dbContext.business.Where(o => le.business_id == o.business_id).DefaultIfEmpty()
                   where le.scope == (int)Enums.LogScope.DISCOUNT
                   select new DiscountLogEventViewModel()
                    {
                        log_id = le.log_id,
                        date_beg = le.date_beg,
                        mess = le.mess,
                        session_id = le.session_id,
                        business_id = le.business_id,
                        user_name = le.user_name,
                        log_event_type_id = le.log_event_type_id,
                        log_event_type_name = le.log_event_type.type_name,
                        obj_id = le.obj_id,
                        scope = le.scope,
                        date_end = le.date_end,
                        mess2 = le.mess2,
                        business_name = b.business_name,
                    }
                    ;
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            Enums.LogScope logScope = Enums.LogScope.DISCOUNT;
            try
            {
                logScope = (Enums.LogScope)parent_id;
            }
            catch
            {
                return null;
            }

            switch (logScope)
            {
                case Enums.LogScope.DISCOUNT:
                    return xGetList();
                default:
                    return null;
            }           
        }
        
    }
}
