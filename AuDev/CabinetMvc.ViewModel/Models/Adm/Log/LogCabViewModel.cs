﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Adm
{
    public class LogCabViewModel : AuBaseViewModel
    {
        public LogCabViewModel()
        {
            //
        }

        public LogCabViewModel(log_cab item)
        {
            log_id = item.log_id;
            date_beg = item.date_beg;
            mess = item.mess;
            user_name = item.user_name;
            obj_type = item.obj_type;
            obj_type_name = item.obj_type_name;
            obj1_id = item.obj1_id;
            obj1_value = item.obj1_value;
            obj2_id = item.obj2_id;
            obj2_value = item.obj2_value;
            mess2 = item.mess2;
                     
            if (!String.IsNullOrEmpty(obj1_value))
            {
                obj1 = new LogObject() 
                {
                    obj_type = obj_type,
                    obj_id = obj1_id,                    
                    obj_value = obj1_value,
                    
                };
            }

            if (!String.IsNullOrEmpty(obj2_value))
            {
                obj2 = new LogObject()
                {
                    obj_type = obj_type,
                    obj_id = obj2_id,                    
                    obj_value = obj2_value,
                };
            }
            
        }

        public long log_id { get; set; }
        public System.DateTime date_beg { get; set; }
        public string mess { get; set; }
        public string user_name { get; set; }
        public Nullable<int> obj_type { get; set; }
        public string obj_type_name { get; set; }
        public Nullable<long> obj1_id { get; set; }
        public string obj1_value { get; set; }        
        public Nullable<long> obj2_id { get; set; }
        public string obj2_value { get; set; }
        public string mess2 { get; set; }

        [JsonIgnore()]
        public string obj1_id_str { get { return obj1_id.ToString(); } }
        [JsonIgnore()]
        public string obj2_id_str { get { return obj2_id.ToString(); } }

        [JsonIgnore()]
        public LogObject obj1 { get; set; }
        [JsonIgnore()]
        public LogObject obj2 { get; set; }
        [JsonIgnore()]
        public AuBaseViewModel modelObj1 { get; set; }
        [JsonIgnore()]
        public AuBaseViewModel modelObj2 { get; set; }

        public string obj_enum_name { get { return obj_type.HasValue ? obj_type.EnumName<Enums.MainObjectType>() : ""; } }
    }
}