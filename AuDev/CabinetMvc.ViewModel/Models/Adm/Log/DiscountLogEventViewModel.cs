﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public class DiscountLogEventViewModel : AuBaseViewModel
    {
        public DiscountLogEventViewModel()
        {
            //
        }

        public DiscountLogEventViewModel(log_event item)
        {
            log_id = item.log_id;
            date_beg = item.date_beg;
            mess = item.mess;
            session_id = item.session_id;
            business_id = item.business_id;
            user_name = item.user_name;
            log_event_type_id = item.log_event_type_id;
            obj_id = item.obj_id;
            scope = item.scope;
            date_end = item.date_end;
            mess2 = item.mess2;
        }

        [Display(Name = "Код")]
        public long log_id { get; set; }
        [Display(Name = "Дата начала")]
        public System.DateTime date_beg { get; set; }
        [Display(Name = "Сообщение")]
        public string mess { get; set; }
        [Display(Name = "Код сессии")]
        public string session_id { get; set; }
        [Display(Name = "Код организации")]
        public Nullable<long> business_id { get; set; }
        [Display(Name = "Пользователь")]
        public string user_name { get; set; }
        [Display(Name = "Тип события")]
        public Nullable<long> log_event_type_id { get; set; }
        [Display(Name = "Код объекта")]
        public Nullable<long> obj_id { get; set; }
        [Display(Name = "scope")]
        public Nullable<int> scope { get; set; }
        [Display(Name = "Дата окончания")]
        public Nullable<System.DateTime> date_end { get; set; }
        [Display(Name = "Сообщение 2")]
        public string mess2 { get; set; }
    

        [Display(Name = "Тип события")]
        [JsonIgnore()]
        public string log_event_type_name { get; set; }
        [Display(Name = "Код организации")]
        public string business_id_str { get { return business_id.HasValue ? business_id.ToString() : ""; } }
        [Display(Name = "Организация")]
        [JsonIgnore()]
        public string business_name { get; set; }

    }
}