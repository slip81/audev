﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Adm
{
    public class LogObject
    {
        public LogObject()
        {
            //
        }

        public LogObject(AuBaseViewModel item)
        {
            obj_type = item.GetObjType() == Enums.MainObjectType.NONE ? null : (int?)item.GetObjType();
            obj_value = SerializeObjValue(item);
            obj_id = item.GetKeyValue();
            obj_type_name = item.GetType().FullName;
        }

        public LogObject(long id)
        {            
            obj_value = "";
            obj_id = id;
        }

        public string SerializeObjValue(AuBaseViewModel item)
        {
            return item == null ? "" : JsonConvert.SerializeObject(item);
        }

        public object DeserializeObjValue(string typeName)
        {
            if (String.IsNullOrEmpty(typeName))
                return null;

            var objVM = Activator.CreateInstance(Type.GetType(typeName));
            Type baseModelItemType = Type.GetType(objVM.GetType().FullName);

            //var res = ViewModelUtil.GetBaseModelByMainObject(obj_type);
            //Type baseModelItemType = Type.GetType(res.GetType().FullName);
            return String.IsNullOrEmpty(obj_value) ? null : JsonConvert.DeserializeObject(obj_value, baseModelItemType);
        }
        
        public Nullable<long> obj_id { get; set; }
        public string obj_value { get; set; }
        public Nullable<int> obj_type { get; set; }
        public string obj_type_name { get; set; }
        public Enums.MainObjectType obj_type_enum
        {
            get 
            {
                Enums.MainObjectType _tmp = Enums.MainObjectType.NONE;
                if (obj_type.HasValue)
                {
                    bool ok = Enum.TryParse<Enums.MainObjectType>(obj_type.ToString(), out _tmp);
                    if (!ok)
                        _tmp = Enums.MainObjectType.NONE;
                }
                return _tmp; 
            } 
        }
    }


}