﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.Auth;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.User;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public interface ILoggerService
    {
        bool ToLog(string mess, long? log_type_id, LogObject log1 = null, LogObject log2 = null, string mess2 = null);
    }

    public class LoggerService : ILoggerService, IDisposable
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
   
        private const string dbName = "Cabinet";
        //private static string dbName = "CardDbDev";
        //private static string dbName = "CardDbProd";

        protected AuMainDb dbContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = new AuMainDb();
                }
                return _dbContext;
            }
        }
        private AuMainDb _dbContext;

        public UserViewModel currUser { get; set; }

        public LoggerService()
        {
            var auth = DependencyResolver.Current.GetService<IAuthentication>();
            currUser = ((auth == null) || (auth.CurrentUser == null)) ? null : ((IUserProvider)auth.CurrentUser.Identity).User;
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                //logger.Info("_dbContext.Dispose() " + GetType().Name);
                _dbContext.Dispose();
            }
        }

        private bool toLog_Db(LogCabViewModel item)
        {            
            log_cab log_cab = new log_cab();
            log_cab.date_beg = DateTime.Now;
            log_cab.mess = item.mess;
            log_cab.user_name = item.user_name;
            log_cab.obj_type = item.obj_type;
            log_cab.obj_type_name = item.obj_type_name;
            log_cab.obj1_id = item.obj1_id;
            log_cab.obj1_value = item.obj1_value;            
            log_cab.obj2_id = item.obj2_id;
            log_cab.obj2_value = item.obj2_value;
            log_cab.mess2 = item.mess2;
            dbContext.log_cab.Add(log_cab);
            dbContext.SaveChanges();
            return true;
        }

        public bool ToLog(string mess, long? log_type_id, LogObject log1 = null, LogObject log2 = null, string mess2 = null)
        {
            if (currUser == null)
                return false;
            LogCabViewModel item = new LogCabViewModel();
            item.date_beg = DateTime.Now;
            item.mess = mess;
            item.mess2 = mess2;
            item.user_name = currUser.UserName;
            item.obj_type = log1 != null ? log1.obj_type : log2.obj_type;
            item.obj_type_name = log1 != null ? log1.obj_type_name : log2.obj_type_name;
            if (log1 != null)
            {
                item.obj1_id = log1.obj_id;
                item.obj1_value = log1.obj_value;                
            }
            if (log2 != null)
            {
                item.obj2_id = log2.obj_id;
                item.obj2_value = log2.obj_value;
            }
            return toLog_Db(item);
        }
        
    }
}
