﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public class CabUserRoleViewModel : AuBaseViewModel
    {
        public CabUserRoleViewModel()
            : base(Enums.MainObjectType.CAB_USER_ROLE)
        {
            //
        }

        public CabUserRoleViewModel(cab_user_role item)
            : base(Enums.MainObjectType.CAB_USER_ROLE)
        {
            item_id = item.item_id;
            role_id = item.role_id;
            user_name = item.user_name;
            use_defaults = item.use_defaults;
            role_name = "";
            org_name = "";
            business_name = "";
            business_id = null;
            use_defaults_str = item.use_defaults ? "Да" : "Нет";
            UserRoleList = new List<CabRoleViewModel>();
        }

        public CabUserRoleViewModel(vw_cab_user item)
            : base(Enums.MainObjectType.CAB_USER_ROLE)
        {
            item_id = item.item_id;
            role_id = item.role_id;
            user_name = item.user_name;
            use_defaults = item.use_defaults;
            role_name = item.role_name;
            org_name = item.org_name;
            business_name = item.business_name;
            business_id = item.business_id;
            use_defaults_str = item.use_defaults ? "Да" : "Нет";
            //
            UserRoleList = new List<CabRoleViewModel>();
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Display(Name = "Код роли")]
        [Required(ErrorMessage = "Не задана роль")]
        public long role_id { get; set; }

        [Display(Name = "Пользователь")]
        [Required(ErrorMessage = "Не задан логин пользователя")]
        public string user_name { get; set; }     

        [Display(Name = "Действия по умолчанию")]
        public bool use_defaults { get; set; }

        [Display(Name = "Роль")]
        [JsonIgnore()]
        public string role_name { get; set; }
        
        [Display(Name = "Отделение")]
        [JsonIgnore()]
        public string org_name { get; set; }

        [Display(Name = "Организация")]
        [JsonIgnore()]
        public string business_name { get; set; }

        [Display(Name = "Код организации")]
        [JsonIgnore()]
        public Nullable<long> business_id { get; set; }

        [Display(Name = "Действия по умолчанию")]
        [JsonIgnore()]
        public string use_defaults_str { get; set; }

        [JsonIgnore()]
        public IEnumerable<CabRoleViewModel> UserRoleList { get; set; }        

    }
}