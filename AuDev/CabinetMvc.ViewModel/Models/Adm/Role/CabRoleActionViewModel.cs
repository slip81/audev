﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public class CabRoleActionViewModel : AuBaseViewModel
    {
        public CabRoleActionViewModel()
            : base(Enums.MainObjectType.CAB_ROLE_ACTION)
        {
            //
        }

        public CabRoleActionViewModel(cab_role_action item)
            : base(Enums.MainObjectType.CAB_ROLE_ACTION)
        {
            item_id = item.item_id;
            role_id = item.role_id;
            group_id = item.group_id;
            action_id = item.action_id;
            group_name = "";
            action_name = "";
            have_group = false;
            have_action = false;
        }

        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Display(Name = "Код роли")]
        [Required(ErrorMessage = "Не задана роль")]
        public long role_id { get; set; }

        [Display(Name = "Код группы действий")]
        public Nullable<long> group_id { get; set; }        

        [Display(Name = "Код действия")]
        public Nullable<long> action_id { get; set; }

        [Display(Name = "Название группы")]
        [JsonIgnore()]
        public string group_name { get; set; }

        [Display(Name = "Название действия")]
        [JsonIgnore()]
        public string action_name { get; set; }

        [Display(Name = "Имеет группу")]
        [JsonIgnore()]
        public bool have_group { get; set; }

        [Display(Name = "Имеет действие")]
        [JsonIgnore()]
        public bool have_action { get; set; }

    }
}