﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public interface ICabRoleService : IAuBaseService
    {
        //
    }

    public class CabRoleService : AuBaseService, ICabRoleService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.cab_role
                .Select(ss => new CabRoleViewModel()
                {
                    role_id = ss.role_id,
                    role_name = ss.role_name,
                }
                );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.cab_role.Where(ss => ss.role_id == id).FirstOrDefault();
            return res != null ? new CabRoleViewModel(res) : null;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            CabRoleViewModel roleAdd = (CabRoleViewModel)item;
            cab_role cab_role = null;
            var existing_role = dbContext.cab_role.Where(ss => ss.role_name.Trim().ToLower().Equals(roleAdd.role_name.Trim().ToLower())).FirstOrDefault();
            if (existing_role != null)
            {
                modelState.AddModelError("", "Уже есть такая роль");
                return null;
            }
            cab_role = new cab_role();
            cab_role.role_name = roleAdd.role_name;
            cab_role.role_id = dbContext.cab_role.Max(ss => ss.role_id) + 1;
            dbContext.cab_role.Add(cab_role);
            dbContext.SaveChanges();

            return new CabRoleViewModel(cab_role);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            CabRoleViewModel roleEdit = (CabRoleViewModel)item;
            cab_role cab_role = null;
            var existing_role = dbContext.cab_role.Where(ss => ss.role_id != roleEdit.role_id && ss.role_name.Trim().ToLower().Equals(roleEdit.role_name.Trim().ToLower())).FirstOrDefault();
            if (existing_role != null)
            {
                modelState.AddModelError("", "Уже есть такая роль");
                return null;
            }
            cab_role = dbContext.cab_role.Where(ss => ss.role_id == roleEdit.role_id).FirstOrDefault();
            if (cab_role != null)
            {
                modelState.AddModelError("", "Не найдена роль с кодом " + roleEdit.role_id.ToString());
                return null;
            }

            cab_role.role_name = roleEdit.role_name;
            dbContext.SaveChanges();

            return new CabRoleViewModel(cab_role);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            CabRoleViewModel roleDel = (CabRoleViewModel)item;
            var existing_user = dbContext.cab_user_role.Where(ss => ss.role_id == roleDel.role_id).FirstOrDefault();
            if (existing_user != null)
            {
                modelState.AddModelError("", "Есть пользователи в этой роли");
                return false;
            }

            dbContext.cab_role_action.RemoveRange(dbContext.cab_role_action.Where(ss => ss.role_id == roleDel.role_id));
            dbContext.cab_role.Remove(dbContext.cab_role.Where(ss => ss.role_id == roleDel.role_id).FirstOrDefault());
            dbContext.SaveChanges();

            return true;
        }
        
    }
}
