﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;
using CabinetMvc.ViewModel.Cabinet;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Adm
{
    public interface ICabUserService : IAuBaseService
    {
        IQueryable<CabUserViewModel> GetList_Executer();
        IQueryable<CabUserViewModel> GetList_Actual();
        IQueryable<CabUserViewModel> GetList_CrmUser();
        bool ChangePassword(string pwd1, string pwd2, string pwd3, ModelStateDictionary modelState);
        bool RestorePassword(string user_email, ModelStateDictionary modelState);
    }

    public class CabUserService : AuBaseService, ICabUserService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_cab_user2                                
                .Select(ss => new CabUserViewModel()
                {
                    user_id = ss.user_id,
                    user_name = ss.user_name,
                    user_login = ss.user_login,                    
                    is_superadmin = ss.is_superadmin,
                    full_name = ss.full_name,
                    email = ss.email,
                    is_crm_user = ss.is_crm_user,
                    is_executer = ss.is_executer,
                    crm_user_role_id = ss.crm_user_role_id,
                    icq = ss.icq,
                    is_active = ss.is_active,                    
                    crm_user_role_name = ss.crm_user_role_name,
                    auth_role_name = ss.auth_role_name,
                    auth_role_id = ss.auth_role_id,
                    pwd = "***",                    
                    is_male = ss.is_male,
                    is_notify_prj = ss.is_notify_prj,
                }
                )
                ;
        }

        public IQueryable<CabUserViewModel> GetList_Executer()
        {
            return dbContext.cab_user.Where(ss => ss.is_executer).AsEnumerable()
            .Select(ss => new CabUserViewModel
            {
                user_id = ss.user_id,
                user_name = ss.user_name,
            })
            .AsQueryable()
            .OrderBy(ss => ss.user_name);
        }


        public IQueryable<CabUserViewModel> GetList_CrmUser()
        {
            return dbContext.cab_user.Where(ss => ss.is_crm_user).AsEnumerable()
            .Select(ss => new CabUserViewModel
            {
                user_id = ss.user_id,
                user_name = ss.user_name,
                crm_user_role_id = ss.crm_user_role_id,
            })
            .AsQueryable()
            .OrderBy(ss => ss.user_name);
        }

        public IQueryable<CabUserViewModel> GetList_Actual()
        {
            return dbContext.vw_cab_user2
                .Where(ss => ss.user_id > 0)
                .Select(ss => new CabUserViewModel()
                {
                    user_id = ss.user_id,
                    user_name = ss.user_name,
                    user_login = ss.user_login,
                    is_superadmin = ss.is_superadmin,
                    full_name = ss.full_name,
                    email = ss.email,
                    is_crm_user = ss.is_crm_user,
                    is_executer = ss.is_executer,
                    crm_user_role_id = ss.crm_user_role_id,
                    icq = ss.icq,
                    is_active = ss.is_active,
                    crm_user_role_name = ss.crm_user_role_name,
                    auth_role_name = ss.auth_role_name,
                    auth_role_id = ss.auth_role_id,
                    pwd = "***",
                    is_male = ss.is_male,
                    is_notify_prj = ss.is_notify_prj,
                }
                )
                ;
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_cab_user2
                .Where(ss => ss.user_id == id)
                .Select(ss => new CabUserViewModel()
                {
                    user_id = ss.user_id,
                    user_name = ss.user_name,
                    user_login = ss.user_login,                    
                    is_superadmin = ss.is_superadmin,
                    full_name = ss.full_name,
                    email = ss.email,
                    is_crm_user = ss.is_crm_user,
                    is_executer = ss.is_executer,
                    crm_user_role_id = ss.crm_user_role_id,
                    icq = ss.icq,
                    is_active = ss.is_active,
                    crm_user_role_name = ss.crm_user_role_name,
                    auth_role_name = ss.auth_role_name,
                    auth_role_id = ss.auth_role_id,
                    pwd = "***",
                    is_male = ss.is_male,
                    is_notify_prj = ss.is_notify_prj,
                }
                )
                .FirstOrDefault()
                ;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            CabUserViewModel itemAdd = (CabUserViewModel)item;

            if (String.IsNullOrEmpty(itemAdd.user_name))
            {
                modelState.AddModelError("", "Не задан логин");
                return null;
            }

            if (String.IsNullOrEmpty(itemAdd.pwd))
            {
                modelState.AddModelError("", "Не задан пароль");
                return null;
            }
            
            /*
            using (var ldapServ = new LdapService())
            {
                var res = ldapServ.CreateAdmin(itemAdd.user_login.Trim(), itemAdd.pwd.Trim());
                if (!res.Item1)
                {
                    modelState.AddModelError("", res.Item2);
                    return null;
                }
            }
            */

            cab_user cab_user = new cab_user();
            cab_user.email = itemAdd.email;
            cab_user.full_name = itemAdd.full_name;
            cab_user.icq = itemAdd.icq;            
            cab_user.is_superadmin = false;
            cab_user.user_login = itemAdd.user_login.Trim();
            cab_user.user_name = itemAdd.user_name;
            cab_user.is_active = itemAdd.is_active;
            cab_user.is_crm_user = itemAdd.is_crm_user;
            cab_user.is_executer = itemAdd.is_executer;
            cab_user.crm_user_role_id = itemAdd.crm_user_role_id;
            //cab_user.crm_user_role_id = itemAdd.CrmUserRole == null ? null : (int?)itemAdd.CrmUserRole.role_id;
            cab_user.is_male = true;
            cab_user.is_notify_prj = itemAdd.is_notify_prj;
            dbContext.cab_user.Add(cab_user);
            dbContext.SaveChanges();

            ClientUserViewModel clientUserVM = new ClientUserViewModel();
            clientUserVM.client_id = 1000; // Аурит
            clientUserVM.sales_id = null;
            clientUserVM.is_active = itemAdd.is_active ? (short)1 : (short)0;
            clientUserVM.is_for_sales = false;
            clientUserVM.is_main_for_sales = false;
            clientUserVM.need_change_pwd = false;
            clientUserVM.user_email = itemAdd.email;
            clientUserVM.user_login = itemAdd.user_login.Trim();
            clientUserVM.user_name = itemAdd.user_name;
            clientUserVM.user_name_full = itemAdd.full_name;
            clientUserVM.user_pwd = itemAdd.pwd.Trim();
            clientUserVM.role_id = itemAdd.auth_role_id;            
            //clientUserVM.role_id = itemAdd.AuthRole != null ? (int?)itemAdd.AuthRole.role_id : null;
            clientUserVM.user_id = cab_user.user_id;
            var clientUserService = DependencyResolver.Current.GetService<IClientUserService>();
            clientUserService.Insert(clientUserVM, modelState);

            int user_id_ETALON = 1;

            List<cab_grid_column_user_settings> cab_grid_column_user_settings_list = dbContext.cab_grid_column_user_settings.Where(ss => ss.user_id == user_id_ETALON).ToList();
            if ((cab_grid_column_user_settings_list != null) && (cab_grid_column_user_settings_list.Count > 0))
            {
                foreach (var cab_grid_column_user_settings in cab_grid_column_user_settings_list)
                {
                    cab_grid_column_user_settings cab_grid_column_user_settings_new = new cab_grid_column_user_settings();
                    cab_grid_column_user_settings_new.column_id = cab_grid_column_user_settings.column_id;
                    cab_grid_column_user_settings_new.column_num = cab_grid_column_user_settings.column_num;
                    cab_grid_column_user_settings_new.is_filterable = cab_grid_column_user_settings.is_filterable;
                    cab_grid_column_user_settings_new.is_sortable = cab_grid_column_user_settings.is_sortable;
                    cab_grid_column_user_settings_new.is_visible = cab_grid_column_user_settings.is_visible;
                    cab_grid_column_user_settings_new.user_id = cab_user.user_id;
                    cab_grid_column_user_settings_new.width = cab_grid_column_user_settings.width;
                    dbContext.cab_grid_column_user_settings.Add(cab_grid_column_user_settings_new);
                }
            }

            List<crm_notify> crm_notify_list = dbContext.crm_notify.Where(ss => ss.user_id == user_id_ETALON).ToList();

            crm_notify crm_notify = new crm_notify();
            crm_notify.email = itemAdd.email;
            crm_notify.icq = itemAdd.icq;
            crm_notify.is_active_email = itemAdd.is_notify_prj;
            crm_notify.is_active_icq = false;
            crm_notify.user_id = cab_user.user_id;
            dbContext.crm_notify.Add(crm_notify);

            if ((crm_notify_list != null) && (crm_notify_list.Count > 0))
            {
                foreach (var crm_notify_list_item in crm_notify_list)
                {
                    crm_notify_param_user crm_notify_param_user = new crm_notify_param_user();
                    crm_notify_param_user.crm_notify = crm_notify;
                    crm_notify_param_user.is_executer = false;
                    crm_notify_param_user.is_owner = false;
                    crm_notify_param_user.user_id = cab_user.user_id;
                    crm_notify_param_user.transport_type = (int)Enums.NotifyTransportTypeEnum.ICQ;
                    dbContext.crm_notify_param_user.Add(crm_notify_param_user);

                    crm_notify_param_user = new crm_notify_param_user();
                    crm_notify_param_user.crm_notify = crm_notify;
                    crm_notify_param_user.is_executer = false;
                    crm_notify_param_user.is_owner = false;
                    crm_notify_param_user.user_id = cab_user.user_id;
                    crm_notify_param_user.transport_type = (int)Enums.NotifyTransportTypeEnum.EMAIL;
                    dbContext.crm_notify_param_user.Add(crm_notify_param_user);
                }
            }

            dbContext.SaveChanges();

            var vw_cab_user = dbContext.vw_cab_user2.Where(ss => ss.user_id == cab_user.user_id).FirstOrDefault();
            return new CabUserViewModel(vw_cab_user);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            CabUserViewModel itemEdit = (CabUserViewModel)item;

            cab_user cab_user = dbContext.cab_user.Where(ss => ss.user_id == itemEdit.user_id).FirstOrDefault();
            if (cab_user == null)
            {
                modelState.AddModelError("", "Не найден пользователь с кодом " + itemEdit.user_id.ToString());
                return null;
            }
            

            modelBeforeChanges = new CabUserViewModel(cab_user);

            if ((cab_user.is_active) && (!itemEdit.is_active))
            {
                my_aspnet_membership membership = dbContext.my_aspnet_membership.Where(ss => ss.userId == itemEdit.user_id).FirstOrDefault();
                if (membership != null)
                {
                    membership.IsApproved = 0;
                }
            }

            crm_notify crm_notify = dbContext.crm_notify.Where(ss => ss.user_id == itemEdit.user_id).FirstOrDefault();
            if (cab_user.is_notify_prj != itemEdit.is_notify_prj)
            {
                if (itemEdit.is_notify_prj)
                {            
                    if (crm_notify != null)
                    {
                        crm_notify.is_active_email = true;
                        crm_notify.email = itemEdit.email;
                    }
                    else
                    {
                        crm_notify = new crm_notify();
                        crm_notify.user_id = itemEdit.user_id;                        
                        crm_notify.is_active_email = true;
                        crm_notify.email = itemEdit.email;
                        dbContext.crm_notify.Add(crm_notify);
                    }
                }
                else
                {
                    if (crm_notify != null)
                    {
                        crm_notify.is_active_email = false;
                        crm_notify.email = null;
                    }
                }
            }

            cab_user.full_name = itemEdit.full_name;
            cab_user.user_name = itemEdit.user_name;
            cab_user.email = itemEdit.email;
            cab_user.icq = itemEdit.icq;
            cab_user.is_active = itemEdit.is_active;
            cab_user.is_crm_user = itemEdit.is_crm_user;
            cab_user.is_executer = itemEdit.is_executer;
            cab_user.crm_user_role_id = itemEdit.crm_user_role_id;
            cab_user.is_notify_prj = itemEdit.is_notify_prj;

            var clientUserService = DependencyResolver.Current.GetService<IClientUserService>();
            ClientUserViewModel clientUserVM = (ClientUserViewModel)clientUserService.GetItem(cab_user.user_id);
            if (clientUserVM != null)
            {
                clientUserVM.user_name = itemEdit.user_name;
                clientUserVM.user_name_full = itemEdit.full_name;
                clientUserVM.role_id = itemEdit.auth_role_id;
                clientUserService.Update(clientUserVM, modelState);
            }

            dbContext.SaveChanges();

            var vw_cab_user = dbContext.vw_cab_user2.Where(ss => ss.user_id == itemEdit.user_id).FirstOrDefault();
            return new CabUserViewModel(vw_cab_user);            
        }

        public bool ChangePassword(string pwd1, string pwd2, string pwd3, ModelStateDictionary modelState)
        {
            if (String.IsNullOrEmpty(pwd1))
            {
                modelState.AddModelError("","Не задан пароль");
                return false;
            }
            
            if (((pwd2 == null) && (pwd3 != null)) || ((pwd2 != null) && (pwd3 == null)))
            {
                modelState.AddModelError("", "Пароли не совпадают");
                return false;
            }

            if ((pwd2 != null) && (!(pwd2.Trim().Equals(pwd3.Trim()))))
            {
                modelState.AddModelError("", "Пароли не совпадают");
                return false;
            }

            my_aspnet_membership user = dbContext.my_aspnet_membership.Where(ss => ss.userId == currUser.CabUserId).FirstOrDefault();
            if (user == null)
            {
                modelState.AddModelError("", "Не найден текущий пользователь");
                return false;
            }

            EncrHelper2 encr = new EncrHelper2();
            string pwd1_encoded = encr._SetHashValue(pwd1);
            string user_pwd_encoded = user.Password;
            if (!(pwd1_encoded.Trim().Equals(user_pwd_encoded.Trim())))
            {
                modelState.AddModelError("", "Введен неверный текущий пароль");
                return false;
            }

            user.Password = encr._SetHashValue(pwd2);
            dbContext.SaveChanges();

            return true;
        }

        public bool RestorePassword(string user_email, ModelStateDictionary modelState)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(user_email))
                {
                    modelState.AddModelError("", "Не указан email");
                    return false;
                }

                my_aspnet_membership user = dbContext.my_aspnet_membership.Where(ss => ss.Email != null && ss.Email.Trim().ToLower().Equals(user_email.Trim().ToLower())).FirstOrDefault();
                if (user == null)
                {
                    modelState.AddModelError("", "Не найден email");
                    return false;
                }

                string user_pwd_encoded = "";
                EncrHelper2 encr = new EncrHelper2();
                try
                {
                    user_pwd_encoded = encr._GetHashValue(user.Password);
                }
                finally
                {
                    encr = null;
                }

                GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", user_email, "Ваш пароль для входа в ЛК", user_pwd_encoded, null, true);

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }        
    }
}
