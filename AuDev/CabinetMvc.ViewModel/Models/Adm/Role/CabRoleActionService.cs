﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public interface ICabRoleActionService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList_AllActions(long id);
        bool UpdateActions(long role_id, List<long> action_id_list, ModelStateDictionary modelState);
    }

    public class CabRoleActionService : AuBaseService, ICabRoleActionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.cab_role_action
                .Select(ss => new CabRoleActionViewModel()
                {
                    role_id = ss.role_id,
                    item_id = ss.item_id,
                    group_id = ss.group_id,
                    action_id = ss.action_id,
                }
                );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.cab_role_action.Where(ss => ss.item_id == id).FirstOrDefault();
            return res != null ? new CabRoleActionViewModel(res) : null;
        }

        public IQueryable<AuBaseViewModel> GetList_AllActions(long id)
        {
            var res = from ca in dbContext.cab_action
                      from cag in dbContext.cab_action_group
                      where ca.group_id == cag.group_id
                      join cra in dbContext.cab_role_action on id equals cra.role_id into cra2
                      from cra_item in cra2
                        //.Where(ss => (ss.group_id == ca.group_id && !ss.action_id.HasValue) || (ss.group_id == ca.group_id && ss.action_id == ca.action_id))
                        .Where(ss => (ss.group_id == ca.group_id && !ss.action_id.HasValue) || (ss.action_id == ca.action_id && !ss.group_id.HasValue))
                        .DefaultIfEmpty()
                      select  new CabRoleActionViewModel()
                        {
                            role_id = id,
                            item_id = 0,
                            group_id = ca.group_id,
                            action_id = ca.action_id,
                            group_name = cag.group_name,
                            action_name = ca.action_name,
                            have_group = cra_item.group_id.HasValue,
                            have_action = (cra_item.group_id.HasValue) || (cra_item.action_id.HasValue),
                        };

            return res;
        }

        public bool UpdateActions(long role_id, List<long> action_id_list,  ModelStateDictionary modelState)
        {
            List<long> full_group_id_list = new List<long>();
            cab_role_action new_action = null;

            dbContext.cab_role_action.RemoveRange(dbContext.cab_role_action.Where(ss => ss.role_id == role_id));

            if ((action_id_list != null) && (action_id_list.Count > 0))
            {
                var new_actions = dbContext.cab_action.Where(ss => action_id_list.Contains(ss.action_id)).ToList();

                foreach (var item_group in new_actions.Select(ss => ss.group_id).Distinct().ToList())
                {
                    var actions_count_in_db = dbContext.cab_action.Where(ss => ss.group_id == item_group).Count();
                    var actions_count_in_list = new_actions.Where(ss => ss.group_id == item_group).Count();
                    if (actions_count_in_db == actions_count_in_list)
                    {
                        full_group_id_list.Add(item_group);
                    }
                }

                foreach (var item in full_group_id_list)
                {
                    new_action = new cab_role_action();
                    new_action.role_id = role_id;
                    new_action.group_id = item;
                    new_action.action_id = null;
                    dbContext.cab_role_action.Add(new_action);
                }

                foreach (var item in new_actions.Where(ss => !full_group_id_list.Contains(ss.group_id)))
                {
                    new_action = new cab_role_action();
                    new_action.role_id = role_id;
                    new_action.group_id = null;
                    new_action.action_id = item.action_id;
                    dbContext.cab_role_action.Add(new_action);
                }
            }

            dbContext.SaveChanges();

            return true;
        }
        
    }
}

