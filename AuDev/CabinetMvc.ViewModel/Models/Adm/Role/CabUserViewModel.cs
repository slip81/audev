﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public class CabUserViewModel : AuBaseViewModel
    {
        public CabUserViewModel()
            : base(Enums.MainObjectType.CAB_USER)
        {
            //
        }

        public CabUserViewModel(cab_user item, int? _user_id = null)
            : base(Enums.MainObjectType.CAB_USER)
        {
            user_id = _user_id.HasValue ? (int)_user_id : item.user_id;
            user_name = item.user_name;
            user_login = item.user_login;            
            is_superadmin = item.is_superadmin;
            full_name = item.full_name;
            email = item.email;
            is_crm_user = item.is_crm_user;
            is_executer = item.is_executer;
            crm_user_role_id = item.crm_user_role_id;
            icq = item.icq;
            is_active = item.is_active;
            crm_user_role_name = "";
            pwd = "***";
            is_male = item.is_male;
            is_notify_prj = item.is_notify_prj;
            
        }

        public CabUserViewModel(vw_cab_user2 item, int? _user_id = null)
            : base(Enums.MainObjectType.CAB_USER)
        {
            user_id = _user_id.HasValue ? (int)_user_id : item.user_id;
            user_name = item.user_name;
            user_login = item.user_login;            
            is_superadmin = item.is_superadmin;
            full_name = item.full_name;
            email = item.email;
            is_crm_user = item.is_crm_user;
            is_executer = item.is_executer;
            crm_user_role_id = item.crm_user_role_id;
            icq = item.icq;
            is_active = item.is_active;
            crm_user_role_name = item.crm_user_role_name;
            auth_role_name = item.auth_role_name;
            auth_role_id = item.auth_role_id;
            pwd = "***";
            is_male = item.is_male;
            is_notify_prj = item.is_notify_prj;
        }

        public CabUserViewModel(string _user_name, string _user_login, int? _user_id = null)
            : base(Enums.MainObjectType.CAB_USER)
        {
            user_id = _user_id.HasValue ? (int)_user_id : 0;
            user_name = _user_name;
            user_login = _user_login;            
            is_superadmin = false;
            full_name = _user_name;
            email = "";
            pwd = "***";
            is_active = true;
            is_male = true;
            is_notify_prj = false;
        }


        [Key()]
        [Display(Name = "Код")]
        public int user_id { get; set; }

        [Display(Name = "Имя")]
        public string user_name { get; set; }

        [Display(Name = "Логин")]
        public string user_login { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public string pwd { get; set; }

        [Display(Name = "Суперадмин")]
        public bool is_superadmin { get; set; }

        [Display(Name = "Полное имя")]
        public string full_name { get; set; }

        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "В Задачах")]
        public bool is_crm_user { get; set; }

        [Display(Name = "В календаре")]
        public bool is_executer { get; set; }

        [Display(Name = "Роль")]
        public Nullable<int> crm_user_role_id { get; set; }

        [Display(Name = "ICQ")]
        public string icq { get; set; }

        [Display(Name = "Активен")]
        public bool is_active { get; set; }

        [Display(Name = "Роль в задачах")]
        public string crm_user_role_name { get; set; }

        [Display(Name = "Роль в задачах")]
        [UIHint("CrmUserRoleCombo")]
        public Crm.CrmUserRoleViewModel CrmUserRole { get; set; }

        [Display(Name = "Роль")]
        public Nullable<int> auth_role_id { get; set; }

        [Display(Name = "Роль")]
        public string auth_role_name { get; set; }

        [Display(Name = "Пол")]
        public bool is_male { get; set; }

        [Display(Name = "Уведомлять о новых задачах")]
        public bool is_notify_prj { get; set; }

        [Display(Name = "Роль")]
        [UIHint("AuthRoleCombo")]
        public Auth.AuthRoleViewModel AuthRole { get; set; }
    }
}