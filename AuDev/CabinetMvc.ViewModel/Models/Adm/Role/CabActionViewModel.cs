﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public class CabActionViewModel : AuBaseViewModel
    {
        public CabActionViewModel()
            : base(Enums.MainObjectType.CAB_ACTION)
        {
            //
        }

        public CabActionViewModel(cab_action item)
            : base(Enums.MainObjectType.CAB_ACTION)
        {
            action_id = item.action_id;
            action_name = item.action_name;
            group_id = item.group_id;
        }

        [Key()]
        [Display(Name = "Код действия")]
        public long action_id { get; set; }

        [Display(Name = "Название действия")]
        [Required(ErrorMessage = "Не задано название действия")]
        public string action_name { get; set; }
                
        [Display(Name = "Код группы")]
        [Required(ErrorMessage = "Не задана группа")]
        public long group_id { get; set; }
        
    }
}