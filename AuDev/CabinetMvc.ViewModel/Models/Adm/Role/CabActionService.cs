﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;

namespace CabinetMvc.ViewModel.Adm
{
    public interface ICabActionService : IAuBaseService
    {
        //
    }

    public class CabActionService : AuBaseService, ICabActionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.cab_action
                .Select(ss => new CabActionViewModel()
                {
                    action_id = ss.action_id,
                    action_name = ss.action_name,
                }
                );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.cab_action.Where(ss => ss.action_id == id).FirstOrDefault();
            return res != null ? new CabActionViewModel(res) : null;
        }
        
    }
}
