﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;

namespace CabinetMvc.ViewModel.Adm
{
    public interface ICabActionGroupService : IAuBaseService
    {
        //
    }

    public class CabActionGroupService : AuBaseService, ICabActionGroupService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.cab_action_group
                .Select(ss => new CabActionGroupViewModel()
                {
                    group_id = ss.group_id,
                    group_name = ss.group_name,
                }
                );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.cab_action_group.Where(ss => ss.group_id == id).FirstOrDefault();
            return res != null ? new CabActionGroupViewModel(res) : null;
        }
        
    }
}
