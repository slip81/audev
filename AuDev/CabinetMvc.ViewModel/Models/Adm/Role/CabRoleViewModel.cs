﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Adm
{
    public class CabRoleViewModel : AuBaseViewModel
    {
        public CabRoleViewModel()
            : base(Enums.MainObjectType.CAB_ROLE)
        {
            //
        }

        public CabRoleViewModel(cab_role item)
            : base(Enums.MainObjectType.CAB_ROLE)
        {
            role_id = item.role_id;
            role_name = item.role_name;
        }

        [Key()]
        [Display(Name = "Код роли")]
        public long role_id { get; set; }

        [Display(Name = "Название роли")]
        [Required(ErrorMessage = "Не задано название роли")]
        public string role_name { get; set; }        
    }
}