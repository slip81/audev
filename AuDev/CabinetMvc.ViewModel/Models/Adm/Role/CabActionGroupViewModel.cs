﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Discount;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Adm
{
    public class CabActionGroupViewModel : AuBaseViewModel
    {
        public CabActionGroupViewModel()
            : base(Enums.MainObjectType.CAB_ACTION_GROUP)
        {
            //
        }

        public CabActionGroupViewModel(cab_action_group item)
            : base(Enums.MainObjectType.CAB_ACTION_GROUP)
        {
            group_id = item.group_id;
            group_name = item.group_name;
        }

        [Key()]
        [Display(Name = "Код группы")]
        public long group_id { get; set; }

        [Display(Name="Название группы")]
        [Required(ErrorMessage = "Не задано название группы")]
        public string group_name { get; set; }
    }
}