﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Discount;

namespace CabinetMvc.ViewModel.Adm
{
    public interface ICabUserRoleService : IAuBaseService
    {
        AuBaseViewModel GetItem_byUserName(string user_name);
    }

    public class CabUserRoleService : AuBaseService, ICabUserRoleService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_cab_user
                .Select(ss => new CabUserRoleViewModel()
                {
                    role_id = ss.role_id,
                    item_id = ss.item_id,
                    use_defaults = ss.use_defaults,
                    user_name = ss.user_name,
                    role_name = ss.role_name,
                    org_name = ss.org_name,
                    business_id = ss.business_id,
                    business_name = ss.business_name,
                    use_defaults_str = ss.use_defaults ? "Да" : "Нет",
                }
                )
                ;
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_cab_user.Where(ss => ss.business_id == parent_id)
                .Select(ss => new CabUserRoleViewModel()
                {
                    role_id = ss.role_id,
                    item_id = ss.item_id,
                    use_defaults = ss.use_defaults,
                    user_name = ss.user_name,
                    role_name = ss.role_name,
                    org_name = ss.org_name,
                    business_id = ss.business_id,
                    business_name = ss.business_name,
                    use_defaults_str = ss.use_defaults ? "Да" : "Нет",
                }
                )
                ;
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.vw_cab_user.Where(ss => ss.item_id == id).FirstOrDefault();
            if (res == null)
                return null;

            CabUserRoleViewModel vm = new CabUserRoleViewModel(res);
            vm.UserRoleList = dbContext.cab_role.Select(ss => new CabRoleViewModel() { role_id = ss.role_id, role_name = ss.role_name, });
            return vm;
        }

        public AuBaseViewModel GetItem_byUserName(string user_name)
        {
            var res = dbContext.vw_cab_user.Where(ss => ss.user_name == user_name).FirstOrDefault();
            if (res == null)
                return null;

            CabUserRoleViewModel vm = new CabUserRoleViewModel(res);
            vm.UserRoleList = dbContext.cab_role.Select(ss => new CabRoleViewModel() { role_id = ss.role_id, role_name = ss.role_name, });
            return vm;
        }
        
    }
}
