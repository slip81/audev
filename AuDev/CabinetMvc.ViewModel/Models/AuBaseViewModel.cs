﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel
{
    public class AuBaseViewModel
    {

        private Enums.MainObjectType ObjType;

        public AuBaseViewModel()
        {
            //
        }

        public AuBaseViewModel(Enums.MainObjectType obj_type)
        {
            ObjType = obj_type;
        }

        public Enums.MainObjectType GetObjType()
        {
            return ObjType;
        }

        public long? GetKeyValue()
        {
            long? res = null;
            foreach (PropertyInfo prop in this.GetType().GetProperties())
            {
                bool is_key = GlobalUtil.HasAttribute(prop, "Key");
                if (is_key)
                {
                    var key = prop.GetValue(this);
                    if (key.GetType() == typeof(long))
                    {
                        res = (long)key;
                    }
                    else if (key.GetType() == typeof(int))
                    {
                        res = (int)key;
                    }

                    break;
                }
            }
            return res;
        }

        [ScaffoldColumn(false)]
        public string ErrMess { get; set; }
    }

}
