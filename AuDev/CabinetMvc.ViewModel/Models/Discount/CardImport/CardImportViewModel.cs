﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardImportViewModel : AuBaseViewModel
    {
        public CardImportViewModel()
            : base(Enums.MainObjectType.CARD_IMPORT)
        {
            action_for_existing_summ = 0;
            action_for_existing_disc_percent = 0;
            action_for_existing_bonus = 0;
        }

        [Key()]
        [Display(Name = "Код")]
        public long import_id { get; set; }

        [Display(Name = "Тип карт")]
        [Required(ErrorMessage = "Не указан тип карт")]
        public long? card_type_id { get; set; }

        [Display(Name = "Статус карт")]
        [Required(ErrorMessage = "Не указан статус карт")]
        public long? card_status_id { get; set; }

        [Display(Name = "Действие для накопленных сумм")]
        public int action_for_existing_summ { get; set; }

        [Display(Name = "Действие для процентов скидки")]
        public int action_for_existing_disc_percent { get; set; }

        [Display(Name = "Действие для бонусных процентов")]
        public int action_for_existing_bonus { get; set; }
    }
}