﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardImportService : IAuBaseService
    {
        bool ImportCards(string file1, string file2, CardImportViewModel cardImport);
    }

    public class CardImportService : AuBaseService, ICardImportService
    {        
        public bool ImportCards(string file1, string file2, CardImportViewModel cardImport)
        {
            CardBatchOperationsViewModel batch = new CardBatchOperationsViewModel();

            batch.batch_name = "Импорт карт";
            batch.batch_type = Enums.BatchType.BatchImport;
            batch.card_type_id = cardImport.card_type_id;
            batch.card_status_id = cardImport.card_status_id;
            batch.action_for_existing_summ = cardImport.action_for_existing_summ;
            batch.action_for_existing_disc_percent = cardImport.action_for_existing_disc_percent;
            batch.action_for_existing_bonus = cardImport.action_for_existing_bonus;
            batch.file1 = file1;
            batch.file2 = file2;

            CardBatchOperationsService serv = new CardBatchOperationsService();
            serv.currUser = currUser;
            serv.Insert(batch, new ModelStateDictionary());

            return true;
        }
    }
}