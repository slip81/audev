﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardTypeBusinessOrgViewModel : AuBaseViewModel
    {
        public CardTypeBusinessOrgViewModel()
            :base(Enums.MainObjectType.CARD_TYPE_BUSINESS_ORG)
        {
            //vw_card_type_business_org
        }

        public CardTypeBusinessOrgViewModel(card_type_business_org item)
            : base(Enums.MainObjectType.CARD_TYPE_BUSINESS_ORG)
        {
            //item_id = item.item_id;
            card_type_id = item.card_type_id;
            business_org_id = item.business_org_id;            
        }

        [Key()]
        [Display(Name = "Тип карты")]
        public long card_type_id { get; set; }
        [Display(Name = "Отделение")]
        public long? business_org_id { get; set; }
        [Display(Name = "Отделение")]
        public string business_org_id_str { get { return business_org_id.ToString(); }}
        [Display(Name = "Отделение")]
        public string business_org_name { get; set; }
        [Display(Name = "Видны в отделении")]
        public bool is_org_ok { get; set; }
        [Display(Name = "Применяется скидка")]
        public bool allow_discount { get; set; }
        [Display(Name = "Списываются бонусы")]
        public bool allow_bonus_for_pay { get; set; }
        [Display(Name = "Начисляются бонусы")]
        public bool allow_bonus_for_card { get; set; }
    }
}