﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardTypeService : IAuBaseService
    {
        //
    }

    public class CardTypeService : AuBaseService, ICardTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return (from ss in dbContext.vw_card_type
                      //from ss1 in dbContext.card_type_check_info
                      //where ss.card_type_id == ss1.card_type_id
                      where ss.business_id == parent_id
                      select new CardTypeViewModel()
                      {
                          card_type_id = ss.card_type_id,
                          card_type_name = ss.card_type_name,
                          business_id = ss.business_id,
                          is_fin_spec = ss.is_fin_spec,
                          card_type_group_id = ss.card_type_group_id,
                          update_nominal_bool = ss.update_nominal == 1,
                          group_name = ss.group_name,
                          programm_id = ss.programm_id,
                          programm_name = ss.programm_name,
                          descr_full = ss.descr_full,
                          descr_short = ss.descr_short,
                          reset_interval_bonus_value = ss.reset_interval_bonus_value,
                          is_reset_without_sales = ss.is_reset_without_sales,
            /*
            is_active = ss1.is_active,
            print_bonus_percent_card = ss1.print_bonus_percent_card,
            print_bonus_value_card = ss1.print_bonus_value_card,
            print_bonus_value_trans = ss1.print_bonus_value_trans,
            print_disc_percent_card = ss1.print_disc_percent_card,
            print_sum_trans = ss1.print_sum_trans,
            */
        }
                      );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var card_type = dbContext.vw_card_type.Where(ss => ss.card_type_id == id).FirstOrDefault();
            var card_type_check_info = dbContext.card_type_check_info.Where(ss => ss.card_type_id == id).FirstOrDefault();
            var card_type_limit_list = dbContext.card_type_limit.Where(ss => ss.card_type_id == id
                //&& ss.limit_type == (int)Enums.CardLimitType.MoneyMonthly
                )
                .Select(ss => new CardTypeLimitViewModel()
                {
                    limit_id = ss.limit_id,
                    card_type_id = ss.card_type_id,
                    limit_type = ss.limit_type,
                    limit_year = ss.limit_year,
                    limit_month = ss.limit_month,
                    limit_date_beg = ss.limit_date_beg,
                    limit_date_end = ss.limit_date_end,
                    limit_value = ss.limit_value,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    upd_date = ss.upd_date,
                    upd_user = ss.upd_user,
                }
            );
            return card_type == null ? null : new CardTypeViewModel(card_type, card_type_check_info, card_type_limit_list);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardTypeViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardTypeViewModel cardTypeAdd = (CardTypeViewModel)item;
            if (
                (cardTypeAdd == null)
                ||
                (String.IsNullOrEmpty(cardTypeAdd.card_type_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты типа карты");
                return null;
            }

            long business_id = currUser == null ? cardTypeAdd.business_id.GetValueOrDefault(0) : currUser.BusinessId;
            cardTypeAdd.business_id = business_id;
            long id = 0;

            List<card_type_unit> card_type_unit_list = new List<card_type_unit>();
            card_type_unit card_type_unit = null;
            switch ((Enums.CardTypeGroup)cardTypeAdd.card_type_group_id)
            {
                case Enums.CardTypeGroup.DISCOUNT:
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.DISCOUNT_PERCENT;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    break;
                case Enums.CardTypeGroup.BONUS:
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit.grow_from_sale_mode = 1;
                    card_type_unit.spend_to_sale_mode = 1;
                    card_type_unit_list.Add(card_type_unit);
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_PERCENT;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    break;
                case Enums.CardTypeGroup.DISCOUNT_BONUS:
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.DISCOUNT_PERCENT;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit.grow_from_sale_mode = 1;
                    card_type_unit.spend_to_sale_mode = 1;
                    card_type_unit_list.Add(card_type_unit);
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_PERCENT;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    break;
                case Enums.CardTypeGroup.CERT:
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.MONEY;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    break;
                default:
                    modelState.AddModelError("", "Не задана группа типа карты");
                    return null;
            }

            card_type card_type = new card_type();
            card_type.card_type_name = cardTypeAdd.card_type_name;
            card_type.business_id = business_id;
            card_type.card_type_group_id = cardTypeAdd.card_type_group_id;
            card_type.update_nominal = cardTypeAdd.update_nominal_bool ? (short)1 : (short)0;
            card_type.is_fin_spec = ((Enums.CardTypeGroup)cardTypeAdd.card_type_group_id == Enums.CardTypeGroup.CERT) ? (short)1 : (short)0;
            card_type.reset_interval_bonus_value = cardTypeAdd.reset_interval_bonus_value;
            card_type.is_reset_without_sales = cardTypeAdd.is_reset_without_sales;
            dbContext.card_type.Add(card_type);

            card_type_check_info card_type_check_info = new card_type_check_info();            
            card_type_check_info.card_type = card_type;
            card_type_check_info.is_active = cardTypeAdd.is_active;
            card_type_check_info.print_bonus_percent_card = cardTypeAdd.print_bonus_percent_card;
            card_type_check_info.print_bonus_value_card = cardTypeAdd.print_bonus_value_card;
            card_type_check_info.print_bonus_value_trans = cardTypeAdd.print_bonus_value_trans;
            card_type_check_info.print_disc_percent_card = cardTypeAdd.print_disc_percent_card;
            card_type_check_info.print_sum_trans = cardTypeAdd.print_sum_trans;
            card_type_check_info.print_inactive_bonus_value_card = cardTypeAdd.print_inactive_bonus_value_card;
            //
            card_type_check_info.print_used_bonus_value_trans = cardTypeAdd.print_used_bonus_value_trans;
            card_type_check_info.print_programm_descr = cardTypeAdd.print_programm_descr;
            card_type_check_info.print_card_num = cardTypeAdd.print_card_num;
            card_type_check_info.print_cert_sum_card = cardTypeAdd.print_cert_sum_card;
            card_type_check_info.print_cert_sum_trans = cardTypeAdd.print_cert_sum_trans;
            card_type_check_info.print_cert_sum_left = cardTypeAdd.print_cert_sum_left;
            //
            dbContext.card_type_check_info.Add(card_type_check_info);

            dbContext.SaveChanges();

            foreach (var card_type_unit_item in card_type_unit_list)
            {
                card_type_unit_item.card_type_id = card_type.card_type_id;
            }
            dbContext.card_type_unit.AddRange(card_type_unit_list);

            if (cardTypeAdd.programm_id.GetValueOrDefault(0) > 0)
            {
                card_type_programm_rel rel = new card_type_programm_rel();
                rel.card_type_id = card_type.card_type_id;
                rel.date_beg = DateTime.Today;
                rel.date_end = null;
                rel.programm_id = (long)cardTypeAdd.programm_id;
                dbContext.card_type_programm_rel.Add(rel);
            }

            card_type_business_org card_type_business_org = null;
            List<business_org> business_org_list = dbContext.business_org.Where(ss => ss.business_id == business_id).ToList();
            if ((business_org_list != null) && (business_org_list.Count > 0))
            {
                foreach (var business_org in business_org_list)
                {
                    card_type_business_org = new card_type_business_org();
                    card_type_business_org.card_type = card_type;
                    card_type_business_org.business_org_id = business_org.org_id;
                    card_type_business_org.allow_discount = true;
                    card_type_business_org.allow_bonus_for_pay = true;
                    card_type_business_org.allow_bonus_for_card = true;
                    dbContext.card_type_business_org.Add(card_type_business_org);
                }
            }

            dbContext.SaveChanges();

            id = card_type.card_type_id;

            var vw_card_type = dbContext.vw_card_type.Where(ss => ss.card_type_id == id).FirstOrDefault();
            return new CardTypeViewModel(vw_card_type, card_type_check_info, null);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardTypeViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardTypeViewModel cardTypeEdit = (CardTypeViewModel)item;
            if (
                (cardTypeEdit == null)
                ||
                (String.IsNullOrEmpty(cardTypeEdit.card_type_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты типа карты");
                return null;
            }            

            long business_id = currUser.BusinessId;
            cardTypeEdit.business_id = business_id;

            var vw_card_type_old = dbContext.vw_card_type.Where(ss => ss.card_type_id == cardTypeEdit.card_type_id).FirstOrDefault();
            if (vw_card_type_old == null)
            {
                modelState.AddModelError("", "Не найден тип карты с кодом " + cardTypeEdit.card_type_id.ToString());
                return null;
            }            
            modelBeforeChanges = new CardTypeViewModel(vw_card_type_old, null, null);

            var card_type = dbContext.card_type.Where(ss => ss.card_type_id == cardTypeEdit.card_type_id).FirstOrDefault();

            if (card_type.card_type_group_id != cardTypeEdit.card_type_group_id)
            {
                if (dbContext.card_card_type_rel.Where(ss => ss.card_type_id == cardTypeEdit.card_type_id && !ss.date_end.HasValue).FirstOrDefault() != null)
                {
                    modelState.AddModelError("", "Нельзя сменить группу типа карт, существуют карты данного типа");
                    return null;
                }
            }

            List<card_type_unit> card_type_unit_list = new List<card_type_unit>();
            card_type_unit card_type_unit = null;
            switch ((Enums.CardTypeGroup)cardTypeEdit.card_type_group_id)
            {
                case Enums.CardTypeGroup.DISCOUNT:
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.DISCOUNT_PERCENT;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    break;
                case Enums.CardTypeGroup.BONUS:
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit.grow_from_sale_mode = 1;
                    card_type_unit.spend_to_sale_mode = 1;
                    card_type_unit_list.Add(card_type_unit);
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_PERCENT;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    break;
                case Enums.CardTypeGroup.DISCOUNT_BONUS:
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.DISCOUNT_PERCENT;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit.grow_from_sale_mode = 1;
                    card_type_unit.spend_to_sale_mode = 1;
                    card_type_unit_list.Add(card_type_unit);
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_PERCENT;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    break;
                case Enums.CardTypeGroup.CERT:
                    card_type_unit = new card_type_unit();
                    card_type_unit.unit_type = (int)Enums.CardUnitTypeEnum.MONEY;
                    card_type_unit.is_allow_neg = 0;
                    card_type_unit_list.Add(card_type_unit);
                    break;
                default:
                    modelState.AddModelError("", "Не задана группа типа карты");
                    return null;
            }

            card_type.card_type_name = cardTypeEdit.card_type_name;
            card_type.update_nominal = cardTypeEdit.update_nominal_bool ? (short)1 : (short)0;
            card_type.reset_interval_bonus_value = cardTypeEdit.reset_interval_bonus_value;
            card_type.is_reset_without_sales = cardTypeEdit.is_reset_without_sales;

            card_type_check_info card_type_check_info = dbContext.card_type_check_info.Where(ss => ss.card_type_id == cardTypeEdit.card_type_id).FirstOrDefault();
            if (card_type_check_info != null)
            {
                card_type_check_info.is_active = cardTypeEdit.is_active;
                card_type_check_info.print_bonus_percent_card = cardTypeEdit.print_bonus_percent_card;
                card_type_check_info.print_bonus_value_card = cardTypeEdit.print_bonus_value_card;
                card_type_check_info.print_bonus_value_trans = cardTypeEdit.print_bonus_value_trans;
                card_type_check_info.print_disc_percent_card = cardTypeEdit.print_disc_percent_card;
                card_type_check_info.print_sum_trans = cardTypeEdit.print_sum_trans;
                card_type_check_info.print_inactive_bonus_value_card = cardTypeEdit.print_inactive_bonus_value_card;
                //
                card_type_check_info.print_used_bonus_value_trans = cardTypeEdit.print_used_bonus_value_trans;
                card_type_check_info.print_programm_descr = cardTypeEdit.print_programm_descr;
                card_type_check_info.print_card_num = cardTypeEdit.print_card_num;
                card_type_check_info.print_cert_sum_card = cardTypeEdit.print_cert_sum_card;
                card_type_check_info.print_cert_sum_trans = cardTypeEdit.print_cert_sum_trans;
                card_type_check_info.print_cert_sum_left = cardTypeEdit.print_cert_sum_left;
            }

            if (card_type.card_type_group_id != cardTypeEdit.card_type_group_id)
            {
                var card_type_unit_list_existing = dbContext.card_type_unit.Where(ss => ss.card_type_id == cardTypeEdit.card_type_id).ToList();
                if (card_type_unit_list_existing != null)
                    dbContext.card_type_unit.RemoveRange(card_type_unit_list_existing);
                foreach (var card_type_unit_item in card_type_unit_list)
                {
                    card_type_unit_item.card_type_id = card_type.card_type_id;
                }
                dbContext.card_type_unit.AddRange(card_type_unit_list);

                card_type.card_type_group_id = cardTypeEdit.card_type_group_id;
                card_type.is_fin_spec = ((Enums.CardTypeGroup)cardTypeEdit.card_type_group_id == Enums.CardTypeGroup.CERT) ? (short)1 : (short)0;
            }

            bool changeProgramm = false;
            var rel_existing = dbContext.card_type_programm_rel.Where(ss => ss.card_type_id == card_type.card_type_id && !ss.date_end.HasValue).FirstOrDefault();
            if (rel_existing != null)
            {
                if (rel_existing.programm_id != cardTypeEdit.programm_id.GetValueOrDefault(0))
                {
                    changeProgramm = true;
                    rel_existing.date_end = DateTime.Today;
                }
            }
            else
            {
                changeProgramm = (cardTypeEdit.programm_id.GetValueOrDefault(0) > 0);
            }

            if ((cardTypeEdit.programm_id.GetValueOrDefault(0) > 0) && (changeProgramm))
            {
                card_type_programm_rel rel = new card_type_programm_rel();
                rel.card_type_id = card_type.card_type_id;
                rel.date_beg = DateTime.Today;
                rel.date_end = null;
                rel.programm_id = (long)cardTypeEdit.programm_id;
                dbContext.card_type_programm_rel.Add(rel);
            }

            dbContext.SaveChanges();

            var vw_card_type = dbContext.vw_card_type.Where(ss => ss.card_type_id == cardTypeEdit.card_type_id).FirstOrDefault();
            return new CardTypeViewModel(vw_card_type, card_type_check_info, null);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardTypeViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            long id = ((CardTypeViewModel)item).card_type_id;

            long business_id = currUser.BusinessId;

            card_type card_type = null;

            card_type = dbContext.card_type.Where(ss => ss.card_type_id == id && ss.business_id == business_id).FirstOrDefault();
            if (card_type == null)
            {
                modelState.AddModelError("serv_result", "Тип карты не найден");
                return false;
            }

            long rel_id = dbContext.card_card_type_rel.Where(ss => ss.card_type_id == id).Select(ss => ss.rel_id).FirstOrDefault();
            if (rel_id > 0)
            {
                modelState.AddModelError("serv_result", "Существуют карты с данным типом карт");
                return false;
            }

            List<card_type_unit> card_type_unit_list = dbContext.card_type_unit.Where(ss => ss.card_type_id == id).Distinct().ToList();
            if ((card_type_unit_list != null) && (card_type_unit_list.Count > 0))
            {
                dbContext.card_type_unit.RemoveRange(card_type_unit_list);
            }

            List<card_type_programm_rel> card_type_programm_rel_list = dbContext.card_type_programm_rel.Where(ss => ss.card_type_id == id).Distinct().ToList();
            if ((card_type_programm_rel_list != null) && (card_type_programm_rel_list.Count > 0))
            {
                dbContext.card_type_programm_rel.RemoveRange(card_type_programm_rel_list);
            }

            var card_type_limit_list = dbContext.card_type_limit.Where(ss => ss.card_type_id == id).Distinct();
            dbContext.card_type_limit.RemoveRange(card_type_limit_list);

            dbContext.card_type_check_info.RemoveRange(dbContext.card_type_check_info.Where(ss => ss.card_type_id == id));
            dbContext.card_type_business_org.RemoveRange(dbContext.card_type_business_org.Where(ss => ss.card_type_id == id));

            dbContext.card_type.Remove(card_type);
            dbContext.SaveChanges();

            return true;
        }

    }
}