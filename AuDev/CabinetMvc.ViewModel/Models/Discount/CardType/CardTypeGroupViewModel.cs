﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardTypeGroupViewModel : AuBaseViewModel
    {
        public CardTypeGroupViewModel()
            : base(Enums.MainObjectType.CARD_TYPE_GROUP)
        {
            //
        }

        public CardTypeGroupViewModel(card_type_group item)
            : base(Enums.MainObjectType.CARD_TYPE_GROUP)
        {
            card_type_group_id = item.card_type_group_id;
            group_name = item.group_name;
        }
        
        [Key()]
        public long card_type_group_id { get; set; }
        public string group_name { get; set; }
    }
}