﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardTypeGroupService : IAuBaseService
    {
        //
    }

    public class CardTypeGroupService : AuBaseService, ICardTypeGroupService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.card_type_group.Select(ss => new CardTypeGroupViewModel()
            {
                card_type_group_id = ss.card_type_group_id,
                group_name = ss.group_name,
            }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var card_type_group = dbContext.card_type_group.Where(ss => ss.card_type_group_id == id).FirstOrDefault();
            return card_type_group == null ? null : new CardTypeGroupViewModel(card_type_group);
        }
    }
}