﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardTypeViewModel : AuBaseViewModel
    {
        public CardTypeViewModel()
            :base(Enums.MainObjectType.CARD_TYPE)
        {
            //
        }

        public CardTypeViewModel(vw_card_type item, card_type_check_info item2, IEnumerable<CardTypeLimitViewModel> card_type_limits)
            : base(Enums.MainObjectType.CARD_TYPE)
        {
            card_type_id = item.card_type_id;
            card_type_name = item.card_type_name;
            business_id = item.business_id;
            is_fin_spec = item.is_fin_spec;
            card_type_group_id = item.card_type_group_id;
            update_nominal_bool = item.update_nominal == 1;
            group_name = item.group_name;
            programm_id = item.programm_id;
            programm_name = item.programm_name;
            descr_short = item.descr_short;
            descr_full = item.descr_full;
            reset_interval_bonus_value = item.reset_interval_bonus_value;
            is_reset_without_sales = item.is_reset_without_sales;
            card_type_limit_list = card_type_limits;
            if (item2 != null)
            {
                is_active = item2.is_active;
                print_bonus_percent_card = item2.print_bonus_percent_card;
                print_bonus_value_card = item2.print_bonus_value_card;
                print_bonus_value_trans = item2.print_bonus_value_trans;
                print_disc_percent_card = item2.print_disc_percent_card;
                print_sum_trans = item2.print_sum_trans;
                print_inactive_bonus_value_card = item2.print_inactive_bonus_value_card;
                //
                print_used_bonus_value_trans = item2.print_used_bonus_value_trans;
                print_programm_descr = item2.print_programm_descr;
                print_card_num = item2.print_card_num;
                print_cert_sum_card = item2.print_cert_sum_card;
                print_cert_sum_trans = item2.print_cert_sum_trans;
                print_cert_sum_left = item2.print_cert_sum_left;
            }
        }

        public CardTypeViewModel(card_type item, card_type_check_info item2)
            : base(Enums.MainObjectType.CARD_TYPE)
        {
            card_type_id = item.card_type_id;
            card_type_name = item.card_type_name;
            business_id = item.business_id;
            is_fin_spec = item.is_fin_spec;
            card_type_group_id = item.card_type_group_id;
            update_nominal_bool = item.update_nominal == 1;
            reset_interval_bonus_value = item.reset_interval_bonus_value;
            is_reset_without_sales = item.is_reset_without_sales;
            card_type_limit_list = item.card_type_limit != null ? item.card_type_limit.Select(ss => new CardTypeLimitViewModel(ss)) : null;
            if (item2 != null)
            {
                is_active = item2.is_active;
                print_bonus_percent_card = item2.print_bonus_percent_card;
                print_bonus_value_card = item2.print_bonus_value_card;
                print_bonus_value_trans = item2.print_bonus_value_trans;
                print_disc_percent_card = item2.print_disc_percent_card;
                print_sum_trans = item2.print_sum_trans;
                print_inactive_bonus_value_card = item2.print_inactive_bonus_value_card;
                //
                print_used_bonus_value_trans = item2.print_used_bonus_value_trans;
                print_programm_descr = item2.print_programm_descr;
                print_card_num = item2.print_card_num;
                print_cert_sum_card = item2.print_cert_sum_card;
                print_cert_sum_trans = item2.print_cert_sum_trans;
                print_cert_sum_left = item2.print_cert_sum_left;
            }
        }

        [Key()]
        [Display(Name = "Код")]
        public long card_type_id { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Не указано наименование")]
        public string card_type_name { get; set; }

        [Display(Name = "Организация")]        
        public Nullable<long> business_id { get; set; }

        [Display(Name = "is_fin_spec")]
        public short is_fin_spec { get; set; }

        [Display(Name = "Группа")]
        [Required(ErrorMessage = "Не указана группа")]
        public Nullable<long> card_type_group_id { get; set; }

        [JsonIgnore()]
        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [Display(Name = "Обновлять номиналы при выборе карты")]
        public bool update_nominal_bool { get; set; }

        [Display(Name = "Сбрасывать бонусы через")]
        public Nullable<short> reset_interval_bonus_value { get; set; }

        [Display(Name = "Сбрасывать бонусы только если не было продаж")]
        public short is_reset_without_sales { get; set; }

        [Display(Name = "Алгоритм")]
        public Nullable<long> programm_id { get; set; }
        
        [JsonIgnore()]
        [Display(Name = "Алгоритм")]
        public string programm_id_str { get { return programm_id.ToString(); } }

        [JsonIgnore()]        
        [Display(Name = "Алгоритм")]
        public string programm_name { get; set; }

        [JsonIgnore()]        
        [Display(Name = "Алгоритм")]
        public string programm_name_not_empty { get { return String.IsNullOrEmpty(programm_name) ? "-" : programm_name.Trim(); } }

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        [Display(Name = "Описание алгоритма (краткое)")]
        public string descr_short { get; set; }

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        [Display(Name = "Описание алгоритма (полное)")]
        public string descr_full { get; set; }
        
        [JsonIgnore()]
        [ScaffoldColumn(false)]
        [Display(Name = "is_fin_spec_str")]
        public string is_fin_spec_str
        {
            get
            {
                return is_fin_spec == 1 ? "Да" : "Нет";
            }
        }
        
        [JsonIgnore()]
        [ScaffoldColumn(false)]
        [Display(Name = "card_type_limit_list")]
        public IEnumerable<CardTypeLimitViewModel> card_type_limit_list { get; set; }

        // card_type_check_info
        [Display(Name = "Печатать на чеке информацию о карте")]
        public bool is_active { get; set; }

        [Display(Name = "Печатать на чеке сумму накоплений")]
        public bool print_sum_trans { get; set; }

        [Display(Name = "Печатать на чеке сумму накопленных бонусов с продажи")]
        public bool print_bonus_value_trans { get; set; }

        [Display(Name = "Печатать на чеке сумму накопленных бонусов на карте")]
        public bool print_bonus_value_card { get; set; }

        [Display(Name = "Печатать на чеке бонусный процент")]
        public bool print_bonus_percent_card { get; set; }

        [Display(Name = "Печатать на чеке процент скидки")]
        public bool print_disc_percent_card { get; set; }

        [Display(Name = "Печатать на чеке сумму неактивных бонусов на карте")]
        public bool print_inactive_bonus_value_card { get; set; }

        // new
        
        [Display(Name = "Печатать на чеке сумму списанных при продаже бонусов")]
        public bool print_used_bonus_value_trans { get; set; }

        [Display(Name = "Печатать на чеке описание алгоритма")]
        public bool print_programm_descr { get; set; }

        [Display(Name = "Печатать на чеке номер карты")]
        public bool print_card_num { get; set; }

        [Display(Name = "Печатать на чеке номинал сертификата")]
        public bool print_cert_sum_card { get; set; }

        [Display(Name = "Печатать на чеке сумму списанную с сертификата")]
        public bool print_cert_sum_trans { get; set; }

        [Display(Name = "Печатать на чеке сумму оставшуюся на сертификате")]
        public bool print_cert_sum_left { get; set; }
    }
}