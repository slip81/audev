﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardTypeLimitViewModel : AuBaseViewModel
    {
        public CardTypeLimitViewModel()
            : base(Enums.MainObjectType.CARD_TYPE_lIMIT)
        {
            limit_type_enum = Enums.CardLimitType.None;
        }

        public CardTypeLimitViewModel(card_type_limit item)
            : base(Enums.MainObjectType.CARD_TYPE_lIMIT)
        {
            limit_id = item.limit_id;
            card_type_id = item.card_type_id;
            limit_type = item.limit_type;
            limit_year = item.limit_year;
            limit_month = item.limit_month;
            limit_date_beg = item.limit_date_beg;
            limit_date_end = item.limit_date_end;
            limit_value = item.limit_value;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            upd_date = item.upd_date;
            upd_user = item.upd_user;
            limit_type_enum = (Enums.CardLimitType)item.limit_type;
        }


        [Key()]
        [ScaffoldColumn(false)]
        public long limit_id { get; set; }
        [ScaffoldColumn(false)]
        public long card_type_id { get; set; }
        [ScaffoldColumn(false)]
        public Nullable<int> limit_type { get; set; }
        [UIHint("CardLimitTypeCombo")]
        [Display(Name="Тип лимита")]
        public Enums.CardLimitType limit_type_enum { get; set; }
        [ScaffoldColumn(false)]
        public Nullable<int> limit_year { get; set; }
        [ScaffoldColumn(false)]
        public Nullable<int> limit_month { get; set; }
        [Display(Name = "Дата начала")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> limit_date_beg { get; set; }
        [Display(Name = "Дата окончания (включительно)")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> limit_date_end { get; set; }
        [Display(Name = "Величина лимита")]
        public Nullable<decimal> limit_value { get; set; }
        [ScaffoldColumn(false)]
        public Nullable<System.DateTime> crt_date { get; set; }
        [ScaffoldColumn(false)]
        public string crt_user { get; set; }
        [ScaffoldColumn(false)]
        public Nullable<System.DateTime> upd_date { get; set; }
        [ScaffoldColumn(false)]
        public string upd_user { get; set; }

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public string limit_type_name
        {
            get { return limit_type.EnumName<Enums.CardLimitType>(); }
        }
    }
}