﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardTypeLimitService : IAuBaseService
    {
        //
    }

    public class CardTypeLimitService : AuBaseService, ICardTypeLimitService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.card_type_limit.Where(ss => ss.card_type_id == parent_id
                //&& ss.limit_type == (int)Enums.CardLimitType.MoneyMonthly
                )
                .Select(ss => new CardTypeLimitViewModel()
                {
                    limit_id = ss.limit_id,
                    card_type_id = ss.card_type_id,
                    limit_type = ss.limit_type,
                    limit_year = ss.limit_year,
                    limit_month = ss.limit_month,
                    limit_date_beg = ss.limit_date_beg,
                    limit_date_end = ss.limit_date_end,
                    limit_value = ss.limit_value,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    upd_date = ss.upd_date,
                    upd_user = ss.upd_user,
                    limit_type_enum = (Enums.CardLimitType)ss.limit_type,
                }
            );
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var card_type_limit = dbContext.card_type_limit.Where(ss => ss.limit_id == id).FirstOrDefault();
            return card_type_limit == null ? null : new CardTypeLimitViewModel(card_type_limit);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardTypeLimitViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardTypeLimitViewModel cardTypeLimitAdd = (CardTypeLimitViewModel)item;
            if (cardTypeLimitAdd == null)
            {
                modelState.AddModelError("", "Не задан лимит карты");
                return null;
            }

            // считаем что у данного типа карты обязательно есть уже хоть одна строка с лимитом, и тип лимита берем оттуда
            // var curr_limit_type = dbContext.card_type_limit.Where(ss => ss.card_type_id == cardTypeLimitAdd.card_type_id).Select(ss => ss.limit_type).FirstOrDefault();
            var curr_limit_type = cardTypeLimitAdd.limit_type_enum;

            // проверка на существующий лимит с пересекающимися датами
            var existing_limit = dbContext.card_type_limit.Where(ss => ss.card_type_id == cardTypeLimitAdd.card_type_id && ss.limit_date_beg >= cardTypeLimitAdd.limit_date_beg && ss.limit_date_end <= cardTypeLimitAdd.limit_date_end).FirstOrDefault();
            if (existing_limit != null)
            {
                modelState.AddModelError("", "Существует лимит с пересекающимся диапазоном");
                return null;
            }

            card_type_limit card_type_limit = new card_type_limit();
            card_type_limit.card_type_id = cardTypeLimitAdd.card_type_id;
            card_type_limit.crt_date = DateTime.Now;
            card_type_limit.crt_user = currUser.UserName;
            card_type_limit.limit_date_beg = cardTypeLimitAdd.limit_date_beg;
            card_type_limit.limit_date_end = cardTypeLimitAdd.limit_date_end;
            card_type_limit.limit_month = cardTypeLimitAdd.limit_date_beg.HasValue ? ((DateTime)cardTypeLimitAdd.limit_date_beg).Month : cardTypeLimitAdd.limit_month;
            //card_type_limit.limit_type = (int)Enums.CardLimitType.MoneyMonthly;
            //card_type_limit.limit_type = curr_limit_type.HasValue ? curr_limit_type : cardTypeLimitAdd.limit_type;
            card_type_limit.limit_type = (int)curr_limit_type;
            card_type_limit.limit_value = cardTypeLimitAdd.limit_value;
            card_type_limit.limit_year = cardTypeLimitAdd.limit_date_beg.HasValue ? ((DateTime)cardTypeLimitAdd.limit_date_beg).Year : cardTypeLimitAdd.limit_year;
            card_type_limit.upd_date = null;
            card_type_limit.upd_user = null;

            dbContext.card_type_limit.Add(card_type_limit);
            dbContext.SaveChanges();

            return new CardTypeLimitViewModel(card_type_limit);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardTypeLimitViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardTypeLimitViewModel cardTypeLimitEdit = (CardTypeLimitViewModel)item;
            if (cardTypeLimitEdit == null)
            {
                modelState.AddModelError("", "Не задан лимит карты");
                return null;
            }

            var card_type_limit = dbContext.card_type_limit.Where(ss => ss.limit_id == cardTypeLimitEdit.limit_id).FirstOrDefault();
            if (card_type_limit == null)
            {
                modelState.AddModelError("", "Не найден лимит карты с кодом " + cardTypeLimitEdit.limit_id.ToString());
                return null;
            }
            modelBeforeChanges = new CardTypeLimitViewModel(card_type_limit);

            if ((cardTypeLimitEdit.limit_date_end.HasValue) && (((DateTime)cardTypeLimitEdit.limit_date_end).Date < DateTime.Today))
            {
                modelState.AddModelError("", "Нельзя изменять диапазон лимита на значение меньше текущей даты");
                return null;
            }

            // проверка на существующий лимит с пересекающимися датами
            var existing_limit = dbContext.card_type_limit.Where(ss => ss.card_type_id == cardTypeLimitEdit.card_type_id && ss.limit_id != cardTypeLimitEdit.limit_id && ss.limit_date_beg >= cardTypeLimitEdit.limit_date_beg && ss.limit_date_end <= cardTypeLimitEdit.limit_date_end).FirstOrDefault();
            if (existing_limit != null)
            {
                modelState.AddModelError("", "Существует лимит с пересекающимся диапазоном");
                return null;
            }

            card_type_limit.limit_date_beg = cardTypeLimitEdit.limit_date_beg;
            card_type_limit.limit_date_end = cardTypeLimitEdit.limit_date_end;
            card_type_limit.limit_month = cardTypeLimitEdit.limit_date_beg.HasValue ? ((DateTime)cardTypeLimitEdit.limit_date_beg).Month : cardTypeLimitEdit.limit_month;
            card_type_limit.limit_value = cardTypeLimitEdit.limit_value;
            card_type_limit.limit_year = cardTypeLimitEdit.limit_date_beg.HasValue ? ((DateTime)cardTypeLimitEdit.limit_date_beg).Year : cardTypeLimitEdit.limit_year;
            card_type_limit.upd_date = DateTime.Now;
            card_type_limit.upd_user = currUser.UserName;

            dbContext.SaveChanges();

            return new CardTypeLimitViewModel(card_type_limit);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardTypeLimitViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            long id = ((CardTypeLimitViewModel)item).limit_id;

            card_type_limit card_type_limit = dbContext.card_type_limit.Where(ss => ss.limit_id == id).FirstOrDefault();
            if (card_type_limit == null)
            {
                modelState.AddModelError("serv_result", "Лимит карты не найден");
                return false;
            }

            dbContext.card_type_limit.Remove(card_type_limit);
            dbContext.SaveChanges();

            return true;
        }

    }
}