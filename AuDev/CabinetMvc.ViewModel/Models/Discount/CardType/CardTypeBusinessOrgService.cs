﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardTypeBusinessOrgService : IAuBaseService
    {
        //
    }

    public class CardTypeBusinessOrgService : AuBaseService, ICardTypeBusinessOrgService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return (from ss in dbContext.vw_card_type_business_org
                      where ss.card_type_id == parent_id
                    select new CardTypeBusinessOrgViewModel()
                      {                        
                        card_type_id = ss.card_type_id,
                        business_org_id = ss.business_org_id,
                        business_org_name = ss.org_name,
                        is_org_ok = ss.is_org_ok,
                        allow_discount = ss.allow_discount,
                        allow_bonus_for_pay = ss.allow_bonus_for_pay,
                        allow_bonus_for_card = ss.allow_bonus_for_card,
                      }
                      );
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardTypeBusinessOrgViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardTypeBusinessOrgViewModel itemEdit = (CardTypeBusinessOrgViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }
            
            card_type card_type = dbContext.card_type.Where(ss => ss.card_type_id == itemEdit.card_type_id).FirstOrDefault();
            if (card_type == null)
            {
                modelState.AddModelError("", "Не найден тип карты " + itemEdit.card_type_id.ToString());
                return null;
            }

            modelBeforeChanges = null;

            var business_org = dbContext.business_org.Where(ss => ss.business_id == currUser.BusinessId && ss.org_name.Trim().ToLower().Equals(itemEdit.business_org_name.Trim().ToLower())).FirstOrDefault();
            if (business_org == null)
            {
                modelState.AddModelError("", "Не найдено отделение " + itemEdit.business_org_name.ToString());
                return null;
            }

            var existing_item = dbContext.card_type_business_org.Where(ss => ss.card_type_id == itemEdit.card_type_id && ss.business_org_id == business_org.org_id).FirstOrDefault();
            if (existing_item == null)
            {
                if (itemEdit.is_org_ok)
                {
                    existing_item = new card_type_business_org();
                    existing_item.card_type_id = itemEdit.card_type_id;
                    existing_item.business_org_id = (long)business_org.org_id;
                    existing_item.allow_discount = itemEdit.allow_discount;
                    existing_item.allow_bonus_for_pay = itemEdit.allow_bonus_for_pay;
                    existing_item.allow_bonus_for_card = itemEdit.allow_bonus_for_card;
                    dbContext.card_type_business_org.Add(existing_item);
                }
            }
            else
            {
                if (!itemEdit.is_org_ok)
                {
                    dbContext.card_type_business_org.Remove(existing_item);
                }
                else
                {
                    existing_item.allow_discount = itemEdit.allow_discount;
                    existing_item.allow_bonus_for_pay = itemEdit.allow_bonus_for_pay;
                    existing_item.allow_bonus_for_card = itemEdit.allow_bonus_for_card;
                }
            }


            dbContext.SaveChanges();

            return new CardTypeBusinessOrgViewModel(existing_item);
        }
    }
}