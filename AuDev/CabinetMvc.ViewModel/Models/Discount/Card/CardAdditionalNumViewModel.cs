﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{    
    public class CardAdditionalNumViewModel : AuBaseViewModel
    {
        public CardAdditionalNumViewModel()
            : base(Enums.MainObjectType.CARD_ADDITIONAL_NUM)
        {
            //
        }

        public CardAdditionalNumViewModel(card_additional_num item)
            : base(Enums.MainObjectType.CARD_ADDITIONAL_NUM)
        {
            item_id = item.item_id;
            card_id = item.card_id;
            card_num = item.card_num;
        }
        
        [Key()]
        [Display(Name = "Код")]
        public long item_id { get; set; }

        [Display(Name = "Карта")]
        public long card_id { get; set; }

        [Display(Name = "Номер")]
        [Required(ErrorMessage = "Не задан номер")]
        public string card_num { get; set; }

    }
}