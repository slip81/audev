﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardService : IAuBaseService
    {
        IQueryable<SimpleComboLong> GetList_Sprav(string num);
    }

    public class CardService : AuBaseService, ICardService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            var card = dbContext.vw_card.Where(ss => ss.card_id == id).FirstOrDefault();
            var card_types = card == null ? null : dbContext.card_type.Where(ss => ss.business_id == card.business_id);
            var card_statuses = card == null ? null : dbContext.card_status;
            return card == null ? null : new CardViewModel(card, card_types, card_statuses);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {            
            return dbContext.vw_card.Where(ss => ss.business_id == parent_id).Select(card => new CardViewModel
            {
                business_id = card.business_id,
                card_id = card.card_id,
                card_num = card.card_num,
                card_num2 = card.card_num2,
                curr_bonus_percent = card.curr_bonus_percent,
                curr_bonus_value = card.curr_bonus_value,
                curr_card_status_id = card.curr_card_status_id,
                curr_card_type_id = card.curr_card_type_id,
                curr_client_id = card.curr_holder_id,
                curr_discount_percent = card.curr_discount_percent,
                curr_discount_value = card.curr_discount_value,
                curr_money_percent = card.curr_money_percent,
                curr_money_value = card.curr_money_value,
                curr_trans_count = card.curr_trans_count,
                curr_trans_sum = card.curr_trans_sum,
                date_beg = card.date_beg,
                date_end = card.date_end,
                mess = card.mess,                
                curr_card_status_name = card.curr_card_status_name,
                curr_card_type_name = card.curr_card_type_name,
                curr_client_name = card.curr_holder_name,
                card_id_str = card.card_id_str,
                card_num_str = card.card_num_str,
                card_type_group_id = card.card_type_group_id,
                curr_client_first_name = card.curr_holder_first_name,
                curr_client_second_name = card.curr_holder_second_name,
                curr_client_date_birth = card.curr_holder_date_birth,
                curr_client_phone_num = card.curr_holder_phone_num,
                crt_date = card.crt_date,
                crt_user = card.crt_user,
                from_au = card.from_au,
                curr_inactive_bonus_value = card.curr_inactive_bonus_value,
                curr_all_bonus_value = card.curr_all_bonus_value,
                curr_client_full_name = card.curr_holder_full_name,
            });
        }

        protected override IQueryable<AuBaseViewModel> xGetList_bySearch(string search1, string search2)
        {
            long business_id = currUser.BusinessId;
            DateTime today = DateTime.Today;

            string search_formatted = search1 == null ? "" : search1.Trim().ToLower();
            string adv_search_formatted = search2 == null ? "" : search2.Trim().ToLower();

            long card_num_long = 0;
            bool card_num_long_parse = long.TryParse(search_formatted, out card_num_long);
            IQueryable<vw_card> cards = dbContext.vw_card.Where(ss => ss.business_id == business_id);

            if (!String.IsNullOrEmpty(adv_search_formatted))
            {
                var cardSearch = JsonConvert.DeserializeObject<CardSearch>(HttpUtility.HtmlDecode(adv_search_formatted));
                if (cardSearch != null)
                {
                    if (cardSearch.CardTypeId.HasValue)
                    {
                        cards = from c in cards
                                from crel in dbContext.card_card_type_rel
                                where c.card_id == crel.card_id
                                && crel.card_type_id == cardSearch.CardTypeId
                                && ((crel.date_beg <= today && (crel.date_end > today || !crel.date_end.HasValue)))
                                select c;
                    }
                    if (cardSearch.CardStatusId.HasValue)
                    {
                        cards = from c in cards
                                from crel in dbContext.card_card_status_rel
                                where c.card_id == crel.card_id
                                && crel.card_status_id == cardSearch.CardStatusId
                                && ((crel.date_beg <= today && (crel.date_end > today || !crel.date_end.HasValue)))
                                select c;
                    }
                    if (cardSearch.CardId.HasValue)
                    {
                        cards = cards.Where(ss => ss.card_id == cardSearch.CardId);
                    }
                    if (cardSearch.CardNum.HasValue)
                    {
                        cards = cards.Where(ss => ss.card_num == cardSearch.CardNum);
                    }
                    if (!String.IsNullOrEmpty(cardSearch.CardNum2))
                    {
                        cards = cards.Where(ss => ss.card_num2.Trim().Equals(cardSearch.CardNum2.Trim()));
                    }
                    if (!String.IsNullOrEmpty(cardSearch.ClientName))
                    {
                        cards = from c in cards
                                from crel in dbContext.card_holder_card_rel
                                from cl in dbContext.card_holder
                                where c.card_id == crel.card_id
                                && crel.card_holder_id == cl.card_holder_id
                                && cl.card_holder_surname.Trim().ToLower().Contains(cardSearch.ClientName.Trim().ToLower())
                                && ((crel.date_beg <= today && (crel.date_end > today || !crel.date_end.HasValue)))
                                select c;
                    }
                    if (cardSearch.DateBeg.HasValue)
                    {
                        cards = cards.Where(ss => ss.date_beg >= cardSearch.DateBeg);
                    }
                    if (cardSearch.DateEnd.HasValue)
                    {
                        cards = cards.Where(ss => ((ss.date_end.HasValue && ss.date_end <= cardSearch.DateEnd) || (!ss.date_end.HasValue)));
                    }
                }
            }
            else if (!String.IsNullOrEmpty(search_formatted))
            {
                cards = cards.Where(ss => 1 == 0);
                // поиск по: фамилии клиента
                cards = cards.Union(from c in dbContext.vw_card
                                    from crel in dbContext.card_holder_card_rel
                                    from cl in dbContext.card_holder
                                    where c.business_id == business_id
                                    && c.card_id == crel.card_id
                                    && crel.card_holder_id == cl.card_holder_id
                                    && cl.card_holder_surname.Trim().ToLower().Contains(search_formatted)
                                    && ((crel.date_beg <= today && (crel.date_end > today || !crel.date_end.HasValue)))
                                    orderby c.card_num
                                    select c);
                // поиск по: номеру карты
                if (card_num_long_parse)
                    cards = cards.Union(dbContext.vw_card.Where(ss => ss.business_id == business_id && ss.card_num == card_num_long));
                // поиск по: доп номеру карты
                cards = cards.Union(dbContext.vw_card.Where(ss => ss.business_id == business_id && ss.card_num2.Trim().Equals(search_formatted)));
            }
            else
            {
                cards = cards.Where(ss => 1 == 0);
            }

            return cards.Select(card => new CardViewModel
            {
                business_id = card.business_id,
                card_id = card.card_id,
                card_num = card.card_num,
                card_num2 = card.card_num2,
                curr_bonus_percent = card.curr_bonus_percent,
                curr_bonus_value = card.curr_bonus_value,
                curr_card_status_id = card.curr_card_status_id,
                curr_card_type_id = card.curr_card_type_id,
                curr_client_id = card.curr_holder_id,
                curr_discount_percent = card.curr_discount_percent,
                curr_discount_value = card.curr_discount_value,
                curr_money_percent = card.curr_money_percent,
                curr_money_value = card.curr_money_value,
                curr_trans_count = card.curr_trans_count,
                curr_trans_sum = card.curr_trans_sum,
                date_beg = card.date_beg,
                date_end = card.date_end,
                mess = card.mess,                
                curr_card_status_name = card.curr_card_status_name,
                curr_card_type_name = card.curr_card_type_name,
                curr_client_name = card.curr_holder_name,
                card_id_str = card.card_id_str,
                card_num_str = card.card_num_str,
                card_type_group_id = card.card_type_group_id,
                curr_client_first_name = card.curr_holder_first_name,
                curr_client_second_name = card.curr_holder_second_name,
                curr_client_date_birth = card.curr_holder_date_birth,
                curr_client_phone_num = card.curr_holder_phone_num,
                crt_date = card.crt_date,
                crt_user = card.crt_user,
                from_au = card.from_au,
                curr_inactive_bonus_value = card.curr_inactive_bonus_value,
                curr_all_bonus_value = card.curr_all_bonus_value,
                curr_client_full_name = card.curr_holder_full_name,
            });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardViewModel cardAdd = (CardViewModel)item;
            if (
                (cardAdd == null)
                ||
                (cardAdd.card_num.GetValueOrDefault(0) <= 0)
                ||
                (!cardAdd.date_beg.HasValue)
                ||
                (cardAdd.curr_card_type_id.GetValueOrDefault(0) <= 0)
                ||
                (cardAdd.curr_card_status_id.GetValueOrDefault(0) <= 0)
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты карты");
                return null;
            }

            long business_id = currUser.BusinessId;
            cardAdd.business_id = business_id;
            long id = 0;

            if (cardAdd.card_num.HasValue)
            {
                long card_id_with_same_num = dbContext.card.Where(ss => ss.business_id == business_id
                    && ss.card_num.HasValue && ss.card_num == cardAdd.card_num).Select(ss => ss.card_id).FirstOrDefault();
                if (card_id_with_same_num > 0)
                {
                    modelState.AddModelError("", "Карта с номером " + cardAdd.card_num.ToString() + " уже существует, код карты " + card_id_with_same_num.ToString());
                    return null;
                }
            }

            string user_name = currUser.UserName;
            DateTime now = DateTime.Now;

            card card = new card();
            card.business_id = business_id;
            card.card_num = cardAdd.card_num;
            card.card_num2 = cardAdd.card_num2;
            card.date_beg = cardAdd.date_beg;
            card.date_end = cardAdd.date_end;
            card.mess = cardAdd.mess;
            card.curr_card_status_id = cardAdd.curr_card_status_id;
            card.curr_card_type_id = cardAdd.curr_card_type_id;
            card.curr_trans_sum = cardAdd.curr_trans_sum;
            card.curr_trans_count = 0;
            card.crt_date = now;
            card.crt_user = currUser.UserName;
            card.from_au = false;

            dbContext.card.Add(card);

            // вставка в card_transaction, card_nominal, card_card_type_rel, card_card_status_rel, card_holder_card_rel
            card_card_status_rel card_card_status_rel = null;
            card_card_type_rel card_card_type_rel = null;
            card_holder_card_rel card_holder_card_rel = null;
            card_transaction card_transaction = null;
            card_nominal card_nominal = null;

            card_transaction = new card_transaction();
            card_transaction.card = card;
            card_transaction.date_beg = DateTime.Now;
            card_transaction.status = (int)Enums.CardTransactionStatus.DONE;
            card_transaction.trans_kind = (int)Enums.CardTransactionType.START_SALDO;
            card_transaction.trans_num = 1;
            card_transaction.trans_sum = cardAdd.curr_trans_sum;
            card_transaction.user_name = user_name;
            dbContext.card_transaction.Add(card_transaction);

            if (cardAdd.curr_card_type_id.HasValue)
            {
                card_card_type_rel = new card_card_type_rel();
                card_card_type_rel.card = card;
                card_card_type_rel.card_type_id = (long)cardAdd.curr_card_type_id;
                card_card_type_rel.date_beg = cardAdd.date_beg;
                //card_card_type_rel.date_end = cardAdd.date_end;
                card_card_type_rel.date_end = null;
                card_card_type_rel.ord = 1;
                dbContext.card_card_type_rel.Add(card_card_type_rel);

                // insert card_nominal
                List<card_type_unit> card_type_unit_list = dbContext.card_type_unit.Where(ss => ss.card_type_id == cardAdd.curr_card_type_id).ToList();
                if ((card_type_unit_list != null) && (card_type_unit_list.Count > 0))
                {
                    foreach (var card_type_unit in card_type_unit_list)
                    {
                        card_nominal = new card_nominal();
                        card_nominal.card = card;
                        card_nominal.date_beg = card.date_beg;
                        //card_nominal.card_transaction = card_transaction;
                        card_nominal.unit_type = card_type_unit.unit_type;
                        switch ((Enums.CardUnitTypeEnum)card_type_unit.unit_type)
                        {
                            case Enums.CardUnitTypeEnum.BONUS_PERCENT:
                                card_nominal.unit_value = cardAdd.curr_bonus_percent;
                                break;
                            case Enums.CardUnitTypeEnum.BONUS_VALUE:
                                card_nominal.unit_value = cardAdd.curr_bonus_value;
                                break;
                            case Enums.CardUnitTypeEnum.DISCOUNT_PERCENT:
                                card_nominal.unit_value = cardAdd.curr_discount_percent;
                                break;
                            case Enums.CardUnitTypeEnum.DISCOUNT_VALUE:
                                card_nominal.unit_value = cardAdd.curr_discount_value;
                                break;
                            case Enums.CardUnitTypeEnum.MONEY:
                                card_nominal.unit_value = cardAdd.curr_money_value;
                                break;
                            default:
                                card_nominal.unit_value = 0;
                                break;
                        }
                        card_nominal.crt_date = DateTime.Now;
                        card_nominal.crt_user = user_name;
                        dbContext.card_nominal.Add(card_nominal);
                    }
                }
            }

            if (cardAdd.curr_card_status_id.HasValue)
            {
                card_card_status_rel = new card_card_status_rel();
                card_card_status_rel.card = card;
                card_card_status_rel.card_status_id = (long)cardAdd.curr_card_status_id;
                card_card_status_rel.date_beg = cardAdd.date_beg;
                //card_card_status_rel.date_end = cardAdd.date_end;
                card_card_status_rel.date_end = null;
                dbContext.card_card_status_rel.Add(card_card_status_rel);
            }

            if (cardAdd.new_client)
            {
                card_holder card_holder = new card_holder();
                card_holder.address = cardAdd.new_address;
                card_holder.business_id = (long)card.business_id;
                card_holder.card_holder_fname = cardAdd.new_client_fname;
                card_holder.card_holder_name = cardAdd.new_client_name;
                card_holder.card_holder_surname = cardAdd.new_client_surname;
                card_holder.comment = cardAdd.new_comment;
                card_holder.date_birth = cardAdd.new_date_birth;
                card_holder.email = cardAdd.new_email;
                card_holder.phone_num = cardAdd.new_phone_num;
                dbContext.card_holder.Add(card_holder);
                card.curr_holder_id = card_holder.card_holder_id;
                card_holder_card_rel = new card_holder_card_rel();
                card_holder_card_rel.card = card;
                card_holder_card_rel.card_holder = card_holder;
                card_holder_card_rel.date_beg = card.date_beg;
                dbContext.card_holder_card_rel.Add(card_holder_card_rel);
            }
            else if (cardAdd.curr_client_id.HasValue)
            {
                card_holder_card_rel = new card_holder_card_rel();
                card_holder_card_rel.card = card;
                card_holder_card_rel.card_holder_id = (long)cardAdd.curr_client_id;
                card_holder_card_rel.date_beg = card.date_beg;
                dbContext.card_holder_card_rel.Add(card_holder_card_rel);
                card.curr_holder_id = cardAdd.curr_client_id;
            }

            dbContext.SaveChanges();

            id = card.card_id;

            return new CardViewModel(dbContext.vw_card.Where(ss => ss.card_id == id).FirstOrDefault());
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardViewModel cardEdit = (CardViewModel)item;
            if (
                (cardEdit == null)
                ||
                (cardEdit.card_num.GetValueOrDefault(0) <= 0)
                ||
                (!cardEdit.date_beg.HasValue)
                ||
                (cardEdit.curr_card_type_id.GetValueOrDefault(0) <= 0)
                ||
                (cardEdit.curr_card_status_id.GetValueOrDefault(0) <= 0)
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты карты");
                return null;
            }

            long business_id = currUser.BusinessId;
            cardEdit.business_id = business_id;

            var card = (from c in dbContext.card
                        where c.business_id == business_id
                        && c.card_id == cardEdit.card_id
                        select c)
                       .FirstOrDefault();

            if (card == null)
            {
                modelState.AddModelError("", "Не найдена карта с кодом " + cardEdit.card_id.ToString());
                return null;
            }

            modelBeforeChanges = new CardViewModel(card);

            if (cardEdit.card_num.HasValue)
            {
                long card_id_with_same_num = dbContext.card.Where(ss => ss.business_id == business_id && ss.card_id != cardEdit.card_id
                    && ss.card_num.HasValue && ss.card_num == cardEdit.card_num).Select(ss => ss.card_id).FirstOrDefault();
                if (card_id_with_same_num > 0)
                {
                    modelState.AddModelError("", "Карта с номером " + cardEdit.card_num.ToString() + " уже существует, код карты " + card_id_with_same_num.ToString());
                    return null;
                }
            }

            string user_name = currUser.UserName;
            DateTime today = DateTime.Today;

            card.date_beg = cardEdit.date_beg;
            card.date_end = cardEdit.date_end;
            card.card_num = cardEdit.card_num;
            card.card_num2 = cardEdit.card_num2;
            card.mess = cardEdit.mess;

            // чтоб тип можно сменить на новый тип только такой же группы (т.е. с такими же карточными единицами),
            // если по карте есть продажи
            if ((cardEdit.curr_card_type_id.HasValue) && (card.curr_card_type_id != cardEdit.curr_card_type_id))
            {
                var old_group = dbContext.card_type.Where(ss => ss.card_type_id == card.curr_card_type_id).Select(ss => ss.card_type_group_id).FirstOrDefault();
                var new_group = dbContext.card_type.Where(ss => ss.card_type_id == cardEdit.curr_card_type_id).Select(ss => ss.card_type_group_id).FirstOrDefault();
                if (old_group != new_group)
                {
                    var card_sale_exists = dbContext.card_transaction.Where(ss => ss.card_id == card.card_id
                        && ss.trans_kind == (int)Enums.CardTransactionType.CALC
                        && ss.status == (int)Enums.CardTransactionStatus.DONE
                        ).FirstOrDefault();
                    if (card_sale_exists != null)
                    {
                        modelState.AddModelError("", "Нельзя сменить тип карты на тип из другой группы - есть продажи");
                        return null;
                    }
                }

                var card_type_rel_existing = dbContext.card_card_type_rel.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).FirstOrDefault();
                if (card_type_rel_existing != null)
                {
                    card_type_rel_existing.date_end = today;
                }
                card_card_type_rel card_type_rel_new = new card_card_type_rel();
                card_type_rel_new.card_id = card.card_id;
                card_type_rel_new.card_type_id = (long)cardEdit.curr_card_type_id;
                card_type_rel_new.date_beg = today;
                card_type_rel_new.date_end = null;
                card_type_rel_new.ord = 1;
                dbContext.card_card_type_rel.Add(card_type_rel_new);

                card.curr_card_type_id = cardEdit.curr_card_type_id;
            }

            if ((cardEdit.curr_card_status_id.HasValue) && (card.curr_card_status_id != cardEdit.curr_card_status_id))
            {
                var card_status_rel_existing = dbContext.card_card_status_rel.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).FirstOrDefault();
                if (card_status_rel_existing != null)
                {
                    card_status_rel_existing.date_end = today;
                }
                card_card_status_rel card_status_rel_new = new card_card_status_rel();
                card_status_rel_new.card_id = card.card_id;
                card_status_rel_new.card_status_id = (long)cardEdit.curr_card_status_id;
                card_status_rel_new.date_beg = today;
                card_status_rel_new.date_end = null;
                dbContext.card_card_status_rel.Add(card_status_rel_new);

                card.curr_card_status_id = cardEdit.curr_card_status_id;
            }

            card_holder_card_rel client_rel_new = null;
            card_holder_card_rel client_rel_existing = null;
            if (cardEdit.new_client)
            {
                card_holder card_holder = new card_holder();
                card_holder.address = cardEdit.new_address;
                card_holder.business_id = (long)card.business_id;
                card_holder.card_holder_fname = cardEdit.new_client_fname;
                card_holder.card_holder_name = cardEdit.new_client_name;
                card_holder.card_holder_surname = cardEdit.new_client_surname;
                card_holder.comment = cardEdit.new_comment;
                card_holder.date_birth = cardEdit.new_date_birth;
                card_holder.email = cardEdit.new_email;
                card_holder.phone_num = cardEdit.new_phone_num;
                dbContext.card_holder.Add(card_holder);
                card.curr_holder_id = card_holder.card_holder_id;
                client_rel_existing = dbContext.card_holder_card_rel.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).FirstOrDefault();
                if (client_rel_existing != null)
                {
                    client_rel_existing.date_end = today;
                }
                client_rel_new = new card_holder_card_rel();
                client_rel_new.card_id = card.card_id;
                client_rel_new.card_holder = card_holder;
                client_rel_new.date_beg = today;
                client_rel_new.date_end = null;
                dbContext.card_holder_card_rel.Add(client_rel_new);
            }
            else if ((cardEdit.curr_client_id.HasValue) && (card.curr_holder_id != cardEdit.curr_client_id))
            {
                client_rel_existing = dbContext.card_holder_card_rel.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).FirstOrDefault();
                if (client_rel_existing != null)
                {
                    client_rel_existing.date_end = today;
                }
                client_rel_new = new card_holder_card_rel();
                client_rel_new.card_id = card.card_id;
                client_rel_new.card_holder_id = (long)cardEdit.curr_client_id;
                client_rel_new.date_beg = today;
                client_rel_new.date_end = null;
                dbContext.card_holder_card_rel.Add(client_rel_new);

                card.curr_holder_id = cardEdit.curr_client_id;
            }
            else if ((!cardEdit.curr_client_id.HasValue) && (card.curr_holder_id.HasValue))
            {
                client_rel_existing = dbContext.card_holder_card_rel.Where(ss => ss.card_id == card.card_id && ss.card_holder_id == card.curr_holder_id && !ss.date_end.HasValue).FirstOrDefault();
                if (client_rel_existing != null)
                {
                    client_rel_existing.date_end = today;
                }
                card.curr_holder_id = null;
            }

            if ((card.curr_trans_sum.GetValueOrDefault(0) != cardEdit.curr_trans_sum.GetValueOrDefault(0)) && (cardEdit.IsTransSumChanged))
            {
                var trans_sum = cardEdit.curr_trans_sum.GetValueOrDefault(0) - card.curr_trans_sum.GetValueOrDefault(0);
                var trans_num = dbContext.card_transaction.Where(ss => ss.card_id == card.card_id).Select(ss => ss.trans_num).Max();
                card_transaction card_transaction = new card_transaction();
                card_transaction.card_id = card.card_id;
                card_transaction.date_beg = DateTime.Now;
                card_transaction.status = (int)Enums.CardTransactionStatus.DONE;
                card_transaction.trans_kind = (int)Enums.CardTransactionType.HAND_INPUT;
                card_transaction.trans_num = trans_num.GetValueOrDefault(0) + 1;
                card_transaction.trans_sum = trans_sum;
                card_transaction.user_name = user_name;
                dbContext.card_transaction.Add(card_transaction);

                card.curr_trans_sum = cardEdit.curr_trans_sum;
                //card.curr_trans_count = card.curr_trans_count.HasValue ? card.curr_trans_count + 1 : 1;
            }

            card_nominal card_nominal_new = null;
            card_nominal card_nominal_existing = null;
            List<card_type_unit> card_type_unit_list = dbContext.card_type_unit.Where(ss => ss.card_type_id == cardEdit.curr_card_type_id).ToList();
            if ((card_type_unit_list != null) && (card_type_unit_list.Count > 0))
            {
                foreach (var card_type_unit in card_type_unit_list)
                {
                    card_nominal_new = new card_nominal();
                    card_nominal_new.card = card;
                    card_nominal_new.date_beg = card.date_beg;
                    card_nominal_new.unit_type = card_type_unit.unit_type;
                    switch ((Enums.CardUnitTypeEnum)card_type_unit.unit_type)
                    {
                        case Enums.CardUnitTypeEnum.BONUS_PERCENT:
                            if (!cardEdit.IsBonusPercentChanged)
                                continue;
                            card_nominal_new.unit_value = cardEdit.curr_bonus_percent;
                            break;
                        case Enums.CardUnitTypeEnum.BONUS_VALUE:
                            if (!cardEdit.IsBonusValueChanged)
                                continue;
                            card_nominal_new.unit_value = cardEdit.curr_bonus_value;
                            break;
                        case Enums.CardUnitTypeEnum.DISCOUNT_PERCENT:
                            if (!cardEdit.IsDiscPercentChanged)
                                continue;
                            card_nominal_new.unit_value = cardEdit.curr_discount_percent;
                            break;
                        //case Enums.CardUnitTypeEnum.DISCOUNT_VALUE:
                            //card_nominal_new.unit_value = cardEdit.curr_discount_value;
                            //break;
                        case Enums.CardUnitTypeEnum.MONEY:
                            if (!cardEdit.IsMoneyValueChanged)
                                continue;
                            card_nominal_new.unit_value = cardEdit.curr_money_value;
                            break;
                        default:
                            card_nominal_new.unit_value = 0;
                            break;
                    }
                    card_nominal_new.crt_date = DateTime.Now;
                    card_nominal_new.crt_user = user_name;

                    card_nominal_existing = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id && ss.unit_type == card_type_unit.unit_type && !ss.date_end.HasValue).FirstOrDefault();
                    if (card_nominal_existing != null)
                    {
                        if (card_nominal_new.unit_value.GetValueOrDefault(0) != card_nominal_existing.unit_value.GetValueOrDefault(0))
                        {
                            card_nominal_existing.date_end = today;
                            dbContext.card_nominal.Add(card_nominal_new);
                        }
                    }
                    else
                    {
                        dbContext.card_nominal.Add(card_nominal_new);
                    }
                }
            }

            dbContext.SaveChanges();

            var card_types = dbContext.card_type.Where(ss => ss.business_id == cardEdit.business_id);
            var card_statuses = dbContext.card_status;
            return new CardViewModel(dbContext.vw_card.Where(ss => ss.card_id == cardEdit.card_id).FirstOrDefault(), card_types, card_statuses);

        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((CardViewModel)item).card_id;

            if (dbContext.card_transaction.Where(ss => ss.card_id == id
                && ((ss.trans_kind == (short)Enums.CardTransactionType.CALC) || (ss.trans_kind == (short)Enums.CardTransactionType.RETURN))
                ).Count() > 0)
            {
                modelState.AddModelError("serv_result", "По данной карте есть транзакции, код карты " + id.ToString());
                return false;
            }

            long business_id = currUser.BusinessId;

            var card = (from c in dbContext.card
                        where c.business_id == business_id
                        && c.card_id == id
                        select c)
                       .FirstOrDefault();

            if (card == null)
            {
                modelState.AddModelError("serv_result", "Не найдена карта с кодом " + id.ToString());
                return false;
            }

            // доч объекты: card_card_status_rel, card_card_type_rel, card_nominal, card_holder_card_rel, card_transaction
            List<card_card_status_rel> card_card_status_rel_list = dbContext.card_card_status_rel.Where(ss => ss.card_id == id).ToList();
            if ((card_card_status_rel_list != null) && (card_card_status_rel_list.Count > 0))
            {
                dbContext.card_card_status_rel.RemoveRange(card_card_status_rel_list);
            }

            List<card_card_type_rel> card_card_type_rel_list = dbContext.card_card_type_rel.Where(ss => ss.card_id == id).ToList();
            if ((card_card_type_rel_list != null) && (card_card_type_rel_list.Count > 0))
            {
                dbContext.card_card_type_rel.RemoveRange(card_card_type_rel_list);
            }

            List<card_nominal> card_nominal_list = dbContext.card_nominal.Where(ss => ss.card_id == id).ToList();
            if ((card_nominal_list != null) && (card_nominal_list.Count > 0))
            {
                dbContext.card_nominal.RemoveRange(card_nominal_list);
            }

            List<card_holder_card_rel> card_holder_card_rel_list = dbContext.card_holder_card_rel.Where(ss => ss.card_id == id).ToList();
            if ((card_holder_card_rel_list != null) && (card_holder_card_rel_list.Count > 0))
            {
                dbContext.card_holder_card_rel.RemoveRange(card_holder_card_rel_list);
            }

            List<card_additional_num> card_additional_num_list = dbContext.card_additional_num.Where(ss => ss.card_id == id).ToList();
            if ((card_additional_num_list != null) && (card_additional_num_list.Count > 0))
            {
                dbContext.card_additional_num.RemoveRange(card_additional_num_list);
            }

            List<card_transaction> card_transaction_list = dbContext.card_transaction.Where(ss => ss.card_id == id).ToList();
            if ((card_transaction_list != null) && (card_transaction_list.Count > 0))
            {
                var card_transaction_detail_list = from ct in dbContext.card_transaction
                                                   from ctd in dbContext.card_transaction_detail
                                                   where ct.card_id == id
                                                   && ct.card_trans_id == ctd.card_trans_id
                                                   select ctd;
                var card_transaction_unit_list = from ct in dbContext.card_transaction
                                                 from ctu in dbContext.card_transaction_unit
                                                 where ct.card_id == id
                                                 && ct.card_trans_id == ctu.card_trans_id
                                                 select ctu;

                dbContext.card_transaction_detail.RemoveRange(card_transaction_detail_list);
                dbContext.card_transaction_unit.RemoveRange(card_transaction_unit_list);
                dbContext.card_transaction.RemoveRange(card_transaction_list);
            }

            dbContext.card.Remove(card);
            dbContext.SaveChanges();

            return true;
        }

        public IQueryable<SimpleComboLong> GetList_Sprav(string num)
        {            
            var card_num_search = num.ToLower();
            return dbContext.vw_card
                .Where(ss => ss.business_id == currUser.BusinessId && ss.card_num_str.ToLower().StartsWith(card_num_search))
                .OrderByDescending(ss => ss.card_num)
                .Select(ss => new SimpleComboLong() { obj_id = ss.card_id, obj_name = ss.card_num_str, })
                ;
        }

    }
}