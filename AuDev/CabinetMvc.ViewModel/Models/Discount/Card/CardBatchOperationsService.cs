﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using System.Data;
using System.Data.Objects;
using System.Data.OleDb;
using System.Xml;
using Npgsql;
using NpgsqlTypes;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardBatchOperationsService : IAuBaseService
    {
        Tuple<string, string> BuildCardNum(CardBatchOperationsViewModel batch);
    }

    public class CardBatchOperationsService : AuBaseService, ICardBatchOperationsService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.batch.Where(ss => ss.business_id == parent_id)
                .Select(ss => new CardBatchOperationsViewModel()
                {
                    batch_id = ss.batch_id,
                    trans_sum_add = ss.trans_sum_add,
                    trans_sum_add_value = ss.trans_sum_add_value,
                    trans_sum_add_mode = (Enums.BatchOperationsMode)ss.trans_sum_add_mode,
                    disc_percent_add = ss.disc_percent_add,
                    disc_percent_add_value = ss.disc_percent_add_value,
                    disc_percent_add_mode = (Enums.BatchOperationsMode)ss.disc_percent_add_mode,
                    bonus_percent_add = ss.bonus_percent_add,
                    bonus_percent_add_value = ss.bonus_percent_add_value,
                    bonus_percent_add_mode = (Enums.BatchOperationsMode)ss.bonus_percent_add_mode,
                    bonus_add = ss.bonus_add,
                    bonus_add_value = ss.bonus_add_value,
                    bonus_add_mode = (Enums.BatchOperationsMode)ss.bonus_add_mode,
                    money_add = ss.money_add,
                    money_add_value = ss.money_add_value,
                    money_add_mode = (Enums.BatchOperationsMode)ss.money_add_mode,
                    mess_add = ss.mess_add,
                    mess_add_value = ss.mess_add_value,
                    mess_add_mode = (Enums.BatchOperationsMode)ss.mess_add_mode,
                    card_list_mode = ss.card_list_mode,
                    Message = ss.mess,
                    GridFilterString = ss.filter_string,
                    GridSortString = ss.sort_string,
                    GridPageSize = ss.page_size,
                    GridPageNumber = ss.page_num,
                    batch_name = ss.batch_name,
                    batch_type = (Enums.BatchType)ss.batch_type,
                    state = (Enums.BatchState)ss.state,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    state_date = ss.state_date,
                    state_user = ss.state_user,
                    business_id = ss.business_id,
                    record_cnt = ss.record_cnt,
                    processed_cnt = ss.processed_cnt,
                    error_cnt = ss.error_cnt,
                    date_beg = ss.date_beg,
                    date_end = ss.date_end,
                    card_count = ss.card_count,
                    num_first = ss.num_first,
                    num_count = ss.num_count,
                    next_num_first = ss.next_num_first,
                    next_num_step = ss.next_num_step,
                    use_check_value = ss.use_check_value,
                    result_num_first = "",
                    result_num_last = "",
                    change_card_status = ss.change_card_status,
                    change_card_type = ss.change_card_type,
                    del_cards = ss.del_cards,
                    card_id_list = ss.card_id_list,
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.batch.Where(ss => ss.batch_id == id).FirstOrDefault();
            return res == null ? null : new CardBatchOperationsViewModel(res);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardBatchOperationsViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardBatchOperationsViewModel batchAdd = (CardBatchOperationsViewModel)item;
            if (batchAdd == null)
            {
                modelState.AddModelError("", "Не задана пакетная операция");
                return null;
            }
            
            batch batch = new batch();
            batch.batch_name = batchAdd.batch_name;
            batch.batch_type = (int)batchAdd.batch_type;
            batch.bonus_add = batchAdd.bonus_add;
            batch.bonus_add_mode = (int)batchAdd.bonus_add_mode;
            batch.bonus_add_value = batchAdd.bonus_add_value;
            batch.money_add = batchAdd.money_add;
            batch.money_add_mode = (int)batchAdd.money_add_mode;
            batch.money_add_value = batchAdd.money_add_value;
            batch.bonus_percent_add = batchAdd.bonus_percent_add;
            batch.bonus_percent_add_mode = (int)batchAdd.bonus_percent_add_mode;
            batch.bonus_percent_add_value = batchAdd.bonus_percent_add_value;
            batch.mess_add = batchAdd.mess_add;
            batch.mess_add_mode = (int)batchAdd.mess_add_mode;
            batch.mess_add_value = batchAdd.mess_add_value;
            batch.card_list_mode = batchAdd.card_list_mode;
            /*
            batch.business_id = batchAdd.business_id;                        
            batch.crt_user = batchAdd.crt_user;
            */
            batch.business_id = currUser.BusinessId;
            batch.crt_user = currUser.UserName;
            batch.crt_date = batchAdd.crt_date;
            batch.disc_percent_add = batchAdd.disc_percent_add;
            batch.disc_percent_add_mode = (int)batchAdd.disc_percent_add_mode;
            batch.disc_percent_add_value = batchAdd.disc_percent_add_value;
            batch.filter_string = batchAdd.GridFilterString;
            batch.mess = batchAdd.Message;
            batch.page_num = batchAdd.GridPageNumber;
            batch.page_size = batchAdd.GridPageSize;
            batch.sort_string = batchAdd.GridSortString;
            batch.state = (int)Enums.BatchState.Running;
            batch.state_date = DateTime.Now;
            //batch.state_user = batchAdd.state_user;
            batch.state_user = batch.crt_user;
            batch.trans_sum_add = batchAdd.trans_sum_add;
            batch.trans_sum_add_mode = (int)batchAdd.trans_sum_add_mode;
            batch.trans_sum_add_value = batchAdd.trans_sum_add_value;
            batch.record_cnt = batchAdd.record_cnt;
            batch.processed_cnt = batchAdd.processed_cnt;
            batch.error_cnt = batchAdd.error_cnt;

            batch.card_type_id = batchAdd.card_type_id;
            batch.card_status_id = batchAdd.card_status_id;
            batch.date_beg = batchAdd.date_beg;
            batch.date_end = batchAdd.date_end;
            batch.card_count = batchAdd.card_count;
            batch.num_first = batchAdd.num_first;
            batch.num_count = batchAdd.num_count;
            batch.next_num_first = batchAdd.next_num_first;
            batch.next_num_step = batchAdd.next_num_step;
            batch.use_check_value = batchAdd.use_check_value;

            batch.change_card_status = batchAdd.change_card_status;
            batch.change_card_type = batchAdd.change_card_type;
            batch.del_cards = batchAdd.del_cards;

            batch.card_id_list = batchAdd.card_id_list;

            dbContext.batch.Add(batch);
            dbContext.SaveChanges();
            batchAdd.batch_id = batch.batch_id;
            
            Task taskBatch = new Task(() =>
            {                
                RunBatch(batchAdd);
            }
            );
            taskBatch.Start();

            return new CardBatchOperationsViewModel(batch);
        }

        private void RunBatch(CardBatchOperationsViewModel batch)
        {
            /*
            var res = xGetItem(batch_id);
            CardBatchOperationsViewModel batch = (CardBatchOperationsViewModel)res;
            */

            switch ((Enums.BatchType)batch.batch_type)
            {
                case Enums.BatchType.BatchOperations:
                    xRunBatch_Operations(batch);
                    return;
                case Enums.BatchType.BatchAdd:
                    xRunBatch_Add(batch);
                    return;
                case Enums.BatchType.BatchImport:
                    xRunBatch_Import(batch);
                    return;
                default:
                    return;
            }
            
        }

        private void xRunBatch_Operations(CardBatchOperationsViewModel batch)
        {
            var serv = new CabinetServ.CabinetServiceClient();
            var res = serv.RunBatch_Operations("iguana", batch.batch_id, currUser.UserName, true);
        }

        private void xRunBatch_Add(CardBatchOperationsViewModel batch)
        {
            var serv = new CabinetServ.CabinetServiceClient();
            var res = serv.RunBatch_Add("iguana", batch.batch_id, currUser.UserName, true);
        }

        public Tuple<string, string> BuildCardNum(CardBatchOperationsViewModel batch)
        {
            try
            {
                string res1 = xBuildCardNum(batch.num_first.GetValueOrDefault(0), batch.num_count.GetValueOrDefault(0), batch.next_num_first.GetValueOrDefault(0), batch.use_check_value);
                if (res1.Length > 19)
                    throw new Exception("Превышена длина (19 символов)");

                string res2 = xBuildCardNum(batch.num_first.GetValueOrDefault(0), batch.num_count.GetValueOrDefault(0), batch.next_num_first.GetValueOrDefault(0) + (batch.next_num_step.GetValueOrDefault(0) * (batch.card_count.GetValueOrDefault(0) - 1)), batch.use_check_value);

                if (res2.Length > 19)
                    throw new Exception("Превышена длина (19 символов)");


                Tuple<string, string> res = new Tuple<string, string>(res1, res2);
                return res;

            }
            catch (Exception ex)
            {
                return new Tuple<string, string>(GlobalUtil.ExceptionInfo(ex), "");
            }
        }

        private string xBuildCardNum(int num_first, int num_count, int num_curr, bool use_check_value)
        {

            string mask_num_string = num_first.ToString()
                + new string('0', num_count - num_first.ToString().Length - num_curr.ToString().Length - (use_check_value ? 1 : 0))
                + num_curr.ToString();

            if (use_check_value)
            {
                char[] mask_char_array = (mask_num_string + "x").ToCharArray();
                Array.Reverse(mask_char_array);

                int summ1 = 0;
                int summ2 = 0;
                int summ3 = 0;
                int summ4 = 0;
                int i;
                int k = 0;

                for (i = 1; i <= mask_char_array.Length - 1; i = i + 2)
                {
                    summ1 += Convert.ToInt16(mask_char_array[i].ToString());
                }
                summ1 = summ1 * 3;

                for (i = 2; i <= mask_char_array.Length - 1; i = i + 2)
                {
                    summ2 += Convert.ToInt16(mask_char_array[i].ToString());
                }

                summ3 = summ1 + summ2;

                summ4 = summ3;
                while (true)
                {
                    if (((summ4 + k) % 10) == 0)
                        break;
                    k++;
                }

                mask_num_string += k.ToString();
            }

            return mask_num_string;

        }

        private void xRunBatch_Import(CardBatchOperationsViewModel batch)
        {
            long batch_id = batch.batch_id;
            DateTime today = DateTime.Today;

            batch db_batch = dbContext.batch.Where(ss => ss.batch_id == batch_id).FirstOrDefault();
            batch_error err = null;

            db_batch.record_cnt = 0;
            db_batch.processed_cnt = 0;
            db_batch.error_cnt = 0;

            int processed_cnt = 0;
            int error_cnt = 0;

            var res1 = xRunBatch_Import_File(batch.file1, 1);
            if (!res1.Item2)
            {
                err = new batch_error();
                err.batch_id = batch_id;
                err.card_num = 0;
                err.card_num2 = "";
                err.mess = res1.Item3;
                dbContext.batch_error.Add(err);

                db_batch.state = (int)Enums.BatchState.Finished;
                db_batch.state_date = DateTime.Now;
                db_batch.mess = res1.Item3;
                db_batch.error_cnt = 1;
                dbContext.SaveChanges();

                return;
            }

            var res2 = xRunBatch_Import_File(batch.file2, 2);
            if (!res2.Item2)
            {
                err = new batch_error();
                err.batch_id = batch_id;
                err.card_num = 0;
                err.card_num2 = "";
                err.mess = res2.Item3;
                dbContext.batch_error.Add(err);

                db_batch.state = (int)Enums.BatchState.Finished;
                db_batch.state_date = DateTime.Now;
                db_batch.mess = res2.Item3;
                db_batch.error_cnt = 1;
                dbContext.SaveChanges();
                return;
            }

            db_batch.record_cnt = res2.Item1;
            dbContext.SaveChanges();

            var res3 = xRunBatch_Import_Db(batch.card_type_id.GetValueOrDefault(0), batch.card_status_id.GetValueOrDefault(0), batch.action_for_existing_summ, batch.action_for_existing_disc_percent, batch.action_for_existing_bonus);
            if (!res3.Item1)
            {
                err = new batch_error();
                err.batch_id = batch_id;
                err.card_num = 0;
                err.card_num2 = "";
                err.mess = res3.Item2;
                dbContext.batch_error.Add(err);

                db_batch.state = (int)Enums.BatchState.Finished;
                db_batch.state_date = DateTime.Now;
                db_batch.mess = res3.Item2;
                db_batch.error_cnt = 1;
                dbContext.SaveChanges();
                return;
            }

            db_batch.state = (int)Enums.BatchState.Finished;
            db_batch.state_date = DateTime.Now;
            db_batch.mess = "Операция завершена";
            db_batch.processed_cnt = db_batch.record_cnt;
            db_batch.error_cnt = error_cnt;

            dbContext.SaveChanges();
        }

        private Tuple<int, bool, string> xRunBatch_Import_File(string fileLocation, int file_num)
        {
            OleDbConnection excelConnection = null;
            DataTable dt = null;
            int record_cnt = 0;
            try
            {
                DataSet ds = new DataSet();
                if (!String.IsNullOrEmpty(fileLocation))
                {
                    string fileExtension = System.IO.Path.GetExtension(fileLocation);

                    if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    {
                        /*
                        string fileLocation = @"C:\Release\Cabinet\File\Done\" + System.IO.Path.GetFileName(file.FileName);

                        if (System.IO.File.Exists(fileLocation))
                        {
                            System.IO.File.Delete(fileLocation);
                        }

                        //file.SaveAs(fileLocation);
                        StaticUtil.WriteFileFromStream(file.InputStream, fileLocation);
                        */

                        string excelConnectionString = string.Empty;
                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        if (fileExtension == ".xls")
                        {
                            //excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                        }
                        /*
                        else if (fileExtension == ".xlsx")
                        {

                            excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                        }
                        */
                        excelConnection = new OleDbConnection(excelConnectionString);
                        excelConnection.Open();
                        dt = new DataTable();

                        dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        if (dt == null)
                        {
                            return new Tuple<int, bool, string>(0, false, "Excel Table = null");
                        }

                        String[] excelSheets = new String[dt.Rows.Count];
                        int t = 0;

                        foreach (DataRow row in dt.Rows)
                        {
                            excelSheets[t] = row["TABLE_NAME"].ToString();
                            t++;
                        }
                        OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);


                        string query = string.Format("Select * from [{0}]", excelSheets[0]);
                        using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                        {
                            dataAdapter.Fill(ds);
                        }
                    }

                    long business_id =currUser.BusinessId;

                    string connString = ViewModelUtil.GetAuMainDbConnectionString();
                    string commText = "";
                    //record_cnt = ds.Tables[0].Rows.Count;

                    using (Npgsql.NpgsqlConnection conn = new NpgsqlConnection(connString))
                    {
                        conn.Open();

                        if (file_num == 1)
                        {
                            commText = "delete from discount.import_discount;";
                        }
                        else
                        {
                            commText = "delete from discount.import_discount_card;";
                        }
                        NpgsqlCommand comm = new NpgsqlCommand(commText, conn);
                        comm.ExecuteNonQuery();

                        commText = "";
                        var curr_num = 0;
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            if (curr_num > 10)
                            {
                                comm.CommandText = commText;
                                comm.ExecuteNonQuery();
                                curr_num = 0;
                                commText = "";
                            }

                            if (String.IsNullOrEmpty(ds.Tables[0].Rows[i][0].ToString()))
                                continue;

                            if (file_num == 1)
                            {
                                commText += " insert into discount.import_discount (id_discount, name_discount, proc_discount) values (";
                                commText += ds.Tables[0].Rows[i][0].ToString()
                                    + ", '" + ds.Tables[0].Rows[i][1].ToString()
                                    + "', " + ds.Tables[0].Rows[i][2].ToString().Replace(',', '.')
                                    + "); ";
                            }
                            else
                            {
                                commText += " insert into discount.import_discount_card (id_card, num_card, date_card, lname, fname, mname, total_sum, id_discount, barcode, total_bonus) values (";
                                commText += ds.Tables[0].Rows[i][0].ToString()
                                    + ", '" + ds.Tables[0].Rows[i][1].ToString()
                                    + "', '" + ds.Tables[0].Rows[i][2].ToString()
                                    + "', '" + ds.Tables[0].Rows[i][3].ToString()
                                    + "', '" + ds.Tables[0].Rows[i][4].ToString()
                                    + "', '" + ds.Tables[0].Rows[i][5].ToString()
                                    + "', " + ds.Tables[0].Rows[i][6].ToString().Replace(',', '.')
                                    + ", " + ds.Tables[0].Rows[i][7].ToString()
                                    + ", '" + ds.Tables[0].Rows[i][8].ToString()
                                    + "', " + ds.Tables[0].Rows[i][9].ToString().Replace(',', '.')
                                    + "); ";
                            }
                            curr_num++;
                            record_cnt++;
                        }

                        if (!String.IsNullOrEmpty(commText))
                        {
                            comm.CommandText = commText;
                            comm.ExecuteNonQuery();
                        }
                    }

                    dt = null;
                    if (excelConnection != null)
                        excelConnection.Close();
                    return new Tuple<int, bool, string>(record_cnt, true, "Excel Table = null");
                }
                else
                {
                    dt = null;
                    if (excelConnection != null)
                        excelConnection.Close();
                    return new Tuple<int, bool, string>(0, false, "Нет строк в файле");
                }

                //return true;                
            }
            catch (Exception ex)
            {
                dt = null;
                if (excelConnection != null)
                    excelConnection.Close();
                return new Tuple<int, bool, string>(0, false, GlobalUtil.ExceptionInfo(ex));
            }
        }

        private Tuple<bool, string> xRunBatch_Import_Db(long card_type_id, long card_status_id, int action_for_existing_summ, int action_for_existing_disc_percent, int action_for_existing_bonus)
        {
            long business_id = currUser.BusinessId;
            string user_name = currUser.UserName;
            try
            {
                string connString = ViewModelUtil.GetAuMainDbConnectionString();
                string commText = "";

                using (Npgsql.NpgsqlConnection conn = new NpgsqlConnection(connString))
                {
                    conn.Open();

                    commText = "select discount.import_card("
                            + business_id.ToString()
                            + ", "
                            + card_type_id.ToString()
                            + ", "
                            + card_status_id.ToString()
                            + ", "
                            + action_for_existing_summ.ToString()
                            + ", "
                            + action_for_existing_disc_percent.ToString()
                            + ", "
                            + action_for_existing_bonus.ToString()
                            + ", "
                            + "'" + user_name.ToString() + "'"
                            + ")";

                    NpgsqlCommand comm = new NpgsqlCommand(commText, conn);
                    comm.ExecuteScalar();

                    return new Tuple<bool, string>(true, "");
                }
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, GlobalUtil.ExceptionInfo(ex));
            }
        }

    }
}