using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardBatchOperationsViewModel : AuBaseViewModel
    {
        public CardBatchOperationsViewModel()
            : base(Enums.MainObjectType.BATCH)
        {            
            trans_sum_add = false;
            trans_sum_add_value = 0;
            trans_sum_add_mode = Enums.BatchOperationsMode.Add;

            bonus_add = false;
            bonus_add_value = 0;
            bonus_add_mode = Enums.BatchOperationsMode.Add;

            money_add = false;
            money_add_value = 0;
            money_add_mode = Enums.BatchOperationsMode.Add;

            disc_percent_add = false;
            disc_percent_add_value = 0;
            disc_percent_add_mode = Enums.BatchOperationsMode.Add;
            
            bonus_percent_add = false;
            bonus_percent_add_value = 0;
            bonus_percent_add_mode = Enums.BatchOperationsMode.Add;

            mess_add = false;
            mess_add_value = "";
            mess_add_mode = Enums.BatchOperationsMode.Add;

            card_list_mode = 0;
            Message = "";
            GridFilterString = "";
            GridSortString = "";
            GridPageSize = 0;
            GridPageNumber = 0;

            batch_id = 0;
            batch_name = "";
            batch_type = Enums.BatchType.BatchOperations;
            state = Enums.BatchState.None;
            crt_date = DateTime.Now;
            // !!!
            //crt_user = currUser.UserName;
            state_date = DateTime.Now;
            //state_user = currUser.UserName;
            //business_id = currUser.BusinessId;
            record_cnt = 0;
            processed_cnt = 0;
            error_cnt = 0;

            date_beg = DateTime.Today;
            date_end = null;
            card_count = 10;
            num_first = 9;
            num_count = 13;
            next_num_first = 1001;
            next_num_step = 1;
            use_check_value = true;
            result_num_first = "";
            result_num_last = "";

            change_card_status = false;
            change_card_type = false;
            del_cards = false;

            action_for_existing_summ = 0;
            action_for_existing_bonus = 0;
            action_for_existing_disc_percent = 0;
            file1 = null;
            file2 = null;

            card_id_list = "";
        }

        public CardBatchOperationsViewModel(batch item)
            : base(Enums.MainObjectType.BATCH)
        {
            batch_id = item.batch_id;
            trans_sum_add = item.trans_sum_add;
            trans_sum_add_value = item.trans_sum_add_value;
            trans_sum_add_mode = (Enums.BatchOperationsMode)item.trans_sum_add_mode;
            disc_percent_add = item.disc_percent_add;
            disc_percent_add_value = item.disc_percent_add_value;
            disc_percent_add_mode = (Enums.BatchOperationsMode)item.disc_percent_add_mode;
            bonus_percent_add = item.bonus_percent_add;
            bonus_percent_add_value = item.bonus_percent_add_value;
            bonus_percent_add_mode = (Enums.BatchOperationsMode)item.bonus_percent_add_mode;
            bonus_add = item.bonus_add;
            bonus_add_value = item.bonus_add_value;
            bonus_add_mode = (Enums.BatchOperationsMode)item.bonus_add_mode;
            money_add = item.money_add;
            money_add_value = item.money_add_value;
            money_add_mode = (Enums.BatchOperationsMode)item.money_add_mode;
            mess_add = item.mess_add;
            mess_add_value = item.mess_add_value;
            mess_add_mode = (Enums.BatchOperationsMode)item.mess_add_mode;
            card_list_mode = item.card_list_mode;
            Message = item.mess;
            GridFilterString = item.filter_string;
            GridSortString = item.sort_string;
            GridPageSize = item.page_size;
            GridPageNumber = item.page_num;
            batch_name = item.batch_name;
            batch_type = (Enums.BatchType)item.batch_type;
            state = (Enums.BatchState)item.state;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            state_date = item.state_date;
            state_user = item.state_user;
            business_id = item.business_id;
            record_cnt = item.record_cnt;
            processed_cnt = item.processed_cnt;
            error_cnt = item.error_cnt;

            card_type_id = item.card_type_id;
            card_status_id = item.card_status_id;
            date_beg = item.date_beg;
            date_end = item.date_end;
            card_count = item.card_count;
            num_first = item.num_first;
            num_count = item.num_count;
            next_num_first = item.next_num_first;
            next_num_step = item.next_num_step;
            use_check_value = item.use_check_value;
            result_num_first = "";
            result_num_last = "";

            change_card_status = item.change_card_status;
            change_card_type = item.change_card_type;
            del_cards = item.del_cards;

            action_for_existing_summ = 0;
            action_for_existing_bonus = 0;
            action_for_existing_disc_percent = 0;
            file1 = null;
            file2 = null;

            card_id_list = item.card_id_list;
        }

        [Display(Name = "�������� �����")]
        public bool trans_sum_add { get; set; }

        [Display(Name = "������� �������� � �����")]
        public decimal? trans_sum_add_value { get; set; }

        [Display(Name = "��������/�������� �����")]
        public Enums.BatchOperationsMode trans_sum_add_mode { get; set; }

        [Display(Name = "�������� % ������")]
        public bool disc_percent_add { get; set; }

        [Display(Name = "������� �������� � % ������")]
        public decimal? disc_percent_add_value { get; set; }

        [Display(Name = "��������/�������� % ������")]
        public Enums.BatchOperationsMode disc_percent_add_mode { get; set; }

        [Display(Name = "�������� �������� %")]
        public bool bonus_percent_add { get; set; }

        [Display(Name = "������� �������� � �������� %")]
        public decimal? bonus_percent_add_value { get; set; }

        [Display(Name = "��������/�������� �������� %")]
        public Enums.BatchOperationsMode bonus_percent_add_mode { get; set; }

        [Display(Name = "�������� ������")]
        public bool bonus_add { get; set; }

        [Display(Name = "������� �������� �������")]
        public decimal? bonus_add_value { get; set; }

        [Display(Name = "��������/�������� ������")]
        public Enums.BatchOperationsMode bonus_add_mode { get; set; }

        [Display(Name = "�������� ����� �� ������������")]
        public bool money_add { get; set; }
        [Display(Name = "������� �������� ����� �� ������������")]
        public decimal? money_add_value { get; set; }
        [Display(Name = "��������/�������� ����� �� ������������")]
        public Enums.BatchOperationsMode money_add_mode { get; set; }

        [Display(Name = "�������� �����������")]
        public bool mess_add { get; set; }

        [Display(Name = "����� �����������")]
        public string mess_add_value { get; set; }

        [Display(Name = "��������/�������� �����������")]
        public Enums.BatchOperationsMode mess_add_mode { get; set; }

        [Display(Name = "� ���� ���������")]
        public int card_list_mode { get; set; }

        [Display(Name = "���������")]
        public string Message { get; set; }

        [Display(Name = "������ �������")]
        public string GridFilterString { get; set; }

        [Display(Name = "������ ����������")]
        public string GridSortString { get; set; }

        [Display(Name = "����� �� ��������")]
        public int? GridPageSize { get; set; }

        [Display(Name = "����� ��������")]
        public int? GridPageNumber { get; set; }

        [Key()]
        [Display(Name = "���")]
        public long batch_id { get; set; }

        [Display(Name = "������������")]
        public string batch_name { get; set; }

        [Display(Name = "���")]
        public Enums.BatchType batch_type { get; set; }

        [Display(Name = "������")]
        public Enums.BatchState state { get; set; }

        [Display(Name = "����� ������")]
        public DateTime? crt_date { get; set; }

        [Display(Name = "��� ������")]
        public string crt_user { get; set; }

        [Display(Name = "����� ��������� � ������")]
        public DateTime? state_date { get; set; }

        [Display(Name = "��� ��������� � ������")]
        public string state_user { get; set; }

        [Display(Name = "��� �����������")]
        public Nullable<long> business_id { get; set; }

        [Display(Name = "����� �����")]
        public Nullable<int> record_cnt { get; set; }

        [Display(Name = "���������� �����")]
        public Nullable<int> processed_cnt { get; set; }

        [Display(Name = "��������� �����")]
        public Nullable<int> error_cnt { get; set; }

        [Display(Name = "��� ����")]
        public long? card_type_id { get; set; }

        [Display(Name = "������ ����")]
        public long? card_status_id { get; set; }

        [Display(Name = "��������� �")]
        [DataType(DataType.Date)]
        public DateTime? date_beg { get; set; }

        [Display(Name = "���-�� ����")]
        public int? card_count { get; set; }

        [Display(Name = "������ ������� (�� 1 �� 3)")]
        public int? num_first { get; set; }

        [Display(Name = "���-�� ��������")]
        public int? num_count { get; set; }

        [Display(Name = "������ �����")]
        public int? next_num_first { get; set; }

        [Display(Name = "��� ���������� ������")]
        public int? next_num_step { get; set; }

        [Display(Name = "������������ ����������� ������")]
        public bool use_check_value { get; set; }

        [Display(Name = "��� ���������� (������ �����)")]
        public string result_num_first { get; set; }

        [Display(Name = "��� ���������� (��������� �����)")]
        public string result_num_last { get; set; }

        [Display(Name = "������� ������ ����")]
        public bool change_card_status { get; set; }

        [Display(Name = "������� ��� ����")]
        public bool change_card_type { get; set; }

        [Display(Name = "������� �����")]
        public bool del_cards { get; set; }

        [Display(Name = "������ ����� ����")]
        public string card_id_list { get; set; }

        [JsonIgnore()]
        [Display(Name = "������������ ����������� �����")]
        public int action_for_existing_summ { get; set; }
        [JsonIgnore()]
        [Display(Name = "������������ % ������")]
        public int action_for_existing_disc_percent { get; set; }
        [JsonIgnore()]
        [Display(Name = "������������ �������� %")]
        public int action_for_existing_bonus { get; set; }
        [JsonIgnore()]
        [Display(Name = "���� � ����� DISCOUNT")]
        public string file1 { get; set; }
        //public HttpPostedFileBase file1 { get; set; }
        [JsonIgnore()]
        [Display(Name = "���� � ����� DISCOUNT_CARD")]
        public string file2 { get; set; }
        //public HttpPostedFileBase file2 { get; set; }

        [Display(Name = "��������� ��")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "������")]
        [JsonIgnore()]
        public string state_str
        {
            get
            {
                switch (state)
                {
                    case Enums.BatchState.Running:
                        return "�����������";
                    case Enums.BatchState.Finished:
                        return "���������";
                    default:
                        return "-";
                };
            }
            set
            {
                _state_str = value;
            }
        }
        private string _state_str;
    }


}
