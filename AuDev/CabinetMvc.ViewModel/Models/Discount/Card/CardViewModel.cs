﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{    
    public class CardViewModel : AuBaseViewModel
    {
        public CardViewModel()
            : base(Enums.MainObjectType.CARD)
        {
            date_beg = DateTime.Today;
            date_end = null;
            curr_bonus_percent = 0;
            curr_bonus_value = 0;
            curr_discount_percent = 0;
            curr_discount_value = 0;
            curr_money_percent = 0;
            curr_money_value = 0;
            curr_trans_count = 0;
            curr_trans_sum = 0;
            //
            CardTypeList = null;
            CardStatusList =  null;
            new_client = false;
            //
            IsBonusPercentChanged = false;
            IsBonusValueChanged = false;
            IsDiscPercentChanged = false;
            IsMoneyValueChanged = false;
            IsTransSumChanged = false;
        }

        public CardViewModel(vw_card card, IQueryable<card_type> cardTypeList = null, IQueryable<card_status> cardStatusList = null)
            : base(Enums.MainObjectType.CARD)
        {
            business_id = card.business_id;
            card_id = card.card_id;
            card_id_str = card.card_id_str;
            card_num_str = card.card_num_str;
            card_num = card.card_num;
            card_num2 = card.card_num2;
            curr_bonus_percent = card.curr_bonus_percent;
            curr_bonus_value = card.curr_bonus_value;
            curr_card_status_id = card.curr_card_status_id;
            curr_card_status_name = card.curr_card_status_name;
            curr_card_type_id = card.curr_card_type_id;
            curr_card_type_name = card.curr_card_type_name;
            curr_client_id = card.curr_holder_id;
            curr_client_name = card.curr_holder_name;
            curr_client_phone_num = card.curr_holder_phone_num;
            curr_discount_percent = card.curr_discount_percent;
            curr_discount_value = card.curr_discount_value;
            curr_money_percent = card.curr_money_percent;
            curr_money_value = card.curr_money_value;
            curr_trans_count = card.curr_trans_count;
            curr_trans_sum = card.curr_trans_sum;
            date_beg = card.date_beg;
            date_end = card.date_end;
            mess = card.mess;            
            card_type_group_id = card.card_type_group_id;
            curr_client_first_name = card.curr_holder_first_name;
            curr_client_second_name = card.curr_holder_second_name;
            curr_client_date_birth = card.curr_holder_date_birth;
            crt_date = card.crt_date;
            crt_user = card.crt_user;
            from_au = card.from_au;
            curr_inactive_bonus_value = card.curr_inactive_bonus_value;
            curr_all_bonus_value = card.curr_all_bonus_value;
            curr_client_full_name = card.curr_holder_full_name;
            //
            CardTypeList = cardTypeList == null ? null : cardTypeList.Select(ss => new SpravCardTypeViewModel() { card_type_id = ss.card_type_id, card_type_name = ss.card_type_name, card_type_group_id = ss.card_type_group_id, });
            CardStatusList = cardStatusList == null ? null : cardStatusList.Select(ss => new SpravCardStatusViewModel(){ card_status_id = ss.card_status_id, card_status_name = ss.card_status_name, no_manual_change = ss.no_manual_change, });
            new_client = false;
            //
            IsBonusPercentChanged = false;
            IsBonusValueChanged = false;
            IsDiscPercentChanged = false;
            IsMoneyValueChanged = false;
            IsTransSumChanged = false;
        }

        public CardViewModel(card card)
            : base(Enums.MainObjectType.CARD)
        {
            business_id = card.business_id;
            card_id = card.card_id;
            card_id_str = card.card_id.ToString();
            card_num = card.card_num;
            card_num_str = card.card_num.ToString();
            card_num2 = card.card_num2;
            curr_card_status_id = card.curr_card_status_id;            
            curr_card_type_id = card.curr_card_type_id;
            curr_client_id = card.curr_holder_id;
            curr_trans_count = card.curr_trans_count;
            curr_trans_sum = card.curr_trans_sum;
            date_beg = card.date_beg;
            date_end = card.date_end;
            mess = card.mess;            
            batch_id = card.batch_id;
            crt_date = card.crt_date;
            crt_user = card.crt_user;
            from_au = card.from_au;
            new_client = false;
        }
        
        [Key()]
        [Display(Name = "Код карты")]
        public long card_id { get; set; }
        
        [Display(Name = "Действует с")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Не задана дата начала действия")]        
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "Действует по")]
        [DataType(DataType.Date)]        
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "Код статуса карты")]
        [Required(ErrorMessage = "Не задан статус карты")]
        public Nullable<long> curr_card_status_id { get; set; }

        [Display(Name = "Код типа карты")]
        [Required(ErrorMessage = "Не задан тип карты")]
        public Nullable<long> curr_card_type_id { get; set; }

        [Display(Name = "№ карты")]
        [Required(ErrorMessage = "Не задан номер карты")]

        [RegularExpression("^[0-9]{1,19}$", ErrorMessage = "Номер должен содержать только числа (макс = 19 чисел)")]        
        //@(Html.Kendo().NumericTextBoxFor(model => model.card_num).HtmlAttributes(new { @class = "form-control attr-change", @id = "card_num" }).Min(0).Format("#").Events(ev => ev.Spin("onAttrChanged")))
        //[DataType("integer")]
        public Nullable<long> card_num { get; set; }

        [Display(Name = "Доп номер")]
        public string card_num2 { get; set; }

        [Display(Name = "Код организации")]        
        public Nullable<long> business_id { get; set; }

        [Display(Name = "Кол-во транзакций")]
        public Nullable<long> curr_trans_count { get; set; }

        [Display(Name = "Сумма транзакций")]
        public Nullable<decimal> curr_trans_sum { get; set; }

        [Display(Name = "Код владельца карты")]
        public Nullable<long> curr_client_id { get; set; }

        [Display(Name = "Сумма бонусов")]
        public Nullable<decimal> curr_bonus_value { get; set; }

        [Display(Name = "Бонусный %")]
        public Nullable<decimal> curr_bonus_percent { get; set; }

        [Display(Name = "Сумма скидки")]
        public Nullable<decimal> curr_discount_value { get; set; }

        [Display(Name = "Процент скидки")]
        public Nullable<decimal> curr_discount_percent { get; set; }

        [Display(Name = "Сумма на сертификате")]
        public Nullable<decimal> curr_money_value { get; set; }

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public Nullable<decimal> curr_money_percent { get; set; }

        [Display(Name = "Комментарий")]
        public string mess { get; set; }

        [Display(Name = "Код пакетной операции")]
        public Nullable<long> batch_id { get; set; }

        [Display(Name = "Код группы типа карты")]
        public Nullable<long> card_type_group_id { get; set; }

        [JsonIgnore()]
        [Display(Name = "Статус карты")]
        public string curr_card_status_name { get; set; }
        
        [JsonIgnore()]
        [Display(Name = "Тип карты")]
        public string curr_card_type_name { get; set; }

        [JsonIgnore()]
        [Display(Name = "Фамилия владельца")]
        public string curr_client_name { get; set; }

        [JsonIgnore()]
        [Display(Name = "Владелец карты")]
        public string curr_client_full_name { get; set; }

        [JsonIgnore()]
        [Display(Name = "Телефон владельца")]
        public string curr_client_phone_num { get; set; }

        [JsonIgnore()]
        [Display(Name = "Код карты (строка)")]
        public string card_id_str { get; set; }        

        [JsonIgnore()]
        [Display(Name = "Код владельца карты (строка)")]
        public string curr_client_id_str { get { return !curr_client_id.HasValue ? "" : curr_client_id.ToString(); } }

        [JsonIgnore()]
        [Display(Name = "№ карты (строка)")]
        public string card_num_str { get; set; }

        [JsonIgnore()]        
        public bool IsSelected { get; set; }

        [Display(Name = "Имя владельца")]
        public string curr_client_first_name { get; set; }

        [Display(Name = "Отчество владельца")]
        public string curr_client_second_name { get; set; }

        [Display(Name = "Дата рождения")]
        public Nullable<System.DateTime> curr_client_date_birth { get; set; }

        [Display(Name = "Когда создана")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Кто создал")]
        public string crt_user { get; set; }

        [Display(Name = "Создана из Кассы")]
        public bool from_au { get; set; }

        [Display(Name = "Сумма неактивных бонусов")]
        public Nullable<decimal> curr_inactive_bonus_value { get; set; }

        [Display(Name = "Всего бонусов")]
        public Nullable<decimal> curr_all_bonus_value { get; set; }

        [JsonIgnore()]
        public bool IsTransSumChanged { get; set; }

        [JsonIgnore()]
        public bool IsDiscPercentChanged { get; set; }

        [JsonIgnore()]
        public bool IsBonusPercentChanged { get; set; }

        [JsonIgnore()]
        public bool IsBonusValueChanged { get; set; }

        [JsonIgnore()]
        public bool IsMoneyValueChanged { get; set; }

        [JsonIgnore()]        
        [DefaultValue(null)]
        public IEnumerable<SpravCardTypeViewModel> CardTypeList { get; set; }

        [JsonIgnore()]        
        [DefaultValue(null)]
        public IEnumerable<SpravCardStatusViewModel> CardStatusList { get; set; }

        // создание нового клиента
        [JsonIgnore()]
        [Display(Name = "Фамилия")]
        public bool new_client { get; set; }
        [JsonIgnore()]
        [Display(Name = "Фамилия")]
        public string new_client_surname { get; set; }
        [JsonIgnore()]
        [Display(Name = "Имя")]
        public string new_client_name { get; set; }
        [JsonIgnore()]
        [Display(Name = "Отчество")]
        public string new_client_fname { get; set; }
        [JsonIgnore()]
        [Display(Name = "Дата рождения")]
        public Nullable<System.DateTime> new_date_birth { get; set; }
        [JsonIgnore()]
        [Display(Name = "Телефон")]
        public string new_phone_num { get; set; }
        [JsonIgnore()]
        [Display(Name = "Адрес")]
        public string new_address { get; set; }
        [JsonIgnore()]
        [Display(Name = "Эл почта")]
        public string new_email { get; set; }
        [JsonIgnore()]
        [Display(Name = "Комментарий")]
        public string new_comment { get; set; }

    }
}