using System;
using System.Collections.Generic;

namespace CabinetMvc.ViewModel.Discount
{    
    public class CardSearch
    {
        public CardSearch()
        {
            //
        }
    
        public long? CardNum { get; set; }
        public string CardNum2 { get; set; }
        public string ClientName { get; set; }
        public long? CardTypeId { get; set; }
        public long? CardStatusId { get; set; }
        public long? CardId { get; set; }
        public DateTime? DateBeg { get; set; }
        public DateTime? DateEnd { get; set; }
    }
}
