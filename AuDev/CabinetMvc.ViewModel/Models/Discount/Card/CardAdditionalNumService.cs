﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardAdditionalNumService : IAuBaseService
    {
        //
    }

    public class CardAdditionalNumService : AuBaseService, ICardAdditionalNumService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.card_additional_num.Where(ss => ss.item_id == id)
                .Select(ss => new CardAdditionalNumViewModel()
                {
                    item_id = ss.item_id,
                    card_id = ss.card_id,
                    card_num = ss.card_num,
                })
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.card_additional_num.Where(ss => ss.card_id == parent_id)
                .Select(ss => new CardAdditionalNumViewModel()
                {
                    item_id = ss.item_id,
                    card_id = ss.card_id,
                    card_num = ss.card_num,
                });
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardAdditionalNumViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardAdditionalNumViewModel cardNumAdd = (CardAdditionalNumViewModel)item;
            if (
                (cardNumAdd == null)
                ||
                (String.IsNullOrEmpty(cardNumAdd.card_num))
                )
            {
                modelState.AddModelError("", "Не задан номер карты");
                return null;
            }

            card_additional_num card_additional_num = new card_additional_num();
            card_additional_num.card_id = cardNumAdd.card_id;
            card_additional_num.card_num = cardNumAdd.card_num;
            dbContext.card_additional_num.Add(card_additional_num);
            dbContext.SaveChanges();
            return new CardAdditionalNumViewModel(card_additional_num);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardAdditionalNumViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardAdditionalNumViewModel cardNumEdit = (CardAdditionalNumViewModel)item;
            if (
                (cardNumEdit == null)
                ||
                (String.IsNullOrEmpty(cardNumEdit.card_num))
                )
            {
                modelState.AddModelError("", "Не задан номер карты");
                return null;
            }


            card_additional_num card_additional_num = dbContext.card_additional_num.Where(ss => ss.item_id == cardNumEdit.item_id).FirstOrDefault();
            if (card_additional_num == null)
            {
                modelState.AddModelError("", "Не найден номер с кодом " + cardNumEdit.item_id.ToString());
                return null;
            }

            modelBeforeChanges = new CardAdditionalNumViewModel(card_additional_num);
            
            card_additional_num.card_num = cardNumEdit.card_num;
            dbContext.SaveChanges();
            return new CardAdditionalNumViewModel(card_additional_num);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardAdditionalNumViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }

            long id = ((CardAdditionalNumViewModel)item).item_id;

            card_additional_num card_additional_num = dbContext.card_additional_num.Where(ss => ss.item_id == id).FirstOrDefault();
            if (card_additional_num == null)
            {
                modelState.AddModelError("serv_result", "Не найден номер с кодом " + id.ToString());
                return false;
            }

            dbContext.card_additional_num.Remove(card_additional_num);
            dbContext.SaveChanges();

            return true;
        }

    }
}