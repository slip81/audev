using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardBatchOperationsErrorViewModel : AuBaseViewModel
    {
        public CardBatchOperationsErrorViewModel()
            : base(Enums.MainObjectType.BATCH_ERROR)
        {
            //
        }

        public CardBatchOperationsErrorViewModel(batch_error item)
            : base(Enums.MainObjectType.BATCH_ERROR)
        {
            batch_error_id = item.batch_error_id;
            batch_id = item.batch_id;
            card_id = item.card_id;
            card_num = item.card_num;
            card_num2 = item.card_num2;
            mess = item.mess;
        }

        [Key()]
        public long batch_error_id { get; set; }
        public long batch_id { get; set; }
        public Nullable<long> card_id { get; set; }
        public Nullable<long> card_num { get; set; }
        public string card_num2 { get; set; }
        public string mess { get; set; }
    }

}
