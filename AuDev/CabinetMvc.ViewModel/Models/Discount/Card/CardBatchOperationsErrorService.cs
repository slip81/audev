﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardBatchOperationsErrorService : IAuBaseService
    {
        //
    }

    public class CardBatchOperationsErrorService : AuBaseService, ICardBatchOperationsErrorService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.batch_error.Where(ss => ss.batch_id == parent_id)
                .Select(ss => new CardBatchOperationsErrorViewModel()
                {
                    batch_error_id = ss.batch_error_id,
                    batch_id = ss.batch_id,
                    card_id = ss.card_id,
                    card_num = ss.card_num,
                    card_num2 = ss.card_num2,
                    mess = ss.mess,
                });
        }
    }
}