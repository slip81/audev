﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardStatusViewModel : AuBaseViewModel
    {
        public CardStatusViewModel()
            :base(Enums.MainObjectType.CARD_STATUS)
        {
            //
        }

        [Key()]
        public long card_status_id { get; set; }
        public string card_status_name { get; set; }
        public Nullable<long> card_status_group_id { get; set; }
    }
}