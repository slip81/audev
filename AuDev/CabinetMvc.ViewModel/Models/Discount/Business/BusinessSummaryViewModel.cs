﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class BusinessSummaryViewModel : AuBaseViewModel
    {
        public BusinessSummaryViewModel()
            : base(Enums.MainObjectType.SALE_SUMMARY)
        {

        }

        public BusinessSummaryViewModel(vw_business_summary item)
            : base(Enums.MainObjectType.BUSINESS)
        {
            business_id = item.business_id;
            card_cnt = item.card_cnt;
            card_type_group_1_cnt = item.card_type_group_1_cnt;
            card_type_group_2_cnt = item.card_type_group_2_cnt;
            card_type_group_3_cnt = item.card_type_group_3_cnt;
            card_holder_cnt = item.card_holder_cnt;
            card_trans_cnt = item.card_trans_cnt;
            card_trans_today_cnt = item.card_trans_today_cnt;
        }

        [Key()]
        [Display(Name = "Код организации")]
        public long business_id { get; set; }

        [Display(Name = "card_cnt")]
        public Nullable<int> card_cnt { get; set; }

        [Display(Name = "card_type_group_1_cnt")]
        public Nullable<int> card_type_group_1_cnt { get; set; }

        [Display(Name = "card_type_group_2_cnt")]
        public Nullable<int> card_type_group_2_cnt { get; set; }

        [Display(Name = "card_type_group_3_cnt")]
        public Nullable<int> card_type_group_3_cnt { get; set; }

        [Display(Name = "card_holder_cnt")]
        public Nullable<int> card_holder_cnt { get; set; }

        [Display(Name = "card_trans_cnt")]
        public Nullable<int> card_trans_cnt { get; set; }

        [Display(Name = "card_trans_today_cnt")]
        public Nullable<int> card_trans_today_cnt { get; set; }

        [JsonIgnore]
        public IEnumerable<BusinessSummaryGraphViewModel> graph { get; set; }
    }
}