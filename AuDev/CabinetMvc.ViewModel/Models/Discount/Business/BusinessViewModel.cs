﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class BusinessViewModel : AuBaseViewModel
    {
        public BusinessViewModel()
            : base(Enums.MainObjectType.BUSINESS)
        {
            add_to_timezone = 2;
            IsScannerEqualsReader = true;
        }

        public BusinessViewModel(business item, IEnumerable<business_org> business_org_list = null, IEnumerable<business_org_user> business_org_user_list = null)
            : base(Enums.MainObjectType.BUSINESS)
        {
            business_id = item.business_id;
            business_name = item.business_name;
            add_to_timezone = item.add_to_timezone;
            IsCardScanOnly = item.card_scan_only == 1 ? true : false;
            IsScannerEqualsReader = item.scanner_equals_reader == 1 ? true : false;
            allow_card_add = item.allow_card_add;
            cabinet_client_id = item.cabinet_client_id;
            business_name_alt = item.business_name_alt;
            allow_phone_num = item.allow_phone_num;
            allow_card_nominal_change = item.allow_card_nominal_change;
            this.business_org_list = business_org_list != null ? business_org_list.Select(ss => new BusinessOrgViewModel(ss) { business_name = item.business_name }) : null;
        }

        [Key()]
        [Display(Name = "Код организации")]
        public long business_id { get; set; }

        [Display(Name="Название организации")]
        [Required(ErrorMessage = "Не задано название организации")]
        public string business_name { get; set; }

        [Display(Name = "Часовой пояс")]
        [Required(ErrorMessage = "Не задан часовой пояс")]
        public Nullable<int> add_to_timezone { get; set; }

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public string business_id_str { get {return business_id.ToString(); } }
        [JsonIgnore()]        
        public string add_to_timezone_str { get { return "MCK" + ((add_to_timezone.GetValueOrDefault(0) + 2 <= 0) ? "" : (" +" + (add_to_timezone.GetValueOrDefault(0) + 2).ToString())); } }
        
        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public IEnumerable<BusinessOrgViewModel> business_org_list { get; set; }

        [Display(Name = "Запрет ручного выбора карт")]        
        public bool IsCardScanOnly { get; set; }

        [Display(Name = "Не различать сканер штрих-кодов и картридер при выборе карты")]
        public bool IsScannerEqualsReader { get; set; }

        [Display(Name = "Разрешено добавлять карты через Кассу")]
        public bool allow_card_add { get; set; }

        [Display(Name = "Код клиента")]        
        public Nullable<int> cabinet_client_id { get; set; }

        [Display(Name = "Название для отчетов")]
        public string business_name_alt { get; set; }

        [Display(Name = "Разрешено отображение телефонов")]
        public bool allow_phone_num { get; set; }

        [Display(Name = "Разрешено изменять данные по карте из Кассы")]
        public bool allow_card_nominal_change { get; set; }
    }
}