﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IBusinessSummaryService : IAuBaseService
    {
        IEnumerable<BusinessSummaryGraphViewModel> GetGraph1(long business_id);
        List<BusinessSummaryGraph2ViewModel> GetGraph2(long business_id);
    }

    public class BusinessSummaryService : AuBaseService, IBusinessSummaryService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            //var business_graph = dbContext.vw_business_summary_graph.Where(ss => ss.business_id == id).ToList();
            var business_summary = dbContext.vw_business_summary.Where(ss => ss.business_id == id).Select(ss => new BusinessSummaryViewModel()
            {
                business_id = ss.business_id,
                card_cnt = ss.card_cnt,
                card_type_group_1_cnt = ss.card_type_group_1_cnt,
                card_type_group_2_cnt = ss.card_type_group_2_cnt,
                card_type_group_3_cnt = ss.card_type_group_3_cnt,
                card_holder_cnt = ss.card_holder_cnt,
                card_trans_cnt = ss.card_trans_cnt,
                card_trans_today_cnt = ss.card_trans_today_cnt,
            })
            .FirstOrDefault();

            if (business_summary == null)
                return new BusinessSummaryViewModel();
            /*
            business_summary.graph = dbContext.vw_business_summary_graph.Where(ss => ss.business_id == id).Select(tt => new BusinessSummaryGraphViewModel()
                {
                    id = tt.id,
                    business_id = tt.business_id,
                    date_beg_date = tt.date_beg_date,
                    trans_sum_total = tt.trans_sum_total,
                });
            */
            return business_summary;
        }

        public IEnumerable<BusinessSummaryGraphViewModel> GetGraph1(long business_id)
        {
            return dbContext.vw_business_summary_graph
                .Where(ss => ss.business_id == business_id)
                .Select(tt => new BusinessSummaryGraphViewModel()
            {
                id = tt.id,
                business_id = tt.business_id,
                date_beg_date = tt.date_beg_date,
                trans_sum_total = tt.trans_sum_total,
            });
        }

        public List<BusinessSummaryGraph2ViewModel> GetGraph2(long business_id)
        {
            return dbContext.vw_business_summary_graph2
                .Where(ss => ss.business_id == business_id)
                .Select(tt => new BusinessSummaryGraph2ViewModel()
                {
                    id = tt.id,
                    business_id = tt.business_id,
                    date_beg_date = tt.date_beg_date,
                    trans_sum_total = tt.trans_sum_total,
                    trans_sum_discount = tt.trans_sum_discount,
                    trans_sum_with_discount = tt.trans_sum_with_discount,
                    trans_sum_bonus_for_card = tt.trans_sum_bonus_for_card,
                    trans_sum_bonus_for_pay = tt.trans_sum_bonus_for_pay,
                })
                .ToList();
        }
    }
}