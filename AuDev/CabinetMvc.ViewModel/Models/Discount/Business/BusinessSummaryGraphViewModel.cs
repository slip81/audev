﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class BusinessSummaryGraphViewModel : AuBaseViewModel
    {
        public BusinessSummaryGraphViewModel()            
        {

        }

        public BusinessSummaryGraphViewModel(vw_business_summary_graph item)            
        {
            id = item.id;
            business_id = item.business_id;
            date_beg_date = item.date_beg_date;
            trans_sum_total = item.trans_sum_total;
        }

        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }
        [Display(Name = "Код организации")]
        public long business_id { get; set; }
        [Display(Name = "date_beg_date")]
        public Nullable<System.DateTime> date_beg_date { get; set; }
        [Display(Name = "trans_sum_total")]
        public Nullable<decimal> trans_sum_total { get; set; }
    }
}