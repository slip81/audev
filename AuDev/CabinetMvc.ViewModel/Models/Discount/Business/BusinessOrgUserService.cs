﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IBusinessOrgUserService : IAuBaseService
    {
        //
    }

    public class BusinessOrgUserService : AuBaseService, IBusinessOrgUserService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.business_org_user.Where(ss => ss.org_user_id == id).FirstOrDefault();
            return res == null ? null : new BusinessOrgUserViewModel(res);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            
            return    from bou in dbContext.business_org_user
                      from vca in dbContext.vw_cab_user
                      where bou.user_name.Trim().ToLower().Equals(vca.user_name.Trim().ToLower())
                      && bou.org_id == parent_id
                      select new BusinessOrgUserViewModel()
                      {
                          org_user_id = bou.org_user_id,
                          user_name = bou.user_name,
                          org_id = bou.org_id,
                          user_descr = bou.user_descr,
                          user_pwd = "*****",
                          RoleId = vca.role_id,
                      };
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessOrgUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            BusinessOrgUserViewModel businessOrgUserAdd = (BusinessOrgUserViewModel)item;
            if (
                (businessOrgUserAdd == null)
                ||
                (String.IsNullOrEmpty(businessOrgUserAdd.user_name))
                ||
                (String.IsNullOrEmpty(businessOrgUserAdd.user_pwd))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return null;
            }

            if (dbContext.business_org_user.Where(ss => ss.user_name.Trim().ToLower().Equals(businessOrgUserAdd.user_name.Trim().ToLower())).FirstOrDefault() != null)
            {
                modelState.AddModelError("", "Существует пользователь с таким же логином");
                return null;
            }

            business_org business_org = dbContext.business_org.Where(ss => ss.org_id == businessOrgUserAdd.org_id).FirstOrDefault();
            if (business_org == null)
            {
                modelState.AddModelError("", "Не найдено отделение с кодом " + businessOrgUserAdd.org_id.ToString());
                return null;
            }

            using (var ldapServ = new LdapService())
            {
                var res = ldapServ.CreateUser(business_org.business_id.ToString(), businessOrgUserAdd.user_name.Trim(), businessOrgUserAdd.user_pwd.Trim());
                if (!res.Item1)
                {
                    modelState.AddModelError("", res.Item2);
                    return null;
                }
            }

            business_org_user business_org_user = new business_org_user();
            business_org_user.org_id = businessOrgUserAdd.org_id;
            business_org_user.user_name = businessOrgUserAdd.user_name;
            business_org_user.user_descr = String.IsNullOrEmpty(businessOrgUserAdd.user_descr) ? businessOrgUserAdd.user_name : businessOrgUserAdd.user_descr;

            dbContext.business_org_user.Add(business_org_user);

            // ставим по умолчанию роль Менеджер
            long curr_role_id = businessOrgUserAdd.RoleId.GetValueOrDefault(0) <= 0 ? 5 : (long)businessOrgUserAdd.RoleId;
            cab_user_role cab_user_role = new cab_user_role();
            cab_user_role.user_name = business_org_user.user_name;
            cab_user_role.role_id = curr_role_id;
            cab_user_role.use_defaults = true;
            dbContext.cab_user_role.Add(cab_user_role);

            var actions = dbContext.cab_role_action.Where(ss => ss.role_id == cab_user_role.role_id).ToList();
            if (actions != null)
            {
                foreach (var action in actions)
                {
                    cab_user_role_action cab_user_role_action = new cab_user_role_action();
                    cab_user_role_action.action_id = action.action_id;
                    cab_user_role_action.group_id = action.group_id;
                    cab_user_role_action.role_id = cab_user_role.role_id;
                    cab_user_role_action.user_name = cab_user_role.user_name;
                    dbContext.cab_user_role_action.Add(cab_user_role_action);
                }
            }

            dbContext.SaveChanges();

            return new BusinessOrgUserViewModel(business_org_user);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessOrgUserViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            BusinessOrgUserViewModel businessOrgUserEdit = (BusinessOrgUserViewModel)item;
            if (
                (businessOrgUserEdit == null)
                ||
                (String.IsNullOrEmpty(businessOrgUserEdit.user_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты пользователя");
                return null;
            }

            if (dbContext.business_org_user.Where(ss => ss.org_user_id != businessOrgUserEdit.org_user_id && ss.user_name.Trim().ToLower().Equals(businessOrgUserEdit.user_name.Trim().ToLower())).FirstOrDefault() != null)
            {
                modelState.AddModelError("", "Существует пользователь с таким же логином");
                return null;
            }

            business_org business_org = dbContext.business_org.Where(ss => ss.org_id == businessOrgUserEdit.org_id).FirstOrDefault();
            if (business_org == null)
            {
                modelState.AddModelError("", "Не найдено отделение с кодом " + businessOrgUserEdit.org_id.ToString());
                return null;
            }

            business_org_user business_org_user = dbContext.business_org_user.Where(ss => ss.org_user_id == businessOrgUserEdit.org_user_id).FirstOrDefault();
            if (business_org_user == null)
            {
                modelState.AddModelError("", "Не найден пользователь с кодом " + businessOrgUserEdit.org_user_id.ToString());
                return null;
            }

            modelBeforeChanges = new BusinessOrgUserViewModel(business_org_user);

            business_org_user.user_name = businessOrgUserEdit.user_name;
            business_org_user.user_descr = String.IsNullOrEmpty(businessOrgUserEdit.user_descr) ? businessOrgUserEdit.user_name : businessOrgUserEdit.user_descr;

            // при корректировании пока не реализуем возможность менять пароль у логина

            dbContext.SaveChanges();

            business_org_user = dbContext.business_org_user.Where(ss => ss.org_user_id == businessOrgUserEdit.org_user_id).FirstOrDefault();
            return new BusinessOrgUserViewModel(business_org_user);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessOrgUserViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            
            long id = ((BusinessOrgUserViewModel)item).org_user_id;
            long org_id = ((BusinessOrgUserViewModel)item).org_id;
            string user_name = ((BusinessOrgUserViewModel)item).user_name.Trim();

            business_org_user business_org_user = dbContext.business_org_user.Where(ss => ss.org_user_id == id).FirstOrDefault();
            if (business_org_user == null)
            {
                modelState.AddModelError("serv_result", "Не найден пользователь с кодом " + id.ToString());
                return false;
            }

            business_org business_org = dbContext.business_org.Where(ss => ss.org_id == org_id).FirstOrDefault();
            if (business_org == null)
            {
                modelState.AddModelError("serv_result", "Не найдено отделение с кодом " + org_id.ToString());
                return false;
            }

            using (var ldapServ = new LdapService())
            {
                var res = ldapServ.DeleteUser(business_org.business_id.ToString(), user_name);
                if (!res.Item1)
                {
                    modelState.AddModelError("serv_result", res.Item2);
                    return false;
                }
            }

            dbContext.cab_user_role_action.RemoveRange(dbContext.cab_user_role_action.Where(ss => ss.user_name == user_name));
            dbContext.cab_user_role.RemoveRange(dbContext.cab_user_role.Where(ss => ss.user_name == user_name));
            dbContext.business_org_user.Remove(business_org_user);

            dbContext.SaveChanges();

            return true;
        }

    }
}