﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IBusinessService : IAuBaseService
    {
        IQueryable<BusinessViewModel> GetList_Sprav();
    }

    public class BusinessService : AuBaseService, IBusinessService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.business.OrderBy(ss => ss.business_id).Select(ss => new BusinessViewModel()
            {
                business_id = ss.business_id,
                add_to_timezone = ss.add_to_timezone,
                business_name = ss.business_name,
                IsCardScanOnly = ss.card_scan_only == 1 ? true : false,
                IsScannerEqualsReader = ss.scanner_equals_reader == 1 ? true : false,
                allow_card_add = ss.allow_card_add,
                cabinet_client_id = ss.cabinet_client_id,
                business_name_alt = ss.business_name_alt,
                allow_phone_num = ss.allow_phone_num,
                allow_card_nominal_change = ss.allow_card_nominal_change,
            });
        }

        public IQueryable<BusinessViewModel> GetList_Sprav()
        {
            return dbContext.business.OrderBy(ss => ss.business_name).Select(ss => new BusinessViewModel()
            {
                business_id = ss.business_id, 
                business_name = ss.business_name,
            });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.business.Where(ss => ss.business_id == id).FirstOrDefault();
            return res == null ? null : new BusinessViewModel(res, res.business_org, null);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            BusinessViewModel businessAdd = (BusinessViewModel)item;
            if (
                (businessAdd == null)
                ||
                (String.IsNullOrEmpty(businessAdd.business_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты организации");
                return null;
            }

            business business = new business();
            business.business_name = businessAdd.business_name;
            business.add_to_timezone = businessAdd.add_to_timezone;
            business.card_scan_only = businessAdd.IsCardScanOnly ? 1 : 0;
            business.scanner_equals_reader = businessAdd.IsScannerEqualsReader ? (short)1 : (short)0;
            business.allow_card_add = businessAdd.allow_card_add;
            business.cabinet_client_id = businessAdd.cabinet_client_id;
            business.business_name_alt = businessAdd.business_name_alt;
            business.allow_phone_num = businessAdd.allow_phone_num;
            business.allow_card_nominal_change = businessAdd.allow_card_nominal_change;            

            business business_existing = dbContext.business.Where(ss => ss.business_name.ToLower().Trim().Equals(businessAdd.business_name.ToLower().Trim())).FirstOrDefault();
            if (business_existing != null)
            {
                modelState.AddModelError("", "Существует организация с таким же названием");
                return null;
            }

            business_existing = dbContext.business.Where(ss => ss.cabinet_client_id == businessAdd.cabinet_client_id).FirstOrDefault();
            if (business_existing != null)
            {
                modelState.AddModelError("", "Эта организация уже существует");
                return null;
            }

            dbContext.business.Add(business);            
            dbContext.SaveChanges();

            using (var ldapServ = new LdapService())
            {
                var res = ldapServ.CreateBusiness(business.business_id.ToString());
                if (!res.Item1)
                {
                    modelState.AddModelError("", res.Item2);
                    return null;
                }
            }

            return new BusinessViewModel(business);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            BusinessViewModel businessEdit = (BusinessViewModel)item;
            if (
                (businessEdit == null)
                ||
                (String.IsNullOrEmpty(businessEdit.business_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты организации");
                return null;
            }

            business business = dbContext.business.Where(ss => ss.business_id == businessEdit.business_id).FirstOrDefault();
            if (business == null)
            {
                modelState.AddModelError("", "Не найдена организация с кодом " + businessEdit.business_id.ToString());
                return null;
            }

            business business_existing = dbContext.business.Where(ss => ss.business_id != businessEdit.business_id && ss.business_name.ToLower().Trim().Equals(businessEdit.business_name.ToLower().Trim())).FirstOrDefault();
            if (business_existing != null)
            {
                modelState.AddModelError("", "Существует организация с таким же названием");
                return null;
            }

            modelBeforeChanges = new BusinessViewModel(business);

            business.business_name = businessEdit.business_name;
            business.add_to_timezone = businessEdit.add_to_timezone;
            business.card_scan_only = businessEdit.IsCardScanOnly ? 1 : 0;
            business.scanner_equals_reader = businessEdit.IsScannerEqualsReader ? (short)1 : (short)0;
            business.allow_card_add = businessEdit.allow_card_add;
            business.business_name_alt = businessEdit.business_name_alt;
            business.allow_phone_num = businessEdit.allow_phone_num;
            business.allow_card_nominal_change = businessEdit.allow_card_nominal_change;
            dbContext.SaveChanges();

            return new BusinessViewModel(business);
            
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            long id = ((BusinessViewModel)item).business_id;

            business business = null;            

            business = dbContext.business.Where(ss => ss.business_id == id).FirstOrDefault();
            if (business == null)
            {
                modelState.AddModelError("serv_result", "Организация с кодом " + id.ToString() + " не найдена");
                return false;
            }

            long card_type_id = dbContext.card_type.Where(ss => ss.business_id == id).Select(ss => ss.card_type_id).FirstOrDefault();
            if (card_type_id > 0)
            {
                modelState.AddModelError("serv_result", "У данной организации существуют типы карт");
                return false;
            }
            long card_id = dbContext.card.Where(ss => ss.business_id == id).Select(ss => ss.card_id).FirstOrDefault();
            if (card_id > 0)
            {
                modelState.AddModelError("serv_result", "У данной организации существуют карты");
                return false;
            }
            long programm_id = dbContext.programm.Where(ss => ss.business_id == id).Select(ss => ss.programm_id).FirstOrDefault();
            if (programm_id > 0)
            {
                modelState.AddModelError("serv_result", "У данной организации существуют алгоритмы расчета");
                return false;
            }
            long card_holder_id = dbContext.card_holder.Where(ss => ss.business_id == id).Select(ss => ss.card_holder_id).FirstOrDefault();
            if (card_holder_id > 0)
            {
                modelState.AddModelError("serv_result", "У данной организации существуют владельцы карт");
                return false;
            }


            List<business_org_user> business_org_user_list = dbContext.business_org_user.Where(ss => ss.business_org.business_id == id).ToList();
            if ((business_org_user_list != null) && (business_org_user_list.Count > 0))
            {
                dbContext.business_org_user.RemoveRange(business_org_user_list);
            }

            List<business_org> business_org_list = dbContext.business_org.Where(ss => ss.business_id == id).ToList();
            if ((business_org_list != null) && (business_org_list.Count > 0))
            {
                dbContext.campaign_business_org.RemoveRange(from t1 in dbContext.business_org
                                                            from t2 in dbContext.campaign_business_org
                                                            where t1.org_id == t2.org_id
                                                            && t1.business_id == id
                                                            select t2);
                dbContext.business_org.RemoveRange(business_org_list);
            }

            using (var ldapServ = new LdapService())
            {
                var res = ldapServ.DeleteBusiness(id.ToString());
                if (!res.Item1)
                {
                    modelState.AddModelError("serv_result", res.Item2);
                    return false;
                }
            }

            dbContext.business.Remove(business);
            dbContext.SaveChanges();

            return true;
        }

    }
}