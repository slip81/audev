﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class BusinessOrgUserViewModel : AuBaseViewModel
    {
        public BusinessOrgUserViewModel()
            : base(Enums.MainObjectType.BUSINESS_ORG_USER)
        {
            //
        }

        public BusinessOrgUserViewModel(business_org_user item)
            : base(Enums.MainObjectType.BUSINESS_ORG_USER)
        {
            org_user_id = item.org_user_id;
            user_name = item.user_name;
            org_id = item.org_id;
            user_descr = item.user_descr;
            user_pwd = "*****";
        }

        
        [Key()]        
        [Display(Name = "Код")]
        public long org_user_id { get; set; }

        [Display(Name = "Логин")]
        public string user_name { get; set; }

        [Display(Name = "Отделение")]
        public long org_id { get; set; }

        [Display(Name = "Описание")]
        public string user_descr { get; set; }

        [JsonIgnore()]
        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public string user_pwd { get; set; }

        [JsonIgnore()]
        [Display(Name = "Отделение")]
        public string org_id_str { get { return org_id.ToString(); } }

        [JsonIgnore()]
        [Display(Name = "Код")]
        public string org_user_id_str { get { return org_user_id.ToString(); } }

        [Display(Name = "Роль")]        
        public long? RoleId { get; set; }
    
    }
}