﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class BusinessSummaryGraph2ViewModel : AuBaseViewModel
    {
        public BusinessSummaryGraph2ViewModel()            
        {

        }

        public BusinessSummaryGraph2ViewModel(vw_business_summary_graph2 item)            
        {
            id = item.id;
            business_id = item.business_id;
            date_beg_date = item.date_beg_date;
            trans_sum_total = item.trans_sum_total;
            trans_sum_discount = item.trans_sum_with_discount;
            trans_sum_with_discount = item.trans_sum_with_discount;
            trans_sum_bonus_for_card = item.trans_sum_bonus_for_card;
            trans_sum_bonus_for_pay = item.trans_sum_bonus_for_pay;
        }

        [Key()]
        [Display(Name = "Код")]
        public int id { get; set; }
        public long business_id { get; set; }
        public Nullable<System.DateTime> date_beg_date { get; set; }
        public Nullable<decimal> trans_sum_total { get; set; }
        public Nullable<decimal> trans_sum_discount { get; set; }
        public Nullable<decimal> trans_sum_with_discount { get; set; }
        public Nullable<decimal> trans_sum_bonus_for_card { get; set; }
        public Nullable<decimal> trans_sum_bonus_for_pay { get; set; }
    }
}