﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IBusinessOrgService : IAuBaseService
    {
        //
    }

    public class BusinessOrgService : AuBaseService, IBusinessOrgService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            var res = dbContext.business_org.Where(ss => ss.org_id == id).FirstOrDefault();
            return res == null ? null : new BusinessOrgViewModel(res);
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            var query = dbContext.business_org.Where(ss => 1==1);
            if (parent_id > 0)
                query = query.Where(ss => ss.business_id == parent_id);
            else
                // кроме отделений Аурит и Монолит
                query = query.Where(ss => ss.business_id != 1007661065565111774 && ss.business_id != 1191989625543984144);

            return from t1 in query
                   from t2 in dbContext.business
                   where t1.business_id == t2.business_id
                   select new BusinessOrgViewModel()
                   {
                       org_id = t1.org_id,
                       org_name = t1.org_name,
                       business_id = t1.business_id,
                       add_to_timezone = t1.add_to_timezone.HasValue ? t1.add_to_timezone + 2 : 2,
                       IsCardScanOnly = t1.card_scan_only == 1 ? true : false,
                       IsScannerEqualsReader = t1.scanner_equals_reader == 1 ? true : false,
                       allow_card_add = t1.allow_card_add,
                       org_name_alt = t1.org_name_alt,
                       business_name = t2.business_name,
                   };
            /*
            return dbContext.business_org
                .Where(ss => ss.business_id == parent_id)
                .Select(ss => new BusinessOrgViewModel()
            {
                org_id = ss.org_id,
                org_name = ss.org_name,
                business_id = ss.business_id,
                add_to_timezone = ss.add_to_timezone.HasValue ? ss.add_to_timezone + 2 : 2,
                IsCardScanOnly = ss.card_scan_only == 1 ? true : false,
                IsScannerEqualsReader = ss.scanner_equals_reader == 1 ? true : false,
                allow_card_add = ss.allow_card_add,
                org_name_alt = ss.org_name_alt,
            }
            );
            */
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessOrgViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            BusinessOrgViewModel businessOrgAdd = (BusinessOrgViewModel)item;
            if (
                (businessOrgAdd == null)
                ||
                (String.IsNullOrEmpty(businessOrgAdd.org_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты отделения");
                return null;
            }

            if (dbContext.business_org.Where(ss => ss.business_id == businessOrgAdd.business_id && ss.org_name.Trim().ToLower().Equals(businessOrgAdd.org_name.Trim().ToLower())).FirstOrDefault() != null)
            {
                modelState.AddModelError("", "Существует отделение с таким же названием");
                return null;
            }

            business_org business_org = new business_org();

            business_org.org_name = businessOrgAdd.org_name;
            business_org.add_to_timezone = businessOrgAdd.add_to_timezone;
            business_org.business_id = businessOrgAdd.business_id;
            business_org.card_scan_only = businessOrgAdd.IsCardScanOnly ? 1 : 0;
            business_org.scanner_equals_reader = businessOrgAdd.IsScannerEqualsReader ? (short)1 : (short)0;
            business_org.allow_card_add = businessOrgAdd.allow_card_add;
            business_org.org_name_alt = businessOrgAdd.org_name_alt;

            dbContext.business_org.Add(business_org);

            card_type_business_org card_type_business_org = null;
            List<card_type> card_type_list = dbContext.card_type.Where(ss => ss.business_id == businessOrgAdd.business_id).ToList();
            if ((card_type_list != null) && (card_type_list.Count > 0))
            {
                foreach (var card_type in card_type_list)
                {
                    card_type_business_org = new card_type_business_org();
                    card_type_business_org.card_type_id = card_type.card_type_id;
                    card_type_business_org.business_org = business_org;
                    card_type_business_org.allow_discount = true;
                    card_type_business_org.allow_bonus_for_pay= true;
                    card_type_business_org.allow_bonus_for_card = true;
                    dbContext.card_type_business_org.Add(card_type_business_org);
                }
            }

            dbContext.SaveChanges();

            return new BusinessOrgViewModel(business_org);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessOrgViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            BusinessOrgViewModel businessOrgEdit = (BusinessOrgViewModel)item;
            if (
                (businessOrgEdit == null)
                ||
                (String.IsNullOrEmpty(businessOrgEdit.org_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты отделения");
                return null;
            }

            business_org business_org = dbContext.business_org.Where(ss => ss.org_id == businessOrgEdit.org_id).FirstOrDefault();
            if (business_org == null)
            {
                modelState.AddModelError("", "Не найдена отделение с кодом " + businessOrgEdit.org_id.ToString());
                return null;
            }

            business_org business_org_existing = dbContext.business_org.Where(ss => ss.org_id != businessOrgEdit.org_id && ss.business_id == businessOrgEdit.business_id && ss.org_name.ToLower().Trim().Equals(businessOrgEdit.org_name.ToLower().Trim())).FirstOrDefault();
            if (business_org_existing != null)
            {
                modelState.AddModelError("", "Существует отделение с таким же названием");
                return null;
            }

            modelBeforeChanges = new BusinessOrgViewModel(business_org);

            business_org.org_name = businessOrgEdit.org_name;
            business_org.add_to_timezone = businessOrgEdit.add_to_timezone;
            business_org.card_scan_only = businessOrgEdit.IsCardScanOnly ? 1 : 0;
            business_org.scanner_equals_reader = businessOrgEdit.IsScannerEqualsReader ? (short)1 : (short)0;
            business_org.allow_card_add = businessOrgEdit.allow_card_add;
            business_org.org_name_alt = businessOrgEdit.org_name_alt;

            dbContext.SaveChanges();

            business_org = dbContext.business_org.Where(ss => ss.org_id == businessOrgEdit.org_id).FirstOrDefault();
            return new BusinessOrgViewModel(business_org);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is BusinessOrgViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            long id = ((BusinessOrgViewModel)item).org_id;

            business_org business_org = dbContext.business_org.Where(ss => ss.org_id == id).FirstOrDefault();
            if (business_org == null)
            {
                modelState.AddModelError("serv_result", "Не найдено отделение с кодом " + id.ToString());
                return false;
            }

            List<business_org_user> business_org_user_list = dbContext.business_org_user.Where(ss => ss.org_id == id).ToList();
            if ((business_org_user_list != null) && (business_org_user_list.Count > 0))
            {
                using (var ldapServ = new LdapService())
                {
                    foreach (var user in business_org_user_list)
                    {
                        var res = ldapServ.DeleteUser(business_org.business_id.ToString(), user.user_name);
                        if (!res.Item1)
                        {
                            modelState.AddModelError("serv_result", res.Item2);
                            return false;
                        }

                        dbContext.cab_user_role_action.RemoveRange(dbContext.cab_user_role_action.Where(ss => ss.user_name == user.user_name));
                        dbContext.cab_user_role.RemoveRange(dbContext.cab_user_role.Where(ss => ss.user_name == user.user_name));
                    }
                }

                dbContext.business_org_user.RemoveRange(business_org_user_list);
            }

            dbContext.card_type_business_org.RemoveRange(dbContext.card_type_business_org.Where(ss => ss.business_org_id == business_org.org_id));
            dbContext.campaign_business_org.RemoveRange(dbContext.campaign_business_org.Where(ss => ss.org_id == business_org.org_id));
            dbContext.log_session.RemoveRange(dbContext.log_session.Where(ss => ss.org_id == business_org.org_id));
            dbContext.business_org.Remove(business_org);
            dbContext.SaveChanges();

            return true;
        }

    }
}