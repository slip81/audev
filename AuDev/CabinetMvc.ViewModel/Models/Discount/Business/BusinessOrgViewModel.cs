﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class BusinessOrgViewModel : AuBaseViewModel
    {
        public BusinessOrgViewModel()
            : base(Enums.MainObjectType.BUSINESS_ORG)
        {
            add_to_timezone = 2;
            IsScannerEqualsReader = true;
        }

        public BusinessOrgViewModel(business_org item)
            : base(Enums.MainObjectType.BUSINESS_ORG)
        {
            org_id = item.org_id;
            org_name = item.org_name;
            business_id = item.business_id;            
            add_to_timezone = item.add_to_timezone.HasValue ? item.add_to_timezone + 2 : 2;
            IsCardScanOnly = item.card_scan_only == 1 ? true : false;
            IsScannerEqualsReader = item.scanner_equals_reader == 1 ? true : false;
            allow_card_add = item.allow_card_add;
            org_name_alt = item.org_name_alt;
        }      

        [Key()]
        [ScaffoldColumn(false)]
        public long org_id { get; set; }

        [Display(Name="Название")]
        public string org_name { get; set; }

        [ScaffoldColumn(false)]
        public long business_id { get; set; }

        [Display(Name = "Часовой пояс (МСК +)")]
        [DefaultValue(2)]
        public Nullable<int> add_to_timezone { get; set; }

        [Display(Name = "Название для отчетов")]
        public string org_name_alt { get; set; }

        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public string business_name { get; set; }
        
        [JsonIgnore()]
        [ScaffoldColumn(false)]
        public string org_id_str { get { return org_id.ToString(); } }       

        [Display(Name = "Запрет ручного выбора карт")]
        public bool IsCardScanOnly { get; set; }

        [Display(Name = "Не различать сканер штрих-кодов и картридер при выборе карты")]
        public bool IsScannerEqualsReader { get; set; }

        [Display(Name = "Разрешено добавлять карты через Кассу")]
        public bool allow_card_add { get; set; }

    }
}