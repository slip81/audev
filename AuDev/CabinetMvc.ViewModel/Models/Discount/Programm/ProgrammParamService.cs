﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammParamService : IAuBaseService
    {
        //
    }

    public class ProgrammParamService : AuBaseService, IProgrammParamService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.programm_param.Where(ss => ss.programm_id == parent_id).Select(ss => new ProgrammParamViewModel()
            {
                param_id = ss.param_id,
                programm_id = ss.programm_id,
                param_num = ss.param_num,
                param_type = ss.param_type,
                string_value = ss.string_value,
                int_value = ss.int_value,
                float_value = ss.float_value,
                date_value = ss.date_value,
                param_name = ss.param_name,
                date_only_value = ss.date_only_value,
                is_internal = ss.is_internal,
                for_unit_pos = ss.for_unit_pos,
                group_id = ss.group_id,
                group_internal_num = ss.group_internal_num,
                param_type_id = ss.param_type,
                is_internal_chb = ss.is_internal == 1 ? true : false,
                for_unit_pos_chb = ss.for_unit_pos == 1 ? true : false,
            }
            )
            .OrderBy(ss => ss.param_name);
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var programm_param = dbContext.programm_param.Where(ss => ss.param_id == id).FirstOrDefault();
            return programm_param == null ? null : new ProgrammParamViewModel(programm_param);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ProgrammParamViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ProgrammParamViewModel ppEdit = (ProgrammParamViewModel)item;
            if (
                (ppEdit == null)
                ||
                (String.IsNullOrEmpty(ppEdit.param_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты организации");
                return null;
            }

            programm_param pp = dbContext.programm_param.Where(ss => ss.programm_id == ppEdit.programm_id && ss.param_num == ppEdit.param_num).FirstOrDefault();
            if (pp == null)
            {
                modelState.AddModelError("", "Не найден параметр алгоритма с кодом " + ppEdit.param_id.ToString());
                return null;
            }

            modelBeforeChanges = new ProgrammParamViewModel(pp);

            pp.string_value = ppEdit.string_value;
            pp.float_value = ppEdit.float_value;
            pp.int_value = ppEdit.int_value;
            pp.date_value = ppEdit.date_value;
            pp.date_only_value = ppEdit.date_only_value;
            pp.time_only_value = ppEdit.time_only_value;
            dbContext.SaveChanges();

            return new ProgrammParamViewModel(pp);
        }        
    }
}