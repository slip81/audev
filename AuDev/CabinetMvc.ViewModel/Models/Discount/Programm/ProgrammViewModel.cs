﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammViewModel : AuBaseViewModel
    {
        public ProgrammViewModel()
            : base(Enums.MainObjectType.PROGRAMM)
        {
            date_beg = DateTime.Today;
        }

        public ProgrammViewModel(programm item)
            : base(Enums.MainObjectType.PROGRAMM)
        {            
            programm_id = item.programm_id;
            programm_name = item.programm_name;
            date_beg = item.date_beg;
            date_end = item.date_end;
            descr_short = item.descr_short;
            descr_full = item.descr_full;
            parent_template_id = item.parent_template_id;
            business_id = item.business_id;
            card_type_group_id = item.card_type_group_id;
            programm_params_external = (item.programm_param != null) ? item.programm_param.Where(ss => ss.is_internal == 0).Select(ss => new ProgrammParamViewModel(ss)) : null;
            programm_params_internal = (item.programm_param != null) ? item.programm_param.Where(ss => ss.is_internal == 1).Select(ss => new ProgrammParamViewModel(ss)) : null;            
        }

        public ProgrammViewModel(vw_programm item)
            : base(Enums.MainObjectType.PROGRAMM)
        {
            programm_id = item.programm_id;
            programm_name = item.programm_name;
            date_beg = item.date_beg;
            date_end = item.date_end;
            descr_short = item.descr_short;
            descr_full = item.descr_full;
            parent_template_id = item.parent_template_id;
            business_id = item.business_id;
            card_type_group_id = item.card_type_group_id;
            group_name = item.group_name;
        }

        [Key()]
        [Display(Name = "Код")]
        public long programm_id { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Не задано наименование")]
        public string programm_name { get; set; }

        [Display(Name = "Действует с")]
        [Required(ErrorMessage = "Не задана дата начала")]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "Действует по")]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "Краткое описание")]
        public string descr_short { get; set; }

        [Display(Name = "Организация")]
        public Nullable<long> business_id { get; set; }

        [Display(Name = "Полное описание")]
        public string descr_full { get; set; }

        [Display(Name = "Создан из шаблона")]
        public Nullable<long> parent_template_id { get; set; }

        [Display(Name = "Группа")]
        public Nullable<long> card_type_group_id { get; set; }

        [JsonIgnore()]
        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [JsonIgnore()]
        [Display(Name = "programm_id_str")]
        public string programm_id_str { get {return programm_id.ToString(); } }
        
        [JsonIgnore()]
        [Display(Name = "programm_params_external")]
        public IEnumerable<ProgrammParamViewModel> programm_params_external { get; set; }
        
        [JsonIgnore()]
        [Display(Name = "programm_params_internal")]
        public IEnumerable<ProgrammParamViewModel> programm_params_internal { get; set; }

        [Display(Name = "Организация")]        
        public string business_name { get; set; }

    }
}