﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammParamTypeService : IAuBaseService
    {
        //
    }

    public class ProgrammParamTypeService : AuBaseService, IProgrammParamTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.programm_param_type.ProjectTo<ProgrammParamTypeViewModel>();
        }

    }
}