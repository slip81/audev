﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammStepLogicViewModel : AuBaseViewModel
    {
        public ProgrammStepLogicViewModel()
            : base(Enums.MainObjectType.PROGRAMM_STEP_LOGIC)
        {                                     
            //vw_programm_step_logic        
        }
    

        // db

        [Key()]
        [Display(Name="Код")]
        public long logic_id { get; set; }

        [Display(Name = "Шаг")]
        public long step_id { get; set; }

        [Display(Name = "Алгоритм")]
        public long programm_id { get; set; }

        [Display(Name = "№")]
        public Nullable<int> logic_num { get; set; }

        [Display(Name = "Тип операции")]
        public int op_type { get; set; }

        [Display(Name = "Первый параметр")]
        public Nullable<long> op1_param_id { get; set; }

        [Display(Name = "Второй параметр")]
        public Nullable<long> op2_param_id { get; set; }

        [Display(Name = "Результат при выполненном условии")]
        public Nullable<long> op3_param_id { get; set; }

        [Display(Name = "Результат при невыполненном условии")]
        public Nullable<long> op4_param_id { get; set; }

        [Display(Name = "Условие")]
        public Nullable<long> condition_id { get; set; }

        [Display(Name = "Шаг")]
        public long step_num { get; set; }

        [Display(Name = "Организация")]
        public Nullable<long> business_id { get; set; }

        [Display(Name = "Тип операции")]
        public int logic_type_id { get; set; }

        [Display(Name = "Тип операции")]
        public string logic_type_name { get; set; }

        [Display(Name = "Первый параметр")]
        public string op1_param_name { get; set; }

        [Display(Name = "Второй параметр")]
        public string op2_param_name { get; set; }

        [Display(Name = "Результат при выполненном условии")]
        public string op3_param_name { get; set; }

        [Display(Name = "Результат при невыполненном условии")]
        public string op4_param_name { get; set; }

        [Display(Name = "Условие операции")]
        public string condition_name { get; set; }

        [Display(Name = "Код логики")]
        public string logic_id_str { get; set; }

        [Display(Name = "Код алгоритма")]
        public string programm_id_str { get; set; }

        [Display(Name = "Код шага")]
        public string step_id_str { get; set; }

        [Display(Name = "Условие операции")]
        public string condition_id_str { get; set; }

        [Display(Name = "Первый параметр")]
        public string op1_param_id_str { get; set; }

        [Display(Name = "Второй параметр")]
        public string op2_param_id_str { get; set; }

        [Display(Name = "Результат при выполненном условии")]
        public string op3_param_id_str { get; set; }

        [Display(Name = "Результат при невыполненном условии")]
        public string op4_param_id_str { get; set; }

        [Display(Name = "Код организации")]
        public string business_id_str { get; set; }
    }
}