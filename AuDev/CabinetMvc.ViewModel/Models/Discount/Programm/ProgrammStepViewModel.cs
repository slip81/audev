﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammStepViewModel : AuBaseViewModel
    {
        public ProgrammStepViewModel()
            : base(Enums.MainObjectType.PROGRAMM_STEP)
        {                                     
            //programm_step
        }

        // db

        [Key()]
        [Display(Name="Код")]
        public long step_id { get; set; }

        [Display(Name = "Алгоритм")]
        public long programm_id { get; set; }

        [Display(Name = "№")]
        public int step_num { get; set; }

        [Display(Name = "Краткое описание")]
        public string descr_short { get; set; }

        [Display(Name = "Полное описание")]
        public string descr_full { get; set; }

        [Display(Name = "Выполняется один раз")]
        public short single_use_only { get; set; }

        //additional

        [Display(Name = "Код")]
        public string step_id_str { get { return step_id.ToString(); } }

        [Display(Name = "Выполняется один раз")]
        public bool single_use_only_chb { get; set; }
    }
}