﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammCardParamTypeService : IAuBaseService
    {
        //
    }

    public class ProgrammCardParamTypeService : AuBaseService, IProgrammCardParamTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.programm_card_param_type.ProjectTo<ProgrammCardParamTypeViewModel>().OrderBy(ss => ss.card_param_type_name);
        }

    }
}