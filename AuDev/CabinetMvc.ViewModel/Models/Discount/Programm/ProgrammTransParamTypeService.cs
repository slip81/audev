﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammTransParamTypeService : IAuBaseService
    {
        //
    }

    public class ProgrammTransParamTypeService : AuBaseService, IProgrammTransParamTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.programm_trans_param_type.ProjectTo<ProgrammTransParamTypeViewModel>().OrderBy(ss => ss.trans_param_type_name);
        }

    }
}