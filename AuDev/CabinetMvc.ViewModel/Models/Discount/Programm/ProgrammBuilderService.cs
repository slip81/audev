﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammBuilderService : IAuBaseService
    {        
        bool InsertProgramm(ProgrammViewModel itemAdd, ModelStateDictionary modelState);
        bool UpdateProgramm(ProgrammViewModel itemEdit, ModelStateDictionary modelState);
        bool DeleteProgramm(ProgrammViewModel itemDel, ModelStateDictionary modelState);
        //
        ProgrammParamViewModel GetProgrammParam(long param_id);
        ProgrammParamViewModel InsertProgrammParam(ProgrammParamViewModel itemAdd, ModelStateDictionary modelState);
        ProgrammParamViewModel UpdateProgrammParam(ProgrammParamViewModel itemEdit, ModelStateDictionary modelState);
        bool DeleteProgrammParam(ProgrammParamViewModel itemDel, ModelStateDictionary modelState);
        //
        IQueryable<ProgrammStepViewModel> GetProgrammStepList(long programm_id);
        ProgrammStepViewModel GetProgrammStep(long step_id);
        ProgrammStepViewModel InsertProgrammStep(ProgrammStepViewModel itemAdd, ModelStateDictionary modelState);
        ProgrammStepViewModel UpdateProgrammStep(ProgrammStepViewModel itemEdit, ModelStateDictionary modelState);
        bool DeleteProgrammStep(ProgrammStepViewModel itemDel, ModelStateDictionary modelState);
        //
        IQueryable<ProgrammStepParamViewModel> GetProgrammStepParamList(long step_id);
        ProgrammStepParamViewModel GetProgrammStepParam(long param_id);
        ProgrammStepParamViewModel InsertProgrammStepParam(ProgrammStepParamViewModel itemAdd, ModelStateDictionary modelState);
        ProgrammStepParamViewModel UpdateProgrammStepParam(ProgrammStepParamViewModel itemEdit, ModelStateDictionary modelState);
        bool DeleteProgrammStepParam(ProgrammStepParamViewModel itemDel, ModelStateDictionary modelState);
        //
        IQueryable<ProgrammStepConditionViewModel> GetProgrammStepConditionList(long step_id);
        ProgrammStepConditionViewModel GetProgrammStepCondition(long condition_id);
        ProgrammStepConditionViewModel InsertProgrammStepCondition(ProgrammStepConditionViewModel itemAdd, ModelStateDictionary modelState);
        ProgrammStepConditionViewModel UpdateProgrammStepCondition(ProgrammStepConditionViewModel itemEdit, ModelStateDictionary modelState);
        bool DeleteProgrammStepCondition(ProgrammStepConditionViewModel itemDel, ModelStateDictionary modelState);
        //
        IQueryable<ProgrammStepLogicViewModel> GetProgrammStepLogicList(long step_id);
        ProgrammStepLogicViewModel GetProgrammStepLogic(long logic_id);
        ProgrammStepLogicViewModel InsertProgrammStepLogic(ProgrammStepLogicViewModel itemAdd, ModelStateDictionary modelState);
        ProgrammStepLogicViewModel UpdateProgrammStepLogic(ProgrammStepLogicViewModel itemEdit, ModelStateDictionary modelState);
        bool DeleteProgrammStepLogic(ProgrammStepLogicViewModel itemDel, ModelStateDictionary modelState);
    }

    public class ProgrammBuilderService : AuBaseService, IProgrammBuilderService
    {
        private IProgrammParamService programmParamService
        {
            get
            {
                if (_programmParamService == null)
                {
                    _programmParamService = DependencyResolver.Current.GetService<IProgrammParamService>();
                }
                return _programmParamService;
            }
        }
        private IProgrammParamService _programmParamService;

        #region Programm

        public bool InsertProgramm(ProgrammViewModel itemAdd, ModelStateDictionary modelState)
        {            
            try
            {
                if ((itemAdd == null)
                    || (itemAdd.card_type_group_id.GetValueOrDefault(0) <= 0)
                    || (String.IsNullOrWhiteSpace(itemAdd.programm_name))
                    || (!itemAdd.date_beg.HasValue)
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты алгоритма");
                    return false;
                }

                programm programm = new programm();
                programm.programm_name = itemAdd.programm_name;
                programm.date_beg = itemAdd.date_beg;
                programm.date_end = itemAdd.date_end;
                programm.descr_short = itemAdd.descr_short;
                programm.descr_full = itemAdd.descr_full;
                programm.card_type_group_id = itemAdd.card_type_group_id;
                dbContext.programm.Add(programm);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        public bool UpdateProgramm(ProgrammViewModel itemEdit, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemEdit == null)
                    || (itemEdit.card_type_group_id.GetValueOrDefault(0) <= 0)
                    || (String.IsNullOrWhiteSpace(itemEdit.programm_name))
                    || (!itemEdit.date_beg.HasValue)
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты алгоритма");
                    return false;
                }

                programm programm = dbContext.programm.Where(ss => ss.programm_id == itemEdit.programm_id).FirstOrDefault();
                if (programm == null)
                {
                    modelState.AddModelError("", "Не найден алгоритм с кодом " + itemEdit.programm_id.ToString());
                    return false;
                }

                programm.programm_name = itemEdit.programm_name;
                programm.date_beg = itemEdit.date_beg;
                programm.date_end = itemEdit.date_end;
                programm.descr_short = itemEdit.descr_short;
                programm.descr_full = itemEdit.descr_full;
                programm.card_type_group_id = itemEdit.card_type_group_id;

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        public bool DeleteProgramm(ProgrammViewModel itemDel, ModelStateDictionary modelState)
        {
            try
            {
                if (itemDel == null)
                {
                    modelState.AddModelError("", "Не задан алгоритм");
                    return false;
                }

                programm programm = dbContext.programm.Where(ss => ss.programm_id == itemDel.programm_id).FirstOrDefault();
                if (programm == null)
                {
                    modelState.AddModelError("", "Не задан алгоритм");
                    return false;
                }

                var existing1 = dbContext.card_type_programm_rel.Where(ss => ss.programm_id == itemDel.programm_id).FirstOrDefault();
                if (existing1 != null)
                {
                    modelState.AddModelError("", "Существует тип карт с этим алгоритмом");
                    return false;
                }

                dbContext.programm_step_logic.RemoveRange(dbContext.programm_step_logic.Where(ss => ss.programm_id == itemDel.programm_id));
                dbContext.programm_step_condition.RemoveRange(dbContext.programm_step_condition.Where(ss => ss.programm_id == itemDel.programm_id));
                dbContext.programm_step_param.RemoveRange(dbContext.programm_step_param.Where(ss => ss.programm_id == itemDel.programm_id));
                dbContext.programm_step.RemoveRange(dbContext.programm_step.Where(ss => ss.programm_id == itemDel.programm_id));
                dbContext.programm.Remove(programm);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region ProgrammParam

        public ProgrammParamViewModel GetProgrammParam(long param_id)
        {
            return (ProgrammParamViewModel)programmParamService.GetItem(param_id);
        }

        public ProgrammParamViewModel InsertProgrammParam(ProgrammParamViewModel itemAdd, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemAdd == null)
                    || (itemAdd.param_num.GetValueOrDefault(0) <= 0)
                    || (itemAdd.param_type_id <= 0)
                    || (String.IsNullOrWhiteSpace(itemAdd.param_name))
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты параметра алгоритма");
                    return null;
                }

                programm_param programm_param_existing = dbContext.programm_param.Where(ss => ss.programm_id == itemAdd.programm_id && ss.param_id != itemAdd.param_id && ss.param_num == itemAdd.param_num).FirstOrDefault();
                if (programm_param_existing != null)
                {
                    modelState.AddModelError("", "Уже есть параметр алгоритма с таким номером");
                    return null;
                }

                Enums.ProgrammParamType programmParamTypeEnum = Enums.ProgrammParamType.STRING_VAL;
                if (!Enum.TryParse<Enums.ProgrammParamType>(itemAdd.param_type_id.ToString(), out programmParamTypeEnum))
                {
                    modelState.AddModelError("", "Неподдерживаемый тип параметра: " + itemAdd.param_type_id.ToString());
                    return null;
                }

                bool isValueOk = false;
                switch (programmParamTypeEnum)
                {
                    case Enums.ProgrammParamType.STRING_VAL:
                        isValueOk = !String.IsNullOrWhiteSpace(itemAdd.string_value);
                        break;
                    case Enums.ProgrammParamType.INT_VAL:
                        isValueOk = itemAdd.int_value.HasValue;
                        break;
                    case Enums.ProgrammParamType.FLOAT_VAL:
                        isValueOk = itemAdd.float_value.HasValue;
                        break;
                    case Enums.ProgrammParamType.DATE_VAL:
                        isValueOk = itemAdd.date_value.HasValue;
                        break;
                    case Enums.ProgrammParamType.DATE_ONLY_VAL:
                        isValueOk = itemAdd.date_only_value.HasValue;
                        break;
                    case Enums.ProgrammParamType.TIME_ONLY_VAL:
                        isValueOk = itemAdd.time_only_value.HasValue;
                        break;
                    default:
                        isValueOk = false;
                        break;
                }
                if (!isValueOk)
                {
                    modelState.AddModelError("", "Не задано значение параметра");
                    return null;
                }

                programm_param programm_param = new programm_param();
                programm_param.programm_id = itemAdd.programm_id;
                programm_param.param_name = itemAdd.param_name;
                programm_param.param_num = itemAdd.param_num;
                programm_param.param_type = itemAdd.param_type_id;
                programm_param.is_internal = itemAdd.is_internal_chb ? (short)1 : (short)0;
                programm_param.for_unit_pos = itemAdd.for_unit_pos_chb ? (short)1 : (short)0;
                switch (programmParamTypeEnum)
                {
                    case Enums.ProgrammParamType.STRING_VAL:
                        programm_param.string_value = itemAdd.string_value;
                        break;
                    case Enums.ProgrammParamType.INT_VAL:
                        programm_param.int_value = itemAdd.int_value;
                        break;
                    case Enums.ProgrammParamType.FLOAT_VAL:
                        programm_param.float_value = itemAdd.float_value;
                        break;
                    case Enums.ProgrammParamType.DATE_VAL:
                        programm_param.date_value = itemAdd.date_value;
                        break;
                    case Enums.ProgrammParamType.DATE_ONLY_VAL:
                        programm_param.date_only_value = itemAdd.date_only_value;
                        break;
                    case Enums.ProgrammParamType.TIME_ONLY_VAL:
                        programm_param.time_only_value = itemAdd.time_only_value;
                        break;
                    default:
                        break;
                }
                // !!!
                //programm_param.group_id = ...
                //programm_param.group_internal_num = ...

                dbContext.programm_param.Add(programm_param);
                dbContext.SaveChanges();

                return GetProgrammParam(programm_param.param_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public ProgrammParamViewModel UpdateProgrammParam(ProgrammParamViewModel itemEdit, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemEdit == null)
                    || (itemEdit.param_num.GetValueOrDefault(0) <= 0)
                    || (itemEdit.param_type_id <= 0)
                    || (String.IsNullOrWhiteSpace(itemEdit.param_name))
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты параметра алгоритма");
                    return null;
                }

                programm_param programm_param_existing = dbContext.programm_param.Where(ss => ss.programm_id == itemEdit.programm_id && ss.param_id != itemEdit.param_id && ss.param_num == itemEdit.param_num).FirstOrDefault();
                if (programm_param_existing != null)
                {
                    modelState.AddModelError("", "Уже есть параметр алгоритма с таким номером");
                    return null;
                }

                Enums.ProgrammParamType programmParamTypeEnum = Enums.ProgrammParamType.STRING_VAL;
                if (!Enum.TryParse<Enums.ProgrammParamType>(itemEdit.param_type_id.ToString(), out programmParamTypeEnum))
                {
                    modelState.AddModelError("", "Неподдерживаемый тип параметра: " + itemEdit.param_type_id.ToString());
                    return null;
                }

                bool isValueOk = false;
                switch (programmParamTypeEnum)
                {
                    case Enums.ProgrammParamType.STRING_VAL:
                        isValueOk = !String.IsNullOrWhiteSpace(itemEdit.string_value);
                        break;
                    case Enums.ProgrammParamType.INT_VAL:
                        isValueOk = itemEdit.int_value.HasValue;
                        break;
                    case Enums.ProgrammParamType.FLOAT_VAL:
                        isValueOk = itemEdit.float_value.HasValue;
                        break;
                    case Enums.ProgrammParamType.DATE_VAL:
                        isValueOk = itemEdit.date_value.HasValue;
                        break;
                    case Enums.ProgrammParamType.DATE_ONLY_VAL:
                        isValueOk = itemEdit.date_only_value.HasValue;
                        break;
                    case Enums.ProgrammParamType.TIME_ONLY_VAL:
                        isValueOk = itemEdit.time_only_value.HasValue;
                        break;
                    default:
                        isValueOk = false;
                        break;
                }
                if (!isValueOk)
                {
                    modelState.AddModelError("", "Не задано значение параметра");
                    return null;
                }

                programm_param programm_param = dbContext.programm_param.Where(ss => ss.programm_id == itemEdit.programm_id && ss.param_id == itemEdit.param_id).FirstOrDefault();
                if (programm_param == null)
                {
                    modelState.AddModelError("", "Не найден параметр алгоритма с номером " + itemEdit.param_num.ToString());
                    return null;
                }
                programm_param.param_name = itemEdit.param_name;
                programm_param.param_num = itemEdit.param_num;
                programm_param.param_type = itemEdit.param_type_id;
                programm_param.is_internal = itemEdit.is_internal_chb ? (short)1 : (short)0;
                programm_param.for_unit_pos = itemEdit.for_unit_pos_chb ? (short)1 : (short)0;
                programm_param.string_value = null;
                programm_param.int_value = null;
                programm_param.float_value = null;
                programm_param.date_value = null;
                programm_param.date_only_value = null;
                programm_param.time_only_value = null;
                switch (programmParamTypeEnum)
                {
                    case Enums.ProgrammParamType.STRING_VAL:
                        programm_param.string_value = itemEdit.string_value;
                        break;
                    case Enums.ProgrammParamType.INT_VAL:
                        programm_param.int_value = itemEdit.int_value;
                        break;
                    case Enums.ProgrammParamType.FLOAT_VAL:
                        programm_param.float_value = itemEdit.float_value;
                        break;
                    case Enums.ProgrammParamType.DATE_VAL:
                        programm_param.date_value = itemEdit.date_value;
                        break;
                    case Enums.ProgrammParamType.DATE_ONLY_VAL:
                        programm_param.date_only_value = itemEdit.date_only_value;
                        break;
                    case Enums.ProgrammParamType.TIME_ONLY_VAL:
                        programm_param.time_only_value = itemEdit.time_only_value;
                        break;
                    default:
                        break;
                }
                // !!!
                //programm_param.group_id = ...
                //programm_param.group_internal_num = ...

                dbContext.SaveChanges();

                return GetProgrammParam(programm_param.param_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public bool DeleteProgrammParam(ProgrammParamViewModel itemDel, ModelStateDictionary modelState)
        {
            try
            {
                if (itemDel == null)
                {
                    modelState.AddModelError("", "Не заданы параметры логики шага");
                    return false;
                }

                programm_param programm_param = dbContext.programm_param.Where(ss => ss.param_id == itemDel.param_id).FirstOrDefault();
                if (programm_param == null)
                {
                    modelState.AddModelError("", "Не найдены параметры условия шага");
                    return false;
                }


                var existing = dbContext.programm_step_param.Where(ss => ss.programm_param_id == itemDel.param_id).FirstOrDefault();
                if (existing != null)
                {
                    modelState.AddModelError("", "Параметр используется как параметр шага");
                    return false;
                }

                dbContext.programm_param.Remove(programm_param);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region ProgrammStep

        public IQueryable<ProgrammStepViewModel> GetProgrammStepList(long programm_id)
        {
            return dbContext.programm_step.Where(ss => ss.programm_id == programm_id).ProjectTo<ProgrammStepViewModel>();
        }

        public ProgrammStepViewModel GetProgrammStep(long step_id)
        {
            return dbContext.programm_step.Where(ss => ss.step_id == step_id).ProjectTo<ProgrammStepViewModel>().FirstOrDefault();
        }

        public ProgrammStepViewModel InsertProgrammStep(ProgrammStepViewModel itemAdd, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemAdd == null)
                    || (itemAdd.step_num <= 0)
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты шага алгоритма");
                    return null;
                }

                programm_step programm_step_existing = dbContext.programm_step.Where(ss => ss.programm_id == itemAdd.programm_id && ss.step_id != itemAdd.step_id && ss.step_num == itemAdd.step_num).FirstOrDefault();
                if (programm_step_existing != null)
                {
                    modelState.AddModelError("", "Уже есть шаг алгоритма с таким номером");
                    return null;
                }

                programm_step programm_step = new programm_step();
                ModelMapper.Map<ProgrammStepViewModel, programm_step>(itemAdd, programm_step);

                dbContext.programm_step.Add(programm_step);
                dbContext.SaveChanges();

                return GetProgrammStep(programm_step.step_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public ProgrammStepViewModel UpdateProgrammStep(ProgrammStepViewModel itemEdit, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemEdit == null)
                    || (itemEdit.step_num <= 0)
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты шага алгоритма");
                    return null;
                }

                programm_step programm_step_existing = dbContext.programm_step.Where(ss => ss.programm_id == itemEdit.programm_id && ss.step_id != itemEdit.step_id && ss.step_num == itemEdit.step_num).FirstOrDefault();
                if (programm_step_existing != null)
                {
                    modelState.AddModelError("", "Уже есть шаг алгоритма с таким номером");
                    return null;
                }

                programm_step programm_step = dbContext.programm_step.Where(ss => ss.programm_id == itemEdit.programm_id && ss.step_id == itemEdit.step_id).FirstOrDefault();
                if (programm_step == null)
                {
                    modelState.AddModelError("", "Не найден шаг алгоритма с номером " + itemEdit.step_num.ToString());
                    return null;
                }
                ModelMapper.Map<ProgrammStepViewModel, programm_step>(itemEdit, programm_step);

                dbContext.SaveChanges();

                return GetProgrammStep(programm_step.step_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public bool DeleteProgrammStep(ProgrammStepViewModel itemDel, ModelStateDictionary modelState)
        {
            try
            {
                if (itemDel == null)
                {
                    modelState.AddModelError("", "Не задан шаг");
                    return false;
                }

                programm_step programm_step = dbContext.programm_step.Where(ss => ss.step_id == itemDel.step_id).FirstOrDefault();
                if (programm_step == null)
                {
                    modelState.AddModelError("", "Не задан шаг");
                    return false;
                }
                
                dbContext.programm_step_logic.RemoveRange(dbContext.programm_step_logic.Where(ss => ss.step_id == itemDel.step_id));
                dbContext.programm_step_condition.RemoveRange(dbContext.programm_step_condition.Where(ss => ss.step_id == itemDel.step_id));
                dbContext.programm_step_param.RemoveRange(dbContext.programm_step_param.Where(ss => ss.step_id == itemDel.step_id));
                dbContext.programm_step.Remove(programm_step);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region ProgrammStepParam

        public IQueryable<ProgrammStepParamViewModel> GetProgrammStepParamList(long step_id)
        {
            return dbContext.vw_programm_step_param.Where(ss => ss.step_id == step_id).ProjectTo<ProgrammStepParamViewModel>();
        }
        
        public ProgrammStepParamViewModel GetProgrammStepParam(long param_id)
        {
            return dbContext.vw_programm_step_param.Where(ss => ss.param_id == param_id).ProjectTo<ProgrammStepParamViewModel>().FirstOrDefault();
        }

        public ProgrammStepParamViewModel InsertProgrammStepParam(ProgrammStepParamViewModel itemAdd, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemAdd == null)
                    || (itemAdd.param_num.GetValueOrDefault(0) <= 0)
                    || (itemAdd.param_type_id.GetValueOrDefault(0) <= 0)
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты параметра шага алгоритма");
                    return null;
                }

                programm_step_param programm_step_param_existing = dbContext.programm_step_param.Where(ss => ss.step_id == itemAdd.step_id && ss.param_id != itemAdd.param_id && ss.param_num == itemAdd.param_num).FirstOrDefault();
                if (programm_step_param_existing != null)
                {
                    modelState.AddModelError("", "Уже есть параметр шага алгоритма с таким номером");
                    return null;
                }

                programm_step_param programm_step_param = new programm_step_param();
                programm_step_param.programm_id = itemAdd.programm_id;
                programm_step_param.step_id = itemAdd.step_id;
                programm_step_param.param_num = itemAdd.param_num;
                programm_step_param.param_name = itemAdd.param_name;
                programm_step_param.is_programm_output = itemAdd.is_programm_output_chb ? (short)1 : (short)0;

                programm_step_param.card_param_type = null;
                programm_step_param.trans_param_type = null;
                programm_step_param.programm_param_id = null;
                programm_step_param.prev_step_param_id = null;

                programm_step_param.param_type_id = itemAdd.param_type_id;
                switch ((Enums.ProgrammStepParamTypeEnum)itemAdd.param_type_id)
                {
                    case Enums.ProgrammStepParamTypeEnum.CARD_PARAM:
                        programm_step_param.card_param_type = itemAdd.card_param_type;
                        break;
                    case Enums.ProgrammStepParamTypeEnum.TRANS_PARAM:
                        programm_step_param.trans_param_type = itemAdd.trans_param_type;
                        break;
                    case Enums.ProgrammStepParamTypeEnum.PROGRAMM_TYPE:
                        programm_step_param.programm_param_id = long.Parse(itemAdd.programm_param_id_str);
                        break;
                    case Enums.ProgrammStepParamTypeEnum.PREV_STEP_PARAM:
                        programm_step_param.prev_step_param_id = long.Parse(itemAdd.prev_step_param_id_str);
                        break;
                    default:
                        modelState.AddModelError("", "Неподдерживаемый тип параметра шага: " + itemAdd.param_type_id.ToString());
                        return null;
                }

                dbContext.programm_step_param.Add(programm_step_param);
                dbContext.SaveChanges();

                return GetProgrammStepParam(programm_step_param.param_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }
        
        public ProgrammStepParamViewModel UpdateProgrammStepParam(ProgrammStepParamViewModel itemEdit, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemEdit == null)
                    || (itemEdit.param_num.GetValueOrDefault(0) <= 0)
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты параметра шага алгоритма");
                    return null;
                }

                programm_step_param programm_step_param_existing = dbContext.programm_step_param.Where(ss => ss.step_id == itemEdit.step_id && ss.param_id != itemEdit.param_id && ss.param_num == itemEdit.param_num).FirstOrDefault();
                if (programm_step_param_existing != null)
                {
                    modelState.AddModelError("", "Уже есть параметр шага алгоритма с таким номером");
                    return null;
                }

                programm_step_param programm_step_param = dbContext.programm_step_param.Where(ss => ss.step_id == itemEdit.step_id && ss.param_id == itemEdit.param_id).FirstOrDefault();
                if (programm_step_param == null)
                {
                    modelState.AddModelError("", "Не найден параметр шага алгоритма с номером " + itemEdit.param_num.ToString());
                    return null;
                }

                programm_step_param.param_num = itemEdit.param_num;
                programm_step_param.param_name = itemEdit.param_name;
                programm_step_param.is_programm_output = itemEdit.is_programm_output_chb ? (short)1 : (short)0;

                programm_step_param.card_param_type = null;
                programm_step_param.trans_param_type = null;
                programm_step_param.programm_param_id = null;
                programm_step_param.prev_step_param_id = null;

                programm_step_param.param_type_id = itemEdit.param_type_id;
                switch ((Enums.ProgrammStepParamTypeEnum)itemEdit.param_type_id)
                {
                    case Enums.ProgrammStepParamTypeEnum.CARD_PARAM:
                        programm_step_param.card_param_type = itemEdit.card_param_type;
                        break;
                    case Enums.ProgrammStepParamTypeEnum.TRANS_PARAM:
                        programm_step_param.trans_param_type = itemEdit.trans_param_type;
                        break;
                    case Enums.ProgrammStepParamTypeEnum.PROGRAMM_TYPE:
                        programm_step_param.programm_param_id = long.Parse(itemEdit.programm_param_id_str);
                        break;
                    case Enums.ProgrammStepParamTypeEnum.PREV_STEP_PARAM:
                        programm_step_param.prev_step_param_id = long.Parse(itemEdit.prev_step_param_id_str);
                        break;
                    default:
                        modelState.AddModelError("", "Неподдерживаемый тип параметра шага: " + itemEdit.param_type_id.ToString());
                        return null;
                }

                dbContext.SaveChanges();

                return GetProgrammStepParam(programm_step_param.param_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }
        
        public bool DeleteProgrammStepParam(ProgrammStepParamViewModel itemDel, ModelStateDictionary modelState)
        {
            try
            {
                if (itemDel == null)
                {
                    modelState.AddModelError("", "Не задан параметр шага");
                    return false;
                }

                programm_step_param programm_step_param = dbContext.programm_step_param.Where(ss => ss.param_id == itemDel.param_id).FirstOrDefault();
                if (programm_step_param == null)
                {
                    modelState.AddModelError("", "Не задан параметр шага");
                    return false;
                }


                var existing1 = dbContext.programm_step_logic
                    .Where(ss => (ss.op1_param_id == itemDel.param_id) || (ss.op2_param_id == itemDel.param_id)
                        || (ss.op3_param_id == itemDel.param_id) || (ss.op4_param_id == itemDel.param_id)
                    )
                    .FirstOrDefault();
                if (existing1 != null)
                {
                    modelState.AddModelError("", "Параметр используется в логике шага");
                    return false;
                }

                var existing2 = dbContext.programm_step_condition
                    .Where(ss => (ss.op1_param_id == itemDel.param_id) || (ss.op2_param_id == itemDel.param_id)                        
                    )
                    .FirstOrDefault();
                if (existing2 != null)
                {
                    modelState.AddModelError("", "Параметр используется в условии шага");
                    return false;
                }

                dbContext.programm_step_param.Remove(programm_step_param);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region ProgrammStepCondition

        public IQueryable<ProgrammStepConditionViewModel> GetProgrammStepConditionList(long step_id)
        {
            return dbContext.vw_programm_step_condition.Where(ss => ss.step_id == step_id).ProjectTo<ProgrammStepConditionViewModel>();
        }

        public ProgrammStepConditionViewModel GetProgrammStepCondition(long condition_id)
        {
            return dbContext.vw_programm_step_condition.Where(ss => ss.condition_id == condition_id).ProjectTo<ProgrammStepConditionViewModel>().FirstOrDefault();
        }

        public ProgrammStepConditionViewModel InsertProgrammStepCondition(ProgrammStepConditionViewModel itemAdd, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemAdd == null)
                    || (itemAdd.condition_num.GetValueOrDefault(0) <= 0)
                    || (itemAdd.condition_type_id.GetValueOrDefault(0) <= 0)
                    || (String.IsNullOrWhiteSpace(itemAdd.op1_param_id_str))
                    || (String.IsNullOrWhiteSpace(itemAdd.op2_param_id_str))
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты условия шага алгоритма");
                    return null;
                }

                programm_step_condition programm_step_condition_existing = dbContext.programm_step_condition.Where(ss => ss.step_id == itemAdd.step_id && ss.condition_id != itemAdd.condition_id && ss.condition_num == itemAdd.condition_num).FirstOrDefault();
                if (programm_step_condition_existing != null)
                {
                    modelState.AddModelError("", "Уже есть условие шага алгоритма с таким номером");
                    return null;
                }

                programm_step_condition programm_step_condition = new programm_step_condition();
                programm_step_condition.programm_id = itemAdd.programm_id;
                programm_step_condition.step_id = itemAdd.step_id;
                programm_step_condition.condition_num = itemAdd.condition_num;
                programm_step_condition.condition_name = itemAdd.condition_name;
                programm_step_condition.op_type = itemAdd.condition_type_id;
                programm_step_condition.op1_param_id = long.Parse(itemAdd.op1_param_id_str);
                programm_step_condition.op2_param_id = long.Parse(itemAdd.op2_param_id_str);

                dbContext.programm_step_condition.Add(programm_step_condition);
                dbContext.SaveChanges();

                return GetProgrammStepCondition(programm_step_condition.condition_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public ProgrammStepConditionViewModel UpdateProgrammStepCondition(ProgrammStepConditionViewModel itemEdit, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemEdit == null)
                    || (itemEdit.condition_num.GetValueOrDefault(0) <= 0)
                    || (itemEdit.condition_type_id.GetValueOrDefault(0) <= 0)
                    || (String.IsNullOrWhiteSpace(itemEdit.op1_param_id_str))
                    || (String.IsNullOrWhiteSpace(itemEdit.op2_param_id_str))
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты условия шага алгоритма");
                    return null;
                }

                programm_step_condition programm_step_condition_existing = dbContext.programm_step_condition.Where(ss => ss.step_id == itemEdit.step_id && ss.condition_id != itemEdit.condition_id && ss.condition_num == itemEdit.condition_num).FirstOrDefault();
                if (programm_step_condition_existing != null)
                {
                    modelState.AddModelError("", "Уже есть условие шага алгоритма с таким номером");
                    return null;
                }

                programm_step_condition programm_step_condition = dbContext.programm_step_condition.Where(ss => ss.step_id == itemEdit.step_id && ss.condition_id == itemEdit.condition_id).FirstOrDefault();
                if (programm_step_condition == null)
                {
                    modelState.AddModelError("", "Не найдено условие шага алгоритма с номером " + itemEdit.condition_num.ToString());
                    return null;
                }

                programm_step_condition.condition_num = itemEdit.condition_num;
                programm_step_condition.condition_name = itemEdit.condition_name;
                programm_step_condition.op_type = itemEdit.condition_type_id;
                programm_step_condition.op1_param_id = long.Parse(itemEdit.op1_param_id_str);
                programm_step_condition.op2_param_id = long.Parse(itemEdit.op2_param_id_str);
                
                dbContext.SaveChanges();

                return GetProgrammStepCondition(programm_step_condition.condition_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public bool DeleteProgrammStepCondition(ProgrammStepConditionViewModel itemDel, ModelStateDictionary modelState)
        {
            try
            {
                if (itemDel == null)
                {
                    modelState.AddModelError("", "Не заданы параметры логики шага");
                    return false;
                }

                programm_step_condition programm_step_condition = dbContext.programm_step_condition.Where(ss => ss.condition_id == itemDel.condition_id).FirstOrDefault();
                if (programm_step_condition == null)
                {
                    modelState.AddModelError("", "Не найдены параметры условия шага");
                    return false;
                }


                var existing = dbContext.programm_step_logic.Where(ss => ss.condition_id == itemDel.condition_id).FirstOrDefault();
                if (existing != null)
                {
                    modelState.AddModelError("", "Условие используется в логике шага");
                    return false;
                }

                dbContext.programm_step_condition.Remove(programm_step_condition);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion

        #region ProgrammStepLogic

        public IQueryable<ProgrammStepLogicViewModel> GetProgrammStepLogicList(long step_id)
        {
            return dbContext.vw_programm_step_logic.Where(ss => ss.step_id == step_id).ProjectTo<ProgrammStepLogicViewModel>();
        }

        public ProgrammStepLogicViewModel GetProgrammStepLogic(long logic_id)
        {
            return dbContext.vw_programm_step_logic.Where(ss => ss.logic_id == logic_id).ProjectTo<ProgrammStepLogicViewModel>().FirstOrDefault();
        }

        public ProgrammStepLogicViewModel InsertProgrammStepLogic(ProgrammStepLogicViewModel itemAdd, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemAdd == null)
                    || (itemAdd.logic_num.GetValueOrDefault(0) <= 0)
                    || (itemAdd.logic_type_id <= 0)
                    || (String.IsNullOrWhiteSpace(itemAdd.op1_param_id_str))                    
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты логики шага алгоритма");
                    return null;
                }

                programm_step_logic programm_step_logic_existing = dbContext.programm_step_logic.Where(ss => ss.step_id == itemAdd.step_id && ss.logic_id != itemAdd.logic_id && ss.logic_num == itemAdd.logic_num).FirstOrDefault();
                if (programm_step_logic_existing != null)
                {
                    modelState.AddModelError("", "Уже есть логика шага алгоритма с таким номером");
                    return null;
                }

                programm_step_logic programm_step_logic = new programm_step_logic();
                programm_step_logic.programm_id = itemAdd.programm_id;
                programm_step_logic.step_id = itemAdd.step_id;
                programm_step_logic.logic_num = itemAdd.logic_num;
                programm_step_logic.condition_id = String.IsNullOrWhiteSpace(itemAdd.condition_id_str) ? null : (long?)long.Parse(itemAdd.condition_id_str);
                programm_step_logic.op_type = itemAdd.logic_type_id;
                programm_step_logic.op1_param_id = long.Parse(itemAdd.op1_param_id_str);
                programm_step_logic.op2_param_id = String.IsNullOrWhiteSpace(itemAdd.op2_param_id_str) ? null : (long?)long.Parse(itemAdd.op2_param_id_str);
                programm_step_logic.op3_param_id = String.IsNullOrWhiteSpace(itemAdd.op3_param_id_str) ? null : (long?)long.Parse(itemAdd.op3_param_id_str);
                programm_step_logic.op4_param_id = String.IsNullOrWhiteSpace(itemAdd.op4_param_id_str) ? null : (long?)long.Parse(itemAdd.op4_param_id_str);

                dbContext.programm_step_logic.Add(programm_step_logic);
                dbContext.SaveChanges();

                return GetProgrammStepLogic(programm_step_logic.logic_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public ProgrammStepLogicViewModel UpdateProgrammStepLogic(ProgrammStepLogicViewModel itemEdit, ModelStateDictionary modelState)
        {
            try
            {
                if ((itemEdit == null)
                    || (itemEdit.logic_num.GetValueOrDefault(0) <= 0)
                    || (itemEdit.logic_type_id <= 0)
                    || (String.IsNullOrWhiteSpace(itemEdit.op1_param_id_str))
                    )
                {
                    modelState.AddModelError("", "Не заданы атрибуты логики шага алгоритма");
                    return null;
                }

                programm_step_logic programm_step_logic_existing = dbContext.programm_step_logic.Where(ss => ss.step_id == itemEdit.step_id && ss.logic_id != itemEdit.logic_id && ss.logic_num == itemEdit.logic_num).FirstOrDefault();
                if (programm_step_logic_existing != null)
                {
                    modelState.AddModelError("", "Уже есть логика шага алгоритма с таким номером");
                    return null;
                }

                programm_step_logic programm_step_logic = dbContext.programm_step_logic.Where(ss => ss.step_id == itemEdit.step_id && ss.logic_id == itemEdit.logic_id).FirstOrDefault();
                if (programm_step_logic == null)
                {
                    modelState.AddModelError("", "Не найдена логика шага алгоритма с номером " + itemEdit.logic_num.ToString());
                    return null;
                }
                programm_step_logic.logic_num = itemEdit.logic_num;
                programm_step_logic.condition_id = String.IsNullOrWhiteSpace(itemEdit.condition_id_str) ? null : (long?)long.Parse(itemEdit.condition_id_str);
                programm_step_logic.op_type = itemEdit.logic_type_id;
                programm_step_logic.op1_param_id = long.Parse(itemEdit.op1_param_id_str);
                programm_step_logic.op2_param_id = String.IsNullOrWhiteSpace(itemEdit.op2_param_id_str) ? null : (long?)long.Parse(itemEdit.op2_param_id_str);
                programm_step_logic.op3_param_id = String.IsNullOrWhiteSpace(itemEdit.op3_param_id_str) ? null : (long?)long.Parse(itemEdit.op3_param_id_str);
                programm_step_logic.op4_param_id = String.IsNullOrWhiteSpace(itemEdit.op4_param_id_str) ? null : (long?)long.Parse(itemEdit.op4_param_id_str);

                dbContext.SaveChanges();

                return GetProgrammStepLogic(programm_step_logic.logic_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        public bool DeleteProgrammStepLogic(ProgrammStepLogicViewModel itemDel, ModelStateDictionary modelState)
        {
            try
            {

                if (itemDel == null)
                {
                    modelState.AddModelError("", "Не заданы параметры логики шага");
                    return false;
                }

                programm_step_logic programm_step_logic = dbContext.programm_step_logic.Where(ss => ss.logic_id == itemDel.logic_id).FirstOrDefault();
                if (programm_step_logic == null)
                {
                    modelState.AddModelError("", "Не найдены параметры логики шага");
                    return false;
                }

                dbContext.programm_step_logic.Remove(programm_step_logic);
                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        #endregion
    }
}