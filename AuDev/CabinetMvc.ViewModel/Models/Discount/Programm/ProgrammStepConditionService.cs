﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammStepConditionService : IAuBaseService
    {
        IQueryable<ProgrammStepConditionViewModel> GetList_forStep(long logic_id);
    }

    public class ProgrammStepConditionService : AuBaseService, IProgrammStepConditionService
    {
        public IQueryable<ProgrammStepConditionViewModel> GetList_forStep(long step_id)
        {
            return dbContext.vw_programm_step_condition.Where(ss => ss.step_id == step_id)
                .ProjectTo<ProgrammStepConditionViewModel>()
                .OrderBy(ss => ss.condition_name);
        }
    }
}