﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammOrderService : IAuBaseService
    {
        //    
    }

    public class ProgrammOrderService : AuBaseService, IProgrammOrderService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            if (!currUser.IsAdmin)
            {
                return xGetList_byParent(currUser.BusinessId);
            }

            return dbContext.vw_programm_order                
                .ProjectTo<ProgrammOrderViewModel>();
            
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_programm_order.Where(ss => ss.business_id == parent_id)
                .ProjectTo<ProgrammOrderViewModel>();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ProgrammOrderViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ProgrammOrderViewModel itemAdd = (ProgrammOrderViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }
            
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            //itemAdd.business_id = currUser.BusinessId;
            itemAdd.crt_user = currUser.UserName;
            itemAdd.crt_date = now;
            
            programm_order programm_order = new programm_order();           
            ModelMapper.Map<ProgrammOrderViewModel, programm_order>(itemAdd, programm_order);

            bool compare_discount_params = true;
            bool compare_bonus_params = true;
            bool needSendEmail = false;
            List<Enums.ProgrammParamGroupEnum> programmParamGroupList = new List<Enums.ProgrammParamGroupEnum>();
            List<SelectListItem> proc_step_disc_deser = null;
            List<SelectListItem> proc_step_zhv_disc_deser = null;
            List<SelectListItem> proc_step_bonus_for_card_deser = null;
            List<SelectListItem> proc_step_zhv_bonus_for_card_deser = null;
            List<SelectListItem> curr_step_list_deser = null;
            IQueryable<programm_descr> programm_descr_query = dbContext.programm_descr;
            List<programm_descr> programm_descr_list = null;
            programm_descr programm_descr = null;
            List<programm_param> paramListSource = null;
            List<programm_param> paramListTarget = null;
            if (!programm_order.mess_contains_ext_info)
            {
                programm_descr_query = programm_descr_query.Where(ss => ss.card_type_group_id == itemAdd.card_type_group_id);
                //
                compare_discount_params = (itemAdd.card_type_group_id == (long)Enums.CardTypeGroup.DISCOUNT) || (itemAdd.card_type_group_id == (long)Enums.CardTypeGroup.DISCOUNT_BONUS);
                compare_bonus_params = (itemAdd.card_type_group_id == (long)Enums.CardTypeGroup.BONUS) || (itemAdd.card_type_group_id == (long)Enums.CardTypeGroup.DISCOUNT_BONUS);
                //
                if (compare_discount_params)
                {
                    if (itemAdd.trans_sum_min_disc.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.trans_sum_min_disc);
                    }
                    //
                    if (itemAdd.proc_const_from_card_disc)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.proc_const_from_card_disc);
                    }
                    else if (itemAdd.proc_const_disc.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.proc_const_disc);
                        programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.CONST_PROC_DISC);
                    }
                    //                    
                    if (!String.IsNullOrEmpty(itemAdd.proc_step_disc))
                    {
                        proc_step_disc_deser = JsonConvert.DeserializeObject<List<SelectListItem>>(itemAdd.proc_step_disc);
                        if ((proc_step_disc_deser != null) && (proc_step_disc_deser.Count > 0))
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.proc_step_count_disc == proc_step_disc_deser.Count);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.BORDER_PROC_DISC);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.BORDER_SUMM_DISC);
                        }
                    }
                    //
                    programm_descr_query = programm_descr_query.Where(ss => ss.sum_step_card_disc == itemAdd.sum_step_card_disc);
                    programm_descr_query = programm_descr_query.Where(ss => ss.sum_step_trans_disc == itemAdd.sum_step_trans_disc);
                    //
                    if (itemAdd.price_margin_percent_lte_forbid_disc.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.price_margin_percent_lte_forbid_disc);
                        programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MIN_PROC_PRICE_MARGIN_DISC);
                    }
                    //
                    if (itemAdd.price_margin_percent_gte_forbid_disc.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.price_margin_percent_gte_forbid_disc);
                        programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MAX_PROC_PRICE_MARGIN_DISC);
                    }
                    //
                    if (itemAdd.use_explicit_max_percent_disc.HasValue && itemAdd.use_explicit_max_percent_disc >= 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.use_explicit_max_percent_disc);
                        programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MAX_PROC_DISC);
                    }
                    else
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.use_max_percent_disc == itemAdd.use_max_percent_disc);
                    }
                    //
                    programm_descr_query = programm_descr_query.Where(ss => ss.same_for_zhv_disc == itemAdd.same_for_zhv_disc);
                    //
                    if (!itemAdd.same_for_zhv_disc)
                    {
                        if (itemAdd.proc_const_from_card_zhv_disc)
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.proc_const_from_card_zhv_disc);
                        }
                        else if (itemAdd.proc_const_zhv_disc.GetValueOrDefault(0) > 0)
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.proc_const_zhv_disc);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.CONST_PROC_ZHV_DISC);
                        }
                        if (!String.IsNullOrEmpty(itemAdd.proc_step_zhv_disc))
                        {
                            proc_step_zhv_disc_deser = JsonConvert.DeserializeObject<List<SelectListItem>>(itemAdd.proc_step_zhv_disc);
                            if ((proc_step_zhv_disc_deser != null) && (proc_step_zhv_disc_deser.Count > 0))
                            {
                                programm_descr_query = programm_descr_query.Where(ss => ss.proc_step_count_zhv_disc == proc_step_zhv_disc_deser.Count);
                                programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.BORDER_PROC_ZHV_DISC);
                                programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.BORDER_SUMM_ZHV_DISC);
                            }
                        }
                        programm_descr_query = programm_descr_query.Where(ss => ss.sum_step_card_zhv_disc == itemAdd.sum_step_card_zhv_disc);
                        programm_descr_query = programm_descr_query.Where(ss => ss.sum_step_trans_zhv_disc == itemAdd.sum_step_trans_zhv_disc);
                        //
                        if (itemAdd.price_margin_percent_lte_forbid_zhv_disc.GetValueOrDefault(0) > 0)
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.price_margin_percent_lte_forbid_zhv_disc);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MIN_PROC_PRICE_MARGIN_ZHV_DISC);
                        }
                        //
                        if (itemAdd.price_margin_percent_gte_forbid_zhv_disc.GetValueOrDefault(0) > 0)
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.price_margin_percent_gte_forbid_zhv_disc);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MAX_PROC_PRICE_MARGIN_ZHV_DISC);
                        }
                        //
                        if (itemAdd.use_explicit_max_percent_zhv_disc.HasValue && itemAdd.use_explicit_max_percent_zhv_disc >= 0)
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.use_explicit_max_percent_zhv_disc);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MAX_PROC_ZHV_DISC);
                        }
                        else
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.use_max_percent_zhv_disc == itemAdd.use_max_percent_zhv_disc);
                        }
                    }
                    //
                    if (itemAdd.allow_order_disc.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.allow_order_disc == itemAdd.allow_order_disc);
                    }
                }
                //
                if (compare_bonus_params)
                {
                    if (itemAdd.trans_sum_min_bonus_for_card.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.trans_sum_min_bonus_for_card);
                    }
                    if (itemAdd.trans_sum_min_bonus_for_pay.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.trans_sum_min_bonus_for_pay);
                    }
                    //
                    if (itemAdd.trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay);
                        programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MAX_SALE_PROC_FOR_PAY_BONUS);
                    }
                    else if (itemAdd.trans_sum_max_percent_bonus_for_pay.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.trans_sum_max_percent_bonus_for_pay);
                        programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MAX_SALE_PROC_FOR_PAY_BONUS);
                    }
                    else if (itemAdd.trans_sum_max_bonus_for_pay.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.trans_sum_max_bonus_for_pay);
                        programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.MAX_SALE_SUMM_FOR_PAY_BONUS);
                    }
                    //                    
                    if (itemAdd.proc_const_from_card_bonus_for_card)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.proc_const_from_card_bonus_for_card);
                    }
                    else if (itemAdd.proc_const_bonus_for_card.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.proc_const_bonus_for_card);
                        programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.CONST_PROC_BONUS);
                    }
                    //
                    if (!String.IsNullOrEmpty(itemAdd.proc_step_bonus_for_card))
                    {
                        proc_step_bonus_for_card_deser = JsonConvert.DeserializeObject<List<SelectListItem>>(itemAdd.proc_step_bonus_for_card);
                        if ((proc_step_bonus_for_card_deser != null) && (proc_step_bonus_for_card_deser.Count > 0))
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.proc_step_count_bonus_for_card == proc_step_bonus_for_card_deser.Count);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.BORDER_SUMM_BONUS);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.BORDER_PROC_BONUS);
                        }
                    }
                    //
                    programm_descr_query = programm_descr_query.Where(ss => ss.sum_step_card_bonus_for_card == itemAdd.sum_step_card_bonus_for_card);
                    programm_descr_query = programm_descr_query.Where(ss => ss.sum_step_trans_bonus_for_card == itemAdd.sum_step_trans_bonus_for_card);
                    //
                    programm_descr_query = programm_descr_query.Where(ss => ss.allow_zero_disc_bonus_for_card == itemAdd.allow_zero_disc_bonus_for_card);
                    programm_descr_query = programm_descr_query.Where(ss => ss.allow_zero_disc_bonus_for_pay == itemAdd.allow_zero_disc_bonus_for_pay);
                    //
                    programm_descr_query = programm_descr_query.Where(ss => ss.same_for_zhv_bonus == itemAdd.same_for_zhv_bonus);
                    //
                    if (!itemAdd.same_for_zhv_bonus)
                    {
                        if (itemAdd.proc_const_from_card_zhv_bonus_for_card)
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.proc_const_from_card_zhv_bonus_for_card);
                        }
                        else if (itemAdd.proc_const_zhv_bonus_for_card.GetValueOrDefault(0) > 0)
                        {
                            programm_descr_query = programm_descr_query.Where(ss => ss.proc_const_zhv_bonus_for_card);
                            programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.CONST_PROC_ZHV_BONUS);
                        }
                        if (!String.IsNullOrEmpty(itemAdd.proc_step_zhv_bonus_for_card))
                        {
                            proc_step_zhv_bonus_for_card_deser = JsonConvert.DeserializeObject<List<SelectListItem>>(itemAdd.proc_step_zhv_bonus_for_card);
                            if ((proc_step_zhv_bonus_for_card_deser != null) && (proc_step_zhv_bonus_for_card_deser.Count > 0))
                            {
                                programm_descr_query = programm_descr_query.Where(ss => ss.proc_step_count_zhv_bonus_for_card == proc_step_zhv_bonus_for_card_deser.Count);
                                programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.BORDER_SUMM_ZHV_BONUS);
                                programmParamGroupList.Add(Enums.ProgrammParamGroupEnum.BORDER_PROC_ZHV_BONUS);
                            }
                        }
                        programm_descr_query = programm_descr_query.Where(ss => ss.sum_step_card_zhv_bonus_for_card == itemAdd.sum_step_card_zhv_bonus_for_card);
                        programm_descr_query = programm_descr_query.Where(ss => ss.sum_step_trans_zhv_bonus_for_card == itemAdd.sum_step_trans_zhv_bonus_for_card);              
                        programm_descr_query = programm_descr_query.Where(ss => ss.allow_zero_disc_zhv_bonus_for_card == itemAdd.allow_zero_disc_zhv_bonus_for_card);
                        programm_descr_query = programm_descr_query.Where(ss => ss.allow_zero_disc_zhv_bonus_for_pay == itemAdd.allow_zero_disc_zhv_bonus_for_pay);
                    }
                    //
                    if (itemAdd.allow_order_bonus_for_card.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.allow_order_bonus_for_card == itemAdd.allow_order_bonus_for_card);
                    }
                    if (itemAdd.allow_order_bonus_for_pay.GetValueOrDefault(0) > 0)
                    {
                        programm_descr_query = programm_descr_query.Where(ss => ss.allow_order_bonus_for_pay == itemAdd.allow_order_bonus_for_pay);
                    }
                }

                //
                programm_descr_query = programm_descr_query.Where(ss => ss.add_to_card_trans_sum == itemAdd.add_to_card_trans_sum);
                programm_descr_query = programm_descr_query.Where(ss => ss.add_to_card_trans_sum_with_disc == itemAdd.add_to_card_trans_sum_with_disc);
                programm_descr_query = programm_descr_query.Where(ss => ss.add_to_card_trans_sum_with_bonus_for_pay == itemAdd.add_to_card_trans_sum_with_bonus_for_pay);
                //
                programm_descr_list = programm_descr_query.OrderByDescending(ss => ss.programm_id).ToList();
                if ((programm_descr_list != null) && (programm_descr_list.Count > 0))
                {
                    programm_descr = programm_descr_list.FirstOrDefault();

                    // !!!
                    // 1.скопировать найденный алгоритм на эту организацию
                    // 2.отвязать от типа карт существующий алгоритм (если есть)
                    // 3.привязать к типу карт найденный алгоритм
                    // 4.указать ссылку на него в programm_order

                    var currentProgrammService = DependencyResolver.Current.GetService<IProgrammService>();
                    var new_programm_id = currentProgrammService.CopyProgramm2(programm_descr.programm_id, (long)itemAdd.business_id, null, itemAdd.descr, itemAdd.descr, modelState);
                    if (new_programm_id < 0)
                    {
                        return null;
                    }
                    
                    if (programmParamGroupList.Count > 0)
                    {
                        // !!!
                        // ...

                        programmParamGroupList = programmParamGroupList.Distinct().ToList();
                        foreach (var programmParamGroup in programmParamGroupList)
                        {
                            switch (programmParamGroup) 
                            { 
                                case Enums.ProgrammParamGroupEnum.BORDER_SUMM_DISC:
                                case Enums.ProgrammParamGroupEnum.BORDER_PROC_DISC:
                                case Enums.ProgrammParamGroupEnum.BORDER_SUMM_ZHV_DISC:
                                case Enums.ProgrammParamGroupEnum.BORDER_PROC_ZHV_DISC:
                                //
                                case Enums.ProgrammParamGroupEnum.BORDER_SUMM_BONUS:
                                case Enums.ProgrammParamGroupEnum.BORDER_PROC_BONUS:
                                case Enums.ProgrammParamGroupEnum.BORDER_SUMM_ZHV_BONUS:
                                case Enums.ProgrammParamGroupEnum.BORDER_PROC_ZHV_BONUS:
                                    paramListSource = dbContext.programm_param.Where(ss => ss.programm_id == programm_descr.programm_id && ss.group_id == (int)programmParamGroup).ToList();
                                    paramListTarget = dbContext.programm_param.Where(ss => ss.programm_id == new_programm_id && ss.group_id == (int)programmParamGroup).ToList();
                                    if ((paramListSource != null) && (paramListSource.Count > 0) && (paramListTarget != null) && (paramListTarget.Count > 0))
                                    {
                                        foreach (var paramSource in paramListSource)
                                        {
                                            var paramTarget = paramListTarget.Where(ss => ss.group_id == paramSource.group_id && ss.group_internal_num == paramSource.group_internal_num).FirstOrDefault();
                                            if (paramTarget != null)
                                            {
                                                switch (programmParamGroup)
                                                {
                                                    case Enums.ProgrammParamGroupEnum.BORDER_SUMM_DISC:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_PROC_DISC:
                                                        curr_step_list_deser = proc_step_disc_deser.OrderBy(ss => decimal.Parse(ss.Text)).ToList();
                                                        break;
                                                    case Enums.ProgrammParamGroupEnum.BORDER_SUMM_ZHV_DISC:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_PROC_ZHV_DISC:
                                                        curr_step_list_deser = proc_step_zhv_disc_deser.OrderBy(ss => decimal.Parse(ss.Text)).ToList();
                                                        break;
                                                    case Enums.ProgrammParamGroupEnum.BORDER_SUMM_BONUS:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_PROC_BONUS:
                                                        curr_step_list_deser = proc_step_bonus_for_card_deser.OrderBy(ss => decimal.Parse(ss.Text)).ToList();
                                                        break;
                                                    case Enums.ProgrammParamGroupEnum.BORDER_SUMM_ZHV_BONUS:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_PROC_ZHV_BONUS:
                                                        curr_step_list_deser = proc_step_zhv_bonus_for_card_deser.OrderBy(ss => decimal.Parse(ss.Text)).ToList();
                                                        break;
                                                    default:
                                                        break;
                                                }                                                                                                                                                

                                                switch (programmParamGroup)
                                                {
                                                    case Enums.ProgrammParamGroupEnum.BORDER_SUMM_DISC:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_SUMM_ZHV_DISC:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_SUMM_BONUS:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_SUMM_ZHV_BONUS:
                                                        switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                        {
                                                            case Enums.ProgrammParamType.INT_VAL:
                                                                paramTarget.int_value = int.Parse(curr_step_list_deser[(int)paramTarget.group_internal_num - 1].Text);
                                                                break;
                                                            case Enums.ProgrammParamType.FLOAT_VAL:
                                                                paramTarget.float_value = decimal.Parse(curr_step_list_deser[(int)paramTarget.group_internal_num - 1].Text);
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                        paramTarget.param_name = "Порог " + paramTarget.group_internal_num.ToString() + ", сумма";
                                                        break;
                                                    case Enums.ProgrammParamGroupEnum.BORDER_PROC_DISC:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_PROC_ZHV_DISC:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_PROC_BONUS:
                                                    case Enums.ProgrammParamGroupEnum.BORDER_PROC_ZHV_BONUS:
                                                        switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                        {
                                                            case Enums.ProgrammParamType.INT_VAL:
                                                                paramTarget.int_value = int.Parse(curr_step_list_deser[(int)paramTarget.group_internal_num - 1].Value);
                                                                break;
                                                            case Enums.ProgrammParamType.FLOAT_VAL:
                                                                paramTarget.float_value = decimal.Parse(curr_step_list_deser[(int)paramTarget.group_internal_num - 1].Value);
                                                                break;
                                                            default:
                                                                break;
                                                        }
                                                        paramTarget.param_name = "Порог " + paramTarget.group_internal_num.ToString() + ", процент";
                                                        break;
                                                    default:                                                        
                                                        break;
                                                }                                                
                                            }
                                        }
                                    }
                                    break;
                                case Enums.ProgrammParamGroupEnum.CONST_PROC_DISC:
                                case Enums.ProgrammParamGroupEnum.CONST_PROC_ZHV_DISC:
                                case Enums.ProgrammParamGroupEnum.CONST_PROC_BONUS:
                                case Enums.ProgrammParamGroupEnum.CONST_PROC_ZHV_BONUS:
                                //
                                case Enums.ProgrammParamGroupEnum.MAX_PROC_DISC:
                                case Enums.ProgrammParamGroupEnum.MAX_PROC_ZHV_DISC:
                                case Enums.ProgrammParamGroupEnum.MAX_PROC_PRICE_MARGIN_DISC:
                                case Enums.ProgrammParamGroupEnum.MAX_PROC_PRICE_MARGIN_ZHV_DISC:
                                case Enums.ProgrammParamGroupEnum.MIN_PROC_PRICE_MARGIN_DISC:
                                case Enums.ProgrammParamGroupEnum.MIN_PROC_PRICE_MARGIN_ZHV_DISC:
                                //
                                case Enums.ProgrammParamGroupEnum.MAX_PROC_BONUS:
                                case Enums.ProgrammParamGroupEnum.MAX_PROC_ZHV_BONUS:
                                case Enums.ProgrammParamGroupEnum.MAX_SALE_SUMM_FOR_PAY_BONUS:
                                case Enums.ProgrammParamGroupEnum.MAX_SALE_SUMM_FOR_PAY_ZHV_BONUS:
                                case Enums.ProgrammParamGroupEnum.MAX_SALE_PROC_FOR_PAY_BONUS:
                                case Enums.ProgrammParamGroupEnum.MAX_SALE_PROC_FOR_PAY_ZHV_BONUS:                                
                                    paramListSource = dbContext.programm_param.Where(ss => ss.programm_id == programm_descr.programm_id && ss.group_id == (int)programmParamGroup).ToList();
                                    paramListTarget = dbContext.programm_param.Where(ss => ss.programm_id == new_programm_id && ss.group_id == (int)programmParamGroup).ToList();
                                    if ((paramListSource != null) && (paramListSource.Count > 0) && (paramListTarget != null) && (paramListTarget.Count > 0))
                                    {
                                        var paramSource = paramListSource.FirstOrDefault();
                                        var paramTarget = paramListTarget.FirstOrDefault();

                                        switch (programmParamGroup)
                                        {
                                            case Enums.ProgrammParamGroupEnum.CONST_PROC_DISC:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.proc_const_disc.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.proc_const_disc.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                }                                     
                                                paramTarget.param_name = "Постоянный процент скидки";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.CONST_PROC_ZHV_DISC:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.proc_const_zhv_disc.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.proc_const_zhv_disc.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                } 
                                                paramTarget.param_name = "Постоянный процент скидки";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.CONST_PROC_BONUS:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.proc_const_bonus_for_card.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.proc_const_bonus_for_card.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                paramTarget.param_name = "Постоянный бонусный процент";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.CONST_PROC_ZHV_BONUS:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.proc_const_zhv_bonus_for_card.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.proc_const_zhv_bonus_for_card.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                paramTarget.param_name = "Постоянный бонусный процент";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_PROC_DISC:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.use_explicit_max_percent_disc.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.use_explicit_max_percent_disc.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                }                                      
                                                paramTarget.param_name = "Максимальный процент скидки";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_PROC_ZHV_DISC:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.use_explicit_max_percent_zhv_disc.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.use_explicit_max_percent_zhv_disc.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                paramTarget.param_name = "Максимальный процент скидки";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_PROC_BONUS:
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_PROC_ZHV_BONUS:                                                
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_PROC_PRICE_MARGIN_DISC:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.price_margin_percent_gte_forbid_disc.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.price_margin_percent_gte_forbid_disc.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                }  
                                                paramTarget.param_name = "Максимальный % розн. наценки для скидки";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_PROC_PRICE_MARGIN_ZHV_DISC:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.price_margin_percent_gte_forbid_zhv_disc.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.price_margin_percent_gte_forbid_zhv_disc.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                } 
                                                paramTarget.param_name = "Максимальный % розн. наценки для скидки";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MIN_PROC_PRICE_MARGIN_DISC:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.price_margin_percent_lte_forbid_disc.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.price_margin_percent_lte_forbid_disc.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                } 
                                                paramTarget.param_name = "Минимальный % розн. наценки для скидки";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MIN_PROC_PRICE_MARGIN_ZHV_DISC:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.price_margin_percent_lte_forbid_zhv_disc.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.price_margin_percent_lte_forbid_zhv_disc.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                } 
                                                paramTarget.param_name = "Минимальный % розн. наценки для скидки";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_SALE_SUMM_FOR_PAY_BONUS:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.trans_sum_max_bonus_for_pay.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.trans_sum_max_bonus_for_pay.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                } 
                                                paramTarget.param_name = "Максимальная сумма покупки для оплаты бонусами";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_SALE_SUMM_FOR_PAY_ZHV_BONUS:
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_SALE_PROC_FOR_PAY_BONUS:
                                                switch ((Enums.ProgrammParamType)paramTarget.param_type)
                                                {
                                                    case Enums.ProgrammParamType.INT_VAL:
                                                        paramTarget.int_value = (int)Math.Truncate(itemAdd.trans_sum_max_percent_bonus_for_pay.GetValueOrDefault(0));
                                                        break;
                                                    case Enums.ProgrammParamType.FLOAT_VAL:
                                                        paramTarget.float_value = itemAdd.trans_sum_max_percent_bonus_for_pay.GetValueOrDefault(0);
                                                        break;
                                                    default:
                                                        break;
                                                } 
                                                paramTarget.param_name = "Максимальный процент покупки для оплаты бонусами";
                                                break;
                                            case Enums.ProgrammParamGroupEnum.MAX_SALE_PROC_FOR_PAY_ZHV_BONUS:                                                
                                                break;
                                            default:                                                
                                                break;
                                        }                                          
                                    }                                    
                                    break;
                                default:
                                    break;
                            }
                        }

                        //dbContext.SaveChanges();
                    }

                    var curr_card_type = dbContext.card_type.Where(ss => ss.card_type_id == itemAdd.card_type_id).FirstOrDefault();
                    if (curr_card_type == null)
                    {
                        modelState.AddModelError("", "Не найден тип карт с кодом " + itemAdd.card_type_id.ToString());
                        return null;
                    }

                    var card_type_programm_rel_list = dbContext.card_type_programm_rel.Where(ss => ss.card_type_id == curr_card_type.card_type_id && !ss.date_end.HasValue).ToList();
                    if ((card_type_programm_rel_list != null) && (card_type_programm_rel_list.Count > 0))
                    {
                        foreach (var rel in card_type_programm_rel_list)
                        {
                            rel.date_end = today;
                        }
                    }

                    card_type_programm_rel card_type_programm_rel = new card_type_programm_rel();
                    card_type_programm_rel.card_type_id = curr_card_type.card_type_id;
                    card_type_programm_rel.programm_id = new_programm_id;
                    card_type_programm_rel.date_beg = today;
                    card_type_programm_rel.date_end = null;
                    dbContext.card_type_programm_rel.Add(card_type_programm_rel);

                    programm_order.done_date = now;
                    programm_order.done_user = currUser.UserName;
                    programm_order.programm_id = new_programm_id;
                }
                else
                {
                    //GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", "pavlov.ms.81@gmail.com", "Новая заявка на алгоритм в ЛК [" + programm_order.order_id + "]", programm_order.descr, null);
                    needSendEmail = true;
                }

                dbContext.programm_order.Add(programm_order);
            }
            else
            {
                dbContext.programm_order.Add(programm_order);
            }

            dbContext.SaveChanges();

            if (needSendEmail)
                GlobalUtil.SendMail("smtp.gmail.com", "cab.aptekaural@gmail.com", "aptekaural", "pavlov.ms.81@gmail.com", "Новая заявка на алгоритм в ЛК [" + programm_order.order_id + "]", programm_order.descr, null);
            
            ProgrammOrderViewModel res = new ProgrammOrderViewModel();
            ModelMapper.Map<programm_order, ProgrammOrderViewModel>(programm_order, res);

            return res;
        }
    }
}