﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammService : IAuBaseService
    {
        AuBaseViewModel CopyProgramm(AuBaseViewModel item, long business_id, ModelStateDictionary modelState);
        long CopyProgramm2(long source_programm_id, long target_business_id, string programm_name, string descr_short, string descr_full, ModelStateDictionary modelState);
        IQueryable<ProgrammViewModel> GetList_Active(long? business_id);
    }

    public class ProgrammService : AuBaseService, IProgrammService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_programm.Where(ss => ss.business_id == parent_id)
                .OrderBy(ss => ss.programm_name)
                .Select(ss => new ProgrammViewModel()
            {
                programm_id = ss.programm_id,
                programm_name = ss.programm_name,
                date_beg = ss.date_beg,
                date_end = ss.date_end,
                descr_full = ss.descr_full,
                descr_short = ss.descr_short,
                parent_template_id = ss.parent_template_id,
                business_id = ss.business_id,
                card_type_group_id = ss.card_type_group_id,
                group_name = ss.group_name,
            }
            );
        }

        public IQueryable<ProgrammViewModel> GetList_Active(long? business_id)
        {
            var query = dbContext.programm.Where(ss => !ss.date_end.HasValue);
            if (business_id.GetValueOrDefault(0) > 0)
                query = query.Where(ss => ss.business_id == business_id);
            else
                // кроме алгоритмов Аурит и Монолит
                query = query.Where(ss => ss.business_id != 1007661065565111774 && ss.business_id != 1191989625543984144);

            return from t1 in query
                   from t2 in dbContext.business
                   where t1.business_id == t2.business_id
                   select new ProgrammViewModel()
                   {
                       programm_id = t1.programm_id,
                       programm_name = t1.programm_name,
                       descr_short = t1.descr_short,
                       business_name = t2.business_name,
                   };            

            /*
            return query
                .OrderBy(ss => ss.programm_name)
                .Select(ss => new ProgrammViewModel()
                {
                    programm_id = ss.programm_id,
                    programm_name = ss.programm_name,
                    descr_short = ss.descr_short,
                }
            */        
        }


        protected override AuBaseViewModel xGetItem(long id)
        {
            var programm = dbContext.programm.Where(ss => ss.programm_id == id).FirstOrDefault();
            return programm == null ? null : new ProgrammViewModel(programm);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ProgrammViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ProgrammViewModel programmEdit = (ProgrammViewModel)item;
            if (
                (programmEdit == null)
                ||
                (String.IsNullOrEmpty(programmEdit.programm_name))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты алгоритма");
                return null;
            }

            long business_id = currUser.BusinessId;
            programmEdit.business_id = business_id;

            programm programm = dbContext.programm.Where(ss => ss.programm_id == programmEdit.programm_id).FirstOrDefault();

            if (programm == null)
            {
                modelState.AddModelError("", "Не найден алгоритм с кодом " + programmEdit.programm_id.ToString());
                return null;
            }
            modelBeforeChanges = new ProgrammViewModel(programm);

            long? card_type_group_id_old = programm.card_type_group_id;
            long? card_type_group_id_new = programmEdit.card_type_group_id;
            if ((card_type_group_id_old.HasValue) && (card_type_group_id_old != card_type_group_id_new))
            {
                var rel_existing = dbContext.card_type_programm_rel.Where(ss => ss.programm_id == programm.programm_id).FirstOrDefault();
                if (rel_existing != null)
                {
                    modelState.AddModelError("", "Нельзя сменить группу алгоритма - он привязан к типу карт");
                    return null;
                }
            }

            programm.card_type_group_id = programmEdit.card_type_group_id;
            programm.programm_name = programmEdit.programm_name;
            programm.date_beg = programmEdit.date_beg;
            programm.date_end = programmEdit.date_end;
            programm.descr_short = programmEdit.descr_short;
            programm.descr_full = programmEdit.descr_full;

            dbContext.SaveChanges();

            return new ProgrammViewModel(programm);
        }        

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is ProgrammViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            long id = ((ProgrammViewModel)item).programm_id;

            long business_id = currUser.BusinessId;

            programm programm = dbContext.programm.Where(ss => ss.programm_id == id).FirstOrDefault();
            if (programm == null)
            {
                modelState.AddModelError("serv_result", "Алгоритм не найден");
                return false;
            }

            long rel_id = dbContext.card_type_programm_rel.Where(ss => ss.programm_id == id).Select(ss => ss.rel_id).FirstOrDefault();
            if (rel_id > 0)
            {
                modelState.AddModelError("serv_result", "Существуют типы карт с данным алгоритмом");
                return false;
            }

            long template_id = dbContext.programm_template.Where(ss => ss.programm_id == id).Select(ss => ss.template_id).FirstOrDefault();
            if (template_id > 0)
            {
                modelState.AddModelError("serv_result", "Существуют шаблоны, созданные на основании данного алгоритма");
                return false;
            }

            long trans_cnt = (from pr in dbContext.programm_result
                              from ct in dbContext.card_transaction
                              where pr.result_id == ct.programm_result_id
                              && pr.programm_id == id
                              select ct).Count();
            if (trans_cnt > 0)
            {
                modelState.AddModelError("serv_result", "Существуют продажи по данному алгоритму");
                return false;
            }

            // удаляем все тестовые расчеты (без транзакций)
            var pr_step_result = from pr in dbContext.programm_result
                                 from psr in dbContext.programm_step_result
                                 where pr.result_id == psr.result_id
                                 && pr.programm_id == id
                                 select psr;
            dbContext.programm_step_result.RemoveRange(pr_step_result);

            var pr_result_detail = from pr in dbContext.programm_result
                                   from prd in dbContext.programm_result_detail
                                   where pr.result_id == prd.result_id
                                   && pr.programm_id == id
                                   select prd;
            dbContext.programm_result_detail.RemoveRange(pr_result_detail);

            var pr_result = dbContext.programm_result.Where(ss => ss.programm_id == id);
            dbContext.programm_result.RemoveRange(pr_result);

            List<programm_step_logic> programm_step_logic_for_del = dbContext.programm_step_logic.Where(ss => ss.programm_id == id).ToList();
            if (programm_step_logic_for_del != null)
            {
                dbContext.programm_step_logic.RemoveRange(programm_step_logic_for_del);
            }

            List<programm_step_condition> programm_step_condition_for_del = dbContext.programm_step_condition.Where(ss => ss.programm_id == id).ToList();
            if (programm_step_logic_for_del != null)
            {
                dbContext.programm_step_condition.RemoveRange(programm_step_condition_for_del);
            }

            List<programm_step_param> programm_step_param_for_del = dbContext.programm_step_param.Where(ss => ss.programm_id == id).ToList();
            if (programm_step_param_for_del != null)
            {
                dbContext.programm_step_param.RemoveRange(programm_step_param_for_del);
            }

            List<programm_step> programm_step_for_del = dbContext.programm_step.Where(ss => ss.programm_id == id).ToList();
            if (programm_step_param_for_del != null)
            {
                dbContext.programm_step.RemoveRange(programm_step_for_del);
            }

            List<programm_param> programm_param_for_del = dbContext.programm_param.Where(ss => ss.programm_id == id).ToList();
            if (programm_step_param_for_del != null)
            {
                dbContext.programm_param.RemoveRange(programm_param_for_del);
            }

            dbContext.programm.Remove(programm);

            dbContext.SaveChanges();

            return true;
        }

        public AuBaseViewModel CopyProgramm(AuBaseViewModel item, long business_id, ModelStateDictionary modelState)
        {
            if (!(item is ProgrammViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            ProgrammViewModel programmAdd = (ProgrammViewModel)item;
            if (programmAdd == null)               
            {
                modelState.AddModelError("", "Не заданы атрибуты алгоритма");
                return null;
            }            

            //long business_id = currUser.BusinessId;
            long parent_programm_id = programmAdd.programm_id;
            DateTime now = DateTime.Now;

            var new_programm_id = CopyProgramm2(programmAdd.programm_id, business_id, programmAdd.programm_name + " (копия от " + now.ToString() + ")", null, null, modelState);
            if ((new_programm_id <= 0) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при копировании алгоритма");
                return null;
            }

            var new_programm = dbContext.programm.Where(ss => ss.programm_id == new_programm_id).FirstOrDefault();
            if (new_programm == null)
            {
                modelState.AddModelError("", "Не найден скопированный алгоритм с кодом " + new_programm_id.ToString());                    
                return null;
            }

            ProgrammViewModel res = new ProgrammViewModel(new_programm);
            ToLog("Добавление", (long)Enums.LogEventType.CABINET, new Adm.LogObject(res));
            return res;

            #region old
            /*
            programmAdd.business_id = business_id;

            programm programm = new programm();
            programm.business_id = programmAdd.business_id;
            programm.date_beg = programmAdd.date_beg;
            programm.date_end = programmAdd.date_end;
            programm.descr_full = programmAdd.descr_full;
            programm.descr_short = programmAdd.descr_short;
            programm.parent_template_id = null;
            programm.programm_name = programmAdd.programm_name + " (копия от " + DateTime.Now.ToString() + ")";
            programm.card_type_group_id = programmAdd.card_type_group_id;

            dbContext.programm.Add(programm);
            dbContext.SaveChanges();

            var res = xCopyProgramm(programm, parent_programm_id);
            
            ToLog("Добавление", (long)Enums.LogEventType.CABINET, new Adm.LogObject(res != null ? res : item));
            
            return res;
            */
            #endregion
        }

        public long CopyProgramm2(long source_programm_id, long target_business_id, string programm_name, string descr_short, string descr_full, ModelStateDictionary modelState)
        {
            long new_programm_id = -1;            
            try
            {
                new_programm_id = dbContext.Database.SqlQuery<long>("select discount.copy_programm(" + source_programm_id.ToString() + ", " + target_business_id.ToString() + ");").FirstOrDefault();
                if ((new_programm_id <= 0) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при копировании алгоритма");
                    return -1;
                }

                var new_programm = dbContext.programm.Where(ss => ss.programm_id == new_programm_id).FirstOrDefault();
                if (new_programm == null)
                {
                    modelState.AddModelError("", "Не найден скопированный алгоритм с кодом " + new_programm_id.ToString());
                    return -1;
                }

                bool needSaveChanges = false;
                if (!String.IsNullOrEmpty(descr_short))
                {
                    new_programm.descr_short = descr_short;
                    needSaveChanges = true;
                }
                if (!String.IsNullOrEmpty(descr_full))
                {
                    new_programm.descr_full = descr_full;
                    needSaveChanges = true;
                }
                if (!String.IsNullOrEmpty(programm_name))
                {
                    new_programm.programm_name = programm_name;
                    needSaveChanges = true;
                }

                if (needSaveChanges)
                {
                    dbContext.SaveChanges();
                }

                return new_programm_id;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return -1;
            }
        }

        #region old
        /*
        private ProgrammViewModel xCopyProgramm(programm programm, long parent_programm_id)
        {
            List<programm_param> programm_param_list = null;
            List<programm_step> programm_step_list = null;
            List<programm_step_param> programm_step_param_list = null;
            List<programm_step_condition> programm_step_condition_list = null;
            List<programm_step_logic> programm_step_logic_list = null;

            List<programm_param> programm_param_list_new = null;
            List<programm_step> programm_step_list_new = null;
            List<programm_step_param> programm_step_param_list_new = null;
            List<programm_step_condition> programm_step_condition_list_new = null;
            List<programm_step_logic> programm_step_logic_list_new = null;

            programm_param_list = dbContext.programm_param.Where(ss => ss.programm_id == parent_programm_id).ToList();
            programm_step_list = dbContext.programm_step.Where(ss => ss.programm_id == parent_programm_id).ToList();
            programm_step_param_list = dbContext.programm_step_param.Where(ss => ss.programm_id == parent_programm_id).ToList();
            programm_step_condition_list = dbContext.programm_step_condition.Where(ss => ss.programm_id == parent_programm_id).ToList();
            programm_step_logic_list = dbContext.programm_step_logic.Where(ss => ss.programm_id == parent_programm_id).ToList();

            if ((programm_param_list != null) && (programm_param_list.Count > 0))
            {
                programm_param_list_new = new List<programm_param>();
                foreach (var item_existing in programm_param_list)
                {
                    programm_param item_new = new programm_param();
                    item_new.date_only_value = item_existing.date_only_value;
                    item_new.date_value = item_existing.date_value;
                    item_new.float_value = item_existing.float_value;
                    item_new.for_unit_pos = item_existing.for_unit_pos;
                    item_new.int_value = item_existing.int_value;
                    item_new.is_internal = item_existing.is_internal;
                    item_new.param_name = item_existing.param_name;
                    item_new.param_num = item_existing.param_num;
                    item_new.param_type = item_existing.param_type;
                    item_new.string_value = item_existing.string_value;
                    item_new.time_only_value = item_existing.time_only_value;
                    item_new.programm_id = programm.programm_id;
                    programm_param_list_new.Add(item_new);
                }

                dbContext.programm_param.AddRange(programm_param_list_new);
                //dbContext.SaveChanges();
            }

            if ((programm_step_list != null) && (programm_step_list.Count > 0))
            {
                programm_step_list_new = new List<programm_step>();
                foreach (var item_existing in programm_step_list)
                {
                    programm_step item_new = new programm_step();
                    item_new.descr_full = item_existing.descr_full;
                    item_new.descr_short = item_existing.descr_short;
                    item_new.single_use_only = item_existing.single_use_only;
                    item_new.step_num = item_existing.step_num;
                    item_new.programm_id = programm.programm_id;
                    programm_step_list_new.Add(item_new);
                }

                dbContext.programm_step.AddRange(programm_step_list_new);
            }

            dbContext.SaveChanges();

            // refresh list
            programm_param_list_new = dbContext.programm_param.Where(ss => ss.programm_id == programm.programm_id).ToList();
            programm_step_list_new = dbContext.programm_step.Where(ss => ss.programm_id == programm.programm_id).ToList();

            if ((programm_step_param_list != null) && (programm_step_param_list.Count > 0))
            {
                programm_step_param_list_new = new List<programm_step_param>();
                foreach (var item_existing in programm_step_param_list)
                {
                    programm_step_param item_new = new programm_step_param();
                    item_new.card_param_type = item_existing.card_param_type;
                    item_new.is_programm_output = item_existing.is_programm_output;
                    item_new.param_name = item_existing.param_name;
                    item_new.param_num = item_existing.param_num;
                    item_new.trans_param_type = item_existing.trans_param_type;

                    if (item_existing.programm_param_id.HasValue)
                    {
                        var item1 = programm_param_list.Where(ss => ss.param_id == item_existing.programm_param_id).FirstOrDefault();
                        item_new.programm_param_id = programm_param_list_new.Where(ss => ss.param_num == item1.param_num).Select(ss => ss.param_id).FirstOrDefault();
                    }
                    else
                    {
                        item_new.programm_param_id = null;
                    }

                    var item2 = programm_step_list.Where(ss => ss.step_id == item_existing.step_id).FirstOrDefault();
                    item_new.step_id = programm_step_list_new.Where(ss => ss.step_num == item2.step_num).Select(ss => ss.step_id).FirstOrDefault();

                    //item_new.prev_step_param_id = null;
                    item_new.prev_step_param_id = item_existing.prev_step_param_id;

                    item_new.programm_id = programm.programm_id;
                    programm_step_param_list_new.Add(item_new);
                }

                dbContext.programm_step_param.AddRange(programm_step_param_list_new);
                dbContext.SaveChanges();

                // refresh list
                programm_step_param_list_new = dbContext.programm_step_param.Where(ss => ss.programm_id == programm.programm_id).ToList();

            }

            if ((programm_step_condition_list != null) && (programm_step_condition_list.Count > 0))
            {
                programm_step_condition_list_new = new List<programm_step_condition>();
                foreach (var item_existing in programm_step_condition_list)
                {
                    programm_step_condition item_new = new programm_step_condition();
                    item_new.condition_name = item_existing.condition_name;
                    item_new.condition_num = item_existing.condition_num;
                    item_new.op_type = item_existing.op_type;

                    if (item_existing.op1_param_id.HasValue)
                    {
                        var item1 = programm_step_param_list.Where(ss => ss.param_id == item_existing.op1_param_id).FirstOrDefault();
                        item_new.op1_param_id = programm_step_param_list_new.Where(ss => ss.param_num == item1.param_num).Select(ss => ss.param_id).FirstOrDefault();
                    }
                    else
                    {
                        item_new.op1_param_id = null;
                    }

                    if (item_existing.op2_param_id.HasValue)
                    {
                        var item1 = programm_step_param_list.Where(ss => ss.param_id == item_existing.op2_param_id).FirstOrDefault();
                        item_new.op2_param_id = programm_step_param_list_new.Where(ss => ss.param_num == item1.param_num).Select(ss => ss.param_id).FirstOrDefault();
                    }
                    else
                    {
                        item_new.op2_param_id = null;
                    }

                    var item2 = programm_step_list.Where(ss => ss.step_id == item_existing.step_id).FirstOrDefault();
                    item_new.step_id = programm_step_list_new.Where(ss => ss.step_num == item2.step_num).Select(ss => ss.step_id).FirstOrDefault();

                    item_new.programm_id = programm.programm_id;
                    programm_step_condition_list_new.Add(item_new);
                }

                dbContext.programm_step_condition.AddRange(programm_step_condition_list_new);
                dbContext.SaveChanges();

                // refresh list
                programm_step_condition_list_new = dbContext.programm_step_condition.Where(ss => ss.programm_id == programm.programm_id).ToList();
            }

            if ((programm_step_logic_list != null) && (programm_step_logic_list.Count > 0))
            {
                programm_step_logic_list_new = new List<programm_step_logic>();
                foreach (var item_existing in programm_step_logic_list)
                {
                    programm_step_logic item_new = new programm_step_logic();

                    item_new.logic_num = item_existing.logic_num;
                    item_new.op_type = item_existing.op_type;

                    if (item_existing.condition_id.HasValue)
                    {
                        var item1 = programm_step_condition_list.Where(ss => ss.condition_id == item_existing.condition_id).FirstOrDefault();
                        item_new.condition_id = programm_step_condition_list_new.Where(ss => ss.condition_num == item1.condition_num).Select(ss => ss.condition_id).FirstOrDefault();
                    }
                    else
                    {
                        item_new.condition_id = null;
                    }

                    var item2 = programm_step_list.Where(ss => ss.step_id == item_existing.step_id).FirstOrDefault();
                    item_new.step_id = programm_step_list_new.Where(ss => ss.step_num == item2.step_num).Select(ss => ss.step_id).FirstOrDefault();

                    if (item_existing.op1_param_id.HasValue)
                    {
                        var item3 = programm_step_param_list.Where(ss => ss.param_id == item_existing.op1_param_id).FirstOrDefault();
                        item_new.op1_param_id = programm_step_param_list_new.Where(ss => ss.param_num == item3.param_num).Select(ss => ss.param_id).FirstOrDefault();
                    }
                    else
                    {
                        item_new.op1_param_id = null;
                    }

                    if (item_existing.op2_param_id.HasValue)
                    {
                        var item3 = programm_step_param_list.Where(ss => ss.param_id == item_existing.op2_param_id).FirstOrDefault();
                        item_new.op2_param_id = programm_step_param_list_new.Where(ss => ss.param_num == item3.param_num).Select(ss => ss.param_id).FirstOrDefault();
                    }
                    else
                    {
                        item_new.op2_param_id = null;
                    }

                    if (item_existing.op3_param_id.HasValue)
                    {
                        var item3 = programm_step_param_list.Where(ss => ss.param_id == item_existing.op3_param_id).FirstOrDefault();
                        item_new.op3_param_id = programm_step_param_list_new.Where(ss => ss.param_num == item3.param_num).Select(ss => ss.param_id).FirstOrDefault();
                    }
                    else
                    {
                        item_new.op3_param_id = null;
                    }

                    if (item_existing.op4_param_id.HasValue)
                    {
                        var item3 = programm_step_param_list.Where(ss => ss.param_id == item_existing.op4_param_id).FirstOrDefault();
                        item_new.op4_param_id = programm_step_param_list_new.Where(ss => ss.param_num == item3.param_num).Select(ss => ss.param_id).FirstOrDefault();
                    }
                    else
                    {
                        item_new.op4_param_id = null;
                    }

                    item_new.programm_id = programm.programm_id;
                    programm_step_logic_list_new.Add(item_new);
                }

                dbContext.programm_step_logic.AddRange(programm_step_logic_list_new);
                dbContext.SaveChanges();

                // refresh list
                programm_step_logic_list_new = dbContext.programm_step_logic.Where(ss => ss.programm_id == programm.programm_id).ToList();
            }

            //dbContext.SaveChanges();

            // второй проход - обновляем ссылки
            foreach (var item_new in programm_step_param_list_new)
            {
                if (item_new.prev_step_param_id.HasValue)
                {
                    int? prev_step_param_num = programm_step_param_list.Where(ss => ss.param_id == item_new.prev_step_param_id).Select(ss => ss.param_num).FirstOrDefault();

                    int prev_step_step_num = (from p1 in programm_step_param_list
                                              from p2 in programm_step_list
                                              where p1.step_id == p2.step_id
                                              && p1.param_id == item_new.prev_step_param_id
                                              select p2.step_num
                                            ).FirstOrDefault();

                    long prev_step_param_id = (from p1 in programm_step_param_list_new
                                               from p2 in programm_step_list_new
                                               where p1.step_id == p2.step_id
                                               && p1.param_num == prev_step_param_num
                                               && p2.step_num == prev_step_step_num
                                               select p1.param_id
                                            ).FirstOrDefault();

                    item_new.prev_step_param_id = prev_step_param_id;
                }

                dbContext.SaveChanges();
            }

            return new ProgrammViewModel(programm);
        }
        */
        #endregion
    }
}