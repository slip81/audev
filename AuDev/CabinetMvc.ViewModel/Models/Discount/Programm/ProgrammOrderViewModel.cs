﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammOrderViewModel : AuBaseViewModel
    {
        public ProgrammOrderViewModel()
            : base(Enums.MainObjectType.PROGRAMM_ORDER)
        {     
            //
            //programm_order
            //vw_programm_order
        }


        [Key()]
        [Display(Name = "Код")]
        public int order_id { get; set; }
        
        [Display(Name = "Организация")]
        public Nullable<long> business_id { get; set; }

        [Display(Name = "mess")]
        public string mess { get; set; }

        [Display(Name = "descr")]
        public string descr { get; set; }

        [Display(Name = "crt_user")]
        public string crt_user { get; set; }

        [Display(Name = "crt_date")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "done_user")]
        public string done_user { get; set; }

        [Display(Name = "done_date")]
        public Nullable<System.DateTime> done_date { get; set; }

        [Display(Name = "programm_id")]
        public Nullable<long> programm_id { get; set; }

        [Display(Name = "card_type_group_id")]
        public Nullable<long> card_type_group_id { get; set; }

        [Display(Name = "date_beg")]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "trans_sum_min_disc")]
        public Nullable<decimal> trans_sum_min_disc { get; set; }

        [Display(Name = "trans_sum_max_disc")]
        public Nullable<decimal> trans_sum_max_disc { get; set; }

        [Display(Name = "trans_sum_min_bonus_for_card")]
        public Nullable<decimal> trans_sum_min_bonus_for_card { get; set; }

        [Display(Name = "trans_sum_max_bonus_for_card")]
        public Nullable<decimal> trans_sum_max_bonus_for_card { get; set; }

        [Display(Name = "trans_sum_min_bonus_for_pay")]
        public Nullable<decimal> trans_sum_min_bonus_for_pay { get; set; }

        [Display(Name = "trans_sum_max_bonus_for_pay")]
        public Nullable<decimal> trans_sum_max_bonus_for_pay { get; set; }

        [Display(Name = "proc_const_disc")]
        public Nullable<decimal> proc_const_disc { get; set; }

        [Display(Name = "proc_step_disc")]
        public string proc_step_disc { get; set; }

        [Display(Name = "sum_step_card_disc")]
        public bool sum_step_card_disc { get; set; }

        [Display(Name = "sum_step_trans_disc")]
        public bool sum_step_trans_disc { get; set; }

        [Display(Name = "proc_const_bonus_for_card")]
        public Nullable<decimal> proc_const_bonus_for_card { get; set; }

        [Display(Name = "proc_step_bonus_for_card")]
        public string proc_step_bonus_for_card { get; set; }

        [Display(Name = "sum_step_card_bonus_for_card")]
        public bool sum_step_card_bonus_for_card { get; set; }

        [Display(Name = "sum_step_trans_bonus_for_card")]
        public bool sum_step_trans_bonus_for_card { get; set; }

        [Display(Name = "use_max_percent_disc")]
        public bool use_max_percent_disc { get; set; }

        [Display(Name = "allow_zero_disc_bonus_for_card")]
        public bool allow_zero_disc_bonus_for_card { get; set; }

        [Display(Name = "allow_zero_disc_bonus_for_pay")]
        public bool allow_zero_disc_bonus_for_pay { get; set; }

        [Display(Name = "same_for_zhv_disc")]
        public bool same_for_zhv_disc { get; set; }

        [Display(Name = "same_for_zhv_bonus")]
        public bool same_for_zhv_bonus { get; set; }

        [Display(Name = "proc_const_zhv_disc")]
        public Nullable<decimal> proc_const_zhv_disc { get; set; }

        [Display(Name = "proc_step_zhv_disc")]
        public string proc_step_zhv_disc { get; set; }

        [Display(Name = "sum_step_card_zhv_disc")]
        public bool sum_step_card_zhv_disc { get; set; }

        [Display(Name = "sum_step_trans_zhv_disc")]        
        public bool sum_step_trans_zhv_disc { get; set; }

        [Display(Name = "proc_const_zhv_bonus_for_card")]
        public Nullable<decimal> proc_const_zhv_bonus_for_card { get; set; }

        [Display(Name = "proc_step_zhv_bonus_for_card")]
        public string proc_step_zhv_bonus_for_card { get; set; }

        [Display(Name = "sum_step_card_zhv_bonus_for_card")]
        public bool sum_step_card_zhv_bonus_for_card { get; set; }

        [Display(Name = "sum_step_trans_zhv_bonus_for_card")]
        public bool sum_step_trans_zhv_bonus_for_card { get; set; }

        [Display(Name = "use_max_percent_zhv_disc")]
        public bool use_max_percent_zhv_disc { get; set; }

        [Display(Name = "allow_zero_disc_zhv_bonus_for_card")]
        public bool allow_zero_disc_zhv_bonus_for_card { get; set; }

        [Display(Name = "allow_zero_disc_zhv_bonus_for_pay")]
        public bool allow_zero_disc_zhv_bonus_for_pay { get; set; }

        [Display(Name = "allow_order_disc")]
        public Nullable<int> allow_order_disc { get; set; }

        [Display(Name = "allow_order_bonus_for_card")]
        public Nullable<int> allow_order_bonus_for_card { get; set; }

        [Display(Name = "allow_order_bonus_for_pay")]
        public Nullable<int> allow_order_bonus_for_pay { get; set; }

        [Display(Name = "add_to_card_trans_sum")]
        public bool add_to_card_trans_sum { get; set; }

        [Display(Name = "add_to_card_trans_sum_with_disc")]
        public bool add_to_card_trans_sum_with_disc { get; set; }

        [Display(Name = "add_to_card_trans_sum_with_bonus_for_pay")]
        public bool add_to_card_trans_sum_with_bonus_for_pay { get; set; }

        [Display(Name = "mess_contains_ext_info")]
        public bool mess_contains_ext_info { get; set; }

        [Display(Name = "card_type_id")]
        public Nullable<long> card_type_id { get; set; }

        [Display(Name = "proc_const_from_card_disc")]
        public bool proc_const_from_card_disc { get; set; }

        [Display(Name = "proc_const_from_card_zhv_disc")]
        public bool proc_const_from_card_zhv_disc { get; set; }

        [Display(Name = "proc_const_from_card_bonus_for_card")]
        public bool proc_const_from_card_bonus_for_card { get; set; }

        [Display(Name = "proc_const_from_card_zhv_bonus_for_card")]
        public bool proc_const_from_card_zhv_bonus_for_card { get; set; }

        [Display(Name = "trans_sum_max_percent_bonus_for_pay")]
        public Nullable<decimal> trans_sum_max_percent_bonus_for_pay { get; set; }

        [Display(Name = "trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay")]
        public Nullable<decimal> trans_sum_max_percent_and_max_disc_not_zero_bonus_for_pay { get; set; }

        [Display(Name = "price_margin_percent_lte_forbid_disc")]
        public Nullable<decimal> price_margin_percent_lte_forbid_disc { get; set; }

        [Display(Name = "price_margin_percent_lte_forbid_zhv_disc")]
        public Nullable<decimal> price_margin_percent_lte_forbid_zhv_disc { get; set; }

        [Display(Name = "price_margin_percent_gte_forbid_disc")]
        public Nullable<decimal> price_margin_percent_gte_forbid_disc { get; set; }

        [Display(Name = "price_margin_percent_gte_forbid_zhv_disc")]
        public Nullable<decimal> price_margin_percent_gte_forbid_zhv_disc { get; set; }

        [Display(Name = "use_explicit_max_percent_disc")]
        public Nullable<decimal> use_explicit_max_percent_disc { get; set; }

        [Display(Name = "use_explicit_max_percent_zhv_disc")]
        public Nullable<decimal> use_explicit_max_percent_zhv_disc { get; set; }

        [Display(Name = "order_state")]
        public string order_state { get; set; }

        [Display(Name = "business_name")]
        public string business_name { get; set; }

        [Display(Name = "card_type_group_name")]
        public string card_type_group_name { get; set; }

        [Display(Name = "card_type_name")]
        public string card_type_name { get; set; }

        [Display(Name = "programm_name")]
        public string programm_name { get; set; }

        [Display(Name = "programm_id_str")]
        public string programm_id_str { get { return programm_id.ToString(); } }
    }
}