﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammStepConditionTypeViewModel : AuBaseViewModel
    {
        public ProgrammStepConditionTypeViewModel()
            : base()
        {            
            //programm_step_condition_type
        }

        [Key()]
        [Display(Name="Код")]
        public int condition_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string condition_type_name { get; set; }
    }
}