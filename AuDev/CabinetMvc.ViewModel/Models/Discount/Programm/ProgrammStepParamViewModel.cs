﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammStepParamViewModel : AuBaseViewModel
    {
        public ProgrammStepParamViewModel()
            : base(Enums.MainObjectType.PROGRAMM_STEP_PARAM)
        {                                     
            //vw_programm_step_param
        }

        // db

        [Key()]
        [Display(Name="Код")]
        public long param_id { get; set; }

        [Display(Name = "Алгоритм")]
        public long programm_id { get; set; }

        [Display(Name = "Шаг")]
        public long step_id { get; set; }

        [Display(Name = "Тип параметра карты")]
        public Nullable<int> card_param_type { get; set; }

        [Display(Name = "Тип параметра продажи")]
        public Nullable<int> trans_param_type { get; set; }

        [Display(Name = "Параметр алгоритма")]
        public Nullable<long> programm_param_id { get; set; }

        [Display(Name = "Параметр другого шага")]
        public Nullable<long> prev_step_param_id { get; set; }

        [Display(Name = "№")]
        public Nullable<int> param_num { get; set; }

        [Display(Name = "Наименование")]
        public string param_name { get; set; }

        [Display(Name = "Выходной для алгоритма")]
        public short is_programm_output { get; set; }

        [Display(Name = "Тип параметра")]
        public Nullable<int> param_type_id { get; set; }

        [Display(Name = "Шаг")]
        public int step_num { get; set; }

        [Display(Name = "Организация")]
        public Nullable<long> business_id { get; set; }

        [Display(Name = "Тип параметра")]
        public string param_type_name { get; set; }

        [Display(Name = "Тип параметра карты")]
        public string card_param_type_name { get; set; }

        [Display(Name = "Тип параметра продажи")]
        public string trans_param_type_name { get; set; }

        [Display(Name = "Параметр алгоритма")]
        public string programm_param_name { get; set; }

        [Display(Name = "Параметр другого шага")]
        public string prev_step_param_name { get; set; }

        [Display(Name = "Выходной для алгоритма")]
        public bool is_programm_output_chb { get; set; }

        [Display(Name = "Используемый параметр")]
        public string curr_param_name { get; set; }

        [Display(Name = "Код")]
        public string param_id_str { get; set; }

        [Display(Name = "Код алгоритма")]
        public string programm_id_str { get; set; }

        [Display(Name = "Код шага")]
        public string step_id_str { get; set; }

        [Display(Name = "Код параметра алгоритма")]
        public string programm_param_id_str { get; set; }

        [Display(Name = "Код параметра другого шага")]
        public string prev_step_param_id_str { get; set; }

        [Display(Name = "Код организации")]
        public string business_id_str { get; set; }
    }
}