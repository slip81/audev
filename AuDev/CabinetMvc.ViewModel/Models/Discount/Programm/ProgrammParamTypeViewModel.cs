﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammParamTypeViewModel : AuBaseViewModel
    {
        public ProgrammParamTypeViewModel()
            : base()
        {            
            //programm_param_type
        }

        [Key()]
        [Display(Name="Код")]
        public int param_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string param_type_name { get; set; }
    }
}