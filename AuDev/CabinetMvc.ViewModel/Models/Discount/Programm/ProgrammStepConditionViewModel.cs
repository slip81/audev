﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammStepConditionViewModel : AuBaseViewModel
    {
        public ProgrammStepConditionViewModel()
            : base(Enums.MainObjectType.PROGRAMM_STEP_CONDITION)
        {                                     
            //vw_programm_step_condition
        }

        // db

        [Key()]
        [Display(Name="Код")]
        public long condition_id { get; set; }

        [Display(Name = "Алгоритм")]
        public long programm_id { get; set; }

        [Display(Name = "Шаг")]
        public long step_id { get; set; }

        [Display(Name = "№")]
        public Nullable<int> condition_num { get; set; }

        [Display(Name = "Тип условия")]
        public Nullable<int> op_type { get; set; }

        [Display(Name = "Первый параметр")]
        public Nullable<long> op1_param_id { get; set; }

        [Display(Name = "Второй параметр")]
        public Nullable<long> op2_param_id { get; set; }

        [Display(Name = "Наименование")]
        public string condition_name { get; set; }

        [Display(Name = "Шаг")]
        public long step_num { get; set; }

        [Display(Name = "Организация")]
        public Nullable<long> business_id { get; set; }

        [Display(Name = "Тип условия")]
        public Nullable<int> condition_type_id { get; set; }

        [Display(Name = "Тип условия")]
        public string condition_type_name { get; set; }

        [Display(Name = "Первый параметр")]
        public string op1_param_name { get; set; }

        [Display(Name = "Второй параметр")]
        public string op2_param_name { get; set; }

        [Display(Name = "Код")]
        public string condition_id_str { get; set; }

        [Display(Name = "Код алгоритма")]
        public string programm_id_str { get; set; }

        [Display(Name = "Код шага")]
        public string step_id_str { get; set; }

        [Display(Name = "Первый параметр")]
        public string op1_param_id_str { get; set; }

        [Display(Name = "Второй параметр")]
        public string op2_param_id_str { get; set; }

        [Display(Name = "Код организации")]
        public string business_id_str { get; set; }
    }
}