﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IProgrammStepParamService : IAuBaseService
    {
        IQueryable<ProgrammStepParamViewModel> GetList_forStepParam(long programm_id, long step_id);
        IQueryable<ProgrammStepParamViewModel> GetList_forStep(long programm_id, long step_id);
    }

    public class ProgrammStepParamService : AuBaseService, IProgrammStepParamService
    {
        public IQueryable<ProgrammStepParamViewModel> GetList_forStepParam(long programm_id, long step_id)
        {
            return dbContext.vw_programm_step_param.Where(ss => ss.programm_id == programm_id && ss.step_id != step_id)
                .ProjectTo<ProgrammStepParamViewModel>()
                .OrderBy(ss => ss.param_name);
        }

        public IQueryable<ProgrammStepParamViewModel> GetList_forStep(long programm_id, long step_id)
        {
            return dbContext.vw_programm_step_param.Where(ss => ss.programm_id == programm_id && ss.step_id == step_id)
                .ProjectTo<ProgrammStepParamViewModel>()
                .OrderBy(ss => ss.param_name);
        }
    }
}