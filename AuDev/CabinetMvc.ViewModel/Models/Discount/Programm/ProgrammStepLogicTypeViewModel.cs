﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammStepLogicTypeViewModel : AuBaseViewModel
    {
        public ProgrammStepLogicTypeViewModel()
            : base()
        {            
            //programm_step_logic_type
        }

        [Key()]
        [Display(Name="Код")]
        public int logic_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string logic_type_name { get; set; }
    }
}