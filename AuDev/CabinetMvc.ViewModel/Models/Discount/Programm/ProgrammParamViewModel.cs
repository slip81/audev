﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammParamViewModel : AuBaseViewModel
    {
        public ProgrammParamViewModel()
            : base(Enums.MainObjectType.PROGRAMM_PARAM)
        {
            //
        }

        public ProgrammParamViewModel(programm_param item)
            : base(Enums.MainObjectType.PROGRAMM_PARAM)
        {                        
            param_id = item.param_id;
            programm_id = item.programm_id;
            param_num = item.param_num;
            param_type = item.param_type;
            string_value = item.string_value;
            int_value = item.int_value;
            float_value = item.float_value;
            date_value = item.date_value;
            param_name = item.param_name;
            date_only_value = item.date_only_value;
            is_internal = item.is_internal;
            for_unit_pos = item.for_unit_pos;
            group_id = item.group_id;
            group_internal_num = item.group_internal_num;
            param_type_id = item.param_type;
            is_internal_chb = item.is_internal == 1 ? true : false;
            for_unit_pos_chb = item.for_unit_pos == 1 ? true : false;
        }

        [Key()]
        [Display(Name="Код")]
        public long param_id { get; set; }

        [Display(Name = "Алгоритм")]
        public long programm_id { get; set; }

        [Display(Name = "№")]
        public Nullable<int> param_num { get; set; }

        [Display(Name = "Тип")]
        public int param_type { get; set; }

        [Display(Name = "Значение")]
        public string string_value { get; set; }

        [Display(Name = "Значение")]
        public Nullable<long> int_value { get; set; }

        [Display(Name = "Значение")]
        public Nullable<decimal> float_value { get; set; }

        [Display(Name = "Значение")]
        public Nullable<System.DateTime> date_value { get; set; }

        [Display(Name = "Наименование")]
        public string param_name { get; set; }

        [Display(Name = "Значение")]
        public Nullable<System.DateTime> date_only_value { get; set; }

        [Display(Name = "Значение")]        
        public Nullable<System.TimeSpan> time_only_value { get; set; }

        [Display(Name = "Внутренний")]
        public short is_internal { get; set; }

        [Display(Name = "Для позиции")]
        public short for_unit_pos { get; set; }

        public Nullable<int> group_id { get; set; }

        public Nullable<int> group_internal_num { get; set; }
        
        [JsonIgnore()]
        public string param_id_str { get { return param_id.ToString(); } }
        [JsonIgnore()]
        public Nullable<int> param_num_read_only { get { return param_num; } }
        [JsonIgnore()]
        public string param_name_read_only { get { return param_name; } }
        [JsonIgnore()]
        public string param_type_str
        {
            get
            {
                return param_type.EnumName<Enums.ProgrammParamType>();
            }
        }
        [JsonIgnore()]
        public string param_value_str
        {
            get
            {
                switch ((Enums.ProgrammParamType)param_type)
                {
                    case Enums.ProgrammParamType.STRING_VAL:
                        return string_value;
                    case Enums.ProgrammParamType.INT_VAL:
                        return int_value.ToString();
                    case Enums.ProgrammParamType.FLOAT_VAL:
                        return float_value.ToString();
                    case Enums.ProgrammParamType.DATE_VAL:
                        return date_value.ToString();
                    case Enums.ProgrammParamType.DATE_ONLY_VAL:
                        return date_only_value.ToString();
                    case Enums.ProgrammParamType.TIME_ONLY_VAL:
                        return time_only_value.ToString();
                    default:
                        return "-";
                }
            }
        }

        [Display(Name = "Тип")]
        public int param_type_id { get; set; }

        [Display(Name = "Внутренний")]
        public bool is_internal_chb { get; set; }

        [Display(Name = "Для позиции")]
        public bool for_unit_pos_chb { get; set; }

    }
}