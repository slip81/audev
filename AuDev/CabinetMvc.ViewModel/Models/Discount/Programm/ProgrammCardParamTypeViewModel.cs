﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class ProgrammCardParamTypeViewModel : AuBaseViewModel
    {
        public ProgrammCardParamTypeViewModel()
            : base()
        {            
            //programm_card_param_type
        }

        [Key()]
        [Display(Name="Код")]
        public int card_param_type { get; set; }

        [Display(Name = "Наименование")]
        public string card_param_type_name { get; set; }
    }
}