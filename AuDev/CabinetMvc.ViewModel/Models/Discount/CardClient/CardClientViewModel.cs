﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardClientViewModel : AuBaseViewModel
    {
        public CardClientViewModel()
            : base(Enums.MainObjectType.CLIENT)
        {
            //
        }

        public CardClientViewModel(card_holder item)
            : base(Enums.MainObjectType.CLIENT)
        {
            client_id = item.card_holder_id;
            client_surname = item.card_holder_surname;
            client_name = item.card_holder_name;
            client_fname = item.card_holder_fname;
            date_birth = item.date_birth;
            phone_num = item.phone_num;
            address = item.address;
            email = item.email;
            comment = item.comment;
            business_id = item.business_id;
            //client_id_str = item.client_id.ToString();
            card_num_rel = item.card == null ? "" : item.card.Select(ss => ss.card_num.ToString()).FirstOrDefault();
        }

        [Key()]
        [Display(Name = "Код владельца")]
        public long client_id { get; set; }

        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Не задана фамилия")]
        public string client_surname { get; set; }

        [Display(Name = "Имя")]
        public string client_name { get; set; }

        [Display(Name = "Отчество")]
        public string client_fname { get; set; }

        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        private Nullable<System.DateTime> date_birth { get; set; }

        // перевод времени в UTC
        // http://docs.telerik.com/kendo-ui/aspnet-mvc/helpers/grid/how-to/utc-time-on-both-server-and-client#using-utc-time-on-both-the-client-and-the-server

        [Display(Name = "Дата рождения")]
        public Nullable<DateTime> DateBirth
        {
            get { return this.date_birth; }
            set
            {
                this.date_birth = value == null ? null : (DateTime?)(new DateTime(((DateTime)value).Ticks, DateTimeKind.Utc));
            }
        }

        [Display(Name = "Телефон")]
        public string phone_num { get; set; }

        [Display(Name = "Адрес")]
        public string address { get; set; }

        [Display(Name = "Эл почта")]
        public string email { get; set; }

        [Display(Name = "Комментарий")]
        public string comment { get; set; }

        [Display(Name = "Организация")]
        public long business_id { get; set; }

        [JsonIgnore()]
        public string client_id_str { get { return client_id.ToString(); } }
        [JsonIgnore()]
        public string client_full_name 
        { 
            get 
            {
                return ((String.IsNullOrEmpty(client_surname) ? "[-] " : client_surname.Trim() + " ")
                    + (String.IsNullOrEmpty(client_name) ? "" : client_name.Trim() + " ")
                    + (String.IsNullOrEmpty(client_fname) ? "" : client_fname.Trim()))
                    .Trim();
            } 
        }

        [Display(Name = "Выбранная карта")]
        public string card_num_rel { get; set; }
    }

}