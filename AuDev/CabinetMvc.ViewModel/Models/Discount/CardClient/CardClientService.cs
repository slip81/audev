﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardClientService : IAuBaseService
    {
        //
    }

    public class CardClientService : AuBaseService, ICardClientService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.card_holder.Where(ss => ss.business_id == parent_id)
                .Select(ss => new CardClientViewModel
                {
                    address = ss.address,
                    business_id = ss.business_id,
                    client_fname = ss.card_holder_fname,
                    client_id = ss.card_holder_id,
                    client_name = ss.card_holder_name,
                    client_surname = ss.card_holder_surname,
                    comment = ss.comment,
                    DateBirth = ss.date_birth,
                    email = ss.email,
                    phone_num = ss.phone_num,
                    //client_id_str = ss.client_id.ToString(),
                });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var client = dbContext.card_holder.Where(ss => ss.card_holder_id == id).FirstOrDefault();
            return client == null ? null : new CardClientViewModel(client);
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardClientViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardClientViewModel client_add = (CardClientViewModel)item;
            if (
                (client_add == null)
                ||
                (String.IsNullOrEmpty(client_add.client_surname))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты владельца карты");
                return null;
            }
            
            DateTime today = DateTime.Today;

            long business_id = currUser.BusinessId;            
            client_add.business_id = business_id;

            card_holder card_holder = new card_holder();
            card_holder.business_id = business_id;
            card_holder.card_holder_surname = client_add.client_surname;
            card_holder.card_holder_name = client_add.client_name;
            card_holder.card_holder_fname = client_add.client_fname;
            card_holder.date_birth = client_add.DateBirth;
            card_holder.phone_num = client_add.phone_num;
            card_holder.address = client_add.address;
            card_holder.email = client_add.email;
            card_holder.comment = client_add.comment;

            dbContext.card_holder.Add(card_holder);

            if (!String.IsNullOrEmpty(client_add.card_num_rel))
            {
                long card_num = 0;
                bool parseOk = long.TryParse(client_add.card_num_rel, out card_num);
                if (!parseOk)
                    card_num = 0;

                card card = dbContext.card.Where(ss => ss.business_id == business_id && ss.card_num == card_num).FirstOrDefault();
                if (card == null)
                {
                    modelState.AddModelError("", "Не найдена карта с номером " + client_add.card_num_rel);
                    return null;
                }

                var card_holder_rel_existing = dbContext.card_holder_card_rel.Where(ss => ss.card_id == card.card_id && ss.date_beg <= today && ((!ss.date_end.HasValue) || (ss.date_end > today))).FirstOrDefault();
                if (card_holder_rel_existing != null)
                {
                    modelState.AddModelError("", "Эта карта уже привязана к: " + card_holder_rel_existing.card_holder.card_holder_surname);
                    return null;
                }

                card_holder_card_rel card_holder_card_rel = new card_holder_card_rel();
                card_holder_card_rel.card_holder_id = card_holder.card_holder_id;
                card_holder_card_rel.card_id = card.card_id;
                card_holder_card_rel.date_beg = today;
                card_holder_card_rel.date_end = null;
                dbContext.card_holder_card_rel.Add(card_holder_card_rel);
                card.curr_holder_id = card_holder.card_holder_id;
            }

            dbContext.SaveChanges();

            return new CardClientViewModel(card_holder);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardClientViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CardClientViewModel client_edit = (CardClientViewModel)item;
            if (
                (client_edit == null)
                ||
                (String.IsNullOrEmpty(client_edit.client_surname))
                )
            {
                modelState.AddModelError("", "Не заданы атрибуты владельца карты");
                return null;
            }

            DateTime today = DateTime.Today;

            long business_id = currUser.BusinessId;
            client_edit.business_id = business_id;

            var card_holder = dbContext.card_holder.Where(ss => ss.card_holder_id == client_edit.client_id).FirstOrDefault();
            if (card_holder == null)
            {
                modelState.AddModelError("", "Не найден владелец карты с кодом " + client_edit.client_id.ToString());
                return null;
            }
            modelBeforeChanges = new CardClientViewModel(card_holder);

            card_holder.card_holder_surname = client_edit.client_surname;
            card_holder.card_holder_name = client_edit.client_name;
            card_holder.card_holder_fname = client_edit.client_fname;
            card_holder.date_birth = client_edit.DateBirth;
            card_holder.phone_num = client_edit.phone_num;
            card_holder.address = client_edit.address;
            card_holder.email = client_edit.email;
            card_holder.comment = client_edit.comment;

            // !!!
            // todo

            /*
            if (!String.IsNullOrEmpty(client_edit.card_num_rel))
            {
                long card_num = 0;
                bool parseOk = long.TryParse(client_edit.card_num_rel, out card_num);
                if (!parseOk)
                    card_num = 0;

                card card = dbContext.card.Where(ss => ss.business_id == business_id && ss.card_num == card_num).FirstOrDefault();
                if (card == null)
                {
                    modelState.AddModelError("", "Не найдена карта с номером " + client_edit.card_num_rel);
                    return null;
                }

                var card_holder_rel_existing = dbContext.card_holder_card_rel.Where(ss => ss.card_id == card.card_id && ss.date_beg <= today && ((!ss.date_end.HasValue) || (ss.date_end > today))).FirstOrDefault();
                if (card_holder_rel_existing != null)
                {
                    modelState.AddModelError("", "Эта карта уже привязана к: " + card_holder_rel_existing.card_holder.card_holder_surname);
                    return null;
                }

                card_holder_card_rel card_holder_card_rel = new card_holder_card_rel();
                card_holder_card_rel.card_holder_id = card_holder.card_holder_id;
                card_holder_card_rel.card_id = card.card_id;
                card_holder_card_rel.date_beg = today;
                card_holder_card_rel.date_end = null;
                dbContext.card_holder_card_rel.Add(card_holder_card_rel);
                card.curr_holder_id = card_holder.card_holder_id;
            }
            */

            dbContext.SaveChanges();

            return new CardClientViewModel(card_holder);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CardClientViewModel))
            {
                modelState.AddModelError("serv_result", "Некорректные параметры");
                return false;
            }
            
            long id = ((CardClientViewModel)item).client_id;
            long business_id = currUser.BusinessId;

            card_holder card_holder = dbContext.card_holder.Where(ss => ss.card_holder_id == id).FirstOrDefault();
            if (card_holder == null)
            {
                modelState.AddModelError("serv_result", "Владелец карты не найден");
                return false;
            }

            List<card> card_list = dbContext.card.Where(ss => ss.curr_holder_id.HasValue && ss.curr_holder_id == id).ToList();
            if ((card_list != null) && (card_list.Count > 0))
            {
                modelState.AddModelError("serv_result", "Есть действующая карта");
                return false;
            }
            
            dbContext.card_holder_card_rel.RemoveRange(dbContext.card_holder_card_rel.Where(ss => ss.card_holder_id == id));
            dbContext.card_holder.Remove(card_holder);
            dbContext.SaveChanges();
            return true;
        }
    }
}