﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IReportDiscountService : IAuBaseService
    {
        //
    }

    public class ReportDiscountService : AuBaseService, IReportDiscountService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.report_discount
                .ProjectTo<ReportDiscountViewModel>()
                ;
        }
    }
}