﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class ReportDiscountViewModel : AuBaseViewModel
    {
        public ReportDiscountViewModel()
            : base(Enums.MainObjectType.REPORT_DISCOUNT)
        {
            //report_discount      
        }

        [Key()]
        [Display(Name = "Код")]
        public int report_id { get; set; }
        
        [Display(Name = "Название")]
        public string report_name { get; set; }
        
        [Display(Name = "Файл")]
        public string report_filename { get; set; }

        [Display(Name = "Описание")]
        public string report_descr { get; set; }

        [Display(Name = "Параметры")]
        public string report_params { get; set; }

        [Display(Name = "StartBtn")]
        [JsonIgnore()]
        public string StartBtn
        {
            get
            {
                return "<button type='button' class='k-button k-button-icontext btn-start-report' title='Сформировать отчет' onclick='reportList.onStartReportBtnClick(" + report_id.ToString() + ")'><span class='k-icon k-i-arrow-e'></span>Сформировать</button>";
            }
        }
    }
}