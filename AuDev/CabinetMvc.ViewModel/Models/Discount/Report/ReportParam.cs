using System;
using System.Collections.Generic;

namespace CabinetMvc.ViewModel.Discount
{    
    public class ReportParam
    {
        public ReportParam()
        {
            //
        }

        public long report_id { get; set; }
        public Nullable<DateTime> date_beg { get; set; }
        public Nullable<DateTime> date_end { get; set; }
        
    }
}
