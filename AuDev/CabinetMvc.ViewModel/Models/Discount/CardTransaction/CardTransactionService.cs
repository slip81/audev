﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardTransactionService : IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetLastCardTransactionList_byParent(long parent_id, int cnt);
        IQueryable<CardTransactionViewModel> GetCardTransactionList_byCard(long card_id);
    }

    public class CardTransactionService : AuBaseService, ICardTransactionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_card_transaction
                .Where(ss => ss.business_id == parent_id
                    //&& ss.trans_kind == (short)Enums.CardTransactionType.CALC
                    && ((ss.trans_kind == (short)Enums.CardTransactionType.CALC) || (ss.trans_kind == (short)Enums.CardTransactionType.RETURN))
                    && ss.status == (int)Enums.CardTransactionStatus.DONE)                    
                    .Select(ss => new CardTransactionViewModel
                    {
                        card_trans_id = ss.card_trans_id,
                        card_id = ss.card_id,
                        card_num = ss.card_num,
                        trans_num = ss.trans_num,
                        date_beg = ss.date_beg,
                        trans_sum = ss.trans_sum,
                        trans_kind = ss.trans_kind,
                        status = ss.status,
                        canceled_trans_id = ss.canceled_trans_id,
                        programm_result_id = ss.programm_result_id,
                        user_name = ss.user_name,
                        date_check = ss.date_check,
                        curr_card_sum = ss.curr_card_sum,
                        curr_card_percent_value = ss.curr_card_percent_value,
                        curr_card_bonus_value = ss.curr_card_bonus_value,
                        trans_card_percent_value = ss.trans_card_percent_value,
                        trans_card_bonus_value = ss.trans_card_bonus_value,
                        org_name = ss.org_name,
                        card_num2 = ss.card_num2,
                        date_check_date_only = ss.date_check_date_only,
                        curr_card_type_name = ss.curr_card_type_name,
                        trans_sum_discount = ss.trans_sum_discount,
                        //trans_sum_with_discount = ss.trans_sum_with_discount,
                        trans_sum_with_discount = ss.trans_sum - ss.trans_sum_discount,
                        trans_sum_bonus_for_card = ss.trans_sum_bonus_for_card,
                        trans_sum_bonus_for_pay = ss.trans_sum_bonus_for_pay,
                        business_id = ss.business_id,
                        status_date = ss.status_date,
                        is_delayed = ss.is_delayed,
                    });
        }

        public IQueryable<AuBaseViewModel> GetLastCardTransactionList_byParent(long parent_id, int cnt)
        {
            return dbContext.vw_card_transaction
                .Where(ss => ss.business_id == parent_id
                    && ss.trans_kind == (short)Enums.CardTransactionType.CALC
                    && ss.status == (int)Enums.CardTransactionStatus.DONE)
                    .OrderByDescending(ss => ss.date_beg)
                    .Take(cnt)
                    .Select(ss => new CardTransactionViewModel
                    {
                        card_trans_id = ss.card_trans_id,
                        card_id = ss.card_id,
                        card_num = ss.card_num,
                        trans_num = ss.trans_num,
                        date_beg = ss.date_beg,
                        trans_sum = ss.trans_sum,
                        trans_kind = ss.trans_kind,
                        status = ss.status,
                        canceled_trans_id = ss.canceled_trans_id,
                        programm_result_id = ss.programm_result_id,
                        user_name = ss.user_name,
                        date_check = ss.date_check,
                        curr_card_sum = ss.curr_card_sum,
                        curr_card_percent_value = ss.curr_card_percent_value,
                        curr_card_bonus_value = ss.curr_card_bonus_value,
                        trans_card_percent_value = ss.trans_card_percent_value,
                        trans_card_bonus_value = ss.trans_card_bonus_value,
                        org_name = ss.org_name,
                        card_num2 = ss.card_num2,
                        date_check_date_only = ss.date_check_date_only,
                        curr_card_type_name = ss.curr_card_type_name,
                        trans_sum_discount = ss.trans_sum_discount,
                        //trans_sum_with_discount = ss.trans_sum_with_discount,
                        trans_sum_with_discount = ss.trans_sum - ss.trans_sum_discount,
                        trans_sum_bonus_for_card = ss.trans_sum_bonus_for_card,
                        trans_sum_bonus_for_pay = ss.trans_sum_bonus_for_pay,
                        business_id = ss.business_id,
                        status_date = ss.status_date,
                        is_delayed = ss.is_delayed,
                    });
        }

        public IQueryable<CardTransactionViewModel> GetCardTransactionList_byCard(long card_id)
        {
            return dbContext.vw_card_transaction
                .Where(ss => ss.card_id == card_id
                    && ((ss.trans_kind == (short)Enums.CardTransactionType.CALC) || (ss.trans_kind == (short)Enums.CardTransactionType.RETURN))
                    && ss.status == (int)Enums.CardTransactionStatus.DONE)
                    .Select(ss => new CardTransactionViewModel
                    {
                        card_trans_id = ss.card_trans_id,
                        card_id = ss.card_id,
                        card_num = ss.card_num,
                        trans_num = ss.trans_num,
                        date_beg = ss.date_beg,
                        trans_sum = ss.trans_sum,
                        trans_kind = ss.trans_kind,
                        status = ss.status,
                        canceled_trans_id = ss.canceled_trans_id,
                        programm_result_id = ss.programm_result_id,
                        user_name = ss.user_name,
                        date_check = ss.date_check,
                        curr_card_sum = ss.curr_card_sum,
                        curr_card_percent_value = ss.curr_card_percent_value,
                        curr_card_bonus_value = ss.curr_card_bonus_value,
                        trans_card_percent_value = ss.trans_card_percent_value,
                        trans_card_bonus_value = ss.trans_card_bonus_value,
                        org_name = ss.org_name,
                        card_num2 = ss.card_num2,
                        date_check_date_only = ss.date_check_date_only,
                        curr_card_type_name = ss.curr_card_type_name,
                        trans_sum_discount = ss.trans_sum_discount,
                        //trans_sum_with_discount = ss.trans_sum_with_discount,
                        trans_sum_with_discount = ss.trans_sum - ss.trans_sum_discount,
                        trans_sum_bonus_for_card = ss.trans_sum_bonus_for_card,
                        trans_sum_bonus_for_pay = ss.trans_sum_bonus_for_pay,
                        business_id = ss.business_id,
                        status_date = ss.status_date,
                        is_delayed = ss.is_delayed,
                    });
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            var card_trans = dbContext.vw_card_transaction.Where(ss => ss.card_trans_id == id).FirstOrDefault();
            var detail_list = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == id).ToList();
            return card_trans == null ? null : new CardTransactionViewModel(card_trans, detail_list);
        }
    }
}