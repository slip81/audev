﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICardTransactionDetailService : IAuBaseService
    {
        //
    }

    public class CardTransactionDetailService : AuBaseService, ICardTransactionDetailService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == parent_id 
                    //&& ss.unit_type == 0
                    )
                    .Select(ss => new CardTransactionDetailViewModel
                    {
                        detail_id = ss.detail_id,
                        card_trans_id = ss.card_trans_id,
                        programm_id = ss.programm_id,
                        unit_num = ss.unit_num,
                        unit_name = ss.unit_name,
                        unit_value = ss.unit_value,
                        unit_value_discount = ss.unit_value_discount,
                        unit_value_with_discount = ss.unit_value_with_discount,
                        unit_percent_discount = ss.unit_percent_discount,
                        unit_value_bonus_for_card = ss.unit_value_bonus_for_card,
                        unit_value_bonus_for_pay = ss.unit_value_bonus_for_pay,
                        unit_percent_bonus_for_card = ss.unit_percent_bonus_for_card,
                        unit_max_discount_percent = ss.unit_max_discount_percent,
                        unit_count = ss.unit_count,
                        unit_type = ss.unit_type,
                        unit_max_bonus_for_card_percent = ss.unit_max_bonus_for_card_percent,
                        unit_max_bonus_for_pay_percent = ss.unit_max_bonus_for_pay_percent,
                        price_margin_percent = ss.price_margin_percent,
                        live_prep = ss.live_prep,
                        promo = ss.promo,
                        tax_percent = ss.tax_percent,
                    });
        }
    }
}