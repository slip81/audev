﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardTransactionDetailViewModel : AuBaseViewModel
    {
        public CardTransactionDetailViewModel()
            : base(Enums.MainObjectType.CARD_TRANS_DETAIL)
        {
            //
        }

        public CardTransactionDetailViewModel(card_transaction_detail item)
            : base(Enums.MainObjectType.CARD_TRANS_DETAIL)
        {
            detail_id = item.detail_id;
            card_trans_id = item.card_trans_id;
            programm_id = item.programm_id;
            unit_num = item.unit_num;
            unit_name = item.unit_name;
            unit_value = item.unit_value;
            unit_value_discount = item.unit_value_discount;
            unit_value_with_discount = item.unit_value_with_discount;
            unit_percent_discount = item.unit_percent_discount;
            unit_value_bonus_for_card = item.unit_value_bonus_for_card;
            unit_value_bonus_for_pay = item.unit_value_bonus_for_pay;
            unit_percent_bonus_for_card = item.unit_percent_bonus_for_card;
            unit_max_discount_percent = item.unit_max_discount_percent;
            unit_count = item.unit_count;
            unit_type = item.unit_type;            
            unit_max_bonus_for_card_percent = item.unit_max_bonus_for_card_percent;
            unit_max_bonus_for_pay_percent = item.unit_max_bonus_for_pay_percent;
            price_margin_percent = item.price_margin_percent;
            artikul = item.artikul;
            live_prep = item.live_prep;
            promo = item.promo;
            tax_percent = item.tax_percent;
        }

        [Key()]
        public long detail_id { get; set; }
        public Nullable<long> card_trans_id { get; set; }
        public Nullable<long> programm_id { get; set; }
        public Nullable<int> unit_num { get; set; }
        public string unit_name { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_value { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_value_discount { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_value_with_discount { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_percent_discount { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_value_bonus_for_card { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_value_bonus_for_pay { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_percent_bonus_for_card { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_max_discount_percent { get; set; }
        public Nullable<decimal> unit_count { get; set; }
        public Nullable<int> unit_type { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_max_bonus_for_card_percent { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> unit_max_bonus_for_pay_percent { get; set; }
        public Nullable<decimal> price_margin_percent { get; set; }
        public Nullable<int> artikul { get; set; }
        public Nullable<int> live_prep { get; set; }
        public Nullable<int> promo { get; set; }        
        public Nullable<decimal> tax_percent { get; set; }
    }
}