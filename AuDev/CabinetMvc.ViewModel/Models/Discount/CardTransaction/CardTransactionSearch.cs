using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Util;

namespace CabinetMvc.ViewModel.Discount
{    
    public class CardTransSearch
    {
        public CardTransSearch()
        {
            //
        }
    
        public long? CardNum { get; set; }
        public string CardNum2 { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateBeg { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateEnd { get; set; }
    }
}
