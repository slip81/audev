﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardTransactionViewModel : AuBaseViewModel
    {

        public CardTransactionViewModel()
            : base(Enums.MainObjectType.CARD_TRANS)
        {
            //
        }

        public CardTransactionViewModel(vw_card_transaction item, List<card_transaction_detail> detail_list = null)
            : base(Enums.MainObjectType.CARD_TRANS)
        {
            card_trans_id = item.card_trans_id;
            card_id = item.card_id;
            card_num = item.card_num;
            trans_num = item.trans_num;
            date_beg = item.date_beg;
            trans_sum = item.trans_sum;
            trans_kind = item.trans_kind;
            status = item.status;
            canceled_trans_id = item.canceled_trans_id;
            programm_result_id = item.programm_result_id;
            user_name = item.user_name;
            date_check = item.date_check;
            curr_card_sum = item.curr_card_sum;
            curr_card_percent_value = item.curr_card_percent_value;
            curr_card_bonus_value = item.curr_card_bonus_value;
            trans_card_percent_value = item.trans_card_percent_value;
            trans_card_bonus_value = item.trans_card_bonus_value;
            org_name = item.org_name;
            card_num2 = item.card_num2;
            date_check_date_only = item.date_check_date_only;
            curr_card_type_name = item.curr_card_type_name;
            trans_sum_discount = item.trans_sum_discount;
            trans_sum_with_discount = item.trans_sum_with_discount;
            trans_sum_bonus_for_card = item.trans_sum_bonus_for_card;
            trans_sum_bonus_for_pay = item.trans_sum_bonus_for_pay;
            business_id = item.business_id;
            status_date = item.status_date;
            is_delayed = item.is_delayed;

            this.card_trans_detail_list = detail_list != null ? detail_list.Select(ss => new CardTransactionDetailViewModel(ss)) : null;
        }
        
        [Key()]
        public long card_trans_id { get; set; }
        public Nullable<long> card_id { get; set; }
        public Nullable<long> card_num { get; set; }
        public Nullable<long> trans_num { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public Nullable<decimal> trans_sum { get; set; }
        public short trans_kind { get; set; }
        public Nullable<int> status { get; set; }
        public Nullable<long> canceled_trans_id { get; set; }
        public Nullable<long> programm_result_id { get; set; }
        public string user_name { get; set; }
        public Nullable<System.DateTime> date_check { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? curr_card_sum { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? curr_card_percent_value { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? curr_card_bonus_value { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? trans_card_percent_value { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? trans_card_bonus_value { get; set; }
        public string org_name { get; set; }
        public string card_num2 { get; set; }
        public Nullable<System.DateTime> date_check_date_only { get; set; }
        public string curr_card_type_name { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? trans_sum_discount { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? trans_sum_with_discount { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? trans_sum_bonus_for_card { get; set; }
        [DisplayFormat(DataFormatString = "{0:N}", ApplyFormatInEditMode = true)]
        public decimal? trans_sum_bonus_for_pay { get; set; }
        public Nullable<long> business_id { get; set; }
        public Nullable<System.DateTime> status_date { get; set; }
        public Nullable<short> is_delayed { get; set; }
        public Nullable<long> batch_id { get; set; }
        [JsonIgnore()]
        public string card_id_str { get { return card_id.ToString(); } }
        [JsonIgnore()]
        public string card_trans_id_str { get { return card_trans_id.ToString(); } }
        [JsonIgnore()]
        public string card_num_str { get { return card_num.ToString(); } }
        [JsonIgnore()]
        public IEnumerable<CardTransactionDetailViewModel> card_trans_detail_list { get; set; }
    }
}