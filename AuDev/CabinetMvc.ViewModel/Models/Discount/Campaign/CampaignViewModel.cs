﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Discount
{
    public class CampaignViewModel : AuBaseViewModel
    {
        public CampaignViewModel()
            : base(Enums.MainObjectType.CAMPAIGN)
        {     
            //campaign
            //vw_campaign
        }

        // Db

        [Key()]
        [Display(Name = "Код")]
        public int campaign_id { get; set; }

        [Display(Name = "Организация")]
        public long business_id { get; set; }

        [Display(Name = "Название акции")]
        [Required(ErrorMessage = "Не задано название акции")]
        public string campaign_name { get; set; }

        [Display(Name = "Описание акции")]
        [Required(ErrorMessage = "Не задано описание акции")]
        public string campaign_descr { get; set; }

        [Display(Name = "Текст на чеке")]
        public string campaign_check_text { get; set; }

        [Display(Name = "Дата начала")]
        [Required(ErrorMessage = "Не задана дата начала")]
        public System.DateTime date_beg { get; set; }

        [Display(Name = "Дата окончания")]
        [Required(ErrorMessage = "Не задана дата окончания")]
        public System.DateTime date_end { get; set; }

        [Display(Name = "Максимальное количество купонов")]
        public Nullable<int> max_card_cnt { get; set; }

        [Display(Name = "Тип карт для купонов")]
        public Nullable<long> card_type_id { get; set; }

        [Display(Name = "Создать купоны автоматически")]
        public bool auto_create_card { get; set; }

        [Display(Name = "Номиналы купонов (руб.)")]
        public Nullable<decimal> card_money_value { get; set; }

        [Display(Name = "Статус")]
        public int state { get; set; }

        [Display(Name = "Комментарий")]
        public string mess { get; set; }

        [Display(Name = "Дата начала выдачи купонов")]
        public System.DateTime issue_date_beg { get; set; }

        [Display(Name = "Дата окончания выдачи купонов")]
        public System.DateTime issue_date_end { get; set; }

        [Display(Name = "Правило выдачи")]
        public int issue_rule { get; set; }

        [Display(Name = "Условие выдачи")]
        public int issue_condition_type { get; set; }

        [Display(Name = "Мин. сумма покупки для выдачи")]
        public Nullable<decimal> issue_condition_sale_sum { get; set; }

        [Display(Name = "Мин. колво позиций для выдачи")]
        public Nullable<int> issue_condition_pos_count { get; set; }

        [Display(Name = "Мин. сумма покупки для использования")]
        public Nullable<decimal> apply_condition_sale_sum { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]
        public string crt_user { get; set; }
        
        [Display(Name = "Дата изменения")]
        public Nullable<System.DateTime> upd_date { get; set; }
        
        [Display(Name = "Автор изменения")]
        public string upd_user { get; set; }

        [Display(Name = "Тип карт для купонов")]
        [JsonIgnore()]
        public string card_type_name { get; set; }

        [Display(Name = "Купонов всего")]
        [JsonIgnore()]
        public Nullable<int> card_count { get; set; }

        [Display(Name = "Купнов активных")]
        [JsonIgnore()]
        public Nullable<int> active_card_count { get; set; }

        // Other

        [Display(Name = "Статус")]
        public string state_name { get { return state.EnumName<Enums.CampaignStateEnum>(); } }

        [Display(Name = "org_checked_ids")]
        [JsonIgnore()]
        public string org_checked_ids { get; set; }

    }
}