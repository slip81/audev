﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper.QueryableExtensions;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICampaignBusinessOrgService : IAuBaseService
    {        
        IQueryable<CampaignBusinessOrgViewModel> GetList_New(long business_id);
    }

    public class CampaignBusinessOrgService : AuBaseService, ICampaignBusinessOrgService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_campaign_business_org.Where(ss => ss.campaign_id == parent_id)
                //.ProjectTo<CampaignBusinessOrgViewModel>()
                .ToList()
                .Select(ss => new CampaignBusinessOrgViewModel()
                {
                    campaign_id = ss.campaign_id,
                    //item_id = (int)ss.item_id,
                    org_id = ss.org_id,
                    business_id = ss.business_id,
                    is_in_campaign = ss.is_in_campaign,
                    org_name = ss.org_name,
                    org_id_str = ss.org_id.ToString(),
                })
                .AsQueryable()
                ;
        }
        
        public IQueryable<CampaignBusinessOrgViewModel> GetList_New(long business_id)
        {
            return dbContext.business_org.Where(ss => ss.business_id == business_id)
                .ToList()
                .Select(ss => new CampaignBusinessOrgViewModel()
                {
                    campaign_id = 0,
                    //item_id = 0,
                    org_id = ss.org_id,
                    business_id = business_id,
                    is_in_campaign = true,
                    org_name = ss.org_name,
                    org_id_str = ss.org_id.ToString(),
                })
                .AsQueryable();
        }

    }
}