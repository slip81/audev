﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Discount
{
    public class CampaignBusinessOrgViewModel : AuBaseViewModel
    {
        public CampaignBusinessOrgViewModel()
            : base(Enums.MainObjectType.CAMPAIGN_BUSINESS_ORG)
        {     
            //vw_campaign_business_org
        }

        // Db

        //[Key()]
        //[Display(Name = "Код")]
        //public int item_id { get; set; }

        [Display(Name = "Акция")]
        public int campaign_id { get; set; }

        [Display(Name = "Отделение")]
        public long org_id { get; set; }

        [Display(Name = "Организация")]
        public long business_id { get; set; }

        [Display(Name = "Отделение")]
        public string org_name { get; set; }

        [Display(Name = "Служебный номер карты")]
        public string card_num_default { get; set; }

        [Display(Name = "В акции")]
        public bool is_in_campaign { get; set; }

        // Other

        [Display(Name = "Отделение")]
        public string org_id_str { get; set; }

    }
}