﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;
using AutoMapper.QueryableExtensions;
using Newtonsoft.Json;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ICampaignService : IAuBaseService
    {
        bool CheckDates(int campaign_id, DateTime? date_beg, DateTime? date_end);
    }

    public class CampaignService : AuBaseService, ICampaignService
    {
        //const long programm_id_BONUS_COUPON = 1406336154831161159;
        //const long programm_id_BONUS_COUPON_VIRTUAL = 1406336154831161159;
        const long programm_id_BONUS_COUPON = 1470094196294550849;
        const long programm_id_BONUS_COUPON_VIRTUAL = 1479722763718493210;

        const int param_num_MIN_SALE_SUM = 10;

        IProgrammService programmService;
        ICardTypeService cardTypeService;

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_campaign.Where(ss => ss.business_id == parent_id).ProjectTo<CampaignViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.campaign.Where(ss => ss.campaign_id == id).ProjectTo<CampaignViewModel>().FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CampaignViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CampaignViewModel itemAdd = (CampaignViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            campaign existing_campaign = dbContext.campaign
                .Where(ss => ss.business_id == itemAdd.business_id
                    && (
                    (itemAdd.date_beg >= ss.date_beg && itemAdd.date_beg <= ss.date_end) 
                    ||
                    (itemAdd.date_end >= ss.date_beg && itemAdd.date_end <= ss.date_end)
                    ||
                    (itemAdd.date_beg < ss.date_beg && itemAdd.date_end > ss.date_end)
                    )
                )
                .FirstOrDefault();
            if (existing_campaign != null)
            {
                modelState.AddModelError("", "Уже есть акция с кодом " + existing_campaign.campaign_id.ToString() + " на заданный период");
                return null;
            }

            if (String.IsNullOrEmpty(itemAdd.org_checked_ids))
            {
                modelState.AddModelError("", "Не заданы отделения для участия в акции");
                return null;
            }
            
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            
            itemAdd.crt_user = currUser.UserName;
            itemAdd.crt_date = now;        

            campaign campaign = new campaign();
            ModelMapper.Map<CampaignViewModel, campaign>(itemAdd, campaign);

            campaign.state = (int)Enums.CampaignStateEnum.PREPARING;
            //campaign.card_num_default = campaign.issue_rule == (int)Enums.CampaignIssueRuleEnum.IN_SALE_FOR_THIS_SALE ? Guid.NewGuid().ToString() : null;

            dbContext.campaign.Add(campaign);

            List<long> orgIdList = itemAdd.org_checked_ids.Split(',').Select(long.Parse).ToList();
            if (orgIdList != null)
            {
                foreach (var orgId in orgIdList)
                {
                    campaign_business_org campaign_business_org = new campaign_business_org();
                    campaign_business_org.campaign = campaign;
                    campaign_business_org.org_id = orgId;
                    campaign_business_org.card_num_default = campaign.issue_rule == (int)Enums.CampaignIssueRuleEnum.IN_SALE_FOR_THIS_SALE ? Guid.NewGuid().ToString() : null;
                    dbContext.campaign_business_org.Add(campaign_business_org);
                }
            }

            dbContext.SaveChanges();

            CampaignViewModel res = new CampaignViewModel();
            ModelMapper.Map<campaign, CampaignViewModel>(campaign, res);

            Task campaignBatch = new Task(() =>
            {
                xxInsert(campaign.campaign_id, modelState);
            }
            );
            campaignBatch.Start();

            return res;
        }

        private void xxInsert(int campaign_id, ModelStateDictionary modelState)
        {
            using (var context = new AuMainDb())
            {
                campaign campaign = context.campaign.Where(ss => ss.campaign_id == campaign_id).FirstOrDefault();
                if (campaign == null)
                {
                    ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(new CampaignViewModel()), null, "Не найдена акция с кодом " + campaign_id.ToString());
                    return;
                }
                CampaignViewModel campaignViewModel = new CampaignViewModel();
                ModelMapper.Map<campaign, CampaignViewModel>(campaign, campaignViewModel);

                DateTime now = DateTime.Now;
                DateTime today = DateTime.Today;
                int cnt = 1;
                string errMess = "";
                try
                {                    
                    if (programmService == null)
                        programmService = DependencyResolver.Current.GetService<IProgrammService>();
                    long source_programm_id = campaign.issue_rule == (int)Enums.CampaignIssueRuleEnum.IN_SALE_FOR_THIS_SALE ? programm_id_BONUS_COUPON_VIRTUAL : programm_id_BONUS_COUPON;
                    long programm_id = programmService.CopyProgramm2(source_programm_id, campaign.business_id, campaign.campaign_name, campaign.campaign_descr, campaign.campaign_descr, modelState);
                    programm programm = context.programm.Where(ss => ss.programm_id == programm_id).FirstOrDefault();
                    if ((programm_id <= 0) || (programm == null) || (!modelState.IsValid))
                    {
                        errMess = "Ошибка при копировании алгоритма";
                        campaign.state = (int)Enums.CampaignStateEnum.ERROR;
                        campaign.upd_date = now;
                        campaign.upd_user = currUser.UserName;
                        campaign.mess = "[" + cnt.ToString() + "] " + errMess;
                        context.SaveChanges();
                        ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(campaignViewModel), null, errMess);
                        return;
                    }

                    List<programm_param> programm_param_list = context.programm_param.Where(ss => ss.programm_id == programm.programm_id && ss.is_internal != 1).ToList();
                    if (programm_param_list != null)
                    {
                        programm_param programm_param_MIN_SALE_SUM = programm_param_list.Where(ss => ss.param_num == param_num_MIN_SALE_SUM).FirstOrDefault();
                        if (programm_param_MIN_SALE_SUM != null)
                        {
                            programm_param_MIN_SALE_SUM.float_value = campaign.apply_condition_sale_sum.GetValueOrDefault(0);
                        }
                    }

                    if (!campaign.card_type_id.HasValue)
                    {
                        cnt = 2;
                        if (cardTypeService == null)
                            cardTypeService = DependencyResolver.Current.GetService<ICardTypeService>();
                        CardTypeViewModel cardType_forCreate = new CardTypeViewModel()
                        {
                            business_id = campaign.business_id,
                            card_type_group_id = (int)Enums.CardTypeGroup.BONUS,
                            card_type_name = campaign.campaign_name,
                            is_active = true,
                            is_fin_spec = 0,
                            programm_id = programm_id,
                            update_nominal_bool = false,
                        };

                        cnt = 3;
                        CardTypeViewModel cardType = (CardTypeViewModel)cardTypeService.Insert(cardType_forCreate, modelState);
                        if ((cardType == null) || (!modelState.IsValid))
                        {
                            errMess = "Ошибка при создании типа карт";
                            campaign.state = (int)Enums.CampaignStateEnum.ERROR;
                            campaign.upd_date = now;
                            campaign.upd_user = currUser.UserName;
                            campaign.mess = "[" + cnt.ToString() + "] " + errMess;
                            context.SaveChanges();
                            ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(campaignViewModel), null, errMess);
                            return;
                        }                        
                        campaign.card_type_id = cardType.card_type_id;

                        // удалаем ненужные card_type_business_org
                        var org_list = context.campaign_business_org.Where(ss => ss.campaign_id == campaign.campaign_id).Select(ss => ss.org_id).ToList();
                        context.card_type_business_org.RemoveRange(context.card_type_business_org.Where(ss => ss.card_type_id == campaign.card_type_id && !org_list.Contains(ss.business_org_id)));
                    }
                    else
                    {
                        cnt = 4;
                        card_type_programm_rel card_type_programm_existing = context.card_type_programm_rel
                            .Where(ss => ss.card_type_id == campaign.card_type_id && !ss.date_end.HasValue)
                            .FirstOrDefault();
                        if (card_type_programm_existing != null)
                            card_type_programm_existing.date_end = today;
                        card_type_programm_rel card_type_programm_rel = new card_type_programm_rel();
                        card_type_programm_rel.card_type_id = (long)campaign.card_type_id;
                        card_type_programm_rel.date_beg = today;
                        card_type_programm_rel.date_end = null;
                        card_type_programm_rel.programm_id = programm_id;
                        context.card_type_programm_rel.Add(card_type_programm_rel);
                    }

                    card card = null;
                    card_card_status_rel card_card_status_rel = null;
                    card_card_type_rel card_card_type_rel = null;
                    card_nominal card_nominal = null;
                    
                    long card_num_tmp = 111222333444;
                    long curr_card_status_id = 2;
                    long curr_card_type_id = (long)campaign.card_type_id;
                    DateTime curr_date_beg = campaign.date_beg;
                    DateTime curr_date_end = campaign.date_end;
                    decimal? curr_card_money_value = campaign.card_money_value;

                    AuMainDb transactionContext = null;
                    if ((campaign.auto_create_card) && (campaign.max_card_cnt.GetValueOrDefault(0) > 0)
                        && ((campaign.issue_rule == (int)Enums.CampaignIssueRuleEnum.BEFORE) || (campaign.issue_rule == (int)Enums.CampaignIssueRuleEnum.IN_SALE_FOR_NEXT_SALE))
                        )
                    {
                        if (campaign.max_card_cnt.GetValueOrDefault(0) > 10000)
                        {
                            errMess = "Превышено максимально допустимое количестов купонов (10000)";
                            campaign.state = (int)Enums.CampaignStateEnum.ERROR;
                            campaign.upd_date = now;
                            campaign.upd_user = currUser.UserName;
                            campaign.mess = "[" + cnt.ToString() + "] " + errMess;
                            context.SaveChanges();
                            ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(campaignViewModel), null, errMess);
                            return;
                        }
                        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                        {
                            transactionContext = null;
                            try
                            {
                                transactionContext = new AuMainDb();
                                transactionContext.Configuration.AutoDetectChangesEnabled = false;

                                cnt = 0;
                                for (int i = 1; i <= campaign.max_card_cnt.GetValueOrDefault(0); i++)
                                {                                    
                                    ++cnt;
                                    card = new card()
                                    {
                                        business_id = campaign.business_id,
                                        card_num = card_num_tmp,
                                        card_num2 = cnt.ToString(),
                                        crt_date = now,
                                        crt_user = currUser.CabUserLogin,
                                        curr_card_status_id = curr_card_status_id,
                                        curr_card_type_id = curr_card_type_id,
                                        curr_trans_count = 0,
                                        curr_trans_sum = 0,
                                        date_beg = curr_date_beg,
                                        date_end = curr_date_end,
                                        from_au = false,                                        
                                    };
                                    card_card_status_rel = new card_card_status_rel()
                                    {
                                        card = card,
                                        card_status_id = curr_card_status_id,
                                        date_beg = curr_date_beg,
                                        date_end = null,
                                    };
                                    card_card_type_rel = new card_card_type_rel()
                                    {
                                        card = card,
                                        card_type_id = curr_card_type_id,
                                        date_beg = curr_date_beg,
                                        date_end = null,                                        
                                    };
                                    card_nominal = new card_nominal()
                                    {
                                        card = card,
                                        crt_date = now,
                                        crt_user = currUser.CabUserLogin,
                                        date_beg = curr_date_beg,
                                        date_end = null,
                                        unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE,
                                        unit_value = curr_card_money_value,
                                    };

                                    // !!!
                                    transactionContext = DbContextUtil.AddToContext_4<card, card_card_status_rel, card_card_type_rel, card_nominal>(
                                        transactionContext, 
                                        card, 
                                        card_card_status_rel,
                                        card_card_type_rel,
                                        card_nominal,
                                        cnt, 
                                        100, 
                                        true
                                    );

                                }
                                transactionContext.SaveChanges();

                            }
                            finally
                            {
                                if (transactionContext != null)
                                    transactionContext.Dispose();
                            }

                            scope.Complete();
                        }

                        int res1 = context.Database.ExecuteSqlCommand("update discount.card set card_num = card_id where business_id = @p0 and card_num = @p1", campaign.business_id, card_num_tmp);
                    }
                    else if (campaign.issue_rule == (int)Enums.CampaignIssueRuleEnum.IN_SALE_FOR_THIS_SALE)
                    {
                        List<campaign_business_org> campaign_business_org_list = context.campaign_business_org.Where(ss => ss.campaign_id == campaign.campaign_id).ToList();
                        if (campaign_business_org_list != null)
                        {
                            foreach (var campaign_business_org in campaign_business_org_list)
                            {
                                card = new card()
                                {
                                    business_id = campaign.business_id,
                                    card_num = card_num_tmp,
                                    card_num2 = campaign_business_org.card_num_default,
                                    crt_date = now,
                                    crt_user = currUser.CabUserLogin,
                                    curr_card_status_id = curr_card_status_id,
                                    curr_card_type_id = curr_card_type_id,
                                    curr_trans_count = 0,
                                    curr_trans_sum = 0,
                                    date_beg = curr_date_beg,
                                    date_end = curr_date_end,
                                    from_au = false,
                                };
                                card_card_status_rel = new card_card_status_rel()
                                {
                                    card = card,
                                    card_status_id = curr_card_status_id,
                                    date_beg = curr_date_beg,
                                    date_end = null,
                                };
                                card_card_type_rel = new card_card_type_rel()
                                {
                                    card = card,
                                    card_type_id = curr_card_type_id,
                                    date_beg = curr_date_beg,
                                    date_end = null,
                                };
                                card_nominal = new card_nominal()
                                {
                                    card = card,
                                    crt_date = now,
                                    crt_user = currUser.CabUserLogin,
                                    date_beg = curr_date_beg,
                                    date_end = null,
                                    unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE,
                                    unit_value = curr_card_money_value,
                                };
                                context.card.Add(card);
                                context.card_card_status_rel.Add(card_card_status_rel);
                                context.card_card_type_rel.Add(card_card_type_rel);
                                context.card_nominal.Add(card_nominal);
                            }
                            
                            context.SaveChanges();
                            int res2 = context.Database.ExecuteSqlCommand("update discount.card set card_num = card_id where business_id = @p0 and card_num = @p1", campaign.business_id, card_num_tmp);
                        }
                    }

                    cnt = 5;
                    bool is_active = ((campaign.date_beg <= today) && (campaign.date_end >= today));
                    campaign.state = is_active ? (int)Enums.CampaignStateEnum.ACTIVE : (int)Enums.CampaignStateEnum.READY;
                    campaign.upd_date = now;
                    campaign.upd_user = currUser.UserName;
                    campaign.mess = "";

                    cnt = 6;

                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    errMess = GlobalUtil.ExceptionInfo(ex);
                    campaign.state = (int)Enums.CampaignStateEnum.ERROR;
                    campaign.upd_date = now;
                    campaign.upd_user = currUser.UserName;
                    campaign.mess = "[" + cnt.ToString() + "] " + errMess;
                    context.SaveChanges();
                    ToLog("Ошибка при добавлении", (long)Enums.LogEventType.CABINET, new LogObject(campaignViewModel), null, errMess);
                }
            }
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CampaignViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            CampaignViewModel itemEdit = (CampaignViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            campaign campaign = dbContext.campaign.Where(ss => ss.campaign_id == itemEdit.campaign_id).FirstOrDefault();
            if (campaign == null)
            {
                modelState.AddModelError("", "Не найдена акция с кодом " + itemEdit.campaign_id.ToString());
                return null;
            }

            if (String.IsNullOrEmpty(itemEdit.org_checked_ids))
            {
                modelState.AddModelError("", "Не заданы отделения для участия в акции");
                return null;
            }

            CampaignViewModel oldModel = new CampaignViewModel();
            ModelMapper.Map<campaign, CampaignViewModel>(campaign, oldModel);
            modelBeforeChanges = oldModel;

            DateTime now = DateTime.Now;           
            
            //ModelMapper.Map<CampaignViewModel, campaign>(itemEdit, campaign);

            List<long> orgIdList = itemEdit.org_checked_ids.Split(',').Select(long.Parse).ToList();
            if (orgIdList != null)
            {
                foreach (var orgId in orgIdList)
                {
                    campaign_business_org campaign_business_org = dbContext.campaign_business_org.Where(ss => ss.campaign_id == itemEdit.campaign_id && ss.org_id == orgId).FirstOrDefault();
                    if (campaign_business_org != null)
                        continue;
                    campaign_business_org = new campaign_business_org();
                    campaign_business_org.campaign = campaign;
                    campaign_business_org.org_id = orgId;
                    campaign_business_org.card_num_default = campaign.issue_rule == (int)Enums.CampaignIssueRuleEnum.IN_SALE_FOR_THIS_SALE ? Guid.NewGuid().ToString() : null;
                    dbContext.campaign_business_org.Add(campaign_business_org);
                    card_type_business_org card_type_business_org = new card_type_business_org();
                    card_type_business_org.card_type_id = (long)campaign.card_type_id;
                    card_type_business_org.business_org_id = orgId;
                    card_type_business_org.allow_discount = true;
                    card_type_business_org.allow_bonus_for_pay = true;
                    card_type_business_org.allow_bonus_for_card = true;
                    dbContext.card_type_business_org.Add(card_type_business_org);
                }

                dbContext.campaign_business_org.RemoveRange(dbContext.campaign_business_org.Where(ss => ss.campaign_id == itemEdit.campaign_id && !orgIdList.Contains(ss.org_id)));
                dbContext.card_type_business_org.RemoveRange(dbContext.card_type_business_org.Where(ss => ss.card_type_id == campaign.card_type_id && !orgIdList.Contains(ss.business_org_id)));
            }

            campaign.apply_condition_sale_sum = itemEdit.apply_condition_sale_sum;
            campaign.campaign_check_text = itemEdit.campaign_check_text;
            campaign.campaign_descr = itemEdit.campaign_descr;
            campaign.campaign_name = itemEdit.campaign_name;
            campaign.date_beg = itemEdit.date_beg;
            campaign.date_end = itemEdit.date_end;
            campaign.issue_condition_pos_count = itemEdit.issue_condition_pos_count;
            campaign.issue_condition_sale_sum = itemEdit.issue_condition_sale_sum;
            campaign.issue_condition_type = itemEdit.issue_condition_type;
            campaign.issue_date_beg = itemEdit.issue_date_beg;
            campaign.issue_date_end = itemEdit.issue_date_end;          
            campaign.upd_user = currUser.UserName;
            campaign.upd_date = now;
            
            dbContext.SaveChanges();

            return itemEdit;
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is CampaignViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            CampaignViewModel itemDel = (CampaignViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return false;
            }

            campaign campaign = dbContext.campaign.Where(ss => ss.campaign_id == itemDel.campaign_id).FirstOrDefault();
            if (campaign == null)
            {
                modelState.AddModelError("", "Не найдена акция с кодом " + itemDel.campaign_id.ToString());
                return false;
            }

            dbContext.campaign_business_org.RemoveRange(dbContext.campaign_business_org.Where(ss => ss.campaign_id == itemDel.campaign_id));
            dbContext.campaign.Remove(campaign);
            dbContext.SaveChanges();

            return true;
        }
        
        public bool CheckDates(int campaign_id, DateTime? date_beg, DateTime? date_end)
        {
            long business_id = currUser.BusinessId;
            campaign existing_campaign = dbContext.campaign
                    .Where(ss => ss.business_id == business_id
                        && ss.campaign_id != campaign_id
                        && (
                        (date_beg >= ss.date_beg && date_beg <= ss.date_end)
                        ||
                        (date_end >= ss.date_beg && date_end <= ss.date_end)
                        ||
                        (date_beg < ss.date_beg && date_end > ss.date_end)
                        )
                    )
                    .FirstOrDefault();
            
            return existing_campaign == null;
        }
    }
}