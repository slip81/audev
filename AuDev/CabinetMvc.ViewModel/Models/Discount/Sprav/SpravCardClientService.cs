﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ISpravCardClientService : IAuBaseService
    {
        List<SpravCardClientViewModel> GetList_byText(long business_id, string text);
    }

    public class SpravCardClientService : AuBaseService, ISpravCardClientService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.card_holder.AsEnumerable().Where(ss => ss.business_id == parent_id)
                .Select(ss => new SpravCardClientViewModel
                {
                    //client_id_str = ss.card_holder_id.ToString(),
                    client_id = ss.card_holder_id,
                    client_full_name = ((String.IsNullOrEmpty(ss.card_holder_surname) ? "[-] " : ss.card_holder_surname.Trim() + " ")
                                        + (String.IsNullOrEmpty(ss.card_holder_name) ? "" : ss.card_holder_name.Trim() + " ")
                                        + (String.IsNullOrEmpty(ss.card_holder_fname) ? "" : ss.card_holder_fname.Trim()))
                                        .Trim(),
                })
                .AsQueryable()
                .OrderBy(ss => ss.client_full_name);
        }

        public List<SpravCardClientViewModel> GetList_byText(long business_id, string text)
        {
            if (String.IsNullOrWhiteSpace(text))
                return null;
            return dbContext.vw_card_holder
                .Where(ss => ss.business_id == business_id
                    && ss.card_holder_surname.Trim().ToLower().StartsWith(text.Trim().ToLower())
                    )
                .Select(ss => new SpravCardClientViewModel
                {
                    client_id = ss.card_holder_id,
                    client_full_name = ss.card_holder_full_name,
                })
                .OrderBy(ss => ss.client_full_name)
                .ToList()
                ;
            /*
            return dbContext.card_holder.AsEnumerable().Where(ss => ss.business_id == business_id
                && ss.card_holder_surname.Trim().ToLower().StartsWith(text.Trim().ToLower())
                )
                .Select(ss => new SpravCardClientViewModel
                {
                    client_id_str = ss.card_holder_id.ToString(),
                    client_full_name = ((String.IsNullOrEmpty(ss.card_holder_surname) ? "[-] " : ss.card_holder_surname.Trim() + " ")
                                        + (String.IsNullOrEmpty(ss.card_holder_name) ? "" : ss.card_holder_name.Trim() + " ")
                                        + (String.IsNullOrEmpty(ss.card_holder_fname) ? "" : ss.card_holder_fname.Trim()))
                                        .Trim(),
                })
                .AsQueryable()
                .OrderBy(ss => ss.client_full_name);
            */
        }
    }
}
