﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ISpravProgrammTemplateService : IAuBaseService
    {
        //
    }

    public class SpravProgrammTemplateService : AuBaseService, ISpravProgrammTemplateService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.programm_template.AsEnumerable()
                .Select(ss => new SpravProgrammTemplateViewModel
                {
                    template_id_str = ss.template_id.ToString(),
                    template_name = ss.template_name,
                    template_descr = ss.programm1.descr_full,
                })
                .AsQueryable()
                .OrderBy(ss => ss.template_name);
        }
    }
}
