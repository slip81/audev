﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Discount
{
    public class SpravProgrammViewModel : AuBaseViewModel
    {
        public SpravProgrammViewModel()
            :base()
        {
            //
        }

        public string programm_id_str { get; set; }
        public string programm_name { get; set; }
        public string programm_descr { get; set; }
    }
}