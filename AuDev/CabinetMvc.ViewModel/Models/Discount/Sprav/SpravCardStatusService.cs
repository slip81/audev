﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ISpravCardStatusService : IAuBaseService
    {
        //
    }

    public class SpravCardStatusService : AuBaseService, ISpravCardStatusService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.card_status.Select(ss => new SpravCardStatusViewModel()
            {
                card_status_id = ss.card_status_id,                
                card_status_name = ss.card_status_name,
                no_manual_change = ss.no_manual_change,
            }
            );
        }
    }
}