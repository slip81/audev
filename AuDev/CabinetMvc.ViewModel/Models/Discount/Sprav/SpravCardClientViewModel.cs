﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Discount
{
    public class SpravCardClientViewModel : AuBaseViewModel
    {
        public SpravCardClientViewModel()
            :base()
        {
            //
        }

        public long client_id { get; set; }
        //public string client_id_str { get; set; }
        public string client_id_str { get { return client_id.ToString(); } }
        public string client_full_name { get; set; }
    }

}