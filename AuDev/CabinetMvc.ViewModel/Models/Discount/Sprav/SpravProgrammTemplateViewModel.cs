﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Discount
{
    public class SpravProgrammTemplateViewModel : AuBaseViewModel
    {
        public SpravProgrammTemplateViewModel()
            :base()
        {
            //
        }

        public string template_id_str { get; set; }
        public string template_name { get; set; }
        public string template_descr { get; set; }            
    }
}