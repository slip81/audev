﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Discount
{
    public class SpravCardStatusViewModel : AuBaseViewModel
    {
        public SpravCardStatusViewModel()
            :base()
        {
            //
        }

        public long card_status_id { get; set; }
        public string card_status_name { get; set; }
        public bool no_manual_change { get; set; }
    }
}