﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ISpravCardTypeService : IAuBaseService
    {
        IQueryable<SpravCardTypeViewModel> GetCardTypeList_forCampaign(int campaign_id);
    }

    public class SpravCardTypeService : AuBaseService, ISpravCardTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.card_type.Where(ss => ss.business_id == parent_id).Select(ss => new SpravCardTypeViewModel()
            {
                card_type_id = ss.card_type_id,
                card_type_name = ss.card_type_name,
                card_type_group_id = ss.card_type_group_id,
                business_id = ss.business_id,
            }
            );
        }

        public IQueryable<SpravCardTypeViewModel> GetCardTypeList_forCampaign(int campaign_id)
        {
            return dbContext.card_type.Where(ss => ss.business_id == currUser.BusinessId 
                && ss.card_type_group_id == (int)Enums.CardTypeGroup.BONUS
                && !dbContext.campaign.Where(tt => tt.campaign_id != campaign_id && tt.card_type_id == ss.card_type_id).Any()
                )
                .Select(ss => new SpravCardTypeViewModel()
            {
                card_type_id = ss.card_type_id,
                card_type_name = ss.card_type_name,
            }
            );
        }
    }
}