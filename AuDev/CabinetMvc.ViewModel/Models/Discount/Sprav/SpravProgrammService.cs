﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Discount
{
    public interface ISpravProgrammService : IAuBaseService
    {
        //
    }

    public class SpravProgrammService : AuBaseService, ISpravProgrammService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.programm.AsEnumerable().Where(ss => ss.business_id == parent_id)
                .Select(ss => new SpravProgrammViewModel
                {
                    programm_id_str = ss.programm_id.ToString(),
                    programm_name = ss.programm_name,
                    programm_descr = ss.descr_full,
                })
                .AsQueryable()
                .OrderBy(ss => ss.programm_name);
        }
    }
}
