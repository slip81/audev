﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel.Discount
{
    public interface IBusinessSummaryService : IAuBaseService
    {
        //
    }

    public class BusinessSummaryService : AuBaseService, IBusinessSummaryService
    {
        protected override AuBaseViewModel xGetItem(long id)
        {
            BusinessSummaryViewModel res = new BusinessSummaryViewModel();
            
            long business_id = id;
            DateTime now = DateTime.Now;
            DateTime last_month_start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1, 0, 00, 00);
            DateTime last_day_start = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 0, 00, 00);
            DateTime prev_day_start = last_day_start.AddDays(-1);

            #region BusinessSummary

            var cnt1 = dbContext.card.Where(ss => ss.business_id == business_id).Count();
            var cnt2 = (from cl in dbContext.card_holder
                        from crel in dbContext.card_holder_card_rel
                        where cl.business_id == business_id
                        && cl.card_holder_id == crel.card_holder_id
                        && !crel.date_end.HasValue
                        select new { client_id = crel.card_holder_id, card_id = crel.card_id })
                       .Distinct()
                       .Count();
            var cnt3 = dbContext.card.Where(ss => ss.business_id == business_id && ss.date_beg >= last_month_start).Count();
            var cnt4 = dbContext.card.Where(ss => ss.business_id == business_id && ss.date_beg >= last_day_start).Count();

            res.business_id = business_id;
            res.business_name = currUser.BusinessName;
            res.date_cut = now;
            res.card_cnt = cnt1;
            res.card_cnt_last_month = cnt3;
            res.card_cnt_last_day = cnt4;
            res.client_cnt = cnt2;

            #endregion

            #region CardTypeSummary

            var card_type_info = (from c in dbContext.card
                                  from crel in dbContext.card_card_type_rel
                                  from ct in dbContext.card_type
                                  where c.business_id == business_id
                                  && c.card_id == crel.card_id
                                  && !crel.date_end.HasValue
                                  && crel.card_type_id == ct.card_type_id
                                  select new { crel.card_id, ct.card_type_id, ct.card_type_name }
                                 )
                                 .GroupBy(ss => new { ss.card_type_id, ss.card_type_name })
                                 .Select(group => new { card_type_id = group.Key.card_type_id, card_type_name = group.Key.card_type_name, card_cnt = group.Count(), })
                                 .OrderBy(ss => ss.card_type_name)
                                 ;

            res.CardTypeSummary_list = card_type_info.Select(summary => new CardTypeSummaryViewModel
            {
                business_id = business_id,
                card_type_id = summary.card_type_id,
                card_type_name = summary.card_type_name,
                card_cnt = summary.card_cnt,
            }).ToList();

            #endregion

            #region CardStatusSummary

            var card_status_info = (from c in dbContext.card
                                    from crel in dbContext.card_card_status_rel
                                    from ct in dbContext.card_status
                                    where c.business_id == business_id
                                    && c.card_id == crel.card_id
                                    && !crel.date_end.HasValue
                                    && crel.card_status_id == ct.card_status_id
                                    select new { crel.card_id, ct.card_status_id, ct.card_status_name }
                 )
                 .GroupBy(ss => new { ss.card_status_id, ss.card_status_name })
                 .Select(group => new { card_status_id = group.Key.card_status_id, card_status_name = group.Key.card_status_name, card_cnt = group.Count(), })
                 .OrderBy(ss => ss.card_status_name)
                 ;

            res.CardStatusSummary_list = card_status_info.Select(summary => new CardStatusSummaryViewModel
            {
                business_id = business_id,
                card_status_id = summary.card_status_id,
                card_status_name = summary.card_status_name,
                card_cnt = summary.card_cnt,
            }).ToList();

            #endregion

            #region BusinessOrg

            var business_orgs = dbContext.business_org.Where(ss => ss.business_id == business_id);

            res.BusinessOrg_list = business_orgs.Select(item => new BusinessOrgViewModel
            {
                business_id = item.business_id,
                business_name = currUser.BusinessName,
                add_to_timezone = item.add_to_timezone.HasValue ? item.add_to_timezone + 2 : 2,
                IsCardScanOnly = item.card_scan_only == 1 ? true : false,
                IsScannerEqualsReader = item.scanner_equals_reader == 1 ? true : false,
                allow_card_add = item.allow_card_add,
                org_id = item.org_id,
                org_name = item.org_name,
                //user_name = (item.business_org_user.FirstOrDefault() != null) ? item.business_org_user.FirstOrDefault().user_name : "",
            }).ToList();

            #endregion

            #region SaleSummary

            res.SaleSummary_list = dbContext.sale_summary.Where(ss => ss.business_id == business_id).Select(item => new SaleSummaryViewModel
            {
                sale_sum = item.sale_sum,
                sale_sum_discount = item.sale_sum_discount,
                sale_sum_with_discount = item.sale_sum_with_discount,
                bonus_for_card_sum = item.bonus_for_card_sum,
                bonus_for_pay_sum = item.bonus_for_pay_sum,
                sale_cnt = item.sale_cnt,
                business_id = item.business_id,
                summary_type = item.summary_type,
            }).ToList();

            #endregion

            return res;

        }

    }
}