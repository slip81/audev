﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardStatusSummaryViewModel : AuBaseViewModel
    {
        public CardStatusSummaryViewModel()
            :base()
        {
            //
        }

        public long business_id { get; set; }
        public long card_status_id { get; set; }
        public string card_status_name { get; set; }
        public long card_cnt { get; set; }
    }
}