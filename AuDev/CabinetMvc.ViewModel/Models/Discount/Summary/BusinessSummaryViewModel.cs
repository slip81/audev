﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Discount
{
    public class BusinessSummaryViewModel : AuBaseViewModel
    {
        public BusinessSummaryViewModel()
            :base()
        {
            //
        }

        public long business_id { get; set; }        
        public string business_name { get; set; }        
        public long card_cnt { get; set; }        
        public long card_cnt_last_month { get; set; }
        public long card_cnt_last_day { get; set; }
        public long client_cnt { get; set; }
        public DateTime date_cut { get; set; }

        // children        
        public IEnumerable<CardTypeSummaryViewModel> CardTypeSummary_list { get; set; }
        public IEnumerable<CardStatusSummaryViewModel> CardStatusSummary_list { get; set; }
        public IEnumerable<BusinessOrgViewModel> BusinessOrg_list { get; set; }        
        public IEnumerable<SaleSummaryViewModel> SaleSummary_list { get; set; }
        //public IEnumerable<CardTransactionViewModel> LastTransaction_list { get; set; }        
    }
}