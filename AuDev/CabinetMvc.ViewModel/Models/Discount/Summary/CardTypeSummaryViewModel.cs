﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CabinetMvc.ViewModel.Discount
{
    public class CardTypeSummaryViewModel : AuBaseViewModel
    {
        public CardTypeSummaryViewModel()
            :base()
        {
            //
        }

        public long business_id { get; set; }        
        public long card_type_id { get; set; }        
        public string card_type_name { get; set; }        
        public long card_cnt { get; set; }
    }
}