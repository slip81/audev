﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Discount
{
    public class SaleSummaryViewModel : AuBaseViewModel
    {
        public SaleSummaryViewModel()
            : base(Enums.MainObjectType.SALE_SUMMARY)
        {
            //
        }

        public SaleSummaryViewModel(sale_summary item)
            : base(Enums.MainObjectType.SALE_SUMMARY)
        {
            sale_sum = item.sale_sum;
            sale_sum_discount = item.sale_sum_discount;
            sale_sum_with_discount = item.sale_sum_with_discount;
            bonus_for_card_sum = item.bonus_for_card_sum;
            bonus_for_pay_sum = item.bonus_for_pay_sum;
            sale_cnt = item.sale_cnt;
            business_id = item.business_id;
            summary_type = item.summary_type;                                              
        }

        public long business_id { get; set; }
        public int summary_type { get; set; }        
        public int sale_cnt { get; set; }
        public decimal? sale_sum { get; set; }
        public decimal? sale_sum_discount { get; set; }
        public decimal? sale_sum_with_discount { get; set; }
        public decimal? bonus_for_card_sum { get; set; }
        public decimal? bonus_for_pay_sum { get; set; }

        [JsonIgnore()]
        public string summary_type_name 
        {
            get
            {
                switch (summary_type)
                {
                    case 1:
                        return "за все время";
                    case 2:
                        return "за последний месяц";
                    case 3:
                        return "за вчерашний день";
                    default:
                        return "";
                }
            }
        }
    }
}