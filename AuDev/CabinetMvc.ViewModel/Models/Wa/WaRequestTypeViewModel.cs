﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Wa
{
    public class WaRequestTypeViewModel : AuBaseViewModel
    {

        public WaRequestTypeViewModel()
            : base(Enums.MainObjectType.WA_REQUEST_TYPE)
        {           
            //wa_request_type
        }

        public int request_type_id { get; set; }
        public string request_type_name { get; set; }
        public Nullable<int> request_type_group1_id { get; set; }
        public Nullable<int> request_type_group2_id { get; set; }
        public bool is_inner { get; set; }
        public bool is_outer { get; set; }
        public int handling_server_max_cnt { get; set; }
   }

}