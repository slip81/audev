﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Wa
{
    public class WaTaskViewModel : AuBaseViewModel
    {

        public WaTaskViewModel()
            : base(Enums.MainObjectType.WA_TASK)
        {
            //vw_wa_task
        }

        //db
        [Key()]
        [Display(Name = "Код")]
        public int task_id { get; set; }
        
        [Display(Name = "Тип запроса")]
        public int request_type_id { get; set; }

        [Display(Name = "Статус")]
        public int task_state_id { get; set; }

        [Display(Name = "Клиент")]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "ТТ")]
        public Nullable<int> sales_id { get; set; }

        [Display(Name = "РМ")]
        public Nullable<int> workplace_id { get; set; }

        [Display(Name = "№ (глобальный)")]
        public int ord { get; set; }

        [Display(Name = "№")]
        public int ord_client { get; set; }

        [Display(Name = "Дата статуса")]
        public Nullable<System.DateTime> state_date { get; set; }

        [Display(Name = "Перевел в статус")]
        public string state_user { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "Создано автоматически")]
        public bool is_auto_created { get; set; }

        [Display(Name = "Комментарий")]
        public string mess { get; set; }

        [Display(Name = "Тип запроса")]
        public string request_type_name { get; set; }
        
        [Display(Name = "Группа запроса")]
        public Nullable<int> request_type_group1_id { get; set; }

        [Display(Name = "Подгруппа запроса")]        
        public Nullable<int> request_type_group2_id { get; set; }

        [Display(Name = "Группа запроса")]
        public string request_type_group1_name { get; set; }

        [Display(Name = "Подгруппа запроса")]
        public string request_type_group2_name { get; set; }

        [Display(Name = "Статус")]
        public string task_state_name { get; set; }

        [Display(Name = "Клиент")]
        public string client_name { get; set; }

        [Display(Name = "ТТ")]
        public string sales_name { get; set; }

        [Display(Name = "РМ")]
        public string workplace_name { get; set; }

        [Display(Name = "Создано автоматически")]
        public string is_auto_created_str { get; set; }

        [Display(Name = "Внутреннее")]
        public bool is_inner { get; set; }

        [Display(Name = "Внешнее")]
        public bool is_outer { get; set; }

        [Display(Name = "Кол-во попыток выполнения на сервере")]
        public Nullable<int> handling_server_cnt { get; set; }

        [Display(Name = "Мас. кол-во попыток выполнения на сервере")]
        public int handling_server_max_cnt { get; set; }

        // additional
        [Display(Name = "Параметры задания")]
        public string task_param { get; set; }

        [Display(Name = "btnCheckState")]
        public string btnCheckState { get { return "<button class='k-button k-button-icontext' type='button' style='min-width: 30px;' title='Проверить статус обработки' onclick='asnaUserEdit.onBtnCheckStateClick(" + task_id.ToString() + ")'><span class='k-icon k-i-arrow-e'></span></button>"; } }

        [Display(Name = "btnCancel")]
        public string btnCancel { get { return "<button class='k-button k-button-icontext' type='button' style='min-width: 30px;' title='Отменить задание' onclick='stockTaskList.onBtnTaskCancelClick(" + task_id.ToString() + ")'><span class='k-icon k-i-cancel'></span></button>"; } }
    }

    public class AsnaServiceParam
    {
        public AsnaServiceParam()
        {
            //
        }

        public int asna_user_id { get; set; }
        public int task_id { get; set; }
    }

    public class AsnaServiceResult
    {
        public AsnaServiceResult()
        {
            //
        }

        public string task_state { get; set; }
    }

    public class StockServiceParam
    {
        public StockServiceParam()
        {
            //
        }

        public int sales_id { get; set; }        
    }

}