﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Wa
{
    public interface IWaRequestTypeService : IAuBaseService
    {
        IQueryable<WaRequestTypeViewModel> GetList_byGroups(int? group1_id, int? group2_id, bool is_inner_only, bool is_outer_only);
    }

    public class WaRequestTypeService : AuBaseService, IWaRequestTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.wa_request_type
                .Where(ss => ss.request_type_group1_id == parent_id)
                .ProjectTo<WaRequestTypeViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.wa_request_type
                .ProjectTo<WaRequestTypeViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.wa_request_type
                .Where(ss => ss.request_type_id == id)
                .ProjectTo<WaRequestTypeViewModel>()
                .FirstOrDefault();
        }

        public IQueryable<WaRequestTypeViewModel> GetList_byGroups(int? group1_id, int? group2_id, bool is_inner_only, bool is_outer_only)
        {
            return dbContext.wa_request_type
                .Where(ss => (((ss.request_type_group1_id == group1_id) && (group1_id.HasValue)) || (!group1_id.HasValue))
                    &&
                    (((ss.request_type_group2_id == group2_id) && (group2_id.HasValue)) || (!group2_id.HasValue))
                    &&
                    (((ss.is_inner == true) && (is_inner_only)) || (!is_inner_only))
                    &&
                    (((ss.is_outer == true) && (is_outer_only)) || (!is_outer_only))
                )
                .ProjectTo<WaRequestTypeViewModel>();
        }
        
    }
}