﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Wa
{
    public interface IWaTaskLcService : IAuBaseService
    {
        //
    }

    public class WaTaskLcService : AuBaseService, IWaTaskLcService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_wa_task_lc
                .Where(ss => ss.task_id == parent_id)
                .ProjectTo<WaTaskLcViewModel>();
        }

    }
}