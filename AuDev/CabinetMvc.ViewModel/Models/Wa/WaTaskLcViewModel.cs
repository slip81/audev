﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace CabinetMvc.ViewModel.Wa
{
    public class WaTaskLcViewModel : AuBaseViewModel
    {

        public WaTaskLcViewModel()
            : base(Enums.MainObjectType.WA_TASK_LC)
        {
            //vw_wa_task_lc
        }

        //db
        [Key()]
        [Display(Name = "Код")]
        public int task_lc_id { get; set; }

        [Display(Name = "Задание")]
        public int task_id { get; set; }

        [Display(Name = "Статус")]
        public int task_state_id { get; set; }

        [Display(Name = "Дата статуса")]
        public Nullable<System.DateTime> state_date { get; set; }

        [Display(Name = "Перевел в статус")]
        public string state_user { get; set; }

        [Display(Name = "Комментарий")]
        public string mess { get; set; }

        [Display(Name = "Создано")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Создал")]
        public string crt_user { get; set; }

        [Display(Name = "Изменено")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Изменил")]
        public string upd_user { get; set; }

        [Display(Name = "is_deleted")]
        public bool is_deleted { get; set; }

        [Display(Name = "del_date")]
        public Nullable<System.DateTime> del_date { get; set; }

        [Display(Name = "del_user")]
        public string del_user { get; set; }

        [Display(Name = "Тип запроса")]
        public int request_type_id { get; set; }

        [Display(Name = "Клиент")]
        public Nullable<int> client_id { get; set; }

        [Display(Name = "ТТ")]
        public Nullable<int> sales_id { get; set; }

        [Display(Name = "РМ")]
        public Nullable<int> workplace_id { get; set; }

        [Display(Name = "№ (глобальный)")]
        public int ord { get; set; }

        [Display(Name = "№")]
        public int ord_client { get; set; }

        [Display(Name = "Создано автоматически")]
        public bool is_auto_created { get; set; }

        [Display(Name = "Тип запроса")]
        public string request_type_name { get; set; }

        [Display(Name = "Группа запроса")]
        public Nullable<int> request_type_group1_id { get; set; }

        [Display(Name = "Подгруппа запроса")]
        public Nullable<int> request_type_group2_id { get; set; }

        [Display(Name = "Группа запроса")]
        public string request_type_group1_name { get; set; }

        [Display(Name = "Подгруппа запроса")]
        public string request_type_group2_name { get; set; }

        [Display(Name = "Статус")]
        public string task_state_name { get; set; }

        [Display(Name = "Создано автоматически")]
        public string is_auto_created_str { get; set; }

        [Display(Name = "Внутреннее")]
        public bool is_inner { get; set; }

        [Display(Name = "Внешнее")]
        public bool is_outer { get; set; }
    }

}