﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CabinetMvc.ViewModel.Wa
{
    public interface IWaTaskService : IAuBaseService
    {
        IQueryable<WaTaskViewModel> GetList_byWorkplace(int workplace_id, int group1_id);
        IQueryable<WaTaskViewModel> GetList_bySales(int sales_id, int group1_id, string task_state_id_list);
        bool UpdateTaskSate(int task_id, int task_state_id, ModelStateDictionary modelState);
        string CheckState(int user_id, int task_id, ModelStateDictionary modelState);
    }

    public class WaTaskService : AuBaseService, IWaTaskService
    {
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_wa_task
                .Where(ss => ss.request_type_id == parent_id)
                .ProjectTo<WaTaskViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_wa_task
                .ProjectTo<WaTaskViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {            
            return dbContext.vw_wa_task
                .Where(ss => ss.task_id == id)
                .ProjectTo<WaTaskViewModel>()
                .FirstOrDefault();
        }

        public IQueryable<WaTaskViewModel> GetList_byWorkplace(int workplace_id, int group1_id)
        {
            return dbContext.vw_wa_task
                .Where(ss => ss.workplace_id == workplace_id && ss.request_type_group1_id == group1_id)
                .ProjectTo<WaTaskViewModel>();
        }

        public IQueryable<WaTaskViewModel> GetList_bySales(int sales_id, int group1_id, string task_state_id_list)
        {
            bool useTaskState = ((!String.IsNullOrWhiteSpace(task_state_id_list)) && (task_state_id_list != "0"));
            List<int> stateIdList = useTaskState ? (task_state_id_list.Split(',').Select(int.Parse).ToList()) : new List<int>();
            useTaskState = ((stateIdList == null) || (stateIdList.Count <= 0)) ? false : useTaskState;

            return dbContext.vw_wa_task
                .Where(ss => ss.sales_id == sales_id && ss.request_type_group1_id == group1_id
                    && (((useTaskState) && (stateIdList.Contains(ss.task_state_id))) || (!useTaskState))
                    )
                .ProjectTo<WaTaskViewModel>();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is WaTaskViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            WaTaskViewModel itemAdd = (WaTaskViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты задания");
                return null;
            }

            DateTime now = DateTime.Now;
            string user = currUser.CabUserName;

            wa_task wa_task_last = dbContext.wa_task.OrderByDescending(ss => ss.ord).FirstOrDefault();
            int ord = wa_task_last != null ? wa_task_last.ord + 1 : 1;
            wa_task_last = dbContext.wa_task.Where(ss => ss.client_id == itemAdd.client_id).OrderByDescending(ss => ss.ord).FirstOrDefault();
            int ord_client = wa_task_last != null ? wa_task_last.ord_client + 1 : 1;

            wa_task wa_task = new wa_task();
            wa_task.request_type_id = itemAdd.request_type_id;
            wa_task.task_state_id = itemAdd.task_state_id;
            wa_task.client_id = itemAdd.client_id;
            wa_task.sales_id = itemAdd.sales_id;
            wa_task.workplace_id = itemAdd.workplace_id;
            wa_task.ord = ord;                       
            wa_task.ord_client = ord_client;            
            wa_task.is_auto_created = false;
            wa_task.handling_server_cnt = null;
            wa_task.mess = null;
            wa_task.state_date = now;
            wa_task.state_user = user;

            wa_task.crt_date = now;
            wa_task.crt_user = user;
            wa_task.upd_date = now;
            wa_task.upd_user = user;
            wa_task.is_deleted = false;

            dbContext.wa_task.Add(wa_task);

            wa_task_lc wa_task_lc = new wa_task_lc();
            wa_task_lc.wa_task = wa_task;
            wa_task_lc.task_state_id = wa_task.task_state_id;
            wa_task_lc.mess = wa_task.mess;
            wa_task_lc.state_date = wa_task.state_date;
            wa_task_lc.state_user = wa_task.state_user;
            wa_task_lc.crt_date = wa_task.crt_date;
            wa_task_lc.crt_user = wa_task.crt_user;
            wa_task_lc.upd_date = wa_task.upd_date;
            wa_task_lc.upd_user = wa_task.upd_user;
            wa_task_lc.is_deleted = wa_task.is_deleted;
            dbContext.wa_task_lc.Add(wa_task_lc);

            if (!String.IsNullOrWhiteSpace(itemAdd.task_param))
            {
                wa_task_param wa_task_param = new wa_task_param();
                wa_task_param.wa_task = wa_task;
                wa_task_param.task_param = itemAdd.task_param;
                dbContext.wa_task_param.Add(wa_task_param);
            }

            dbContext.SaveChanges();

            return xGetItem(wa_task.task_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is WaTaskViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            WaTaskViewModel itemEdit = (WaTaskViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы параметры задания");
                return null;
            }

            wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == itemEdit.task_id).FirstOrDefault();
            if (wa_task == null)
            {
                modelState.AddModelError("", "Не найдены параметры задания c кодом " + itemEdit.task_id.ToString());
                return null;
            }

            if (wa_task.task_state_id != (int)Enums.WaTaskStateEnum.ISSUED_CLIENT)
            {
                modelState.AddModelError("", "Нельзя поменять параметры задания со статусом " + wa_task.task_state_id.EnumName<Enums.WaTaskStateEnum>());
                return null;
            }

            DateTime now = DateTime.Now;
            string user = currUser.CabUserName;

            WaTaskViewModel oldModel = new WaTaskViewModel();
            ModelMapper.Map<wa_task, WaTaskViewModel>(wa_task, oldModel);
            modelBeforeChanges = oldModel;

            wa_task.request_type_id = itemEdit.request_type_id;
            wa_task.upd_date = now;
            wa_task.upd_user = user;

            dbContext.SaveChanges();

            return xGetItem(wa_task.task_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is WaTaskViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            WaTaskViewModel itemDel = (WaTaskViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы параметры задания");
                return false;
            }

            wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == itemDel.task_id).FirstOrDefault();
            if (wa_task == null)
            {
                modelState.AddModelError("", "Не найдены параметры задания с кодом " + itemDel.task_id.ToString());
                return false;
            }

            if (wa_task.task_state_id != (int)Enums.WaTaskStateEnum.ISSUED_CLIENT)
            {
                modelState.AddModelError("", "Нельзя удалить задание со статусом " + wa_task.task_state_id.EnumName<Enums.WaTaskStateEnum>());
                return false;
            }

            dbContext.wa_task_lc.RemoveRange(dbContext.wa_task_lc.Where(ss => ss.task_id == wa_task.task_id));
            dbContext.wa_task_param.RemoveRange(dbContext.wa_task_param.Where(ss => ss.task_id == wa_task.task_id));
            dbContext.wa_task.Remove(wa_task);
            dbContext.SaveChanges();

            return true;            
        }

        public bool UpdateTaskSate(int task_id, int task_state_id, ModelStateDictionary modelState)
        {
            try
            {
                wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
                if (wa_task == null)
                {
                    modelState.AddModelError("", "Не найдены параметры задания c кодом " + task_id.ToString());
                    return false;
                }

                wa_task_state wa_task_state = dbContext.wa_task_state.Where(ss => ss.task_state_id == task_state_id).FirstOrDefault();
                if (wa_task_state == null)
                {
                    modelState.AddModelError("", "Не найден статус задания c кодом " + task_state_id.ToString());
                    return false;
                }

                List<int> allowed_state_id_list = new List<int>();
                switch ((Enums.WaTaskStateEnum)task_state_id)
                {
                    case Enums.WaTaskStateEnum.CANCELED_SERVER:
                        allowed_state_id_list.Add((int)Enums.WaTaskStateEnum.ISSUED_CLIENT);
                        allowed_state_id_list.Add((int)Enums.WaTaskStateEnum.ISSUED_SERVER);
                        break;
                    default:                        
                        modelState.AddModelError("", "Нельзя изменить статус задания с " + wa_task.task_state_id.EnumName<Enums.WaTaskStateEnum>() + " на " + task_state_id.EnumName<Enums.WaTaskStateEnum>());
                        return false;
                }

                if (!allowed_state_id_list.Contains(wa_task.task_state_id))
                {
                    modelState.AddModelError("", "Нельзя изменить статус задания с " + wa_task.task_state_id.EnumName<Enums.WaTaskStateEnum>() + " на " + task_state_id.EnumName<Enums.WaTaskStateEnum>());
                    return false;
                }
                
                /*
                switch ((Enums.WaTaskStateEnum)wa_task.task_state_id)
                {
                    case Enums.WaTaskStateEnum.ISSUED_CLIENT:
                    case Enums.WaTaskStateEnum.ISSUED_SERVER:
                        break;
                    default:
                        modelState.AddModelError("", "Нельзя изменить статус задания с " + wa_task.task_state_id.EnumName<Enums.WaTaskStateEnum>() + " на " + task_state_id.EnumName<Enums.WaTaskStateEnum>());
                        return false;
                }
                */

                DateTime now = DateTime.Now;
                string user = currUser.CabUserName;

                WaTaskViewModel oldModel = new WaTaskViewModel();
                ModelMapper.Map<wa_task, WaTaskViewModel>(wa_task, oldModel);
                modelBeforeChanges = oldModel;

                string mess = null;

                wa_task.task_state_id = task_state_id;
                wa_task.mess = mess;
                wa_task.upd_date = now;
                wa_task.upd_user = user;
                wa_task.state_date = now;
                wa_task.state_user = user;

                wa_task_lc wa_task_lc = new wa_task_lc();
                wa_task_lc.task_id = task_id;
                wa_task_lc.task_state_id = task_state_id;
                wa_task_lc.state_date = now;
                wa_task_lc.state_user = user;
                wa_task_lc.mess = mess;
                wa_task_lc.crt_date = now;
                wa_task_lc.crt_user = user;
                wa_task_lc.upd_date = now;
                wa_task_lc.upd_user = user;
                wa_task_lc.is_deleted = false;
                dbContext.wa_task_lc.Add(wa_task_lc);

                dbContext.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        public string CheckState(int user_id, int task_id, ModelStateDictionary modelState)
        {
            try
            {
                return checkState(user_id, task_id);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
        }

        private string checkState(int user_id, int task_id)
        {            
            AsnaServiceParam asnaServiceParam = new AsnaServiceParam()
            {
                asna_user_id = user_id,
                task_id = task_id,
            };

            string asnaServiceParamStr = JsonConvert.SerializeObject(asnaServiceParam);

            System.Net.ServicePointManager.Expect100Continue = false;

            string res = null;
            string url = "http://service.aptekaural.ru/auesnapi/asna/GetTaskState";
            bool isResponseOk = false;
            using (HttpClient httpClient = new HttpClient())
            {                
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Post,
                };

                request.Headers.Add("Cache-Control", "no-cache");
                request.Content = new StringContent(asnaServiceParamStr,
                                    Encoding.UTF8,
                                    "application/json");

                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode;
                        if (!isResponseOk)
                        {
                            return;
                        }

                        //var jsonTask = response.Content.ReadAsAsync<AsnaServiceResult>();
                        // jsonTask.Wait();

                        var jsonTask = response.Content.ReadAsStringAsync();
                        jsonTask.Wait();
                        var jsonTaskDeserialized = JsonConvert.DeserializeObject<AsnaServiceResult>(jsonTask.Result);
                        res = jsonTask.Result == null ? "Не найдено" : jsonTaskDeserialized.task_state;

                        //var locationHeader = response.Headers.Where(ss => ss.Key == "Location").FirstOrDefault();
                        //location = locationHeader.Value.Count() >= 0 ? string.Join(",", locationHeader.Value.ToArray()) : null;
                    });
                task.Wait();
            }

            return res;
        }
    }
}
