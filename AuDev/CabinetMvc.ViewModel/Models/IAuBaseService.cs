﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace CabinetMvc.ViewModel
{
    public interface IAuBaseService
    {
        IQueryable<AuBaseViewModel> GetList();
        IQueryable<AuBaseViewModel> GetList_byParent(long parent_id);
        IQueryable<AuBaseViewModel> GetList_bySearch(string search1, string search2);
        AuBaseViewModel GetItem(long id);
        AuBaseViewModel Insert(AuBaseViewModel item, ModelStateDictionary modelState);
        AuBaseViewModel Update(AuBaseViewModel item, ModelStateDictionary modelState);
        bool Delete(AuBaseViewModel item, ModelStateDictionary modelState);
    }
}
