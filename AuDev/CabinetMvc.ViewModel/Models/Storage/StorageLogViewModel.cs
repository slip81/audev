﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageLogViewModel : AuBaseViewModel
    {

        public StorageLogViewModel()
            : base(Enums.MainObjectType.STORAGE_LOG)
        {            
            //vw_log_storage
        }

        [Key()]
        [Display(Name = "Код")]
        public long log_id { get; set; }

        [Display(Name = "Дата начала")]
        public Nullable<System.DateTime> date_beg { get; set; }

        [Display(Name = "Дата окончания")]
        public Nullable<System.DateTime> date_end { get; set; }

        [Display(Name = "Автор")]
        public Nullable<int> user_id { get; set; }

        [Display(Name = "Сообщение")]
        public string mess { get; set; }

        [Display(Name = "Файл")]
        public Nullable<int> file_id { get; set; }

        [Display(Name = "Папка")]
        public Nullable<int> folder_id { get; set; }

        [Display(Name = "Файл")]
        public string file_name { get; set; }

        [Display(Name = "Папка")]
        public string folder_name { get; set; }

        [Display(Name = "Автор")]
        public string user_name { get; set; }

        [Display(Name = "Автор")]
        public string full_name { get; set; }

    }
}

