﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageFileTypeGroupViewModel : AuBaseViewModel
    {

        public StorageFileTypeGroupViewModel()
            : base(Enums.MainObjectType.STORAGE_FILE_TYPE_GROUP)
        {            
            //storage_file_type_group
        }

        [Key()]
        [Display(Name = "Код")]
        public int group_id { get; set; }

        [Display(Name = "Наименование")]
        public string group_name { get; set; }

        [Display(Name = "Картинка")]
        public string img { get; set; }

    }
}

