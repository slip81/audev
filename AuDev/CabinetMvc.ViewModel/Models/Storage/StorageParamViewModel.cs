﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageParamViewModel : AuBaseViewModel
    {

        public StorageParamViewModel()
            : base(Enums.MainObjectType.STORAGE_PARAM)
        {
            //storage_param
        }

        // storage_param

        [Key()]
        [Display(Name = "Код")]
        public int storage_id { get; set; }

        [Display(Name = "Описание")]
        public string descr { get; set; }

        [Display(Name = "Максимальный размер файла")]
        public string max_file_size { get; set; }

        [Display(Name = "Максимальное число файлов в папке")]
        public Nullable<int> max_folder_file_count { get; set; }
    }
}