﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageFileTypeViewModel : AuBaseViewModel
    {

        public StorageFileTypeViewModel()
            : base(Enums.MainObjectType.STORAGE_FILE_TYPE)
        {            
            //storage_file_type
        }

        [Key()]
        [Display(Name = "Код")]
        public int file_type_id { get; set; }

        [Display(Name = "Наименование")]
        public string file_type_name { get; set; }

        [Display(Name = "Группа")]
        public int group_id { get; set; }

        [Display(Name = "Группа")]
        [UIHint("FileTypeGroupCombo")]
        public StorageFileTypeGroupViewModel FileTypeGroup { get; set; }

        [Display(Name = "Группа")]
        public string group_name { get; set; }

        [Display(Name = "Иконка")]
        public string img { get; set; }
    }
}

