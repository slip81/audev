﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageFileViewModel : AuBaseViewModel
    {

        public StorageFileViewModel()
            : base(Enums.MainObjectType.STORAGE_FILE)
        {            
            //vw_storage_file
            forbidDelPhysicalFile = false;
        }

        // storage_file

        [Key()]
        [Display(Name = "Код")]
        public int file_id { get; set; }

        [Display(Name = "Наименование")]
        public string file_name { get; set; }

        [Display(Name = "Номер")]
        public int file_nom { get; set; }

        [Display(Name = "Папка")]
        public int folder_id { get; set; }

        [Display(Name = "Порядковый номер")]
        public int ord { get; set; }

        [Display(Name = "Размер")]
        public string file_size { get; set; }

        [Display(Name = "Физический путь")]
        public string physical_path { get; set; }

        [Display(Name = "Физическое имя")]
        public string physical_name { get; set; }

        [Display(Name = "Ссылка")]
        public string download_link { get; set; }

        [Display(Name = "Тип файла")]
        public int file_type_id { get; set; }

        [Display(Name = "Статус файла")]
        public int file_state_id { get; set; }

        [Display(Name = "Есть версии")]
        public bool has_version { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]
        public string crt_user { get; set; }

        [Display(Name = "Дата изменения")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Автор изменения")]
        public string upd_user { get; set; }

        [Display(Name = "Дата статуса")]
        public Nullable<System.DateTime> state_date { get; set; }

        [Display(Name = "Автор статуса")]
        public string state_user { get; set; }

        // vw_storage_file

        [Display(Name = "Папка")]
        public string folder_name { get; set; }

        [Display(Name = "Уровень папки")]
        public int folder_level { get; set; }

        [Display(Name = "Тип файла")]
        public string file_type_name { get; set; }

        [Display(Name = "Группа файла")]
        public Nullable<int> group_id { get; set; }
        
        [Display(Name = "Группа файла")]
        public string group_name { get; set; }

        [Display(Name = "Статус файла")]
        public string file_state_name { get; set; }

        [Display(Name = "sprite_css_class")]
        public string sprite_css_class { get; set; }

        [Display(Name = "Файл")]
        public string file_name_with_extention { get; set; }

        [Display(Name = "v1_exists")]
        public bool v1_exists { get; set; }

        [Display(Name = "v1_file_version_id")]
        public Nullable<int> v1_file_version_id { get; set; }

        [Display(Name = "v1_file_size")]
        public string v1_file_size { get; set; }

        [Display(Name = "v1_physical_path")]
        public string v1_physical_path { get; set; }

        [Display(Name = "v1_physical_name")]
        public string v1_physical_name { get; set; }

        [Display(Name = "v1_download_link")]
        public string v1_download_link { get; set; }

        [Display(Name = "v1_crt_date")]
        public Nullable<System.DateTime> v1_crt_date { get; set; }

        [Display(Name = "v1_crt_user")]
        public string v1_crt_user { get; set; }

        [Display(Name = "v2_exists")]
        public bool v2_exists { get; set; }

        [Display(Name = "v2_file_version_id")]
        public Nullable<int> v2_file_version_id { get; set; }

        [Display(Name = "v2_file_size")]
        public string v2_file_size { get; set; }

        [Display(Name = "v2_physical_path")]
        public string v2_physical_path { get; set; }

        [Display(Name = "v2_physical_name")]
        public string v2_physical_name { get; set; }

        [Display(Name = "v2_download_link")]
        public string v2_download_link { get; set; }

        [Display(Name = "v2_crt_date")]
        public Nullable<System.DateTime> v2_crt_date { get; set; }

        [Display(Name = "v2_crt_user")]
        public string v2_crt_user { get; set; }

        [Display(Name = "file_name_full")]
        public string file_name_full { get; set; }

        [Display(Name = "crt_date_str")]
        public string crt_date_str { get; set; }

        [Display(Name = "upd_date_str")]
        public string upd_date_str { get; set; }

        [Display(Name = "state_date_str")]
        public string state_date_str { get; set; }

        [Display(Name = "file_size_str")]
        public string file_size_str { get; set; }

        [Display(Name = "v1_file_name_full")]
        public string v1_file_name_full { get; set; }

        [Display(Name = "v1_crt_date_str")]
        public string v1_crt_date_str { get; set; }

        [Display(Name = "v1_upd_date_str")]
        public string v1_upd_date_str { get; set; }

        [Display(Name = "v1_file_size_str")]
        public string v1_file_size_str { get; set; }

        [Display(Name = "v1_version_num_actual")]
        public Nullable<int> v1_version_num_actual { get; set; }

        [Display(Name = "v2_file_name_full")]
        public string v2_file_name_full { get; set; }

        [Display(Name = "v2_crt_date_str")]
        public string v2_crt_date_str { get; set; }

        [Display(Name = "v2_upd_date_str")]
        public string v2_upd_date_str { get; set; }

        [Display(Name = "v2_file_size_str")]
        public string v2_file_size_str { get; set; }

        [Display(Name = "v2_version_num_actual")]
        public Nullable<int> v2_version_num_actual { get; set; }

        [Display(Name = "is_local_link")]
        public bool is_local_link { get; set; }

        [Display(Name = "Дата файла")]
        public Nullable<System.DateTime> file_date { get; set; }

        [Display(Name = "Дата файла")]
        public string file_date_str { get; set; }

        [Display(Name = "item_type")]
        public Nullable<int> item_type { get; set; }
        
        // additional
        [Display(Name = "Не удалять физически")]
        public bool forbidDelPhysicalFile { get; set; }

        // children

        [Display(Name = "ChildrenList")]
        [JsonIgnore()]
        public IEnumerable<StorageFileViewModel> ChildrenList { get; set; }

        // TreeViewModel         

        [Display(Name = "id")]
        public int? id { get; set; }

        [Display(Name = "parent_id")]
        public int? parent_id { get; set; }

        [Display(Name = "hasChildren")]
        public bool hasChildren { get; set; }
    }

    public class StorageFileSimpleViewModel
    {
        [Key()]
        [Display(Name = "Код")]
        public int file_id { get; set; }

        [Display(Name = "Папка")]
        public int folder_id { get; set; }
    }
}

