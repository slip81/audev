﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using AutoMapper.QueryableExtensions;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Db.Model;
using AuDev.Common.Network;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageFileVersionService : IAuBaseService
    {
        bool PrepareForInsert(int file_id, ModelStateDictionary modelState);
        bool Reassign(int src_file_id, int target_file_id, ModelStateDictionary modelState);
        StorageFileVersionViewModel GetFileVersion(int file_id, int version);
    }

    public class StorageFileVersionService : AuBaseService, IStorageFileVersionService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.storage_file_version
                .ProjectTo<StorageFileVersionViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.storage_file_version
                .Where(ss => ss.file_version_id == id)
                .ProjectTo<StorageFileVersionViewModel>()
                .FirstOrDefault();
        }
      

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.storage_file_version
                .Where(ss => ss.file_id == parent_id)
                .ProjectTo<StorageFileVersionViewModel>();
        }

        public StorageFileVersionViewModel GetFileVersion(int file_id, int version)
        {
            return dbContext.storage_file_version
                .Where(ss => ss.file_id == file_id && ss.version_num_actual == version)
                .ProjectTo<StorageFileVersionViewModel>()
                .FirstOrDefault();
        }

        public bool PrepareForInsert(int file_id, ModelStateDictionary modelState)
        {
            try
            {
                int allowed_version_num_max = 2;
                int version_num_max = 0;
                List<storage_file_version> existing_versions = dbContext.storage_file_version.Where(ss => ss.file_id == file_id).ToList();
                if ((existing_versions != null) && (existing_versions.Count > 0))
                {
                    version_num_max = existing_versions.Select(ss => ss.version_num).Max();
                    if (version_num_max >= allowed_version_num_max)
                    {
                        var existing_versions_for_del = existing_versions.Where(ss => ss.version_num >= version_num_max).ToList();
                        if ((existing_versions_for_del != null) && (existing_versions_for_del.Count > 0))
                        {
                            foreach (var existing_version_for_del in existing_versions_for_del)
                            {
                                dbContext.storage_file_version.Remove(existing_version_for_del);
                                version_num_max = version_num_max - 1;
                            }
                        }
                    }
                    foreach (var existing_version in existing_versions.Where(ss => ss.version_num <= version_num_max))
                    {
                        var fileNameParsed = GlobalUtil.ParseStorageFileName(existing_version.physical_name);                        
                        var fileNameNew = GlobalUtil.BuildStorageFileName(fileNameParsed.Item1, fileNameParsed.Item1, fileNameParsed.Item2, fileNameParsed.Item3, fileNameParsed.Item4, (existing_version.version_num + 1).ToString());

                        string srcFileNameAndPath = Path.Combine(existing_version.physical_path, existing_version.physical_name);
                        string destFileNameAndPath = Path.Combine(existing_version.physical_path, fileNameNew.Item4);
                        var res1 = xMoveFile(srcFileNameAndPath, destFileNameAndPath, modelState);
                        if ((!res1) || (!modelState.IsValid))
                        {
                            if (modelState.IsValid)
                                modelState.AddModelError("", "Ошибка при физическом переносе версии существующего файла");
                            return false;
                        }

                        existing_version.version_num = existing_version.version_num + 1;
                        existing_version.physical_name = fileNameNew.Item4;
                        //int download_link_lastSlashIndex = existing_version.download_link.LastIndexOf('/');
                        //existing_version.download_link = existing_version.download_link.Substring(0, download_link_lastSlashIndex) + "/" + existing_version.physical_name;                        
                        existing_version.download_link = ViewModelUtil.GetStorageFileLink() + existing_version.file_id.ToString() + "&version=" + existing_version.version_num.ToString();
                    }
                    dbContext.SaveChanges();
                }
                return true;
            }
            catch(Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                ToLog("Ошибка при обновлении", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileVersionViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            }
        }

        public bool Reassign(int src_file_id, int target_file_id, ModelStateDictionary modelState)
        {
            try
            {
                var src_file = dbContext.storage_file.Where(ss => ss.file_id == src_file_id).FirstOrDefault();
                if (src_file == null)
                {
                    modelState.AddModelError("", "Не найден файл-источник " + src_file_id.ToString());
                    return false;
                }

                var target_file = dbContext.storage_file.Where(ss => ss.file_id == target_file_id).FirstOrDefault();
                if (target_file == null)
                {
                    modelState.AddModelError("", "Не найден файл-цель " + target_file_id.ToString());
                    return false;
                }

                var versions = dbContext.storage_file_version.Where(ss => ss.file_id == src_file_id).ToList();
                if ((versions != null) && (versions.Count > 0))
                {
                    foreach (var version in versions)
                    {
                        version.file_id = target_file_id;
                    }
                    dbContext.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                ToLog("Ошибка при обновлении", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileVersionViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            }
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFileVersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageFileVersionViewModel itemAdd = (StorageFileVersionViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }           

            storage_file parent_file = dbContext.storage_file.Where(ss => ss.file_id == itemAdd.file_id).FirstOrDefault();
            if (parent_file == null)
            {
                modelState.AddModelError("", "Не найден файл с кодом " + itemAdd.file_id);
                return null;
            }

            var fileName_parsed = GlobalUtil.ParseStorageFileName(parent_file.physical_name);
            var fileName_withoutExtention = fileName_parsed.Item1;
            var fileName_extention = fileName_parsed.Item2;
            var fileName_date = fileName_parsed.Item3;
            var fileName_user = fileName_parsed.Item4;
            var fileName_version = fileName_parsed.Item5;
            //var fileName_version = itemAdd.version_num;

            var fileName_new = GlobalUtil.BuildStorageFileName(parent_file.file_name + "." + fileName_extention, parent_file.file_name, fileName_extention, fileName_date, fileName_user, itemAdd.version_num.ToString());


            DateTime now = DateTime.Now;
            storage_file_version storage_file_version = new storage_file_version();
            storage_file_version.crt_date = now;
            storage_file_version.crt_user = currUser.CabUserName;
            storage_file_version.file_id = itemAdd.file_id;
            storage_file_version.file_size = itemAdd.file_size;
            storage_file_version.physical_name = fileName_new.Item4;
            storage_file_version.physical_path = itemAdd.physical_path;
            storage_file_version.version_num = itemAdd.version_num;
            storage_file_version.version_num_actual = itemAdd.version_num_actual;
            //int download_link_lastSlashIndex = parent_file.download_link.LastIndexOf('/');            
            //storage_file_version.download_link = parent_file.download_link.Substring(0, download_link_lastSlashIndex) + "/" + storage_file_version.physical_name;
            storage_file_version.download_link = ViewModelUtil.GetStorageFileLink() + storage_file_version.file_id.ToString() + "&version=" + storage_file_version.version_num.ToString();

            dbContext.storage_file_version.Add(storage_file_version);

            parent_file.has_version = true;

            dbContext.SaveChanges();

            return GetItem(storage_file_version.file_version_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFileVersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageFileVersionViewModel itemEdit = (StorageFileVersionViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            storage_file storage_file = dbContext.storage_file.Where(ss => ss.file_id == itemEdit.file_id).FirstOrDefault();
            if (storage_file == null)
            {
                modelState.AddModelError("", "Не найден файл " + itemEdit.file_id.ToString());
                return null;
            }

            storage_file_version storage_file_version = dbContext.storage_file_version.Where(ss => ss.file_version_id == itemEdit.file_version_id).FirstOrDefault();
            if (storage_file_version == null)
            {
                modelState.AddModelError("", "Не найдена версия файла " + itemEdit.file_version_id.ToString());
                return null;
            }


            modelBeforeChanges = new StorageFileVersionViewModel()
            {
                file_version_id = storage_file_version.file_version_id,
                file_id = storage_file_version.file_id,
                version_num = storage_file_version.version_num,
                file_size = storage_file_version.file_size,
                physical_path = storage_file_version.physical_path,
                physical_name = storage_file_version.physical_name,
                download_link = storage_file_version.download_link,
                crt_date = storage_file_version.crt_date,
                crt_user = storage_file_version.crt_user,
                upd_date = storage_file_version.upd_date,
                upd_user = storage_file_version.upd_user,
                version_num_actual = storage_file_version.version_num_actual,
            };

            DateTime now = DateTime.Now;

            var fileName_parsed = GlobalUtil.ParseStorageFileName(storage_file_version.physical_name);
            var fileName_withoutExtention = fileName_parsed.Item1;
            var fileName_extention = fileName_parsed.Item2;
            var fileName_date = fileName_parsed.Item3;
            var fileName_user = fileName_parsed.Item4;
            var fileName_version = fileName_parsed.Item5;

            var fileName_new = GlobalUtil.BuildStorageFileName(storage_file.file_name + "." + fileName_extention, storage_file.file_name, fileName_extention, fileName_date, fileName_user, fileName_version);
            
            storage_file_version.physical_name = fileName_new.Item4;
            //int download_link_lastSlashIndex = storage_file_version.download_link.LastIndexOf('/');
            //storage_file_version.download_link = storage_file_version.download_link.Substring(0, download_link_lastSlashIndex) + "/" + storage_file_version.physical_name;

            storage_file_version.upd_date = now;
            storage_file_version.upd_user = currUser.CabUserName;

            var res1 = xMoveFile(Path.Combine(itemEdit.physical_path, itemEdit.physical_name), Path.Combine(storage_file_version.physical_path, storage_file_version.physical_name), modelState);
            if ((!res1) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при физическом перемещении версии файла");
            }

            dbContext.SaveChanges();

            return GetItem(storage_file_version.file_version_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFileVersionViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            StorageFileVersionViewModel itemDel = (StorageFileVersionViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return false;
            }

            storage_file_version storage_file_version = dbContext.storage_file_version.Where(ss => ss.file_version_id == itemDel.file_version_id).FirstOrDefault();
            if (storage_file_version == null)
            {
                modelState.AddModelError("", "Не найдена версия файла с кодом " + itemDel.file_version_id.ToString());
                return false;
            }

            if (!itemDel.forbidDelPhysicalFile)
            {
                string delFileName = Path.Combine(storage_file_version.physical_path, storage_file_version.physical_name);
                try
                {
                    // !!!
                    //NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");
                    //NetworkCredential writeCredentials = new NetworkCredential("pavlov", "Pavloff99991", "tsb");
                    //using (new NetworkConnection(filePath2_orig, writeCredentials))
                    //{                        

                    if (File.Exists(delFileName))
                        File.Delete(delFileName);

                    //}
                }
                catch (Exception ex)
                {
                    string ex_info = GlobalUtil.ExceptionInfo(ex);
                    ToLog("Ошибка при удалении", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileVersionViewModel()), null, ex_info);
                    modelState.AddModelError("", "Ошибка: " + ex_info);
                    return false;
                }
            }
            
            dbContext.storage_file_version.Remove(storage_file_version);

            dbContext.SaveChanges();

            return true;
        }


        private bool xMoveFile(string fileSrcPath, string fileDestPath, ModelStateDictionary modelState)
        {
            try
            {
                return GlobalUtil.MoveFile(ViewModelUtil.GetStorageFilePath(), fileSrcPath, fileDestPath);
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                ToLog("Ошибка при переносе файла", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileVersionViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            }
        }

        private bool xDeleteFile(string filePath, ModelStateDictionary modelState)
        {
            try
            {
                return GlobalUtil.DeleteFile(ViewModelUtil.GetStorageFilePath(), filePath);
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                ToLog("Ошибка при удалении", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileVersionViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            }
        }

    }
}
