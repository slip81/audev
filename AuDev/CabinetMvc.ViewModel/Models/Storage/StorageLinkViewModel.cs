﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageLinkViewModel : AuBaseViewModel
    {

        public StorageLinkViewModel()
            : base(Enums.MainObjectType.STORAGE_LINK)
        {            
            //storage_link
        }

        [Key()]
        [Display(Name = "Код")]
        public int link_id { get; set; }

        [Display(Name = "Название")]
        public string link_name { get; set; }

        [Display(Name = "Папка")]
        public int folder_id { get; set; }

        [Display(Name = "Адрес")]
        public string download_link { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]
        public string crt_user { get; set; }

    }
}

