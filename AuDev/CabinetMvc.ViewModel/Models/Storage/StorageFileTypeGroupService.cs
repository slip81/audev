﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageFileTypeGroupService : IAuBaseService
    {
        //
    }

    public class StorageFileTypeGroupService : AuBaseService, IStorageFileTypeGroupService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.storage_file_type_group
                .Where(ss => ss.group_id > 0)
                .ProjectTo<StorageFileTypeGroupViewModel>()
                .OrderBy(ss => ss.group_name);
        }
    }
}