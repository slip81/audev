﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageFolderViewModel : AuBaseViewModel
    {

        public StorageFolderViewModel()
            : base(Enums.MainObjectType.STORAGE_FOLDER)
        {            
            //vw_storage_folder            
        }

        // vw_storage_folder

        [Key()]
        [Display(Name = "Код")]
        public int folder_id { get; set; }

        [Display(Name = "Название")]
        public string folder_name { get; set; }

        [Display(Name = "Родитель")]
        public Nullable<int> parent_id { get; set; }

        [Display(Name = "Уровень")]
        public int folder_level { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Кто создал")]
        public string crt_user { get; set; }

        [Display(Name = "Файлов")]
        public int file_cnt { get; set; }

        [Display(Name = "Папок")]
        public int folder_cnt { get; set; }

        [Display(Name = "Кто создал")]
        public string crt_date_str { get; set; }

        // children

        [Display(Name = "ChildrenList")]
        [JsonIgnore()]
        public IEnumerable<StorageFolderViewModel> ChildrenList { get; set; }

        // TreeViewModel         

        [Display(Name = "id")]
        public int? id { get; set; }

        [Display(Name = "hasChildren")]
        public bool hasChildren { get; set; }
    }
}

