﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.ComponentModel.DataAnnotations;
using CabinetMvc.ViewModel.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageFolderService : IAuBaseService
    {
        IEnumerable<StorageFolderViewModel> GetTreeList();
        bool Move(int folder_id, int? parent_id, ModelStateDictionary modelState);
        string Download(ModelStateDictionary modelState);
        bool Remove(int folder_id, ModelStateDictionary modelState);
        bool AddTaskToFolder(int task_id, string task_num, int folder_id_new, int? folder_id_old, ModelStateDictionary modelState);
        bool RemoveTaskFromFolder(int task_id, string task_num, int? folder_id, ModelStateDictionary modelState);
    }

    public class StorageFolderService : AuBaseService, IStorageFolderService
    {

        public IEnumerable<StorageFolderViewModel> GetTreeList()
        {
            List<StorageFolderViewModel> mainList = dbContext.vw_storage_folder
                .Where(ss => ss.folder_level == 0 && !ss.is_deleted)
                .Select(ss => new StorageFolderViewModel()
                {
                    folder_id = ss.folder_id,
                    folder_name = ss.folder_name,
                    parent_id = ss.parent_id,
                    folder_level = ss.folder_level,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    file_cnt = ss.file_cnt,
                    folder_cnt = ss.folder_cnt,
                    crt_date_str = ss.crt_date_str,
                    id = ss.folder_id,
                    hasChildren = ss.vw_storage_folder1.Where(tt => !tt.is_deleted).Any(),
                }
                )
                .OrderBy(ss => ss.folder_name)
                .ToList();

            if ((mainList == null) || (mainList.Count <= 0))
                return mainList;

            foreach (var mainListItem in mainList)
            {
                mainListItem.ChildrenList = getChildrenList(mainListItem.folder_id);
            }

            return mainList.OrderBy(ss => ss.folder_level).ThenBy(ss => ss.folder_name);            
        }

        private IEnumerable<StorageFolderViewModel> getChildrenList(int folder_id)
        {
            var childList = dbContext.vw_storage_folder
                .Where(ss => ss.parent_id == folder_id && !ss.is_deleted)
                .Select(ss => new StorageFolderViewModel()
                {
                    folder_id = ss.folder_id,
                    folder_name = ss.folder_name,
                    parent_id = ss.parent_id,
                    folder_level = ss.folder_level,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    file_cnt = ss.file_cnt,
                    folder_cnt = ss.folder_cnt,
                    crt_date_str = ss.crt_date_str,
                    id = ss.folder_id,
                    hasChildren = ss.vw_storage_folder1.Where(tt => !tt.is_deleted).Any(),
                }
                )
                .OrderBy(ss => ss.folder_name)
                .ToList();
            if ((childList != null) && (childList.Count > 0))
            {
                foreach (var childListItem in childList)
                {
                    childListItem.ChildrenList = getChildrenList(childListItem.folder_id);
                }
            }
            return childList;
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        { 
            return dbContext.vw_storage_folder
                .Where(ss => ss.folder_level == 0 && !ss.is_deleted)
                .Select(ss => new StorageFolderViewModel()
                {
                    folder_id = ss.folder_id,
                    folder_name = ss.folder_name,
                    parent_id = ss.parent_id,
                    folder_level = ss.folder_level,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    file_cnt = ss.file_cnt,
                    folder_cnt = ss.folder_cnt,
                    crt_date_str = ss.crt_date_str,
                    id = ss.folder_id,
                    hasChildren = ss.vw_storage_folder1.Where(tt => !tt.is_deleted).Any(),                    
                }
                )
                .OrderBy(ss => ss.folder_name);
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_storage_folder
                .Where(ss => ss.folder_id == id && !ss.is_deleted)
                .Select(ss => new StorageFolderViewModel()
                {
                    folder_id = ss.folder_id,
                    folder_name = ss.folder_name,
                    parent_id = ss.parent_id,
                    folder_level = ss.folder_level,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    file_cnt = ss.file_cnt,
                    folder_cnt = ss.folder_cnt,
                    crt_date_str = ss.crt_date_str,
                    id = ss.folder_id,
                    hasChildren = ss.vw_storage_folder1.Where(tt => !tt.is_deleted).Any(),  
                }
                )
                .FirstOrDefault();
        }
      

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_storage_folder
                .Where(ss => ss.parent_id == parent_id && !ss.is_deleted)
                .Select(ss => new StorageFolderViewModel()
                {
                    folder_id = ss.folder_id,
                    folder_name = ss.folder_name,
                    parent_id = ss.parent_id,
                    folder_level = ss.folder_level,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    file_cnt = ss.file_cnt,
                    folder_cnt = ss.folder_cnt,
                    crt_date_str = ss.crt_date_str,
                    id = ss.folder_id,
                    hasChildren = ss.vw_storage_folder1.Where(tt => !tt.is_deleted).Any(),  
                }
                )
                .OrderBy(ss => ss.folder_name);
        }

        private void xToLog_Storage(string mess, int folder_id)
        {
            IStorageLogService storageLogService = DependencyResolver.Current.GetService<IStorageLogService>();
            storageLogService.ToLog_Storage(mess, null, folder_id);
        }

        public bool Move(int folder_id, int? parent_id, ModelStateDictionary modelState)
        {
            var moved_folder = dbContext.storage_folder.Where(ss => ss.folder_id == folder_id).FirstOrDefault();
            var parent_folder = dbContext.storage_folder.Where(ss => ss.folder_id == parent_id).FirstOrDefault();

            var existing_folder = dbContext.storage_folder.Where(ss => ss.folder_id != moved_folder.folder_id && !ss.is_deleted && ss.parent_id == parent_id && ss.folder_name.Trim().ToLower().Equals(moved_folder.folder_name.Trim().ToLower())).FirstOrDefault();
            if (existing_folder != null)
            {
                modelState.AddModelError("", "Уже есть такая папка");
                return false;
            }

            moved_folder.parent_id = parent_id;
            moved_folder.folder_level = parent_folder != null ? parent_folder.folder_level + 1 : 0;
            dbContext.SaveChanges();
            xToLog_Storage("Папка " + moved_folder.folder_name + " перемещена " + (parent_folder == null ? "в корень" : ("в папку " + parent_folder.folder_name)), moved_folder.folder_id);
            return true;
        }

        public string Download(ModelStateDictionary modelState)
        {
            try
            {
                var folderTree = GetTreeList().ToList();
                if ((folderTree == null) || (folderTree.Count <= 0))
                {
                    modelState.AddModelError("", "Нет папок в хранилище");
                    return null;
                }


                List<vw_storage_file> fileList = dbContext.vw_storage_file.ToList();
                if ((fileList == null) || (fileList.Count <= 0))
                {
                    modelState.AddModelError("", "Нет файлов в хранилище");
                    return null;
                }

                DateTime now = DateTime.Now;
                string src_path = ViewModelUtil.GetStorageFilePath();
                string target_path= ViewModelUtil.GetStorageTmpFilePath();
                target_path = Path.Combine(target_path, currUser.CabUserId + "_bkp_" + now.ToString("ddMMyyyyHHmmss"));
                foreach (var folder in folderTree.Where(ss => ss.folder_level == 0).ToList())
                {
                    xCreateFolder(folder, target_path);
                }

                
                List<vw_storage_folder_tree> storage_folder_tree_list = dbContext.vw_storage_folder_tree.ToList();

                string file_path = "";
                foreach (var file in fileList.Where(ss => ss.item_type == (int)Enums.StorageItemEnum.FILE).OrderBy(ss => ss.folder_name))
                {
                    var curr_folder = storage_folder_tree_list.Where(ss => ss.folder_id == file.folder_id).FirstOrDefault();
                    if (curr_folder == null)
                    {
                        modelState.AddModelError("", "Не найдена папка с кодом " + file.folder_id.ToString() + " для файла с кодом " + file.file_id.ToString());
                        return null;
                    }
                    file_path = Path.Combine(target_path, curr_folder.full_path, file.file_name_with_extention);
                    var copyFileRes = xCopyFile(Path.Combine(file.physical_path, file.physical_name), file_path, modelState);
                    if ((!copyFileRes) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при копировании файла с кодом " + file.file_id.ToString());
                        return null;
                    }
                }

                // !!!
                // временно отключим скачивание "Внешних ссылок"

                /*
                string file_text = "";
                foreach (var file in fileList.Where(ss => ss.item_type == (int)Enums.StorageItemEnum.LINK).OrderBy(ss => ss.folder_name))
                {
                    var curr_folder = storage_folder_tree_list.Where(ss => ss.folder_id == file.folder_id).FirstOrDefault();
                    if (curr_folder == null)
                    {
                        modelState.AddModelError("", "Не найдена папка с кодом " + file.folder_id.ToString() + " для файла с кодом " + file.file_id.ToString());
                        return null;
                    }
                    file_path = Path.Combine(target_path, curr_folder.full_path, file.file_name + ".url");
                    file_text = "[InternetShortcut]" + System.Environment.NewLine + "URL=" + file.download_link.Trim();
                    if (File.Exists(file_path))
                        File.Delete(file_path);
                    File.WriteAllText(file_path, file_text, System.Text.Encoding.GetEncoding("windows-1251"));
                }
                */

                string file_path_arc = target_path.TrimEnd('\\') + ".zip";
                ZipFile.CreateFromDirectory(target_path, file_path_arc, CompressionLevel.Fastest, false, System.Text.Encoding.GetEncoding("cp866"));

                string arc_name = "storage_bkp_" + currUser.CabUserId + "_" + now.ToString("ddMMyyyyHHmmss") + ".zip";
                string folder_path_arc = Path.Combine(src_path, arc_name);
                var moveFileRes = xMoveFile(file_path_arc, folder_path_arc, modelState);
                if ((!moveFileRes) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при переносе архива");
                    return null;
                }

                if (Directory.Exists(target_path))
                    Directory.Delete(target_path, true);

                return ViewModelUtil.GetStorageArcLink() + arc_name;                           
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return null;
            }
            
        }

        private void xCreateFolder(StorageFolderViewModel folder, string parent_folder_path)
        {
            string folder_path = Path.Combine(parent_folder_path, folder.folder_name);
            if (Directory.Exists(folder_path))
                Directory.Delete(folder_path, true);
            Directory.CreateDirectory(folder_path);

            if ((folder.ChildrenList != null) && (folder.ChildrenList.Count() > 0))
            {
                foreach (var childFolder in folder.ChildrenList)
                {
                    xCreateFolder(childFolder, folder_path);
                }
            }            
        }

        private bool xCopyFile(string fileSrcPath, string fileDestPath, ModelStateDictionary modelState)
        {
            try
            {
                return GlobalUtil.CopyFile(ViewModelUtil.GetStorageFilePath(), fileSrcPath, fileDestPath);
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                //ToLog("Ошибка при переносе файла", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            }
        }

        private bool xMoveFile(string fileSrcPath, string fileDestPath, ModelStateDictionary modelState)
        {
            try
            {
                return GlobalUtil.MoveFile(ViewModelUtil.GetStorageFilePath(), fileSrcPath, fileDestPath);
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                //ToLog("Ошибка при переносе файла", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            }
        }

        private bool xMoveFile_Crm(string fileSrcPath, string fileDestPath, ModelStateDictionary modelState)
        {
            try
            {
                return GlobalUtil.MoveFile(ViewModelUtil.GetCrmFileStoragePath(), fileSrcPath, fileDestPath);
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                //ToLog("Ошибка при переносе файла", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            }
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFolderViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageFolderViewModel itemAdd = (StorageFolderViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            DateTime now = DateTime.Now;

            storage_folder parent_storage_folder = null;
            int? parent_id = null;
            if (itemAdd.parent_id.GetValueOrDefault(0) > 0)
            {
                parent_storage_folder = dbContext.storage_folder.Where(ss => ss.folder_id == itemAdd.parent_id).FirstOrDefault();
                if (parent_storage_folder == null)
                {
                    modelState.AddModelError("", "Не найден родитель с кодом " + itemAdd.parent_id);
                    return null;
                }
                parent_id = parent_storage_folder.folder_id;
            }

            var existing_folder = dbContext.storage_folder.Where(ss => !ss.is_deleted && ss.parent_id == parent_id && ss.folder_name.Trim().ToLower().Equals(itemAdd.folder_name.Trim().ToLower())).FirstOrDefault();
            if (existing_folder != null)
            {
                modelState.AddModelError("", "Уже есть такая папка");
                return null;
            }

            storage_folder storage_folder = new storage_folder();
            storage_folder.crt_date = now;
            storage_folder.crt_user = currUser.CabUserName;
            storage_folder.folder_level = parent_storage_folder != null ? parent_storage_folder.folder_level + 1 : 0;
            storage_folder.folder_name = itemAdd.folder_name;
            storage_folder.parent_id = itemAdd.parent_id;            
            dbContext.storage_folder.Add(storage_folder);

            dbContext.SaveChanges();

            xToLog_Storage("Создана папка " + storage_folder.folder_name + (parent_storage_folder == null ? " в корне" : (" в папке " + parent_storage_folder.folder_name)), storage_folder.folder_id);

            return GetItem(storage_folder.folder_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFolderViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageFolderViewModel itemEdit = (StorageFolderViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            storage_folder storage_folder = dbContext.storage_folder.Where(ss => ss.folder_id == itemEdit.folder_id).FirstOrDefault();
            if (storage_folder == null)
            {
                modelState.AddModelError("", "Не найдена папка " + itemEdit.folder_id.ToString());
                return null;
            }

            var existing_folder = dbContext.storage_folder.Where(ss => ss.folder_id != itemEdit.folder_id && !ss.is_deleted && ss.parent_id == itemEdit.parent_id && ss.folder_name.Trim().ToLower().Equals(itemEdit.folder_name.Trim().ToLower())).FirstOrDefault();
            if (existing_folder != null)
            {
                modelState.AddModelError("", "Уже есть такая папка");
                return null;
            }

            modelBeforeChanges = new StorageFolderViewModel()
                {
                    folder_id = storage_folder.folder_id,
                    folder_name = storage_folder.folder_name,
                    parent_id = storage_folder.parent_id,
                    folder_level = storage_folder.folder_level,
                    crt_date = storage_folder.crt_date,
                    crt_user = storage_folder.crt_user,                    
                };

            string old_folder_name = storage_folder.folder_name;
            storage_folder.folder_name = itemEdit.folder_name;
            dbContext.SaveChanges();

            xToLog_Storage("Папка " + old_folder_name + " переименована в " + storage_folder.folder_name, storage_folder.folder_id);

            return GetItem(storage_folder.folder_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFolderViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            StorageFolderViewModel itemDel = (StorageFolderViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return false;
            }

            storage_folder storage_folder = dbContext.storage_folder.Where(ss => ss.folder_id == itemDel.folder_id).FirstOrDefault();
            if (storage_folder == null)
            {
                modelState.AddModelError("", "Не найдена папка с кодом " + itemDel.folder_id.ToString());
                return false;
            }

            string old_folder_name = storage_folder.folder_name;
            int old_folder_id = storage_folder.folder_id;
            
            var res = deleteWithChildren(itemDel.folder_id, modelState);
            if ((!res) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при удалении папки с кодом " + itemDel.folder_id);
                return false;
            }

            //dbContext.SaveChanges();

            xToLog_Storage("Папка " + old_folder_name + " удалена", old_folder_id);

            return true;
        }

        public bool Remove(int folder_id, ModelStateDictionary modelState)
        {
            storage_folder storage_folder = dbContext.storage_folder.Where(ss => ss.folder_id == folder_id).FirstOrDefault();
            if (storage_folder == null)
            {
                modelState.AddModelError("", "Не найдена папка с кодом " + folder_id.ToString());
                return false;
            }

            string old_folder_name = storage_folder.folder_name;
            int old_folder_id = storage_folder.folder_id;

            var res = deleteWithChildren(folder_id, modelState, true);
            if ((!res) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при удалении папки с кодом " + folder_id);
                return false;
            }

            //dbContext.SaveChanges();

            xToLog_Storage("Папка " + old_folder_name + " удалена", old_folder_id);

            return true;
        }

        private bool deleteWithChildren(int folder_id, ModelStateDictionary modelState, bool removeCompletely = false)
        {
            List<storage_folder> storage_folder_children = dbContext.storage_folder.Where(ss => ss.parent_id == folder_id).ToList();
            if ((storage_folder_children != null) && (storage_folder_children.Count > 0))
            {
                foreach (var storage_folder_child in storage_folder_children)
                {
                    deleteWithChildren(storage_folder_child.folder_id, modelState);
                }
            }

            IStorageFileService storageFileService = DependencyResolver.Current.GetService<IStorageFileService>();
            List<storage_file> storage_file_list_for_del = dbContext.storage_file.Where(ss => ss.folder_id == folder_id).ToList();
            if ((storage_file_list_for_del != null) && (storage_file_list_for_del.Count > 0))
            {
                foreach (var storage_file_for_del in storage_file_list_for_del)
                {
                    StorageFileViewModel fileDel = new StorageFileViewModel() { file_id = storage_file_for_del.file_id };
                    bool fileDelRes = false;
                    if (removeCompletely)
                    {
                        fileDelRes = storageFileService.Remove(fileDel.file_id, modelState);
                    }
                    else
                    {
                        fileDelRes = storageFileService.Delete(fileDel, modelState);
                    }
                    if ((!fileDelRes) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при удалении файла с кодом " + storage_file_for_del.file_id.ToString());
                        return false;
                    }                    
                }
            }

            DateTime now = DateTime.Now;

            if (removeCompletely)
            {
                using (AuMainDb newContext = new AuMainDb())
                {
                    newContext.storage_link.RemoveRange(newContext.storage_link.Where(ss => ss.folder_id == folder_id));
                    newContext.storage_folder.Remove(newContext.storage_folder.Where(ss => ss.folder_id == folder_id).FirstOrDefault());
                    newContext.SaveChanges();
                }
            }
            else
            {
                List<storage_link> storage_link_list = dbContext.storage_link.Where(ss => ss.folder_id == folder_id).ToList();
                if (storage_link_list != null)
                {
                    foreach (var storage_link in storage_link_list)
                    {
                        storage_link.is_deleted = true;
                        storage_link.del_date = now;
                        storage_link.del_user = currUser.CabUserName;
                    }
                }

                storage_folder storage_folder = dbContext.storage_folder.Where(ss => ss.folder_id == folder_id).FirstOrDefault();
                storage_folder.is_deleted = true;
                storage_folder.del_date = now;
                storage_folder.del_user = currUser.CabUserName;
                dbContext.SaveChanges();
            }

            return true;
        }

        public bool AddTaskToFolder(int task_id, string task_num, int folder_id_new, int? folder_id_old, ModelStateDictionary modelState)
        {
            try
            {
                return false;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

        public bool RemoveTaskFromFolder(int task_id, string task_num, int? folder_id, ModelStateDictionary modelState)
        {
            try
            {
                if (String.IsNullOrEmpty(task_num))
                {
                    modelState.AddModelError("", "Не задан номер задачи");
                    return false;
                }

                DateTime now = DateTime.Now;

                storage_folder storage_folder = dbContext.storage_folder.Where(ss => ss.folder_id == folder_id).FirstOrDefault();
                if (storage_folder == null)
                {
                    modelState.AddModelError("", "Не найдена папка с кодом " + folder_id.ToString());
                    return false;
                }

                storage_folder storage_folder_task = dbContext.storage_folder.Where(ss => ss.parent_id == storage_folder.folder_id && ss.folder_name.Trim().ToLower().Equals(task_num.Trim().ToLower())).FirstOrDefault();
                if (storage_folder_task != null)
                {
                    StorageFolderViewModel storageFolderVM = new StorageFolderViewModel();
                    storageFolderVM.folder_id = storage_folder_task.folder_id;
                    storageFolderVM.folder_level = storage_folder_task.folder_level - 1;
                    storageFolderVM.folder_name = task_num.Trim().ToUpper();
                    storageFolderVM.parent_id = storage_folder_task.folder_id;
                    var delFolderResult = Delete(storageFolderVM, modelState);
                    if ((!delFolderResult) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при создании папки " + task_num);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return false;
            }
        }

    }
}
