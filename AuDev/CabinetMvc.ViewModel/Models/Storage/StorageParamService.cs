﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageParamService : IAuBaseService
    {
        string GetMaxFileSize(int storage_id);
        List<string> GetAllowedExtentions(int storage_id);
    }

    public class StorageParamService : AuBaseService, IStorageParamService
    {

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.storage_param
                .Where(ss => ss.storage_id == id)
                .ProjectTo<StorageParamViewModel>()
                .FirstOrDefault();
        }

        public string GetMaxFileSize(int storage_id)
        {
            return dbContext.storage_param
                .Where(ss => ss.storage_id == storage_id)
                .Select(ss => ss.max_file_size)
            .FirstOrDefault();
        }

        public List<string> GetAllowedExtentions(int storage_id)
        {
            return (from t1 in dbContext.storage_param
                    from t2 in dbContext.storage_file_type
                    where t1.storage_id == storage_id
                    select t2)
                      .Select(ss => "." + ss.file_type_name.Trim().ToLower())
                      .ToList();
        }
      
        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageParamViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageParamViewModel itemEdit = (StorageParamViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            storage_param storage_param = dbContext.storage_param.Where(ss => ss.storage_id == itemEdit.storage_id).FirstOrDefault();
            if (storage_param == null)
            {
                modelState.AddModelError("", "Не найдено хранилище " + itemEdit.storage_id.ToString());
                return null;
            }

            modelBeforeChanges = new StorageParamViewModel()
                {
                    storage_id = storage_param.storage_id,
                    descr = storage_param.descr,
                    max_file_size = storage_param.max_file_size,
                    max_folder_file_count = storage_param.max_folder_file_count,
                };

            storage_param.max_file_size = itemEdit.max_file_size;
            storage_param.max_folder_file_count = itemEdit.max_folder_file_count;
            dbContext.SaveChanges();

            return GetItem(storage_param.storage_id);
        }
    }
}
