﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageTrashViewModel : AuBaseViewModel
    {

        public StorageTrashViewModel()
            : base(Enums.MainObjectType.STORAGE_TRASH)
        {            
            //vw_storage_trash
        }

        [Key()]
        [Display(Name = "Код")]
        public int item_id { get; set; }
        public string item_name { get; set; }
        public int file_nom { get; set; }
        public int folder_id { get; set; }
        public int ord { get; set; }
        public string file_size { get; set; }
        public string physical_path { get; set; }
        public string physical_name { get; set; }
        public string download_link { get; set; }
        public int file_type_id { get; set; }
        public int file_state_id { get; set; }
        public bool has_version { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public Nullable<System.DateTime> state_date { get; set; }
        public string state_user { get; set; }
        public string folder_name { get; set; }
        public int folder_level { get; set; }
        public string file_type_name { get; set; }
        public Nullable<int> group_id { get; set; }
        public string group_name { get; set; }
        public string file_state_name { get; set; }
        public string sprite_css_class { get; set; }
        public string file_name_full { get; set; }
        public string crt_date_str { get; set; }
        public string upd_date_str { get; set; }
        public string state_date_str { get; set; }
        [Display(Name="Наименование")]
        public string item_name_with_extention { get; set; }
        public string file_size_str { get; set; }
        [Display(Name = "Когда удалено")]
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_date_str { get; set; }
        [Display(Name = "Кем удалено")]
        public string del_user { get; set; }
        public Nullable<int> item_type { get; set; }

    }
}

