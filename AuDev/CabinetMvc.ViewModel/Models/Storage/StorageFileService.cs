﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using CabinetMvc.ViewModel.Util;
using CabinetMvc.ViewModel.Adm;
using AuDev.Common.Db.Model;
using AuDev.Common.Network;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageFileService : IAuBaseService
    {
        StorageFileViewModel UploadFile(HttpPostedFileBase postedFile, int folder_id, bool overwrite, string file_date, ModelStateDictionary modelState);
        bool Move(int file_id, int folder_id, ModelStateDictionary modelState);
        bool CheckExists(string fileName, int folder_id);
        List<StorageFileSimpleViewModel> Find(string item_name);
        bool Remove(int file_id, ModelStateDictionary modelState);
        StorageFileViewModel UpdateProperties(StorageFileViewModel itemEdit, ModelStateDictionary modelState);
    }

    public class StorageFileService : AuBaseService, IStorageFileService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        { 
            return dbContext.vw_storage_file                
                .Select(ss => new StorageFileViewModel()
                {
                    file_id = ss.file_id,
                    file_name = ss.file_name,
                    file_nom = ss.file_nom,
                    folder_id = ss.folder_id,
                    ord = ss.ord,                    
                    file_size = ss.file_size,
                    physical_path = ss.physical_path,
                    physical_name = ss.physical_name,
                    download_link = ss.download_link,
                    file_type_id = ss.file_type_id,
                    file_state_id = ss.file_state_id,
                    has_version = ss.has_version,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    upd_date = ss.upd_date,
                    upd_user = ss.upd_user,
                    state_date = ss.state_date,
                    state_user = ss.state_user,

                    folder_name = ss.folder_name,
                    folder_level = ss.folder_level,
                    file_type_name = ss.file_type_name,
                    group_id = ss.group_id,
                    group_name = ss.group_name,
                    file_state_name = ss.file_state_name,                    
                    sprite_css_class = ss.sprite_css_class,
                    file_name_with_extention = ss.file_name_with_extention,
                    file_size_str = ss.file_size_str,
                    v1_exists = ss.v1_exists,
                    v1_file_version_id = ss.v1_file_version_id,
                    v1_file_size = ss.v1_file_size,
                    v1_physical_path = ss.v1_physical_path,
                    v1_physical_name = ss.v1_physical_name,
                    v1_download_link = ss.v1_download_link,
                    v1_crt_date = ss.v1_crt_date,
                    v1_crt_user = ss.v1_crt_user,
                    v1_file_size_str = ss.v1_file_size_str,
                    v1_version_num_actual = ss.v1_version_num_actual,
                    v2_exists = ss.v2_exists,
                    v2_file_version_id = ss.v2_file_version_id,
                    v2_file_size = ss.v2_file_size,
                    v2_physical_path = ss.v2_physical_path,
                    v2_physical_name = ss.v2_physical_name,
                    v2_download_link = ss.v2_download_link,
                    v2_crt_date = ss.v2_crt_date,
                    v2_crt_user = ss.v2_crt_user,
                    v2_file_size_str = ss.v2_file_size_str,
                    v2_version_num_actual = ss.v2_version_num_actual,
                    file_name_full = ss.file_name_full,
                    crt_date_str = ss.crt_date_str,
                    upd_date_str = ss.upd_date_str,
                    state_date_str = ss.state_date_str,
                    v1_file_name_full = ss.v1_file_name_full,
                    v1_crt_date_str = ss.v1_crt_date_str,
                    v1_upd_date_str = ss.v1_upd_date_str,
                    v2_file_name_full = ss.v2_file_name_full,
                    v2_crt_date_str = ss.v2_crt_date_str,
                    v2_upd_date_str = ss.v2_upd_date_str,
                    is_local_link = ss.is_local_link,
                    file_date = ss.file_date,
                    file_date_str = ss.file_date_str,
                    item_type = ss.item_type,

                    id = ss.file_id,
                    parent_id = ss.folder_id,
                    hasChildren = false,
                }
                )
                .OrderBy(ss => ss.file_name);
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_storage_file
                .Where(ss => ss.file_id == id)
                .Select(ss => new StorageFileViewModel()
                {
                    file_id = ss.file_id,
                    file_name = ss.file_name,
                    file_nom = ss.file_nom,
                    folder_id = ss.folder_id,
                    ord = ss.ord,                    
                    file_size = ss.file_size,
                    physical_path = ss.physical_path,
                    physical_name = ss.physical_name,
                    download_link = ss.download_link,
                    file_type_id = ss.file_type_id,
                    file_state_id = ss.file_state_id,
                    has_version = ss.has_version,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    upd_date = ss.upd_date,
                    upd_user = ss.upd_user,
                    state_date = ss.state_date,
                    state_user = ss.state_user,

                    folder_name = ss.folder_name,
                    folder_level = ss.folder_level,
                    file_type_name = ss.file_type_name,
                    group_id = ss.group_id,
                    group_name = ss.group_name,
                    file_state_name = ss.file_state_name,
                    sprite_css_class = ss.sprite_css_class,
                    file_name_with_extention = ss.file_name_with_extention,
                    v1_exists = ss.v1_exists,
                    v1_file_version_id = ss.v1_file_version_id,
                    v1_file_size = ss.v1_file_size,
                    v1_physical_path = ss.v1_physical_path,
                    v1_physical_name = ss.v1_physical_name,
                    v1_download_link = ss.v1_download_link,
                    v1_crt_date = ss.v1_crt_date,
                    v1_crt_user = ss.v1_crt_user,
                    v1_version_num_actual = ss.v1_version_num_actual,
                    v2_exists = ss.v2_exists,
                    v2_file_version_id = ss.v2_file_version_id,
                    v2_file_size = ss.v2_file_size,
                    v2_physical_path = ss.v2_physical_path,
                    v2_physical_name = ss.v2_physical_name,
                    v2_download_link = ss.v2_download_link,
                    v2_crt_date = ss.v2_crt_date,
                    v2_crt_user = ss.v2_crt_user,
                    v2_version_num_actual = ss.v2_version_num_actual,
                    file_name_full = ss.file_name_full,
                    crt_date_str = ss.crt_date_str,
                    upd_date_str = ss.upd_date_str,
                    state_date_str = ss.state_date_str,
                    file_size_str = ss.file_size_str,
                    v1_file_name_full = ss.v1_file_name_full,
                    v1_crt_date_str = ss.v1_crt_date_str,
                    v1_upd_date_str = ss.v1_upd_date_str,
                    v1_file_size_str = ss.v1_file_size_str,
                    v2_file_name_full = ss.v2_file_name_full,
                    v2_crt_date_str = ss.v2_crt_date_str,
                    v2_upd_date_str = ss.v2_upd_date_str,
                    v2_file_size_str = ss.v2_file_size_str,
                    is_local_link = ss.is_local_link,
                    file_date = ss.file_date,
                    file_date_str = ss.file_date_str,
                    item_type = ss.item_type,

                    id = ss.file_id,
                    parent_id = ss.folder_id,
                    hasChildren = false,
                }
                )
                .FirstOrDefault();
        }
      
        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_storage_file
                .Where(ss => ss.folder_id == parent_id)
                .Select(ss => new StorageFileViewModel()
                {
                    file_id = ss.file_id,
                    file_name = ss.file_name,
                    file_nom = ss.file_nom,
                    folder_id = ss.folder_id,
                    ord = ss.ord,                    
                    file_size = ss.file_size,
                    physical_path = ss.physical_path,
                    physical_name = ss.physical_name,
                    download_link = ss.download_link,
                    file_type_id = ss.file_type_id,
                    file_state_id = ss.file_state_id,
                    has_version = ss.has_version,
                    crt_date = ss.crt_date,
                    crt_user = ss.crt_user,
                    upd_date = ss.upd_date,
                    upd_user = ss.upd_user,
                    state_date = ss.state_date,
                    state_user = ss.state_user,

                    folder_name = ss.folder_name,
                    folder_level = ss.folder_level,
                    file_type_name = ss.file_type_name,
                    group_id = ss.group_id,
                    group_name = ss.group_name,
                    file_state_name = ss.file_state_name,
                    sprite_css_class = ss.sprite_css_class,
                    file_name_with_extention = ss.file_name_with_extention,
                    v1_exists = ss.v1_exists,
                    v1_file_version_id = ss.v1_file_version_id,
                    v1_file_size = ss.v1_file_size,
                    v1_physical_path = ss.v1_physical_path,
                    v1_physical_name = ss.v1_physical_name,
                    v1_download_link = ss.v1_download_link,
                    v1_crt_date = ss.v1_crt_date,
                    v1_crt_user = ss.v1_crt_user,
                    v1_version_num_actual = ss.v1_version_num_actual,
                    v2_exists = ss.v2_exists,
                    v2_file_version_id = ss.v2_file_version_id,
                    v2_file_size = ss.v2_file_size,
                    v2_physical_path = ss.v2_physical_path,
                    v2_physical_name = ss.v2_physical_name,
                    v2_download_link = ss.v2_download_link,
                    v2_crt_date = ss.v2_crt_date,
                    v2_crt_user = ss.v2_crt_user,
                    v2_version_num_actual = ss.v2_version_num_actual,
                    file_name_full = ss.file_name_full,
                    crt_date_str = ss.crt_date_str,
                    upd_date_str = ss.upd_date_str,
                    state_date_str = ss.state_date_str,
                    file_size_str = ss.file_size_str,
                    v1_file_name_full = ss.v1_file_name_full,
                    v1_crt_date_str = ss.v1_crt_date_str,
                    v1_upd_date_str = ss.v1_upd_date_str,
                    v1_file_size_str = ss.v1_file_size_str,
                    v2_file_name_full = ss.v2_file_name_full,
                    v2_crt_date_str = ss.v2_crt_date_str,
                    v2_upd_date_str = ss.v2_upd_date_str,
                    v2_file_size_str = ss.v2_file_size_str,
                    is_local_link = ss.is_local_link,
                    file_date = ss.file_date,
                    file_date_str = ss.file_date_str,
                    item_type = ss.item_type,

                    id = ss.file_id,
                    parent_id = ss.folder_id,
                    hasChildren = false,
                }
                )
                .OrderBy(ss => ss.file_name);
        }

        public List<StorageFileSimpleViewModel> Find(string item_name)
        {
            if (String.IsNullOrEmpty(item_name))
                return null;

            List<StorageFileSimpleViewModel> res = new List<StorageFileSimpleViewModel>();

            res.AddRange(dbContext.storage_folder
                    .Where(ss => ss.folder_name.Trim().ToLower().Contains(item_name.Trim().ToLower()))
                    .Select(ss => new StorageFileSimpleViewModel()
                    {
                        file_id = 0,
                        folder_id = ss.folder_id,
                    })                    
                    .ToList()
                    );
            string item_ext = "";
            if (item_name.EndsWith(".*"))
            {
                item_name = item_name.Substring(0, item_name.LastIndexOf('*'));
            }
            if (item_name.StartsWith("*."))
            {
                item_ext = item_name.Substring(item_name.LastIndexOf('.') + 1);
                if (String.IsNullOrEmpty(item_ext) || (item_ext.Trim().ToLower().Equals("*")))
                    return null;
            }
            if (String.IsNullOrEmpty(item_ext))
            {
                res.AddRange(dbContext.vw_storage_file
                        .Where(ss => ss.file_name_with_extention.Trim().ToLower().Contains(item_name.Trim().ToLower()))
                        .Select(ss => new StorageFileSimpleViewModel()
                        {
                            file_id = ss.file_id,
                            folder_id = ss.folder_id,
                        })
                        .ToList()
                        );
            }
            else
            {
                res.AddRange(dbContext.vw_storage_file
                        .Where(ss => ss.file_type_name.Trim().ToLower().Equals(item_ext.Trim().ToLower()))
                        .Select(ss => new StorageFileSimpleViewModel()
                        {
                            file_id = ss.file_id,
                            folder_id = ss.folder_id,
                        })
                        .ToList()
                        );
            }

            return res.OrderBy(ss => ss.folder_id).ThenBy(ss => ss.file_id).ToList();
        }

        private void xToLog_Storage(string mess, int? file_id, int? folder_id)
        {
            IStorageLogService storageLogService = DependencyResolver.Current.GetService<IStorageLogService>();
            storageLogService.ToLog_Storage(mess, file_id, folder_id);
        }

        public StorageFileViewModel UploadFile(HttpPostedFileBase postedFile, int folder_id, bool overwrite, string file_date, ModelStateDictionary modelState)
        {
            if (postedFile == null)
            {
                modelState.AddModelError("", "Не указан файл");
                return null;
            }

            var storageParamService = DependencyResolver.Current.GetService<IStorageParamService>();
            StorageParamViewModel param = (StorageParamViewModel)storageParamService.GetItem((int)Enums.StorageEnum.MAIN);
            long max_file_size = 0;
            bool parseOk = long.TryParse(param.max_file_size, out max_file_size);
            if ((parseOk) && (postedFile.ContentLength > max_file_size))
            {
                modelState.AddModelError("", "Файл превышает допустимый размер (" + max_file_size.ToString() + ")");
                return null;
            }

            var storageFileTypeService = DependencyResolver.Current.GetService<IStorageFileTypeService>();            
            List<StorageFileTypeViewModel> fileTypeList = storageFileTypeService.GetList().AsEnumerable().Cast<StorageFileTypeViewModel>().ToList();

            string fileExt = Path.GetExtension(postedFile.FileName);
            var fileType = fileTypeList.Where(ss => ss.file_type_name.TrimStart('.').ToLower().Equals(fileExt.TrimStart('.').ToLower())).FirstOrDefault();
            if (fileType == null)
            {
                modelState.AddModelError("", "Недопустимый тип файла (" + fileExt + ")");
                return null;
            }            

            string fileSize = postedFile.ContentLength.ToString();
            string fileName_withoutExtention = Path.GetFileNameWithoutExtension(postedFile.FileName);

            string fileName_withoutExtention_actual = fileName_withoutExtention;
            bool isEndsWithStoragePattern = false;
            isEndsWithStoragePattern = Regex.Match(fileName_withoutExtention_actual, @"\d{18}$").Success;
            if (!isEndsWithStoragePattern)
            {
                string fileName_withoutExtention_tmp = fileName_withoutExtention_actual.Substring(0, fileName_withoutExtention_actual.Length - 1);
                if (fileName_withoutExtention_tmp.EndsWith("-"))
                {
                    fileName_withoutExtention_tmp = fileName_withoutExtention_tmp.Substring(0, fileName_withoutExtention_tmp.Length - 1);
                    isEndsWithStoragePattern = Regex.Match(fileName_withoutExtention_tmp, @"\d{18}$").Success;
                    if (isEndsWithStoragePattern)
                    {
                        fileName_withoutExtention_actual = fileName_withoutExtention_tmp;
                    }
                }
                else if (fileName_withoutExtention_tmp.EndsWith(")"))
                {
                    fileName_withoutExtention_tmp = fileName_withoutExtention_tmp.Substring(0, fileName_withoutExtention_tmp.Length - 3);
                    isEndsWithStoragePattern = Regex.Match(fileName_withoutExtention_tmp, @"\d{18}$").Success;
                    if (isEndsWithStoragePattern)
                    {
                        fileName_withoutExtention_actual = fileName_withoutExtention_tmp;
                    }
                }
            }
            if (isEndsWithStoragePattern)
            {
                try
                {
                    var fileName_Parsed = GlobalUtil.ParseStorageFileName(fileName_withoutExtention_actual + Path.GetExtension(postedFile.FileName));  
                    
                    string fileName_date = fileName_Parsed.Item3;
                    DateTime fileName_date_DateTime = DateTime.ParseExact(fileName_date, "ddMMyyHHmmss", System.Globalization.CultureInfo.CurrentCulture);
                    string fileName_user = fileName_Parsed.Item4;
                    if (fileName_user.Trim().Length > 2)
                    {
                        throw new Exception("fileName_user.Trim().Length > 2");
                    }
                    string fileName_version = fileName_Parsed.Item5;
                    if (fileName_version.Trim().Length > 2)
                    {
                        throw new Exception("fileName_version.Trim().Length > 2");
                    }

                    fileName_withoutExtention_actual = fileName_Parsed.Item1;
                    //postedFile.FileName = fileName_withoutExtention_actual + Path.GetExtension(postedFile.FileName);
                }
                catch
                {
                    fileName_withoutExtention_actual = fileName_withoutExtention;
                }
            }

            Tuple<bool, string, string> uploadResult;
            var existing_file = dbContext.storage_file.Where(ss => ss.folder_id == folder_id && !ss.is_deleted && ss.file_type_id == fileType.file_type_id && ss.file_name.Trim().ToLower().Equals(fileName_withoutExtention_actual.Trim().ToLower())).FirstOrDefault();
            if (existing_file != null)
            {
                if (!overwrite)
                {
                    modelState.AddModelError("", "Уже есть такой файл в этой папке");
                    return null;
                }
                StorageFileViewModel existingFile = (StorageFileViewModel)GetItem(existing_file.file_id);
                uploadResult = xUploadFile_Overwrite(postedFile, existingFile, modelState);
            }
            else
            {
                uploadResult = xUploadFile_New(postedFile, fileName_withoutExtention_actual + Path.GetExtension(postedFile.FileName), modelState);
            }

            if ((!uploadResult.Item1) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при загрузке файла");
                return null;
            }

            string fileName_fullPhysicalPathAndName = Path.Combine(uploadResult.Item2, uploadResult.Item3);
            //string downloadLink = ViewModelUtil.GetStorageFileLink() + currUser.CabUserId.ToString() + @"/" + uploadResult.Item3;
            DateTime now = DateTime.Now;
            DateTime? file_date_parsed = null;
            try
            {
                file_date_parsed = String.IsNullOrEmpty(file_date) ? null : (DateTime?)DateTime.ParseExact(file_date.Substring(0, 24), "ddd MMM d yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            }
            catch
            {
                file_date_parsed = null;
            }
            StorageFileViewModel itemEdit = new StorageFileViewModel()
                {
                    file_id = existing_file != null ? existing_file.file_id : 0,
                    file_name = fileName_withoutExtention_actual,
                    file_nom = 0,                    
                    folder_id = folder_id,
                    ord = 0,                    
                    file_size = fileSize,
                    physical_path = uploadResult.Item2,
                    physical_name = uploadResult.Item3,
                    download_link = "",                    
                    file_type_id = fileType.file_type_id,
                    file_state_id = (int)Enums.StorageFileStateEnum.ENABLED,
                    has_version = existing_file != null,
                    file_date = file_date_parsed,
                };

            StorageFileViewModel res = null;
            if (existing_file != null)
                res = (StorageFileViewModel)UpdateProperties(itemEdit, modelState);
            else
                res = (StorageFileViewModel)Insert(itemEdit, modelState);

            if ((res == null) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при добавлении файла");
                return null;
            }

            xToLog_Storage((existing_file != null ? ("Перезаписан файл " + res.file_name) : ("Загружен файл " + res.file_name)), res.file_id, res.folder_id);
            return res;
        }

        private Tuple<bool, string, string> xUploadFile_New(HttpPostedFileBase uploadingFile, string fileName, ModelStateDictionary modelState)
        {
            DateTime now = DateTime.Now;            
            string filePath_physical = ViewModelUtil.GetStorageFilePath() + @"\" + currUser.CabUserId.ToString();

            string fileName_withoutExtention = "";
            string fileName_Extention = "";
            string fileName_physical = "";

            //var fileNameBuildResult = GlobalUtil.BuildStorageFileName(uploadingFile.FileName, currUser.CabUserId, 0);
            var fileNameBuildResult = GlobalUtil.BuildStorageFileName(fileName, currUser.CabUserId, 0);
            fileName_withoutExtention = fileNameBuildResult.Item2;
            fileName_Extention = fileNameBuildResult.Item3;
            fileName_physical = fileNameBuildResult.Item4;

            return xUploadFile(uploadingFile, filePath_physical, fileName_physical, modelState);
        }

        private Tuple<bool, string, string> xUploadFile_Overwrite(HttpPostedFileBase uploadingFile, StorageFileViewModel existingFile, ModelStateDictionary modelState)
        {
            try
            {
                DateTime now = DateTime.Now;
                string filePath_physical = ViewModelUtil.GetStorageFilePath() + @"\" + currUser.CabUserId.ToString();

                var fileNameParsed = GlobalUtil.ParseStorageFileName(existingFile.physical_name);
                string fileName_physical = existingFile.physical_name;
                string fileName_withoutExtention = fileNameParsed.Item1;
                string fileName_Extention = fileNameParsed.Item2;
                string fileName_date = fileNameParsed.Item3;
                string fileName_user = fileNameParsed.Item4;
                string fileName_version = fileNameParsed.Item5;

                int version_num_actual_new = 1;
                int version_num_new = 1;
                var max_version = dbContext.storage_file_version.Where(ss => ss.file_id == existingFile.file_id).OrderByDescending(ss => ss.version_num_actual).FirstOrDefault();
                if (max_version != null)
                {
                    version_num_actual_new = max_version.version_num_actual + 1;
                    version_num_new = 2;

                    if (max_version.version_num >= 2)
                    {                        
                        List<storage_file_version> exceeded_versions_to_del = dbContext.storage_file_version.Where(ss => ss.file_id == existingFile.file_id && ss.version_num > 2).ToList();
                        if ((exceeded_versions_to_del != null) && (exceeded_versions_to_del.Count > 0))
                        {
                            foreach (var exceeded_version_to_del in exceeded_versions_to_del)
                            {
                                xDeleteFile(Path.Combine(exceeded_version_to_del.physical_path, exceeded_version_to_del.physical_name), modelState);
                            }
                            dbContext.storage_file_version.RemoveRange(exceeded_versions_to_del);
                        }

                        storage_file_version first_version = dbContext.storage_file_version.Where(ss => ss.file_id == existingFile.file_id && ss.version_num == 1).FirstOrDefault();
                        xDeleteFile(Path.Combine(first_version.physical_path, first_version.physical_name), modelState);
                        dbContext.storage_file_version.Remove(first_version);

                        var maxVersion_fileNameParsed = GlobalUtil.ParseStorageFileName(max_version.physical_name);
                        string maxVersion_fileName_physical = max_version.physical_name;
                        string maxVersion_fileName_withoutExtention = maxVersion_fileNameParsed.Item1;
                        string maxVersion_fileName_Extention = maxVersion_fileNameParsed.Item2;
                        string maxVersion_fileName_date = maxVersion_fileNameParsed.Item3;
                        string maxVersion_fileName_user = maxVersion_fileNameParsed.Item4;
                        string maxVersion_fileName_version = maxVersion_fileNameParsed.Item5;                        

                        var first_version_physical_name_tuple_new = GlobalUtil.BuildStorageFileName(maxVersion_fileName_withoutExtention, maxVersion_fileName_withoutExtention, maxVersion_fileName_Extention, maxVersion_fileName_date, maxVersion_fileName_user, 1.ToString());
                        string first_version_physical_name_new = first_version_physical_name_tuple_new.Item4;

                        xMoveFile(Path.Combine(max_version.physical_path, max_version.physical_name), Path.Combine(max_version.physical_path, first_version_physical_name_new), modelState);

                        max_version.version_num = 1;
                        max_version.physical_name = first_version_physical_name_new;
                        max_version.download_link = ViewModelUtil.GetStorageFileLink() + max_version.file_id.ToString() + "&version=1";
                        max_version.upd_date = now;
                        max_version.upd_user = currUser.CabUserName;

                        dbContext.SaveChanges();
                    }
                }
              
                var physical_name_tuple_new = GlobalUtil.BuildStorageFileName(fileName_withoutExtention, fileName_withoutExtention, fileName_Extention, fileName_date, fileName_user, version_num_new.ToString());
                string physical_name_new = physical_name_tuple_new.Item4;
                bool moveFileResult = xMoveFile(Path.Combine(existingFile.physical_path, existingFile.physical_name), Path.Combine(existingFile.physical_path, physical_name_new), modelState);
                if ((!moveFileResult) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при физическом переносе существующего файла");
                    return new Tuple<bool, string, string>(false, "", "");
                }

                storage_file_version storage_file_version = new storage_file_version();
                storage_file_version.crt_date = now;
                storage_file_version.crt_user = currUser.CabUserName;
                storage_file_version.download_link = ViewModelUtil.GetStorageFileLink() + existingFile.file_id.ToString() + "&version=" + version_num_new.ToString();
                storage_file_version.file_id = existingFile.file_id;
                storage_file_version.file_size = existingFile.file_size;
                storage_file_version.physical_name = physical_name_new;
                storage_file_version.physical_path = existingFile.physical_path;
                storage_file_version.version_num = version_num_new; // 1 или 2
                storage_file_version.version_num_actual = version_num_actual_new;
                dbContext.storage_file_version.Add(storage_file_version);

                dbContext.SaveChanges();

                return xUploadFile(uploadingFile, filePath_physical, fileName_physical, modelState);
            }
            catch (Exception ex)
            {
                modelState.AddModelError("", GlobalUtil.ExceptionInfo(ex));
                return new Tuple<bool, string, string>(false, "", "");
            }
        }

        private Tuple<bool, string, string> xUploadFile(HttpPostedFileBase uploadingFile, string filePath, string fileName, ModelStateDictionary modelState)
        {
            try
            {                
                return GlobalUtil.UploadFile(uploadingFile, ViewModelUtil.GetStorageFilePath(), filePath, fileName);
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                ToLog("Ошибка при добавлении файла", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return new Tuple<bool, string, string>(false, "", "");
            }
        }

        private bool xMoveFile(string fileSrcPath, string fileDestPath, ModelStateDictionary modelState)
        {
            try
            {
                return GlobalUtil.MoveFile(ViewModelUtil.GetStorageFilePath(), fileSrcPath, fileDestPath);
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                ToLog("Ошибка при переносе файла", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            }
        }

        private bool xDeleteFile(string filePath, ModelStateDictionary modelState)
        {            
            try
            {
                return GlobalUtil.DeleteFile(ViewModelUtil.GetStorageFilePath(), filePath);
            }
            catch (Exception ex)
            {
                string ex_info = GlobalUtil.ExceptionInfo(ex);
                ToLog("Ошибка при удалении", (long)Enums.LogEventType.CABINET, new LogObject(new StorageFileViewModel()), null, ex_info);
                modelState.AddModelError("", "Ошибка: " + ex_info);
                return false;
            } 
        }

        public bool Move(int file_id, int folder_id, ModelStateDictionary modelState)
        {
            var moved_file = dbContext.storage_file.Where(ss => ss.file_id == file_id).FirstOrDefault();            
            var parent_folder = dbContext.storage_folder.Where(ss => ss.folder_id == folder_id).FirstOrDefault();

            var existing_file = dbContext.storage_file.Where(ss => ss.folder_id == folder_id
                && !ss.is_deleted
                && ss.file_id != moved_file.file_id
                && ss.file_name.Trim().ToLower().Equals(moved_file.file_name.Trim().ToLower()))
                .FirstOrDefault();
            if (existing_file != null)
            {
                modelState.AddModelError("", "В этой папке уже есть такой файл");
                return false;
            }

            var old_folder = dbContext.storage_folder.Where(ss => ss.folder_id == moved_file.folder_id).FirstOrDefault();
            moved_file.folder_id = folder_id;            
            dbContext.SaveChanges();

            xToLog_Storage("Файл " + moved_file.file_name + " перемещен из папки " + old_folder.folder_name + " в папку " + parent_folder.folder_name, moved_file.file_id, moved_file.folder_id);
            return true;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFileViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageFileViewModel itemAdd = (StorageFileViewModel)item;
            if (itemAdd == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            DateTime now = DateTime.Now;            

            storage_file storage_file = new storage_file();
            storage_file.file_name = itemAdd.file_name;
            storage_file.file_nom = 1;
            storage_file.folder_id = itemAdd.folder_id;
            storage_file.ord = 0;            
            storage_file.file_size = itemAdd.file_size;
            storage_file.physical_path = itemAdd.physical_path;
            storage_file.physical_name = itemAdd.physical_name;
            //storage_file.download_link = itemAdd.download_link;            
            storage_file.file_type_id = itemAdd.file_type_id;
            storage_file.file_state_id = itemAdd.file_state_id;
            storage_file.has_version = false;
            storage_file.crt_date = now;
            storage_file.crt_user = currUser.CabUserName;
            storage_file.upd_date = null;
            storage_file.upd_user = null;
            storage_file.state_date = null;
            storage_file.state_user = null;
            storage_file.file_date = itemAdd.file_date;
            dbContext.storage_file.Add(storage_file);

            dbContext.SaveChanges();

            storage_file.download_link = ViewModelUtil.GetStorageFileLink() + storage_file.file_id.ToString();
            dbContext.SaveChanges();

            return GetItem(storage_file.file_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFileViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageFileViewModel itemEdit = (StorageFileViewModel)item;
            if (itemEdit == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            storage_file storage_file = dbContext.storage_file.Where(ss => ss.file_id == itemEdit.file_id).FirstOrDefault();
            if (storage_file == null)
            {
                modelState.AddModelError("", "Не найден файл " + itemEdit.file_id.ToString());
                return null;
            }

            storage_file storage_file_existing = dbContext.storage_file.Where(ss => ss.file_id != itemEdit.file_id
                && !ss.is_deleted
                && ss.folder_id == itemEdit.folder_id
                && ss.file_type_id == itemEdit.file_type_id
                && ss.file_name.Trim().ToLower().Equals(itemEdit.file_name.Trim().ToLower())).FirstOrDefault();
            if (storage_file_existing != null)
            {
                modelState.AddModelError("", "В этой папке уже есть такой файл");
                return null;
            }

            if (storage_file.file_name.Trim().ToLower().Equals(itemEdit.file_name.Trim().ToLower()))
            {
                return itemEdit;
            }            

            modelBeforeChanges = new StorageFileViewModel()
            {
                file_id = storage_file.file_id,
                file_name = storage_file.file_name,
                file_nom = storage_file.file_nom,
                folder_id = storage_file.folder_id,
                ord = storage_file.ord,                
                file_size = storage_file.file_size,
                physical_path = storage_file.physical_path,
                physical_name = storage_file.physical_name,
                download_link = storage_file.download_link,
                file_type_id = storage_file.file_type_id,
                file_state_id = storage_file.file_state_id,
                has_version = storage_file.has_version,
                crt_date = storage_file.crt_date,
                crt_user = storage_file.crt_user,
                upd_date = storage_file.upd_date,
                upd_user = storage_file.upd_user,
                state_date = storage_file.state_date,
                state_user = storage_file.state_user,
                file_date = storage_file.file_date,
            };

            DateTime now = DateTime.Now;

            var fileName_parsed = GlobalUtil.ParseStorageFileName(storage_file.physical_name);
            var fileName_withoutExtention = fileName_parsed.Item1;
            var fileName_extention = fileName_parsed.Item2;
            var fileName_date = fileName_parsed.Item3;
            var fileName_user = fileName_parsed.Item4;
            var fileName_version = fileName_parsed.Item5;

            var fileName_new = GlobalUtil.BuildStorageFileName(storage_file.file_name + "." + fileName_extention, itemEdit.file_name, fileName_extention, fileName_date, fileName_user, fileName_version);

            string old_file_name = storage_file.file_name;
            storage_file.file_name = itemEdit.file_name;
            storage_file.physical_name = fileName_new.Item4;
            //int download_link_lastSlashIndex = storage_file.download_link.LastIndexOf('/');
            //storage_file.download_link = storage_file.download_link.Substring(0, download_link_lastSlashIndex) + "/" + storage_file.physical_name;
                
            storage_file.upd_date = now;
            storage_file.upd_user = currUser.CabUserName;

            storage_file.file_date = itemEdit.file_date;

            var res1 = xMoveFile(Path.Combine(itemEdit.physical_path, itemEdit.physical_name), Path.Combine(storage_file.physical_path, storage_file.physical_name),modelState);
            if ((!res1) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при физическом перемещении файла");
            }

            dbContext.SaveChanges();

            var versions = dbContext.storage_file_version.Where(ss => ss.file_id == itemEdit.file_id).ToList();
            if ((versions != null) && (versions.Count > 0))
            {
                var versionServ = DependencyResolver.Current.GetService<IStorageFileVersionService>();
                foreach (var version in versions)
                {
                    var resVersion = versionServ.Update(new StorageFileVersionViewModel() {
                        file_version_id = version.file_version_id,
                        file_id = version.file_id,
                        version_num = version.version_num,
                        file_size = version.file_size,
                        physical_path = version.physical_path,
                        physical_name = version.physical_name,
                        //download_link = version.download_link,
                        crt_date = version.crt_date,
                        crt_user = version.crt_user,
                        upd_date = version.upd_date,
                        upd_user = version.upd_user,
                    }, modelState);
                    if ((resVersion == null) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при переименовании версии файла");
                        return null;
                    }
                }
            }

            xToLog_Storage("Файл " + old_file_name + " переименован в " + itemEdit.file_name, storage_file.file_id, storage_file.folder_id);

            return GetItem(storage_file.file_id);
        }

        public StorageFileViewModel UpdateProperties(StorageFileViewModel itemEdit, ModelStateDictionary modelState)
        {
            storage_file storage_file = dbContext.storage_file.Where(ss => ss.file_id == itemEdit.file_id).FirstOrDefault();
            if (storage_file == null)
            {
                modelState.AddModelError("", "Не найден файл " + itemEdit.file_id.ToString());
                return null;
            }

            storage_file storage_file_existing = dbContext.storage_file.Where(ss => ss.file_id != itemEdit.file_id
                && !ss.is_deleted
                && ss.folder_id == itemEdit.folder_id
                && ss.file_type_id == itemEdit.file_type_id
                && ss.file_name.Trim().ToLower().Equals(itemEdit.file_name.Trim().ToLower())).FirstOrDefault();
            if (storage_file_existing != null)
            {
                modelState.AddModelError("", "В этой папке уже есть такой файл");
                return null;
            }

            DateTime now = DateTime.Now;
            storage_file.file_name = itemEdit.file_name;
            storage_file.folder_id = itemEdit.folder_id;
            storage_file.file_size = itemEdit.file_size;
            storage_file.physical_path = itemEdit.physical_path;
            storage_file.physical_name = itemEdit.physical_name;
            storage_file.file_type_id = itemEdit.file_type_id;
            storage_file.file_state_id = itemEdit.file_state_id;
            storage_file.has_version = itemEdit.has_version;                        
            storage_file.download_link = ViewModelUtil.GetStorageFileLink() + storage_file.file_id.ToString();
            storage_file.upd_date = now;
            storage_file.upd_user = currUser.CabUserName;
            dbContext.SaveChanges();

            return (StorageFileViewModel)GetItem(storage_file.file_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFileViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            StorageFileViewModel itemDel = (StorageFileViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return false;
            }

            storage_file storage_file = dbContext.storage_file.Where(ss => ss.file_id == itemDel.file_id).FirstOrDefault();
            if (storage_file == null)
            {
                modelState.AddModelError("", "Не найден файл с кодом " + itemDel.file_id.ToString());
                return false;
            }

            DateTime now = DateTime.Now;
            
            /*
            if (!itemDel.forbidDelPhysicalFile)
            {
                var res1 = xDeleteFile(Path.Combine(storage_file.physical_path, storage_file.physical_name), modelState);
                if ((!res1) || (!modelState.IsValid))
                {
                    if (modelState.IsValid)
                        modelState.AddModelError("", "Ошибка при физическом удалении файла");
                }
            }

            var versions = dbContext.storage_file_version.Where(ss => ss.file_id == itemDel.file_id).ToList();
            if ((versions != null) && (versions.Count > 0))
            {
                var versionServ = DependencyResolver.Current.GetService<IStorageFileVersionService>();
                foreach (var version in versions)
                {
                    var resVersion = versionServ.Delete(new StorageFileVersionViewModel()
                    {
                        file_version_id = version.file_version_id,
                        file_id = version.file_id,
                        version_num = version.version_num,
                        file_size = version.file_size,
                        physical_path = version.physical_path,
                        physical_name = version.physical_name,
                        //download_link = version.download_link,
                        crt_date = version.crt_date,
                        crt_user = version.crt_user,
                        upd_date = version.upd_date,
                        upd_user = version.upd_user,
                        forbidDelPhysicalFile = itemDel.forbidDelPhysicalFile,
                    }, modelState);
                    if ((resVersion == null) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при удалении версии файла");
                        return false;
                    }
                }
            }


            string old_file_name = storage_file.file_name;
            int old_file_id = storage_file.file_id;
            int old_folder_id = storage_file.folder_id;

            using (AuMainDb newContext = new AuMainDb())
            {
                newContext.storage_file.Remove(newContext.storage_file.Where(ss => ss.file_id == old_file_id).FirstOrDefault());
                newContext.SaveChanges();
            }
            */

            string old_file_name = storage_file.file_name;
            int old_file_id = storage_file.file_id;
            int old_folder_id = storage_file.folder_id;

            storage_file.is_deleted = true;
            storage_file.del_date = now;
            storage_file.del_user = currUser.CabUserName;
            dbContext.SaveChanges();

            xToLog_Storage("Файл " + old_file_name + " удален", old_file_id, old_folder_id);

            return true;
        }

        public bool Remove(int file_id, ModelStateDictionary modelState)
        {
            storage_file storage_file = dbContext.storage_file.Where(ss => ss.file_id == file_id).FirstOrDefault();
            if (storage_file == null)
            {
                modelState.AddModelError("", "Не найден файл с кодом " + file_id.ToString());
                return false;
            }

            DateTime now = DateTime.Now;

            var res1 = xDeleteFile(Path.Combine(storage_file.physical_path, storage_file.physical_name), modelState);
            if ((!res1) || (!modelState.IsValid))
            {
                if (modelState.IsValid)
                    modelState.AddModelError("", "Ошибка при физическом удалении файла");
            }

            var versions = dbContext.storage_file_version.Where(ss => ss.file_id == file_id).ToList();
            if ((versions != null) && (versions.Count > 0))
            {
                var versionServ = DependencyResolver.Current.GetService<IStorageFileVersionService>();
                foreach (var version in versions)
                {
                    var resVersion = versionServ.Delete(new StorageFileVersionViewModel()
                    {
                        file_version_id = version.file_version_id,
                        file_id = version.file_id,
                        version_num = version.version_num,
                        file_size = version.file_size,
                        physical_path = version.physical_path,
                        physical_name = version.physical_name,
                        //download_link = version.download_link,
                        crt_date = version.crt_date,
                        crt_user = version.crt_user,
                        upd_date = version.upd_date,
                        upd_user = version.upd_user,
                        //forbidDelPhysicalFile = itemDel.forbidDelPhysicalFile,
                        forbidDelPhysicalFile = false,
                    }, modelState);
                    if ((resVersion == null) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при удалении версии файла");
                        return false;
                    }
                }
            }

            string old_file_name = storage_file.file_name;
            int old_file_id = storage_file.file_id;
            int old_folder_id = storage_file.folder_id;

            using (AuMainDb newContext = new AuMainDb())
            {
                newContext.storage_file.Remove(newContext.storage_file.Where(ss => ss.file_id == old_file_id).FirstOrDefault());
                newContext.SaveChanges();
            }

            xToLog_Storage("Файл " + old_file_name + " удален", old_file_id, old_folder_id);

            return true;
        }

        public bool CheckExists(string fileName, int folder_id)
        {
            return true;
        }
    }
}
