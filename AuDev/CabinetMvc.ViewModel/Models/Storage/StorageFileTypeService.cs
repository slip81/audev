﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageFileTypeService : IAuBaseService
    {
        //
    }

    public class StorageFileTypeService : AuBaseService, IStorageFileTypeService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.storage_file_type
                .Where(ss => ss.file_type_id > 0)
                .ProjectTo<StorageFileTypeViewModel>();        
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.storage_file_type
                .Where(ss => ss.file_type_id == id)
                .ProjectTo<StorageFileTypeViewModel>()
                .FirstOrDefault();
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFileTypeViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageFileTypeViewModel itemEdit = (StorageFileTypeViewModel)item;
            if ((itemEdit == null) || (String.IsNullOrEmpty(itemEdit.file_type_name)))
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            storage_file_type storage_file_type_existing = dbContext.storage_file_type.Where(ss => ss.file_type_name.Trim().ToLower().Equals(itemEdit.file_type_name.Trim().ToLower())).FirstOrDefault();
            if (storage_file_type_existing != null)
            {
                modelState.AddModelError("", "Уже есть файл такого типа");
                return null;
            }

            int? group_id = itemEdit.FileTypeGroup != null ? (int?)itemEdit.FileTypeGroup.group_id : null;
            var max_item = dbContext.storage_file_type.OrderByDescending(ss => ss.file_type_id).FirstOrDefault();
            int file_type_id = max_item != null ? max_item.file_type_id + 1 : 1;
            storage_file_type storage_file_type = new storage_file_type();
            storage_file_type.file_type_id = file_type_id;
            storage_file_type.file_type_name = itemEdit.file_type_name;
            storage_file_type.group_id = group_id.GetValueOrDefault(1);
            dbContext.storage_file_type.Add(storage_file_type);
            dbContext.SaveChanges();

            return GetItem(storage_file_type.file_type_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageFileTypeViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            StorageFileTypeViewModel itemDel = (StorageFileTypeViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return false;
            }

            storage_file_type storage_file_type = dbContext.storage_file_type.Where(ss => ss.file_type_id == itemDel.file_type_id).FirstOrDefault();
            if (storage_file_type == null)
            {
                modelState.AddModelError("", "Не найден тип файла с кодом " + itemDel.file_type_id.ToString());
                return false;
            }

            storage_file storage_file_existing = dbContext.storage_file.Where(ss => ss.file_type_id == itemDel.file_type_id).FirstOrDefault();
            if (storage_file_existing != null)
            {
                modelState.AddModelError("", "Есть файлы такого типа");
                return false;
            }

            dbContext.storage_file_type.Remove(storage_file_type);
            dbContext.SaveChanges();

            return true;
        }

    }
}