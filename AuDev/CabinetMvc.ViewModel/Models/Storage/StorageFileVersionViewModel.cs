﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;

namespace CabinetMvc.ViewModel.Storage
{
    public class StorageFileVersionViewModel : AuBaseViewModel
    {

        public StorageFileVersionViewModel()
            : base(Enums.MainObjectType.STORAGE_FILE_VERSION)
        {            
            //storage_file_version
            forbidDelPhysicalFile = false;
        }        

        [Key()]
        [Display(Name = "Код")]
        public int file_version_id { get; set; }

        [Display(Name = "Файл")]
        public int file_id { get; set; }

        [Display(Name = "Номер версии")]
        public int version_num { get; set; }

        [Display(Name = "Размер")]
        public string file_size { get; set; }

        [Display(Name = "Физический путь")]
        public string physical_path { get; set; }

        [Display(Name = "Физическое имя")]
        public string physical_name { get; set; }

        [Display(Name = "Ссылка")]
        public string download_link { get; set; }

        [Display(Name = "Дата создания")]
        public Nullable<System.DateTime> crt_date { get; set; }

        [Display(Name = "Автор создания")]
        public string crt_user { get; set; }

        [Display(Name = "Дата изменеия")]
        public Nullable<System.DateTime> upd_date { get; set; }

        [Display(Name = "Автор изменения")]
        public string upd_user { get; set; }

        [Display(Name = "Номер версии реальный")]
        public int version_num_actual { get; set; }

        // additional
        [Display(Name = "Не удалять физически")]
        public bool forbidDelPhysicalFile { get; set; }
    }
}

