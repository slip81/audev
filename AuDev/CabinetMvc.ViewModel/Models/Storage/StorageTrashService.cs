﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using CabinetMvc.ViewModel.Adm;


namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageTrashService : IAuBaseService
    {
        bool Clear(ModelStateDictionary modelState);
        bool Restore(int item_id, int? folder_id, ModelStateDictionary modelState);
    }

    public class StorageTrashService : AuBaseService, IStorageTrashService
    {

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_storage_trash                
                .ProjectTo<StorageTrashViewModel>();
        }

        public bool Clear(ModelStateDictionary modelState)
        {
            var storageFolderService = DependencyResolver.Current.GetService<IStorageFolderService>();
            var storageFileService = DependencyResolver.Current.GetService<IStorageFileService>();
            var storageLinkService = DependencyResolver.Current.GetService<IStorageLinkService>();

            List<storage_folder> storage_folder_list = dbContext.storage_folder.Where(ss => ss.is_deleted).ToList();
            if (storage_folder_list != null)
            {
                foreach (var storage_folder in storage_folder_list)
                {
                    var res1 = storageFolderService.Remove(storage_folder.folder_id, modelState);
                    if ((!res1) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при удалении папки " + storage_folder.folder_id.ToString());
                        return false;
                    }
                }
            }

            List<storage_file> storage_file_list = dbContext.storage_file.Where(ss => ss.is_deleted).ToList();
            if (storage_file_list != null)
            {
                foreach (var storage_file in storage_file_list)
                {
                    var res2 = storageFileService.Remove(storage_file.file_id, modelState);
                    if ((!res2) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при удалении файла " + storage_file.file_id.ToString());
                        return false;
                    }
                }
            }

            List<storage_link> storage_link_list = dbContext.storage_link.Where(ss => ss.is_deleted).ToList();
            if (storage_link_list != null)
            {
                foreach (var storage_link in storage_link_list)
                {
                    var res3 = storageLinkService.Remove(storage_link.link_id, modelState);
                    if ((!res3) || (!modelState.IsValid))
                    {
                        if (modelState.IsValid)
                            modelState.AddModelError("", "Ошибка при удалении ссылки " + storage_link.link_id.ToString());
                        return false;
                    }
                }
            }

            xToLog_Storage("Очистка корзины", null, null);

            return true;
        }

        public bool Restore(int item_id, int? folder_id, ModelStateDictionary modelState)
        {
            vw_storage_trash vw_storage_trash = dbContext.vw_storage_trash.Where(ss => ss.item_id == item_id).FirstOrDefault();
            if (vw_storage_trash == null)
            {
                modelState.AddModelError("", "Не найден объект в корзине с кодом " + item_id.ToString());
                return false;
            }

            int? curr_file_id = null;
            int? curr_folder_id = null;
            storage_folder parent_folder = null;
            if (folder_id.GetValueOrDefault(0) > 0)
            {
                parent_folder = dbContext.storage_folder.Where(ss => ss.folder_id == folder_id).FirstOrDefault();
                if (parent_folder == null)
                {
                    modelState.AddModelError("", "Не найдена папка с кодом " + folder_id.ToString());
                    return false;
                }
            }

            switch ((Enums.StorageItemEnum)vw_storage_trash.item_type)
            {
                case Enums.StorageItemEnum.FOLDER:
                    storage_folder storage_folder = dbContext.storage_folder.Where(ss => ss.folder_id == vw_storage_trash.item_id).FirstOrDefault();
                    if (storage_folder == null)
                    {
                        modelState.AddModelError("", "Не найдена папка с кодом " + item_id.ToString());
                        return false;
                    }
                    var existing_folder = dbContext.storage_folder.Where(ss => !ss.is_deleted && ss.parent_id == folder_id && ss.folder_name.Trim().ToLower().Equals(storage_folder.folder_name.Trim().ToLower())).FirstOrDefault();
                    if (existing_folder != null)
                    {
                        modelState.AddModelError("", "Уже есть такая папка");
                        return false;
                    }
                    curr_folder_id = storage_folder.folder_id;
                    storage_folder.is_deleted = false;
                    storage_folder.del_date = null;
                    storage_folder.del_user = null;
                    storage_folder.parent_id = folder_id;
                    storage_folder.folder_level = parent_folder == null ? 0 : parent_folder.folder_level - 1;
                    break;
                case Enums.StorageItemEnum.FILE:
                    storage_file storage_file = dbContext.storage_file.Where(ss => ss.file_id == vw_storage_trash.item_id).FirstOrDefault();
                    if (storage_file == null)
                    {
                        modelState.AddModelError("", "Не найден файл с кодом " + item_id.ToString());
                        return false;
                    }
                    if (parent_folder == null)
                    {
                        modelState.AddModelError("", "Не найдена папка с кодом " + folder_id.ToString());
                        return false;
                    }
                    var existing_file = dbContext.storage_file.Where(ss => ss.folder_id == folder_id && !ss.is_deleted && ss.file_type_id == storage_file.file_type_id && ss.file_name.Trim().ToLower().Equals(storage_file.file_name.Trim().ToLower())).FirstOrDefault();
                    if (existing_file != null)
                    {
                        modelState.AddModelError("", "Уже есть такой файл в этой папке");
                        return false;
                    }
                    curr_file_id = storage_file.file_id;
                    storage_file.is_deleted = false;
                    storage_file.del_date = null;
                    storage_file.del_user = null;
                    storage_file.folder_id = (int)folder_id;
                    break;
                case Enums.StorageItemEnum.LINK:
                    storage_link storage_link = dbContext.storage_link.Where(ss => ss.link_id == vw_storage_trash.item_id).FirstOrDefault();
                    if (storage_link == null)
                    {
                        modelState.AddModelError("", "Не найдена ссылка с кодом " + item_id.ToString());
                        return false;
                    }
                    if (parent_folder == null)
                    {
                        modelState.AddModelError("", "Не найдена папка с кодом " + folder_id.ToString());
                        return false;
                    }
                    var existing_link = dbContext.storage_link.Where(ss => ss.folder_id == folder_id && !ss.is_deleted && ss.download_link.Trim().ToLower().Equals(storage_link.download_link.Trim().ToLower())).FirstOrDefault();
                    if (existing_link != null)
                    {
                        modelState.AddModelError("", "Уже есть такая ссылка в этой папке");
                        return false;
                    }
                    curr_file_id = storage_link.link_id;
                    storage_link.is_deleted = false;
                    storage_link.del_date = null;
                    storage_link.del_user = null;
                    storage_link.folder_id = (int)folder_id;
                    break;
                default:
                    break;
            }            

            dbContext.SaveChanges();

            xToLog_Storage("Восстановление из корзины", curr_file_id, curr_folder_id);
            
            return true;
        }

        private void xToLog_Storage(string mess, int? file_id, int? folder_id)
        {
            IStorageLogService storageLogService = DependencyResolver.Current.GetService<IStorageLogService>();
            storageLogService.ToLog_Storage(mess, file_id, folder_id);
        }
    }
}