﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageLogService : IAuBaseService
    {
        void ToLog_Storage(string mess, int? file_id, int? folder_id);
    }

    public class StorageLogService : AuBaseService, IStorageLogService
    {
        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.vw_log_storage
                .ProjectTo<StorageLogViewModel>();        
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.vw_log_storage
                .Where(ss => ss.log_id == id)
                .ProjectTo<StorageLogViewModel>()
                .FirstOrDefault();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.vw_log_storage
                .Where(ss => ss.file_id == parent_id)
                .ProjectTo<StorageLogViewModel>();        
        }

        public void ToLog_Storage(string mess, int? file_id, int? folder_id)
        {
            DateTime now = DateTime.Now;
            log_storage log_storage = new log_storage();
            log_storage.date_beg = now;
            log_storage.date_end = now;
            log_storage.file_id = file_id;
            log_storage.folder_id = folder_id;
            log_storage.mess = mess;
            log_storage.user_id = currUser.CabUserId;
            dbContext.log_storage.Add(log_storage);
            dbContext.SaveChanges();            
        }


    }
}