﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using AutoMapper.QueryableExtensions;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;


namespace CabinetMvc.ViewModel.Storage
{
    public interface IStorageLinkService : IAuBaseService
    {
        bool Move(int link_id, int folder_id, ModelStateDictionary modelState);
        bool Remove(int link_id, ModelStateDictionary modelState);
    }

    public class StorageLinkService : AuBaseService, IStorageLinkService
    {

        private void xToLog_Storage(string mess, int? link_id, int? folder_id)
        {
            IStorageLogService storageLogService = DependencyResolver.Current.GetService<IStorageLogService>();
            storageLogService.ToLog_Storage(mess, link_id, folder_id);
        }

        protected override IQueryable<AuBaseViewModel> xGetList()
        {
            return dbContext.storage_link                
                .ProjectTo<StorageLinkViewModel>();
        }

        protected override IQueryable<AuBaseViewModel> xGetList_byParent(long parent_id)
        {
            return dbContext.storage_link
                .Where(ss => ss.folder_id == parent_id)
                .ProjectTo<StorageLinkViewModel>();
        }

        protected override AuBaseViewModel xGetItem(long id)
        {
            return dbContext.storage_link
                .Where(ss => ss.link_id == id)
                .ProjectTo<StorageLinkViewModel>()
                .FirstOrDefault();
        }

        public bool Move(int link_id, int folder_id, ModelStateDictionary modelState)
        {
            var moved_link = dbContext.storage_link.Where(ss => ss.link_id == link_id).FirstOrDefault();
            var parent_folder = dbContext.storage_folder.Where(ss => ss.folder_id == folder_id).FirstOrDefault();

            var existing_link = dbContext.storage_link.Where(ss => ss.folder_id == folder_id
                && ss.link_id != moved_link.link_id
                && ss.download_link.Trim().ToLower().Equals(moved_link.download_link.Trim().ToLower()))
                .FirstOrDefault();
            if (existing_link != null)
            {
                modelState.AddModelError("", "В этой папке уже есть такая ссылка");
                return false;
            }

            var old_folder = dbContext.storage_folder.Where(ss => ss.folder_id == moved_link.folder_id).FirstOrDefault();
            moved_link.folder_id = folder_id;
            dbContext.SaveChanges();

            xToLog_Storage("Ссылка " + moved_link.link_name + " перемещена из папки " + old_folder.folder_name + " в папку " + parent_folder.folder_name, moved_link.link_id, moved_link.folder_id);
            return true;
        }

        protected override AuBaseViewModel xInsert(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageLinkViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageLinkViewModel itemAdd = (StorageLinkViewModel)item;
            if ((itemAdd == null) || (String.IsNullOrEmpty(itemAdd.download_link)))
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            if (!(itemAdd.download_link.StartsWith("http") 
                || (itemAdd.download_link.StartsWith("www")) 
                || (itemAdd.download_link.StartsWith("ftp")) 
                || (itemAdd.download_link.StartsWith(@"\\"))))
            {
                modelState.AddModelError("", "Недопустимая ссылка");
                return null;
            }

            if (itemAdd.download_link.StartsWith("www"))
            {
                itemAdd.download_link = @"https://" + itemAdd.download_link;
            }

            storage_link storage_link_existing = dbContext.storage_link.Where(ss => ss.folder_id == itemAdd.folder_id && ss.download_link.Trim().ToLower().Equals(itemAdd.download_link.Trim().ToLower())).FirstOrDefault();
            if (storage_link_existing != null)
            {
                modelState.AddModelError("", "Уже есть такая ссылка в этой папке");
                return null;
            }

            storage_link storage_link = new storage_link();
            storage_link.crt_date = DateTime.Now;
            storage_link.crt_user = currUser.CabUserName;
            storage_link.download_link = itemAdd.download_link.StartsWith(@"\\") ? (@"file:///" + itemAdd.download_link) : itemAdd.download_link;
            storage_link.folder_id = itemAdd.folder_id;
            storage_link.link_name = itemAdd.link_name;            
            dbContext.storage_link.Add(storage_link);
            dbContext.SaveChanges();

            xToLog_Storage("Добавлена ссылка " + storage_link.link_name, storage_link.link_id, storage_link.folder_id);
            return GetItem(storage_link.link_id);
        }

        protected override AuBaseViewModel xUpdate(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageLinkViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return null;
            }

            StorageLinkViewModel itemEdit = (StorageLinkViewModel)item;
            if ((itemEdit == null) || (String.IsNullOrEmpty(itemEdit.download_link)))
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return null;
            }

            storage_link storage_link = dbContext.storage_link.Where(ss => ss.link_id == itemEdit.link_id).FirstOrDefault();
            if (storage_link == null)
            {
                modelState.AddModelError("", "Не найдена ссылка с кодом " + itemEdit.link_id.ToString());
                return null;
            }

            if (!(itemEdit.download_link.StartsWith("http")
                || (itemEdit.download_link.StartsWith("www"))
                || (itemEdit.download_link.StartsWith("ftp"))
                || (itemEdit.download_link.StartsWith(@"\\"))))
            {
                modelState.AddModelError("", "Недопустимая ссылка");
                return null;
            }

            if (itemEdit.download_link.StartsWith("www"))
            {
                itemEdit.download_link = @"https://" + itemEdit.download_link;
            }

            storage_link storage_link_existing = dbContext.storage_link.Where(ss => ss.link_id != itemEdit.link_id && ss.folder_id == itemEdit.folder_id && ss.download_link.Trim().ToLower().Equals(itemEdit.download_link.Trim().ToLower())).FirstOrDefault();
            if (storage_link_existing != null)
            {
                modelState.AddModelError("", "Уже есть такая ссылка в этой папке");
                return null;
            }

            string download_link_old = storage_link.download_link;
            string link_name_old = storage_link.link_name;

            storage_link.download_link = itemEdit.download_link;
            storage_link.link_name = itemEdit.link_name;            
            dbContext.SaveChanges();

            xToLog_Storage("Ссылка " + link_name_old + " переименован в " + itemEdit.link_name, storage_link.link_id, storage_link.folder_id);
            return GetItem(storage_link.link_id);
        }

        protected override bool xDelete(AuBaseViewModel item, ModelStateDictionary modelState)
        {
            if (!(item is StorageLinkViewModel))
            {
                modelState.AddModelError("error", "Некорректные параметры");
                return false;
            }

            StorageLinkViewModel itemDel = (StorageLinkViewModel)item;
            if (itemDel == null)
            {
                modelState.AddModelError("", "Не заданы атрибуты");
                return false;
            }

            storage_link storage_link = dbContext.storage_link.Where(ss => ss.link_id == itemDel.link_id).FirstOrDefault();
            if (storage_link == null)
            {
                modelState.AddModelError("", "Не найдена ссылка с кодом " + itemDel.link_id.ToString());
                return false;
            }

            DateTime now = DateTime.Now;
            int old_link_id = storage_link.link_id;
            int old_folder_id = storage_link.folder_id;
            string old_link_name = storage_link.link_name;
            /*
            dbContext.storage_link.Remove(storage_link);
            dbContext.SaveChanges();
            */
            storage_link.is_deleted = true;
            storage_link.del_date = now;
            storage_link.del_user = currUser.CabUserName;
            dbContext.SaveChanges();
            xToLog_Storage("Ссылка " + old_link_name + " удалена", old_link_id, old_folder_id);

            return true;
        }

        public bool Remove(int link_id, ModelStateDictionary modelState)
        {
            storage_link storage_link = dbContext.storage_link.Where(ss => ss.link_id == link_id).FirstOrDefault();
            if (storage_link == null)
            {
                modelState.AddModelError("", "Не найдена ссылка с кодом " + link_id.ToString());
                return false;
            }

            DateTime now = DateTime.Now;
            int old_link_id = storage_link.link_id;
            int old_folder_id = storage_link.folder_id;
            string old_link_name = storage_link.link_name;
            
            dbContext.storage_link.Remove(storage_link);
            dbContext.SaveChanges();

            xToLog_Storage("Ссылка " + old_link_name + " удалена", old_link_id, old_folder_id);

            return true;
        }

    }
}