﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AuDev.Common.Util;
using System.Net.Http;

namespace CabinetTestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //
            System.Net.ServicePointManager.Expect100Continue = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus("iguana");
                MessageBox.Show(res ? "OK" : "Error");
            }
            finally
            {
                Cursor = Cursors.Default;
            }            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.ChangeBonusDailyPeriod("iguana");
                MessageBox.Show(res ? "OK" : "Error");
            }
            finally
            {
                Cursor = Cursors.Default;
            } 
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // encode
            string txt = textBox1.Text;
            if (String.IsNullOrEmpty(txt))
                return;
            EncrHelper2 encr = new EncrHelper2();
            textBox2.Text = encr._SetHashValue(txt);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // decode
            string txt = textBox2.Text;
            if (String.IsNullOrEmpty(txt))
                return;
            EncrHelper2 encr = new EncrHelper2();
            textBox1.Text = encr._GetHashValue(txt);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.StorageTrashClear("iguana");
                MessageBox.Show(res ? "OK" : "Error");
            }
            finally
            {
                Cursor = Cursors.Default;
            } 
        }

        private void button6_Click(object sender, EventArgs e)
        {
            CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.VacuumAnalyze("iguana");
                MessageBox.Show(res ? "OK" : "Error");
            }
            finally
            {
                Cursor = Cursors.Default;
            } 
        }

        private void button7_Click(object sender, EventArgs e)
        {
            CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.DelOldData("iguana");
                MessageBox.Show(res ? "OK" : "Error");
            }
            finally
            {
                Cursor = Cursors.Default;
            } 
        }

        private void button8_Click(object sender, EventArgs e)
        {
            CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.UpdateCardDiscountPercentVip("iguana");
                MessageBox.Show(res ? "OK" : "Error");
            }
            finally
            {
                Cursor = Cursors.Default;
            } 
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox3.Text = "";
            OpenFileDialog od = new OpenFileDialog();
            Cursor = Cursors.WaitCursor;
            if (od.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    var fileContent = System.IO.File.ReadAllBytes(od.FileName);
                    var fileContentStr = Convert.ToBase64String(fileContent);
                    textBox3.Text = fileContentStr;
                }
                finally
                {
                    Cursor = Cursors.Default;
                }
            }            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox3.Text = "";
            Cursor = Cursors.WaitCursor;
            try
            {                
                using (HttpClient httpClient = new HttpClient())
                {
                    bool isResponseOk = false;
                    System.Net.HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;

                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri("http://localhost:38039/asna/CreateWorker"),
                        //RequestUri = new Uri("https://service.aptekaural.ru/auesnapi/asna/CreateWorker"),
                        Method = HttpMethod.Post,
                    };

                    request.Headers.Add("Cache-Control", "no-cache");
                    request.Content = new StringContent("{\"pwd\":\"iguana\",\"log_enabled\":\"1\"}",
                                        Encoding.UTF8,
                                        "application/json");

                    var task = httpClient.SendAsync(request)
                        .ContinueWith((taskwithmsg) =>
                        {
                            var response = taskwithmsg.Result;
                            isResponseOk = response.IsSuccessStatusCode;
                            if (!isResponseOk)
                            {
                                responseStatusCode = response.StatusCode;                                
                                MessageBox.Show("Запрос вернул ошибочный статус: " + responseStatusCode.ToString());
                                //Logger.Log("Запрос вернул ошибочный статус: " + responseStatusCode.ToString());
                                return;
                            }                            
                            MessageBox.Show("Запрос выполнен: " + taskwithmsg.Result.ToString());
                            //Logger.Log("Запрос выполнен: " + taskwithmsg.Result);
                        });
                    task.Wait();
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                MessageBox.Show(GlobalUtil.ExceptionInfo(ex));                
                //Logger.Log(ex);
            }
        }

    }
}
