﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using System.Data.Entity;
//using System.Data.EntityClient;
using System.Threading;
using System.Threading.Tasks;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
#endregion

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        const int UNIT_NUM_FOR_SUM = 999999;
        const string UNIT_NAME_SEPARATOR = "{|}";

        #region NLog

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private void ToNlog_Info(bool logEnabled, TimeSpan logTimeSpan, string mess)
        {        
            if (!logEnabled)
                return;
            logger.Info(mess + "; " + logTimeSpan.TotalMilliseconds.ToString() + " ms;");         
        }

        private void ToNlog_Error(bool logEnabled, string mess)
        {
            if (!logEnabled)
                return;
            logger.Error(mess);         
        }

        #endregion

        #region CalcCard

        #region Card_CalcBefore

        private void xCard_CalcBefore(card cardToPrepare, string user_name, bool newThread)
        {
            if (newThread)
            {
                Task task = new Task(() =>
                {
                    xxCard_CalcBefore(cardToPrepare, user_name);
                }
                );
                task.Start();
            }
            else
            {
                xxCard_CalcBefore(cardToPrepare, user_name);
            }
        }

        private void xxCard_CalcBefore(card cardToPrepare, string user_name)
        {
            #region Define
            long card_id = cardToPrepare.card_id;
            long business_id = (long)cardToPrepare.business_id;
            DateTime now = DateTime.Now;
            DateTime logStartTime = now;
            bool logEnabled = false;

            List<CalcPrepareCard> card_list = null;
            CalcPrepareCard calc_prepare_card = null;
            card card = null;
            long card_status_id = 0;
            long card_type_id = 0;
            programm programm = null;
            List<card_type_unit> card_type_unit_list = null;
            List<programm_param> programm_param_list = null;
            List<programm_step> programm_step_list = null;
            List<programm_step_param> programm_step_param_list = null;
            List<programm_step_condition> programm_step_condition_list = null;
            List<programm_step_logic> programm_step_logic_list = null;
            List<card_nominal> card_nominal_unit_list = null;
            List<card_type_limit> curr_limit_list = null;
            List<card_ext1> card_ext1_list = null;
            decimal? inacive_bonus_sum = null;
            #endregion

            try
            {
                #region Nlog
                try
                {
                    logEnabled = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["NlogEnabled"]);
                }
                catch (Exception ex)
                {
                    logEnabled = false;
                }
                #endregion

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc before start");

                using (AuMainDb dbContext = new AuMainDb())
                {
                    #region Подготовка CalcPrepareCard

                    var calc_prepare_res = dbContext.Database.SqlQuery<CalcPrepareCard>("select * from discount.get_calc_prepare_data(" + business_id.ToString() + "," + card_id.ToString() + ",'" + user_name + "')");
                    card_list = calc_prepare_res.ToList();

                    if ((card_list == null) || (card_list.Count <= 0))
                        return;
                    if (card_list.Count > 1)
                        return;

                    calc_prepare_card = card_list.FirstOrDefault();

                    #endregion

                    #region Подготовка card, card_type, card_status

                    card = new card()
                    {
                        card_id = calc_prepare_card.card_id,
                        date_beg = calc_prepare_card.date_beg,
                        date_end = calc_prepare_card.date_end,
                        curr_card_status_id = calc_prepare_card.curr_card_status_id,
                        business_id = calc_prepare_card.business_id,
                        curr_trans_count = calc_prepare_card.curr_trans_count,
                        card_num = calc_prepare_card.card_num,
                        card_num2 = calc_prepare_card.card_num2,
                        curr_trans_sum = calc_prepare_card.curr_trans_sum,
                        curr_holder_id = calc_prepare_card.curr_client_id,
                        curr_card_type_id = calc_prepare_card.curr_card_type_id,
                        warn = calc_prepare_card.warn,
                        source_card_id = calc_prepare_card.source_card_id,
                        from_au = calc_prepare_card.from_au,
                    };

                    if (!card.curr_card_status_id.HasValue)
                        return;

                    if (!card.curr_card_type_id.HasValue)
                        return;

                    card_status_id = (long)card.curr_card_status_id;
                    card_type_id = (long)card.curr_card_type_id;

                    #endregion

                    #region Подготовка programm

                    if (!calc_prepare_card.programm_id.HasValue)
                        return;

                    programm = new programm()
                    {
                        programm_id = (long)calc_prepare_card.programm_id,
                    };

                    #endregion

                    #region Подготовка card_type_unit_list

                    card_type_unit_list = dbContext.card_type_unit.Where(ss => ss.card_type_id == card_type_id).ToList();
                    if ((card_type_unit_list == null) || (card_type_unit_list.Count <= 0))
                        return;

                    #endregion

                    #region Подготовка списков programm_xxx

                    programm_param_list = dbContext.programm_param.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.param_num).ToList();
                    programm_step_list = dbContext.programm_step.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.step_num).ToList();
                    programm_step_param_list = dbContext.programm_step_param.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.step_id).ThenBy(ss => ss.param_num).ToList();
                    programm_step_condition_list = dbContext.programm_step_condition.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.step_id).ThenBy(ss => ss.condition_num).ToList();
                    programm_step_logic_list = dbContext.programm_step_logic.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.step_id).ThenBy(ss => ss.logic_num).ToList();

                    #endregion

                    #region Инициализация начальных значений параметров карты

                    // номиналы карты
                    card_nominal_unit_list = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).ToList();

                    // лимиты карты
                    curr_limit_list = dbContext.card_type_limit.Where(ss => ss.card_type_id == card_type_id)
                        .Distinct().ToList();

                    // использованные лимиты карты
                    card_ext1_list = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id).ToList();

                    // неактивные бонусы
                    inacive_bonus_sum = dbContext.card_nominal_inactive.Where(ss => ss.card_id == card.card_id && ss.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_VALUE).Sum(ss => ss.unit_value);

                    #endregion
                }

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc before end");
            }
            catch (Exception ex)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, ex, null, business_id, user_name, null, (long)Enums.LogEventType.ERROR, card.card_id, now, null, true);
            }
        }

        #endregion

        #region Card_CalcAfter

        private void xCard_CalcAfter(card_transaction calc, CalcRound round_info, bool newThread)
        {
            if (newThread)
            {
                Task task = new Task(() =>
                {
                    xxCard_CalcAfter(calc, round_info);
                }
                );
                task.Start();
            }
            else
            {
                xxCard_CalcAfter(calc, round_info);
            }
        }

        private void xxCard_CalcAfter(card_transaction calc, CalcRound round_info)
        {
            #region Define
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            bool logEnabled = false;
            DateTime logStartTime = DateTime.Now;

            card card = null;
            List<card_nominal> card_nominal_list = new List<card_nominal>();
            List<card_nominal> card_nominal_existing_list = null;
            card_nominal card_nominal_existing = null;
            List<card_nominal_uncommitted> card_nominal_uncommitted_list = null;
            card_nominal_uncommitted card_nominal_uncommitted_first = null;
            #endregion

            try
            {
                #region Nlog
                try
                {
                    logEnabled = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["NlogEnabled"]);
                }
                catch (Exception ex)
                {
                    logEnabled = false;
                }
                #endregion

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc after start");

                using (AuMainDb dbContext = new AuMainDb())
                {
                    // !!!
                    // пока отключим, это также делается каждую ночь
                    #region Удаление предыдущих активных (неподтвержденных) транзакций по карте
                    /*
                        dbContext.card_nominal_uncommitted.RemoveRange(from ct in dbContext.card_transaction
                                                                       from unc in dbContext.card_nominal_uncommitted
                                                                       where ct.card_id == card_id && ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                       && ct.card_trans_id == unc.card_trans_id
                                                                       select unc);

                        dbContext.programm_step_result.RemoveRange(from ct in dbContext.card_transaction
                                                                   from pr in dbContext.programm_result
                                                                   from pst in dbContext.programm_step_result
                                                                   where ct.card_id == card_id && ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                   && ct.programm_result_id.HasValue
                                                                   && ct.programm_result_id == pr.result_id
                                                                   && pr.result_id == pst.result_id
                                                                   select pst);

                        dbContext.programm_result_detail.RemoveRange(from ct in dbContext.card_transaction
                                                                     from pr in dbContext.programm_result
                                                                     from prd in dbContext.programm_result_detail
                                                                     where ct.card_id == card_id && ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                     && ct.programm_result_id.HasValue
                                                                     && ct.programm_result_id == pr.result_id
                                                                     && pr.result_id == prd.result_id
                                                                     select prd);

                        dbContext.programm_result.RemoveRange(from ct in dbContext.card_transaction
                                                              from pr in dbContext.programm_result
                                                              where ct.card_id == card_id && ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                              && ct.programm_result_id.HasValue
                                                              && ct.programm_result_id == pr.result_id
                                                              select pr);

                        dbContext.card_transaction_detail.RemoveRange(from ct in dbContext.card_transaction
                                                                      from ctd in dbContext.card_transaction_detail
                                                                      where ct.card_id == card_id && ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                      && ct.card_trans_id == ctd.card_trans_id
                                                                      select ctd);

                        dbContext.card_transaction_unit.RemoveRange(from ct in dbContext.card_transaction
                                                                    from ctu in dbContext.card_transaction_unit
                                                                    where ct.card_id == card_id && ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                                    && ct.card_trans_id == ctu.card_trans_id
                                                                    select ctu);

                        dbContext.card_transaction.RemoveRange(from ct in dbContext.card_transaction
                                                               where ct.card_id == card_id && ct.status == (int)Enums.CardTransactionStatus.ACTIVE
                                                               select ct);

                        dbContext.SaveChanges();
                    */
                    #endregion

                    #region Считываем карту
                    card = dbContext.card.Where(ss => ss.card_id == calc.card_id).FirstOrDefault();
                    if (card == null)
                        return;
                    #endregion

                    card_nominal_uncommitted_list = dbContext.card_nominal_uncommitted.Where(ss => ss.card_trans_id == calc.card_trans_id).ToList();
                    if ((card_nominal_uncommitted_list != null) && (card_nominal_uncommitted_list.Count > 0))
                    {
                        #region Переносим значения из card_nominal_uncommitted в card_nominal
                        card_nominal_existing_list = dbContext.card_nominal.Where(ss => ss.card_id == calc.card_id && !ss.date_end.HasValue).ToList();
                        foreach (var item in card_nominal_uncommitted_list.Where(ss => ss.unit_type.HasValue))
                        {
                            card_nominal_existing = card_nominal_existing_list.Where(ss => ss.unit_type == item.unit_type).FirstOrDefault();
                            if (card_nominal_existing != null)
                            {
                                if ((card_nominal_existing.unit_value.HasValue) && (card_nominal_existing.unit_value == item.unit_value))
                                {
                                    continue;
                                }
                                card_nominal_existing.date_end = calc.date_beg;
                            }

                            card_nominal_list.Add(new card_nominal()
                            {
                                card_id = item.card_id,
                                card_trans_id = item.card_trans_id,
                                date_beg = item.date_beg,
                                date_end = item.date_end,
                                date_expire = item.date_expire,
                                unit_type = item.unit_type,
                                unit_value = item.unit_value,
                                crt_date = now,
                                crt_user = calc.user_name,
                            }
                            );

                            //dbContext.SaveChanges();
                        }

                        #endregion

                        #region Обновляем card.curr_trans_count, card.curr_trans_sum

                        card_nominal_uncommitted_first = card_nominal_uncommitted_list.FirstOrDefault();

                        card.curr_trans_count = card_nominal_uncommitted_first.curr_trans_count;
                        card.curr_trans_sum = card_nominal_uncommitted_first.curr_trans_sum;

                        #endregion

                        #region Переносим неактивные бонусы из card_nominal_uncommitted в card_nominal_inactive

                        if (card_nominal_uncommitted_first.bonus_value_inactive.GetValueOrDefault(0) != 0)
                        {
                            card_nominal_inactive inactive_bonus = new card_nominal_inactive();
                            inactive_bonus.card_id = card.card_id;
                            inactive_bonus.crt_date = now;
                            inactive_bonus.crt_user = calc.user_name;
                            inactive_bonus.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                            inactive_bonus.unit_value = card_nominal_uncommitted_first.bonus_value_inactive;
                            //inactive_bonus.active_date = now.AddHours(24);
                            DateTime tomorrow_with_time = now.AddDays(1);
                            DateTime tomorrow = new DateTime(tomorrow_with_time.Year, tomorrow_with_time.Month, tomorrow_with_time.Day, 0, 0, 0);
                            inactive_bonus.active_date = tomorrow;
                            inactive_bonus.card_trans_id = calc.card_trans_id;
                            dbContext.card_nominal_inactive.Add(inactive_bonus);
                            //dbContext.SaveChanges();
                        }

                        #endregion

                        #region Меняем статус карты

                        long new_card_status_id = (long)card_nominal_uncommitted_first.curr_card_status_id.GetValueOrDefault(0);
                        if (new_card_status_id > 0)
                        {
                            card_card_status_rel card_card_status_rel_new = null;
                            card_card_status_rel card_card_status_rel_existing = dbContext.card_card_status_rel.Where(ss => ss.card_id == calc.card_id
                                && !ss.date_end.HasValue)
                                .FirstOrDefault();
                            if ((card_card_status_rel_existing != null) && (card_card_status_rel_existing.card_status_id != new_card_status_id))
                            {
                                card_card_status_rel_existing.date_end = calc.date_beg;

                                card_card_status_rel_new = new card_card_status_rel();
                                card_card_status_rel_new.card_id = card.card_id;
                                card_card_status_rel_new.card_status_id = new_card_status_id;
                                card_card_status_rel_new.date_beg = calc.date_beg;

                                dbContext.card_card_status_rel.Add(card_card_status_rel_new);

                                card.curr_card_status_id = new_card_status_id;

                                // если карта стала неактивной - обновляем сообщение card.warn
                                var is_card_inactive = from cs in dbContext.card_status
                                                       from csg in dbContext.card_status_group
                                                       where cs.card_status_group_id == csg.card_status_group_id
                                                       && cs.card_status_id == new_card_status_id
                                                       && csg.is_active != 1
                                                       select cs;
                                if (is_card_inactive != null)
                                {
                                    // пока возможен только вариант, что карта стала неактивной после исчерпывания всего лимита продаж
                                    card.warn = "Исчерпан лимит карты за текущий период";
                                }

                                //dbContext.SaveChanges();
                            }
                        }

                        #endregion

                        #region Увеличиваем использованные лимиты

                        if (card_nominal_uncommitted_first.limit_id.HasValue)
                        {
                            var money_used_limit_value = card_nominal_uncommitted_first.money_used_limit_value.GetValueOrDefault(0);
                            if (money_used_limit_value != 0)
                            {
                                card_ext1 curr_limit_used = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id && ss.curr_limit_id == card_nominal_uncommitted_first.limit_id).FirstOrDefault();
                                if (curr_limit_used == null)
                                {
                                    curr_limit_used = new card_ext1();
                                    curr_limit_used.card_id = card.card_id;
                                    curr_limit_used.curr_limit_id = card_nominal_uncommitted_first.limit_id;
                                    curr_limit_used.curr_sum = money_used_limit_value;
                                    dbContext.card_ext1.Add(curr_limit_used);
                                }
                                else
                                {
                                    curr_limit_used.curr_sum = money_used_limit_value;
                                }
                            }
                        }
                        if (card_nominal_uncommitted_first.bonus_limit_id.HasValue)
                        {
                            var bonus_for_pay_sum = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == calc.card_trans_id && ss.unit_type == 0).Sum(ss => ss.unit_value_bonus_for_pay);
                            card_ext1 curr_limit_used = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id && ss.curr_limit_id == card_nominal_uncommitted_first.bonus_limit_id).FirstOrDefault();
                            if ((curr_limit_used == null) && (bonus_for_pay_sum.GetValueOrDefault(0) != 0))
                            {
                                curr_limit_used = new card_ext1();
                                curr_limit_used.card_id = card.card_id;
                                curr_limit_used.curr_limit_id = card_nominal_uncommitted_first.bonus_limit_id;
                                curr_limit_used.curr_sum = bonus_for_pay_sum;

                                dbContext.card_ext1.Add(curr_limit_used);
                            }
                            else
                            {
                                curr_limit_used.curr_sum = curr_limit_used.curr_sum.HasValue ? (curr_limit_used.curr_sum + bonus_for_pay_sum) : bonus_for_pay_sum;
                            }
                        }

                        #endregion

                        #region Обновляем card_nominal
                        if (card_nominal_list.Count > 0)
                            dbContext.card_nominal.AddRange(card_nominal_list);
                        #endregion

                        #region Чистим card_nominal_uncommitted
                        dbContext.card_nominal_uncommitted.RemoveRange(card_nominal_uncommitted_list);
                        #endregion
                    }

                    #region Применяем округление

                    if ((round_info != null) && (round_info.round_value != 0))
                    {
                        var calc_detail_round_victim = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == calc.card_trans_id && ss.artikul == round_info.artikul).FirstOrDefault();
                        if (calc_detail_round_victim != null)
                        {
                            if ((calc_detail_round_victim.unit_value_with_discount.HasValue) && (calc_detail_round_victim.unit_value_with_discount > 0))
                            {
                                calc_detail_round_victim.unit_value_with_discount -= round_info.round_value;
                                calc_detail_round_victim.unit_value_discount += round_info.round_value;
                            }
                        }
                    }

                    #endregion

                    dbContext.SaveChanges();
                }

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc after end");
            }
            catch (Exception ex)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, ex, null, null, null, null, (long)Enums.LogEventType.ERROR, calc.card_id, now, null, true);
            }
        }

        #endregion

        #region Card_CalcSaveContext (old)
        /*
                    // сохраняем результат алоритма
                    if (is_test == 1)
                        dbContext.programm_result.Add(programm_result);

						
                    if (is_test == 0)
                        dbContext.card_transaction.Add(card_transaction);
						
                        if (is_test == 0)
                            dbContext.card_transaction_detail.Add(card_transaction_detail);

                        if (is_test == 1)
                            dbContext.programm_result_detail.Add(programm_result_detail);							
							
                            if (is_test == 0)
                                dbContext.card_transaction_detail.Add(card_transaction_detail);

                            if (is_test == 1)
                                dbContext.programm_result_detail.Add(programm_result_detail);

							                    if (is_test == 0)
                                                {
                                                    if (card_transaction_unit_list_for_add.Count > 0)
                                                    {
                                                        dbContext.card_transaction_unit.AddRange(card_transaction_unit_list_for_add);
                                                    }
                                                }

                    dbContext.card_nominal_uncommitted.Add(card_nominal);

                    dbContext.card_nominal_uncommitted.Add(card_nominal);	
        */

        /*
        private void xCard_CalcSaveContext(string session_id, long business_id, AuMainDb contextforSave, long trans_id, bool doCommit, bool newThread)
        {
            if (newThread)
            {
                Task task = new Task(() =>
                {
                    xxCard_CalcSaveContext(session_id, business_id, contextforSave, trans_id, doCommit);
                }
                );
                task.Start();
            }
            else
            {
                xxCard_CalcSaveContext(session_id, business_id, contextforSave, trans_id, doCommit);
            }
        }
        */

        /*
        private void xxCard_CalcSaveContext(string session_id, long business_id, AuMainDb contextforSave, long trans_id, bool doCommit)
        {
            #region Define
            bool logEnabled = false;
            DateTime now = DateTime.Now;
            DateTime logStartTime = now;
            #endregion

            try
            {
                #region Nlog
                try
                {
                    logEnabled = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["NlogEnabled"]);
                }
                catch (Exception ex)
                {
                    logEnabled = false;
                }
                #endregion

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc save context start");

                contextforSave.SaveChanges();

                if (doCommit)
                {
                    xCard_CalcCommit(session_id, business_id, trans_id, null);
                }

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc save context end");
            }
            catch (Exception ex)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, ex, null, null, null, null, (long)Enums.LogEventType.ERROR, null, now, null, true);
            }
        }
        */
        #endregion

        #region Card_CalcStart

        // про оптимизацию PostqreSQL
        // https://www.myintervals.com/blog/2014/12/16/five-simple-postgresql-optimizations-for-improving-performance/
        
        // is_test: 0 - реальная продажа, 1 - тест с детализацией, 2 - тест без детализации
        private CalcStartResult xCard_CalcStart(string session_id, long business_id, long card_num, List<CalcUnit> calc_unit_list, decimal bonus_for_pay, short is_scanned, bool isTestMode, bool doCommit)
        {

            #region Define

            int i = 1;
            int cond_num = 1;
            long trans_id = 0;
            short is_scanned_actual = is_scanned == 1 ? (short)1 : (short)0;
            short is_cardreadered_actual = is_scanned == 2 ? (short)1 : (short)0;

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            bool logEnabled = false;
            DateTime logStartTime = DateTime.Now;

            // параметры продажи
            DateTime calc_trans_param_value_TIME = DateTime.Now;
            DateTime calc_trans_param_value_TIME_with_timezone = calc_trans_param_value_TIME;

            decimal calc_trans_param_value_SUMMA = 0;
            decimal calc_trans_param_value_SUMMA_WITH_DISCOUNT = 0;
            decimal calc_trans_param_value_SUMMA_DISCOUNT = 0;
            decimal calc_trans_param_value_PERCENT_DISCOUNT = 0;
            decimal calc_trans_param_value_SUMMA_BONUS_FOR_CARD = 0;
            decimal calc_trans_param_value_PERCENT_BONUS_FOR_CARD = 0;
            decimal calc_trans_param_value_SUMMA_BONUS_FOR_PAY = bonus_for_pay;
            decimal calc_trans_param_value_PERCENT_BONUS_FOR_PAY = 0;
            List<string> calc_trans_param_value_POS_NAME_LIST = new List<string>();
            string calc_trans_param_value_POS_MESS = "";
            int calc_trans_param_value_POS_COUNT = 0;
            int calc_trans_param_value_CARD_IS_SCANNED = is_scanned_actual;
            int calc_trans_param_value_CARD_IS_CARD_READERED = is_cardreadered_actual;

            List<CalcUnit> calc_unit_list_res = null;
            int calc_unit_count = 0;
            decimal unit_value_with_discount_sum = 0;

            // параметры карты
            decimal? calc_card_param_value_BONUS_PERCENT = 0;
            decimal? calc_card_param_value_BONUS_VALUE = 0;
            decimal? calc_card_param_value_DISCOUNT_PERCENT = 0;
            decimal? calc_card_param_value_DISCOUNT_VALUE = 0;
            decimal? calc_card_param_value_MONEY = 0;
            decimal? calc_card_param_value_SUMMA = 0;
            decimal? calc_card_param_value_TRANS_COUNT = 0;
            decimal? calc_card_param_value_CARD_STATUS = 0;
            decimal? calc_card_param_value_CARD_MONEY_LIMIT = 0;
            decimal? calc_card_param_value_CARD_MONEY_LIMIT_USED = 0;
            decimal? calc_card_param_value_CARD_BONUS_LIMIT = 0;
            decimal? calc_card_param_value_CARD_BONUS_LIMIT_USED = 0;
            decimal? calc_card_param_value_BONUS_VALUE_INACTIVE = 0;

            decimal? calc_card_param_value_CARD_MONEY_LIMIT_USED_init_value = 0;
            decimal? calc_card_param_value_CARD_BONUS_LIMIT_USED_init_value = 0;

            CardNominalList nominal_list_for_return = new CardNominalList();

            business business = null;
            business_org business_org = null;
            card card = null;
            CardList card_list_founded = null;
            Card card_founded = null;
            List<CalcPrepareCard> card_list = null;
            CalcPrepareCard calc_prepare_card = null;
            //List<card_status> card_status_list = null;
            card_status card_status = null;
            card_nominal card_nominal_bonus_for_pay = null;
            programm programm = null;
            long card_type_id = 0;
            long card_status_id = 0;
            List<card_type_unit> card_type_unit_list = null;
            List<programm_param> programm_param_list = null;
            List<programm_step> programm_step_list = null;
            List<programm_step_param> programm_step_param_list = null;
            List<programm_step_condition> programm_step_condition_list = null;
            List<programm_step_logic> programm_step_logic_list = null;
            List<card_transaction_unit> card_transaction_unit_list_for_add = null;
            List<card_nominal> card_nominal_unit_list = null;
            //List<card_nominal> card_nominal_exisiting_list = null;
            IQueryable<card_nominal_uncommitted> card_nominal_uncommitted_existing = null;
            List<card_transaction> card_transaction_active_list = null;
            programm_result programm_result = null;
            programm_step_result programm_step_result = null;
            List<programm_step_result> programm_step_result_list = null;

            List<card_type_limit> curr_limit_list = null;
            card_type_limit curr_money_limit = null;
            card_type_limit curr_bonus_limit = null;

            int? card_scan_only = 0;
            short scanner_equals_reader = 1;
            CheckPrintInfo checkPrintInfo = null;
            bool ctbo_allow_discount = true;
            bool ctbo_allow_bonus_for_pay = true;
            bool ctbo_allow_bonus_for_card = true;

            int block_num = 0;
            int block_subnum = 0;
            string err_mess2 = "";

            string warn = "";

            #endregion

            block_num = 1;
            try
            {

                #region Nlog
                try
                {
                    logEnabled = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["NlogEnabled"]);
                }
                catch (Exception ex)
                {
                    logEnabled = false;
                }
                #endregion

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: start");

                #region Сессия не найдена

                LogSession ls = xGetActiveSession(session_id);
                if (!IsValidSession(ls))
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена"));

                #endregion
                
                #region Список товаров пуст

                if ((calc_unit_list == null) || (calc_unit_list.Count <= 0))
                {
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Список товаров пуст"));
                }

                #endregion

                #region Инициализация calc_unit_list_res

                calc_unit_list_res = new List<CalcUnit>();
                foreach (var calc_unit in calc_unit_list)
                {
                    CalcUnit new_calc_unit = new CalcUnit(calc_unit);
                    calc_unit_list_res.Add(new_calc_unit);
                }

                foreach (var item in calc_unit_list_res)
                {
                    if (item.unit_count <= 0)
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Кол-во товаров <= 0, номер позиции " + item.unit_num.ToString()));

                    calc_trans_param_value_SUMMA += GlobalUtil.ToTwoDigitsAfterComma(item.unit_value);

                    if (!String.IsNullOrWhiteSpace(item.unit_name))
                        calc_trans_param_value_POS_NAME_LIST.Add(item.unit_name.Trim());

                    // дополнительно инициализируем выходные параметры (которые могут меняться в алгоритме)
                    item.unit_value_with_discount = item.promo == 1 ? item.unit_value_with_discount : GlobalUtil.ToTwoDigitsAfterComma(item.unit_value);
                    item.unit_value_discount = item.promo == 1 ? item.unit_value_discount : 0;
                    item.unit_percent_discount = item.promo == 1 ? item.unit_percent_discount : 0;
                    item.unit_value_bonus_for_card = 0;
                    item.unit_value_bonus_for_pay = 0;
                    item.unit_percent_bonus_for_card = 0;
                    item.unit_percent_bonus_for_pay = 0;
                    item.unit_type = 0;

                    calc_unit_count++;
                }

                if (calc_trans_param_value_SUMMA <= 0)
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Сумма продажи = 0"));

                // второй проход - считаем unit_percent_of_summa
                decimal unit_percent_of_summa_sum = 0;
                foreach (var item in calc_unit_list_res)
                {
                    item.unit_percent_of_summa = GlobalUtil.ToTwoDigitsAfterComma((item.unit_value / calc_trans_param_value_SUMMA) * 100);
                    unit_percent_of_summa_sum += item.unit_percent_of_summa;
                }

                // корректировка unit_percent_of_summa, если они в сумме не дали 100.00 %
                unit_percent_of_summa_sum = GlobalUtil.ToTwoDigitsAfterComma(unit_percent_of_summa_sum);
                decimal hundred = GlobalUtil.ToTwoDigitsAfterComma(100);
                if (unit_percent_of_summa_sum != hundred)
                {
                    decimal unit_percent_of_summa_diff = GlobalUtil.ToTwoDigitsAfterComma(hundred - unit_percent_of_summa_sum);
                    CalcUnit unit_percent_of_summa_victim = calc_unit_list_res.OrderByDescending(ss => ss.unit_percent_of_summa).FirstOrDefault();
                    unit_percent_of_summa_victim.unit_percent_of_summa = unit_percent_of_summa_victim.unit_percent_of_summa + unit_percent_of_summa_diff;
                }

                calc_unit_list_res.Add(new CalcUnit()
                {
                    unit_num = UNIT_NUM_FOR_SUM,
                    unit_name = "итог",
                    unit_value = calc_trans_param_value_SUMMA,
                    unit_value_with_discount = calc_trans_param_value_SUMMA,
                    unit_value_discount = 0,
                    unit_percent_discount = 0,
                    unit_value_bonus_for_card = 0,
                    unit_value_bonus_for_pay = 0,
                    unit_percent_bonus_for_card = 0,
                    unit_percent_bonus_for_pay = 0,
                    unit_type = 1,
                    unit_count = 1,
                    unit_percent_of_summa = 100,
                }
                );

                #endregion
                
                using (AuMainDb dbContext = new AuMainDb())
                {

                    block_num = 2;
                    #region Подготовка CalcPrepareCard

                    //getCard
                    
                    card_list_founded = xGetCardList_byCardNum(session_id, business_id, card_num, card_num.ToString(), (short)Enums.CardSearchMode.BY_ALL_NUMS, null, true, ls.org_id);
                    if ((card_list_founded == null) || (card_list_founded.error != null) || (card_list_founded.Card_list == null) || (card_list_founded.Card_list.Count <= 0))
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена пригодная для покупки карта с номером " + card_num.ToString()));
                    if (card_list_founded.Card_list.Count > 1)
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Найдено несколько карт с номером " + card_num.ToString()));
                    card_founded = card_list_founded.Card_list.FirstOrDefault();

                    var calc_prepare_res = dbContext.Database.SqlQuery<CalcPrepareCard>("select * from discount.get_calc_prepare_data(" + business_id.ToString() + "," + card_founded.card_id.ToString() + ",'" + ls.user_name + "')");
                    card_list = calc_prepare_res.ToList();

                    if ((card_list == null) || (card_list.Count <= 0))
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена пригодная для покупки карта с номером " + card_num.ToString()));
                    if (card_list.Count > 1)
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT, "Найдено несколько карт с номером " + card_num.ToString()));

                    calc_prepare_card = card_list.FirstOrDefault();

                    ctbo_allow_discount = calc_prepare_card.allow_discount == 1 ? true : false;
                    ctbo_allow_bonus_for_pay = calc_prepare_card.allow_bonus_for_pay == 1 ? true : false;
                    ctbo_allow_bonus_for_card = calc_prepare_card.allow_bonus_for_card == 1 ? true : false;

                    #endregion

                    block_num = 3;
                    #region Подготовка card, card_type, card_status

                    card = new card()
                    {
                        card_id = calc_prepare_card.card_id,
                        date_beg = calc_prepare_card.date_beg,
                        date_end = calc_prepare_card.date_end,
                        curr_card_status_id = calc_prepare_card.curr_card_status_id,
                        business_id = calc_prepare_card.business_id,
                        curr_trans_count = calc_prepare_card.curr_trans_count,
                        card_num = calc_prepare_card.card_num,
                        card_num2 = calc_prepare_card.card_num2,
                        curr_trans_sum = calc_prepare_card.curr_trans_sum,
                        curr_holder_id = calc_prepare_card.curr_client_id,
                        curr_card_type_id = calc_prepare_card.curr_card_type_id,
                        warn = calc_prepare_card.warn,
                        source_card_id = calc_prepare_card.source_card_id,
                        from_au = calc_prepare_card.from_au,
                    };

                    if (!card.curr_card_status_id.HasValue)
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден текущий статус на карте с номером " + card_num.ToString()));

                    if (!card.curr_card_type_id.HasValue)
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден текущий тип на карте с номером " + card_num.ToString()));

                    if (calc_prepare_card.card_status_group_id != (long)Enums.CardStatusGroup.ACTIVE)
                    {
                        string err_mess = "Карта не активна, номер карты " + card_num.ToString();
                        if (!String.IsNullOrEmpty(card.warn))
                            err_mess += ". " + card.warn.Trim();
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, err_mess));
                    }

                    card_status_id = (long)card.curr_card_status_id;
                    card_type_id = (long)card.curr_card_type_id;                    

                    #endregion

                    block_num = 4;
                    #region Подготовка check_print_info

                    card_type_check_info check_print_info = new card_type_check_info()
                    {
                        is_active = calc_prepare_card.print_is_active,
                        print_sum_trans = calc_prepare_card.print_sum_trans,
                        print_bonus_value_trans = calc_prepare_card.print_bonus_value_trans,
                        print_bonus_value_card = calc_prepare_card.print_bonus_value_card,
                        print_bonus_percent_card = calc_prepare_card.print_bonus_percent_card,
                        print_disc_percent_card = calc_prepare_card.print_disc_percent_card,
                        print_inactive_bonus_value_card = calc_prepare_card.print_inactive_bonus_value_card,
                        print_used_bonus_value_trans = calc_prepare_card.print_used_bonus_value_trans,
                        print_programm_descr = calc_prepare_card.print_programm_descr,
                        print_card_num = calc_prepare_card.print_card_num,
                        print_cert_sum_card = calc_prepare_card.print_cert_sum_card,
                        print_cert_sum_trans = calc_prepare_card.print_cert_sum_trans,
                        print_cert_sum_left = calc_prepare_card.print_cert_sum_left,
                    };
                    if (check_print_info != null)
                        checkPrintInfo = new CheckPrintInfo(check_print_info);
                    else
                        checkPrintInfo = new CheckPrintInfo() { is_active = false, };

                    #endregion

                    block_num = 5;
                    #region Подготовка programm

                    if (!calc_prepare_card.programm_id.HasValue)
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карта на текущую дату не привязана к алгоритмам расчета, номер карты " + card_num.ToString()));

                    programm = new programm()
                    {
                        programm_id = (long)calc_prepare_card.programm_id,
                    };

                    #endregion

                    block_num = 6;
                    #region Подготовка card_type_unit_list

                    card_type_unit_list = dbContext.card_type_unit.Where(ss => ss.card_type_id == card_type_id).ToList();
                    if ((card_type_unit_list == null) || (card_type_unit_list.Count <= 0))
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены карточные единицы, номер карты " + card_num.ToString()));

                    #endregion

                    block_num = 7;
                    #region Подготовка business, business_org

                    if (!calc_prepare_card.business_id.HasValue)
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));
                    business = new business()
                    {
                        business_id = (long)calc_prepare_card.business_id,
                        add_to_timezone = calc_prepare_card.add_to_timezone,
                        card_scan_only = calc_prepare_card.card_scan_only,
                        scanner_equals_reader = calc_prepare_card.scanner_equals_reader,
                        allow_card_add = calc_prepare_card.allow_card_add,
                    };
                    business_org = new business_org()
                    {
                        org_id = calc_prepare_card.org_id.GetValueOrDefault(0),
                        business_id = (long)calc_prepare_card.business_id,
                        add_to_timezone = calc_prepare_card.bo_add_to_timezone,
                        card_scan_only = calc_prepare_card.bo_card_scan_only,
                        scanner_equals_reader = calc_prepare_card.bo_scanner_equals_reader.HasValue ? (short)calc_prepare_card.bo_scanner_equals_reader : (short)0,
                        allow_card_add = calc_prepare_card.bo_allow_card_add.HasValue ? calc_prepare_card.bo_allow_card_add == 1 : false,
                    };

                    #endregion

                    #region Инициализация calc_trans_param_value_TIME_with_timezone, calc_trans_param_value_POS_COUNT

                    // заполняем начальные значения параметра "дата-время продажи" с учетом часового пояса
                    if ((business_org != null) && (business_org.add_to_timezone.HasValue))
                    {
                        calc_trans_param_value_TIME_with_timezone = calc_trans_param_value_TIME.AddHours((double)business_org.add_to_timezone);
                    }
                    else if (business.add_to_timezone.HasValue)
                    {
                        calc_trans_param_value_TIME_with_timezone = calc_trans_param_value_TIME.AddHours((double)business.add_to_timezone);
                    }

                    calc_trans_param_value_POS_COUNT = calc_unit_count;

                    #endregion

                    #region Учет параметра card_scan_only

                    card_scan_only = business.card_scan_only;
                    scanner_equals_reader = business.scanner_equals_reader;
                    if ((business_org != null) && (business_org.card_scan_only.HasValue))
                    {
                        card_scan_only = business_org.card_scan_only;
                    }
                    if (business_org != null)
                    {
                        scanner_equals_reader = business_org.scanner_equals_reader;
                    }
                    if (scanner_equals_reader == 1)
                    {
                        is_scanned_actual = is_scanned > 0 ? (short)1 : (short)0; ;
                        is_cardreadered_actual = is_scanned > 0 ? (short)1 : (short)0;
                        calc_trans_param_value_CARD_IS_SCANNED = is_scanned_actual;                        
                        calc_trans_param_value_CARD_IS_CARD_READERED = is_cardreadered_actual;
                    }

                    // !!!
                    // бонусные карты Уралпромснаба - по ним выдаем warn если они выбраны не сканером
                    // if (((business_id == 1113650283650483295) || (business_id == 1191989625543984144)) && (is_scanned_actual != 1))
                    if ((business_id == 1113650283650483295) && (is_scanned_actual != 1))
                    {
                        long? card_type_group_id = dbContext.card_type.Where(ss => ss.card_type_id == card_type_id).Select(ss => ss.card_type_group_id).FirstOrDefault();
                        if (card_type_group_id == (long)Enums.CardTypeGroup.BONUS)
                        {
                            warn = "Использование бонусной карты без сканера штрих-кодов запрещено !";
                        }
                    }

                    if ((card_scan_only.GetValueOrDefault(0) == 1) && (is_scanned_actual == 0))
                        //return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Установлен запрет на ручной выбор карты. Пожалуйста выберите карту с помощью сканера"));
                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Выбор карты отменен. Установлен запрет на ручной выбор карты. Пожалуйста, выберите карту с помощью сканера. Можете продолжить работу с набранным чеком."));

                    #endregion

                    #region Удаление предыдущих активных (неподтвержденных) транзакций по карте (перенесно в Card_CalcPrepare)

                    /*
                if (is_test == 0)
                {
                    card_transaction_active_list = dbContext.card_transaction.Where(ss => ss.card_id == card.card_id && ss.status == (int)Enums.CardTransactionStatus.ACTIVE).ToList();
                    if ((card_transaction_active_list != null) && (card_transaction_active_list.Count > 0))
                    {
                        foreach (var card_transaction_active in card_transaction_active_list)
                        {
                            card_nominal_uncommitted_existing = dbContext.card_nominal_uncommitted.Where(ss => ss.card_trans_id == card_transaction_active.card_trans_id);
                            dbContext.card_nominal_uncommitted.RemoveRange(card_nominal_uncommitted_existing);

                            programm_result programm_result_active = dbContext.programm_result.Where(ss => ss.result_id == card_transaction_active.programm_result_id).FirstOrDefault();
                            if (programm_result_active != null)
                            {
                                IQueryable<programm_step_result> programm_step_result_active_list = dbContext.programm_step_result.Where(ss => ss.result_id == programm_result_active.result_id);
                                dbContext.programm_step_result.RemoveRange(programm_step_result_active_list);
                                IQueryable<programm_result_detail> programm_result_detail_active_list = dbContext.programm_result_detail.Where(ss => ss.result_id == programm_result_active.result_id);
                                dbContext.programm_result_detail.RemoveRange(programm_result_detail_active_list);
                                dbContext.programm_result.Remove(programm_result_active);
                            }

                            IQueryable<card_transaction_detail> card_transaction_detail_active_list = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == card_transaction_active.card_trans_id);
                            IQueryable<card_transaction_unit> card_transaction_unit_active_list = dbContext.card_transaction_unit.Where(ss => ss.card_trans_id == card_transaction_active.card_trans_id);
                            dbContext.card_transaction_detail.RemoveRange(card_transaction_detail_active_list);
                            dbContext.card_transaction_unit.RemoveRange(card_transaction_unit_active_list);
                            dbContext.card_transaction.Remove(card_transaction_active);
                            dbContext.SaveChanges();
                        }
                    }
                }
                */
                    #endregion

                    block_num = 8;
                    #region Подготовка списков programm_xxx

                    programm_param_list = dbContext.programm_param.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.param_num).ToList();
                    programm_step_list = dbContext.programm_step.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.step_num).ToList();
                    programm_step_param_list = dbContext.programm_step_param.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.step_id).ThenBy(ss => ss.param_num).ToList();
                    programm_step_condition_list = dbContext.programm_step_condition.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.step_id).ThenBy(ss => ss.condition_num).ToList();
                    programm_step_logic_list = dbContext.programm_step_logic.Where(ss => ss.programm_id == programm.programm_id).OrderBy(ss => ss.step_id).ThenBy(ss => ss.logic_num).ToList();

                    // изначально считаем что все параметры шага - это одиночные параметры,
                    // но все же создаем массив param_calc_value_pos (чтоб дальше знать что он гарантировано есть)
                    foreach (var item in programm_step_param_list)
                    {
                        item.for_unit_pos = 0;
                        item.param_calc_value_pos = new object[calc_unit_count];
                    }

                    #endregion

                    block_num = 9;
                    #region Инициализация начальных значений параметров карты

                    // номиналы карты
                    card_nominal_unit_list = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).ToList();
                    if (card_nominal_unit_list == null)
                        card_nominal_unit_list = new List<card_nominal>();
                    foreach (var card_nominal_unit_item in card_nominal_unit_list)
                    {
                        switch ((Enums.CardUnitTypeEnum)card_nominal_unit_item.unit_type)
                        {
                            case Enums.CardUnitTypeEnum.BONUS_PERCENT:
                                calc_card_param_value_BONUS_PERCENT = card_nominal_unit_item.unit_value;
                                break;
                            case Enums.CardUnitTypeEnum.BONUS_VALUE:
                                calc_card_param_value_BONUS_VALUE = card_nominal_unit_item.unit_value;
                                break;
                            case Enums.CardUnitTypeEnum.DISCOUNT_PERCENT:
                                calc_card_param_value_DISCOUNT_PERCENT = card_nominal_unit_item.unit_value;
                                break;
                            case Enums.CardUnitTypeEnum.DISCOUNT_VALUE:
                                calc_card_param_value_DISCOUNT_VALUE = card_nominal_unit_item.unit_value;
                                break;
                            case Enums.CardUnitTypeEnum.MONEY:
                                calc_card_param_value_MONEY = card_nominal_unit_item.unit_value;
                                break;
                            default:
                                break;
                        }
                    }

                    calc_card_param_value_SUMMA = card.curr_trans_sum;
                    calc_card_param_value_TRANS_COUNT = card.curr_trans_count;
                    calc_card_param_value_CARD_STATUS = card_status_id;

                    // лимиты карты и использованные лимиты карты

                    // !!!
                    /*
                    curr_limit_list = dbContext.card_type_limit.Where(ss => ss.card_type_id == card_type_id
                        //&& ss.limit_type == (int)Enums.CardLimitType.MONEY_MONTHLY
                        && ss.limit_date_beg.HasValue && ss.limit_date_end.HasValue
                        && ss.limit_date_beg <= calc_trans_param_value_TIME && ss.limit_date_end >= calc_trans_param_value_TIME
                        ).Distinct().ToList();
                    */
                    DateTime calc_trans_param_value_TIME_date = calc_trans_param_value_TIME.Date;
                    curr_limit_list = dbContext.card_type_limit.Where(ss => ss.card_type_id == card_type_id
                        //&& ss.limit_type == (int)Enums.CardLimitType.MONEY_MONTHLY
                        && ss.limit_date_beg.HasValue && ss.limit_date_end.HasValue
                        && ss.limit_date_beg <= calc_trans_param_value_TIME_date && ss.limit_date_end >= calc_trans_param_value_TIME_date
                        ).Distinct().ToList();
                    if ((curr_limit_list != null) && (curr_limit_list.Count > 0))
                    {
                        foreach (var curr_limit in curr_limit_list)
                        {
                            switch ((Enums.CardLimitType)curr_limit.limit_type)
                            {
                                case Enums.CardLimitType.MoneyMonthly:
                                case Enums.CardLimitType.MoneyYearly:
                                    curr_money_limit = curr_limit;
                                    calc_card_param_value_CARD_MONEY_LIMIT = curr_limit.limit_value;
                                    //var curr_money_limit_used = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id && ss.curr_limit_id == curr_limit.limit_id).FirstOrDefault();
                                    var curr_money_limit_used = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id && ss.curr_limit_id == curr_limit.limit_id).ToList();
                                    if ((curr_money_limit_used != null) && (curr_money_limit_used.Count > 0))
                                    {
                                        calc_card_param_value_CARD_MONEY_LIMIT_USED = curr_money_limit_used.Select(ss => ss.curr_sum).Sum();
                                        calc_card_param_value_CARD_MONEY_LIMIT_USED_init_value = calc_card_param_value_CARD_MONEY_LIMIT_USED;
                                    }
                                    break;
                                case Enums.CardLimitType.BonusDaily:
                                    curr_bonus_limit = curr_limit;
                                    calc_card_param_value_CARD_BONUS_LIMIT = curr_limit.limit_value;
                                    //var curr_bonus_limit_used = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id && ss.curr_limit_id == curr_limit.limit_id).FirstOrDefault();
                                    var curr_bonus_limit_used = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id && ss.curr_limit_id == curr_limit.limit_id).ToList();
                                    if ((curr_bonus_limit_used != null) && (curr_bonus_limit_used.Count > 0))
                                    {
                                        calc_card_param_value_CARD_BONUS_LIMIT_USED = curr_bonus_limit_used.Select(ss => ss.curr_sum).Sum();
                                        calc_card_param_value_CARD_BONUS_LIMIT_USED_init_value = calc_card_param_value_CARD_BONUS_LIMIT_USED;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    // неактивные бонусы
                    calc_card_param_value_BONUS_VALUE_INACTIVE = dbContext.card_nominal_inactive.Where(ss => ss.card_id == card.card_id && ss.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_VALUE).Sum(ss => ss.unit_value);

                    #endregion

                    #region Инициализация начальных значений параметров алгоритма (для одиночных параметров)

                    foreach (var pp in programm_param_list)
                    {
                        if (pp.for_unit_pos == 1)
                            continue;

                        switch ((Enums.ProgrammParamType)pp.param_type)
                        {
                            case Enums.ProgrammParamType.INT_VAL:
                                pp.param_calc_value = pp.int_value;
                                break;
                            case Enums.ProgrammParamType.FLOAT_VAL:
                                pp.param_calc_value = pp.float_value;
                                break;
                            case Enums.ProgrammParamType.DATE_VAL:
                            case Enums.ProgrammParamType.DATE_ONLY_VAL:
                                DateTime? tmpDateTime = ((Enums.ProgrammParamType)pp.param_type) == Enums.ProgrammParamType.DATE_VAL ? pp.date_value : pp.date_only_value;
                                if (tmpDateTime.HasValue)
                                {
                                    if ((business_org != null) && (business_org.add_to_timezone.HasValue))
                                        tmpDateTime.Value.AddHours((double)business_org.add_to_timezone);
                                    else if (business.add_to_timezone.HasValue)
                                        tmpDateTime.Value.AddHours((double)business.add_to_timezone);
                                    if ((Enums.ProgrammParamType)pp.param_type == Enums.ProgrammParamType.DATE_ONLY_VAL)
                                    {
                                        tmpDateTime = ((DateTime)tmpDateTime).Date;
                                    }
                                }
                                pp.param_calc_value = tmpDateTime;
                                break;
                            case Enums.ProgrammParamType.TIME_ONLY_VAL:
                                pp.param_calc_value = pp.time_only_value;
                                break;
                            case Enums.ProgrammParamType.STRING_VAL:
                                pp.param_calc_value = pp.string_value;
                                break;
                            default:
                                break;
                        }
                    }

                    #endregion

                    #region Инициализация начальных значений параметров алгоритма (для параметров по позициям продажи)

                    foreach (var pp in programm_param_list)
                    {
                        if (pp.for_unit_pos != 1)
                            continue;

                        pp.param_calc_value_pos = new object[calc_unit_count];

                        foreach (var unit in calc_unit_list_res.OrderBy(ss => ss.unit_num))
                        {
                            if (unit.unit_num == UNIT_NUM_FOR_SUM)
                                continue;

                            switch ((Enums.ProgrammParamType)pp.param_type)
                            {
                                case Enums.ProgrammParamType.INT_VAL:
                                    pp.param_calc_value_pos[unit.unit_num - 1] = pp.int_value;
                                    break;
                                case Enums.ProgrammParamType.FLOAT_VAL:
                                    pp.param_calc_value_pos[unit.unit_num - 1] = pp.float_value;
                                    break;
                                case Enums.ProgrammParamType.DATE_VAL:
                                case Enums.ProgrammParamType.DATE_ONLY_VAL:
                                    DateTime? tmpDateTime = ((Enums.ProgrammParamType)pp.param_type) == Enums.ProgrammParamType.DATE_VAL ? pp.date_value : pp.date_only_value;
                                    if (tmpDateTime.HasValue)
                                    {
                                        if ((business_org != null) && (business_org.add_to_timezone.HasValue))
                                            tmpDateTime.Value.AddHours((double)business_org.add_to_timezone);
                                        else if (business.add_to_timezone.HasValue)
                                            tmpDateTime.Value.AddHours((double)business.add_to_timezone);
                                        if ((Enums.ProgrammParamType)pp.param_type == Enums.ProgrammParamType.DATE_ONLY_VAL)
                                        {
                                            tmpDateTime = ((DateTime)tmpDateTime).Date;
                                        }
                                    }
                                    pp.param_calc_value_pos[unit.unit_num - 1] = tmpDateTime;
                                    break;
                                case Enums.ProgrammParamType.TIME_ONLY_VAL:
                                    pp.param_calc_value_pos[unit.unit_num - 1] = pp.time_only_value;
                                    break;
                                case Enums.ProgrammParamType.STRING_VAL:
                                    pp.param_calc_value_pos[unit.unit_num - 1] = pp.string_value;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    #endregion

                    block_num = 10;
                    #region Инициализация результата расчета

                    programm_result = new programm_result();
                    programm_result.programm_id = programm.programm_id;
                    programm_result.date_beg = calc_trans_param_value_TIME;
                    programm_result.is_test = isTestMode ? (short)1 : (short)0;

                    #endregion

                    ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc start");

                    block_num = 11;
                    block_subnum = 0;
                    #region Расчет

                    // цикл по всем шагам алгоритма
                    foreach (var step in programm_step_list)
                    {
                        // цикл по всем логикам шага
                        foreach (var curr_logic in programm_step_logic_list.Where(ss => ss.step_id == step.step_id).OrderBy(ss => ss.logic_num))
                        {
                            block_subnum = 1;
                            var op1_param = (from l in programm_step_logic_list
                                             from p in programm_step_param_list
                                             where l.step_id == p.step_id
                                             && l.op1_param_id == p.param_id
                                             && l.step_id == step.step_id
                                             && l.logic_id == curr_logic.logic_id
                                             select p)
                                            .FirstOrDefault();
                            if (op1_param == null)
                                return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден первый параметр шага " + step.step_num.ToString()));
                            var op2_param = (from l in programm_step_logic_list
                                             from p in programm_step_param_list
                                             where l.step_id == p.step_id
                                             && l.op2_param_id == p.param_id
                                             && l.step_id == step.step_id
                                             && l.logic_id == curr_logic.logic_id
                                             select p)
                                            .FirstOrDefault();

                            // op2_param может быть не задан
                            if (op2_param == null)
                            {
                                op2_param = new programm_step_param()
                                {
                                    param_id = 0,
                                    programm_id = op1_param.programm_id,
                                    step_id = op1_param.step_id,
                                    card_param_type = null,
                                    trans_param_type = null,
                                    programm_param_id = null,
                                    prev_step_param_id = null,
                                    param_num = 2,
                                    param_calc_value = 0,
                                }
                                ;
                            }
                            var op3_param = (from l in programm_step_logic_list
                                             from p in programm_step_param_list
                                             where l.step_id == p.step_id
                                             && l.op3_param_id == p.param_id
                                             && l.step_id == step.step_id
                                             && l.logic_id == curr_logic.logic_id
                                             select p)
                                            .FirstOrDefault();
                            if (op3_param == null)
                                return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден третий параметр шага " + step.step_num.ToString()));
                            var op4_param = (from l in programm_step_logic_list
                                             from p in programm_step_param_list
                                             where l.step_id == p.step_id
                                             && l.op4_param_id == p.param_id
                                             && l.step_id == step.step_id
                                             && l.logic_id == curr_logic.logic_id
                                             select p)
                                            .FirstOrDefault();
                            // op4_param может быть не задан
                            if (op4_param == null)
                            {
                                op4_param = new programm_step_param()
                                {
                                    param_id = 0,
                                    programm_id = op3_param.programm_id,
                                    step_id = op3_param.step_id,
                                    card_param_type = null,
                                    trans_param_type = null,
                                    programm_param_id = null,
                                    prev_step_param_id = null,
                                    param_num = 999999,
                                    param_calc_value = 0,
                                }
                                ;
                            }

                            programm_step_param op1_condition_param = null;
                            programm_step_param op2_condition_param = null;

                            // цикл по всем позициям продажи
                            foreach (var position in calc_unit_list_res.OrderBy(ss => ss.unit_num))
                            {
                                block_subnum = 2;

                                if ((step.single_use_only == 1) && (position.unit_num != UNIT_NUM_FOR_SUM))
                                    continue;
                                if ((step.single_use_only != 1) && (position.unit_num == UNIT_NUM_FOR_SUM))
                                    continue;

                                // результат шага: данные шага и продажи
                                programm_step_result = new programm_step_result();
                                programm_step_result.programm_result = programm_result;
                                programm_step_result.programm_id = step.programm_id;
                                programm_step_result.step_id = step.step_id;
                                programm_step_result.step_num = step.step_num;
                                programm_step_result.single_use_only = step.single_use_only;
                                programm_step_result.position_num = position.unit_num;
                                programm_step_result.position_name = position.unit_name;
                                programm_step_result.position_value = position.unit_value;
                                programm_step_result.position_max_discount_percent = position.unit_max_discount_percent;

                                // todo
                                //programm_step_result.position_max_bonus_for_card_percent = position.unit_max_bonus_for_card_percent;
                                //programm_step_result.position_max_bonus_for_pay_percent = position.unit_max_bonus_for_pay_percent;                            

                                foreach (var op_param in programm_step_param_list.Where(ss => ss.step_id == step.step_id))
                                {
                                    block_subnum = 3;
                                    if (op_param.card_param_type.HasValue)
                                    {
                                        switch ((Enums.CardParamType)op_param.card_param_type)
                                        {
                                            case Enums.CardParamType.BONUS_PERCENT:
                                                op_param.param_calc_value = calc_card_param_value_BONUS_PERCENT;
                                                break;
                                            case Enums.CardParamType.BONUS_VALUE:
                                                op_param.param_calc_value = calc_card_param_value_BONUS_VALUE;
                                                break;
                                            case Enums.CardParamType.DISCOUNT_PERCENT:
                                                op_param.param_calc_value = calc_card_param_value_DISCOUNT_PERCENT;
                                                break;
                                            case Enums.CardParamType.DISCOUNT_VALUE:
                                                op_param.param_calc_value = calc_card_param_value_DISCOUNT_VALUE;
                                                break;
                                            case Enums.CardParamType.MONEY:
                                                op_param.param_calc_value = calc_card_param_value_MONEY;
                                                break;
                                            case Enums.CardParamType.SUMMA:
                                                op_param.param_calc_value = calc_card_param_value_SUMMA;
                                                break;
                                            case Enums.CardParamType.TRANS_COUNT:
                                                op_param.param_calc_value = calc_card_param_value_TRANS_COUNT;
                                                break;
                                            case Enums.CardParamType.CARD_STATUS:
                                                op_param.param_calc_value = calc_card_param_value_CARD_STATUS;
                                                break;
                                            case Enums.CardParamType.CARD_MONEY_LIMIT:
                                                op_param.param_calc_value = calc_card_param_value_CARD_MONEY_LIMIT;
                                                break;
                                            case Enums.CardParamType.CARD_MONEY_LIMIT_USED:
                                                op_param.param_calc_value = calc_card_param_value_CARD_MONEY_LIMIT_USED;
                                                break;
                                            case Enums.CardParamType.CARD_BONUS_LIMIT:
                                                op_param.param_calc_value = calc_card_param_value_CARD_BONUS_LIMIT;
                                                break;
                                            case Enums.CardParamType.CARD_BONUS_LIMIT_USED:
                                                op_param.param_calc_value = calc_card_param_value_CARD_BONUS_LIMIT_USED;
                                                break;
                                            case Enums.CardParamType.BONUS_VALUE_INACTIVE:
                                                op_param.param_calc_value = calc_card_param_value_BONUS_VALUE_INACTIVE;
                                                break;
                                            case Enums.CardParamType.EXP_DATE:
                                                // !!!
                                                // todo
                                                break;
                                        }
                                    }
                                    else if (op_param.trans_param_type.HasValue)
                                    {
                                        switch ((Enums.TransParamType)op_param.trans_param_type)
                                        {
                                            case Enums.TransParamType.TIME:
                                                op_param.param_calc_value = calc_trans_param_value_TIME_with_timezone;
                                                break;
                                            case Enums.TransParamType.SUMMA:
                                                op_param.param_calc_value = calc_trans_param_value_SUMMA;
                                                break;
                                            case Enums.TransParamType.SUMMA_DISCOUNT:
                                                op_param.param_calc_value = ctbo_allow_discount ? calc_trans_param_value_SUMMA_DISCOUNT : 0;
                                                break;
                                            case Enums.TransParamType.PERCENT_DISCOUNT:
                                                op_param.param_calc_value = ctbo_allow_discount ? calc_trans_param_value_PERCENT_DISCOUNT : 0;
                                                break;
                                            case Enums.TransParamType.SUMMA_BONUS_FOR_CARD:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_card ? calc_trans_param_value_SUMMA_BONUS_FOR_CARD : 0;
                                                break;
                                            case Enums.TransParamType.PERCENT_BONUS_FOR_CARD:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_card ? calc_trans_param_value_PERCENT_BONUS_FOR_CARD : 0;
                                                break;
                                            case Enums.TransParamType.SUMMA_BONUS_FOR_PAY:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_pay ? calc_trans_param_value_SUMMA_BONUS_FOR_PAY : 0;
                                                break;
                                            case Enums.TransParamType.PERCENT_BONUS_FOR_PAY:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_pay ? calc_trans_param_value_PERCENT_BONUS_FOR_PAY : 0;
                                                break;
                                            case Enums.TransParamType.SUMMA_WITH_DISCOUNT:
                                                op_param.param_calc_value = ctbo_allow_discount ? calc_trans_param_value_SUMMA_WITH_DISCOUNT : calc_trans_param_value_SUMMA;
                                                break;
                                            case Enums.TransParamType.POS_SUMMA_DISCOUNT:
                                                op_param.param_calc_value = ctbo_allow_discount ? position.unit_value_discount : 0;
                                                break;
                                            case Enums.TransParamType.POS_PERCENT_DISCOUNT:
                                                op_param.param_calc_value = ctbo_allow_discount ? position.unit_percent_discount : 0;
                                                break;
                                            case Enums.TransParamType.POS_SUMMA_BONUS_FOR_CARD:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_card ? position.unit_value_bonus_for_card : 0;
                                                break;
                                            case Enums.TransParamType.POS_PERCENT_BONUS_FOR_CARD:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_card ? position.unit_percent_bonus_for_card : 0;
                                                break;
                                            case Enums.TransParamType.POS_SUMMA_BONUS_FOR_PAY:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_pay ? position.unit_value_bonus_for_pay : 0;
                                                break;
                                            case Enums.TransParamType.POS_PERCENT_BONUS_FOR_PAY:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_pay ? position.unit_percent_bonus_for_pay : 0;
                                                break;
                                            case Enums.TransParamType.POS_SUMMA_WITH_DISCOUNT:
                                                op_param.param_calc_value = ctbo_allow_discount ? position.unit_value_with_discount : position.unit_value;
                                                break;
                                            case Enums.TransParamType.POS_SUMMA:
                                                op_param.param_calc_value = position.unit_value;
                                                break;
                                            case Enums.TransParamType.POS_NUM:
                                                op_param.param_calc_value = position.unit_num;
                                                break;
                                            case Enums.TransParamType.POS_NAME:
                                                op_param.param_calc_value = position.unit_name;
                                                break;
                                            case Enums.TransParamType.POS_MAX_DISCOUNT_PERCENT:
                                                op_param.param_calc_value = ctbo_allow_discount ? position.unit_max_discount_percent : 0;
                                                break;
                                            case Enums.TransParamType.POS_MAX_BONUS_FOR_CARD_PERCENT:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_card ? position.unit_max_bonus_for_card_percent : 0;
                                                break;
                                            case Enums.TransParamType.POS_MAX_BONUS_FOR_PAY_PERCENT:
                                                op_param.param_calc_value = ctbo_allow_bonus_for_pay ? position.unit_max_bonus_for_pay_percent : 0;
                                                break;
                                            case Enums.TransParamType.POS_PRICE_MARGIN_PERCENT:
                                                op_param.param_calc_value = position.price_margin_percent;
                                                break;
                                            case Enums.TransParamType.POS_PERCENT_OF_SUMMA:
                                                op_param.param_calc_value = position.unit_percent_of_summa;
                                                break;
                                            case Enums.TransParamType.POS_COUNT:
                                                op_param.param_calc_value = calc_trans_param_value_POS_COUNT;
                                                break;
                                            case Enums.TransParamType.POS_UNIT_COUNT:
                                                op_param.param_calc_value = position.unit_count;
                                                break;
                                            case Enums.TransParamType.POS_LIVE_PREP:
                                                op_param.param_calc_value = position.live_prep;
                                                break;
                                            case Enums.TransParamType.POS_PROMO:
                                                op_param.param_calc_value = position.promo;
                                                break;
                                            case Enums.TransParamType.POS_TAX_PERCENT:
                                                op_param.param_calc_value = position.tax_percent;
                                                break;
                                            case Enums.TransParamType.CARD_IS_SCANNED:
                                                op_param.param_calc_value = calc_trans_param_value_CARD_IS_SCANNED;
                                                break;
                                            case Enums.TransParamType.CARD_IS_CARD_READERED:
                                                op_param.param_calc_value = calc_trans_param_value_CARD_IS_CARD_READERED;
                                                break;
                                            case Enums.TransParamType.POS_TAX_SUMMA:
                                                op_param.param_calc_value = position.tax_summa;
                                                break;
                                            case Enums.TransParamType.POS_NAME_LIST:
                                                op_param.param_calc_value = calc_trans_param_value_POS_NAME_LIST;
                                                break;
                                            case Enums.TransParamType.POS_UNIT_PRICE_TAX:
                                                op_param.param_calc_value = position.unit_price_tax;
                                                break;
                                            case Enums.TransParamType.POS_MESS:
                                                op_param.param_calc_value = calc_trans_param_value_POS_MESS;
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                    else if (op_param.programm_param_id.HasValue)
                                    {
                                        programm_param pp = programm_param_list.Where(ss => ss.param_id == op_param.programm_param_id).FirstOrDefault();
                                        if (pp == null)
                                            return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Шаг " + step.step_num.ToString() + ": не найден параметр алгоритма " + op_param.programm_param_id.ToString()));

                                        if (pp.for_unit_pos != 1)
                                        {
                                            op_param.param_calc_value = pp.param_calc_value;
                                            op_param.for_unit_pos = 0;
                                        }
                                        else
                                        {
                                            op_param.param_calc_value_pos[position.unit_num - 1] = pp.param_calc_value_pos[position.unit_num - 1];
                                            op_param.for_unit_pos = 1;
                                        }
                                    }
                                    else if (op_param.prev_step_param_id.HasValue)
                                    {
                                        programm_step_param psp = programm_step_param_list.Where(ss => ss.param_id == op_param.prev_step_param_id
                                            && !ss.prev_step_param_id.HasValue).FirstOrDefault();
                                        if (psp == null)
                                            return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден параметр шага алгоритма " + op_param.prev_step_param_id.ToString()));
                                        if (psp.for_unit_pos != 1)
                                        {
                                            op_param.param_calc_value = psp.param_calc_value;
                                        }
                                        else
                                        {
                                            op_param.param_calc_value_pos[position.unit_num - 1] = psp.param_calc_value_pos[position.unit_num - 1];
                                        }
                                    }

                                } // foreach (var op_param in op_param_list)

                                // учитываем условие шага
                                bool cond_result = true;
                                List<programm_step_condition> curr_condition_list = programm_step_condition_list.Where(ss => ss.step_id == step.step_id && ss.condition_id == curr_logic.condition_id.GetValueOrDefault(0)).OrderBy(ss => ss.condition_num).ToList();
                                if (curr_condition_list != null)
                                {
                                    cond_num = 1;
                                    foreach (programm_step_condition curr_condition in curr_condition_list.OrderBy(ss => ss.condition_num))
                                    {
                                        block_subnum = 4;
                                        op1_condition_param = (from c in programm_step_condition_list
                                                               from p in programm_step_param_list
                                                               where c.step_id == p.step_id
                                                               && c.op1_param_id == p.param_id
                                                               && c.step_id == step.step_id
                                                               && c.condition_id == curr_condition.condition_id
                                                               select p)
                                                                    .FirstOrDefault();
                                        if (op1_condition_param == null)
                                            return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден первый параметр условия " + curr_condition.condition_num.ToString() + " выполнения шага " + step.step_num.ToString()));
                                        op2_condition_param = (from c in programm_step_condition_list
                                                               from p in programm_step_param_list
                                                               where c.step_id == p.step_id
                                                               && c.op2_param_id == p.param_id
                                                               && c.step_id == step.step_id
                                                               && c.condition_id == curr_condition.condition_id
                                                               select p)
                                                                    .FirstOrDefault();
                                        if (op2_condition_param == null)
                                            return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден второй параметр условия " + curr_condition.condition_num.ToString() + " выполнения шага " + step.step_num.ToString()));

                                        object curr_op1_condition_param_calc_value = op1_condition_param.for_unit_pos != 1 ? op1_condition_param.param_calc_value : op1_condition_param.param_calc_value_pos[position.unit_num - 1];
                                        object curr_op2_condition_param_calc_value = op2_condition_param.for_unit_pos != 1 ? op2_condition_param.param_calc_value : op2_condition_param.param_calc_value_pos[position.unit_num - 1];

                                        switch ((Enums.ConditionType)curr_condition.op_type)
                                        {
                                            // todo:
                                            // сделать не только для decimal
                                            case Enums.ConditionType.EQUALS:
                                                cond_result = Convert.ToDecimal(curr_op1_condition_param_calc_value == null ? 0 : curr_op1_condition_param_calc_value)
                                                    .Equals(Convert.ToDecimal(curr_op2_condition_param_calc_value == null ? 0 : curr_op2_condition_param_calc_value));
                                                break;
                                            case Enums.ConditionType.NOT_EQUALS:
                                                cond_result = !(Convert.ToDecimal(curr_op1_condition_param_calc_value == null ? 0 : curr_op1_condition_param_calc_value)
                                                    .Equals(Convert.ToDecimal(curr_op2_condition_param_calc_value == null ? 0 : curr_op2_condition_param_calc_value)));
                                                break;
                                            case Enums.ConditionType.MORE_THAN:
                                                cond_result = Convert.ToDecimal(curr_op1_condition_param_calc_value == null ? 0 : curr_op1_condition_param_calc_value)
                                                    > Convert.ToDecimal(curr_op2_condition_param_calc_value == null ? 0 : curr_op2_condition_param_calc_value);
                                                break;
                                            case Enums.ConditionType.MORE_THAN_OR_EQUALS:
                                                cond_result = Convert.ToDecimal(curr_op1_condition_param_calc_value == null ? 0 : curr_op1_condition_param_calc_value)
                                                    >= Convert.ToDecimal(curr_op2_condition_param_calc_value == null ? 0 : curr_op2_condition_param_calc_value);
                                                break;
                                            case Enums.ConditionType.IN_LIST:
                                                string par1 = curr_op1_condition_param_calc_value == null ? null : curr_op1_condition_param_calc_value as string;
                                                cond_result = GlobalUtil.ListContainsName(par1
                                                    , Convert.ToString(curr_op2_condition_param_calc_value == null ? "" : curr_op2_condition_param_calc_value)
                                                    , UNIT_NAME_SEPARATOR
                                                    );
                                                break;
                                            case Enums.ConditionType.MULTIPLE: // кратно
                                                cond_result = (Convert.ToInt32(curr_op1_condition_param_calc_value == null ? 0 : curr_op1_condition_param_calc_value)
                                                    % Convert.ToInt32(curr_op2_condition_param_calc_value == null ? 0 : curr_op2_condition_param_calc_value)) == 0;
                                                break;
                                            default:
                                                cond_result = false;
                                                break;
                                        }

                                        // результат шага: условия шага
                                        programm_step_result.condition_id = curr_condition.condition_id;
                                        programm_step_result.condition_op_type = curr_condition.op_type;
                                        programm_step_result.condition_op1_param_id = op1_condition_param.param_id;
                                        programm_step_result.condition_op1_param_value = Convert.ToString(curr_op1_condition_param_calc_value);
                                        programm_step_result.condition_op2_param_id = op2_condition_param.param_id;
                                        programm_step_result.condition_op2_param_value = Convert.ToString(curr_op2_condition_param_calc_value);
                                        programm_step_result.condition_result = cond_result ? (short)1 : (short)0;
                                        programm_step_result.condition_num = curr_condition.condition_num;
                                        programm_step_result.condition_name = curr_condition.condition_name;

                                        if (!cond_result)
                                            break;
                                    }
                                }

                                object curr_op1_param_param_calc_value = op1_param != null ? (op1_param.for_unit_pos != 1 ? op1_param.param_calc_value : op1_param.param_calc_value_pos[position.unit_num - 1]) : null;
                                object curr_op2_param_param_calc_value = op2_param != null ? (op2_param.for_unit_pos != 1 ? op2_param.param_calc_value : op2_param.param_calc_value_pos[position.unit_num - 1]) : null;
                                object curr_op3_param_param_calc_value = op3_param != null ? (op3_param.for_unit_pos != 1 ? op3_param.param_calc_value : op3_param.param_calc_value_pos[position.unit_num - 1]) : null;
                                object curr_op4_param_param_calc_value = op4_param != null ? (op4_param.for_unit_pos != 1 ? op4_param.param_calc_value : op4_param.param_calc_value_pos[position.unit_num - 1]) : null;

                                // выполняем логику шага
                                if (cond_result)
                                {
                                    block_subnum = 5;

                                    switch ((Enums.LogicType)curr_logic.op_type)
                                    {
                                        case Enums.LogicType.PLUS:
                                            curr_op3_param_param_calc_value = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(curr_op1_param_param_calc_value == null ? 0 : curr_op1_param_param_calc_value)
                                                + Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value));
                                            break;
                                        case Enums.LogicType.MINUS:
                                            curr_op3_param_param_calc_value = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(curr_op1_param_param_calc_value == null ? 0 : curr_op1_param_param_calc_value)
                                                - Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value));
                                            break;
                                        case Enums.LogicType.PERCENT:
                                            // GetPercent_Value - вернет процент
                                            // (для расчета по алгоритмам "начисление бонусов")
                                            curr_op3_param_param_calc_value = GlobalUtil.ToTwoDigitsAfterComma(GlobalUtil.GetPercent_Value(Convert.ToDecimal(curr_op1_param_param_calc_value == null ? 0 : curr_op1_param_param_calc_value)
                                                , Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value)));
                                            break;
                                        case Enums.LogicType.RESULT_WITH_PERCENT:
                                            // GetPercent_ResultWithPercent - вернет сумму за вычетом процента
                                            // (для расчета по алгоритмам "процент скидки")
                                            curr_op3_param_param_calc_value = GlobalUtil.ToTwoDigitsAfterComma(GlobalUtil.GetPercent_ResultWithPercent(Convert.ToDecimal(curr_op1_param_param_calc_value == null ? 0 : curr_op1_param_param_calc_value)
                                                , Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value)));
                                            break;
                                        case Enums.LogicType.PERCENT_SUMMA:
                                            // GetPercent_Summa - вернет процент числа от суммы
                                            // (для расчета процента позиции от суммы)
                                            curr_op3_param_param_calc_value = GlobalUtil.ToTwoDigitsAfterComma(GlobalUtil.GetPercent_Summa(Convert.ToDecimal(curr_op1_param_param_calc_value == null ? 0 : curr_op1_param_param_calc_value)
                                                , Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value)));
                                            break;
                                        case Enums.LogicType.MULTIPLY:
                                            curr_op3_param_param_calc_value = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(curr_op1_param_param_calc_value == null ? 0 : curr_op1_param_param_calc_value)
                                                * Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value));
                                            break;
                                        case Enums.LogicType.DIVIDE:
                                            if (Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value) == 0)
                                            {
                                                curr_op3_param_param_calc_value = 0;
                                                break;
                                            }
                                            curr_op3_param_param_calc_value = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(curr_op1_param_param_calc_value == null ? 0 : curr_op1_param_param_calc_value)
                                                / Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value));
                                            break;
                                        case Enums.LogicType.TAKE_PARAM_1:
                                            curr_op3_param_param_calc_value = curr_op1_param_param_calc_value;
                                            break;
                                        case Enums.LogicType.TAKE_DATE:
                                            curr_op3_param_param_calc_value = Convert.ToDateTime(curr_op1_param_param_calc_value).Date;
                                            break;
                                        case Enums.LogicType.TAKE_TIME:
                                            curr_op3_param_param_calc_value = Convert.ToDateTime(curr_op1_param_param_calc_value).TimeOfDay;
                                            break;
                                        case Enums.LogicType.TAKE_YEAR:
                                            curr_op3_param_param_calc_value = Convert.ToDateTime(curr_op1_param_param_calc_value).Year;
                                            break;
                                        case Enums.LogicType.TAKE_MONTH:
                                            curr_op3_param_param_calc_value = Convert.ToDateTime(curr_op1_param_param_calc_value).Month;
                                            break;
                                        case Enums.LogicType.TAKE_DAY_OF_MONTH:
                                            curr_op3_param_param_calc_value = Convert.ToDateTime(curr_op1_param_param_calc_value).Day;
                                            break;
                                        case Enums.LogicType.TAKE_DAY_OF_WEEK:
                                            DayOfWeek tmpDay = Convert.ToDateTime(curr_op1_param_param_calc_value).DayOfWeek;
                                            curr_op3_param_param_calc_value = (tmpDay == DayOfWeek.Sunday) ? 7 : (int)tmpDay;
                                            break;
                                        case Enums.LogicType.TAKE_HOUR:
                                            curr_op3_param_param_calc_value = Convert.ToDateTime(curr_op1_param_param_calc_value).Hour;
                                            break;
                                        case Enums.LogicType.TAKE_MINUTE:
                                            curr_op3_param_param_calc_value = Convert.ToDateTime(curr_op1_param_param_calc_value).Minute;
                                            break;
                                        case Enums.LogicType.DIVISION_WHOLE:
                                            if (Convert.ToDecimal(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value) == 0)
                                            {
                                                curr_op3_param_param_calc_value = 0;
                                                break;
                                            }
                                            curr_op3_param_param_calc_value = Convert.ToInt32(curr_op1_param_param_calc_value == null ? 0 : curr_op1_param_param_calc_value)
                                                / Convert.ToInt32(curr_op2_param_param_calc_value == null ? 0 : curr_op2_param_param_calc_value);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                else
                                {
                                    // op4_param - результат логики, если условие не выполнилось
                                    if (curr_op4_param_param_calc_value.GetType() == typeof(string))
                                    {
                                        curr_op3_param_param_calc_value = Convert.ToString(curr_op4_param_param_calc_value);
                                    }
                                    else
                                    {
                                        curr_op3_param_param_calc_value = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(curr_op4_param_param_calc_value == null ? 0 : curr_op4_param_param_calc_value));
                                    }
                                }

                                if (op3_param.for_unit_pos != 1)
                                {
                                    op3_param.param_calc_value = curr_op3_param_param_calc_value;
                                }
                                else
                                {
                                    op3_param.param_calc_value_pos[position.unit_num - 1] = curr_op3_param_param_calc_value;
                                }

                                block_subnum = 6;

                                // результат шага: логика и параметры шага
                                programm_step_result.logic_id = curr_logic.logic_id;
                                programm_step_result.logic_num = curr_logic.logic_num;
                                programm_step_result.logic_op_type = curr_logic.op_type;

                                programm_step_result.logic_op1_card_param_type = op1_param.card_param_type;
                                programm_step_result.logic_op1_is_programm_output = op1_param.is_programm_output;
                                programm_step_result.logic_op1_param_id = op1_param.param_id;
                                programm_step_result.logic_op1_param_name = op1_param.param_name;
                                programm_step_result.logic_op1_param_num = op1_param.param_num;
                                programm_step_result.logic_op1_param_value = Convert.ToString(curr_op1_param_param_calc_value);
                                programm_step_result.logic_op1_prev_step_param_id = op1_param.prev_step_param_id;
                                programm_step_result.logic_op1_programm_param_id = op1_param.programm_param_id;
                                programm_step_result.logic_op1_trans_param_type = op1_param.trans_param_type;

                                programm_step_result.logic_op2_card_param_type = op2_param.card_param_type;
                                programm_step_result.logic_op2_is_programm_output = op2_param.is_programm_output;
                                programm_step_result.logic_op2_param_id = op2_param.param_id;
                                programm_step_result.logic_op2_param_name = op2_param.param_name;
                                programm_step_result.logic_op2_param_num = op2_param.param_num;
                                programm_step_result.logic_op2_param_value = Convert.ToString(curr_op2_param_param_calc_value);
                                programm_step_result.logic_op2_prev_step_param_id = op2_param.prev_step_param_id;
                                programm_step_result.logic_op2_programm_param_id = op2_param.programm_param_id;
                                programm_step_result.logic_op2_trans_param_type = op2_param.trans_param_type;

                                programm_step_result.logic_op3_card_param_type = op3_param.card_param_type;
                                programm_step_result.logic_op3_is_programm_output = op3_param.is_programm_output;
                                programm_step_result.logic_op3_param_id = op3_param.param_id;
                                programm_step_result.logic_op3_param_name = op3_param.param_name;
                                programm_step_result.logic_op3_param_num = op3_param.param_num;
                                programm_step_result.logic_op3_param_value = Convert.ToString(curr_op3_param_param_calc_value);
                                programm_step_result.logic_op3_prev_step_param_id = op3_param.prev_step_param_id;
                                programm_step_result.logic_op3_programm_param_id = op3_param.programm_param_id;
                                programm_step_result.logic_op3_trans_param_type = op3_param.trans_param_type;

                                programm_step_result.logic_op4_card_param_type = op4_param.card_param_type;
                                programm_step_result.logic_op4_is_programm_output = op4_param.is_programm_output;
                                programm_step_result.logic_op4_param_id = op4_param.param_id;
                                programm_step_result.logic_op4_param_name = op4_param.param_name;
                                programm_step_result.logic_op4_param_num = op4_param.param_num;
                                programm_step_result.logic_op4_param_value = Convert.ToString(curr_op4_param_param_calc_value);
                                programm_step_result.logic_op4_prev_step_param_id = op4_param.prev_step_param_id;
                                programm_step_result.logic_op4_programm_param_id = op4_param.programm_param_id;
                                programm_step_result.logic_op4_trans_param_type = op4_param.trans_param_type;

                                if (programm_step_result_list == null)
                                    programm_step_result_list = new List<programm_step_result>();
                                programm_step_result_list.Add(programm_step_result);
                                if (isTestMode)
                                    dbContext.programm_step_result.Add(programm_step_result);

                                // обновляем промежуточные результаты
                                block_subnum = 7;
                                if (op3_param.trans_param_type.HasValue)
                                {
                                    switch ((Enums.TransParamType)op3_param.trans_param_type)
                                    {
                                        case Enums.TransParamType.TIME:
                                            calc_trans_param_value_TIME_with_timezone = Convert.ToDateTime(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.SUMMA:
                                            calc_trans_param_value_SUMMA = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.SUMMA_DISCOUNT:
                                            calc_trans_param_value_SUMMA_DISCOUNT = ctbo_allow_discount ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.PERCENT_DISCOUNT:
                                            calc_trans_param_value_PERCENT_DISCOUNT = ctbo_allow_discount ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.SUMMA_BONUS_FOR_CARD:
                                            calc_trans_param_value_SUMMA_BONUS_FOR_CARD = ctbo_allow_bonus_for_card ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.PERCENT_BONUS_FOR_CARD:
                                            calc_trans_param_value_PERCENT_BONUS_FOR_CARD = ctbo_allow_bonus_for_card ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.SUMMA_BONUS_FOR_PAY:
                                            calc_trans_param_value_SUMMA_BONUS_FOR_PAY = ctbo_allow_bonus_for_pay ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.PERCENT_BONUS_FOR_PAY:
                                            calc_trans_param_value_PERCENT_BONUS_FOR_PAY = ctbo_allow_bonus_for_pay ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.SUMMA_WITH_DISCOUNT:
                                            calc_trans_param_value_SUMMA_WITH_DISCOUNT = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_SUMMA_DISCOUNT:
                                            position.unit_value_discount = ctbo_allow_discount ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_PERCENT_DISCOUNT:
                                            position.unit_percent_discount = ctbo_allow_discount ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_SUMMA_BONUS_FOR_CARD:
                                            position.unit_value_bonus_for_card = ctbo_allow_bonus_for_card ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_PERCENT_BONUS_FOR_CARD:
                                            position.unit_percent_bonus_for_card = ctbo_allow_bonus_for_card ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_SUMMA_BONUS_FOR_PAY:
                                            position.unit_value_bonus_for_pay = ctbo_allow_bonus_for_pay ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_PERCENT_BONUS_FOR_PAY:
                                            position.unit_percent_bonus_for_pay = ctbo_allow_bonus_for_pay ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_SUMMA_WITH_DISCOUNT:
                                            position.unit_value_with_discount = ctbo_allow_discount ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_SUMMA:
                                            position.unit_value = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_NUM:
                                            position.unit_num = Convert.ToInt32(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_NAME:
                                            position.unit_name = Convert.ToString(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_MAX_DISCOUNT_PERCENT:
                                            position.unit_max_discount_percent = ctbo_allow_discount ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_MAX_BONUS_FOR_CARD_PERCENT:
                                            position.unit_max_bonus_for_card_percent = ctbo_allow_bonus_for_card ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_MAX_BONUS_FOR_PAY_PERCENT:
                                            position.unit_max_bonus_for_pay_percent = ctbo_allow_bonus_for_pay ? Convert.ToDecimal(op3_param.param_calc_value) : 0;
                                            break;
                                        case Enums.TransParamType.POS_PRICE_MARGIN_PERCENT:
                                            position.price_margin_percent = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_PERCENT_OF_SUMMA:
                                            position.unit_percent_of_summa = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_COUNT:
                                            calc_trans_param_value_POS_COUNT = Convert.ToInt32(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_UNIT_COUNT:
                                            position.unit_count = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_LIVE_PREP:
                                            position.live_prep = Convert.ToInt32(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.CARD_IS_SCANNED:
                                            calc_trans_param_value_CARD_IS_SCANNED = Convert.ToInt32(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.CARD_IS_CARD_READERED:
                                            calc_trans_param_value_CARD_IS_CARD_READERED = Convert.ToInt32(op3_param.param_calc_value);                                            
                                            break;
                                        case Enums.TransParamType.POS_NAME_LIST:
                                            calc_trans_param_value_POS_NAME_LIST = op3_param.param_calc_value as List<string>;
                                            break;
                                        case Enums.TransParamType.POS_UNIT_PRICE_TAX:
                                            position.unit_price_tax = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.TransParamType.POS_MESS:
                                            calc_trans_param_value_POS_MESS = Convert.ToString(op3_param.param_calc_value);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                else if (op3_param.card_param_type.HasValue)
                                {
                                    switch ((Enums.CardParamType)op3_param.card_param_type)
                                    {
                                        case Enums.CardParamType.BONUS_PERCENT:
                                            calc_card_param_value_BONUS_PERCENT = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.BONUS_VALUE:
                                            calc_card_param_value_BONUS_VALUE = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.DISCOUNT_PERCENT:
                                            calc_card_param_value_DISCOUNT_PERCENT = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.DISCOUNT_VALUE:
                                            calc_card_param_value_DISCOUNT_VALUE = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.MONEY:
                                            calc_card_param_value_MONEY = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.EXP_DATE:
                                            break;
                                        case Enums.CardParamType.SUMMA:
                                            calc_card_param_value_SUMMA = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.TRANS_COUNT:
                                            break;
                                        case Enums.CardParamType.CARD_STATUS:
                                            calc_card_param_value_CARD_STATUS = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.CARD_MONEY_LIMIT:
                                            calc_card_param_value_CARD_MONEY_LIMIT = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.CARD_MONEY_LIMIT_USED:
                                            calc_card_param_value_CARD_MONEY_LIMIT_USED = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.CARD_BONUS_LIMIT:
                                            calc_card_param_value_CARD_BONUS_LIMIT = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.CARD_BONUS_LIMIT_USED:
                                            calc_card_param_value_CARD_BONUS_LIMIT_USED = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        case Enums.CardParamType.BONUS_VALUE_INACTIVE:
                                            calc_card_param_value_BONUS_VALUE_INACTIVE = Convert.ToDecimal(op3_param.param_calc_value);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                else if (op3_param.programm_param_id.HasValue)
                                {
                                    programm_param pp = programm_param_list.Where(ss => ss.param_id == op3_param.programm_param_id).FirstOrDefault();
                                    if (pp == null)
                                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Шаг " + step.step_num.ToString() + ": не найден параметр алгоритма " + op3_param.programm_param_id.ToString()));
                                    if (pp.for_unit_pos != 1)
                                    {
                                        pp.param_calc_value = curr_op3_param_param_calc_value;
                                    }
                                    else
                                    {
                                        pp.param_calc_value_pos[position.unit_num - 1] = curr_op3_param_param_calc_value;
                                    }
                                }
                                else if (op3_param.prev_step_param_id.HasValue)
                                {
                                    programm_step_param psp = programm_step_param_list.Where(ss => ss.param_id == op3_param.prev_step_param_id && !ss.prev_step_param_id.HasValue).FirstOrDefault();
                                    if (psp == null)
                                        return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден параметр шага алгоритма " + op3_param.prev_step_param_id.ToString()));
                                    if (psp.for_unit_pos != 1)
                                    {
                                        psp.param_calc_value = curr_op3_param_param_calc_value;
                                    }
                                    else
                                    {
                                        psp.param_calc_value_pos[position.unit_num - 1] = curr_op3_param_param_calc_value;
                                    }
                                }

                            } // foreach (var position in calc_unit_list_res)

                        } // foreach (var curr_logic in programm_step_logic_list)

                    } // foreach (var item in programm_step_list)

                    #endregion
                    block_subnum = 0;

                    ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc write");

                    block_num = 12;
                    #region Сохранение результата расчета

                    // сохраняем результат алоритма
                    if (isTestMode)
                        dbContext.programm_result.Add(programm_result);

                    // insert: card_transaction
                    
                    card_transaction card_transaction = new card_transaction();
                    
                    // !!!
                    // если это убрать - надо поставить Identity в модели для card_trans_id
                    var card_trans_id = dbContext.Database.SqlQuery<long>("select discount.id_generator()").ToList().FirstOrDefault();
                    card_transaction.card_trans_id = card_trans_id;

                    card_transaction.card_id = card.card_id;
                    //card_transaction.trans_num = dbContext.card_transaction.Where(ss => ss.card_id == card.card_id).OrderByDescending(ss => ss.trans_num).Select(ss => ss.trans_num).FirstOrDefault();
                    //card_transaction.trans_num = card_transaction.trans_num.HasValue ? card_transaction.trans_num + 1 : 1;
                    card_transaction.trans_num = 0;
                    card_transaction.date_beg = calc_trans_param_value_TIME;
                    card_transaction.trans_kind = 1;
                    card_transaction.trans_sum = calc_trans_param_value_SUMMA;
                    card_transaction.status = (int)Enums.CardTransactionStatus.ACTIVE;
                    if (isTestMode)
                        card_transaction.programm_result_id = programm_result.result_id;
                    card_transaction.user_name = ls.user_name;
                    card_transaction.date_check = calc_trans_param_value_TIME_with_timezone;

                    if (!isTestMode)
                        dbContext.card_transaction.Add(card_transaction);

                    i = 1;

                    // обновляем параметры продажи
                    // insert: card_transaction_detail
                    List<card_transaction_detail> card_transaction_detail_list = new List<card_transaction_detail>();
                    List<programm_result_detail> programm_result_detail_list = new List<programm_result_detail>();
                    List<programm_step_result> res_item_list = null;
                    foreach (CalcUnit unit in calc_unit_list_res.Where(ss => ss.unit_type == 0))
                    {
                        List<programm_step_result> step_result_list = programm_step_result_list
                            .Where(ss => ss.logic_op3_is_programm_output == 1
                                && ss.logic_op3_trans_param_type.HasValue
                                && ss.position_num == unit.unit_num
                                )
                                .ToList();

                        foreach (var step_result_item in step_result_list)
                        {
                            switch (step_result_item.logic_op3_trans_param_type)
                            {
                                case (int)Enums.TransParamType.POS_PERCENT_BONUS_FOR_CARD:
                                    unit.unit_percent_bonus_for_card = ctbo_allow_bonus_for_card ? Convert.ToDecimal(step_result_item.logic_op3_param_value) : 0;
                                    break;
                                case (int)Enums.TransParamType.POS_PERCENT_BONUS_FOR_PAY:
                                    unit.unit_percent_bonus_for_pay = ctbo_allow_bonus_for_pay ? Convert.ToDecimal(step_result_item.logic_op3_param_value) : 0;
                                    break;
                                case (int)Enums.TransParamType.POS_PERCENT_DISCOUNT:
                                    unit.unit_percent_discount = ctbo_allow_discount ? Convert.ToDecimal(step_result_item.logic_op3_param_value) : 0;
                                    break;
                                case (int)Enums.TransParamType.POS_SUMMA_BONUS_FOR_CARD:
                                    unit.unit_value_bonus_for_card = ctbo_allow_bonus_for_card ? GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value)) : 0;
                                    break;
                                case (int)Enums.TransParamType.POS_SUMMA_BONUS_FOR_PAY:
                                    unit.unit_value_bonus_for_pay = ctbo_allow_bonus_for_pay ? GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value)) : 0;
                                    break;
                                case (int)Enums.TransParamType.POS_SUMMA_DISCOUNT:
                                    unit.unit_value_discount = ctbo_allow_discount ? GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value)) : 0;
                                    break;
                                case (int)Enums.TransParamType.POS_SUMMA_WITH_DISCOUNT:
                                    unit.unit_value_with_discount = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value));
                                    break;
                                case (int)Enums.TransParamType.POS_MAX_BONUS_FOR_CARD_PERCENT:
                                    unit.unit_max_bonus_for_card_percent = ctbo_allow_bonus_for_card ? GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value)) : 0;
                                    break;
                                case (int)Enums.TransParamType.POS_MAX_BONUS_FOR_PAY_PERCENT:
                                    unit.unit_max_bonus_for_pay_percent = ctbo_allow_bonus_for_pay ? GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value)) : 0;
                                    break;
                                case (int)Enums.TransParamType.POS_PRICE_MARGIN_PERCENT:
                                    unit.price_margin_percent = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value));
                                    break;
                                case (int)Enums.TransParamType.POS_TAX_PERCENT:
                                    unit.tax_percent = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value));
                                    break;
                                case (int)Enums.TransParamType.POS_TAX_SUMMA:
                                    unit.tax_summa = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value));
                                    break;
                                case (int)Enums.TransParamType.POS_MESS:
                                    unit.mess = Convert.ToString(step_result_item.logic_op3_param_value);
                                    break;
                                default:
                                    break;
                            }
                        }

                        #region old
                        /*
                    programm_step_result res_item = null;
                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_PERCENT_BONUS_FOR_CARD
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_percent_bonus_for_card = Convert.ToDecimal(res_item.logic_op3_param_value);
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_PERCENT_BONUS_FOR_PAY
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_percent_bonus_for_pay = Convert.ToDecimal(res_item.logic_op3_param_value);
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_PERCENT_DISCOUNT
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_percent_discount = Convert.ToDecimal(res_item.logic_op3_param_value);
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_SUMMA_BONUS_FOR_CARD
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_value_bonus_for_card = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_SUMMA_BONUS_FOR_PAY
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_value_bonus_for_pay = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_SUMMA_DISCOUNT
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_value_discount = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_SUMMA_WITH_DISCOUNT
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_value_with_discount = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                    }
                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_MAX_BONUS_FOR_CARD_PERCENT
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_max_bonus_for_card_percent = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                    }
                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_MAX_BONUS_FOR_PAY_PERCENT
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.unit_max_bonus_for_pay_percent = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                    }
                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.POS_PRICE_MARGIN_PERCENT
                            && ss.position_num == unit.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit.price_margin_percent = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                    }
                    */
                        #endregion

                        unit_value_with_discount_sum += unit.unit_value_with_discount;

                        card_transaction_detail card_transaction_detail = new card_transaction_detail();
                        card_transaction_detail.card_trans_id = card_transaction.card_trans_id;
                        card_transaction_detail.programm_id = programm.programm_id;
                        card_transaction_detail.unit_count = unit.unit_count;
                        card_transaction_detail.unit_max_discount_percent = unit.unit_max_discount_percent;
                        card_transaction_detail.unit_max_bonus_for_card_percent = unit.unit_max_bonus_for_card_percent;
                        card_transaction_detail.unit_max_bonus_for_pay_percent = unit.unit_max_bonus_for_pay_percent;
                        card_transaction_detail.price_margin_percent = unit.price_margin_percent;
                        card_transaction_detail.artikul = unit.artikul;
                        card_transaction_detail.live_prep = unit.live_prep;
                        card_transaction_detail.promo = unit.promo;
                        card_transaction_detail.tax_percent = unit.tax_percent;
                        card_transaction_detail.tax_summa = unit.tax_summa;
                        card_transaction_detail.mess = unit.mess;

                        card_transaction_detail.unit_name = unit.unit_name;
                        card_transaction_detail.unit_num = unit.unit_num;
                        card_transaction_detail.unit_percent_bonus_for_card = unit.unit_percent_bonus_for_card;
                        card_transaction_detail.unit_percent_bonus_for_pay = unit.unit_percent_bonus_for_pay;
                        card_transaction_detail.unit_percent_discount = unit.unit_percent_discount;
                        card_transaction_detail.unit_type = unit.unit_type;

                        card_transaction_detail.unit_value = unit.unit_value;
                        card_transaction_detail.unit_value_bonus_for_card = unit.unit_value_bonus_for_card;
                        card_transaction_detail.unit_value_bonus_for_pay = unit.unit_value_bonus_for_pay;
                        card_transaction_detail.unit_value_discount = unit.unit_value_discount;
                        card_transaction_detail.unit_value_with_discount = unit.unit_value_with_discount;                        

                        if (!isTestMode)
                            dbContext.card_transaction_detail.Add(card_transaction_detail);
                        card_transaction_detail_list.Add(card_transaction_detail);

                        programm_result_detail programm_result_detail = new programm_result_detail();
                        programm_result_detail.result_id = programm_result.result_id;
                        programm_result_detail.programm_id = programm_result.programm_id;
                        programm_result_detail.unit_count = unit.unit_count;
                        programm_result_detail.unit_max_discount_percent = unit.unit_max_discount_percent;
                        programm_result_detail.unit_max_bonus_for_card_percent = unit.unit_max_bonus_for_card_percent;
                        programm_result_detail.unit_max_bonus_for_pay_percent = unit.unit_max_bonus_for_pay_percent;
                        programm_result_detail.price_margin_percent = unit.price_margin_percent;
                        programm_result_detail.artikul = unit.artikul;
                        programm_result_detail.live_prep = unit.live_prep;
                        programm_result_detail.promo = unit.promo;
                        programm_result_detail.tax_percent = unit.tax_percent;
                        programm_result_detail.tax_summa = unit.tax_summa;
                        programm_result_detail.mess = unit.mess;

                        programm_result_detail.unit_name = unit.unit_name;
                        programm_result_detail.unit_num = unit.unit_num;
                        programm_result_detail.unit_percent_bonus_for_card = unit.unit_percent_bonus_for_card;
                        programm_result_detail.unit_percent_bonus_for_pay = unit.unit_percent_bonus_for_pay;
                        programm_result_detail.unit_percent_discount = unit.unit_percent_discount;
                        programm_result_detail.unit_type = unit.unit_type;

                        programm_result_detail.unit_value = unit.unit_value;
                        programm_result_detail.unit_value_bonus_for_card = unit.unit_value_bonus_for_card;
                        programm_result_detail.unit_value_bonus_for_pay = unit.unit_value_bonus_for_pay;
                        programm_result_detail.unit_value_discount = unit.unit_value_discount;
                        programm_result_detail.unit_value_with_discount = unit.unit_value_with_discount;

                        if (isTestMode)
                            dbContext.programm_result_detail.Add(programm_result_detail);
                        programm_result_detail_list.Add(programm_result_detail);

                    }

                    CalcUnit unit_total = calc_unit_list_res.Where(ss => ss.unit_type == 1).FirstOrDefault();
                    bool unit_total_in_res = false;
                    if (unit_total != null)
                    {
                        List<programm_step_result> step_result_list = programm_step_result_list
                            .Where(ss => ss.logic_op3_is_programm_output == 1
                                && ss.logic_op3_trans_param_type.HasValue
                                && ss.position_num == unit_total.unit_num
                                )
                                .ToList();

                        foreach (var step_result_item in step_result_list)
                        {
                            switch (step_result_item.logic_op3_trans_param_type)
                            {
                                case (int)Enums.TransParamType.PERCENT_BONUS_FOR_CARD:
                                    unit_total.unit_percent_bonus_for_card = ctbo_allow_bonus_for_card ? Convert.ToDecimal(step_result_item.logic_op3_param_value) : 0;
                                    unit_total_in_res = true;
                                    break;
                                case (int)Enums.TransParamType.PERCENT_BONUS_FOR_PAY:
                                    unit_total.unit_percent_bonus_for_pay = ctbo_allow_bonus_for_pay ? Convert.ToDecimal(step_result_item.logic_op3_param_value) : 0;
                                    unit_total_in_res = true;
                                    break;
                                case (int)Enums.TransParamType.PERCENT_DISCOUNT:
                                    unit_total.unit_percent_discount = ctbo_allow_discount ? Convert.ToDecimal(step_result_item.logic_op3_param_value) : 0;
                                    unit_total_in_res = true;
                                    break;
                                case (int)Enums.TransParamType.SUMMA_BONUS_FOR_CARD:
                                    unit_total.unit_value_bonus_for_card = ctbo_allow_bonus_for_card ? GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value)) : 0;
                                    unit_total_in_res = true;
                                    break;
                                case (int)Enums.TransParamType.SUMMA_DISCOUNT:
                                    unit_total.unit_value_discount = ctbo_allow_discount ? GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value)) : 0;
                                    unit_total_in_res = true;
                                    break;
                                case (int)Enums.TransParamType.SUMMA_WITH_DISCOUNT:
                                    unit_total.unit_value_with_discount = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value));
                                    unit_total_in_res = true;
                                    break;
                                case (int)Enums.TransParamType.SUMMA_BONUS_FOR_PAY:
                                    unit_total.unit_value_bonus_for_pay = ctbo_allow_bonus_for_pay ? GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(step_result_item.logic_op3_param_value)) : 0;
                                    unit_total_in_res = true;
                                    break;
                                default:
                                    break;
                            }
                        }

                        #region old
                        /*
                    programm_step_result res_item = null;
                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.PERCENT_BONUS_FOR_CARD
                            && ss.position_num == unit_total.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit_total.unit_percent_bonus_for_card = Convert.ToDecimal(res_item.logic_op3_param_value);
                        unit_total_in_res = true;
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.PERCENT_BONUS_FOR_PAY
                            && ss.position_num == unit_total.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit_total.unit_percent_bonus_for_pay = Convert.ToDecimal(res_item.logic_op3_param_value);
                        unit_total_in_res = true;
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.PERCENT_DISCOUNT
                            && ss.position_num == unit_total.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit_total.unit_percent_discount = Convert.ToDecimal(res_item.logic_op3_param_value);
                        unit_total_in_res = true;
                    }

                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.SUMMA_BONUS_FOR_CARD
                            && ss.position_num == unit_total.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit_total.unit_value_bonus_for_card = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                        unit_total_in_res = true;
                    }
                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.SUMMA_DISCOUNT
                            && ss.position_num == unit_total.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit_total.unit_value_discount = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                        unit_total_in_res = true;
                    }
                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.SUMMA_WITH_DISCOUNT
                            && ss.position_num == unit_total.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit_total.unit_value_with_discount = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                        unit_total_in_res = true;
                    }
                    res_item = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_trans_param_type.HasValue
                            && ss.logic_op3_trans_param_type == (int)Enums.TransParamType.SUMMA_BONUS_FOR_PAY
                            && ss.position_num == unit_total.unit_num
                            )
                            .FirstOrDefault();
                    if (res_item != null)
                    {
                        unit_total.unit_value_bonus_for_pay = Util.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                        unit_total_in_res = true;
                    }
                    */
                        #endregion

                        if (unit_total_in_res)
                        {
                            card_transaction_detail card_transaction_detail = new card_transaction_detail();
                            card_transaction_detail.card_trans_id = card_transaction.card_trans_id;
                            card_transaction_detail.programm_id = programm.programm_id;
                            card_transaction_detail.unit_count = unit_total.unit_count;
                            card_transaction_detail.unit_max_discount_percent = unit_total.unit_max_discount_percent;
                            card_transaction_detail.unit_max_bonus_for_card_percent = unit_total.unit_max_bonus_for_card_percent;
                            card_transaction_detail.unit_max_bonus_for_pay_percent = unit_total.unit_max_bonus_for_pay_percent;
                            card_transaction_detail.price_margin_percent = unit_total.price_margin_percent;
                            card_transaction_detail.artikul = null;
                            card_transaction_detail.live_prep = null;
                            card_transaction_detail.promo = null;
                            card_transaction_detail.tax_percent = null;
                            card_transaction_detail.tax_summa = null;
                            card_transaction_detail.mess = null;

                            card_transaction_detail.unit_name = unit_total.unit_name;
                            card_transaction_detail.unit_num = unit_total.unit_num;
                            card_transaction_detail.unit_percent_bonus_for_card = unit_total.unit_percent_bonus_for_card;
                            card_transaction_detail.unit_percent_bonus_for_pay = unit_total.unit_percent_bonus_for_pay;
                            card_transaction_detail.unit_percent_discount = unit_total.unit_percent_discount;
                            card_transaction_detail.unit_type = unit_total.unit_type;

                            card_transaction_detail.unit_value = unit_total.unit_value;
                            card_transaction_detail.unit_value_discount = unit_total.unit_value_discount;
                            card_transaction_detail.unit_value_with_discount = unit_total.unit_value_with_discount;
                            card_transaction_detail.unit_value_bonus_for_card = unit_total.unit_value_bonus_for_card;
                            card_transaction_detail.unit_value_bonus_for_pay = unit_total.unit_value_bonus_for_pay;

                            if (!isTestMode)
                                dbContext.card_transaction_detail.Add(card_transaction_detail);
                            card_transaction_detail_list.Add(card_transaction_detail);

                            programm_result_detail programm_result_detail = new programm_result_detail();
                            programm_result_detail.result_id = programm_result.result_id;
                            programm_result_detail.programm_id = programm_result.programm_id;
                            programm_result_detail.unit_count = unit_total.unit_count;
                            programm_result_detail.unit_max_discount_percent = unit_total.unit_max_discount_percent;
                            programm_result_detail.unit_max_bonus_for_card_percent = unit_total.unit_max_bonus_for_card_percent;
                            programm_result_detail.unit_max_bonus_for_pay_percent = unit_total.unit_max_bonus_for_pay_percent;
                            programm_result_detail.price_margin_percent = unit_total.price_margin_percent;
                            programm_result_detail.artikul = null;
                            programm_result_detail.live_prep = null;
                            programm_result_detail.promo = null;
                            programm_result_detail.tax_percent = null;
                            programm_result_detail.tax_summa = null;
                            programm_result_detail.mess = null;

                            programm_result_detail.unit_name = unit_total.unit_name;
                            programm_result_detail.unit_num = unit_total.unit_num;
                            programm_result_detail.unit_percent_bonus_for_card = unit_total.unit_percent_bonus_for_card;
                            programm_result_detail.unit_percent_bonus_for_pay = unit_total.unit_percent_bonus_for_pay;
                            programm_result_detail.unit_percent_discount = unit_total.unit_percent_discount;
                            programm_result_detail.unit_type = unit_total.unit_type;

                            programm_result_detail.unit_value = unit_total.unit_value;
                            programm_result_detail.unit_value_discount = unit_total.unit_value_discount;
                            programm_result_detail.unit_value_with_discount = unit_total.unit_value_with_discount;
                            programm_result_detail.unit_value_bonus_for_card = unit_total.unit_value_bonus_for_card;
                            programm_result_detail.unit_value_bonus_for_pay = unit_total.unit_value_bonus_for_pay;

                            if (isTestMode)
                                dbContext.programm_result_detail.Add(programm_result_detail);
                            programm_result_detail_list.Add(programm_result_detail);
                        }
                        else
                        {
                            calc_unit_list_res.Remove(unit_total);
                        }
                    }

                    #endregion

                    block_num = 13;
                    #region Обновление параметров карты

                    //insert: card_transaction_unit
                    card_transaction_unit_list_for_add = new List<card_transaction_unit>();

                    res_item_list = programm_step_result_list
                        .Where(ss => ss.logic_op3_is_programm_output == 1
                            && ss.logic_op3_card_param_type.HasValue)
                            .ToList();

                    if (res_item_list != null)
                    {
                        foreach (programm_step_result res_item in res_item_list)
                        {
                            card_transaction_unit card_transaction_unit_for_add = null;

                            switch ((Enums.CardParamType)res_item.logic_op3_card_param_type)
                            {
                                case Enums.CardParamType.BONUS_PERCENT:
                                case Enums.CardParamType.DISCOUNT_PERCENT:
                                    card_transaction_unit_for_add = new card_transaction_unit();
                                    card_transaction_unit_for_add.card_trans_id = card_transaction.card_trans_id;
                                    card_transaction_unit_for_add.unit_type = (int)res_item.logic_op3_card_param_type;
                                    card_transaction_unit_for_add.unit_value = Convert.ToDecimal(res_item.logic_op3_param_value);
                                    card_transaction_unit_list_for_add.Add(card_transaction_unit_for_add);
                                    break;
                                case Enums.CardParamType.BONUS_VALUE:
                                case Enums.CardParamType.DISCOUNT_VALUE:
                                case Enums.CardParamType.MONEY:
                                    card_transaction_unit_for_add = new card_transaction_unit();
                                    card_transaction_unit_for_add.card_trans_id = card_transaction.card_trans_id;
                                    card_transaction_unit_for_add.unit_type = (int)res_item.logic_op3_card_param_type;
                                    card_transaction_unit_for_add.unit_value = GlobalUtil.ToTwoDigitsAfterComma(Convert.ToDecimal(res_item.logic_op3_param_value));
                                    card_transaction_unit_list_for_add.Add(card_transaction_unit_for_add);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    #endregion

                    block_num = 14;
                    #region Обновление номиналов карты

                    if (!isTestMode)
                    {
                        if (card_transaction_unit_list_for_add.Count > 0)
                        {
                            dbContext.card_transaction_unit.AddRange(card_transaction_unit_list_for_add);
                        }

                        //card_nominal_exisiting_list = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id && !ss.date_end.HasValue).ToList();

                        //insert: card_nominal_uncommitted
                        card_nominal_uncommitted card_nominal = null;
                        foreach (var item in card_transaction_unit_list_for_add)
                        {
                            card_nominal = new card_nominal_uncommitted();
                            card_nominal.card_id = card.card_id;
                            card_nominal.date_beg = calc_trans_param_value_TIME;
                            card_nominal.date_end = null;
                            card_nominal.card_transaction = card_transaction;
                            card_nominal.unit_type = item.unit_type;
                            card_nominal.unit_value = item.unit_value;

                            // !!!
                            // card_nominal.date_expire = ...

                            card_nominal.curr_trans_count = card.curr_trans_count.GetValueOrDefault(0) + 1;
                            if (calc_card_param_value_SUMMA.GetValueOrDefault(0) != 0)
                            {
                                card_nominal.curr_trans_sum = calc_card_param_value_SUMMA;
                            }
                            if (calc_card_param_value_CARD_STATUS != card_status_id)
                            {
                                card_nominal.curr_card_status_id = (long?)calc_card_param_value_CARD_STATUS;
                            }                            
                            if ((curr_money_limit != null) && (calc_card_param_value_CARD_MONEY_LIMIT_USED != calc_card_param_value_CARD_MONEY_LIMIT_USED_init_value))
                            {
                                card_nominal.limit_id = curr_money_limit.limit_id;
                                card_nominal.money_used_limit_value = calc_card_param_value_CARD_MONEY_LIMIT_USED;
                            }
                            if ((curr_bonus_limit != null) && (calc_card_param_value_CARD_BONUS_LIMIT_USED != calc_card_param_value_CARD_BONUS_LIMIT_USED_init_value))
                            {
                                card_nominal.bonus_limit_id = curr_bonus_limit.limit_id;
                                card_nominal.bonus_used_limit_value = calc_card_param_value_CARD_BONUS_LIMIT_USED;
                            }
                            if (calc_card_param_value_BONUS_VALUE_INACTIVE.GetValueOrDefault(0) != 0)
                            {
                                card_nominal.bonus_value_inactive = calc_card_param_value_BONUS_VALUE_INACTIVE;
                            }

                            dbContext.card_nominal_uncommitted.Add(card_nominal);

                            if (nominal_list_for_return.CardNominal_list == null)
                                nominal_list_for_return.CardNominal_list = new List<CardNominal>();
                            nominal_list_for_return.CardNominal_list.Add(new CardNominal()
                            {
                                card_id = card.card_id,
                                date_beg = calc_trans_param_value_TIME,
                                date_end = null,
                                unit_type = item.unit_type,
                                unit_value = item.unit_value,
                            }
                                );

                        }

                        // добавляем к выходному списку номиналов неизмененные номиналы из card_nominal_unit_list
                        foreach (var item in card_nominal_unit_list)
                        {
                            if (nominal_list_for_return.CardNominal_list == null)
                                nominal_list_for_return.CardNominal_list = new List<CardNominal>();

                            if (nominal_list_for_return.CardNominal_list.Where(ss => ss.unit_type == item.unit_type).FirstOrDefault() != null)
                                continue;

                            nominal_list_for_return.CardNominal_list.Add(new CardNominal()
                            {
                                card_id = card.card_id,
                                date_beg = item.date_beg,
                                date_end = item.date_end,
                                unit_type = item.unit_type,
                                unit_value = item.unit_value,
                            }
                            );
                        }

                        // если нет ни одного card_transaction_unit, но надо увеличить curr_trans_count, curr_trans_sum
                        if (card_nominal == null)
                        {
                            card_nominal = new card_nominal_uncommitted();

                            card_nominal.card_id = card.card_id;
                            card_nominal.date_beg = calc_trans_param_value_TIME;
                            card_nominal.date_end = null;
                            card_nominal.card_transaction = card_transaction;

                            card_nominal.curr_trans_count = card.curr_trans_count.GetValueOrDefault(0) + 1;
                            if (calc_card_param_value_SUMMA.GetValueOrDefault(0) != 0)
                            {
                                card_nominal.curr_trans_sum = calc_card_param_value_SUMMA;
                            }
                            if (calc_card_param_value_CARD_STATUS != card_status_id)
                            {
                                card_nominal.curr_card_status_id = (long?)calc_card_param_value_CARD_STATUS;
                            }
                            if ((curr_money_limit != null) && (calc_card_param_value_CARD_MONEY_LIMIT_USED != calc_card_param_value_CARD_MONEY_LIMIT_USED_init_value))
                            {
                                card_nominal.limit_id = curr_money_limit.limit_id;
                                card_nominal.money_used_limit_value = calc_card_param_value_CARD_MONEY_LIMIT_USED;
                            }
                            if ((curr_bonus_limit != null) && (calc_card_param_value_CARD_BONUS_LIMIT_USED != calc_card_param_value_CARD_BONUS_LIMIT_USED_init_value))
                            {
                                card_nominal.bonus_limit_id = curr_bonus_limit.limit_id;
                                card_nominal.bonus_used_limit_value = calc_card_param_value_CARD_BONUS_LIMIT_USED;
                            }
                            if (calc_card_param_value_BONUS_VALUE_INACTIVE.GetValueOrDefault(0) != 0)
                            {
                                card_nominal.bonus_value_inactive = calc_card_param_value_BONUS_VALUE_INACTIVE;
                            }

                            dbContext.card_nominal_uncommitted.Add(card_nominal);
                        }

                    } // if (!(is_test == 1))

                    #endregion

                    ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc save");

                    block_num = 15;

                    #region Запись в БД и лог (в новом потоке)

                    dbContext.SaveChanges();
                    //xCard_CalcSaveContext(session_id, business_id, dbContext, card_transaction.card_trans_id, doCommit, true);

                    if (!isTestMode)
                        trans_id = card_transaction.card_trans_id;
                    else
                        trans_id = programm_result.result_id;

                    // это в xCard_CalcSaveContext      
                    if (doCommit)
                    {
                        xCard_CalcCommit(session_id, business_id, trans_id, null, 0);
                    }

                    #endregion

                    ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: end");
                    ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "---------");

                    return new CalcStartResult()
                    {
                        Id = 0,
                        error = null,
                        calc_unit_result = calc_unit_list_res,
                        trans_id = trans_id,
                        curr_trans_sum = calc_card_param_value_SUMMA.GetValueOrDefault(0),
                        curr_trans_count = calc_card_param_value_TRANS_COUNT.GetValueOrDefault(0),
                        card_nominal = nominal_list_for_return,
                        print_info = checkPrintInfo,
                        warn = warn,
                    };

                }
            }
            catch (Exception ex)
            {
                err_mess2 = "block=" + block_num.ToString()
                    + "_"
                    + "block_subnum=" + block_subnum.ToString()                    
                    + "_"
                    + "cardnum=" + card_num.ToString()
                    + "_"
                    + "unitcount=" + (calc_unit_list != null ? calc_unit_list.Count.ToString() : "0")
                    ;
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, ex, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, card_num, now, err_mess2, true);
                return CreateErrCalcStartResult(null, ex);
            }
        }

        #endregion

        #region Card_CalcCommit

        private CalcCommitResult xCard_CalcCommit(string session_id, long business_id, long trans_id, CalcRound round_info, short is_delayed)
        {

            #region Define
            DateTime now = DateTime.Now;
            bool logEnabled = false;
            DateTime logStartTime = DateTime.Now;
            card_transaction calc = null;
            #endregion

            #region Nlog
            try
            {
                logEnabled = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["NlogEnabled"]);
            }
            catch (Exception ex)
            {
                logEnabled = false;
            }
            #endregion

            ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc commit start");

            #region Сессия не найдена
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return CreateErrCalcCommitResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена"));
            #endregion            

            using (AuMainDb dbContext = new AuMainDb())
            {
                #region Считываем и подтверждаем транзакцию
                calc = dbContext.card_transaction.Where(ss => ss.card_trans_id == trans_id).FirstOrDefault();
                if (calc == null)
                    return CreateErrCalcCommitResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена транзакция с кодом " + trans_id.ToString()));
                if (calc.status != (int)Enums.CardTransactionStatus.ACTIVE)
                    return CreateErrCalcCommitResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Транзакция не в статусе Активна, код транзакции " + trans_id.ToString()));

                calc.trans_num = dbContext.card_transaction.Where(ss => ss.card_id == calc.card_id && ss.status == (int)Enums.CardTransactionStatus.DONE).OrderByDescending(ss => ss.trans_num).Select(ss => ss.trans_num).FirstOrDefault();
                calc.trans_num = calc.trans_num.HasValue ? calc.trans_num + 1 : 1;
                calc.status = (int)Enums.CardTransactionStatus.DONE;
                calc.status_date = DateTime.Now;
                calc.is_delayed = is_delayed;
                dbContext.SaveChanges();

                #endregion

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: calc commit end");
            }

            Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, trans_id.ToString(), session_id, business_id, "", ls, (long)Enums.LogEventType.SALE_COMMIT, trans_id, now, null, true);

            // доп. обработка после расчета
            xCard_CalcAfter(calc, round_info, true);

            return new CalcCommitResult() { Id = 0, error = null };
        }        

        #endregion

        #region Card_CalcCancel
        
        private CalcCancelResult xCard_CalcCancel(string session_id, long business_id, long trans_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return CreateErrCalcCancelResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена"));
            
            DateTime now = DateTime.Now;
            using (AuMainDb dbContext = new AuMainDb())
            {
                card_transaction calc = dbContext.card_transaction.Where(ss => ss.card_trans_id == trans_id).FirstOrDefault();
                if (calc == null)
                    return CreateErrCalcCancelResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена транзакция с кодом " + trans_id.ToString()));
                if (calc.status != (int)Enums.CardTransactionStatus.ACTIVE)
                    return CreateErrCalcCancelResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Транзакция не в статусе Активна, код транзакции " + trans_id.ToString()));

                List<card_nominal_uncommitted> card_nominal_uncommitted_list = dbContext.card_nominal_uncommitted.Where(ss => ss.card_trans_id == trans_id).ToList();
                if ((card_nominal_uncommitted_list != null) && (card_nominal_uncommitted_list.Count > 0))
                    dbContext.card_nominal_uncommitted.RemoveRange(card_nominal_uncommitted_list);

                //calc.status = (int)Enums.CardTransactionStatus.CANCEL;

                //начиная с версии 1.2.1.3 - отмененные транзакции удаляем
                programm_result programm_result = dbContext.programm_result.Where(ss => ss.result_id == calc.programm_result_id).FirstOrDefault();
                if (programm_result != null)
                {
                    IQueryable<programm_step_result> programm_step_result_list = dbContext.programm_step_result.Where(ss => ss.result_id == programm_result.result_id);
                    dbContext.programm_step_result.RemoveRange(programm_step_result_list);
                    IQueryable<programm_result_detail> programm_result_detail_list = dbContext.programm_result_detail.Where(ss => ss.result_id == programm_result.result_id);
                    dbContext.programm_result_detail.RemoveRange(programm_result_detail_list);
                    dbContext.programm_result.Remove(programm_result);
                }

                IQueryable<card_transaction_detail> card_transaction_detail_list = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == calc.card_trans_id);
                dbContext.card_transaction_detail.RemoveRange(card_transaction_detail_list);
                IQueryable<card_transaction_unit> card_transaction_unit_list = dbContext.card_transaction_unit.Where(ss => ss.card_trans_id == calc.card_trans_id);
                dbContext.card_transaction_unit.RemoveRange(card_transaction_unit_list);
                dbContext.card_transaction.Remove(calc);

                dbContext.SaveChanges();

                //Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, "trans_id: " + trans_id.ToString(), session_id, ls.user_name);
                Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, trans_id.ToString(), session_id, business_id, "", ls, (long)Enums.LogEventType.SALE_CANCEL, trans_id, now, null, true);
            }
            return new CalcCancelResult() { Id = 0, error = null };
        }

        #endregion

        #region Card_RefundStart

        private CalcStartResult xCard_RefundStart(string session_id, long business_id, long trans_id, List<CalcUnit> calc_unit_list)
        {
            #region Session

            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена"));

            #endregion

            #region calc_unit_list empty

            if ((calc_unit_list == null) || (calc_unit_list.Count <= 0))
            {
                return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Список товаров пуст"));
            }

            #endregion

            #region Define

            long new_trans_id = 0;
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            
            List<CalcUnit> calc_unit_list_res = new List<CalcUnit>();
            
            decimal unit_value_return_with_discount = 0;
            decimal unit_value_return = 0;
            decimal unit_value_return_discount = 0;

            decimal unit_value_return_for_one_count = 0;
            decimal unit_value_bonus_for_card_for_one_count = 0;
            decimal unit_value_bonus_for_pay_for_one_count = 0;
            
            decimal unit_value_return_sum = 0;
            decimal unit_value_return_with_discount_sum = 0;
            
            decimal unit_value_bonus_for_card_sum = 0;
            decimal unit_value_bonus_for_pay_sum = 0;
            decimal unit_count_already_returned = 0;
            decimal unit_value_already_returned = 0;
            decimal unit_value_with_discount_already_returned = 0;
            decimal unit_value_discount_already_returned = 0;
            decimal unit_value_bonus_for_card_already_returned = 0;
            decimal unit_value_bonus_for_pay_value_already_returned = 0;

            decimal unit_count_available_for_return = 0;
            decimal unit_value_available_for_return = 0;
            decimal unit_value_with_discount_available_for_return = 0;
            decimal unit_value_discount_available_for_return = 0;
            decimal unit_value_bonus_for_card_available_for_return = 0;
            decimal unit_value_bonus_for_pay_value_available_for_return = 0;

            DateTime date_check = now;

            card_transaction card_transaction = null;
            card_transaction card_transaction_new = null;
            card_transaction card_transaction_already_returned = null;
            card_transaction_detail card_transaction_detail = null;            
            card_transaction_detail card_transaction_detail_new = null;
            List<card_transaction_detail> card_transaction_detail_already_returned_list = null;
            //card_transaction_detail card_transaction_detail_already_returned = null;
            card card = null;
            card_nominal card_nominal = null;
            card_nominal card_nominal_new = null;

            #endregion

            using (AuMainDb dbContext = new AuMainDb())
            {
                #region Считываем card_transaction - транзакцию продажи, которую возвращаем

                card_transaction = dbContext.card_transaction.Where(ss => ss.card_trans_id == trans_id).FirstOrDefault();
                if (card_transaction == null)
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена запрашиваемая транзакция " + trans_id.ToString()));
                if (card_transaction.status != (int)Enums.CardTransactionStatus.DONE)
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Запрашиваемая транзакция не завершена, код транзакции " + trans_id.ToString()));
                if (card_transaction.trans_kind != (int)Enums.CardTransactionType.CALC)
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Запрашиваемая транзакция создана не из расчетов, код транзакции " + trans_id.ToString()));

                #endregion

                #region Считываем карту card

                card = (from c in dbContext.card                        
                        where c.business_id == business_id                        
                        && c.card_id == card_transaction.card_id
                        select c)
                        .FirstOrDefault();
                if (card == null)
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта, код " + card_transaction.card_id.ToString()));

                #endregion

                #region Считываем card_transaction_already_returned - подтвержденную транзакцию возврата по той же продаже, по которой делаем возврат
                
                card_transaction_already_returned = dbContext.card_transaction.Where(ss => ss.canceled_trans_id == trans_id && ss.status == (int)Enums.CardTransactionStatus.DONE).FirstOrDefault();

                #endregion

                #region Считываем business, business_org

                var business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));

                var business_org = (from bo in dbContext.business_org
                                from bou in dbContext.business_org_user
                                where bo.org_id == bou.org_id
                                && bo.business_id == business_id
                                && bou.user_name.Trim().Equals(ls.user_name.Trim())
                                select bo)
                                .FirstOrDefault();

                // заполняем начальные значения параметра "дата-время продажи" с учетом часового пояса
                if ((business_org != null) && (business_org.add_to_timezone.HasValue))
                {
                    date_check = now.AddHours((double)business_org.add_to_timezone);
                }
                else if (business.add_to_timezone.HasValue)
                {
                    date_check = now.AddHours((double)business.add_to_timezone);
                }

                #endregion

                #region Создаем транзакцию возврата card_transaction_new

                card_transaction_new = new card_transaction();
                // !!!
                // если это убрать - надо поставить Identity в модели для card_trans_id
                var card_trans_id = dbContext.Database.SqlQuery<long>("select discount.id_generator()").ToList().FirstOrDefault();
                card_transaction_new.card_trans_id = card_trans_id;
                card_transaction_new.card_id = card_transaction.card_id;
                card_transaction_new.date_beg = now;
                card_transaction_new.date_check = date_check;
                card_transaction_new.status = (int)Enums.CardTransactionStatus.DONE;
                card_transaction_new.trans_kind = (int)Enums.CardTransactionType.RETURN;
                card_transaction_new.trans_num = dbContext.card_transaction.Where(ss => ss.card_id == card_transaction.card_id && ss.status == (int)Enums.CardTransactionStatus.DONE).OrderByDescending(ss => ss.trans_num).Select(ss => ss.trans_num).FirstOrDefault();
                card_transaction_new.trans_num = card_transaction_new.trans_num.HasValue ? card_transaction_new.trans_num + 1 : 1;
                card_transaction_new.canceled_trans_id = trans_id;
                card_transaction_new.user_name = ls.user_name;

                dbContext.card_transaction.Add(card_transaction_new);

                #endregion

                foreach (CalcUnit unit in calc_unit_list)
                {
                    #region Подготовка к возврату позиции

                    unit_count_already_returned = 0;
                    unit_value_already_returned = 0;
                    unit_value_with_discount_already_returned = 0;
                    unit_value_discount_already_returned = 0;
                    unit_value_bonus_for_card_already_returned = 0;
                    unit_value_bonus_for_pay_value_already_returned = 0;

                    #endregion

                    #region Возвращаем card_transaction_detail - позицию в транзакции продажи

                    card_transaction_detail = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == trans_id && ss.unit_num == unit.unit_num).FirstOrDefault();                    

                    if (card_transaction_detail != null)
                    {
                        if (unit.unit_count <= 0)
                            return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нельзя вернуть ноль товаров, номер позиции " + unit.unit_num.ToString()));

                        #region old
                        //unit_value_return_with_discount = card_transaction_detail.unit_value_with_discount.HasValue ? (decimal)card_transaction_detail.unit_value_with_discount : card_transaction_detail.unit_value.GetValueOrDefault(unit.unit_value);                        
                        //unit_value_return_discount = card_transaction_detail.unit_value_discount.GetValueOrDefault(0);
                        #endregion

                        // !!!
                        // новое из AuDiscountApi
                        if (unit.unit_value_with_discount > 0)
                            unit_value_return_with_discount = unit.unit_value_with_discount;
                        else
                            unit_value_return_with_discount = card_transaction_detail.unit_value_with_discount.HasValue ? (decimal)card_transaction_detail.unit_value_with_discount : card_transaction_detail.unit_value.GetValueOrDefault(unit.unit_value);

                        // !!!
                        // новое из AuDiscountApi
                        if (unit.unit_value_discount > 0)
                            unit_value_return_discount = unit.unit_value_discount;
                        else
                            unit_value_return_discount = card_transaction_detail.unit_value_discount.GetValueOrDefault(0);

                        unit_value_return = card_transaction_detail.unit_value.GetValueOrDefault(unit.unit_value);

                        unit_count_available_for_return = card_transaction_detail.unit_count.GetValueOrDefault(0);
                        unit_value_available_for_return = card_transaction_detail.unit_value.GetValueOrDefault(0);

                        #region old
                        //unit_value_with_discount_available_for_return = card_transaction_detail.unit_value_with_discount.GetValueOrDefault(0);
                        //unit_value_discount_available_for_return = card_transaction_detail.unit_value_discount.GetValueOrDefault(0);
                        #endregion

                        // !!!
                        // новое из AuDiscountApi
                        unit_value_with_discount_available_for_return = unit_value_return_with_discount;
                        unit_value_discount_available_for_return = unit_value_return_discount;

                        unit_value_bonus_for_card_available_for_return = card_transaction_detail.unit_value_bonus_for_card.GetValueOrDefault(0);
                        unit_value_bonus_for_pay_value_available_for_return = card_transaction_detail.unit_value_bonus_for_pay.GetValueOrDefault(0);

                        if (unit.unit_count > unit_count_available_for_return)
                            return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Возвращаемых товаров больше чем приобретенных, номер позиции " + unit.unit_num.ToString()));
                        
                        if (card_transaction_already_returned != null)
                        {
                            card_transaction_detail_already_returned_list = (from ct in dbContext.card_transaction
                                                                       from ctd in dbContext.card_transaction_detail
                                                                       where ct.canceled_trans_id == trans_id
                                                                       && ctd.card_trans_id == ct.card_trans_id
                                                                       && ctd.unit_num == unit.unit_num
                                                                       select ctd
                                                                       )
                                                                       .Distinct()
                                                                       .ToList();
                            if ((card_transaction_detail_already_returned_list != null) && (card_transaction_detail_already_returned_list.Count > 0))
                            {                                
                                foreach (var returned_item in card_transaction_detail_already_returned_list)
                                {
                                    unit_count_already_returned += returned_item.unit_count.GetValueOrDefault(0);
                                    unit_value_already_returned -= returned_item.unit_value.GetValueOrDefault(0);
                                    unit_value_with_discount_already_returned -= returned_item.unit_value_with_discount.GetValueOrDefault(0);
                                    unit_value_discount_already_returned -= returned_item.unit_value_discount.GetValueOrDefault(0);
                                    unit_value_bonus_for_card_already_returned -= returned_item.unit_value_bonus_for_card.GetValueOrDefault(0);
                                    unit_value_bonus_for_pay_value_already_returned -= returned_item.unit_value_bonus_for_pay.GetValueOrDefault(0);
                                }
                                
                                unit_count_available_for_return = unit_count_available_for_return - unit_count_already_returned;
                                unit_value_available_for_return = unit_value_available_for_return - unit_value_already_returned;
                                unit_value_with_discount_available_for_return = unit_value_with_discount_available_for_return - unit_value_with_discount_already_returned;
                                unit_value_discount_available_for_return = unit_value_discount_available_for_return - unit_value_discount_already_returned;
                                unit_value_bonus_for_card_available_for_return = unit_value_bonus_for_card_available_for_return - unit_value_bonus_for_card_already_returned;
                                unit_value_bonus_for_pay_value_available_for_return = unit_value_bonus_for_pay_value_available_for_return - unit_value_bonus_for_pay_value_already_returned;

                                if (unit_count_available_for_return <= 0)
                                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Все товары по данной позиции уже были возвращены, номер позиции " + unit.unit_num.ToString()));
                                if (unit_count_available_for_return < unit.unit_count)
                                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "По данной позиции уже были возвращено " + unit_count_already_returned.ToString() + " единицы товара , номер позиции " + unit.unit_num.ToString()));
                            }
                        }
                        
                        if (unit.unit_count < card_transaction_detail.unit_count)
                        {
                            // возвращаем все оставшиеся товары в позиции
                            if (unit.unit_count == unit_count_available_for_return)
                            {
                                unit_value_return = unit_value_available_for_return;
                                unit_value_return_with_discount = unit_value_with_discount_available_for_return;
                                unit_value_return_discount = unit_value_discount_available_for_return;
                                unit.unit_value_bonus_for_card = unit_value_bonus_for_card_available_for_return;
                                unit.unit_value_bonus_for_pay = unit_value_bonus_for_pay_value_available_for_return;
                            }
                            else
                            {
                                unit_value_return_for_one_count = (decimal)(unit_value_return_with_discount / card_transaction_detail.unit_count);
                                unit_value_return_with_discount = GlobalUtil.ToTwoDigitsAfterComma(unit.unit_count * unit_value_return_for_one_count);
                                //
                                unit_value_return_for_one_count = (decimal)(unit_value_return / card_transaction_detail.unit_count);
                                unit_value_return = GlobalUtil.ToTwoDigitsAfterComma(unit.unit_count * unit_value_return_for_one_count);
                                //
                                unit_value_return_for_one_count = (decimal)(unit_value_return_discount / card_transaction_detail.unit_count);
                                unit_value_return_discount = GlobalUtil.ToTwoDigitsAfterComma(unit.unit_count * unit_value_return_for_one_count);
                                //
                                unit_value_bonus_for_card_for_one_count = (decimal)(card_transaction_detail.unit_value_bonus_for_card.GetValueOrDefault(0) / card_transaction_detail.unit_count);
                                unit.unit_value_bonus_for_card = GlobalUtil.ToTwoDigitsAfterComma(unit.unit_count * unit_value_bonus_for_card_for_one_count);
                                //
                                unit_value_bonus_for_pay_for_one_count = (decimal)(card_transaction_detail.unit_value_bonus_for_pay.GetValueOrDefault(0) / card_transaction_detail.unit_count);
                                unit.unit_value_bonus_for_pay = GlobalUtil.ToTwoDigitsAfterComma(unit.unit_count * unit_value_bonus_for_pay_for_one_count);
                            }
                        }
                        else
                        {
                            unit.unit_value_bonus_for_card = card_transaction_detail.unit_value_bonus_for_card.GetValueOrDefault(0);
                            unit.unit_value_bonus_for_pay = card_transaction_detail.unit_value_bonus_for_pay.GetValueOrDefault(0);
                        }

                        unit.unit_value_with_discount = unit_value_return_with_discount;
                        unit.unit_value = unit_value_return;
                        unit.unit_value_discount = unit_value_return_discount;
                                                
                        unit_value_return_sum += unit_value_return;
                        unit_value_return_with_discount_sum += unit_value_return_with_discount;
                        
                        unit_value_bonus_for_card_sum += unit.unit_value_bonus_for_card;
                        unit_value_bonus_for_pay_sum += unit.unit_value_bonus_for_pay;
                        unit.unit_percent_bonus_for_card = card_transaction_detail.unit_percent_bonus_for_card.GetValueOrDefault(0);                        
                        unit.unit_percent_bonus_for_pay = card_transaction_detail.unit_percent_bonus_for_pay.GetValueOrDefault(0);

                        // !!!
                        // unit_value_bonus_for_pay
                        unit.unit_name = card_transaction_detail.unit_name;

                        calc_unit_list_res.Add(unit);

                        card_transaction_detail_new = new card_transaction_detail();
                        card_transaction_detail_new.card_trans_id = card_transaction_new.card_trans_id;
                        card_transaction_detail_new.unit_count = unit.unit_count;
                        card_transaction_detail_new.unit_max_discount_percent = 100;
                        card_transaction_detail_new.unit_max_bonus_for_card_percent = 100;
                        card_transaction_detail_new.unit_max_bonus_for_pay_percent = 100;
                        card_transaction_detail_new.price_margin_percent = 0;
                        card_transaction_detail_new.artikul = unit.artikul;
                        card_transaction_detail_new.live_prep = unit.live_prep;
                        card_transaction_detail_new.promo = unit.promo;
                        card_transaction_detail_new.tax_percent = unit.tax_percent;
                        card_transaction_detail_new.tax_summa = unit.tax_summa;
                        card_transaction_detail_new.mess = unit.mess;

                        card_transaction_detail_new.unit_name = unit.unit_name;
                        card_transaction_detail_new.unit_num = unit.unit_num;
                        card_transaction_detail_new.unit_percent_bonus_for_card = unit.unit_percent_bonus_for_card;
                        card_transaction_detail_new.unit_percent_bonus_for_pay = unit.unit_percent_bonus_for_pay;
                        card_transaction_detail_new.unit_type = unit.unit_type;

                        card_transaction_detail_new.unit_value = -unit.unit_value;
                        card_transaction_detail_new.unit_value_with_discount = -unit.unit_value_with_discount;
                        card_transaction_detail_new.unit_value_discount = -unit.unit_value_discount;                        

                        card_transaction_detail_new.unit_value_bonus_for_card = -unit.unit_value_bonus_for_card;
                        card_transaction_detail_new.unit_value_bonus_for_pay = -unit.unit_value_bonus_for_pay;

                        dbContext.card_transaction_detail.Add(card_transaction_detail_new);
                    }

                    #endregion
                }

                #region Если ничего не вернули

                if (calc_unit_list_res.Count <= 0)
                    return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены позиции для возврата, код транзакции продажи " + trans_id.ToString()));

                #endregion

                #region Обновляем атрибуты транзакции, карты, лимиты, номиналы

                card_transaction_new.trans_sum = -unit_value_return_sum;

                if (unit_value_return_sum > 0)
                {
                    decimal card_curr_trans_sum = card.curr_trans_sum.GetValueOrDefault(0);

                    // decimal card_new_trans_sum = card_curr_trans_sum - unit_value_return_sum;
                    // или так:
                    decimal card_new_trans_sum = card_curr_trans_sum - unit_value_return_with_discount_sum;

                    if (card_new_trans_sum < 0)
                    {
                        card_new_trans_sum = 0;
                        // !!!
                        // надо что-то еще сделать
                    }
                    card.curr_trans_sum = card_new_trans_sum;

                    // уменьшаем использованный лимит на сумму возврата
                    // var curr_limit = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id).FirstOrDefault();
                    var curr_limit = (from ce in dbContext.card_ext1
                                     from l in dbContext.card_type_limit
                                     where ce.card_id == card.card_id
                                     && ce.curr_limit_id == l.limit_id
                                     && l.limit_date_beg <= card_transaction_new.date_beg
                                     && l.limit_date_end >= card_transaction_new.date_beg
                                     select ce)
                                     .FirstOrDefault();

                    if ((curr_limit != null) && (curr_limit.curr_sum.GetValueOrDefault(0) > 0))
                    {
                        decimal curr_limit_value = curr_limit.curr_sum.GetValueOrDefault(0);
                        decimal new_limit_value = curr_limit_value - unit_value_return_with_discount_sum;

                        if (new_limit_value < 0)
                        {
                            new_limit_value = 0;
                            // !!!
                            // надо что-то еще сделать
                        }
                        curr_limit.curr_sum = new_limit_value;
                    }
                }

                // сделать Enums.CardUnitTypeEnum.MONEY
                if ((unit_value_bonus_for_card_sum > 0) || (unit_value_bonus_for_pay_sum > 0))
                {                                     
                    // сначала пытаемся уменьшить неактивные бонусы
                    var bonus_for_card_left = unit_value_bonus_for_card_sum;
                    var card_nominal_inactive_existing = dbContext.card_nominal_inactive
                        .Where(ss => ss.card_id == card.card_id
                            && ss.card_trans_id == card_transaction.card_trans_id
                            && ss.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_VALUE)
                            .FirstOrDefault();
                    if (card_nominal_inactive_existing != null)
                    {
                        var curr_inactive_bonus_init = card_nominal_inactive_existing.unit_value.GetValueOrDefault(0);
                        var curr_inactive_bonus = curr_inactive_bonus_init;
                        if (curr_inactive_bonus > 0)
                        {
                            curr_inactive_bonus = curr_inactive_bonus - unit_value_bonus_for_card_sum;
                            if (curr_inactive_bonus <= 0)
                                bonus_for_card_left = unit_value_bonus_for_card_sum - curr_inactive_bonus_init;
                            else
                                bonus_for_card_left = 0;
                            if (curr_inactive_bonus <= 0)
                            {                                
                                dbContext.card_nominal_inactive.Remove(card_nominal_inactive_existing);
                            }
                            else
                            {
                                card_nominal_inactive_existing.unit_value = curr_inactive_bonus;
                            }
                        }
                        else
                        {
                            dbContext.card_nominal_inactive.Remove(card_nominal_inactive_existing);
                        }
                    }
                    if (bonus_for_card_left <= 0)
                        bonus_for_card_left = 0;
                    unit_value_bonus_for_card_sum = bonus_for_card_left;

                    decimal card_curr_bonus_value = 0;
                    decimal card_new_bonus_value = 0;
                    card_nominal = dbContext.card_nominal
                        .Where(ss => ss.card_id == card_transaction.card_id
                            && ss.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_VALUE
                            && !ss.date_end.HasValue)
                            .FirstOrDefault();
                    if (card_nominal != null)
                    {
                        card_nominal.date_end = now;
                        card_curr_bonus_value = card_nominal.unit_value.GetValueOrDefault(0);
                        //card_new_bonus_value = card_curr_bonus_value - unit_value_bonus_for_card_sum;
                        card_new_bonus_value = card_curr_bonus_value - unit_value_bonus_for_card_sum + unit_value_bonus_for_pay_sum;
                        if (card_new_bonus_value < 0)
                        {
                            card_new_bonus_value = 0;
                            // !!!
                            // ...
                        }
                    }

                    card_nominal_new = new card_nominal();
                    card_nominal_new.card_id = (long)card_transaction.card_id;
                    card_nominal_new.card_trans_id = card_transaction_new.card_trans_id;
                    card_nominal_new.date_beg = today;
                    card_nominal_new.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                    card_nominal_new.unit_value = card_new_bonus_value;
                    card_nominal_new.crt_date = now;
                    card_nominal_new.crt_user = ls.user_name;
                    dbContext.card_nominal.Add(card_nominal_new);
                }

                #endregion

                #region SaveChanges

                dbContext.SaveChanges();
                new_trans_id = card_transaction_new.card_trans_id;

                #endregion

                Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, new_trans_id.ToString(), session_id, business_id, "", ls, (long)Enums.LogEventType.SALE_REFUND, new_trans_id, now, null, true);
                return new CalcStartResult() { Id = 0, error = null, calc_unit_result = calc_unit_list_res, trans_id = new_trans_id };
            }
        }

        #endregion

        #endregion

        #region Card_CalcCommit_Emergency (old)
        /*
        private DeleteResult xCard_CalcCommit_Emergency(long min_uncommitted_id, long max_uncommited_id)
        {

            #region Define
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            bool logEnabled = false;
            DateTime logStartTime = DateTime.Now;

            card card = null;
            List<card_nominal> card_nominal_list = new List<card_nominal>();
            List<card_nominal> card_nominal_existing_list = null;
            card_nominal card_nominal_existing = null;
            List<card_nominal_uncommitted> card_nominal_uncommitted_list = null;
            card_nominal_uncommitted card_nominal_uncommitted_first = null;
            #endregion

            long cnt = 0;
            long trans_id = 0;
            using (var dbContext = new AuMainDb())
            {
                var trans = (from un in dbContext.card_nominal_uncommitted
                             from ct in dbContext.card_transaction
                             where un.card_trans_id == ct.card_trans_id
                             && ct.status == (int)Enums.CardTransactionStatus.DONE
                             && un.card_nominal_uncommitted_id >= min_uncommitted_id
                             && un.card_nominal_uncommitted_id <= max_uncommited_id
                             select ct.card_trans_id)
                               .Distinct()
                               .ToList();

                //return trans.Count;

                if ((trans != null) && (trans.Count > 0))
                {
                    foreach (var tran in trans)
                    {
                        trans_id = tran;

                        #region Считываем и подтверждаем транзакцию
                        var calc = dbContext.card_transaction.Where(ss => ss.card_trans_id == trans_id).FirstOrDefault();
                        if (calc == null)
                            return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена транзакция " + trans_id.ToString()), Id = 0, };
                        //if (calc.status != (int)Enums.CardTransactionStatus.ACTIVE)
                        //return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;

                        calc.trans_num = dbContext.card_transaction.Where(ss => ss.card_id == calc.card_id && ss.status == (int)Enums.CardTransactionStatus.DONE).OrderByDescending(ss => ss.trans_num).Select(ss => ss.trans_num).FirstOrDefault();
                        calc.trans_num = calc.trans_num.HasValue ? calc.trans_num + 1 : 1;
                        calc.status = (int)Enums.CardTransactionStatus.DONE;

                        #endregion

                        #region Считываем карту
                        card = dbContext.card.Where(ss => ss.card_id == calc.card_id).FirstOrDefault();
                        if (card == null)
                            return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта " + calc.card_id.ToString()), Id = 0, };
                        #endregion

                        card_nominal_uncommitted_list = dbContext.card_nominal_uncommitted.Where(ss => ss.card_trans_id == calc.card_trans_id).ToList();
                        if ((card_nominal_uncommitted_list != null) && (card_nominal_uncommitted_list.Count > 0))
                        {
                            #region Переносим значения из card_nominal_uncommitted в card_nominal
                            card_nominal_existing_list = dbContext.card_nominal.Where(ss => ss.card_id == calc.card_id && !ss.date_end.HasValue).ToList();
                            foreach (var item in card_nominal_uncommitted_list.Where(ss => ss.unit_type.HasValue))
                            {
                                card_nominal_existing = card_nominal_existing_list.Where(ss => ss.unit_type == item.unit_type).FirstOrDefault();
                                if (card_nominal_existing != null)
                                {
                                    if ((card_nominal_existing.unit_value.HasValue) && (card_nominal_existing.unit_value == item.unit_value))
                                    {
                                        continue;
                                    }
                                    card_nominal_existing.date_end = calc.date_beg;
                                }

                                card_nominal_list.Add(new card_nominal()
                                {
                                    card_id = item.card_id,
                                    card_trans_id = item.card_trans_id,
                                    date_beg = item.date_beg,
                                    date_end = item.date_end,
                                    date_expire = item.date_expire,
                                    unit_type = item.unit_type,
                                    unit_value = item.unit_value,
                                    crt_date = now,
                                    crt_user = calc.user_name,
                                }
                                );

                                //dbContext.SaveChanges();
                            }

                            #endregion

                            #region Обновляем card.curr_trans_count, card.curr_trans_sum

                            card_nominal_uncommitted_first = card_nominal_uncommitted_list.FirstOrDefault();

                            card.curr_trans_count = card_nominal_uncommitted_first.curr_trans_count;
                            card.curr_trans_sum = card_nominal_uncommitted_first.curr_trans_sum;

                            #endregion

                            #region Переносим неактивные бонусы из card_nominal_uncommitted в card_nominal_inactive

                            if (card_nominal_uncommitted_first.bonus_value_inactive.GetValueOrDefault(0) != 0)
                            {
                                card_nominal_inactive inactive_bonus = new card_nominal_inactive();
                                inactive_bonus.card_id = card.card_id;
                                inactive_bonus.crt_date = now;
                                inactive_bonus.crt_user = calc.user_name;
                                inactive_bonus.unit_type = (int)Enums.CardUnitTypeEnum.BONUS_VALUE;
                                inactive_bonus.unit_value = card_nominal_uncommitted_first.bonus_value_inactive;
                                inactive_bonus.active_date = now.AddHours(24);
                                inactive_bonus.card_trans_id = calc.card_trans_id;
                                dbContext.card_nominal_inactive.Add(inactive_bonus);
                                //dbContext.SaveChanges();
                            }

                            #endregion

                            #region Меняем статус карты

                            long new_card_status_id = (long)card_nominal_uncommitted_first.curr_card_status_id.GetValueOrDefault(0);
                            if (new_card_status_id > 0)
                            {
                                card_card_status_rel card_card_status_rel_new = null;
                                card_card_status_rel card_card_status_rel_existing = dbContext.card_card_status_rel.Where(ss => ss.card_id == calc.card_id
                                    && !ss.date_end.HasValue)
                                    .FirstOrDefault();
                                if ((card_card_status_rel_existing != null) && (card_card_status_rel_existing.card_status_id != new_card_status_id))
                                {
                                    card_card_status_rel_existing.date_end = calc.date_beg;

                                    card_card_status_rel_new = new card_card_status_rel();
                                    card_card_status_rel_new.card_id = card.card_id;
                                    card_card_status_rel_new.card_status_id = new_card_status_id;
                                    card_card_status_rel_new.date_beg = calc.date_beg;

                                    dbContext.card_card_status_rel.Add(card_card_status_rel_new);

                                    card.curr_card_status_id = new_card_status_id;

                                    // если карта стала неактивной - обновляем сообщение card.warn
                                    var is_card_inactive = from cs in dbContext.card_status
                                                           from csg in dbContext.card_status_group
                                                           where cs.card_status_group_id == csg.card_status_group_id
                                                           && cs.card_status_id == new_card_status_id
                                                           && csg.is_active != 1
                                                           select cs;
                                    if (is_card_inactive != null)
                                    {
                                        // пока возможен только вариант, что карта стала неактивной после исчерпывания всего лимита продаж
                                        card.warn = "Исчерпан лимит карты за текущий период";
                                    }

                                    //dbContext.SaveChanges();
                                }
                            }

                            #endregion

                            #region Увеличиваем использованные лимиты

                            if (card_nominal_uncommitted_first.limit_id.HasValue)
                            {
                                var trans_sum_with_discount = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == calc.card_trans_id && ss.unit_type == 0).Sum(ss => ss.unit_value_with_discount);
                                card_ext1 curr_limit_used = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id && ss.curr_limit_id == card_nominal_uncommitted_first.limit_id).FirstOrDefault();
                                if ((curr_limit_used == null) && (trans_sum_with_discount.GetValueOrDefault(0) != 0))
                                {
                                    curr_limit_used = new card_ext1();
                                    curr_limit_used.card_id = card.card_id;
                                    curr_limit_used.curr_limit_id = card_nominal_uncommitted_first.limit_id;

                                    // вот так - значит использованный лимит увеличится на сумму без учета скидки
                                    // curr_limit_used.curr_sum = calc.trans_sum;
                                    // вот так - значит использованный лимит увеличится на сумму со скидкой
                                    curr_limit_used.curr_sum = trans_sum_with_discount;

                                    dbContext.card_ext1.Add(curr_limit_used);
                                }
                                else
                                {
                                    //curr_limit_used.curr_sum = curr_limit_used.curr_sum.HasValue ? (curr_limit_used.curr_sum + calc.trans_sum) : calc.trans_sum;
                                    curr_limit_used.curr_sum = curr_limit_used.curr_sum.HasValue ? (curr_limit_used.curr_sum + trans_sum_with_discount) : trans_sum_with_discount;
                                }
                            }
                            if (card_nominal_uncommitted_first.bonus_limit_id.HasValue)
                            {
                                var bonus_for_pay_sum = dbContext.card_transaction_detail.Where(ss => ss.card_trans_id == calc.card_trans_id && ss.unit_type == 0).Sum(ss => ss.unit_value_bonus_for_pay);
                                card_ext1 curr_limit_used = dbContext.card_ext1.Where(ss => ss.card_id == card.card_id && ss.curr_limit_id == card_nominal_uncommitted_first.bonus_limit_id).FirstOrDefault();
                                if ((curr_limit_used == null) && (bonus_for_pay_sum.GetValueOrDefault(0) != 0))
                                {
                                    curr_limit_used = new card_ext1();
                                    curr_limit_used.card_id = card.card_id;
                                    curr_limit_used.curr_limit_id = card_nominal_uncommitted_first.bonus_limit_id;
                                    curr_limit_used.curr_sum = bonus_for_pay_sum;

                                    dbContext.card_ext1.Add(curr_limit_used);
                                }
                                else
                                {
                                    curr_limit_used.curr_sum = curr_limit_used.curr_sum.HasValue ? (curr_limit_used.curr_sum + bonus_for_pay_sum) : bonus_for_pay_sum;
                                }
                            }

                            #endregion

                            #region Обновляем card_nominal
                            if (card_nominal_list.Count > 0)
                                dbContext.card_nominal.AddRange(card_nominal_list);
                            #endregion

                            #region Чистим card_nominal_uncommitted
                            dbContext.card_nominal_uncommitted.RemoveRange(card_nominal_uncommitted_list);
                            #endregion
                        }

                        #region Подтверждаем ограничения

                        var restr_list = dbContext.card_restr_value.Where(ss => ss.card_id == calc.card_id && ss.card_trans_id == calc.card_trans_id && !ss.is_committed).ToList();
                        if ((restr_list != null) && (restr_list.Count > 0))
                        {
                            foreach (var restr in restr_list)
                            {
                                restr.is_committed = true;
                                restr.commit_date = now;
                            }
                        }

                        #endregion

                        #region Удаляем неподтвержденные ограничения

                        var exists_uncommitted = dbContext.card_restr_value.Where(ss => ss.card_id == calc.card_id && !ss.is_committed).Count() > 0;
                        if (exists_uncommitted)
                        {
                            dbContext.card_restr_value.RemoveRange(dbContext.card_restr_value.Where(ss => ss.card_id == calc.card_id && ss.card_trans_id != calc.card_trans_id && !ss.is_committed));
                        }

                        #endregion

                        dbContext.SaveChanges();
                    }
                }
                return new DeleteResult() { error = null, Id = 0, };
            }
        }
        */

        #endregion

    }    
}
