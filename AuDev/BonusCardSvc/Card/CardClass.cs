﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Linq.Expressions;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region Card
    
    [DataContract]
    public class Card : BonusCardBase
    {
        public Card()
            : base()
        {
            //
        }

        public Card(card card)
            : base()
        {
            db_card = card;
            card_id = card.card_id;
            date_beg = card.date_beg;
            date_end = card.date_end;
            curr_card_status_id = card.curr_card_status_id;
            curr_card_type_id = card.curr_card_type_id;
            card_num = card.card_num;
            card_num2 = card.card_num2;            
            business_id = card.business_id;
            curr_trans_count = card.curr_trans_count;
            curr_trans_sum = card.curr_trans_sum;
            curr_client_id = card.curr_holder_id;
            mess = card.mess;
            batch_id = card.batch_id;
            warn = card.warn;
            source_card_id = card.source_card_id;
            crt_date = card.crt_date;
            crt_user = card.crt_user;
            from_au = card.from_au;
            business = new Business(card.business);
            if (card.card_status != null)
                curr_card_status = new CardStatus(card.card_status);
            if (card.card_type != null)
                curr_card_type = new CardType(card.card_type);
            if (card.card_holder != null)
                client = new Client(card.card_holder);
            if (card.card_nominal != null)
                //card_nominal = new CardNominalList(card.card_nominal.ToList());
                card_nominal = new CardNominalList(card.card_nominal.Where(ss => !ss.date_end.HasValue).ToList());
        }

        public Card(vw_card card)
            : base()
        {
            db_vw_card = card;
            card_id = card.card_id;
            date_beg = card.date_beg;
            date_end = card.date_end;
            curr_card_status_id = card.curr_card_status_id;
            curr_card_type_id = card.curr_card_type_id;
            card_num = card.card_num;
            card_num2 = card.card_num2;            
            business_id = card.business_id;
            curr_trans_count = card.curr_trans_count;
            curr_trans_sum = card.curr_trans_sum;
            curr_client_id = card.curr_holder_id;
            //
            curr_bonus_value = card.curr_bonus_value;
            curr_bonus_percent = card.curr_bonus_percent;
            curr_discount_value = card.curr_discount_value;
            curr_discount_percent = card.curr_discount_percent;
            curr_money_value = card.curr_money_value;
            curr_money_percent = card.curr_money_percent;
            mess = card.mess;
            source_card_id = card.source_card_id;
            curr_client_first_name = card.curr_holder_first_name;
            curr_client_second_name = card.curr_holder_second_name;
            curr_client_date_birth = card.curr_holder_date_birth;
            crt_date = card.crt_date;
            crt_user = card.crt_user;
            from_au = card.from_au;
            curr_inactive_bonus_value = card.curr_inactive_bonus_value;
            curr_all_bonus_value = card.curr_all_bonus_value;
        }

        [DataMember]
        public long card_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public Nullable<long> curr_card_status_id { get; set; }
        [DataMember]
        public Nullable<long> curr_card_type_id { get; set; }
        [DataMember]
        public Nullable<long> card_num { get; set; }
        [DataMember]
        public string card_num2 { get; set; }
        [DataMember]
        public Nullable<long> business_id { get; set; }
        [DataMember]
        public Nullable<long> curr_trans_count { get; set; }
        [DataMember]
        public Nullable<decimal> curr_trans_sum { get; set; }
        [DataMember]
        public Nullable<long> curr_client_id { get; set; }
        [DataMember]
        public CardStatus curr_card_status { get; set; }
        [DataMember]
        public CardType curr_card_type { get; set; }
        [DataMember]
        public Nullable<decimal> curr_bonus_value { get; set; }
        [DataMember]
        public Nullable<decimal> curr_bonus_percent { get; set; }
        [DataMember]
        public Nullable<decimal> curr_discount_value { get; set; }
        [DataMember]
        public Nullable<decimal> curr_discount_percent { get; set; }
        [DataMember]
        public Nullable<decimal> curr_money_value { get; set; }
        [DataMember]
        public Nullable<decimal> curr_money_percent { get; set; }
        [DataMember]
        public string mess { get; set; }
        [DataMember]
        public Nullable<long> batch_id { get; set; }
        [DataMember]
        public string warn { get; set; }
        [DataMember]
        public string curr_client_first_name { get; set; }
        [DataMember]
        public string curr_client_second_name { get; set; }
        [DataMember]
        public Nullable<System.DateTime> curr_client_date_birth { get; set; }
        [DataMember]
        public Nullable<System.DateTime> crt_date { get; set; }
        [DataMember]
        public string crt_user { get; set; }
        [DataMember]
        public bool from_au { get; set; }
        [DataMember]
        public Nullable<decimal> curr_inactive_bonus_value { get; set; }
        [DataMember]
        public Nullable<decimal> curr_all_bonus_value { get; set; }
        [DataMember]
        public Nullable<long> source_card_id { get; set; }
        [DataMember]
        public Business business { get; set; }
        [DataMember]
        public Client client { get; set; }
        [DataMember]
        public CardNominalList card_nominal { get; set; }

        public card Db_card { get { return db_card; } }
        private card db_card;

        public vw_card Db_vw_card { get { return db_vw_card; } }
        private vw_card db_vw_card;
    }    

    [DataContract]
    public class CardList : BonusCardBase
    {

        public CardList()
            : base()
        {
            //
        }

        public CardList(List<card> cardList)
        {
            if (cardList != null)
            {
                Card_list = new List<Card>();
                foreach (var item in cardList)
                {
                    Card_list.Add(new Card(item));
                }
            }
        }

        public CardList(List<vw_card> cardList)
        {
            if (cardList != null)
            {
                Card_list = new List<Card>();
                foreach (var item in cardList)
                {
                    Card_list.Add(new Card(item));
                }
            }
        }

        [DataMember]
        public List<Card> Card_list;
    }

    #endregion

    #region CardCardStatusRel

    [DataContract]
    public class CardCardStatusRel : BonusCardBase
    {
        public CardCardStatusRel()
            : base()
        {
            //
        }

        public CardCardStatusRel(card_card_status_rel card_card_status_rel)
        {
            db_card_card_status_rel = card_card_status_rel;
            rel_id = db_card_card_status_rel.rel_id;
            card_id = db_card_card_status_rel.card_id;
            card_status_id = db_card_card_status_rel.card_status_id;
            date_beg = db_card_card_status_rel.date_beg;
            date_end = db_card_card_status_rel.date_end;
            card = new Card(card_card_status_rel.card);
            card_status = new CardStatus(card_card_status_rel.card_status);
        }

        [DataMember]
        public long rel_id { get; set; }
        [DataMember]
        public long card_id { get; set; }
        [DataMember]
        public long card_status_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        //[DataMember]
        public Nullable<long> next_status_id { get; set; }
        //[DataMember]
        public Nullable<System.DateTime> next_status_date_beg { get; set; }
        [DataMember]
        public Card card { get; set; }
        [DataMember]
        public CardStatus card_status { get; set; }

        public card_card_status_rel Db_card_card_status_rel { get { return db_card_card_status_rel; } }
        private card_card_status_rel db_card_card_status_rel;
    }

    [DataContract]
    public class CardCardStatusRelList : BonusCardBase
    {

        public CardCardStatusRelList()
            : base()
        {
            //
        }

        public CardCardStatusRelList(List<card_card_status_rel> cardCardStatusRelList)
        {
            if (cardCardStatusRelList != null)
            {
                CardCardStatusRel_list = new List<CardCardStatusRel>();
                foreach (var item in cardCardStatusRelList)
                {
                    CardCardStatusRel_list.Add(new CardCardStatusRel(item));
                }
            }
        }

        [DataMember]
        public List<CardCardStatusRel> CardCardStatusRel_list;
    }

    #endregion

    #region CardCardTypeRel

    [DataContract]
    public class CardCardTypeRel : BonusCardBase
    {
        public CardCardTypeRel()
            : base()
        {
            //
        }

        public CardCardTypeRel(card_card_type_rel card_card_type_rel)
        {
            db_card_card_type_rel = card_card_type_rel;

            rel_id = card_card_type_rel.rel_id;
            card_id = card_card_type_rel.card_id;
            card_type_id = card_card_type_rel.card_type_id;
            date_beg = card_card_type_rel.date_beg;
            date_end = card_card_type_rel.date_end;
            ord = card_card_type_rel.ord;
            card = new Card(card_card_type_rel.card);
            card_type = new CardType(card_card_type_rel.card_type);

        }
        [DataMember]
        public long rel_id { get; set; }
        [DataMember]
        public long card_id { get; set; }
        [DataMember]
        public long card_type_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public Nullable<int> ord { get; set; }
        [DataMember]
        public Card card { get; set; }
        [DataMember]
        public CardType card_type { get; set; }

        public card_card_type_rel Db_card_card_type_rel { get { return Db_card_card_type_rel; } }
        private card_card_type_rel db_card_card_type_rel;
    }

    [DataContract]
    public class CardCardTypeRelList : BonusCardBase
    {

        public CardCardTypeRelList()
            : base()
        {
            //
        }

        public CardCardTypeRelList(List<card_card_type_rel> cardCardTypeRelList)
        {
            if (cardCardTypeRelList != null)
            {
                CardCardTypeRel_list = new List<CardCardTypeRel>();
                foreach (var item in cardCardTypeRelList)
                {
                    CardCardTypeRel_list.Add(new CardCardTypeRel(item));
                }
            }
        }

        [DataMember]
        public List<CardCardTypeRel> CardCardTypeRel_list;
    }

    #endregion

    #region CardNominal

    [DataContract]
    public class CardNominal : BonusCardBase
    {

        public CardNominal()
            : base()
        {
            //
        }

        public CardNominal(card_nominal card_nominal)
        {
            db_card_nominal = card_nominal;
            card_nominal_id = card_nominal.card_nominal_id;
            card_id = card_nominal.card_id;
            date_beg = card_nominal.date_beg;
            date_end = card_nominal.date_end;
            card_trans_id = card_nominal.card_trans_id;
            unit_type = card_nominal.unit_type;
            unit_value = card_nominal.unit_value;
            date_expire = card_nominal.date_expire;
            crt_date = card_nominal.crt_date;
            crt_user = card_nominal.crt_user;
            batch_id = card_nominal.batch_id;
            //card = new Card(card_nominal.card);
        }

        [DataMember]
        public long card_nominal_id { get; set; }
        [DataMember]
        public long card_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public Nullable<long> card_trans_id { get; set; }
        [DataMember]
        public Nullable<int> unit_type { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_expire { get; set; }
        [DataMember]
        public Nullable<System.DateTime> crt_date { get; set; }
        [DataMember]
        public string crt_user { get; set; }
        [DataMember]
        public Nullable<long> batch_id { get; set; }
        //[DataMember]
        //public Card card { get; set; }

        public card_nominal Db_card_nominal { get { return db_card_nominal; } }
        private card_nominal db_card_nominal;
    }

    [DataContract]
    public class CardNominalList : BonusCardBase
    {
        public CardNominalList()
            : base()
        {
            //
        }

        public CardNominalList(List<card_nominal> cardNominalList)
        {
            if (cardNominalList != null)
            {
                CardNominal_list = new List<CardNominal>();
                foreach (var item in cardNominalList)
                {
                    CardNominal_list.Add(new CardNominal(item));
                }
            }
        }

        [DataMember]
        public List<CardNominal> CardNominal_list;

    }

    #endregion
}
