﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region Card

        private Card xGetCard(string session_id, long business_id, long card_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card card = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                card = (from c in dbContext.card                      
                        from ct in dbContext.card_type_business_org
                           where c.business_id == business_id                           
                           && c.card_id == card_id
                           && c.curr_card_type_id == ct.card_type_id
                           && ct.business_org_id == ls.org_id
                           select c)
                           .FirstOrDefault();
                if (card == null)
                {
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new Card(card);
            }
        }

        private CardList xGetCardList(string session_id, long business_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card> card_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                card_list = (from c in dbContext.card
                             from ct in dbContext.card_type_business_org
                             where c.business_id == business_id
                             && c.curr_card_type_id == ct.card_type_id
                             && ct.business_org_id == ls.org_id
                             select c)
                             .OrderBy(ss => ss.business_id)
                             .ThenBy(ss => ss.card_id)
                             .ToList()
                             ;

                if (card_list == null)
                {
                    return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardList(card_list);
            }
        }
        
        private CardList xGetCardList_byCardNum(string session_id, long business_id, long? card_num, string card_num2, short? search_mode, string mask, bool forCalc, long? org_id)
        {
            bool logEnabled = false;
            DateTime logStartTime = DateTime.Now;

            #region Nlog
            try
            {
                logEnabled = ((bool.Parse(System.Configuration.ConfigurationManager.AppSettings["NlogEnabled"])) && (!forCalc));
            }
            catch (Exception ex)
            {
                logEnabled = false;
            }
            #endregion

            ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: get card start");

            long? ls_org_id = org_id;
            LogSession ls = null;
            if (!forCalc)
            {
                ls = xGetActiveSession(session_id);
                if (!IsValidSession(ls))
                    return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
                ls_org_id = ls.org_id;
            }
            
            switch (search_mode.GetValueOrDefault(0))
            {
                case (int)Enums.CardSearchMode.BY_CARD_NUM_ONLY:
                case (int)Enums.CardSearchMode.BY_CARD_NUM2_ONLY:
                case (int)Enums.CardSearchMode.BY_CARD_NUM_THEN_CARD_NUM2:
                case (int)Enums.CardSearchMode.BY_CARD_NUM2_THEN_CARD_NUM:
                case (int)Enums.CardSearchMode.BY_ALL_NUMS:
                    break;
                default:
                    return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден режим поиска с кодом " + search_mode.ToString()) };
            }
            List<card> card_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                //speed up
                //if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    //return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                switch (search_mode.GetValueOrDefault(0))
                {
                    case (int)Enums.CardSearchMode.BY_CARD_NUM_ONLY:
                        if (card_num.HasValue)
                        {
                            card_list = (from c in dbContext.card
                                         from ct in dbContext.card_type_business_org
                                         where c.business_id == business_id
                                         && (c.card_num.HasValue && c.card_num == card_num)
                                         && c.curr_card_type_id == ct.card_type_id
                                         && ct.business_org_id == ls_org_id
                                         select c)
                               .OrderBy(ss => ss.business_id)
                               .ThenBy(ss => ss.card_num)
                               .ToList()
                               ;
                        }
                        break;
                    case (int)Enums.CardSearchMode.BY_CARD_NUM2_ONLY:
                        if (!String.IsNullOrEmpty(card_num2))
                        {
                            card_list = (from c in dbContext.card
                                         from ct in dbContext.card_type_business_org
                                         where c.business_id == business_id
                                         && (!String.IsNullOrEmpty(c.card_num2) && c.card_num2.Equals(card_num2))
                                         && c.curr_card_type_id == ct.card_type_id
                                         && ct.business_org_id == ls_org_id
                                         select c)
                               .OrderBy(ss => ss.business_id)
                               .ThenBy(ss => ss.card_num2)
                               .ToList()
                               ;
                        }
                        break;
                    case (int)Enums.CardSearchMode.BY_CARD_NUM2_THEN_CARD_NUM:
                        if (!String.IsNullOrEmpty(card_num2))
                        {
                            card_list = (from c in dbContext.card
                                         from ct in dbContext.card_type_business_org
                                         where c.business_id == business_id
                                         && (!String.IsNullOrEmpty(c.card_num2) && c.card_num2.Equals(card_num2)) 
                                         && c.curr_card_type_id == ct.card_type_id
                                         && ct.business_org_id == ls_org_id
                                         select c)
                               .OrderBy(ss => ss.business_id)
                               .ThenBy(ss => ss.card_num2)
                               .ToList()
                               ;
                        }
                        if ((card_list == null) || (card_list.Count <= 0))
                        {
                            if (card_num.HasValue)
                            {
                                card_list = (from c in dbContext.card
                                             from ct in dbContext.card_type_business_org
                                             where c.business_id == business_id
                                             && (c.card_num.HasValue && c.card_num == card_num)
                                             && c.curr_card_type_id == ct.card_type_id
                                             && ct.business_org_id == ls_org_id
                                             select c)
                                   .OrderBy(ss => ss.business_id)
                                   .ThenBy(ss => ss.card_num)
                                   .ToList()
                                   ;
                            }
                        }
                        break;
                    // чтобы в старых версиях Кассы не менять
                    case (int)Enums.CardSearchMode.BY_CARD_NUM_THEN_CARD_NUM2:
                    case (int)Enums.CardSearchMode.BY_ALL_NUMS:
                        if (card_num.HasValue)
                        {
                            card_list = (from c in dbContext.card
                                         from ct in dbContext.card_type_business_org
                                         where c.business_id == business_id
                                         && (c.card_num.HasValue && c.card_num == card_num)
                                         && c.curr_card_type_id == ct.card_type_id
                                         && ct.business_org_id == ls_org_id
                                         select c)
                               .OrderBy(ss => ss.business_id)
                               .ThenBy(ss => ss.card_num)
                               .ToList()
                               ;
                        }
                        if ((card_list == null) || (card_list.Count <= 0))
                        {
                            if (!String.IsNullOrEmpty(card_num2))
                            {
                                card_list = (from c in dbContext.card
                                             from ct in dbContext.card_type_business_org
                                             where c.business_id == business_id
                                             && (!String.IsNullOrEmpty(c.card_num2) && c.card_num2.Equals(card_num2))
                                             && c.curr_card_type_id == ct.card_type_id
                                             && ct.business_org_id == ls_org_id
                                             select c)
                                   .OrderBy(ss => ss.business_id)
                                   .ThenBy(ss => ss.card_num2)
                                   .ToList()
                                   ;
                            }
                        }
                        if ((card_list == null) || (card_list.Count <= 0))
                        {
                            if (!String.IsNullOrEmpty(card_num2))
                            {
                                card_list = (from c in dbContext.card
                                             from can in dbContext.card_additional_num
                                             from ct in dbContext.card_type_business_org
                                             where c.business_id == business_id
                                             && c.card_id == can.card_id
                                             && (!String.IsNullOrEmpty(can.card_num) && can.card_num.Equals(card_num2))
                                             && c.curr_card_type_id == ct.card_type_id
                                             && ct.business_org_id == ls_org_id
                                             select c)
                                   .OrderBy(ss => ss.business_id)
                                   .ThenBy(ss => ss.card_num)
                                   .Distinct()
                                   .ToList()
                                   ;
                            }
                        }
                        break;
                    default:
                        break;
                }

                ToNlog_Info(logEnabled, DateTime.Now - logStartTime, "nlog: get card end");

                if (card_list == null)
                {
                    return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                if ((card_list.Count == 1) && (!forCalc))
                {
                    // обновляем номиналы, если на типе карт установлен такой признак
                    var single_card = card_list.FirstOrDefault();
                    var update_nominal = dbContext.card_type.Where(ss => ss.card_type_id == single_card.curr_card_type_id && ss.update_nominal == 1).FirstOrDefault();
                    if (update_nominal != null)
                    {
                        // !!!
                        xRecalcCardNominal(single_card.card_id, ls);
                    }

                    // !!!
                    // место для настройки - можно менять последний параметр true/false
                    // подготовка к расчету
                    xCard_CalcBefore(single_card, ls.user_name, true);
                        
                }

                return new CardList(card_list);
            }

        }

        private CardList xGetCardList_byClient(string session_id, long business_id, string client_surname, string client_name, string client_fname, int? client_age)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            if ((String.IsNullOrEmpty(client_surname)) && (String.IsNullOrEmpty(client_name)) && (String.IsNullOrEmpty(client_fname)) && (client_age.GetValueOrDefault(0) <= 0))
            {
                return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы параметры поиска") };
            }

            DateTime today = DateTime.Today;
            IQueryable<card> card_query = null;
            List<card> card_list = null;
            IQueryable<card_holder> client_query = null;
            DateTime date_birth = DateTime.Today;
            using (AuMainDb dbContext = new AuMainDb())
            {
                //card_query = dbContext.card.Where(ss => ss.business_id == business_id && ((!ss.date_end.HasValue) || (ss.date_end > today)));
                client_query = from c in dbContext.card
                               from crel in dbContext.card_holder_card_rel
                               from cl in dbContext.card_holder
                               from ct in dbContext.card_type_business_org
                               where c.business_id == business_id
                               && ((!c.date_end.HasValue) || (c.date_end > today))
                               && c.card_id == crel.card_id
                               && crel.card_holder_id == cl.card_holder_id
                               && ((!crel.date_end.HasValue) || (crel.date_end > today))                             
                               && cl.business_id == business_id
                               && c.curr_card_type_id == ct.card_type_id
                               && ct.business_org_id == ls.org_id
                               select cl;
                if (!(String.IsNullOrEmpty(client_surname)))
                {
                    client_query = client_query.Where(cl => cl.card_holder_surname != null
                        //&& cl.client_surname.Trim().ToLower().Equals(client_surname.Trim().ToLower()));
                        && cl.card_holder_surname.Trim().ToLower().StartsWith(client_surname.Trim().ToLower()));
                }
                if (!(String.IsNullOrEmpty(client_name)))
                {
                    client_query = client_query.Where(cl => cl.card_holder_name != null
                        && cl.card_holder_name.Trim().ToLower().Equals(client_name.Trim().ToLower()));
                }
                if (!(String.IsNullOrEmpty(client_fname)))
                {
                    client_query = client_query.Where(cl => cl.card_holder_fname != null
                        && cl.card_holder_fname.Trim().ToLower().Equals(client_fname.Trim().ToLower()));
                }
                if (client_age.GetValueOrDefault(0) > 0)
                {
                    date_birth = today.AddYears(-client_age.GetValueOrDefault(0));
                    client_query = client_query.Where(cl => cl.date_birth.HasValue 
                        && cl.date_birth == date_birth);                        
                }

                card_query = from c in dbContext.card
                             from cl in client_query
                             where c.curr_holder_id == cl.card_holder_id
                             select c;

                card_list = card_query.ToList();
                if ((card_list == null) || (card_list.Count <= 0))
                {
                    return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдено ни одной карты") };
                }

                if (card_list.Count == 1)
                {
                    // обновляем номиналы, если на типе карт установлен такой признак
                    var single_card = card_list.FirstOrDefault();
                    var update_nominal = dbContext.card_type.Where(ss => ss.card_type_id == single_card.curr_card_type_id && ss.update_nominal == 1).FirstOrDefault();
                    if (update_nominal != null)
                        xRecalcCardNominal(single_card.card_id, ls);

                    // !!!
                    // место для настройки - можно менять последний параметр true/false
                    // подготовка к расчету
                    xCard_CalcBefore(single_card, ls.user_name, true);
                }

                return new CardList(card_list);
            }
        }

        private CardList xGetCardList_byAttrs(string session_id, long business_id
            , long? card_type_id, long? card_status_id, long? card_id, long? card_num, string card_num2, string client_name
            , DateTime? date_beg, DateTime? date_end, long? reestr_id
            , int? first_num, int? last_num, int? sort_field)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            //IQueryable<card> card_query = null;
            IQueryable<vw_card> card_query = null;
            //List<card> card_list = null;
            List<vw_card> card_list = null;
            DateTime today = DateTime.Today;
            Enums.CardSortField card_sort_field = Enums.CardSortField.DEFAULT;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (card_type_id.HasValue)
                {
                    if (dbContext.card_type.Where(ss => ss.card_type_id == card_type_id).Count() <= 0)
                        return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден тип карт с кодом " + card_type_id.ToString()) };
                }
                if (card_status_id.HasValue)
                {
                    if (dbContext.card_status.Where(ss => ss.card_status_id == card_status_id).Count() <= 0)
                        return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден статус карт с кодом " + card_status_id.ToString()) };
                }

                card_query = from c in dbContext.vw_card
                             from ct in dbContext.card_type_business_org
                             where c.business_id == business_id
                             && c.curr_card_type_id == ct.card_type_id
                             && ct.business_org_id == ls.org_id
                             select c;
                if (card_type_id.HasValue)
                {
                    card_query = from c in card_query
                                 from crel in dbContext.card_card_type_rel
                                 where c.card_id == crel.card_id
                                 && crel.card_type_id == card_type_id
                                 && ((crel.date_beg <= today && (crel.date_end > today || !crel.date_end.HasValue)))
                                 select c;
                }
                if (card_status_id.HasValue)
                {
                    card_query = from c in card_query
                                 from crel in dbContext.card_card_status_rel
                                 where c.card_id == crel.card_id
                                 && crel.card_status_id == card_status_id
                                 && ((crel.date_beg <= today && (crel.date_end > today || !crel.date_end.HasValue)))
                                 select c;
                }
                if (card_id.HasValue)
                {
                    card_query = card_query.Where(ss => ss.card_id == card_id);
                }
                if (card_num.HasValue)
                {
                    card_query = card_query.Where(ss => ss.card_num == card_num);
                }
                if (!String.IsNullOrEmpty(card_num2))
                {
                    card_query = card_query.Where(ss => ss.card_num2.Trim().Equals(card_num2.Trim()));
                }
                if (!String.IsNullOrEmpty(client_name))
                {
                    card_query = from c in card_query
                                 from crel in dbContext.card_holder_card_rel
                                 from cl in dbContext.card_holder
                                 where c.card_id == crel.card_id
                                 && crel.card_holder_id == cl.card_holder_id
                                 && cl.card_holder_surname.Trim().ToLower().Contains(client_name.Trim().ToLower())
                                 && ((crel.date_beg <= today && (crel.date_end > today || !crel.date_end.HasValue)))
                                 select c;
                }
                if (date_beg.HasValue)
                {
                    card_query = card_query.Where(ss => ss.date_beg >= date_beg);
                }
                if (date_end.HasValue)
                {
                    card_query = card_query.Where(ss => ((ss.date_end.HasValue && ss.date_end <= date_end) || (!ss.date_end.HasValue)));
                }


                if ((first_num.HasValue) || (last_num.HasValue))
                {
                    if (sort_field.HasValue)
                    {
                        try
                        {
                            card_sort_field = (Enums.CardSortField)((int)sort_field);
                        }
                        catch
                        {
                            card_sort_field = Enums.CardSortField.DEFAULT;
                        }
                    }

                    switch (card_sort_field)
                    {
                        case Enums.CardSortField.CARD_ID:
                        case Enums.CardSortField.DEFAULT:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.card_id).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.card_id).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;
                        case Enums.CardSortField.CARD_NUM:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.card_num).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.card_num).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;

                        case Enums.CardSortField.CARD_NUM2:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.card_num2).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.card_num2).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;
                        case Enums.CardSortField.CARD_STATUS_ID:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_card_status_id).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_card_status_id).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;
                        case Enums.CardSortField.CARD_TYPE_ID:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_card_type_id).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_card_type_id).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;
                        case Enums.CardSortField.CLIENT_ID:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_holder_id).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_holder_id).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;
                        case Enums.CardSortField.DATE_BEG:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.date_beg).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.date_beg).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;
                        case Enums.CardSortField.TRANS_COUNT:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_trans_count).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_trans_count).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;
                        case Enums.CardSortField.TRANS_SUM:
                            if (first_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_trans_sum).Skip((int)first_num - 1);
                            }
                            if (last_num.HasValue)
                            {
                                card_query = card_query.OrderBy(ss => ss.curr_trans_sum).Take((int)last_num - first_num.GetValueOrDefault(1) + 1);
                            }
                            break;
                        default:
                            break;
                    }

                }
                else
                {
                    card_query = card_query.OrderBy(ss => ss.business_id).ThenBy(ss => ss.card_id);
                }

                //card_list = card_query.OrderBy(ss => ss.business_id).ThenBy(ss => ss.card_id).ToList();
                card_list = card_query.ToList();

                if ((card_list == null) || (card_list.Count <= 0))
                {
                    return new CardList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Карты не найдены") };
                }

                if (card_list.Count == 1)
                {
                    // обновляем номиналы, если на типе карт установлен такой признак
                    var single_card = card_list.FirstOrDefault();
                    var update_nominal = dbContext.card_type.Where(ss => ss.card_type_id == single_card.curr_card_type_id && ss.update_nominal == 1).FirstOrDefault();
                    if (update_nominal != null)
                        xRecalcCardNominal(single_card.card_id, ls);
                }

                return new CardList(card_list);
            }
        }        

        private Card xInsertCard(string session_id, long business_id, DateTime? date_beg, DateTime? date_end, long? curr_card_type_id, long? curr_card_status_id
            , long? card_num, string card_num2, string mess, long? source_card_id, List<string> card_nums, string app_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };            

            bool fromAu = false;
            card card = new card();
            vw_card vw_card = null;
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            using (AuMainDb dbContext = new AuMainDb())
            {
                Business business = GetBusiness(session_id, business_id);
                if ((business == null) || (business.error != null))
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                fromAu = GlobalUtil.IsKassa(app_id);
                if (fromAu)
                {
                    BusinessOrg bo = xGetBusinessOrg_byUserName(business_id, ls.user_name);
                    if ((bo == null) || (bo.error != null))
                        return new Card() { error = (((bo != null) && (bo.error != null)) ? bo.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдено отделении организации с кодом " + business_id.ToString())) };
                    if ((!bo.allow_card_add) || (!business.allow_card_add))
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Запрет добавления карт через Кассу") };
                }

                CardType card_type = null;
                if (curr_card_type_id.HasValue)
                {
                    card_type = GetCardType(session_id, business_id, (long)curr_card_type_id);
                    if ((card_type == null) || (card_type.error != null))
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден тип карт с кодом " + curr_card_type_id.ToString()) };
                }

                CardStatus card_status = null;
                if (curr_card_status_id.HasValue)
                {
                    card_status = GetCardStatus(session_id, (long)curr_card_status_id);
                    if ((card_status == null) || (card_status.error != null))
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден статус карт с кодом " + curr_card_status_id.ToString()) };
                }

                /*
                if (card_num.HasValue)
                {
                    long card_id_with_same_num = dbContext.card.Where(ss => ss.business_id == business_id 
                        && ss.card_num.HasValue && ss.card_num == card_num).Select(ss => ss.card_id).FirstOrDefault();
                    if (card_id_with_same_num > 0)
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT, "Карта с номером " + card_num.ToString() + " уже существует, код карты " + card_id_with_same_num.ToString()) };
                }
                */
                var existing_cards = GetCardList_byCardNum(session_id, business_id, card_num, card_num2, (short)Enums.CardSearchMode.BY_ALL_NUMS, "");
                if ((existing_cards != null) && (existing_cards.Card_list != null) && (existing_cards.Card_list.Count > 0))
                {
                    var card_with_same_num = existing_cards.Card_list.FirstOrDefault();
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT, "Карта с указанным номером уже существует, код карты " + card_with_same_num.card_id.ToString()) };
                }

                card.business_id = business_id;
                card.card_num = card_num;
                card.card_num2 = card_num2;
                card.date_beg = date_beg;
                card.date_end = date_end;
                card.curr_card_status_id = curr_card_status_id;
                card.curr_card_type_id = curr_card_type_id;
                card.mess = mess;
                card.source_card_id = source_card_id;
                card.crt_date = now;
                card.crt_user = ls.user_name;
                card.from_au = fromAu;

                dbContext.card.Add(card);

                // вставка в card_card_type_rel, card_card_status_rel, card_nominal
                card_card_status_rel card_card_status_rel = null;
                card_card_type_rel card_card_type_rel = null;                
                card_nominal card_nominal = null;
                if (curr_card_type_id.HasValue)
                {
                    card_card_type_rel = new card_card_type_rel();
                    card_card_type_rel.card = card;
                    card_card_type_rel.card_type_id = (long)curr_card_type_id;
                    card_card_type_rel.date_beg = date_beg;
                    card_card_type_rel.date_end = date_end;
                    card_card_type_rel.ord = 1;
                    dbContext.card_card_type_rel.Add(card_card_type_rel);

                    // insert empty card_nominal
                    List<card_type_unit> card_type_unit_list = dbContext.card_type_unit.Where(ss => ss.card_type_id == curr_card_type_id).ToList();
                    if ((card_type_unit_list != null) && (card_type_unit_list.Count > 0))
                    {
                        foreach (var card_type_unit in card_type_unit_list)
                        {
                            card_nominal = new card_nominal();
                            card_nominal.card = card;
                            card_nominal.date_beg = card.date_beg;                            
                            card_nominal.unit_type = card_type_unit.unit_type;
                            card_nominal.unit_value = 0;
                            card_nominal.crt_date = now;
                            card_nominal.crt_user = ls.user_name;                            
                            dbContext.card_nominal.Add(card_nominal);
                        }
                    }
                }

                if (curr_card_status_id.HasValue)
                {
                    card_card_status_rel = new card_card_status_rel();
                    card_card_status_rel.card = card;
                    card_card_status_rel.card_status_id = (long)curr_card_status_id;
                    card_card_status_rel.date_beg = date_beg;
                    card_card_status_rel.date_end = date_end;
                    dbContext.card_card_status_rel.Add(card_card_status_rel);
                }

                if ((card_nums != null) && (card_nums.Count > 0))
                {
                    foreach (var num in card_nums.Where(ss => !String.IsNullOrEmpty(ss)))
                    {
                        card_additional_num card_additional_num = new card_additional_num();
                        card_additional_num.card = card;
                        card_additional_num.card_num = num.Trim();
                        dbContext.card_additional_num.Add(card_additional_num);
                    }
                }

                dbContext.SaveChanges();

                card.business = business != null ? business.Db_business : null;
                card.card_status = card_status != null ? card_status.Db_card_status : null;
                card.card_type = card_type != null ? card_type.Db_card_type : null;

                vw_card = dbContext.vw_card.Where(ss => ss.card_id == card.card_id).FirstOrDefault();

                Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, card.card_id.ToString(), session_id, business_id, "", ls, (long)Enums.LogEventType.CARD_ADD, card.card_id, now, null, true);

                //return new Card(card);
                return new Card(vw_card);
            }
        }

        private Card xMergeCard(string session_id, long business_id, string card_num, long new_card_num, string new_card_num2, int transfer_nominal_mode, string app_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            //List<card> existing_card_list = null;
            long card_num_long = 0;
            long new_card_num_long = new_card_num;
            CardList existing_CardList = null;
            card existing_card = null;
            Card existing_Card = null;
            //List<card> new_card_list = null;
            CardList new_CardList = null;
            card new_card = null;
            Card new_Card = null;
            bool create_new_card = false;
            Enums.CardTransferNominalMode transferMode = Enums.CardTransferNominalMode.REPLACE_ALL;

            try
            {
                card_num_long = Convert.ToInt64(card_num);
            }
            catch
            {
                card_num_long = 0;
            }

            using (AuMainDb dbContext = new AuMainDb())
            {
                existing_CardList = GetCardList_byCardNum(session_id, business_id, card_num_long, card_num, (short)Enums.CardSearchMode.BY_ALL_NUMS, "");
                if ((existing_CardList == null) || (existing_CardList.error != null) || (existing_CardList.Card_list == null) || (existing_CardList.Card_list.Count <= 0))
                {
                    return new Card() { error = existing_CardList.error != null ? existing_CardList.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с номером " + card_num.ToString()) };
                }
                if (existing_CardList.Card_list.Count > 1)
                {
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT, "Найдено несколько карт с номером " + card_num.ToString()) };
                }
                existing_Card = existing_CardList.Card_list.FirstOrDefault();
                existing_card = dbContext.card.Where(ss => ss.card_id == existing_Card.card_id).FirstOrDefault();

                // не давать заменять карту со статусами "Закрыта", "Утеряна" (статус карты, которую заменяем)
                if ((existing_card.curr_card_status_id == (long)Enums.CardStatusEnum.LOST) || (existing_card.curr_card_status_id == (long)Enums.CardStatusEnum.CLOSED))
                {
                    return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нельзя заменить утерянную/закрытую карту") };
                }

                //new_card_list = dbContext.card.Where(ss => ss.card_num == new_card_num && ss.business_id == business_id).ToList();
                new_CardList = GetCardList_byCardNum(session_id, business_id, new_card_num_long, new_card_num2, (short)Enums.CardSearchMode.BY_ALL_NUMS, "");
                if ((new_CardList == null) || (new_CardList.error != null) || (new_CardList.Card_list == null) || (new_CardList.Card_list.Count <= 0))
                {
                    create_new_card = true;
                }
                else
                {
                    if (new_CardList.Card_list.Count > 1)
                    {
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT, "Найдено несколько карт с заданным номером. Укажите другой номер") };
                    }
                    //new_card = new_card_list.FirstOrDefault();                    
                    new_Card = new_CardList.Card_list.FirstOrDefault();
                    new_card = dbContext.card.Where(ss => ss.card_id == new_Card.card_id).FirstOrDefault();
                }

                // либо создаем новую карту
                if (create_new_card)
                {
                    if (new_card_num_long <= 0)
                    {
                        try
                        {
                            new_card_num_long = String.IsNullOrEmpty(new_card_num2) ? 0 : Convert.ToInt64(new_card_num2);
                        }
                        catch
                        {
                            new_card_num_long = 0;
                        }

                        if (new_card_num_long <= 0)
                        {
                            var num = dbContext.card.Where(ss => ss.business_id == business_id).Max(ss => ss.card_num);
                            new_card_num_long = num.GetValueOrDefault(0) > 0 ? (long)num + 1 : 1;

                        }
                    }

                    var card_with_new_card_num = dbContext.card.Where(ss => ss.business_id == business_id && ss.card_num == new_card_num_long).FirstOrDefault();
                    if (card_with_new_card_num != null)
                    {
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT, "Невозможно создать карту с номером " + new_card_num_long.ToString() + ". Такая карта уже есть") };
                    }

                    var curr_card_type_id = existing_card.curr_card_type_id;
                    if (curr_card_type_id.GetValueOrDefault(0) <= 0)
                    {
                        curr_card_type_id = dbContext.card_card_type_rel.Where(ss => ss.card_id == existing_card.card_id && !ss.date_end.HasValue)
                            .OrderByDescending(ss => ss.date_beg).ThenBy(ss => ss.rel_id)
                            .Select(ss => ss.card_type_id)
                            .FirstOrDefault();
                    }
                    if (curr_card_type_id.GetValueOrDefault(0) <= 0)
                    {
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден тип карты у карты номер " + card_num.ToString()) };
                    }

                    long curr_card_status_id = (long)Enums.CardStatusEnum.ACTIVE;

                    new_Card = InsertCard(session_id, business_id, today, null, curr_card_type_id, curr_card_status_id, new_card_num_long, new_card_num2, "Выдана взамен карты номер " + card_num.ToString(), existing_card.card_id, null, app_id);
                    if ((new_Card == null) || (new_Card.error != null))
                    {
                        return new Card() { error = (new_Card.error != null ? new_Card.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Ошибка при создании карты с номером " + card_num.ToString())) };
                    }
                }
                // либо берем существующую
                else
                {
                    // нельзя заменять карту на саму себя
                    if (new_card.card_id == existing_card.card_id)
                    {
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нельзя заменить карту на саму себя") };
                    }

                    // не давать заменять карту на карту со статусами "Закрыта", "Утеряна" (статус карты, на которую заменяем)
                    if ((new_card.curr_card_status_id == (long)Enums.CardStatusEnum.LOST) || (new_card.curr_card_status_id == (long)Enums.CardStatusEnum.CLOSED))
                    {
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нельзя заменить карту на утерянную/закрытую карту") };
                    }

                    // проверим, что тип карты совпадает
                    if (new_card.curr_card_type_id != existing_card.curr_card_type_id)
                    {
                        return new Card() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "У новой карты должен быть тот же тип, что и у заменяемой") };
                    }
                    new_Card = new Card(new_card);
                }

                try
                {
                    transferMode = (Enums.CardTransferNominalMode)transfer_nominal_mode;
                }
                catch
                {
                    transferMode = Enums.CardTransferNominalMode.REPLACE_ALL;
                }

                //new_card = new_Card.Db_card;
                new_card = dbContext.card.Where(ss => ss.card_id == new_Card.card_id).FirstOrDefault();

                // переносим владельца                
                new_card.curr_holder_id = existing_card.curr_holder_id;
                var curr_client = dbContext.card_holder_card_rel.Where(ss => ss.card_holder_id == existing_card.curr_holder_id && ss.card_id == existing_card.card_id && !ss.date_end.HasValue)
                    .OrderByDescending(ss => ss.date_beg).ThenByDescending(ss => ss.rel_id)
                    .FirstOrDefault();
                if (curr_client != null)
                {
                    curr_client.date_end = today;
                }
                if (new_card.curr_holder_id.HasValue)
                {
                    card_holder_card_rel new_client = new card_holder_card_rel();
                    new_client.card_id = new_card.card_id;
                    new_client.card_holder_id = (long)new_card.curr_holder_id;
                    new_client.date_beg = today;
                    new_client.date_end = null;
                    dbContext.card_holder_card_rel.Add(new_client);
                }
                existing_card.curr_holder_id = null;

                // todo: переносим продажи (или не надо ?)

                // переносим номиналы, накопленную сумму, лимиты
                switch (transferMode)
                {
                    case Enums.CardTransferNominalMode.REPLACE_ALL:
                    case Enums.CardTransferNominalMode.ADD_ALL:
                        var existing_nominal_list = dbContext.card_nominal.Where(ss => ss.card_id == existing_card.card_id && !ss.date_end.HasValue).ToList();
                        if ((existing_nominal_list != null) && (existing_nominal_list.Count > 0))
                        {
                            var existing_nominals_on_new_card = dbContext.card_nominal.Where(ss => ss.card_id == new_card.card_id && !ss.date_end.HasValue).ToList();
                            if ((existing_nominals_on_new_card != null) && (existing_nominals_on_new_card.Count > 0))
                            {
                                foreach (var existing_nominal_on_new_card in existing_nominals_on_new_card)
                                {
                                    existing_nominal_on_new_card.date_end = today;
                                }
                            }

                            List<card_nominal> new_nominal_list = new List<card_nominal>();
                            foreach (var existing_nominal in existing_nominal_list)
                            {
                                var new_nominal = new card_nominal();
                                new_nominal.card_id = new_card.card_id;
                                new_nominal.crt_date = now;
                                new_nominal.crt_user = ls.user_name;
                                new_nominal.date_beg = today;
                                new_nominal.date_end = null;
                                if (transferMode == Enums.CardTransferNominalMode.REPLACE_ALL)
                                {
                                    new_nominal.unit_type = existing_nominal.unit_type;
                                    new_nominal.unit_value = existing_nominal.unit_value;
                                }
                                else
                                {
                                    switch ((Enums.CardUnitTypeEnum)existing_nominal.unit_type)
                                    {
                                        case Enums.CardUnitTypeEnum.BONUS_VALUE:
                                        case Enums.CardUnitTypeEnum.DISCOUNT_VALUE:
                                        case Enums.CardUnitTypeEnum.MONEY:
                                            new_nominal.unit_type = existing_nominal.unit_type;
                                            var existing_value_on_new_card = dbContext.card_nominal.Where(ss => ss.card_id == new_card.card_id && ss.unit_type == existing_nominal.unit_type).OrderByDescending(ss => ss.card_nominal_id).FirstOrDefault().unit_value;
                                            new_nominal.unit_value = existing_nominal.unit_value.GetValueOrDefault(0) + existing_value_on_new_card.GetValueOrDefault(0);
                                            break;
                                        default:
                                            new_nominal.unit_type = existing_nominal.unit_type;
                                            new_nominal.unit_value = existing_nominal.unit_value;
                                            break;
                                    }
                                }
                                new_nominal_list.Add(new_nominal);
                            }
                            dbContext.card_nominal.AddRange(new_nominal_list);
                        }

                        if (transferMode == Enums.CardTransferNominalMode.REPLACE_ALL)
                        {
                            new_card.curr_trans_count = 0;
                            new_card.curr_trans_sum = existing_card.curr_trans_sum;
                        }
                        else
                        {
                            new_card.curr_trans_count = new_card.curr_trans_count.GetValueOrDefault(0) + existing_card.curr_trans_count.GetValueOrDefault(0);
                            new_card.curr_trans_sum = new_card.curr_trans_sum.GetValueOrDefault(0) + existing_card.curr_trans_sum.GetValueOrDefault(0);
                        }

                        var existing_limits = dbContext.card_ext1.Where(ss => ss.card_id == existing_card.card_id).ToList();
                        if (existing_limits != null)
                        {
                            var existing_limits_copy = existing_limits.Select(ss => new { ss.curr_limit_id, ss.curr_sum, });
                            dbContext.card_ext1.RemoveRange(existing_limits);

                            var new_limits = dbContext.card_ext1.Where(ss => ss.card_id == new_card.card_id);
                            var new_limits_copy = new_limits.Select(ss => new { ss.curr_limit_id, ss.curr_sum, });
                            dbContext.card_ext1.RemoveRange(new_limits);

                            foreach (var existing_limit in existing_limits_copy)
                            {
                                card_ext1 new_limit = new card_ext1();
                                new_limit.card_id = new_card.card_id;
                                new_limit.curr_limit_id = existing_limit.curr_limit_id;
                                if (transferMode == Enums.CardTransferNominalMode.REPLACE_ALL)
                                {
                                    new_limit.curr_sum = existing_limit.curr_sum;
                                }
                                else
                                {
                                    new_limit.curr_sum = existing_limit.curr_sum.GetValueOrDefault(0) + new_limits_copy.Where(ss => ss.curr_limit_id == existing_limit.curr_limit_id).FirstOrDefault().curr_sum.GetValueOrDefault(0);
                                }
                                dbContext.card_ext1.Add(new_limit);
                            }
                        }
                        break;
                    case Enums.CardTransferNominalMode.NONE:
                        break;
                    default:
                        break;
                }

                // закрываем старую карту
                card_card_status_rel curr_existing_card_status = dbContext.card_card_status_rel
                    .Where(ss => ss.card_id == existing_card.card_id && ss.card_status_id == existing_card.curr_card_status_id && !ss.date_end.HasValue)
                    .OrderByDescending(ss => ss.date_end).ThenByDescending(ss => ss.rel_id)
                    .FirstOrDefault();
                if (curr_existing_card_status != null)
                {
                    curr_existing_card_status.date_end = today;
                }
                card_card_status_rel new_existing_card_status = new card_card_status_rel();
                new_existing_card_status.card_id = existing_card.card_id;
                new_existing_card_status.card_status_id = (long)Enums.CardStatusEnum.CLOSED;
                new_existing_card_status.date_beg = today;
                new_existing_card_status.date_end = null;
                dbContext.card_card_status_rel.Add(new_existing_card_status);

                existing_card.date_end = today;
                existing_card.curr_card_status_id = (long)Enums.CardStatusEnum.CLOSED;
                existing_card.mess = "Карта закрыта. Вместо нее выпущена карта номер " + new_card_num_long.ToString();

                dbContext.SaveChanges();

                return new_Card;
            }
        }

        private long xIsCardActive(string session_id, long business_id, long card_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return (long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND;

            using (AuMainDb dbContext = new AuMainDb())
            {
                var res = (from c in dbContext.card
                           from cs in dbContext.card_status
                           from csg in dbContext.card_status_group
                           from ct in dbContext.card_type_business_org
                           where c.business_id == business_id
                           && c.curr_card_status_id == cs.card_status_id
                           && cs.card_status_group_id == csg.card_status_group_id
                           //&& cs.card_status_group_id == 1
                           && c.card_id == card_id
                           && c.curr_card_type_id == ct.card_type_id
                           && ct.business_org_id == ls.org_id
                           select new { card = c, csg.is_active })
                           .FirstOrDefault();

                if (res == null)
                    return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;
                if (res.card == null)
                    return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;

                return (long)res.is_active;
            }
        }

        private decimal xGetCardValueForPay(string session_id, long business_id, long card_num)
        {

            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return 0;

            // т.к. метод работает по card_num - ищем только по конкретной business, без учета связей
            List<card> card_list = null;
            card card = null;
            DateTime today = DateTime.Today;

            using (AuMainDb dbContext = new AuMainDb())
            {
                card_list = (from c in dbContext.card
                            from ct in dbContext.card_type_business_org
                            where c.business_id == business_id && c.card_num.HasValue && c.card_num == card_num
                            && c.curr_card_type_id == ct.card_type_id
                            && ct.business_org_id == ls.org_id
                            select c)
                            .ToList();

                if ((card_list == null) || (card_list.Count <= 0))
                    return 0;
                if (card_list.Count > 1)
                    return 0;
                
                card = card_list.FirstOrDefault();

                card_type_unit card_type_unit = (from crel1 in dbContext.card_card_type_rel
                                                 from crel2 in dbContext.card_type_programm_rel
                                                 from ppar in dbContext.programm_step_param
                                                 from ctu in dbContext.card_type_unit
                                                 from crel3 in dbContext.card_card_status_rel
                                                 from cs in dbContext.card_status
                                                 from csg in dbContext.card_status_group
                                                 where crel1.card_id == card.card_id
                                                 && crel1.date_beg <= today && ((!crel1.date_end.HasValue) || (crel1.date_end >= today))
                                                 && crel1.card_type_id == crel2.card_type_id
                                                 && crel2.date_beg <= today && ((!crel2.date_end.HasValue) || (crel2.date_end >= today))
                                                 && crel2.programm_id == ppar.programm_id
                                                 && crel3.card_id == card.card_id
                                                 && crel3.date_beg <= today && ((!crel3.date_end.HasValue) || (crel3.date_end >= today))
                                                 && cs.card_status_id == crel3.card_status_id
                                                 && csg.card_status_group_id == cs.card_status_group_id
                                                 && csg.is_active == 1
                                                 && crel1.card_type_id == ctu.card_type_id
                                                 && ppar.card_param_type == ctu.unit_type
                                                 && ppar.is_programm_output == 1
                                                 // !!!
                                                 // && ctu.spend_to_sale_mode.HasValue && ctu.spend_to_sale_mode > 0
                                                 && ((ctu.unit_type == (int)Enums.CardUnitTypeEnum.BONUS_VALUE) || (ctu.unit_type == (int)Enums.CardUnitTypeEnum.MONEY))
                                                 select ctu)
                                                .FirstOrDefault();

                if (card_type_unit == null)
                    return 0;

                // обновляем номиналы, если на типе карт установлен такой признак
                var update_nominal = dbContext.card_type.Where(ss => ss.card_type_id == card.curr_card_type_id && ss.update_nominal == 1).FirstOrDefault();
                if (update_nominal != null)
                    xRecalcCardNominal(card.card_id, ls);

                card_nominal card_nominal = dbContext.card_nominal.Where(ss => ss.card_id == card.card_id && ss.unit_type == card_type_unit.unit_type
                    && !ss.date_end.HasValue
                    // !!!
                    && ((!ss.date_expire.HasValue) || (ss.date_expire >= today))
                    )
                    .FirstOrDefault();

                if (card_nominal == null)
                    return 0;

                return card_nominal.unit_value.GetValueOrDefault(0);
            }            
        }
        
        #endregion

        #region CardCardStatusRel

        private CardCardStatusRel xInsertCardStatus_byCard(string session_id, long business_id, long card_id, long card_status_id, DateTime date_beg, bool from_batch)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new CardCardStatusRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            Card card_existing = GetCard(session_id, business_id, card_id);
            if ((card_existing == null) || (card_existing.error != null))
                return new CardCardStatusRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };
            CardStatus card_status_existing = GetCardStatus(session_id, card_status_id);
            if ((card_status_existing == null) || (card_status_existing.error != null))
                return new CardCardStatusRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден статус карт с кодом " + card_status_id.ToString()) };


            DateTime date_beg_date_only = date_beg.Date;
            CardCardStatusRel card_status_rel_existing = null;
            CardCardStatusRelList card_status_rel_list_existing = GetCardStatusList_byCard(session_id, business_id, card_id, null);

            card_card_status_rel card_card_status_rel_new = null;
            bool isCardStatusExists = false;
            bool isCardStatusChanged = false;
            if ((card_status_rel_list_existing != null) && (card_status_rel_list_existing.CardCardStatusRel_list != null) && (card_status_rel_list_existing.CardCardStatusRel_list.Count > 0))
            {
                card_status_rel_existing = card_status_rel_list_existing.CardCardStatusRel_list.OrderBy(ss => ss.date_beg).ThenBy(ss => ss.rel_id).FirstOrDefault();                
                if (card_status_rel_existing.date_beg > date_beg_date_only)
                    return new CardCardStatusRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DATE_INTERSECTION, "Существует статус карты с более поздней датой начала, код карты " + card_id.ToString()) };
                card_status_rel_existing = card_status_rel_list_existing.CardCardStatusRel_list.OrderByDescending(ss => ss.date_beg).ThenByDescending(ss => ss.rel_id).FirstOrDefault();
                if (card_status_rel_existing.date_end.HasValue)
                    return new CardCardStatusRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DATE_INTERSECTION, "Последний статус карты закрыт, код карты " + card_id.ToString()) };
                if (card_status_rel_existing.date_beg > date_beg_date_only)
                    return new CardCardStatusRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DATE_INTERSECTION, "Существует открытый статус карты с более поздней датой начала, код карты " + card_id.ToString()) };
                if (card_status_rel_existing.card_status_id == card_status_id)
                    return new CardCardStatusRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DATE_INTERSECTION, "На карте уже установлен данный статус, код карты " + card_id.ToString()) };

                isCardStatusExists = true;
            }

            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardCardStatusRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (isCardStatusExists)
                {
                    card_card_status_rel card_status_rel_existing_for_update = dbContext.card_card_status_rel.Where(ss => ss.rel_id == card_status_rel_existing.rel_id).FirstOrDefault();
                    if (card_status_rel_existing_for_update != null)
                        card_status_rel_existing_for_update.date_end = date_beg_date_only;
                }

                card_card_status_rel_new = new card_card_status_rel()
                {
                    card_id = card_id,
                    card_status_id = card_status_id,
                    date_beg = date_beg_date_only,
                    date_end = null,
                };

                DateTime now = DateTime.Now;
                DateTime today = DateTime.Today;
                card card = null;
                if (date_beg_date_only <= today)
                {
                    card = dbContext.card
                        .Include("business")                        
                        .Include("card_status")
                        .Include("card_status.card_status_group")
                        .Where(ss => ss.card_id == card_id).FirstOrDefault();
                    if (card.curr_card_status_id != card_status_id)
                    {
                        card.curr_card_status_id = card_status_id;
                        isCardStatusChanged = true;
                    }
                }

                dbContext.card_card_status_rel.Add(card_card_status_rel_new);
                dbContext.SaveChanges();

                if (isCardStatusChanged)
                {
                    card.card_status = card_status_existing.Db_card_status;
                    Card card_existing2 = new Card(card);
                    card_existing = card_existing2;
                }
                card_card_status_rel_new.card = card_existing.Db_card;
                card_card_status_rel_new.card_status = card_status_existing.Db_card_status;

                if (!from_batch)
                    Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, card_id.ToString(), session_id, business_id, "", ls, (long)Enums.LogEventType.CARD_CARD_STATUS_EDIT, card_id, now, null, true);

                return new CardCardStatusRel(card_card_status_rel_new);
            }

        }

        private CardCardStatusRelList xGetCardStatusList_byCard(string session_id, long business_id, long card_id, DateTime? date_beg)
        {
            if (!IsValidSession(session_id))
                return new CardCardStatusRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            //DateTime date_beg_date_only = date_beg.Date;
            List<card_card_status_rel> card_card_status_rel_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardCardStatusRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (dbContext.card.Where(ss => ss.card_id == card_id).Count() <= 0)
                    return new CardCardStatusRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };

                card_card_status_rel_list = (from c in dbContext.card                           
                           from crel in dbContext.card_card_status_rel
                           where c.business_id == business_id
                           && c.card_id == crel.card_id
                           && ((!date_beg.HasValue) || ((crel.date_beg <= date_beg && (crel.date_end > date_beg || !crel.date_end.HasValue))))                           
                           && c.card_id == card_id
                           select crel)
                           .OrderBy(ss => ss.rel_id)
                            .ThenBy(ss => ss.date_beg)
                            .ThenBy(ss => ss.card_status_id)
                            /*
                            .OrderBy(ss => ss.date_beg)
                            .ThenBy(ss => ss.card_status_id)
                            */
                            .ToList()
                            ;

                if (card_card_status_rel_list == null)
                {
                    return new CardCardStatusRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardCardStatusRelList(card_card_status_rel_list);
            }

            
        }
        
        #endregion

        #region CardCardTypeRel

        private CardCardTypeRelList xGetCardTypeList_byCard(string session_id, long business_id, long card_id, DateTime? date_beg)
        {
            if (!IsValidSession(session_id))
                return new CardCardTypeRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_card_type_rel> card_card_type_rel_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardCardTypeRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (dbContext.card.Where(ss => ss.card_id == card_id).Count() <= 0)
                    return new CardCardTypeRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };

                card_card_type_rel_list = (from c in dbContext.card                                           
                                           from crel in dbContext.card_card_type_rel
                                           where c.business_id == business_id
                                           && c.card_id == crel.card_id
                                           && ((!date_beg.HasValue) || ((crel.date_beg <= date_beg && (crel.date_end > date_beg || !crel.date_end.HasValue))))                                           
                                           && c.card_id == card_id
                                           select crel)
                                           .OrderBy(ss => ss.rel_id)
                                           .ThenBy(ss => ss.date_beg)
                                           .ThenBy(ss => ss.card_type_id)
                                           .ToList()
                                           ;

                if (card_card_type_rel_list == null)
                {
                    return new CardCardTypeRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardCardTypeRelList(card_card_type_rel_list);
            }
            
        }
        
        #endregion

        #region CardNominal

        private CardNominal xGetCardNominal_byCard(string session_id, long business_id, long card_id, int unit_type)
        {
            if (!IsValidSession(session_id))
                return new CardNominal() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            switch (unit_type)
            {
                case (int)Enums.CardUnitTypeEnum.MONEY:
                case (int)Enums.CardUnitTypeEnum.BONUS_VALUE:
                case (int)Enums.CardUnitTypeEnum.DISCOUNT_PERCENT:
                case (int)Enums.CardUnitTypeEnum.BONUS_PERCENT:
                case (int)Enums.CardUnitTypeEnum.DISCOUNT_VALUE:
                    break;
                default:
                    return new CardNominal() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карточная единица с кодом " + unit_type.ToString()) };
            }

            card_nominal card_nominal = null;

            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardNominal() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                Card card1 = xGetCard(session_id, business_id, card_id);
                if (card1 == null)
                    return new CardNominal() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };
                if (card1.error != null)
                    return new CardNominal() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };
                card card = card1.Db_card;
                if (card == null)
                    return new CardNominal() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };

                card_nominal = dbContext.card_nominal.Where(ss => ss.card_id == card_id && ss.unit_type == unit_type && !ss.date_end.HasValue).FirstOrDefault();
                if (card_nominal == null)
                    return new CardNominal() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Текущий номинал отсутствует на карте с кодом " + card_id.ToString()) };
                card_nominal.card = card1.Db_card;

                return new CardNominal(card_nominal);
            }
        }

        private CardNominalList xGetCardNominalList_byCard(string session_id, long business_id, long card_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new CardNominalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_nominal> card_nominal_list = null;

            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardNominalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                Card card1 = xGetCard(session_id, business_id, card_id);
                if (card1 == null)
                    return new CardNominalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };
                if (card1.error != null)
                    return new CardNominalList() { error = card1.error };
                card card = card1.Db_card;
                if (card == null)
                    return new CardNominalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };

                // обновляем номиналы, если на типе карт установлен такой признак
                var update_nominal = dbContext.card_type.Where(ss => ss.card_type_id == card.curr_card_type_id && ss.update_nominal == 1).FirstOrDefault();
                if (update_nominal != null)
                    xRecalcCardNominal(card_id, ls);

                card_nominal_list = dbContext.card_nominal.Where(ss => ss.card_id == card_id && !ss.date_end.HasValue).ToList();
                if (card_nominal_list == null)
                    return new CardNominalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Текущий номинал отсутствует на карте с кодом " + card_id.ToString()) };
                if (card_nominal_list.Count <= 0)
                    return new CardNominalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Текущий номинал отсутствует на карте с кодом " + card_id.ToString()) };
                
                // !!!
                //card_nominal.card = card1.Db_card;

                return new CardNominalList(card_nominal_list);
            }
        }

        private void xRecalcCardNominal(long card_id, LogSession ls)
        {
            /*
                usage in:             
                xGetCardList_byCardNum
                xGetCardList_byClient
                xGetCardValueForPay
                xGetCardList_byAttrs
                xGetCardNominalList_byCard
            */

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            //List<long> id_for_del = new List<long>();
            using (AuMainDb dbContext = new AuMainDb())
            {
                var nominal_inactive_list = dbContext.card_nominal_inactive.Where(ss => ss.card_id == card_id && ss.active_date <= now).ToList();
                if (nominal_inactive_list == null)
                    return;
                var nominal_inactive_list_grouped = nominal_inactive_list
                    .GroupBy(ss => new { ss.unit_type })
                    .Select(ss => new { unit_type = ss.Key.unit_type, unit_value = ss.Sum(tt => tt.unit_value.GetValueOrDefault(0)) })
                    ;
                foreach (var nominal_inactive in nominal_inactive_list_grouped)
                {
                    switch (nominal_inactive.unit_type)
                    {
                        case (int)Enums.CardUnitTypeEnum.BONUS_VALUE:
                            var nominal = dbContext.card_nominal.Where(ss => ss.card_id == card_id && ss.unit_type == nominal_inactive.unit_type && !ss.date_end.HasValue).OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                            if (nominal == null)
                            {
                                nominal = new card_nominal();
                                nominal.card_id = card_id;
                                nominal.crt_date = now;
                                nominal.crt_user = ls.user_name;
                                nominal.date_beg = today;
                                nominal.date_end = null;
                                nominal.unit_type = nominal_inactive.unit_type;
                                nominal.unit_value = nominal_inactive.unit_value;
                            }
                            else
                            {                                                             
                                nominal.date_end = today;
                                var new_nominal = new card_nominal();
                                new_nominal.card_id = card_id;
                                new_nominal.crt_date = now;
                                new_nominal.crt_user = ls.user_name;
                                new_nominal.date_beg = today;
                                new_nominal.date_end = null;
                                new_nominal.unit_type = nominal_inactive.unit_type;
                                new_nominal.unit_value = nominal.unit_value.GetValueOrDefault(0) + nominal_inactive.unit_value;
                                dbContext.card_nominal.Add(new_nominal);
                            }
                            //id_for_del.Add(nominal_inactive.inactive_id);
                            break;
                        default:
                            break;
                    }

                    dbContext.SaveChanges();
                }
                //dbContext.card_nominal_inactive.RemoveRange(dbContext.card_nominal_inactive.Where(ss => id_for_del.Contains(ss.inactive_id)));
                dbContext.card_nominal_inactive.RemoveRange(nominal_inactive_list);
                dbContext.SaveChanges();
            }
        }

        #endregion
    }
}
