﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region CardImport

        private CardImportResult xCardImport(string session_id, long business_id, long card_type_id, long card_status_id,
            int action_for_existing_summ, int action_for_existing_disc_percent, int action_for_existing_bonus,
            ImportDiscountCardList discountCardList, ImportDiscountList discountList)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new CardImportResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена"));
            if ((discountCardList == null) || (discountCardList.ImportDiscountCard_list == null) || (discountCardList.ImportDiscountCard_list.Count <= 0))
                return new CardImportResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Список карт для импорта (import_discount_card) пуст"));
            if ((discountList == null) || (discountList.ImportDiscount_list == null) || (discountList.ImportDiscount_list.Count <= 0))
                return new CardImportResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Список карт для импорта (import_discount) пуст"));

            import_discount import_discount = null;
            import_discount_card import_discount_card = null;            
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault() == null)
                    return new CardImportResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));
                if (dbContext.card_type.Where(ss => ss.business_id == business_id && ss.card_type_id == card_type_id).FirstOrDefault() == null)
                    return new CardImportResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден тип карт с кодом " + card_type_id.ToString()));
                if (dbContext.card_status.Where(ss => ss.card_status_id == card_status_id).FirstOrDefault() == null)
                    return new CardImportResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден статус карт с кодом " + card_status_id.ToString()));

                // action_for_existing_summ == -1: т.е. это первый запуск процедуры импорта, без указания что делать с существующими картами
                if (action_for_existing_summ == -1)
                {
                    /*
                    card_num_existing_list = dbContext.card.Where(ss => ss.business_id == business_id && ss.card_num.HasValue 
                        && discountCardList.ImportDiscountCard_list.Select(tt => tt.barcode).Contains(ss.card_num.ToString()))                        
                        .Select(ss => (long)ss.card_num).ToList();
                    */
                    var card_list = dbContext.card.Where(ss => ss.business_id == business_id && ss.card_num.HasValue).Select(ss => ((long)ss.card_num)).ToList();
                    if ((card_list != null) && (card_list.Count > 0))
                    {
                        var import_card_list = discountCardList.ImportDiscountCard_list.Where(tt => !String.IsNullOrEmpty(tt.barcode)).Select(tt => tt.barcode.Trim().ToLower()).ToList();
                        var intersect_list = card_list.Select(ss => Convert.ToString(ss)).ToList().Intersect(import_card_list).ToList();
                        if ((intersect_list != null) && (intersect_list.Count > 0))
                        {
                            CardImportResult res = new CardImportResult();
                            res.ImportCardError_list = new List<BaseObject>();
                            foreach (var item in intersect_list)
                            {
                                res.ImportCardError_list.Add(new BaseObject(Convert.ToInt64(item), ""));
                            }
                            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_IMPORT_CARD_EXISTS, "Существуют карты с аналогичными номерами");
                            return res;
                        }
                    }

                }

                dbContext.import_discount.RemoveRange(dbContext.import_discount);
                dbContext.import_discount_card.RemoveRange(dbContext.import_discount_card);

                foreach (var importDiscount in discountList.ImportDiscount_list)
                {
                    import_discount = new import_discount()
                        {                            
                            id_discount = importDiscount.id_discount,
                            name_discount = importDiscount.name_discount,
                            proc_discount = importDiscount.proc_discount,
                        };
                    
                    dbContext.import_discount.Add(import_discount);
                }

                foreach (var importDiscountCard in discountCardList.ImportDiscountCard_list)
                {
                    import_discount_card = new import_discount_card()
                    {
                        barcode = importDiscountCard.barcode,
                        date_card = importDiscountCard.date_card,
                        fname = importDiscountCard.fname,
                        id_card = importDiscountCard.id_card,
                        id_discount = importDiscountCard.id_discount,
                        lname = importDiscountCard.lname,
                        mname = importDiscountCard.mname,
                        num_card = importDiscountCard.num_card,
                        total_bonus = importDiscountCard.total_bonus,
                        total_sum = importDiscountCard.total_sum,
                    };

                    dbContext.import_discount_card.Add(import_discount_card);
                }

                dbContext.SaveChanges();

                using (var conn = new Npgsql.NpgsqlConnection(dbContext.Database.Connection.ConnectionString))
                {
                    conn.Open();
                    var cmd = new Npgsql.NpgsqlCommand("select discount.import_card(" 
                        + business_id.ToString()
                        + ", "
                        + card_type_id.ToString()
                        + ", "
                        + card_status_id.ToString()
                        + ", "
                        + action_for_existing_summ.ToString()
                        + ", "
                        + action_for_existing_disc_percent.ToString()
                        + ", "
                        + action_for_existing_bonus.ToString()
                        + ")", conn);
                    cmd.ExecuteScalar();
                }  

                return new CardImportResult() { Id = 0, error = null };
            }
        }

        #endregion
    }
}
