﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region ImportDiscount

    [DataContract]
    public class ImportDiscount : BonusCardBase
    {
        public ImportDiscount()
            : base()
        {
            //
        }

        public ImportDiscount(import_discount import_discount)
        {
            db_import_discount = import_discount;

            id_discount = import_discount.id_discount;
            name_discount = import_discount.name_discount;
            proc_discount = import_discount.proc_discount;
        }

        [DataMember]
        public long? id_discount { get; set; }
        [DataMember]
        public string name_discount { get; set; }
        [DataMember]
        public decimal? proc_discount { get; set; }
        [DataMember]
        public string comment { get; set; }

        public import_discount Db_import_discount { get { return db_import_discount; } }
        private import_discount db_import_discount;
    }

    [DataContract]
    public class ImportDiscountList : BonusCardBase
    {
        public ImportDiscountList()
            : base()
        {
            //
        }

        public ImportDiscountList(List<import_discount> importDiscountList)
        {
            if (importDiscountList != null)
            {
                ImportDiscount_list = new List<ImportDiscount>();
                foreach (var item in importDiscountList)
                {
                    ImportDiscount_list.Add(new ImportDiscount(item));
                }
            }
        }

        [DataMember]
        public List<ImportDiscount> ImportDiscount_list;
    }

    #endregion

    #region ImportDiscountCard

    [DataContract]
    public class ImportDiscountCard : BonusCardBase
    {
        public ImportDiscountCard()
            : base()
        {
            //
        }

        public ImportDiscountCard(import_discount_card import_discount_card)
        {
            db_import_discount_card = import_discount_card;

            id_card = import_discount_card.id_card;
            num_card = import_discount_card.num_card;
            date_card = import_discount_card.date_card;
            lname = import_discount_card.lname;
            fname = import_discount_card.fname;
            mname = import_discount_card.mname;
            total_sum = import_discount_card.total_sum;
            id_discount = import_discount_card.id_discount;
            barcode = import_discount_card.barcode;
            total_bonus = import_discount_card.total_bonus;
            is_card_exists = import_discount_card.is_card_exists;
        }

        [DataMember]
        public Nullable<long> id_card { get; set; }
        [DataMember]
        public string num_card { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_card { get; set; }
        [DataMember]
        public string lname { get; set; }
        [DataMember]
        public string fname { get; set; }
        [DataMember]
        public string mname { get; set; }
        [DataMember]
        public Nullable<decimal> total_sum { get; set; }
        [DataMember]
        public Nullable<long> id_discount { get; set; }
        [DataMember]
        public string barcode { get; set; }
        [DataMember]
        public Nullable<decimal> total_bonus { get; set; }
        [DataMember]
        public short is_card_exists { get; set; }
        [DataMember]
        public string comment { get; set; }

        public import_discount_card Db_import_discount_card { get { return db_import_discount_card; } }
        private import_discount_card db_import_discount_card;
    }

    [DataContract]
    public class ImportDiscountCardList : BonusCardBase
    {
        public ImportDiscountCardList()
            : base()
        {
            //
        }

        public ImportDiscountCardList(List<import_discount_card> importDiscountCardList)
        {
            if (importDiscountCardList != null)
            {
                ImportDiscountCard_list = new List<ImportDiscountCard>();
                foreach (var item in importDiscountCardList)
                {
                    ImportDiscountCard_list.Add(new ImportDiscountCard(item));
                }
            }
        }

        [DataMember]
        public List<ImportDiscountCard> ImportDiscountCard_list;
    }

    #endregion
}
