﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AuDev.Common.Util;


namespace BonusCardLib
{
    [DataContract]
    public class BonusCardBase
    {
        public BonusCardBase() : base()
        {
            //            
        }


        [DataMember]
        public ErrInfo error { get; set; }
    }


    [DataContract]
    public class ErrInfo
    {
        public ErrInfo(long? id, string message = "")
        {
            Id = id;
            Message = message;                        
        }

        public ErrInfo(long? id, Exception e)
        {
            Id = id;
            if (e == null)
                return;
            /*
            Message = String.IsNullOrEmpty(e.Message) ? "" : e.Message.Trim();
            Message += e.InnerException == null ? "" : (String.IsNullOrEmpty(e.InnerException.Message) ? "" : " [" + e.InnerException.Message.Trim() + "]");
            */
            Message = GlobalUtil.ExceptionInfo(e);
        }

        [DataMember]
        public long? Id { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    [DataContract]
    public class DeleteResult
    {
        public DeleteResult()
        {
        }

        public DeleteResult(long _id, ErrInfo _error)
        {
            Id = _id;
            error = _error;
        }

        public DeleteResult(long _id, Exception _e)
        {
            Id = _id;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public DeleteResult(ErrInfo _error)
        {
            Id = (_error == null) ? (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION : (long)_error.Id;
            error = _error;
        }

        public DeleteResult(Exception _e)
        {
            Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public void AddObj(BaseObject _obj)
        {
            if (DeletedObject_list == null)
                DeletedObject_list = new List<BaseObject>();
            DeletedObject_list.Add(_obj);
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
        [DataMember]
        public List<BaseObject> DeletedObject_list { get; set; }
    }

    [DataContract]
    public class BaseObject
    {
        public BaseObject(long _id, string _name)
        {
            Id = _id;
            Name = _name;
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }

    }

    [DataContract]
    public class InsertResult
    {
        public InsertResult()
        {
        }

        public InsertResult(long _id, ErrInfo _error)
        {
            Id = _id;
            error = _error;
        }

        public InsertResult(long _id, Exception _e)
        {
            Id = _id;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public InsertResult(ErrInfo _error)
        {
            Id = (_error == null) ? (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION : (long)_error.Id;
            error = _error;
        }

        public InsertResult(Exception _e)
        {
            Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
    }

    [DataContract]
    public class ProcessResult
    {
        public ProcessResult()
        {
        }

        public ProcessResult(long _id, ErrInfo _error)
        {
            Id = _id;
            error = _error;
        }

        public ProcessResult(long _id, Exception _e)
        {
            Id = _id;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public ProcessResult(ErrInfo _error)
        {
            Id = (_error == null) ? (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION : (long)_error.Id;
            error = _error;
        }

        public ProcessResult(Exception _e)
        {
            Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public long Count { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
    }

    [DataContract]
    public class BatchOperationResult
    {
        public BatchOperationResult()
        {
        }

        public BatchOperationResult(long _id, ErrInfo _error)
        {
            Id = _id;
            error = _error;
        }

        public BatchOperationResult(long _id, Exception _e)
        {
            Id = _id;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public BatchOperationResult(ErrInfo _error)
        {
            Id = (_error == null) ? (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION : (long)_error.Id;
            error = _error;
        }

        public BatchOperationResult(Exception _e)
        {
            Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public void AddObj(BatchObject _obj)
        {
            if (BatchObject_list == null)
                BatchObject_list = new List<BatchObject>();
            BatchObject_list.Add(_obj);
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
        [DataMember]
        public List<BatchObject> BatchObject_list { get; set; }
    }

    [DataContract]
    public class BatchObject
    {
        public BatchObject(long _id, string _name)
        {
            Id = _id;
            Name = _name;
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
    }

    [DataContract]
    public class CalcUnit
    {
        public CalcUnit()
        {
            // default values
            group1_property_initialized = 0;
            group2_property_initialized = 0;
            group3_property_initialized = 0;
            unit_max_discount_percent = 100;
            unit_max_bonus_for_card_percent = 100;
            unit_max_bonus_for_pay_percent = 100;
            live_prep = 0;
            unit_price = 0;
            unit_price_tax = 0;
            unit_price_opt = 0;
            unit_price_opt_tax = 0;
            tax_percent = 0;
            tax_summa = 0;
            price_margin_value = 0;
            price_margin_percent = 0;
            artikul = 0;
            mess = "";
            unit_producer_price = 0;
            unit_pack_count = 0;
            date_expire = null;
            producer_name = "";
            producer_id  = null;
            farm_group_name = "";
            farm_group_id = null;
            mnn_name = "";
            mnn_id = null;
            ean13 = "";
        }
        
        public CalcUnit(CalcUnit source_calc_unit)
        {
            unit_num = source_calc_unit.unit_num;
            unit_name = source_calc_unit.unit_name;        
            unit_value = source_calc_unit.unit_value;        
            unit_max_discount_percent = source_calc_unit.unit_max_discount_percent;        
            unit_value_discount = source_calc_unit.unit_value_discount;        
            unit_value_with_discount = source_calc_unit.unit_value_with_discount;        
            unit_percent_discount = source_calc_unit.unit_percent_discount;        
            unit_value_bonus_for_card = source_calc_unit.unit_value_bonus_for_card;        
            unit_value_bonus_for_pay = source_calc_unit.unit_value_bonus_for_pay;        
            unit_percent_bonus_for_card = source_calc_unit.unit_percent_bonus_for_card;        
            unit_percent_bonus_for_pay = source_calc_unit.unit_percent_bonus_for_pay;        
            unit_count = source_calc_unit.unit_count;
            unit_type = source_calc_unit.unit_type;
            unit_percent_of_summa = source_calc_unit.unit_percent_of_summa;
            promo = source_calc_unit.promo;

            group1_property_initialized = source_calc_unit.group1_property_initialized;
            group2_property_initialized = source_calc_unit.group2_property_initialized;
            group3_property_initialized = source_calc_unit.group3_property_initialized;

            unit_max_bonus_for_card_percent = group1_property_initialized == 1 ? source_calc_unit.unit_max_bonus_for_card_percent : 100;
            unit_max_bonus_for_pay_percent = group1_property_initialized == 1 ? source_calc_unit.unit_max_bonus_for_pay_percent : 100;        
            live_prep = group1_property_initialized == 1 ? source_calc_unit.live_prep : 0;        
            price_margin_value = group1_property_initialized == 1 ? source_calc_unit.price_margin_value : 0;
            price_margin_percent = group1_property_initialized == 1 ? source_calc_unit.price_margin_percent : 0;
            artikul = group1_property_initialized == 1 ? source_calc_unit.artikul : 0;
            mess = "";

            unit_price = group2_property_initialized == 1 ? source_calc_unit.unit_price : 0; 
            unit_price_tax = group2_property_initialized == 1 ? source_calc_unit.unit_price_tax : 0;
            unit_price_opt = group2_property_initialized == 1 ? source_calc_unit.unit_price_opt : 0;
            unit_price_opt_tax = group2_property_initialized == 1 ? source_calc_unit.unit_price_opt_tax : 0;
            tax_percent = group2_property_initialized == 1 ? source_calc_unit.tax_percent : 0;
            tax_summa = group2_property_initialized == 1 ? source_calc_unit.tax_summa : 0;
            unit_producer_price = group2_property_initialized == 1 ? source_calc_unit.unit_producer_price : 0;
            unit_pack_count = group2_property_initialized == 1 ? source_calc_unit.unit_pack_count : 0; 

            date_expire = group3_property_initialized == 1 ? source_calc_unit.date_expire : null;
            producer_name = group3_property_initialized == 1 ? source_calc_unit.producer_name : "";
            producer_id = group3_property_initialized == 1 ? source_calc_unit.producer_id : null;
            farm_group_name = group3_property_initialized == 1 ? source_calc_unit.farm_group_name : "";
            farm_group_id = group3_property_initialized == 1 ? source_calc_unit.farm_group_id : null;
            mnn_name = group3_property_initialized == 1 ? source_calc_unit.mnn_name : "";
            mnn_id = group3_property_initialized == 1 ? source_calc_unit.mnn_id : null;
            ean13 = group3_property_initialized == 1 ? source_calc_unit.ean13 : ""; 
        }


        // основная группа (всегда инициализирована)
        [DataMember]
        public int unit_num { get; set; }
        [DataMember]
        public string unit_name { get; set; }
        [DataMember]
        public decimal unit_value { get; set; }
        [DataMember]
        public decimal unit_max_discount_percent { get; set; }
        [DataMember]
        public decimal unit_value_discount { get; set; }
        [DataMember]
        public decimal unit_value_with_discount { get; set; }
        [DataMember]
        public decimal unit_percent_discount { get; set; }
        [DataMember]
        public decimal unit_value_bonus_for_card { get; set; }
        [DataMember]
        public decimal unit_value_bonus_for_pay { get; set; }
        [DataMember]
        public decimal unit_percent_bonus_for_card { get; set; }
        [DataMember]
        public decimal unit_percent_bonus_for_pay { get; set; }       
        [DataMember]
        //public int unit_count { get; set; }
        public decimal unit_count { get; set; }
        [DataMember]
        public int unit_type { get; set; }

        // всегда считается в сервисе, перед началом применения алгоритма        
        public decimal unit_percent_of_summa { get; set; }

        // признак "акционный товар"
        [DataMember]
        public int promo { get; set; }

        [DataMember]
        public int group1_property_initialized { get; set; }
        [DataMember]
        public int group2_property_initialized { get; set; }
        [DataMember]
        public int group3_property_initialized { get; set; }
        
        // доп. группы (инициализированы в зависимости от значения соответствующего атрибута)

        // доп группа 1
        [DataMember]
        public decimal unit_max_bonus_for_card_percent { get; set; }
        [DataMember]
        public decimal unit_max_bonus_for_pay_percent { get; set; }
        [DataMember]
        public int live_prep { get; set; }
        [DataMember]
        public decimal price_margin_value { get; set; }
        [DataMember]
        public decimal price_margin_percent { get; set; }
        [DataMember]
        public int? artikul { get; set; }
        [DataMember]
        public string mess { get; set; }

        // доп группа 2
        [DataMember]
        public decimal unit_price { get; set; }
        [DataMember]
        public decimal unit_price_tax { get; set; }
        [DataMember]
        public decimal unit_price_opt { get; set; }
        [DataMember]
        public decimal unit_price_opt_tax { get; set; }
        [DataMember]
        public decimal tax_percent { get; set; }
        [DataMember]
        public decimal tax_summa { get; set; }
        [DataMember]
        public decimal unit_producer_price { get; set; }
        [DataMember]
        public int unit_pack_count { get; set; }
        // доп группа 3
        [DataMember]
        public DateTime? date_expire { get; set; }
        [DataMember]
        public string producer_name { get; set; }
        [DataMember]
        public long? producer_id { get; set; }
        [DataMember]
        public string farm_group_name { get; set; }
        [DataMember]
        public long? farm_group_id { get; set; }
        [DataMember]
        public string mnn_name { get; set; }
        [DataMember]
        public long? mnn_id { get; set; }
        [DataMember]
        public string ean13 { get; set; }
    }


    [DataContract]
    public class CalcAbstractResult
    {

    }

    [DataContract]
    public class CalcStartResult : CalcAbstractResult
    {
        public CalcStartResult()
        {
        }
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
        [DataMember]
        public long trans_id { get; set; }
        [DataMember]
        public List<CalcUnit> calc_unit_result { get; set; }
        [DataMember]
        public decimal curr_trans_sum { get; set; }
        [DataMember]
        public decimal curr_trans_count { get; set; }
        [DataMember]
        public CardNominalList card_nominal { get; set; }
        [DataMember]
        public CheckPrintInfo print_info { get; set; }
        [DataMember]
        public string warn { get; set; }        
    }

    [DataContract]
    public class CalcCommitResult : CalcAbstractResult
    {
        public CalcCommitResult() : base()
        {
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
    }

    [DataContract]
    public class CalcCancelResult : CalcAbstractResult
    {
        public CalcCancelResult() : base()
        {
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }

    }

    [DataContract]
    public class CalcRound
    {
        [DataMember]
        public decimal round_value { get; set; }
        [DataMember]
        public int? artikul { get; set; }
    }

    [DataContract]
    public class CardImportResult
    {
        public CardImportResult()
        {
        }

        public CardImportResult(long _id, ErrInfo _error)
        {
            Id = _id;
            error = _error;
        }

        public CardImportResult(long _id, Exception _e)
        {
            Id = _id;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public CardImportResult(ErrInfo _error)
        {
            Id = (_error == null) ? (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION : (long)_error.Id;
            error = _error;
        }

        public CardImportResult(Exception _e)
        {
            Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public List<BaseObject> ImportCardError_list { get; set; }
        [DataMember]
        public ErrInfo error { get; set; }
    }

    [DataContract]
    public class UserInfo : BonusCardBase
    {
        public UserInfo()
        {
            //
        }

        [DataMember]
        public bool is_authenticated { get; set; }
        [DataMember]
        public string login_name { get; set; }
        [DataMember]
        public string role_name { get; set; }
        [DataMember]
        public long? role_id { get; set; }
        [DataMember]
        public long? business_id { get; set; }
    }

    [DataContract]
    public class UserInfoList : BonusCardBase
    {
        public UserInfoList()
            : base()
        {
            //
        }

        public UserInfoList(List<UserInfo> userInfoList)
        {
            if (userInfoList != null)
            {
                UserInfo_list = new List<UserInfo>(userInfoList);
            }
            else
            {
                UserInfo_list = new List<UserInfo>();
            }
        }

        [DataMember]
        public List<UserInfo> UserInfo_list;
    }


}
