﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using AuDev.Common.WCF;

namespace BonusCardLib
{
    //[ServiceContract(Namespace = "http://service.aptekaural.ru/bonuscard/")]
    //[ServiceContract(Namespace = "http://tempuri.org/", SessionMode = SessionMode.Required)]
    [ServiceContract(Namespace = "http://tempuri.org/")]
    [WsdlDocumentation("Сервис дисконтных карт")]
    public interface IBonusCardService
    {

        #region GetVersion
        
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Запрос текущей версии сервиса")]
        [return: WsdlParamOrReturnDocumentation("Возвращает строку с номером версии сервиса")]
        string GetVersion([WsdlParamOrReturnDocumentation("Код сессии")]string session_id);

        #endregion

        #region GetVersionDb

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Запрос текущей версии БД")]
        [return: WsdlParamOrReturnDocumentation("Возвращает строку с номером версии БД")]
        string GetVersionDb([WsdlParamOrReturnDocumentation("Код сессии")]string session_id);

        #endregion        

        #region Session

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Создание рабочей сессии с сервисом")]
        [return: WsdlParamOrReturnDocumentation("Возвращает строку с кодом созданной сессии")]
        string StartSession([WsdlParamOrReturnDocumentation("Логин")]string user_name
            , [WsdlParamOrReturnDocumentation("Пароль (в зашифрованном виде)")]string user_pwd
            , [WsdlParamOrReturnDocumentation("Код организации, для которой создается сессия. Если не задан - организация определяется по логину")]long? business_id
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Закрытие рабочей сессии с сервисом")]
        [return: WsdlParamOrReturnDocumentation("Возвращает 0 если сессия успешно закрыта, иначе - код ошибки")]
        long CloseSession([WsdlParamOrReturnDocumentation("Код сессии")]string session_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        LogSession GetActiveSession(string session_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        LogSession GetSession(string session_id);


        //[OperationContract]
        //string EncryptString(string str);

        //[OperationContract]
        //string DecryptString(string str);

        #endregion

        #region Business

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Business GetBusiness(string session_id, long business_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        BusinessList GetBusinessList(string session_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Business GetBusiness_byCard(string session_id, long card_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        BusinessList GetBusinessList_byAttrs(string session_id, string business_name);

        #endregion

        #region BusinessRel (old)
                
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        BusinessRelList GetBusinessRelList(string session_id);        

        #endregion

        #region CardType
        
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardType GetCardType(string session_id, long business_id, long card_type_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardType GetCardType_byCard(string session_id, long business_id, long card_id);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTypeList GetCardTypeList(string session_id, long business_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTypeList GetCardTypeList_byAttrs(string session_id, long business_id, string card_type_name);

        #endregion

        #region CardTypeUnit

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTypeUnitList GetCardTypeUnitList_byCardType(string session_id, long business_id, long card_type_id);

        #endregion

        #region CardStatusGroup

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardStatusGroupList GetCardStatusGroupList(string session_id);

        #endregion

        #region CardStatus

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardStatus GetCardStatus(string session_id, long card_status_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardStatusList GetCardStatusList(string session_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardStatus GetCardStatus_byCard(string session_id, long card_id);

        #endregion

        #region Card

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Card GetCard(string session_id, long business_id, long card_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardList GetCardList(string session_id, long business_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardList GetCardList_byCardNum(string session_id, long business_id, long? card_num, string card_num2, short? search_mode, string mask);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardList GetCardList_byClient(string session_id, long business_id, string client_surname, string client_name, string client_fname, int? client_age);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardList GetCardList_byAttrs(string session_id, long business_id
            , long? card_type_id, long? card_status_id, long? card_id, long? card_num, string card_num2, string client_name
            , DateTime? date_beg, DateTime? date_end, long? reestr_id
            , int? first_num, int? last_num, int? sort_field
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        long IsCardActive(string session_id, long business_id, long card_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Card InsertCard(string session_id, long business_id, DateTime? date_beg, DateTime? date_end, long? curr_card_type_id, long? curr_card_status_id, long? card_num, string card_num2, string mess, long? source_card_id, List<string> card_nums, string app_id);        
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        decimal GetCardValueForPay(string session_id, long business_id, long card_num);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Замена выбранной карты с созданием новой карты либо с указанием уже существующей карты")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект Card")]
        Card MergeCard([WsdlParamOrReturnDocumentation("Код сессии")]string session_id,
            [WsdlParamOrReturnDocumentation("Код организации")]long business_id,
            [WsdlParamOrReturnDocumentation("Номер карты, которую нужно заменить")]string card_num,
            [WsdlParamOrReturnDocumentation("Номер новой карты (числовой). Если карты с таким номером еще нет, то она будет автоматически создана")]long new_card_num,
            [WsdlParamOrReturnDocumentation("Номер новой карты (символьный). Если карты с таким номером еще нет, то она будет автоматически создана")]string new_card_num2,
            [WsdlParamOrReturnDocumentation("0 - перенести все номиналы со старой карты и заменить ими номиналы новой карты, 1 - перенести все номиналы со старой карты и добавить их к номиналам новой карты, 2 - не переносить номиналы со старой карты")]int transfer_nominal_mode,
            [WsdlParamOrReturnDocumentation("Уникальный код приложения, вызвавшего этот метод")]string app_id
            );

        #endregion

        #region CardCardStatusRel

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardCardStatusRel InsertCardStatus_byCard(string session_id, long business_id, long card_id, long card_status_id, DateTime date_beg, bool from_batch);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardCardStatusRelList GetCardStatusList_byCard(string session_id, long business_id, long card_id, DateTime? date_beg);

        #endregion
        
        #region CardCardTypeRel

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardCardTypeRelList GetCardTypeList_byCard(string session_id, long business_id, long card_id, DateTime? date_beg);

        #endregion

        #region Programm

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Programm InsertProgramm(string session_id, long business_id, string programm_name, DateTime? date_beg, DateTime? date_end, string descr_short, string descr_full, long? card_type_group_id, ProgrammTemplate templ);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Programm UpdateProgramm(string session_id, long business_id, long programm_id, string programm_name, DateTime? date_beg, DateTime? date_end, string descr_short, string descr_full, long? card_type_group_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        DeleteResult DeleteProgramm(string session_id, long business_id, long programm_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Programm GetProgramm(string session_id, long business_id, long programm_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammList GetProgrammList(string session_id, long business_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammList GetProgrammList_forTemplate(string session_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammList GetProgrammList_byName(string session_id, long business_id, string programm_name, short? search_mode);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Programm CopyProgramm(string session_id, long business_id, long programm_id, string new_programm_name, long new_business_id);

        #endregion        

        #region ProgrammParam

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammParam InsertProgrammParam(string session_id, long business_id, long programm_id, int? param_num, int param_type
            , string string_value, long? int_value, decimal? float_value, DateTime? date_value, DateTime? date_only_value, TimeSpan? time_only_value
            , string param_name, short is_internal, short for_unit_pos);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammParam UpdateProgrammParam(string session_id, long business_id, long programm_id, long param_id, int? param_num, int param_type
            , string string_value, long? int_value, decimal? float_value, DateTime? date_value, DateTime? date_only_value, TimeSpan? time_only_value
            , string param_name, short is_internal, short for_unit_pos);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        DeleteResult DeleteProgrammParam(string session_id, long business_id, long programm_id, long param_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammParam GetProgrammParam(string session_id, long business_id, long programm_id, long param_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammParamList GetProgrammParamList_byProgramm(string session_id, long business_id, long programm_id);

        #endregion

        #region ProgrammStep

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStep InsertProgrammStep(string session_id, long business_id, long programm_id, int? step_num, string descr_short, string descr_full, short single_use_only);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStep UpdateProgrammStep(string session_id, long business_id, long programm_id, long step_id, int? step_num, string descr_short, string descr_full, short single_use_only);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        DeleteResult DeleteProgrammStep(string session_id, long business_id, long programm_id, long step_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStep GetProgrammStep(string session_id, long business_id, long programm_id, long step_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepList GetProgrammStepList_byProgramm(string session_id, long business_id, long programm_id);

        #endregion

        #region ProgrammStepParam

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepParam InsertProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, int? card_param_type, int? trans_param_type, long? programm_param_id, long? prev_step_param_id, int? param_num, string param_name, short is_programm_output);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepParam UpdateProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id, int? card_param_type, int? trans_param_type, long? programm_param_id, long? prev_step_param_id, int? param_num, string param_name, short is_programm_output);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        DeleteResult DeleteProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepParam GetProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepParamList GetProgrammStepParamList_byStep(string session_id, long business_id, long programm_id, long step_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepParamList GetProgrammStepParamList_byProgramm(string session_id, long business_id, long programm_id);

        #endregion

        #region ProgrammStepCondition
        
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepCondition InsertProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, int? condition_num, int? op_type, long? op1_param_id, long? op2_param_id, string condition_name);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepCondition UpdateProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id, int? condition_num, int? op_type, long? op1_param_id, long? op2_param_id, string condition_name);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        DeleteResult DeleteProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepCondition GetProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepConditionList GetProgrammStepConditionList_byStep(string session_id, long business_id, long programm_id, long step_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepConditionList GetProgrammStepConditionList_byProgramm(string session_id, long business_id, long programm_id);

        #endregion

        #region ProgrammStepLogic

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepLogic InsertProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, int? logic_num, int? op_type, long? op1_param_id, long? op2_param_id, long? op3_param_id, long? op4_param_id, long? condition_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepLogic UpdateProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id, int? logic_num, int? op_type, long? op1_param_id, long? op2_param_id, long? op3_param_id, long? op4_param_id, long? condition_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        DeleteResult DeleteProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepLogic GetProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepLogicList GetProgrammStepLogicList_byStep(string session_id, long business_id, long programm_id, long step_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepLogicList GetProgrammStepLogicList_byProgramm(string session_id, long business_id, long programm_id);

        #endregion

        #region CardTypeProgrammRel

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTypeProgrammRelList GetCardTypeProgrammList_byCardType(string session_id, long business_id, long card_type_id, DateTime? date_beg);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTypeProgrammRel InsertCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long programm_id, DateTime date_beg);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTypeProgrammRel UpdateCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long rel_id, DateTime date_beg);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        DeleteResult DeleteCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long rel_id);

        #endregion

        #region CardNominal

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardNominal GetCardNominal_byCard(string session_id, long business_id, long card_id, int unit_type);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardNominalList GetCardNominalList_byCard(string session_id, long business_id, long card_id);

        #endregion

        #region CalcCard
        
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CalcAbstractResult GetCalcAbstractResult();

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CalcStartResult Card_CalcStart(string session_id, long business_id, long card_num, List<CalcUnit> calc_unit_list, decimal bonus_for_pay, short is_scanned);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CalcStartResult Card_CalcStartTest(string session_id, long business_id, long card_num, List<CalcUnit> calc_unit_list, decimal bonus_for_pay, short is_scanned, short is_test, short do_commit);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CalcCommitResult Card_CalcCommit(string session_id, long business_id, long trans_id, CalcRound round_info, short is_delayed);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CalcCancelResult Card_CalcCancel(string session_id, long business_id, long trans_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CalcStartResult Card_RefundStart(string session_id, long business_id, long trans_id, List<CalcUnit> calc_unit_list);

        #endregion

        #region CardTransaction

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTransaction GetCardTransaction(string session_id, long business_id, long trans_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTransactionList GetCardTransactionList(string session_id, long business_id, long? card_num, string card_num2, short? trans_kind, int? status
            , DateTime? date_beg, DateTime? date_end, DateTime? date_check_beg, DateTime? date_check_end);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardTransactionDetailList GetCardTransactionDetailList(string session_id, long business_id, long? card_trans_id, DateTime? date_beg, DateTime? date_end
            , short? trans_kind, int? status);

        #endregion        

        #region ProgrammResult

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammResult GetProgrammResult(string session_id, long result_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammResultDetailList GetProgrammResultDetailList(string session_id, long result_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammStepResultList GetProgrammStepResultList(string session_id, long result_id);

        #endregion

        #region Log

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        LogSessionList GetLogSessionList(string session_id, string user_name, DateTime? date_beg, DateTime? date_end);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        LogEventList GetLogEventList(string session_id, DateTime? date_beg, DateTime? date_end, string user_name, int? log_event_type_id, long? business_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        LogEventTypeList GetLogEventTypeList(string session_id);

        #endregion

        #region Client

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ClientList GetClientList(string session_id, long business_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ClientList GetClientList_byCard(string session_id, long business_id, long card_id, DateTime date_beg);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ClientList GetClientList_byAttrs(string session_id, long business_id, string client_surname, long? card_num);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Client InsertClient(string session_id, long business_id, string client_surname, string client_name, string client_fname, DateTime? date_birth, string phone_num, string address, string email, string comment);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        Client UpdateClient(string session_id, long business_id, long client_id, string client_surname, string client_name, string client_fname, DateTime? date_birth, string phone_num, string address, string email, string comment);

        #endregion

        #region ClientCardRel

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ClientCardRelList GetClientCardList_byClient(string session_id, long business_id, long client_id, DateTime? date_beg);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ClientCardRel InsertClientCard(string session_id, long business_id, long client_id, long card_id, DateTime date_beg, DateTime? date_end);

        #endregion                

        #region CardImport

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Импорт дисконтных карт в БД сервиса")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект CardImportResult. Основные атрибуты:"
            + " Id - 0: операция завершена успешно, > 0: код ошибки"
            + " ,error - объект типа ErrInfo"
            + " ,ImportCardError_list - список номеров не созданных карт"
            )]
        CardImportResult CardImport([WsdlParamOrReturnDocumentation("Код сессии")]string session_id,
            [WsdlParamOrReturnDocumentation("Код организации")]long business_id,
            [WsdlParamOrReturnDocumentation("Код типа карт")]long card_type_id,
            [WsdlParamOrReturnDocumentation("Код статуса карт")]long card_status_id,
            [WsdlParamOrReturnDocumentation("Что делать с накопленными суммами по существующим картам:"
                + " 0 - оставлять, 1 - заменять, 2 - суммировать")]int action_for_existing_summ,
            [WsdlParamOrReturnDocumentation("Что делать с процентом скидки по существующим картам:"
                + " 0 - оставлять, 1 - заменять, 2 - суммировать")]int action_for_existing_disc_percent,
            [WsdlParamOrReturnDocumentation("Что делать с накопленными бонусами по существующим картам:"
                + " 0 - оставлять, 1 - заменять, 2 - суммировать")]int action_for_existing_bonus,
            [WsdlParamOrReturnDocumentation("Список строк из таблицы БД АУ DISCOUNT_CARD с импортируемыми картами")]ImportDiscountCardList discountCardList,
            [WsdlParamOrReturnDocumentation("Список строк из таблицы БД АУ DISCOUNT с импортируемыми картами")]ImportDiscountList discountList
            );

        /*
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CardImportResult CardImportUpdate(string session_id, long business_id, short update_card_summ, short update_card_client, ImportDiscountCardList discountCardList);
        */

        #endregion

        #region ProgrammTemplate

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammTemplate InsertProgrammTemplate(string session_id, string template_name, long programm_id, ProgrammTemplateParamList paramList);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammTemplate UpdateProgrammTemplate(string session_id, long template_id, string template_name, ProgrammTemplateParamList paramList);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        DeleteResult DeleteProgrammTemplate(string session_id, long template_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammTemplate GetProgrammTemplate(string session_id, long template_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammTemplateList GetProgrammTemplateList(string session_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ProgrammTemplateList GetProgrammTemplateList_byAttrs(string session_id, string template_name);

        #endregion        

        #region UserInfo

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        UserInfoList GetUserInfoList(string session_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        UserInfoList GetUserInfoList_byBusiness(string session_id, long business_id);

        #endregion

        #region BusinessSummary

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        BusinessSummary GetBusinessSummary(string session_id, long business_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        BusinessSummaryList GetBusinessSummaryList(string session_id);

        #endregion

        #region JSON Support

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        string TestJson(string session_id);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        string TestJson2();

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        decimal TestJson3(decimal param1);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        CalcStartResult TestJson4(string session_id, long business_id, long card_num, List<CalcUnit> calc_unit_list, decimal bonus_for_pay);

        [OperationContract]
        [WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
        void GetOptions();

        #endregion
    }

}