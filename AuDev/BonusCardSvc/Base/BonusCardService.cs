﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Diagnostics;
using System.Threading;
using AuDev.Common.Util;
#endregion

namespace BonusCardLib
{
    //[ServiceBehavior(Namespace = "http://service.aptekaural.ru/bonuscard/")]
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]    
    public partial class BonusCardService : IBonusCardService
    {        

        #region CreateErrBonusCardBase

        public T CreateErrBonusCardBase<T>(Exception e)
            where T:BonusCardBase
        {
            // !!!
            // todo: log
            T res = Activator.CreateInstance<T>();
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION
                , GlobalUtil.ExceptionInfo(e));
                //, e.Message.Trim() + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"));
            return res;
        }

        #endregion
        
        #region CreateErrCalcResult

        public CalcStartResult CreateErrCalcStartResult(List<CalcUnit> calc_unit, Exception e)
        {
            CalcStartResult res = new CalcStartResult();
            
            res.Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            /*
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim() 
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                )
                );
            */
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(e));
           
            res.trans_id = 0;

            res.calc_unit_result = null;
            /*
            res.calc_unit_result = new List<CalcUnitResult>();
            if (calc_unit != null)
            {
                foreach (var item in calc_unit)
                {
                    res.calc_unit_result.Add(new CalcUnitResult() { unit_result_num = item.unit_num });
                }
            }
            */
            return res;
        }

        public CalcStartResult CreateErrCalcStartResult(List<CalcUnit> calc_unit, ErrInfo err)
        {
            CalcStartResult res = new CalcStartResult();

            res.Id = err.Id.HasValue ? (long)err.Id : (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;
            res.error = err;
            res.trans_id = 0;
            res.calc_unit_result = null;
            /*
            res.calc_unit_result = new List<CalcUnitResult>();
            if (calc_unit != null)
            {
                foreach (var item in calc_unit)
                {
                    res.calc_unit_result.Add(new CalcUnitResult() { unit_result_num = item.unit_num });
                }
            }
            */
            return res;
            
        }

        public CalcCommitResult CreateErrCalcCommitResult(Exception e)
        {
            CalcCommitResult res = new CalcCommitResult();

            res.Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            /*
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim()
                    + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                    + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                    )
                    );
            */
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(e));            

            return res;
        }

        public CalcCommitResult CreateErrCalcCommitResult(ErrInfo err)
        {
            CalcCommitResult res = new CalcCommitResult();

            res.Id = err.Id.HasValue ? (long)err.Id : (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;
            res.error = err;

            return res;
        }

        public CalcCancelResult CreateErrCalcCancelResult(Exception e)
        {
            CalcCancelResult res = new CalcCancelResult();

            res.Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            //res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim() + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"));
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(e));

            return res;
        }

        public CalcCancelResult CreateErrCalcCancelResult(ErrInfo err)
        {
            CalcCancelResult res = new CalcCancelResult();

            res.Id = err.Id.HasValue ? (long)err.Id : (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;
            res.error = err;

            return res;
        }

        


        #endregion

        #region GetVersion

        public string GetVersion(string session_id)
        {
            try
            {
                return xGetVersion(session_id);
            }
            catch (Exception e)
            {
                return Enums.ErrCodeEnum.ERR_CODE_EXCEPTION.ToString();
            }
        }

        #endregion

        #region GetVersionDb

        public string GetVersionDb(string session_id)
        {
            try
            {
                return xGetVersionDb(session_id);
            }
            catch (Exception e)
            {
                return "";
            }
        }

        #endregion        

        #region Session

        public string StartSession(string user_name, string user_pwd, long? business_id)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xStartSession(user_name, user_pwd, business_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, "", business_id, user_name, null, (long)Enums.LogEventType.ERROR, null, date_beg, null, true);
                return "";
            }
        }

        public long CloseSession(string session_id)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xCloseSession(session_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, null, "", null, (long)Enums.LogEventType.ERROR, null, date_beg, null, true);
                return (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        public LogSession GetActiveSession(string session_id)
        {
            try
            {
                return xGetActiveSession(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<LogSession>(e);
            }
        }


        public LogSession GetSession(string session_id)
        {
            try
            {
                return xGetSession(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<LogSession>(e);
            }
        }


        #endregion

        #region Business

        public Business GetBusiness(string session_id, long business_id)
        {            
            try
            {
                return xGetBusiness(session_id, business_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<Business>(e);
            } 
        }

        public BusinessList GetBusinessList(string session_id)
        {
            try
            {
                return xGetBusinessList(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<BusinessList>(e);
            } 
        }

        public Business GetBusiness_byCard(string session_id, long card_id)
        {
            try
            {
                return xGetBusiness_byCard(session_id, card_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<Business>(e);
            } 
        }

        public BusinessList GetBusinessList_byAttrs(string session_id, string business_name)
        {
            try
            {
                return xGetBusinessList_byName(session_id, business_name, 0);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<BusinessList>(e);
            }
        }

        #endregion

        #region BusinessRel (old)
        
        public BusinessRelList GetBusinessRelList(string session_id)
        {
            try
            {
                return xGetBusinessRelList(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<BusinessRelList>(e);
            }
        }
        
        #endregion

        #region CardType

        public CardType GetCardType(string session_id, long business_id, long card_type_id)
        {
            try
            {
                return xGetCardType(session_id, business_id, card_type_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardType>(e);
            }
        }

        public CardType GetCardType_byCard(string session_id, long business_id, long card_id)
        {
            try
            {
                return xGetCardType_byCard(session_id, business_id, card_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardType>(e);
            }
        }

        public CardTypeList GetCardTypeList(string session_id, long business_id)
        {
            try
            {
                return xGetCardTypeList(session_id, business_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardTypeList>(e);
            }
        }

        public CardTypeList GetCardTypeList_byAttrs(string session_id, long business_id, string card_type_name)
        {
            try
            {
                return xGetCardTypeList_byAttrs(session_id, business_id, card_type_name);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardTypeList>(e);
            }
        }

        #endregion

        #region CardTypeUnit

        public CardTypeUnitList GetCardTypeUnitList_byCardType(string session_id, long business_id, long card_type_id)
        {
            try
            {
                return xGetCardTypeUnitList_byCardType(session_id, business_id, card_type_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardTypeUnitList>(e);
            }
        }

        #endregion

        #region CardStatusGroup
        
        public CardStatusGroupList GetCardStatusGroupList(string session_id)
        {
            try
            {
                return xGetCardStatusGroupList(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardStatusGroupList>(e);
            } 
        }

        #endregion

        #region CardStatus

        public CardStatus GetCardStatus(string session_id, long card_status_id)
        {
            try
            {
                return xGetCardStatus(session_id, card_status_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardStatus>(e);
            } 
        }

        public CardStatusList GetCardStatusList(string session_id)
        {
            try
            {
                return xGetCardStatusList(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardStatusList>(e);
            }  
        }

        public CardStatus GetCardStatus_byCard(string session_id, long card_id)
        {
            try
            {
                return xGetCardStatus_byCard(session_id, card_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardStatus>(e);
            } 
        }


        #endregion

        #region Card

        public Card GetCard(string session_id, long business_id, long card_id)
        {
            try
            {
                return xGetCard(session_id, business_id, card_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<Card>(e);
            } 
        }

        public CardList GetCardList(string session_id, long business_id)
        {
            try
            {
                return xGetCardList(session_id, business_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardList>(e);
            } 
        }

        public CardList GetCardList_byCardNum(string session_id, long business_id, long? card_num, string card_num2, short? search_mode, string mask)
        {
            try
            {
                return xGetCardList_byCardNum(session_id, business_id, card_num, card_num2, search_mode, mask, false, null);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardList>(e);
            }
        }

        public CardList GetCardList_byClient(string session_id, long business_id, string client_surname, string client_name, string client_fname, int? client_age)
        {
            try
            {
                return xGetCardList_byClient(session_id, business_id, client_surname, client_name, client_fname, client_age);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardList>(e);
            }
        }

        public CardList GetCardList_byAttrs(string session_id, long business_id
            , long? card_type_id, long? card_status_id, long? card_id, long? card_num, string card_num2, string client_name
            , DateTime? date_beg, DateTime? date_end, long? reestr_id
            , int? first_num, int? last_num, int? sort_field)
        {
            try
            {
                return xGetCardList_byAttrs(session_id, business_id
                    , card_type_id, card_status_id, card_id, card_num, card_num2, client_name
                    , date_beg, date_end, reestr_id
                    , first_num, last_num, sort_field);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardList>(e);
            }
        }

        public long IsCardActive(string session_id, long business_id, long card_id)
        {
            try
            {
                return xIsCardActive(session_id, business_id, card_id);
            }
            catch (Exception e)
            {
                return (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        public Card InsertCard(string session_id, long business_id, DateTime? date_beg, DateTime? date_end, long? curr_card_type_id, long? curr_card_status_id
            , long? card_num, string card_num2, string mess, long? source_card_id, List<string> card_nums, string app_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xInsertCard(session_id, business_id, date_beg, date_end, curr_card_type_id, curr_card_status_id, card_num, card_num2, mess, source_card_id, card_nums, app_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, null, log_date_beg, null, true);
                return CreateErrBonusCardBase<Card>(e);
            }
        }

        public decimal GetCardValueForPay(string session_id, long business_id, long card_num)
        {
            try
            {
                return xGetCardValueForPay(session_id, business_id, card_num);
            }
            catch (Exception e)
            {                
                return (decimal)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        public Card MergeCard(string session_id, long business_id, string card_num, long new_card_num, string new_card_num2, int transfer_nominal_mode, string app_id)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xMergeCard(session_id, business_id, card_num, new_card_num, new_card_num2, transfer_nominal_mode, app_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, null, date_beg, null, true);
                return CreateErrBonusCardBase<Card>(e);
            } 
        }

        #endregion

        #region CardCardStatusRel

        public CardCardStatusRel InsertCardStatus_byCard(string session_id, long business_id, long card_id, long card_status_id, DateTime date_beg, bool from_batch)
        {
            try
            {
                return xInsertCardStatus_byCard(session_id, business_id, card_id, card_status_id, date_beg, from_batch);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardCardStatusRel>(e);
            } 
        }
        
        public CardCardStatusRelList GetCardStatusList_byCard(string session_id, long business_id, long card_id, DateTime? date_beg)
        {            
            try
            {
                return xGetCardStatusList_byCard(session_id, business_id, card_id, date_beg);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardCardStatusRelList>(e);
            } 
        }
       
        #endregion

        #region CardCardTypeRel
        
        public CardCardTypeRelList GetCardTypeList_byCard(string session_id, long business_id, long card_id, DateTime? date_beg)
        {
            try
            {
                return xGetCardTypeList_byCard(session_id, business_id, card_id, date_beg);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardCardTypeRelList>(e);
            } 
        }
        
        #endregion

        #region Programm

        public Programm InsertProgramm(string session_id, long business_id, string programm_name, DateTime? date_beg, DateTime? date_end, string descr_short, string descr_full, long? card_type_group_id, ProgrammTemplate templ)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xInsertProgramm(session_id, business_id, programm_name, date_beg, date_end, descr_short, descr_full, card_type_group_id, templ);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, null, log_date_beg, null, true);
                return CreateErrBonusCardBase<Programm>(e);
            }
        }

        public Programm UpdateProgramm(string session_id, long business_id, long programm_id, string programm_name, DateTime? date_beg, DateTime? date_end, string descr_short, string descr_full, long? card_type_group_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xUpdateProgramm(session_id, business_id, programm_id, programm_name, date_beg, date_end, descr_short, descr_full, card_type_group_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, programm_id, log_date_beg, null, true);
                return CreateErrBonusCardBase<Programm>(e);
            }
        }

        public DeleteResult DeleteProgramm(string session_id, long business_id, long programm_id)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xDeleteProgramm(session_id, business_id, programm_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, programm_id, date_beg, null, true);
                return new DeleteResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        public Programm GetProgramm(string session_id, long business_id, long programm_id)
        {
            try
            {
                return xGetProgramm(session_id, business_id, programm_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<Programm>(e);
            }
        }

        public ProgrammList GetProgrammList(string session_id, long business_id)
        {
            try
            {
                return xGetProgrammList(session_id, business_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammList>(e);
            }
        }

        public ProgrammList GetProgrammList_byName(string session_id, long business_id, string programm_name, short? search_mode)
        {
            try
            {
                return xGetProgrammList_byName(session_id, business_id, programm_name, search_mode);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammList>(e);
            }
        }

        public ProgrammList GetProgrammList_forTemplate(string session_id)
        {
            try
            {
                return xGetProgrammList_forTemplate(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammList>(e);
            }
        }

        public Programm CopyProgramm(string session_id, long business_id, long programm_id, string new_programm_name, long new_business_id)
        {
            try
            {
                return xCopyProgramm(session_id, business_id, programm_id, new_programm_name, new_business_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<Programm>(e);
            }
        }

        #endregion

        #region ProgrammParam

        public ProgrammParam InsertProgrammParam(string session_id, long business_id, long programm_id, int? param_num, int param_type
            , string string_value, long? int_value, decimal? float_value, DateTime? date_value, DateTime? date_only_value, TimeSpan? time_only_value
            , string param_name, short is_internal, short for_unit_pos)
        {
            try
            {
                return xInsertProgrammParam(session_id, business_id, programm_id, param_num, param_type
                    , string_value, int_value, float_value, date_value, date_only_value, time_only_value
                    , param_name, is_internal, for_unit_pos);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammParam>(e);
            }
        }

        public ProgrammParam UpdateProgrammParam(string session_id, long business_id, long programm_id, long param_id, int? param_num, int param_type
            , string string_value, long? int_value, decimal? float_value, DateTime? date_value, DateTime? date_only_value, TimeSpan? time_only_value
            , string param_name, short is_internal, short for_unit_pos)
        {
            try
            {
                return xUpdateProgrammParam(session_id, business_id, programm_id, param_id, param_num, param_type
                    , string_value, int_value, float_value, date_value, date_only_value, time_only_value
                    , param_name, is_internal, for_unit_pos);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammParam>(e);
            }
        }

        public DeleteResult DeleteProgrammParam(string session_id, long business_id, long programm_id, long param_id)
        {
            try
            {
                return xDeleteProgrammParam(session_id, business_id, programm_id, param_id);
            }
            catch (Exception e)
            {
                return new DeleteResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        public ProgrammParam GetProgrammParam(string session_id, long business_id, long programm_id, long param_id)
        {
            try
            {
                return xGetProgrammParam(session_id, business_id, programm_id, param_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammParam>(e);
            }
        }

        public ProgrammParamList GetProgrammParamList_byProgramm(string session_id, long business_id, long programm_id)
        {
            try
            {
                return xGetProgrammParamList_byProgramm(session_id, business_id, programm_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammParamList>(e);
            }
        }

        #endregion

        #region ProgrammStep

        public ProgrammStep InsertProgrammStep(string session_id, long business_id, long programm_id, int? step_num, string descr_short, string descr_full, short single_use_only)
        {
            try
            {
                return xInsertProgrammStep(session_id, business_id, programm_id, step_num, descr_short, descr_full, single_use_only);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStep>(e);
            }
        }

        public ProgrammStep UpdateProgrammStep(string session_id, long business_id, long programm_id, long step_id, int? step_num, string descr_short, string descr_full, short single_use_only)
        {
            try
            {
                return xUpdateProgrammStep(session_id, business_id, programm_id, step_id, step_num, descr_short, descr_full, single_use_only);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStep>(e);
            }
        }

        public DeleteResult DeleteProgrammStep(string session_id, long business_id, long programm_id, long step_id)
        {
            try
            {
                return xDeleteProgrammStep(session_id, business_id, programm_id, step_id);
            }
            catch (Exception e)
            {
                return new DeleteResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        public ProgrammStep GetProgrammStep(string session_id, long business_id, long programm_id, long step_id)
        {
            try
            {
                return xGetProgrammStep(session_id, business_id, programm_id, step_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStep>(e);
            }
        }

        public ProgrammStepList GetProgrammStepList_byProgramm(string session_id, long business_id, long programm_id)
        {
            try
            {
                return xGetProgrammStepList_byProgramm(session_id, business_id, programm_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepList>(e);
            }
        }

        #endregion

        #region ProgrammStepParam

        public ProgrammStepParam InsertProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, int? card_param_type, int? trans_param_type, long? programm_param_id, long? prev_step_param_id, int? param_num, string param_name, short is_programm_output)
        {
            try
            {
                return xInsertProgrammStepParam(session_id, business_id, programm_id, step_id, card_param_type, trans_param_type, programm_param_id, prev_step_param_id, param_num, param_name, is_programm_output);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepParam>(e);
            }
        }

        public ProgrammStepParam UpdateProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id, int? card_param_type, int? trans_param_type, long? programm_param_id, long? prev_step_param_id, int? param_num, string param_name, short is_programm_output)
        {
            try
            {
                return xUpdateProgrammStepParam(session_id, business_id, programm_id, step_id, param_id, card_param_type, trans_param_type, programm_param_id, prev_step_param_id, param_num, param_name, is_programm_output);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepParam>(e);
            }
        }

        public DeleteResult DeleteProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id)
        {
            try
            {
                return xDeleteProgrammStepParam(session_id, business_id, programm_id, step_id, param_id);
            }
            catch (Exception e)
            {
                return new DeleteResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        public ProgrammStepParam GetProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id)
        {
            try
            {
                return xGetProgrammStepParam(session_id, business_id, programm_id, step_id, param_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepParam>(e);
            }
        }

        public ProgrammStepParamList GetProgrammStepParamList_byStep(string session_id, long business_id, long programm_id, long step_id)
        {
            try
            {
                return xGetProgrammStepParamList_byStep(session_id, business_id, programm_id, step_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepParamList>(e);
            }
        }

        public ProgrammStepParamList GetProgrammStepParamList_byProgramm(string session_id, long business_id, long programm_id)
        {
            try
            {
                return xGetProgrammStepParamList_byProgramm(session_id, business_id, programm_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepParamList>(e);
            }
        }

        #endregion

        #region ProgrammStepCondition

        public ProgrammStepCondition InsertProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, int? condition_num, int? op_type, long? op1_param_id, long? op2_param_id, string condition_name)
        {
            try
            {
                return xInsertProgrammStepCondition(session_id, business_id, programm_id, step_id, condition_num, op_type, op1_param_id, op2_param_id, condition_name);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepCondition>(e);
            }
        }

        public ProgrammStepCondition UpdateProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id, int? condition_num, int? op_type, long? op1_param_id, long? op2_param_id, string condition_name)
        {
            try
            {
                return xUpdateProgrammStepCondition(session_id, business_id, programm_id, step_id, condition_id, condition_num, op_type, op1_param_id, op2_param_id, condition_name);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepCondition>(e);
            }
        }

        public DeleteResult DeleteProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id)
        {
            try
            {
                return xDeleteProgrammStepCondition(session_id, business_id, programm_id, step_id, condition_id);
            }
            catch (Exception e)
            {
                return new DeleteResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        public ProgrammStepCondition GetProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id)
        {
            try
            {
                return xGetProgrammStepCondition(session_id, business_id, programm_id, step_id, condition_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepCondition>(e);
            }
        }

        public ProgrammStepConditionList GetProgrammStepConditionList_byStep(string session_id, long business_id, long programm_id, long step_id)
        {
            try
            {
                return xGetProgrammStepConditionList_byStep(session_id, business_id, programm_id, step_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepConditionList>(e);
            }
        }

        public ProgrammStepConditionList GetProgrammStepConditionList_byProgramm(string session_id, long business_id, long programm_id)
        {
            try
            {
                return xGetProgrammStepConditionList_byProgramm(session_id, business_id, programm_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepConditionList>(e);
            }
        }

        #endregion

        #region ProgrammStepLogic

        public ProgrammStepLogic InsertProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, int? logic_num, int? op_type, long? op1_param_id, long? op2_param_id, long? op3_param_id, long? op4_param_id, long? condition_id)
        {
            try
            {
                return xInsertProgrammStepLogic(session_id, business_id, programm_id, step_id, logic_num, op_type, op1_param_id, op2_param_id, op3_param_id, op4_param_id, condition_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepLogic>(e);
            }
        }

        public ProgrammStepLogic UpdateProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id, int? logic_num, int? op_type, long? op1_param_id, long? op2_param_id, long? op3_param_id, long? op4_param_id, long? condition_id)
        {
            try
            {
                return xUpdateProgrammStepLogic(session_id, business_id, programm_id, step_id, logic_id, logic_num, op_type, op1_param_id, op2_param_id, op3_param_id, op4_param_id, condition_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepLogic>(e);
            }
        }

        public DeleteResult DeleteProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id)
        {
            try
            {
                return xDeleteProgrammStepLogic(session_id, business_id, programm_id, step_id, logic_id);
            }
            catch (Exception e)
            {
                return new DeleteResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        public ProgrammStepLogic GetProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id)
        {
            try
            {
                return xGetProgrammStepLogic(session_id, business_id, programm_id, step_id, logic_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepLogic>(e);
            }
        }

        public ProgrammStepLogicList GetProgrammStepLogicList_byStep(string session_id, long business_id, long programm_id, long step_id)
        {
            try
            {
                return xGetProgrammStepLogicList_byStep(session_id, business_id, programm_id, step_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepLogicList>(e);
            }
        }

        public ProgrammStepLogicList GetProgrammStepLogicList_byProgramm(string session_id, long business_id, long programm_id)
        {
            try
            {
                return xGetProgrammStepLogicList_byProgramm(session_id, business_id, programm_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepLogicList>(e);
            }
        }

        #endregion

        #region CardTypeProgrammRel

        public CardTypeProgrammRelList GetCardTypeProgrammList_byCardType(string session_id, long business_id, long card_type_id, DateTime? date_beg)
        {
            try
            {
                return xGetCardTypeProgrammList_byCardType(session_id, business_id, card_type_id, date_beg);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardTypeProgrammRelList>(e);
            }
        }

        public CardTypeProgrammRel InsertCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long programm_id, DateTime date_beg)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xInsertCardTypeProgramm_byCardType(session_id, business_id, card_type_id, programm_id, date_beg);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, card_type_id, log_date_beg, null, true);
                return CreateErrBonusCardBase<CardTypeProgrammRel>(e);
            }
        }

        public CardTypeProgrammRel UpdateCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long rel_id, DateTime date_beg)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xUpdateCardTypeProgramm_byCardType(session_id, business_id, card_type_id, rel_id, date_beg);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, card_type_id, log_date_beg, null, true);
                return CreateErrBonusCardBase<CardTypeProgrammRel>(e);
            }
        }

        public DeleteResult DeleteCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long rel_id)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xDeleteCardTypeProgramm_byCardType(session_id, business_id, card_type_id, rel_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, card_type_id, date_beg, null, true);
                return new DeleteResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        #endregion

        #region CardNominal

        public CardNominal GetCardNominal_byCard(string session_id, long business_id, long card_id, int unit_type)
        {
            try
            {
                return xGetCardNominal_byCard(session_id, business_id, card_id, unit_type);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardNominal>(e);
            }
        }

        public CardNominalList GetCardNominalList_byCard(string session_id, long business_id, long card_id)
        {
            try
            {
                return xGetCardNominalList_byCard(session_id, business_id, card_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardNominalList>(e);
            }
        }

        #endregion

        #region CalcCard
        
        public CalcAbstractResult GetCalcAbstractResult()
        {
            return new CalcAbstractResult();
        }

        public CalcStartResult Card_CalcStart(string session_id, long business_id, long card_num, List<CalcUnit> calc_unit_list, decimal bonus_for_pay, short is_scanned)
        {
            // try-catch перенессен на уровень ниже
            return xCard_CalcStart(session_id, business_id, card_num, calc_unit_list, bonus_for_pay, is_scanned, false, false);
        }
        
        public CalcStartResult Card_CalcStartTest(string session_id, long business_id, long card_num, List<CalcUnit> calc_unit_list, decimal bonus_for_pay, short is_scanned, short is_test, short do_commit)
        {
            short is_test_actual = is_test;
            if (do_commit == 1)
                is_test_actual = 0;
            return xCard_CalcStart(session_id, business_id, card_num, calc_unit_list, bonus_for_pay, is_scanned, is_test_actual == 1, do_commit == 1);
        }

        public CalcCommitResult Card_CalcCommit(string session_id, long business_id, long trans_id, CalcRound round_info, short is_delayed)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xCard_CalcCommit(session_id, business_id, trans_id, round_info, is_delayed);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, trans_id, date_beg, null, true);
                return CreateErrCalcCommitResult(e);
            }
        }

        public CalcCancelResult Card_CalcCancel(string session_id, long business_id, long trans_id)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xCard_CalcCancel(session_id, business_id, trans_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, trans_id, date_beg, null, true);
                return CreateErrCalcCancelResult(e);
            }
        }

        public CalcStartResult Card_RefundStart(string session_id, long business_id, long trans_id, List<CalcUnit> calc_unit_list)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xCard_RefundStart(session_id, business_id, trans_id, calc_unit_list);
            }
            catch (Exception e)
            {
                //return CreateErrCalcStartResult(calc_unit_list, e);
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, trans_id, date_beg, null, true);
                return CreateErrCalcStartResult(null, e);
            }
        }

        #endregion

        #region CardTransaction

        public CardTransaction GetCardTransaction(string session_id, long business_id, long trans_id)
        {
            try
            {
                return xGetCardTransaction(session_id, business_id, trans_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardTransaction>(e);
            }
        }

        public CardTransactionList GetCardTransactionList(string session_id, long business_id, long? card_num, string card_num2, short? trans_kind, int? status
            , DateTime? date_beg, DateTime? date_end, DateTime? date_check_beg, DateTime? date_check_end)
        {
            try
            {
                return xGetCardTransactionList(session_id, business_id, card_num, card_num2, trans_kind, status
                    , date_beg, date_end, date_check_beg, date_check_end);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardTransactionList>(e);
            }
        }

        public CardTransactionDetailList GetCardTransactionDetailList(string session_id, long business_id, long? card_trans_id
            , DateTime? date_beg, DateTime? date_end, short? trans_kind, int? status)
        {
            try
            {
                return xGetCardTransactionDetailList(session_id, business_id, card_trans_id, date_beg, date_end
                    , trans_kind, status);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<CardTransactionDetailList>(e);
            }
        }

        #endregion        

        #region ProgrammResult
        
        public ProgrammResult GetProgrammResult(string session_id, long result_id)
        {
            try
            {
                return xGetProgrammResult(session_id, result_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammResult>(e);
            }
        }

        public ProgrammResultDetailList GetProgrammResultDetailList(string session_id, long result_id)
        {
            try
            {
                return xGetProgrammResultDetailList(session_id, result_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammResultDetailList>(e);
            }
        }

        public ProgrammStepResultList GetProgrammStepResultList(string session_id, long result_id)
        {
            try
            {
                return xGetProgrammStepResultList(session_id, result_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammStepResultList>(e);
            }
        }

        #endregion

        #region Log

        public LogSessionList GetLogSessionList(string session_id, string user_name, DateTime? date_beg, DateTime? date_end)
        {
            try
            {
                return xGetLogSessionList(session_id, user_name, date_beg, date_end);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<LogSessionList>(e);
            }
        }

        public LogEventList GetLogEventList(string session_id, DateTime? date_beg, DateTime? date_end, string user_name, int? log_event_type_id, long? business_id)
        {
            try
            {
                return xGetLogEventList(session_id, date_beg, date_end, user_name, log_event_type_id, business_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<LogEventList>(e);
            }
        }

        public LogEventTypeList GetLogEventTypeList(string session_id)
        {
            try
            {
                return xGetLogEventTypeList(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<LogEventTypeList>(e);
            }
        }

        #endregion

        #region Client

        public ClientList GetClientList(string session_id, long business_id)
        {
            try
            {
                return xGetClientList(session_id, business_id);
            }
            catch (Exception e)
            {                
                return CreateErrBonusCardBase<ClientList>(e);
            }
        }

        public ClientList GetClientList_byCard(string session_id, long business_id, long card_id, DateTime date_beg)
        {
            try
            {
                return xGetClientList_byCard(session_id, business_id, card_id, date_beg);
            }
            catch (Exception e)
            {                
                return CreateErrBonusCardBase<ClientList>(e);
            }
        }

        public ClientList GetClientList_byAttrs(string session_id, long business_id, string client_surname, long? card_num)
        {
            try
            {
                return xGetClientList_byAttrs(session_id, business_id, client_surname, card_num);
            }
            catch (Exception e)
            {                
                return CreateErrBonusCardBase<ClientList>(e);
            }
        }

        public Client InsertClient(string session_id, long business_id, string client_surname, string client_name, string client_fname, DateTime? date_birth, string phone_num, string address, string email, string comment)
        {
            DateTime now = DateTime.Now;
            try
            {
                return xInsertClient(session_id, business_id, client_surname, client_name, client_fname, date_birth, phone_num, address, email, comment);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, null, now, null, true);
                return CreateErrBonusCardBase<Client>(e);
            }
        }

        public Client UpdateClient(string session_id, long business_id, long client_id, string client_surname, string client_name, string client_fname, DateTime? date_birth, string phone_num, string address, string email, string comment)
        {
            DateTime now = DateTime.Now;
            try
            {
                return xUpdateClient(session_id, business_id, client_id, client_surname, client_name, client_fname, date_birth, phone_num, address, email, comment);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, client_id, now, null, true);
                return CreateErrBonusCardBase<Client>(e);
            }
        }


        #endregion

        #region ClientCardRel

        public ClientCardRelList GetClientCardList_byClient(string session_id, long business_id, long client_id, DateTime? date_beg)
        {
            try
            {
                return xGetClientCardList_byClient(session_id, business_id, client_id, date_beg);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ClientCardRelList>(e);
            }
        }

        public ClientCardRel InsertClientCard(string session_id, long business_id, long client_id, long card_id, DateTime date_beg, DateTime? date_end)
        {
            try
            {
                return xInsertClientCard(session_id, business_id, client_id, card_id, date_beg, date_end);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ClientCardRel>(e);
            }
        }

        #endregion

        #region CardImport

        public CardImportResult CardImport(string session_id, long business_id, long card_type_id, long card_status_id,
            int action_for_existing_summ, int action_for_existing_disc_percent, int action_for_existing_bonus,
            ImportDiscountCardList discountCardList, ImportDiscountList discountList)
        {                
            DateTime date_beg = DateTime.Now;
            try
            {
                return xCardImport(session_id, business_id, card_type_id, card_status_id, action_for_existing_summ, action_for_existing_disc_percent, action_for_existing_bonus, discountCardList, discountList);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, null, date_beg, null, true);
                return new CardImportResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        /*
        public CardImportResult CardImportUpdate(string session_id, long business_id, short update_card_summ, short update_card_client, ImportDiscountCardList discountCardList)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xCardImportUpdate(session_id, business_id, update_card_summ, update_card_client, discountCardList);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, business_id, "", null, (long)Enums.LogEventType.ERROR, null, date_beg, null, true);
                return new CardImportResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }
        */

        #endregion

        #region ProgrammTemplate

        public ProgrammTemplate InsertProgrammTemplate(string session_id, string template_name, long programm_id, ProgrammTemplateParamList paramList)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xInsertProgrammTemplate(session_id, template_name, programm_id, paramList);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, null, "", null, (long)Enums.LogEventType.ERROR, null, date_beg, null, true);
                return CreateErrBonusCardBase<ProgrammTemplate>(e);
            }
        }

        public ProgrammTemplate UpdateProgrammTemplate(string session_id, long template_id, string template_name, ProgrammTemplateParamList paramList)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xUpdateProgrammTemplate(session_id, template_id, template_name, paramList);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, null, "", null, (long)Enums.LogEventType.ERROR, template_id, date_beg, null, true);
                return CreateErrBonusCardBase<ProgrammTemplate>(e);
            }
        }
        
        public DeleteResult DeleteProgrammTemplate(string session_id, long template_id)
        {
            DateTime date_beg = DateTime.Now;
            try
            {
                return xDeleteProgrammTemplate(session_id, template_id);
            }
            catch (Exception e)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, e, session_id, null, "", null, (long)Enums.LogEventType.ERROR, template_id, date_beg, null, true);
                return new DeleteResult((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e);
            }
        }

        public ProgrammTemplate GetProgrammTemplate(string session_id, long template_id)
        {
            try
            {
                return xGetProgrammTemplate(session_id, template_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammTemplate>(e);
            }
        }

        public ProgrammTemplateList GetProgrammTemplateList(string session_id)
        {
            try
            {
                return xGetProgrammTemplateList(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammTemplateList>(e);
            }
        }

        public ProgrammTemplateList GetProgrammTemplateList_byAttrs(string session_id, string template_name)
        {
            try
            {
                return xGetProgrammTemplateList_byAttrs(session_id, template_name);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<ProgrammTemplateList>(e);
            }
        }
        

        #endregion        

        #region UserInfo
       
        public UserInfoList GetUserInfoList(string session_id)
        {
            try
            {
                return xGetUserInfoList(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<UserInfoList>(e);
            }
        }

        public UserInfoList GetUserInfoList_byBusiness(string session_id, long business_id)
        {
            try
            {
                return xGetUserInfoList_byBusiness(session_id, business_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<UserInfoList>(e);
            }
        }

        #endregion

        #region BusinessSummary

        public BusinessSummary GetBusinessSummary(string session_id, long business_id)
        {
            try
            {
                return xGetBusinessSummary(session_id, business_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<BusinessSummary>(e);
            }
        }

        public BusinessSummaryList GetBusinessSummaryList(string session_id)
        {
            try
            {
                return xGetBusinessSummaryList(session_id);
            }
            catch (Exception e)
            {
                return CreateErrBonusCardBase<BusinessSummaryList>(e);
            }
        }

        #endregion               

        #region JSON Support

        public string TestJson(string session_id)
        {
            if (String.IsNullOrEmpty(session_id))
                return "Your session_id is empty";
            else
                return "Your session_id is " + session_id.ToString();
            //return "TestJson reporting";
        }

        public string TestJson2()
        {
            return "TestJson2 reporting";
        }

        public decimal TestJson3(decimal param1)
        {
            decimal res = param1 + Convert.ToDecimal(5.5);
            return res;
        }

        public CalcStartResult TestJson4(string session_id, long business_id, long card_num, List<CalcUnit> calc_unit_list, decimal bonus_for_pay)
        {
            if ((calc_unit_list == null) || (calc_unit_list.Count <= 0))
            {
                return CreateErrCalcStartResult(calc_unit_list, new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Список товаров пуст"));
            }

            return new CalcStartResult() { calc_unit_result = new List<CalcUnit>(calc_unit_list), curr_trans_count = 1, curr_trans_sum = 111, error = null, Id = 0, trans_id = 2121, card_nominal = null, };
        }


        public void GetOptions()
        {
            // Заголовки обработаются в EnableCorsMessageInspector 
        }


        #endregion

    }
        
}
