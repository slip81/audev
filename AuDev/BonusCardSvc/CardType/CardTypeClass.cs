﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region CardType

    [DataContract]
    public class CardType : BonusCardBase
    {
        public CardType()
            : base()
        {
            //
        }

        public CardType(card_type card_type)
        {
            db_card_type = card_type;
            card_type_id = card_type.card_type_id;
            card_type_name = card_type.card_type_name;
            business_id = card_type.business_id;
            is_fin_spec = card_type.is_fin_spec;
            card_type_group_id = card_type.card_type_group_id;
            update_nominal = card_type.update_nominal;
            business = new Business(card_type.business);
        }

        [DataMember]
        public long card_type_id { get; set; }
        [DataMember]
        public string card_type_name { get; set; }
        [DataMember]
        public Nullable<long> business_id { get; set; }
        [DataMember]
        public short is_fin_spec { get; set; }
        [DataMember]
        public Nullable<long> card_type_group_id { get; set; }
        [DataMember]
        public short update_nominal { get; set; }
        [DataMember]
        public Business business { get; set; }

        public card_type Db_card_type { get { return db_card_type; } }
        private card_type db_card_type;
    }

    [DataContract]
    public class CardTypeList : BonusCardBase
    {
        public CardTypeList()
            : base()
        {
            //
        }

        public CardTypeList(List<card_type> cardTypeList)
        {
            if (cardTypeList != null)
            {
                CardType_list = new List<CardType>();
                foreach (var item in cardTypeList)
                {
                    CardType_list.Add(new CardType(item));
                }
            }
        }

        [DataMember]
        public List<CardType> CardType_list;

    }

    #endregion

    #region CardTypeUnit

    [DataContract]
    public class CardTypeUnit : BonusCardBase
    {
        public CardTypeUnit()
            : base()
        {
            //
        }

        public CardTypeUnit(card_type_unit card_type_unit)
        {
            db_card_type_unit = card_type_unit;
            card_type_unit_id = card_type_unit.card_type_unit_id;
            card_type_id = card_type_unit.card_type_id;
            unit_type = card_type_unit.unit_type;
            is_allow_neg = card_type_unit.is_allow_neg;
            min_value = card_type_unit.min_value;
            max_value = card_type_unit.max_value;
            max_trans_count = card_type_unit.max_trans_count;
            grow_from_sale_mode = card_type_unit.grow_from_sale_mode;
            spend_to_sale_mode = card_type_unit.spend_to_sale_mode;
            card_type = new CardType(card_type_unit.card_type);
        }

        [DataMember]
        public long card_type_unit_id { get; set; }
        [DataMember]
        public long card_type_id { get; set; }
        [DataMember]
        public Nullable<int> unit_type { get; set; }
        [DataMember]
        public short is_allow_neg { get; set; }
        [DataMember]
        public Nullable<decimal> min_value { get; set; }
        [DataMember]
        public Nullable<decimal> max_value { get; set; }
        [DataMember]
        public Nullable<long> max_trans_count { get; set; }
        [DataMember]
        public Nullable<int> grow_from_sale_mode { get; set; }
        [DataMember]
        public Nullable<int> spend_to_sale_mode { get; set; }
        [DataMember]
        public CardType card_type { get; set; }

        public card_type_unit Db_card_type_unit { get { return db_card_type_unit; } }
        private card_type_unit db_card_type_unit;
    }

    [DataContract]
    public class CardTypeUnitList : BonusCardBase
    {
        public CardTypeUnitList()
            : base()
        {
            //
        }

        public CardTypeUnitList(List<card_type_unit> cardTypeUnitList)
        {
            if (cardTypeUnitList != null)
            {
                CardTypeUnit_list = new List<CardTypeUnit>();
                foreach (var item in cardTypeUnitList)
                {
                    CardTypeUnit_list.Add(new CardTypeUnit(item));
                }
            }
        }

        [DataMember]
        public List<CardTypeUnit> CardTypeUnit_list;

    }

    #endregion

    #region CheckPrintInfo

    [DataContract]
    public class CheckPrintInfo : BonusCardBase
    {
        public CheckPrintInfo()
        {
            //
        }

        public CheckPrintInfo(card_type_check_info item)
        {
            card_type_id = item.card_type_id;
            is_active = item.is_active;
            print_sum_trans = item.print_sum_trans;
            print_bonus_value_trans = item.print_bonus_value_trans;
            print_bonus_value_card = item.print_bonus_value_card;
            print_bonus_percent_card = item.print_bonus_percent_card;
            print_disc_percent_card = item.print_disc_percent_card;
            //
            print_inactive_bonus_value_card = item.print_inactive_bonus_value_card;
            print_used_bonus_value_trans = item.print_used_bonus_value_trans;
            print_programm_descr = item.print_programm_descr;
            print_card_num = item.print_card_num;
            print_cert_sum_card = item.print_cert_sum_card;
            print_cert_sum_trans = item.print_cert_sum_trans;
            print_cert_sum_left = item.print_cert_sum_left;
        }

        public long card_type_id { get; set; }
        [DataMember]
        public bool is_active { get; set; }
        [DataMember]
        public bool print_sum_trans { get; set; }
        [DataMember]
        public bool print_bonus_value_trans { get; set; }
        [DataMember]
        public bool print_bonus_value_card { get; set; }
        [DataMember]
        public bool print_bonus_percent_card { get; set; }
        [DataMember]
        public bool print_disc_percent_card { get; set; }
        // new
        [DataMember]
        public bool print_inactive_bonus_value_card { get; set; }
        [DataMember]
        public bool print_used_bonus_value_trans { get; set; }
        [DataMember]
        public bool print_programm_descr { get; set; }
        [DataMember]
        public bool print_card_num { get; set; }
        [DataMember]
        public bool print_cert_sum_card { get; set; }
        [DataMember]
        public bool print_cert_sum_trans { get; set; }
        [DataMember]
        public bool print_cert_sum_left { get; set; }
    }

    #endregion
}
