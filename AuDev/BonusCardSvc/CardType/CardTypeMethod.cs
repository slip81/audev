﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region CardType

        private CardType xGetCardType(string session_id, long business_id, long card_type_id)
        {
            if (!IsValidSession(session_id))
                return new CardType() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card_type card_type = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardType() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                card_type = (from ct in dbContext.card_type
                             where ct.business_id == business_id
                             && ct.card_type_id == card_type_id
                             select ct)
                           .FirstOrDefault();

                if (card_type == null)
                {
                    return new CardType() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardType(card_type);
            }
        }

        private CardType xGetCardType_byCard(string session_id, long business_id, long card_id)
        {
            if (!IsValidSession(session_id))
                return new CardType() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card_type card_type = null;
            card card = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardType() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                card = dbContext.card.Where(ss => ss.card_id == card_id).FirstOrDefault();
                if (card == null)
                    return new CardType() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };
                if (card.curr_card_type_id.HasValue)
                {
                    card_type = (from ct in dbContext.card_type
                                 where ct.business_id == business_id
                                 && ct.card_type_id == card.curr_card_type_id
                                 select ct)
                               .FirstOrDefault();
                }

                if (card_type == null)
                {
                    return new CardType() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardType(card_type);
            }
        }

        private CardTypeList xGetCardTypeList(string session_id, long business_id)
        {
            if (!IsValidSession(session_id))
                return new CardTypeList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_type> card_type_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardTypeList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                card_type_list = (from ct in dbContext.card_type
                                  where ct.business_id == business_id
                                  select ct)
                           .OrderBy(ss => ss.business_id)
                           .ThenBy(ss => ss.card_type_id)
                           .ToList()
                           ;
                if (card_type_list == null)
                {
                    return new CardTypeList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardTypeList(card_type_list);
            }
        }

        private CardTypeList xGetCardTypeList_byAttrs(string session_id, long business_id, string card_type_name)
        {
            if (!IsValidSession(session_id))
                return new CardTypeList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_type> card_type_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardTypeList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                card_type_list = (from ct in dbContext.card_type
                                  where ct.business_id == business_id
                                  && ((String.IsNullOrEmpty(card_type_name)) || ((!String.IsNullOrEmpty(card_type_name)) && (ct.card_type_name.Trim().ToLower().Contains(card_type_name.Trim().ToLower()))))
                                  select ct)
                           .OrderBy(ss => ss.business_id)
                           .ThenBy(ss => ss.card_type_id)
                           .ToList()
                           ;
                if ((card_type_list == null) || (card_type_list.Count <= 0))
                {
                    return new CardTypeList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Типы карт не найдены") };
                }

                return new CardTypeList(card_type_list);
            }
        }

        #endregion

        #region CardTypeUnit

        private CardTypeUnitList xGetCardTypeUnitList_byCardType(string session_id, long business_id, long card_type_id)
        {
            if (!IsValidSession(session_id))
                return new CardTypeUnitList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_type_unit> card_type_unit_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardTypeUnitList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (dbContext.card_type.Where(ss => ss.card_type_id == card_type_id).Count() <= 0)
                    return new CardTypeUnitList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден тип карты с кодом " + card_type_id.ToString()) };

                card_type_unit_list = (from ct in dbContext.card_type                           
                           from ctu in dbContext.card_type_unit
                           where ct.business_id == business_id
                           && ct.card_type_id == ctu.card_type_id                           
                           && ct.card_type_id == card_type_id
                           select ctu)
                           .OrderBy(ss => ss.card_type_id)
                           .ThenBy(ss => ss.unit_type)
                           .ToList()
                           ;
 
                if (card_type_unit_list == null)
                {
                    return new CardTypeUnitList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardTypeUnitList(card_type_unit_list);
            }            
        }

        #endregion

    }
}
