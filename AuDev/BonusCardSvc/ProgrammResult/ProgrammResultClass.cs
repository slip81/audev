﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region ProgrammResult

    [DataContract]
    public class ProgrammResult : BonusCardBase
    {

        public ProgrammResult()
            : base()
        {
            //
        }

        public ProgrammResult(programm_result programm_result)
        {
            db_programm_result = programm_result;
            result_id = programm_result.result_id;
            programm_id = programm_result.programm_id;
            date_beg = programm_result.date_beg;
            is_test = programm_result.is_test;
        }

        [DataMember]
        public long result_id { get; set; }
        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public short is_test { get; set; }
        // !!!
        /*
        [DataMember]
        public List<programm_result_detail> programm_result_detail { get; set; }
        [DataMember]
        public List<programm_step_result> programm_step_result { get; set; }
        */

        public programm_result Db_programm_result { get { return db_programm_result; } }
        private programm_result db_programm_result;
    }

    [DataContract]
    public class ProgrammResultList : BonusCardBase
    {
        public ProgrammResultList()
            : base()
        {
            //
        }

        public ProgrammResultList(List<programm_result> programmResultList)
        {
            if (programmResultList != null)
            {
                ProgrammResult_list = new List<ProgrammResult>();
                foreach (var item in programmResultList)
                {
                    ProgrammResult_list.Add(new ProgrammResult(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammResult> ProgrammResult_list;

    }

    #endregion

    #region ProgrammResultDetail

    [DataContract]
    public class ProgrammResultDetail : BonusCardBase
    {

        public ProgrammResultDetail()
            : base()
        {
            //
        }

        public ProgrammResultDetail(programm_result_detail programm_result_detail)
        {
            db_programm_result_detail = programm_result_detail;
            detail_id = programm_result_detail.detail_id;
            programm_id = programm_result_detail.programm_id;
            unit_num = programm_result_detail.unit_num;
            unit_name = programm_result_detail.unit_name;
            unit_value = programm_result_detail.unit_value;
            unit_value_discount = programm_result_detail.unit_value_discount;
            unit_value_with_discount = programm_result_detail.unit_value_with_discount;
            unit_percent_discount = programm_result_detail.unit_percent_discount;
            unit_value_bonus_for_card = programm_result_detail.unit_value_bonus_for_card;
            unit_value_bonus_for_pay = programm_result_detail.unit_value_bonus_for_pay;
            unit_percent_bonus_for_card = programm_result_detail.unit_percent_bonus_for_card;
            unit_percent_bonus_for_pay = programm_result_detail.unit_percent_bonus_for_pay;
            unit_max_discount_percent = programm_result_detail.unit_max_discount_percent;
            unit_count = programm_result_detail.unit_count;
            unit_type = programm_result_detail.unit_type;
            unit_max_bonus_for_card_percent = programm_result_detail.unit_max_bonus_for_card_percent;
            unit_max_bonus_for_pay_percent = programm_result_detail.unit_max_bonus_for_pay_percent;
            price_margin_percent = programm_result_detail.price_margin_percent;
            artikul = programm_result_detail.artikul;
            live_prep = programm_result_detail.live_prep;
            promo = programm_result_detail.promo;
            tax_percent = programm_result_detail.tax_percent;
        }

        [DataMember]
        public long detail_id { get; set; }
        [DataMember]
        public Nullable<long> result_id { get; set; }
        [DataMember]
        public Nullable<long> programm_id { get; set; }
        [DataMember]
        public Nullable<int> unit_num { get; set; }
        [DataMember]
        public string unit_name { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value_discount { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value_with_discount { get; set; }
        [DataMember]
        public Nullable<decimal> unit_percent_discount { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value_bonus_for_card { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value_bonus_for_pay { get; set; }
        [DataMember]
        public Nullable<decimal> unit_percent_bonus_for_card { get; set; }
        [DataMember]
        public Nullable<decimal> unit_percent_bonus_for_pay { get; set; }
        [DataMember]
        public Nullable<decimal> unit_max_discount_percent { get; set; }
        [DataMember]
        //public Nullable<int> unit_count { get; set; }
        public Nullable<decimal> unit_count { get; set; }
        [DataMember]
        public Nullable<int> unit_type { get; set; }
        [DataMember]
        public Nullable<decimal> unit_max_bonus_for_card_percent { get; set; }
        [DataMember]
        public Nullable<decimal> unit_max_bonus_for_pay_percent { get; set; }
        [DataMember]
        public Nullable<decimal> price_margin_percent { get; set; }
        [DataMember]
        public Nullable<int> artikul { get; set; }
        [DataMember]
        public Nullable<int> live_prep { get; set; }
        [DataMember]
        public Nullable<int> promo { get; set; }
        [DataMember]
        public Nullable<decimal> tax_percent { get; set; }

        public programm_result_detail Db_programm_result_detail { get { return db_programm_result_detail; } }
        private programm_result_detail db_programm_result_detail;
    }

    [DataContract]
    public class ProgrammResultDetailList : BonusCardBase
    {
        public ProgrammResultDetailList()
            : base()
        {
            //
        }

        public ProgrammResultDetailList(List<programm_result_detail> programmResultDetailList)
        {
            if (programmResultDetailList != null)
            {
                ProgrammResultDetail_list = new List<ProgrammResultDetail>();
                foreach (var item in programmResultDetailList)
                {
                    ProgrammResultDetail_list.Add(new ProgrammResultDetail(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammResultDetail> ProgrammResultDetail_list;

    }

    #endregion

    #region ProgrammStepResult

    [DataContract]
    public class ProgrammStepResult : BonusCardBase
    {

        public ProgrammStepResult()
            : base()
        {
            //
        }

        public ProgrammStepResult(programm_step_result programm_step_result)
        {
            db_programm_step_result = programm_step_result;

            step_result_id = programm_step_result.step_result_id;
            result_id = programm_step_result.result_id;
            programm_id = programm_step_result.programm_id;
            step_num = programm_step_result.step_num;
            position_num = programm_step_result.position_num;
            position_value = programm_step_result.position_value;
            position_name = programm_step_result.position_name;
            position_max_discount_percent = programm_step_result.position_max_discount_percent;
            condition_num = programm_step_result.condition_num;
            condition_name = programm_step_result.condition_name;
            condition_op1_param_value = programm_step_result.condition_op1_param_value;
            condition_op2_param_value = programm_step_result.condition_op2_param_value;
            condition_result = programm_step_result.condition_result;
            logic_num = programm_step_result.logic_num;
            logic_op_type = programm_step_result.logic_op_type;
            logic_op1_param_value = programm_step_result.logic_op1_param_value;
            logic_op1_param_name = programm_step_result.logic_op1_param_name;
            logic_op1_is_programm_output = programm_step_result.logic_op1_is_programm_output;
            logic_op2_param_value = programm_step_result.logic_op2_param_value;
            logic_op2_param_name = programm_step_result.logic_op2_param_name;
            logic_op2_is_programm_output = programm_step_result.logic_op2_is_programm_output;
            logic_op3_param_value = programm_step_result.logic_op3_param_value;
            logic_op3_param_name = programm_step_result.logic_op3_param_name;
            logic_op3_is_programm_output = programm_step_result.logic_op3_is_programm_output;
            logic_op4_param_value = programm_step_result.logic_op4_param_value;
            logic_op4_param_name = programm_step_result.logic_op4_param_name;
            logic_op4_is_programm_output = programm_step_result.logic_op4_is_programm_output;

        }

        [DataMember]
        public long step_result_id { get; set; }
        [DataMember]
        public long result_id { get; set; }
        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public Nullable<int> step_num { get; set; }
        [DataMember]
        public Nullable<int> position_num { get; set; }
        [DataMember]
        public Nullable<decimal> position_value { get; set; }
        [DataMember]
        public string position_name { get; set; }
        [DataMember]
        public Nullable<decimal> position_max_discount_percent { get; set; }
        [DataMember]
        public Nullable<int> condition_num { get; set; }
        [DataMember]
        public string condition_name { get; set; }
        [DataMember]
        public string condition_op1_param_value { get; set; }
        [DataMember]
        public Nullable<long> condition_op2_param_id { get; set; }
        [DataMember]
        public string condition_op2_param_value { get; set; }
        [DataMember]
        public Nullable<short> condition_result { get; set; }
        [DataMember]
        public Nullable<int> logic_num { get; set; }
        [DataMember]
        public Nullable<int> logic_op_type { get; set; }
        [DataMember]
        public string logic_op1_param_value { get; set; }
        [DataMember]
        public string logic_op1_param_name { get; set; }
        [DataMember]
        public Nullable<short> logic_op1_is_programm_output { get; set; }
        [DataMember]
        public string logic_op2_param_value { get; set; }
        [DataMember]
        public string logic_op2_param_name { get; set; }
        [DataMember]
        public Nullable<short> logic_op2_is_programm_output { get; set; }
        [DataMember]
        public string logic_op3_param_value { get; set; }
        [DataMember]
        public string logic_op3_param_name { get; set; }
        [DataMember]
        public Nullable<short> logic_op3_is_programm_output { get; set; }
        [DataMember]
        public string logic_op4_param_value { get; set; }
        [DataMember]
        public string logic_op4_param_name { get; set; }
        [DataMember]
        public Nullable<short> logic_op4_is_programm_output { get; set; }


        public programm_step_result Db_programm_step_result { get { return db_programm_step_result; } }
        private programm_step_result db_programm_step_result;
    }

    [DataContract]
    public class ProgrammStepResultList : BonusCardBase
    {
        public ProgrammStepResultList()
            : base()
        {
            //
        }

        public ProgrammStepResultList(List<programm_step_result> programmStepResultList)
        {
            if (programmStepResultList != null)
            {
                ProgrammStepResult_list = new List<ProgrammStepResult>();
                foreach (var item in programmStepResultList)
                {
                    ProgrammStepResult_list.Add(new ProgrammStepResult(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammStepResult> ProgrammStepResult_list;

    }

    #endregion
}
