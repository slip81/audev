﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region ProgrammResult

        private ProgrammResult xGetProgrammResult(string session_id, long result_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_result programm_result = null;

            using (AuMainDb dbContext = new AuMainDb())
            {
                programm_result = dbContext.programm_result.Where(ss => ss.result_id == result_id).FirstOrDefault();


                if (programm_result == null)
                {
                    return new ProgrammResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammResult(programm_result);
            }
        }

        private ProgrammResultDetailList xGetProgrammResultDetailList(string session_id, long result_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammResultDetailList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_result_detail> programm_result_detail_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                programm_result_detail_list = dbContext.programm_result_detail.Where(ss => ss.result_id == result_id).OrderBy(ss => ss.unit_num).ToList();

                if (programm_result_detail_list == null)
                {
                    return new ProgrammResultDetailList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammResultDetailList(programm_result_detail_list);
            }
            
        }

        private ProgrammStepResultList xGetProgrammStepResultList(string session_id, long result_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepResultList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_step_result> programm_step_result_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                programm_step_result_list = dbContext.programm_step_result.Where(ss => ss.result_id == result_id)
                    .OrderBy(ss => ss.step_num).ThenBy(ss => ss.logic_num).ThenBy(ss => ss.position_num).ToList();

                if (programm_step_result_list == null)
                {
                    return new ProgrammStepResultList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepResultList(programm_step_result_list);
            }
        }

        #endregion
    }
}
