﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region CardStatusGroup

    [DataContract]
    public class CardStatusGroup : BonusCardBase
    {
        public CardStatusGroup()
            : base()
        {
            //
        }

        public CardStatusGroup(card_status_group card_status_group)
            : base()
        {
            db_card_status_group = card_status_group;
            card_status_group_id = db_card_status_group.card_status_group_id;
            card_status_group_name = db_card_status_group.card_status_group_name;
            is_active = db_card_status_group.is_active;
        }

        [DataMember]
        public long card_status_group_id { get; set; }
        [DataMember]
        public string card_status_group_name { get; set; }
        [DataMember]
        public short is_active { get; set; }

        public card_status_group Db_card_status_group { get { return db_card_status_group; } }
        private card_status_group db_card_status_group;
    }

    [DataContract]
    public class CardStatusGroupList : BonusCardBase
    {

        public CardStatusGroupList()
            : base()
        {
            //
        }

        public CardStatusGroupList(List<card_status_group> cardStatusGroupList)
        {
            if (cardStatusGroupList != null)
            {
                CardStatusGroup_list = new List<CardStatusGroup>();
                foreach (var item in cardStatusGroupList)
                {
                    CardStatusGroup_list.Add(new CardStatusGroup(item));
                }
            }
        }

        [DataMember]
        public List<CardStatusGroup> CardStatusGroup_list;
    }

    #endregion

    #region CardStatus

    [DataContract]
    public class CardStatus : BonusCardBase
    {
        public CardStatus()
            : base()
        {
            //
        }

        public CardStatus(card_status card_status)
            : base()
        {
            db_card_status = card_status;
            card_status_id = db_card_status.card_status_id;
            card_status_name = db_card_status.card_status_name;
            card_status_group_id = db_card_status.card_status_group_id;
            card_status_group = new CardStatusGroup(db_card_status.card_status_group);
        }

        [DataMember]
        public long card_status_id { get; set; }
        [DataMember]
        public string card_status_name { get; set; }
        [DataMember]
        public Nullable<long> card_status_group_id { get; set; }
        [DataMember]
        public CardStatusGroup card_status_group { get; set; }

        public card_status Db_card_status { get { return db_card_status; } }
        private card_status db_card_status;
    }

    [DataContract]
    public class CardStatusList : BonusCardBase
    {

        public CardStatusList()
            : base()
        {
            //
        }

        public CardStatusList(List<card_status> cardStatusList)
        {
            if (cardStatusList != null)
            {
                CardStatus_list = new List<CardStatus>();
                foreach (var item in cardStatusList)
                {
                    CardStatus_list.Add(new CardStatus(item));
                }
            }
        }

        [DataMember]
        public List<CardStatus> CardStatus_list;
    }

    #endregion
}
