﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region CardStatusGroup

        private CardStatusGroupList xGetCardStatusGroupList(string session_id)
        {
            if (!IsValidSession(session_id))
                return new CardStatusGroupList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_status_group> card_status_group_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                card_status_group_list = dbContext.card_status_group.OrderBy(ss => ss.card_status_group_id).ToList();
                if (card_status_group_list == null)
                {
                    return new CardStatusGroupList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardStatusGroupList(card_status_group_list);
            }
        }

        #endregion

        #region CardStatus

        private CardStatus xGetCardStatus(string session_id, long card_status_id)
        {
            if (!IsValidSession(session_id))
                return new CardStatus() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card_status card_status = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                card_status = dbContext.card_status
                    .Include("card_status_group")
                    .Where(ss => ss.card_status_id == card_status_id).FirstOrDefault();
                if (card_status == null)
                {
                    return new CardStatus() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardStatus(card_status);
            }
        }

        private CardStatusList xGetCardStatusList(string session_id)
        {
            if (!IsValidSession(session_id))
                return new CardStatusList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_status> card_status_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                card_status_list = dbContext.card_status
                    .Include("card_status_group")
                    .OrderBy(ss => ss.card_status_id).ToList();
                if (card_status_list == null)
                {
                    return new CardStatusList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardStatusList(card_status_list);
            }
        }

        private CardStatus xGetCardStatus_byCard(string session_id, long card_id)
        {
            if (!IsValidSession(session_id))
                return new CardStatus() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card_status card_status = null;
            card card = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                card = dbContext.card.Where(ss => ss.card_id == card_id).FirstOrDefault();
                if (card == null)
                    return new CardStatus() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };
                if (card.curr_card_status_id.HasValue)
                {
                    card_status = dbContext.card_status.Where(ss => ss.card_status_id == card.curr_card_status_id).FirstOrDefault();
                }

                if (card_status == null)
                {
                    return new CardStatus() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardStatus(card_status);
            }
        }

        #endregion
    }
}
