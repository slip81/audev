﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {

        #region Programm

        private Programm xInsertProgramm(string session_id, long business_id, string programm_name, DateTime? date_beg, DateTime? date_end, string descr_short, string descr_full, long? card_type_group_id, ProgrammTemplate templ)
        {
            if (!IsValidSession(session_id))
                return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm programm = new programm();

            using (AuMainDb dbContext = new AuMainDb())
            {
                business business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (templ != null)
                {
                    business template_programm_business = (from p in dbContext.programm
                                                           from b in dbContext.business
                                                           where p.business_id == b.business_id
                                                           && p.programm_id == templ.programm_id
                                                           select b)
                                                          .FirstOrDefault();
                    if (template_programm_business == null)
                        return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация исходного алгоритма с кодом " + templ.programm_id.ToString()) };

                    Programm res = xCopyProgramm(session_id, template_programm_business.business_id, templ.programm_id, programm_name, business_id);
                    if (res.error != null)
                        return new Programm() { error = res.error };
                    programm = dbContext.programm.Where(ss => ss.programm_id == res.programm_id).FirstOrDefault();
                    programm.parent_template_id = templ.template_id;

                    if (templ.programm_template_param != null)
                    {
                        foreach (var item1 in templ.programm_template_param.ProgrammTemplateParam_list)
                        {
                            //programm_param programm_param = dbContext.programm_param.Where(ss => ss.programm_id == programm.programm_id && ss.param_id == item1.param_id).FirstOrDefault();
                            programm_param programm_param = (from pp1 in dbContext.programm_param
                                                             from pp2 in dbContext.programm_param
                                                             where pp1.param_num == pp2.param_num
                                                             && pp1.param_id == item1.param_id
                                                             && pp2.programm_id == programm.programm_id
                                                             select pp2)
                                                            .FirstOrDefault();
                            if (programm_param != null)
                            {
                                programm_param.date_value = item1.date_value_def;
                                programm_param.float_value = item1.float_value_def;
                                programm_param.int_value = item1.int_value_def;
                                programm_param.string_value = item1.string_value_def;
                                programm_param.date_only_value = item1.date_only_value_def;
                                programm_param.time_only_value = item1.time_only_value_def;
                                //programm_param.param_name = item1.template_param_name;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    programm.business_id = business_id;
                    programm.programm_name = programm_name;
                    programm.date_beg = date_beg;
                    programm.date_end = date_end;
                    programm.descr_short = descr_short;
                    programm.descr_full = descr_full;
                    programm.card_type_group_id = card_type_group_id;
                    dbContext.programm.Add(programm);                    
                    programm.business = business;                    
                }

                dbContext.SaveChanges();
                return new Programm(programm);
            }
            
        }

        private Programm xUpdateProgramm(string session_id, long business_id, long programm_id, string programm_name, DateTime? date_beg, DateTime? date_end, string descr_short, string descr_full, long? card_type_group_id)
        {
            if (!IsValidSession(session_id))
                return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm programm = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm = (from pr in dbContext.programm                           
                           where pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           select pr)
                             .FirstOrDefault();

                if (programm == null)
                {
                    return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                programm.programm_name = programm_name;
                programm.date_beg = date_beg;
                programm.date_end = date_end;
                programm.descr_short = descr_short;
                programm.descr_full = descr_full;
                programm.card_type_group_id = card_type_group_id;
                dbContext.SaveChanges();

                return new Programm(programm);
            }            
        }

        private DeleteResult xDeleteProgramm(string session_id, long business_id, long programm_id)
        {
            if (!IsValidSession(session_id))
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm programm = null;
            DeleteResult deleteResult = new DeleteResult() { Id = 0 };
            using (AuMainDb dbContext = new AuMainDb())
            {

                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));

                programm = (from pr in dbContext.programm                           
                           where pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           select pr)
                             .FirstOrDefault();

                if (programm == null)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND));
                }

                long card_type_cnt = dbContext.card_type_programm_rel.Where(ss => ss.programm_id == programm_id).Count();
                if (card_type_cnt > 0)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_CHILD_OBJ_EXISTS, "Существуют типы карт с данным алгоритмом"));
                }

                long template_cnt = dbContext.programm_template.Where(ss => ss.programm_id == programm_id).Count();
                if (template_cnt > 0)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_CHILD_OBJ_EXISTS, "Существуют шаблоны созданные на основании данного алгоритма"));
                }

                long trans_cnt = (from pr in dbContext.programm_result
                                  from ct in dbContext.card_transaction
                                  where pr.result_id == ct.programm_result_id
                                  && pr.programm_id == programm_id
                                  select ct).Count();
                if (trans_cnt > 0)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_CHILD_OBJ_EXISTS, "Существуют продажи по данному алгоритму"));
                }

                // удаляем все тестовые расчеты (без транзакций)
                var pr_step_result = from pr in dbContext.programm_result
                                     from psr in dbContext.programm_step_result
                                     where pr.result_id == psr.result_id
                                     && pr.programm_id == programm_id
                                     select psr;
                dbContext.programm_step_result.RemoveRange(pr_step_result);

                var pr_result_detail = from pr in dbContext.programm_result
                                       from prd in dbContext.programm_result_detail
                                       where pr.result_id == prd.result_id
                                       && pr.programm_id == programm_id
                                       select prd;
                dbContext.programm_result_detail.RemoveRange(pr_result_detail);

                var pr_result = dbContext.programm_result.Where(ss => ss.programm_id == programm_id);
                dbContext.programm_result.RemoveRange(pr_result);

                List<programm_step_logic> programm_step_logic_for_del = dbContext.programm_step_logic.Where(ss => ss.programm_id == programm_id).ToList();
                if (programm_step_logic_for_del != null)
                {
                    foreach (var item1 in programm_step_logic_for_del)
                    {
                        deleteResult.AddObj(new BaseObject(item1.logic_id, "ProgrammStepLogic"));
                    }
                    dbContext.programm_step_logic.RemoveRange(programm_step_logic_for_del);
                }

                List<programm_step_condition> programm_step_condition_for_del = dbContext.programm_step_condition.Where(ss => ss.programm_id == programm_id).ToList();
                if (programm_step_logic_for_del != null)
                {
                    foreach (var item2 in programm_step_condition_for_del)
                    {
                        deleteResult.AddObj(new BaseObject(item2.condition_id, "ProgrammStepCondition"));
                    }
                    dbContext.programm_step_condition.RemoveRange(programm_step_condition_for_del);
                }

                List<programm_step_param> programm_step_param_for_del = dbContext.programm_step_param.Where(ss => ss.programm_id == programm_id).ToList();
                if (programm_step_param_for_del != null)
                {
                    foreach (var item3 in programm_step_param_for_del)
                    {
                        deleteResult.AddObj(new BaseObject(item3.param_id, "ProgrammStepParam"));
                    }
                    dbContext.programm_step_param.RemoveRange(programm_step_param_for_del);
                }

                List<programm_step> programm_step_for_del = dbContext.programm_step.Where(ss => ss.programm_id == programm_id).ToList();
                if (programm_step_param_for_del != null)
                {
                    foreach (var item4 in programm_step_for_del)
                    {
                        deleteResult.AddObj(new BaseObject(item4.step_id, "ProgrammStep"));
                    }
                    dbContext.programm_step.RemoveRange(programm_step_for_del);
                }

                List<programm_param> programm_param_for_del = dbContext.programm_param.Where(ss => ss.programm_id == programm_id).ToList();
                if (programm_step_param_for_del != null)
                {
                    foreach (var item5 in programm_param_for_del)
                    {
                        deleteResult.AddObj(new BaseObject(item5.param_id, "ProgrammParam"));
                    }
                    dbContext.programm_param.RemoveRange(programm_param_for_del);
                }

                deleteResult.AddObj(new BaseObject(programm.programm_id, "Programm"));
                dbContext.programm.Remove(programm);
                dbContext.SaveChanges();

                return deleteResult;
            }            
        }

        private Programm xGetProgramm(string session_id, long business_id, long programm_id)
        {
            if (!IsValidSession(session_id))
                return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm programm = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm = (from pr in dbContext.programm                           
                           where pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           select pr)
                           .FirstOrDefault();

                if (programm == null)
                {
                    return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new Programm(programm);
            }            
        }

        private ProgrammList xGetProgrammList(string session_id, long business_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm> programm_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm_list = (from pr in dbContext.programm                           
                           where pr.business_id == business_id                           
                           select pr)
                           .OrderBy(ss => ss.business_id)
                           .ThenBy(ss => ss.programm_id)
                           .ToList()
                           ;

                if (programm_list == null)
                {
                    return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammList(programm_list);
            }
            
        }

        private ProgrammList xGetProgrammList_forTemplate(string session_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1)
                return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Нет прав на данную операцию") };

            List<programm> programm_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                programm_list = dbContext.programm
                    .OrderBy(ss => ss.business_id)
                    .ThenBy(ss => ss.programm_name)
                    .ToList()
                    ;

                if (programm_list == null)
                {
                    return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammList(programm_list);
            }

        }

        private ProgrammList xGetProgrammList_byName(string session_id, long business_id, string programm_name, short? search_mode)
        {
            if (!IsValidSession(session_id))
                return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm> programm_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (search_mode.GetValueOrDefault(0) == 0)
                {
                    programm_list = (from pr in dbContext.programm                              
                               where pr.business_id == business_id                               
                               && pr.programm_name.ToLower().Trim().Contains(programm_name.ToLower().Trim())
                               select pr)
                               .OrderBy(ss => ss.business_id)
                               .ThenBy(ss => ss.programm_id)
                               .ToList()
                               ;
                }
                else
                {
                    programm_list = (from pr in dbContext.programm                                
                                where pr.business_id == business_id                                
                                && pr.programm_name.ToLower().Trim().Equals(programm_name.ToLower().Trim())
                                select pr)
                               .OrderBy(ss => ss.business_id)
                               .ThenBy(ss => ss.programm_id)
                               .ToList()
                               ;
                }

                if (programm_list == null)
                {
                    return new ProgrammList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammList(programm_list);
            }            
        }

        private Programm xCopyProgramm(string session_id, long business_id, long programm_id, string new_programm_name, long new_business_id)
        {
            if (!IsValidSession(session_id))
                return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm programm = null;
            List<programm_param> programm_param_list = null;
            List<programm_step> programm_step_list = null;
            List<programm_step_param> programm_step_param_list = null;
            List<programm_step_condition> programm_step_condition_list = null;
            List<programm_step_logic> programm_step_logic_list = null;
            programm programm_new = null;
            List<programm_param> programm_param_list_new = null;
            List<programm_step> programm_step_list_new = null;
            List<programm_step_param> programm_step_param_list_new = null;
            List<programm_step_condition> programm_step_condition_list_new = null;
            List<programm_step_logic> programm_step_logic_list_new = null;
            business new_business = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm = (from pr in dbContext.programm                            
                            where pr.business_id == business_id                            
                            && pr.programm_id == programm_id
                            select pr)
                           .FirstOrDefault();

                if (programm == null)
                {
                    return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                }

                new_business = dbContext.business.Where(ss => ss.business_id == new_business_id).FirstOrDefault();
                if (new_business == null)
                    return new Programm() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + new_business_id.ToString()) };

                programm_param_list = dbContext.programm_param.Where(ss => ss.programm_id == programm.programm_id).ToList();
                programm_step_list = dbContext.programm_step.Where(ss => ss.programm_id == programm.programm_id).ToList();
                programm_step_param_list = dbContext.programm_step_param.Where(ss => ss.programm_id == programm.programm_id).ToList();
                programm_step_condition_list = dbContext.programm_step_condition.Where(ss => ss.programm_id == programm.programm_id).ToList();
                programm_step_logic_list = dbContext.programm_step_logic.Where(ss => ss.programm_id == programm.programm_id).ToList();

                programm_new = new programm();
                programm_new.business_id = new_business.business_id;
                programm_new.business = new_business;
                programm_new.date_beg = programm.date_beg;
                programm_new.date_end = programm.date_end;
                programm_new.descr_full = programm.descr_full;
                programm_new.descr_short = programm.descr_short;
                programm_new.card_type_group_id = programm.card_type_group_id;
                programm_new.programm_name = new_programm_name;


                dbContext.programm.Add(programm_new);

                if ((programm_param_list != null) && (programm_param_list.Count > 0))
                {
                    programm_param_list_new = new List<programm_param>(programm_param_list);
                    foreach (var item in programm_param_list_new)
                    {
                        item.programm_id = programm_new.programm_id;
                    }

                    dbContext.programm_param.AddRange(programm_param_list_new);
                }

                if ((programm_step_list != null) && (programm_step_list.Count > 0))
                {
                    programm_step_list_new = new List<programm_step>(programm_step_list);
                    foreach (var item in programm_step_list_new)
                    {
                        item.programm_id = programm_new.programm_id;
                    }

                    dbContext.programm_step.AddRange(programm_step_list_new);
                }

                if ((programm_step_param_list != null) && (programm_step_param_list.Count > 0))
                {
                    programm_step_param_list_new = new List<programm_step_param>(programm_step_param_list);
                    foreach (var item in programm_step_param_list_new)
                    {
                        item.programm_id = programm_new.programm_id;
                        //item.programm_param_id = ...
                        // item.prev_step_param_id = ...
                    }

                    dbContext.programm_step_param.AddRange(programm_step_param_list_new);
                }

                if ((programm_step_condition_list != null) && (programm_step_condition_list.Count > 0))
                {
                    programm_step_condition_list_new = new List<programm_step_condition>(programm_step_condition_list);
                    foreach (var item in programm_step_condition_list_new)
                    {
                        item.programm_id = programm_new.programm_id;
                    }

                    dbContext.programm_step_condition.AddRange(programm_step_condition_list_new);
                }

                if ((programm_step_logic_list != null) && (programm_step_logic_list.Count > 0))
                {
                    programm_step_logic_list_new = new List<programm_step_logic>(programm_step_logic_list);
                    foreach (var item in programm_step_logic_list_new)
                    {
                        item.programm_id = programm_new.programm_id;
                    }

                    dbContext.programm_step_logic.AddRange(programm_step_logic_list_new);
                }

                dbContext.SaveChanges();

                programm_param_list = dbContext.programm_param.Where(ss => ss.programm_id == programm.programm_id).ToList();
                programm_step_list = dbContext.programm_step.Where(ss => ss.programm_id == programm.programm_id).ToList();
                programm_step_param_list = dbContext.programm_step_param.Where(ss => ss.programm_id == programm.programm_id).ToList();
                programm_step_condition_list = dbContext.programm_step_condition.Where(ss => ss.programm_id == programm.programm_id).ToList();
                programm_step_logic_list = dbContext.programm_step_logic.Where(ss => ss.programm_id == programm.programm_id).ToList();

                // второй проход - обновляем ссылки
                foreach (var item in programm_step_param_list_new)
                {                    
                    if (item.prev_step_param_id.HasValue)
                    {
                        int? prev_step_param_num = programm_step_param_list.Where(ss => ss.param_id == item.prev_step_param_id).Select(ss => ss.param_num).FirstOrDefault();

                        int prev_step_step_num = (from p1 in programm_step_param_list
                                                  from p2 in programm_step_list
                                                  where p1.step_id == p2.step_id
                                                  && p1.param_id == item.prev_step_param_id
                                                  select p2.step_num
                                                ).FirstOrDefault();

                        long prev_step_param_id = (from p1 in programm_step_param_list_new
                                                  from p2 in programm_step_list_new
                                                  where p1.step_id == p2.step_id
                                                  && p1.param_num == prev_step_param_num
                                                  && p2.step_num == prev_step_step_num
                                                  select p1.param_id
                                                ).FirstOrDefault();
                        
                        item.prev_step_param_id = prev_step_param_id;
                    }
                    else if (item.programm_param_id.HasValue)
                    {
                        int? programm_param_param_num = programm_param_list.Where(ss => ss.param_id == item.programm_param_id).Select(ss => ss.param_num).FirstOrDefault();                        
                        long programm_param_id = programm_param_list_new.Where(ss => ss.param_num == programm_param_param_num).Select(ss => ss.param_id).FirstOrDefault();                        
                        item.programm_param_id = programm_param_id;
                    }
                }

                dbContext.SaveChanges();

                return new Programm(programm_new);
            }
        }

        #endregion        

        #region ProgrammParam

        private ProgrammParam xInsertProgrammParam(string session_id, long business_id, long programm_id, int? param_num, int param_type
            , string string_value, long? int_value, decimal? float_value, DateTime? date_value, DateTime? date_only_value, TimeSpan? time_only_value
            , string param_name, short is_internal, short for_unit_pos)
        {
            if (!IsValidSession(session_id))
                return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_param programm_param = new programm_param();

            using (AuMainDb dbContext = new AuMainDb())
            {
                business business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm programm = dbContext.programm.Where(ss => ss.programm_id == programm_id).FirstOrDefault();
                if (programm == null)
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                if (dbContext.programm_param.Where(ss => ss.programm_id == programm_id && ss.param_num == param_num).Count() > 0)
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_param.programm_id = programm_id;
                programm_param.param_num = param_num;
                programm_param.param_type = param_type;
                programm_param.string_value = string_value;
                programm_param.int_value = int_value;
                programm_param.float_value = float_value;
                programm_param.date_value = date_value;
                programm_param.date_only_value = date_only_value;
                programm_param.time_only_value = time_only_value;
                programm_param.param_name = param_name;
                programm_param.is_internal = is_internal;
                programm_param.for_unit_pos = for_unit_pos;

                dbContext.programm_param.Add(programm_param);
                dbContext.SaveChanges();
                programm_param.programm = programm;

                return new ProgrammParam(programm_param);
            }            
        }

        private ProgrammParam xUpdateProgrammParam(string session_id, long business_id, long programm_id, long param_id, int? param_num, int param_type
            , string string_value, long? int_value, decimal? float_value, DateTime? date_value, DateTime? date_only_value, TimeSpan? time_only_value
            , string param_name, short is_internal, short for_unit_pos)
        {
            if (!IsValidSession(session_id))
                return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_param programm_param = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_param.Where(ss => ss.programm_id == programm_id && ss.param_num == param_num && ss.param_id != param_id).Count() > 0)
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_param = (from pp in dbContext.programm_param
                           from pr in dbContext.programm                           
                           where pp.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && pp.param_id == param_id
                           select pp)
                             .FirstOrDefault();

                if (programm_param == null)
                {
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                programm_param.param_num = param_num;
                programm_param.param_type = param_type;
                programm_param.string_value = string_value;
                programm_param.int_value = int_value;
                programm_param.float_value = float_value;
                programm_param.date_value = date_value;
                programm_param.date_only_value = date_only_value;
                programm_param.time_only_value = time_only_value;
                programm_param.param_name = param_name;
                programm_param.is_internal = is_internal;
                programm_param.for_unit_pos = for_unit_pos;
                dbContext.SaveChanges();

                return new ProgrammParam(programm_param);
            }            
        }

        private DeleteResult xDeleteProgrammParam(string session_id, long business_id, long programm_id, long param_id)
        {
            if (!IsValidSession(session_id))
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_param programm_param = null;
            DeleteResult deleteResult = new DeleteResult() { Id = 0 };
            using (AuMainDb dbContext = new AuMainDb())
            {

                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()));

                programm_param = (from pp in dbContext.programm_param
                           from pr in dbContext.programm                           
                           where pp.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && pp.param_id == param_id
                           select pp)
                             .FirstOrDefault();
                if (programm_param == null)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND));
                }

                // !!!
                // todo:
                // удалить все дочерние объекты

                deleteResult.AddObj(new BaseObject(programm_param.param_id, "ProgrammParam"));
                dbContext.programm_param.Remove(programm_param);
                dbContext.SaveChanges();

                return deleteResult;
            }            
        }

        private ProgrammParam xGetProgrammParam(string session_id, long business_id, long programm_id, long param_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_param programm_param = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                programm_param = (from pp in dbContext.programm_param
                           from pr in dbContext.programm                           
                           where pp.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && pp.param_id == param_id
                           select pp)
                             .FirstOrDefault();

                if (programm_param == null)
                {
                    return new ProgrammParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammParam(programm_param);
            }            
        }

        private ProgrammParamList xGetProgrammParamList_byProgramm(string session_id, long business_id, long programm_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_param> programm_param_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };


                programm_param_list = (from pp in dbContext.programm_param
                           from pr in dbContext.programm                           
                           where pp.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           select pp)
                           .OrderBy(ss => ss.param_num)
                           .ToList()
                           ;

                if (programm_param_list == null)
                {
                    return new ProgrammParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammParamList(programm_param_list);
            }            
        }

        #endregion

        #region ProgrammStep

        private ProgrammStep xInsertProgrammStep(string session_id, long business_id, long programm_id, int? step_num, string descr_short, string descr_full, short single_use_only)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step programm_step = new programm_step();

            using (AuMainDb dbContext = new AuMainDb())
            {
                business business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm programm = dbContext.programm.Where(ss => ss.programm_id == programm_id).FirstOrDefault();
                if (programm == null)
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                if (dbContext.programm_step.Where(ss => ss.programm_id == programm_id && ss.step_num == step_num).Count() > 0)
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_step.programm_id = programm_id;
                programm_step.step_num = (int)step_num;
                programm_step.descr_short = descr_short;
                programm_step.descr_full = descr_full;
                programm_step.single_use_only = single_use_only;

                dbContext.programm_step.Add(programm_step);
                dbContext.SaveChanges();
                programm_step.programm = programm;

                return new ProgrammStep(programm_step);
            }            
        }

        private ProgrammStep xUpdateProgrammStep(string session_id, long business_id, long programm_id, long step_id, int? step_num, string descr_short, string descr_full, short single_use_only)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step programm_step = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.programm_id == programm_id && ss.step_num == step_num && ss.step_id != step_id).Count() > 0)
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_step = (from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           select ps)
                             .FirstOrDefault();

                if (programm_step == null)
                {
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                programm_step.step_num = (int)step_num;
                programm_step.descr_short = descr_short;
                programm_step.descr_full = descr_full;
                programm_step.single_use_only = single_use_only;

                dbContext.SaveChanges();

                return new ProgrammStep(programm_step);
            }
            
        }

        private DeleteResult xDeleteProgrammStep(string session_id, long business_id, long programm_id, long step_id)
        {
            if (!IsValidSession(session_id))
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step programm_step = null;
            DeleteResult deleteResult = new DeleteResult() { Id = 0 };
            using (AuMainDb dbContext = new AuMainDb())
            {

                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()));

                programm_step = (from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           select ps)
                             .FirstOrDefault();

                if (programm_step == null)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND));
                }

                List<programm_step_logic> programm_step_logic_for_del = dbContext.programm_step_logic.Where(ss => ss.step_id == step_id).ToList();
                if (programm_step_logic_for_del != null)
                {
                    foreach (var item1 in programm_step_logic_for_del)
                    {
                        deleteResult.AddObj(new BaseObject(item1.logic_id, "ProgrammStepLogic"));
                    }
                    dbContext.programm_step_logic.RemoveRange(programm_step_logic_for_del);
                }

                List<programm_step_condition> programm_step_condition_for_del = dbContext.programm_step_condition.Where(ss => ss.step_id == step_id).ToList();
                if (programm_step_logic_for_del != null)
                {
                    foreach (var item2 in programm_step_condition_for_del)
                    {
                        deleteResult.AddObj(new BaseObject(item2.condition_id, "ProgrammStepCondition"));
                    }
                    dbContext.programm_step_condition.RemoveRange(programm_step_condition_for_del);
                }

                List<programm_step_param> programm_step_param_for_del = dbContext.programm_step_param.Where(ss => ss.step_id == step_id).ToList();
                if (programm_step_param_for_del != null)
                {
                    foreach (var item3 in programm_step_param_for_del)
                    {
                        deleteResult.AddObj(new BaseObject(item3.param_id, "ProgrammStepParam"));
                    }
                    dbContext.programm_step_param.RemoveRange(programm_step_param_for_del);
                }

                deleteResult.AddObj(new BaseObject(programm_step.step_id, "ProgrammStep"));
                dbContext.programm_step.Remove(programm_step);
                dbContext.SaveChanges();

                return deleteResult;
            }            
        }

        private ProgrammStep xGetProgrammStep(string session_id, long business_id, long programm_id, long step_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step programm_step = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                programm_step = (from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           select ps)
                             .FirstOrDefault();

                if (programm_step == null)
                {
                    return new ProgrammStep() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStep(programm_step);
            }            
        }

        private ProgrammStepList xGetProgrammStepList_byProgramm(string session_id, long business_id, long programm_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_step> programm_step_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };


                programm_step_list = (from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           select ps)
                           .OrderBy(ss => ss.step_num)
                           .ToList()
                           ;

                if (programm_step_list == null)
                {
                    return new ProgrammStepList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepList(programm_step_list);
            }            
        }

        #endregion

        #region ProgrammStepParam

        private ProgrammStepParam xInsertProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, int? card_param_type, int? trans_param_type, long? programm_param_id, long? prev_step_param_id, int? param_num, string param_name, short is_programm_output)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_param programm_step_param = new programm_step_param();

            using (AuMainDb dbContext = new AuMainDb())
            {
                business business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm programm = dbContext.programm.Where(ss => ss.programm_id == programm_id).FirstOrDefault();
                if (programm == null)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                programm_step programm_step = dbContext.programm_step.Where(ss => ss.step_id == step_id).FirstOrDefault();
                if (programm_step == null)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                if (dbContext.programm_step_param.Where(ss => ss.step_id == step_id && ss.param_num == param_num).Count() > 0)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_step_param.programm_id = programm_id;
                programm_step_param.step_id = step_id;
                programm_step_param.card_param_type = card_param_type;
                programm_step_param.trans_param_type = trans_param_type;
                programm_step_param.programm_param_id = programm_param_id;
                programm_step_param.prev_step_param_id = prev_step_param_id;
                programm_step_param.param_num = param_num;
                programm_step_param.param_name = param_name;
                programm_step_param.is_programm_output = is_programm_output;

                dbContext.programm_step_param.Add(programm_step_param);
                dbContext.SaveChanges();
                programm_step_param.programm = programm;
                programm_step_param.programm_step = programm_step;

                return new ProgrammStepParam(programm_step_param);
            }            
        }

        private ProgrammStepParam xUpdateProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id, int? card_param_type, int? trans_param_type, long? programm_param_id, long? prev_step_param_id, int? param_num, string param_name, short is_programm_output)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_param programm_step_param = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };
                if (dbContext.programm_step_param.Where(ss => ss.step_id == step_id && ss.param_num == param_num && ss.param_id != param_id).Count() > 0)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_step_param = (from psp in dbContext.programm_step_param
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where psp.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psp.param_id == param_id
                           select psp)
                             .FirstOrDefault();

                if (programm_step_param == null)
                {
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                programm_step_param.card_param_type = card_param_type;
                programm_step_param.trans_param_type = trans_param_type;
                programm_step_param.programm_param_id = programm_param_id;
                programm_step_param.prev_step_param_id = prev_step_param_id;
                programm_step_param.param_num = param_num;
                programm_step_param.param_name = param_name;
                programm_step_param.is_programm_output = is_programm_output;
                
                dbContext.SaveChanges();

                return new ProgrammStepParam(programm_step_param);
            }            
        }

        private DeleteResult xDeleteProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id)
        {
            if (!IsValidSession(session_id))
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_param programm_step_param = null;
            DeleteResult deleteResult = new DeleteResult() { Id = 0 };
            using (AuMainDb dbContext = new AuMainDb())
            {

                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()));
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()));

                programm_step_param = (from psp in dbContext.programm_step_param
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where psp.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psp.param_id == param_id
                           select psp)
                             .FirstOrDefault();

                if (programm_step_param == null)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND));
                }

                // !!!
                // todo:
                // удалить все дочерние объекты

                deleteResult.AddObj(new BaseObject(programm_step_param.param_id, "ProgrammStepParam"));
                dbContext.programm_step_param.Remove(programm_step_param);
                dbContext.SaveChanges();

                return deleteResult;
            }            
        }

        private ProgrammStepParam xGetProgrammStepParam(string session_id, long business_id, long programm_id, long step_id, long param_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_param programm_step_param = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                programm_step_param = (from psp in dbContext.programm_step_param
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psp.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psp.param_id == param_id
                           select psp)
                             .FirstOrDefault();

                if (programm_step_param == null)
                {
                    return new ProgrammStepParam() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepParam(programm_step_param);
            }
            
        }

        private ProgrammStepParamList xGetProgrammStepParamList_byStep(string session_id, long business_id, long programm_id, long step_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_step_param> programm_step_param_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                programm_step_param_list = (from psp in dbContext.programm_step_param
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psp.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           select psp)
                           .OrderBy(ss => ss.param_num)
                           .ToList();                          

                if (programm_step_param_list == null)
                {
                    return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepParamList(programm_step_param_list);
            }            
        }

        private ProgrammStepParamList xGetProgrammStepParamList_byProgramm(string session_id, long business_id, long programm_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_step_param> programm_step_param_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                programm_step_param_list = (from psp in dbContext.programm_step_param
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psp.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           select psp)
                           .OrderBy(ss => ss.programm_step.step_num)
                           .ThenBy(ss => ss.param_num)
                           .ToList();

                if (programm_step_param_list == null)
                {
                    return new ProgrammStepParamList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepParamList(programm_step_param_list);
            }            
        }

        #endregion

        #region ProgrammStepCondition

        private ProgrammStepCondition xInsertProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, int? condition_num, int? op_type, long? op1_param_id, long? op2_param_id, string condition_name)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_condition programm_step_condition = new programm_step_condition();

            using (AuMainDb dbContext = new AuMainDb())
            {
                business business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm programm = dbContext.programm.Where(ss => ss.programm_id == programm_id).FirstOrDefault();
                if (programm == null)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                programm_step programm_step = dbContext.programm_step.Where(ss => ss.step_id == step_id).FirstOrDefault();
                if (programm_step == null)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                if (dbContext.programm_step_condition.Where(ss => ss.step_id == step_id && ss.condition_num == condition_num).Count() > 0)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_step_condition.programm_id = programm_id;
                programm_step_condition.step_id = step_id;
                programm_step_condition.condition_num = condition_num;
                programm_step_condition.op_type = op_type;
                programm_step_condition.op1_param_id = op1_param_id;
                programm_step_condition.op2_param_id = op2_param_id;
                programm_step_condition.condition_name = condition_name;

                dbContext.programm_step_condition.Add(programm_step_condition);
                dbContext.SaveChanges();
                programm_step_condition.programm = programm;
                programm_step_condition.programm_step = programm_step;

                return new ProgrammStepCondition(programm_step_condition);
            }            
        }

        private ProgrammStepCondition xUpdateProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id, int? condition_num, int? op_type, long? op1_param_id, long? op2_param_id, string condition_name)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_condition programm_step_condition = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };
                if (dbContext.programm_step_condition.Where(ss => ss.step_id == step_id && ss.condition_num == condition_num && ss.condition_id != condition_id).Count() > 0)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_step_condition = (from psc in dbContext.programm_step_condition
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psc.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psc.condition_id == condition_id
                           select psc)
                             .FirstOrDefault();

                if (programm_step_condition == null)
                {
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                programm_step_condition.condition_num = condition_num;
                programm_step_condition.op_type = op_type;
                programm_step_condition.op1_param_id= op1_param_id;
                programm_step_condition.op2_param_id = op2_param_id;
                programm_step_condition.condition_name = condition_name;

                dbContext.SaveChanges();

                return new ProgrammStepCondition(programm_step_condition);
            }            
        }

        private DeleteResult xDeleteProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id)
        {
            if (!IsValidSession(session_id))
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_condition programm_step_condition = null;
            DeleteResult deleteResult = new DeleteResult() { Id = 0 };
            using (AuMainDb dbContext = new AuMainDb())
            {

                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()));
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()));

                programm_step_condition = (from psc in dbContext.programm_step_condition
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psc.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psc.condition_id == condition_id
                           select psc)
                             .FirstOrDefault();
   
                if (programm_step_condition == null)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND));
                }

                // !!!
                // todo:
                // удалить все дочерние объекты

                deleteResult.AddObj(new BaseObject(programm_step_condition.condition_id, "ProgrammStepCondition"));
                dbContext.programm_step_condition.Remove(programm_step_condition);
                dbContext.SaveChanges();

                return deleteResult;
            }

        }

        private ProgrammStepCondition xGetProgrammStepCondition(string session_id, long business_id, long programm_id, long step_id, long condition_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_condition programm_step_condition = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                programm_step_condition = (from psc in dbContext.programm_step_condition
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where psc.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psc.condition_id == condition_id
                           select psc)
                             .FirstOrDefault();

                if (programm_step_condition == null)
                {
                    return new ProgrammStepCondition() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepCondition(programm_step_condition);
            }            
        }

        private ProgrammStepConditionList xGetProgrammStepConditionList_byStep(string session_id, long business_id, long programm_id, long step_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_step_condition> programm_step_condition_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                programm_step_condition_list = (from psc in dbContext.programm_step_condition
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psc.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           select psc)
                           .OrderBy(ss => ss.condition_num)
                           .ToList();

                if (programm_step_condition_list == null)
                {
                    return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepConditionList(programm_step_condition_list);
            }            
        }

        private ProgrammStepConditionList xGetProgrammStepConditionList_byProgramm(string session_id, long business_id, long programm_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_step_condition> programm_step_condition_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                programm_step_condition_list = (from psc in dbContext.programm_step_condition
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psc.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           select psc)
                           .OrderBy(ss => ss.programm_step.step_num)
                           .ThenBy(ss => ss.condition_num)
                           .ToList();

                if (programm_step_condition_list == null)
                {
                    return new ProgrammStepConditionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepConditionList(programm_step_condition_list);
            }            
        }

        #endregion

        #region ProgrammStepLogic

        private ProgrammStepLogic xInsertProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, int? logic_num, int? op_type, long? op1_param_id, long? op2_param_id, long? op3_param_id, long? op4_param_id, long? condition_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_logic programm_step_logic = new programm_step_logic();

            using (AuMainDb dbContext = new AuMainDb())
            {
                business business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                programm programm = dbContext.programm.Where(ss => ss.programm_id == programm_id).FirstOrDefault();
                if (programm == null)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                programm_step programm_step = dbContext.programm_step.Where(ss => ss.step_id == step_id).FirstOrDefault();
                if (programm_step == null)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                if (dbContext.programm_step_logic.Where(ss => ss.step_id == step_id && ss.logic_num == logic_num).Count() > 0)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_step_logic.programm_id = programm_id;
                programm_step_logic.step_id = step_id;
                programm_step_logic.logic_num = logic_num;
                programm_step_logic.op_type = (int)op_type;
                programm_step_logic.op1_param_id = op1_param_id;
                programm_step_logic.op2_param_id = op2_param_id;
                programm_step_logic.op3_param_id = op3_param_id;
                programm_step_logic.op4_param_id = op4_param_id;
                programm_step_logic.condition_id = condition_id;

                dbContext.programm_step_logic.Add(programm_step_logic);
                dbContext.SaveChanges();
                programm_step_logic.programm = programm;
                programm_step_logic.programm_step = programm_step;

                return new ProgrammStepLogic(programm_step_logic);
            }            
        }

        private ProgrammStepLogic xUpdateProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id, int? logic_num, int? op_type, long? op1_param_id, long? op2_param_id, long? op3_param_id, long? op4_param_id, long? condition_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_logic programm_step_logic = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };
                if (dbContext.programm_step_logic.Where(ss => ss.step_id == step_id && ss.logic_num == logic_num && ss.logic_id != logic_id).Count() > 0)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT) };

                programm_step_logic = (from psl in dbContext.programm_step_logic
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psl.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psl.logic_id == logic_id
                           select psl)
                             .FirstOrDefault();

                if (programm_step_logic == null)
                {
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                programm_step_logic.logic_num = logic_num;
                programm_step_logic.op_type = (int)op_type;
                programm_step_logic.op1_param_id = op1_param_id;
                programm_step_logic.op2_param_id = op2_param_id;
                programm_step_logic.op3_param_id = op3_param_id;
                programm_step_logic.op4_param_id = op4_param_id;
                programm_step_logic.condition_id = condition_id;

                dbContext.SaveChanges();

                return new ProgrammStepLogic(programm_step_logic);
            }            
        }

        private DeleteResult xDeleteProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id)
        {
            if (!IsValidSession(session_id))
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_logic programm_step_logic = null;
            DeleteResult deleteResult = new DeleteResult() { Id = 0 };
            using (AuMainDb dbContext = new AuMainDb())
            {

                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()));
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()));

                programm_step_logic = (from psl in dbContext.programm_step_logic
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psl.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psl.logic_id == logic_id
                           select psl)
                             .FirstOrDefault();

                if (programm_step_logic == null)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND));
                }

                // !!!
                // todo:
                // удалить все дочерние объекты

                deleteResult.AddObj(new BaseObject(programm_step_logic.logic_id, "ProgrammStepLogic"));
                dbContext.programm_step_logic.Remove(programm_step_logic);
                dbContext.SaveChanges();

                return deleteResult;
            }            
        }

        private ProgrammStepLogic xGetProgrammStepLogic(string session_id, long business_id, long programm_id, long step_id, long logic_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            programm_step_logic programm_step_logic = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                programm_step_logic = (from psl in dbContext.programm_step_logic
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where psl.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           && psl.logic_id == logic_id
                           select psl)
                             .FirstOrDefault();

                if (programm_step_logic == null)
                {
                    return new ProgrammStepLogic() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepLogic(programm_step_logic);
            }            
        }

        private ProgrammStepLogicList xGetProgrammStepLogicList_byStep(string session_id, long business_id, long programm_id, long step_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_step_logic> programm_step_logic_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };
                if (dbContext.programm_step.Where(ss => ss.step_id == step_id).Count() <= 0)
                    return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаг алгоритма с кодом " + step_id.ToString()) };

                programm_step_logic_list = (from psl in dbContext.programm_step_logic
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm
                           where psl.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id
                           && ps.step_id == step_id
                           select psl)
                           .OrderBy(ss => ss.logic_num)
                           .ToList();

                if (programm_step_logic_list == null)
                {
                    return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepLogicList(programm_step_logic_list);
            }            
        }

        private ProgrammStepLogicList xGetProgrammStepLogicList_byProgramm(string session_id, long business_id, long programm_id)
        {
            if (!IsValidSession(session_id))
                return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<programm_step_logic> programm_step_logic_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                if (dbContext.programm.Where(ss => ss.programm_id == programm_id).Count() <= 0)
                    return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                programm_step_logic_list = (from psl in dbContext.programm_step_logic
                           from ps in dbContext.programm_step
                           from pr in dbContext.programm                           
                           where psl.step_id == ps.step_id
                           && ps.programm_id == pr.programm_id
                           && pr.business_id == business_id                           
                           && pr.programm_id == programm_id                           
                           select psl)
                           .OrderBy(ss => ss.step_id)
                           .ThenBy(ss => ss.logic_num)
                           .ToList();

                if (programm_step_logic_list == null)
                {
                    return new ProgrammStepLogicList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammStepLogicList(programm_step_logic_list);
            }            
        }

        #endregion

        #region CardTypeProgrammRel

        private CardTypeProgrammRelList xGetCardTypeProgrammList_byCardType(string session_id, long business_id, long card_type_id, DateTime? date_beg)
        {
            if (!IsValidSession(session_id))
                return new CardTypeProgrammRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_type_programm_rel> card_type_programm_rel_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardTypeProgrammRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (dbContext.card_type.Where(ss => ss.card_type_id == card_type_id).Count() <= 0)
                    return new CardTypeProgrammRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден тип карты с кодом " + card_type_id.ToString()) };

                card_type_programm_rel_list = (from c in dbContext.card_type
                                               from crel in dbContext.card_type_programm_rel
                                               where c.business_id == business_id
                                               && c.card_type_id == crel.card_type_id
                                               && ((!date_beg.HasValue) || ((crel.date_beg <= date_beg && (crel.date_end > date_beg || !crel.date_end.HasValue))))
                                               && c.card_type_id == card_type_id
                                               select crel)
                            .OrderBy(ss => ss.date_beg)
                            .ThenBy(ss => ss.programm_id)
                            .ToList()
                            ;

                if (card_type_programm_rel_list == null)
                {
                    return new CardTypeProgrammRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardTypeProgrammRelList(card_type_programm_rel_list);
            }
        }

        private CardTypeProgrammRel xInsertCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long programm_id, DateTime date_beg)
        {
            if (!IsValidSession(session_id))
                return new CardTypeProgrammRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            CardType card_type_existing = GetCardType(session_id, business_id, card_type_id);
            if ((card_type_existing == null) || (card_type_existing.error != null))
                return new CardTypeProgrammRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден тип карты с кодом " + card_type_id.ToString()) };
            Programm programm_existing = GetProgramm(session_id, business_id, programm_id);
            if ((programm_existing == null) || (programm_existing.error != null))
                return new CardTypeProgrammRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };


            CardTypeProgrammRel card_type_programm_rel_existing = null;
            CardTypeProgrammRelList card_type_programm_rel_list_existing = GetCardTypeProgrammList_byCardType(session_id, business_id, card_type_id, null);

            card_type_programm_rel card_type_programm_rel_new = null;
            bool isCardTypeProgramExists = false;
            if ((card_type_programm_rel_list_existing != null) && (card_type_programm_rel_list_existing.CardTypeProgrammRel_list != null) && (card_type_programm_rel_list_existing.CardTypeProgrammRel_list./*Where(ss => ss.programm_id == programm_id).*/Count() > 0))
            {
                card_type_programm_rel_existing = card_type_programm_rel_list_existing.CardTypeProgrammRel_list./*Where(ss => ss.programm_id == programm_id).*/OrderBy(ss => ss.date_beg).FirstOrDefault();
                if (card_type_programm_rel_existing.date_beg >= date_beg)
                    return new CardTypeProgrammRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DATE_INTERSECTION, "Существует алгоритм на типе карты с более поздней датой начала") };
                card_type_programm_rel_existing = card_type_programm_rel_list_existing.CardTypeProgrammRel_list./*Where(ss => ss.programm_id == programm_id).*/OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                if (card_type_programm_rel_existing.date_end.HasValue)
                    return new CardTypeProgrammRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DATE_INTERSECTION, "Последний алгоритм на типе карты карты закрыт") };
                if (card_type_programm_rel_existing.date_beg >= date_beg)
                    return new CardTypeProgrammRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DATE_INTERSECTION, "Существует открытый алгоритм на типе карты с более поздней датой начала") };

                isCardTypeProgramExists = true;
            }

            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardTypeProgrammRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (isCardTypeProgramExists)
                {
                    card_type_programm_rel card_type_programm_rel_existing_for_update = dbContext.card_type_programm_rel.Where(ss => ss.rel_id == card_type_programm_rel_existing.rel_id).FirstOrDefault();
                    if (card_type_programm_rel_existing_for_update != null)
                        card_type_programm_rel_existing_for_update.date_end = date_beg;
                }

                card_type_programm_rel_new = new card_type_programm_rel()
                {
                    card_type_id = card_type_id,
                    programm_id = programm_id,
                    date_beg = date_beg,
                    date_end = null,
                };

                dbContext.card_type_programm_rel.Add(card_type_programm_rel_new);
                dbContext.SaveChanges();

                card_type_programm_rel_new.card_type = card_type_existing.Db_card_type;
                card_type_programm_rel_new.programm = programm_existing.Db_programm;

                return new CardTypeProgrammRel(card_type_programm_rel_new);
            }
        }

        private CardTypeProgrammRel xUpdateCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long rel_id, DateTime date_beg)
        {
            // !!!
            throw new Exception("не реализовано");
        }

        private DeleteResult xDeleteCardTypeProgramm_byCardType(string session_id, long business_id, long card_type_id, long rel_id)
        {
            if (!IsValidSession(session_id))
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card_type_programm_rel card_type_programm_rel = null;
            CardTypeProgrammRelList card_type_programm_rel_existing = null;
            long id_prev = 0;
            card_type_programm_rel card_type_programm_rel_prev = null;
            DeleteResult deleteResult = new DeleteResult() { Id = 0 };

            card_type_programm_rel_existing = GetCardTypeProgrammList_byCardType(session_id, business_id, card_type_id, null);
            if ((card_type_programm_rel_existing != null) && (card_type_programm_rel_existing.CardTypeProgrammRel_list != null) && (card_type_programm_rel_existing.CardTypeProgrammRel_list.Count > 0))
            {
                if (card_type_programm_rel_existing.error != null)
                {
                    return new DeleteResult(card_type_programm_rel_existing.error);
                }

                CardTypeProgrammRel item = card_type_programm_rel_existing.CardTypeProgrammRel_list.Where(ss => ss.rel_id == rel_id && !ss.date_end.HasValue).FirstOrDefault();
                if (item == null)
                {
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DEL_DENIED, "Нельзя удалить закрытый алгоритм"));
                }

                CardTypeProgrammRel item_prev = card_type_programm_rel_existing.CardTypeProgrammRel_list.Where(ss => /*ss.programm_id == item.programm_id && */ss.rel_id != rel_id).OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                if (item_prev != null)
                {
                    id_prev = item_prev.rel_id;
                }

            }

            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()));

                card_type_programm_rel = dbContext.card_type_programm_rel.Where(ss => ss.rel_id == rel_id).FirstOrDefault();
                if (card_type_programm_rel == null)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND));
                if (id_prev > 0)
                {
                    card_type_programm_rel_prev = dbContext.card_type_programm_rel.Where(ss => ss.rel_id == id_prev).FirstOrDefault();
                    if (card_type_programm_rel_prev != null)
                    {
                        card_type_programm_rel_prev.date_end = null;
                    }
                }
                deleteResult.AddObj(new BaseObject(card_type_programm_rel.rel_id, "CardTypeProgrammRel"));
                dbContext.card_type_programm_rel.Remove(card_type_programm_rel);
                dbContext.SaveChanges();

                return deleteResult;
            }
        }

        #endregion

    }
}
