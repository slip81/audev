﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region Programm

    [DataContract]
    public class Programm : BonusCardBase
    {
        public Programm()
            : base()
        {
            //
        }

        public Programm(programm programm)
        {
            db_programm = programm;
            programm_id = programm.programm_id;
            programm_name = programm.programm_name;
            date_beg = programm.date_beg;
            date_end = programm.date_end;
            descr_short = programm.descr_short;
            descr_full = programm.descr_full;
            business_id = programm.business_id;
            card_type_group_id = programm.card_type_group_id;
            business = new Business(programm.business);
            if (programm.programm_template != null)
                parent_template = new ProgrammTemplate(programm.programm_template);
        }

        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public string programm_name { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public string descr_short { get; set; }
        [DataMember]
        public string descr_full { get; set; }
        [DataMember]
        public Nullable<long> business_id { get; set; }
        [DataMember]
        public Nullable<long> card_type_group_id { get; set; }
        [DataMember]
        public Business business { get; set; }
        [DataMember]
        public ProgrammTemplate parent_template { get; set; }

        public programm Db_programm { get { return db_programm; } }
        private programm db_programm;

    }

    [DataContract]
    public class ProgrammList : BonusCardBase
    {
        public ProgrammList()
            : base()
        {
            //
        }

        public ProgrammList(List<programm> programmList)
        {
            if (programmList != null)
            {
                Programm_list = new List<Programm>();
                foreach (var item in programmList)
                {
                    Programm_list.Add(new Programm(item));
                }
            }
        }

        [DataMember]
        public List<Programm> Programm_list;

    }

    #endregion    

    #region ProgrammParam

    [DataContract]
    public class ProgrammParam : BonusCardBase
    {
        public ProgrammParam()
            : base()
        {
            //
        }

        public ProgrammParam(programm_param programm_param)
        {
            db_programm_param = programm_param;

            programm_id = programm_param.programm_id;
            param_id = programm_param.param_id;
            param_num = programm_param.param_num;
            param_type = programm_param.param_type;
            string_value = programm_param.string_value;
            int_value = programm_param.int_value;
            float_value = programm_param.float_value;
            date_value = programm_param.date_value;
            param_name = programm_param.param_name;
            date_only_value = programm_param.date_only_value;
            time_only_value = programm_param.time_only_value;
            is_internal = programm_param.is_internal;
            for_unit_pos = programm_param.for_unit_pos;
            programm = new Programm(programm_param.programm);
        }

        [DataMember]
        public long param_id { get; set; }
        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public Nullable<int> param_num { get; set; }
        [DataMember]
        public int param_type { get; set; }
        [DataMember]
        public string string_value { get; set; }
        [DataMember]
        public Nullable<long> int_value { get; set; }
        [DataMember]
        public Nullable<decimal> float_value { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_value { get; set; }
        [DataMember]
        public string param_name { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_only_value { get; set; }
        [DataMember]
        public Nullable<System.TimeSpan> time_only_value { get; set; }
        [DataMember]
        public short is_internal { get; set; }
        [DataMember]
        public short for_unit_pos { get; set; }

        [DataMember]
        public Programm programm { get; set; }

        public programm_param Db_programm_param { get { return db_programm_param; } }
        private programm_param db_programm_param;

    }

    [DataContract]
    public class ProgrammParamList : BonusCardBase
    {
        public ProgrammParamList()
            : base()
        {
            //
        }

        public ProgrammParamList(List<programm_param> programmParamList)
        {
            if (programmParamList != null)
            {
                ProgrammParam_list = new List<ProgrammParam>();
                foreach (var item in programmParamList)
                {
                    ProgrammParam_list.Add(new ProgrammParam(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammParam> ProgrammParam_list;

    }

    #endregion

    #region ProgrammStep

    [DataContract]
    public class ProgrammStep : BonusCardBase
    {
        public ProgrammStep()
            : base()
        {
            //
        }

        public ProgrammStep(programm_step programm_step)
        {
            db_programm_step = programm_step;

            programm_id = programm_step.programm_id;
            step_id = programm_step.step_id;
            step_num = programm_step.step_num;
            descr_short = programm_step.descr_short;
            descr_full = programm_step.descr_full;
            single_use_only = programm_step.single_use_only;
            programm = new Programm(programm_step.programm);
        }

        [DataMember]
        public long step_id { get; set; }
        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public int step_num { get; set; }
        [DataMember]
        public string descr_short { get; set; }
        [DataMember]
        public string descr_full { get; set; }
        [DataMember]
        public short single_use_only { get; set; }

        [DataMember]
        public Programm programm { get; set; }

        public programm_step Db_programm_step { get { return db_programm_step; } }
        private programm_step db_programm_step;

    }

    [DataContract]
    public class ProgrammStepList : BonusCardBase
    {
        public ProgrammStepList()
            : base()
        {
            //
        }

        public ProgrammStepList(List<programm_step> programmStepList)
        {
            if (programmStepList != null)
            {
                ProgrammStep_list = new List<ProgrammStep>();
                foreach (var item in programmStepList)
                {
                    ProgrammStep_list.Add(new ProgrammStep(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammStep> ProgrammStep_list;

    }

    #endregion

    #region ProgrammStepParam

    [DataContract]
    public class ProgrammStepParam : BonusCardBase
    {
        public ProgrammStepParam()
            : base()
        {
            //
        }

        public ProgrammStepParam(programm_step_param programm_step_param)
        {
            db_programm_step_param = programm_step_param;

            programm_id = programm_step_param.programm_id;
            param_id = programm_step_param.param_id;
            step_id = programm_step_param.step_id;
            card_param_type = programm_step_param.card_param_type;
            trans_param_type = programm_step_param.trans_param_type;
            programm_param_id = programm_step_param.programm_param_id;
            prev_step_param_id = programm_step_param.prev_step_param_id;
            param_num = programm_step_param.param_num;
            param_name = programm_step_param.param_name;
            is_programm_output = programm_step_param.is_programm_output;
            programm = new Programm(programm_step_param.programm);
            programm_step = new ProgrammStep(programm_step_param.programm_step);
        }

        [DataMember]
        public long param_id { get; set; }
        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public long step_id { get; set; }
        [DataMember]
        public Nullable<int> card_param_type { get; set; }
        [DataMember]
        public Nullable<int> trans_param_type { get; set; }
        [DataMember]
        public Nullable<long> programm_param_id { get; set; }
        [DataMember]
        public string programm_param_name { get; set; }
        [DataMember]
        public Nullable<long> prev_step_param_id { get; set; }
        [DataMember]
        public string prev_step_param_name { get; set; }
        [DataMember]
        public Nullable<int> param_num { get; set; }
        [DataMember]
        public string param_name { get; set; }
        [DataMember]
        public short is_programm_output { get; set; }
        [DataMember]
        public Programm programm { get; set; }
        [DataMember]
        public ProgrammStep programm_step { get; set; }

        public programm_step_param Db_programm_step_param { get { return db_programm_step_param; } }
        private programm_step_param db_programm_step_param;

    }

    [DataContract]
    public class ProgrammStepParamList : BonusCardBase
    {
        public ProgrammStepParamList()
            : base()
        {
            //
        }

        public ProgrammStepParamList(List<programm_step_param> programmStepParamList)
        {
            if (programmStepParamList != null)
            {
                ProgrammStepParam_list = new List<ProgrammStepParam>();
                foreach (var item in programmStepParamList)
                {
                    ProgrammStepParam_list.Add(new ProgrammStepParam(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammStepParam> ProgrammStepParam_list;

    }

    #endregion

    #region ProgrammStepCondition

    [DataContract]
    public class ProgrammStepCondition : BonusCardBase
    {
        public ProgrammStepCondition()
            : base()
        {
            //
        }

        public ProgrammStepCondition(programm_step_condition programm_step_condition)
        {
            db_programm_step_condition = programm_step_condition;

            condition_id = programm_step_condition.condition_id;
            programm_id = programm_step_condition.programm_id;
            step_id = programm_step_condition.step_id;
            condition_num = programm_step_condition.condition_num;
            op_type = programm_step_condition.op_type;
            op1_param_id = programm_step_condition.op1_param_id;
            op2_param_id = programm_step_condition.op2_param_id;
            condition_name = programm_step_condition.condition_name;
            programm = new Programm(programm_step_condition.programm);
            programm_step = new ProgrammStep(programm_step_condition.programm_step);
        }

        [DataMember]
        public long condition_id { get; set; }
        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public long step_id { get; set; }
        [DataMember]
        public Nullable<int> condition_num { get; set; }
        [DataMember]
        public Nullable<int> op_type { get; set; }
        [DataMember]
        public Nullable<long> op1_param_id { get; set; }
        [DataMember]
        public string op1_param_name { get; set; }
        [DataMember]
        public Nullable<long> op2_param_id { get; set; }
        [DataMember]
        public string op2_param_name { get; set; }
        [DataMember]
        public string condition_name { get; set; }

        [DataMember]
        public Programm programm { get; set; }
        [DataMember]
        public ProgrammStep programm_step { get; set; }

        public programm_step_condition Db_programm_step_condition { get { return db_programm_step_condition; } }
        private programm_step_condition db_programm_step_condition;
    }

    [DataContract]
    public class ProgrammStepConditionList : BonusCardBase
    {
        public ProgrammStepConditionList()
            : base()
        {
            //
        }

        public ProgrammStepConditionList(List<programm_step_condition> programmStepConditionList)
        {
            if (programmStepConditionList != null)
            {
                ProgrammStepCondition_list = new List<ProgrammStepCondition>();
                foreach (var item in programmStepConditionList)
                {
                    ProgrammStepCondition_list.Add(new ProgrammStepCondition(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammStepCondition> ProgrammStepCondition_list;

    }

    #endregion

    #region ProgrammStepLogic

    [DataContract]
    public class ProgrammStepLogic : BonusCardBase
    {
        public ProgrammStepLogic()
            : base()
        {
            //
        }

        public ProgrammStepLogic(programm_step_logic programm_step_logic)
        {
            db_programm_step_logic = programm_step_logic;

            logic_id = programm_step_logic.logic_id;
            programm_id = programm_step_logic.programm_id;
            step_id = programm_step_logic.step_id;
            logic_num = programm_step_logic.logic_num;
            op_type = programm_step_logic.op_type;
            op1_param_id = programm_step_logic.op1_param_id;
            op2_param_id = programm_step_logic.op2_param_id;
            op3_param_id = programm_step_logic.op3_param_id;
            op4_param_id = programm_step_logic.op4_param_id;
            condition_id = programm_step_logic.condition_id;

            programm = new Programm(programm_step_logic.programm);
            programm_step = new ProgrammStep(programm_step_logic.programm_step);
        }

        [DataMember]
        public long logic_id { get; set; }
        [DataMember]
        public long step_id { get; set; }
        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public Nullable<int> logic_num { get; set; }
        [DataMember]
        public int op_type { get; set; }
        [DataMember]
        public Nullable<long> op1_param_id { get; set; }
        [DataMember]
        public string op1_param_name { get; set; }
        [DataMember]
        public Nullable<long> op2_param_id { get; set; }
        [DataMember]
        public string op2_param_name { get; set; }
        [DataMember]
        public Nullable<long> op3_param_id { get; set; }
        [DataMember]
        public string op3_param_name { get; set; }
        [DataMember]
        public Nullable<long> op4_param_id { get; set; }
        [DataMember]
        public string op4_param_name { get; set; }
        [DataMember]
        public Nullable<long> condition_id { get; set; }
        [DataMember]
        public string condition_name { get; set; }

        [DataMember]
        public Programm programm { get; set; }
        [DataMember]
        public ProgrammStep programm_step { get; set; }

        public programm_step_logic Db_programm_step_logic { get { return db_programm_step_logic; } }
        private programm_step_logic db_programm_step_logic;
    }

    [DataContract]
    public class ProgrammStepLogicList : BonusCardBase
    {
        public ProgrammStepLogicList()
            : base()
        {
            //
        }

        public ProgrammStepLogicList(List<programm_step_logic> programmStepLogicList)
        {
            if (programmStepLogicList != null)
            {
                ProgrammStepLogic_list = new List<ProgrammStepLogic>();
                foreach (var item in programmStepLogicList)
                {
                    ProgrammStepLogic_list.Add(new ProgrammStepLogic(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammStepLogic> ProgrammStepLogic_list;

    }

    #endregion

    #region CardTypeProgrammRel

    [DataContract]
    public class CardTypeProgrammRel : BonusCardBase
    {
        public CardTypeProgrammRel()
            : base()
        {
            //
        }

        public CardTypeProgrammRel(card_type_programm_rel card_type_programm_rel)
        {
            db_card_type_programm_rel = card_type_programm_rel;

            rel_id = card_type_programm_rel.rel_id;
            card_type_id = card_type_programm_rel.card_type_id;
            programm_id = card_type_programm_rel.programm_id;
            date_beg = card_type_programm_rel.date_beg;
            date_end = card_type_programm_rel.date_end;

            card_type = new CardType(card_type_programm_rel.card_type);
            programm = new Programm(card_type_programm_rel.programm);
        }

        [DataMember]
        public long rel_id { get; set; }
        [DataMember]
        public long card_type_id { get; set; }
        [DataMember]
        public long programm_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public CardType card_type { get; set; }
        [DataMember]
        public Programm programm { get; set; }

        public card_type_programm_rel Db_card_type_programm_rel { get { return db_card_type_programm_rel; } }
        private card_type_programm_rel db_card_type_programm_rel;
    }

    [DataContract]
    public class CardTypeProgrammRelList : BonusCardBase
    {
        public CardTypeProgrammRelList()
            : base()
        {
            //
        }

        public CardTypeProgrammRelList(List<card_type_programm_rel> cardTypeProgrammRelList)
        {
            if (cardTypeProgrammRelList != null)
            {
                CardTypeProgrammRel_list = new List<CardTypeProgrammRel>();
                foreach (var item in cardTypeProgrammRelList)
                {
                    CardTypeProgrammRel_list.Add(new CardTypeProgrammRel(item));
                }
            }
        }

        [DataMember]
        public List<CardTypeProgrammRel> CardTypeProgrammRel_list;

    }

    #endregion
}
