﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region Log

        private LogSessionList xGetLogSessionList(string session_id, string user_name, DateTime? date_beg, DateTime? date_end)
        { 
            if (!IsValidSession(session_id))
                return new LogSessionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<log_session> log_session_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                log_session_list = dbContext.log_session.Where(ss => ss.log_id > 0
                    && ((String.IsNullOrEmpty(user_name)) || ((!String.IsNullOrEmpty(user_name)) && (ss.user_name.Equals(user_name))))
                    && (((date_beg.HasValue) && (ss.date_beg >= date_beg)) || (!date_beg.HasValue))
                    && (((date_end.HasValue) && (ss.date_beg <= date_end)) || (!date_end.HasValue))
                    ).OrderBy(ss => ss.date_beg).ToList();

                if (log_session_list == null)
                {
                    return new LogSessionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new LogSessionList(log_session_list);
            } 
        }

        private LogEventList xGetLogEventList(string session_id, DateTime? date_beg, DateTime? date_end, string user_name, int? log_event_type_id, long? business_id)
        {
            if (!IsValidSession(session_id))
                return new LogEventList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            IQueryable<log_event> log_event_query = null;
            List<log_event> log_event_list = null;
            DateTime? date_end_actual = date_end.HasValue ? ((DateTime)date_end).AddDays(1) : date_end;
            using (AuMainDb dbContext = new AuMainDb())
            {
                log_event_query = dbContext.log_event.Where(ss => ss.log_id > 0
                    && (((date_beg.HasValue) && (ss.date_beg >= date_beg)) || (!date_beg.HasValue))
                    && (((date_end.HasValue) && (ss.date_beg <= date_end_actual)) || (!date_end.HasValue)));

                if (!String.IsNullOrEmpty(user_name))
                    log_event_query = log_event_query.Where(ss => ss.user_name.Trim().ToLower().Equals(user_name.Trim().ToLower()));
                if (log_event_type_id.HasValue)
                    log_event_query = log_event_query.Where(ss => ss.log_event_type_id == log_event_type_id);
                if (business_id.HasValue)
                    log_event_query = log_event_query.Where(ss => ss.business_id == business_id);

                log_event_list = log_event_query.OrderByDescending(ss => ss.date_beg).ToList();

                if ((log_event_list == null) || (log_event_list.Count <= 0))
                {
                    return new LogEventList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "События не найдены") };
                }

                return new LogEventList(log_event_list);
            } 
        }

        private LogEventTypeList xGetLogEventTypeList(string session_id)
        {
            if (!IsValidSession(session_id))
                return new LogEventTypeList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<log_event_type> log_event_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                log_event_list = dbContext.log_event_type.OrderBy(ss => ss.type_id).ToList();

                if ((log_event_list == null) || (log_event_list.Count <= 0))
                {
                    return new LogEventTypeList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "События не найдены") };
                }

                return new LogEventTypeList(log_event_list);
            } 
        }

        #endregion
    }
}
