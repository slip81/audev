﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region LogSession

    [DataContract]
    public class LogSession : BonusCardBase
    {
        public LogSession()
            : base()
        {
            //
        }

        public LogSession(log_session log_session)
        {
            db_log_session = log_session;
            log_id = log_session.log_id;
            session_id = log_session.session_id;
            business_id = log_session.business_id;
            user_name = log_session.user_name;
            date_beg = log_session.date_beg;
            comment = log_session.comment;
            state = log_session.state;
            date_end = log_session.date_end;
            user_role = log_session.user_role;
            is_admin = log_session.is_admin;
            org_id = log_session.org_id;
        }

        [DataMember]
        public long log_id { get; set; }
        [DataMember]
        public string session_id { get; set; }
        [DataMember]
        public Nullable<long> business_id { get; set; }
        //[DataMember]
        //public Nullable<long> user_id { get; set; }
        [DataMember]
        public string user_name { get; set; }
        [DataMember]
        public System.DateTime date_beg { get; set; }
        [DataMember]
        public string comment { get; set; }
        [DataMember]
        public int state { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public Nullable<long> user_role { get; set; }
        [DataMember]
        public Nullable<short> is_admin { get; set; }
        [DataMember]
        public Nullable<long> org_id { get; set; }
        [DataMember]
        public string business_name { get; set; }
        [DataMember]
        public BusinessSummary business_summary { get; set; }

        public log_session Db_log_session { get { return db_log_session; } }
        private log_session db_log_session;

    }

    [DataContract]
    public class LogSessionList : BonusCardBase
    {
        public LogSessionList()
            : base()
        {
            //
        }

        public LogSessionList(List<log_session> logSessionList)
        {
            if (logSessionList != null)
            {
                LogSession_list = new List<LogSession>();
                foreach (var item in logSessionList)
                {
                    LogSession_list.Add(new LogSession(item));
                }
            }
        }

        [DataMember]
        public List<LogSession> LogSession_list;

    }

    #endregion

    #region LogEvent

    [DataContract]
    public class LogEvent : BonusCardBase
    {
        public LogEvent()
            : base()
        {
            //
        }

        public LogEvent(log_event log_event)
        {
            db_log_event = log_event;
            log_id = log_event.log_id;
            date_beg = log_event.date_beg;
            mess = log_event.mess;
            session_id = log_event.session_id;
            business_id = log_event.business_id;
            user_name = log_event.user_name;
            log_event_type_id = log_event.log_event_type_id;
            obj_id = log_event.obj_id;
            scope = log_event.scope;
            date_end = log_event.date_end;
            mess2 = log_event.mess2;
        }

        [DataMember]
        public long log_id { get; set; }
        [DataMember]
        public System.DateTime date_beg { get; set; }
        [DataMember]
        public string mess { get; set; }
        [DataMember]
        public string session_id { get; set; }
        [DataMember]
        public Nullable<long> business_id { get; set; }
        [DataMember]
        public string user_name { get; set; }
        [DataMember]
        public Nullable<long> log_event_type_id { get; set; }
        [DataMember]
        public Nullable<long> obj_id { get; set; }
        [DataMember]
        public Nullable<int> scope { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public string mess2 { get; set; }
        [DataMember]
        public LogEventType log_event_type { get; set; }


        public log_event Db_log_event { get { return db_log_event; } }
        private log_event db_log_event;

    }

    [DataContract]
    public class LogEventList : BonusCardBase
    {
        public LogEventList()
            : base()
        {
            //
        }

        public LogEventList(List<log_event> logEventList)
        {
            if (logEventList != null)
            {
                LogEvent_list = new List<LogEvent>();
                foreach (var item in logEventList)
                {
                    LogEvent_list.Add(new LogEvent(item));
                }
            }
        }

        [DataMember]
        public List<LogEvent> LogEvent_list;

    }

    #endregion

    #region LogEventType

    [DataContract]
    public class LogEventType : BonusCardBase
    {
        public LogEventType()
            : base()
        {
            //
        }

        public LogEventType(log_event_type log_event_type)
        {
            db_log_event_type = log_event_type;
            type_id = log_event_type.type_id;
            type_name = log_event_type.type_name;
        }

        [DataMember]
        public long type_id { get; set; }
        [DataMember]
        public string type_name { get; set; }

        public log_event_type Db_log_event_type { get { return db_log_event_type; } }
        private log_event_type db_log_event_type;
    }

    [DataContract]
    public class LogEventTypeList : BonusCardBase
    {
        public LogEventTypeList()
            : base()
        {
            //
        }

        public LogEventTypeList(List<log_event_type> logEventTypeList)
        {
            if (logEventTypeList != null)
            {
                LogEventType_list = new List<LogEventType>();
                foreach (var item in logEventTypeList)
                {
                    LogEventType_list.Add(new LogEventType(item));
                }
            }
        }

        [DataMember]
        public List<LogEventType> LogEventType_list;

    }

    #endregion
}
