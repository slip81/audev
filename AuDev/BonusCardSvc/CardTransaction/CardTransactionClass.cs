﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region CardTransaction

    [DataContract]
    public class CardTransaction : BonusCardBase
    {

        public CardTransaction()
            : base()
        {
            //
        }

        public CardTransaction(card_transaction card_transaction)
        {
            db_card_transaction = card_transaction;
            card_trans_id = card_transaction.card_trans_id;
            card_id = card_transaction.card_id;
            if (card_transaction.card != null)
                card_num = card_transaction.card.card_num;
            date_beg = card_transaction.date_beg;
            trans_num = card_transaction.trans_num;
            trans_sum = card_transaction.trans_sum;
            trans_kind = card_transaction.trans_kind;
            status = card_transaction.status;
            canceled_trans_id = card_transaction.canceled_trans_id;
            programm_result_id = card_transaction.programm_result_id;
            user_name = card_transaction.user_name;
            date_check = card_transaction.date_check;
            curr_card_sum = card_transaction.curr_card_sum;
            curr_card_bonus_value = card_transaction.curr_card_bonus_value;
            curr_card_percent_value = card_transaction.curr_card_percent_value;
            trans_card_bonus_value = card_transaction.trans_card_bonus_value;
            trans_card_percent_value = card_transaction.trans_card_percent_value;
            org_name = card_transaction.org_name;
            card_num2 = card_transaction.card_num2;
            date_check_date_only = card_transaction.date_check_date_only;
            curr_card_type_name = card_transaction.curr_card_type_name;
            trans_sum_discount = card_transaction.trans_sum_discount;
            trans_sum_with_discount = card_transaction.trans_sum_with_discount;
            trans_sum_bonus_for_card = card_transaction.trans_sum_bonus_for_card;
            trans_sum_bonus_for_pay = card_transaction.trans_sum_bonus_for_pay;
            business_id = card_transaction.business_id;
            batch_id = card_transaction.batch_id;
            status_date = card_transaction.status_date;
            is_delayed = card_transaction.is_delayed;
            
            if (card_transaction.card_transaction_detail.ToList() != null)
                //card_transaction_detail = card_transaction.card_transaction_detail.ToList();
                card_transaction_detail = new CardTransactionDetailList(card_transaction.card_transaction_detail.ToList());
        }

        public CardTransaction(vw_card_transaction vw_card_transaction)
        {
            db_vw_card_transaction = vw_card_transaction;
            card_trans_id = vw_card_transaction.card_trans_id;
            card_id = vw_card_transaction.card_id;
            card_num = vw_card_transaction.card_num;
            date_beg = vw_card_transaction.date_beg;
            trans_num = vw_card_transaction.trans_num;
            trans_sum = vw_card_transaction.trans_sum;
            trans_kind = vw_card_transaction.trans_kind;
            status = vw_card_transaction.status;
            canceled_trans_id = vw_card_transaction.canceled_trans_id;
            programm_result_id = vw_card_transaction.programm_result_id;
            user_name = vw_card_transaction.user_name;
            date_check = vw_card_transaction.date_check;
            curr_card_sum = vw_card_transaction.curr_card_sum;
            curr_card_bonus_value = vw_card_transaction.curr_card_bonus_value;
            curr_card_percent_value = vw_card_transaction.curr_card_percent_value;
            trans_card_bonus_value = vw_card_transaction.trans_card_bonus_value;
            trans_card_percent_value = vw_card_transaction.trans_card_percent_value;
            org_name = vw_card_transaction.org_name;
            card_num2 = vw_card_transaction.card_num2;
            date_check_date_only = vw_card_transaction.date_check_date_only;
            curr_card_type_name = vw_card_transaction.curr_card_type_name;
            trans_sum_discount = vw_card_transaction.trans_sum_discount;
            trans_sum_with_discount = vw_card_transaction.trans_sum_with_discount;
            trans_sum_bonus_for_card = vw_card_transaction.trans_sum_bonus_for_card;
            trans_sum_bonus_for_pay = vw_card_transaction.trans_sum_bonus_for_pay;
            business_id = vw_card_transaction.business_id;
            status_date = vw_card_transaction.status_date;
            is_delayed = vw_card_transaction.is_delayed;
        }

        [DataMember]
        public long card_trans_id { get; set; }
        [DataMember]
        public Nullable<long> card_id { get; set; }
        [DataMember]
        public Nullable<long> card_num { get; set; }
        [DataMember]
        public Nullable<long> trans_num { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<decimal> trans_sum { get; set; }
        [DataMember]
        public short trans_kind { get; set; }
        [DataMember]
        public Nullable<int> status { get; set; }
        [DataMember]
        public Nullable<long> canceled_trans_id { get; set; }
        [DataMember]
        public Nullable<long> programm_result_id { get; set; }
        [DataMember]
        public string user_name { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_check { get; set; }
        [DataMember]
        public decimal? curr_card_sum { get; set; }
        [DataMember]
        public decimal? curr_card_percent_value { get; set; }
        [DataMember]
        public decimal? curr_card_bonus_value { get; set; }
        [DataMember]
        public decimal? trans_card_percent_value { get; set; }
        [DataMember]
        public decimal? trans_card_bonus_value { get; set; }
        [DataMember]
        public string org_name { get; set; }
        [DataMember]
        public string card_num2 { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_check_date_only { get; set; }
        [DataMember]
        public string curr_card_type_name { get; set; }
        [DataMember]
        public decimal? trans_sum_discount { get; set; }
        [DataMember]
        public decimal? trans_sum_with_discount { get; set; }
        [DataMember]
        public decimal? trans_sum_bonus_for_card { get; set; }
        [DataMember]
        public decimal? trans_sum_bonus_for_pay { get; set; }
        [DataMember]
        public Nullable<long> business_id { get; set; }
        [DataMember]
        public Nullable<long> batch_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> status_date { get; set; }
        [DataMember]
        public Nullable<short> is_delayed { get; set; }
        [DataMember]
        public CardTransactionDetailList card_transaction_detail { get; set; }

        public card_transaction Db_card_transaction { get { return db_card_transaction; } }
        private card_transaction db_card_transaction;

        public vw_card_transaction Db_vw_card_transaction { get { return db_vw_card_transaction; } }
        private vw_card_transaction db_vw_card_transaction;
    }

    [DataContract]
    public class CardTransactionList : BonusCardBase
    {
        public CardTransactionList()
            : base()
        {
            //
        }

        public CardTransactionList(List<card_transaction> cardTransactionList)
        {
            if (cardTransactionList != null)
            {
                CardTransaction_list = new List<CardTransaction>();
                foreach (var item in cardTransactionList)
                {
                    CardTransaction_list.Add(new CardTransaction(item));
                }
            }
        }

        public CardTransactionList(List<vw_card_transaction> cardTransactionList)
        {
            if (cardTransactionList != null)
            {
                CardTransaction_list = new List<CardTransaction>();
                foreach (var item in cardTransactionList)
                {
                    CardTransaction_list.Add(new CardTransaction(item));
                }
            }
        }

        [DataMember]
        public List<CardTransaction> CardTransaction_list;

    }

    #endregion

    #region CardTransactionDetail

    [DataContract]
    public class CardTransactionDetail : BonusCardBase
    {

        public CardTransactionDetail()
            : base()
        {
            //
        }

        public CardTransactionDetail(card_transaction_detail card_transaction_detail)
        {
            db_card_transaction_detail = card_transaction_detail;
            card_trans_id = card_transaction_detail.card_trans_id;
            detail_id = card_transaction_detail.detail_id;
            programm_id = card_transaction_detail.programm_id;
            unit_num = card_transaction_detail.unit_num;
            unit_name = card_transaction_detail.unit_name;
            unit_value = card_transaction_detail.unit_value;
            unit_value_discount = card_transaction_detail.unit_value_discount;
            unit_value_with_discount = card_transaction_detail.unit_value_with_discount;
            unit_percent_discount = card_transaction_detail.unit_percent_discount;
            unit_value_bonus_for_card = card_transaction_detail.unit_value_bonus_for_card;
            unit_value_bonus_for_pay = card_transaction_detail.unit_value_bonus_for_pay;
            unit_percent_bonus_for_card = card_transaction_detail.unit_percent_bonus_for_card;
            unit_percent_bonus_for_pay = card_transaction_detail.unit_percent_bonus_for_pay;
            unit_max_discount_percent = card_transaction_detail.unit_max_discount_percent;
            unit_count = card_transaction_detail.unit_count;
            unit_type = card_transaction_detail.unit_type;
            unit_max_bonus_for_card_percent = card_transaction_detail.unit_max_bonus_for_card_percent;
            unit_max_bonus_for_pay_percent = card_transaction_detail.unit_max_bonus_for_pay_percent;
            price_margin_percent = card_transaction_detail.price_margin_percent;
            artikul = card_transaction_detail.artikul;
            live_prep = card_transaction_detail.live_prep;
            promo = card_transaction_detail.promo;
            tax_percent = card_transaction_detail.tax_percent;
        }

        [DataMember]
        public long detail_id { get; set; }
        [DataMember]
        public Nullable<long> card_trans_id { get; set; }
        [DataMember]
        public Nullable<long> programm_id { get; set; }
        [DataMember]
        public Nullable<int> unit_num { get; set; }
        [DataMember]
        public string unit_name { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value_discount { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value_with_discount { get; set; }
        [DataMember]
        public Nullable<decimal> unit_percent_discount { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value_bonus_for_card { get; set; }
        [DataMember]
        public Nullable<decimal> unit_value_bonus_for_pay { get; set; }
        [DataMember]
        public Nullable<decimal> unit_percent_bonus_for_card { get; set; }
        [DataMember]
        public Nullable<decimal> unit_percent_bonus_for_pay { get; set; }
        [DataMember]
        public Nullable<decimal> unit_max_discount_percent { get; set; }
        [DataMember]
        //public Nullable<int> unit_count { get; set; }
        public Nullable<decimal> unit_count { get; set; }
        [DataMember]
        public Nullable<int> unit_type { get; set; }
        [DataMember]
        public Nullable<decimal> unit_max_bonus_for_card_percent { get; set; }
        [DataMember]
        public Nullable<decimal> unit_max_bonus_for_pay_percent { get; set; }
        [DataMember]
        public Nullable<decimal> price_margin_percent { get; set; }
        [DataMember]
        public Nullable<int> artikul { get; set; }
        [DataMember]
        public Nullable<int> live_prep { get; set; }
        [DataMember]
        public Nullable<int> promo { get; set; }
        [DataMember]
        public Nullable<decimal> tax_percent { get; set; }

        public card_transaction_detail Db_card_transaction_detail { get { return db_card_transaction_detail; } }
        private card_transaction_detail db_card_transaction_detail;
    }

    [DataContract]
    public class CardTransactionDetailList : BonusCardBase
    {
        public CardTransactionDetailList()
            : base()
        {
            //
        }

        public CardTransactionDetailList(List<card_transaction_detail> cardTransactionDetailList)
        {
            if (cardTransactionDetailList != null)
            {
                CardTransactionDetail_list = new List<CardTransactionDetail>();
                foreach (var item in cardTransactionDetailList)
                {
                    CardTransactionDetail_list.Add(new CardTransactionDetail(item));
                }
            }
        }

        [DataMember]
        public List<CardTransactionDetail> CardTransactionDetail_list;

    }

    #endregion
}
