﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region CardTransaction

        private CardTransaction xGetCardTransaction(string session_id, long business_id, long trans_id)
        {
            if (!IsValidSession(session_id))
                return new CardTransaction() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card_transaction card_transaction = null;            

            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardTransaction() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };
                
                card_transaction = (from cr in dbContext.card_transaction
                           from c in dbContext.card                           
                           where cr.card_id == c.card_id
                           && c.business_id == business_id                           
                           && cr.card_trans_id == trans_id
                           select cr)
                           .FirstOrDefault();

                if (card_transaction == null)
                {
                    return new CardTransaction() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardTransaction(card_transaction);
            }

            //return new CardTransaction(card_transaction);
        }
        
        private CardTransactionList xGetCardTransactionList(string session_id, long business_id, long? card_num, string card_num2, short? trans_kind, int? status
            , DateTime? date_beg, DateTime? date_end, DateTime? date_check_beg, DateTime? date_check_end)
        {
            if (!IsValidSession(session_id))
                return new CardTransactionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            IQueryable<vw_card_transaction> vw_card_transaction_query = null;
            List<vw_card_transaction> vw_card_transaction_list = null;
            DateTime? date_end_actual = date_end.HasValue ? ((DateTime)date_end).AddDays(1) : date_end;
            DateTime? date_check_end_actual = date_check_end.HasValue ? ((DateTime)date_check_end).AddDays(1) : date_check_end;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardTransactionList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                /*
                vw_card_transaction_list = dbContext.vw_card_transaction.Where(ss => ss.business_id == business_id
                                         && (((date_beg.HasValue) && (ss.date_beg >= date_beg)) || (!date_beg.HasValue))
                                         && (((date_end.HasValue) && (ss.date_beg <= date_end_actual)) || (!date_end.HasValue))
                                         && (((date_check_beg.HasValue) && (ss.date_check >= date_check_beg)) || (!date_check_beg.HasValue))
                                         && (((date_check_end.HasValue) && (ss.date_check <= date_check_end_actual)) || (!date_check_end.HasValue))
                                         && (((card_num.HasValue) && (ss.card_num == card_num)) || (!card_num.HasValue))
                                         && (((!String.IsNullOrEmpty(card_num2)) && (ss.card_num2.Equals(card_num2))) || (String.IsNullOrEmpty(card_num2)))
                                         && (((trans_kind.HasValue) && (ss.trans_kind == trans_kind)) || (!trans_kind.HasValue))
                                         && (((status.HasValue) && (ss.status == status)) || (!status.HasValue))
                                            )
                                            .OrderBy(ss => ss.card_trans_id)
                                            .ToList()
                                            ;  
                */
                vw_card_transaction_query = dbContext.vw_card_transaction.Where(ss => ss.business_id == business_id);

                if (date_beg.HasValue)
                    vw_card_transaction_query = vw_card_transaction_query.Where(ss => ss.date_beg >= date_beg);
                if (date_end.HasValue)
                    vw_card_transaction_query = vw_card_transaction_query.Where(ss => ss.date_beg <= date_end_actual);
                if (date_check_beg.HasValue)
                    vw_card_transaction_query = vw_card_transaction_query.Where(ss => ss.date_check >= date_check_beg);
                if (date_check_end.HasValue)
                    vw_card_transaction_query = vw_card_transaction_query.Where(ss => ss.date_check <= date_check_end_actual);
                if (card_num.HasValue)
                    vw_card_transaction_query = vw_card_transaction_query.Where(ss => ss.card_num == card_num);
                if (!String.IsNullOrWhiteSpace(card_num2))
                    vw_card_transaction_query = vw_card_transaction_query.Where(ss => ss.card_num2.Equals(card_num2));
                if (trans_kind.HasValue)
                    vw_card_transaction_query = vw_card_transaction_query.Where(ss => ss.trans_kind == trans_kind);
                if (status.HasValue)
                    vw_card_transaction_query = vw_card_transaction_query.Where(ss => ss.status == status);

                vw_card_transaction_list = vw_card_transaction_query.OrderBy(ss => ss.card_trans_id).ToList();

                return new CardTransactionList(vw_card_transaction_list);
            }

       
        }

        private CardTransactionDetailList xGetCardTransactionDetailList(string session_id, long business_id, long? card_trans_id
            , DateTime? date_beg, DateTime? date_end, short? trans_kind, int? status)
        {
            if (!IsValidSession(session_id))
                return new CardTransactionDetailList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            IQueryable<card_transaction_detail> card_transaction_detail_query = null;
            List<card_transaction_detail> card_transaction_detail_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new CardTransactionDetailList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                card_transaction_detail_query = from ct in dbContext.vw_card_transaction
                                                from ctd in dbContext.card_transaction_detail
                                                where ct.card_trans_id == ctd.card_trans_id
                                                && ct.business_id == business_id
                                                && ((!card_trans_id.HasValue) || ((card_trans_id.HasValue) && (ct.card_trans_id == card_trans_id)))
                                                && ((!trans_kind.HasValue) || ((trans_kind.HasValue) && (ct.trans_kind == trans_kind)))
                                                && ((!status.HasValue) || ((status.HasValue) && (ct.status == status)))
                                                && ((!date_beg.HasValue) || ((date_beg.HasValue) && (ct.date_beg >= date_beg)))
                                                && ((!date_end.HasValue) || ((date_end.HasValue) && (ct.date_beg <= date_end)))
                                                select ctd;
                        
                card_transaction_detail_list = card_transaction_detail_query
                           .OrderBy(ss => ss.unit_num)
                           .ToList()
                           ;

                if (card_transaction_detail_list == null)
                {
                    return new CardTransactionDetailList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new CardTransactionDetailList(card_transaction_detail_list);
            }

        }

        #endregion
    }
}
