﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region Business

    [DataContract]
    public class Business : BonusCardBase
    {
        public Business()
            : base()
        {
            //
        }

        public Business(business business)
        {
            db_business = business;
            business_id = business.business_id;
            business_name = business.business_name;
            add_to_timezone = business.add_to_timezone;
            scanner_equals_reader = business.scanner_equals_reader;
            allow_card_add = business.allow_card_add;
        }

        [DataMember]
        public long business_id { get; set; }
        [DataMember]
        public string business_name { get; set; }
        [DataMember]
        public Nullable<int> add_to_timezone { get; set; }
        [DataMember]
        public short scanner_equals_reader { get; set; }
        [DataMember]
        public bool allow_card_add { get; set; }

        public business Db_business { get { return db_business; } }
        private business db_business;
    }
    
    [DataContract]
    public class BusinessList : BonusCardBase
    {
        public BusinessList()
            : base()
        {
            //
        }

        public BusinessList(List<business> businessList)
        {
            if (businessList != null)
            {
                Business_list = new List<Business>();
                foreach (var item in businessList)
                {
                    Business_list.Add(new Business(item));
                }
            }
        }

        [DataMember]
        public List<Business> Business_list;

    }

    #endregion

    #region BusinessRel (old)

    
    [DataContract]
    public class BusinessRel : BonusCardBase
    {
        public BusinessRel()
            : base()
        {
            //
        }

        [DataMember]
        public long rel_id { get; set; }
        [DataMember]
        public Nullable<long> business_id { get; set; }
        [DataMember]
        public Nullable<long> rel_business_id { get; set; }
        [DataMember]
        public Nullable<int> rel_type { get; set; }
        [DataMember]
        public short is_allow_work { get; set; }
        [DataMember]
        public Business business { get; set; }
        [DataMember]
        public Business rel_business { get; set; }
    }

    [DataContract]
    public class BusinessRelList : BonusCardBase
    {
        public BusinessRelList()
            : base()
        {
            //
        }


        [DataMember]
        public List<BusinessRel> BusinessRel_list;

    }
    
    #endregion

    #region BusinessOrg

    [DataContract]
    public class BusinessOrg : BonusCardBase
    {
        public BusinessOrg()
            : base()
        {
            //
        }

        public BusinessOrg(business_org business_org)
        {
            db_business_org = business_org;

            org_id = business_org.org_id;
            org_name = business_org.org_name;
            business_id = business_org.business_id;
            add_to_timezone = business_org.add_to_timezone;
            scanner_equals_reader = business_org.scanner_equals_reader;
            allow_card_add = business_org.allow_card_add;
            business = new Business(business_org.business);
        }

        [DataMember]
        public long org_id { get; set; }
        [DataMember]
        public string org_name { get; set; }
        [DataMember]
        public long business_id { get; set; }
        [DataMember]
        public Nullable<int> add_to_timezone { get; set; }
        [DataMember]
        public short scanner_equals_reader { get; set; }
        [DataMember]
        public bool allow_card_add { get; set; }
        [DataMember]
        public Business business { get; set; }

        public business_org Db_business_org { get { return db_business_org; } }
        private business_org db_business_org;
    }

    [DataContract]
    public class BusinessOrgList : BonusCardBase
    {
        public BusinessOrgList()
            : base()
        {
            //
        }

        public BusinessOrgList(List<business_org> businessOrgList)
        {
            if (businessOrgList != null)
            {
                BusinessOrg_list = new List<BusinessOrg>();
                foreach (var item in businessOrgList)
                {
                    BusinessOrg_list.Add(new BusinessOrg(item));
                }
            }
        }

        [DataMember]
        public List<BusinessOrg> BusinessOrg_list;
    }

    #endregion

    #region BusinessOrgUser

    [DataContract]
    public class BusinessOrgUser : BonusCardBase
    {
        public BusinessOrgUser()
            : base()
        {
            //
        }

        public BusinessOrgUser(business_org_user business_org_user)
        {
            db_business_org_user = business_org_user;

            org_user_id = business_org_user.org_user_id;
            user_name = business_org_user.user_name;
            org_id = business_org_user.org_id;
            business_org = new BusinessOrg(business_org_user.business_org);
        }

        [DataMember]
        public long org_user_id { get; set; }
        [DataMember]
        public string user_name { get; set; }
        [DataMember]
        public long org_id { get; set; }
        [DataMember]
        public BusinessOrg business_org { get; set; }

        public business_org_user Db_business_org_user { get { return db_business_org_user; } }
        private business_org_user db_business_org_user;
    }

    [DataContract]
    public class BusinessOrgUserList : BonusCardBase
    {
        public BusinessOrgUserList()
            : base()
        {
            //
        }

        public BusinessOrgUserList(List<business_org_user> businessOrgUserList)
        {
            if (businessOrgUserList != null)
            {
                BusinessOrgUser_list = new List<BusinessOrgUser>();
                foreach (var item in businessOrgUserList)
                {
                    BusinessOrgUser_list.Add(new BusinessOrgUser(item));
                }
            }
        }

        [DataMember]
        public List<BusinessOrgUser> BusinessOrgUser_list;
    }

    #endregion

    #region CardTypeSummary

    [DataContract]
    public class CardTypeSummary : BonusCardBase
    {
        public CardTypeSummary()
            : base()
        {
            //
        }

        [DataMember]
        public long business_id { get; set; }
        [DataMember]
        public long card_type_id { get; set; }
        [DataMember]
        public string card_type_name { get; set; }
        [DataMember]
        public long card_cnt { get; set; }
    }

    [DataContract]
    public class CardTypeSummaryList : BonusCardBase
    {
        public CardTypeSummaryList()
            : base()
        {
            //
        }

        [DataMember]
        public List<CardTypeSummary> CardTypeSummary_list;

    }

    #endregion

    #region CardStatusSummary

    [DataContract]
    public class CardStatusSummary : BonusCardBase
    {
        public CardStatusSummary()
            : base()
        {
            //
        }

        [DataMember]
        public long business_id { get; set; }
        [DataMember]
        public long card_status_id { get; set; }
        [DataMember]
        public string card_status_name { get; set; }
        [DataMember]
        public long card_cnt { get; set; }
    }

    [DataContract]
    public class CardStatusSummaryList : BonusCardBase
    {
        public CardStatusSummaryList()
            : base()
        {
            //
        }

        [DataMember]
        public List<CardStatusSummary> CardStatusSummary_list;
    }

    #endregion 

    #region SaleSummary

    [DataContract]
    public class SaleSummary : BonusCardBase
    {
        public SaleSummary()
            : base()
        {

        }

        [DataMember]
        public long business_id { get; set; }
        [DataMember]
        public int summary_type { get; set; }
        [DataMember]
        public string summary_type_name { get; set; }
        [DataMember]
        public int sale_cnt { get; set; }
        [DataMember]
        public decimal? sale_sum { get; set; }
        [DataMember]
        public decimal? sale_sum_discount { get; set; }
        [DataMember]
        public decimal? sale_sum_with_discount { get; set; }
        [DataMember]
        public decimal? bonus_for_card_sum { get; set; }
        [DataMember]
        public decimal? bonus_for_pay_sum { get; set; }
    }

    [DataContract]
    public class SaleSummaryList : BonusCardBase
    {
        public SaleSummaryList()
            : base()
        {
            //
        }

        [DataMember]
        public List<SaleSummary> SaleSummary_list;

    }

    #endregion

    #region BusinessSummary

    [DataContract]
    public class BusinessSummary : BonusCardBase
    {
        public BusinessSummary()
            : base()
        {
            //
        }

        [DataMember]
        public long business_id { get; set; }
        [DataMember]
        public string business_name { get; set; }
        [DataMember]
        public long card_cnt { get; set; }
        [DataMember]
        public long card_cnt_last_month { get; set; }
        [DataMember]
        public long card_cnt_last_day { get; set; }
        [DataMember]
        public long client_cnt { get; set; }
        [DataMember]
        public DateTime date_cut { get; set; }
        [DataMember]
        public CardTypeSummaryList card_type_summary { get; set; }
        [DataMember]
        public CardTransactionList card_transaction_last { get; set; }
        [DataMember]
        public CardStatusSummaryList card_status_summary { get; set; }
        [DataMember]
        public BusinessOrgList business_org_list { get; set; }
        [DataMember]
        public BusinessOrgUserList org_user_list { get; set; }
        [DataMember]
        public SaleSummaryList sale_summary_list { get; set; }
    }

    [DataContract]
    public class BusinessSummaryList : BonusCardBase
    {
        public BusinessSummaryList()
            : base()
        {
            //
        }

        [DataMember]
        public List<BusinessSummary> BusinessSummary_list;

    }

    #endregion
}
