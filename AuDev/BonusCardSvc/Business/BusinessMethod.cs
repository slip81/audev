﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {

        #region Business

        private Business xGetBusiness(string session_id, long business_id)
        {
            if (!IsValidSession(session_id))
                return new Business() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            business business = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                {
                    return new Business() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new Business(business);
            }
        }

        private Business xGetBusiness_byCard(string session_id, long card_id)
        {
            if (!IsValidSession(session_id))
                return new Business() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            business business = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                business = (from c in dbContext.card
                            from b in dbContext.business
                            where c.business_id == b.business_id
                            && c.card_id == card_id
                            select b
                            )
                            .FirstOrDefault();

                if (business == null)
                {
                    return new Business() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new Business(business);
            }
        }

        private BusinessList xGetBusinessList(string session_id)
        {
            if (!IsValidSession(session_id))
                return new BusinessList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<business> business_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                business_list = dbContext.business.OrderBy(ss => ss.business_id).ToList();
                if (business_list == null)
                {
                    return new BusinessList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }
                if (business_list.Count <= 0)
                {
                    return new BusinessList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new BusinessList(business_list);
            }
        }        

        private BusinessList xGetBusinessList_byName(string session_id, string business_name, short? search_mode)
        {
            if (!IsValidSession(session_id))
                return new BusinessList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<business> business_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (search_mode.GetValueOrDefault(0) == 0)
                    business_list = dbContext.business
                        .Where(ss => (ss.business_name.ToLower().Trim().Contains(business_name.ToLower().Trim()) && !String.IsNullOrEmpty(business_name)) || (String.IsNullOrEmpty(business_name)))
                        .OrderBy(ss => ss.business_id)
                        .ToList();
                else
                    business_list = dbContext.business
                        .Where(ss => (ss.business_name.ToLower().Trim().Equals(business_name.ToLower().Trim()) && !String.IsNullOrEmpty(business_name)) || (String.IsNullOrEmpty(business_name)))
                        .OrderBy(ss => ss.business_id)
                        .ToList();

                if (business_list == null)
                {
                    return new BusinessList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Организации не найдены") };
                }
                if (business_list.Count <= 0)
                {
                    return new BusinessList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Организации не найдены") };
                }

                return new BusinessList(business_list);
            }
        }
        

        #endregion

        #region BusinessOrg

        private BusinessOrg xGetBusinessOrg_byUserName(long business_id, string user_name)
        {
            int block_num = 0;
            try
            {
                if (String.IsNullOrEmpty(user_name))
                    return new BusinessOrg() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не указан логин") };

                block_num = 1;
                business_org business_org = null;
                using (AuMainDb dbContext = new AuMainDb())
                {
                    block_num = 2;
                    business_org = (from bo in dbContext.business_org
                                    from bou in dbContext.business_org_user
                                    where bo.org_id == bou.org_id
                                    && bo.business_id == business_id
                                    && bou.user_name != null
                                    && bou.user_name.Trim().ToLower().Equals(user_name.Trim().ToLower())
                                    select bo)
                                   .FirstOrDefault();
                    block_num = 3;
                    if (business_org == null)
                    {
                        block_num = 4;
                        return new BusinessOrg() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдено отделение для заданной организации и логина") };
                    }
                    
                    block_num = 5;
                    return new BusinessOrg(business_org);
                }
            }
            catch (Exception ex)
            {
                Loggly.Exception(System.Reflection.MethodBase.GetCurrentMethod().Name, ex, "", business_id, "", null, (long)Enums.LogEventType.ERROR, null, DateTime.Now, block_num.ToString(), true);
                return CreateErrBonusCardBase<BusinessOrg>(ex);
            }
        }

        #endregion

        #region BusinessRel (old)

        
        private BusinessRelList xGetBusinessRelList(string session_id)
        {
            if (!IsValidSession(session_id))
                return new BusinessRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<business> business_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                business_list = dbContext.business.OrderBy(ss => ss.business_id).ToList();
                if( (business_list == null) || (business_list.Count <= 0))
                {
                    return new BusinessRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                BusinessRelList res = new BusinessRelList() { BusinessRel_list = new List<BusinessRel>(), error = null, };
                foreach (var business_list_item in business_list)
                {
                    Business business_orig = new Business(business_list_item);
                    res.BusinessRel_list.Add(new BusinessRel() 
                    {
                        business = business_orig,
                        rel_business = business_orig,
                        business_id = business_orig.business_id,                        
                        rel_business_id = business_orig.business_id,
                        is_allow_work = 1,
                        rel_id = 0,
                        //rel_type = (int)Enums.BusinessRelTypeEnum.SELF,
                        rel_type = 1,
                    });
                }

                return res;

            }

        }
        
        #endregion        

        #region BusinessSummary

        private BusinessSummary xGetBusinessSummary(string session_id, long business_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new BusinessSummary() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            BusinessSummary res = new BusinessSummary() 
            { 
                card_type_summary = new CardTypeSummaryList() 
                    { CardTypeSummary_list = new List<CardTypeSummary>() }, 
                card_transaction_last = new CardTransactionList() 
                    { CardTransaction_list = new List<CardTransaction>() },
            };
            DateTime now = DateTime.Now;
            using (AuMainDb dbContext = new AuMainDb())
            {
                var business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)                
                {
                    return new BusinessSummary() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }
                var cnt1 = dbContext.card.Where(ss => ss.business_id == business_id).Count();
                var cnt2 = (from cl in dbContext.card_holder
                            from crel in dbContext.card_holder_card_rel
                            where cl.business_id == business_id
                            && cl.card_holder_id == crel.card_holder_id
                            && !crel.date_end.HasValue
                            select new { client_id = crel.card_holder_id, card_id = crel.card_id })
                           .Distinct()
                           .Count();

                var card_type_info = (from c in dbContext.card
                                      from crel in dbContext.card_card_type_rel
                                      from ct in dbContext.card_type
                                      where c.business_id == business_id
                                      && c.card_id == crel.card_id
                                      && !crel.date_end.HasValue
                                      && crel.card_type_id == ct.card_type_id
                                      select new { crel.card_id, ct.card_type_id, ct.card_type_name }
                                     )
                                     .GroupBy(ss => new { ss.card_type_id, ss.card_type_name })
                                     .Select(group => new { card_type_id = group.Key.card_type_id, card_type_name = group.Key.card_type_name, card_cnt = group.Count(), })
                                     .OrderBy(ss => ss.card_type_name)
                                     ;
                if (card_type_info != null)
                {
                    foreach (var group_item in card_type_info)
                    {
                        res.card_type_summary.CardTypeSummary_list.Add(new CardTypeSummary() 
                            { 
                                business_id = business_id, 
                                card_type_id = (long)group_item.card_type_id, 
                                card_type_name = group_item.card_type_name,
                                card_cnt = group_item.card_cnt, 
                            });
                    }
                }

                var card_trans_info = dbContext.vw_card_transaction
                    .Where(ss => ss.business_id == business_id
                        && ss.trans_kind == (short)Enums.CardTransactionType.CALC
                        && ss.status == (int)Enums.CardTransactionStatus.DONE)
                        .OrderByDescending(ss => ss.date_check)
                        .Take(10)
                        .ToList();

                if (card_trans_info != null)
                {
                    res.card_transaction_last = new CardTransactionList(card_trans_info);
                }

                res.business_id = business.business_id;
                res.business_name = business.business_name;
                res.card_cnt = cnt1;
                res.client_cnt = cnt2;
                res.date_cut = now;
                
                return res;
            }
        }

        private BusinessSummaryList xGetBusinessSummaryList(string session_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new BusinessSummaryList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1)
                return new BusinessSummaryList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Недостаточно прав") };
                                       
            BusinessSummaryList res = new BusinessSummaryList() { BusinessSummary_list = new List<BusinessSummary>(), };
            using (AuMainDb dbContext = new AuMainDb())
            {
                var groups = dbContext.card.GroupBy(ss => ss.business_id).Select(group => new { business_id = group.Key, card_cnt = group.Count() }).OrderBy(ss => ss.business_id);
                if (groups != null)
                {
                    foreach (var group_item in groups)
                    {
                        res.BusinessSummary_list.Add(new BusinessSummary() {business_id = (long)group_item.business_id, card_cnt = group_item.card_cnt, });
                    }
                }

                return res;
            }
        }

        #endregion

    }
}
