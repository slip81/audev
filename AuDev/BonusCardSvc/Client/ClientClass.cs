﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region Client

    [DataContract]
    public class Client : BonusCardBase
    {
        public Client()
            : base()
        {
            //
        }

        public Client(card_holder client)
        {
            db_card_holder = client;
            client_id = client.card_holder_id;
            client_surname = client.card_holder_surname;
            client_name = client.card_holder_name;
            client_fname = client.card_holder_fname;
            client_full_name = (
                (String.IsNullOrEmpty(client_surname) ? "" : client_surname.Trim() + " ")
                + (String.IsNullOrEmpty(client_name) ? "" : client_name.Trim() + " ")
                + (String.IsNullOrEmpty(client_fname) ? "" : client_fname.Trim())
                ).Trim();
            date_birth = client.date_birth;
            phone_num = client.phone_num;
            address = client.address;
            email = client.email;
            comment = client.comment;
            business_id = client.business_id;
            business = new Business(db_card_holder.business);
        }

        [DataMember]
        public long client_id { get; set; }
        [DataMember]
        public string client_surname { get; set; }
        [DataMember]
        public string client_name { get; set; }
        [DataMember]
        public string client_fname { get; set; }
        [DataMember]
        public string client_full_name { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_birth { get; set; }
        [DataMember]
        public string phone_num { get; set; }
        [DataMember]
        public string address { get; set; }
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string comment { get; set; }
        [DataMember]
        public long business_id { get; set; }
        [DataMember]
        public Business business { get; set; }

        public card_holder Db_card_holder { get { return db_card_holder; } }
        private card_holder db_card_holder;

    }

    [DataContract]
    public class ClientList : BonusCardBase
    {
        public ClientList()
            : base()
        {
            //
        }

        public ClientList(List<card_holder> clientList)
        {
            if (clientList != null)
            {
                Client_list = new List<Client>();
                foreach (var item in clientList)
                {
                    Client_list.Add(new Client(item));
                }
            }
        }

        [DataMember]
        public List<Client> Client_list;

    }

    #endregion

    #region ClientCardRel

    [DataContract]
    public class ClientCardRel : BonusCardBase
    {
        public ClientCardRel()
            : base()
        {
            //
        }

        public ClientCardRel(card_holder_card_rel client_card_rel)
        {
            db_card_holder_card_rel = client_card_rel;
            rel_id = client_card_rel.rel_id;
            client_id = client_card_rel.card_holder_id;
            card_id = client_card_rel.card_id;
            date_beg = client_card_rel.date_beg;
            date_end = client_card_rel.date_end;

            card = new Card(client_card_rel.card);
            client = new Client(client_card_rel.card_holder);
        }

        [DataMember]
        public long rel_id { get; set; }
        [DataMember]
        public long client_id { get; set; }
        [DataMember]
        public long card_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public Card card { get; set; }
        [DataMember]
        public Client client { get; set; }

        public card_holder_card_rel Db_card_holder_card_rel { get { return db_card_holder_card_rel; } }
        private card_holder_card_rel db_card_holder_card_rel;

    }

    [DataContract]
    public class ClientCardRelList : BonusCardBase
    {
        public ClientCardRelList()
            : base()
        {
            //
        }

        public ClientCardRelList(List<card_holder_card_rel> clientCardRelList)
        {
            if (clientCardRelList != null)
            {
                ClientCardRel_list = new List<ClientCardRel>();
                foreach (var item in clientCardRelList)
                {
                    ClientCardRel_list.Add(new ClientCardRel(item));
                }
            }
        }

        [DataMember]
        public List<ClientCardRel> ClientCardRel_list;

    }

    #endregion
}
