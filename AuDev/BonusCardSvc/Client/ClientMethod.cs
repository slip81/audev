﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region Client

        private ClientList xGetClientList(string session_id, long business_id)
        {
            if (!IsValidSession(session_id))
                return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_holder> client_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                client_list = (from cl in dbContext.card_holder                                 
                                 where cl.business_id == business_id                                 
                                 select cl)
                           .OrderBy(ss => ss.business_id)
                           .ThenBy(ss => ss.card_holder_id)
                           .ToList()
                           ;

                if ((client_list == null) || (client_list.Count <= 0))
                {
                    return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нет ни одного владельца карт") };
                }

                return new ClientList(client_list);
            }
        }

        private ClientList xGetClientList_byCard(string session_id, long business_id, long card_id, DateTime date_beg)
        {
            if (!IsValidSession(session_id))
                return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_holder> client_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (dbContext.card.Where(ss => ss.card_id == card_id).Count() <= 0)
                    return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };

                client_list = (from cl in dbContext.card_holder
                               from crel in dbContext.card_holder_card_rel
                               where cl.business_id == business_id
                               && cl.card_holder_id == crel.card_holder_id
                               && crel.card_id == card_id
                               && crel.date_beg <= date_beg && ((!crel.date_end.HasValue) || (crel.date_end >= date_beg))
                               select cl)
                           .OrderBy(ss => ss.business_id)
                           .ThenBy(ss => ss.card_holder_id)
                           .ToList()
                           ;

                if ((client_list == null) || (client_list.Count <= 0))
                {
                    return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нет владельца карты") };
                }

                return new ClientList(client_list);
            }
        }

        private ClientList xGetClientList_byAttrs(string session_id, long business_id, string client_surname, long? card_num)
        {
            if (!IsValidSession(session_id))
                return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            IQueryable<card_holder> client_query = null;
            List<card_holder> client_list = null;
            DateTime today = DateTime.Today;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                client_query = (from cl in dbContext.card_holder                               
                               where cl.business_id == business_id
                               && ((String.IsNullOrEmpty(client_surname)) || ((!String.IsNullOrEmpty(client_surname)) && (cl.card_holder_surname.Trim().ToLower().Contains(client_surname.Trim().ToLower()))))
                               select cl);

                if (card_num.HasValue)
                {
                    client_query = from cl in client_query
                                   from crel in dbContext.card_holder_card_rel
                                   from c in dbContext.card
                                   where cl.card_holder_id == crel.card_holder_id
                                   && crel.card_id == c.card_id
                                   && c.business_id == business_id
                                   && c.card_num == (long)card_num
                                   && crel.date_beg <= today && ((!crel.date_end.HasValue) || (crel.date_end > today))
                                   select cl;
                }

                client_list = client_query.OrderBy(ss => ss.business_id)
                                            .ThenBy(ss => ss.card_holder_id)
                                            .ToList()
                                            ;

                if ((client_list == null) || (client_list.Count <= 0))
                {
                    return new ClientList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нет ни одного владельца карт") };
                }

                return new ClientList(client_list);
            }
        }

        private Client xInsertClient(string session_id, long business_id, string client_surname, string client_name, string client_fname, DateTime? date_birth, string phone_num, string address, string email, string comment)
        {
            if (!IsValidSession(session_id))
                return new Client() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card_holder client = new card_holder();

            using (AuMainDb dbContext = new AuMainDb())
            {
                business business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return new Client() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                DateTime? date_birth_inner = date_birth;
                if (date_birth_inner.HasValue)
                {
                    if (((DateTime)date_birth_inner).Kind == DateTimeKind.Utc)
                        date_birth_inner = ((DateTime)date_birth_inner).ToLocalTime();
                }
                
                client.business_id = business_id;
                client.card_holder_surname = client_surname;
                client.card_holder_name = client_name;
                client.card_holder_fname = client_fname;
                client.date_birth = date_birth_inner;
                client.phone_num = phone_num;
                client.address = address;
                client.email = email;
                client.comment = comment;

                dbContext.card_holder.Add(client);
                dbContext.SaveChanges();
                client.business = business;

                return new Client(client);
            }
        }

        private Client xUpdateClient(string session_id, long business_id, long client_id, string client_surname, string client_name, string client_fname, DateTime? date_birth, string phone_num, string address, string email, string comment)
        {
            if (!IsValidSession(session_id))
                return new Client() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            card_holder client = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new Client() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                client = (from cl in dbContext.card_holder                          
                          where cl.business_id == business_id
                          && cl.card_holder_id == client_id
                          select cl)
                             .FirstOrDefault();

                if (client == null)
                {
                    return new Client() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                client.card_holder_surname = client_surname;
                client.card_holder_name = client_name;
                client.card_holder_fname = client_fname;
                client.date_birth = date_birth;
                client.phone_num = phone_num;
                client.address = address;
                client.email = email;
                client.comment = comment;

                dbContext.SaveChanges();

                return new Client(client);
            }
        }

        #endregion

        #region ClientCardRel

        private ClientCardRelList xGetClientCardList_byClient(string session_id, long business_id, long client_id, DateTime? date_beg)
        {
            if (!IsValidSession(session_id))
                return new ClientCardRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            List<card_holder_card_rel> client_card_rel_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.business.Where(ss => ss.business_id == business_id).Count() <= 0)
                    return new ClientCardRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                if (dbContext.card_holder.Where(ss => ss.card_holder_id == client_id).Count() <= 0)
                    return new ClientCardRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден владелец карты с кодом " + client_id.ToString()) };

                client_card_rel_list = (from cl in dbContext.card_holder
                                        from crel in dbContext.card_holder_card_rel
                               where cl.business_id == business_id
                               && cl.card_holder_id == crel.card_holder_id
                               && cl.card_holder_id == client_id
                               && (((date_beg.HasValue) && (crel.date_beg <= date_beg && ((!crel.date_end.HasValue) || (crel.date_end >= date_beg)))) || (!date_beg.HasValue))
                               select crel)
                           .OrderBy(ss => ss.card_id)
                           .ToList()
                           ;

                if ((client_card_rel_list == null) || (client_card_rel_list.Count <= 0))
                {
                    return new ClientCardRelList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ClientCardRelList(client_card_rel_list);
            }
        }

        private ClientCardRel xInsertClientCard(string session_id, long business_id, long client_id, long card_id, DateTime date_beg, DateTime? date_end)
        {
            if (!IsValidSession(session_id))
                return new ClientCardRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            if ((date_end.HasValue) && (date_end < date_beg))
                return new ClientCardRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Дата окончания не может быть меньше даты начала") };

            return xxInsertClientCard(session_id, business_id, client_id, card_id, date_beg, date_end);
        }

        private ClientCardRel xxInsertClientCard(string session_id, long business_id, long client_id, long card_id, DateTime date_beg, DateTime? date_end)
        {
            card_holder_card_rel client_card_rel = new card_holder_card_rel();
            DateTime today = DateTime.Today;

            DateTime date_beg_no_time = new DateTime(date_beg.Year, date_beg.Month, date_beg.Day);
            DateTime? date_end_no_time = date_end.HasValue ? (DateTime?)(new DateTime(((DateTime)date_end).Year, ((DateTime)date_end).Month, ((DateTime)date_end).Day)) : null;

            using (AuMainDb dbContext = new AuMainDb())
            {
                business business = dbContext.business.Where(ss => ss.business_id == business_id).FirstOrDefault();
                if (business == null)
                    return new ClientCardRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена организация с кодом " + business_id.ToString()) };

                card_holder client = dbContext.card_holder.Where(ss => ss.card_holder_id == client_id).FirstOrDefault();
                if (client == null)
                    return new ClientCardRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден владелец карты с кодом " + client_id.ToString()) };

                Card card = GetCard(session_id, business_id, card_id);
                if ((card == null) || (card.error != null))
                    return new ClientCardRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена карта с кодом " + card_id.ToString()) };

                card_holder_card_rel client_rel_exisiting = dbContext.card_holder_card_rel.Where(ss => ss.card_holder_id == client_id && ss.card_id == card_id).FirstOrDefault();
                if (client_rel_exisiting != null)
                    return new ClientCardRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "У владельца карты с кодом " + client_rel_exisiting.card_holder_id.ToString() + " уже есть эта карта, код карты " + client_rel_exisiting.card_id.ToString()) };

                client_rel_exisiting = dbContext.card_holder_card_rel.Where(ss => ss.card_id == card_id
                    && ss.date_beg <= date_beg_no_time && ((!ss.date_end.HasValue) || (ss.date_end >= date_beg_no_time)))
                    .FirstOrDefault();
                if (client_rel_exisiting != null)
                    return new ClientCardRel() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "У владельца карты с кодом " + client_rel_exisiting.card_holder_id.ToString() + " уже есть эта карта, код карты " + client_rel_exisiting.card_id.ToString()) };

                client_card_rel.card_holder_id = client_id;
                client_card_rel.card_id = card_id;
                client_card_rel.date_beg = date_beg_no_time;
                client_card_rel.date_end = date_end_no_time;

                if ((today >= date_beg_no_time) && ((!date_end_no_time.HasValue) || (today <= date_end_no_time)))
                {
                    card db_card = dbContext.card.Where(ss => ss.card_id == card_id).FirstOrDefault();
                    if (db_card != null)
                        db_card.curr_holder_id = client_id;
                }

                dbContext.card_holder_card_rel.Add(client_card_rel);
                dbContext.SaveChanges();

                client_card_rel.card = card.Db_card;
                client_card_rel.card_holder = client;

                return new ClientCardRel(client_card_rel);
            }

        }
        
        #endregion
    }
}
