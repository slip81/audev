﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace BonusCardLib
{
    #region ProgrammTemplate

    [DataContract]
    public class ProgrammTemplate : BonusCardBase
    {
        public ProgrammTemplate()
            : base()
        {
            //
        }

        public ProgrammTemplate(programm_template programm_template)
        {
            db_programm_template = programm_template;

            template_id = programm_template.template_id;
            template_name = programm_template.template_name;
            programm_id = programm_template.programm_id;
            programm = new Programm(programm_template.programm1);
            if (programm_template.programm_template_param != null)
                programm_template_param = new ProgrammTemplateParamList(programm_template.programm_template_param.ToList());
        }

        [DataMember]
        public long template_id { get; set; }
        [DataMember]
        public string template_name { get; set; }
        [DataMember]
        public long programm_id { get; set; }

        [DataMember]
        public Programm programm { get; set; }
        [DataMember]
        public ProgrammTemplateParamList programm_template_param { get; set; }

        public programm_template Db_programm_template { get { return db_programm_template; } }
        private programm_template db_programm_template;
    }

    [DataContract]
    public class ProgrammTemplateList : BonusCardBase
    {
        public ProgrammTemplateList()
            : base()
        {
            //
        }

        public ProgrammTemplateList(List<programm_template> programmTemplateList)
        {
            if (programmTemplateList != null)
            {
                ProgrammTemplate_list = new List<ProgrammTemplate>();
                foreach (var item in programmTemplateList)
                {
                    ProgrammTemplate_list.Add(new ProgrammTemplate(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammTemplate> ProgrammTemplate_list;
    }

    #endregion

    #region ProgrammTemplateParam

    [DataContract]
    public class ProgrammTemplateParam : BonusCardBase
    {
        public ProgrammTemplateParam()
            : base()
        {
            //
        }

        public ProgrammTemplateParam(programm_template_param programm_template_param)
        {
            db_programm_template_param = programm_template_param;
            template_param_id = programm_template_param.template_param_id;
            param_id = programm_template_param.param_id;
            template_param_name = programm_template_param.template_param_name;
            string_value_def = programm_template_param.string_value_def;
            int_value_def = programm_template_param.int_value_def;
            float_value_def = programm_template_param.float_value_def;
            date_value_def = programm_template_param.date_value_def;
            template_id = programm_template_param.template_id;
            date_only_value_def = programm_template_param.date_only_value_def;
            time_only_value_def = programm_template_param.time_only_value_def;
            if (programm_template_param.programm_param != null)
                programm_param = new ProgrammParam(programm_template_param.programm_param);
        }

        [DataMember]
        public long template_param_id { get; set; }
        [DataMember]
        public Nullable<long> param_id { get; set; }
        [DataMember]
        public string template_param_name { get; set; }
        [DataMember]
        public string string_value_def { get; set; }
        [DataMember]
        public Nullable<long> int_value_def { get; set; }
        [DataMember]
        public Nullable<decimal> float_value_def { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_value_def { get; set; }
        [DataMember]
        public long template_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_only_value_def { get; set; }
        [DataMember]
        public Nullable<System.TimeSpan> time_only_value_def { get; set; }

        [DataMember]
        public bool is_checked { get; set; }

        [DataMember]
        public ProgrammParam programm_param { get; set; }

        public programm_template_param Db_programm_template_param { get { return db_programm_template_param; } }
        private programm_template_param db_programm_template_param;
    }

    [DataContract]
    public class ProgrammTemplateParamList : BonusCardBase
    {
        public ProgrammTemplateParamList()
            : base()
        {
            //
        }

        public ProgrammTemplateParamList(List<programm_template_param> programmTemplateParamList)
        {
            if (programmTemplateParamList != null)
            {
                ProgrammTemplateParam_list = new List<ProgrammTemplateParam>();
                foreach (var item in programmTemplateParamList)
                {
                    ProgrammTemplateParam_list.Add(new ProgrammTemplateParam(item));
                }
            }
        }

        [DataMember]
        public List<ProgrammTemplateParam> ProgrammTemplateParam_list;
    }

    #endregion
}
