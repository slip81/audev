﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {

        #region ProgrammTemplate

        private ProgrammTemplate xInsertProgrammTemplate(string session_id, string template_name, long programm_id, ProgrammTemplateParamList paramList)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1) 
                return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Нет прав на данную операцию") };

            if (String.IsNullOrEmpty(template_name))
                return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Не задано наименование шаблона") };

            programm_template programm_template = null;
            List<programm_template_param> programm_template_param_list = new List<programm_template_param>();
            using (AuMainDb dbContext = new AuMainDb())
            {
                programm programm = dbContext.programm.Where(ss => ss.programm_id == programm_id).FirstOrDefault();
                if (programm == null)
                    return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден алгоритм с кодом " + programm_id.ToString()) };

                if (dbContext.programm_template.Where(ss => ss.template_name.Trim().ToLower().Equals(template_name.Trim().ToLower())).FirstOrDefault() != null)
                    return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT, "Существует шаблон с аналогичным наименованием") };

                programm_template = new programm_template();
                programm_template.template_name = template_name;
                programm_template.programm_id = programm_id;
                programm_template.programm1 = programm;                                

                dbContext.programm_template.Add(programm_template);

                if ((paramList != null) && (paramList.ProgrammTemplateParam_list != null) && (paramList.ProgrammTemplateParam_list.Count > 0))
                {
                    foreach (var item1 in paramList.ProgrammTemplateParam_list)
                    {
                        programm_template_param_list.Add(new programm_template_param()
                        {
                            template_id = programm_template.template_id,
                            param_id = item1.param_id,
                            template_param_name = item1.template_param_name,
                            float_value_def = item1.float_value_def,
                            int_value_def = item1.int_value_def,
                            string_value_def = item1.string_value_def,
                            date_value_def = item1.date_value_def,
                            date_only_value_def = item1.date_only_value_def,
                            time_only_value_def = item1.time_only_value_def,
                        });
                    }

                    dbContext.programm_template_param.AddRange(programm_template_param_list);
                }

                dbContext.SaveChanges();

                return new ProgrammTemplate(programm_template);
            }


        }

        private ProgrammTemplate xUpdateProgrammTemplate(string session_id, long template_id, string template_name, ProgrammTemplateParamList paramList)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1)
                return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Нет прав на данную операцию") };

            if (String.IsNullOrEmpty(template_name))
                return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Не задано наименование шаблона") };

            programm_template programm_template = null;
            programm_template_param programm_template_param = null;
            List<programm_template_param> programm_template_param_list = new List<programm_template_param>();
            using (AuMainDb dbContext = new AuMainDb())
            {
                if (dbContext.programm_template.Where(ss => ss.template_id != template_id && ss.template_name.Trim().ToLower().Equals(template_name.Trim().ToLower())).FirstOrDefault() != null)
                    return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT, "Существует шаблон с аналогичным наименованием") };

                programm_template = dbContext.programm_template.Where(ss => ss.template_id == template_id).FirstOrDefault();
                if (programm_template == null)
                    return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаблон с кодом " + template_id.ToString()) };

                programm_template.template_name = template_name;

                if ((paramList != null) && (paramList.ProgrammTemplateParam_list != null) && (paramList.ProgrammTemplateParam_list.Count > 0))
                {
                    foreach (var item1 in paramList.ProgrammTemplateParam_list)
                    {
                        programm_template_param = dbContext.programm_template_param.Where(ss => ss.template_param_id == item1.template_param_id).FirstOrDefault();
                        if (programm_template_param != null)
                        {                            
                            programm_template_param.float_value_def = item1.float_value_def;
                            programm_template_param.int_value_def = item1.int_value_def;
                            programm_template_param.string_value_def = item1.string_value_def;
                            programm_template_param.date_value_def = item1.date_value_def;
                            programm_template_param.date_only_value_def = item1.date_only_value_def;
                            programm_template_param.time_only_value_def = item1.time_only_value_def;
                            programm_template_param.template_param_name = item1.template_param_name;
                        }
                    }
                }

                dbContext.SaveChanges();

                return new ProgrammTemplate(programm_template);
            }
        }

        private DeleteResult xDeleteProgrammTemplate(string session_id, long template_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1)
                return new DeleteResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Нет прав на данную операцию") };

            programm_template programm_template = null;
            DeleteResult res = new DeleteResult() { Id = 0 };
            using (AuMainDb dbContext = new AuMainDb())
            {
                programm_template = dbContext.programm_template.Where(ss => ss.template_id == template_id).FirstOrDefault();
                if (programm_template == null)
                    return new DeleteResult(new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден шаблон с кодом " + template_id.ToString()));

                List<programm> programm_list = dbContext.programm.Where(ss => ss.parent_template_id == programm_template.template_id).ToList();
                if ((programm_list != null) && (programm_list.Count > 0))
                {
                    foreach (var item in programm_list)
                    {
                        item.parent_template_id = null;
                    }
                }

                List<programm_template_param> programm_template_param_list = dbContext.programm_template_param.Where(ss => ss.template_id == programm_template.template_id).ToList();
                if ((programm_template_param_list != null) && (programm_template_param_list.Count > 0))
                {
                    foreach (var item in programm_template_param_list)
                    {
                        res.AddObj(new BaseObject(item.template_param_id, "ProgrammTemplateParam"));
                        dbContext.programm_template_param.Remove(item);
                    }
                }

                res.AddObj(new BaseObject(programm_template.template_id, "ProgrammTemplate"));
                dbContext.programm_template.Remove(programm_template);
                dbContext.SaveChanges();

                return res;
            }
        }

        private ProgrammTemplate xGetProgrammTemplate(string session_id, long template_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1)
                return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Нет прав на данную операцию") };

            programm_template programm_template = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                programm_template = dbContext.programm_template.Where(ss => ss.template_id == template_id).FirstOrDefault();
                if (programm_template == null)
                {
                    return new ProgrammTemplate() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                }

                return new ProgrammTemplate(programm_template);
            }
        }

        private ProgrammTemplateList xGetProgrammTemplateList(string session_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new ProgrammTemplateList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1)
                return new ProgrammTemplateList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Нет прав на данную операцию") };

            List<programm_template> programm_template_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                programm_template_list = dbContext.programm_template.OrderBy(ss => ss.template_id).ToList();
                if (programm_template_list == null)
                {
                    return new ProgrammTemplateList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Шаблоны не найдены") };
                }

                return new ProgrammTemplateList(programm_template_list);
            }
        }

        private ProgrammTemplateList xGetProgrammTemplateList_byAttrs(string session_id, string template_name)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new ProgrammTemplateList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1)
                return new ProgrammTemplateList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Нет прав на данную операцию") };

            List<programm_template> programm_template_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                programm_template_list = dbContext.programm_template
                    .Where(ss => ((!String.IsNullOrEmpty(template_name)) && (ss.template_name.Trim().ToLower().Contains(template_name.Trim().ToLower()))) || (String.IsNullOrEmpty(template_name)))
                    .OrderBy(ss => ss.template_id).ToList();
                if ((programm_template_list == null) || (programm_template_list.Count <= 0))
                {
                    return new ProgrammTemplateList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Шаблоны не найдены") };
                }

                return new ProgrammTemplateList(programm_template_list);
            }
        }

        #endregion

    }
}
