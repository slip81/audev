﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region Authenticate

        private Dictionary<long, string> UserRoles { get; set; }
        private void CreateUserRoles()
        {
            UserRoles = new Dictionary<long, string>();
            UserRoles.Add(101, "Administrators");
            UserRoles.Add(102, "Testers");
            UserRoles.Add(103, "Users");
            UserRoles.Add(104, "Superadmin");
        }
        
        private UserInfo Authenticate(string user_name, string user_pwd)
        {
            CreateUserRoles();

            EncrHelper2 encr = new EncrHelper2();

            int step_num = 0;

            step_num = 1;
            DirectoryEntry de = new DirectoryEntry();

            bool isAu = false;
            string currRoleName = "";
            long currRoleId = 0;

            DirectorySearcher deSearch;
            SearchResult result;

            DirectoryEntry de_found;
            long business_id_from_user = 0;

            List<long> business_id_list = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                business_id_list = dbContext.business.Select(ss => ss.business_id).ToList();
            }

            foreach (var userRole in UserRoles)
            {
                isAu = false;
                currRoleName = userRole.Value;
                currRoleId = userRole.Key;

                foreach (var business_id in business_id_list)
                {

                    de.Path = "LDAP://10.0.1.2:389/uid=" + business_id.ToString() + ",ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";
                    //de.Path = "LDAP://10.0.1.2:389/ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";                    
                    
                    de.Username = "cn=" + user_name + ",uid=" + business_id.ToString() + ",ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";
                    //de.Username = "cn=" + user_name + ",ou=" + currRoleName.Trim() + ",dc=aptekaural,dc=ru";                    

                    de.Password = encr._GetHashValue(user_pwd);
                    de.AuthenticationType = AuthenticationTypes.None;

                    deSearch = new DirectorySearcher();
                    deSearch.SearchRoot = de;

                    try
                    {
                        result = deSearch.FindOne();
                        isAu = true;

                        business_id_from_user = business_id;
                        break;

                        de_found = result.GetDirectoryEntry();

                        foreach (DirectoryEntry child in de_found.Children)
                        {
                            PropertyCollection pc = child.Properties;
                            IDictionaryEnumerator ide = pc.GetEnumerator();
                            ide.Reset();
                            while (ide.MoveNext())
                            {
                                PropertyValueCollection pvc = ide.Entry.Value as PropertyValueCollection;
                                if ((ide.Entry.Key.ToString().ToLower().Trim().Equals("cn"))
                                    && (pvc.Value.ToString().ToLower().Trim().Equals(user_name.ToLower().Trim())))
                                {
                                    foreach (DirectoryEntry child2 in child.Children)
                                    {
                                        PropertyCollection pc2 = child2.Properties;
                                        IDictionaryEnumerator ide2 = pc2.GetEnumerator();
                                        ide2.Reset();
                                        while (ide2.MoveNext())
                                        {
                                            PropertyValueCollection pvc2 = ide2.Entry.Value as PropertyValueCollection;
                                            if (ide2.Entry.Key.ToString().ToLower().Trim().Equals("uid"))
                                            {
                                                business_id_from_user = Convert.ToInt64(pvc2.Value.ToString());
                                            }
                                            if (business_id_from_user > 0)
                                                break;
                                        }
                                        if (business_id_from_user > 0)
                                            break;
                                    }
                                }

                                if (business_id_from_user > 0)
                                    break;
                            }
                            if (business_id_from_user > 0)
                                break;
                        }
                    }
                    catch
                    {
                        isAu = false;
                    }

                    if (isAu)
                        break;

                }
                if (isAu)
                    break;
            }
            
            return new UserInfo()
                {
                    is_authenticated = isAu,
                    login_name = user_name,
                    role_name = currRoleName,
                    role_id = currRoleId,
                    business_id = business_id_from_user,
                };

        }

        #endregion

        #region UserInfo

        private UserInfoList xGetUserInfoList(string session_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new UserInfoList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };
            if (ls.is_admin.GetValueOrDefault(0) != 1)
                return new UserInfoList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_INSUFFICIENT_RIGHTS, "Нет прав на данную операцию") };

            return xxGetUserInfoList();
        }

        private UserInfoList xGetUserInfoList_byBusiness(string session_id, long business_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if (!IsValidSession(ls))
                return new UserInfoList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Сессия не найдена") };

            UserInfoList res1 = xxGetUserInfoList();
            if ((res1 == null) || (res1.error != null) || (res1.UserInfo_list == null) || (res1.UserInfo_list.Count <= 0))
                return new UserInfoList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_NOT_FOUND, "Пользователи не найдены") };
            else            
                return new UserInfoList(res1.UserInfo_list.Where(ss => ss.business_id == business_id).ToList());                            
        }

        private UserInfoList xxGetUserInfoList()
        {
            DirectoryEntry de = new DirectoryEntry();

            bool isAu = false;

            DirectorySearcher deSearch;
            SearchResult result;

            DirectoryEntry de_found;
            long business_id_from_user = 0;

            isAu = false;

            string user_name = "pavlov";
            string user_pwd = "PIyr21q";
            string user_role = "Administrators";

            //de.Path = "LDAP://10.0.1.2:389/ou=" + user_role.Trim() + ",dc=aptekaural,dc=ru";
            de.Path = "LDAP://10.0.1.2:389/ou=Users,dc=aptekaural,dc=ru";
            de.Username = "cn=" + user_name.Trim() + ",ou=" + user_role.Trim() + ",dc=aptekaural,dc=ru";
            de.Password = user_pwd;
            //de.Password = encr._GetHashValue(user_pwd);
            de.AuthenticationType = AuthenticationTypes.None;

            deSearch = new DirectorySearcher();
            deSearch.SearchRoot = de;

            UserInfo userInfo = null;
            List<UserInfo> userInfo_list = new List<UserInfo>();

            try
            {
                result = deSearch.FindOne();
                isAu = true;

                de_found = result.GetDirectoryEntry();

                foreach (DirectoryEntry child in de_found.Children)
                {
                    PropertyCollection pc = child.Properties;
                    IDictionaryEnumerator ide = pc.GetEnumerator();
                    ide.Reset();
                    while (ide.MoveNext())
                    {                        
                        PropertyValueCollection pvc = ide.Entry.Value as PropertyValueCollection;
                        /*
                        if (ide.Entry.Key.ToString().ToLower().Trim().Equals("cn"))
                        {
                            userInfo = new UserInfo()
                            {
                                is_authenticated = false,
                                login_name = pvc.Value.ToString().ToLower().Trim(),
                                role_name = "",
                                role_id = null,
                                business_id = null,
                            };

                            foreach (DirectoryEntry child2 in child.Children)
                            {

                                PropertyCollection pc2 = child2.Properties;
                                IDictionaryEnumerator ide2 = pc2.GetEnumerator();
                                ide2.Reset();
                                while (ide2.MoveNext())
                                {
                                    PropertyValueCollection pvc2 = ide2.Entry.Value as PropertyValueCollection;

                                    if (ide2.Entry.Key.ToString().ToLower().Trim().Equals("uid"))
                                    {
                                        userInfo.business_id = Convert.ToInt64(pvc2.Value.ToString());
                                        userInfo_list.Add(userInfo);
                                        break;
                                    }
                                }

                                if (userInfo.business_id.HasValue)
                                    break;
                            }

                        }
                        */
                        //добавлено в версии 1.2.1.3
                        //учет модели, когда uid наверху, и к нему привязаны логины
                        if (ide.Entry.Key.ToString().ToLower().Trim().Equals("uid"))
                        {

                            foreach (DirectoryEntry child2 in child.Children)
                            {

                                PropertyCollection pc2 = child2.Properties;
                                IDictionaryEnumerator ide2 = pc2.GetEnumerator();
                                ide2.Reset();
                                while (ide2.MoveNext())
                                {
                                    PropertyValueCollection pvc2 = ide2.Entry.Value as PropertyValueCollection;

                                    if (ide2.Entry.Key.ToString().ToLower().Trim().Equals("cn"))
                                    {
                                        userInfo = new UserInfo()
                                        {
                                            is_authenticated = false,
                                            login_name = pvc2.Value.ToString().ToLower().Trim(),
                                            role_name = "",
                                            role_id = null,
                                            business_id = Convert.ToInt64(pvc.Value.ToString()),
                                        };                                        
                                        userInfo_list.Add(userInfo);
                                        break;
                                    }
                                }

                                //if (userInfo.business_id.HasValue)
                                    //break;
                            }

                        }
                    }
                }
            }
            catch
            {
                isAu = false;
            }

            if (isAu)
                return new UserInfoList(userInfo_list);
            else
                return new UserInfoList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Пользователи не найдены") };

        }

        #endregion
    }

}
