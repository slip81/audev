﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
//using System.Data.Objects;
using System.Security.Cryptography;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region GetVersion

        private string xGetVersion(string session_id)
        {
            return "v1.5.4-d2016.02.08";
        }

        #endregion

        #region GetVersionDb

        private string xGetVersionDb(string session_id)
        {
            using (CardDbEdm dbContext = new CardDbEdm())
            {
                var ver = dbContext.version_db.FirstOrDefault();
                if (ver != null)
                {
                    return ver.version_num;
                }
                else
                {
                    return "";
                }
            }
        }

        #endregion

        #region LogSession
       
        private string xStartSession(string user_name, string user_pwd, long? business_id)
        {

            EncrHelper2 encr = new EncrHelper2();

            UserInfo ui = null;

            // !!!
            // убрать
            
            if (user_name.Trim().ToLower().Equals("empty") && (user_pwd.Equals("")))
            {
                ui = new UserInfo();
                ui.is_authenticated = true;
                ui.login_name = user_name;
                ui.business_id = 957862396381103979;
                ui.role_id = (long)Enums.UserRole.ADMIN;
            }
            else                      
            {
                ui = new UserInfo();
                ui = Authenticate(user_name, user_pwd);
                if (!(ui.is_authenticated))
                    return "";
            }

            LogSession LogSession = null;

            // создание session_id
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            string session_id = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            using (CardDbEdm dbContext = new CardDbEdm())
            {
                log_session ls = new log_session();
                ls.date_beg = DateTime.Now;
                ls.session_id = session_id.Trim();
                ls.business_id = ui.business_id.GetValueOrDefault(0) <= 0 ? (business_id.GetValueOrDefault(0) <= 0 ? null : business_id) : ui.business_id;
                ls.user_name = user_name;
                ls.user_role = ui.role_id;
                ls.is_admin = ((ui.role_id.GetValueOrDefault(0) == (long)Enums.UserRole.ADMIN) || (ui.role_id.GetValueOrDefault(0) == (long)Enums.UserRole.SUPERADMIN)) ? (short)1 : (short)0;
                ls.state = 0;                
                dbContext.log_session.Add(ls);
                dbContext.SaveChanges();
                
                LogSession = new LogSession(ls);
                /*
                if (ls.business_id.GetValueOrDefault(0) > 0)
                    LogSession.business_name = dbContext.business.Where(ss => ss.business_id == ls.business_id).Select(ss => ss.business_name).FirstOrDefault();
                */
                
            }

            //Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, "started session_id: " + session_id, session_id, user_name);            
            Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, session_id, session_id, "", LogSession, (long)Enums.LogEventType.SESSION_START, null, true);
            return session_id.Trim();
        }

        private long xCloseSession(string session_id)
        {
            if (String.IsNullOrEmpty(session_id))
                return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;

            LogSession LogSession = null;
            using (CardDbEdm dbContext = new CardDbEdm())
            {
                log_session ls = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).FirstOrDefault();
                if (ls == null)
                    return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;
                if (ls.state == 0)
                {
                    //dbContext.log_session.Remove(ls);
                    ls.state = 1;
                    ls.date_end = DateTime.Now;
                }
                else
                    return (long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED;

                dbContext.SaveChanges();

                LogSession = new BonusCardLib.LogSession(ls);

                //Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, "closed session_id: " + session_id, session_id, ls.user_name); 
                Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, session_id, session_id, "", LogSession, (long)Enums.LogEventType.SESSION_CLOSE, null, true);
            }

            return 0;
        }

        private bool IsValidSession(string session_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if ((ls == null) || (ls.error != null))
                return false;
            else
                return true;
        }

        private bool IsValidSession(LogSession session)
        {
            if ((session == null) || (session.error != null))
                return false;
            else
                return true;
        }

        private LogSession xGetActiveSession(string session_id)
        {
            log_session ls = null;
            List<log_session> ls_list = null;
            DateTime today = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            string str = "";
            BusinessSummary bs = null;
            using (CardDbEdm dbContext = new CardDbEdm())
            {
                ls_list = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).ToList();
                if (ls_list == null)
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Сессия не найдена") };
                if (ls_list.Count <= 0)
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Сессия не найдена") };
                if (ls_list.Count > 1)
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT, "Сессия не уникальна") };

                ls = ls_list.FirstOrDefault();
                if (ls.state != 0)
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED, "Сессия закрыта") };

                DateTime session_day = new DateTime(ls.date_beg.Year, ls.date_beg.Month, ls.date_beg.Day);
                if (session_day < today)
                {
                    long res1 = xCloseSession(session_id);
                    if (res1 != 0)
                        return new LogSession() { error = new ErrInfo(res1, "Ошибка при попытке закрыть просроченную сессию") };
                    else
                        return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED, "Сессия просрочена и закрыта") };
                }

                if (ls.business_id.GetValueOrDefault(0) > 0)
                {
                    str = dbContext.business.Where(ss => ss.business_id == ls.business_id).Select(ss => ss.business_name).FirstOrDefault();
                    var cnt = dbContext.card.Where(ss => ss.business_id == ls.business_id).Count();
                    bs = new BusinessSummary() { business_id = (long)ls.business_id, card_cnt = cnt, };                    
                }
                
                return new LogSession(ls) { business_name = str, business_summary = bs, };

            }
            
        }

        private LogSession xGetSession(string session_id)
        {
            log_session ls = null;
            List<log_session> ls_list = null;
            string str = "";
            BusinessSummary bs = null;
            using (CardDbEdm dbContext = new CardDbEdm())
            {
                ls_list = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).ToList();
                if (ls_list == null)
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                if (ls_list.Count <= 0)
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND) };
                if (ls_list.Count > 1)
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_MULTIPLE_RESULT) };

                ls = ls_list.FirstOrDefault();
                if (ls.business_id.GetValueOrDefault(0) > 0)
                {
                    str = dbContext.business.Where(ss => ss.business_id == ls.business_id).Select(ss => ss.business_name).FirstOrDefault();
                    var cnt = dbContext.card.Where(ss => ss.business_id == ls.business_id).Count();
                    bs = new BusinessSummary() { business_id = (long)ls.business_id, card_cnt = cnt, };
                }

                return new LogSession(ls) { business_name = str, business_summary = bs, };
            }
            
        }


        #endregion


    }
}

