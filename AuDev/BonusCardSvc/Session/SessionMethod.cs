﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace BonusCardLib
{
    public partial class BonusCardService
    {
        #region GetVersion

        private string xGetVersion(string session_id)
        {
            return "v1.6.4-d2016.03.19";
        }

        #endregion

        #region GetVersionDb

        private string xGetVersionDb(string session_id)
        {
            using (AuMainDb dbContext = new AuMainDb())
            {
                var ver = dbContext.version_db.FirstOrDefault();
                if (ver != null)
                {
                    return ver.version_num;
                }
                else
                {
                    return "";
                }
            }
        }

        #endregion

        #region LogSession
       
        private string xStartSession(string user_name, string user_pwd, long? business_id)
        {

            EncrHelper2 encr = new EncrHelper2();
            
            DateTime now = DateTime.Now;
            UserInfo ui = null;

            // !!!
            // убрать
            
            if (user_name.Trim().ToLower().Equals("empty") && (user_pwd.Equals("")))
            {
                ui = new UserInfo();
                ui.is_authenticated = true;
                ui.login_name = user_name;
                ui.role_id = (long)Enums.UserRole.ADMIN;
                //ui.business_id = 957862396381103979;      // "ТРИТ, ООО"
                //ui.business_id = 1202118759788054458;     // "Аптека В1, ООО"
                ui.business_id = 1191989625543984144;       // "!Тест_Монолит"                
            }
            else                      
            {
                ui = new UserInfo();
                ui = Authenticate(user_name, user_pwd);
                if (!(ui.is_authenticated))
                    return "";
            }
            
            LogSession LogSession = null;

            // создание session_id
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            string session_id = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            using (AuMainDb dbContext = new AuMainDb())
            {
                long? curr_business_id = ui.business_id.GetValueOrDefault(0) <= 0 ? (business_id.GetValueOrDefault(0) <= 0 ? null : business_id) : ui.business_id;

                var org = (from bo in dbContext.business_org
                           from bou in dbContext.business_org_user
                           where bo.org_id == bou.org_id
                           && bo.business_id == curr_business_id
                           && bou.user_name.Trim().ToLower().Equals(user_name.Trim().ToLower())
                           select bou)
                          .FirstOrDefault();

                log_session ls = new log_session();
                ls.date_beg = now;
                ls.session_id = session_id.Trim();
                ls.business_id = curr_business_id;
                ls.user_name = user_name;
                ls.user_role = ui.role_id;
                ls.is_admin = ((ui.role_id.GetValueOrDefault(0) == (long)Enums.UserRole.ADMIN) || (ui.role_id.GetValueOrDefault(0) == (long)Enums.UserRole.SUPERADMIN)) ? (short)1 : (short)0;
                ls.state = 0;
                ls.org_id = org != null ? (long?)org.org_id : null;
                dbContext.log_session.Add(ls);
                dbContext.SaveChanges();
                
                LogSession = new LogSession(ls);
                /*
                if (ls.business_id.GetValueOrDefault(0) > 0)
                    LogSession.business_name = dbContext.business.Where(ss => ss.business_id == ls.business_id).Select(ss => ss.business_name).FirstOrDefault();
                */
                
            }

            //Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, "started session_id: " + session_id, session_id, user_name);            
            Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, session_id, session_id, business_id, "", LogSession, (long)Enums.LogEventType.SESSION_START, null, now, null, true);
            return session_id.Trim();
        }

        private long xCloseSession(string session_id)
        {
            if (String.IsNullOrEmpty(session_id))
                return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;

            DateTime now = DateTime.Now;
            LogSession LogSession = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                log_session ls = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).FirstOrDefault();
                if (ls == null)
                    return (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND;
                if (ls.state == 0)
                {
                    //dbContext.log_session.Remove(ls);
                    ls.state = 1;
                    ls.date_end = now;
                }
                else
                    return (long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED;

                dbContext.SaveChanges();

                LogSession = new BonusCardLib.LogSession(ls);

                //Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, "closed session_id: " + session_id, session_id, ls.user_name); 
                Loggly.Info(System.Reflection.MethodBase.GetCurrentMethod().Name, session_id, session_id, LogSession.business_id, "", LogSession, (long)Enums.LogEventType.SESSION_CLOSE, null, now, null, true);
            }

            return 0;
        }

        private bool IsValidSession(string session_id)
        {
            LogSession ls = xGetActiveSession(session_id);
            if ((ls == null) || (ls.error != null))
                return false;
            else
                return true;
        }

        private bool IsValidSession(LogSession session)
        {
            if ((session == null) || (session.error != null))
                return false;
            else
                return true;
        }

        private LogSession xGetActiveSession(string session_id)
        {
            log_session ls = null;            
            DateTime today = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            using (AuMainDb dbContext = new AuMainDb())
            {
                ls = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).FirstOrDefault();
                if (ls.state != 0)
                    return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED, "Сессия закрыта") };

                DateTime session_day = new DateTime(ls.date_beg.Year, ls.date_beg.Month, ls.date_beg.Day);
                if (session_day < today)
                {
                    long res1 = xCloseSession(session_id);
                    if (res1 != 0)
                        return new LogSession() { error = new ErrInfo(res1, "Ошибка при попытке закрыть просроченную сессию") };
                    else
                        return new LogSession() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_SESSION_CLOSED, "Сессия просрочена и закрыта") };
                }

                /*
                if (ls.business_id.GetValueOrDefault(0) > 0)
                {
                    str = dbContext.business.Where(ss => ss.business_id == ls.business_id).Select(ss => ss.business_name).FirstOrDefault();
                    var cnt = dbContext.card.Where(ss => ss.business_id == ls.business_id).Count();
                    bs = new BusinessSummary() { business_id = (long)ls.business_id, card_cnt = cnt, };                    
                }
                */

                return new LogSession(ls) { business_name = ls.business != null ? ls.business.business_name : "", business_summary = null, };
            }
            
        }

        private LogSession xGetSession(string session_id)
        {
            log_session ls = null;
            List<log_session> ls_list = null;
            string str = "";
            BusinessSummary bs = null;
            using (AuMainDb dbContext = new AuMainDb())
            {
                ls = dbContext.log_session.Where(ss => ss.session_id.ToLower().Trim().Equals(session_id.ToLower().Trim())).FirstOrDefault();
                /*
                if (ls.business_id.GetValueOrDefault(0) > 0)
                {
                    str = dbContext.business.Where(ss => ss.business_id == ls.business_id).Select(ss => ss.business_name).FirstOrDefault();
                    var cnt = dbContext.card.Where(ss => ss.business_id == ls.business_id).Count();
                    bs = new BusinessSummary() { business_id = (long)ls.business_id, card_cnt = cnt, };
                }
                */
                return new LogSession(ls) { business_name = ls.business != null ? ls.business.business_name : "", business_summary = null, };
            }
            
        }

        #endregion

    }
}

