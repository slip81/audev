﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

using CabinetMvc.Models.User;

namespace CabinetMvc.Auth
{
    public class UserIndentity : IIdentity, IUserProvider
    { 
        public UserViewModel User { get; set; }

        public string AuthenticationType { get { return typeof(UserViewModel).ToString(); } }

        public bool IsAuthenticated { get { return User != null; } }

        public string Name 
        { 
            get 
            { 
                if (User != null) 
                { 
                    //return User.Email;
                    return User.UserName;
                } 
                //иначе аноним 
                return "anonym"; 
            } 
        }

        public void Init(string userName, long business_id, long role_id, long cabinet_client_id, UserService serv)
        {
            if (!string.IsNullOrEmpty(userName)) 
            {
                User = serv.GetUser(userName, business_id, role_id, cabinet_client_id); 
            } 
        } 
    }
}