﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;

using CabinetMvc.Models.User;

namespace CabinetMvc.Auth
{
    public interface IUserProvider 
    { 
        UserViewModel User { get; set; } 
    }
}