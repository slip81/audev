﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Principal;



namespace CabinetMvc.Auth
{
    public class CustomAuthentication : IAuthentication 
    { 
        //private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private const string cookieName1 = "_CAB_AUTH_COOKIE_LOGIN";
        private const string cookieName2 = "_CAB_AUTH_COOKIE_BUSINESS";
        private const string cookieName3 = "_CAB_AUTH_COOKIE_ROLE";
        private const string cookieName4 = "_CAB_AUTH_COOKIE_CLIENT";
        public HttpContext HttpContext { get; set; }
        
        // !!!
        // [Inject] 
        // public IRepository Repository { get; set; }
        public UserService userService { get; set; }

        public CustomAuthentication()
        {
            userService = new UserService();
        }         

        public UserViewModel Login(string userName, string Password, bool isPersistent) 
        {
            //UserViewModel retUser = Repository.Login(userName, Password);
            // !!!
            UserViewModel retUser = userService.Login(userName, Password);
            if (retUser != null) 
            {
                CreateCookie(retUser.UserName, retUser.BusinessId.ToString(), retUser.RoleId.ToString(), retUser.CabClientId.ToString(), isPersistent); 
            } 
            
            return retUser; 
        }

        private void CreateCookie(string userName, string business_id, string role_id, string cab_client_id, bool isPersistent = false) 
        {
            DateTime now = DateTime.Now;
            DateTime now_with_timeout = now.Add(FormsAuthentication.Timeout);

            var ticket1 = new FormsAuthenticationTicket( 1, userName,
                now, now_with_timeout, 
                isPersistent, string.Empty, FormsAuthentication.FormsCookiePath);

            // Encrypt the ticket
            var encTicket1 = FormsAuthentication.Encrypt(ticket1);
            // Create the cookie
            var AuthCookie1 = new HttpCookie(cookieName1) 
            { 
                Value = encTicket1,
                Expires = now_with_timeout,
            };

            var ticket2 = new FormsAuthenticationTicket(1, business_id,
                now, now_with_timeout,
                isPersistent, string.Empty, FormsAuthentication.FormsCookiePath);
            var encTicket2 = FormsAuthentication.Encrypt(ticket2);
            var AuthCookie2 = new HttpCookie(cookieName2)
            {
                Value = encTicket2,
                Expires = now_with_timeout,
            };

            var ticket3 = new FormsAuthenticationTicket(1, role_id,
                now, now_with_timeout,
                isPersistent, string.Empty, FormsAuthentication.FormsCookiePath);
            var encTicket3 = FormsAuthentication.Encrypt(ticket3);
            var AuthCookie3 = new HttpCookie(cookieName3)
            {
                Value = encTicket3,
                Expires = now_with_timeout,
            };

            var ticket4 = new FormsAuthenticationTicket(1, cab_client_id,
                now, now_with_timeout,
                isPersistent, string.Empty, FormsAuthentication.FormsCookiePath);
            var encTicket4 = FormsAuthentication.Encrypt(ticket4);
            var AuthCookie4 = new HttpCookie(cookieName4)
            {
                Value = encTicket4,
                Expires = now_with_timeout
            };
            
            HttpContext.Response.Cookies.Set(AuthCookie1);
            HttpContext.Response.Cookies.Set(AuthCookie2);
            HttpContext.Response.Cookies.Set(AuthCookie3);
            HttpContext.Response.Cookies.Set(AuthCookie4);
        }

        public void LogOut() 
        { 
            var httpCookie1 = HttpContext.Response.Cookies[cookieName1];
            var httpCookie2 = HttpContext.Response.Cookies[cookieName2];
            var httpCookie3 = HttpContext.Response.Cookies[cookieName3];
            var httpCookie4 = HttpContext.Response.Cookies[cookieName4]; 
            if (httpCookie1 != null) 
            { 
                httpCookie1.Value = string.Empty; 
            }
            if (httpCookie2 != null)
            {
                httpCookie2.Value = string.Empty;
            }
            if (httpCookie3 != null)
            {
                httpCookie3.Value = string.Empty;
            }
            if (httpCookie4 != null)
            {
                httpCookie4.Value = string.Empty;
            }
        }

        public bool ChangeBusiness(string userName, long business_id, long role_id, long cab_client_id, bool isPersistent) 
        {
            _currentUser = null;
            CreateCookie(userName, business_id.ToString(), role_id.ToString(), cab_client_id.ToString(), isPersistent); 
            return true;
        }

        private IPrincipal _currentUser;        
        public IPrincipal CurrentUser 
        {
            get 
            { 
                if (_currentUser == null) 
                { 
                    try 
                    { 
                        HttpCookie authCookie1 = HttpContext.Request.Cookies.Get(cookieName1);
                        HttpCookie authCookie2 = HttpContext.Request.Cookies.Get(cookieName2);
                        HttpCookie authCookie3 = HttpContext.Request.Cookies.Get(cookieName3);
                        HttpCookie authCookie4 = HttpContext.Request.Cookies.Get(cookieName4);
                        if ((authCookie1 != null) && (!String.IsNullOrEmpty(authCookie1.Value)) && (authCookie2 != null) && (authCookie3 != null) && (authCookie4 != null))
                        { 
                            FormsAuthenticationTicket ticket1 = FormsAuthentication.Decrypt(authCookie1.Value);
                            var user_name = ticket1.Name;
                            FormsAuthenticationTicket ticket2 = FormsAuthentication.Decrypt(authCookie2.Value);
                            long business_id = 0;
                            long.TryParse(ticket2.Name, out business_id);
                            FormsAuthenticationTicket ticket3 = FormsAuthentication.Decrypt(authCookie3.Value);
                            long role_id = 0;
                            long.TryParse(ticket3.Name, out role_id);
                            FormsAuthenticationTicket ticket4 = FormsAuthentication.Decrypt(authCookie4.Value);
                            long cab_client_id = 0;
                            long.TryParse(ticket4.Name, out cab_client_id);
                            //
                            _currentUser = new UserProvider(user_name, business_id, role_id, cab_client_id, userService); 
                        } 
                        else 
                        { 
                            _currentUser = new UserProvider(null, 0, 0, 0, null); 
                        } 
                    } 
                    catch (Exception ex) 
                    { 
                        //logger.Error("Failed authentication: " + ex.Message); 
                        _currentUser = new UserProvider(null, 0, 0, 0, null); 
                    } 
                } 
                return _currentUser; 
            } 
        } 
 
}
}