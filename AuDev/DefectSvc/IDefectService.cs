﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.ServiceModel.Web;
using AuDev.Common.WCF;

namespace DefectSvc
{
    [ServiceContract(Namespace="http://services.aurit.ru/esn/")]
    public interface IDefectService
    {
        #region Defect

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Скачивает с сайта Росздравнадзора список брака за указанный период")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DefectList. Основные атрибуты DefectList:"
            + "Defect_list - список строк брака"            
            )]
        DefectList DownloadDefectList([WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password,
            [WsdlParamOrReturnDocumentation("Начало диапазона скачивания")]DateTime? date_beg,
            [WsdlParamOrReturnDocumentation("Конец диапазона скачивания")]DateTime? date_end,
            [WsdlParamOrReturnDocumentation("True - вместе со строками брака скачиваются письма")]bool download_body
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает скачанный список брака за указанный период")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DefectList. Основные атрибуты DefectList:"
            + "Defect_list - список строк брака"
            )]
        DefectList GetDefectList([WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password,
            [WsdlParamOrReturnDocumentation("Начало диапазона скачивания")]DateTime? date_beg,
            [WsdlParamOrReturnDocumentation("Конец диапазона скачивания")]DateTime? date_end
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает XML с новыми строками списка брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DefectXml. Основные атрибуты DefectXml:"
            + "Data - XML с новыми строками списка брака, RecordCount - кол-во строк в XML, RequestDate - дата-время запроса списка новых строк"
            + ", RequestId - код запроса, IsArchive - true если XML запакован в ZIP (в этом методе всегда false)"
            )]
        DefectXml GetDefectXml([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );


        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает XML со всеми текущими строками списка брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DefectXml. Основные атрибуты DefectXml:"
            + "Data - XML со всеми текущими строками списка брака, RecordCount - в этом методе всегда 0, RequestDate - дата-время запроса списка строк"
            + ", RequestId - код запроса, IsArchive - true если XML запакован в ZIP (в этом методе всегда false)"
            )]
        DefectXml GetDefectXmlInit([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает запакованный в ZIP XML со всеми текущими строками списка брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DefectXml. Основные атрибуты DefectXml:"
            + "Data - XML со всеми текущими строками списка брака, RecordCount - в этом методе всегда 0, RequestDate - дата-время запроса списка строк"
            + ", RequestId - код запроса, IsArchive - true если XML запакован в ZIP (в этом методе всегда true)"
            )]
        DefectXml GetDefectXmlInit_Arc([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает справочник статусов строк брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DefectStatusList - список элементов DefectStatus. Атрибуты DefectStatus:"
            + "status_id -код статуса, status_name - наименование статуса"            
            )]
        DefectStatusList GetDefectStatusList();

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает справочник источников строк брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DefectSourceList - список элементов DefectSource. Атрибуты DefectSource:"
            + "source_id - код источника, source_name - наименование источника"
            )]
        DefectSourceList GetDefectSourceList();

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает последний успешный (подтвержденный) запрос на скачивание брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DefectRequest. Атрибуты DefectRequest:"
            + "date_request - дата-время последнего скачивания брака, date_letter - дата последнего письма"
            )]
        DefectRequest GetLastDefectRequest([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает информацию о новых строчках в списке брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект CheckNewDefectResult. Атрибуты CheckNewDefectResult:"
            + "cnt - кол-во новых строк, date_letter - дата последнего письма"
            )]
        CheckNewDefectResult CheckNewDefect([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );

        #endregion

        #region Document

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает список писем о браке за указанный период")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DocumentList - список элементов Document. Основные атрибуты Document:"
            + "document_id - код документа, document_name - наименование письма, document_num - номер письма, accept_date - дата письма"
            + ", body - содержимое письма, link - ссылка на скачивание письма"
            )]
        DocumentList GetDocumentList([WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password,
            [WsdlParamOrReturnDocumentation("Начало диапазона скачивания")]DateTime? date_beg,
            [WsdlParamOrReturnDocumentation("Конец диапазона скачивания")]DateTime? date_end
            );
        
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает указанное письмо о браке")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект Document. Основные атрибуты:"
            + "document_id - код документа, document_name - наименование письма, document_num - номер письма, accept_date - дата письма"
            + ", body - содержимое письма, link - ссылка на скачивание письма"
            )]
        Document GetDocument([WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password,
            [WsdlParamOrReturnDocumentation("Код письма")]long document_id
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает справочник типов писем")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект DocumentTypeList - список элементов DocumentType. Атрибуты DocumentType:"
            + "type_id - код типа, type_name - наименование типа"
            )]
        DocumentTypeList GetDocumentTypeList();

        #endregion
        
        #region Request

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает список запросов на скачивание брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект RequestList - список элементов Request. Основные атрибуты Request:"
            + "request_id - код запроса, request_date - дата запроса, state - состочние запроса (0 - подтвержден, 1 - нет), record_cnt - кол-во скачанных строк"
            + ", request_type - тип запроса (0 - только новое, 1 - все строки), commit_date - дата-время подтверждения запроса"
            )]
        RequestList GetRequestList([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает список неподтвержденных запросов на скачивание брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект RequestList - список элементов Request. Основные атрибуты Request:"
            + "request_id - код запроса, request_date - дата запроса, state - состочние запроса (0 - подтвержден, 1 - нет), record_cnt - кол-во скачанных строк"
            + ", request_type - тип запроса (0 - только новое, 1 - все строки), commit_date - дата-время подтверждения запроса"
            )]
        RequestList GetActiveRequestList([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Подтверждает запрос на скачивание брака")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект Request. Основные атрибуты:"
            + "request_id - код запроса, request_date - дата запроса, state - состочние запроса (0 - подтвержден, 1 - нет), record_cnt - кол-во скачанных строк"
            + ", request_type - тип запроса (0 - только новое, 1 - все строки), commit_date - дата-время подтверждения запроса"
            )]
        Request CommitRequest([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num,
            [WsdlParamOrReturnDocumentation("Код запроса для подтверждения")]long request_id
            );

        #endregion
     
        #region Ved

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Скачивает с определенного источника госреестр ЖНВЛП")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект ServiceOperationResult. Атрибуты:"
            + "Id - код результата (0 - успех, > 0 - код ошибки), error - объект типа ErrInfo с кодом и текстом ошибки (если null - ошибки нет), Message - сообщение"
            )]
        ServiceOperationResult Ved_DownloadList_Gos([WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Скачивает с определенного источника реестр ЖНВЛП указанного региона")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект ServiceOperationResult. Атрибуты:"
            + "Id - код результата (0 - успех, > 0 - код ошибки), error - объект типа ErrInfo с кодом и текстом ошибки (если null - ошибки нет), Message - сообщение"
            )]
        ServiceOperationResult Ved_DownloadList_Region([WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password,
            [WsdlParamOrReturnDocumentation("Код региона")]int region_id
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Скачивает гореестр и все определенные региональные реестры ЖНВЛП")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект ServiceOperationResult. Атрибуты:"
            + "Id - код результата (0 - успех, > 0 - код ошибки), error - объект типа ErrInfo с кодом и текстом ошибки (если null - ошибки нет), Message - сообщение"
            )]
        ServiceOperationResult Ved_DownloadList_All([WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password);

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает XML со списками ЖВНЛП из реестра региона указанного пользователя")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект VedXmlList. Атрибуты VedXmlList:"
            + "VedXmlMain - XML с основным списком ЖВНЛП, VedXmlOut - XML со списком исключенных из ЖВНЛП, VedXmlError - XML со списком несопоставленных позиций ЖВНЛП, "
            + "RequestId - код запроса (его нужно использовать в методе подтверждения запроса), RequestDate - дата запроса"
            )]
        VedXmlList Ved_GetXmlList_Region(
            [WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Если True - вернется весь список ЖВНЛП, иначе - только новые данные, полученные почле последнего подтвержденного пользователем запроса")]bool getAll
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает XML со списками ЖВНЛП из госреестра")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект VedXmlList. Атрибуты VedXmlList:"
            + "VedXmlMain - XML с основным списком ЖВНЛП, VedXmlOut - XML со списком исключенных из ЖВНЛП, VedXmlError - XML со списком несопоставленных позиций ЖВНЛП (в этом методе - он всегда пуст), "
            + "RequestId - код запроса (его нужно использовать в методе подтверждения запроса), RequestDate - дата запроса"
            )]
        VedXmlList Ved_GetXmlList_Gos(
            [WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Если True - вернется весь список ЖВНЛП, иначе - только новые данные, полученные почле последнего подтвержденного пользователем запроса")]bool getAll
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Подтверждение запроса на скачивание списка ЖВНЛП из реестра региона указанного пользователя")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект Request. Основные атрибуты Request:"
            + "request_id - код запроса, request_date - дата запроса, record_cnt - количество строк в основном XML, state - статус запроса (0 - не подтвержден, 1 - подтвержден)"            
            )]
        Request Ved_CommitRequest_Region(
            [WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Код запроса, который нужно подтвердить. Нужно брать из выходных параметров метода Ved_GetXmlList_Region")]long request_id
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Подтверждение запроса на скачивание списка ЖВНЛП из госреестра")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект Request. Основные атрибуты Request:"
            + "request_id - код запроса, request_date - дата запроса, record_cnt - количество строк в основном XML, state - статус запроса (0 - не подтвержден, 1 - подтвержден)"
            )]
        Request Ved_CommitRequest_Gos(
            [WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Код запроса, который нужно подтвердить. Нужно брать из выходных параметров метода Ved_GetXmlList_Gos")]long request_id
            );

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Проверка, есть ли обновление в списках ЖВНЛП в реестре региона указанного пользователя")]
        [return: WsdlParamOrReturnDocumentation("Возвращает: 1 - есть обновление, 0 - нет обновления, >= 100 - код ошибки"            
            )]
        int Ved_IsExistsNew(
            [WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo
            );

        #endregion

        #region  GR

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Скачивает госрреестр предельных отпускных цен с сайта Росминздрава")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект ServiceOperationResult. Атрибуты:"
            + "Id - код результата (0 - успех, > 0 - код ошибки), error - объект типа ErrInfo с кодом и текстом ошибки (если null - ошибки нет), Message - сообщение"
            )]
        ServiceOperationResult GR_DownloadList(
            [WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password
            );

        #endregion

        #region Medical

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Скачивает с сайта Росздравнадзора список медицинских изделий за указанный период")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект MedicalList. Основные атрибуты MedicalList:"
            + "Medical_list - список строк мед. изделий"
            )]
        MedicalList DownloadMedicalList([WsdlParamOrReturnDocumentation("Пароль для запуска метода")]string password,
            [WsdlParamOrReturnDocumentation("Начало диапазона скачивания")]DateTime? date_beg,
            [WsdlParamOrReturnDocumentation("Конец диапазона скачивания")]DateTime? date_end
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает XML с новыми строками списка мед. изделий")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект MedicalXml. Основные атрибуты MedicalXml:"
            + "Data - XML с новыми строками списка мед. изделий, RecordCount - кол-во строк в XML, RequestDate - дата-время запроса списка новых строк"
            + ", RequestId - код запроса, IsArchive - true если XML запакован в ZIP (в этом методе всегда false)"
            )]
        MedicalXml GetMedicalXml([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает запакованный в ZIP XML со всеми текущими строками списка мед. изделий")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект MedicalXml. Основные атрибуты MedicalXml:"
            + "Data - XML со всеми текущими строками списка мед. изделий, RecordCount - в этом методе всегда 0, RequestDate - дата-время запроса списка строк"
            + ", RequestId - код запроса, IsArchive - true если XML запакован в ZIP (в этом методе всегда true)"
            )]
        MedicalXml GetMedicalXmlInit([WsdlParamOrReturnDocumentation("Логин пользователя")]string login,
            [WsdlParamOrReturnDocumentation("Идентификатор рабочего места пользователя")]string workplace,
            [WsdlParamOrReturnDocumentation("Номер версии программы")]string version_num
            );

        #endregion

        #region GetErrorCodeList

        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает список всех возможных кодов ошибок")]
        [return: WsdlParamOrReturnDocumentation("Возвращает коллекцию элементов с атрибутами:"
            + "Id - код ошибки, Name - описание ошибки"
            )]
        List<ErrorCode> GetErrorCodeList();

        #endregion
    }
}
