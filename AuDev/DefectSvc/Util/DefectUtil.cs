﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Data.EntityClient;
using System.Configuration;
using System.Globalization;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuDev.Common.Util;

namespace DefectSvc
{    
    public static class DefectUtil
    {
        public static string GetCabinetDbConnectionString()
        {
            string entityConnectionString = ConfigurationManager.ConnectionStrings["CabinetDb"].ConnectionString;
            return new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;
        }

        public static XElement GetDefectXmlItem(vw_defect def)
        {
            XElement xml = new XElement("Defect",
               new XAttribute("full_name", String.IsNullOrEmpty(def.full_name) ? "_" : def.full_name),
               new XAttribute("producer", String.IsNullOrEmpty(def.producer) ? "_" : def.producer),
               new XAttribute("country", String.IsNullOrEmpty(def.country_name) ? "_" : def.country_name),
               new XAttribute("series", String.IsNullOrEmpty(def.series) ? "_" : def.series),
               new XAttribute("scope", String.IsNullOrEmpty(def.scope) ? "_" : def.scope),
               new XAttribute("defect_type", String.IsNullOrEmpty(def.defect_type) ? "_" : def.defect_type),
               new XAttribute("defect_status_name", String.IsNullOrEmpty(def.status_name) ? "_" : def.status_name),
               new XAttribute("defect_status_text", String.IsNullOrEmpty(def.defect_status_text) ? "_" : def.defect_status_text),
               new XAttribute("doc_num", String.IsNullOrEmpty(def.document_num) ? "_" : def.document_num),
               new XAttribute("doc_accept_date", def.accept_date.HasValue ? def.accept_date : new DateTime(1900, 1, 1)),
               new XAttribute("doc_link", String.IsNullOrEmpty(def.link) ? "_" : def.link),
               new XAttribute("comm_name", String.IsNullOrEmpty(def.comm_name) ? "_" : def.comm_name),
               new XAttribute("pack_name", String.IsNullOrEmpty(def.pack_name) ? "_" : def.pack_name),
               new XAttribute("labaratory", "_"),
               new XAttribute("distributor", "_"),
               new XAttribute("id", def.defect_id),
               new XAttribute("parent_id", def.parent_defect_id.GetValueOrDefault(0))
           );

            return xml;
        }

        public static XElement GetMedicalXmlItem(vw_medical med)
        {
            XElement xml = new XElement("Medical",
               new XAttribute("full_name", String.IsNullOrWhiteSpace(med.medical_name) ? "_" : med.medical_name),
               new XAttribute("producer", String.IsNullOrWhiteSpace(med.producer) ? "_" : med.producer),
               new XAttribute("publish_date", med.publish_date.HasValue ? med.publish_date : new DateTime(1900, 1, 1)),
               new XAttribute("reg_num", String.IsNullOrWhiteSpace(med.reg_num) ? "_" : med.reg_num),
               new XAttribute("reg_date", med.reg_date.HasValue ? med.reg_date : new DateTime(1900, 1, 1)),

               new XAttribute("doc_num", String.IsNullOrWhiteSpace(med.document_num) ? "_" : med.document_num),
               new XAttribute("doc_date", med.document_create_date.HasValue ? med.document_create_date : new DateTime(1900, 1, 1)),
               new XAttribute("doc_subject", String.IsNullOrWhiteSpace(med.document_type_name) ? "_" : med.document_type_name),
               new XAttribute("doc_link", String.IsNullOrWhiteSpace(med.document_link) ? "_" : med.document_link),

               new XAttribute("add_doc_num", String.IsNullOrWhiteSpace(med.add_document_num) ? "_" : med.add_document_num),
               new XAttribute("add_doc_date", med.add_document_create_date.HasValue ? med.add_document_create_date : new DateTime(1900, 1, 1)),
               new XAttribute("add_doc_link", String.IsNullOrWhiteSpace(med.add_document_link) ? "_" : med.add_document_link),

               new XAttribute("id", med.medical_id)               
           );

            return xml;
        }

        public static XElement GetVedXmlItem(vw_ved_reestr_item item)
        {
            XElement xml = new XElement("Ved",
               new XAttribute("item_code", item.item_code.ToString()),
               new XAttribute("mnn", String.IsNullOrEmpty(item.mnn) ? "_" : item.mnn),
               new XAttribute("comm_name", String.IsNullOrEmpty(item.comm_name) ? "_" : item.comm_name),
               new XAttribute("producer", String.IsNullOrEmpty(item.producer) ? "_" : item.producer),
               new XAttribute("pack_name", String.IsNullOrEmpty(item.pack_name) ? "_" : item.pack_name),
               new XAttribute("pack_count", item.pack_count.GetValueOrDefault(0)),
               new XAttribute("price_for_init_pack", item.price_for_init_pack ? "Да" : "Нет"),
               new XAttribute("reg_num", String.IsNullOrEmpty(item.reg_num) ? "_" : item.reg_num),
               new XAttribute("price_reg_date", String.IsNullOrEmpty(item.price_reg_date) ? "_" : item.price_reg_date),
               new XAttribute("price_reg_ndoc", String.IsNullOrEmpty(item.price_reg_ndoc) ? "_" : item.price_reg_ndoc),
               new XAttribute("ean13", String.IsNullOrEmpty(item.ean13) ? "_" : item.ean13),
               new XAttribute("limit_price", !item.limit_price.HasValue ? "_" : item.limit_price.ToString()),
               new XAttribute("limit_opt_incr", "_"),
               new XAttribute("limit_rozn_incr", "_"),
               new XAttribute("limit_rozn_price", "_"),
               new XAttribute("limit_rozn_price_with_nds", "_"),
               new XAttribute("out_date", "_"),
               new XAttribute("out_reazon", "_")
           );

            return xml;
        }

        public static XElement GetVedXmlItem(vw_ved_reestr_item_out item)
        {
            XElement xml = new XElement("Ved",
               new XAttribute("item_code", item.item_code.ToString()),
               new XAttribute("mnn", String.IsNullOrEmpty(item.mnn) ? "_" : item.mnn),
               new XAttribute("comm_name", String.IsNullOrEmpty(item.comm_name) ? "_" : item.comm_name),
               new XAttribute("producer", String.IsNullOrEmpty(item.producer) ? "_" : item.producer),
               new XAttribute("pack_name", String.IsNullOrEmpty(item.pack_name) ? "_" : item.pack_name),
               new XAttribute("pack_count", item.pack_count.GetValueOrDefault(0)),
               new XAttribute("price_for_init_pack", item.price_for_init_pack ? "Да" : "Нет"),
               new XAttribute("reg_num", String.IsNullOrEmpty(item.reg_num) ? "_" : item.reg_num),
               new XAttribute("price_reg_date", String.IsNullOrEmpty(item.price_reg_date) ? "_" : item.price_reg_date),
               new XAttribute("price_reg_ndoc", String.IsNullOrEmpty(item.price_reg_ndoc) ? "_" : item.price_reg_ndoc),
               new XAttribute("ean13", String.IsNullOrEmpty(item.ean13) ? "_" : item.ean13),
               new XAttribute("limit_price", !item.limit_price.HasValue ? "_" : item.limit_price.ToString()),
               new XAttribute("limit_opt_incr", "_"),
               new XAttribute("limit_rozn_incr", "_"),
               new XAttribute("limit_rozn_price", "_"),
               new XAttribute("limit_rozn_price_with_nds", "_"),
               new XAttribute("out_date", !item.out_date.HasValue ? "_" : ((DateTime)item.out_date).ToString("dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture)),
               new XAttribute("out_reazon", String.IsNullOrEmpty(item.out_reazon) ? "_" : item.out_reazon)
           );

            return xml;
        }

        public static XElement GetVedXmlItem(vw_ved_reestr_region_item item)
        {
            XElement xml = new XElement("Ved",
               new XAttribute("item_code", item.item_code.ToString()),
               new XAttribute("mnn", String.IsNullOrEmpty(item.mnn) ? "_" : item.mnn),
               new XAttribute("comm_name", String.IsNullOrEmpty(item.comm_name) ? "_" : item.comm_name),
               new XAttribute("producer", String.IsNullOrEmpty(item.producer) ? "_" : item.producer),
               new XAttribute("pack_name", String.IsNullOrEmpty(item.pack_name) ? "_" : item.pack_name),
               new XAttribute("pack_count", item.pack_count.GetValueOrDefault(0)),
               new XAttribute("price_for_init_pack", item.price_for_init_pack ? "Да" : "Нет"),
               new XAttribute("reg_num", String.IsNullOrEmpty(item.reg_num) ? "_" : item.reg_num),
               new XAttribute("price_reg_date", String.IsNullOrEmpty(item.price_reg_date) ? "_" : item.price_reg_date),
               new XAttribute("price_reg_ndoc", String.IsNullOrEmpty(item.price_reg_ndoc) ? "_" : item.price_reg_ndoc),
               new XAttribute("ean13", String.IsNullOrEmpty(item.ean13) ? "_" : item.ean13),
               new XAttribute("limit_price", !item.limit_price.HasValue ? "_" : item.limit_price.ToString()),
               new XAttribute("limit_opt_incr", !item.limit_opt_incr.HasValue ? "_" : item.limit_opt_incr.ToString()),
               new XAttribute("limit_rozn_incr", !item.limit_rozn_incr.HasValue ? "_" : item.limit_rozn_incr.ToString()),
               new XAttribute("limit_rozn_price", !item.limit_rozn_price.HasValue ? "_" : item.limit_rozn_price.ToString()),
               new XAttribute("limit_rozn_price_with_nds", !item.limit_rozn_price_with_nds.HasValue ? "_" : item.limit_rozn_price_with_nds.ToString()),
               new XAttribute("out_date", "_"),
               new XAttribute("out_reazon", "_")
           );

            return xml;
        }

        public static XElement GetVedXmlItem(vw_ved_reestr_region_item_out item)
        {
            XElement xml = new XElement("Ved",
               new XAttribute("item_code", item.item_code.ToString()),
               new XAttribute("mnn", String.IsNullOrEmpty(item.mnn) ? "_" : item.mnn),
               new XAttribute("comm_name", String.IsNullOrEmpty(item.comm_name) ? "_" : item.comm_name),
               new XAttribute("producer", String.IsNullOrEmpty(item.producer) ? "_" : item.producer),
               new XAttribute("pack_name", String.IsNullOrEmpty(item.pack_name) ? "_" : item.pack_name),
               new XAttribute("pack_count", item.pack_count.GetValueOrDefault(0)),
               new XAttribute("price_for_init_pack", item.price_for_init_pack ? "Да" : "Нет"),
               new XAttribute("reg_num", String.IsNullOrEmpty(item.reg_num) ? "_" : item.reg_num),
               new XAttribute("price_reg_date", String.IsNullOrEmpty(item.price_reg_date) ? "_" : item.price_reg_date),
               new XAttribute("price_reg_ndoc", String.IsNullOrEmpty(item.price_reg_ndoc) ? "_" : item.price_reg_ndoc),
               new XAttribute("ean13", String.IsNullOrEmpty(item.ean13) ? "_" : item.ean13),
               new XAttribute("limit_price", !item.limit_price.HasValue ? "_" : item.limit_price.ToString()),
               new XAttribute("limit_opt_incr", "_"),
               new XAttribute("limit_rozn_incr", "_"),
               new XAttribute("limit_rozn_price", "_"),
               new XAttribute("limit_rozn_price_with_nds", "_"),
               new XAttribute("out_date", !item.out_date.HasValue ? "_" : ((DateTime)item.out_date).ToString("dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture)),
               new XAttribute("out_reazon", String.IsNullOrEmpty(item.out_reazon) ? "_" : item.out_reazon)
           );

            return xml;
        }

        public static int ColumnNum(this List<ved_template_column> templ_columns, Enums.VedColumn col)
        {
            return templ_columns.Where(ss => ss.column_id == (int)col).Select(ss => ss.column_num).FirstOrDefault() - 1;
        }

        public static bool ParseDocumentName(string name, out string num, out string date, out string size, out string error)
        {
            // № 01И-1467/15 от 10.09.2015 290 Кб
            string unified_name = name.Trim().ToLower();
            num = "";
            date = "";
            size = "";
            error = "";

            try
            {
                int ind = unified_name.IndexOf("от");
                if (ind > 0)
                {
                    num = name.Left(ind - 1).Trim();
                }
                if (ind >= 0)
                {
                    date = name.Substring(ind + 3, 10).Trim();
                    int ind_of_size = ind + 3 + 10 + 1;
                    size = name.Substring(ind_of_size).Trim();
                }

                return true;
            }
            catch (Exception ex)
            {
                num = "";
                date = "";
                size = "";
                error = ex.Message;
                return false;
            }
        }

        public static bool ParseDocumentUrl(string name, out string url, out string error)
        {
            url = "";
            error = "";
            try
            {
                /*
                url = XElement.Parse(name)
                    .Descendants("a")
                    .Select(x => x.Attribute("href").Value)
                    .FirstOrDefault();
                
                */

                string regex = @"<(?<Tag_Name>(a)|img)\b[^>]*?\b(?<URL_Type>(?(1)href|src))\s*=\s*(?:""(?<URL>(?:\\""|[^""])*)""|'(?<URL>(?:\\'|[^'])*)')";
                Match match = Regex.Match(name, regex);
                if (match.Success)
                {
                    url = match.Groups[4].Value;
                    return true;
                }
                else
                {
                    error = "Не найден URL с помощью рег. выражения";
                    return false; ;
                }
            }
            catch (Exception ex)
            {
                url = "";
                error = ex.Message;
                return false;
            }

        }

        public static string GetPriceRegDate(string price_reg_date_full)
        {
            if (String.IsNullOrEmpty(price_reg_date_full))
                return "";
            var parts = price_reg_date_full.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ").Trim().Split(' ');
            if (parts == null)
                return "";
            if (parts.Count() < 1)
                return "";

            return String.IsNullOrEmpty(parts[0]) ? "" : parts[0];
        }

        public static string GetPriceRegNum(string price_reg_date_full)
        {
            if (String.IsNullOrEmpty(price_reg_date_full))
                return "";
            var parts = price_reg_date_full.Replace("\r\n", " ").Replace("\r", " ").Replace("\n", " ").Trim().Split(' ');
            if (parts == null)
                return "";
            if (parts.Count() < 2)
                return "";

            var part2 = String.IsNullOrEmpty(parts[1]) ? "" : (parts[1].Trim().StartsWith("(") ? parts[1].TrimStart(new[] { '(' }) : parts[1].Trim());
            part2 = String.IsNullOrEmpty(part2) ? "" : (part2.Trim().EndsWith(")") ? part2.TrimEnd(new[] { ')' }) : part2.Trim());
            return part2;
        }
          
    }
}
