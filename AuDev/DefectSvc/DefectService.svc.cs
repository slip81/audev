﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {
        #region CreateErrDefectBase
        public T CreateErrDefectBase<T>(Exception e)
            where T : DefectBase, new()
        {
            //T res = Activator.CreateInstance<T>();
            T res = new T();
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                )
                );
            return res;
        }
        #endregion

        #region Defect

        public DefectList DownloadDefectList(string password, DateTime? date_beg, DateTime? date_end, bool download_body)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xDownloadDefectList(password, date_beg, date_end, download_body);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в DownloadDefectList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DefectList>(e);
            }
        }

        public DefectList GetDefectList(string password, DateTime? date_beg, DateTime? date_end)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDefectList(password, date_beg, date_end);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDefectList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DefectList>(e);
            }
        }

        public DefectXml GetDefectXml(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDefectXml(login, workplace, version_num);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDefectXml: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DefectXml>(e);
            }
        }

        public DefectXml GetDefectXmlInit(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDefectXmlInit(login, workplace, version_num, false);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDefectXmlInit: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DefectXml>(e);
            }
        }

        public DefectXml GetDefectXmlInit_Arc(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDefectXmlInit(login, workplace, version_num, true);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDefectXmlInit_Arc: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DefectXml>(e);
            }
        }

        public DefectStatusList GetDefectStatusList()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDefectStatusList();
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDefectStatusList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DefectStatusList>(e);
            }
        }

        public DefectSourceList GetDefectSourceList()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDefectSourceList();
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDefectSourceList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DefectSourceList>(e);
            }
        }

        public DefectRequest GetLastDefectRequest(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetLastDefectRequest(login, workplace, version_num);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetLastDefectRequest: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DefectRequest>(e);
            }
        }

        public CheckNewDefectResult CheckNewDefect(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xCheckNewDefect(login, workplace, version_num);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в CheckNewDefect: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<CheckNewDefectResult>(e);
            }
        }

        #endregion

        #region Country (old)
        /*
        public CountryList GetCountryList()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetCountryList();
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetCountryList: " + Util.ErrorMessage(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<CountryList>(e);
            }
        }
        */
        #endregion

        #region Document

        public DocumentList GetDocumentList(string password, DateTime? date_beg, DateTime? date_end)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDocumentList(password, date_beg, date_end);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDocumentList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DocumentList>(e);
            }
        }

        public Document GetDocument(string password, long document_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDocument(password, document_id);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDocument: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<Document>(e);
            }
        }

        public DocumentTypeList GetDocumentTypeList()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDocumentTypeList();
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetDocumentTypeList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<DocumentTypeList>(e);
            }
        }

        #endregion

        #region Request

        public RequestList GetRequestList(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetRequestList(login, workplace, version_num);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetRequestList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<RequestList>(e);
            }
        }

        public RequestList GetActiveRequestList(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetActiveRequestList(login, workplace, version_num);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetActiveRequestList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<RequestList>(e);
            }
        }

        public Request CommitRequest(string login, string workplace, string version_num, long request_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xCommitRequest(login, workplace, version_num, request_id, (int)Enums.LogScope.DEFECT, null);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в CommitRequest: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<Request>(e);
            }
        }

        #endregion


        #region GetErrorCodeList

        public List<ErrorCode> GetErrorCodeList()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetErrorCodeList();
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Ved("Ошибка в GetErrorCodeList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return new List<ErrorCode>();
            }
        }

        #endregion

        #region CreateErrVedBase
        public T CreateErrVedBase<T>(Exception e)
            where T : VedBase, new()
        {
            //T res = Activator.CreateInstance<T>();
            T res = new T();
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                )
                );
            return res;
        }
        #endregion

        #region Ved

        public ServiceOperationResult Ved_DownloadList_Gos(string password)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xVed_DownloadList(password);
            }
            catch (Exception e)
            {                
                string err = GlobalUtil.ExceptionInfo(e);
                Loggly.InsertLog_Ved("Ошибка в Ved_DownloadList_Gos: " + err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, err), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, };
            }
        }

        public ServiceOperationResult Ved_DownloadList_Region(string password, int region_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xVed_DownloadList(password, region_id);
            }
            catch (Exception e)
            {
                string err = GlobalUtil.ExceptionInfo(e);
                Loggly.InsertLog_Ved("Ошибка в Ved_DownloadList_Region: " + err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, err), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, };
            }
        }

        public ServiceOperationResult Ved_DownloadList_All(string password)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xVed_DownloadList_All(password);
            }
            catch (Exception e)
            {
                string err = GlobalUtil.ExceptionInfo(e);
                Loggly.InsertLog_Ved("Ошибка в Ved_DownloadList_All: " + err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, err), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, };
            }
        }

        public VedXmlList Ved_GetXmlList_Region(UserInfo userInfo, bool getAll)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xVed_GetXmlList_Region(userInfo, getAll);
            }
            catch (Exception e)
            {
                string err = GlobalUtil.ExceptionInfo(e);
                Loggly.InsertLog_Ved("Ошибка в Ved_GetXmlList_Region: " + err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);
                return new VedXmlList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, err), VedXmlError = null, VedXmlMain = null, VedXmlOut = null, RequestDate = null, RequestId = 0, };
            }
        }

        public Request Ved_CommitRequest_Region(UserInfo userInfo, long request_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xCommitRequest(userInfo.login, userInfo.workplace, userInfo.version_num, request_id, (int)Enums.LogScope.VED, (int)Enums.RequestScope2.VED_REGION);
            }
            catch (Exception e)
            {
                string err = GlobalUtil.ExceptionInfo(e);
                Loggly.InsertLog_Ved("Ошибка в Ved_CommitRequest_Region: " + err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);                
                return CreateErrDefectBase<Request>(e);
            }
        }

        public VedXmlList Ved_GetXmlList_Gos(UserInfo userInfo, bool getAll)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xVed_GetXmlList_Gos(userInfo, getAll);
            }
            catch (Exception e)
            {
                string err = GlobalUtil.ExceptionInfo(e);
                Loggly.InsertLog_Ved("Ошибка в Ved_GetXmlList_Gos: " + err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);
                return new VedXmlList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, err), VedXmlError = null, VedXmlMain = null, VedXmlOut = null, RequestDate = null, RequestId = 0, };
            }
        }

        public Request Ved_CommitRequest_Gos(UserInfo userInfo, long request_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xCommitRequest(userInfo.login, userInfo.workplace, userInfo.version_num, request_id, (int)Enums.LogScope.VED, null);
            }
            catch (Exception e)
            {
                string err = GlobalUtil.ExceptionInfo(e);
                Loggly.InsertLog_Ved("Ошибка в Ved_CommitRequest_Gos: " + err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);
                return CreateErrDefectBase<Request>(e);
            }
        }

        public int Ved_IsExistsNew(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xVed_IsExistsNew(userInfo);
            }
            catch (Exception e)
            {
                string err = GlobalUtil.ExceptionInfo(e);
                Loggly.InsertLog_Ved("Ошибка в Ved_IsExistsNew: " + err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);
                return (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        #endregion

        #region GR

        public ServiceOperationResult GR_DownloadList(string password)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGR_DownloadList(password);
            }
            catch (Exception e)
            {
                string err = GlobalUtil.ExceptionInfo(e);                
                Loggly.InsertLog_Defect("Ошибка в GR_DownloadList: " + err, (long)Enums.LogEsnType.ERROR, "downloader-gr", null, log_date_beg, null);
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, err), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, };
            }
        }

        #endregion

        #region Medical

        public MedicalList DownloadMedicalList(string password, DateTime? date_beg, DateTime? date_end)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xDownloadMedicalList(password, date_beg, date_end);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в DownloadMedicalList: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<MedicalList>(e);
            }
        }

        public MedicalXml GetMedicalXml(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetMedicalXml(login, workplace, version_num);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetMedicalXml: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<MedicalXml>(e);
            }
        }

        public MedicalXml GetMedicalXmlInit(string login, string workplace, string version_num)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetMedicalXmlInit(login, workplace, version_num);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Defect("Ошибка в GetMedicalXmlInit: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "downloader", null, log_date_beg, null);
                return CreateErrDefectBase<MedicalXml>(e);
            }
        }

        #endregion
    }
}
