﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class Country : DefectBase
    {
        public Country()
        {
            //
        }

        public Country(prep_country item)
        {
            db_country = item;
            country_id = item.country_id;
            name = item.name;
            name_short = item.name_short;
            numeric_code = item.numeric_code;
            ean13_part1 = item.ean13_part1;
            ean13_part2 = item.ean13_part2;
            created_on = item.created_on;
            created_by = item.created_by;
            updated_on = item.updated_on;
            updated_by = item.updated_by;
        }
        
        [DataMember]
        public long country_id { get; set; }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string created_by { get; set; }
        [DataMember]
        public Nullable<System.DateTime> created_on { get; set; }
        [DataMember]
        public string updated_by { get; set; }
        [DataMember]
        public Nullable<System.DateTime> updated_on { get; set; }
        [DataMember]
        public string name_short { get; set; }
        [DataMember]
        public Nullable<long> numeric_code { get; set; }
        [DataMember]
        public Nullable<int> ean13_part1 { get; set; }
        [DataMember]
        public Nullable<int> ean13_part2 { get; set; }

        public prep_country Db_country { get { return db_country; } }
        private prep_country db_country;
    }

    [DataContract]
    public class CountryList : DefectBase
    {
        public CountryList()
            : base()
        {
            //
        }

        public CountryList(List<prep_country> countryList)
        {
            if (countryList != null)
            {
                Country_list = new List<Country>();
                foreach (var item in countryList)
                {
                    Country_list.Add(new Country(item));
                }
            }
        }

        [DataMember]
        public List<Country> Country_list;
    }
}
