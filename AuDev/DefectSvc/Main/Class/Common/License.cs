﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;


namespace DefectSvc
{
    public class License : DefectBase
    {
        public License()
        {
            //
        }
        
        public long LicenseId { get; set; }        
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> DateStart { get; set; }
        public Nullable<System.DateTime> DateEnd { get; set; }
        public string Login { get; set; }
        public string Workplace { get; set; }
        public string VersionNum { get; set; }
        public string SalesName { get; set; }
        public string ClientName { get; set; }
        public Nullable<int> SalesPrefix { get; set; }
        public long ClientId { get; set; }
        public long SalesId { get; set; }
        public long? RegionId { get; set; }
    }

    public class LicenseList : DefectBase
    {
        public LicenseList()
            : base()
        {
            //
        }

        public List<License> License_list;
    }

}
