﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class LogEvent : DefectBase
    {
        public LogEvent()
            : base()
        {
            //
        }

        public LogEvent(log_esn log_esn)
        {
            db_log_esn = log_esn;
            log_id = log_esn.log_id;
            date_beg = log_esn.date_beg;
            mess = log_esn.mess;
            user_name = log_esn.user_name;
            log_event_type_id = log_esn.log_esn_type_id;
            obj_id = log_esn.obj_id;
            scope = log_esn.scope;
            date_end = log_esn.date_end;
            mess2 = log_esn.mess2;
        }

        [DataMember]
        public long log_id { get; set; }
        [DataMember]
        public System.DateTime date_beg { get; set; }
        [DataMember]
        public string mess { get; set; }        
        [DataMember]
        public string user_name { get; set; }
        [DataMember]
        public Nullable<long> log_event_type_id { get; set; }
        [DataMember]
        public Nullable<long> obj_id { get; set; }
        [DataMember]
        public Nullable<int> scope { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }
        [DataMember]
        public string mess2 { get; set; }

        public log_esn Db_log_esn { get { return db_log_esn; } }
        private log_esn db_log_esn;

    }

    [DataContract]
    public class LogEventList : DefectBase
    {
        public LogEventList()
            : base()
        {
            //
        }

        public LogEventList(List<log_esn> logEventList)
        {
            if (logEventList != null)
            {
                LogEvent_list = new List<LogEvent>();
                foreach (var item in logEventList)
                {
                    LogEvent_list.Add(new LogEvent(item));
                }
            }
        }

        [DataMember]
        public List<LogEvent> LogEvent_list;

    }

    [DataContract]
    public class LogEsnType : DefectBase
    {
        public LogEsnType()
            : base()
        {
            //
        }

        public LogEsnType(log_esn_type log_esn_type)
        {
            db_log_esn_type = log_esn_type;
            type_id = log_esn_type.type_id;
            type_name = log_esn_type.type_name;
        }

        [DataMember]
        public long type_id { get; set; }
        [DataMember]
        public string type_name { get; set; }

        public log_esn_type Db_log_esn_type { get { return db_log_esn_type; } }
        private log_esn_type db_log_esn_type;
    }

    [DataContract]
    public class LogEventTypeList : DefectBase
    {
        public LogEventTypeList()
            : base()
        {
            //
        }

        public LogEventTypeList(List<log_esn_type> logEventTypeList)
        {
            if (logEventTypeList != null)
            {
                LogEventType_list = new List<LogEsnType>();
                foreach (var item in logEventTypeList)
                {
                    LogEventType_list.Add(new LogEsnType(item));
                }
            }
        }

        [DataMember]
        public List<LogEsnType> LogEventType_list;

    }
}
