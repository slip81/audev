﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class Request : DefectBase
    {
        public Request()
        {
            //
        }

        public Request(request item)
        {
            db_request = item;
            request_id = item.request_id;
            login = item.login;
            workplace = item.workplace;
            version_num = item.version_num;
            request_date = item.request_date;
            state = item.state;
            record_cnt = item.record_cnt;
            request_type = item.request_type;
            commit_date = item.commit_date;
            scope = item.scope;
            scope2 = item.scope2;
        }
        
        [DataMember]        
        public long request_id { get; set; }
        [DataMember]
        public string login { get; set; }
        [DataMember]
        public string workplace { get; set; }
        [DataMember]
        public string version_num { get; set; }
        [DataMember]
        public Nullable<System.DateTime> request_date { get; set; }
        [DataMember]
        public int state { get; set; }
        [DataMember]
        public Nullable<int> record_cnt { get; set; }
        [DataMember]
        public int request_type { get; set; }
        [DataMember]
        public Nullable<System.DateTime> commit_date { get; set; }
        [DataMember]
        public int scope { get; set; }
        [DataMember]
        public Nullable<int> scope2 { get; set; }

        public request Db_request { get { return db_request; } }
        private request db_request;
    }

    [DataContract]
    public class RequestList : DefectBase
    {
        public RequestList()
            : base()
        {
            //
        }

        public RequestList(List<request> requestList)
        {
            if (requestList != null)
            {
                Request_list = new List<Request>();
                foreach (var item in requestList)
                {
                    Request_list.Add(new Request(item));
                }
            }
        }

        [DataMember]
        public List<Request> Request_list;
    }

}
