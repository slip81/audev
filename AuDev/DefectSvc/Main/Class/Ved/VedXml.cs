﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class VedXml : DefectBase
    {
        public VedXml()
            : base()
        {
            //
        }


        public void StartReestr(long reestr_id, DateTime? reestr_crt_date, string reestr_name)
        {
            Data += "<Reestr id=" + "\"" + reestr_id.ToString() + "\""
                + " date=" + "\"" + (reestr_crt_date.HasValue ? ((DateTime)reestr_crt_date).ToString("yyyy-MM-dd") : "_") + "\""
                + " name=" + "\"" + (String.IsNullOrEmpty(reestr_name) ? "_" : reestr_name.Trim()) + "\""
                + ">"
                ;
        }

        public void EndReestr()
        {
            Data += "</Reestr>"
                ;
        }

        public void AddVedItem(vw_ved_reestr_item item)
        {
            XElement xml = DefectUtil.GetVedXmlItem(item);
            Data += xml;
        }

        public void AddVedItem(vw_ved_reestr_item_out item)
        {
            XElement xml = DefectUtil.GetVedXmlItem(item);
            Data += xml;
        }

        public void AddVedItem(vw_ved_reestr_region_item item)
        {
            XElement xml = DefectUtil.GetVedXmlItem(item);
            Data += xml;
        }

        public void AddVedItem(vw_ved_reestr_region_item_out item)
        {
            XElement xml = DefectUtil.GetVedXmlItem(item);
            Data += xml;
        }

        public void AddDataCompleted()
        {
            var str1 = @"<?xml version=""1.0"" encoding=""windows-1251""?>";
            var str2 = "<root>";
            var str3 = "</root>";
            Data = (str1 + str2 + Data.Trim() + str3).Trim();
        }

        [DataMember]
        public string Data { get; set; }
        [DataMember]
        public int RecordCount { get; set; }
    }

    [DataContract]
    public class VedXmlList : DefectBase
    {
        public VedXmlList()
            : base()
        {
            //
        }

        [DataMember]
        public VedXml VedXmlMain { get; set; }
        [DataMember]
        public VedXml VedXmlOut { get; set; }
        [DataMember]
        public VedXml VedXmlError { get; set; }
        [DataMember]
        public DateTime? RequestDate { get; set; }
        [DataMember]
        public long RequestId { get; set; }
    }
}
