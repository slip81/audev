﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;

namespace DefectSvc
{
    public class VedImportResult
    {
        public VedImportResult()
        {
            //
        }
                
        public long? _reestr_id { get; set; }
        public int? _new_item_cnt { get; set; }
        public int? _out_item_cnt { get; set; }
        public int? _err_item_cnt { get; set; }
    }
}
