﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class VedSource : VedBase
    {
        public VedSource()
        {
            //
        }

        public VedSource(ved_source item)
        {
            db_ved_source = item;
            ved_source_id = item.ved_source_id;
            url_site = item.url_site;
            url_file_main = item.url_file_main;
            url_file_res = item.url_file_res;
            descr = item.descr;            
            arc_type = item.arc_type;
            num = item.num;
        }

        [DataMember]
        public long ved_source_id { get; set; }        
        [DataMember]
        public string url_site { get; set; }
        [DataMember]
        public string url_file_main { get; set; }
        [DataMember]
        public string url_file_res { get; set; }
        [DataMember]
        public string descr { get; set; }
        [DataMember]
        public int arc_type { get; set; }
        [DataMember]
        public int num { get; set; }

        public ved_source Db_ved_source { get { return db_ved_source; } }
        private ved_source db_ved_source;
    }

    [DataContract]
    public class VedSourceList : DefectBase
    {
        public VedSourceList()
            : base()
        {
            //
        }

        public VedSourceList(List<ved_source> vedSourceList)
        {
            if (vedSourceList != null)
            {
                VedSource_list = new List<VedSource>();
                foreach (var item in vedSourceList)
                {
                    VedSource_list.Add(new VedSource(item));
                }
            }
        }

        [DataMember]
        public List<VedSource> VedSource_list;    
    }

}
