﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class Medical : DefectBase
    {
        public Medical()
        {
            //
        }

        public Medical(medical item)
        {
            db_medical = item;
            medical_id = item.medical_id;
            document_id = item.document_id;
            publish_date = item.publish_date;
            medical_name = item.medical_name;
            reg_num = item.reg_num;
            reg_date = item.reg_date;
            producer = item.producer;
            add_document_id = item.add_document_id;
        }
        
        [DataMember]
        public long medical_id { get; set; }
        [DataMember]
        public Nullable<long> document_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> publish_date { get; set; }
        [DataMember]
        public string medical_name { get; set; }
        [DataMember]
        public string reg_num { get; set; }
        [DataMember]
        public Nullable<System.DateTime> reg_date { get; set; }
        [DataMember]
        public string producer { get; set; }
        [DataMember]
        public Nullable<long> add_document_id { get; set; }

        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }


        public medical Db_medical { get { return db_medical; } }
        private medical db_medical;

    }

    [DataContract]
    public class MedicalList : DefectBase
    {
        public MedicalList()
            : base()
        {
            //
        }

        public MedicalList(List<medical> medicalList)
        {
            if (medicalList != null)
            {
                Medical_list = new List<Medical>();
                foreach (var item in medicalList)
                {
                    Medical_list.Add(new Medical(item));
                }
            }
        }

        [DataMember]
        public List<Medical> Medical_list;

        public bool NeedRefreshXml { get; set; }

    }

}
