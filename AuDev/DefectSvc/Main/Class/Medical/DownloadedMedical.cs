﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DefectSvc
{

    [DataContract]
    public class DownloadedMedicals
    {
        [JsonProperty("data")]
        [DataMember]
        public List<DownloadedMedical> Records { get; set; }
        [JsonProperty("draw")]
        [DataMember]
        public string Draw { get; set; }
        [JsonProperty("recordsFiltered")]
        [DataMember]
        public string RecordsFiltered { get; set; }
        [JsonProperty("recordsTotal")]
        [DataMember]
        public string RecordsTotal { get; set; }
    }

    [DataContract]
    public class DownloadedMedical
    {
        [JsonProperty("col1")]
        [DataMember]
        public PrepLabel DocNum { get; set; }
        [JsonProperty("col2")]
        [DataMember]
        public PrepLabel DocDate { get; set; }
        [JsonProperty("col3")]
        [DataMember]
        public PrepLabel PublishDate { get; set; }
        [JsonProperty("col4")]
        [DataMember]
        public PrepLabel Name { get; set; }
        [JsonProperty("col5")]
        [DataMember]
        public PrepLabel RegNum { get; set; }
        [JsonProperty("col6")]
        [DataMember]
        public PrepLabel RegDate { get; set; }
        [JsonProperty("col7")]
        [DataMember]
        public PrepLabel Producer { get; set; }
        [JsonProperty("col8")]
        [DataMember]
        public PrepLabel DocSubject { get; set; }
        [JsonProperty("col9")]
        [DataMember]
        public PrepLabel DocLink { get; set; }
        [JsonProperty("col10")]
        [DataMember]
        public PrepLabel DocNumAdd { get; set; }
        [JsonProperty("col11")]
        [DataMember]
        public PrepLabel DocDateAdd { get; set; }
        [JsonProperty("col12")]
        [DataMember]
        public PrepLabel DocLinkAdd { get; set; }
    }

}
