﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class DefectRequest : DefectBase
    {
        public DefectRequest()
        {
            //
        }
        
        [DataMember]
        public string date_request { get; set; }
        [DataMember]
        public string date_letter { get; set; }
    }
}
