﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class DefectStatus : DefectBase
    {
        public DefectStatus()
        {
            //
        }

        public DefectStatus(defect_status item)
        {
            db_defect_status = item;
            status_id = item.status_id;
            status_name = item.status_name;
        }
        
        [DataMember]
        public int status_id { get; set; }
        [DataMember]
        public string status_name { get; set; }

        public defect_status Db_defect_status { get { return db_defect_status; } }
        private defect_status db_defect_status;
    }

    [DataContract]
    public class DefectStatusList : DefectBase
    {
        public DefectStatusList()
            : base()
        {
            //
        }

        public DefectStatusList(List<defect_status> defectStatusList)
        {
            if (defectStatusList != null)
            {
                DefectStatus_list = new List<DefectStatus>();
                foreach (var item in defectStatusList)
                {
                    DefectStatus_list.Add(new DefectStatus(item));
                }
            }
        }

        [DataMember]
        public List<DefectStatus> DefectStatus_list;
    }
}
