﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class CheckNewDefectResult : DefectBase
    {
        public CheckNewDefectResult()
        {
            //
        }
        
        [DataMember]
        public int cnt { get; set; }
        [DataMember]
        public string date_letter { get; set; }
    }
}
