﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class DocumentType : DefectBase
    {
        public DocumentType()
        {
            //
        }

        public DocumentType(document_type item)
        {
            db_document_type = item;
            type_id = item.type_id;
            type_name = item.type_name;
        }
        
        [DataMember]
        public int type_id { get; set; }
        [DataMember]
        public string type_name { get; set; }

        public document_type Db_document_type { get { return db_document_type; } }
        private document_type db_document_type;
    }

    [DataContract]
    public class DocumentTypeList : DefectBase
    {
        public DocumentTypeList()
            : base()
        {
            //
        }

        public DocumentTypeList(List<document_type> documentTypeList)
        {
            if (documentTypeList != null)
            {
                DocumentType_list = new List<DocumentType>();
                foreach (var item in documentTypeList)
                {
                    DocumentType_list.Add(new DocumentType(item));
                }
            }
        }

        [DataMember]
        public List<DocumentType> DocumentType_list;
    }
}
