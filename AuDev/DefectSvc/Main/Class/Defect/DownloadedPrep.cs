﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DefectSvc
{

    [DataContract]
    public class DownloadedPreps
    {
        [JsonProperty("data")]
        [DataMember]
        public List<DownloadedPrep> Records { get; set; }
        [JsonProperty("draw")]
        [DataMember]
        public string Draw { get; set; }
        [JsonProperty("recordsFiltered")]
        [DataMember]
        public string RecordsFiltered { get; set; }
        [JsonProperty("recordsTotal")]
        [DataMember]
        public string RecordsTotal { get; set; }
    }

    [DataContract]
    public class DownloadedPrep
    {        
        [JsonProperty("col1")]
        [DataMember]
        public PrepLabel CommName { get; set; }        
        [JsonProperty("col2")]
        [DataMember]
        public PrepLabel Pack { get; set; }        
        [JsonProperty("col3")]
        [DataMember]
        public PrepLabel Series { get; set; }        
        [JsonProperty("col4")]
        [DataMember]
        public PrepLabel Producer { get; set; }        
        [JsonProperty("col5")]
        [DataMember]
        public PrepLabel Country { get; set; }        
        [JsonProperty("col6")]
        [DataMember]
        public PrepLabel DefectStatus { get; set; }        
        [JsonProperty("col7")]
        [DataMember]
        public PrepLabel DefectType { get; set; }        
        [JsonProperty("col8")]
        [DataMember]
        public PrepLabel Scope { get; set; }        
        [JsonProperty("col9")]
        [DataMember]
        public PrepLabel Document { get; set; }
    }

    [DataContract]
    public class PrepLabel
    {
        [JsonProperty("label")]
        [DataMember]
        public string ValueShort { get; set; }
        [JsonProperty("title")]
        [DataMember]
        public string ValueFull { get; set; }
    }

}
