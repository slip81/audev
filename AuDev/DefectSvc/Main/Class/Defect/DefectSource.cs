﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class DefectSource : DefectBase
    {
        public DefectSource()
        {
            //
        }

        public DefectSource(defect_source item)
        {
            db_defect_source = item;
            source_id = item.source_id;
            source_name = item.source_name;
        }
        
        [DataMember]
        public int source_id { get; set; }
        [DataMember]
        public string source_name { get; set; }

        public defect_source Db_defect_sources { get { return db_defect_source; } }
        private defect_source db_defect_source;
    }

    [DataContract]
    public class DefectSourceList : DefectBase
    {
        public DefectSourceList()
            : base()
        {
            //
        }

        public DefectSourceList(List<defect_source> defectSourceList)
        {
            if (defectSourceList != null)
            {
                DefectSource_list = new List<DefectSource>();
                foreach (var item in defectSourceList)
                {
                    DefectSource_list.Add(new DefectSource(item));
                }
            }
        }

        [DataMember]
        public List<DefectSource> DefectSource_list;
    }
}
