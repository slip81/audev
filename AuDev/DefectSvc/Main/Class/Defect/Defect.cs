﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class Defect : DefectBase
    {
        public Defect()
        {
            //
        }

        public Defect(defect item)
        {
            db_defect = item;
            defect_id = item.defect_id;
            comm_name = item.comm_name;
            pack_name = item.pack_name;
            full_name = item.full_name;
            series = item.series;
            producer = item.producer;
            defect_status_id = item.defect_status_id;
            defect_type = item.defect_type;
            country_id = item.country_id;
            document_id = item.document_id;
            parent_defect_id = item.parent_defect_id;
            defect_status_text = item.defect_status_text;
            scope = item.scope;
            is_drug = item.is_drug;
            old_id = item.old_id;
            defect_source_id = item.defect_source_id;
            created_on = item.created_on;
            created_by = item.created_by;
            updated_on = item.updated_on;
            updated_by = item.updated_by;
        }
        
        [DataMember]
        public long defect_id { get; set; }
        [DataMember]
        public string comm_name { get; set; }
        [DataMember]
        public string pack_name { get; set; }
        [DataMember]
        public string full_name { get; set; }
        [DataMember]
        public string series { get; set; }
        [DataMember]
        public string producer { get; set; }
        [DataMember]
        public Nullable<int> defect_status_id { get; set; }
        [DataMember]
        public string defect_type { get; set; }
        [DataMember]
        public Nullable<long> country_id { get; set; }
        [DataMember]
        public Nullable<long> document_id { get; set; }
        [DataMember]
        public Nullable<long> parent_defect_id { get; set; }
        [DataMember]        
        public string defect_status_text { get; set; }
        [DataMember]
        public string scope { get; set; }
        [DataMember]
        public short is_drug { get; set; }
        [DataMember]
        public Nullable<int> old_id { get; set; }
        [DataMember]
        public Nullable<int> defect_source_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> created_on { get; set; }
        [DataMember]
        public string created_by { get; set; }
        [DataMember]
        public Nullable<System.DateTime> updated_on { get; set; }
        [DataMember]
        public string updated_by { get; set; }

        public defect Db_defect { get { return db_defect; } }
        private defect db_defect;


    }

    [DataContract]
    public class DefectList : DefectBase
    {
        public DefectList()
            : base()
        {
            //
        }

        public DefectList(List<defect> defectList)
        {
            if (defectList != null)
            {
                Defect_list = new List<Defect>();
                foreach (var item in defectList)
                {
                    Defect_list.Add(new Defect(item));
                }
            }
        }

        [DataMember]
        public List<Defect> Defect_list;

        public bool NeedRefreshXml { get; set; }

    }

}
