﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class DefectXml : DefectBase
    {
        public DefectXml()
            : base()
        {
            //
            IsArchive = false;
        }

        public void AddData(vw_defect def)
        {
            XElement xml = DefectUtil.GetDefectXmlItem(def);            
            Data += xml;
        }

        public void AddDataCompleted()
        {
            //var str1 = @"<?xml version=""1.0"" encoding=""utf-8""?>";
            //var str1 = @"<?xml version=""1.0"" encoding=""win-1251""?>";
            var str1 = @"<?xml version=""1.0"" encoding=""windows-1251""?>";
            var str2 = "<root>";
            var str3 = "</root>";
            Data = (str1 + str2 + Data.Trim() + str3).Trim();
        }

        [DataMember]
        public string Data { get; set; }
        [DataMember]
        public int RecordCount { get; set; }
        [DataMember]
        public DateTime? RequestDate { get; set; }
        [DataMember]
        public long RequestId { get; set; }
        [DataMember]
        public bool IsArchive { get; set; }
        [DataMember]
        public bool IsBinary { get; set; }
        [DataMember]
        public byte[] DataBinary { get; set; }
    }

}
