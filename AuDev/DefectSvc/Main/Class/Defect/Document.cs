﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    [DataContract]
    public class Document : DefectBase
    {
        public Document()
        {
            //
        }

        public Document(document item)
        {
            db_document = item;
            document_id = item.document_id;
            document_name = item.document_name;
            document_num = item.document_num;
            accept_date = item.accept_date;
            create_date = item.create_date;
            link = item.link;
            // только по запросу!
            body = null;
            comment = item.comment;
            document_type_id = item.document_type_id;
            body_size_kb = item.body_size_kb;
            old = item.old;
            created_on = item.created_on;
            created_by = item.created_by;
            updated_on = item.updated_on;
            updated_by = item.updated_by;            
            if (item.document_type != null)
                document_type = new DocumentType(item.document_type);
        }
        
        [DataMember]
        public long document_id { get; set; }
        [DataMember]
        public string document_name { get; set; }
        [DataMember]
        public string document_num { get; set; }
        [DataMember]
        public Nullable<System.DateTime> accept_date { get; set; }
        [DataMember]
        public Nullable<System.DateTime> create_date { get; set; }
        [DataMember]
        public string link { get; set; }
        [DataMember]
        public byte[] body { get; set; }
        [DataMember]
        public string comment { get; set; }
        [DataMember]
        public Nullable<int> document_type_id { get; set; }
        [DataMember]
        public Nullable<decimal> body_size_kb { get; set; }
        [DataMember]
        public Nullable<short> old { get; set; }
        [DataMember]
        public Nullable<System.DateTime> created_on { get; set; }
        [DataMember]
        public string created_by { get; set; }
        [DataMember]
        public Nullable<System.DateTime> updated_on { get; set; }
        [DataMember]
        public string updated_by { get; set; }
        [DataMember]
        public DocumentType document_type { get; set; }

        public document Db_document { get { return db_document; } }
        private document db_document;
    }

    [DataContract]
    public class DocumentList : DefectBase
    {
        public DocumentList()
            : base()
        {
            //
        }

        public DocumentList(List<document> documentList)
        {
            if (documentList != null)
            {
                Document_list = new List<Document>();
                foreach (var item in documentList)
                {
                    Document_list.Add(new Document(item));
                }
            }
        }

        [DataMember]
        public List<Document> Document_list;
    }
}
