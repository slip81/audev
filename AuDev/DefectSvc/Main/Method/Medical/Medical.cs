﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SocialExplorer.IO.FastDBF;
using WinSCP;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
using AuDev.Common.Extensions;
#endregion

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {        
        #region DownloadMedicalList

        private MedicalList xDownloadMedicalList(string password, DateTime? date_beg, DateTime? date_end)
        {
            if (password != "iguana")
                return new MedicalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            DateTime date_beg_int = date_beg.HasValue ? ((DateTime)date_beg).Date : new DateTime(2017, 10, 1);
            DateTime date_end_int = date_end.HasValue ? ((DateTime)date_end).Date : new DateTime(2090, 1, 1);
            
            // некорректный диапазон
            if (date_beg_int > date_end_int)
                return new MedicalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Дата начала больше даты окончания") };
            // максим. диапазон - месяц
            // !!!
            // пока отключим такую проверку
            /*
            DateTime max_date_end = date_beg_int.AddMonths(1);
            if (max_date_end < date_end_int)
                return new MedicalList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Маскимальный диапазон - месяц") };
            */

            var task = xxDownloadMedicalList(date_beg_int, date_end_int);
            var res = task.Result;
            
            if (res.error != null)
            {
                return new MedicalList() { error = res.error };
            }
            
            Task taskMakeFiles = new Task(() =>
            {
                xMakeXml_Medical();
            }
            );
            taskMakeFiles.Start();

            return res;
        }

        private void xMakeXml_Medical()
        {
            MedicalXml res = new MedicalXml() { Data = "", RequestDate = null, RecordCount = 0, error = null, };
            string filePath = @"c:\Release\Defect\File\MedicalXml.xml";
            string zipPath = @"c:\Release\Defect\File\MedicalXml.zip";
            DateTime now = DateTime.Now;
            try
            {
                Encoding enc = Encoding.GetEncoding("windows-1251");
                if (File.Exists(filePath))
                    File.Delete(filePath);
                if (File.Exists(zipPath))
                    File.Delete(zipPath);

                using (var dbContext = new AuMainDb())
                {

                    var vw_medical = dbContext.vw_medical                        
                        .OrderBy(ss => ss.crt_date)
                        .ThenBy(ss => ss.publish_date)
                        .ThenBy(ss => ss.document_id)
                        .ThenBy(ss => ss.medical_id)
                        .ToList();

                    if ((vw_medical != null) && (vw_medical.Count > 0))
                    {
                        using (StreamWriter outfile = new StreamWriter(filePath, true, enc))
                        {
                            outfile.Write(@"<?xml version=""1.0"" encoding=""windows-1251""?><root>");
                            foreach (var item in vw_medical)
                            {
                                outfile.Write(DefectUtil.GetMedicalXmlItem(item));
                            }
                            outfile.Write("</root>");
                        }
                    }
                    else
                    {
                        Loggly.InsertLog_Defect("vw_medical is empty", (long)Enums.LogEsnType.ERROR, "downloader-xml", null, now, null);
                    }

                    // упаковываем XML в ZIP
                    using (ZipArchive archive = ZipFile.Open(zipPath, ZipArchiveMode.Create, enc))
                    {
                        archive.CreateEntryFromFile(filePath, "MedicalXml.xml");
                    }

                    Loggly.InsertLog_Defect("В XML записано " + vw_medical.Count.ToString() + " строк", (long)Enums.LogEsnType.MAKE_MEDICAL_XML, "downloader-xml", null, now, null);
                }
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Defect(GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, "downloader-xml", null, now, null);
                return;
            }
        }

        private async Task<MedicalList> xxDownloadMedicalList(DateTime date_beg, DateTime date_end)
        {
            MedicalList res = new MedicalList() { Medical_list = new List<Medical>() };

            DownloadedMedicals medicals = null;
            List<medical> medical_list = new List<medical>();
            bool needRefreshXml = false;

            string date_beg_param = ((date_beg.Day.ToString().Trim().Length < 2) ? ("0" + date_beg.Day.ToString()) : date_beg.Day.ToString())
                + "." + ((date_beg.Month.ToString().Trim().Length < 2) ? ("0" + date_beg.Month.ToString()) : date_beg.Month.ToString())
                + "." + date_beg.Year.ToString();

            string date_end_param = ((date_end.Day.ToString().Trim().Length < 2) ? ("0" + date_end.Day.ToString()) : date_end.Day.ToString())
                + "." + ((date_end.Month.ToString().Trim().Length < 2) ? ("0" + date_end.Month.ToString()) : date_end.Month.ToString())
                + "." + date_end.Year.ToString();

            using (var client = new HttpClient())
            //using (var client = new WebClient())
            {
                client.DefaultRequestHeaders.ExpectContinue = false;
                

                var values = new Dictionary<string, string>
                {
                   { "draw", "3" },

                   { "columns%5B0%5D%5Bdata%5D", "col1.label" },
                   { "columns%5B0%5D%5Bname%5D", "" },
                   { "columns%5B0%5D%5Bsearchable%5D", "true" },
                   { "columns%5B0%5D%5Borderable%5D", "true" },
                   { "columns%5B0%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B0%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B1%5D%5Bdata%5D", "col2.label" },
                   { "columns%5B1%5D%5Bname%5D", "" },
                   { "columns%5B1%5D%5Bsearchable%5D", "true" },
                   { "columns%5B1%5D%5Borderable%5D", "true" },
                   { "columns%5B1%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B1%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B2%5D%5Bdata%5D", "col3.label" },
                   { "columns%5B2%5D%5Bname%5D", "" },
                   { "columns%5B2%5D%5Bsearchable%5D", "true" },
                   { "columns%5B2%5D%5Borderable%5D", "true" },
                   { "columns%5B2%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B2%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B3%5D%5Bdata%5D", "col4.label" },
                   { "columns%5B3%5D%5Bname%5D", "" },
                   { "columns%5B3%5D%5Bsearchable%5D", "true" },
                   { "columns%5B3%5D%5Borderable%5D", "true" },
                   { "columns%5B3%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B3%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B4%5D%5Bdata%5D", "col5.label" },
                   { "columns%5B4%5D%5Bname%5D", "" },
                   { "columns%5B4%5D%5Bsearchable%5D", "true" },
                   { "columns%5B4%5D%5Borderable%5D", "true" },
                   { "columns%5B4%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B4%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B5%5D%5Bdata%5D", "col6.label" },
                   { "columns%5B5%5D%5Bname%5D", "" },
                   { "columns%5B5%5D%5Bsearchable%5D", "true" },
                   { "columns%5B5%5D%5Borderable%5D", "true" },
                   { "columns%5B5%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B5%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B6%5D%5Bdata%5D", "col7.label" },
                   { "columns%5B6%5D%5Bname%5D", "" },
                   { "columns%5B6%5D%5Bsearchable%5D", "true" },
                   { "columns%5B6%5D%5Borderable%5D", "true" },
                   { "columns%5B6%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B6%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B7%5D%5Bdata%5D", "col8.label" },
                   { "columns%5B7%5D%5Bname%5D", "" },
                   { "columns%5B7%5D%5Bsearchable%5D", "true" },
                   { "columns%5B7%5D%5Borderable%5D", "true" },
                   { "columns%5B7%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B7%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B8%5D%5Bdata%5D", "col9.label" },
                   { "columns%5B8%5D%5Bname%5D", "" },
                   { "columns%5B8%5D%5Bsearchable%5D", "true" },
                   { "columns%5B8%5D%5Borderable%5D", "true" },
                   { "columns%5B8%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B8%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B9%5D%5Bdata%5D", "col10.label" },
                   { "columns%5B9%5D%5Bname%5D", "" },
                   { "columns%5B9%5D%5Bsearchable%5D", "true" },
                   { "columns%5B9%5D%5Borderable%5D", "true" },
                   { "columns%5B9%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B9%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B10%5D%5Bdata%5D", "col11.label" },
                   { "columns%5B10%5D%5Bname%5D", "" },
                   { "columns%5B10%5D%5Bsearchable%5D", "true" },
                   { "columns%5B10%5D%5Borderable%5D", "true" },
                   { "columns%5B10%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B10%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B11%5D%5Bdata%5D", "col12.label" },
                   { "columns%5B11%5D%5Bname%5D", "" },
                   { "columns%5B11%5D%5Bsearchable%5D", "true" },
                   { "columns%5B11%5D%5Borderable%5D", "true" },
                   { "columns%5B11%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B11%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "order%5B0%5D%5Bcolumn%5D", "0" },
                   { "order%5B0%5D%5Bdir%5D", "asc" },
                   { "start", "0" },
                   { "length", "500" },
                   { "search%5Bvalue%5D", "" },
                   { "search%5Bregex%5D", "false" },
                   { "dt_letter_from", date_beg_param.Trim().ToLower() },
                   { "dt_letter_to", date_end_param.Trim().ToLower() },
                };                               

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync("http://www.roszdravnadzor.ru/ajax/services/unreg", content);                
                var result = await response.Content.ReadAsStringAsync();
                medicals = JsonConvert.DeserializeObject<DownloadedMedicals>(result);                
                
            }

            if ((medicals == null) || (medicals.Records == null) || (medicals.Records.Count <= 0))
            {
                return res;
            }

            DateTime min_document_accept_date = new DateTime(2100, 1, 1);
            DateTime max_document_accept_date = new DateTime(2000, 1, 1);
            foreach (var prep in medicals.Records)
            {
                string doc_date_str = (String.IsNullOrEmpty(prep.DocDate.ValueFull) ? (String.IsNullOrEmpty(prep.DocDate.ValueShort) ? null : prep.DocDate.ValueShort.Trim()) : prep.DocDate.ValueFull.Trim());
                DateTime? doc_date = doc_date_str.ToDateTimeStrict("dd.MM.yyyy");
                if ((doc_date.HasValue) && (doc_date < min_document_accept_date))
                {
                    min_document_accept_date = (DateTime)doc_date;
                }
                if ((doc_date.HasValue) && (doc_date > max_document_accept_date))
                {
                    max_document_accept_date = (DateTime)doc_date;
                }
            }

            string url = "";
            string url_err = "";
            bool doc_link_parse_res = false;
            List<document> add_document_list = new List<document>();
            int next_document_type_id = 1;
            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            string user = "downloader";
            using (var dbContext = new AuMainDb())
            {
                // типы документов
                var document_type_list = dbContext.document_type.ToList();
                if (document_type_list == null)
                    document_type_list = new List<document_type>();
                next_document_type_id = document_type_list.Max(ss => ss.type_id) + 1;

                // препараты, привязанные к документам
                var existing_medicals = dbContext.vw_medical
                    .Where(ss => ss.document_accept_date >= min_document_accept_date && ss.document_accept_date <= max_document_accept_date)
                    .ToList();
                if (existing_medicals == null)
                    existing_medicals = new List<vw_medical>();

                foreach (var prep in medicals.Records)
                {
                    string publish_date_str = (String.IsNullOrEmpty(prep.PublishDate.ValueFull) ? (String.IsNullOrEmpty(prep.PublishDate.ValueShort) ? null : prep.PublishDate.ValueShort.Trim()) : prep.PublishDate.ValueFull.Trim());
                    string medical_name = (String.IsNullOrEmpty(prep.Name.ValueFull) ? (String.IsNullOrEmpty(prep.Name.ValueShort) ? null : prep.Name.ValueShort.Trim()) : prep.Name.ValueFull.Trim());
                    string reg_num = (String.IsNullOrEmpty(prep.RegNum.ValueFull) ? (String.IsNullOrEmpty(prep.RegNum.ValueShort) ? null : prep.RegNum.ValueShort.Trim()) : prep.RegNum.ValueFull.Trim());
                    string reg_date_str = (String.IsNullOrEmpty(prep.RegDate.ValueFull) ? (String.IsNullOrEmpty(prep.RegDate.ValueShort) ? null : prep.RegDate.ValueShort.Trim()) : prep.RegDate.ValueFull.Trim());
                    string producer = (String.IsNullOrEmpty(prep.Producer.ValueFull) ? (String.IsNullOrEmpty(prep.Producer.ValueShort) ? null : prep.Producer.ValueShort.Trim()) : prep.Producer.ValueFull.Trim());
                    string document_num = (String.IsNullOrEmpty(prep.DocNum.ValueFull) ? (String.IsNullOrEmpty(prep.DocNum.ValueShort) ? null : prep.DocNum.ValueShort.Trim()) : prep.DocNum.ValueFull.Trim());
                    string document_type_name = (String.IsNullOrEmpty(prep.DocSubject.ValueFull) ? (String.IsNullOrEmpty(prep.DocSubject.ValueShort) ? "[не указан]" : prep.DocSubject.ValueShort.Trim()) : prep.DocSubject.ValueFull.Trim());
                    string document_date_str = (String.IsNullOrEmpty(prep.DocDate.ValueFull) ? (String.IsNullOrEmpty(prep.DocDate.ValueShort) ? null : prep.DocDate.ValueShort.Trim()) : prep.DocDate.ValueFull.Trim());
                    DateTime? document_date = document_date_str.ToDateTimeStrict("dd.MM.yyyy");
                    string document_link = (String.IsNullOrEmpty(prep.DocLink.ValueFull) ? (String.IsNullOrEmpty(prep.DocLink.ValueShort) ? null : prep.DocLink.ValueShort.Trim()) : prep.DocLink.ValueFull.Trim());
                    string document_name = "Информационное письмо " + (!String.IsNullOrWhiteSpace(document_num) ? document_num : "") + " от " + (document_date.HasValue ? document_date.ToStringStrict("dd.MM.yyyy") : "");

                    string add_document_num = (String.IsNullOrEmpty(prep.DocNumAdd.ValueFull) ? (String.IsNullOrEmpty(prep.DocNumAdd.ValueShort) ? null : prep.DocNumAdd.ValueShort.Trim()) : prep.DocNumAdd.ValueFull.Trim());
                    string add_document_date_str = (String.IsNullOrEmpty(prep.DocDateAdd.ValueFull) ? (String.IsNullOrEmpty(prep.DocDateAdd.ValueShort) ? null : prep.DocDateAdd.ValueShort.Trim()) : prep.DocDateAdd.ValueFull.Trim());
                    DateTime? add_document_date = add_document_date_str.ToDateTimeStrict("dd.MM.yyyy");
                    string add_document_link = (String.IsNullOrEmpty(prep.DocLinkAdd.ValueFull) ? (String.IsNullOrEmpty(prep.DocLinkAdd.ValueShort) ? null : prep.DocLinkAdd.ValueShort.Trim()) : prep.DocLinkAdd.ValueFull.Trim());
                    string add_document_name = "Дополнительное письмо " + (!String.IsNullOrWhiteSpace(add_document_num) ? add_document_num : "") + " от " + (add_document_date.HasValue ? add_document_date.ToStringStrict("dd.MM.yyyy") : "");

                    medical_name = medical_name != null ? Regex.Replace(medical_name, @"\s+", " ") : medical_name;
                    reg_num = reg_num != null ? Regex.Replace(reg_num, @"\s+", " ") : reg_num;
                    producer = producer != null ? Regex.Replace(producer, @"\s+", " ") : producer;

                    if (String.IsNullOrWhiteSpace(medical_name))
                        continue;

                    var existing_medical = existing_medicals
                        .Where(ss => ss.medical_name.Trim().ToLower().Equals(medical_name.Trim().ToLower())
                            && ((ss.document_num != null && document_num != null && ss.document_num.Trim().ToLower().Equals(document_num.Trim().ToLower())) || (ss.document_num == null && document_num == null))
                            && ((ss.reg_num != null && reg_num != null && ss.reg_num.Trim().ToLower().Equals(reg_num.Trim().ToLower())) || (ss.reg_num == null && reg_num == null))
                            && ((ss.document_type_name != null && document_type_name != null && ss.document_type_name.Trim().ToLower().Equals(document_type_name.Trim().ToLower())) || (ss.document_type_name == null && document_type_name == null))
                        )
                        .FirstOrDefault();
                    if (existing_medical != null)
                        continue;
                    
                    document_type document_type = null;
                    document document = null;
                    if (!String.IsNullOrWhiteSpace(document_num))
                    {
                        document_type = document_type_list.Where(ss => document_type_name != null && ss.type_name.Trim().ToLower().Equals(document_type_name.Trim().ToLower())).FirstOrDefault();
                        if (document_type == null)
                        {
                            document_type = new document_type();
                            document_type.type_id = next_document_type_id;
                            document_type.type_name = document_type_name.Trim();
                            dbContext.document_type.Add(document_type);
                            document_type_list.Add(document_type);
                            next_document_type_id = next_document_type_id + 1;
                        }

                        url =  "";
                        url_err = "";
                        doc_link_parse_res = DefectUtil.ParseDocumentUrl(document_link, out url, out url_err);
                        if (doc_link_parse_res)
                        {
                            url = "http://roszdravnadzor.ru/services/unreg" + url.Trim();
                        }

                        document = new document();
                        document.document_name = document_name;
                        document.document_num = document_num.Trim();
                        document.accept_date = document_date;
                        document.create_date = document_date;
                        document.link = url;
                        document.document_type = document_type;
                        document.created_on = now;
                        document.created_by = user;
                        document.updated_on = now;
                        document.updated_by = user;                        
                        dbContext.document.Add(document);
                    }

                    document add_document = null;
                    if (!String.IsNullOrWhiteSpace(add_document_num))
                    {
                        add_document = add_document_list.Where(ss => ss.document_type_id != 1
                            && ss.document_num != null && ss.document_num.Trim().ToLower().Equals(add_document_num.Trim().ToLower()) && ss.accept_date == add_document_date)
                            .FirstOrDefault();
                        if (add_document == null)
                        {
                            add_document = dbContext.document.Where(ss => ss.document_type_id != 1
                                && ss.document_num != null && ss.document_num.Trim().ToLower().Equals(add_document_num.Trim().ToLower()) && ss.accept_date == add_document_date)
                                .FirstOrDefault();
                        }
                        if (add_document == null)
                        {
                            url = "";
                            url_err = "";
                            doc_link_parse_res = DefectUtil.ParseDocumentUrl(add_document_link, out url, out url_err);
                            if (doc_link_parse_res)
                            {
                                url = "http://roszdravnadzor.ru/services/unreg" + url.Trim();
                            }

                            add_document = new document();
                            add_document.document_name = add_document_name;
                            add_document.document_num = add_document_num.Trim();
                            add_document.accept_date = add_document_date;
                            add_document.create_date = add_document_date;
                            add_document.link = url;
                            add_document.created_on = now;
                            add_document.created_by = user;
                            add_document.updated_on = now;
                            add_document.updated_by = user;
                            dbContext.document.Add(add_document);
                            add_document_list.Add(add_document);
                        }
                    }

                    
                    medical medical = new medical();
                    medical.document1 = document;
                    medical.publish_date = publish_date_str.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
                    medical.medical_name = medical_name;
                    medical.reg_num = reg_num;
                    medical.reg_date = reg_date_str.ToDateTimeStrict("dd.MM.yyyy");
                    medical.producer = producer;
                    medical.document = add_document;
                    medical.crt_date = now;
                    medical.crt_user = user;
                    medical.upd_date = now;
                    medical.upd_user = user;
                    dbContext.medical.Add(medical);

                    medical_list.Add(medical);
                }

                if (medical_list.Count > 0)
                {
                    needRefreshXml = true;                    
                    dbContext.SaveChanges();
                    res = new MedicalList(medical_list);
                }

                Loggly.InsertLog_Defect("Скачано " + medical_list.Count.ToString() + " новых строк", (long)Enums.LogEsnType.DOWNLOAD_MEDICAL, user, null, now, null);

                res.NeedRefreshXml = needRefreshXml;
             
                return res;
            }
            
        }

        #endregion

        #region GetMedicalXml

        private MedicalXml xGetMedicalXml(string login, string workplace, string version_num)
        {
            request req = null;
            request req_new = null;
            MedicalXml res = new MedicalXml() { Data = "", RequestDate = null, RecordCount = 0, error = null, };

            /*
             проверка лицензии
            */
            License lic = GetLicense(login, workplace, version_num, license_id_DEFECT);
            if (lic.error != null)
            {
                return new MedicalXml() { error = lic.error };
            }

            using (var dbContext = new AuMainDb())
            {
                /*
                    при закачке только нового:
                    1. ищем последний подвержденный request
                    2. если не находим - ругаемся и предлагаем закачать все
                    3. создаем новый request с типом 0 и state = 0
                    4. отсылаем все данные c created_on > request.request_date c прицепеленным request, ждем подтверждения
                */

                req = dbContext.request
                    .Where(ss => ss.scope == (int)Enums.LogScope.MEDICAL
                        && ss.state == 1
                        && ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                    )
                    .OrderByDescending(ss => ss.request_date)
                    .FirstOrDefault();

                if (req == null)
                    return new MedicalXml() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден ни один подтвержденный запрос, необходимо закачать весь список мед. изделий") };

                req_new = new request();
                req_new.scope = (int)Enums.LogScope.MEDICAL;
                req_new.login = login;
                req_new.record_cnt = 0;
                req_new.request_date = DateTime.Now;
                req_new.state = 0;
                req_new.version_num = version_num;
                req_new.workplace = workplace;
                req_new.request_type = 0;

                var vw_medical = dbContext.vw_medical
                    .Where(ss => ss.crt_date > req.request_date)
                    .OrderBy(ss => ss.publish_date).ThenBy(ss => ss.document_id).ThenBy(ss => ss.medical_name).ToList();

                if (vw_medical != null)
                {
                    foreach (var item in vw_medical)
                    {
                        res.AddData(item);
                    }
                    res.AddDataCompleted();
                }

                req_new.record_cnt = (vw_medical == null) ? 0 : vw_medical.Count;
                dbContext.request.Add(req_new);
                dbContext.SaveChanges();

                res.RequestDate = req_new.request_date;
                res.RecordCount = req_new.record_cnt.GetValueOrDefault(0);
                res.RequestId = req_new.request_id;

                return res;
            }
        }

        private MedicalXml xGetMedicalXmlInit(string login, string workplace, string version_num)
        {
            request req_new = null;
            MedicalXml res = new MedicalXml() { Data = "", RequestDate = null, RecordCount = 0, error = null, };

            /*
             проверка лицензии
            */
            License lic = GetLicense(login, workplace, version_num, license_id_DEFECT);
            if (lic.error != null)
            {
                return new MedicalXml() { error = lic.error };
            }

            using (var dbContext = new AuMainDb())
            {
                /*
                при полной закачке:
                    1. не обращаем внимание на существующие request
                    2. создаем новый request с типом 1 и state = 0
                    3. отсылаем все данные с прицепеленным request, ждем подтверждения
                */

                req_new = new request();
                req_new.scope = (int)Enums.LogScope.MEDICAL;
                req_new.login = login;
                req_new.record_cnt = 0;
                req_new.request_date = DateTime.Now;
                req_new.state = 0;
                req_new.version_num = version_num;
                req_new.workplace = workplace;
                req_new.request_type = 1;

                Encoding enc = Encoding.GetEncoding("windows-1251");
                string contents = "";
                byte[] contents_binary = null;
                contents_binary = File.ReadAllBytes(@"c:\Release\Defect\File\MedicalXml.zip");

                res.Data = "";
                res.DataBinary = contents_binary;

                res.IsArchive = true;
                res.IsBinary = true;                

                dbContext.request.Add(req_new);
                dbContext.SaveChanges();

                res.RequestDate = req_new.request_date;
                res.RecordCount = req_new.record_cnt.GetValueOrDefault(0);
                res.RequestId = req_new.request_id;

                return res;
            }
        }

        #endregion        
    }
}
