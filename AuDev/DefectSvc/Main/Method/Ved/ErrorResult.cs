﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;
using System.IO.Compression;
using System.Transactions;

using HtmlAgilityPack;
using LinqToExcel;
using NUnrar;
using NUnrar.Archive;
using NUnrar.Common;

using AuDev.Common.Util;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {

        private ServiceOperationResult Ved_ServiceOperationResultError(DateTime date_beg, string err, long err_code)
        {
            Loggly.InsertLog_Ved(err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, date_beg, null);
            return new ServiceOperationResult()
            {
                error = new ErrInfo(err_code, err),
                Id = err_code,
                Message = "",
            };
        }

        private VedXmlList Ved_VedXmlListError(DateTime date_beg, string err, long err_code, string user_name)
        {
            Loggly.InsertLog_Ved(err, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, null);
            return new VedXmlList()
            {
                error = new ErrInfo(err_code, err),
                VedXmlError = null,
                VedXmlMain = null,
                VedXmlOut = null,
                RequestId = 0,
                RequestDate = null,
            };
        }

    }

}
