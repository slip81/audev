﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;
using System.IO.Compression;
using System.Transactions;
using System.Text;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

using HtmlAgilityPack;
using LinqToExcel;
using NUnrar.Archive;
using NUnrar.Common;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

#endregion

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {

        private readonly string filePath_VedXml_Tmp = @"C:\Release\Ved\File\";
        private readonly string filePath_VedXml_Full = @"C:\Release\Ved\File\Xml\";

        #region Ved_DownloadList_All

        private ServiceOperationResult xVed_DownloadList_All(string password)
        {
            DateTime log_date_beg = DateTime.Now;
            string mess = "";
            string err = "";
            bool ok = false;
            ServiceOperationResult res1 = null;
            try
            {
                res1 = xVed_DownloadList(password, null, false);
                if (res1.error != null)
                {
                    mess += "Ошибка скачивания госреестра: " + res1.Message;
                    mess += System.Environment.NewLine;
                }
                else
                {
                    ok = true;
                    mess += res1.Message;
                    mess += System.Environment.NewLine;
                }

                using (var dbContext = new AuMainDb())
                {
                    var regions = dbContext.ved_source_region.Select(ss => ss.region_id).Distinct().ToList();
                    if ((regions != null) && (regions.Count > 0))
                    {
                        foreach (var region_id in regions)
                        {
                            res1 = xVed_DownloadList(password, region_id, false);
                            if (res1.error != null)
                            {
                                mess += "Ошибка скачивания регионального реестра [код региона " + region_id.ToString() + "]: " + res1.Message;
                                mess += System.Environment.NewLine;
                            }
                            else
                            {
                                ok = true;
                                mess += res1.Message;
                                mess += System.Environment.NewLine;
                            }
                        }
                    }
                    else
                    {
                        mess += "Ошибка скачивания региональных реестров: не найдены источники";
                        mess += System.Environment.NewLine;
                    }
                }
            }
            catch (Exception ex)
            {
                err = "Ошибка в Ved_DownloadList_All: " + GlobalUtil.ExceptionInfo(ex);
                mess += "Ошибка в Ved_DownloadList_All: " + GlobalUtil.ExceptionInfo(ex);
                Loggly.InsertLog_Ved(err, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, log_date_beg, null);
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, mess), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, Message = mess, };
            }

            if (!ok)
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, mess), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, Message = mess, };
            else
                return new ServiceOperationResult() { error = null, Id = 0, Message = mess, };
        }
        
        #endregion

        #region Ved_DownloadList

        private ServiceOperationResult xVed_DownloadList(string password, int? region_id = null, bool makeFilesInNewTask = true)
        {
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            if (password != "iguana")
            {
                return Ved_ServiceOperationResultError(now, "Нет прав", (long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED);
            }

            long source_id = 0;
            string url_site = "";
            string url_file = "";
            string url_file_header = "";
            string url_for_download = "";
            string url_file_a_content = "";
            string arc_type_str = "";
            long main_template_id = 0;
            long out_template_id = 0;
            string tmp_file_name = System.Guid.NewGuid().ToString();
            string tmp_file_full_name = Path.Combine(filePath_VedXml_Tmp, tmp_file_name);
            Enums.ArcType arcType = Enums.ArcType.NONE;            
            List<string> linkedPages = null;
            string linkedPage = null;
            string mess = "";
            string mess2 = "";
            int cnt = 0;
            int cnt2 = 0;
            bool isGosReestr = region_id.GetValueOrDefault(0) <= 0;
            bool isListMain = false;
            bool isListOut = false;

            List<string> worksheetNames = null;
            List<string> worksheetNames_Sorted = null;
            string worksheetName_Main = "";
            string worksheetName_Out = "";
            string worksheetName_Main_orig = "";
            string worksheetName_Out_orig = "";
            string worksheetName_Curr = "";
            int excelRowsCnt = 0;
            int max_ind = 0;

            string mnn = "";
            int mnn_ind = 0;

            string comm_name = "";
            int comm_name_ind = 0;

            string pack_name = "";
            int pack_name_ind = 0;

            string producer = "";
            int producer_ind = 0;

            string reg_num = "";
            int reg_num_ind = 0;

            string price_reg_date = "";
            int price_reg_date_ind = 0;

            string ean13 = "";
            int ean13_ind = 0;

            int? pack_count = 0;
            int pack_count_ind = 0;
            string pack_count_str = "";
            
            decimal? limit_price = 0;
            int limit_price_ind = 0;
            string limit_price_str = "";

            decimal? limit_opt_incr = 0;
            int limit_opt_incr_ind = 0;
            string limit_opt_incr_str = "";

            decimal? limit_rozn_incr = 0;
            int limit_rozn_incr_ind = 0;
            string limit_rozn_incr_str = "";

            decimal? limit_rozn_price = 0;
            int limit_rozn_price_ind = 0;
            string limit_rozn_price_str = "";

            decimal? limit_rozn_price_with_nds = 0;
            int limit_rozn_price_with_nds_ind = 0;
            string limit_rozn_price_with_nds_str = "";

            bool price_for_init_pack = false;
            int price_for_init_pack_ind = 0;
            string price_for_init_pack_str = "";

            DateTime? out_date = null;
            int out_date_ind = 0;
            string out_date_str = "";

            string out_reazon = "";
            int out_reazon_ind = 0;

            int row_num_to_start = 0;
            //int ved_import_result = -1;
            //int ved_import_out_result = -1;
            VedImportResult ved_import_result = null;
            VedImportResult ved_import_out_result = null;
            
            ved_source src = null;
            ved_source_region src_region = null;
            List<ved_reestr_item> reestr_item_list = new List<ved_reestr_item>();;
            tmp_ved_reestr_item tmp_reestr_item = null;
            tmp_ved_reestr_item_out tmp_reestr_item_out = null;
            ExcelQueryFactory excel = null;
            RowNoHeader excelRow = null;
            List<RowNoHeader> excelRows = null;
            ved_template templ_main = null;
            List<ved_template_column> templ_main_columns = null;
            ved_template templ_out = null;
            List<ved_template_column> templ_out_columns = null;
            ved_template templ_curr = null;
            List<ved_template_column> templ_curr_columns = null;
            AuMainDb transactionContext = null;

            int defStartRowNum_Main = 3;
            int defStartRowNum_Out = 1;
            int defStartRowNum_curr = 0;

            int new_item_cnt = 0;
            int out_item_cnt = 0;
            int err_item_cnt = 0;

            int block_num = 0;
            try
            {
                using (var dbContext = new AuMainDb())
                {
                    if (isGosReestr)
                    {
                        mess2 = "";
                        src = dbContext.ved_source.FirstOrDefault();
                        if (src == null)
                        {
                            return Ved_ServiceOperationResultError(now, "Не найден источник для скачивания реестра ЖВНЛП", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                        }

                        source_id = src.ved_source_id;
                        main_template_id = src.template_id.GetValueOrDefault(0);
                        out_template_id = src.out_template_id.GetValueOrDefault(0);
                        url_site = src.url_site;
                        url_file = src.url_file_main;
                        if (String.IsNullOrEmpty(url_file))
                        {
                            url_file = src.url_file_res;
                        }
                        url_file_a_content = src.url_file_a_content;
                        if ((!String.IsNullOrEmpty(url_file_a_content)) && (url_file_a_content.Contains("*")))
                            url_file_a_content = url_file_a_content.Replace("*", "");
                        arc_type_str = src.arc_type.ToString();

                    }
                    else
                    {
                        mess2 = " [код региона: " + region_id.ToString() + "]";
                        src_region = dbContext.ved_source_region.Where(ss => ss.region_id == region_id).FirstOrDefault();
                        if (src_region == null)
                        {
                            return Ved_ServiceOperationResultError(now, "Не найден источник для скачивания реестра ЖВНЛП" + mess2, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                        }

                        source_id = src_region.item_id;
                        main_template_id = src_region.template_id.GetValueOrDefault(0);
                        out_template_id = src_region.out_template_id.GetValueOrDefault(0);
                        url_site = src_region.url_site;
                        url_file = src_region.url_file_main;
                        if (String.IsNullOrEmpty(url_file))
                        {
                            url_file = src_region.url_file_res;
                        }
                        url_file_a_content = src_region.url_file_a_content;
                        if ((!String.IsNullOrEmpty(url_file_a_content)) && (url_file_a_content.Contains("*")))
                            url_file_a_content = url_file_a_content.Replace("*", "");
                        arc_type_str = src_region.arc_type.ToString();
                    }

                    if (main_template_id <= 0)
                    {
                        return Ved_ServiceOperationResultError(now, "В параметрах источника рееста ЖВНЛП не указан шаблон для основных строк реестра" + mess2, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                    }

                    if (out_template_id <= 0)
                    {
                        return Ved_ServiceOperationResultError(now, "В параметрах источника рееста ЖВНЛП не указан шаблон для исключенных строк реестра" + mess2, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                    }

                    if (String.IsNullOrEmpty(url_file))
                    {
                        return Ved_ServiceOperationResultError(now, "В параметрах источника рееста ЖВНЛП не указан адрес ссылки на скачивание файла" + mess2, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                    }

                    if ((String.IsNullOrEmpty(url_site)) && (url_file.Contains("*")))
                    {
                        return Ved_ServiceOperationResultError(now, "В параметрах источника рееста ЖВНЛП не указан адрес сайта для скачивания файла" + mess2, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                    }

                    var boo = Enum.TryParse<Enums.ArcType>(arc_type_str, true, out arcType);
                    if (!boo)
                    {
                        return Ved_ServiceOperationResultError(now, "В параметрах источника рееста ЖВНЛП указан неподдерживаемый тип сжатия файла [" + arc_type_str + "]" + mess2, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                    }


                    int startInd = 0;
                    if (url_file.StartsWith("http://"))
                    {
                        startInd = 7;
                    }
                    else if (url_file.StartsWith("ftp://"))
                    {
                        startInd = 6;
                    }

                    if (url_file.Contains("*"))
                    {
                        url_file_header = url_file.IndexOf("/", startInd) > 0 ? url_file.Left(url_file.IndexOf("/", startInd) + 1) : "";
                        url_file = !String.IsNullOrEmpty(url_file_header) ? url_file.Replace(url_file_header, "") : url_file;
                        var doc = new HtmlWeb().Load(url_site);
                        url_file = url_file.Replace("*", "");
                        linkedPages = doc.DocumentNode.Descendants("a")
                            .Where(ss => (!String.IsNullOrEmpty(url_file_a_content) && ss.InnerText.Contains(url_file_a_content)) || String.IsNullOrEmpty(url_file_a_content))
                            .Select(a => a.GetAttributeValue("href", null))
                            .Where(u => (!String.IsNullOrEmpty(u)) && (u.Contains(url_file)))
                            .ToList()
                            ;

                        if ((linkedPages == null) || (linkedPages.Count <= 0))
                        {
                            return Ved_ServiceOperationResultError(now, "На странице скачивания рееста ЖВНЛП не найдено ссылок на файлы" + mess2, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                        }
                        var linkedPagesCnt = linkedPages.Count;

                        linkedPage = linkedPages.FirstOrDefault();

                        if (linkedPage.StartsWith(url_file_header))
                            url_for_download = linkedPage.TrimStart(new[] { '/' });
                        else
                            url_for_download = url_file_header.TrimEnd(new[] { '/' }) + "/" + linkedPage.TrimStart(new[] { '/' });
                    }
                    else
                    {
                        url_for_download = url_file;
                    }

                    if (File.Exists(tmp_file_full_name))
                    {
                        File.Delete(tmp_file_full_name);
                    }

                    block_num = 1;
                    using (var client = new WebClient())
                    {
                        client.DownloadFile(url_for_download, tmp_file_full_name);
                    }

                    switch (arcType)
                    {
                        case Enums.ArcType.ZIP:
                            var zipArchive = ZipFile.OpenRead(tmp_file_full_name);
                            tmp_file_name = zipArchive.Entries.FirstOrDefault().Name;
                            tmp_file_full_name = Path.Combine(filePath_VedXml_Tmp, tmp_file_name);
                            if (File.Exists(tmp_file_full_name))
                            {
                                File.Delete(tmp_file_full_name);
                            }
                            zipArchive.ExtractToDirectory(filePath_VedXml_Tmp);
                            break;
                        case Enums.ArcType.RAR:
                            RarArchive archive = RarArchive.Open(tmp_file_full_name);
                            RarArchiveEntry entry = archive.Entries.FirstOrDefault();
                            tmp_file_name = entry.FilePath;
                            tmp_file_full_name = Path.Combine(filePath_VedXml_Tmp, tmp_file_name);
                            if (File.Exists(tmp_file_full_name))
                            {
                                File.Delete(tmp_file_full_name);
                            }
                            entry.WriteToFile(tmp_file_full_name, ExtractOptions.Overwrite);
                            break;
                        case Enums.ArcType.SevenZ:
                            return Ved_ServiceOperationResultError(now, "В параметрах источника рееста ЖВНЛП указан неподдерживаемый тип сжатия файла [" + arcType.ToString() + "]" + mess2, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                        default:
                            break;
                    }

                    templ_main = dbContext.ved_template.Where(ss => ss.template_id == main_template_id).FirstOrDefault();
                    templ_main_columns = dbContext.ved_template_column.Where(ss => ss.template_id == templ_main.template_id).ToList();
                    if ((templ_main_columns == null) || (templ_main_columns.Count <= 0))
                    {
                        return Ved_ServiceOperationResultError(now, "Не найдены колонки в шаблоне с кодом " + templ_main.template_id.ToString(), (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                    }

                    templ_out = dbContext.ved_template.Where(ss => ss.template_id == out_template_id).FirstOrDefault();
                    templ_out_columns = dbContext.ved_template_column.Where(ss => ss.template_id == templ_out.template_id).ToList();
                    if ((templ_out_columns == null) || (templ_out_columns.Count <= 0))
                    {
                        return Ved_ServiceOperationResultError(now, "Не найдены колонки в шаблоне с кодом " + templ_main.template_id.ToString(), (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                    }


                    block_num = 2;
                    excel = new ExcelQueryFactory(tmp_file_full_name);
                    excel.ReadOnly = true;

                    worksheetNames = excel.GetWorksheetNames().ToList();

                    if (!String.IsNullOrEmpty(templ_main.list_name))
                    {
                        worksheetName_Main = worksheetNames.FindAll(s => s.IndexOf(templ_main.list_name.Trim(), StringComparison.OrdinalIgnoreCase) >= 0).FirstOrDefault();
                        if (String.IsNullOrEmpty(worksheetName_Main))
                            worksheetName_Main = worksheetNames[1];
                    }
                    else
                    {
                        worksheetName_Main = worksheetNames[1];
                    }

                    if (!String.IsNullOrEmpty(templ_out.list_name))
                    {
                        worksheetName_Out = worksheetNames.FindAll(s => s.IndexOf(templ_out.list_name.Trim(), StringComparison.OrdinalIgnoreCase) >= 0).FirstOrDefault();
                        if (String.IsNullOrEmpty(worksheetName_Out))
                            worksheetName_Out = worksheetNames[0];
                    }
                    else
                    {
                        worksheetName_Out = worksheetNames[0];
                    }

                    worksheetName_Main_orig = worksheetName_Main;
                    worksheetName_Out_orig = worksheetName_Out;
                    worksheetName_Main = "11_" + worksheetName_Main;
                    worksheetName_Out = "12_" + worksheetName_Out;
                    worksheetNames_Sorted = new List<string> { worksheetName_Main, worksheetName_Out };
                    worksheetNames_Sorted = worksheetNames_Sorted.OrderBy(s => s).ToList();

                    block_num = 3;
                    //dbContext.tmp_ved_reestr_item_out.RemoveRange(dbContext.tmp_ved_reestr_item_out);
                    //dbContext.tmp_ved_reestr_item.RemoveRange(dbContext.tmp_ved_reestr_item);
                    //dbContext.SaveChanges();

                }

                foreach (var excelWorksheet in worksheetNames_Sorted)
                {
                    block_num = 4;

                    isListMain = false;
                    isListOut = false;

                    if (String.IsNullOrEmpty(excelWorksheet))
                        continue;

                    if (excelWorksheet.Trim().ToLower().Equals(worksheetName_Main.Trim().ToLower()))
                    {
                        isListMain = true;
                        templ_curr = templ_main;
                        templ_curr_columns = new List<ved_template_column>(templ_main_columns);
                        defStartRowNum_curr = defStartRowNum_Main;
                        worksheetName_Curr = worksheetName_Main_orig;
                    }
                    else if (excelWorksheet.Trim().ToLower().Equals(worksheetName_Out.Trim().ToLower()))
                    {
                        isListOut = true;
                        templ_curr = templ_out;
                        templ_curr_columns = new List<ved_template_column>(templ_out_columns);
                        defStartRowNum_curr = defStartRowNum_Out;
                        worksheetName_Curr = worksheetName_Out_orig;
                    }
                    else continue;

                    block_num = 5;

                    excelRows = excel.WorksheetNoHeader(worksheetName_Curr).ToList();
                    if ((excelRows == null) || (excelRows.Count <= 0))
                    {
                        if (isListMain)
                            return Ved_ServiceOperationResultError(now, "Скачано 0 строк [" + url_for_download + "]" + mess, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
                        else
                            continue;
                    }

                    block_num = 6;

                    excelRowsCnt = excelRows.Count;
                    row_num_to_start = templ_curr.start_row_num > 0 ? templ_curr.start_row_num - 1 : defStartRowNum_curr;

                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0)))
                    {
                        transactionContext = null;
                        try
                        {
                            transactionContext = new AuMainDb();
                            transactionContext.Configuration.AutoDetectChangesEnabled = false;

                            cnt = 0;
                            for (int i = row_num_to_start; i < excelRowsCnt; i++)
                            {
                                block_num = 7;

                                ++cnt;
                                excelRow = excelRows.ElementAtOrDefault(i);
                                max_ind = excelRow.Count - 1;
                                
                                mnn_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.MNN);
                                mnn = ((mnn_ind) >= 0 && (mnn_ind <= max_ind)) ? excelRow[mnn_ind].Cast<string>() : "";

                                comm_name_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.COMM_NAME);
                                comm_name = ((comm_name_ind) >= 0 && (comm_name_ind <= max_ind)) ? excelRow[comm_name_ind].Cast<string>() : "";

                                pack_name_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.PACK_NAME);
                                pack_name = ((pack_name_ind) >= 0 && (pack_name_ind <= max_ind)) ? excelRow[pack_name_ind].Cast<string>() : "";

                                producer_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.PRODUCER);
                                producer = ((producer_ind) >= 0 && (producer_ind <= max_ind)) ? excelRow[producer_ind].Cast<string>() : "";

                                producer_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.PRODUCER);
                                producer = ((producer_ind) >= 0 && (producer_ind <= max_ind)) ? excelRow[producer_ind].Cast<string>() : "";

                                pack_count_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.PACK_COUNT);
                                pack_count_str = ((pack_count_ind) >= 0 && (pack_count_ind <= max_ind)) ? excelRow[pack_count_ind].Cast<string>() : "";
                                pack_count = String.IsNullOrEmpty(pack_count_str) ? null : (int?)int.Parse(pack_count_str.ExceptChars(new[] { ' ' }));

                                limit_price_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.LIMIT_PRICE);
                                limit_price_str = ((limit_price_ind) >= 0 && (limit_price_ind <= max_ind)) ? excelRow[limit_price_ind].Cast<string>() : "";
                                limit_price = String.IsNullOrEmpty(limit_price_str) ? null : (decimal?)decimal.Parse(limit_price_str.ExceptChars(new[] { ' ' }));

                                limit_opt_incr_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.LIMIT_OPT_INCR);
                                limit_opt_incr_str = ((limit_opt_incr_ind) >= 0 && (limit_opt_incr_ind <= max_ind)) ? excelRow[limit_opt_incr_ind].Cast<string>() : "";
                                limit_opt_incr = String.IsNullOrEmpty(limit_opt_incr_str) ? null : (decimal?)decimal.Parse(limit_opt_incr_str.ExceptChars(new[] { ' ' }));

                                limit_rozn_incr_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.LIMIT_ROZN_INCR);
                                limit_rozn_incr_str = ((limit_rozn_incr_ind) >= 0 && (limit_rozn_incr_ind <= max_ind)) ? excelRow[limit_rozn_incr_ind].Cast<string>() : "";
                                limit_rozn_incr = String.IsNullOrEmpty(limit_rozn_incr_str) ? null : (decimal?)decimal.Parse(limit_rozn_incr_str.ExceptChars(new[] { ' ' }));

                                limit_rozn_price_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.LIMIT_ROZN_PRICE);
                                limit_rozn_price_str = ((limit_rozn_price_ind) >= 0 && (limit_rozn_price_ind <= max_ind)) ? excelRow[limit_rozn_price_ind].Cast<string>() : "";
                                limit_rozn_price = String.IsNullOrEmpty(limit_rozn_price_str) ? null : (decimal?)decimal.Parse(limit_rozn_price_str.ExceptChars(new[] { ' ' }));

                                limit_rozn_price_with_nds_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.LIMIT_ROZN_PRICE_WITH_NDS);
                                limit_rozn_price_with_nds_str = ((limit_rozn_price_with_nds_ind) >= 0 && (limit_rozn_price_with_nds_ind <= max_ind)) ? excelRow[limit_rozn_price_with_nds_ind].Cast<string>() : "";
                                limit_rozn_price_with_nds = String.IsNullOrEmpty(limit_rozn_price_with_nds_str) ? null : (decimal?)decimal.Parse(limit_rozn_price_with_nds_str.ExceptChars(new[] { ' ' }));

                                price_for_init_pack_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.PRICE_FOR_INIT_PACK);
                                price_for_init_pack_str = ((price_for_init_pack_ind) >= 0 && (price_for_init_pack_ind <= max_ind)) ? excelRow[price_for_init_pack_ind].Cast<string>() : "";
                                price_for_init_pack = String.IsNullOrEmpty(price_for_init_pack_str) ? false : bool.Parse(price_for_init_pack_str.ExceptChars(new[] { ' ' }));

                                reg_num_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.REG_NUM);
                                reg_num = ((reg_num_ind) >= 0 && (reg_num_ind <= max_ind)) ? excelRow[reg_num_ind].Cast<string>() : "";

                                price_reg_date_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.PRICE_REG_DATE);
                                price_reg_date = ((price_reg_date_ind) >= 0 && (price_reg_date_ind <= max_ind)) ? excelRow[price_reg_date_ind].Cast<string>() : "";

                                ean13_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.EAN13);
                                ean13 = ((ean13_ind) >= 0 && (ean13_ind <= max_ind)) ? excelRow[ean13_ind].Cast<string>() : "";

                                out_date_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.OUT_DATE);
                                out_date_str = ((out_date_ind) >= 0 && (out_date_ind <= max_ind)) ? excelRow[out_date_ind].Cast<string>() : "";
                                out_date = String.IsNullOrEmpty(out_date_str) ? null : (DateTime?)DateTime.ParseExact(out_date_str.ExceptChars(new[] { ' ' }), "dd.MM.yyyy", CultureInfo.InvariantCulture);

                                out_reazon_ind = templ_curr_columns.ColumnNum(Enums.VedColumn.OUT_REAZON);
                                out_reazon = ((out_reazon_ind) >= 0 && (out_reazon_ind <= max_ind)) ? excelRow[out_reazon_ind].Cast<string>() : "";

                                block_num = 8;

                                if (isListMain)
                                {
                                    tmp_reestr_item = new tmp_ved_reestr_item();
                                    tmp_reestr_item.mnn = mnn;
                                    tmp_reestr_item.comm_name = comm_name;
                                    tmp_reestr_item.pack_name = pack_name;
                                    tmp_reestr_item.producer = producer;
                                    tmp_reestr_item.pack_count = pack_count;
                                    tmp_reestr_item.limit_price = limit_price;
                                    tmp_reestr_item.limit_opt_incr = limit_opt_incr;
                                    tmp_reestr_item.limit_rozn_incr = limit_rozn_incr;
                                    tmp_reestr_item.limit_rozn_price = limit_rozn_price;
                                    tmp_reestr_item.limit_rozn_price_with_nds = limit_rozn_price_with_nds;
                                    tmp_reestr_item.price_for_init_pack = price_for_init_pack;
                                    tmp_reestr_item.reg_num = reg_num;
                                    tmp_reestr_item.price_reg_date_full = price_reg_date;
                                    tmp_reestr_item.price_reg_date = DefectUtil.GetPriceRegDate(price_reg_date);
                                    tmp_reestr_item.price_reg_ndoc = DefectUtil.GetPriceRegNum(price_reg_date);
                                    tmp_reestr_item.ean13 = ean13;

                                    tmp_reestr_item.item_code = cnt;
                                    tmp_reestr_item.crt_date = now;
                                    tmp_reestr_item.crt_user = "downloader-ved";

                                    transactionContext = AddToContext<tmp_ved_reestr_item>(transactionContext, tmp_reestr_item, cnt, 100, true);
                                }
                                else
                                {
                                    tmp_reestr_item_out = new tmp_ved_reestr_item_out();
                                    tmp_reestr_item_out.mnn = mnn;
                                    tmp_reestr_item_out.comm_name = comm_name;
                                    tmp_reestr_item_out.pack_name = pack_name;
                                    tmp_reestr_item_out.producer = producer;
                                    tmp_reestr_item_out.pack_count = pack_count;
                                    tmp_reestr_item_out.limit_price = limit_price;                                    
                                    tmp_reestr_item_out.price_for_init_pack = price_for_init_pack;
                                    tmp_reestr_item_out.reg_num = reg_num;
                                    tmp_reestr_item_out.price_reg_date_full = price_reg_date;
                                    tmp_reestr_item_out.price_reg_date = DefectUtil.GetPriceRegDate(price_reg_date);
                                    tmp_reestr_item_out.price_reg_ndoc = DefectUtil.GetPriceRegNum(price_reg_date);
                                    tmp_reestr_item_out.ean13 = ean13;
                                    tmp_reestr_item_out.out_reazon = out_reazon;
                                    tmp_reestr_item_out.out_date = out_date;

                                    tmp_reestr_item_out.item_code = cnt;
                                    tmp_reestr_item_out.crt_date = now;
                                    tmp_reestr_item_out.crt_user = "downloader-ved";
                                    
                                    transactionContext = AddToContext<tmp_ved_reestr_item_out>(transactionContext, tmp_reestr_item_out, cnt, 100, true);
                                }

                            }

                            block_num = 9;
                            transactionContext.SaveChanges();

                        }
                        finally
                        {
                            if (transactionContext != null)
                                transactionContext.Dispose();
                        }

                        scope.Complete();
                    }

                    using (var dbContext = new AuMainDb())
                    {
                        // удаляем данные из временной таблицы перед импортом Excel-листа
                        dbContext.tmp_ved_reestr_item_out.RemoveRange(dbContext.tmp_ved_reestr_item_out);
                        dbContext.tmp_ved_reestr_item.RemoveRange(dbContext.tmp_ved_reestr_item);
                        dbContext.SaveChanges();

                        if (isGosReestr)
                        {
                            if (isListMain)
                            {
                                ved_import_result = dbContext.Database.SqlQuery<VedImportResult>("select * from esn.ved_reestr_item_import(" + source_id.ToString() + ")").FirstOrDefault();
                            }
                            else
                                ved_import_out_result = dbContext.Database.SqlQuery<VedImportResult>("select * from esn.ved_reestr_item_out_import(" + source_id.ToString() + ")").FirstOrDefault();
                        }
                        else
                        {
                            if (isListMain)
                                ved_import_result = dbContext.Database.SqlQuery<VedImportResult>("select * from esn.ved_reestr_region_item_import(" + source_id.ToString() + ")").FirstOrDefault();
                            else
                                ved_import_out_result = dbContext.Database.SqlQuery<VedImportResult>("select * from esn.ved_reestr_region_item_out_import(" + source_id.ToString() + ")").FirstOrDefault();
                        }
                    }
                }

                block_num = 10;

                new_item_cnt = 0;
                out_item_cnt = 0;
                err_item_cnt = 0;
                if (((ved_import_result != null) && (ved_import_result._reestr_id > 0)) || ((ved_import_out_result != null) && (ved_import_out_result._reestr_id > 0)))
                {                    
                    new_item_cnt = ved_import_result != null ? ved_import_result._new_item_cnt.GetValueOrDefault(0) : 0;
                    out_item_cnt = ved_import_out_result != null ? ved_import_out_result._out_item_cnt.GetValueOrDefault(0) : 0;
                    err_item_cnt = ved_import_out_result != null ? ved_import_out_result._err_item_cnt.GetValueOrDefault(0) : 0;
                    block_num = 11;

                    // формируем файлы: VedXmlMain_xxx.xml, VedXmlOut_xxx.xml, VedXmlError_xxx.xml
                    if (makeFilesInNewTask)
                    {
                        block_num = 12;
                        mess2 = mess2 + ". Начато формирование XML файлов";
                        Task taskMakeFiles = new Task(() =>
                        {
                            xVed_MakeFiles(region_id);
                        }
                        );
                        block_num = 13;
                        taskMakeFiles.Start();
                    }
                    else
                    {
                        block_num = 14;
                        xVed_MakeFiles(region_id);
                        mess2 = mess2 + ". Сформированы XML файлы";
                        block_num = 15;
                    }
                }

                block_num = 16;
                //mess = "Скачано " + cnt.ToString() + " новых строк реестра ЖВНЛП, исключенных строк: " + cnt2.ToString() + " [" + url_for_download + "]" + mess2;
                mess = "Результат скачивания реестра ЖВНЛП: новых строк: " + new_item_cnt.ToString() + ", исключенных строк: " + out_item_cnt.ToString() + ", ошибочных строк: " + err_item_cnt.ToString() + " [" + url_for_download + "]" + mess2;
                Loggly.InsertLog_Ved(mess, (long)Enums.LogEsnType.DOWNLOAD_VED, "downloader-ved", null, now, null);

                block_num = 17;
                return new ServiceOperationResult() { error = null, Id = 0, Message = mess, };
            }
            catch (Exception ex)
            {
                throw new Exception("block_num: " + block_num.ToString() + " " + ex.Message);
            }
        }

        #endregion        

        #region Ved_IsExistsNew

        private int xVed_IsExistsNew(UserInfo userInfo)
        {
            DateTime now = DateTime.Now;

            /*
                проверка лицензии
            */

            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_VED);
            if (lic.error != null)
            {
                Loggly.InsertLog_Ved("Ошибка в Ved_IsExistsNew: " + lic.error.Message, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, now, null);
                return (int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND;
            }

            string login = userInfo.login;
            string workplace = userInfo.workplace;
            string version_num = userInfo.version_num;
            DateTime date_to_check = DateTime.Now;

            using (var dbContext = new AuMainDb())
            {

                // ищем последний подтвержденный request
                var req = dbContext.request.Where(ss => ss.scope == (int)Enums.LogScope.VED
                    && ss.scope2.HasValue && ss.scope2 == (int)Enums.RequestScope2.VED_REGION
                    && ss.state == 1
                    && ss.login.Trim().ToLower().Equals(login.Trim().ToLower()))
                    .OrderByDescending(ss => ss.request_date)
                    .FirstOrDefault();

                if (req == null)
                {
                    date_to_check = new DateTime(1990, 1, 1);
                }
                else
                {
                    date_to_check = (DateTime)req.request_date;
                }

                var reestr_item = dbContext.vw_ved_reestr_region_item
                    .Where(ss => ss.region_id == lic.RegionId && ss.crt_date > date_to_check)
                    .FirstOrDefault();

                return reestr_item != null ? 1 : 0;
            }
        }

        #endregion        

        #region Ved_GetXmlList_Gos

        private VedXmlList xVed_GetXmlList_Gos(UserInfo userInfo, bool getAll)
        {

            DateTime now = DateTime.Now;

            /*
                проверка лицензии
            */

            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_VED);
            if (lic.error != null)
            {
                return Ved_VedXmlListError(now, lic.error.Message, (long)lic.error.Id, userInfo.login);
            }

            string login = userInfo.login;
            string workplace = userInfo.workplace;
            string version_num = userInfo.version_num;
            DateTime today = DateTime.Today;            
            request req = null;
            request req_new = null;
            IQueryable<vw_ved_reestr_item> reestr_item_query = null;
            List<vw_ved_reestr_item> reestr_item_list = null;
            IQueryable<vw_ved_reestr_item_out> reestr_item_out_query = null;
            List<vw_ved_reestr_item_out> reestr_item_out_list = null;
            VedXmlList res = new VedXmlList() { RequestDate = null, error = null, RequestId = 0, VedXmlError = null, VedXmlMain = null, VedXmlOut = null };
            VedXml xmlMain = new VedXml() { Data = "", error = null, RecordCount = 0, };
            VedXml xmlOut = new VedXml() { Data = "", error = null, RecordCount = 0, };
            VedXml xmlError = new VedXml() { Data = "", error = null, RecordCount = 0, };
            Encoding enc = Encoding.GetEncoding("windows-1251");            
            string filePath = "";

            using (var dbContext = new AuMainDb())
            {
                /*
                    при закачке только нового:
                    1. ищем последний подвержденный request
                    2. если не находим - ругаемся и предлагаем закачать все
                    3. создаем новый request с типом 0 и state = 0
                    4. отсылаем все данные c crt_date > request.request_date c прицепеленным request, ждем подтверждения

                    при полной закачке:
                    1. не обращаем внимание на существующие request
                    2. создаем новый request с типом 1 и state = 0
                    3. отсылаем все данные с прицепеленным request, ждем подтверждения
                */

                req = dbContext.request
                    .Where(ss => ss.scope == (int)Enums.LogScope.VED
                        && ss.state == 1
                        && ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                        )
                    .OrderByDescending(ss => ss.request_date)
                    .FirstOrDefault();

                if ((req == null) && (!getAll))
                {
                    return Ved_VedXmlListError(now, "Не найден ни один подтвержденный запрос, необходимо закачать весь список ЖВНЛП", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, login);
                }

                req_new = new request();
                req_new.scope = (int)Enums.LogScope.VED;
                req_new.login = login;
                req_new.record_cnt = 0;
                req_new.request_date = now;
                req_new.state = 0;
                req_new.version_num = version_num;
                req_new.workplace = workplace;
                req_new.request_type = getAll ? 1 : 0;

                // 1. 
                // при закачке только нового:
                // отдаем все строки из госреестра, с датой создания больше даты последнего подтвержденного запроса
                // при полной закачке:
                // отдаем все строки из госреестра
                if (!getAll)
                {
                    reestr_item_query = dbContext.vw_ved_reestr_item
                        .Where(ss => ss.crt_date > req.request_date)
                        .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code)
                        ;
                    reestr_item_list = reestr_item_query.ToList();
                    req_new.record_cnt = (reestr_item_list == null) ? 0 : reestr_item_list.Count;
                    xmlMain = xVed_MakeXml(reestr_item_list);
                }
                else
                {
                    filePath = Path.Combine(filePath_VedXml_Full, "VedXmlMain_Gos.xml");
                    if (!File.Exists(filePath))
                    {
                        xmlMain.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден сформированный XML файл для госреестра");
                    }
                    else
                    {
                        xmlMain.Data = File.ReadAllText(filePath, enc);
                    }
                }
               
                res.VedXmlMain = xmlMain;

                // 2. 
                // при закачке только нового:
                // отдаем все строки из госреестра, исключенные из ЖВНЛП, с датой создания больше даты последнего подтвержденного запроса
                // при полной закачке:
                // отдаем все строки из госреестра, исключенные из ЖНЛП
                if (!getAll)
                {
                    reestr_item_out_query = dbContext.vw_ved_reestr_item_out
                            .Where(ss => ss.crt_date > req.request_date)
                            .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code)
                            ;
                    reestr_item_out_list = reestr_item_out_query.ToList();
                    xmlOut = xVed_MakeXml(reestr_item_out_list);                    
                }
                else
                {
                    filePath = Path.Combine(filePath_VedXml_Full, "VedXmlOut_Gos.xml");
                    if (!File.Exists(filePath))
                    {
                        //return Ved_VedXmlListError(now, "Не найден сформированный XML файл исключений из госреестра", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, login);
                        xmlOut.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден сформированный XML файл исключений из госреестра");
                    }
                    else
                    {
                        xmlOut.Data = File.ReadAllText(filePath, enc);
                    }
                }
                
                res.VedXmlOut = xmlOut;

                xmlError.RecordCount = 0;
                res.VedXmlError = xmlError;

                dbContext.request.Add(req_new);
                dbContext.SaveChanges();
                res.RequestDate = req_new.request_date;
                res.RequestId = req_new.request_id;

                return res;
            }
        }

        #endregion

        #region Ved_GetXmlList_Region

        private VedXmlList xVed_GetXmlList_Region(UserInfo userInfo, bool getAll)
        {

            DateTime now = DateTime.Now;

            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_VED);
            if (lic.error != null)
            {
                return Ved_VedXmlListError(now, lic.error.Message, (long)lic.error.Id, userInfo.login);
            }

            if (lic.RegionId.GetValueOrDefault(0) <= 0)
            {
                return Ved_VedXmlListError(now, "Не найден регион для логина " + userInfo.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo.login);
            }

            string login = userInfo.login;
            string workplace = userInfo.workplace;
            string version_num = userInfo.version_num;
            long region_id = (long)lic.RegionId;
            DateTime today = DateTime.Today;
            request req = null;
            request req_new = null;
            IQueryable<vw_ved_reestr_region_item> reestr_item_query = null;
            List<vw_ved_reestr_region_item> reestr_item_list = null;
            IQueryable<vw_ved_reestr_region_item_out> reestr_item_out_query = null;
            List<vw_ved_reestr_region_item_out> reestr_item_out_list = null;
            VedXmlList res = new VedXmlList() { RequestDate = null, error = null, RequestId = 0, VedXmlError = null, VedXmlMain = null, VedXmlOut = null };
            VedXml xmlMain = new VedXml() { Data = "", error = null, RecordCount = 0, };
            VedXml xmlOut = new VedXml() { Data = "", error = null, RecordCount = 0, };
            VedXml xmlError = new VedXml() { Data = "", error = null, RecordCount = 0, };
            Encoding enc = Encoding.GetEncoding("windows-1251");
            string filePath = "";

            using (var dbContext = new AuMainDb())
            {
                /*
                    при закачке только нового:
                    1. ищем последний подвержденный request
                    2. если не находим - ругаемся и предлагаем закачать все
                    3. создаем новый request с типом 0 и state = 0
                    4. отсылаем все данные c crt_date > request.request_date c прицепеленным request, ждем подтверждения

                    при полной закачке:
                    1. не обращаем внимание на существующие request
                    2. создаем новый request с типом 1 и state = 0
                    3. отсылаем все данные с прицепеленным request, ждем подтверждения
                */

                req = dbContext.request
                    .Where(ss => ss.scope == (int)Enums.LogScope.VED
                        && ss.scope2.HasValue && ss.scope2 == (int)Enums.RequestScope2.VED_REGION
                        && ss.state == 1
                        && ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                        )
                    .OrderByDescending(ss => ss.request_date)
                    .FirstOrDefault();

                if ((req == null) && (!getAll))
                {
                    return Ved_VedXmlListError(now, "Не найден ни один подтвержденный запрос, необходимо закачать весь список ЖВНЛП", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, login);
                }

                req_new = new request();
                req_new.scope = (int)Enums.LogScope.VED;
                req_new.scope2 = (int)Enums.RequestScope2.VED_REGION;
                req_new.login = login;
                req_new.record_cnt = 0;
                req_new.request_date = now;
                req_new.state = 0;
                req_new.version_num = version_num;
                req_new.workplace = workplace;
                req_new.request_type = getAll ? 1 : 0;

                // 1.
                // при закачке только нового:
                // отдаем все строки из регионального реестра, привязанные к госреестру, с датой создания больше даты последнего подтвержденного запроса
                // при полной закачке:
                // отдаем все строки из регионального реестра, привязанные к госреестру
                if (!getAll)
                {
                    reestr_item_query = dbContext.vw_ved_reestr_region_item
                        .Where(ss => ss.region_id == region_id && ss.gos_reestr_item_id.HasValue && ss.gos_reestr_item_id > 0 && ss.crt_date > req.request_date)
                        .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code)
                        ;
                    reestr_item_list = reestr_item_query.ToList();
                    req_new.record_cnt = (reestr_item_list == null) ? 0 : reestr_item_list.Count;
                    xmlMain = xVed_MakeXml(reestr_item_list);
                }
                else
                {
                    filePath = Path.Combine(filePath_VedXml_Full, "VedXmlMain_Region_" + region_id.ToString() + ".xml");
                    if (!File.Exists(filePath))
                    {
                        //return Ved_VedXmlListError(now, "Не найден сформированный XML файл для регионального реестра [код региона: " + region_id.ToString() + "]", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, login);
                        xmlMain.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден сформированный XML файл для регионального реестра [код региона: " + region_id.ToString() + "]");
                    }
                    else
                    {
                        xmlMain.Data = File.ReadAllText(filePath, enc);
                    }
                }
                
                res.VedXmlMain = xmlMain;

                // 2.
                // при закачке только нового:
                // отдаем все строки из регионального реестра, привязанные к госреестру, исключенные из ЖВНЛП, с датой создания больше даты последнего подтвержденного запроса
                // при полной закачке:
                // отдаем все строки из регионального реестра, привязанные к госреестру, исключенные из ЖВНЛП
                if (!getAll)
                {
                    reestr_item_out_query = dbContext.vw_ved_reestr_region_item_out
                        .Where(ss => ss.region_id == region_id && ss.gos_reestr_item_id.HasValue && ss.gos_reestr_item_id > 0 && ss.crt_date > req.request_date)
                        .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code);
                    reestr_item_out_list = reestr_item_out_query.ToList();
                    xmlOut = xVed_MakeXml(reestr_item_out_list);
                }
                else
                {
                    filePath = Path.Combine(filePath_VedXml_Full, "VedXmlOut_Region_" + region_id.ToString() + ".xml");
                    if (!File.Exists(filePath))
                    {
                        //return Ved_VedXmlListError(now, "Не найден сформированный XML файл исключений из регионального реестра [код региона: " + region_id.ToString() + "]", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, login);
                        xmlOut.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден сформированный XML файл исключений из регионального реестра [код региона: " + region_id.ToString() + "]");
                    }
                    else
                    {
                        xmlOut.Data = File.ReadAllText(filePath, enc);
                    }                                        
                }                

                res.VedXmlOut = xmlOut;

                // 3.
                // при закачке только нового:
                // отдаем все строки из регионального реестра, не привязанные к госреестру, с датой создания больше даты последнего подтвержденного запроса
                // при полной закачке:
                // отдаем все строки из регионального реестра, не привязанные к госреестру
                if (!getAll)
                {
                    reestr_item_query = dbContext.vw_ved_reestr_region_item
                        .Where(ss => ss.region_id == region_id && ss.crt_date > req.request_date && ((!ss.gos_reestr_item_id.HasValue) || (ss.gos_reestr_item_id <= 0)))
                        .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code);
                    reestr_item_list = reestr_item_query.ToList();
                    xmlError = xVed_MakeXml(reestr_item_list);
                }
                else
                {
                    filePath = Path.Combine(filePath_VedXml_Full, "VedXmlError_Region_" + region_id.ToString() + ".xml");
                    if (!File.Exists(filePath))
                    {
                        //return Ved_VedXmlListError(now, "Не найден сформированный XML файл ошибок регионального реестра [код региона: " + region_id.ToString() + "]", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, login);
                        xmlError.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден сформированный XML файл ошибок регионального реестра [код региона: " + region_id.ToString() + "]");
                    }
                    else
                    {
                        xmlError.Data = File.ReadAllText(filePath, enc);
                    }
                }
                
                res.VedXmlError = xmlError;

                dbContext.request.Add(req_new);
                dbContext.SaveChanges();

                res.RequestDate = req_new.request_date;
                res.RequestId = req_new.request_id;

                return res;
            }
        }

        #endregion        


        #region Ved_MakeXml

        private VedXml xVed_MakeXml(List<vw_ved_reestr_item> reestr_item_list)
        {
            bool firstRow = true;
            long curr_reestr_id = 0;
            long prev_reestr_id = 0;
            VedXml res = new VedXml() { Data = "", error = null, RecordCount = 0, };

            firstRow = true;
            curr_reestr_id = 0;
            prev_reestr_id = 0;
            if ((reestr_item_list != null) && (reestr_item_list.Count > 0))
            {
                foreach (var item in reestr_item_list)
                {
                    curr_reestr_id = item.reestr_id;
                    if (firstRow)
                        res.StartReestr(item.reestr_id, item.reestr_crt_date, item.reestr_name);
                    else if (curr_reestr_id != prev_reestr_id)
                    {
                        res.EndReestr();
                        res.StartReestr(item.reestr_id, item.reestr_crt_date, item.reestr_name);
                    }

                    res.AddVedItem(item);

                    prev_reestr_id = curr_reestr_id;
                    firstRow = false;
                }
                res.EndReestr();

                res.AddDataCompleted();
            }

            res.RecordCount = (reestr_item_list == null) ? 0 : reestr_item_list.Count;
            return res;
        }

        private VedXml xVed_MakeXml(List<vw_ved_reestr_item_out> reestr_item_out_list)
        {
            bool firstRow = true;
            long curr_reestr_id = 0;
            long prev_reestr_id = 0;
            VedXml res = new VedXml() { Data = "", error = null, RecordCount = 0, };

            firstRow = true;
            curr_reestr_id = 0;
            prev_reestr_id = 0;
            if ((reestr_item_out_list != null) && (reestr_item_out_list.Count > 0))
            {
                foreach (var item in reestr_item_out_list)
                {
                    curr_reestr_id = item.reestr_id;
                    if (firstRow)
                        res.StartReestr(item.reestr_id, item.reestr_crt_date, item.reestr_name);
                    else if (curr_reestr_id != prev_reestr_id)
                    {
                        res.EndReestr();
                        res.StartReestr(item.reestr_id, item.reestr_crt_date, item.reestr_name);
                    }

                    res.AddVedItem(item);

                    prev_reestr_id = curr_reestr_id;
                    firstRow = false;
                }
                res.EndReestr();

                res.AddDataCompleted();
            }

            res.RecordCount = (reestr_item_out_list == null) ? 0 : reestr_item_out_list.Count;
            return res;
        }

        private VedXml xVed_MakeXml(List<vw_ved_reestr_region_item> reestr_item_list)
        {
            bool firstRow = true;
            long curr_reestr_id = 0;
            long prev_reestr_id = 0;
            VedXml res = new VedXml() { Data = "", error = null, RecordCount = 0, };

            firstRow = true;
            curr_reestr_id = 0;
            prev_reestr_id = 0;
            if ((reestr_item_list != null) && (reestr_item_list.Count > 0))
            {
                foreach (var item in reestr_item_list)
                {
                    curr_reestr_id = item.reestr_id;
                    if (firstRow)
                        res.StartReestr(item.reestr_id, item.reestr_crt_date, item.reestr_name);
                    else if (curr_reestr_id != prev_reestr_id)
                    {
                        res.EndReestr();
                        res.StartReestr(item.reestr_id, item.reestr_crt_date, item.reestr_name);
                    }

                    res.AddVedItem(item);

                    prev_reestr_id = curr_reestr_id;
                    firstRow = false;
                }
                res.EndReestr();

                res.AddDataCompleted();
            }

            res.RecordCount = (reestr_item_list == null) ? 0 : reestr_item_list.Count;
            return res;
        }

        private VedXml xVed_MakeXml(List<vw_ved_reestr_region_item_out> reestr_item_out_list)
        {
            bool firstRow = true;
            long curr_reestr_id = 0;
            long prev_reestr_id = 0;
            VedXml res = new VedXml() { Data = "", error = null, RecordCount = 0, };

            firstRow = true;
            curr_reestr_id = 0;
            prev_reestr_id = 0;
            if ((reestr_item_out_list != null) && (reestr_item_out_list.Count > 0))
            {
                foreach (var item in reestr_item_out_list)
                {
                    curr_reestr_id = item.reestr_id;
                    if (firstRow)
                        res.StartReestr(item.reestr_id, item.reestr_crt_date, item.reestr_name);
                    else if (curr_reestr_id != prev_reestr_id)
                    {
                        res.EndReestr();
                        res.StartReestr(item.reestr_id, item.reestr_crt_date, item.reestr_name);
                    }

                    res.AddVedItem(item);

                    prev_reestr_id = curr_reestr_id;
                    firstRow = false;
                }
                res.EndReestr();

                res.AddDataCompleted();
            }

            res.RecordCount = (reestr_item_out_list == null) ? 0 : reestr_item_out_list.Count;
            return res;
        }

        #endregion

        #region Ved_MakeFiles

        private void xVed_MakeFiles(int? region_id = null)
        {
            DateTime now = DateTime.Now;
            VedXml resMain = new VedXml() { Data = "",  RecordCount = 0, error = null, };
            VedXml resOut = new VedXml() { Data = "", RecordCount = 0, error = null, };
            VedXml resError = new VedXml() { Data = "", RecordCount = 0, error = null, };
            string filePath = "";
            string fileName = "";
            int cnt = 0;
            Encoding enc = Encoding.GetEncoding("windows-1251");
            List<vw_ved_reestr_item> reestr_item_list = null;
            List<vw_ved_reestr_item_out> reestr_item_out_list = null;
            List<vw_ved_reestr_region_item> reestr_region_item_list = null;
            List<vw_ved_reestr_region_item_out> reestr_region_item_out_list = null;

            try
            {
                using (var dbContext = new AuMainDb())
                {
                    // создаем XML файлы для госреестра
                    if (region_id == null)
                    {
                        fileName = "VedXmlMain_Gos.xml";
                        filePath = Path.Combine(filePath_VedXml_Full, fileName);
                        File.Delete(filePath);

                        reestr_item_list = dbContext.vw_ved_reestr_item
                            .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code)
                            .ToList()
                            ;

                        if ((reestr_item_list != null) && (reestr_item_list.Count > 0))
                        {
                            cnt = reestr_item_list.Count;
                            resMain = xVed_MakeXml(reestr_item_list);
                            using (StreamWriter outfile = new StreamWriter(filePath, true, enc))
                            {                                
                                outfile.Write(resMain.Data);
                            }
                            Loggly.InsertLog_Ved("В файл " + fileName + " записано " + cnt.ToString() + " строк", (long)Enums.LogEsnType.DOWNLOAD_VED, "downloader-ved", null, now, null);
                        }
                        else
                        {
                            Loggly.InsertLog_Ved("Нет данных для записи в файл " + fileName, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, now, null);
                        }

                        fileName = "VedXmlOut_Gos.xml";
                        filePath = Path.Combine(filePath_VedXml_Full, fileName);
                        File.Delete(filePath);

                        reestr_item_out_list = dbContext.vw_ved_reestr_item_out
                            .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code)
                            .ToList()
                            ;

                        if ((reestr_item_out_list != null) && (reestr_item_out_list.Count > 0))
                        {
                            cnt = reestr_item_out_list.Count;
                            resOut = xVed_MakeXml(reestr_item_out_list);
                            using (StreamWriter outfile = new StreamWriter(filePath, true, enc))
                            {
                                outfile.Write(resOut.Data);
                            }
                            Loggly.InsertLog_Ved("В файл " + fileName + " записано " + cnt.ToString() + " строк", (long)Enums.LogEsnType.DOWNLOAD_VED, "downloader-ved", null, now, null);
                        }
                        else
                        {
                            Loggly.InsertLog_Ved("Нет данных для записи в файл " + fileName, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, now, null);
                        }
                        
                    }
                    else
                    // создаем XML файлы для регионального реестра
                    {
                        fileName = "VedXmlMain_Region_" + region_id.ToString() + ".xml";
                        filePath = Path.Combine(filePath_VedXml_Full, fileName);
                        File.Delete(filePath);

                        reestr_region_item_list = dbContext.vw_ved_reestr_region_item
                            .Where(ss => ss.region_id == region_id && ss.gos_reestr_item_id.HasValue && ss.gos_reestr_item_id > 0)
                            .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code)
                            .ToList()
                            ;

                        if ((reestr_region_item_list != null) && (reestr_region_item_list.Count > 0))
                        {
                            cnt = reestr_region_item_list.Count;
                            resMain = xVed_MakeXml(reestr_region_item_list);
                            using (StreamWriter outfile = new StreamWriter(filePath, true, enc))
                            {
                                outfile.Write(resMain.Data);
                            }
                            Loggly.InsertLog_Ved("В файл " + fileName + " записано " + cnt.ToString() + " строк", (long)Enums.LogEsnType.DOWNLOAD_VED, "downloader-ved", null, now, null);
                        }
                        else
                        {
                            Loggly.InsertLog_Ved("Нет данных для записи в файл " + fileName, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, now, null);
                        }

                        fileName = "VedXmlOut_Region_" + region_id.ToString() + ".xml";
                        filePath = Path.Combine(filePath_VedXml_Full, fileName);
                        File.Delete(filePath);

                        reestr_region_item_out_list = dbContext.vw_ved_reestr_region_item_out
                            .Where(ss => ss.region_id == region_id && ss.gos_reestr_item_id.HasValue && ss.gos_reestr_item_id > 0)
                            .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code)
                            .ToList()
                            ;

                        if ((reestr_region_item_out_list != null) && (reestr_region_item_out_list.Count > 0))
                        {
                            cnt = reestr_region_item_out_list.Count;
                            resOut = xVed_MakeXml(reestr_region_item_out_list);
                            using (StreamWriter outfile = new StreamWriter(filePath, true, enc))
                            {
                                outfile.Write(resOut.Data);
                            }
                            Loggly.InsertLog_Ved("В файл " + fileName + " записано " + cnt.ToString() + " строк", (long)Enums.LogEsnType.DOWNLOAD_VED, "downloader-ved", null, now, null);
                        }
                        else
                        {
                            Loggly.InsertLog_Ved("Нет данных для записи в файл " + fileName, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, now, null);
                        }

                        fileName = "VedXmlError_Region_" + region_id.ToString() + ".xml";
                        filePath = Path.Combine(filePath_VedXml_Full, fileName);
                        File.Delete(filePath);

                        reestr_region_item_list = dbContext.vw_ved_reestr_region_item
                            .Where(ss => ss.region_id == region_id && ((!ss.gos_reestr_item_id.HasValue) || (ss.gos_reestr_item_id <= 0)))
                            .OrderBy(ss => ss.reestr_id).ThenBy(ss => ss.item_code)
                            .ToList();

                        if ((reestr_region_item_list != null) && (reestr_region_item_list.Count > 0))
                        {
                            cnt = reestr_region_item_list.Count;
                            resError = xVed_MakeXml(reestr_region_item_list);
                            using (StreamWriter outfile = new StreamWriter(filePath, true, enc))
                            {
                                outfile.Write(resError.Data);
                            }
                            Loggly.InsertLog_Ved("В файл " + fileName + " записано " + cnt.ToString() + " строк", (long)Enums.LogEsnType.DOWNLOAD_VED, "downloader-ved", null, now, null);
                        }
                        else
                        {
                            Loggly.InsertLog_Ved("Нет данных для записи в файл " + fileName, (long)Enums.LogEsnType.ERROR, "downloader-ved", null, now, null);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Ved(GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, "downloader-ved", null, now, null);
                return;
            }
        }

        #endregion

    }
}
