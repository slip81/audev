﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;
using System.IO.Compression;
using System.Transactions;
using System.Text;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

using HtmlAgilityPack;
using LinqToExcel;
using NUnrar.Archive;
using NUnrar.Common;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

#endregion

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {
        private readonly string filePath_GR_Tmp = @"C:\Release\GR\File\";
        private readonly string filePath_GR_Full = @"C:\Release\GR\File\Txt\";

        private ServiceOperationResult GR_ServiceOperationResultError(DateTime date_beg, string err, long err_code)
        {            
            Loggly.InsertLog_Defect(err, (long)Enums.LogEsnType.ERROR, "downloader-gr", null, date_beg, null);
            return new ServiceOperationResult()
            {
                error = new ErrInfo(err_code, err),
                Id = err_code,
                Message = "",
            };
        }

        private ServiceOperationResult xGR_DownloadList(string password)
        {
            if (password != "iguana")
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            string file_name = "reestr.txt";
            string file_full_name = Path.Combine(filePath_GR_Full, file_name);
            string zip_file_name = "reestr.zip";
            string zip_file_full_name = Path.Combine(filePath_GR_Full, zip_file_name);
            string tmp_file_name = "";
            string tmp_file_full_name = "";
            string tmp_new_file_name = System.Guid.NewGuid().ToString() + ".txt";
            string tmp_new_file_full_name = Path.Combine(filePath_GR_Tmp, tmp_new_file_name);
            string tmp_downloaded_file_name = System.Guid.NewGuid().ToString();
            string tmp_downloaded_file_full_name = Path.Combine(filePath_GR_Tmp, tmp_downloaded_file_name);
            string url_site = @"http://grls.rosminzdrav.ru/pricelims.aspx";
            //string url_file = @"http://grls.rosminzdrav.ru/GetLimPrice.aspx?FileGUID=0c119cdc-3cc2-4530-9112-2d00673fa14e&UserReq=1195440";
            string url_file_header = @"http://grls.rosminzdrav.ru/";
            //string url_file = @"GetLimPrice.aspx?FileGUID=";
            string url_file = @"GetLimPrice.ashx?FileGUID=";
            string url_file_a_content = "в файле ZIP";
            string url_for_download = "";
            string file_header = "";
            string file_content = "";
            string file_content_cena = "";
            string file_content_ean13 = "";
            string file_content_name_tovar = "";
            string file_content_num_reg_doc = "";
            List<string> linkedPages = null;
            int row_num_to_start = 0;
            DateTime now = DateTime.Now;
            ExcelQueryFactory excel = null;
            RowNoHeader excelRow = null;
            List<RowNoHeader> excelRows = null;
            int excelRowsCnt = 0;

            int index_mnn = 0;
            int index_tovar = 1;
            int index_form = 2;
            int index_firm = 3;

            /*
            int index_kol = 4;
            int index_cena = 5;
            int index_reg = 8;
            int index_ean13 = 9;
            */

            // новый формат файла после 18.02.2019
            int index_kol = 5;
            int index_cena = 6;
            int index_reg = 9;
            int index_ean13 = 10;

            Encoding enc = Encoding.GetEncoding("windows-1251");

            if (File.Exists(tmp_downloaded_file_full_name))
            {
                File.Delete(tmp_downloaded_file_full_name);
            }

            var doc = new HtmlWeb().Load(url_site);            
            linkedPages = doc.DocumentNode.Descendants("a")
                .Where(ss => ss.InnerText.Contains(url_file_a_content, StringComparison.OrdinalIgnoreCase))
                .Select(a => a.GetAttributeValue("href", null))
                .Where(u => (!String.IsNullOrEmpty(u)) && (u.Contains(url_file, StringComparison.OrdinalIgnoreCase)))
                .ToList()
                ;

            if ((linkedPages == null) || (linkedPages.Count <= 0))
            {
                return GR_ServiceOperationResultError(now, "На странице скачивания госрееста не найдено ссылок на файлы", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND);
            }

            url_for_download = url_file_header + linkedPages.FirstOrDefault();

            using (var client = new WebClient())
            {
                client.DownloadFile(url_for_download, tmp_downloaded_file_full_name);
            }

            using (var zipArchive = ZipFile.OpenRead(tmp_downloaded_file_full_name))
            {
                tmp_file_name = zipArchive.Entries.FirstOrDefault().Name;
                tmp_file_full_name = Path.Combine(filePath_GR_Tmp, tmp_file_name);
                if (File.Exists(tmp_file_full_name))
                {
                    File.Delete(tmp_file_full_name);
                }
                zipArchive.ExtractToDirectory(filePath_GR_Tmp);
            }

            using (excel = new ExcelQueryFactory(tmp_file_full_name))
            {
                excel.ReadOnly = true;
                // индекс страницы по алфавиту !
                excelRows = excel.WorksheetNoHeader(1).ToList();
                excelRowsCnt = excelRows.Count;
                row_num_to_start = 2;

                using (StreamWriter outfile = new StreamWriter(tmp_new_file_full_name, false, enc, 4096))
                {
                    file_header = "NAME_MNN\tNAME_TOVAR\tFORM\tNAME_FIRM\tKOL\tCENA_GR\tNUM_REG_UDOST\tEAN13\tCENA_GR_DOL\tVALUT";
                    outfile.Write(file_header);
                    for (int i = row_num_to_start; i < excelRowsCnt; i++)
                    {
                        file_content = "";
                        excelRow = excelRows.ElementAtOrDefault(i);

                        file_content_ean13 = excelRow[index_ean13].Cast<string>().TrimIfNotNull();
                        if (file_content_ean13.Length > 13)
                            continue;

                        file_content_name_tovar = excelRow[index_tovar].Cast<string>().RemoveLineEndings().TrimIfNotNull();
                        if (String.IsNullOrWhiteSpace(file_content_name_tovar))
                            continue;

                        outfile.Write(System.Environment.NewLine);

                        //NAME_MNN
                        file_content += excelRow[index_mnn].Cast<string>().RemoveLineEndings().TrimIfNotNull();
                        file_content += "\t";
                        //NAME_TOVAR
                        file_content += file_content_name_tovar;
                        file_content += "\t";
                        //FORM
                        file_content += excelRow[index_form].Cast<string>().RemoveLineEndings().TrimIfNotNull();
                        file_content += "\t";
                        //NAME_FIRM
                        file_content += excelRow[index_firm].Cast<string>().RemoveLineEndings().TrimIfNotNull();
                        file_content += "\t";
                        //KOL
                        file_content += excelRow[index_kol].Cast<string>().RemoveWhitespace().TrimIfNotNull();
                        file_content += "\t";
                        //CENA_GR
                        file_content_cena = excelRow[index_cena].Cast<string>().TrimIfNotNull().Replace(',', '.').RemoveWhitespace().TrimIfNotNull();
                        file_content += file_content_cena;
                        file_content += "\t";
                        // NUM_REG_UDOST
                        file_content_num_reg_doc = excelRow[index_reg].Cast<string>().TrimIfNotNull();
                        file_content += (DefectUtil.GetPriceRegNum(file_content_num_reg_doc)
                            + " от "
                            + DefectUtil.GetPriceRegDate(file_content_num_reg_doc)
                            )
                            ;
                        file_content += "\t";
                        //EAN13
                        file_content += file_content_ean13;
                        file_content += "\t";
                        //CENA_GR_DOL
                        file_content += file_content_cena;
                        file_content += "\t";
                        //VALUT
                        file_content += "Руб.";

                        outfile.Write(file_content);
                    }
                }
            }

            if (File.Exists(file_full_name))
            {
                File.Delete(file_full_name);
            }
            File.Move(tmp_new_file_full_name, file_full_name);

            if (File.Exists(zip_file_full_name))
            {
                File.Delete(zip_file_full_name);
            }
            
            using (ZipArchive archive = ZipFile.Open(zip_file_full_name, ZipArchiveMode.Create, enc))
            {
                archive.CreateEntryFromFile(file_full_name, file_name);
            }

            Loggly.InsertLog_Defect("Скачан госреестр", (long)Enums.LogEsnType.DOWNLOAD_DEFECT, "downloader-gr", null, now, null);

            string FilesStore = @"C:\Sites\CAP\CAP.SiteNew\App_Data\Files\Услуги";
            File.Copy(zip_file_full_name, Path.Combine(FilesStore, zip_file_name), true);

            // т.к. сервис 1С по скачиванию и выкладыванию Госреестра не работает - обновляем файл в Release для отправки клиентам на почту
            string releaseFileName = @"C:\Release\Defect\File\Arc\reestr.zip";
            if (File.Exists(releaseFileName))
            {
                File.Delete(releaseFileName);
            }
            File.Copy(Path.Combine(FilesStore, zip_file_name), releaseFileName, true);
            Loggly.InsertLog_Defect("Госреестр скопирован в Release", (long)Enums.LogEsnType.DOWNLOAD_DEFECT, "downloader-gr", null, now, null);

            GC.WaitForPendingFinalizers();
            if (File.Exists(tmp_file_full_name))
            {
                File.Delete(tmp_file_full_name);
            }
            if (File.Exists(tmp_downloaded_file_full_name))
            {
                File.Delete(tmp_downloaded_file_full_name);
            }
            if (File.Exists(file_full_name))
            {
                File.Delete(file_full_name);
            }
            
            Loggly.InsertLog_Defect("Госреестр скопирован в Услуги", (long)Enums.LogEsnType.DOWNLOAD_DEFECT, "downloader-gr", null, now, null);

            return new ServiceOperationResult();
        }
    }
}
