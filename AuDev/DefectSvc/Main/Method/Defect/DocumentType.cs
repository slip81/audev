﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {
        #region GetDocumentTypeList

        private DocumentTypeList xGetDocumentTypeList()
        {
            DocumentTypeList res = new DocumentTypeList() { DocumentType_list = new List<DocumentType>(), };
            using (var dbContext = new AuMainDb())
            {
                var list = dbContext.document_type.OrderBy(ss => ss.type_id).ToList();
                if ((list == null) || (list.Count <= 0))
                    return res;
                res = new DocumentTypeList(list);
                return res;
            }
        }

        #endregion
    }
}
