﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {
        #region GetDefectSourceList

        private DefectSourceList xGetDefectSourceList()
        {
            DefectSourceList res = new DefectSourceList() { DefectSource_list = new List<DefectSource>(), };
            using (var dbContext = new AuMainDb())
            {
                var list = dbContext.defect_source.OrderBy(ss => ss.source_id).ToList();
                if ((list == null) || (list.Count <= 0))
                    return res;
                res = new DefectSourceList(list);
                return res;
            }
        }

        #endregion
    }

}
