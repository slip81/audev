﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {        
        #region GetDefectStatusList

        private DefectStatusList xGetDefectStatusList()
        {
            DefectStatusList res = new DefectStatusList() { DefectStatus_list = new List<DefectStatus>(), };
            using (var dbContext = new AuMainDb())
            {
                var list = dbContext.defect_status.OrderBy(ss => ss.status_id).ToList();
                if ((list == null) || (list.Count <= 0))
                    return res;
                res = new DefectStatusList(list);
                return res;
            }
        }

        #endregion
    }

}
