﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {
        #region GetDocumentList

        private DocumentList xGetDocumentList(string password, DateTime? date_beg, DateTime? date_end)
        {
            if (password != "iguana")
                return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            DateTime date_beg_int = date_beg.HasValue ? ((DateTime)date_beg).Date : new DateTime(1, 1, 1990);
            DateTime date_end_int = date_end.HasValue ? ((DateTime)date_end).Date : new DateTime(1, 1, 2090);

            DocumentList res = new DocumentList() { Document_list = new List<Document>(), };
            using (var dbContext = new AuMainDb())
            {
                var list = dbContext.document.Where(ss => ss.accept_date >= date_beg_int && ss.accept_date <= date_end_int).OrderBy(ss => ss.document_id).ToList();
                if ((list == null) || (list.Count <= 0))
                    return res;
                res = new DocumentList(list);
                return res;
            }
        }

        #endregion

        #region GetDocument

        private Document xGetDocument(string password, long document_id)
        {
            if (password != "iguana")
                return new Document() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            Document res = null;
            using (var dbContext = new AuMainDb())
            {
                var item = dbContext.document.FirstOrDefault(ss => ss.document_id == document_id);
                if (item == null)
                    return new Document() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден документ с кодом " + document_id.ToString()) };
                res = new Document(item);
                return res;
            }
        }

        #endregion
    }

}
