﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SocialExplorer.IO.FastDBF;
using WinSCP;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {        
        #region DownloadDefectList

        private DefectList xDownloadDefectList(string password, DateTime? date_beg, DateTime? date_end, bool download_body)
        {
            if (password != "iguana")
                return new DefectList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            DateTime date_beg_int = date_beg.HasValue ? ((DateTime)date_beg).Date : new DateTime(1, 1, 1990);
            DateTime date_end_int = date_end.HasValue ? ((DateTime)date_end).Date : new DateTime(1, 1, 2090);
            
            // некорректный диапазон
            if (date_beg_int > date_end_int)
                return new DefectList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Дата начала больше даты окончания") };
            // максим. диапазон - месяц
            DateTime max_date_end = date_beg_int.AddMonths(1);
            if (max_date_end < date_end_int)
                return new DefectList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Маскимальный диапазон - месяц") };

            var task = xxDownloadDefectList(date_beg_int, date_end_int, download_body);
            var res = task.Result;
            
            if (res.error != null)
            {
                return new DefectList() { error = res.error };
            }

            Task taskMakeFiles = new Task(() =>
            {
                xMakeFiles();
            }
            );
            taskMakeFiles.Start();
            /*
            Task taskMakeXml = new Task(() =>
            {
                xMakeXml();
            }
            );
            taskMakeXml.Start();

            Task taskMakeDbf = new Task(() =>
            {
                xMakeDbf();
            }
            );
            taskMakeDbf.Start();
            */

            return res;
        }

        private async Task<DefectList> xxDownloadDefectList(DateTime date_beg, DateTime date_end, bool download_body)
        {
            DefectList res = new DefectList() { Defect_list = new List<Defect>() };

            DownloadedPreps preps = null;
            List<defect> defect_list = new List<defect>();
            bool needRefreshXml = false;

            string date_beg_param = ((date_beg.Day.ToString().Trim().Length < 2) ? ("0" + date_beg.Day.ToString()) : date_beg.Day.ToString())
                + "." + ((date_beg.Month.ToString().Trim().Length < 2) ? ("0" + date_beg.Month.ToString()) : date_beg.Month.ToString())
                + "." + date_beg.Year.ToString();

            string date_end_param = ((date_end.Day.ToString().Trim().Length < 2) ? ("0" + date_end.Day.ToString()) : date_end.Day.ToString())
                + "." + ((date_end.Month.ToString().Trim().Length < 2) ? ("0" + date_end.Month.ToString()) : date_end.Month.ToString())
                + "." + date_end.Year.ToString();

            using (var client = new HttpClient())
            //using (var client = new WebClient())
            {
                client.DefaultRequestHeaders.ExpectContinue = false;
                

                var values = new Dictionary<string, string>
                {
                   { "draw", "5" },

                   { "columns%5B0%5D%5Bdata%5D", "col1.label" },
                   { "columns%5B0%5D%5Bname%5D", "" },
                   { "columns%5B0%5D%5Bsearchable%5D", "true" },
                   { "columns%5B0%5D%5Borderable%5D", "true" },
                   { "columns%5B0%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B0%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B1%5D%5Bdata%5D", "col2.label" },
                   { "columns%5B1%5D%5Bname%5D", "" },
                   { "columns%5B1%5D%5Bsearchable%5D", "true" },
                   { "columns%5B1%5D%5Borderable%5D", "true" },
                   { "columns%5B1%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B1%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B2%5D%5Bdata%5D", "col3.label" },
                   { "columns%5B2%5D%5Bname%5D", "" },
                   { "columns%5B2%5D%5Bsearchable%5D", "true" },
                   { "columns%5B2%5D%5Borderable%5D", "true" },
                   { "columns%5B2%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B2%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B3%5D%5Bdata%5D", "col4.label" },
                   { "columns%5B3%5D%5Bname%5D", "" },
                   { "columns%5B3%5D%5Bsearchable%5D", "true" },
                   { "columns%5B3%5D%5Borderable%5D", "true" },
                   { "columns%5B3%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B3%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B4%5D%5Bdata%5D", "col5.label" },
                   { "columns%5B4%5D%5Bname%5D", "" },
                   { "columns%5B4%5D%5Bsearchable%5D", "true" },
                   { "columns%5B4%5D%5Borderable%5D", "true" },
                   { "columns%5B4%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B4%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B5%5D%5Bdata%5D", "col6.label" },
                   { "columns%5B5%5D%5Bname%5D", "" },
                   { "columns%5B5%5D%5Bsearchable%5D", "true" },
                   { "columns%5B5%5D%5Borderable%5D", "true" },
                   { "columns%5B5%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B5%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B6%5D%5Bdata%5D", "col7.label" },
                   { "columns%5B6%5D%5Bname%5D", "" },
                   { "columns%5B6%5D%5Bsearchable%5D", "true" },
                   { "columns%5B6%5D%5Borderable%5D", "true" },
                   { "columns%5B6%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B6%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B7%5D%5Bdata%5D", "col8.label" },
                   { "columns%5B7%5D%5Bname%5D", "" },
                   { "columns%5B7%5D%5Bsearchable%5D", "true" },
                   { "columns%5B7%5D%5Borderable%5D", "true" },
                   { "columns%5B7%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B7%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "columns%5B8%5D%5Bdata%5D", "col9.label" },
                   { "columns%5B8%5D%5Bname%5D", "" },
                   { "columns%5B8%5D%5Bsearchable%5D", "true" },
                   { "columns%5B8%5D%5Borderable%5D", "true" },
                   { "columns%5B8%5D%5Bsearch%5D%5Bvalue%5D", "" },
                   { "columns%5B8%5D%5Bsearch%5D%5Bregex%5D", "false" },

                   { "order%5B0%5D%5Bcolumn%5D", "0" },
                   { "order%5B0%5D%5Bdir%5D", "asc" },
                   { "start", "0" },
                   { "length", "500" },
                   { "search%5Bvalue%5D", "" },
                   { "search%5Bregex%5D", "false" },
                   { "let_from", date_beg_param.Trim().ToLower() },
                   { "let_to", date_end_param.Trim().ToLower() },
                   /*
                   { "let_from", "08.09.2015" },
                   { "let_to", "10.09.2015" },
                   */
                };                               

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync("http://www.roszdravnadzor.ru/ajax/services/lssearch", content);                
                var result = await response.Content.ReadAsStringAsync();
                preps = JsonConvert.DeserializeObject<DownloadedPreps>(result);                
                
            }

            if ((preps == null) || (preps.Records == null) || (preps.Records.Count <= 0))
            {
                return res;
            }

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            using (var dbContext = new AuMainDb())
            {
                // страны
                var countries = dbContext.prep_country.OrderBy(ss => ss.country_id).ToList();
                if (countries == null)
                    countries = new List<prep_country>();
                // статусы
                var defect_statuses = dbContext.defect_status.OrderBy(ss => ss.status_id).ToList();
                if (defect_statuses == null)
                    defect_statuses = new List<defect_status>();
                // документы
                var documents = dbContext.document.Where(ss => ss.accept_date >= date_beg && ss.accept_date <= date_end).OrderBy(ss => ss.document_id).ToList();
                if (documents == null)
                    documents = new List<document>();
                // препараты, привязанные к документам
                var existing_defects = (from def in dbContext.defect
                               from doc in dbContext.document
                               where def.document_id == doc.document_id
                               && doc.accept_date >= date_beg && doc.accept_date <= date_end
                               orderby doc.accept_date, def.full_name
                               select def)
                              .ToList();
                if (existing_defects == null)
                    existing_defects = new List<defect>();

                foreach (var prep in preps.Records)
                {
                    defect defect = new defect();

                    string pack_name = (String.IsNullOrEmpty(prep.Pack.ValueFull) ? (String.IsNullOrEmpty(prep.Pack.ValueShort) ? "[не указан]" : prep.Pack.ValueShort.Trim()) : prep.Pack.ValueFull.Trim());
                    string comm_name = (String.IsNullOrEmpty(prep.CommName.ValueFull) ? (String.IsNullOrEmpty(prep.CommName.ValueShort) ? "[не указан]" : prep.CommName.ValueShort.Trim()) : prep.CommName.ValueFull.Trim());
                    string producer = (String.IsNullOrEmpty(prep.Producer.ValueFull) ? (String.IsNullOrEmpty(prep.Producer.ValueShort) ? "[не указан]" : prep.Producer.ValueShort.Trim()) : prep.Producer.ValueFull.Trim());
                    string country_name = (String.IsNullOrEmpty(prep.Country.ValueFull) ? (String.IsNullOrEmpty(prep.Country.ValueShort) ? "[не указан]" : prep.Country.ValueShort.Trim()) : prep.Country.ValueFull.Trim());
                    string full_name = comm_name + " " + pack_name.Trim();

                    /*
                        Убираем из pack_name и из full_name множественные пробелы - заменяем их одним
                    */
                    pack_name = Regex.Replace(pack_name, @"\s+", " ");
                    full_name = Regex.Replace(full_name, @"\s+", " ");

                    /*
                        Если:
                            1) серия начинается с "Ан" (русские буквы), то серию оставляем как есть
                            2) серия = "без серии", то серию оставляем как есть
                        Иначе:                            
                            или 1) если встречается ; - создаем одну позицию: с серией до ; (без ;), и вторую позицию: серию обрезаем с конца до 1-го пробела (1-ый пробел с конца),
                                   все что между этими ; и пробелом - в начало значения defect_status_text второй позиции.
                            или 2) серию обрезаем до первого пробела - то, что слева (без пробела) оставляем как серию,
                                   то что справа (с пробелом в начале) добавляем в начало значения в поле "defect_status_text"
                        Дополнительно:
                            1) если правая часть не заканчивается на "." - добавлять "."
                            2) если основная часть заканчивается на "," - убирать ","
                            3) для серий (кроме анализов) переводить все схожие по написанию русские буквы в английские
                    */
                    string series_main_part = "";
                    string series_right_part = "";
                    string series_main_part2 = "";
                    string series_right_part2 = "";
                    bool needCreateSecondDefect = false;
                    bool needReplaceRusLetters = true;
                    if (!String.IsNullOrEmpty(prep.Series.ValueShort))
                    {
                        if (prep.Series.ValueShort.Trim().ToLower().StartsWith("ан"))
                        {
                            series_main_part = prep.Series.ValueShort.Trim();
                            needReplaceRusLetters = false;
                        }
                        else if (prep.Series.ValueShort.Trim().ToLower().StartsWith("без серии"))
                        {
                            series_main_part = prep.Series.ValueShort.Trim();
                            needReplaceRusLetters = false;
                        }
                        else
                        {
                            if (prep.Series.ValueShort.Trim().ToLower().Contains(";"))
                            {
                                needCreateSecondDefect = true;
                                series_main_part = prep.Series.ValueShort.Trim().Split(';')[0].Trim();
                                series_main_part2 = prep.Series.ValueShort.Trim().Split(' ').Last();
                                var series_length = prep.Series.ValueShort.Trim().Length;
                                var first_index_of_point_with_comma = prep.Series.ValueShort.Trim().IndexOf(';');
                                var last_index_of_space = prep.Series.ValueShort.Trim().LastIndexOf(' ');
                                try
                                {
                                    series_right_part2 = prep.Series.ValueShort.Trim().Substring(first_index_of_point_with_comma + 1, last_index_of_space - first_index_of_point_with_comma);
                                }
                                catch
                                {
                                    series_right_part2 = "";
                                }
                            }
                            else
                            {
                                series_main_part = prep.Series.ValueShort.Trim().Split(' ')[0].Trim();
                                try
                                {
                                    series_right_part = prep.Series.ValueShort.Trim().Substring(series_main_part.Length);
                                }
                                catch
                                {
                                    series_right_part = "";
                                }
                            }
                            if ((!String.IsNullOrEmpty(series_right_part)) && (!series_right_part.Trim().EndsWith(".")))
                                series_right_part = series_right_part.Trim() + ".";
                            if ((!String.IsNullOrEmpty(series_right_part2)) && (!series_right_part2.Trim().EndsWith(".")))
                                series_right_part2 = series_right_part2.Trim() + ".";
                        }
                        if ((!String.IsNullOrEmpty(series_main_part)) && (series_main_part.Trim().EndsWith(",")))
                            series_main_part = series_main_part.Trim().Remove(series_main_part.Trim().Length - 1);
                        if ((!String.IsNullOrEmpty(series_main_part2)) && (series_main_part2.Trim().EndsWith(",")))
                            series_main_part2 = series_main_part2.Trim().Remove(series_main_part2.Trim().Length - 1);

                        if (needReplaceRusLetters)
                        {
                            series_main_part = String.IsNullOrEmpty(series_main_part) ? series_main_part : GlobalUtil.ReplaceRusLetters(series_main_part);
                            series_main_part2 = String.IsNullOrEmpty(series_main_part2) ? series_main_part2 : GlobalUtil.ReplaceRusLetters(series_main_part2);
                            series_right_part = String.IsNullOrEmpty(series_right_part) ? series_right_part : GlobalUtil.ReplaceRusLetters(series_right_part);
                            series_right_part2 = String.IsNullOrEmpty(series_right_part2) ? series_right_part2 : GlobalUtil.ReplaceRusLetters(series_right_part2);
                        }
                    }

                    decimal? size_kb = null;
                    string num = "";
                    string date = "";
                    string size = "";
                    string url = "";
                    string err = "";
                    bool parse_res = false;
                    parse_res = DefectUtil.ParseDocumentName(prep.Document.ValueFull, out num, out date, out size, out err);
                    if (!parse_res)
                    {
                        res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка разбора номера письма [" + err.Trim() + "]");
                        return res;
                    }
                    string size_numbers = Regex.Replace(size, "[^0-9]", "");
                    size_kb = String.IsNullOrEmpty(size_numbers) ? null : (decimal?)Convert.ToDecimal(size_numbers);

                    parse_res = DefectUtil.ParseDocumentUrl(prep.Document.ValueShort, out url, out err);
                    if (parse_res)
                    {
                        url = "http://roszdravnadzor.ru/services/lssearch" + url.Trim();
                    }

                    var document = documents.Where(ss => ss.document_num.Trim().ToLower().Equals(num.Trim().ToLower())).FirstOrDefault();
                    if (document == null)
                    {
                        document = new document();
                        document.accept_date = Convert.ToDateTime(date).Date;
                        document.body_size_kb = size_kb;
                        document.link = url;
                        document.create_date = document.accept_date;
                        document.created_by = "pavlov";
                        document.created_on = now;
                        document.document_name = "Информационное письмо " + prep.Document.ValueFull.Trim();
                        document.document_num = num;
                        document.document_type_id = 1;

                        if ((!String.IsNullOrEmpty(url)) && (download_body))
                        {
                            using (var client = new HttpClient())
                            {
                                try
                                {
                                    var body = await client.GetByteArrayAsync(url);
                                    document.body = body;
                                }
                                catch (Exception ex)
                                {
                                    document.comment = GlobalUtil.ExceptionInfo(ex);
                                    document.body = null;
                                }
                            }                            
                        }

                        dbContext.document.Add(document);
                        dbContext.SaveChanges();
                        documents.Add(document);
                    }
                    
                    defect.document_id = document.document_id;
                    // ищем в списке препаратов, привязанных к этому же документу, аналогичные (торг. наимен. + упаковка + серия + производитель), и дубликаты пропускаем
                    var existing_defect = existing_defects.Where(ss => ss.document_id == defect.document_id                         
                        //&& ss.series.Trim().ToLower().Equals(prep.Series.ValueShort.Trim().ToLower())
                        && (ss.comm_name == null ? "" : ss.comm_name.Trim().ToLower()).Equals(comm_name.Trim().ToLower())
                        && (ss.pack_name == null ? "" : ss.pack_name.Trim().ToLower()).Equals(pack_name.Trim().ToLower())
                        && (ss.series == null ? "" : ss.series.Trim().ToLower()).Equals(series_main_part.Trim().ToLower())
                        && (ss.producer == null ? "" : ss.producer.Trim().ToLower()).Equals(producer.Trim().ToLower())
                        )
                        .FirstOrDefault();
                    if (existing_defect != null)
                        continue;

                    var country = countries.Where(ss => ss.name.Trim().ToLower().Equals(country_name.Trim().ToLower())).FirstOrDefault();
                    if (country == null)
                    {
                        country = new prep_country();
                        country.created_by = "pavlov";
                        country.created_on = now;
                        country.name = country_name.Trim();
                        dbContext.prep_country.Add(country);
                        dbContext.SaveChanges();
                        countries.Add(country);
                    }
                    defect.country_id = country.country_id;

                    defect_status defect_status = null;
                    var defect_status_downloaded = prep.DefectStatus.ValueShort;
                    defect_status_downloaded = String.IsNullOrEmpty(defect_status_downloaded) ? "" : defect_status_downloaded.Trim().ToLower();
                    if (String.IsNullOrEmpty(defect_status_downloaded))
                    {
                        defect_status = defect_statuses.Where(ss => ss.status_id == 101).FirstOrDefault();
                    }
                    else
                    {
                        defect_status = defect_statuses.Where(ss => ss.status_name.Trim().ToLower().Equals(defect_status_downloaded)).FirstOrDefault();
                    }
                    if (defect_status == null)
                        defect_status = defect_statuses.Where(ss => ss.status_id == 101).FirstOrDefault();
                    if (defect_status == null)
                    {
                        defect_status = new defect_status();
                        defect_status.status_id = 101;
                        defect_status.status_name = "Прочее";
                        dbContext.defect_status.Add(defect_status);
                        dbContext.SaveChanges();
                        defect_statuses.Add(defect_status);
                    }                    
                    defect.defect_status_id = defect_status.status_id;
                    defect.defect_status_text = series_right_part + (defect.defect_status_id == 101 ? " " + (String.IsNullOrEmpty(defect_status_downloaded) ? "" : defect_status_downloaded) : "");
                    
                    defect.defect_type = prep.DefectType.ValueShort.Trim();
                    defect.comm_name = comm_name;
                    defect.full_name = full_name;
                    defect.is_drug = 1;
                    defect.pack_name = pack_name;
                    defect.parent_defect_id = null;
                    defect.producer = producer;
                    defect.scope = prep.Scope.ValueShort.Trim();
                    //defect.series = prep.Series.ValueShort.Trim();
                    defect.series = series_main_part.Trim();
                    defect.defect_source_id = (int)Enums.DefectSourceEnum.ROSZDRAVNADZOR;
                    defect.created_by = "pavlov";
                    defect.created_on = now;
                    defect_list.Add(defect);

                    if (needCreateSecondDefect)
                    {
                        var existing_defect2 = existing_defects.Where(ss => ss.document_id == defect.document_id
                            && (ss.comm_name == null ? "" : ss.comm_name.Trim().ToLower()).Equals(defect.comm_name.Trim().ToLower())
                            && (ss.pack_name == null ? "" : ss.pack_name.Trim().ToLower()).Equals(defect.pack_name.Trim().ToLower())
                            && (ss.series == null ? "" : ss.series.Trim().ToLower()).Equals(series_main_part2.Trim().ToLower())
                            && (ss.producer == null ? "" : ss.producer.Trim().ToLower()).Equals(defect.producer.Trim().ToLower())
                            )
                            .FirstOrDefault();
                        if (existing_defect2 != null)
                            continue;
                        defect second_defect = new defect();
                        second_defect.comm_name = defect.comm_name;
                        second_defect.country_id = defect.country_id;
                        second_defect.created_by = defect.created_by;
                        second_defect.created_on = defect.created_on;
                        second_defect.defect_source_id = defect.defect_source_id;
                        second_defect.defect_status_id = defect.defect_status_id;
                        second_defect.defect_status_text = series_right_part2 + (defect.defect_status_id == 101 ? " " + (String.IsNullOrEmpty(defect_status_downloaded) ? "" : defect_status_downloaded) : "");
                        second_defect.defect_type = defect.defect_type;
                        second_defect.document_id = defect.document_id;
                        second_defect.full_name = defect.full_name;
                        second_defect.is_drug = defect.is_drug;
                        second_defect.pack_name = defect.pack_name;
                        second_defect.producer = defect.producer;
                        second_defect.scope = defect.scope;
                        second_defect.series = series_main_part2.Trim();
                        defect_list.Add(second_defect);    
                    }
                }
                
                if (defect_list.Count > 0)
                {
                    needRefreshXml = true;
                    dbContext.defect.AddRange(defect_list);
                    dbContext.SaveChanges();
                    res = new DefectList(defect_list);
                }
                
                Loggly.InsertLog_Defect("Скачано " + defect_list.Count.ToString() + " новых строк", (long)Enums.LogEsnType.DOWNLOAD_DEFECT, "downloader", null, now, null);

                res.NeedRefreshXml = needRefreshXml;
                return res;
            }
        }

        private void xMakeFiles()
        {
            xMakeXml();
            xMakeDbf();
        }

        private void xMakeXml()
        {
            DefectXml res = new DefectXml() { Data = "", RequestDate = null, RecordCount = 0, error = null, };
            string filePath = @"c:\Release\Defect\File\DefectXml.xml";
            string zipPath = @"c:\Release\Defect\File\DefectXml.zip";
            DateTime now = DateTime.Now;
            try
            {
                Encoding enc = Encoding.GetEncoding("windows-1251");
                if (File.Exists(filePath))
                    File.Delete(filePath);
                if (File.Exists(zipPath))
                    File.Delete(zipPath);

                using (var dbContext = new AuMainDb())
                {

                    var vw_defect = dbContext.vw_defect
                        .Where(ss => ss.is_drug == 1)
                        .OrderBy(ss => ss.created_on)
                        .ThenBy(ss => ss.accept_date)
                        .ThenBy(ss => ss.document_id)
                        .ThenBy(ss => ss.defect_id)                        
                        .ToList();

                    if ((vw_defect != null) && (vw_defect.Count > 0))
                    {
                        using (StreamWriter outfile = new StreamWriter(filePath, true, enc))
                        {
                            outfile.Write(@"<?xml version=""1.0"" encoding=""windows-1251""?><root>");
                            foreach (var item in vw_defect)
                            {
                                outfile.Write(DefectUtil.GetDefectXmlItem(item));
                            }
                            outfile.Write("</root>");
                        }
                    }
                    else
                    {
                        Loggly.InsertLog_Defect("vw_defect is empty", (long)Enums.LogEsnType.ERROR, "downloader-xml", null, now, null);
                    }

                    // упаковываем XML в ZIP
                    using (ZipArchive archive = ZipFile.Open(zipPath, ZipArchiveMode.Create, enc))
                    {
                        archive.CreateEntryFromFile(filePath, "DefectXml.xml");
                    }

                    Loggly.InsertLog_Defect("В XML записано " + vw_defect.Count.ToString() + " строк", (long)Enums.LogEsnType.MAKE_DEFECT_XML, "downloader-xml", null, now, null);
                }
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Defect(GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, "downloader-xml", null, now, null);
                return;
            }
        }

        private void xMakeDbf()
        {
            int cnt = 0;
            string curr_str = "";
            DateTime now = DateTime.Now;
            try
            {

                // http://www.cyberforum.ru/csharp-beginners/thread453711.html

                //var odbf = new DbfFile(Encoding.GetEncoding(1252));
                var odbf = new DbfFile(Encoding.Default);
                odbf.Open(@"C:\Release\Defect\File\Dbf\remove.dbf", FileMode.Create);

                //create a header
                odbf.Header.AddColumn(new DbfColumn("MNFGRNX", DbfColumn.DbfColumnType.Number, 20, 0));
                odbf.Header.AddColumn(new DbfColumn("MNFNX", DbfColumn.DbfColumnType.Number, 20, 0));
                odbf.Header.AddColumn(new DbfColumn("MNFNMR", DbfColumn.DbfColumnType.Character, 254, 0));
                odbf.Header.AddColumn(new DbfColumn("COUNTRYR", DbfColumn.DbfColumnType.Character, 150, 0));
                odbf.Header.AddColumn(new DbfColumn("DRUGTXT", DbfColumn.DbfColumnType.Character, 254, 0));
                odbf.Header.AddColumn(new DbfColumn("SERNM", DbfColumn.DbfColumnType.Character, 50, 0));
                odbf.Header.AddColumn(new DbfColumn("LETTERSNR", DbfColumn.DbfColumnType.Character, 50, 0));
                odbf.Header.AddColumn(new DbfColumn("LETTERSDT", DbfColumn.DbfColumnType.Date));
                odbf.Header.AddColumn(new DbfColumn("LABNMR", DbfColumn.DbfColumnType.Character, 200, 0));
                odbf.Header.AddColumn(new DbfColumn("QUALNMR", DbfColumn.DbfColumnType.Character, 254, 0));
                odbf.Header.AddColumn(new DbfColumn("TRADENMNX", DbfColumn.DbfColumnType.Number, 20, 0));
                odbf.Header.AddColumn(new DbfColumn("INNNX", DbfColumn.DbfColumnType.Number, 20, 0));
                odbf.Header.AddColumn(new DbfColumn("ISALLS", DbfColumn.DbfColumnType.Number, 20, 0));
                odbf.Header.AddColumn(new DbfColumn("DISTRIB", DbfColumn.DbfColumnType.Character, 80, 0));
                odbf.Header.AddColumn(new DbfColumn("STATDEF", DbfColumn.DbfColumnType.Character, 80, 0));

                var orec = new DbfRecord(odbf.Header) { AllowDecimalTruncate = true };

                using (var dbContext = new AuMainDb())
                {
                    var defect_dbf = dbContext.vw_defect_dbf.OrderBy(ss => ss.ID).ToList();
                    if ((defect_dbf != null) && (defect_dbf.Count > 0))
                    {
                        cnt = defect_dbf.Count;
                        foreach (var item in defect_dbf)
                        {
                            curr_str = item.DRUGTXT;
                            orec[0] = item.MNFGRNX.ToString();
                            orec[1] = item.MNFNX.ToString();
                            orec[2] = item.MNFNMR.ToString();
                            orec[3] = item.COUNTRYR;
                            orec[4] = item.DRUGTXT;
                            orec[5] = item.SERNM;
                            orec[6] = item.LETTERSNR;
                            orec[7] = item.LETTERSDT.ToString();
                            orec[8] = item.LABNMR;
                            orec[9] = item.QUALNMR;
                            orec[10] = item.TRADENMNX.ToString();
                            orec[11] = item.INNNX.ToString();
                            orec[12] = item.ISALLS.ToString();
                            orec[13] = item.DISTRIB;
                            orec[14] = item.STATDEF;
                            odbf.Write(orec, true);
                        }
                    }
                }
                
                odbf.WriteHeader();
                odbf.Close();
                Loggly.InsertLog_Defect("В DBF записано " + cnt.ToString() + " строк", (long)Enums.LogEsnType.MAKE_DEFECT_XML, "downloader-dbf", null, now, null);

                // pack DBF and upload to ftp

                if (File.Exists(@"C:\Release\Defect\File\Arc\remove.zip"))
                {
                    File.Delete(@"C:\Release\Defect\File\Arc\remove.zip");
                }

                string startPath = @"C:\Release\Defect\File\Dbf";
                string zipPath = @"C:\Release\Defect\File\Arc\remove.zip";
                ZipFile.CreateFromDirectory(startPath, zipPath, CompressionLevel.Optimal, false);

                File.Copy(@"C:\Release\Defect\File\Arc\remove.zip", @"C:\Sites\CAP\CAP.SiteNew\App_Data\Files\Услуги\remove.zip", true);
                Loggly.InsertLog_Defect("DBF скопирован в Услуги", (long)Enums.LogEsnType.MAKE_DEFECT_XML, "downloader-dbf", null, now, null);
                
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Scp,
                    HostName = "aptekaural.ru",
                    UserName = "aptekaural",
                    //Password = "U435JkjDF",
                    //Password = "U447JkjDF",
                    Password = "U447JkjDf",
                    //SshHostKeyFingerprint = "ssh-rsa 2048 53:d7:cf:bb:c2:29:50:08:a7:7e:4d:6f:62:f8:b8:da",
                    SshHostKeyFingerprint = "ssh-rsa 2048 00:a9:f8:0a:52:7e:12:c2:b5:54:2b:d5:b6:42:f0:f6",
                };

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = WinSCP.TransferMode.Binary;                    

                    TransferOperationResult transferResult;
                    transferResult = session.PutFiles(@"C:\Release\Defect\File\Arc\remove.zip", "/web/aptekaural/site/www/docs/reestr/", false, transferOptions);
                    transferResult = session.PutFiles(@"C:\Release\Defect\File\Arc\remove.zip", "/web/aptekaural/site/www/inc/", false, transferOptions);
                    transferResult = session.PutFiles(@"C:\Release\Defect\File\Arc\remove.zip", "/web/aptekaural/site/www/downloads_all/", false, transferOptions);
                    transferResult = session.PutFiles(@"C:\Release\Defect\File\Arc\remove.zip", "/web/aptekaural/site/www/remove/", false, transferOptions);
                    
                    // Throw on any error
                    transferResult.Check();                                        
                }
                Loggly.InsertLog_Defect("DBF скопирован на FTP", (long)Enums.LogEsnType.MAKE_DEFECT_XML, "downloader-dbf", null, now, null);

                // copy to \192.168.0.101\Услуги$                
                /*
                NetworkCredential writeCredentials = new NetworkCredential("Михаил Павлов", "123456", "aptekaural");                
                using (new NetworkConnection(@"\\192.168.0.101\Услуги$", writeCredentials))
                {
                    File.Copy(@"C:\Release\Defect\File\Arc\remove.zip", @"\\192.168.0.101\Услуги$\remove.zip", true);
                }
                Loggly.InsertLog_Defect("DBF скопирован на .101", (long)Enums.LogEsnType.MAKE_DEFECT_XML, "downloader-dbf", null, now, null);
                */
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_Defect(GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, "downloader-dbf", null, now, null);
                return;
            }
            
        }

        #endregion

        #region GetDefectList

        private DefectList xGetDefectList(string password, DateTime? date_beg, DateTime? date_end)
        {
            if (password != "iguana")
                return new DefectList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            DateTime date_beg_int = date_beg.HasValue ? ((DateTime)date_beg).Date : new DateTime(1, 1, 1990);
            DateTime date_end_int = date_end.HasValue ? ((DateTime)date_end).Date : new DateTime(1, 1, 2090);
            DefectList res = new DefectList() { Defect_list = new List<Defect>(), };
            using (var dbContext = new AuMainDb())
            {
                var list = (from def in dbContext.defect
                            where def.created_on >= date_beg_int && def.created_on <= date_end_int
                            orderby def.created_on, def.full_name
                            select def)
                           .ToList();
                if ((list == null) || (list.Count <= 0))
                    return res;
                res = new DefectList(list);
                return res;
            }
        }

        #endregion

        #region GetDefectXml

        private DefectXml xGetDefectXml(string login, string workplace, string version_num)
        {
            request req = null;
            request req_new = null;
            DefectXml res = new DefectXml() { Data = "", RequestDate = null, RecordCount = 0, error = null, };

            /*
             проверка лицензии
            */
            License lic = GetLicense(login, workplace, version_num, license_id_DEFECT);
            if (lic.error != null)
            {
                return new DefectXml() { error = lic.error };
            }

            using (var dbContext = new AuMainDb())
            {
                /*
                    при закачке только нового:
                    1. ищем последний подвержденный request
                    2. если не находим - ругаемся и предлагаем закачать все
                    3. создаем новый request с типом 0 и state = 0
                    4. отсылаем все данные c created_on > request.request_date c прицепеленным request, ждем подтверждения
                */

                req = dbContext.request
                    .Where(ss => ss.scope == (int)Enums.LogScope.DEFECT 
                        && ss.state == 1
                        && ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                    )
                    .OrderByDescending(ss => ss.request_date)
                    .FirstOrDefault();

                if (req == null)
                    return new DefectXml() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден ни один подтвержденный запрос, необходимо закачать весь список брака") };

                req_new = new request();
                req_new.scope = (int)Enums.LogScope.DEFECT;
                req_new.login = login;
                req_new.record_cnt = 0;
                req_new.request_date = DateTime.Now;
                req_new.state = 0;
                req_new.version_num = version_num;
                req_new.workplace = workplace;
                req_new.request_type = 0;

                var vw_defect = dbContext.vw_defect
                    .Where(ss => ss.created_on > req.request_date)
                    .OrderBy(ss => ss.accept_date).ThenBy(ss => ss.document_id).ThenBy(ss => ss.full_name).ToList();

                if (vw_defect != null)
                {
                    foreach (var item in vw_defect)
                    {
                        res.AddData(item);
                    }
                    res.AddDataCompleted();
                }

                req_new.record_cnt = (vw_defect == null) ? 0 : vw_defect.Count;
                dbContext.request.Add(req_new);
                dbContext.SaveChanges();

                res.RequestDate = req_new.request_date;
                res.RecordCount = req_new.record_cnt.GetValueOrDefault(0);
                res.RequestId = req_new.request_id;

                return res;
            }
        }

        private DefectXml xGetDefectXmlInit(string login, string workplace, string version_num, bool is_arc)
        {            
            request req_new = null;
            DefectXml res = new DefectXml() { Data = "", RequestDate = null, RecordCount = 0, error = null, };

            /*
             проверка лицензии
            */
            License lic = GetLicense(login, workplace, version_num, license_id_DEFECT);
            if (lic.error != null)
            {
                return new DefectXml() { error = lic.error };
            }

            using (var dbContext = new AuMainDb())
            {
                /*
                при полной закачке:
                    1. не обращаем внимание на существующие request
                    2. создаем новый request с типом 1 и state = 0
                    3. отсылаем все данные с прицепеленным request, ждем подтверждения
                */

                req_new = new request();
                req_new.scope = (int)Enums.LogScope.DEFECT;
                req_new.login = login;
                req_new.record_cnt = 0;
                req_new.request_date = DateTime.Now;
                req_new.state = 0;
                req_new.version_num = version_num;
                req_new.workplace = workplace;
                req_new.request_type = 1;

                Encoding enc = Encoding.GetEncoding("windows-1251");
                string contents = "";
                byte[] contents_binary = null;
                if (!is_arc)
                    contents = File.ReadAllText(@"c:\Release\Defect\File\DefectXml.xml", enc);
                else
                {
                    //contents = File.ReadAllText(@"c:\Release\Defect\File\DefectXml.zip", enc);

                    //contents_binary = File.ReadAllBytes(@"c:\Release\Defect\File\DefectXml.zip");
                    //contents = Convert.ToBase64String(contents_binary);

                    //contents_binary = GlobalUtil.Zip(File.ReadAllText(@"c:\Release\Defect\File\DefectXml.zip", enc));
                    //contents = Convert.ToBase64String(contents_binary);
                    
                    //contents = GlobalUtil.CompressString(File.ReadAllText(@"c:\Release\Defect\File\DefectXml.zip", enc));                    
                    
                    contents_binary = File.ReadAllBytes(@"c:\Release\Defect\File\DefectXml.zip");
                    
                }

                if (is_arc)
                {
                    res.Data = "";
                    res.DataBinary = contents_binary;
                }
                else
                {
                    res.Data = contents;
                    res.DataBinary = null;
                }
                res.IsArchive = is_arc;
                res.IsBinary = is_arc;
                //res.AddDataCompleted();

                dbContext.request.Add(req_new);
                dbContext.SaveChanges();

                res.RequestDate = req_new.request_date;
                res.RecordCount = req_new.record_cnt.GetValueOrDefault(0);
                res.RequestId = req_new.request_id;                

                return res;
            }
        }

        #endregion        

        #region GetLastDefectRequest

        private DefectRequest xGetLastDefectRequest(string login, string workplace, string version_num)
        {
            if (String.IsNullOrEmpty(login))
            {
                return new DefectRequest() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не указан логин") };
            }

            if (String.IsNullOrEmpty(workplace))
            {
                return new DefectRequest() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не указано рабочее место") };
            }

            DefectRequest res = new DefectRequest() { date_letter = "", date_request = "", };
            using (var dbContext = new AuMainDb())
            {
                var last_request = dbContext.request.Where(ss => ss.state == (int)Enums.RequestStateEnum.CONFIRMED && ss.commit_date.HasValue && ss.login.Trim().ToLower().Equals(login.Trim().ToLower()))
                    .OrderByDescending(ss => ss.commit_date)
                    .FirstOrDefault();
                if (last_request == null)
                    return res;
                
                var date_request = (DateTime)last_request.commit_date;
                res.date_request = date_request.ToString("dd.MM.yyyy HH:mm:ss");
                
                var last_letter  = dbContext.document.Where(ss => ss.create_date <= date_request)
                    .OrderByDescending(ss => ss.create_date)
                    .Select(ss => ss.accept_date)
                    .FirstOrDefault();
                if (last_letter.HasValue)
                    res.date_letter = ((DateTime)last_letter).ToString("dd.MM.yyyy HH:mm:ss");

                return res;
            }
        }

        #endregion

        #region CheckNewDefect

        private CheckNewDefectResult xCheckNewDefect(string login, string workplace, string version_num)
        {
            if (String.IsNullOrEmpty(login))
            {
                return new CheckNewDefectResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не указан логин") };
            }

            if (String.IsNullOrEmpty(workplace))
            {
                return new CheckNewDefectResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не указано рабочее место") };
            }

            var getLastDefectRequestResult = xGetLastDefectRequest(login, workplace, version_num);
            if ((getLastDefectRequestResult == null) || (getLastDefectRequestResult.error != null))
            {                
                return new CheckNewDefectResult() { error = getLastDefectRequestResult != null ? getLastDefectRequestResult.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нет данных") };
            }

            DateTime date_request = new DateTime(2000, 1, 1);
            if (!String.IsNullOrWhiteSpace(getLastDefectRequestResult.date_request))
            {
                if (!DateTime.TryParseExact(getLastDefectRequestResult.date_request.Trim().ToLower(), "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.CurrentCulture, System.Globalization.DateTimeStyles.None, out date_request))
                    date_request = new DateTime(2000, 1, 1);
            }

            using (var context = new AuMainDb())
            {
                int defectCnt = context.defect.Where(ss => ss.created_on > date_request).Count();
                document lastDocument = context.document.Where(ss => ss.created_on > date_request).OrderByDescending(ss => ss.created_on).FirstOrDefault();
                DateTime? lastDocumentDate = lastDocument == null ? null : lastDocument.create_date;

                return new CheckNewDefectResult() { error = null, cnt = defectCnt, date_letter = lastDocumentDate.HasValue ? ((DateTime)lastDocumentDate).ToString("dd.MM.yyyy HH:mm:ss") : null };
            }

        }

        #endregion
    }
}
