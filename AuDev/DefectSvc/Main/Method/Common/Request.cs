﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {
        private RequestList xGetRequestList(string login, string workplace, string version_num)
        {
            RequestList res = new RequestList() { Request_list = new List<Request>(), };
            using (var dbContext = new AuMainDb())
            {
                var list = dbContext.request
                    .Where(ss => ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && ((ss.workplace.Trim().ToLower().Equals(workplace.Trim().ToLower())) || (String.IsNullOrEmpty(workplace.Trim())))
                        && ((ss.version_num.Trim().ToLower().Equals(version_num.Trim().ToLower())) || (String.IsNullOrEmpty(version_num.Trim())))
                    )
                    .OrderBy(ss => ss.request_date).ToList();
                if ((list == null) || (list.Count <= 0))
                    return res;
                res = new RequestList(list);
                return res;
            }
        }

        private RequestList xGetActiveRequestList(string login, string workplace, string version_num)
        {
            RequestList res = new RequestList() { Request_list = new List<Request>(), };
            using (var dbContext = new AuMainDb())
            {
                var list = dbContext.request
                    .Where(ss => ss.state == 0
                        && ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && ((ss.workplace.Trim().ToLower().Equals(workplace.Trim().ToLower())) || (String.IsNullOrEmpty(workplace.Trim())))
                        && ((ss.version_num.Trim().ToLower().Equals(version_num.Trim().ToLower())) || (String.IsNullOrEmpty(version_num.Trim())))
                    )
                    .OrderBy(ss => ss.request_date).ToList();
                if ((list == null) || (list.Count <= 0))
                    return res;
                res = new RequestList(list);
                return res;
            }
        }

        private Request xCommitRequest(string login, string workplace, string version_num, long request_id, int scope, int? scope2)
        {
            Request res = new Request();            
            using (var dbContext = new AuMainDb())
            {
                var req = dbContext.request
                    .Where(ss => ss.request_id == request_id
                        && ss.scope == scope
                        && (((scope2.HasValue) && (ss.scope2.HasValue) && (ss.scope2 == scope2)) || (!scope2.HasValue))
                        && ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && ss.workplace.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        //&& ss.version_num.Trim().ToLower().Equals(version_num.Trim().ToLower())
                        )
                        .FirstOrDefault();
                if (req == null)
                    return new Request() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден запрос с кодом " + request_id.ToString()) };
                if (req.state != 0)
                    return new Request() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Запрос с кодом " + request_id.ToString() + " не в состоянии ожидания подтверждения") };

                DateTime now = DateTime.Now;
                req.state = 1;
                req.commit_date = now;

                var old_req_list = dbContext.request
                    .Where(ss => ss.request_id != request_id
                        && ss.scope == scope
                        && ss.request_date < req.request_date
                        && ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                        /*
                        && ss.workplace.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        && ss.version_num.Trim().ToLower().Equals(version_num.Trim().ToLower())
                        */
                        )
                        .ToList();
                if (old_req_list != null)
                {
                    foreach (var item in old_req_list)
                    {
                        item.state = 1;
                        item.commit_date = now;
                    }
                }               

                dbContext.SaveChanges();
                
                res = new Request(req);
                return res;
            }
        }
    }

}
