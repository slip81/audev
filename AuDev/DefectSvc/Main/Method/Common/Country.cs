﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {
        private CountryList xGetCountryList()
        {
            CountryList res = new CountryList() { Country_list = new List<Country>(), };
            using (var dbContext = new AuMainDb())
            {
                var list = dbContext.prep_country.OrderBy(ss => ss.name).ToList();
                if ((list == null) || (list.Count <= 0))
                    return res;
                res = new CountryList(list);
                return res;
            }
        }
    }
}
