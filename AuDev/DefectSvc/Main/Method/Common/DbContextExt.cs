﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;

namespace DefectSvc
{
    public partial class DefectService : IDefectService
    {


        // Fastest Way of Inserting in Entity Framework
        // http://stackoverflow.com/questions/5940225/fastest-way-of-inserting-in-entity-framework

        private AuMainDb AddToContext<TEntity>(AuMainDb context,
            TEntity entity, int count, int commitCount, bool recreateContext)
            where TEntity : class
        {
            context.Set<TEntity>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

    }
}
