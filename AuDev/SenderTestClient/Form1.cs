﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SenderTestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //
            System.Net.ServicePointManager.Expect100Continue = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SenderServ.SenderServiceClient serv = new SenderServ.SenderServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.SendMail_Defect();
                MessageBox.Show(res == 0 ? "OK" : "Error: " + res.ToString());
            }
            finally
            {
                Cursor = Cursors.Default;
            }  
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SenderServ.SenderServiceClient serv = new SenderServ.SenderServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.SendMail_Grls();
                MessageBox.Show(res == 0 ? "OK" : "Error: " + res.ToString());
            }
            finally
            {
                Cursor = Cursors.Default;
            } 
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            SenderServ.SenderServiceClient serv = new SenderServ.SenderServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetExchangeData_Plan(new SenderServ.UserInfo() { login = "1", workplace = "2", version_num = "3", });                
                //MessageBox.Show(res.error == null ? "OK" : "Error: " + res.error.Message);
                textBox1.Text = res.error == null ? res.data : ("Error: " + res.error.Message);
            }
            finally
            {
                Cursor = Cursors.Default;
            } 
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            SenderServ.SenderServiceClient serv = new SenderServ.SenderServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.CVA_GetSendInfo(new SenderServ.UserInfo() { login = "farma1", workplace = "7A58-0C40-6CAC-E155", version_num = "", });
                //MessageBox.Show(res.error == null ? "OK" : "Error: " + res.error.Message);
                textBox1.Text = res.error == null ? res.cvaInfo.last_sent_date.ToString() : ("Error: " + res.error.Message);
            }
            finally
            {
                Cursor = Cursors.Default;
            } 
        }
    }
}
