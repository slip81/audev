﻿namespace CabinetNotifyClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox5 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox6 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox7 = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(346, 220);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(77, 29);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "Start";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // radTextBox1
            // 
            this.radTextBox1.Location = new System.Drawing.Point(45, 12);
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(120, 20);
            this.radTextBox1.TabIndex = 1;
            this.radTextBox1.Text = "666065592";
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(45, 38);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.PasswordChar = '*';
            this.radTextBox2.Size = new System.Drawing.Size(120, 20);
            this.radTextBox2.TabIndex = 2;
            this.radTextBox2.Text = "Toucan82";
            // 
            // radTextBox3
            // 
            this.radTextBox3.AutoSize = false;
            this.radTextBox3.Location = new System.Drawing.Point(45, 110);
            this.radTextBox3.Multiline = true;
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox3.Size = new System.Drawing.Size(176, 139);
            this.radTextBox3.TabIndex = 3;
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(429, 220);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(77, 29);
            this.radButton2.TabIndex = 4;
            this.radButton2.Text = "End";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // radTextBox4
            // 
            this.radTextBox4.AutoSize = false;
            this.radTextBox4.Location = new System.Drawing.Point(228, 12);
            this.radTextBox4.Multiline = true;
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox4.Size = new System.Drawing.Size(195, 41);
            this.radTextBox4.TabIndex = 5;
            this.radTextBox4.Text = "пришло это сообщение ?";
            // 
            // radButton3
            // 
            this.radButton3.Location = new System.Drawing.Point(429, 12);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(77, 29);
            this.radButton3.TabIndex = 6;
            this.radButton3.Text = "Send";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radTextBox5
            // 
            this.radTextBox5.AutoSize = false;
            this.radTextBox5.Location = new System.Drawing.Point(228, 110);
            this.radTextBox5.Multiline = true;
            this.radTextBox5.Name = "radTextBox5";
            this.radTextBox5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox5.Size = new System.Drawing.Size(195, 45);
            this.radTextBox5.TabIndex = 7;
            // 
            // radTextBox6
            // 
            this.radTextBox6.Location = new System.Drawing.Point(45, 84);
            this.radTextBox6.Name = "radTextBox6";
            this.radTextBox6.Size = new System.Drawing.Size(120, 20);
            this.radTextBox6.TabIndex = 8;
            this.radTextBox6.Text = "253002215";
            // 
            // radTextBox7
            // 
            this.radTextBox7.AutoSize = false;
            this.radTextBox7.Location = new System.Drawing.Point(228, 63);
            this.radTextBox7.Multiline = true;
            this.radTextBox7.Name = "radTextBox7";
            this.radTextBox7.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.radTextBox7.Size = new System.Drawing.Size(195, 41);
            this.radTextBox7.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 261);
            this.Controls.Add(this.radTextBox7);
            this.Controls.Add(this.radTextBox6);
            this.Controls.Add(this.radTextBox5);
            this.Controls.Add(this.radButton3);
            this.Controls.Add(this.radTextBox4);
            this.Controls.Add(this.radButton2);
            this.Controls.Add(this.radTextBox3);
            this.Controls.Add(this.radTextBox2);
            this.Controls.Add(this.radTextBox1);
            this.Controls.Add(this.radButton1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadTextBox radTextBox4;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadTextBox radTextBox5;
        private Telerik.WinControls.UI.RadTextBox radTextBox6;
        private Telerik.WinControls.UI.RadTextBox radTextBox7;
    }
}

