﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;

namespace CabinetNotifyClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Form1 fm = new Form1();

            System.Timers.Timer myTimer = new System.Timers.Timer();
            myTimer.AutoReset = true;
            myTimer.Elapsed += new ElapsedEventHandler(fm.ProcessMessages);
            myTimer.Interval = 30000; // 30 секунд
            myTimer.Start();

            Application.Run(fm);
        }
    }
}
