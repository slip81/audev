﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Data.SqlClient;
using Npgsql;

using IcqSharp;

namespace CabinetNotifyClient
{
    public partial class Form1 : Form
    {
        Session s;
        string onDisconnected = "";
        string onConnected = "";
        string onConnecting = "";
        string onConnectionError = "";
        string onMessageSent = "";
        string onMessageReceived = "";

        int last_id = 0;
        //SqlConnection conn;
        //SqlCommand sql;
        NpgsqlConnection conn;
        NpgsqlCommand sql;
        bool db_connected = false;
        bool icq_connected = false;
            
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (s != null)
                s.Dispose();
        }

        public void AppendTextBox3(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AppendTextBox3), new object[] { value });
                return;
            }
            radTextBox3.Text += value;
            radTextBox3.Text += System.Environment.NewLine;
        }

        public void AppendTextBox5(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AppendTextBox5), new object[] { value });
                return;
            }
            radTextBox5.Text += value;
            radTextBox5.Text += System.Environment.NewLine;
        }

        public void AppendTextBox7(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AppendTextBox7), new object[] { value });
                return;
            }
            radTextBox7.Text += value;
            radTextBox7.Text += System.Environment.NewLine;
        }

        private void Do_Connect()
        {
            if ((s != null) && (s.IsConnected))
                return;

            //433179087 - Техносбыт Technosb1
            //666065592 - Аурит Toucan82
            string uin = radTextBox1.Text;
            string password = radTextBox2.Text;

            radTextBox3.Text = "";
            radTextBox5.Text = "";
            radTextBox7.Text = "";

            s = new Session(uin, password);

            s.Connecting += s_Connecting;
            s.Connected += s_Connected;
            s.ConnectionError += s_ConnectionError;
            s.Disconnected += s_Disconnected;
            s.Messaging.MessageSent += Messaging_MessageSent;
            s.Messaging.MessageReceived += Messaging_MessageReceived;

            s.Connect();

            //
            DoConnect_DB();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            Do_Connect();
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            Disconnect();
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            SendMessage(radTextBox6.Text.Trim(), radTextBox4.Text.Trim());
        }

        private void Disconnect(bool disconnectIcq = true, bool disonnectDb = true)
        {
            if (disconnectIcq)
            {
                if (s != null)
                    s.Dispose();
            }

            if (disonnectDb)
            {
                if (conn != null)
                    conn.Dispose();
            }
        }

        void s_Disconnected(object sender, EventArgs e)
        {
            icq_connected = false;
            onDisconnected = "Disconnected";
            AppendTextBox3(onDisconnected);
        }

        void s_ConnectionError(object sender, EventArgs e)
        {
            icq_connected = false;
            onConnectionError = "Connection error";
            AppendTextBox3(onConnectionError);
        }
        
        void s_Connecting(object sender, EventArgs e)
        {
            onConnecting = "Connecting...";
            AppendTextBox3(onConnecting);
        }

        void s_Connected(object sender, EventArgs e)
        {
            icq_connected = true;
            onConnected = "Connected !";
            AppendTextBox3(onConnected);
        }

        void Messaging_MessageSent(IcqSharp.Base.Message message)
        {
            onMessageSent = "Message sent !";
            AppendTextBox5(onMessageSent);
        }

        void Messaging_MessageReceived(IcqSharp.Base.Message message)
        {
            onMessageReceived = "Message received !";
            AppendTextBox5(onMessageReceived);
            AppendTextBox7(message.Text);
        }

        private string getCurrentTime()
        {
            return "[" + DateTime.Now.ToString("HH:mm:ss") + "] ";
        }

        private void DoConnect_DB()
        {
            if (db_connected) return;

            /*
            string conn_str = "Persist Security Info=true;"
                            + "User ID=test;"
                            + "Initial Catalog=technosb;"                
                            + "Data Source=SQL2008;"
                            + "Password=123;"
                            + "Packet Size=4096;"
                            + "Application Name=tsbicq;"
                            ;
            */

            //string conn_str = "PORT=5432;TIMEOUT=15;POOLING=True;MINPOOLSIZE=1;MAXPOOLSIZE=20;COMMANDTIMEOUT=0;COMPATIBLE=2.2.3.0;PASSWORD=123;USER ID=test;DATABASE=AuMainDb;HOST=localhost;PRELOADREADER=True";
            string conn_str = System.Configuration.ConfigurationManager.ConnectionStrings["AuMainDb"].ConnectionString;

            conn = new NpgsqlConnection(conn_str);
            try
            {
                conn.Open();
                AppendTextBox3(getCurrentTime() + "Подключение к БД установлено");
            }
            catch (Exception exc)
            {
                //ChatlistBox.Items.Add(getCurrentTime() + "Не удалось подключиться к базе данных");
                //ChatlistBox.Items.Add(getCurrentTime() + exc.ToString());
                AppendTextBox3(getCurrentTime() + "Не удалось подключиться к базе данных");
                AppendTextBox3(getCurrentTime() + exc.ToString());
                conn.Dispose();
                return;
            }

            DoConnect_DB_after();

            db_connected = true;

        }

        private void DoConnect_DB_after()
        {
            /*
            sql = new NpgsqlCommand(" SELECT isnull(MAX(t1.EventMessageId), 0) as last_id FROM EventMessage t1 WITH (NOLOCK)"
                                + " WHERE isnull(t1.message_sent, 0) <> 1"
                                + " AND isnull(t1.message_target_id, 0) > 0"
                                , conn);
            */
            
            CabinetServ.CabinetServiceClient serv =  new CabinetServ.CabinetServiceClient();
            var res = serv.GetLastNotify("iguana");
            if (res.error != null)
            {
                AppendTextBox3(getCurrentTime() + res.error.Message);
                Disconnect();
                return;
            }
            last_id = res.notify_id;
            if (last_id == 0)
            {
                AppendTextBox3(getCurrentTime() + "Нет сообщений в таблице notify");
            }

            /*
            sql = new NpgsqlCommand("select coalesce(max(notify_id), 0) as max_notify_id from cabinet.vw_notify where scope = 1 and message_sent = false and trim(coalesce(icq, '')) != ''"
                                , conn);
            last_id = (int)sql.ExecuteScalar();
            if (last_id == 0)
            {
                sql = new NpgsqlCommand("select coalesce(max(notify_id), 0) as max_notify_id from cabinet.vw_notify where scope = 1"
                    , conn);
                last_id = (int)sql.ExecuteScalar();
                if (last_id == 0)
                {
                    AppendTextBox3(getCurrentTime() + "Нет сообщений в таблице EventMessage");
                }
                //Disconnect(false, true);                
                //return;
            }
            */ 

        }

        public void ProcessMessages(object source, ElapsedEventArgs e)
        {                        
            if (!icq_connected)
            {
                AppendTextBox3(getCurrentTime() + "Нет связи с сервером ICQ");
                return;
            }

            if (!db_connected)
            {
                AppendTextBox3(getCurrentTime() + "Нет связи с сервером БД");
                return;
            }           

            NpgsqlDataReader rdr;
            bool f = false;
            string ids_for_update = "";
            string mess = "";
            string icq = "";

            CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient();
            var res = serv.GetNotifyList("iguana", last_id);
            if (res.error != null)
            {
                AppendTextBox3(getCurrentTime() + res.error.Message);
                Disconnect();
                return;
            }

            try
            {
                foreach (var notify in res.Notify_list)
                {
                    last_id = notify.notify_id;
                    if (notify.message_sent)
                        continue;

                    mess = notify.message;
                    icq = notify.icq;                    
                    if (!String.IsNullOrEmpty(icq))
                    {
                        if (SendMessage(icq, mess))
                        {
                            AppendTextBox3(getCurrentTime() + "-> " + icq + ": " + mess);
                        }
                        else
                        {
                            AppendTextBox3(getCurrentTime() + "-> " + " !! НЕ ОТПРАВЛЕНО !! " + icq + ": " + mess);
                        }  
                    }                    
                    if (f) ids_for_update = ids_for_update + ",";
                    ids_for_update += last_id.ToString();
                    f = true;
                }
            }
            finally
            {
                if (ids_for_update.Length > 0)
                {
                    var upd_res = serv.UpdateNotifyList("iguana", ids_for_update);
                    if (upd_res.error != null)
                    {
                        AppendTextBox3(getCurrentTime() + upd_res.error.Message);
                        Disconnect();                        
                    }
                }
                
            }

            /*
            if (sql == null)
                sql = new NpgsqlCommand("", conn);

            sql.CommandText = " SELECT t1.notify_id, t1.message, t1.crt_date, t1.icq"
                            + " FROM cabinet.vw_notify t1"
                            + " WHERE t1.notify_id >= " + last_id.ToString()
                            + " AND t1.message_sent = false"
                            + " AND trim(coalesce(t1.icq, '')) != ''"
                            + " ORDER BY t1.notify_id";

            rdr = sql.ExecuteReader();
            try
            {
                while (rdr.Read())
                {
                    rdr_res = rdr.GetString(1);

                    if (rdr.GetString(3).Length > 0)
                    {
                        if (SendMessage(rdr.GetString(3), rdr_res))
                        {
                            AppendTextBox3(getCurrentTime() + "-> " + rdr.GetString(3).Trim() + ": " + rdr_res);
                        }
                        else
                        {
                            AppendTextBox3(getCurrentTime() + "-> " + " !! НЕ ОТПРАВЛЕНО !! " + rdr.GetString(3).Trim() + ": " + rdr_res);
                        }                        
                    }

                    last_id = rdr.GetInt32(0);
                    if (f) ids_for_update = ids_for_update + ",";
                    ids_for_update = ids_for_update + last_id.ToString();
                    f = true;
                }
            }
            finally
            {
                rdr.Close();
                if (ids_for_update.Length > 0)
                {

                    sql.CommandText = " UPDATE cabinet.notify"
                                    + " SET message_sent = true, sent_date = current_timestamp"
                                    + " WHERE notify_id IN (" + ids_for_update.Trim() + ")"
                                    ;
                    sql.ExecuteNonQuery();
                }
            }
            */
        }

        private bool SendMessage(string to, string mess)
        {
            if ((s != null) && (s.IsConnected))
            {
                IcqSharp.Base.Contact contact = new IcqSharp.Base.Contact(to);
                IcqSharp.Base.Message message = new IcqSharp.Base.Message(contact, IcqSharp.Base.MessageType.Outgoing, mess);
                s.Messaging.Send(message);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Do_Connect();
        }
    }
}
