//
// CliIcbmParamRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliIcbmParamRequest : SnacPacket
    {
        public CliIcbmParamRequest()
            : base(0x0004, 0x0004)
        {
        }
    }
}
