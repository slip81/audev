//
// CliSsiRemoveYourself.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;

namespace IcqSharp.Packets
{
    class CliSsiRemoveYourself : SnacPacket
    {
        public CliSsiRemoveYourself (string uin)
            : base (0x0013, 0x0016)
        {
            AddByte ((byte)uin.Length);
            AddString (uin);
        }

        public CliSsiRemoveYourself (Contact c)
            : this (c.Uin)
        {
        }
    }
}
