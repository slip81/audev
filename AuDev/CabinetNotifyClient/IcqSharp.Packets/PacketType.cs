//
// SnacPacketType.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    [AttributeUsage (AttributeTargets.Class)]
    class SnacPacketType : Attribute
    {
        public ushort Channel;
        public ushort Family;
        public ushort Command;

        public SnacPacketType (ushort channel, ushort family, ushort command)
        {
            this.Channel = channel;
            this.Family = family;
            this.Command = command;
        }

        public SnacPacketType (ushort family, ushort command)
            : this (0x02, family, command)
        {
        }
    }
}
