//
// SrvFamiliesVersions.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Packets;
using IcqSharp.Connections;
using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0001, 0x0018)]
    class SrvFamiliesVersions : PacketHandler
    {
        public SrvFamiliesVersions (Session session, SnacPacket packet)
            : base (session, packet)
        {
            for (int i = 0; i < packet.Data.Length; i += 8) {
                ushort family = LittleEndianBitConverter.Big.ToUInt16 (packet.Data, i);
                ushort version = LittleEndianBitConverter.Big.ToUInt16(packet.Data, i + 2);
                
                Log.Debug ("Registered family: {0:X} (version {1})", family, version);
            }
        }

        public override List<FlapPacket> Response (FlapPacket packet)
        {
            List<FlapPacket> responses = new List<FlapPacket>();

            // Request server rate limits - CLI_RATES_REQUEST
            CliRatesRequest rates_req = new CliRatesRequest();
            responses.Add(rates_req);

            return responses;
        }
    }
}
