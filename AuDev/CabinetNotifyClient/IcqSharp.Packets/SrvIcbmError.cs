//
// SrvIcbmError.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0004, 0x0001)]
    class SrvIcbmError : PacketHandler
    {
        public SrvIcbmError(Session session, SnacPacket packet)
            : base (session, packet)
        {
            // FIXME: Implement this
            ushort error_code = LittleEndianBitConverter.Big.ToUInt16 (packet.Data, 0);
            //Log.Error ("IcbmError: {0:X}", error_code);
        }
    }
}
