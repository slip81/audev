//
// CliReady.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliReady : SnacPacket
    {
        public CliReady ()
            : base (0x0001, 0x0002)
        {
            // FIXME: Add families :-(
            AddWord (0x0001); AddWord (0x0003); AddDword (0x0110047B);
            AddWord (0x0013); AddWord (0x0002); AddDword (0x0110047B);
            AddWord (0x0002); AddWord (0x0001); AddDword (0x0110047B);
            AddWord (0x0003); AddWord (0x0001); AddDword (0x0110047B);
            AddWord (0x0015); AddWord (0x0001); AddDword (0x0110047B);
            AddWord (0x0004); AddWord (0x0001); AddDword (0x0110047B);
            AddWord (0x0006); AddWord (0x0001); AddDword (0x0110047B);
            AddWord (0x0009); AddWord (0x0001); AddDword (0x0110047B);
            AddWord (0x000A); AddWord (0x0001); AddDword (0x0110047B);
            AddWord (0x000B); AddWord (0x0001); AddDword (0x0110047B);
        }
    }
}