//
// CliBuddylistRightsRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliBuddylistRightsRequest : SnacPacket
    {
        public CliBuddylistRightsRequest()
            : base(0x0003, 0x0002)
        {
        }
    }
}