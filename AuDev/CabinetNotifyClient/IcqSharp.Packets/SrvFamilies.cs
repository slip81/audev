//
// SrvFamilies.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0001, 0x0003)]
    class SrvFamilies : PacketHandler
    {
        public SrvFamilies(Session session, SnacPacket packet)
            : base (session, packet)
        {
            for (int i = 0; i < packet.Data.Length; i += 4) {
                ushort family = LittleEndianBitConverter.Big.ToUInt16 (packet.Data, i);
                
                Log.Debug ("ServerConnection provides family: {0:X}", family);
            }
        }

        public override List<FlapPacket> Response (FlapPacket packet)
        {
            List<FlapPacket> responses = new List<FlapPacket>();

            CliFamiliesVersions versions = new CliFamiliesVersions ();
            responses.Add(versions);

            return responses;
        }
    }
}
