//
// CliSetStatus.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Util;

namespace IcqSharp.Packets
{
    class CliSetStatus : SnacPacket
    {
        public CliSetStatus (uint status)
            : base (0x0001, 0x001E)
        {
            AddTlv(new Tlv(0x06, status));
        }

        public CliSetStatus (Status status)
            : base (0x0001, 0x001E)
        {
           ushort value = 0xffff;

            switch (status)
            {
                case Status.Online:
                    value = 0x0000;
                    break;

                case Status.Away:
                    value = 0x0001;
                    break;

                case Status.NA:
                    value = 0x0005;
                    break;

                case Status.FreeForChat:
                    value = 0x0020;
                    break;

                case Status.Occupied:
                    value = 0x0011;
                    break;

                case Status.DoNotDisturb:
                    value = 0x0013;
                    break;

                case Status.Invisible:
                    value = 0x0100;
                    break;
            }

            AddTlv(new Tlv(0x06, (0x01000000) | value));
        }
    }
}
