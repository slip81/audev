using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliFamiliesVersions : SnacPacket
    {
        public CliFamiliesVersions ()
            : base (0x0001, 0x0017)
        {
            AddWord (0x0001); AddWord (0x0004);
            AddWord (0x0013); AddWord (0x0004);
            AddWord (0x0002); AddWord (0x0001);
            AddWord (0x0003); AddWord (0x0001);
            AddWord (0x0015); AddWord (0x0001);
            AddWord (0x0004); AddWord (0x0001);
            AddWord (0x0006); AddWord (0x0001);
            AddWord (0x0009); AddWord (0x0001);
            AddWord (0x000A); AddWord (0x0001);
            AddWord (0x000B); AddWord (0x0001);

            /*AddWord (0x0001); AddWord (0x0003);
            AddWord (0x0013); AddWord (0x0002);
            AddWord (0x0002); AddWord (0x0001);
            AddWord (0x0003); AddWord (0x0001);
            AddWord (0x0015); AddWord (0x0001);
            AddWord (0x0004); AddWord (0x0001);
            AddWord (0x0006); AddWord (0x0001);
            AddWord (0x0009); AddWord (0x0001);
            AddWord (0x000A); AddWord (0x0001);
            AddWord (0x000B); AddWord (0x0001);*/
            
            /*AddDword ((uint)0x10003);
            AddDword ((uint)0x20001);
            AddDword ((uint)0x30001);
            AddDword ((uint)0x150001);
            AddDword ((uint)0x40001);
            AddDword ((uint)0x60001);
            AddDword ((uint)0x90001);
            AddDword ((uint)0xa0001);*/
        }
    }
}