//
// ClisSsiActivate.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Packets;

namespace IcqSharp.Packets
{
    class CliSsiActivate : SnacPacket
    {
        public CliSsiActivate ()
            : base(0x0013, 0x0007)
        {
        }
    }
}
