//
// CliIcbmSend.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    class CliIcbmSend : SnacPacket
    {
        public CliIcbmSend (string uin, string message, bool ack, bool offline)
            : base(0x0004, 0x0006)
        {
            // FIXME: Parse this and make sense
            AddQword(0);
            AddWord(0x0001);

            byte[] uin_raw = Encoding.UTF8.GetBytes(uin);
            AddByte((byte)uin_raw.Length);
            Add(uin_raw);

            // CH1

            // !!!
            //byte[] text_raw = Encoding.UTF8.GetBytes(message);
            byte[] text_raw = Encoding.GetEncoding("windows-1251").GetBytes(message);

            // Message data TLV
            AddWord(0x0002);
            AddWord((ushort)(13 + text_raw.Length));
            
            AddWord(0x0501);
            AddWord(0x0001);
            AddByte(0x01);

            AddWord(0x0101);
            AddWord((ushort)(4 + text_raw.Length));
            AddDword(0x00000000);
            Add(text_raw);

            // Request an ack from the server
            if (ack)
                AddTlv(new Tlv(0x03));

            // Store message if receipent is offline
            if (offline)
                AddTlv(new Tlv(0x06));
        }

        public CliIcbmSend (string uin, string message)
            : this (uin, message, false, true)
        {
        }
    }
}
