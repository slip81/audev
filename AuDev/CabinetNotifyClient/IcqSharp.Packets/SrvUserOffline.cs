//
// SrvUserOffline.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0003, 0x000C)]
    class SrvUserOffline : PacketHandler
    {
        public SrvUserOffline(Session session, SnacPacket packet)
            : base (session, packet)
        {
            int length = (int)packet.Data[0];
            string uin = Encoding.UTF8.GetString (packet.Data, 1, length);

            Log.Debug ("{0} has gone offline!", uin);

            Contact c = session.ContactList [uin];

            if (c == null)
            {
                Log.Error("Received status notification for a user not in contact list!");
                return;
            }

            c.SetStatus (Status.Offline);

            c.OnSignedOff(c);
            session.ContactList.OnContactSignedOff(c);

            c.OnStatusChanged(c);
            session.ContactList.OnContactStatusChanged(c);
        }
    }
}
