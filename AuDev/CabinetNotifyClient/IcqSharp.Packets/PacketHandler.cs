//
// PacketHandler.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Connections;

namespace IcqSharp.Packets
{
    abstract class PacketHandler
    {
        private Session session;

        public PacketHandler (Session session, FlapPacket packet)
        {
            this.session = session;
        }

        public virtual List<FlapPacket> Response (FlapPacket packet)
        {
            return null;
        }

        protected Session Session
        {
            get { return session; }
        }
    }
}
