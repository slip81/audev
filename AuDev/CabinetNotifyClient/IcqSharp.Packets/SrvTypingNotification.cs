//
// SrvTypingNotification.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType(0x0004, 0x0014)]
    class SrvTypingNotification : PacketHandler
    {
        public SrvTypingNotification(Session session, SnacPacket packet)
            : base(session, packet)
        {
            //int cookie = LittleEndianBitConverter.Big.ToUInt32(packet.Data, 0);
            //ushort channel = LittleEndianBitConverter.Big.ToUInt16 (packet.Data, 8);
            int length = (int)packet.Data[10];
            string uin = Encoding.UTF8.GetString(packet.Data, 11, length);
            ushort type = LittleEndianBitConverter.Big.ToUInt16 (packet.Data, 11 + length);

            // FIXME: We only support contacts on our contactlist
            Contact contact = session.ContactList[uin];

            if (contact != null)
                session.Messaging.OnTypingNotification(contact, (TypingNotification)type);
        }
    }
}
