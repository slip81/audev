//
// CliPmsVisibleRemove.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;

namespace IcqSharp.Packets
{
    class CliPmsVisibleRemove : SnacPacket
    {
        public CliPmsVisibleRemove()
            : base(0x0009, 0x0006)
        {
        }

        public void Add(string uin)
        {
            AddByte((byte)uin.Length);
            AddString(uin);
        }

        public void Add(Contact contact)
        {
            Add(contact.Uin);
        }
    }
}
