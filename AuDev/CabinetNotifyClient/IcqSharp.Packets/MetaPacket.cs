//
// MetaPacket.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    public class MetaPacket : SnacPacket
    {
        private Session session;

        public MetaPacket(Session session)
            : base (0x0015, 0x0002)
        {
            this.session = session;
        }

        /*public override byte[] Write()
        {
            return base.Write();
        }*/

        protected Session Session
        {
            get { return session; }
        }
    }
}
