//
// CliPrivacyRightsRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliPrivacyRightsRequest : SnacPacket
    {
        public CliPrivacyRightsRequest ()
            : base (0x0009, 0x0002)
        {
        }
    }
}
