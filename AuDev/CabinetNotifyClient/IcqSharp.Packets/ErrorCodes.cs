//
// ErrorCodes.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections;
using System.Text;

namespace IcqSharp.Packets
{
    class ErrorCodes
    {
        private static Hashtable errors;

        static ErrorCodes()
        {
            errors = new Hashtable();

            errors[0x01] = "Invalid SNAC header";
            errors[0x02] = "ServerConnection rate limit exceeded";
            errors[0x03] = "Client rate limit exceeded";
            errors[0x04] = "Recipient is not logged in";
            errors[0x05] = "Requested service unavailable";
            errors[0x06] = "Requested service not defined";
            errors[0x07] = "You sent obsolete SNAC";
            errors[0x08] = "Not supported by server";
            errors[0x09] = "Not supported by client";
            errors[0x0A] = "Refused by client";
            errors[0x0B] = "Reply too big";
            errors[0x0C] = "Responses lost";
            errors[0x0D] = "Request denied";
            errors[0x0E] = "Incorrect SNAC format";
            errors[0x0F] = "Insufficient rights";
            errors[0x10] = "In local permit/deny (recipient blocked)";
            errors[0x11] = "Sender too evil";
            errors[0x12] = "Receiver too evil";
            errors[0x13] = "User temporarily unavailable";
            errors[0x14] = "No match";
            errors[0x15] = "List overflow";
            errors[0x16] = "Request ambiguous";
            errors[0x17] = "ServerConnection queue full";
            errors[0x18] = "Not while on AOL";
        }

        public static string GetError(ushort error_code)
        {
            if (errors.ContainsKey(error_code))
                return (string)errors[error_code];

            return "Unknown error!";
        }
    }
}
