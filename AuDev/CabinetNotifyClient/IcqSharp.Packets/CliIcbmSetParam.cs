//
// CliIcbmSetParam.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliIcbmSetParam : SnacPacket
    {
        public CliIcbmSetParam ()
            : base(0x0004, 0x0002)
        {
            AddWord (0x0001); // Channel to setup
            AddDword (0x0000000b); //Message flags
            AddWord (0x1f40); // Max message snac size
            AddWord (0x03e7); // max sender warning level
            AddWord (0x03e7); // max receiver warning level
            AddWord (0x00); // minimum message interval (sec)
            AddWord (0x00); // unknown parameter (also seen 03 E8)
        }
    }
}
