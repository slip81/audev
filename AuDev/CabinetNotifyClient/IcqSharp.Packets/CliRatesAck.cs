//
// CliRatesAck.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliRatesAck : SnacPacket
    {
        public CliRatesAck()
            : base(0x0001, 0x0008)
        {
            // FIXME: The response is hard-coded we should parse the data!
            AddWord(0x0001);
            AddWord(0x0002);
            AddWord(0x0003);
            AddWord(0x0004);
            AddWord(0x0005);

            /*AddDword ((uint)0x10002);
            AddDword ((uint)0x30004);
            AddWord (5);*/
        }
    }
}
