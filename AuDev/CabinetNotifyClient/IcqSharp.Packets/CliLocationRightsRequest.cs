//
// CliLocationRightsRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliLocationRightsRequest : SnacPacket
    {
        public CliLocationRightsRequest()
            : base(0x0002, 0x0002)
        {
        }
    }
}
