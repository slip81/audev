//
// SrvIcbmAck.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0004, 0x000C)]
    class SrvIcbmAck : PacketHandler
    {
        public SrvIcbmAck(Session session, SnacPacket packet)
            : base (session, packet)
        {
            ushort channel = LittleEndianBitConverter.Big.ToUInt16 (packet.Data, 8);
            int length = (int)packet.Data[10];
            string uin = Encoding.UTF8.GetString (packet.Data, 11, length);        
        }
    }
}
