//
// CliMetaRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    class CliMetaRequest : SnacPacket
    {
        public CliMetaRequest (MetaPacket packet)
            : base (0x0015, 0x0002)
        {
            // TLV 0x0001
            /*AddWord (0x0001);
            AddWord ((ushort)(8 + packet.Data.Length));

            AddWord ((ushort)(6 + pack.Data.Length)); // tlv length
            AddWord (UInt16.Parse (Session.Instance.Uin)); // UIN
            AddWord (packet.Type); // req type
            AddWord (packet.Sequence); // req seq*/
        }
    }
}

