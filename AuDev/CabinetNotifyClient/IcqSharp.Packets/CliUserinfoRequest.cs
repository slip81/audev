//
// CliUserinfoRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;

namespace IcqSharp.Packets
{
    class CliUserinfoRequest : SnacPacket
    {
        public CliUserinfoRequest (string uin, ushort type)
            : base(0x0002, 0x0005)
        {
            AddWord (type);
            AddByte ((byte)uin.Length);
            AddString (uin);
        }

        public CliUserinfoRequest(Contact c, ushort type)
            : this(c.Uin, type)
        {
        }
    }
}
