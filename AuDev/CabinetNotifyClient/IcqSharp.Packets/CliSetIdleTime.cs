//
// CliSetIdleTime.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    class CliSetIdleTime : SnacPacket
    {
        public CliSetIdleTime (uint seconds)
            : base(0x0001, 0x0011)
        {
            AddDword (seconds);
        }
    }
}
