//
// CliSsiAuthRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliSsiAuthRequest : SnacPacket
    {
        public CliSsiAuthRequest (string uin, string message)
            : base (0x0013, 0x0018)
        {
            AddByte ((byte)uin.Length);
            AddString (uin);

            AddByte ((byte)message.Length);
            AddString (message);

            AddWord (0x0000);
        }
    }
}
