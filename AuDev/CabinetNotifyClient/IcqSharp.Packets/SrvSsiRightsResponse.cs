//
// SrvSsiRightsResponse.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0013, 0x0003)]
    class SrvSsiRightsResponse : PacketHandler
    {
        public SrvSsiRightsResponse (Session session, SnacPacket packet)
            : base (session, packet)
        {
        }

        public override List<FlapPacket> Response (FlapPacket packet)
        {
            List<FlapPacket> responses = new List<FlapPacket> ();

            // FIXME: Implement CliSsiCheckoutRequest (0x0013, 0x0005)
            CliSsiRequest ssi = new CliSsiRequest ();
            responses.Add (ssi);

            return responses;
        }
    }
}
