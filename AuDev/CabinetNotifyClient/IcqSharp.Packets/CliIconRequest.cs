//
// CliIconRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;

namespace IcqSharp.Packets
{
    class CliIconRequest : SnacPacket
    {
        public CliIconRequest (string uin, byte[] hash)
            : base (0x0010, 0x0006)
        {
             //xx  	   	byte  	   	uin length
             //xx .. 	UTF8 	  	uin string
             //01 	  	byte 	  	unknown (command ?)
             //00 01 	word 	  	icon id (not sure)
             //01 	  	byte 	  	icon flags (bitmask, purpose unknown)
             //10 	  	byte 	  	md5 hash size (16)
             //xx .. 	array 	  	requested icon md5 hash

            AddByte((byte)uin.Length);
            AddString(uin);
            AddByte(0x01);
            AddByte((byte)hash.Length);
            Add(hash);
        }
    }
}
