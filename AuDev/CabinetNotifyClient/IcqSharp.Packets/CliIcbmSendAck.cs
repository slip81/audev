//
// CliIcbmSendAck.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    class CliIcbmSendAck : SnacPacket
    {
        public CliIcbmSendAck (string uin, string message)
            : base(0x0004, 0x000b)
        {
            byte[] text_raw = Encoding.UTF8.GetBytes(message);
            byte[] uin_raw = Encoding.UTF8.GetBytes(uin);

            AddQword(0); // msg-id cookie
            AddWord(0x0002); // msg channel
            
            AddByte((byte)uin_raw.Length);
            Add(uin_raw);

            AddWord(0x0003); // reason code

            // Channel specific

            AddWord(0x0501);
            AddWord(0x0001);
            AddByte(0x01);

            AddWord(0x0101);
            AddWord((ushort)(4 + text_raw.Length));
            AddWord(0x0000);
            AddWord(0xffff);
            Add(text_raw);
        }
    }
}
