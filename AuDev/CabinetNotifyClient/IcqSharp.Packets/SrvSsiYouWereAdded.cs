//
// SrvSsiYouWereAdded.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0013, 0x001C)]
    class SrvSsiYouWereAdded : PacketHandler
    {
        public SrvSsiYouWereAdded(Session session, SnacPacket packet)
            : base (session, packet)
        {
            int length = (int)packet.Data[0];
            string uin = Encoding.UTF8.GetString (packet.Data, 1, length);

            Log.Debug ("{0} has added you to their contactlist!", uin);

            Contact contact = new Contact (uin);
            session.ContactList.OnYouWereAdded (contact);
        }
    }
}
