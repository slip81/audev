//
// SrvUserinfoResponse.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0002, 0x0006)]
    class SrvUserinfoResponse : PacketHandler
    {
        public SrvUserinfoResponse(Session session, SnacPacket packet)
            : base (session, packet)
        {
            DebugFu.WriteBytes(packet.Data);

            int length = (int)packet.Data [0];
            string uin = Encoding.UTF8.GetString (packet.Data, 1, length);
            ushort warn_level = LittleEndianBitConverter.Big.ToUInt16 (packet.Data, length + 1);
            ushort tlv_count = LittleEndianBitConverter.Big.ToUInt16 (packet.Data, length + 3);

            TlvList tlvs = TlvList.Parse (packet.Data, length + 5, packet.Data.Length - (length + 5));

            foreach (Tlv tlv in tlvs)
            {
                Log.Debug("UserInfo: {0}", tlv.Type);

                switch (tlv.Type)
                {
                    case 0x0004:
                        string msg = Encoding.UTF8.GetString(tlv.Data, 0, tlv.Length);
                        Log.Debug("Away Message: {0}", msg);
                        break;
                }
            }
        }
    }
}
