//
// CliSsiRightsRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliSsiRightsRequest : SnacPacket
    {
        public CliSsiRightsRequest ()
            : base (0x0013, 0x0002)
        {
        }
    }
}
