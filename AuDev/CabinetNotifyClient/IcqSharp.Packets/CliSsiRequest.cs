//
// CliSsiRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliSsiRequest : SnacPacket
    {
        public CliSsiRequest ()
            : base (0x0013, 0x0004)
        {
        }
    }
}
