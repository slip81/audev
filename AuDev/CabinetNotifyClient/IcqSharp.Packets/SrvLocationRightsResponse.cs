//
// SrvLocationRightsResponse.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0002, 0x0003)]
    class SrvLocationRightsResponse : PacketHandler
    {
        public SrvLocationRightsResponse(Session session, SnacPacket packet)
            : base (session, packet)
        {
        }

        public override List<FlapPacket> Response (FlapPacket packet)
        {
            List<FlapPacket> responses = new List<FlapPacket> ();

            CliLocationSetInfo set_location_info = new CliLocationSetInfo (Session);
            responses.Add (set_location_info);

            return responses;
        }
    }
}
