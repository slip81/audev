//
// SrvIcbmMsgFailed.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0004, 0x000a)]
    class SrvIcbmMsgFailed : PacketHandler
    {
        public SrvIcbmMsgFailed(Session session, SnacPacket packet)
            : base (session, packet)
        {
            // FIXME: Implement this
            ushort channel = EndianBitConverter.Big.ToUInt16(packet.Data, 0);
            byte uin_length = packet.Data[2];
            string uin = Encoding.UTF8.GetString(packet.Data, 3, uin_length);

            Log.Warn("Message undelivered!");
        }
    }
}
