//
// CliTypingNotification.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;

namespace IcqSharp.Packets
{
    class CliTypingNotification : SnacPacket
    {
        public CliTypingNotification (string uin, TypingNotification type)
            : base (0x0004, 0x0014)
        {
            AddDword(0x00000000); // cookie
            AddWord(0x0001); // channel
            AddByte((byte)uin.Length); //uin length
            AddString(uin); // uin
            AddWord((ushort)type); // type
        }

        public CliTypingNotification(Contact c, TypingNotification type)
            : this(c.Uin, type)
        {
        }
    }
}