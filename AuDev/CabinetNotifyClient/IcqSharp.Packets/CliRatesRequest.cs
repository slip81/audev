//
// CliRatesRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Packets;

namespace IcqSharp.Packets
{
    class CliRatesRequest : SnacPacket
    {
        public CliRatesRequest()
            : base(0x0001, 0x0006)
        {
        }
    }
}
