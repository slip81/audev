//
// SrvIcbmParamResponse.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0004, 0x0005)]
    class SrvIcbmParamResponse : PacketHandler
    {
        public SrvIcbmParamResponse(Session session, SnacPacket packet)
            : base (session, packet)
        {
            // FIXME: Implement this
        }

        public override List<FlapPacket> Response (FlapPacket packet)
        {
            List<FlapPacket> responses = new List<FlapPacket> ();

            CliIcbmSetParam icbm_set_param = new CliIcbmSetParam ();
            responses.Add (icbm_set_param);

            return responses;
        }
    }
}
