//
// CliSelfInfoRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Packets
{
    class CliSelfInfoRequest : SnacPacket
    {
        public CliSelfInfoRequest ()
            : base (0x0001, 0x000e)
        {
        }
    }
}
