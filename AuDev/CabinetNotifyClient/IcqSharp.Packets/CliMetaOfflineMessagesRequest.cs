//
// CliMetaRequest.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    class CliMetaOfflineMessagesRequest : MetaPacket
    {
        public CliMetaOfflineMessagesRequest(Session session)
            : base (session)
        {
            AddDword((uint)0x1000a);
            AddWord(0x800);

            byte[] uin_raw = EndianBitConverter.Little.GetBytes(Convert.ToInt32(session.Uin));
            Add(uin_raw);

            AddWord(0x3c00);
            AddWord(0x200);
        }
    }
}

