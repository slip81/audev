//
// SrvPrivacyRightsResponse.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType (0x0009, 0x0003)]
    class SrvPrivacyRightsResponse : PacketHandler
    {
        public SrvPrivacyRightsResponse(Session session, SnacPacket packet)
            : base (session, packet)
        {
        }
    }
}
