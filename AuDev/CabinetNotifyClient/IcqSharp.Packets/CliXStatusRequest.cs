//
// CliIcbmSend.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Util;

namespace IcqSharp.Packets
{
    class CliXStatusRequest : SnacPacket
    {
        public CliXStatusRequest(Contact c)
            : this(c.Uin)
        {
        }

        public CliXStatusRequest(string uin)
            : base(0x0004, 0x0006)
        {
            // FIXME: Parse this and make sense
            AddQword(0);
            AddWord(0x0002);

            byte[] uin_raw = Encoding.UTF8.GetBytes(uin);
            AddByte((byte)uin_raw.Length);
            Add(uin_raw);

            // Message data TLV
            AddWord(0x0005);
            AddWord(0x01C8);
            //AddWord((ushort)(13 + text_raw.Length)); FIXME Length

            AddWord(0x0000);
            AddQword(0);
            Add(new byte[16] { 0x09, 0x46, 0x13, 0x49, 0x4C, 0x7F, 0x11, 0xD1, 0x82, 0x22, 0x44, 0x45, 0x53, 0x54, 0x00, 0x00 });

            // TLV unknown
            AddWord(0x000a);
            AddWord(0x0002);
            AddWord(0x0001);

            // TLV unknown
            AddWord(0x000f);
            AddWord(0x0000);

            // TLV Extended data
            AddWord(0x2711);
            AddWord(0x01A0);

            AddWord(0x1B00);
            AddWord(0x000A);
            Add(new byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
            AddWord(0x0000);
            AddDword(0x03000000);
            AddByte(0);
            AddWord(0x2F35);
            AddWord(0x0E00);
            AddWord(0x2F35);

            AddDword(0x00000000);
            AddDword(0x00000000);
            AddDword(0x00000000);
            AddByte(0x1A);
            AddByte(0);
            AddWord(0x0000);
            AddWord(0x0100);
            AddWord(0x0100);
            AddByte(0);

            byte[] bytes = new byte[] {
            0x4F, 0x00, 0x3B, 0x60, 0xB3, 0xEF, 0xD8, 0x2A, 0x6C, 0x45,
            0xA4, 0xE0, 0x9C, 0x5A, 0x5E, 0x67, 0xE8, 0x65, 0x08, 0x00, 0x2A, 0x00, 0x00, 0x00, 0x53, 0x63,
            0x72, 0x69, 0x70, 0x74, 0x20, 0x50, 0x6C, 0x75, 0x67, 0x2D, 0x69, 0x6E, 0x3A, 0x20, 0x52, 0x65,
            0x6D, 0x6F, 0x74, 0x65, 0x20, 0x4E, 0x6F, 0x74, 0x69, 0x66, 0x69, 0x63, 0x61, 0x74, 0x69, 0x6F,
            0x6E, 0x20, 0x41, 0x72, 0x72, 0x69, 0x76, 0x65, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x15, 0x01, 0x00, 0x00, 0x11, 0x01, 0x00, 0x00
            };

            Add(bytes);

            string xml = String.Format(
                "<N><QUERY>&lt;Q&gt;&lt;PluginID&gt;srvMng&lt;/PluginID&gt;&lt;/Q&gt;</QUERY>" +
                "<NOTIFY>&lt;srv&gt;&lt;id&gt;cAwaySrv&lt;/id&gt;&lt;req&gt;&lt;id&gt;AwayStat&lt;/id&gt;&lt;trans&gt;6&lt;/trans&gt;" +
                "&lt;senderId&gt;{0}&lt;/senderId&gt;&lt;/req&gt;&lt;/srv&gt;</NOTIFY></N>", "458285492");

            AddString(xml);

            AddWord(0x0D0A);

            // Request an ack from the server
            AddTlv(new Tlv(0x03));

        }
    }
}
