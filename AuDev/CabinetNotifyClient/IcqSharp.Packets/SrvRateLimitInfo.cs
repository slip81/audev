//
// SrvRateLimitInfo.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Packets;
using IcqSharp.Connections;
using IcqSharp.Util;

namespace IcqSharp.Packets
{
    [SnacPacketType(0x0001, 0x0007)]
    class SrvRateLimitInfo : PacketHandler
    {
        public SrvRateLimitInfo(Session session,SnacPacket packet)
            : base(session, packet)
        {
            ushort classes_count = LittleEndianBitConverter.Big.ToUInt16(packet.Data, 0);
            Log.Debug ("Received {0} rate classes", classes_count);

            // FIXME: Parse the rate classes
            ReadRateClass ();
        }

        public static void ReadRateClass()
        {
        }

        public override List<FlapPacket> Response (FlapPacket packet)
        {
            List<FlapPacket> responses = new List<FlapPacket> ();

            // CLI_RATES_ACK - accept rate limits
            CliRatesAck rates_ack = new CliRatesAck();
            responses.Add (rates_ack);

            // Services setup below
            CliSelfInfoRequest selfinfo = new CliSelfInfoRequest ();
            responses.Add (selfinfo);

            CliSsiRightsRequest ssi_rights = new CliSsiRightsRequest ();
            responses.Add (ssi_rights);

            CliLocationRightsRequest location_rights = new CliLocationRightsRequest ();
            responses.Add (location_rights);

            CliBuddylistRightsRequest buddylist_rights = new CliBuddylistRightsRequest ();
            responses.Add (buddylist_rights);

            CliIcbmParamRequest icbm_param = new CliIcbmParamRequest ();
            responses.Add (icbm_param);

            CliPrivacyRightsRequest privacy_rights = new CliPrivacyRightsRequest ();
            responses.Add (privacy_rights);
            
            return responses;
        }
    }
}
