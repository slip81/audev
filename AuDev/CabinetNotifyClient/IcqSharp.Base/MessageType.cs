//
// MessageType.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Base
{
    public enum MessageType
    {
        Incoming,
        Outgoing,
        Offline
    }
}
