//
// Status.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Base
{
    // NOTE: Please note that it isn't possible to set your own
    // status to offline, it is here only for the purpose to check
    // whetever any of your contacts is offline. To go offline
    // you have to disconnect from the server.

    public enum Status : ushort
    {
        Offline = 0,
        Online = 1,
        Away = 2,
        NA = 3,
        DoNotDisturb = 4,
        Occupied = 5,
	    FreeForChat = 6,
        Invisible = 7
    }
}
