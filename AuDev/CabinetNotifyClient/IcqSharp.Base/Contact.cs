//
// Contact.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Util;

namespace IcqSharp.Base
{
    public delegate void ContactEventDelegate (Contact contact);

    public class Contact
    {
        private string uin;
        private string nickname;

        private DateTime online_since;

        private Status status;
        private XStatus xstatus;

        private List<Clsid> capabilities;

        public event MessageEventDelegate MessageReceived;
        public event MessageEventDelegate MessageSent;        
        public event ContactEventDelegate StatusChanged;

        public event ContactEventDelegate SignedOn;
        public event ContactEventDelegate SignedOff;

        public Contact (string uin, Status status)
        {
            this.uin = uin;
            this.status = status;

            capabilities = new List<Clsid> ();
        }

        public Contact (string uin)
            : this (uin, Status.Offline)
        {
        }

        public void SetStatus (Status status)
        {
            SetStatus(status, xstatus);
        }

        public void SetStatus (Status status, XStatus xstatus)
        {
            this.status = status;
            this.xstatus = xstatus;
        }

        public void OnStatusChanged (Contact c)
        {
            if (StatusChanged != null)
                StatusChanged (this);
        }

        public void OnSignedOn(Contact c)
        {
            if (SignedOn != null)
                SignedOn(c);
        }

        public void OnSignedOff(Contact c)
        {
            if (SignedOff != null)
                SignedOff(c);
        }

        public void OnMessageReceived (Message m)
        {
            if (MessageReceived != null)
                MessageReceived (m);
        }

        public void OnMessageSent (Message m)
        {
            if (MessageSent != null)
                MessageSent (m);
        }

        public string Uin
        {
            get { return uin; }
        }

        public string Nickname
        {
            get { return nickname; }
            set { nickname = value; }
        }

        public DateTime OnlineSince
        {
            get { return online_since; }
            set { online_since = value; }
        }

        public Status Status
        {
            get { return status; }
        }

        public XStatus XStatus
        {
            get { return xstatus; }
        }

        public List<Clsid> Capabilities
        {
            get { return capabilities; }
        }
    }
}
