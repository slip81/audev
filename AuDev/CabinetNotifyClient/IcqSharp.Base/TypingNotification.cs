//
// TypingNotification.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Base
{
    public delegate void TypingNotificationDelegate(Contact c, TypingNotification n);

    public enum TypingNotification : ushort
    {
        Finished = 0x0000,
        Begun = 0x0002
    }
}
