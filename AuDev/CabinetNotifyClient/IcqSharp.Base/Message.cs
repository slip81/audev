//
// Message.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Base
{
    public delegate void MessageEventDelegate (Message message);

    public class Message
    {
        private Contact contact;
        private MessageType message_type;
        private DateTime timestamp;
        private string text;

        public Message(Contact contact, MessageType message_type, string text)
            : this(contact, message_type, DateTime.Now, text)
        {
        }

        public Message (Contact contact, MessageType message_type, DateTime timestamp, string text)
        {
            this.contact = contact;
            this.message_type = message_type;
            this.timestamp = timestamp;
            this.text = text;            
        }

        public Contact Contact
        {
            get { return contact; }
        }

        public MessageType MessageType
        {
            get { return message_type; }
        }

        public string Text
        {
            get { return text; }
        }

        public DateTime Timestamp
        {
            get { return timestamp; }
        }
    }
}
