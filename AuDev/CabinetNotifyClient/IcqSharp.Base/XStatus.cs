//
// XStatus.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Base
{
    public delegate void XStatusEventDelegate(Contact c, string title, string desc);

    public enum XStatus
    {
        None = 0,
        Angry,
        Duck,
        Tired,
        Party,
        Beer,
        Thinking,
        Eating,
        TV,
        Friends,
        Coffee,
        Music,
        Business,
        Camera,
        Funny,
        Phone,
        Games,
        College,
        Shopping,
        Sick,
        Sleeping,
        Surfing,
        Internet,
        Engineering,
        Typing,
        Barbecue,
        PPC,
        Mobile,
        Man,
        WC,
        Smoking,
        Sex,
        Quest,
        Geometry,
        Love
    }
}
