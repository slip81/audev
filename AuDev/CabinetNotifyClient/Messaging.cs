//
// Messaging.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Packets;

namespace IcqSharp
{
    public class Messaging
    {
        private Session session;

        public event MessageEventDelegate MessageReceived;
        public event MessageEventDelegate MessageSent;
        public event MessageEventDelegate OfflineMessageReceived;
        public event XStatusEventDelegate XStatusReceived;
        public event TypingNotificationDelegate TypingNotification;

        public Messaging (Session session)
        {
            this.session = session;
        }

        public void Send (Message message)
        {
            CliIcbmSend icbm_send = new CliIcbmSend (message.Contact.Uin, message.Text);            
            session.Send (icbm_send);

            OnMessageSent(message);
        }

        public void SendTypingNotification(Contact c, TypingNotification type)
        {
            CliTypingNotification notification = new CliTypingNotification (c, type);
            session.Send (notification);
        }

        public void RequestOfflineMessages()
        {
            CliMetaOfflineMessagesRequest offline = new CliMetaOfflineMessagesRequest (session);
            session.Send(offline);
        }

        public void RequestXStatus(Contact c)
        {
            CliXStatusRequest req = new CliXStatusRequest(c);
            session.Send(req);
        }

        public void OnMessageReceived (Message message)
        {
            if (MessageReceived != null)
                MessageReceived (message);
        }

        public void OnMessageSent (Message message)
        {
            if (MessageSent != null)
                MessageSent (message);
        }
        
        public void OnOfflineMessageReceived(Message message)
        {
            if (OfflineMessageReceived != null)
                OfflineMessageReceived(message);
        }

        public void OnXStatusReceived(Contact c, string title, string desc)
        {
            if (XStatusReceived != null)
                XStatusReceived(c, title, desc);
        }

        public void OnTypingNotification(Contact c, TypingNotification n)
        {
            if (TypingNotification != null)
                TypingNotification(c, n);
        }
    }
}
