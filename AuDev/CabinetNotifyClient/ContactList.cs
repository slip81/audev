//
// ContactList.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Packets;

namespace IcqSharp.Base
{
    public class ContactList
    {
        private Session session;

        private Dictionary<string, Contact> contacts;

        public event ContactEventDelegate ContactSignedOn;
        public event ContactEventDelegate ContactSignedOff;
        public event ContactEventDelegate ContactStatusChanged;

        public event ContactEventDelegate ContactAdded;
        public event ContactEventDelegate ContactRemoved;
        public event EventHandler ContactListReceived;

        public event ContactEventDelegate YouWereAdded;
        public event ContactEventDelegate YouWereRemoved;

        public ContactList (Session session)
        {
            this.session = session;
            this.contacts = new Dictionary<string, Contact> ();
        }

        public void Add (Contact c)
        {
            if (contacts.ContainsKey (c.Uin))
                return;

            contacts.Add (c.Uin, c);

            if (ContactAdded != null)
                ContactAdded (c);
        }

        public void Remove (Contact c)
        {
            if (!contacts.ContainsKey (c.Uin))
                return;

            contacts.Remove (c.Uin);

            if (ContactRemoved != null)
                ContactRemoved (c);
        }

        private List<Contact> GetContacts ()
        {
            List<Contact> list = new List<Contact> ();

            foreach (Contact c in contacts.Values)
                list.Add (c);

            return list;
        }

        private Contact GetContact (string uin)
        {
            if (contacts.ContainsKey (uin))
                return contacts[uin];

            return null;
        }

        public void RemoveYourself(Contact c)
        {
            RemoveYourself (c.Uin);
        }

        public void RemoveYourself(string uin)
        {
            CliSsiRemoveYourself p = new CliSsiRemoveYourself(uin);
            session.Send(p);
        }

        ///////////////////////////////////////////////////

        public void OnContactSignedOn(Contact c)
        {
            if (ContactSignedOn != null)
                ContactSignedOn(c);
        }

        public void OnContactSignedOff(Contact c)
        {
            if (ContactSignedOff != null)
                ContactSignedOff(c);
        }

        public void OnContactStatusChanged(Contact c)
        {
            if (ContactStatusChanged != null)
                ContactStatusChanged(c);
        }

        public void OnContactListReceived (object o, EventArgs args)
        {
            if (ContactListReceived != null)
                ContactListReceived(o, args);
        }

        public void OnYouWereAdded (Contact c)
        {
            if (YouWereAdded != null)
                YouWereAdded (c);
        }

        public void OnYouWereRemoved(Contact c)
        {
            if (YouWereRemoved != null)
                YouWereRemoved (c);
        }

        ///////////////////////////////////////////////////

        public Contact this[string uin]
        {
            get { return GetContact (uin); }
        }

        public List<Contact> Contacts
        {
            get { return GetContacts (); }
        }
    }
}
