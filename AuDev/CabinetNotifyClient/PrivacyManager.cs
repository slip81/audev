//
// PrivacyManager.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Packets;
using IcqSharp.Util;

namespace IcqSharp
{
    public class PrivacyManager
    {
        private Session session;

        public PrivacyManager(Session session)
        {
            this.session = session;
        }

        public void BecomeInvisible()
        {
            CliPmsVisibleRemove visible = new CliPmsVisibleRemove();
            CliPmsInvisibleAdd invisible = new CliPmsInvisibleAdd();

            foreach (Contact c in session.ContactList.Contacts)
            {
                visible.Add(c);
                invisible.Add(c);
            }

            session.Send(new CliSetStatus(Status.Invisible));

            session.Send(visible);
            session.Send(invisible);                       
        }

        public void BecomeVisible()
        {
            CliPmsVisibleAdd add = new CliPmsVisibleAdd();
            CliPmsInvisibleRemove remove = new CliPmsInvisibleRemove();

            foreach (Contact c in session.ContactList.Contacts)
            {
                add.Add(c);
                remove.Add(c);
            }

            session.Send(add);
            session.Send(remove);
        }
    }
}
