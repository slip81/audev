//
// Log.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Text;
using System.IO;

namespace IcqSharp.Util
{
    public class Log
    {
        public static bool Enable = true;

        private static FileStream file_stream = null;
        private static StreamWriter data_stream = null;

        public static event EventHandler WriteEvent;

        static Log ()
        {
            if (!Enable)
                return;

            try
            {
                file_stream = new FileStream("Debug.txt", FileMode.Create);

                data_stream = new StreamWriter(file_stream);
                data_stream.AutoFlush = true;
                
                data_stream.WriteLine("Current time: {0}", DateTime.Now);
            }
            catch (Exception e)
            {
            }
        }

        ~Log ()
        {
            if (file_stream != null)
                file_stream.Close ();
        }

        public static void Debug (string message, params object[] args)
        {
            Write ("Debug", message, args);
        }

        public static void Debug (Exception e)
        {
            Debug (e.ToString ());
        }

        public static void Error (string message, params object[] args)
        {
            Write ("ERROR", message, args);
        }

        public static void Error (Exception e)
        {
            Error (e.ToString ());
        }

        public static void Warn(string message, params object[] args)
        {
            Write("Warn", message, args);
        }

        public static void Warn(Exception e)
        {
            Error(e.ToString());
        }

        private static void Write (string type, string message, params object[] args)
        {
            if (!Enable || file_stream == null)
                return;

            string s = String.Format ("{0} - {1}: {2}", DateTime.Now.ToLongTimeString (), type, String.Format (message, args));

            Console.WriteLine (s);

            if (data_stream != null)
                data_stream.WriteLine (s);

            if (WriteEvent != null)
                WriteEvent(s, EventArgs.Empty);
        }
    }
}
