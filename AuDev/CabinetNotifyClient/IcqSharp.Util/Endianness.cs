//
// Endianness.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;

namespace IcqSharp.Util
{
	/// <summary>
	/// Endianness of a converter
	/// </summary>
	public enum Endianness
	{
		/// <summary>
		/// Little endian - least significant byte first
		/// </summary>
		LittleEndian,
		/// <summary>
		/// Big endian - most significant byte first
		/// </summary>
		BigEndian
	}
}
