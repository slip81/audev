using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Util
{
    public class Clsid
    {
        private Guid guid;

        public Clsid (string guid)
        {
            this.guid = new Guid (guid);
        }

        public Clsid (byte[] guid)
        {
            this.guid = new Guid (guid);
        }

        // FIXME: This doesnt work with big endian format
        public byte[] ToByteArray ()
        {
            return guid.ToByteArray ();
        }

        public override string ToString ()
        {
            return guid.ToString ();
        }

        public static List<Clsid> Parse (byte[] data, int index, int count)
        {
            List<Clsid> clsids = new List<Clsid> ();

            for (int i = 0; i < count; i += 16) {
                byte[] clsid = new byte [16];
                Array.Copy (data, index + i, clsid, 0, 16);

                clsids.Add (new Clsid (clsid));
            }

            return clsids;
        }

        public static List<Clsid> Parse (byte[] data)
        {
            return Parse (data, 0, data.Length);
        }
    }
}
