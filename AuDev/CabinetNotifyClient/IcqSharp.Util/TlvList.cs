//
// TlvList.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Util
{
    public class TlvList : List<Tlv>
    {
        public TlvList()
        {
        }

        private Tlv GetByType(ushort type)
        {
            foreach (Tlv tlv in this)
            {
                if (tlv.Type == type)
                    return tlv;
            }

            return null;
        }

        public static TlvList Parse (byte[] data)
        {
            return Parse (data, 0, data.Length);
        }

        public static TlvList Parse (byte[] data, int index, int count)
        {
            TlvList tlvs = new TlvList ();

            int bytes_read = 0;

            while (bytes_read < count) {
                Tlv tlv = Tlv.Parse (data, index + bytes_read);
                tlvs.Add (tlv);

                bytes_read += 4 + tlv.Length;
            }

            return tlvs;
        }

        public Tlv this[ushort type]
        {
            get { return GetByType (type); }
        }
    }
}
