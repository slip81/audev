//
// DebugFu.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Util
{
    public class DebugFu
    {
        public static void WriteBytes (byte[] data)
        {
            string s = String.Format ("+========== Bytes ({0}) ======================\n", data.Length);
            
            for (int i = 0; i <= (data.Length - 1) / 16; i++)
            {
                int count = (data.Length - i * 16) >= 16 ? 16 : (data.Length - i * 16);

                for (int j = 0; j < count; j++)
                    s += String.Format("{0:X2} ", (byte)data[i*16 + j]);

                s += " | ";

                for (int j = 0; j < count; j++)
                    s += String.Format("{0}", (char)data[i * 16 + j]);

                s += "\n";               
            }

            s += "+=============================================";

            Console.WriteLine(s);
        }
    }
}
