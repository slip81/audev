//
// Channels.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Connections
{
    enum Channels : byte
    {
        NewConnection = 0x01,
        SnacData = 0x02,
        FlapError = 0x03,
        CloseConnection = 0x04,
        KeepAlive = 0x05
    }
}
