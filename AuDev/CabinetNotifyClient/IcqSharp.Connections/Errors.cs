using System;
using System.Collections.Generic;
using System.Text;

namespace IcqSharp.Connections
{
    public class Errors
    {
        public static string GetError(ushort error)
        {
            switch (error)
            {
                case 0x0000:
                    return "No error";
                case 0x0001:
                    return "Multiple logins (on same UIN)!";
                case 0x0004:
                case 0x0005:
                    return "Bad password!";
                case 0x0007:
                case 0x0008:
                    return "Non-existant ICQ#!";
                case 0x0015:
                case 0x0016:
                    return "Too many clients from same IP!";
                case 0x0018:
                    return "Connection rate exceeded (turboing). This means you are connecting and disconnecting too quickly with the same login. You have been banned temporarily.";
                case 0x001B:
                    return "Old version of ICQ, need to upgrade!";
                case 0x001D:
                    return "You are reconnecting too fast!";
                case 0x001E:
                    return "Can't register on ICQ network, try again later!";
            }

            return "Unknown error";
        }
    }
}
