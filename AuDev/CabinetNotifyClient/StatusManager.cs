//
// StatusManager.cs
//
// Copyright (c) 2007 Lukas Lipka.
//

using System;
using System.Collections.Generic;
using System.Text;

using IcqSharp.Base;
using IcqSharp.Packets;
using IcqSharp.Util;

namespace IcqSharp
{
    public class StatusManager
    {
        private Session session;
        private Status status;
        private XStatus xstatus;

        public StatusManager(Session session, Status status)
            : this(session)
        {
            this.status = status;
        }

        public StatusManager(Session session)
        {
            this.session = session;
        }

        private void SetStatus(Status status)
        {
            if (status == Status.Offline)
            {
                throw new NotSupportedException("Cannot set own status to 'Offline'");
            }

            CliSetStatus set_status = new CliSetStatus(status);
            session.Send(set_status);

            // FIXME: The idletime is broken, we wont handle it
            // instead let the clients handle it
            /*uint idle_time = 0;

            if (status == Status.Away || status == Status.NA)
                idle_time = 60;

            SetIdleTime(idle_time);*/

            this.status = status;
        }

        public void SetXStatus(XStatus xstatus)
        {
            this.xstatus = xstatus;

            // Update our capabilities list to contain our
            // x-status icon CLSID.
            CliLocationSetInfo info = new CliLocationSetInfo(session);
            session.Send(info);

            // We also have to broadcast our status for the changes
            // to take affect
            SetStatus(status);
        }

        public void SetCustomStatus(string title, string message)
        {
            throw new NotImplementedException("Setting custom status not implemented!");
        }

        public void SetIdleTime(uint seconds)
        {
            CliSetIdleTime time = new CliSetIdleTime(seconds);
            session.Send(time);
        }

        public Status Status
        {
            get { return status; }
            set { SetStatus(value); }
        }

        public XStatus XStatus
        {
            get { return xstatus; }
            set { SetXStatus(value); }
        }
    }
}
