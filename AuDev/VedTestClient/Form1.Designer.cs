﻿namespace VedTestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radButton9 = new Telerik.WinControls.UI.RadButton();
            this.radButton8 = new Telerik.WinControls.UI.RadButton();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radSpinEditor2 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radSpinEditor1 = new Telerik.WinControls.UI.RadSpinEditor();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Controls.Add(this.splitPanel3);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(951, 523);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radTextBox4);
            this.splitPanel1.Controls.Add(this.radTextBox3);
            this.splitPanel1.Controls.Add(this.radTextBox2);
            this.splitPanel1.Controls.Add(this.radButton9);
            this.splitPanel1.Controls.Add(this.radButton8);
            this.splitPanel1.Controls.Add(this.radButton7);
            this.splitPanel1.Controls.Add(this.radButton6);
            this.splitPanel1.Controls.Add(this.radButton5);
            this.splitPanel1.Controls.Add(this.radCheckBox1);
            this.splitPanel1.Controls.Add(this.radSpinEditor2);
            this.splitPanel1.Controls.Add(this.radButton4);
            this.splitPanel1.Controls.Add(this.radButton3);
            this.splitPanel1.Controls.Add(this.radSpinEditor1);
            this.splitPanel1.Controls.Add(this.radButton2);
            this.splitPanel1.Controls.Add(this.radButton1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(951, 186);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.02783171F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 14);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radButton9
            // 
            this.radButton9.Location = new System.Drawing.Point(654, 150);
            this.radButton9.Name = "radButton9";
            this.radButton9.Size = new System.Drawing.Size(147, 33);
            this.radButton9.TabIndex = 11;
            this.radButton9.Text = "GetErrorCodeList";
            this.radButton9.Click += new System.EventHandler(this.radButton9_Click);
            // 
            // radButton8
            // 
            this.radButton8.Location = new System.Drawing.Point(443, 48);
            this.radButton8.Name = "radButton8";
            this.radButton8.Size = new System.Drawing.Size(148, 33);
            this.radButton8.TabIndex = 10;
            this.radButton8.Text = "Ved_DownloadList_All";
            this.radButton8.Click += new System.EventHandler(this.radButton8_Click);
            // 
            // radButton7
            // 
            this.radButton7.Location = new System.Drawing.Point(443, 150);
            this.radButton7.Name = "radButton7";
            this.radButton7.Size = new System.Drawing.Size(147, 33);
            this.radButton7.TabIndex = 9;
            this.radButton7.Text = "Ved_IsExistsNew";
            this.radButton7.Click += new System.EventHandler(this.radButton7_Click);
            // 
            // radButton6
            // 
            this.radButton6.Location = new System.Drawing.Point(171, 150);
            this.radButton6.Name = "radButton6";
            this.radButton6.Size = new System.Drawing.Size(147, 33);
            this.radButton6.TabIndex = 8;
            this.radButton6.Text = "Ved_CommitRequest_Gos";
            this.radButton6.Click += new System.EventHandler(this.radButton6_Click);
            // 
            // radButton5
            // 
            this.radButton5.Location = new System.Drawing.Point(17, 150);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(148, 33);
            this.radButton5.TabIndex = 7;
            this.radButton5.Text = "Ved_GetXmlList_Gos";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click);
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox1.Location = new System.Drawing.Point(116, 125);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(49, 18);
            this.radCheckBox1.TabIndex = 6;
            this.radCheckBox1.Text = "getAll";
            this.radCheckBox1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radSpinEditor2
            // 
            this.radSpinEditor2.Location = new System.Drawing.Point(332, 124);
            this.radSpinEditor2.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.radSpinEditor2.Name = "radSpinEditor2";
            this.radSpinEditor2.Size = new System.Drawing.Size(75, 20);
            this.radSpinEditor2.TabIndex = 5;
            this.radSpinEditor2.TabStop = false;
            this.radSpinEditor2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // radButton4
            // 
            this.radButton4.Location = new System.Drawing.Point(171, 87);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(155, 33);
            this.radButton4.TabIndex = 4;
            this.radButton4.Text = "Ved_CommitRequest_Region";
            this.radButton4.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // radButton3
            // 
            this.radButton3.Location = new System.Drawing.Point(17, 87);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(148, 33);
            this.radButton3.TabIndex = 3;
            this.radButton3.Text = "Ved_GetXmlList_Region";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radSpinEditor1
            // 
            this.radSpinEditor1.Location = new System.Drawing.Point(332, 61);
            this.radSpinEditor1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.radSpinEditor1.Name = "radSpinEditor1";
            this.radSpinEditor1.Size = new System.Drawing.Size(75, 20);
            this.radSpinEditor1.TabIndex = 2;
            this.radSpinEditor1.TabStop = false;
            this.radSpinEditor1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(171, 48);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(155, 33);
            this.radButton2.TabIndex = 1;
            this.radButton2.Text = "Ved_DownloadList_Region";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(17, 48);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(148, 33);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "Ved_DownloadList_Gos";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radTextBox1);
            this.splitPanel2.Location = new System.Drawing.Point(0, 190);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(951, 278);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.2064725F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 106);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radTextBox1
            // 
            this.radTextBox1.AutoSize = false;
            this.radTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox1.Location = new System.Drawing.Point(0, 0);
            this.radTextBox1.Multiline = true;
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(951, 278);
            this.radTextBox1.TabIndex = 0;
            // 
            // splitPanel3
            // 
            this.splitPanel3.Location = new System.Drawing.Point(0, 472);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(951, 51);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.2343042F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -120);
            this.splitPanel3.TabIndex = 2;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(17, 3);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.Size = new System.Drawing.Size(148, 20);
            this.radTextBox2.TabIndex = 12;
            this.radTextBox2.Text = "apteka_ul2";
            // 
            // radTextBox3
            // 
            this.radTextBox3.Location = new System.Drawing.Point(178, 3);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(229, 20);
            this.radTextBox3.TabIndex = 13;
            this.radTextBox3.Text = "A665-6367-D407-4543";
            // 
            // radTextBox4
            // 
            this.radTextBox4.Location = new System.Drawing.Point(443, 3);
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.Size = new System.Drawing.Size(148, 20);
            this.radTextBox4.TabIndex = 14;
            this.radTextBox4.Text = "3.2.1.14";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 523);
            this.Controls.Add(this.radSplitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            this.splitPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSpinEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor1;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadSpinEditor radSpinEditor2;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadButton radButton6;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadButton radButton7;
        private Telerik.WinControls.UI.RadButton radButton8;
        private Telerik.WinControls.UI.RadButton radButton9;
        private Telerik.WinControls.UI.RadTextBox radTextBox4;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
    }
}

