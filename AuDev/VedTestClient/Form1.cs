﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace VedTestClient
{
    public partial class Form1 : Form
    {

        DefectServ.UserInfo userInfo;

        public Form1()
        {
            InitializeComponent();
            //
            System.Net.ServicePointManager.Expect100Continue = false;
            //
            userInfo = new DefectServ.UserInfo() { login = radTextBox2.Text, workplace = radTextBox3.Text, version_num = radTextBox4.Text, };
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Ved_DownloadList_Gos("iguana");
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = res.Message;
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Ved_DownloadList_Region("iguana", (int)radSpinEditor1.Value);
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = res.Message;
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";

            Encoding enc = Encoding.GetEncoding("windows-1251");            
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Ved_GetXmlList_Region(userInfo, radCheckBox1.Checked);
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    if (res.VedXmlMain == null)
                    {
                        Cursor = Cursors.Default;
                        radTextBox1.Text = "res.VedXmlMain == null";
                        return;
                    }

                    File.WriteAllText(@"C:\Release\Ved\File\VedXmlMain_Region.xml", res.VedXmlMain.Data, enc);
                    radTextBox1.Text += "VedXmlMain_Region cnt:" + res.VedXmlMain.RecordCount.ToString();
                    radTextBox1.Text += System.Environment.NewLine;

                    if ((res.VedXmlOut != null) && (!String.IsNullOrEmpty(res.VedXmlOut.Data)))
                    {
                        File.WriteAllText(@"C:\Release\Ved\File\VedXmlOut_Region.xml", res.VedXmlOut.Data, enc);
                        radTextBox1.Text += "VedXmlOut_Region cnt:" + res.VedXmlOut.RecordCount.ToString();
                        radTextBox1.Text += System.Environment.NewLine;
                    }
                    else
                    {
                        radTextBox1.Text += "VedXmlOut_Region empty";
                        radTextBox1.Text += System.Environment.NewLine;
                    }

                    if ((res.VedXmlError != null) && (!String.IsNullOrEmpty(res.VedXmlError.Data)))
                    {
                        File.WriteAllText(@"C:\Release\Ved\File\VedXmlError_Region.xml", res.VedXmlError.Data, enc);
                        radTextBox1.Text += "VedXmlError_Region cnt:" + res.VedXmlError.RecordCount.ToString();
                        radTextBox1.Text += System.Environment.NewLine;
                    }
                    else
                    {
                        radTextBox1.Text += "VedXmlError_Region empty";
                        radTextBox1.Text += System.Environment.NewLine;
                    }

                    radTextBox1.Text += "RequestId:" + res.RequestId.ToString();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton4_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Ved_CommitRequest_Region(userInfo, (int)radSpinEditor2.Value);
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = "RequestId " + ((int)radSpinEditor2.Value).ToString() + " committed !";
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton5_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";

            Encoding enc = Encoding.GetEncoding("windows-1251");            
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Ved_GetXmlList_Gos(userInfo, radCheckBox1.Checked);
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    if (res.VedXmlMain == null)
                    {
                        Cursor = Cursors.Default;
                        radTextBox1.Text = "res.VedXmlMain == null";
                        return;
                    }

                    File.WriteAllText(@"C:\Release\Ved\File\VedXmlMain_Gos.xml", res.VedXmlMain.Data, enc);
                    radTextBox1.Text += "VedXmlMain_Gos cnt:" + res.VedXmlMain.RecordCount.ToString();
                    radTextBox1.Text += System.Environment.NewLine;

                    if ((res.VedXmlOut != null) && (!String.IsNullOrEmpty(res.VedXmlOut.Data)))
                    {
                        File.WriteAllText(@"C:\Release\Ved\File\VedXmlOut_Gos.xml", res.VedXmlOut.Data, enc);
                        radTextBox1.Text += "VedXmlOut_Gos cnt:" + res.VedXmlOut.RecordCount.ToString();
                        radTextBox1.Text += System.Environment.NewLine;
                    }
                    else
                    {
                        radTextBox1.Text += "VedXmlOut_Gos empty";
                        radTextBox1.Text += System.Environment.NewLine;
                    }

                    if ((res.VedXmlError != null) && (!String.IsNullOrEmpty(res.VedXmlError.Data)))
                    {
                        File.WriteAllText(@"C:\Release\Ved\File\VedXmlError_Gos.xml", res.VedXmlError.Data, enc);
                        radTextBox1.Text += "VedXmlError_Gos cnt:" + res.VedXmlError.RecordCount.ToString();
                        radTextBox1.Text += System.Environment.NewLine;
                    }
                    else
                    {
                        radTextBox1.Text += "VedXmlError_Gos empty";
                        radTextBox1.Text += System.Environment.NewLine;
                    }

                    radTextBox1.Text += "RequestId:" + res.RequestId.ToString();
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton6_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Ved_CommitRequest_Gos(userInfo, (int)radSpinEditor2.Value);
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = "RequestId " + ((int)radSpinEditor2.Value).ToString() + " committed !";
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton7_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Ved_IsExistsNew(userInfo);
                radTextBox1.Text = res.ToString();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton8_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Ved_DownloadList_All("iguana");
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = res.Message;
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton9_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            DefectServ.DefectServiceClient serv = new DefectServ.DefectServiceClient();
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetErrorCodeList();
                if (res == null)
                {
                    radTextBox1.Text = "res == null";
                }
                else
                {
                    foreach (var item in res)
                    {
                        radTextBox1.Text += item.Id.ToString() + ": " + item.Name;
                        radTextBox1.Text += System.Environment.NewLine;
                    }                    
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
    }
}
