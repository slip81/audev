﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using AuDev.Common.Db.Model;

namespace AuStockApi.Log
{   
    public class LogSession : AuStockApi.Models.AuStockBaseClass
    {
        public LogSession()
        {
            //log_session
        }

        public long log_id { get; set; }
        public string session_id { get; set; }
        public Nullable<long> business_id { get; set; }
        public string user_name { get; set; }
        public System.DateTime date_beg { get; set; }
        public string comment { get; set; }
        public int state { get; set; }
        public Nullable<System.DateTime> date_end { get; set; }
        public Nullable<short> is_admin { get; set; }
        public Nullable<long> user_role { get; set; }
        public Nullable<long> org_id { get; set; }
        //public string business_name { get; set; }
    }    
    
}
