﻿using System;
using System.Text;
using System.Net;
using UnidecodeSharpFork;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace AuStockApi.Log
{
    public static class Loggly
    {
        private static bool isBanLog = false;

        public static bool InsertLog_Stock(string mess, long? type_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess;
                log_esn.log_esn_type_id = type_id;
                log_esn.user_name = user_name;
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.STOCK;
                log_esn.date_end = DateTime.Now;
                //log_esn.mess2 = !String.IsNullOrWhiteSpace(mess2) ? "подробно..." : null;
                log_esn.mess2 = mess2;
                dbContext.log_esn.Add(log_esn);

                if (!String.IsNullOrWhiteSpace(mess2))
                {
                    log_esn_data log_esn_data = new log_esn_data();
                    log_esn_data.log_esn = log_esn;
                    log_esn_data.data_text = mess2;
                    dbContext.log_esn_data.Add(log_esn_data);
                }

                dbContext.SaveChanges();
            }

            return true;
        }

        public static bool InsertLog_Asna(string mess, long? type_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess;
                log_esn.log_esn_type_id = type_id;
                log_esn.user_name = user_name;
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.ASNA;
                log_esn.date_end = DateTime.Now;
                log_esn.mess2 = !String.IsNullOrWhiteSpace(mess2) ? "подробно..." : null;
                dbContext.log_esn.Add(log_esn);

                if (!String.IsNullOrWhiteSpace(mess2))
                {
                    log_esn_data log_esn_data = new log_esn_data();
                    log_esn_data.log_esn = log_esn;
                    log_esn_data.data_text = mess2;
                    dbContext.log_esn_data.Add(log_esn_data);
                }

                dbContext.SaveChanges();
            }

            return true;
        }

        public static bool InsertLog_Wa(string mess, long? type_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess;
                log_esn.log_esn_type_id = type_id;
                log_esn.user_name = user_name;
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.WA;
                log_esn.date_end = DateTime.Now;
                log_esn.mess2 = !String.IsNullOrWhiteSpace(mess2) ? "подробно..." : null;
                dbContext.log_esn.Add(log_esn);

                if (!String.IsNullOrWhiteSpace(mess2))
                {
                    log_esn_data log_esn_data = new log_esn_data();
                    log_esn_data.log_esn = log_esn;
                    log_esn_data.data_text = mess2;
                    dbContext.log_esn_data.Add(log_esn_data);
                }

                dbContext.SaveChanges();
            }

            return true;
        }
    }
}
