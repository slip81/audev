﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;

namespace AuStockApi.Service
{
    public partial class TaskService
    {
        #region GetTasks

        public GetTasksResult GetTasks(GetTasksServiceParam getTasksServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getTasks(getTasksServiceParam);
            }
            catch (Exception ex)
            {
                string user = getTasksServiceParam != null && !String.IsNullOrWhiteSpace(getTasksServiceParam.login) ? getTasksServiceParam.login : "esn";
                Loggly.InsertLog_Asna("Ошибка в GetTasks: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetTasksResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetTasksResult getTasks(GetTasksServiceParam getTasksServiceParam)
        {
            GetTasksResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = getTasksServiceParam != null && !String.IsNullOrWhiteSpace(getTasksServiceParam.login) ? getTasksServiceParam.login : "esn";
            int service_id = getTasksServiceParam != null ? getTasksServiceParam.service_id : 0;
            
            var checkLicenseResult = mainService.CheckLicense(getTasksServiceParam, service_id);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new GetTasksResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            // !!!
            // пока заглушка
            return new GetTasksResult()
            {
                tasks = new List<RequestTask>(),
            };
        }
        
        #endregion
    }
}
