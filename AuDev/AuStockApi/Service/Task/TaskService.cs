﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuStockApi.Models;

namespace AuStockApi.Service
{
    public partial class TaskService : AuStockBaseService, ITaskService
    {
        private IMainService mainService
        {
            get
            {
                if (_mainService == null)
                {
                    _mainService = GetInjectedService<IMainService>();
                }
                return _mainService;
            }
        }
        private IMainService _mainService;
        public TaskService()
            : base()
        {
            //
        }
    }
}