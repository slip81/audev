﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;
using System.Net.Http;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;

namespace AuStockApi.Service
{
    public class AuStockBaseService : IDisposable
    {
        protected AuMainDb dbContext;

        protected static Ninject.IKernel ninjectKernel;

        [Ninject.Inject]
        public AutoMapper.IMapper ModelMapper { get; set; }

        protected Dictionary<int, Tuple<string, string>> MethodDescrDictionary { get { return methodDescrDictionary; } }
        private static Dictionary<int, Tuple<string, string>> methodDescrDictionary { get; set; }

        public AuStockBaseService()
        {
            dbContext = new AuMainDb("AuMainDb");
            //dbContext = new AuMainDb("AuTestDb");
        }

        static AuStockBaseService()
        {
            var resolver = GlobalConfiguration.Configuration.DependencyResolver;
            ninjectKernel = (Ninject.IKernel)resolver.GetService(typeof(Ninject.IKernel));            
            //
            methodDescrDictionary = new Dictionary<int, Tuple<string, string>>();
            //
            methodDescrDictionary.Add(1, new Tuple<string, string>("GetMethodsDescr", "Получение описания всех доступных методов API"));
            methodDescrDictionary.Add(2, new Tuple<string, string>("CheckLicense", "Проверка лицензии"));
            methodDescrDictionary.Add(3, new Tuple<string, string>("SendDoc", "Отправка позиций накладных из БД АУ на веб-сервис для обработки"));
            methodDescrDictionary.Add(4, new Tuple<string, string>("GetDocState", "Просмотр статуса отправленных накладных"));
            methodDescrDictionary.Add(5, new Tuple<string, string>("GetDocStates", "Справочник статусов отправленных накладных"));
            methodDescrDictionary.Add(6, new Tuple<string, string>("GetDocLog", "Просмотр истории отправки / загрузки накладных"));
            methodDescrDictionary.Add(7, new Tuple<string, string>("GetDoc", "Загрузка с веб-сервиса обработанных позиций накладных"));
            methodDescrDictionary.Add(8, new Tuple<string, string>("SendChanges", "Отправка на веб-сервис изменений по состыкованным товарам"));
            methodDescrDictionary.Add(9, new Tuple<string, string>("GetTasks", "Получение списка заданий на выполнение различных операций (из списка доступных методов)"));
            methodDescrDictionary.Add(10, new Tuple<string, string>("SendTaskReceived", "Отправка уведомления веб-сервису о том, что задание получено"));
            methodDescrDictionary.Add(11, new Tuple<string, string>("SendTaskCompleted", "Отправка уведомления веб-сервису о том, что задание успешно выполнено"));
            methodDescrDictionary.Add(12, new Tuple<string, string>("SendTaskFailed", "Отправка уведомления веб-сервису о том, что задание выполнено с ошибкой"));
            methodDescrDictionary.Add(13, new Tuple<string, string>("SendStock", "Отправка наличия из БД АУ на веб-сервис для формирования сводного наличия"));
            methodDescrDictionary.Add(14, new Tuple<string, string>("GetStock", "Получение с веб-сервиса сводного наличия"));
            methodDescrDictionary.Add(15, new Tuple<string, string>("ResetSendStock", "Сброс активной цепочки пакетов отправки наличия на веб-сервис"));
            methodDescrDictionary.Add(16, new Tuple<string, string>("ResetGetStock", "Сброс активной цепочки пакетов загрузки наличия с веб-сервиса"));
            methodDescrDictionary.Add(17, new Tuple<string, string>("GetRequestTypes", "Получение типов запросов для выполнения заданий"));
            methodDescrDictionary.Add(18, new Tuple<string, string>("SendTaskCanceled", "Отправка уведомления веб-сервису о том, что задание отменено"));
            methodDescrDictionary.Add(19, new Tuple<string, string>("asna/SendRemains", "АСНА: отправка остатков на веб-сервис для дальнейшей передачи в АСНА"));
            methodDescrDictionary.Add(20, new Tuple<string, string>("asna/ResetSendRemains", "АСНА: сброс активной цепочки пакетов отправки остатков на веб-сервис"));
        }

        public void Dispose()
        {
            if (dbContext != null)
            {                
                dbContext.Dispose();
            }
        }

        public void Noop()
        {
            //
        }

        public void Noop2()
        {
            version_db version_db = dbContext.version_db.FirstOrDefault();
        }

        protected T GetInjectedService<T>()
            where T : IInjectableService
        {
            return (T)ninjectKernel.GetService(typeof(T));
        }

        // Fastest Way of Inserting in Entity Framework
        // http://stackoverflow.com/questions/5940225/fastest-way-of-inserting-in-entity-framework

        protected AuMainDb AddToContext<TEntity>(AuMainDb context,
            TEntity entity, int count, int commitCount, bool recreateContext)
            where TEntity : class
        {
            context.Set<TEntity>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }
    }
}