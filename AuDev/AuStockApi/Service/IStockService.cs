﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuStockApi.Models;
#endregion

namespace AuStockApi.Service
{
    public interface IStockService : IInjectableService
    {
        AuStockBaseResult SendStock(SendStockServiceParam sendStockServiceParam);
        GetStockResult GetStock(GetStockServiceParam getStockServiceParam);
        AuStockBaseResult ResetSendStock(UserInfo resetSendStockServiceParam);
        AuStockBaseResult ResetGetStock(UserInfo resetGetStockServiceParam);
        //
        AuStockBaseResult DelStock(DelStockServiceParam delStockServiceParam);
        //
        AuStockBaseResult ConfirmSendStock(ConfirmStockServiceParam сonfirmStockServiceParam);
        AuStockBaseResult ConfirmGetStock(ConfirmStockServiceParam сonfirmStockServiceParam);
        GetLastBatchStockResult GetLastBatchSendStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam);
        GetLastBatchStockResult GetLastBatchGetStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam);
        //
        AuStockBaseResult SendStockReport(StockReportServiceParam stockReportServiceParam);
        AuStockBaseResult GetStockReport(StockReportServiceParam stockReportServiceParam);
    }
}
