﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuStockApi.Models;
using AuStockApi.Log;
using AutoMapper;

namespace AuStockApi.Service
{

    public class AutoMapperCommon: IMapper
    {
        static AutoMapperCommon() 
        {    
            //
        }

        public IConfigurationProvider ConfigurationProvider { get { return Mapper.Configuration; } }

        public Func<Type, object> ServiceCtor { get { return Mapper.Configuration.ServiceCtor; } }

        
        public object Map(object source, Type sourceType, Type destinationType)
        {                        
            return Mapper.Map(source, sourceType, destinationType); 
        }

        public TDestination Map<TDestination>(object source)
        {
            return Mapper.Map<TDestination>(source);
        }

        public TDestination Map<TSource, TDestination>(TSource source)
        {
            return Mapper.Map<TSource, TDestination>(source);
        }

        public TDestination Map<TDestination>(object source, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map<TDestination>(source, opts);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination)
        {
            return Mapper.Map<TSource, TDestination>(source, destination);
        }

        public TDestination Map<TSource, TDestination>(TSource source, TDestination destination, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            return Mapper.Map<TSource, TDestination>(source, destination, opts);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType)
        {
            return Mapper.Map(source, destination, sourceType, destinationType);
        }

        public object Map(object source, Type sourceType, Type destinationType, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map(source, sourceType, destinationType, opts);
        }

        public object Map(object source, object destination, Type sourceType, Type destinationType, Action<IMappingOperationOptions> opts)
        {
            return Mapper.Map(source, destination, sourceType, destinationType, opts);
        }

        public TDestination Map<TSource, TDestination>(TSource source, Action<IMappingOperationOptions<TSource, TDestination>> opts)
        {
            return Mapper.Map<TSource, TDestination>(source, opts);
        }
    }
}