﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;

namespace AuStockApi.Service
{
    public partial class MainService
    {

        #region CheckLicense

        public CheckLicenseResult CheckLicense(CheckLicenseServiceParam checkLicenseServiceParam)
        {
            try
            {
                return сheckLicense(checkLicenseServiceParam);
            }
            catch (Exception ex)
            {
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        public CheckLicenseResult CheckLicense(UserInfo userInfo, int serviceId = AuStockConst.license_id_UND)
        {
            try
            {
                CheckLicenseServiceParam checkLicenseServiceParam = new CheckLicenseServiceParam()
                {
                    login = userInfo.login,
                    workplace = userInfo.workplace,
                    version_num = userInfo.version_num,
                    service_id = serviceId,
                };
                return сheckLicense(checkLicenseServiceParam);
            }
            catch (Exception ex)
            {
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private CheckLicenseResult сheckLicense(CheckLicenseServiceParam checkLicenseServiceParam)
        {
            DateTime now = DateTime.Now;
            DateTime now_initial = DateTime.Now;
            CheckLicenseResult errRes = null;
            string user = checkLicenseServiceParam != null && !String.IsNullOrWhiteSpace(checkLicenseServiceParam.login) ? checkLicenseServiceParam.login : "stock";

            // !!!
            /*
            Loggly.InsertLog_Stock("Не найдена лицензия (test)", (long)Enums.LogEsnType.ERROR, "stock", null, now, null);
            return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ESN_SERVICE_UNAVAILABLE, "Не найдена лицензия (test)") };
            */


            if (checkLicenseServiceParam == null)
            {
                errRes = new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Не заданы входные параметры") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;                
            }

            string login = checkLicenseServiceParam.login;
            string workplace = checkLicenseServiceParam.workplace;
            string version_num = checkLicenseServiceParam.version_num;
            int serviceId = checkLicenseServiceParam.service_id.HasValue ? (int)checkLicenseServiceParam.service_id : AuStockConst.license_id_UND;

            if (String.IsNullOrEmpty(login))
            {
                errRes = new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_LOGIN_NOT_FOUND, "Не задан логин") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (String.IsNullOrEmpty(workplace))
            {
                errRes = new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_WORKPLACE_NOT_FOUND, "Не задан идентификатор рабочего места") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (String.IsNullOrEmpty(version_num))
            {
                errRes = new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Не задан номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            workplace user_workplace = (from t1 in dbContext.vw_client_user
                        from t2 in dbContext.workplace
                        where t1.is_active == 1
                        && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && t2.sales_id == t1.sales_id
                        && t2.is_deleted != 1
                        && t2.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        select t2)
                       .FirstOrDefault();

            if (user_workplace == null)
            {
                errRes = new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Не найден пользователь с логином " + login.Trim() + " и рабочим местом " + workplace.Trim()) };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            string version_name_start = new string(version_num.Take(4).ToArray());
            var license = (from t1 in dbContext.vw_client_service
                           where t1.workplace_id == user_workplace.id
                           && t1.service_id == serviceId
                           && t1.version_name != null && t1.version_name.StartsWith(version_name_start)
                           select t1)
                          .FirstOrDefault();            
            if (license == null)
            {
                errRes = new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Не найдена лицензия: версия " + version_num.Trim() + ", логин: " + login.Trim() + ", рабочее место: " + workplace.Trim()) };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (license.sales_id <= 0)
            {
                errRes = new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "В лицензии sales_id <= 0: версия " + version_num.Trim() + ", логин: " + login.Trim() + ", рабочее место: " + workplace.Trim() + ", sales_id: " + license.sales_id.ToString()) };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            //Loggly.InsertLog_Stock("Есть лицензия на услугу СН: версия " + version_num.Trim() + ", логин: " + login.Trim() + ", рабочее место: " + workplace.Trim(), (long)Enums.LogEsnType.STOCK_UPLOAD, user, null, now_initial, null);

            return new CheckLicenseResult() { result = 1, client_id = license.client_id, sales_id = license.sales_id, workplace_id = license.workplace_id, };
        }
        
        #endregion
    }
}