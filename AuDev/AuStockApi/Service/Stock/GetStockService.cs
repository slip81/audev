﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;
#endregion

namespace AuStockApi.Service
{
    public partial class StockService
    {        
        #region GetStock

        private const int batch_row_cnt_DEFAULT = 100;
        //private const int batch_row_cnt_DEFAULT = 20;

        // не используется
        // private int getStockChainCount_MAX = 5;

        public GetStockResult GetStock(GetStockServiceParam getStockServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getStock(getStockServiceParam);
            }
            catch (Exception ex)
            {
                string user = getStockServiceParam != null && !String.IsNullOrWhiteSpace(getStockServiceParam.login) ? getStockServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в GetStock: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetStockResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };                
            }
        }

        private GetStockResult getStock(GetStockServiceParam getStockServiceParam)
        {            
            GetStockResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = getStockServiceParam != null && !String.IsNullOrWhiteSpace(getStockServiceParam.login) ? getStockServiceParam.login : "stock";
            
            var checkLicenseResult = mainService.CheckLicense(getStockServiceParam, AuStockConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new GetStockResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
            }

            var checkStockVersionResult = CheckStockVersion(getStockServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new GetStockResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            bool first_call = getStockServiceParam.part_num == 1;
            int part_num = getStockServiceParam.part_num;
            int part_cnt = 0;
            int skip_cnt = 0;
            DateTime download_batch_date_beg = new DateTime(2017, 1, 1, 0, 0, 0);
            DateTime download_batch_date_end = DateTime.Now;
            int batch_row_cnt = batch_row_cnt_DEFAULT;
            List<vw_stock_row_tmp> newRows = null;
            List<Stock> rows = null;
            int newRowsCnt = 0;
            bool force_all = getStockServiceParam.force_all.GetValueOrDefault(0) == 1;
            string checksum = null;
            int? checksum_cnt = 0;
            ChecksumInfo checksum_info = null;

            stock stock = dbContext.stock.Where(ss => ss.sales_id == sales_id).FirstOrDefault();
            if (stock == null)
            {
                errRes = new GetStockResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Не найдена запись в таблице stock для sales_id=" + sales_id.ToString()),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            // от ПАП:
            // Не возвращать данные по наличию торговым точкам, если есть хотя бы одно невыполненное задание для удаления сводного наличия по организации
            var activeTaskForDel = dbContext.wa_task
                .Where(ss => ss.request_type_id == (int)Enums.WaRequestTypeEnum.STOCK_DelStock
                && ss.client_id == client_id
                && !ss.is_deleted
                && ((ss.task_state_id == (int)Enums.WaTaskStateEnum.ISSUED_SERVER) || (ss.task_state_id == (int)Enums.WaTaskStateEnum.HANDLING_SERVER) || (ss.task_state_id == (int)Enums.WaTaskStateEnum.HANDLING_CLIENT))
                )
                .FirstOrDefault();
            if (activeTaskForDel != null)
            {
                errRes = new GetStockResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_ACTIVE_TASK_FOR_DEL_EXISTS, "Есть активное задание с кодом " + activeTaskForDel.task_id.ToString() + " на удаление сводного наличия"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                return errRes;
                //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
            }

            if (part_num <= 0)
            {
                errRes = new GetStockResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_UNCORRECT_BATCH_NUM, "Неверный номер пакета (" + part_num.ToString() + ")"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                return errRes;
                //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
            }

            stock_sales_download stock_sales_download = null;
            
            if (!first_call)
            {
                stock_sales_download stock_sales_download_existing = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && ss.is_active && !ss.is_deleted && ss.is_confirmed && !ss.is_fake && ss.is_new_version).OrderByDescending(ss => ss.part_num).FirstOrDefault();
                if (stock_sales_download_existing == null)
                {
                    errRes = new GetStockResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_ACTIVE_CHAIN_NOT_FOUND, "Не найдена активная цепочка получения строк сводного наличия"),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                    return errRes;
                    //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
                }
                if ((part_num > stock_sales_download_existing.part_cnt) || (part_num != stock_sales_download_existing.part_num + 1))
                {
                    errRes = new GetStockResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_WRONG_BATCH_NUM, "Неверный номер пакета. Предыдущий полученный пакет: " + stock_sales_download_existing.part_num.GetValueOrDefault(0).ToString() + " из " + stock_sales_download_existing.part_cnt.GetValueOrDefault(0).ToString()
                            + ", запрошенный пакет: " + part_num.ToString()),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                    return errRes;
                    //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
                }

                if (stock_sales_download_existing.force_all != force_all)
                {
                    errRes = new GetStockResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Значение параметра force_all отличается от предыдущего, текущее значение: " + force_all.ToString() + ", предыдущее: " + stock_sales_download_existing.force_all.ToString()),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                    return errRes;
                    //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };                    
                }

                batch_row_cnt = stock_sales_download_existing.batch_row_cnt;
                part_cnt = stock_sales_download_existing.part_cnt.GetValueOrDefault(0);
                download_batch_date_beg = (DateTime)stock_sales_download_existing.download_batch_date_beg;
                download_batch_date_end = (DateTime)stock_sales_download_existing.download_batch_date_end;
                skip_cnt = batch_row_cnt * stock_sales_download_existing.part_num.GetValueOrDefault(0);
                checksum = stock_sales_download_existing.checksum;
                checksum_cnt = stock_sales_download_existing.checksum_cnt;
                checksum_info = new ChecksumInfo() { checksum = checksum, cnt = checksum_cnt, };
            }
            else
            {
                // !!!      
                /*
                var stockChainCount = dbContext.Database.SqlQuery<int>("select * from und.get_stock_chain_count(0, 1);").FirstOrDefault();
                if (stockChainCount >= getStockChainCount_MAX)
                {
                    errRes = new GetStockResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_SERVICE_UNAVAILABLE, "Превышено максимальное число активных цепочек получения (" + getStockChainCount_MAX.ToString() + ")"),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                    return errRes;
                }
                */

                stock_sales_download stock_sales_download_last_confirmed = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && !ss.is_deleted && !ss.is_fake && ss.is_new_version).OrderByDescending(ss => ss.download_id).FirstOrDefault();
                if ((stock_sales_download_last_confirmed != null) && (!stock_sales_download_last_confirmed.is_confirmed))
                {
                    errRes = new GetStockResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_PREVIOUS_CHAIN_NOT_CONFIRMED, "Предыдущая цепочка получения не подтверждена"),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                    return errRes;
                }

                List<stock_sales_download> stock_sales_download_list = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && ss.is_active && !ss.is_deleted && !ss.is_fake && ss.is_new_version).ToList();
                if ((stock_sales_download_list != null) && (stock_sales_download_list.Count > 0))
                {
                    DateTime inactive_date = DateTime.Now;
                    foreach (var stock_sales_download_list_item in stock_sales_download_list)
                    {
                        stock_sales_download_list_item.is_confirmed = true;
                        stock_sales_download_list_item.confirm_date = inactive_date;
                        stock_sales_download_list_item.is_active = false;
                        stock_sales_download_list_item.inactive_date = inactive_date;
                        stock_sales_download_list_item.upd_date = inactive_date;
                        stock_sales_download_list_item.upd_user = user;
                    }
                    dbContext.SaveChanges();
                }

                stock_sales_download stock_sales_download_last = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && !ss.is_deleted && ss.is_confirmed && !ss.is_fake && ss.is_new_version).OrderByDescending(ss => ss.download_id).FirstOrDefault();
                if ((stock_sales_download_last != null) && (!force_all))
                {
                    download_batch_date_beg = (DateTime)stock_sales_download_last.download_batch_date_end;                    
                }
                else
                {
                    download_batch_date_beg = new DateTime(2017, 1, 1, 0, 0, 0);                    
                }

                download_batch_date_end = DateTime.Now;

                service_stock service_stock = dbContext.service_stock.Where(ss => ss.workplace_id == workplace_id).FirstOrDefault();
                batch_row_cnt = service_stock != null && service_stock.batch_row_cnt > 0 ? service_stock.batch_row_cnt : batch_row_cnt_DEFAULT;

                // !!!                
                checksum_info = dbContext.Database.SqlQuery<ChecksumInfo>(
                    //"START TRANSACTION ISOLATION LEVEL SERIALIZABLE;" +
                    "select * from und.prepare_vw_stock_row_tmp(" 
                    + sales_id.ToString() 
                    + "," + client_id.ToString()
                    + ",'" + download_batch_date_beg.ToString("yyyy-MM-dd HH:mm:ss") + "'"
                    + ",'" + download_batch_date_end.ToString("yyyy-MM-dd HH:mm:ss") + "'"
                    + "," + (force_all ? "1" : "0")
                    + ")")
                    .FirstOrDefault();
                

                /*
                string readCommitedTranName = "read committed";
                string serializableTranName = "serializable";
                bool isSqlError = false;
                bool needReturn = false;
                bool needCommit = false;
                string sqlExceptionText = null;
                try
                {
                    //var startTranResult = dbContext.Database.SqlQuery<object>("START TRANSACTION ISOLATION LEVEL SERIALIZABLE;").FirstOrDefault();
                    dbContext.Database.ExecuteSqlCommand("START TRANSACTION ISOLATION LEVEL SERIALIZABLE;");
                    needCommit = true;
                    var currentTranResult = dbContext.Database.SqlQuery<string>("SELECT current_setting('transaction_isolation');").FirstOrDefault();
                    if ((String.IsNullOrWhiteSpace(currentTranResult)) || !(currentTranResult.Trim().ToLower().Equals(serializableTranName.Trim().ToLower())))
                    {
                        isSqlError = true;
                        sqlExceptionText = "Неверный уровень изоляции транзакции на входе: " + currentTranResult + " вместо " + serializableTranName;
                    }
                    else
                    {                        
                        checksum_info = dbContext.Database.SqlQuery<ChecksumInfo>("select * from und.prepare_vw_stock_row_tmp("
                            + sales_id.ToString()
                            + "," + client_id.ToString()
                            + ",'" + download_batch_date_beg.ToString("yyyy-MM-dd HH:mm:ss") + "'"
                            + ",'" + download_batch_date_end.ToString("yyyy-MM-dd HH:mm:ss") + "'"
                            + "," + (force_all ? "1" : "0")
                            + ")")
                            .FirstOrDefault();
                    }
                }
                catch (Exception e)
                {
                    isSqlError = true;
                    sqlExceptionText = GlobalUtil.ExceptionInfo(e);
                }
                finally
                {
                    if (needCommit)
                    {
                        var commitTranResult = dbContext.Database.SqlQuery<object>("COMMIT;").FirstOrDefault();
                        //dbContext.Database.ExecuteSqlCommand("COMMIT;");
                        needCommit = false;
                        var finallyTranResult = dbContext.Database.SqlQuery<string>("SELECT current_setting('transaction_isolation');").FirstOrDefault();
                        if ((String.IsNullOrWhiteSpace(finallyTranResult)) || !(finallyTranResult.Trim().ToLower().Equals(readCommitedTranName.Trim().ToLower())))
                        {
                            isSqlError = true;
                            sqlExceptionText = "Неверный уровень изоляции транзакции на выходе: " + finallyTranResult + " вместо " + readCommitedTranName;
                        }
                    }
                    if (isSqlError)
                    {
                        needReturn = true;                        
                    }
                }

                if (needReturn)
                {
                    errRes = new GetStockResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, sqlExceptionText),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                    return errRes;
                }
                */

                /*
                    START TRANSACTION ISOLATION LEVEL SERIALIZABLE;
                    SELECT current_setting('transaction_isolation');
                    SELECT * FROM und.prepare_vw_stock_row_tmp(1, 1, current_timestamp::timestamp without time zone, current_timestamp::timestamp without time zone, 0);
                    COMMIT;
                    SELECT current_setting('transaction_isolation');
                */

                checksum = checksum_info == null ? null : checksum_info.checksum;
                checksum_cnt = checksum_info == null ? null : checksum_info.cnt;

                newRows = dbContext.vw_stock_row_tmp.Where(ss => ss.receiver_id == sales_id).ToList();    
                newRowsCnt = newRows != null ? newRows.Count : 0;
                part_cnt = (int)(Math.Ceiling((decimal)newRowsCnt / batch_row_cnt));

                /*
                checksum_info = getStockChecksum2_forClient(client_id, download_batch_date_beg_for_checksum, download_batch_date_end_for_checksum, user);
                checksum = checksum_info == null ? null : checksum_info.checksum;
                checksum_cnt = checksum_info == null ? null : checksum_info.cnt;
                */
            }

            // !!!
            int doUpdate = first_call ? 1 : 0;
            string getStockDelListQuery = "select * from und.get_stock_del_list(" + sales_id.ToString() + ", '" + download_batch_date_beg.ToString("yyyy-MM-dd HH:mm:ss") + "', " + doUpdate.ToString() + ");";
            // !!!
            if (first_call)
            {
                var getStockDelListResult = dbContext.Database.SqlQuery<int>(getStockDelListQuery).ToList();
                if ((getStockDelListResult != null) && (getStockDelListResult.Count > 0))
                {
                    Loggly.InsertLog_Stock("Обнаружена очистка сводного наличия [отделений: " + getStockDelListResult.Count().ToString() + "]", (long)Enums.LogEsnType.STOCK_DOWNLOAD, user, null, DateTime.Now, stock.pharmacy_name);

                    stock_sales_download = new stock_sales_download();
                    stock_sales_download.sales_id = sales_id;
                    stock_sales_download.part_cnt = part_cnt;
                    stock_sales_download.part_num = part_num;
                    stock_sales_download.row_cnt = 0;
                    stock_sales_download.skip_row_cnt = 0;
                    stock_sales_download.new_row_cnt = 0;
                    stock_sales_download.force_all = force_all;
                    stock_sales_download.is_active = true;
                    stock_sales_download.inactive_date = null;
                    stock_sales_download.download_batch_date_beg = download_batch_date_beg;
                    stock_sales_download.download_batch_date_end = download_batch_date_end;
                    stock_sales_download.batch_row_cnt = 0;
                    stock_sales_download.is_confirmed = false;
                    stock_sales_download.checksum = checksum;
                    stock_sales_download.checksum_cnt = checksum_cnt;
                    stock_sales_download.is_fake = true;
                    stock_sales_download.is_new_version = true;

                    stock_sales_download.crt_date = DateTime.Now;
                    stock_sales_download.crt_user = user;
                    stock_sales_download.upd_date = stock_sales_download.crt_date;
                    stock_sales_download.upd_user = user;
                    stock_sales_download.is_deleted = false;
                    dbContext.stock_sales_download.Add(stock_sales_download);
                    dbContext.SaveChanges();

                    return new GetStockResult() { result = 2, error = null, checksum = null, checksum_info = null, part_cnt = 0, orgs = getStockDelListResult, rows = null, };
                }
                else
                {
                    // !!!
                    /*
                    stock_chain stock_chain = new stock_chain();
                    stock_chain.stock_id = stock.stock_id;
                    stock_chain.is_upload = false;
                    stock_chain.is_active = true;
                    stock_chain.crt_date = DateTime.Now;
                    stock_chain.crt_user = user;
                    stock_chain.upd_date = stock_chain.crt_date;
                    stock_chain.upd_user = user;
                    stock_chain.is_deleted = false;
                    dbContext.stock_chain.Add(stock_chain);
                    */
                }
            }

            if ((newRows != null) && (newRowsCnt > 0))
            {
                rows = newRows
                    .OrderBy(ss => ss.row_id)
                    .Skip(skip_cnt)
                    .Take(batch_row_cnt)                    
                    .Select(ss => new Stock()
                    {
                        artikul = ss.artikul,
                        stock_id = ss.row_stock_id,
                        state = (byte)ss.state,
                        is_mine = ss.sales_id == sales_id ? (byte)1 : (byte)0,
                        esn_id = ss.esn_id,
                        prep_id = ss.prep_id,
                        prep_name = ss.prep_name,
                        firm_id = ss.firm_id,
                        firm_name = ss.firm_name,
                        country_name = ss.country_name,
                        unpacked_cnt = ss.unpacked_cnt,
                        all_cnt = ss.all_cnt,
                        price = ss.price,
                        valid_date = ss.valid_date.HasValue ? ((DateTime)ss.valid_date).ToString("dd.MM.yyyy") : null,
                        series = ss.series,
                        is_vital = ss.is_vital ? (byte)1 : (byte)0,
                        barcode = ss.barcode,
                        price_firm = ss.price_firm,
                        price_gr = ss.price_gr,
                        percent_gross = ss.percent_gross,
                        price_gross = ss.price_gross,
                        sum_gross = ss.sum_gross,
                        percent_nds_gross = ss.percent_nds_gross,
                        price_nds_gross = ss.price_nds_gross,
                        sum_nds_gross = ss.sum_nds_gross,
                        percent = ss.percent,
                        sum = ss.sum,
                        supplier_name = ss.supplier_name,
                        supplier_doc_num = ss.supplier_doc_num,
                        gr_date = ss.gr_date.HasValue ? ((DateTime)ss.gr_date).ToString("dd.MM.yyyy") : null,
                        farm_group_name = ss.farm_group_name,
                        supplier_doc_date = ss.supplier_doc_date.HasValue ? ((DateTime)ss.supplier_doc_date).ToString("dd.MM.yyyy") : null,
                        profit = ss.profit,
                        mess = ss.mess,
                        depart_id = ss.depart_id,
                        depart_name = ss.depart_name,
                        pharmacy_name = ss.pharmacy_name,
                        depart_address = ss.depart_address,
                        org_id = ss.stock_id,
                    })
                    .ToList();
            }
            else
            {
                /*
                                rows = dbContext.vw_stock_row.Where(ss => ss.client_id == client_id
                                    && ss.batch_confirm_date > download_batch_date_beg
                                    && ss.batch_confirm_date <= download_batch_date_end
                                    && (((ss.state != (int)Enums.UndStockRowStateEnum.REMOVED) && (force_all)) || (!force_all))
                                    && ss.is_active == false
                                    && ss.batch_is_confirmed
                                    )
                */
                rows = dbContext.vw_stock_row_tmp.Where(ss => ss.receiver_id == sales_id)
                    .OrderBy(ss => ss.row_id)
                    .Skip(skip_cnt)
                    .Take(batch_row_cnt)
                    .AsEnumerable()
                    .Select(ss => new Stock()
                    {
                        artikul = ss.artikul,
                        stock_id = ss.row_stock_id,
                        state = (byte)ss.state,
                        is_mine = ss.sales_id == sales_id ? (byte)1 : (byte)0,
                        esn_id = ss.esn_id,
                        prep_id = ss.prep_id,
                        prep_name = ss.prep_name,
                        firm_id = ss.firm_id,
                        firm_name = ss.firm_name,
                        country_name = ss.country_name,
                        unpacked_cnt = ss.unpacked_cnt,
                        all_cnt = ss.all_cnt,
                        price = ss.price,
                        valid_date = ss.valid_date.HasValue ? ((DateTime)ss.valid_date).ToString("dd.MM.yyyy") : null,
                        series = ss.series,
                        is_vital = ss.is_vital ? (byte)1 : (byte)0,
                        barcode = ss.barcode,
                        price_firm = ss.price_firm,
                        price_gr = ss.price_gr,
                        percent_gross = ss.percent_gross,
                        price_gross = ss.price_gross,
                        sum_gross = ss.sum_gross,
                        percent_nds_gross = ss.percent_nds_gross,
                        price_nds_gross = ss.price_nds_gross,
                        sum_nds_gross = ss.sum_nds_gross,
                        percent = ss.percent,
                        sum = ss.sum,
                        supplier_name = ss.supplier_name,
                        supplier_doc_num = ss.supplier_doc_num,
                        gr_date = ss.gr_date.HasValue ? ((DateTime)ss.gr_date).ToString("dd.MM.yyyy") : null,
                        farm_group_name = ss.farm_group_name,
                        supplier_doc_date = ss.supplier_doc_date.HasValue ? ((DateTime)ss.supplier_doc_date).ToString("dd.MM.yyyy") : null,
                        profit = ss.profit,
                        mess = ss.mess,
                        depart_id = ss.depart_id,
                        depart_name = ss.depart_name,
                        pharmacy_name = ss.pharmacy_name,
                        depart_address = ss.depart_address,
                        org_id = ss.stock_id,
                    })
                    .ToList();
            }

            int cntNew = rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.ADDED).Count();
            int cntRemoved = rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.REMOVED).Count();
            int cntChanged = rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.CHANGED).Count();

            stock_sales_download = new stock_sales_download();
            stock_sales_download.sales_id = sales_id;            
            stock_sales_download.part_cnt = part_cnt;
            stock_sales_download.part_num = part_num;
            stock_sales_download.row_cnt = rows.Count;
            stock_sales_download.skip_row_cnt = skip_cnt;
            stock_sales_download.new_row_cnt = newRowsCnt;
            stock_sales_download.force_all = force_all;
            stock_sales_download.is_active = true;
            stock_sales_download.inactive_date = null;
            stock_sales_download.download_batch_date_beg = download_batch_date_beg;
            stock_sales_download.download_batch_date_end = download_batch_date_end;
            stock_sales_download.batch_row_cnt = batch_row_cnt;
            stock_sales_download.is_confirmed = false;
            stock_sales_download.checksum = checksum;
            stock_sales_download.checksum_cnt = checksum_cnt;
            stock_sales_download.is_new_version = true;

            stock_sales_download.crt_date = DateTime.Now;
            stock_sales_download.crt_user = user;
            stock_sales_download.upd_date = stock_sales_download.crt_date;
            stock_sales_download.upd_user = user;
            stock_sales_download.is_deleted = false;
            dbContext.stock_sales_download.Add(stock_sales_download);
            dbContext.SaveChanges();

            Loggly.InsertLog_Stock("Получено строк: " + stock_sales_download.row_cnt.ToString() 
                + " [новых " + cntNew.ToString() + ", измененных " + cntChanged.ToString() + ", удаленных " + cntRemoved.ToString() 
                + "] [пакет " + part_num.ToString() + "/" + part_cnt.ToString() + "]" 
                + " [KC: " + (checksum_info == null ? "null" : checksum_info.checksum) + ", строк: " + (checksum_info == null ? "0" : checksum_info.cnt.GetValueOrDefault(0).ToString()) + "]"
                , (long)Enums.LogEsnType.STOCK_DOWNLOAD, user, stock_sales_download.download_id, DateTime.Now, stock.pharmacy_name);

            return new GetStockResult() { result = 1, part_cnt = part_cnt, checksum = checksum, checksum_info = checksum_info, rows = new List<Stock>(rows), };
        }

        #endregion

    }
}