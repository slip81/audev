﻿using System;

namespace AuStockApi.Service
{
    public partial class StockService : AuStockBaseService, IStockService
    {
        private IMainService mainService
        {
            get
            {
                if (_mainService == null)
                {
                    _mainService = GetInjectedService<IMainService>();
                }
                return _mainService;
            }
        }
        private IMainService _mainService;

        public StockService()
            :base()
        {
            //
        }
    }
}