﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;

namespace AuStockApi.Service
{
    public partial class StockService
    {
        #region GetLastBatchSendStock

        public GetLastBatchStockResult GetLastBatchSendStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getLastBatchSendStock(getLastBatchStockServiceParam);
            }
            catch (Exception ex)
            {
                string user = getLastBatchStockServiceParam != null && !String.IsNullOrWhiteSpace(getLastBatchStockServiceParam.login) ? getLastBatchStockServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в GetLastBatchSendStock: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetLastBatchStockResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };                
            }
        }

        private GetLastBatchStockResult getLastBatchSendStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam)
        {
            GetLastBatchStockResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = getLastBatchStockServiceParam != null && !String.IsNullOrWhiteSpace(getLastBatchStockServiceParam.login) ? getLastBatchStockServiceParam.login : "stock";

            var checkLicenseResult = mainService.CheckLicense(getLastBatchStockServiceParam, AuStockConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new GetLastBatchStockResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;                
            }

            var checkStockVersionResult = CheckStockVersion(getLastBatchStockServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new GetLastBatchStockResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            int depart_id = getLastBatchStockServiceParam.depart_id.GetValueOrDefault(0);            
  
            stock stock = dbContext.stock.Where(ss => ss.sales_id == sales_id && ss.depart_id == depart_id).FirstOrDefault();
            if (stock == null)
            {
                errRes = new GetLastBatchStockResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Не найдены данные по работе со сводным наличием для данной торговой точки и подразделения (stock = null)"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            stock_batch stock_batch_active = dbContext.stock_batch.Where(ss => ss.stock_id == stock.stock_id && ss.is_active && !ss.is_deleted)
                .OrderByDescending(ss => ss.part_num).FirstOrDefault();
            if (stock_batch_active == null)
            {
                errRes = new GetLastBatchStockResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_ACTIVE_CHAIN_NOT_FOUND, "Не найдена текущая активая цепочка отправки пакетов"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                return errRes;
            }

            stock_batch stock_batch_last_confirmed = dbContext.stock_batch.Where(ss => ss.stock_id == stock.stock_id && ss.is_active && !ss.is_deleted && ss.is_confirmed)
                .OrderByDescending(ss => ss.part_num).FirstOrDefault();

            int part_cnt = stock_batch_active.part_cnt.GetValueOrDefault(0);
            int part_num = stock_batch_last_confirmed == null ? 0 : stock_batch_last_confirmed.part_num.GetValueOrDefault(0);


            Loggly.InsertLog_Stock("Запрошен последний подтвержденный отправленный пакет [" + part_num.ToString() + " из " + part_cnt.ToString() + "]", (long)Enums.LogEsnType.STOCK_UPLOAD, user, null, DateTime.Now, stock.pharmacy_name);
            
            return new GetLastBatchStockResult() { result = 1, part_cnt = part_cnt, part_num = part_num, };
        }
        
        #endregion
    }
}