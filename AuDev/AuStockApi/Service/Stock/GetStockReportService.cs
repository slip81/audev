﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;

namespace AuStockApi.Service
{
    public partial class StockService
    {
        #region GetStockReport

        public AuStockBaseResult GetStockReport(StockReportServiceParam stockReportServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                bool is_error = ((stockReportServiceParam != null) && (stockReportServiceParam.is_error.GetValueOrDefault(0) == 1));
                return toStockReport(stockReportServiceParam, is_error ? (long)Enums.LogEsnType.ERROR : (long)Enums.LogEsnType.STOCK_DOWNLOAD);
            }
            catch (Exception ex)
            {
                string user = stockReportServiceParam != null && !String.IsNullOrWhiteSpace(stockReportServiceParam.login) ? stockReportServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в GetStockReport: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuStockBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };                
            }
        }
   
        #endregion
    }
}