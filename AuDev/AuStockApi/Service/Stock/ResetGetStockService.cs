﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;

namespace AuStockApi.Service
{
    public partial class StockService
    {
        #region ResetGetStock

        public AuStockBaseResult ResetGetStock(UserInfo resetGetStockServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return resetGetStock(resetGetStockServiceParam);
            }
            catch (Exception ex)
            {
                string user = resetGetStockServiceParam != null && !String.IsNullOrWhiteSpace(resetGetStockServiceParam.login) ? resetGetStockServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в ResetSendStock: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuStockBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuStockBaseResult resetGetStock(UserInfo resetGetStockServiceParam)
        {
            AuStockBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = resetGetStockServiceParam != null && !String.IsNullOrWhiteSpace(resetGetStockServiceParam.login) ? resetGetStockServiceParam.login : "stock";
            
            var checkLicenseResult = mainService.CheckLicense(resetGetStockServiceParam, AuStockConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuStockBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            var checkStockVersionResult = CheckStockVersion(resetGetStockServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new AuStockBaseResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            stock stock = dbContext.stock.Where(ss => ss.sales_id == sales_id).FirstOrDefault();

            List<stock_sales_download> stock_sales_download_list = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && ss.is_active && !ss.is_deleted && !ss.is_fake).ToList();
            if ((stock_sales_download_list == null) || (stock_sales_download_list.Count <= 0))
            {
                errRes = new AuStockBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_ACTIVE_CHAIN_NOT_FOUND, "Не найдена активная цепочка пакетов загрузки наличия"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, stock.pharmacy_name);
                return errRes;
            }

            // !!!
            var clearBatchResult = dbContext.Database.SqlQuery<int>("select * from und.clear_vw_stock_row_tmp("
                + sales_id.ToString()
                + ")")
                .FirstOrDefault();

            foreach (var stock_sales_download in stock_sales_download_list)
            {
                stock_sales_download.is_deleted = true;
                stock_sales_download.del_date = DateTime.Now;
                stock_sales_download.del_user = user;
            }
            dbContext.SaveChanges();

            Loggly.InsertLog_Stock("Сброшена активная цепочка пакетов загрузки наличия [" + stock_sales_download_list.Count.ToString() + "]", (long)Enums.LogEsnType.STOCK_DOWNLOAD, user, null, DateTime.Now, stock.pharmacy_name);
            return new AuStockBaseResult() { result = 1 };
        }        
        #endregion
    }
}