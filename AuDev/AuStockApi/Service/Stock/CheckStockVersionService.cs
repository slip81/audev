﻿using System;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;

namespace AuStockApi.Service
{
    public partial class StockService
    {
        #region CheckStockVersion

        private const string VERSION_NUM_CURRENT = "1.2.0.0";

        public AuStockBaseResult CheckStockVersion(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return checkStockVersion(userInfo);
            }
            catch (Exception ex)
            {
                string user = userInfo != null && !String.IsNullOrWhiteSpace(userInfo.login) ? userInfo.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в CheckStockVersion: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuStockBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };                
            }
        }

        private AuStockBaseResult checkStockVersion(UserInfo userInfo)
        {
            return new AuStockBaseResult() { result = 1 };

            /*
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = userInfo != null && !String.IsNullOrWhiteSpace(userInfo.login) ? userInfo.login : "stock";
            string userInfo_version_num = ((userInfo == null) || (String.IsNullOrWhiteSpace(userInfo.version_num))) ? "не задано" : userInfo.version_num;

            if ((String.IsNullOrWhiteSpace(userInfo_version_num)) || (!userInfo_version_num.Trim().ToLower().Equals(VERSION_NUM_CURRENT.Trim().ToLower())))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Номер версии не соответствует текущей [" + userInfo_version_num + "<>" + VERSION_NUM_CURRENT + "]"),
                };
                //Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            
            return new AuEsnBaseResult() { result = 1 };
            */
        }
        
        #endregion
    }
}