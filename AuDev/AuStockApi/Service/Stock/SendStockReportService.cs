﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuStockApi.Models;
using AuStockApi.Log;

namespace AuStockApi.Service
{
    public partial class StockService
    {
        #region SendStockReport

        public AuStockBaseResult SendStockReport(StockReportServiceParam stockReportServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                bool is_error = ((stockReportServiceParam != null) && (stockReportServiceParam.is_error.GetValueOrDefault(0) == 1));
                return toStockReport(stockReportServiceParam, is_error ? (long)Enums.LogEsnType.ERROR : (long)Enums.LogEsnType.STOCK_UPLOAD);
            }
            catch (Exception ex)
            {
                string user = stockReportServiceParam != null && !String.IsNullOrWhiteSpace(stockReportServiceParam.login) ? stockReportServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в SendStockReport: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuStockBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };                
            }
        }

        private AuStockBaseResult toStockReport(StockReportServiceParam stockReportServiceParam, long type_id)
        {
            AuStockBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = stockReportServiceParam != null && !String.IsNullOrWhiteSpace(stockReportServiceParam.login) ? stockReportServiceParam.login : "stock";

            var checkLicenseResult = mainService.CheckLicense(stockReportServiceParam, AuStockConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuStockBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;                
            }

            var checkStockVersionResult = CheckStockVersion(stockReportServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new AuStockBaseResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            stock stock = dbContext.stock.Where(ss => ss.sales_id == sales_id && !ss.is_deleted).FirstOrDefault();
            if (stock == null)
            {
                errRes = new AuStockBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Не найдена запись в таблице stock для sales_id=" + sales_id.ToString()),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            Loggly.InsertLog_Stock((String.IsNullOrWhiteSpace(stockReportServiceParam.mess) ? "пустое сообщение" : stockReportServiceParam.mess), type_id, user, null, DateTime.Now, stock.pharmacy_name);
            
            return new AuStockBaseResult() { result = 1, error = null, };
        }
        
        #endregion
    }
}