﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuStockApi.Models;
#endregion

namespace AuStockApi.Service
{
    public interface ITaskService : IInjectableService
    {
        GetTasksResult GetTasks(GetTasksServiceParam getTasksServiceParam);//      получение списка заданий на выполнение различных операций
    }
}
