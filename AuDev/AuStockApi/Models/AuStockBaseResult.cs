﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Util;

namespace AuStockApi.Models
{
    /// <summary>
    /// Результат вызова метода
    /// </summary>
    public class AuStockBaseResult : AuStockBaseClass
    {
        public AuStockBaseResult()
        {
            //            
        }

        /// <summary>
        /// Код результата
        /// </summary>
        public int result { get; set; }

        /// <summary>
        /// Код сформированного запроса
        /// </summary>
        public string request_id { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string mess { get; set; }
    }
}