﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuStockApi.Models
{
    /// <summary>
    /// Данные пользователя
    /// </summary>
    public class UserInfo
    {
        public UserInfo()
        {
            //
        }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string login { get; set; }

        /// <summary>
        /// Идентификатор рабочего места
        /// </summary>
        public string workplace { get; set; }

        /// <summary>
        /// Номер установленной версии
        /// </summary>
        public string version_num { get; set; }
    }
}