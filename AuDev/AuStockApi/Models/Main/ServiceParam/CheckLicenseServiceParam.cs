﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Данные пользователя с указанием кода услуги
    /// </summary>
    public class CheckLicenseServiceParam : UserInfo
    {
        public CheckLicenseServiceParam()
        {
            //
        }

        /// <summary>
        /// Код услуги
        /// </summary>
        public int? service_id { get; set; }
    }
}