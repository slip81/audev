﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Результат проверки лицензии
    /// </summary>
    public class CheckLicenseResult : AuStockBaseResult
    {
        public CheckLicenseResult()
        {
            //
        }

        /// <summary>
        /// Код клиента
        /// </summary>
        public int client_id { get; set; }

        /// <summary>
        /// Код торговой точки
        /// </summary>
        public int sales_id { get; set; }

        /// <summary>
        /// Код рабочего места
        /// </summary>
        public int workplace_id { get; set; }
    }

}