﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Сводное наличие
    /// </summary>
    public class Stock
    {
        public Stock()
        {
            //
        }

        /// <summary>
        /// Артикул
        /// </summary>
        public int artikul { get; set; }

        /// <summary>
        /// Локальный код товара в наличии (NALIC.id_nalic)
        /// </summary>
        public int stock_id { get; set; }

        /// <summary>
        /// Статус строки (0 - строка добавлена в наличие, 1 - строка удалена из наличия, 2 - строка изменена в наличии)
        /// </summary>
        public byte state { get; set; }

        /// <summary>
        /// Признак принадлежности строки (0 - строка другой аптеки, 1 - строка текущей аптеки)
        /// </summary>
        public byte is_mine { get; set; }

        /// <summary>
        /// Код ЕСН
        /// </summary>
        public int? esn_id { get; set; }

        /// <summary>
        /// Код товара
        /// </summary>
        public int? prep_id { get; set; }

        /// <summary>
        /// Наименование товара
        /// </summary>
        public string prep_name { get; set; }

        /// <summary>
        /// Код производителя товара
        /// </summary>
        public int? firm_id { get; set; }

        /// <summary>
        /// Наименование производителя товара
        /// </summary>
        public string firm_name { get; set; }

        /// <summary>
        /// Страна производителя товара
        /// </summary>
        public string country_name { get; set; }

        /// <summary>
        /// Распаковано
        /// </summary>
        public int? unpacked_cnt { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public decimal? all_cnt { get; set; }

        /// <summary>
        /// Цена розничная
        /// </summary>
        public decimal? price { get; set; }

        /// <summary>
        /// Срок годности (dd.MM.yyyy)
        /// </summary>
        public string valid_date { get; set; }

        /// <summary>
        /// Серии
        /// </summary>
        public string series { get; set; }

        /// <summary>
        /// Признак что товар является ЖНВЛП
        /// </summary>
        public byte is_vital { get; set; }

        /// <summary>
        /// Штрих-код товара
        /// </summary>
        public string barcode { get; set; }

        /// <summary>
        /// Цена производителя
        /// </summary>
        public decimal? price_firm { get; set; }

        /// <summary>
        /// Цена госреестра
        /// </summary>
        public decimal? price_gr { get; set; }

        /// <summary>
        /// Процент оптовой наценки
        /// </summary>
        public decimal? percent_gross { get; set; }

        /// <summary>
        /// Цена оптовая
        /// </summary>
        public decimal? price_gross { get; set; }

        /// <summary>
        /// Сумма оптовая
        /// </summary>
        public decimal? sum_gross { get; set; }

        /// <summary>
        /// Процент оптовый НДС
        /// </summary>
        public decimal? percent_nds_gross { get; set; }

        /// <summary>
        /// Цена оптовая с НДС
        /// </summary>
        public decimal? price_nds_gross { get; set; }

        /// <summary>
        /// Сумма оптовая с НДС
        /// </summary>
        public decimal? sum_nds_gross { get; set; }

        /// <summary>
        /// Процент розничной наценки
        /// </summary>
        public decimal? percent { get; set; }

        /// <summary>
        /// Сумма розничная
        /// </summary>
        public decimal? sum { get; set; }

        /// <summary>
        /// Поставщик
        /// </summary>
        public string supplier_name { get; set; }

        /// <summary>
        /// № документа поставщика
        /// </summary>
        public string supplier_doc_num { get; set; }

        /// <summary>
        /// Дата госреестра (dd.MM.yyyy)
        /// </summary>
        public string gr_date { get; set; }

        /// <summary>
        /// Группа
        /// </summary>
        public string farm_group_name { get; set; }

        /// <summary>
        /// Дата документа поставщика (dd.MM.yyyy)
        /// </summary>
        public string supplier_doc_date { get; set; }

        /// <summary>
        /// Сумма прибыли за одну упаковку (Цена розн — Цена опт)
        /// </summary>
        public decimal? profit { get; set; }

        /// <summary>
        /// Примечание
        /// </summary>
        public string mess { get; set; }

        /// <summary>
        /// Код отделения
        /// </summary>
        public int? depart_id { get; set; }

        /// <summary>
        /// Наименование отделения
        /// </summary>
        public string depart_name { get; set; }

        /// <summary>
        /// Наименование аптеки
        /// </summary>
        public string pharmacy_name { get; set; }

        /// <summary>
        /// Адрес отделения
        /// </summary>
        public string depart_address { get; set; }

        /// <summary>
        /// Уникальный код отделения
        /// </summary>
        public int? org_id { get; set; }
    }
}