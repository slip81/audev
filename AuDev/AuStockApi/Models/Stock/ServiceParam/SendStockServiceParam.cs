﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Параметры отправки сводного наличия
    /// </summary>
    public class SendStockServiceParam : UserInfo
    {
        public SendStockServiceParam()
        {
            //
        }

        /// <summary>
        /// Код подразделения
        /// </summary>
        public int depart_id { get; set; }

        /// <summary>
        /// Наименование подразделения
        /// </summary>
        public string depart_name { get; set; }

        /// <summary>
        /// Наименование аптеки
        /// </summary>
        public string pharmacy_name { get; set; }

        /// <summary>
        /// Адрес подразделения
        /// </summary>
        public string depart_address { get; set; }

        /// <summary>
        /// Номер отправляемого пакета в текущей активной цепочки отправки наличия
        /// </summary>
        public int part_num { get; set; }

        /// <summary>
        /// Общее количество пакетов в текущей активной цепочки отправки наличия
        /// </summary>
        public int part_cnt { get; set; }

        /// <summary>
        /// Отправляемое наличие
        /// </summary>        
        public List<Stock> rows { get; set; }
    }
}