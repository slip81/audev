﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Параметры удаления сводного наличия
    /// </summary>
    public class DelStockServiceParam : UserInfo
    {
        public DelStockServiceParam()
        {
            //
        }

        /// <summary>
        /// Код торговой точки
        /// </summary>
        public int sales_id { get; set; }

        /// <summary>
        /// Код подразделения
        /// </summary>
        public int? depart_id { get; set; }
    }
}