﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    ///Передача в лог сообщения
    /// </summary>
    public class StockReportServiceParam : UserInfo
    {
        public StockReportServiceParam()
        {
            //
        }

        /// <summary>
        /// Сообщение в лог
        /// </summary>
        public string mess { get; set; }

        /// <summary>
        /// Признак, что сообщение об ошибке
        /// </summary>
        public int? is_error { get; set; }
    }
}