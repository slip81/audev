﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Результат запроса последнего подтвержденного пакета в текущей активной цепочке получения/отправки сводного наличия
    /// </summary>
    public class GetLastBatchStockResult : AuStockBaseResult
    {
        public GetLastBatchStockResult()
        {
            //
        }

        /// <summary>
        /// Номер последнего подтвержденного пакета в текущей активной цепочке получения/отправки сводного наличия
        /// </summary>
        public int part_num { get; set; }

        /// <summary>
        /// Количество пакетов в текущей активной цепочке получения/отправки сводного наличия
        /// </summary>
        public int part_cnt { get; set; }

    }

}