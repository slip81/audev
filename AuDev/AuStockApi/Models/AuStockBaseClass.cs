﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Util;

namespace AuStockApi.Models
{
    /// <summary>
    /// Базовый класс результата вызова метода
    /// </summary>
    public class AuStockBaseClass
    {
        public AuStockBaseClass()
        {
            //            
        }

        /// <summary>
        /// Информация об ошибке
        /// </summary>
        public ErrInfo error { get; set; }

        public long? error_code
        {
            get { return error?.id; }
        }
    }

    /// <summary>
    /// Информация об ошибке
    /// </summary>
    public class ErrInfo
    {
        public ErrInfo(long? _id, string _message = "")
        {
            id = _id;
            message = _message;
        }

        public ErrInfo(long? _id, Exception e)
        {
            id = _id;
            if (e == null)
                return;

            message = GlobalUtil.ExceptionInfo(e);
        }

        /// <summary>
        /// Код ошибки
        /// </summary>
        public long? id { get; set; }

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string message { get; set; }
    }
}