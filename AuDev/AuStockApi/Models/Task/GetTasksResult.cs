﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Результат запроса получения списка заданий на выполнение
    /// </summary>
    public class GetTasksResult : AuStockBaseClass
    {
        public GetTasksResult()
        {
            //
        }

        /// <summary>
        /// Список заданий на выполнение
        /// </summary>
        public List<RequestTask> tasks { get; set; }
    }

}