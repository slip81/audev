﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Задание
    /// </summary>
    public class RequestTask
    {
        public RequestTask()
        {
            //wa_task
        }

        /// <summary>
        /// Код задания
        /// </summary>
        public int task_id { get; set; }

        /// <summary>
        /// Код типа запроса для выполнения задания
        /// </summary>
        public int request_type_id { get; set; }

        /// <summary>
        /// Код статуса задания
        /// </summary>
        public int task_state_id { get; set; }

        /// <summary>
        /// Порядковый номер задания
        /// </summary>
        public int ord { get; set; }

        /// <summary>
        /// Признак автоматического создания задания
        /// </summary>
        public int is_auto_created { get; set; }
    }
}