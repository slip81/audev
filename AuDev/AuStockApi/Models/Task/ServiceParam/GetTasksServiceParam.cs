﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuStockApi.Models
{
    /// <summary>
    /// Параметры запроса получения списка заданий на выполнение
    /// </summary>
    public class GetTasksServiceParam : UserInfo
    {
        public GetTasksServiceParam()
        {
            //
        }

        /// <summary>
        /// Код услуги
        /// </summary>          
        public int service_id { get; set; }
    }
}