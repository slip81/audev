﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuStockApi.Models;
using AuStockApi.Service;
using AuStockApi.Json;
using Swashbuckle.Swagger.Annotations;
#endregion

namespace AuStockApi.Controllers
{
    /// <summary>
    /// Методы для работы с ЕСН
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]    
    public class StockController : BaseController
    {
        public StockController(IMainService _mainService, IStockService _stockService, ITaskService _taskService)
            : base(_mainService, _stockService, _taskService)
        {
            //
        }


        /// <summary>
        /// Проверка связи с сервисом (вариант 1, без проверки связи с БД)
        /// </summary>
        [HttpPost]
        [Route("Noop")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop()
        {
            stockService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Проверка связи с сервисом (вариант 2, с проверкой связи с БД)
        /// </summary>
        [HttpPost]
        [Route("Noop2")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop2()
        {
            stockService.Noop2();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Отправка наличия на веб-сервис для формирования сводного наличия
        /// </summary>
        /// <param name="sendStockServiceParam">Данные отправляемого наличия</param>
        /// <returns>Результат отправки</returns>
        [HttpPost]
        [Route("SendStock")]
        [JsonDeserializeExt(Param = "sendStockServiceParam", JsonDataType = typeof(SendStockServiceParam))]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Отправка позиций из наличия БД АУ на веб-сервис. Чтобы уменьшить кол-во позиций в одном запросе,"
            + " реализован вариант отправки данных пакетами: на клиенте нужно сначала сформировать весь набор данных для отправки,"
            + " затем разбить его на пакеты по N записей, пронумеровать все пакеты, подсчитать общее число пакетов"
            + " и начать поочередную  отправку пакетов. В параметры part_num и part_cnt нужно записывать номер текущего пакета"
            + " и общее кол-во пакетов соответственно."
            + "<br/>"
            + " <strong>ВНИМАНИЕ !</strong> Обязательно нужно отправить все сформированные пакеты по очереди,"
            + " в случае нарушения порядка отправки или неотправки последних пакетов веб-сервис вернет ошибку",
            typeof(AuStockBaseResult))
        ]
        public HttpResponseMessage SendStock(SendStockServiceParam sendStockServiceParam)
        {
            var result = stockService.SendStock(sendStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Получение с веб-сервиса сводного наличия
        /// </summary>
        /// <param name="getStockServiceParam">Параметры получения</param>
        /// <returns>Полученное сводное наличие</returns>
        [HttpPost]
        [Route("GetStock")]        
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Загрузка с веб-сервиса строк со сводным наличием по всем отделениям текущего пользователя"
            + " в БД АУ. Будут загружены в том числе и данные о наличии в текущем подразделении,"
            + " их можно отличить от остальных строк по признаку is_mine = 1."
            + " Если значение параметра force_all = 1 – с веб-сервиса придут все строки с наличием,"
            + " даже те, которые уже были запрошены ранее."
            + " <br/>"
            + " Чтобы уменьшить кол-во позиций в одном запросе, реализован вариант"
            + " загрузки данных пакетами:  при первом вызове этого метода параметр part_num"
            + " должен быть заполнен значением 1. В выходных параметрах метода нужно будет"
            + " взять параметр part_cnt, и далее вызывать в цикле этот метод с постепенным"
            + " увеличением параметра part_num на единицу, пока он не достигнет значения"
            + " параметра part_cnt."
            + " <br/>"
            + " <strong>ВАЖНО !</strong> Во всех вызовах GetStock в одном цикле загрузки"
            + " значение параметра force_all должно быть одинаковым"
            + " (равно значению из первого вызова GetStock)",
            typeof(GetStockResult))
        ]
        public HttpResponseMessage GetStock(GetStockServiceParam getStockServiceParam)
        {
            var result = stockService.GetStock(getStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Сброс активной цепочки пакетов отправки наличия на веб-сервис
        /// </summary>
        /// <param name="resetSendStockServiceParam">Данные пользователя</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ResetSendStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "В случае непредвиденной ошибки при отправке одного из цепочки пакетов на веб-сервис"
            + " этот метод позволяет сбросить текущую активную цепочку и начать отправлять наличие заново",
            typeof(AuStockBaseResult))
        ]
        public HttpResponseMessage ResetSendStock(UserInfo resetSendStockServiceParam)
        {
            var result = stockService.ResetSendStock(resetSendStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Сброс активной цепочки пакетов загрузки наличия с веб-сервиса
        /// </summary>
        /// <param name="resetGetStockServiceParam">Данные пользователя</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ResetGetStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "В случае непредвиденной ошибки при загрузке одного из цепочки пакетов с веб-сервиса"
            + " этот метод позволяет сбросить текущую активную цепочку и начать получать наличие заново",
            typeof(AuStockBaseResult))
        ]
        public HttpResponseMessage ResetGetStock(UserInfo resetGetStockServiceParam)
        {
            var result = stockService.ResetGetStock(resetGetStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Подтверждение успешной отправки пакета сводного наличия на веб-сервис
        /// </summary>
        /// <param name="confirmStockServiceParam">Параметры подтверждения</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ConfirmSendStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Подтверждение успешной отправки пакета сводного наличия на веб-сервис",
            typeof(AuStockBaseResult))
        ]
        public HttpResponseMessage ConfirmSendStock(ConfirmStockServiceParam confirmStockServiceParam)
        {
            var result = stockService.ConfirmSendStock(confirmStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Подтверждение успешного получения пакета сводного наличия от веб-сервиса
        /// </summary>
        /// <param name="confirmStockServiceParam">Параметры подтверждения</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ConfirmGetStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Подтверждение успешного получения пакета сводного наличия от веб-сервиса",
            typeof(AuStockBaseResult))
        ]
        public HttpResponseMessage ConfirmGetStock(ConfirmStockServiceParam confirmStockServiceParam)
        {
            var result = stockService.ConfirmGetStock(confirmStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Получение последнего подтвержденного пакета отправки сводного наличия на веб-сервис
        /// </summary>
        /// <param name="getLastBatchStockServiceParam">Параметры получения пакета</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetLastBatchSendStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение последнего подтвержденного пакета отправки сводного наличия на веб-сервис",
            typeof(GetLastBatchStockResult))
        ]
        public HttpResponseMessage GetLastBatchSendStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam)
        {
            var result = stockService.GetLastBatchSendStock(getLastBatchStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Получение последнего подтвержденного пакета получения сводного наличия с веб-сервиса
        /// </summary>
        /// <param name="getLastBatchStockServiceParam">Параметры получения пакета</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetLastBatchGetStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение последнего подтвержденного пакета получения сводного наличия с веб-сервиса",
            typeof(GetLastBatchStockResult))
        ]
        public HttpResponseMessage GetLastBatchGetStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam)
        {
            var result = stockService.GetLastBatchGetStock(getLastBatchStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        // !!!
        // когда запущено удаление наличия на сервере - надо запрещать любые обмены всех ТТ этого клиента

        /// <summary>
        /// Удаление сводного наличия по заданной торговой точке с веб-сервиса
        /// </summary>
        /// <param name="delStockServiceParam">Параметры удаления наличия</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("DelStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Удаление сводного наличия по заданной торговой точке с веб-сервиса",
            typeof(AuStockBaseResult))
        ]
        public HttpResponseMessage DelStock(DelStockServiceParam delStockServiceParam)
        {
            var result = stockService.DelStock(delStockServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Передача в лог сообщения по типу операции "СН: отправка"
        /// </summary>
        /// <param name="stockReportServiceParam">Информация о пользователе и текст сообщения</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendStockReport")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Передача в лог сообщения по типу операции 'СН: отправка'",
            typeof(AuStockBaseResult))
        ]
        public HttpResponseMessage SendStockReport(StockReportServiceParam stockReportServiceParam)
        {
            var result = stockService.SendStockReport(stockReportServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Передача в лог сообщения по типу операции "СН: загрузка"
        /// </summary>
        /// <param name="stockReportServiceParam">Информация о пользователе и текст сообщения</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetStockReport")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Передача в лог сообщения по типу операции 'СН: загрузка'",
            typeof(AuStockBaseResult))
        ]
        public HttpResponseMessage GetStockReport(StockReportServiceParam stockReportServiceParam)
        {
            var result = stockService.GetStockReport(stockReportServiceParam);
            return getJsonResponse(result, result.error_code);
        }

    }
  
}
