﻿#region
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
#endregion

namespace AuStockApi.Controllers
{
    // надо использовать для методов контроллера, у которых во входных параметрах есть списки объектов (List<>)
    public class JsonDeserializeExtAttribute : ActionFilterAttribute
    {
        public string Param { get; set; }
        public Type JsonDataType { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string inputContent;
            //using (var stream = new StreamReader(actionContext.Request.Content.ReadAsStreamAsync().Result, Encoding.GetEncoding("windows-1251")))
            using (var stream = new StreamReader(actionContext.Request.Content.ReadAsStreamAsync().Result, Encoding.UTF8))
            {
                stream.BaseStream.Position = 0;
                inputContent = stream.ReadToEnd();
            }

            var result = JsonConvert.DeserializeObject(inputContent, JsonDataType);
            actionContext.ActionArguments[Param] = result;
        }

    }
}
