﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using AuDev.Common.Util;
using AuStockApi.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuStockApi.Models;
using AuStockApi.Service;
#endregion

namespace AuStockApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]
    public class BaseController : ApiController
    {
        protected readonly IMainService mainService;        
        protected readonly IStockService stockService;
        protected readonly ITaskService taskService;

        public BaseController(IMainService _mainService, IStockService _stockService, ITaskService _taskService)
        {            
            this.mainService = _mainService;
            this.stockService = _stockService;
            this.taskService = _taskService;
        }

        protected HttpResponseMessage getJsonResponse(object value, long? resultCode)
        {
            string responseContent = "";
            string mediaType = "text/plain";
            HttpStatusCode responseStatusCode = HttpStatusCode.OK;

            if ((resultCode.HasValue) && (resultCode == (long) Enums.ErrCodeEnum.ERR_CODE_ESN_SERVICE_UNAVAILABLE))
            {
                responseContent = "Service temporarily busy";
                mediaType = "text/plain";
                responseStatusCode = HttpStatusCode.ServiceUnavailable;
            }
            else
            {                
                var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver(), };
                responseContent = JsonConvert.SerializeObject(value, settings);
                mediaType = "application/json";
                responseStatusCode = HttpStatusCode.OK;
            }

            var response = this.Request.CreateResponse(responseStatusCode);
            response.Content = new StringContent(responseContent, Encoding.UTF8, mediaType);
            return response;
        }
    }
}