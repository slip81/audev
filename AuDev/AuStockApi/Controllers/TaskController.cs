﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuStockApi.Models;
using AuStockApi.Service;
using AuStockApi.Json;
using Swashbuckle.Swagger.Annotations;
#endregion

namespace AuStockApi.Controllers
{
    /// <summary>
    /// Методы для работы с ЕСН
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]    
    public class TaskController : BaseController
    {
        public TaskController(IMainService _mainService, IStockService _stockService, ITaskService _taskService)
            : base(_mainService, _stockService, _taskService)
        {
            //
        }

        /// <summary>
        /// Получение с веб-сервиса списка заданий на выполнение
        /// </summary>
        /// <param name="getTasksServiceParam">Данные пользователя с указанием кода услуги</param>
        /// <returns>Список заданий</returns>
        [HttpPost]
        [Route("GetTasks")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Загрузка с веб-сервиса строк с активными заданиями по заданной услуге"
            + " для заданного пользователя. После успешного получения задания необходимо"
            + " отправить веб-сервису уведомление о том, что задание получено"
            + " (см. метод  /SendTaskReceived ), выполнять задание можно только после этого."
            + " Допустимые типы запросов для заданий (т.е. расшифровку возможных значений"
            + " атрибута request_type_id) можно узнать методом /GetRequestTypes",
            typeof(GetTasksResult))
        ]
        public HttpResponseMessage GetTasks(GetTasksServiceParam getTasksServiceParam)
        {
            var result = taskService.GetTasks(getTasksServiceParam);
            return getJsonResponse(result, result.error_code);
        }
    }
  
}
