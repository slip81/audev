﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuStockApi.Models;
using AuStockApi.Service;
using AuStockApi.Json;
using Swashbuckle.Swagger.Annotations;
#endregion

namespace AuStockApi.Controllers
{
    /// <summary>
    /// Основные методы API
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]    
    public class MainController : BaseController
    {
        public MainController(IMainService _mainService, IStockService _stockService, ITaskService _taskService)
            : base(_mainService, _stockService, _taskService)
        {
            //
        }

        /// <summary>
        /// Проверка лицензии
        /// </summary>
        /// <param name="checkLicenseServiceParam">Данные пользователя с указанием кода услуги</param>
        /// <returns>Результат проверки лицензии</returns>
        [HttpPost]
        [Route("CheckLicense")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Проверка наличия лицензии на заданную услугу  у заданного пользователя", 
            typeof(CheckLicenseResult))
        ]
        public HttpResponseMessage CheckLicense(CheckLicenseServiceParam checkLicenseServiceParam)
        {
            var result = mainService.CheckLicense(checkLicenseServiceParam);
            return getJsonResponse(result, result.error_code);
        }

        /// <summary>
        /// Получение описания всех доступных методов API
        /// </summary>
        /// <param name="userInfo">Данные пользователя</param>
        /// <returns>Список всех доступных методов API</returns>
        [HttpPost]
        [Route("GetMethodsDescr")]
        [SwaggerResponse(
            HttpStatusCode.OK
            , "Получение информации обо всех доступных в API методов, с указанием кодов методов, их названий и кратких описаний"
            , typeof(GetMethodsDescrResult))
        ]
        public HttpResponseMessage GetMethodsDescr(UserInfo userInfo)
        {
            var result = mainService.GetMethodsDescr(userInfo);
            return getJsonResponse(result, null);
        }

    }
   
}
