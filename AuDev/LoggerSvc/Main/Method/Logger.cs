﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
#endregion

namespace LoggerSvc
{
    public partial class LoggerService : ILoggerService
    {        
        #region Log

        private int xLog(string node, List<LoggerFile> files)        
        {
            // int res = 0;
            int res = 100;

            if (String.IsNullOrEmpty(node))
                return (int)Enums.ErrCodeEnum.ERR_CODE_ATTR_NOT_FOUND;

            if ((files == null) || (files.Count <= 0))
                return (int)Enums.ErrCodeEnum.ERR_CODE_ATTR_NOT_FOUND;

            int log_num = 0;
            DateTime now = DateTime.Now;

            string step = "0";

            try
            {

                using (var dbContext = new AuMainDb())
                {
                    List<exchange_file_node_item> exchange_file_node_item_list = null;
                    exchange_file_node exchange_file_node = dbContext.exchange_file_node.Where(ss => ss.node_name.Trim().ToLower().Equals(node.Trim().ToLower())).FirstOrDefault();
                    if (exchange_file_node != null)
                    {
                        exchange_file_node_item_list = dbContext.exchange_file_node_item.Where(ss => ss.node_id == exchange_file_node.node_id).ToList();
                    }

                    step = "1";

                    log_num = dbContext.exchange_file_log.OrderByDescending(ss => ss.log_num).Select(ss => ss.log_num).FirstOrDefault();
                    log_num += 1;

                    exchange_file_log exchange_file_log = null;

                    foreach (var singleFile in files)
                    {
                        exchange_file_log = new exchange_file_log();

                        string file_name_mask = "";
                        string name_mask = "*";
                        string ext_mask = "*";
                        bool identifiedName = false;
                        bool identifiedExt = false;
                        bool identified = false;
                        if ((exchange_file_node_item_list != null) && (exchange_file_node_item_list.Count > 0))
                        {
                            step = "2";

                            string myFileName = singleFile.filename;
                            string name = myFileName.Split('.')[0];
                            string ext = myFileName.Split('.')[1];
                            
                            step = "3";

                            foreach (var item in exchange_file_node_item_list.Where(ss => (ss.file_name_mask.Split('.')[0] != "*") && (ss.file_name_mask.Split('.')[1] != "*")))
                            {
                                #region

                                file_name_mask = item.file_name_mask;
                                name_mask = "*";
                                ext_mask = "*";

                                identifiedName = false;
                                identifiedExt = false;
                                identified = false;

                                step = "4";

                                if (String.IsNullOrEmpty(file_name_mask))
                                    file_name_mask = "*.*";
                                if (file_name_mask.IndexOf(".") < 0)
                                    file_name_mask = "*.*";

                                name_mask = file_name_mask.Split('.')[0];
                                ext_mask = file_name_mask.Split('.')[1];

                                step = "5";

                                if (!((name_mask == "*") || (ext_mask == "*")))
                                {
                                    if (!String.IsNullOrEmpty(name))
                                    {
                                        if (name_mask.Contains("*"))
                                        {
                                            step = "6";

                                            name_mask = name_mask.Replace("*", "");
                                            if (name.Trim().ToLower().Contains(name_mask.Trim().ToLower()))
                                            {
                                                identifiedName = true;
                                            }

                                            step = "7";
                                        }
                                        else
                                        {
                                            if (name.Trim().ToLower().Equals(name_mask.Trim().ToLower()))
                                            {
                                                step = "8";
                                                identifiedName = true;
                                            }
                                        }
                                    }

                                    if (!String.IsNullOrEmpty(ext))
                                    {
                                        if (ext_mask.Contains("*"))
                                        {
                                            step = "9";

                                            ext_mask = ext_mask.Replace("*", "");
                                            if (ext.Trim().ToLower().Contains(ext_mask.Trim().ToLower()))
                                            {
                                                identifiedExt = true;
                                            }

                                            step = "10";
                                        }
                                        else
                                        {
                                            if (ext.Trim().ToLower().Equals(ext_mask.Trim().ToLower()))
                                            {
                                                step = "11";

                                                identifiedExt = true;
                                            }
                                        }
                                    }

                                    step = "12";

                                    identified = identifiedName && identifiedExt;
                                }
                                else
                                {
                                    if ((name_mask == "*") && (ext_mask != "*"))
                                    {
                                        if (!String.IsNullOrEmpty(ext))
                                        {                                            

                                            if (ext_mask.Contains("*"))
                                            {
                                                step = "13";

                                                ext_mask = ext_mask.Replace("*", "");
                                                if (ext.Trim().ToLower().Contains(ext_mask.Trim().ToLower()))
                                                {
                                                    identified = true;
                                                }

                                                step = "14";
                                            }
                                            else
                                            {
                                                step = "15";

                                                if (ext.Trim().ToLower().Equals(ext_mask.Trim().ToLower()))
                                                {
                                                    identified = true;
                                                }

                                                step = "16";
                                            }
                                        }
                                    }
                                    else if ((name_mask != "*") && (ext_mask == "*"))
                                    {
                                        if (!String.IsNullOrEmpty(name))
                                        {
                                            if (name_mask.Contains("*"))
                                            {
                                                step = "17";

                                                name_mask = name_mask.Replace("*", "");
                                                if (name.Trim().ToLower().Contains(name_mask.Trim().ToLower()))
                                                {
                                                    identified = true;
                                                }

                                                step = "18";
                                            }
                                            else
                                            {
                                                step = "19";

                                                if (name.Trim().ToLower().Equals(name_mask.Trim().ToLower()))
                                                {
                                                    identified = true;
                                                }

                                                step = "20";
                                            }
                                        }
                                    }
                                    else if ((name_mask == "*") && (ext_mask == "*"))
                                    {
                                        step = "21";

                                        identified = true;
                                    }
                                }


                                if (identified)
                                {
                                    step = "22";
                                    exchange_file_log.exchange_item_id = item.exchange_item_id;
                                    break;
                                }

                                #endregion
                            }

                            // !!!
                            // второй цикл - для маски вида *.xxx, xxx.*, *.*

                            if (!identified)
                            {
                                foreach (var item in exchange_file_node_item_list.Where(ss => (ss.file_name_mask.Split('.')[0] == "*") || (ss.file_name_mask.Split('.')[1] == "*")))
                                {
                                    #region

                                    file_name_mask = item.file_name_mask;
                                    name_mask = "*";
                                    ext_mask = "*";

                                    identifiedName = false;
                                    identifiedExt = false;
                                    identified = false;

                                    step = "4";

                                    if (String.IsNullOrEmpty(file_name_mask))
                                        file_name_mask = "*.*";
                                    if (file_name_mask.IndexOf(".") < 0)
                                        file_name_mask = "*.*";

                                    name_mask = file_name_mask.Split('.')[0];
                                    ext_mask = file_name_mask.Split('.')[1];

                                    step = "5";

                                    if (!((name_mask == "*") || (ext_mask == "*")))
                                    {
                                        if (!String.IsNullOrEmpty(name))
                                        {
                                            if (name_mask.Contains("*"))
                                            {
                                                step = "6";

                                                name_mask = name_mask.Replace("*", "");
                                                if (name.Trim().ToLower().Contains(name_mask.Trim().ToLower()))
                                                {
                                                    identifiedName = true;
                                                }

                                                step = "7";
                                            }
                                            else
                                            {
                                                if (name.Trim().ToLower().Equals(name_mask.Trim().ToLower()))
                                                {
                                                    step = "8";
                                                    identifiedName = true;
                                                }
                                            }
                                        }

                                        if (!String.IsNullOrEmpty(ext))
                                        {
                                            if (ext_mask.Contains("*"))
                                            {
                                                step = "9";

                                                ext_mask = ext_mask.Replace("*", "");
                                                if (ext.Trim().ToLower().Contains(ext_mask.Trim().ToLower()))
                                                {
                                                    identifiedExt = true;
                                                }

                                                step = "10";
                                            }
                                            else
                                            {
                                                if (ext.Trim().ToLower().Equals(ext_mask.Trim().ToLower()))
                                                {
                                                    step = "11";

                                                    identifiedExt = true;
                                                }
                                            }
                                        }

                                        step = "12";

                                        identified = identifiedName && identifiedExt;
                                    }
                                    else
                                    {
                                        if ((name_mask == "*") && (ext_mask != "*"))
                                        {
                                            if (!String.IsNullOrEmpty(ext))
                                            {

                                                if (ext_mask.Contains("*"))
                                                {
                                                    step = "13";

                                                    ext_mask = ext_mask.Replace("*", "");
                                                    if (ext.Trim().ToLower().Contains(ext_mask.Trim().ToLower()))
                                                    {
                                                        identified = true;
                                                    }

                                                    step = "14";
                                                }
                                                else
                                                {
                                                    step = "15";

                                                    if (ext.Trim().ToLower().Equals(ext_mask.Trim().ToLower()))
                                                    {
                                                        identified = true;
                                                    }

                                                    step = "16";
                                                }
                                            }
                                        }
                                        else if ((name_mask != "*") && (ext_mask == "*"))
                                        {
                                            if (!String.IsNullOrEmpty(name))
                                            {
                                                if (name_mask.Contains("*"))
                                                {
                                                    step = "17";

                                                    name_mask = name_mask.Replace("*", "");
                                                    if (name.Trim().ToLower().Contains(name_mask.Trim().ToLower()))
                                                    {
                                                        identified = true;
                                                    }

                                                    step = "18";
                                                }
                                                else
                                                {
                                                    step = "19";

                                                    if (name.Trim().ToLower().Equals(name_mask.Trim().ToLower()))
                                                    {
                                                        identified = true;
                                                    }

                                                    step = "20";
                                                }
                                            }
                                        }
                                        else if ((name_mask == "*") && (ext_mask == "*"))
                                        {
                                            step = "21";

                                            identified = true;
                                        }
                                    }


                                    if (identified)
                                    {
                                        step = "22";
                                        exchange_file_log.exchange_item_id = item.exchange_item_id;
                                        break;
                                    }

                                    #endregion
                                }                                    
                            }                                    
                        }

                        step = "23";
                        
                        exchange_file_log.filename = singleFile.filename;
                        exchange_file_log.period = singleFile.period;
                        exchange_file_log.crt_date = now;
                        exchange_file_log.crt_user = "logger";
                        exchange_file_log.log_num = log_num;
                        exchange_file_log.node = node;

                        dbContext.exchange_file_log.Add(exchange_file_log);
                    }

                    dbContext.SaveChanges();
                }

                Loggly.InsertLog_Logger("[" + node.Trim() + "] [Файлов: " + files.Count.ToString()  + "] Данные получены", (long)Enums.LogEsnType.LOGGER, "logger", log_num, now, null);

                return res;
            }
            catch(Exception ex)
            {
                string mess = "[" + step + "] " + GlobalUtil.ExceptionInfo(ex);
                throw new Exception(mess);
            }
        }

        private int xLog2(string node)
        {
            int res = 0;

            /*
            if (String.IsNullOrEmpty(node))
                return (int)Enums.ErrCodeEnum.ERR_CODE_ATTR_NOT_FOUND;

            int log_num = 0;
            DateTime now = DateTime.Now;

            using (var dbContext = new AuMainDb())
            {

                log_num = dbContext.exchange_file_log.OrderByDescending(ss => ss.log_num).Select(ss => ss.log_num).FirstOrDefault();
                log_num += 1;

                exchange_file_log exchange_file_log = null;

                exchange_file_log = new exchange_file_log();
                exchange_file_log.crt_date = now;
                exchange_file_log.crt_user = "logger";
                exchange_file_log.log_num = log_num;
                exchange_file_log.node = node;

                dbContext.exchange_file_log.Add(exchange_file_log);

                dbContext.SaveChanges();
            }

            Loggly.InsertLog_Logger("Данные получены", (long)Enums.LogEsnType.LOGGER, "logger", log_num, now, null);
            */
            return res;
        }

        #endregion

        #region GetOptions (JSON Support)

        public void GetOptions()
        {
            // Заголовки обработаются в EnableCorsMessageInspector 
        }

        #endregion

    }
}
