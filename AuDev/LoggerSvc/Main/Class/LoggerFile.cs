﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;

namespace LoggerSvc
{
    [DataContract]
    public class LoggerFile
    {        

        public LoggerFile()
        {
            //
        }

        [DataMember]
        public string filename { get; set; }

        [DataMember]
        public string period { get; set; }
    }
}
