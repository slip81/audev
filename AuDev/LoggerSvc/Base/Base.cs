﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace LoggerSvc
{
    #region LoggerBase

    [DataContract]
    public class LoggerBase
    {
        public LoggerBase()
        {
            //            
        }

        [DataMember]
        public ErrInfo error { get; set; }
    }

    #endregion

    #region ErrInfo

    [DataContract]
    public class ErrInfo
    {
        public ErrInfo(long? id, string message = "")
        {
            Id = id;
            Message = message;
        }

        public ErrInfo(long? id, Exception e)
        {
            Id = id;
            if (e == null)
                return;

            Message = e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                );

        }

        [DataMember]
        public long? Id { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    #endregion

}
