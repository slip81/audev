﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace LoggerSvc
{
    public partial class LoggerService : ILoggerService
    {
        #region CreateErrLoggerBase
        public T CreateErrLoggerBase<T>(Exception e)
            where T : LoggerBase, new()
        {
            //T res = Activator.CreateInstance<T>();
            T res = new T();
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                )
                );
            return res;
        }
        #endregion

        #region Log

        public int Log(string node, List<LoggerFile> files)        
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xLog(node, files);                
            }
            catch (Exception e)
            {                
                Loggly.InsertLog_Logger("Ошибка в Log: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "logger", null, log_date_beg, null);
                return (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        public int Log2(string node)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xLog2(node);
            }
            catch (Exception e)
            {
                Loggly.InsertLog_Logger("Ошибка в Log2: " + GlobalUtil.ExceptionInfo(e), (long)Enums.LogEsnType.ERROR, "logger", null, log_date_beg, null);
                return (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        #endregion

    }
}
