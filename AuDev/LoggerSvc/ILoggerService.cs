﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.ServiceModel.Web;
using System.Web.Script.Services;
using AuDev.Common.WCF;

namespace LoggerSvc
{
    [ServiceContract(Namespace = "http://services.aurit.ru/logger/")]
    public interface ILoggerService
    {

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)] // no crossdomain settings
        [WsdlDocumentation("Отправляет в лог данные об обмене")]
        [return: WsdlParamOrReturnDocumentation("0 - отправка прошла удачно, > 0 - код ошибки"
            )]
        int Log([WsdlParamOrReturnDocumentation("Кодовое наименование точки")]string node,
            [WsdlParamOrReturnDocumentation("Структура с данными о наименовании (filename) и дате-времени (period) отправки файлов")]List<LoggerFile> files
            );

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.WrappedRequest, ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        int Log2(string node);

        #region JSON Support


        [OperationContract]
        [WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
        void GetOptions();

        #endregion

    }
}
