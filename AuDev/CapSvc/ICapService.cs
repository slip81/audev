﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.ServiceModel.Web;
using AuDev.Common.WCF;

namespace CapSvc
{
    //[ServiceContract(Namespace = "http://services.aurit.ru/sender/")]
    [ServiceContract()]
    public interface ICapService
    {
        #region License

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        int Login2(string login, string hardwareId, int applicationId, string version, out int clientId, out int salesId);

        #endregion

        #region Download

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Wrapped)]
        byte[] DownloadIfPossible(string login, string hardwareId, int applicationId, string version);        

        #endregion

        #region JSON Support


        [OperationContract]
        [WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
        void GetOptions();

        #endregion

    }
}
