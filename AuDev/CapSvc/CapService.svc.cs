﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace CapSvc
{
    public partial class CapService : ICapService
    {
        #region CreateErrSenderBase
        public T CreateErrSenderBase<T>(Exception e)
            where T : LicenseBase, new()
        {
            //T res = Activator.CreateInstance<T>();
            T res = new T();
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                )
                );
            return res;
        }
        #endregion

        #region License

        public int Login2(string login, string hardwareId, int applicationId, string version, out int clientId, out int salesId)
        {
            clientId = -1;
            salesId = -1;
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xLogin2(login, hardwareId, applicationId, version, out clientId, out salesId);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_License("[Login2] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, (String.IsNullOrEmpty(login) ? "cap" : login.Trim()), null, log_date_beg, null);
                return -1;
            }
        }

        #endregion

        #region Download

        public byte[] DownloadIfPossible(string login, string hardwareId, int applicationId, string version)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xDownloadIfPossible(login, hardwareId, applicationId, version);
            }
            catch (Exception ex)
            {
                Loggly.InsertLog_License("[DownloadIfPossible] Ошибка: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEventType.ERROR, (String.IsNullOrEmpty(login) ? "cap" : login.Trim()), null, log_date_beg, null);
                return null;
            }
        }

        #endregion

    }
}
