﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace CapSvc
{
    #region LicenseBase

    [DataContract]
    public class LicenseBase
    {
        public LicenseBase()
        {
            //            
        }

        [DataMember]
        public ErrInfo error { get; set; }
    }

    #endregion

    #region ErrInfo

    [DataContract]
    public class ErrInfo
    {
        public ErrInfo(long? id, string message = "")
        {
            Id = id;
            Message = message;
        }

        public ErrInfo(long? id, Exception e)
        {
            Id = id;
            if (e == null)
                return;

            Message = e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                );

        }

        [DataMember]
        public long? Id { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    #endregion

    #region ServiceOperationResult

    [DataContract]
    public class ServiceOperationResult : LicenseBase
    {
        public ServiceOperationResult()
            : base()
        {
        }

        public ServiceOperationResult(long _id, ErrInfo _error)
        {
            Id = _id;
            error = _error;
        }

        public ServiceOperationResult(long _id, Exception _e)
        {
            Id = _id;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public ServiceOperationResult(ErrInfo _error)
        {
            Id = (_error == null) ? (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION : (long)_error.Id;
            error = _error;
        }

        public ServiceOperationResult(Exception _e)
        {
            Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        [DataMember]
        public long Id { get; set; }
    }

    #endregion

    #region UserInfo
    /// <summary>
    /// Параметры обмена пользователя с сервисом 
    /// </summary>
    [DataContract]
    public class UserInfo
    {
        public UserInfo()
        {
            //            
        }

        /// <summary>
        /// Логин торговой точки
        /// </summary>
        [DataMember]
        public string login { get; set; }
        /// <summary>
        /// Код рабочего места
        /// </summary>
        [DataMember]
        public string workplace { get; set; }
        /// <summary>
        /// Номер версии программы
        /// </summary>
        [DataMember]
        public string version_num { get; set; }
    }
    #endregion

}
