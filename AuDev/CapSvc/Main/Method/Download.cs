﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
#endregion

namespace CapSvc
{
    public partial class CapService : ICapService
    {
        #region Download

        private byte[] xDownloadIfPossible(string login, string hardwareId, int applicationId, string version)
        {
            DateTime now = DateTime.Now;

            int clientId = -1;
            int salesId = -1;
            var licOk = Login2(login, hardwareId, applicationId, version, out clientId, out salesId);
            if ((licOk != 1) || (clientId == -1) || (salesId == -1))
            {
                return null;
            }

            string FilesStore = @"C:\Sites\CAP\CAP.SiteNew\App_Data\Files\Услуги";
            string fileNameFull = "";

            Enums.ClientServiceEnum applicationEnum = Enums.ClientServiceEnum.NONE;
            bool parseOk = Enum.TryParse<Enums.ClientServiceEnum>(applicationId.ToString(), out applicationEnum);
            if (!parseOk)
                applicationEnum = Enums.ClientServiceEnum.NONE;

            switch (applicationEnum)
            {
                case Enums.ClientServiceEnum.DEFECT:
                    fileNameFull = Path.Combine(FilesStore, "remove.zip");
                    return File.ReadAllBytes(fileNameFull);
                case Enums.ClientServiceEnum.GRLS:
                    fileNameFull = Path.Combine(FilesStore, "reestr.zip");
                    return File.ReadAllBytes(fileNameFull);
                default:
                    return null;
            }
        }

        /*
                public byte[] DownloadIfPossible(string login, string hardwareId, int applicationId, string version)
                {
                    try
                    {
                        if (String.IsNullOrEmpty(login))
                            throw new Exception("Empty login");
                        var user = Membership.GetUser(login);
                        if (user == null)
                            throw new Exception("No such login");
                        if (!user.IsApproved)
                            throw new Exception("User is not approved");
                        Sales sales;
                        using (var repo = new SalesRepository())
                            sales = repo.GetByMemberShipUser(user);
                        if (sales == null)
                            throw new Exception("No such sales");
                        using (var repo = new SalesRepository())
                            sales = repo.GetById(sales.Id);
                        var wp = sales.WorkPlaces.FirstOrDefault(w => w.RegistrationKey == hardwareId);
                        if (wp == null)
                            throw new Exception("No such workplace");
                        List<ServiceRegistration> regs;
                        using (var repo = new ServiceRegistrationRepository())
                            regs = repo.GetByWorkPlace(wp);
                        var appReg =
                            regs.Where(
                                r =>
                                r.License.OrderItem.Service.Id == applicationId && r.License.OrderItem.VersionStr == version);
                        if (appReg.Count() == 0)
                            throw new Exception("No registration");
                        var lic = appReg.First().License;
                        if (lic.DtStart.HasValue && lic.DtStart > DateTime.Now)
                            throw new Exception("License period expires");
                        if (lic.DtEnd.HasValue && lic.DtEnd < DateTime.Now)
                            throw new Exception("License period expires");

                        var file = lic.OrderItem.Version.File;
                        if (file==null)
                            throw new Exception("Version without file");

                        var fileName = Path.GetFileName(file.Name);
                        string fileNameFull = String.Format(@"{0}{1}", FilesStore, file.Name.Substring(1));
                        return File.ReadAllBytes(fileNameFull);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex);
                        throw new FaultException<ServiceFaultException>(new ServiceFaultException(ex.Message), ex.Message);
                    }
                }
        */

        #endregion
    }
}
