﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Text;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Network;
#endregion

namespace CapSvc
{
    public partial class CapService : ICapService
    {
        #region License

        private int xLogin2(string login, string hardwareId, int applicationId, string version, out int clientId, out int salesId)
        {
            clientId = -1;
            salesId = -1;

            DateTime now = DateTime.Now;

            if (String.IsNullOrEmpty(login))
            {
                Loggly.InsertLog_License("Не задан логин", (long)Enums.LogEventType.ERROR, "license-checker", null, now, null);
                return -1;
            }

            if (String.IsNullOrEmpty(hardwareId))
            {
                Loggly.InsertLog_License("Не задан железный ключ", (long)Enums.LogEventType.ERROR, login, null, now, null);
                return -1;
            }

            if (applicationId <= 0)
            {
                Loggly.InsertLog_License("Не задан код услуги", (long)Enums.LogEventType.ERROR, login, null, now, null);
                return -1;
            }

            using (var dbContext = new AuMainDb())
            {
                var user = (from t1 in dbContext.vw_client_user
                            from t2 in dbContext.workplace
                            where t1.is_active == 1
                            && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                            && t2.sales_id == t1.sales_id
                            && t2.is_deleted != 1
                            && t2.registration_key.Trim().ToLower().Equals(hardwareId.Trim().ToLower())
                            select new { client_id = t1.client_id, sales_id = t1.sales_id, workplace_id = t2.id })
                           .FirstOrDefault();

                if (user == null)
                {
                    Loggly.InsertLog_License("Не найден пользователь с логином " + login.Trim() + " и жел. ключом " + hardwareId.Trim(), (long)Enums.LogEventType.ERROR, login, null, now, null);
                    return -1;
                }

                bool versionSpecified = !String.IsNullOrEmpty(version);
                string version_name_start = !versionSpecified  ? "" : new string(version.Take(4).ToArray());
                var license = (from t1 in dbContext.vw_client_service
                               where t1.workplace_id == user.workplace_id
                               && t1.service_id == applicationId
                               && ((versionSpecified && t1.version_name != null && t1.version_name.StartsWith(version_name_start)) || (!versionSpecified))
                               select t1)
                              .FirstOrDefault();

                if (license == null)
                {
                    Loggly.InsertLog_License("Не найдена лицензия на услугу с кодом " + applicationId.ToString() + ", " + (versionSpecified ? ("версия " + version.Trim() + ", "): "") + "пользователь с логином " + login.Trim() + " и жел. ключом " + hardwareId.Trim(), (long)Enums.LogEventType.ERROR, login, null, now, null);
                    return -1;
                }

                clientId = user.client_id;
                salesId = user.sales_id.GetValueOrDefault(-1);
                return 1;
            }
        }

        #endregion

        #region GetOptions (JSON Support)

        public void GetOptions()
        {
            // Заголовки обработаются в EnableCorsMessageInspector 
        }

        #endregion

    }
}
