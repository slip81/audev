﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace CapSvc
{
    public static class Loggly
    {

        private static bool isBanLog = false;

        public static bool InsertLog_License(string mess, long? type_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess;
                log_esn.log_esn_type_id = type_id;
                log_esn.user_name = user_name;
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.LICENSE;
                log_esn.date_end = DateTime.Now;
                log_esn.mess2 = mess2;
                dbContext.log_esn.Add(log_esn);
                dbContext.SaveChanges();
            }            

            return true;
        }

    }
}
