﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuUpdateApi.Models;
using AuUpdateApi.Service;
using AuUpdateApi.Json;
#endregion

namespace AuUpdateApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]
    public class BaseController : ApiController
    {
        protected readonly IAuUpdateMainService mainService;

        public BaseController(IAuUpdateMainService _mainService)
        {
            this.mainService = _mainService;
        }

        protected HttpResponseMessage getJsonResponse(object value)
        {
            var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver(), };
            string json = JsonConvert.SerializeObject(value, settings);
            //string json = JsonConvert.SerializeObject(value);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }
    }
}