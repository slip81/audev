﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuUpdateApi.Models;
using AuUpdateApi.Service;
using AuUpdateApi.Json;
using Swashbuckle.Swagger.Annotations;
#endregion

namespace AuUpdateApi.Controllers
{
    /// <summary>
    /// Основные методы API
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]
    public partial class MainController : BaseController
    {

        public MainController(IAuUpdateMainService _mainService)
            : base(_mainService)
        {
            //
        }


        /// <summary>
        /// Проверка связи с сервисом
        /// </summary>
        [HttpPost]
        [Route("Noop")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop()
        {
            mainService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Проверка обновления
        /// </summary>
        /// <param name="userInfo">Данные пользователя</param>
        /// <returns>Результат проверки обновления</returns>
        [HttpPost]
        [Route("CheckUpdate")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Проверка наличия обновлений модулей для клиента с заданным логином и рабочим местом",
            typeof(UserInfo))
        ]
        public HttpResponseMessage CheckUpdate(UserInfo userInfo)
        {
            var result = mainService.CheckUpdate(userInfo);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение обновления
        /// </summary>
        /// <param name="getUpdateServiceParam">Данные пользователя с указанием версии модуля для обновления</param>
        /// <returns>Архив с пакетом обновления указанного модуля</returns>
        [HttpPost]
        [Route("GetUpdate")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение пакета обновления с указанной версией указанного модуля. Пакет всегда приходит в формате ZIP, в котором 2 файла:"
            + " <br/>"
            + " - XML-файл с инструкцией по обновлению"
            + " <br/>"
            + " - ZIP-архив со всеми необходимыми для обновления файлами",
            typeof(GetUpdateServiceParam))
        ]
        public HttpResponseMessage GetUpdate(GetUpdateServiceParam getUpdateServiceParam)
        {
            byte[] bytes = mainService.GetUpdate(getUpdateServiceParam);
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(bytes);
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            return result;
        }

        /// <summary>
        /// Подтверждение установки обновления
        /// </summary>
        /// <param name="confirmUpdateServiceParam">Данные пользователя с указанием версии модуля для подтверждения обновления</param>
        /// <returns>Признак успешного подтверждения обновления</returns>
        [HttpPost]
        [Route("ConfirmUpdate")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Подтверждение обновления версии модуля. Метод нужно вызывать после завершения установки новой версии из полученного пакета обновления.",
            typeof(ConfirmUpdateServiceParam))
        ]
        public HttpResponseMessage ConfirmUpdate(ConfirmUpdateServiceParam confirmUpdateServiceParam)
        {
            var result = mainService.ConfirmUpdate(confirmUpdateServiceParam);
            return getJsonResponse(result);            
        }

        /// <summary>
        /// Получение информации о существующих модулях для обновления на сервере
        /// </summary>
        /// <param name="getUpdateSoftInfoServiceParam">Данные пользователя с указанием кода группы модулей для получения информации об обновлениях</param>
        /// <returns>Список доступных обновлений</returns>
        [HttpPost]
        [Route("GetUpdateSoftInfo")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение информации о существующих на сервере модулей, которые могут быть обновлены у клиентов через данный сервис",
            typeof(GetUpdateSoftInfoServiceParam))
        ]
        public HttpResponseMessage GetUpdateSoftInfo(GetUpdateSoftInfoServiceParam getUpdateSoftInfoServiceParam)
        {
            var result = mainService.GetUpdateSoftInfo(getUpdateSoftInfoServiceParam);
            return getJsonResponse(result);            
        }

        /// <summary>
        /// Загрузка на сервер новой версии файлов для обновления
        /// </summary>
        /// <param name="sendUpdateSoftContentServiceParam">Данные пользователя и содержимое загружаемого обновления</param>
        /// <returns>Признак успешной загрузки обновлений</returns>
        [HttpPost]
        [Route("SendUpdateSoftContent")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Загрузка на сервер Аурита новой версии указанного модуля. <strong>Обязательно</strong> соблюдение следующих правил:"
            + " <br/>"
            + " 1) перед загрузкой обновления должно быть сформировано 2 файла:"
            + " <br/>"
            + " updatecontent.zip – ZIP-архив со всеми необходимыми для обновления файлами (библиотеки, исполняемые файлы, ресурсы, отчеты, …)"
            + " <br/>"
            + " manifest.xml – XML-файл с инструкцией по обновлению"
            + " <br/>"
            + " 2) эти два файла должны быть упакованы в ZIP-архив с произвольным названием (xxx.zip)"
            + " <br/>"
            + " 3) в параметр content нужно записать закодированное как base64-строка содержимое файла xxx.zip",
            typeof(SendUpdateSoftContentServiceParam))
        ]
        public HttpResponseMessage SendUpdateSoftContent(SendUpdateSoftContentServiceParam sendUpdateSoftContentServiceParam)
        {
            var result = mainService.SendUpdateSoftContent(sendUpdateSoftContentServiceParam);
            return getJsonResponse(result);
        }
    }

    // надо использовать для методов контроллера, у которых во входных параметрах есть списки объектов (List<>)
    public class JsonDeserializeExtAttribute : ActionFilterAttribute
    {
        public string Param { get; set; }
        public Type JsonDataType { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string inputContent;
            using (var stream = new StreamReader(actionContext.Request.Content.ReadAsStreamAsync().Result, Encoding.GetEncoding("windows-1251")))
            {
                stream.BaseStream.Position = 0;
                inputContent = stream.ReadToEnd();
            }

            var result = JsonConvert.DeserializeObject(inputContent, JsonDataType);
            actionContext.ActionArguments[Param] = result;
        }

    }
   
}
