﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuUpdateApi.Models;
using AuUpdateApi.Service;
using AuUpdateApi.Json;
using System.Web.Http.Description;
#endregion

namespace AuUpdateApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("test")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class TestController : ApiController
    {
        private readonly IAuUpdateTestService mainService;

        public TestController(IAuUpdateTestService _mainService)
        {
            this.mainService = _mainService;
        }

        private HttpResponseMessage getJsonResponse(object value)
        {
            var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver() };
            string json = JsonConvert.SerializeObject(value, settings);
            //string json = JsonConvert.SerializeObject(value);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }

        [HttpPost]
        [Route("Noop")]        
        public HttpResponseMessage Noop()
        {
            mainService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("CheckUpdate")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage CheckUpdate(UserInfo userInfo)
        {
            var result = mainService.CheckUpdate(userInfo);
            return getJsonResponse(result);
        }
    }
}
