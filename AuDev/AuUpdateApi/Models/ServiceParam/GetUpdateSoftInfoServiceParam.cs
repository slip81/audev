﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Параметры запроса для получения информаии о доступных обновлении
    /// </summary>
    public class GetUpdateSoftInfoServiceParam : UserInfo
    {
        public GetUpdateSoftInfoServiceParam()
        {
            //
        }

        /// <summary>
        /// Код группы модулей: 1 – модули АУ (Аптека Урал), 2 – модули НАП (Надлежащая Аптечная Практика)
        /// </summary>
        public int? group_id { get; set; }
    }
}