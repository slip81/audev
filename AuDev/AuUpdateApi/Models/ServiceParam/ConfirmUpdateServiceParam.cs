﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Данные об установленном обновлении
    /// </summary>
    public class ConfirmUpdateServiceParam : GetUpdateServiceParam
    {
        public ConfirmUpdateServiceParam()
        {
            //
        }
    }
}