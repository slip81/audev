﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Параметры запроса для загрузки обновлении
    /// </summary>
    public class GetUpdateServiceParam : UserInfo
    {
        public GetUpdateServiceParam()
        {
            //
        }

        /// <summary>
        /// Код модуля
        /// </summary>
        public int soft_id { get; set; }

        /// <summary>
        /// Код версии модуля
        /// </summary>
        public int version_id { get; set; }
    }
}