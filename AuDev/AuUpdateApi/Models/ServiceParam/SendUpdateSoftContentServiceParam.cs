﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Парметры загрузки на сервер новой версии модуля
    /// </summary>
    public class SendUpdateSoftContentServiceParam : UserInfo
    {
        public SendUpdateSoftContentServiceParam()
        {
            //
        }

        /// <summary>
        /// Код модуля
        /// </summary>
        public int soft_id { get; set; }

        /// <summary>
        /// Cодержимое обновление, закодированное как base64-строка 
        /// </summary>
        public string content { get; set; }
    }
}