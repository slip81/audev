﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Данные о пользователе
    /// </summary>
    public class CheckUserResult : AuUpdateBaseResult
    {
        public CheckUserResult()
        {
            //
        }

        /// <summary>
        /// Код клиента
        /// </summary>
        public int client_id { get; set; }

        /// <summary>
        /// Код ТТ
        /// </summary>
        public int sales_id { get; set; }

        /// <summary>
        /// Код РМ
        /// </summary>
        public int workplace_id { get; set; }
    }

}