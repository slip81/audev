﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Результат метода проверки обновлений
    /// </summary>
    public class CheckUpdateResult : AuUpdateBaseResult
    {
        public CheckUpdateResult()
        {
            //
        }

        /// <summary>
        /// Список доступных обновлений
        /// </summary>
        public List<UpdateSoft> update_soft_list { get; set; }
    }

    /// <summary>
    /// Обновление модуля
    /// </summary>
    public class UpdateSoft
    {
        public UpdateSoft()
        {

        }

        /// <summary>
        /// Код модуля
        /// </summary>
        public int soft_id { get; set; }

        /// <summary>
        /// GUID модуля
        /// </summary>
        public string soft_guid { get; set; }

        /// <summary>
        /// Наименование модуля
        /// </summary>
        public string soft_name { get; set; }

        /// <summary>
        /// Код версии модуля
        /// </summary>
        public int version_id { get; set; }

        /// <summary>
        /// Номер версии модуля
        /// </summary>
        public string version_num { get; set; }

        /// <summary>
        /// Билд версии модуля
        /// </summary>
        public string version_build { get; set; }

        /// <summary>
        /// Дата версии модуля
        /// </summary>
        public string version_date { get; set; }

        /// <summary>
        /// GUID версии модуля
        /// </summary>
        public string version_guid { get; set; }

        /// <summary>
        /// Порядковый номер элемента в списке для обновления 
        /// </summary>
        public int ord { get; set; }
    }
}