﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Util;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Результат вызова метода
    /// </summary>
    public class AuUpdateBaseResult : AuUpdateBaseClass
    {
        public AuUpdateBaseResult()
        {
            //            
        }

        /// <summary>
        /// Код результата
        /// </summary>
        public int result { get; set; }
    }
}