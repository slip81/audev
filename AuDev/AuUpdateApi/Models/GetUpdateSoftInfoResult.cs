﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Данные о доступных на сервере обновлениях модулей
    /// </summary>
    public class GetUpdateSoftInfoResult : AuUpdateBaseResult
    {
        public GetUpdateSoftInfoResult()
        {
            //
        }

        /// <summary>
        /// Модули для обновления
        /// </summary>
        public List<UpdateSoftInfo> update_soft_info_list { get; set; }
    }

    /// <summary>
    /// Модуль для обновления
    /// </summary>
    public class UpdateSoftInfo
    {
        public UpdateSoftInfo()
        {

        }

        /// <summary>
        /// Код модуля
        /// </summary>
        public int soft_id { get; set; }

        /// <summary>
        /// GUID модуля
        /// </summary>        
        public string soft_guid { get; set; }

        /// <summary>
        /// Наименование модуля
        /// </summary>
        public string soft_name { get; set; }

        /// <summary>
        /// Код группы модуля
        /// </summary>
        public int group_id { get; set; }

        /// <summary>
        /// Наименование группы модуля
        /// </summary>
        public string group_name { get; set; }       
    }
}