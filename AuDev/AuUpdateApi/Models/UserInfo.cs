﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuUpdateApi.Models
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class UserInfo
    {
        public UserInfo()
        {
            //
        }

        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string login { get; set; }

        /// <summary>
        /// Идентификатор рабочего места пользователя
        /// </summary>
        public string workplace { get; set; }
        //public string version_num { get; set; }
    }
}