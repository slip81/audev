﻿using System;
using System.Text;
using System.Net;
using UnidecodeSharpFork;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace AuUpdateApi.Log
{
    public static class Loggly
    {
        //private static string logglyToken = "0488bdc7-402e-47ad-9217-adc6626dfccc";
        private static string logglyToken = "55e313a1-2945-450c-8e33-a93f4d8c02e8";
        private static string dbName = "AuMainDb";
        //private static string dbName = "AuTestDb";

        // !!!
        private static bool isBanLog = false;
        private static bool isBanLog_DB = false;
        private static bool isBanLog_Loggly = true;

        private static string GenerateUrlWithTag(string tag = "")
        {
            return "https://logs-01.loggly.com/inputs/"
                + logglyToken
                + "/tag/" + (String.IsNullOrEmpty(tag) ? dbName : tag + "," + dbName)
                + "/"
                ;          
        }

        private static bool SendLog(string mess, string tag, LogSession ls, long? type_id, long? obj_id, DateTime date_beg, string mess2, bool toLoggly)
        {
            if (!isBanLog_DB)
            {
                using (AuMainDb dbContext = new AuMainDb())
                {
                    log_event log_event = new log_event();
                    log_event.date_beg = date_beg;
                    log_event.mess = mess;
                    log_event.log_event_type_id = type_id;
                    log_event.obj_id = obj_id;
                    log_event.business_id = (ls != null ? ls.business_id : null);
                    log_event.session_id = (ls != null ? ls.session_id : null);
                    log_event.user_name = (ls != null ? ls.user_name : null);
                    log_event.scope = (int)Enums.LogScope.DISCOUNT;
                    log_event.date_end = DateTime.Now;
                    log_event.mess2 = mess2;
                    dbContext.log_event.Add(log_event);
                    dbContext.SaveChanges();
                }
            }

            return true;
        }

        public static bool Info(string source, string mess, string session_id, long? business_id, string user_name, LogSession ls, long? type_id, long? obj_id, DateTime date_beg, string mess2, bool toLoggly)
        {
            string user_name_int = ls == null ? (String.IsNullOrEmpty(user_name) ? "" : user_name) : ls.user_name;
            string log = "[" + (String.IsNullOrEmpty(session_id) ? "" : session_id) + "] "
                + "[" + (String.IsNullOrEmpty(user_name_int) ? "" : user_name_int) + "] "
                + "[" + (String.IsNullOrEmpty(source) ? "" : source) + "] "
                + (String.IsNullOrEmpty(mess) ? "" : mess)
                ;

            return SendLog("[" + dbName + "] " + log, "info", ls == null ? new LogSession() { business_id = business_id, session_id = session_id, user_name = "", } : ls, type_id, obj_id, date_beg, mess2, toLoggly);
        }

        public static bool Error(string source, string mess, string session_id, long? business_id, string user_name, LogSession ls, long? type_id, long? obj_id, DateTime date_beg, string mess2, bool toLoggly)
        {
            string user_name_int = ls == null ? (String.IsNullOrEmpty(user_name) ? "" : user_name) : ls.user_name;
            string log = "[" + (String.IsNullOrEmpty(session_id) ? "" : session_id) + "] "
                + "[" + (String.IsNullOrEmpty(user_name_int) ? "" : user_name_int) + "] "
                + "[" + (String.IsNullOrEmpty(source) ? "" : source) + "] "
                + (String.IsNullOrEmpty(mess) ? "" : mess)
                ;

            return SendLog("[" + dbName + "] " + log, "error", ls == null ? new LogSession() { business_id = business_id, session_id = session_id, user_name = "", } : ls, type_id, obj_id, date_beg, mess2, toLoggly);
        }

        public static bool Exception(string source, string mess, string session_id, long? business_id, string user_name, LogSession ls, long? type_id, long? obj_id, DateTime date_beg, string mess2, bool toLoggly)
        {
            string user_name_int = ls == null ? (String.IsNullOrEmpty(user_name) ? "" : user_name) : ls.user_name;
            string log = "[" + (String.IsNullOrEmpty(session_id) ? "" : session_id) + "] "
                + "[" + (String.IsNullOrEmpty(user_name_int) ? "" : user_name_int) + "] "
                + "[" + (String.IsNullOrEmpty(source) ? "" : source) + "] "
                + (String.IsNullOrEmpty(mess) ? "" : mess)
                ;

            return SendLog("[" + dbName + "] " + log, "exception", ls == null ? new LogSession() { business_id = business_id, session_id = session_id, user_name = "", } : ls, type_id, obj_id, date_beg, mess2, toLoggly);
        }

        public static bool Exception(string source, Exception ex, string session_id, long? business_id, string user_name, LogSession ls, long? type_id, long? obj_id, DateTime date_beg, string mess2, bool toLoggly)
        {
            string user_name_int = ls == null ? (String.IsNullOrEmpty(user_name) ? "" : user_name) : ls.user_name;
            string log = "[" + (String.IsNullOrEmpty(session_id) ? "" : session_id) + "] "
                + "[" + (String.IsNullOrEmpty(user_name_int) ? "" : user_name_int) + "] "
                + "[" + (String.IsNullOrEmpty(source) ? "" : source) + "] "
                + (ex == null ? "unknown exception" : GlobalUtil.ExceptionInfo(ex))
                ;

            return SendLog("[" + dbName + "] " + log, "exception", ls == null ? new LogSession() { business_id = business_id, session_id = session_id, user_name = "",} : ls, type_id, obj_id, date_beg, mess2, toLoggly);
        }

    }
}
