﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuUpdateApi.Models;
using AuUpdateApi.Log;

namespace AuUpdateApi.Service
{
    public partial class AuUpdateMainService
    {
        #region CheckUpdate

        public CheckUpdateResult CheckUpdate(UserInfo userInfo)
        {
            try
            {
                return checkUpdate(userInfo);
            }
            catch (Exception ex)
            {
                return new CheckUpdateResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private CheckUpdateResult checkUpdate(UserInfo userInfo)
        {
            if (userInfo == null)
                return new CheckUpdateResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            string login = userInfo.login;
            string workplace = userInfo.workplace;

            if (String.IsNullOrEmpty(login))
                return new CheckUpdateResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан логин") };

            if (String.IsNullOrEmpty(workplace))
                return new CheckUpdateResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан идентификатор рабочего места") };

            var user = (from t1 in dbContext.vw_client_user
                        from t2 in dbContext.workplace
                        where t1.is_active == 1
                        && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && t2.sales_id == t1.sales_id
                        && t2.is_deleted != 1
                        && t2.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        select new { client_id = t1.client_id, sales_id = t1.sales_id, workplace_id = t2.id })
                        .FirstOrDefault();

            if (user == null)
                return new CheckUpdateResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден клиент с логином " + login.Trim() + " и рабочим местом " + workplace.Trim()) };

            var batch_list = (from t1 in dbContext.update_batch
                              from t2 in dbContext.update_batch_item
                              from t3 in dbContext.update_soft
                              from t4 in dbContext.update_soft_version
                              where t1.batch_id == t2.batch_id
                              && t2.soft_id == t3.soft_id
                              && t3.soft_id == t4.soft_id
                              && t1.client_id == user.client_id
                              select new
                              {
                                  soft_id = t3.soft_id,
                                  soft_name = t3.soft_name,
                                  version_id = t4.version_id,
                                  version_num = t4.version_name,
                                  version_build = t4.version_build,
                                  version_date = t4.version_date,
                                  ord = t2.ord,
                                  soft_guid = t3.soft_guid,
                                  version_guid = t4.version_guid,
                              })
                             .ToList();

            if ((batch_list == null) || (batch_list.Count <= 0))
                return new CheckUpdateResult() { result = 0, };

            var client_update_soft_list = dbContext.client_update_soft.Where(ss => ss.client_id == user.client_id && ss.sales_id == user.sales_id && ss.workplace_id == user.workplace_id).ToList();
            if ((client_update_soft_list != null) && (client_update_soft_list.Count > 0))
            {
                var batch_list_for_remove = batch_list.Where(ss => client_update_soft_list.Where(tt => tt.version_id.HasValue && tt.soft_id == ss.soft_id && tt.version_id <= ss.version_id).Any()).ToList();
                if ((batch_list_for_remove != null) && (batch_list_for_remove.Count > 0))
                {
                    foreach (var batch_list_for_remove_item in batch_list_for_remove)
                    {
                        batch_list.Remove(batch_list.Where(ss => ss.soft_id == batch_list_for_remove_item.soft_id && ss.version_id == batch_list_for_remove_item.version_id).FirstOrDefault());
                    }
                }
            }

            if ((batch_list == null) || (batch_list.Count <= 0))
                return new CheckUpdateResult() { result = 0, };

            return new CheckUpdateResult()
            {
                result = 1,
                update_soft_list = batch_list.Select(ss => new UpdateSoft() 
                { 
                    soft_id = ss.soft_id,
                    soft_name = ss.soft_name == null ? "" : ss.soft_name, 
                    soft_guid = ss.soft_guid,
                    version_id = ss.version_id,
                    version_num = ss.version_num == null ? "" : ss.version_num, 
                    version_build = ss.version_build == null ? "" : ss.version_build, 
                    version_date = ss.version_date.HasValue ? ((DateTime)ss.version_date).ToString("dd.MM.yyyy") : "", 
                    version_guid = ss.version_guid,
                    ord = 1, 
                })
                .ToList(),
            };

        }
        
        #endregion
    }
}