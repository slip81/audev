﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuUpdateApi.Models;
using AuUpdateApi.Log;


namespace AuUpdateApi.Service
{
    public partial class AuUpdateMainService
    {

        #region CheckUser

        public CheckUserResult CheckUser(UserInfo userInfo)
        {
            try
            {
                return checkUser(userInfo);
            }
            catch (Exception ex)
            {
                return new CheckUserResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private CheckUserResult checkUser(UserInfo userInfo)
        {
            DateTime now = DateTime.Now;

            if (userInfo == null)
                return new CheckUserResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            string login = userInfo.login;
            string workplace = userInfo.workplace;

            if (String.IsNullOrEmpty(login))
                return new CheckUserResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан логин") };

            if (String.IsNullOrEmpty(workplace))
                return new CheckUserResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан идентификатор рабочего места") };

            var user = (from t1 in dbContext.vw_client_user
                        from t2 in dbContext.workplace
                        where t1.is_active == 1
                        && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && t2.sales_id == t1.sales_id
                        && t2.is_deleted != 1
                        && t2.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        select new { client_id = t1.client_id, sales_id = t1.sales_id, workplace_id = t2.id })
                        .FirstOrDefault();

            if (user == null)
            {
                return new CheckUserResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь с логином " + login.Trim() + " и рабочим местом " + workplace.Trim()) };
            }

            return new CheckUserResult() { result = 1, client_id = user.client_id, sales_id = (int)user.sales_id, workplace_id = user.workplace_id, };
        }
        
        #endregion
    }
}