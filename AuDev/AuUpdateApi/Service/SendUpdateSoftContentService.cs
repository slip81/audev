﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.IO;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using Ionic.Zip;
using AuUpdateApi.Models;
using AuUpdateApi.Log;
#endregion

namespace AuUpdateApi.Service
{
    public partial class AuUpdateMainService
    {
        #region SendUpdateSoftContent

        public AuUpdateBaseResult SendUpdateSoftContent(SendUpdateSoftContentServiceParam sendUpdateSoftContentServiceParam)
        {
            try
            {
                return sendUpdateSoftContent(sendUpdateSoftContentServiceParam);
            }
            catch (Exception ex)
            {
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuUpdateBaseResult sendUpdateSoftContent(SendUpdateSoftContentServiceParam sendUpdateSoftContentServiceParam)
        {
            var user = CheckUser(sendUpdateSoftContentServiceParam);
            if ((user == null) || (user.error != null))
            {
                return new GetUpdateSoftInfoResult() { error = user != null ? user.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при поиске пользователя") };
            }

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            string userMame = sendUpdateSoftContentServiceParam.login;

            int soft_id = sendUpdateSoftContentServiceParam.soft_id;
            string contentStr = sendUpdateSoftContentServiceParam.content;

            if (soft_id <= 0)
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код софта") };

            if (String.IsNullOrWhiteSpace(contentStr))
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задано содержимое обновления") };

            byte[] content = Convert.FromBase64String(contentStr);

            if ((content == null) || (content.Length <= 0))
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задано содержимое обновления") };

            update_soft update_soft = dbContext.update_soft.Where(ss => ss.soft_id == soft_id).FirstOrDefault();
            if (update_soft == null)
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден софт с кодом " + soft_id.ToString()) };

            //major.minor.build.revision = "1.0.0.0"
            Version updateSoftVersion_existing = null;
            Version updateSoftVersion_new = null;
            update_soft_version update_soft_version_existing = dbContext.update_soft_version.Where(ss => ss.soft_id == soft_id).OrderByDescending(ss => ss.version_id).FirstOrDefault();
            if (update_soft_version_existing == null)
            {
                updateSoftVersion_new = new Version(1, 0);
            }
            else
            {
                updateSoftVersion_existing = new Version(update_soft_version_existing.version_name);
                int major = updateSoftVersion_existing.Major;
                int minor = updateSoftVersion_existing.Minor;                
                if (minor == 9)
                {
                    updateSoftVersion_new = new Version(major + 1, 0);
                }
                else
                {
                    updateSoftVersion_new = new Version(major, minor + 1);
                }                
            }
            
            string file_name = "updatecontent.zip";
            string file_path = "";
            string xml_file_name = "manifest.xml";
            string xml_file_path = "";

            string folder1 = @"C:\Release\iis\cabinet\App_Data\pub\" + System.Guid.NewGuid().ToString();
            if (!Directory.Exists(folder1))
                Directory.CreateDirectory(folder1);
            file_path = Path.Combine(folder1, file_name);

            string folder2 = folder1;
            if (!Directory.Exists(folder2))
                Directory.CreateDirectory(folder2);
            xml_file_path = Path.Combine(folder2, xml_file_name);

            using (MemoryStream stream = new MemoryStream(content))
            using (ZipFile zipFile = ZipFile.Read(stream))
            {
                zipFile.ExtractAll(folder1, ExtractExistingFileAction.OverwriteSilently);
            }

            update_soft_version update_soft_version = new update_soft_version();
            update_soft_version.file_name = file_name;
            update_soft_version.file_path = file_path;
            update_soft_version.soft_id = soft_id;
            update_soft_version.upload_date = now;
            update_soft_version.upload_user = userMame;
            update_soft_version.version_build = null;
            update_soft_version.version_date = today;
            update_soft_version.version_guid = new System.Guid().ToString();
            update_soft_version.version_name = updateSoftVersion_new.Major.ToString() + "." + updateSoftVersion_new.Minor.ToString();
            update_soft_version.xml_file_name = xml_file_name;
            update_soft_version.xml_file_path = xml_file_path;
            update_soft_version.version_guid = System.Guid.NewGuid().ToString();
            dbContext.update_soft_version.Add(update_soft_version);
            dbContext.SaveChanges();

            return new AuUpdateBaseResult() { result = 1 };
        }

        #endregion
    }
}