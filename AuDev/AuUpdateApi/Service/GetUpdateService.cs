﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.IO;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using Ionic.Zip;
using AuUpdateApi.Models;
using AuUpdateApi.Log;
#endregion

namespace AuUpdateApi.Service
{
    public partial class AuUpdateMainService
    {
        #region GetUpdate

        public byte[] GetUpdate(GetUpdateServiceParam getUpdateServiceParam)
        {
            try
            {
                return getUpdate(getUpdateServiceParam);
            }
            catch (Exception ex)
            {                
                return null;
            }
        }

        private byte[] getUpdate(GetUpdateServiceParam getUpdateServiceParam)
        {
            if (getUpdateServiceParam == null)
                return null;

            string login = getUpdateServiceParam.login;
            string workplace = getUpdateServiceParam.workplace;
            int soft_id = getUpdateServiceParam.soft_id;
            int version_id = getUpdateServiceParam.version_id;

            if (String.IsNullOrEmpty(login))
                return null;

            if (String.IsNullOrEmpty(workplace))
                return null;

            var user = (from t1 in dbContext.vw_client_user
                        from t2 in dbContext.workplace
                        where t1.is_active == 1
                        && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && t2.sales_id == t1.sales_id
                        && t2.is_deleted != 1
                        && t2.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        select new { client_id = t1.client_id, sales_id = t1.sales_id, workplace_id = t2.id })
                        .FirstOrDefault();

            if (user == null)
                return null;

            var version = (from t1 in dbContext.update_soft
                           from t2 in dbContext.update_soft_version
                           where t1.soft_id == t2.soft_id
                           && t1.soft_id == soft_id
                           && t2.version_id == version_id
                           select t2)
                          .FirstOrDefault();

            if (version == null)
                return null;

            string file_path = version.file_path;
            string xml_file_path = version.xml_file_path;
            if (String.IsNullOrEmpty(file_path))
                return null;
            if (String.IsNullOrEmpty(xml_file_path))
                return null;
            
            using (MemoryStream ms = new MemoryStream())
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFile(file_path, "");
                zip.AddFile(xml_file_path, "");
                //zip.Save("BlahBlah.zip");
                zip.Save(ms);
                return ms.ToArray();
            }           
        }
        
        #endregion
    }
}