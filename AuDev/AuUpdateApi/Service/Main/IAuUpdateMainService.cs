﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuUpdateApi.Models;

namespace AuUpdateApi.Service
{
    public interface IAuUpdateMainService
    {
        void Noop();
        //
        CheckUpdateResult CheckUpdate(UserInfo userInfo);
        byte[] GetUpdate(GetUpdateServiceParam getUpdateServiceParam);
        AuUpdateBaseResult ConfirmUpdate(ConfirmUpdateServiceParam confirmUpdateServiceParam);
        //
        GetUpdateSoftInfoResult GetUpdateSoftInfo(GetUpdateSoftInfoServiceParam getUpdateSofrInfoServiceParam);
        AuUpdateBaseResult SendUpdateSoftContent(SendUpdateSoftContentServiceParam sendUpdateSoftContentServiceParam);
    }
}
