﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuUpdateApi.Models;
using AuUpdateApi.Log;

namespace AuUpdateApi.Service
{
    public partial class AuUpdateMainService : IDisposable, IAuUpdateMainService
    {
        protected AuMainDb dbContext;

        [Ninject.Inject]
        public AutoMapper.IMapper ModelMapper { get; set; }

        public AuUpdateMainService()
        {
            dbContext = new AuMainDb("AuMainDb");
            //dbContext = new AuMainDb("AuTestDb");
        }

        public void Dispose()
        {
            if (dbContext != null)
            {                
                dbContext.Dispose();
            }
        }
    }
}