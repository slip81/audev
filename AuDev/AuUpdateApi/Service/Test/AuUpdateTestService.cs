﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuUpdateApi.Models;
using AuUpdateApi.Log;

namespace AuUpdateApi.Service
{
    public class AuUpdateTestService : AuUpdateMainService, IAuUpdateTestService
    {
        public AuUpdateTestService()
        {
            //dbContext = new AuMainDb("AuMainDb");
            dbContext = new AuMainDb("AuTestDb");
        }

    }
}