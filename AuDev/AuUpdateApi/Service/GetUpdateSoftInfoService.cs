﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.IO;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using Ionic.Zip;
using AuUpdateApi.Models;
using AuUpdateApi.Log;
#endregion

namespace AuUpdateApi.Service
{
    public partial class AuUpdateMainService
    {
        #region GetUpdateSoftInfo

        public GetUpdateSoftInfoResult GetUpdateSoftInfo(GetUpdateSoftInfoServiceParam getUpdateSofrInfoServiceParam)
        {
            try
            {
                return getUpdateSoftInfo(getUpdateSofrInfoServiceParam);
            }
            catch (Exception ex)
            {
                return new GetUpdateSoftInfoResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetUpdateSoftInfoResult getUpdateSoftInfo(GetUpdateSoftInfoServiceParam getUpdateSofrInfoServiceParam)
        {
            var user = CheckUser(getUpdateSofrInfoServiceParam);
            if ((user == null) || (user.error != null))
            {
                return new GetUpdateSoftInfoResult() { error = user != null ? user.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при поиске пользователя") };
            }
            
            int? group_id = getUpdateSofrInfoServiceParam.group_id;

            List<UpdateSoftInfo> updateSoftInfo = dbContext.vw_update_soft
                .Where(ss => ((ss.group_id == group_id) && (group_id.HasValue)) || (!group_id.HasValue))
                .Select(ss => new UpdateSoftInfo()
                {
                    group_id = ss.group_id,
                    group_name = ss.group_name,
                    soft_guid = ss.soft_guid,
                    soft_id = ss.soft_id,
                    soft_name = ss.soft_name,
                })
                .ToList();

            return new GetUpdateSoftInfoResult() { result = 1, update_soft_info_list = new List<UpdateSoftInfo>(updateSoftInfo) };
        }

        #endregion
    }
}