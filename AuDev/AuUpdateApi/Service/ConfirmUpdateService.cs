﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.IO;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuUpdateApi.Models;
using AuUpdateApi.Log;

namespace AuUpdateApi.Service
{
    public partial class AuUpdateMainService
    {
        #region ConfirmUpdate

        public AuUpdateBaseResult ConfirmUpdate(ConfirmUpdateServiceParam confirmUpdateServiceParam)
        {
            try
            {
                return confirmUpdate(confirmUpdateServiceParam);
            }
            catch (Exception ex)
            {
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuUpdateBaseResult confirmUpdate(ConfirmUpdateServiceParam confirmUpdateServiceParam)
        {
            if (confirmUpdateServiceParam == null)
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            string login = confirmUpdateServiceParam.login;
            string workplace = confirmUpdateServiceParam.workplace;
            int soft_id = confirmUpdateServiceParam.soft_id;
            int version_id = confirmUpdateServiceParam.version_id;

            if (String.IsNullOrEmpty(login))
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан логин") };

            if (String.IsNullOrEmpty(workplace))
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан идентификатор рабочего места") };

            var user = (from t1 in dbContext.vw_client_user
                        from t2 in dbContext.workplace
                        where t1.is_active == 1
                        && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && t2.sales_id == t1.sales_id
                        && t2.is_deleted != 1
                        && t2.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        select new { client_id = t1.client_id, sales_id = t1.sales_id, workplace_id = t2.id })
                        .FirstOrDefault();

            if (user == null)
                return new AuUpdateBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден клиент с логином " + login.Trim() + " и рабочим местом " + workplace.Trim()) };

            client_update_soft client_update_soft = dbContext.client_update_soft.Where(ss => ss.client_id == user.client_id && ss.sales_id == user.sales_id && ss.workplace_id == user.workplace_id && ss.soft_id == soft_id).FirstOrDefault();
            if (client_update_soft == null)
            {
                client_update_soft = new client_update_soft();
                client_update_soft.client_id = user.client_id;
                client_update_soft.sales_id = user.sales_id;
                client_update_soft.workplace_id = user.workplace_id;
                client_update_soft.soft_id = soft_id;
                client_update_soft.version_id = version_id;
                dbContext.client_update_soft.Add(client_update_soft);
            }
            else
            {
                client_update_soft.version_id = version_id;
            }

            dbContext.SaveChanges();
            
            return new AuUpdateBaseResult() { result = 1, };
        }
        
        #endregion
    }
}