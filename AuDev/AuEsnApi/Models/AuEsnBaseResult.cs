﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Util;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Результат вызова метода
    /// </summary>
    public class AuEsnBaseResult : AuEsnBaseClass
    {
        public AuEsnBaseResult()
        {
            //            
        }

        /// <summary>
        /// Код результата
        /// </summary>
        public int result { get; set; }

        /// <summary>
        /// Код сформированного запроса
        /// </summary>
        public string request_id { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        public string mess { get; set; }
    }
}