﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Синоним товара
    /// </summary>
    public class PrepSyn
    {
        public PrepSyn()
        {
            //
        }

        /// <summary>
        /// Код синонима (PK)
        /// </summary>
        public int syn_id { get; set; }

        /// <summary>
        /// Наименование синонима
        /// </summary>
        public string syn_name { get; set; }

        /// <summary>
        /// Штрих-код синонима
        /// </summary>
        public string barcode { get; set; }        
    }
}