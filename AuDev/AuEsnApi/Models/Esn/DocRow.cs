﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Содержимое документа
    /// </summary>
    public class DocRow
    {
        public DocRow()
        {
            //
        }

        /// <summary>
        /// Артикул
        /// </summary>
        public int artikul { get; set; }

        /// <summary>
        /// Наименование товара
        /// </summary>
        public string prep_name { get; set; }

        /// <summary>
        /// Наименование производителя
        /// </summary>
        public string firm_name { get; set; }

        /// <summary>
        /// Штрих-код товара
        /// </summary>
        public string barcode { get; set; }

        /// <summary>
        /// Код товара
        /// </summary>
        public int? prep_id { get; set; }

        /// <summary>
        /// Код производителя
        /// </summary>
        public int? firm_id { get; set; }

        /// <summary>
        /// Синонимы товара
        /// </summary>
        List<PrepSyn> syns { get; set; }        
    }
}