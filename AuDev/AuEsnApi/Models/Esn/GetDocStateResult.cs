﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Результат запроса статуса документа
    /// </summary>
    public class GetDocStateResult : AuEsnBaseClass
    {
        public GetDocStateResult()
        {
            //
        }

        /// <summary>
        /// Статус документа
        /// </summary>
        public DocState state { get; set; }
    }

}