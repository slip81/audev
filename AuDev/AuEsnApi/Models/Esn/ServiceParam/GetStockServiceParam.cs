﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры запроса получения сводного наличия
    /// </summary>
    public class GetStockServiceParam : UserInfo
    {
        public GetStockServiceParam()
        {
            //
        }

        /// <summary>
        /// Номер пакета текущей активной цепочки получения
        /// </summary>
        public int part_num { get; set; }

        /// <summary>
        /// Признак намеренного получения всего имеющегося на сервере сводного наличия
        /// </summary>
        public int? force_all { get; set; }
    }
}