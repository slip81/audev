﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры отправки документа
    /// </summary>
    public class SendDocServiceParam : UserInfo
    {
        public SendDocServiceParam()
        {
            //
        }

        /// <summary>
        /// Код документа (PK)
        /// </summary>
        public int doc_id { get; set; }

        /// <summary>
        /// Наименование (номер) документа
        /// </summary>
        public string doc_name { get; set; }

        /// <summary>
        /// Дата документа
        /// </summary>
        public string doc_date { get; set; }

        /// <summary>
        /// Код подразделения
        /// </summary>
        public int? depart_id { get; set; }

        /// <summary>
        /// Режим разбора документа (0 - быстрый разбор, 1 - полный разбор)
        /// </summary>
        public int? mode { get; set; } // 0 - быстрый разбор (только авто), 1 - полный разбор (авто + ручной)

        /// <summary>
        /// Содержимое документа
        /// </summary>
        public List<DocRow> rows { get; set; }
    }
}