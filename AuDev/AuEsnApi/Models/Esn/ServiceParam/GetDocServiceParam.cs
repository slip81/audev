﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры запроса загрузки обработанного документа
    /// </summary>
    public class GetDocServiceParam : UserInfo
    {
        public GetDocServiceParam()
        {
            //
        }

        /// <summary>
        /// Код документа (PK)
        /// </summary>
        public int doc_id { get; set; }
    }
}