﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры запроса на подтверждение получения/отправки пакета Сводного наличия
    /// </summary>
    public class ConfirmStockServiceParam : UserInfo
    {
        public ConfirmStockServiceParam()
        {
            //
        }

        /// <summary>
        /// Код подразделения
        /// </summary>
        public int? depart_id { get; set; }

        /// <summary>
        /// Номер подтверждаемого пакета текущей активной цепочки
        /// </summary>
        public int part_num { get; set; }

    }
}