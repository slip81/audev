﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры запроса истории обработки документа
    /// </summary>
    public class GetDocLogServiceParam : UserInfo
    {
        public GetDocLogServiceParam()
        {
            //
        }

        /// <summary>
        /// Код документа (PK)
        /// </summary>
        public int? doc_id { get; set; }

        /// <summary>
        /// Дата начала истории
        /// </summary>
        public string date_beg { get; set; }

        /// <summary>
        /// Дата окончания истории
        /// </summary>
        public string date_end { get; set; }
    }
}