﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры запроса получения последнего подтвержденного пакета отправки/получения сводного наличия
    /// </summary>
    public class GetLastBatchStockServiceParam : UserInfo
    {
        public GetLastBatchStockServiceParam()
        {
            //
        }

        /// <summary>
        /// Код подразделения
        /// </summary>
        public int? depart_id { get; set; }
    }
}