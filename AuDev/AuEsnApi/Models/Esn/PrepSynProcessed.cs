﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Обработанный синоним товара
    /// </summary>
    public class PrepSynProcessed
    {
        public PrepSynProcessed()
        {
            //
        }

        /// <summary>
        /// Код синонима
        /// </summary>                        
        public int syn_id { get; set; }

        /// <summary>
        /// Товар из ЕСН
        /// </summary>  
        PrepEsn esn { get; set; }
    }
}