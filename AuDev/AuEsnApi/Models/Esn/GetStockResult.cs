﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Результат запроса сводного наличия
    /// </summary>
    public class GetStockResult : AuEsnBaseResult
    {
        public GetStockResult()
        {
            orgs = new List<int>();
        }

        /// <summary>
        /// Количество пакетов в текущей активной цепочке получения сводного наличия
        /// </summary>
        public int part_cnt { get; set; }

        /// <summary>
        /// Контрольная сумма (MD5-хэш) по списку разделенных запятой пар "Артикул_Количество" текущего сводного наличия данного клиента на веб-сервисе
        /// </summary>
        public string checksum { get; set; }

        /// <summary>
        /// Структура с информацией по контрольной сумме строк сводного наличия
        /// </summary>
        public ChecksumInfo checksum_info { get; set; }

        /// <summary>
        /// Список отделений, по которым запущена очистка наличия
        /// </summary>
        public List<int> orgs { get; set; }

        /// <summary>
        /// Полученое сводное наличие
        /// </summary>
        public List<Stock> rows { get; set; }

    }

    /// <summary>
    /// Структура с информацией по контрольной сумме строк сводного наличия
    /// </summary>
    public class ChecksumInfo
    {
        /// <summary>
        /// Контрольная сумма (MD5-хэш) по списку разделенных запятой пар "Артикул_Количество" текущего сводного наличия данного клиента на веб-сервисе
        /// </summary>
        public string checksum { get; set; }

        /// <summary>
        /// Количество строк, по которому была посчитана КС
        /// </summary>
        public int? cnt { get; set; }
    }

    }