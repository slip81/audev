﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Резудьтат запроса истории документа
    /// </summary>
    public class GetDocLogResult : AuEsnBaseClass
    {
        public GetDocLogResult()
        {
            //
        }

        /// <summary>
        /// Список истории документа
        /// </summary>
        public List<DocLog> logs { get; set; }
    }

    /// <summary>
    /// История документа
    /// </summary>
    public class DocLog
    {
        public DocLog()
        {
            //
        }

        /// <summary>
        /// Код истории (PK)
        /// </summary>
        public int log_id { get; set; }

        /// <summary>
        /// Код документа
        /// </summary>
        public int doc_id { get; set; }

        /// <summary>
        /// Наименование (номер) документа
        /// </summary>
        public string doc_name { get; set; }

        /// <summary>
        /// Дата документа
        /// </summary>
        public string doc_date { get; set; }

        /// <summary>
        /// Дата истории
        /// </summary>
        public string log_date { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string mess { get; set; }
    }

}