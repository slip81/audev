﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Результат запроса списка статусов документов
    /// </summary>
    public class GetDocStatesResult : AuEsnBaseClass
    {
        public GetDocStatesResult()
        {
            //
        }

        /// <summary>
        /// Список статусов документов
        /// </summary>
        public List<DocState> states { get; set; }
    }

    /// <summary>
    /// Статус документа
    /// </summary>
    public class DocState
    {
        public DocState()
        {
            //
        }

        /// <summary>
        /// Код статуса документа
        /// </summary>
        public int doc_state_id { get; set; }

        /// <summary>
        /// Наименование статуса документа
        /// </summary>
        public string doc_state_name { get; set; }
    }

}