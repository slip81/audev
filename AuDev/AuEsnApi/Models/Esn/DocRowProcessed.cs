﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{

    /// <summary>
    /// Обработанная строка документа
    /// </summary>
    public class DocRowProcessed
    {
        public DocRowProcessed()
        {
            //
        }

        /// <summary>
        /// Артикул
        /// </summary>
        public int artikul { get; set; }

        /// <summary>
        /// Связанный товар из ЕСН
        /// </summary>
        public PrepEsn esn { get; set; }

        /// <summary>
        /// Обработанные синонимы товара
        /// </summary>
        public List<PrepSynProcessed> syns;
    }
}