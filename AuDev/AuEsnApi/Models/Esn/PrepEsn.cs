﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Товар из ЕСН
    /// </summary>
    public class PrepEsn
    {
        public PrepEsn()
        {
            //
        }

        /// <summary>
        /// Код товара в ЕСН (PK)
        /// </summary>
        public int? esn_id { get; set; }

        /// <summary>
        /// Наименование товара в ЕСН
        /// </summary>
        public string prep_name_esn { get; set; }

        /// <summary>
        /// Наименование производителя товара в ЕСН
        /// </summary>
        public string firm_name_esn { get; set; }

        /// <summary>
        /// Штрих-код товара в ЕСН
        /// </summary>
        public string barcode_esn { get; set; }

        /// <summary>
        /// Код товара в ЕСН
        /// </summary>
        public int? prep_id_esn { get; set; }

        /// <summary>
        /// Код производителя товара в ЕСН
        /// </summary>
        public int? firm_id_esn { get; set; }
    }
}