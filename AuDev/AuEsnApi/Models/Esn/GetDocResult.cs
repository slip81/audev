﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Результат запроса документа
    /// </summary>
    public class GetDocResult : AuEsnBaseClass
    {
        public GetDocResult()
        {
            //
        }

        /// <summary>
        /// Статус документа
        /// </summary>
        public DocState state { get; set; }

        /// <summary>
        /// Количество строк
        /// </summary>
        public int row_count { get; set; }

        /// <summary>
        /// Количество связанных строк
        /// </summary>
        public int row_count_linked { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string mess { get; set; }

        /// <summary>
        /// Обработанные строки
        /// </summary>
        public List<DocRowProcessed> rows { get; set; }
    }

}