﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Результат получения списка доступных методов API
    /// </summary>
    public class GetMethodsDescrResult : AuEsnBaseClass
    {
        public GetMethodsDescrResult()
        {
            //
        }

        /// <summary>
        /// Cписок доступных методов API
        /// </summary>
        public List<MethodDescription> descriptions { get; set; }
    }

    /// <summary>
    /// Метод API
    /// </summary>
    public class MethodDescription
    {
        public MethodDescription()
        {
            //
        }

        /// <summary>
        /// Код метода (PK)
        /// </summary>
        public int method_id { get; set; }

        /// <summary>
        /// Наименование метода
        /// </summary>
        public string method_name { get; set; }

        /// <summary>
        /// Описание метода
        /// </summary>
        public string method_descr { get; set; }
    }
}