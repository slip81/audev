﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Результат авторизации в АСНА WebAPI
    /// </summary>
    public class AsnaAuthResult : AuEsnBaseClass
    {
        public AsnaAuthResult()
        {
            //
        }

        /// <summary>
        /// Авторизация в АСНА WebAPI
        /// </summary>
        public AsnaAuth asna_auth { get; set; }        
    }
}