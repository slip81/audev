﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Util;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Настройка связи с АСНА WebAPI
    /// </summary>
    public static class AsnaParam
    {
        /// <summary>
        /// Основной адрес АСНА WebAPI
        /// </summary>
        public static string baseUrl = "https://api.asna.cloud";

        /// <summary>
        /// Список методов АСНА WebAPI
        /// </summary>
        public static List<AsnaService> ServiceList { get; set; }

        static AsnaParam()
        {
            ServiceList = new List<AsnaService>()
            {
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_Auth, 
                    descr = "Авторизация",
                    request_type = "POST",                     
                    url = "https://sso.asna.cloud:5000/connect/token"                    
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_GetStoreInfo, 
                    descr = "Информация об аптеке",
                    request_type = "POST", 
                    version = "v4", 
                    url = "references/store_info" 
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_GetGoodsLinks, 
                    descr = "Справочник связок номенклатуры",
                    request_type = "GET", 
                    version = "v4", 
                    url = "references/goods_links" 
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_SendRemains, 
                    descr = "Передача остатков",
                    request_type = "POST", 
                    version = "v4", 
                    url = "stores/{storeId}/stocks" 
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_DelRemains, 
                    descr = "Удаление остатков",
                    request_type = "DELETE", 
                    version = "v4", 
                    url = "stores/{storeId}/stocks" 
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_GetRemains, 
                    descr = "Получение остатков",
                    request_type = "GET", 
                    version = "v4", 
                    url = "stores/{storeId}/stocks" 
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_GetOrders, 
                    descr = "Получение заказов",
                    request_type = "GET", 
                    version = "v4", 
                    url = "stores/{storeId}/orders_exchanger" 
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_SendOrders, 
                    descr = "Отправка заказов",
                    request_type = "PUT", 
                    version = "v4", 
                    url = "stores/{storeId}/orders_exchanger" 
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_SendConsPrice, 
                    descr = "Передача сводных прайс-листов",
                    request_type = "POST", 
                    version = "v4", 
                    url = "legal_entities/{legalId}/preorders" 
                },
                new AsnaService() { 
                    id = AuEsnConst.ASNA_service_id_GetTaskState, 
                    descr = "Получение статусов обработки заданий",
                    request_type = "GET", 
                    version = "v4", 
                    url = "messages/{messageId}/status" 
                },
            };
        }

        /// <summary>
        /// Получение метода АСНА WebAPI по коду
        /// </summary>
        /// <param name="serviceId">Код метода</param>
        /// <returns>Метод АСНА WebAPI</returns>
        public static AsnaService GetService(int serviceId)
        {
            return ServiceList.Where(ss => ss.id == serviceId).FirstOrDefault();
        }

        /// <summary>
        /// Получение относительного адреса метода АСНА WebAPI по коду метода
        /// </summary>
        /// <param name="serviceId">Код метода</param>
        /// <returns>Относительный адрес метода АСНА WebAPI</returns>
        public static string GetServiceSelfUrl(int serviceId)
        {
            AsnaService asnaService = GetService(serviceId);
            return ((asnaService == null) || (String.IsNullOrWhiteSpace(asnaService.url))) ?
                null
                : asnaService.url;
        }

        /// <summary>
        /// Получение полного адреса метода АСНА WebAPI по коду метода
        /// </summary>
        /// <param name="serviceId">Код метода</param>
        /// <returns>Полный адрес метода АСНА WebAPI</returns>
        public static string GetServiceFullUrl(int serviceId)
        {
            AsnaService asnaService = GetService(serviceId);
            return ((asnaService == null) || (String.IsNullOrWhiteSpace(baseUrl))) ? 
                null
                : baseUrl + (String.IsNullOrWhiteSpace(asnaService.version) ? "" : ("/" + asnaService.version.Trim())) + (String.IsNullOrWhiteSpace(asnaService.url) ? "" : ("/" + asnaService.url.Trim()));
        }
    }

    /// <summary>
    /// Метод АСНА WebAPI
    /// </summary>
    public class AsnaService
    {
        public AsnaService()
        {
            //
        }

        /// <summary>
        /// Код метода
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Описание метода
        /// </summary>
        public string descr { get; set; }

        /// <summary>
        /// Тип вызова метода
        /// </summary>
        public string request_type { get; set; }

        /// <summary>
        /// Версия метода
        /// </summary>
        public string version { get; set; }

        /// <summary>
        /// Относительный адрес метода
        /// </summary>
        public string url { get; set; }        
    }
}