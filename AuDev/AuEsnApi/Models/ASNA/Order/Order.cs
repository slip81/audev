﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Заказы
    /// </summary>
    public class Order
    {
        public Order()
        {
            //vw_asna_order
        }

        /// <summary>
        /// Код заказа
        /// </summary>        
        public int order_id { get; set; }

        /// <summary>
        /// Код аптеки по АСНА, на которой сделан заказ
        /// </summary>        
        public string store_id { get; set; }

        /// <summary>
        /// Код аптеки по АСНА, которая выдает заказ
        /// </summary>        
        public string issuer_store_id { get; set; }

        /// <summary>
        /// ТТ, на которой сделан заказ
        /// </summary>        
        public string sales_name { get; set; }

        /// <summary>
        /// ТТ, которая выдает заказ
        /// </summary>        
        public string issuer_sales_name { get; set; }

        /// <summary>
        /// Код заказа по АСНА
        /// </summary>        
        public string asna_order_id { get; set; }

        /// <summary>
        /// Источник заказа
        /// </summary>        
        public string src { get; set; }

        /// <summary>
        /// Номер заказа
        /// </summary>        
        public string num { get; set; }

        /// <summary>
        /// Дата заказа (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        public string date { get; set; } // dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Имя покупателя
        /// </summary>        
        public string name { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>        
        public string m_phone { get; set; }

        /// <summary>
        /// Тип оплаты
        /// </summary>        
        public string pay_type { get; set; }

        /// <summary>
        /// ИД типа оплаты по справочнику АСНА
        /// </summary>        
        public string pay_type_id { get; set; }

        /// <summary>
        /// Дисконтная карта
        /// </summary>        
        public string d_card { get; set; }

        /// <summary>
        /// Признак АСНА-Экономия
        /// </summary>        
        public int ae { get; set; }

        /// <summary>
        /// ИД совместной покупки
        /// </summary>        
        public string union_id { get; set; }

        /// <summary>
        /// Дату и время (UTC) создания или обновления заголовка заказа (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        public string ts { get; set; } // dd.MM.yyyy HH:mm:ss

    }
}