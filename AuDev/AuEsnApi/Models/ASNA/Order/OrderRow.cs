﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Строки заказов
    /// </summary>
    public class OrderRow
    {
        public OrderRow()
        {
            //vw_asna_order_row
        }

        /// <summary>
        /// Код строки заказа
        /// </summary>        
        public int row_id { get; set; }

        /// <summary>
        /// Код заказа
        /// </summary>        
        public int order_id { get; set; }

        /// <summary>
        /// Код строки заказа по АСНА
        /// </summary>
        public string asna_row_id { get; set; }

        /// <summary>
        /// Код заказа по АСНА
        /// </summary>
        public string asna_order_id { get; set; }

        /// <summary>
        /// Тип строки (0 - наличие, 1 - предзаказ)
        /// </summary>
        public int row_type { get; set; }

        /// <summary>
        /// Уникальный код партии
        /// </summary>
        public string prt_id { get; set; }

        /// <summary>
        /// Код товара по справочнику АСНА
        /// </summary>        
        public int nnt { get; set; }

        /// <summary>
        /// Количество в заказе
        /// </summary>
        public decimal qnt { get; set; }

        /// <summary>
        /// Цена товара в момент заказа
        /// </summary>
        public decimal prc { get; set; }

        /// <summary>
        /// Цена товара по АСНА-Экономия
        /// </summary>
        public decimal? prc_dsc { get; set; }

        /// <summary>
        /// Признак дисконта
        /// </summary>
        public string dsc_union { get; set; }

        /// <summary>
        /// Дотация для единицы товара
        /// </summary>
        public decimal? dtn { get; set; }

        /// <summary>
        /// Цена реализации со скидкой по программам лояльности
        /// </summary>
        public decimal? prc_loyal { get; set; }

        /// <summary>
        /// Цена поставки
        /// </summary>
        public decimal? prc_opt_nds { get; set; }

        /// <summary>
        /// ИНН поставщика
        /// </summary>
        public string sup_inn { get; set; }

        /// <summary>
        /// Дата поставки (dd.MM.yyyy HH:mm:ss)
        /// </summary>
        public string dlv_date { get; set; } //dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Количество незарезервированного товара
        /// </summary>
        public decimal? qnt_unrsv { get; set; }

        /// <summary>
        /// Фиксированная цена
        /// </summary>
        public decimal? prc_fix { get; set; }

        /// <summary>
        /// Дату и время (UTC) создания или обновления строки заказа (dd.MM.yyyy HH:mm:ss)
        /// </summary>
        public string ts { get; set; } //dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Уникальный код товара в аптеке
        /// </summary>        
        public string sku { get; set; }
    }
}