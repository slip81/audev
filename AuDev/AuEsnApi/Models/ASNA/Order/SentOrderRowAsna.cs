﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Строки заказов для отправки в АСНА в формате АСНА
    /// </summary>
    public class SentOrderRowAsna
    {
        public SentOrderRowAsna()
        {
            //
        }

        /// <summary>
        /// Код строки заказа
        /// </summary>
        [JsonProperty("rowId")]
        public string row_id { get; set; }

        /// <summary>
        /// Количество незарезервированного товара
        /// </summary>
        [JsonProperty("qntUnrsv")]
        public decimal? qnt_unrsv { get; set; }

    }
}