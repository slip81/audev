﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Данные о строках и стаусах заказов для передачи в АСНА в формате АСНА
    /// </summary>
    public class SentOrderAsna
    {
        public SentOrderAsna()
        {
            //
        }

        /// <summary>
        /// Строки заказа
        /// </summary>
        [JsonProperty("rows")]
        public List<SentOrderRowAsna> rows { get; set; }

        /// <summary>
        /// Статусы заказа
        /// </summary>
        [JsonProperty("statuses")]
        public List<SentOrderStateAsna> statuses { get; set; }
    }
}