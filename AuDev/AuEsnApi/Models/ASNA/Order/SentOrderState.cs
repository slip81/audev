﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Статусы заказов для отправки в АСНА
    /// </summary>
    public class SentOrderState
    {
        public SentOrderState()
        {
            //
        }

        /// <summary>
        /// Код строки заказа (заполняется только если статус на строку) (берется из обрабатываемого заказа)
        /// </summary>        
        public int? row_id { get; set; }

        /// <summary>
        /// Код заказа (берется из обрабатываемого заказа)
        /// </summary>        
        public int order_id { get; set; }

        /// <summary>
        /// Код аптеки по АСНА (берется из обрабатываемого заказа)
        /// </summary>        
        public string store_id { get; set; }

        /// <summary>
        /// Дата и время статуса (dd.MM.yyyy HH:mm:ss)
        /// </summary>
        public string date { get; set; } // dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Код статуса
        /// </summary>        
        public int status { get; set; }

        /// <summary>
        /// Дата и время сброса резерва (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        public string rc_date { get; set; } // dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Комментарий
        /// </summary>        
        public string cmnt { get; set; }

    }
}