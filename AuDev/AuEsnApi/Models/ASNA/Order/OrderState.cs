﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Статусы заказов
    /// </summary>
    public class OrderState
    {
        public OrderState()
        {
            //
        }

        /// <summary>
        /// Код статуса
        /// </summary>        
        public int status_id { get; set; }

        /// <summary>
        /// Код строки заказа (заполняется только если статус на строку)
        /// </summary>        
        public int? row_id { get; set; }

        /// <summary>
        /// Код заказа
        /// </summary>        
        public int order_id { get; set; }

        /// <summary>
        /// Код статуса
        /// </summary>
        public string asna_status_id { get; set; }

        /// <summary>
        /// Код строки заказа (заполняется только если статус на строку)
        /// </summary>
        public string asna_row_id { get; set; }

        /// <summary>
        /// Код заказа
        /// </summary>
        public string asna_order_id { get; set; }

        /// <summary>
        /// Дата и время статуса (dd.MM.yyyy HH:mm:ss)
        /// </summary>
        public string date { get; set; } // dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Код статуса
        /// </summary>        
        public int status { get; set; }

        /// <summary>
        /// Статус
        /// </summary>        
        public string status_name { get; set; }

        /// <summary>
        /// Дата и время сброса резерва (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        public string rc_date { get; set; } // dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Комментарий
        /// </summary>        
        public string cmnt { get; set; }

        /// <summary>
        /// Дату и время (UTC) создания или обновления статуса (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        public string ts { get; set; } // dd.MM.yyyy HH:mm:ss

    }
}