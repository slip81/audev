﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Статусы заказов в формате АСНА
    /// </summary>
    public class OrderStateAsna
    {
        public OrderStateAsna()
        {
            //
        }

        public override bool Equals(object obj)
        {
            if (!(obj is OrderStateAsna))
            {
                return false;
            }

            OrderStateAsna item = obj as OrderStateAsna;
            return item == null ? false : this.status_id.Trim().ToLower().Equals(item.status_id.Trim().ToLower());
        }

        /// <summary>
        /// Код статуса
        /// </summary>
        [JsonProperty("statusId")]
        public string status_id { get; set; }

        /// <summary>
        /// Код строки заказа (заполняется только если статус на строку)
        /// </summary>
        [JsonProperty("rowId")]
        public string row_id { get; set; }

        /// <summary>
        /// Код заказа
        /// </summary>
        [JsonProperty("orderId")]
        public string order_id { get; set; }

        /// <summary>
        /// Код аптеки
        /// </summary>
        [JsonProperty("storeId")]
        public string store_id { get; set; }

        /// <summary>
        /// Дата и время статуса
        /// </summary>
        [JsonProperty("date")]
        public DateTime? date { get; set; }

        /// <summary>
        /// Код статуса
        /// </summary>
        [JsonProperty("status")]
        public int status { get; set; }

        /// <summary>
        /// Дата и время сброса резерва
        /// </summary>
        [JsonProperty("rcDate")]
        public DateTime? rc_date { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [JsonProperty("cmnt")]
        public string cmnt { get; set; }

        /// <summary>
        /// Дату и время (UTC) создания или обновления статуса
        /// </summary>
        [JsonProperty("ts")]
        public DateTime? ts { get; set; }
        
        /// <summary>
        /// Код заказа в БД веб-сервиса
        /// </summary>
        [JsonIgnore()]
        public int? db_order_id { get; set; }
        
        /// <summary>
        /// Код строки заказа в БД веб-сервиса
        /// </summary>
        [JsonIgnore()]
        public int? db_row_id { get; set; }

        /// <summary>
        /// Код статуса в БД веб-сервиса
        /// </summary>
        [JsonIgnore()]
        public int? db_state_id { get; set; }

        /// <summary>
        /// Признак: новый статус или существующмй статус
        /// </summary>
        [JsonIgnore()]
        public bool is_new { get; set; }

    }
}