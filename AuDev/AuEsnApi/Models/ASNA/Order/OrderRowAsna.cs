﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Строки заказов в формате АСНА
    /// </summary>
    public class OrderRowAsna
    {
        public OrderRowAsna()
        {
            //
        }

        public override bool Equals(object obj)
        {
            if (!(obj is OrderRowAsna))
            {
                return false;
            }

            OrderRowAsna item = obj as OrderRowAsna;
            return item == null ? false : this.row_id.Trim().ToLower().Equals(item.row_id.Trim().ToLower());
        }

        /// <summary>
        /// Код строки заказа
        /// </summary>
        [JsonProperty("rowId")]
        public string row_id { get; set; }

        /// <summary>
        /// Код заказа
        /// </summary>
        [JsonProperty("orderId")]
        public string order_id { get; set; }

        /// <summary>
        /// Тип строки (0 - наличие, 1 - предзаказ)
        /// </summary>
        [JsonProperty("rowType")]
        public int row_type { get; set; }

        /// <summary>
        /// Уникальный код партии
        /// </summary>
        [JsonProperty("prtId")]
        public string prt_id { get; set; }

        /// <summary>
        /// Код товара по справочнику АСНА
        /// </summary>
        [JsonProperty("nnt")]
        public int nnt { get; set; }

        /// <summary>
        /// Количество в заказе
        /// </summary>
        [JsonProperty("qnt")]
        public decimal qnt { get; set; }

        /// <summary>
        /// Цена товара в момент заказа
        /// </summary>
        [JsonProperty("prc")]
        public decimal prc { get; set; }

        /// <summary>
        /// Цена товара по АСНА-Экономия
        /// </summary>
        [JsonProperty("prcDsc")]
        public decimal? prc_dsc { get; set; }

        /// <summary>
        /// Признак дисконта
        /// </summary>
        [JsonProperty("dscUnion")]
        public string dsc_union { get; set; }

        /// <summary>
        /// Дотация для единицы товара
        /// </summary>
        [JsonProperty("dtn")]
        public decimal? dtn { get; set; }

        /// <summary>
        /// Цена реализации со скидкой по программам лояльности
        /// </summary>
        [JsonProperty("prcLoyal")]
        public decimal? prc_loyal { get; set; }

        /// <summary>
        /// Цена поставки
        /// </summary>
        [JsonProperty("prcOptNds")]
        public decimal? prc_opt_nds { get; set; }

        /// <summary>
        /// ИНН поставщика
        /// </summary>
        [JsonProperty("supInn")]
        public string sup_inn { get; set; }

        /// <summary>
        /// Дата поставки в формате ISO8601
        /// </summary>
        [JsonProperty("dlvDate")]
        public DateTime? dlv_date { get; set; }

        /// <summary>
        /// Количество незарезервированного товара
        /// </summary>
        [JsonProperty("qntUnrsv")]
        public decimal? qnt_unrsv { get; set; }

        /// <summary>
        /// Фиксированная цена
        /// </summary>
        [JsonProperty("prcFix")]
        public decimal? prc_fix { get; set; }

        /// <summary>
        /// Дату и время (UTC) создания или обновления строки заказа
        /// </summary>
        [JsonProperty("ts")]
        public DateTime? ts { get; set; }

        /// <summary>
        /// Код заказа в БД веб-сервиса
        /// </summary>
        [JsonIgnore()]
        public int? db_order_id { get; set; }

        /// <summary>
        /// Код строки заказа в БД веб-сервиса
        /// </summary>
        [JsonIgnore()]
        public int? db_row_id { get; set; }

        /// <summary>
        /// Признак: новая строка или существующая строка
        /// </summary>
        [JsonIgnore()]
        public bool is_new { get; set; }

        /// <summary>
        /// Уникальный код товара в аптеке
        /// </summary>
        [JsonIgnore()]
        public string db_sku { get; set; }

    }
}