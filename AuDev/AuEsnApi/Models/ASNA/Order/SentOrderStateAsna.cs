﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Статусы заказов для отправки в АСНА в формате АСНА
    /// </summary>
    public class SentOrderStateAsna
    {
        public SentOrderStateAsna()
        {
            //
        }

        /// <summary>
        /// Код статуса
        /// </summary>
        [JsonProperty("statusId")]
        public string status_id { get; set; }

        /// <summary>
        /// Код строки заказа (заполняется только если статус на строку)
        /// </summary>
        [JsonProperty("rowId")]
        public string row_id { get; set; }

        /// <summary>
        /// Код заказа
        /// </summary>
        [JsonProperty("orderId")]
        public string order_id { get; set; }

        /// <summary>
        /// Код аптеки
        /// </summary>
        [JsonProperty("storeId")]
        public string store_id { get; set; }

        /// <summary>
        /// Дата и время статуса
        /// </summary>
        [JsonProperty("date")]
        public DateTime? date { get; set; }

        /// <summary>
        /// Код статуса
        /// </summary>
        [JsonProperty("status")]
        public int status { get; set; }

        /// <summary>
        /// Дата и время сброса резерва
        /// </summary>
        [JsonProperty("rcDate")]        
        public DateTime? rc_date { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [JsonProperty("cmnt")]        
        public string cmnt { get; set; }

    }
}