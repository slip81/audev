﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Заказы в формате АСНА
    /// </summary>
    public class OrderAsna
    {
        public OrderAsna()
        {
            //            
        }

        public override bool Equals(object obj)
        {
            if (!(obj is OrderAsna))
            {
                return false;
            }

            OrderAsna item = obj as OrderAsna;
            return item == null ? false : this.order_id.Trim().ToLower().Equals(item.order_id.Trim().ToLower());
        }

        /// <summary>
        /// Код заказа
        /// </summary>
        [JsonProperty("orderId")]
        public string order_id { get; set; }

        /// <summary>
        /// Код аптеки, на которой сделан заказ
        /// </summary>
        [JsonProperty("storeId")]
        public string store_id { get; set; }

        /// <summary>
        /// Код аптеки, которая выдает заказ
        /// </summary>
        [JsonProperty("issuerId")]
        public string issuer_id { get; set; }

        /// <summary>
        /// Источник заказа
        /// </summary>
        [JsonProperty("src")]
        public string src { get; set; }

        /// <summary>
        /// Номер заказа
        /// </summary>
        [JsonProperty("num")]
        public string num { get; set; }

        /// <summary>
        /// Дата заказа в формате ISO8601
        /// </summary>
        [JsonProperty("date")]
        public DateTime? date { get; set; }

        /// <summary>
        /// Имя покупателя
        /// </summary>
        [JsonProperty("name")]
        public string name { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        [JsonProperty("mPhone")]
        public string m_phone { get; set; }

        /// <summary>
        /// Тип оплаты
        /// </summary>
        [JsonProperty("payType")]
        public string pay_type { get; set; }

        /// <summary>
        /// ИД типа оплаты по справочнику АСНА
        /// </summary>
        [JsonProperty("payTypeId")]
        public string pay_type_id { get; set; }

        /// <summary>
        /// Дисконтная карта
        /// </summary>
        [JsonProperty("d_card")]
        public string d_card { get; set; }

        /// <summary>
        /// Признак АСНА-Экономия
        /// </summary>
        [JsonProperty("ae")]
        public int ae { get; set; }

        /// <summary>
        /// ИД совместной покупки
        /// </summary>
        [JsonProperty("unionId")]
        public string union_id { get; set; }

        /// <summary>
        /// Дату и время (UTC) создания или обновления заголовка заказа
        /// </summary>
        [JsonProperty("ts")]
        public DateTime? ts { get; set; }

        /// <summary>
        /// Код заказа в БД веб-сервиса
        /// </summary>
        [JsonIgnore()]
        public int? db_order_id { get; set; }

        /// <summary>
        /// Признак: новый заказ или существующий заказ
        /// </summary>
        [JsonIgnore()]
        public bool is_new { get; set; }

    }
}