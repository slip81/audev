﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Полученные заказы от АСНА
    /// </summary>
    public class ReceivedOrdersAsna
    {
        /// <summary>
        /// Массив заголовков заказов
        /// </summary>
        [JsonProperty("headers")]
        public List<OrderAsna> headers { get; set; }

        /// <summary>
        /// Массив строк заказов
        /// </summary>
        [JsonProperty("rows")]
        public List<OrderRowAsna> rows { get; set; }

        /// <summary>
        /// Массив статусов заказов
        /// </summary>
        [JsonProperty("statuses")]
        public List<OrderStateAsna> statuses { get; set; }
    }
}