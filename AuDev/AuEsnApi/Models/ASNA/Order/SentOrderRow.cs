﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Строки заказов для отправки в АСНА
    /// </summary>
    public class SentOrderRow
    {
        public SentOrderRow()
        {
            //vw_asna_order_row
        }

        /// <summary>
        /// Код строки заказа
        /// </summary>        
        public int row_id { get; set; }

        /// <summary>
        /// Количество незарезервированного товара
        /// </summary>
        public decimal? qnt_unrsv { get; set; }

    }
}