﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Полученная информация о заказах
    /// </summary>
    public class GetOrdersResult : AuEsnBaseClass
    {
        public GetOrdersResult()
        {
            //
        }

        /// <summary>
        /// Заголовки заказов
        /// </summary>
        public List<Order> orders { get; set; }

        /// <summary>
        /// Строки заказов
        /// </summary>
        public List<OrderRow> rows { get; set; }

        /// <summary>
        /// Статусы заголовков заказов
        /// </summary>
        public List<OrderState> orders_states { get; set; }

        /// <summary>
        /// Статусы строк заказов
        /// </summary>
        public List<OrderState> rows_states { get; set; }
    }
}