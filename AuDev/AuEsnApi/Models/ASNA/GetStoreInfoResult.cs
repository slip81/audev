﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Полученная информация об аптеке
    /// </summary>
    public class GetStoreInfoResult : AuEsnBaseClass
    {
        public GetStoreInfoResult()
        {
            //
        }

        /// <summary>
        /// Информация об аптеке
        /// </summary>
        public StoreInfo store_info { get; set; }
    }
}