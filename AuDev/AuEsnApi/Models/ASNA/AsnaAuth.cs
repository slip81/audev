﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Авторизация в АСНА WebAPI
    /// </summary>
    public class AsnaAuth
    {
        public AsnaAuth()
        {
            //
        }

        /// <summary>
        /// Авторизационный токен
        /// </summary>
        [JsonProperty("access_token")]
        public string access_token { get; set; }

        /// <summary>
        /// Период действия авторизации
        /// </summary>
        [JsonProperty("expires_in")]
        public string expires_in { get; set; }

        /// <summary>
        /// Тип токена
        /// </summary>
        [JsonProperty("token_type")]
        public string token_type { get; set; }

        /// <summary>
        /// Дата-время начала действия авторизации
        /// </summary>
        [JsonIgnore()]
        public DateTime? auth_date_beg { get; set; }

        /// <summary>
        /// Дата-время окончания действия авторизации
        /// </summary>
        [JsonIgnore()]
        public DateTime? auth_date_end { get; set; }
    }
}