﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Авторизованный в АСНА WebAPI пользователь АСНА
    /// </summary>
    public class GetAuthorizedUserResult : AuEsnBaseClass
    {
        public GetAuthorizedUserResult()
        {
            //
        }

        /// <summary>
        /// Пользователь АСНА
        /// </summary>
        public asna_user asna_user { get; set; }
    }
}