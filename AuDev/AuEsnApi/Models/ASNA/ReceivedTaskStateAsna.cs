﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Полученный статус задания от АСНА
    /// </summary>
    public class ReceivedTaskStateAsna
    {
        /// <summary>
        /// Идентификатор статуса
        /// </summary>
        [JsonProperty("id")]
        public int? asna_state_id { get; set; }
        
        /// <summary>
        /// Идентификатор задания
        /// </summary>
        [JsonProperty("messageId")]
        public string asna_message_id { get; set; }

        /// <summary>
        /// Код типа статуса
        /// </summary>
        [JsonProperty("statusId")]
        public int? asna_state_type_id { get; set; }

        /// <summary>
        /// Наименование типа статуса
        /// </summary>
        [JsonProperty("statusMessage")]
        public string asna_state_type_name { get; set; }

        /// <summary>
        /// Дата статуса в формате ISO8601
        /// </summary>
        [JsonProperty("statusDate")]
        public DateTime? asna_state_date { get; set; }
    }
}