﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Расписание работы аптеки
    /// </summary>
    public class StoreInfoSchedule
    {
        public StoreInfoSchedule()
        {
            //asna_store_info_schedule
        }

        /// <summary>
        /// Порядковый номер дня недели, начиная с 1(понедельник)
        /// </summary>
        public int week_day_id { get; set; }

        /// <summary>
        /// Время открытия аптеки
        /// </summary>        
        public string start_time { get; set; }

        /// <summary>
        /// Время закрытия аптеки
        /// </summary>        
        public string end_time { get; set; }

        /// <summary>
        /// Признак рабочего дня
        /// </summary>        
        public bool work { get; set; }

    }
}