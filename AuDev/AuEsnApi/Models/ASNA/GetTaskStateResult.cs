﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Полученная информация об аптеке
    /// </summary>
    public class GetTaskStateResult
    {
        public GetTaskStateResult()
        {
            //
        }

        /// <summary>
        /// Информация о статусе
        /// </summary>
        // public ReceivedTaskStateAsna task_state { get; set; }
        public string task_state { get; set; }
    }
}