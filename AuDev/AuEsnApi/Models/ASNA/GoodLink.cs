﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Связка товаров в АСНА WebAPI
    /// </summary>
    public class GoodLink
    {
        public GoodLink()
        {
            //asna_link
        }

        /// <summary>
        /// Код связки
        /// </summary>
        [JsonIgnore()]
        public int link_id { get; set; }

        /// <summary>
        /// Код пользователя АСНА
        /// </summary>
        [JsonIgnore()]
        public int asna_user_id { get; set; }

        /// <summary>
        /// Код товара в АСНА
        /// </summary>
        [JsonProperty("nnt")]
        public int asna_nnt { get; set; }

        /// <summary>
        /// Код товара в аптеке
        /// </summary>
        [JsonProperty("sku")]
        public string asna_sku { get; set; }

        /// <summary>
        /// Наименование товара
        /// </summary>
        [JsonProperty("name")]
        public string asna_name { get; set; }

    }
}