﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры отправки сводного прайс-листа
    /// </summary>
    public class SendConsPriceServiceParam : UserInfo
    {
        public SendConsPriceServiceParam()
        {
            //
        }

        /// <summary>
        /// Признак отправки полного прайс-листа или изменений в прайс-листе
        /// </summary>
        public int? is_full { get; set; }

        /// <summary>
        /// Код исходного задания на отправку прайс-листа
        /// </summary>
        public int? task_id { get; set; }

        /// <summary>
        /// Дата-время выгрузки прайс-листа (dd.MM.yyyy HH:mm:ss)
        /// </summary>
        public string price_date { get; set; } //dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Номер пакета в цепочке
        /// </summary>
        public int part_num { get; set; }

        /// <summary>
        /// Общее количество пакетов в цепочке
        /// </summary>
        public int part_cnt { get; set; }

        /// <summary>
        /// Признак режима тестирования передачи пакетов
        /// </summary>
        public int? is_test { get; set; }

        /// <summary>
        /// Прайс-листы
        /// </summary>
        public List<ConsPrice> prices { get; set; }
    }
}