﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры получения статусов обработки заданий из АСНА
    /// </summary>
    public class GetTaskStateFromAsnaServiceParam : AsnaServiceParam
    {

        /// <summary>
        ///  Код задания, статус обработки которого нужно получить
        /// </summary>
        public int task_id { get; set; }
    }
}