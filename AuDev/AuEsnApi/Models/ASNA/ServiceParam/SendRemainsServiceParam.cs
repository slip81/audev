﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры отправки остатков
    /// </summary>
    public class SendRemainsServiceParam : UserInfo
    {
        public SendRemainsServiceParam()
        {
            //
        }

        /// <summary>
        /// Признак отправки полных остатков или измененных остатков
        /// </summary>
        public int? is_full { get; set; }

        /// <summary>
        /// Код исходного задания на отправку остатков
        /// </summary>
        public int? task_id { get; set; }

        /// <summary>
        /// Дата-время формирования остатков (dd.MM.yyyy HH:mm:ss)
        /// </summary>
        public string stock_date { get; set; } //dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Номер пакета в цепочке
        /// </summary>
        public int part_num { get; set; }

        /// <summary>
        /// Общее количество пакетов в цепочке
        /// </summary>
        public int part_cnt { get; set; }

        /// <summary>
        /// Признак режима тестирования передачи пакетов
        /// </summary>
        public int? is_test { get; set; }

        /// <summary>
        /// Строки наличия
        /// </summary>
        public List<Remains> rows { get; set; }
    }
}