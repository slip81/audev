﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры получения информации о сводных прайс-листах из АСНА
    /// </summary>
    public class GetConsPriceFromAsnaServiceParam : AsnaServiceParam
    {

        /// <summary>
        ///  Дата/время, с которой необходимо получить прайс-листы
        /// </summary>
        // public DateTime? price_date { get; set; }
    }
}