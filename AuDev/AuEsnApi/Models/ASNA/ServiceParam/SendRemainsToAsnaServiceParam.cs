﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Цепочка отправки остатков в АСНА
    /// </summary>
    public class SendRemainsToAsnaServiceParam
    {
        public SendRemainsToAsnaServiceParam()
        {
            //
        }

        /// <summary>
        /// Код пользователя
        /// </summary>
        public int asna_user_id { get; set; }

        /// <summary>
        /// Код цепочки
        /// </summary>
        public int chain_id { get; set; }
    }
}