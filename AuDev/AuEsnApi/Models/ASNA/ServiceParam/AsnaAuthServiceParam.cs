﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры авторизации в АСНА WebAPI
    /// </summary>
    public class AsnaAuthServiceParam
    {
        public AsnaAuthServiceParam()
        {
            //
        }

        /// <summary>
        /// Код аптеки по АСНА
        /// </summar
        public string asna_store_id { get; set; }

        /// <summary>
        /// Пароль аптеки
        /// </summar
        public string asna_pwd { get; set; }
    }
}