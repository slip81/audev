﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры отправки информации о заказах
    /// </summary>
    public class SendOrdersServiceParam : UserInfo
    {
        public SendOrdersServiceParam()
        {
            //
        }

        /// <summary>
        /// Строки заказов
        /// </summary>
        public List<SentOrderRow> rows { get; set; }

        /// <summary>
        /// Статусы заголовков заказов
        /// </summary>
        public List<SentOrderState> orders_states { get; set; }

        /// <summary>
        /// Статусы строк заказов
        /// </summary>
        public List<SentOrderState> rows_states { get; set; }

    }
}