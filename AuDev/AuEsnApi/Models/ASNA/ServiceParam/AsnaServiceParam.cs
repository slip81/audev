﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Данные пользователя АСНА
    /// </summary>
    public class AsnaServiceParam
    {
        public AsnaServiceParam()
        {
            //
        }

        /// <summary>
        /// Код пользователя АСНА
        /// </summary>
        public int asna_user_id { get; set; }
    }
}