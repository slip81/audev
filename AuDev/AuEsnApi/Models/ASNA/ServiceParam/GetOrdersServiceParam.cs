﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры получения информации о заказах
    /// </summary>
    public class GetOrdersServiceParam : UserInfo
    {
        public GetOrdersServiceParam()
        {
            //
        }

        /// <summary>
        /// Дата, с которой необходимо получить изменения (не включительно)  (dd.MM.yyyy HH:mm:ss)
        /// </summary>
        public string date_beg { get; set; } //dd.MM.yyyy HH:mm:ss
    }
}