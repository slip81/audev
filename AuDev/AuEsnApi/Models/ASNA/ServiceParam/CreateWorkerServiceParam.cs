﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры запуска обработчика
    /// </summary>
    public class CreateWorkerServiceParam
    {
        public CreateWorkerServiceParam()
        {
            //
        }

        /// <summary>
        /// Пароль для запуска
        /// </summary>
        public string pwd { get; set; }

        /// <summary>
        /// Признак ведения лога
        /// </summary>
        public int? log_enabled { get; set; }
    }
}