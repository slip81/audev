﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Информация об аптеке
    /// </summary>
    public class StoreInfoAsna
    {
        public StoreInfoAsna()
        {
            //asna_store_info
            //asna_user
        }

        /// <summary>
        /// Код (PK)
        /// </summary>
        [JsonIgnore()]
        public int info_id { get; set; }

        /// <summary>
        /// Код пользователя АСНА
        /// </summary>
        [JsonIgnore()]
        public int asna_user_id { get; set; }

        /// <summary>
        /// Название аптеки (короткое)
        /// </summary>
        [JsonProperty("name")]
        public string asna_name { get; set; }

        /// <summary>
        /// Название аптеки (полное)
        /// </summary>
        [JsonProperty("fullName")]
        public string asna_full_name { get; set; }

        /// <summary>
        /// ИНН аптеки
        /// </summary>
        [JsonProperty("inn")]
        public string asna_inn { get; set; }

        /// <summary>
        /// Время резерва товара c терминала
        /// </summary>
        [JsonProperty("rtTerminal")]
        public Nullable<int> asna_rt_terminal { get; set; }

        /// <summary>
        /// Время резерва товара с мобильного приложения
        /// </summary>
        [JsonProperty("rtMobile")]
        public Nullable<int> asna_rt_mobile { get; set; }

        /// <summary>
        /// Время резерва товара с сайта
        /// </summary>
        [JsonProperty("rtSite")]
        public Nullable<int> asna_rt_site { get; set; }

        /// <summary>
        /// Признак возможности редактирования чеков
        /// </summary>
        [JsonProperty("editCheck")]
        public bool asna_edit_check { get; set; }

        /// <summary>
        /// Признак возможности отмены заказа
        /// </summary>
        [JsonProperty("cancelOrder")]
        public bool asna_cancel_order { get; set; }

        /// <summary>
        /// Признак круглосуточной работы аптеки
        /// </summary>
        [JsonProperty("fullTime")]
        public bool asna_full_time { get; set; }

        /// <summary>
        /// Уникальный код аптеки по справочнику АСНА
        /// </summary>
        [JsonProperty("storeId")]
        public string asna_store_id { get; set; }

        /// <summary>
        /// Уникальный код ЮЛ по справочнику АСНА
        /// </summary>
        [JsonProperty("legalId")]
        public string asna_legal_id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [JsonIgnore()]
        public Nullable<System.DateTime> crt_date { get; set; }

        /// <summary>
        /// Кто создал
        /// </summary>
        [JsonIgnore()]
        public string crt_user { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        [JsonIgnore()]
        public Nullable<System.DateTime> upd_date { get; set; }

        /// <summary>
        /// Кто изменил
        /// </summary>
        [JsonIgnore()]
        public string upd_user { get; set; }

        /// <summary>
        /// Признак удаленной строки
        /// </summary>
        [JsonIgnore()]
        public bool is_deleted { get; set; }

        /// <summary>
        /// Дата удаления
        /// </summary>
        [JsonIgnore()]
        public Nullable<System.DateTime> del_date { get; set; }

        /// <summary>
        /// Кто удалил
        /// </summary>
        [JsonIgnore()]
        public string del_user { get; set; }

        /// <summary>
        /// Расписание работы аптеки
        /// </summary>
        [JsonProperty("schedule")]
        public List<StoreInfoScheduleAsna> schedule { get; set; }

    }
}