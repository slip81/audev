﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Остатки товаров
    /// </summary>
    public class Remains
    {
        public Remains()
        {
            //
        }

        /// <summary>
        /// Артикул
        /// </summary>
        public int artikul { get; set; }

        /// <summary>
        /// Код товара в аптеке
        /// </summary>        
        public int prep_id { get; set; }

        /// <summary>
        /// Наименование товара в аптеке
        /// </summary>        
        public string prep_name { get; set; }

        /// <summary>
        /// Код производителя товара в аптеке
        /// </summary>        
        public int? firm_id { get; set; }

        /// <summary>
        /// Наименование производителя товара в аптеке
        /// </summary>
        public string firm_name { get; set; }

        /// <summary>
        /// Штрих-код товара в аптеке
        /// </summary>
        public string barcode { get; set; }

        /// <summary>
        /// Код ЕСН товара в аптеке
        /// </summary>
        public int? esn_id { get; set; }

        /// <summary>
        /// Уникальный код партии товара
        /// </summary>
        public string prt_id { get; set; }

        /// <summary>
        /// Уникальный код товара в аптеке
        /// </summary>        
        public string sku { get; set; }

        /// <summary>
        /// Код товара в АСНА
        /// </summary>
        public int? nnt { get; set; }

        /// <summary>
        /// Текущее количество в партии
        /// </summary>        
        public decimal? all_cnt { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string supplier_doc_num { get; set; }

        /// <summary>
        /// ИНН поставщика
        /// </summary>
        public string supplier_inn { get; set; }

        /// <summary>
        /// Дата поставки
        /// </summary>
        public string supplier_doc_date { get; set; }

        /// <summary>
        /// НДС товара (0, 10, 18)
        /// </summary>
        public int? percent_nds { get; set; }
        
        /// <summary>
        /// Признак ЖНВЛС товара
        /// </summary>
        public int? is_vital { get; set; }

        /// <summary>
        /// Признак Детское питание
        /// </summary>        
        public int? is_child_food { get; set; }

        /// <summary>
        /// Серия товара
        /// </summary>        
        public string series { get; set; }

        /// <summary>
        /// Срок годности
        /// </summary>        
        public string valid_date { get; set; }

        /// <summary>
        /// Цена производителя
        /// </summary>
        public decimal? price_firm { get; set; }

        /// <summary>
        /// Цена оптовая
        /// </summary>
        public decimal? price_gross { get; set; }

        /// <summary>
        /// Цена оптовая с НДС
        /// </summary>
        public decimal? price_nds_gross { get; set; }

        /// <summary>
        /// Цена реализации
        /// </summary>
        public decimal? price_retail { get; set; }

        /// <summary>
        /// Цена госреестра
        /// </summary>
        public decimal? price_gr { get; set; }

        /// <summary>
        /// Максимально допустимая розничная цена для ЖНВЛС
        /// </summary>
        public decimal? price_max_vital { get; set; }

        /// <summary>
        /// Сумма оптовая без НДС
        /// </summary>
        public decimal? sum_gross { get; set; }

        /// <summary>
        /// Сумма оптовая с НДС
        /// </summary>
        public decimal? sum_nds_gross { get; set; }

        /// <summary>
        /// Процент наценки для ЖВЛНС и Детского питания 
        /// </summary>
        public decimal? percent_vital { get; set; }

        /// <summary>
        /// Признак товара (0 - нормальный, 1- сроковый, 2 - уценка, 3 - мятая упаковка)
        /// </summary>
        public int? tag { get; set; }
    }
}