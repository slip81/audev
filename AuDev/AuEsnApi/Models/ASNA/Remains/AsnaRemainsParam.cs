﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Класс для передачи остатков в АСНА
    /// </summary>
    public class AsnaRemainsParam
    {
        public AsnaRemainsParam()
        {
            //
        }

        /// <summary>
        /// Дата выгрузки остатков
        /// </summary>
        [JsonProperty("date")]
        public DateTime stock_date { get; set; }

        /// <summary>
        /// Массив остатков
        /// </summary>
        [JsonProperty("stocks")]
        public List<RemainsAsna> remains { get; set; }
    }
}