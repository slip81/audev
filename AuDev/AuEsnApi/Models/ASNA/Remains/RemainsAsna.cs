﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Остатки товаров в формате АСНА
    /// </summary>
    public class RemainsAsna
    {
        public RemainsAsna()
        {
            //
        }

        /// <summary>
        /// Наименование производителя товара в аптеке
        /// </summary>
        [JsonProperty("man")]
        public string firm_name { get; set; }

        /// <summary>
        /// Уникальный код партии товара
        /// </summary>
        [JsonProperty("prtId")]
        public string prt_id { get; set; }

        /// <summary>
        /// Уникальный код товара в аптеке
        /// </summary>
        [JsonProperty("sku")]
        public string sku { get; set; }

        /// <summary>
        /// Код товара в АСНА
        /// </summary>
        [JsonProperty("nnt")]
        public int? nnt { get; set; }

        /// <summary>
        /// Текущее количество в партии
        /// </summary>
        [JsonProperty("qnt")]
        public decimal? all_cnt { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        [JsonProperty("doc")]
        public string supplier_doc_num { get; set; }

        /// <summary>
        /// ИНН поставщика
        /// </summary>
        [JsonProperty("supInn")]
        public string supplier_inn { get; set; }

        /// <summary>
        /// Дата поставки
        /// </summary>
        [JsonProperty("supDate")]
        public string supplier_doc_date { get; set; }

        /// <summary>
        /// НДС товара (0, 10, 18)
        /// </summary>
        [JsonProperty("nds")]
        public int? percent_nds { get; set; }
        
        /// <summary>
        /// Признак ЖНВЛС товара
        /// </summary>
        [JsonProperty("gnvls")]
        //public int? is_vital { get; set; }
        public bool is_vital { get; set; }

        /// <summary>
        /// Признак Детское питание
        /// </summary>
        [JsonProperty("dp")]
        //public int? is_child_food { get; set; }
        public bool is_child_food { get; set; }

        /// <summary>
        /// Серия товара
        /// </summary>
        [JsonProperty("series")]
        public string series { get; set; }

        /// <summary>
        /// Срок годности
        /// </summary>
        [JsonProperty("exp")]
        public string valid_date { get; set; }

        /// <summary>
        /// Цена производителя
        /// </summary>
        [JsonProperty("prcMan")]
        public decimal? price_firm { get; set; }

        /// <summary>
        /// Цена оптовая
        /// </summary>
        [JsonProperty("prcOpt")]
        public decimal? price_gross { get; set; }

        /// <summary>
        /// Цена оптовая с НДС
        /// </summary>
        [JsonProperty("prcOptNds")]
        public decimal? price_nds_gross { get; set; }

        /// <summary>
        /// Цена реализации
        /// </summary>
        [JsonProperty("prcRet")]
        public decimal? price_retail { get; set; }

        /// <summary>
        /// Цена госреестра
        /// </summary>
        [JsonProperty("prcReestr")]
        public decimal? price_gr { get; set; }

        /// <summary>
        /// Максимально допустимая розничная цена для ЖНВЛС
        /// </summary>
        [JsonProperty("prcGnvlsMax")]
        public decimal? price_max_vital { get; set; }

        /// <summary>
        /// Сумма оптовая без НДС
        /// </summary>
        [JsonProperty("sumOpt")]
        public decimal? sum_gross { get; set; }

        /// <summary>
        /// Сумма оптовая с НДС
        /// </summary>
        [JsonProperty("sumOptNds")]
        public decimal? sum_nds_gross { get; set; }

        /// <summary>
        /// Процент наценки для ЖВЛНС и Детского питания 
        /// </summary>
        [JsonProperty("mrGnvls")]
        public decimal? percent_vital { get; set; }

        /// <summary>
        /// Признак товара (0 - нормальный, 1- сроковый, 2 - уценка, 3 - мятая упаковка)
        /// </summary>
        [JsonProperty("tag")]
        public int? tag { get; set; }
    }
}