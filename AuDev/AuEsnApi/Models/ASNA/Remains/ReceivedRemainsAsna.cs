﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Полученные остатки товаров от АСНА
    /// </summary>
    public class ReceivedRemainsAsna : RemainsAsna
    {
        /// <summary>
        /// Идентификатор сети
        /// </summary>
        [JsonProperty("realNetId")]
        public int? asna_real_net_id { get; set; }
        
        /// <summary>
        /// Идентификатор аптеки по справочнику АСНА
        /// </summary>
        [JsonProperty("storeId")]
        public string asna_store_id { get; set; }

        /// <summary>
        /// Дата остатков в формате ISO8601
        /// </summary>
        [JsonProperty("stkDate")]
        public DateTime? asna_stock_date { get; set; }

        /// <summary>
        /// Дата загрузки остатков в формате ISO8601
        /// </summary>
        [JsonProperty("loadDate")]
        public DateTime? asna_load_date { get; set; }

        /// <summary>
        /// Версия записи
        /// </summary>
        [JsonProperty("rv")]
        public byte[] asna_rv { get; set; }
    }
}