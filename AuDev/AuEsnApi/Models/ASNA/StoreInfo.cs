﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Информация об аптеке
    /// </summary>
    public class StoreInfo
    {
        public StoreInfo()
        {
            //asna_store_info
            //asna_user
        }

        /// <summary>
        /// Название аптеки (короткое)
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Название аптеки (полное)
        /// </summary>
        public string full_name { get; set; }

        /// <summary>
        /// ИНН аптеки
        /// </summary>
        public string inn { get; set; }

        /// <summary>
        /// Время резерва товара c терминала
        /// </summary>
        public Nullable<int> rt_terminal { get; set; }

        /// <summary>
        /// Время резерва товара с мобильного приложения
        /// </summary>
        public Nullable<int> rt_mobile { get; set; }

        /// <summary>
        /// Время резерва товара с сайта
        /// </summary>
        public Nullable<int> rt_site { get; set; }

        /// <summary>
        /// Признак возможности редактирования чеков
        /// </summary>        
        public bool edit_check { get; set; }

        /// <summary>
        /// Признак возможности отмены заказа
        /// </summary>        
        public bool cancel_order { get; set; }

        /// <summary>
        /// Признак круглосуточной работы аптеки
        /// </summary>
        public bool full_time { get; set; }

        /// <summary>
        /// Уникальный код аптеки по справочнику АСНА
        /// </summary>
        public string store_id { get; set; }

        /// <summary>
        /// Уникальный код ЮЛ по справочнику АСНА
        /// </summary>        
        public string legal_id { get; set; }

        /// <summary>
        /// Расписание работы аптеки
        /// </summary>        
        public List<StoreInfoSchedule> schedule { get; set; }

    }
}