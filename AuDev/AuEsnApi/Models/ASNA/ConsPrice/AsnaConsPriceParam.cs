﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Класс для передачи сводного прайс-листа в АСНА
    /// </summary>
    public class AsnaConsPriceParam
    {
        public AsnaConsPriceParam()
        {
            //
        }

        /// <summary>
        /// Дата выгрузки прайс-листа
        /// </summary>
        [JsonProperty("date")]
        public DateTime price_date { get; set; }

        /// <summary>
        /// Массив строк прайс-листа
        /// </summary>
        [JsonProperty("preorders")]
        public List<ConsPriceAsna> prices { get; set; }
    }
}