﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Полученные сводные прайс-листы от АСНА
    /// </summary>
    public class ReceivedConsPriceAsna : ConsPriceAsna
    {
        /// <summary>
        /// Уникальный код ЮЛ по АСНА
        /// </summary>        
        [JsonProperty("srcId")]
        public string src_id { get; set; }
    }
}