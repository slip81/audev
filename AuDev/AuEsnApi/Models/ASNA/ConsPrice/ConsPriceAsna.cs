﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Сводный прайс-лист в формате АСНА
    /// </summary>
    public class ConsPriceAsna
    {
        public ConsPriceAsna()
        {
            //
        }


        /// <summary>
        /// Уникальный код товара в аптеке
        /// </summary>        
        [JsonProperty("sku")]
        public string sku { get; set; }

        /// <summary>
        /// Код товара в АСНА
        /// </summary>
        [JsonProperty("nnt")]
        public int? nnt { get; set; }

        /// <summary>
        /// ИНН поставщика
        /// </summary>        
        [JsonProperty("supInn")]
        public string sup_inn { get; set; }

        /// <summary>
        /// Цена оптовая с НДС
        /// </summary>        
        [JsonProperty("prcOptNds")]
        public decimal? prc_opt_nds { get; set; }

        /// <summary>
        /// Максимальная розничная цена для ЖНВЛС и ДП
        /// </summary>        
        [JsonProperty("prcGnvlsMax")]
        public decimal? prc_gnvls_max { get; set; }
        
        /// <summary>
        /// Признак активной или удаленной строки (0 - активная, 1 - удаленная)
        /// </summary>        
        [JsonProperty("status")]
        public int? status { get; set; }

        /// <summary>
        /// Срок годности (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        [JsonProperty("exp")]
        public string exp { get; set; } //dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Дата поставки товара (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        [JsonProperty("supDate")]
        public string sup_date { get; set; } //dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Дата прайс-листа (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        [JsonProperty("prclDate")]
        public string prcl_date { get; set; } //dd.MM.yyyy HH:mm:ss
    }
}