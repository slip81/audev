﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Сводный прайс-лист
    /// </summary>
    public class ConsPrice
    {
        public ConsPrice()
        {
            //
        }

        /// <summary>
        /// Артикул
        /// </summary>
        public int artikul { get; set; }

        /// <summary>
        /// Код товара в аптеке
        /// </summary>        
        public int prep_id { get; set; }

        /// <summary>
        /// Наименование товара в аптеке
        /// </summary>        
        public string prep_name { get; set; }

        /// <summary>
        /// Код производителя товара в аптеке
        /// </summary>        
        public int? firm_id { get; set; }

        /// <summary>
        /// Наименование производителя товара в аптеке
        /// </summary>
        public string firm_name { get; set; }

        /// <summary>
        /// Штрих-код товара в аптеке
        /// </summary>
        public string barcode { get; set; }

        /// <summary>
        /// Код ЕСН товара в аптеке
        /// </summary>
        public int? esn_id { get; set; }

        /// <summary>
        /// Уникальный код товара в аптеке
        /// </summary>        
        public string sku { get; set; }

        /// <summary>
        /// Код товара в АСНА
        /// </summary>
        public int? nnt { get; set; }

        /// <summary>
        /// ИНН поставщика
        /// </summary>        
        public string sup_inn { get; set; }

        /// <summary>
        /// Цена оптовая с НДС
        /// </summary>        
        public decimal? prc_opt_nds { get; set; }

        /// <summary>
        /// Максимальная розничная цена для ЖНВЛС и ДП
        /// </summary>        
        public decimal? prc_gnvls_max { get; set; }
        
        /// <summary>
        /// Признак активной или удаленной строки (0 - активная, 1 - удаленная)
        /// </summary>        
        public int? status { get; set; }

        /// <summary>
        /// Срок годности (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        public string exp { get; set; } //dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Дата поставки товара (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        public string sup_date { get; set; } //dd.MM.yyyy HH:mm:ss

        /// <summary>
        /// Дата прайс-листа (dd.MM.yyyy HH:mm:ss)
        /// </summary>        
        public string prcl_date { get; set; } //dd.MM.yyyy HH:mm:ss
    }
}