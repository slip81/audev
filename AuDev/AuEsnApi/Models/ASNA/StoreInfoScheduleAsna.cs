﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Расписание работы аптеки
    /// </summary>
    public class StoreInfoScheduleAsna
    {
        public StoreInfoScheduleAsna()
        {
            //asna_store_info_schedule
        }

        /// <summary>
        /// Код расписания (PK)
        /// </summary>
        [JsonIgnore()]
        public int schedule_id { get; set; }

        /// <summary>
        /// Код информации об аптеке
        /// </summary>
        [JsonIgnore()]
        public int info_id { get; set; }

        /// <summary>
        /// Порядковый номер дня недели, начиная с 1(понедельник)
        /// </summary>
        [JsonProperty("weekDayId")]
        public int asna_week_day_id { get; set; }

        /// <summary>
        /// Время открытия аптеки
        /// </summary>
        [JsonProperty("startTime")]
        public string asna_start_time { get; set; }

        /// <summary>
        /// Время закрытия аптеки
        /// </summary>
        [JsonProperty("endTime")]
        public string asna_end_time { get; set; }

        /// <summary>
        /// Признак рабочего дня
        /// </summary>
        [JsonProperty("work")]
        public bool asna_work { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [JsonIgnore()]
        public Nullable<System.DateTime> crt_date { get; set; }

        /// <summary>
        /// Кто создал
        /// </summary>
        [JsonIgnore()]
        public string crt_user { get; set; }

        /// <summary>
        /// Дата изменения
        /// </summary>
        [JsonIgnore()]
        public Nullable<System.DateTime> upd_date { get; set; }

        /// <summary>
        /// Кто изменил
        /// </summary>
        [JsonIgnore()]
        public string upd_user { get; set; }

        /// <summary>
        /// Признак удаленной строки
        /// </summary>
        [JsonIgnore()]
        public bool is_deleted { get; set; }

        /// <summary>
        /// Дата удаления
        /// </summary>
        [JsonIgnore()]
        public Nullable<System.DateTime> del_date { get; set; }

        /// <summary>
        /// Кто удалил
        /// </summary>
        [JsonIgnore()]
        public string del_user { get; set; }

    }
}