﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AuDev.Common.Util;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Костанты
    /// </summary>
    public static class AuEsnConst
    {
        /// <summary>
        /// Код услуги ЕСН 2.0
        /// </summary>
        public const int license_id_UND = 64;

        /// <summary>
        /// Код услуги Сводное наличие
        /// </summary>
        public const int license_id_STOCK = 65;

        /// <summary>
        /// Код услуги АСНА 2.0
        /// </summary>
        public const int license_id_ASNA = 66;

        /// <summary>
        /// Максимальное кол-во попыток выполнения задания на сервере
        /// </summary>
        public const int ASNA_handling_server_max_cnt = 2;

        /// <summary>
        /// Максимальное кол-во попыток выполнения задания на сервере (для полных остатков)
        /// </summary>
        public const int ASNA_handling_server_max_cnt2 = 4;

        /// <summary>
        /// Признак: расширенные сообщения в логе
        /// </summary>
        public const bool ASNA_extended_messages = true;

        /// <summary>
        /// Код метода авторизации в АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_Auth = -1;

        /// <summary>
        /// Код метода получения информации об аптеке в АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_GetStoreInfo = 1;

        /// <summary>
        /// Код метода получения справочника связок в АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_GetGoodsLinks = 2;

        /// <summary>
        /// Код метода отправки остатков в АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_SendRemains = 3;

        /// <summary>
        /// Код метода удаления остатков в АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_DelRemains = 4;

        /// <summary>
        /// Код метода получения остатков из АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_GetRemains = 5;

        /// <summary>
        /// Код метода получения заказов из АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_GetOrders = 6;

        /// <summary>
        /// Код метода отправки заказов в АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_SendOrders = 7;

        /// <summary>
        /// Код метода отправки сводных прайс-листов в АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_SendConsPrice = 8;

        /// <summary>
        /// Код метода получения сводных прайс-листов из АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_GetConsPrice = 9;

        /// <summary>
        /// Код метода получения статусов обработки заданий из АСНА WebAPI
        /// </summary>
        public const int ASNA_service_id_GetTaskState = 10;
    }
}