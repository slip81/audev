﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Результат редактирования задания
    /// </summary>
    public class UpdateTaskResult : AuEsnBaseClass
    {
        public UpdateTaskResult()
        {
            //
        }

        /// <summary>
        /// Код задания
        /// </summary>
        public int task_id { get; set; }

        /// <summary>
        /// Код статуса задания
        /// </summary>
        public int task_state_id { get; set; }

        /// <summary>
        /// Код типа запроса задания
        /// </summary>
        public int request_type_id { get; set; }

        /// <summary>
        /// Код РМ
        /// </summary>
        public int? workplace_id { get; set; }

        /// <summary>
        /// Количество попыток выполнения на сервере
        /// </summary>
        public int? handling_server_cnt { get; set; }
    }

}