﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Тип запроса
    /// </summary>
    public class RequestType
    {
        public RequestType()
        {
            //wa_request_type
        }

        /// <summary>
        /// Код типа запроса
        /// </summary>
        public int request_type_id { get; set; }

        /// <summary>
        /// Наименование типа запроса
        /// </summary>
        public string request_type_name { get; set; }
    }
}