﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры получения списка типов запросов для выполнения заданий
    /// </summary>
    public class GetRequestTypesServiceParam : UserInfo
    {
        public GetRequestTypesServiceParam()
        {
            //
        }

        /// <summary>
        /// Код услуги
        /// </summary>         
        public int service_id { get; set; }
    }
}