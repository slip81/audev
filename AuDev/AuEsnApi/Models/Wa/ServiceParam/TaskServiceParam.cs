﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Models
{
    /// <summary>
    /// Параметры отправки уведомлений о смене статуса задания
    /// </summary>
    public class TaskServiceParam : UserInfo
    {
        public TaskServiceParam()
        {
            //
        }

        /// <summary>
        /// Код услуги
        /// </summary>
        public int service_id { get; set; }

        /// <summary>
        /// Код задания
        /// </summary>
        public int task_id { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string mess { get; set; }
    }
}