﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using AutoMapper;

namespace AuEsnApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // !!!
            //AuEsnApi.Service.AutoMapperCommon.Initialize();
            GlobalConfiguration.Configure(WebApiConfig.Register);            
        }
    }
}
