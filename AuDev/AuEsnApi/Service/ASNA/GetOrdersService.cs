﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
#endregion

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region GetOrders

        public GetOrdersResult GetOrders(GetOrdersServiceParam getOrdersServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getOrders(getOrdersServiceParam);
            }
            catch (Exception ex)
            {
                string user = getOrdersServiceParam != null && !String.IsNullOrWhiteSpace(getOrdersServiceParam.login) ? getOrdersServiceParam.login : "asna";
                Loggly.InsertLog_Asna("Ошибка в GetOrders: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetOrdersResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetOrdersResult getOrders(GetOrdersServiceParam getOrdersServiceParam)
        {
            GetOrdersResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = getOrdersServiceParam != null && !String.IsNullOrWhiteSpace(getOrdersServiceParam.login) ? getOrdersServiceParam.login : "asna";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[GetOrders] " : "";

            var checkLicenseResult = mainService.CheckLicense(getOrdersServiceParam, AuEsnConst.license_id_ASNA);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new GetOrdersResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            if (String.IsNullOrWhiteSpace(getOrdersServiceParam.date_beg))
            {
                errRes = new GetOrdersResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задана дата начала получения изменений"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            DateTime date_beg = DateTime.Now;
            DateTime? date_beg_nullable = getOrdersServiceParam.date_beg.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
            if (!date_beg_nullable.HasValue)
            {
                errRes = new GetOrdersResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный формат даты начала получения изменений: " + getOrdersServiceParam.date_beg + ". Ожидается формат: dd.MM.yyyy HH:mm:ss"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            date_beg = (DateTime)date_beg_nullable;

            workplace workplace = null;
            asna_user asna_user = dbContext.asna_user.Where(ss => ss.workplace_id == workplace_id && !ss.is_deleted).FirstOrDefault();
            if (asna_user == null)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new GetOrdersResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки для рабочего места " + workplace.registration_key),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            if (!asna_user.is_active)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new GetOrdersResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Настройки для рабочего места " + workplace.registration_key + " неактивны"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user.asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new GetOrdersResult()
                {
                    error = getAuthorizedUserResult.error
                };
                return errRes;
            }
            asna_user = getAuthorizedUserResult.asna_user;

            string asna_store_id = asna_user.asna_store_id;
            if (String.IsNullOrWhiteSpace(asna_store_id))
            {
                errRes = new GetOrdersResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код аптеки по АСНА у пользователя с кодом " + asna_user.asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int asna_user_id = asna_user.asna_user_id;
            string auth = asna_user.auth;

            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_GetOrders);
            if (String.IsNullOrWhiteSpace(url))
            {
                errRes = new GetOrdersResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан URL сервиса получения заказов из АСНА WebAPI"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            url = url.Replace("{storeId}", asna_store_id);

            /*
            JsonSerializerSettings dateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local,
            };
            string date_beg_str = JsonConvert.SerializeObject(date_beg, dateFormatSettings);
            */            

            string date_beg_str = date_beg.ToUniversalTime().ToStringStrict("yyyy-MM-ddTHH:mm:ss"); // 2017-09-21T08:00:00
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["since"] = date_beg_str;
            query["orderId"] = null;
            query["excludeOwn"] = "true";
            //query["excludeOwn"] = "false";
            url = url + "?" + query.ToString();

            HttpMethod httpMethod = HttpMethod.Get;

            ReceivedOrdersAsna receivedOrdersAsna = null;
            bool isResponseOk = false;
            HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = httpMethod,
                };

                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");

                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var jsonTask = response.Content.ReadAsAsync<ReceivedOrdersAsna>();
                        jsonTask.Wait();
                        receivedOrdersAsna = jsonTask.Result;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new GetOrdersResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }            

            if (receivedOrdersAsna == null)
                receivedOrdersAsna = new ReceivedOrdersAsna();
            if (receivedOrdersAsna.headers == null)
                receivedOrdersAsna.headers = new List<OrderAsna>();
            if (receivedOrdersAsna.rows == null)
                receivedOrdersAsna.rows = new List<OrderRowAsna>();
            if (receivedOrdersAsna.statuses == null)
                receivedOrdersAsna.statuses = new List<OrderStateAsna>();
            
            string mess2 = JsonConvert.SerializeObject(receivedOrdersAsna);
            if (mess2 == null)
                mess2 = "";
            mess2 += " [url: " + httpMethod.ToString() + " " + url + "]";
            /*
            if (mess2.Length >= 8000)
            {
                mess2 = "Длина строки превышает 8000 [" + mess2.Length.ToString() + "]";
                mess2 += " [url: " + httpMethod.ToString() + " " + url + "]";
            }
            */

            // убираем дубликаты
            receivedOrdersAsna.headers = receivedOrdersAsna.headers.Distinct().ToList();
            receivedOrdersAsna.rows = receivedOrdersAsna.rows.Distinct().ToList();
            receivedOrdersAsna.statuses = receivedOrdersAsna.statuses.Distinct().ToList();

            asna_order asna_order = null;
            asna_order_row asna_order_row = null;
            asna_order_state asna_order_state = null;
            asna_order_row_state asna_order_row_state = null;
            vw_asna_user owner_asna_user = null;            
            vw_asna_user issuer_asna_user = null;
            asna_link_row asna_link_row = null;
            List<vw_asna_user> asna_user_list = new List<vw_asna_user>();
            List<asna_order_state_type> asna_order_state_type_list = null;
            int order_id = 0;
            int row_id = 0;
            int owner_asna_user_id = 0;
            int issuer_asna_user_id = 0;
            bool isNew = true;
            DateTime now = DateTime.Now;
            int cntOrder = 0;
            int cntOrder_new = 0;
            int cntOrder_existing = 0;
            int cntRow = 0;
            int cntRow_new = 0;
            int cntRow_existing = 0;
            int cntRow_missing = 0;
            int cntOrderState = 0;
            int cntOrderState_new = 0;
            int cntOrderState_existing = 0;
            int cntOrderState_missing = 0;
            int cntOrderRowState = 0;
            int cntOrderRowState_new = 0;
            int cntOrderRowState_existing = 0;
            int cntOrderRowState_missing = 0;

            // добавляем из БД в receivedOrdersAsna.headers заказы, по которым есть статусы в receivedOrdersAsna.statuses
            // или строки в receivedOrdersAsna.rows, но которых нет в receivedOrdersAsna.headers
            List<string> missingOrderIdList = new List<string>();
            missingOrderIdList.AddRange(receivedOrdersAsna.rows
                .Where(ss => !receivedOrdersAsna.headers.Where(tt => tt.order_id.Trim().ToLower().Equals(ss.order_id.Trim().ToLower())).Any())
                .Select(ss => ss.order_id).ToList());
            missingOrderIdList.AddRange(receivedOrdersAsna.statuses
                .Where(ss => !receivedOrdersAsna.headers.Where(tt => tt.order_id.Trim().ToLower().Equals(ss.order_id.Trim().ToLower())).Any())
                .Select(ss => ss.order_id).ToList());

            missingOrderIdList = missingOrderIdList.Distinct().ToList();
            if ((missingOrderIdList != null) && (missingOrderIdList.Count > 0))
            {
                foreach (var missingOrderId in missingOrderIdList)
                {
                    asna_order = dbContext.asna_order.Where(ss => ss.asna_user_id == asna_user_id && ss.asna_order_id.Trim().ToLower().Equals(missingOrderId.Trim().ToLower())).FirstOrDefault();
                    if (asna_order == null)
                        continue;
                    receivedOrdersAsna.headers.Add(new OrderAsna()
                        {
                            ae = asna_order.asna_ae,
                            date = asna_order.asna_date,
                            d_card = asna_order.asna_d_card,      
                            issuer_id = asna_user.asna_store_id != null ? asna_user.asna_store_id.Trim().ToLower() : null,
                            m_phone = asna_order.asna_m_phone,
                            name = asna_order.asna_name,
                            num = asna_order.asna_num,
                            order_id = asna_order.asna_order_id,
                            pay_type = asna_order.asna_pay_type,
                            pay_type_id = asna_order.asna_pay_type_id,
                            src = asna_order.asna_src,
                            store_id = asna_user.asna_store_id != null ? asna_user.asna_store_id.Trim().ToLower() : null,
                            ts = asna_order.asna_ts,
                            union_id = asna_order.asna_union_id,                            
                        });
                }
            }

            foreach (var header in receivedOrdersAsna.headers
                .OrderBy(ss => ss.order_id)
                .ThenBy(ss => ss.ts)
                .ThenBy(ss => ss.date)                
                )
            {
                cntOrder = cntOrder + 1;
                now = DateTime.Now;                

                asna_order = dbContext.asna_order.Where(ss => ss.asna_order_id != null && ss.asna_order_id.Trim().ToLower().Equals(header.order_id.Trim().ToLower())).FirstOrDefault();
                isNew = asna_order == null;

                if (isNew)
                {
                    asna_order = new asna_order();
                }

                owner_asna_user = asna_user_list.Where(ss => ss.asna_store_id != null && header.store_id != null && ss.asna_store_id.Trim().ToLower().Equals(header.store_id.Trim().ToLower())).FirstOrDefault();
                if (owner_asna_user == null)
                {
                    //owner_asna_user = dbContext.vw_asna_user.Where(ss => ss.asna_store_id != null && header.store_id != null && ss.asna_store_id.Trim().ToLower().Equals(header.store_id.Trim().ToLower())).FirstOrDefault();
                    if (header.store_id != null)
                        owner_asna_user = dbContext.vw_asna_user.Where(ss => ss.asna_store_id != null && ss.asna_store_id.Trim().ToLower().Equals(header.store_id.Trim().ToLower())).FirstOrDefault();
                    if (owner_asna_user != null)
                        asna_user_list.Add(owner_asna_user);
                }
                owner_asna_user_id = owner_asna_user == null ? asna_user_id : owner_asna_user.asna_user_id;
                asna_order.asna_user_id = owner_asna_user_id;
                if (header.issuer_id != header.store_id)
                {
                    issuer_asna_user = asna_user_list.Where(ss => ss.asna_store_id != null && header.issuer_id != null && ss.asna_store_id.Trim().ToLower().Equals(header.issuer_id.Trim().ToLower())).FirstOrDefault();
                    if (issuer_asna_user == null)
                    {
                        //issuer_asna_user = dbContext.vw_asna_user.Where(ss => ss.asna_store_id != null && header.issuer_id != null && ss.asna_store_id.Trim().ToLower().Equals(header.issuer_id.Trim().ToLower())).FirstOrDefault();
                        if (header.issuer_id != null)
                            issuer_asna_user = dbContext.vw_asna_user.Where(ss => ss.asna_store_id != null && ss.asna_store_id.Trim().ToLower().Equals(header.issuer_id.Trim().ToLower())).FirstOrDefault();
                        if (issuer_asna_user != null)
                            asna_user_list.Add(issuer_asna_user);
                    }
                    issuer_asna_user_id = issuer_asna_user == null ? asna_user_id : issuer_asna_user.asna_user_id;
                }
                else
                {
                    issuer_asna_user_id = owner_asna_user_id;
                }
                asna_order.issuer_asna_user_id = issuer_asna_user_id;

                asna_order.asna_order_id = header.order_id;
                asna_order.asna_src = header.src;
                asna_order.asna_num = header.num;
                asna_order.asna_date = header.date;
                asna_order.asna_name = header.name;
                asna_order.asna_m_phone = header.m_phone;
                asna_order.asna_pay_type = header.pay_type;
                asna_order.asna_pay_type_id = header.pay_type_id;
                asna_order.asna_d_card = header.d_card;
                asna_order.asna_ae = (short)header.ae;
                asna_order.asna_union_id = header.union_id;
                asna_order.asna_ts = header.ts;
                if (isNew)
                {
                    asna_order.crt_date = now;
                    asna_order.crt_user = user;
                }
                asna_order.upd_date = now;
                asna_order.upd_user = user;
                asna_order.is_deleted = false;
                
                if (isNew)
                {
                    dbContext.asna_order.Add(asna_order);
                }

                dbContext.SaveChanges();
                header.db_order_id = asna_order.order_id;
                header.is_new = isNew;
                
                cntOrder_new = isNew ? cntOrder_new + 1 : cntOrder_new;
                cntOrder_existing = !isNew ? cntOrder_existing + 1 : cntOrder_existing;                
            }

            foreach (var row in receivedOrdersAsna.rows
                .OrderBy(ss => ss.order_id)
                .ThenBy(ss => ss.ts)
                )
            {
                cntRow = cntRow + 1;
                now = DateTime.Now;

                asna_order_row = dbContext.asna_order_row.Where(ss => ss.asna_row_id != null && ss.asna_row_id.Trim().ToLower().Equals(row.row_id.Trim().ToLower())).FirstOrDefault();
                isNew = asna_order_row == null;

                if (isNew)
                {
                    asna_order = dbContext.asna_order.Where(ss => ss.asna_order_id != null && ss.asna_order_id.Trim().ToLower().Equals(row.order_id.Trim().ToLower())).FirstOrDefault();
                    if (asna_order == null)
                    {
                        cntRow_missing = cntRow_missing + 1;
                        continue;
                    }
                    order_id = asna_order.order_id;
                    asna_order_row = new asna_order_row();
                }

                asna_link_row = (from t1 in dbContext.asna_link
                                 from t2 in dbContext.asna_link_row
                                 where t1.link_id == t2.link_id
                                 && t1.asna_user_id == asna_user_id
                                 && !t1.is_deleted
                                 && t2.asna_nnt == row.nnt
                                 select t2)
                                .FirstOrDefault();

                order_id = isNew ? order_id : asna_order_row.order_id;

                asna_order_row.order_id = order_id;

                asna_order_row.asna_row_id = row.row_id;
                asna_order_row.asna_order_id = row.order_id;
                asna_order_row.asna_row_type = (short)row.row_type;
                asna_order_row.asna_prt_id = row.prt_id;
                asna_order_row.asna_nnt = row.nnt;
                asna_order_row.asna_sku = asna_link_row == null ? "" : asna_link_row.asna_sku;
                asna_order_row.asna_qnt = row.qnt;
                asna_order_row.asna_qnt_unrsv = row.qnt_unrsv.GetValueOrDefault(0);
                asna_order_row.asna_prc = row.prc;
                asna_order_row.asna_prc_dsc = row.prc_dsc;
                asna_order_row.asna_dsc_union = row.dsc_union;
                asna_order_row.asna_dtn = row.dtn;
                asna_order_row.asna_prc_loyal = row.prc_loyal;
                asna_order_row.asna_prc_opt_nds = row.prc_opt_nds;
                asna_order_row.asna_supp_inn = row.sup_inn;
                asna_order_row.asna_dlv_date = row.dlv_date;
                asna_order_row.asna_qnt_unrsv = row.qnt_unrsv;
                asna_order_row.asna_prc_fix = row.prc_fix;
                asna_order_row.asna_ts = row.ts;

                if (isNew)
                {
                    asna_order_row.crt_date = now;
                    asna_order_row.crt_user = user;
                }
                asna_order_row.upd_date = now;
                asna_order_row.upd_user = user;
                asna_order_row.is_deleted = false;

                if (isNew)
                {
                    dbContext.asna_order_row.Add(asna_order_row);
                }

                dbContext.SaveChanges();
                row.db_order_id = asna_order_row.order_id;
                row.db_row_id = asna_order_row.row_id;
                row.db_sku = asna_order_row.asna_sku;
                row.is_new = isNew;
                
                cntRow_new = isNew ? cntRow_new + 1 : cntRow_new;
                cntRow_existing = !isNew ? cntRow_existing + 1 : cntRow_existing; 
            }

            foreach (var orderStatus in receivedOrdersAsna.statuses.Where(ss => String.IsNullOrWhiteSpace(ss.row_id))
                .OrderBy(ss => ss.order_id)
                .ThenBy(ss => ss.ts)
                .ThenBy(ss => ss.date)
                )
            {
                cntOrderState = cntOrderState + 1;
                now = DateTime.Now;

                asna_order_state = dbContext.asna_order_state.Where(ss => ss.asna_status_id != null && ss.asna_status_id.Trim().ToLower().Equals(orderStatus.status_id.Trim().ToLower())).FirstOrDefault();
                isNew = asna_order_state == null;

                if (isNew)
                {
                    asna_order = dbContext.asna_order.Where(ss => ss.asna_order_id != null && ss.asna_order_id.Trim().ToLower().Equals(orderStatus.order_id.Trim().ToLower())).FirstOrDefault();
                    if (asna_order == null)
                    {
                        cntOrderState_missing = cntOrderState_missing + 1;
                        continue;
                    }
                    order_id = asna_order.order_id;
                    asna_order_state = new asna_order_state();
                }

                order_id = isNew ? order_id : asna_order_state.order_id;

                asna_order_state.order_id = order_id;
                asna_order_state.state_type_id = orderStatus.status;
                asna_order_state.asna_status_id = orderStatus.status_id;
                asna_order_state.asna_order_id = orderStatus.order_id;
                asna_order_state.asna_date = orderStatus.date;
                asna_order_state.asna_rc_date = orderStatus.rc_date;
                asna_order_state.asna_cmnt = orderStatus.cmnt;
                asna_order_state.asna_ts = orderStatus.ts;

                if (isNew)
                {
                    asna_order_state.crt_date = now;
                    asna_order_state.crt_user = user;
                }
                asna_order_state.upd_date = now;
                asna_order_state.upd_user = user;
                asna_order_state.is_deleted = false;

                if (isNew)
                {
                    dbContext.asna_order_state.Add(asna_order_state);
                }

                dbContext.SaveChanges();
                orderStatus.db_order_id = asna_order_state.order_id;
                orderStatus.db_state_id = asna_order_state.order_state_id;
                orderStatus.is_new = isNew;

                cntOrderState_new = isNew ? cntOrderState_new + 1 : cntOrderState_new;
                cntOrderState_existing = !isNew ? cntOrderState_existing + 1 : cntOrderState_existing; 
            }


            foreach (var rowStatus in receivedOrdersAsna.statuses.Where(ss => !String.IsNullOrWhiteSpace(ss.row_id))
                .OrderBy(ss => ss.order_id)
                .ThenBy(ss => ss.row_id)
                .ThenBy(ss => ss.ts)
                .ThenBy(ss => ss.date)
                )
            {
                cntOrderRowState = cntOrderRowState + 1;
                now = DateTime.Now;

                asna_order_row_state = dbContext.asna_order_row_state.Where(ss => ss.asna_status_id != null && ss.asna_status_id.Trim().ToLower().Equals(rowStatus.status_id.Trim().ToLower())).FirstOrDefault();
                isNew = asna_order_row_state == null;

                asna_order_row = dbContext.asna_order_row.Where(ss => ss.asna_row_id != null && ss.asna_row_id.Trim().ToLower().Equals(rowStatus.row_id.Trim().ToLower())).FirstOrDefault();
                if (isNew)
                {                    
                    if (asna_order_row == null)
                    {
                        cntOrderRowState_missing = cntOrderRowState_missing + 1;
                        continue;
                    }                    
                    row_id = asna_order_row.row_id;
                    asna_order_row_state = new asna_order_row_state();
                }
                
                row_id = isNew ? row_id : asna_order_row_state.row_id;                

                asna_order_row_state.row_id = row_id;
                asna_order_row_state.state_type_id = rowStatus.status;
                asna_order_row_state.asna_status_id = rowStatus.status_id;
                asna_order_row_state.asna_order_id = rowStatus.order_id;
                asna_order_row_state.asna_date = rowStatus.date;
                asna_order_row_state.asna_rc_date = rowStatus.rc_date;
                asna_order_row_state.asna_cmnt = rowStatus.cmnt;
                asna_order_row_state.asna_ts = rowStatus.ts;

                if (isNew)
                {
                    asna_order_row_state.crt_date = now;
                    asna_order_row_state.crt_user = user;
                }
                asna_order_row_state.upd_date = now;
                asna_order_row_state.upd_user = user;
                asna_order_row_state.is_deleted = false;

                if (isNew)
                {
                    dbContext.asna_order_row_state.Add(asna_order_row_state);
                }

                dbContext.SaveChanges();
                rowStatus.db_order_id = asna_order_row.order_id;
                rowStatus.db_row_id = asna_order_row_state.row_id;
                rowStatus.db_state_id = asna_order_row_state.row_state_id;
                rowStatus.is_new = isNew;

                cntOrderRowState_new = isNew ? cntOrderRowState_new + 1 : cntOrderRowState_new;
                cntOrderRowState_existing = !isNew ? cntOrderRowState_existing + 1 : cntOrderRowState_existing;
            }

            bool needSaveChanges = false;
            foreach (var header in receivedOrdersAsna.headers
                    .Where(ss => ss.db_order_id.GetValueOrDefault(0) > 0)
                    .OrderBy(ss => ss.order_id)
                    .ThenBy(ss => ss.ts)
                    .ThenBy(ss => ss.date)
                    )
            {
                asna_order = dbContext.asna_order.Where(ss => ss.order_id == header.db_order_id).FirstOrDefault();
                if (asna_order == null)
                    continue;
                asna_order_state = dbContext.asna_order_state.Where(ss => ss.order_id == asna_order.order_id).OrderByDescending(ss => ss.asna_ts).FirstOrDefault();
                if (asna_order_state == null)
                    continue;
                asna_order.state_type_id = asna_order_state.state_type_id;
                needSaveChanges = true;
            }
            if (needSaveChanges)
                dbContext.SaveChanges();

            needSaveChanges = false;
            foreach (var row in receivedOrdersAsna.rows
                .Where(ss => ss.db_row_id.GetValueOrDefault(0) > 0)
                .OrderBy(ss => ss.order_id)
                .ThenBy(ss => ss.ts)                
                )
            {
                asna_order_row = dbContext.asna_order_row.Where(ss => ss.row_id == row.db_row_id).FirstOrDefault();
                if (asna_order_row == null)
                    continue;
                asna_order_row_state = dbContext.asna_order_row_state.Where(ss => ss.row_id == asna_order_row.row_id).OrderByDescending(ss => ss.asna_ts).FirstOrDefault();
                if (asna_order_row_state == null)
                    continue;
                asna_order_row.state_type_id = asna_order_row_state.state_type_id;
                needSaveChanges = true;
            }
            if (needSaveChanges)
                dbContext.SaveChanges();

            asna_order_state_type_list = dbContext.asna_order_state_type.ToList();

            string logMess = "Получено заказов: " + cntOrder.ToString() + " [новых " + cntOrder_new.ToString() + ", существующих " + cntOrder_existing.ToString() + "],"
                + " получено строк заказов: " + cntRow.ToString() + " [новых " + cntRow_new.ToString() + ", существующих " + cntRow_existing.ToString() + ", не найденных: " + cntRow_missing.ToString() + "],"
                + " получено статусов заказов: " + cntOrderState.ToString() + " [новых " + cntOrderState_new.ToString() + ", существующих " + cntOrderState_existing.ToString() + ", не найденных: " + cntOrderState_missing.ToString() + "],"
                + " получено статусов строк: " + cntOrderRowState.ToString() + " [новых " + cntOrderRowState_new.ToString() + ", существующих " + cntOrderRowState_existing.ToString() + ", не найденных: " + cntOrderRowState_missing.ToString() + "]"
                ;            
            Loggly.InsertLog_Asna(logMess, (long)Enums.LogEsnType.ASNA_GET_ORDERS, user, null, now_initial, mess2);

            return new GetOrdersResult()
            {
                orders = receivedOrdersAsna.headers
                    .Where(ss => ss.db_order_id.GetValueOrDefault(0) > 0)
                    .Select(ss => new Order()
                    {
                        order_id = (int)ss.db_order_id,
                        store_id = ss.store_id,
                        issuer_store_id = ss.issuer_id,
                        sales_name = asna_user_list.Where(tt => tt.asna_store_id.Trim().ToLower().Equals(ss.store_id.Trim().ToLower())).Select(tt => tt.sales_name).FirstOrDefault(),
                        issuer_sales_name = asna_user_list.Where(tt => tt.asna_store_id.Trim().ToLower().Equals(ss.issuer_id.Trim().ToLower())).Select(tt => tt.sales_name).FirstOrDefault(),
                        asna_order_id = ss.order_id,
                        src = ss.src,
                        num = ss.num,
                        date = ss.date.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                        name = ss.name,
                        m_phone = ss.m_phone,
                        pay_type = ss.pay_type,
                        pay_type_id = ss.pay_type_id,
                        d_card = ss.d_card,
                        ae = ss.ae,
                        union_id = ss.union_id,
                        ts = ss.ts.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                    })
                    .OrderBy(ss => ss.order_id)
                    .ThenBy(ss => ss.ts)
                    .ThenBy(ss => ss.date)
                    .ToList(),

                rows = receivedOrdersAsna.rows
                    .Where(ss => ss.db_row_id.GetValueOrDefault(0) > 0)
                    .Select(ss => new OrderRow()
                    {
                        row_id = (int)ss.db_row_id,
                        order_id = (int)ss.db_order_id,
                        asna_row_id = ss.row_id,
                        asna_order_id = ss.order_id,
                        row_type = ss.row_type,
                        prt_id = ss.prt_id,
                        nnt = ss.nnt,
                        // !!!
                        sku = ss.db_sku,
                        qnt = ss.qnt,
                        prc = ss.prc,
                        prc_dsc = ss.prc_dsc,
                        dsc_union = ss.dsc_union,
                        dtn = ss.dtn,
                        prc_loyal = ss.prc_loyal,
                        prc_opt_nds = ss.prc_opt_nds,
                        sup_inn = ss.sup_inn,
                        dlv_date = ss.dlv_date.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                        qnt_unrsv = ss.qnt_unrsv,
                        prc_fix = ss.prc_fix,
                        ts = ss.ts.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                    })
                    .OrderBy(ss => ss.order_id)
                    .OrderBy(ss => ss.row_id)
                    .ThenBy(ss => ss.ts)                    
                    .ToList(),
                

                orders_states = receivedOrdersAsna.statuses                
                    .Where(ss => String.IsNullOrWhiteSpace(ss.row_id) && ss.db_state_id.GetValueOrDefault(0) > 0)
                    .Select(ss => new OrderState()
                    {
                        status_id = (int)ss.db_state_id,
                        order_id = (int)ss.db_order_id,
                        row_id = null,
                        asna_status_id = ss.status_id,                        
                        asna_row_id = ss.row_id,
                        asna_order_id = ss.order_id,
                        date = ss.date.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                        status = ss.status,
                        status_name = asna_order_state_type_list.Where(tt => tt.state_type_id == ss.status).Select(tt => tt.state_type_name).FirstOrDefault(),
                        rc_date = ss.rc_date.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                        cmnt = ss.cmnt,
                        ts = ss.ts.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                    })
                    .OrderBy(ss => ss.order_id)                    
                    .ThenBy(ss => ss.ts)
                    .ThenBy(ss => ss.date)
                    .ToList(),

                rows_states = receivedOrdersAsna.statuses
                    .Where(ss => !String.IsNullOrWhiteSpace(ss.row_id) && ss.db_state_id.GetValueOrDefault(0) > 0)
                    .Select(ss => new OrderState()
                    {
                        status_id = (int)ss.db_state_id,
                        order_id = (int)ss.db_order_id,
                        row_id = (int)ss.db_row_id,
                        asna_status_id = ss.status_id,
                        asna_row_id = ss.row_id,
                        asna_order_id = ss.order_id,
                        date = ss.date.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                        status = ss.status,
                        status_name = asna_order_state_type_list.Where(tt => tt.state_type_id == ss.status).Select(tt => tt.state_type_name).FirstOrDefault(),
                        rc_date = ss.rc_date.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                        cmnt = ss.cmnt,
                        ts = ss.ts.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                    })
                    .OrderBy(ss => ss.order_id)
                    .ThenBy(ss => ss.row_id)
                    .ThenBy(ss => ss.ts)
                    .ThenBy(ss => ss.date)
                    .ToList(),
            };
        }
        
        #endregion
    }
}
