﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net.Http;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Net.Http.Headers;
using System.Transactions;
using System.Net;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region GetGoodLinks

        public AuEsnBaseResult GetGoodsLinks(AsnaServiceParam asnaServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getGoodsLinks(asnaServiceParam);
            }
            catch (Exception ex)
            {
                string user = "asnabot";
                Loggly.InsertLog_Asna("Ошибка в GetGoodsLinks: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult getGoodsLinks(AsnaServiceParam asnaServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[GetGoodsLinks] " : "";

            if (asnaServiceParam == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int asna_user_id = asnaServiceParam.asna_user_id;

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = getAuthorizedUserResult.error
                };                
                return errRes;
            }

            asna_user asna_user = getAuthorizedUserResult.asna_user;
            if (asna_user == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (!asna_user.asna_rr_id.HasValue)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не указан идентификатор справочника связок для пользователя " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            DateTime allowed_last_link_date = DateTime.Now.AddDays(-1);
            asna_link asna_link_current = dbContext.asna_link.Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted).FirstOrDefault();
            if ((asna_link_current != null) && (asna_link_current.crt_date > allowed_last_link_date))
            {
                DateTime asna_link_current_crt_date = (DateTime)asna_link_current.crt_date;
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Следующий запрос на получение справочника связок возможен не раньше " + asna_link_current_crt_date.AddDays(1).ToString("dd.MM.yyyy HH:mm:ss")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            string auth = asna_user.auth;
            List<GoodLink> links = null;

            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_GetGoodsLinks);
            var query = HttpUtility.ParseQueryString(string.Empty);
            query["rrId"] = asna_user.asna_rr_id.ToString();
            query["names"] = "true";
            url = url + "?" + query.ToString();

            bool isResponseOk = false;
            HttpStatusCode responseStatusCode = HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get,
                };

                request.Headers.Add("Cache-Control", "no-cache");

                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");

                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var jsonTask = response.Content.ReadAsAsync<List<GoodLink>>();
                        jsonTask.Wait();
                        links = jsonTask.Result;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if ((links == null) || (links.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "От источника получены пустые данные"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            
            var delResult = dbContext.Database.ExecuteSqlCommand("select und.del_asna_link(@p0);", asna_user_id);
            
            asna_link asna_link = new asna_link();
            asna_link.asna_user_id = asna_user_id;
            asna_link.crt_date = DateTime.Now;
            asna_link.crt_user = user;
            asna_link.upd_date = asna_link.crt_date;
            asna_link.upd_user = user;
            asna_link.is_deleted = false;
            dbContext.asna_link.Add(asna_link);
            dbContext.SaveChanges();
            int link_id = asna_link.link_id;            

            asna_link_row asna_link_row = null;

            int cnt = 0;
            AuMainDb transactionContext = null;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
            {
                transactionContext = null;
                try
                {
                    transactionContext = new AuMainDb();
                    transactionContext.Configuration.AutoDetectChangesEnabled = false;

                    cnt = 0;
                    
                    foreach (var link in links)
                    {
                        ++cnt;

                        asna_link_row = new asna_link_row();
                        asna_link_row.link_id = link_id;
                        asna_link_row.asna_nnt = link.asna_nnt;
                        asna_link_row.asna_sku = link.asna_sku;
                        asna_link_row.asna_name = link.asna_name;

                        asna_link_row.crt_date = DateTime.Now;
                        asna_link_row.crt_user = user;
                        asna_link_row.upd_date = asna_link_row.crt_date;
                        asna_link_row.upd_user = user;
                        asna_link_row.is_deleted = false;

                        transactionContext = AddToContext<asna_link_row>(transactionContext, asna_link_row, cnt, 100, true);
                    }

                    transactionContext.SaveChanges();

                }
                finally
                {
                    if (transactionContext != null)
                        transactionContext.Dispose();
                }

                scope.Complete();

            }

            dbContext.SaveChanges();

            var updateResult = dbContext.Database.ExecuteSqlCommand("select und.update_asna_link(@p0);", link_id);

            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}