﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net.Http;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Net.Http.Headers;
using System.Transactions;
using System.Net;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region GetTaskStateFromAsna

        public GetTaskStateResult GetTaskStateFromAsna(GetTaskStateFromAsnaServiceParam getTaskStateFromAsnaServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getTaskStateFromAsna(getTaskStateFromAsnaServiceParam);
            }
            catch (Exception ex)
            {
                string user = "asnabot";
                Loggly.InsertLog_Asna("Ошибка в GetTaskStateFromAsna: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetTaskStateResult() { task_state = GlobalUtil.ExceptionInfo(ex) };
            }
        }

        private GetTaskStateResult getTaskStateFromAsna(GetTaskStateFromAsnaServiceParam getTaskStateFromAsnaServiceParam)
        {
            GetTaskStateResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[GetTaskStateFromAsna] " : "";

            if (getTaskStateFromAsnaServiceParam == null)
            {
                errRes = new GetTaskStateResult()
                {
                    task_state = "Не заданы входные параметры",
                };
                //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int asna_user_id = getTaskStateFromAsnaServiceParam.asna_user_id;
            int task_id = getTaskStateFromAsnaServiceParam.task_id; 
            
            string message_id = "";

            wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
            if (wa_task == null)
            {
                errRes = new GetTaskStateResult()
                {
                     task_state = "Не найдено задание " + task_id.ToString(),
                };
                //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }                

            switch ((Enums.WaRequestTypeEnum)wa_task.request_type_id)
            {
                case Enums.WaRequestTypeEnum.ASNA_SendStocksChanges:
                case Enums.WaRequestTypeEnum.ASNA_SendStocksFull:
                    wa_task_param wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == task_id).FirstOrDefault();
                    if (wa_task_param == null)
                    {
                        errRes = new GetTaskStateResult()
                        {
                             task_state = "Нет данных о статусе задания " + task_id.ToString(),
                        };
                        //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                        return errRes;
                    }
                        
                    SendRemainsToAsnaServiceParam sendRemainsToAsnaServiceParam = JsonConvert.DeserializeObject<SendRemainsToAsnaServiceParam>(wa_task_param.task_param);
                    if (sendRemainsToAsnaServiceParam == null)
                    {
                        errRes = new GetTaskStateResult()
                        {
                             task_state = "Нет данных о статусе задания " + task_id.ToString(),
                        };
                        //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                        return errRes;
                    }

                    int chain_id = sendRemainsToAsnaServiceParam.chain_id;
                    asna_stock_batch_out asna_stock_batch_out = dbContext.asna_stock_batch_out.Where(ss => ss.in_chain_id == chain_id).FirstOrDefault();
                    if (asna_stock_batch_out == null)
                    {
                        errRes = new GetTaskStateResult()
                        {
                             task_state = "Нет данных о статусе задания " + task_id.ToString(),
                        };
                        //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                        return errRes;
                    }
                    if (String.IsNullOrWhiteSpace(asna_stock_batch_out.mess))
                    {
                        errRes = new GetTaskStateResult()
                        {
                             task_state = "Выполнено сразу",
                        };
                        //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                        return errRes;
                    }

                    message_id = asna_stock_batch_out.mess;

                    break;
                default:
                        errRes = new GetTaskStateResult()
                        {
                             task_state = "Операция не поддерживается для задания " + task_id.ToString(),
                        };
                        //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                        return errRes;                    
            }

            if (String.IsNullOrWhiteSpace(message_id))
            {
                errRes = new GetTaskStateResult()
                {
                    task_state = "Операция не поддерживается для задания " + task_id.ToString(),
                };
                //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;  
            }

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new GetTaskStateResult()
                {
                    task_state = getAuthorizedUserResult.error.message
                };
                return errRes;
            }

            asna_user asna_user = getAuthorizedUserResult.asna_user;
            if (asna_user == null)
            {
                errRes = new GetTaskStateResult()
                {
                    task_state = "Не найдены настройки с кодом " + asna_user_id.ToString(),
                };
                //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }            

            string auth = asna_user.auth;

            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_GetTaskState);
            if (String.IsNullOrWhiteSpace(url))
            {
                errRes = new GetTaskStateResult()
                {
                    task_state = "Не найден адрес сервиса проверки статуса задания в АСНА",
                };
                //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            //url = url.Replace("{messageId}", message_id);
            url = message_id;

            string taskState = null;
            bool isResponseOk = false;
            HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get,
                };
                
                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var jsonTask = response.Content.ReadAsAsync<ReceivedTaskStateAsna>();
                        jsonTask.Wait();
                        var res = jsonTask.Result;
                        taskState = res == null ? "Не найдено" : res.asna_state_type_name;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new GetTaskStateResult()
                {
                     task_state = "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : ""),
                };
                //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (taskState == null)
            {
                errRes = new GetTaskStateResult()
                {
                     task_state = "От источника получены пустые данные",
                };
                //Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            // !!!
            // todo: update .. set mess = null

            return new GetTaskStateResult() 
            { 
                task_state = taskState,
                //error = null, 
            };
        }
        
        #endregion
    }
}