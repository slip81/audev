﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net.Http;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Net.Http.Headers;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region GetStoreInfoAsna

        public AuEsnBaseResult GetStoreInfoAsna(AsnaServiceParam asnaServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getStoreInfoAsna(asnaServiceParam);
            }
            catch (Exception ex)
            {
                string user = "asnabot";
                Loggly.InsertLog_Asna("Ошибка в GetStoreInfoAsna: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult getStoreInfoAsna(AsnaServiceParam asnaServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[GetStoreInfoAsna] " : "";

            if (asnaServiceParam == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int asna_user_id = asnaServiceParam.asna_user_id;

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = getAuthorizedUserResult.error
                };
                return errRes;
            }

            asna_user asna_user = getAuthorizedUserResult.asna_user;
            if (asna_user == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            string auth = asna_user.auth;
            StoreInfoAsna storeInfo = null;
            bool isResponseOk = false;
            System.Net.HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_GetStoreInfo);
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get,
                };

                request.Headers.Add("Cache-Control", "no-cache");

                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");
  
                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {                        
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var jsonTask = response.Content.ReadAsAsync<StoreInfoAsna>();
                        jsonTask.Wait();
                        storeInfo = jsonTask.Result;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (storeInfo == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "От источника получены пустые данные"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            asna_user.asna_legal_id = storeInfo.asna_legal_id;

            asna_store_info_schedule asna_store_info_schedule = null;
            asna_store_info asna_store_info = dbContext.asna_store_info.Where(ss => ss.asna_user_id == asna_user_id).FirstOrDefault();
            if (asna_store_info != null)
            {
                asna_store_info.asna_name = storeInfo.asna_name;
                asna_store_info.asna_full_name = storeInfo.asna_full_name;
                asna_store_info.asna_inn = storeInfo.asna_inn;
                asna_store_info.asna_rt_terminal = storeInfo.asna_rt_terminal;
                asna_store_info.asna_rt_mobile = storeInfo.asna_rt_mobile;
                asna_store_info.asna_rt_site = storeInfo.asna_rt_site;
                asna_store_info.asna_edit_check = storeInfo.asna_edit_check;
                asna_store_info.asna_cancel_order = storeInfo.asna_cancel_order;
                asna_store_info.asna_full_time = storeInfo.asna_full_time;

                asna_store_info.upd_date = DateTime.Now;
                asna_store_info.upd_user = user;

                dbContext.asna_store_info_schedule.RemoveRange(dbContext.asna_store_info_schedule.Where(ss => ss.info_id == asna_store_info.info_id));
            }
            else
            {
                asna_store_info = new asna_store_info();
                asna_store_info.asna_user_id = asna_user_id;
                asna_store_info.asna_name = storeInfo.asna_name;
                asna_store_info.asna_full_name = storeInfo.asna_full_name;
                asna_store_info.asna_inn = storeInfo.asna_inn;
                asna_store_info.asna_rt_terminal = storeInfo.asna_rt_terminal;
                asna_store_info.asna_rt_mobile = storeInfo.asna_rt_mobile;
                asna_store_info.asna_rt_site = storeInfo.asna_rt_site;
                asna_store_info.asna_edit_check = storeInfo.asna_edit_check;
                asna_store_info.asna_cancel_order = storeInfo.asna_cancel_order;
                asna_store_info.asna_full_time = storeInfo.asna_full_time;

                asna_store_info.crt_date = DateTime.Now;
                asna_store_info.crt_user = user;
                asna_store_info.upd_date = asna_store_info.crt_date;
                asna_store_info.upd_user = user;
                asna_store_info.is_deleted = false;
                
                dbContext.asna_store_info.Add(asna_store_info);
            }

            if ((storeInfo.schedule != null) && (storeInfo.schedule.Count > 0))
            {
                foreach (var scheduleItem in storeInfo.schedule)
                {
                    asna_store_info_schedule = new asna_store_info_schedule();
                    asna_store_info_schedule.asna_store_info = asna_store_info;
                    asna_store_info_schedule.asna_week_day_id = scheduleItem.asna_week_day_id;
                    asna_store_info_schedule.asna_start_time = scheduleItem.asna_start_time;
                    asna_store_info_schedule.asna_end_time = scheduleItem.asna_end_time;
                    asna_store_info_schedule.asna_work = scheduleItem.asna_work;

                    asna_store_info_schedule.crt_date = DateTime.Now;
                    asna_store_info_schedule.crt_user = user;
                    asna_store_info_schedule.upd_date = asna_store_info_schedule.crt_date;
                    asna_store_info_schedule.upd_user = user;
                    asna_store_info_schedule.is_deleted = false;
                    dbContext.asna_store_info_schedule.Add(asna_store_info_schedule);
                }
            }

            dbContext.SaveChanges();

            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}