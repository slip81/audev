﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
#endregion

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region SendRemainsToAsna

        public AuEsnBaseResult SendRemainsToAsna(SendRemainsToAsnaServiceParam sendRemainsToAsnaServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendRemainsToAsna(sendRemainsToAsnaServiceParam);
            }
            catch (Exception ex)
            {
                string user = "asnabot";
                Loggly.InsertLog_Asna("Ошибка в SendRemainsToAsna: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                //return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
                return new AuEsnBaseResult() { result = 1, mess = GlobalUtil.ExceptionInfo(ex), };
            }
        }

        private AuEsnBaseResult sendRemainsToAsna(SendRemainsToAsnaServiceParam sendRemainsToAsnaServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[SendRemainsToAsna] " : "";

            if (sendRemainsToAsnaServiceParam == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }

            // !!!
            /*
            errRes = new AuEsnBaseResult()
            {
                error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Метод временно отключен"),
            };
            Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
            return errRes;
            */

            int chain_id = sendRemainsToAsnaServiceParam.chain_id;
            int asna_user_id = sendRemainsToAsnaServiceParam.asna_user_id;

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = getAuthorizedUserResult.error
                };
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = getAuthorizedUserResult.error.message, };
            }

            asna_user asna_user = getAuthorizedUserResult.asna_user;
            if (asna_user == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }

            string asna_store_id = asna_user.asna_store_id;
            if (String.IsNullOrWhiteSpace(asna_store_id))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код аптеки по АСНА у пользователя с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }

            DateTime allowed_last_link_date = DateTime.Now.AddDays(-1);
            asna_link asna_link = dbContext.asna_link.Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted).OrderByDescending(ss => ss.link_id).FirstOrDefault();
            if ((asna_link == null) || (asna_link.crt_date <= allowed_last_link_date))
            {                
                // пытаемся обновить справочник связок
                AsnaServiceParam asnaServiceParam = new AsnaServiceParam() { asna_user_id = asna_user_id };
                var getGoodsLinksResult = GetGoodsLinks(asnaServiceParam);
                if ((getGoodsLinksResult.error != null) && (getGoodsLinksResult.error.id == (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION))
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = getGoodsLinksResult.error
                    };
                    //return errRes;
                    return new AuEsnBaseResult() { result = 1, mess = getGoodsLinksResult.error.message, };
                }
                /*
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден справочник связок для пользователя с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                */
            }

            asna_stock_chain asna_stock_chain = dbContext.asna_stock_chain.Where(ss => ss.asna_user_id == asna_user_id && ss.chain_id == chain_id && !ss.is_deleted).FirstOrDefault();
            if (asna_stock_chain == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена цепочка с кодом " + chain_id.ToString() + " для пользователя с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }

            if (!asna_stock_chain.stock_date.HasValue)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для пользователя " + asna_user_id.ToString() + " по цепочке с кодом " + chain_id.ToString() + " не указана дата выгрузки остатков"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }

            asna_stock_batch_out asna_stock_batch_out_existing = dbContext.asna_stock_batch_out.Where(ss => ss.asna_user_id == asna_user_id && ss.in_chain_id == chain_id && !ss.is_deleted).FirstOrDefault();
            if (asna_stock_batch_out_existing != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для пользователя " + asna_user_id.ToString() + " по цепочке с кодом " + chain_id.ToString() + " уже есть отправка в АСНА с кодом " + asna_stock_batch_out_existing.batch_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }

            List<vw_asna_stock_row_linked> vw_asna_stock_row_linked_list = dbContext.vw_asna_stock_row_linked
                .Where(ss => ss.chain_id == chain_id && ss.is_unique)
                .ToList();
            if ((vw_asna_stock_row_linked_list == null) || (vw_asna_stock_row_linked_list.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены связанные остатки для пользователя с кодом " + asna_user_id.ToString() + " по цепочке с кодом " + chain_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }

            asna_stock_batch_out asna_stock_batch_out_last = dbContext.asna_stock_batch_out
                .Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted)
                .OrderByDescending(ss => ss.batch_num)
                .FirstOrDefault();

            int asna_stock_batch_out_batch_id = 0;

            string mess2 = null;
            string asnaRemainsParamStr = null;
            int row_cnt = 0;
            bool is_full = asna_stock_chain.is_full;
            
            List<RemainsAsna> remainsList = vw_asna_stock_row_linked_list.Select(ss => new RemainsAsna()
            {
                firm_name = ss.asna_man,
                prt_id = ss.asna_prt_id,
                sku = ss.asna_sku,
                nnt = ss.asna_nnt,
                all_cnt = ss.asna_qnt,
                supplier_doc_num = ss.asna_doc,
                supplier_inn = ss.asna_sup_inn,
                //supplier_doc_date = ss.asna_sup_date.HasValue ? ((DateTime)ss.asna_sup_date).ToString("o") : null,
                supplier_doc_date = ss.asna_sup_date.HasValue ? ((DateTime)ss.asna_sup_date).ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'") : null,                
                percent_nds = ss.asna_nds,
                is_vital = ss.asna_gnvls,
                is_child_food = ss.asna_dp,
                series = ss.asna_series,
                //valid_date = ss.asna_exp.HasValue ? ((DateTime)ss.asna_exp).ToString("o") : null,
                valid_date = ss.asna_exp.HasValue ? ((DateTime)ss.asna_exp).ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'") : null,
                price_firm = ss.asna_prc_man,
                price_gross = ss.asna_prc_opt,
                price_nds_gross = ss.asna_prc_opt_nds,
                price_retail = ss.asna_prc_ret,
                price_gr = ss.asna_prc_reestr,
                price_max_vital = ss.asna_prc_gnvls_max,
                sum_gross = ss.asna_sum_opt,
                sum_nds_gross = ss.asna_sum_opt_nds,
                percent_vital = ss.asna_mr_gnvls,
                tag = ss.asna_tag,
            })
            .ToList();

            JsonSerializerSettings dateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local,
            };

            AsnaRemainsParam asnaRemainsParam = new AsnaRemainsParam()
            {
                stock_date = (DateTime)asna_stock_chain.stock_date,
                remains = new List<RemainsAsna>(remainsList),
            };

            row_cnt = asnaRemainsParam.remains != null ? asnaRemainsParam.remains.Count : 0;

            asnaRemainsParamStr = JsonConvert.SerializeObject(asnaRemainsParam, dateFormatSettings);

            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_SendRemains);
            if (String.IsNullOrWhiteSpace(url))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан URL сервиса отправки остатков в АСНА WebAPI"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }
            url = url.Replace("{storeId}", asna_store_id);

            HttpMethod httpMethod = is_full ? HttpMethod.Post : HttpMethod.Put;

            mess2 = asnaRemainsParamStr;
            if (mess2 == null)
                mess2 = "";
            mess2 += " [url: " + httpMethod.ToString() + " " + url + "]";
            /*
            if (mess2.Length >= 8000)
            {
                mess2 = "Длина строки превышает 8000 [" + mess2.Length.ToString() + "]";
                mess2 += " [url: " + httpMethod.ToString() + " " + url + "]";
            }
            */

            string auth = asna_user.auth;
            bool isResponseOk = true;
            string location = null;
            HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = httpMethod,
                };

                request.Headers.Add("Cache-Control", "no-cache");
                
                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");

                request.Content = new StringContent(asnaRemainsParamStr,
                                    Encoding.UTF8,
                                    "application/json");

                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var locationHeader = response.Headers.Where(ss => ss.Key == "Location").FirstOrDefault();
                        location = locationHeader.Value.Count() >= 0 ? string.Join(",", locationHeader.Value.ToArray()) : null;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, mess = errMess + errRes.error.message, };
            }

            asna_stock_batch_out asna_stock_batch_out = new asna_stock_batch_out();
            asna_stock_batch_out.asna_user_id = asna_user_id;
            asna_stock_batch_out.batch_num = asna_stock_batch_out_last != null ? asna_stock_batch_out_last.batch_num + 1 : 1;
            asna_stock_batch_out.row_cnt = vw_asna_stock_row_linked_list.Count;
            asna_stock_batch_out.stock_date = asna_stock_chain.stock_date;
            asna_stock_batch_out.is_full = asna_stock_chain.is_full;
            asna_stock_batch_out.is_active = false;
            asna_stock_batch_out.in_chain_id = asna_stock_chain.chain_id;
            asna_stock_batch_out.mess = location;
            asna_stock_batch_out.crt_date = DateTime.Now;
            asna_stock_batch_out.crt_user = user;
            asna_stock_batch_out.upd_date = asna_stock_batch_out.crt_date;
            asna_stock_batch_out.upd_user = user;
            asna_stock_batch_out.is_deleted = false;
            asna_stock_batch_out.inactive_date = asna_stock_batch_out.crt_date;
            dbContext.asna_stock_batch_out.Add(asna_stock_batch_out);
            dbContext.SaveChanges();
            asna_stock_batch_out_batch_id = asna_stock_batch_out.batch_id;

            var delResult = dbContext.Database.ExecuteSqlCommand("select und.send_remains_to_asna_finalize(@p0, @p1);", asna_stock_chain.chain_id, asna_stock_batch_out_batch_id);

            string logMess = "Отправлено строк: " + row_cnt.ToString();
            Enums.LogEsnType logEsnType = is_full ? Enums.LogEsnType.ASNA_SEND_REMAINS_FULL : Enums.LogEsnType.ASNA_SEND_REMAINS_CHANGED;
            Loggly.InsertLog_Asna(logMess, (long)logEsnType, user, null, now_initial, mess2);
            // Отправлено строк: 653           
            
            return new AuEsnBaseResult() { result = 1, };
        }
        
        #endregion
    }
}
