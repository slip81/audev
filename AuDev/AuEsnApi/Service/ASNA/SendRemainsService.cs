﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region SendRemains

        public AuEsnBaseResult SendRemains(SendRemainsServiceParam sendRemainsServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendRemains(sendRemainsServiceParam);
            }
            catch (Exception ex)
            {
                string user = sendRemainsServiceParam != null && !String.IsNullOrWhiteSpace(sendRemainsServiceParam.login) ? sendRemainsServiceParam.login : "asna";
                Loggly.InsertLog_Asna("Ошибка в SendRemains: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                //return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = GlobalUtil.ExceptionInfo(ex), };
            }
        }

        private AuEsnBaseResult sendRemains(SendRemainsServiceParam sendRemainsServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = sendRemainsServiceParam != null && !String.IsNullOrWhiteSpace(sendRemainsServiceParam.login) ? sendRemainsServiceParam.login : "asna";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[SendRemains] " : "";

            // !!!
            /*
            errRes = new AuEsnBaseResult()
            {
                error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Метод временно отключен"),
            };
            Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
            return errRes;
            */

            var checkLicenseResult = mainService.CheckLicense(sendRemainsServiceParam, AuEsnConst.license_id_ASNA);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }

            if (sendRemainsServiceParam.is_test.GetValueOrDefault(0) == 1)
            {
                return new AuEsnBaseResult() { result = 1 };
            }

            if ((sendRemainsServiceParam.rows == null) || (sendRemainsServiceParam.rows.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нет данных об остатках"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }

            string mess2 = JsonConvert.SerializeObject(sendRemainsServiceParam);
            if (mess2 == null)
                mess2 = "";
            /*
            if (mess2.Length >= 8000)
                mess2 = "Длина строки превышает 8000 [" + mess2.Length.ToString() + "]";
            */

            if (String.IsNullOrWhiteSpace(sendRemainsServiceParam.stock_date))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задана дата выгрузки остатков"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;            

            int part_cnt = sendRemainsServiceParam.part_cnt;
            int part_num = sendRemainsServiceParam.part_num;
            int row_cnt = sendRemainsServiceParam.rows.Count;
            bool is_full = sendRemainsServiceParam.is_full == 1;
            int? task_id = sendRemainsServiceParam.task_id;
            
            DateTime stock_date = DateTime.Now;
            DateTime? stock_date_nullable = sendRemainsServiceParam.stock_date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
            //bool parseOk = DateTime.TryParseExact(sendRemainsServiceParam.stock_date, "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out stock_date);            
            //if (!parseOk)
            if (!stock_date_nullable.HasValue)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный формат даты выгрузки остатков: " + sendRemainsServiceParam.stock_date + ". Ожидается формат: dd.MM.yyyy HH:mm:ss" ),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }
            stock_date = (DateTime)stock_date_nullable;

            if (part_num <= 0)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный номер пакета (" + part_num.ToString() + ")"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }

            if (part_num > part_cnt)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный номер пакета (" + part_num.ToString() + " из " + part_cnt.ToString() + ")"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }
            
            workplace workplace = null;
            asna_stock_chain asna_stock_chain_existing = null;
            asna_stock_chain asna_stock_chain = null;
            int chain_id = 0;

            vw_asna_user vw_asna_user = dbContext.vw_asna_user.Where(ss => ss.workplace_id == workplace_id && !ss.is_deleted).FirstOrDefault();
            if (vw_asna_user == null)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки для рабочего места " + workplace.registration_key),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }
            if (!vw_asna_user.is_active)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Настройки для рабочего места " + workplace.registration_key + " неактивны"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }
            
            int tax_system_type_id = vw_asna_user.sales_tax_system_type_id.HasValue ? vw_asna_user.sales_tax_system_type_id.GetValueOrDefault(0) : vw_asna_user.client_tax_system_type_id.GetValueOrDefault(0);
            if (tax_system_type_id <= 0)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для рабочего места " + workplace.registration_key + " не найдена система налогообложения"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                //return errRes;
                return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
            }

            int region_zone_id = vw_asna_user.sales_region_zone_id.HasValue ? vw_asna_user.sales_region_zone_id.GetValueOrDefault(0) : vw_asna_user.client_region_zone_id.GetValueOrDefault(0);
            int region_id = vw_asna_user.sales_region_id.HasValue ? vw_asna_user.sales_region_id.GetValueOrDefault(0) : vw_asna_user.client_region_id.GetValueOrDefault(0);
            bool isRegionZoneExists = region_zone_id > 0;

            decimal limit_value_less_or_equals_50 = 0;
            decimal limit_value_greater_50_less_or_equals_500 = 0;
            decimal limit_value_greater_500 = 0;            
            List<region_zone_price_limit> region_zone_price_limit_list = null;
            List<region_price_limit> region_price_limit_list = null;
            if (isRegionZoneExists)
            {
                region_zone_price_limit_list = dbContext.region_zone_price_limit.Where(ss => ss.zone_id == region_zone_id).ToList();
                if ((region_zone_price_limit_list == null) || (region_zone_price_limit_list.Count <= 0))
                {
                    workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для рабочего места " + workplace.registration_key + " не найдены предельные надбавки по зоне региона"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                    //return errRes;
                    return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
                }
                limit_value_less_or_equals_50 = (region_zone_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.LESS_OR_EQUALS_50).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
                limit_value_greater_50_less_or_equals_500 = (region_zone_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.GREATER_50_LESS_OR_EQUALS_500).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
                limit_value_greater_500 = (region_zone_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.GREATER_500).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
            }
            else
            {
                region_price_limit_list = dbContext.region_price_limit.Where(ss => ss.region_id == region_id).ToList();
                if ((region_price_limit_list == null) || (region_price_limit_list.Count <= 0))
                {
                    workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для рабочего места " + workplace.registration_key + " не найдены предельные надбавки по региону"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                    //return errRes;
                    return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
                }
                limit_value_less_or_equals_50 = (region_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.LESS_OR_EQUALS_50).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
                limit_value_greater_50_less_or_equals_500 = (region_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.GREATER_50_LESS_OR_EQUALS_500).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
                limit_value_greater_500 = (region_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.GREATER_500).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
            }

            int asna_user_id = vw_asna_user.asna_user_id;

            asna_stock_chain_existing = dbContext.asna_stock_chain
                .Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted)
                .OrderByDescending(ss => ss.chain_num)
                .FirstOrDefault();

            asna_stock_batch_in asna_stock_batch_in_existing = dbContext.asna_stock_batch_in
                .Where(ss => ss.asna_user_id == asna_user_id && ss.is_active && !ss.is_deleted)
                .OrderByDescending(ss => ss.part_num)
                .FirstOrDefault();
            if (asna_stock_batch_in_existing != null)
            {
                if ((part_cnt != asna_stock_batch_in_existing.part_cnt) || (part_num != asna_stock_batch_in_existing.part_num + 1))
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный номер пакета. Предыдущий загруженный пакет: " + asna_stock_batch_in_existing.part_num.GetValueOrDefault(0).ToString() + " из " + asna_stock_batch_in_existing.part_cnt.GetValueOrDefault(0).ToString()
                            + ", пришедший пакет: " + part_num.ToString() + " из " + part_cnt.ToString()),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                    //return errRes;
                    return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
                }

                if (asna_stock_chain_existing == null)
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена активная цепочка пактов"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                    //return errRes;
                    return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
                }

                asna_stock_chain = dbContext.asna_stock_chain.Where(ss => ss.chain_id == asna_stock_chain_existing.chain_id).FirstOrDefault();
                chain_id = asna_stock_chain_existing.chain_id;
            }
            else
            {
                if (part_num != 1)
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный номер пакета. Ожидается пакет с номером 1, пришедший пакет: " + part_num.ToString() + " из " + part_cnt.ToString()),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                    //return errRes;
                    return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
                }

                asna_stock_chain = new asna_stock_chain();
                asna_stock_chain.asna_user_id = asna_user_id;
                asna_stock_chain.chain_num = (asna_stock_chain_existing != null ? asna_stock_chain_existing.chain_num + 1 : 1);
                asna_stock_chain.stock_date = stock_date;
                asna_stock_chain.is_full = is_full;
                asna_stock_chain.is_active = true;
                asna_stock_chain.inactive_date = null;
                asna_stock_chain.crt_date = DateTime.Now;
                asna_stock_chain.crt_user = user;
                asna_stock_chain.upd_date = asna_stock_chain.crt_date;
                asna_stock_chain.upd_user = user;
                asna_stock_chain.is_deleted = false;
                dbContext.asna_stock_chain.Add(asna_stock_chain);
                dbContext.SaveChanges();
                chain_id = asna_stock_chain.chain_id;
            }

            asna_stock_batch_in asna_stock_batch_in_last = dbContext.asna_stock_batch_in
                .Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted)
                .OrderByDescending(ss => ss.batch_num)
                .FirstOrDefault();

            asna_stock_batch_in asna_stock_batch_in = new asna_stock_batch_in();
            asna_stock_batch_in.asna_user_id = asna_user_id;
            asna_stock_batch_in.chain_id = chain_id;
            asna_stock_batch_in.batch_num = asna_stock_batch_in_last != null ? asna_stock_batch_in_last.batch_num + 1 : 1;
            asna_stock_batch_in.part_cnt = part_cnt;
            asna_stock_batch_in.part_num = part_num;
            asna_stock_batch_in.row_cnt = row_cnt;
            asna_stock_batch_in.is_active = true;
            asna_stock_batch_in.inactive_date = null;
            asna_stock_batch_in.crt_date = DateTime.Now;
            asna_stock_batch_in.crt_user = user;
            asna_stock_batch_in.upd_date = asna_stock_batch_in.crt_date;
            asna_stock_batch_in.upd_user = user;
            asna_stock_batch_in.is_deleted = false;
            dbContext.asna_stock_batch_in.Add(asna_stock_batch_in);
            dbContext.SaveChanges();

            asna_stock_row asna_stock_row = null;

            int cnt = 0;

            AuMainDb transactionContext = null;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
            {
                transactionContext = null;
                try
                {
                    transactionContext = new AuMainDb();
                    transactionContext.Configuration.AutoDetectChangesEnabled = false;

                    cnt = 0;
                    foreach (var itemAdd in sendRemainsServiceParam.rows)
                    {
                        cnt = cnt + 1;                        

                        asna_stock_row = new asna_stock_row();
                        asna_stock_row.asna_user_id = asna_user_id;
                        asna_stock_row.in_batch_id = asna_stock_batch_in.batch_id;
                                                
                        asna_stock_row.artikul = itemAdd.artikul;
                        //asna_stock_row.row_stock_id = itemAdd.stock_id;
                        asna_stock_row.esn_id = itemAdd.esn_id;                                                                        
                        asna_stock_row.prep_id = itemAdd.prep_id;
                        asna_stock_row.prep_name = itemAdd.prep_name;
                        asna_stock_row.firm_id = itemAdd.firm_id;
                        asna_stock_row.firm_name = itemAdd.firm_name;
                        asna_stock_row.barcode = itemAdd.barcode;
                        asna_stock_row.asna_prt_id = itemAdd.prt_id;                        
                        asna_stock_row.asna_sku = itemAdd.sku;

                        asna_stock_row.asna_qnt = itemAdd.all_cnt;
                        asna_stock_row.asna_doc = itemAdd.supplier_doc_num;
                        asna_stock_row.asna_sup_inn = itemAdd.supplier_inn;
                        asna_stock_row.asna_sup_id = null;
                        asna_stock_row.asna_sup_date = itemAdd.supplier_doc_date.ToDateTimeStrict("dd.MM.yyyy");
                        asna_stock_row.asna_nds = itemAdd.percent_nds;
                        asna_stock_row.asna_gnvls = itemAdd.is_vital == 1 ? true : false;
                        asna_stock_row.asna_dp = itemAdd.is_child_food == 1 ? true : false;
                        asna_stock_row.asna_man = itemAdd.firm_name;
                        asna_stock_row.asna_series = itemAdd.series;
                        asna_stock_row.asna_exp = itemAdd.valid_date.ToDateTimeStrict("dd.MM.yyyy");
                        asna_stock_row.asna_prc_man = itemAdd.price_firm;
                        asna_stock_row.asna_prc_opt = itemAdd.price_gross;
                        asna_stock_row.asna_prc_opt_nds = itemAdd.price_nds_gross;
                        asna_stock_row.asna_prc_ret = itemAdd.price_retail;
                        asna_stock_row.asna_prc_reestr = itemAdd.price_gr;

                        // !!!
                        asna_stock_row.asna_prc_gnvls_max = itemAdd.is_vital == 1 ? AsnaPrcGnvlsMaxCalcHelper.Calc(tax_system_type_id, limit_value_less_or_equals_50, limit_value_greater_50_less_or_equals_500, limit_value_greater_500, itemAdd.price_firm, itemAdd.price_gross, itemAdd.percent_nds) : null;

                        asna_stock_row.asna_sum_opt = itemAdd.sum_gross;
                        asna_stock_row.asna_sum_opt_nds = itemAdd.sum_nds_gross;
                        asna_stock_row.asna_mr_gnvls = itemAdd.percent_vital;
                        asna_stock_row.asna_tag = itemAdd.tag;

                        asna_stock_row.crt_date = DateTime.Now;
                        asna_stock_row.crt_user = user;
                        asna_stock_row.upd_date = asna_stock_row.crt_date;
                        asna_stock_row.upd_user = user;
                        asna_stock_row.is_deleted = false;

                        transactionContext = AddToContext<asna_stock_row>(transactionContext, asna_stock_row, cnt, 100, true);
                    }

                    transactionContext.SaveChanges();
                }
                finally
                {
                    if (transactionContext != null)
                        transactionContext.Dispose();
                }

                scope.Complete();
            }

            if (part_cnt == part_num)
            {
                List<asna_stock_batch_in> asna_stock_batch_in_list = dbContext.asna_stock_batch_in
                    .Where(ss => ss.asna_user_id == asna_user_id && ss.is_active && !ss.is_deleted).ToList();
                if ((asna_stock_batch_in_list != null) && (asna_stock_batch_in_list.Count > 0))
                {
                    foreach (var asna_stock_batch_in_list_item in asna_stock_batch_in_list)
                    {
                        asna_stock_batch_in_list_item.is_active = false;
                        asna_stock_batch_in_list_item.inactive_date = DateTime.Now;
                        asna_stock_batch_in_list_item.upd_date = asna_stock_batch_in_list_item.inactive_date;
                        asna_stock_batch_in_list_item.upd_user = user;
                    }                    
                }
                asna_stock_chain.is_active = false;
                asna_stock_chain.inactive_date = DateTime.Now;
                asna_stock_chain.upd_date = asna_stock_chain.inactive_date;
                asna_stock_chain.upd_user = user;
                dbContext.SaveChanges();
                
                // если этот метод был вызван без задания (task_id = null) - после успешного принятия всех пакетов из цепочки создаем задание в статусе "Выдано серверу"
                if (task_id.GetValueOrDefault(0) <= 0)
                {
                    AsnaServiceParam asnaServiceParam = new AsnaServiceParam() { asna_user_id = asna_user_id};
                    Enums.WaRequestTypeEnum requestType = is_full ? Enums.WaRequestTypeEnum.ASNA_SendStocksFull : Enums.WaRequestTypeEnum.ASNA_SendStocksChanges;
                    UpdateTaskResult updateTaskResult = waService.CreateTask(user, (int)requestType, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, client_id, sales_id, workplace_id, false, null, JsonConvert.SerializeObject(asnaServiceParam));
                    if (updateTaskResult.error != null)
                    {
                        errRes = new AuEsnBaseResult() { error = updateTaskResult.error };
                        Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                        //return errRes;
                        return new AuEsnBaseResult() { result = 1, request_id = "0", mess = errMess + errRes.error.message, };
                    }
                    task_id = updateTaskResult.task_id;
                }

                SendRemainsToAsnaServiceParam sendRemainsToAsnaServiceParam = new SendRemainsToAsnaServiceParam() 
                {
                    asna_user_id = asna_user_id,
                    chain_id = chain_id,
                };
                waService.UpdateTaskParam(user, task_id.GetValueOrDefault(0), JsonConvert.SerializeObject(sendRemainsToAsnaServiceParam));
            }            

            string logMess = "Подготовлено строк к отправке: " + row_cnt.ToString();
            Enums.LogEsnType logEsnType = is_full ? Enums.LogEsnType.ASNA_SEND_REMAINS_FULL : Enums.LogEsnType.ASNA_SEND_REMAINS_CHANGED;
            Loggly.InsertLog_Asna(logMess, (long)logEsnType, user, null, now_initial, mess2);
            // Подготовлено строк к отправке: 653           

            return new AuEsnBaseResult() { result = 1, request_id = task_id.ToString(), };
        }
        
        #endregion
    }
}
