﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region SendOrders

        public AuEsnBaseResult SendOrders(SendOrdersServiceParam sendOrdersServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendOrders(sendOrdersServiceParam);
            }
            catch (Exception ex)
            {
                string user = sendOrdersServiceParam != null && !String.IsNullOrWhiteSpace(sendOrdersServiceParam.login) ? sendOrdersServiceParam.login : "asna";
                Loggly.InsertLog_Asna("Ошибка в SendOrders: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult sendOrders(SendOrdersServiceParam sendOrdersServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = sendOrdersServiceParam != null && !String.IsNullOrWhiteSpace(sendOrdersServiceParam.login) ? sendOrdersServiceParam.login : "asna";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[SendOrders] " : "";

            var checkLicenseResult = mainService.CheckLicense(sendOrdersServiceParam, AuEsnConst.license_id_ASNA);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            workplace workplace = null;
            asna_user asna_user = dbContext.asna_user.Where(ss => ss.workplace_id == workplace_id && !ss.is_deleted).FirstOrDefault();
            if (asna_user == null)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки для рабочего места " + workplace.registration_key),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            if (!asna_user.is_active)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Настройки для рабочего места " + workplace.registration_key + " неактивны"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            // !!!

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user.asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = getAuthorizedUserResult.error
                };
                return errRes;
            }            
            asna_user = getAuthorizedUserResult.asna_user;

            string asna_store_id = asna_user.asna_store_id;
            if (String.IsNullOrWhiteSpace(asna_store_id))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код аптеки по АСНА у пользователя с кодом " + asna_user.asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int asna_user_id = asna_user.asna_user_id;
            string auth = asna_user.auth;
            int row_cnt1 = 0;
            int row_cnt2 = 0;

            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_SendOrders);
            if (String.IsNullOrWhiteSpace(url))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан URL сервиса отправки заказов из АСНА WebAPI"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            url = url.Replace("{storeId}", asna_store_id);

            HttpMethod httpMethod = HttpMethod.Put;

            if (sendOrdersServiceParam.rows == null)
                sendOrdersServiceParam.rows = new List<SentOrderRow>();
            if (sendOrdersServiceParam.orders_states == null)
                sendOrdersServiceParam.orders_states = new List<SentOrderState>();
            if (sendOrdersServiceParam.rows_states == null)
                sendOrdersServiceParam.rows_states = new List<SentOrderState>();

            SentOrderAsna sentOrderAsna = new SentOrderAsna();
            sentOrderAsna.rows = new List<SentOrderRowAsna>();
            sentOrderAsna.statuses = new List<SentOrderStateAsna>();

            asna_order asna_order = null;
            asna_order_row asna_order_row = null;
            asna_order_state asna_order_state = null;
            asna_order_row_state asna_order_row_state = null;
            List<asna_order_row> asna_order_row_list = null;

            bool somethingChanged = false;

            foreach (var row in sendOrdersServiceParam.rows)
            {                
                asna_order_row = dbContext.asna_order_row.Where(ss => ss.row_id == row.row_id).FirstOrDefault();
                if (asna_order_row == null)
                    continue;                
                sentOrderAsna.rows.Add(new SentOrderRowAsna()
                    {
                        row_id = asna_order_row.asna_row_id,
                        qnt_unrsv = row.qnt_unrsv,
                    });

                asna_order_row.asna_qnt_unrsv = row.qnt_unrsv.GetValueOrDefault(0);

                somethingChanged = true;
            }
            if (somethingChanged)
                dbContext.SaveChanges();

            somethingChanged = false;
            foreach (var orderState in sendOrdersServiceParam.orders_states)
            {
                asna_order = dbContext.asna_order.Where(ss => ss.order_id == orderState.order_id).FirstOrDefault();
                if (asna_order == null)
                    continue;
                sentOrderAsna.statuses.Add(new SentOrderStateAsna()
                    {
                        status_id = Guid.NewGuid().ToString(),
                        row_id = null,
                        order_id = asna_order.asna_order_id,
                        store_id = orderState.store_id,
                        date = orderState.date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss"),
                        status = orderState.status,
                        rc_date = orderState.rc_date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss"),
                        cmnt = orderState.cmnt,
                    });

                asna_order_state = new asna_order_state();
                asna_order_state.order_id = asna_order.order_id;
                asna_order_state.state_type_id = orderState.status;
                asna_order_state.asna_status_id = orderState.status.ToString();
                asna_order_state.asna_order_id = asna_order.asna_order_id;
                asna_order_state.asna_date = orderState.date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
                asna_order_state.asna_rc_date = orderState.rc_date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
                asna_order_state.asna_cmnt = orderState.cmnt;
                asna_order_state.asna_ts = now_initial;
                asna_order_state.crt_date = now_initial;                
                asna_order_state.crt_user = user;
                asna_order_state.upd_date = now_initial;
                asna_order_state.upd_user = user;
                asna_order_state.is_deleted = false;
                dbContext.asna_order_state.Add(asna_order_state);

                asna_order.state_type_id = orderState.status;

                //state = 200: для всех строк незарезервированное кол - во = 0
                //state = 202: для всех строк незарезервированное кол - во = кол - во в заказе
                if ((asna_order.state_type_id == 200) || (asna_order.state_type_id == 202))
                {
                    asna_order_row_list = dbContext.asna_order_row.Where(ss => ss.order_id == asna_order.order_id).ToList();
                    if ((asna_order_row_list != null) && (asna_order_row_list.Count > 0))
                    {
                        foreach (var asna_order_row_list_item in asna_order_row_list)
                        {
                            asna_order_row_list_item.asna_qnt_unrsv = asna_order.state_type_id == 200 ? 0 : asna_order_row_list_item.asna_qnt;
                        }
                    }                    
                }

                somethingChanged = true;
            }
            if (somethingChanged)
                dbContext.SaveChanges();

            somethingChanged = false;
            foreach (var rowState in sendOrdersServiceParam.rows_states)
            {
                asna_order_row = dbContext.asna_order_row.Where(ss => ss.row_id == rowState.row_id).FirstOrDefault();
                if (asna_order_row == null)
                    continue;
                sentOrderAsna.statuses.Add(new SentOrderStateAsna()
                {
                    status_id = Guid.NewGuid().ToString(),
                    row_id = asna_order_row.asna_row_id,
                    order_id = asna_order_row.asna_order_id,
                    store_id = rowState.store_id,
                    date = rowState.date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss"),
                    status = rowState.status,
                    rc_date = rowState.rc_date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss"),
                    cmnt = rowState.cmnt,
                });

                asna_order_row_state = new asna_order_row_state();                
                asna_order_row_state.row_id = asna_order_row.row_id;
                asna_order_row_state.state_type_id = rowState.status;
                asna_order_row_state.asna_status_id = rowState.status.ToString();
                asna_order_row_state.asna_order_id = asna_order_row.asna_order_id;
                asna_order_row_state.asna_row_id = asna_order_row.asna_row_id;
                asna_order_row_state.asna_date = rowState.date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
                asna_order_row_state.asna_rc_date = rowState.rc_date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
                asna_order_row_state.asna_cmnt = rowState.cmnt;
                asna_order_row_state.asna_ts = now_initial;
                asna_order_row_state.crt_date = now_initial;
                asna_order_row_state.crt_user = user;
                asna_order_row_state.upd_date = now_initial;
                asna_order_row_state.upd_user = user;
                asna_order_row_state.is_deleted = false;
                dbContext.asna_order_row_state.Add(asna_order_row_state);

                asna_order_row.state_type_id = rowState.status;

                somethingChanged = true;
            }
            if (somethingChanged)
                dbContext.SaveChanges();

            //dbContext.SaveChanges();

            row_cnt1 = sentOrderAsna.rows.Count;
            row_cnt1 = sentOrderAsna.statuses.Count;

            JsonSerializerSettings dateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local,
            };
            string sentOrderAsnaStr = JsonConvert.SerializeObject(sentOrderAsna, dateFormatSettings);

            string mess2 = sentOrderAsnaStr;
            if (mess2 == null)
                mess2 = "";
            mess2 += " [url: " + httpMethod.ToString() + " " + url + "]";
            if (mess2.Length >= 8000)
            {
                mess2 = "Длина строки превышает 8000 [" + mess2.Length.ToString() + "]";
                mess2 += " [url: " + httpMethod.ToString() + " " + url + "]";
            }

            string location = "";
            bool isResponseOk = false;
            HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = httpMethod,
                };

                request.Headers.Add("Cache-Control", "no-cache");

                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");

                request.Content = new StringContent(sentOrderAsnaStr,
                                    Encoding.UTF8,
                                    "application/json");

                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        location = response.Headers.Location == null ? null : response.Headers.Location.AbsoluteUri;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, mess2);
                return errRes;
            }
            
            string logMess = "Отправлено строк: " + row_cnt1.ToString() + ", статусов: " + row_cnt2.ToString();
            Enums.LogEsnType logEsnType = Enums.LogEsnType.ASNA_SEND_ORDERS;
            Loggly.InsertLog_Asna(logMess, (long)logEsnType, user, null, now_initial, mess2);
            
            return new AuEsnBaseResult() { result = 1, };
        }
        
        #endregion
    }
}
