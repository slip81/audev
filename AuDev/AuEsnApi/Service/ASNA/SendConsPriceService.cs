﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region SendConsPrice

        public AuEsnBaseResult SendConsPrice(SendConsPriceServiceParam sendConsPriceServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendConsPrice(sendConsPriceServiceParam);
            }
            catch (Exception ex)
            {
                string user = sendConsPriceServiceParam != null && !String.IsNullOrWhiteSpace(sendConsPriceServiceParam.login) ? sendConsPriceServiceParam.login : "asna";
                Loggly.InsertLog_Asna("Ошибка в SendConsPrice: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult sendConsPrice(SendConsPriceServiceParam sendConsPriceServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = sendConsPriceServiceParam != null && !String.IsNullOrWhiteSpace(sendConsPriceServiceParam.login) ? sendConsPriceServiceParam.login : "asna";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[SendConsPrice] " : "";

            var checkLicenseResult = mainService.CheckLicense(sendConsPriceServiceParam, AuEsnConst.license_id_ASNA);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (sendConsPriceServiceParam.is_test.GetValueOrDefault(0) == 1)
            {
                return new AuEsnBaseResult() { result = 1 };
            }

            if ((sendConsPriceServiceParam.prices == null) || (sendConsPriceServiceParam.prices.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нет данных о прайс-листе"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (String.IsNullOrWhiteSpace(sendConsPriceServiceParam.price_date))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задана дата выгрузки прайс-листа"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            int part_cnt = sendConsPriceServiceParam.part_cnt;
            int part_num = sendConsPriceServiceParam.part_num;            
            int row_cnt = sendConsPriceServiceParam.prices.Count;
            bool is_full = sendConsPriceServiceParam.is_full == 1;
            int? task_id = sendConsPriceServiceParam.task_id;
            
            DateTime price_date = DateTime.Now;
            DateTime? price_date_nullable = sendConsPriceServiceParam.price_date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
            if (!price_date_nullable.HasValue)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный формат даты выгрузки прайс-листа: " + sendConsPriceServiceParam.price_date + ". Ожидается формат: dd.MM.yyyy HH:mm:ss"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            price_date = (DateTime)price_date_nullable;

            if (part_num <= 0)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный номер пакета (" + part_num.ToString() + ")"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if (part_num > part_cnt)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный номер пакета (" + part_num.ToString() + " из " + part_cnt.ToString() + ")"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }           

            workplace workplace = null;
            asna_price_chain asna_price_chain_existing = null;
            asna_price_chain asna_price_chain = null;
            int chain_id = 0;

            vw_asna_user vw_asna_user = dbContext.vw_asna_user.Where(ss => ss.workplace_id == workplace_id && !ss.is_deleted).FirstOrDefault();
            if (vw_asna_user == null)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки для рабочего места " + workplace.registration_key),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            if (!vw_asna_user.is_active)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Настройки для рабочего места " + workplace.registration_key + " неактивны"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int tax_system_type_id = vw_asna_user.sales_tax_system_type_id.HasValue ? vw_asna_user.sales_tax_system_type_id.GetValueOrDefault(0) : vw_asna_user.client_tax_system_type_id.GetValueOrDefault(0);
            if (tax_system_type_id <= 0)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для рабочего места " + workplace.registration_key + " не найдена система налогообложения"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int region_zone_id = vw_asna_user.sales_region_zone_id.HasValue ? vw_asna_user.sales_region_zone_id.GetValueOrDefault(0) : vw_asna_user.client_region_zone_id.GetValueOrDefault(0);
            int region_id = vw_asna_user.sales_region_id.HasValue ? vw_asna_user.sales_region_id.GetValueOrDefault(0) : vw_asna_user.client_region_id.GetValueOrDefault(0);
            bool isRegionZoneExists = region_zone_id > 0;

            decimal limit_value_less_or_equals_50 = 0;
            decimal limit_value_greater_50_less_or_equals_500 = 0;
            decimal limit_value_greater_500 = 0;
            List<region_zone_price_limit> region_zone_price_limit_list = null;
            List<region_price_limit> region_price_limit_list = null;
            if (isRegionZoneExists)
            {
                region_zone_price_limit_list = dbContext.region_zone_price_limit.Where(ss => ss.zone_id == region_zone_id).ToList();
                if ((region_zone_price_limit_list == null) || (region_zone_price_limit_list.Count <= 0))
                {
                    workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для рабочего места " + workplace.registration_key + " не найдены предельные надбавки по зоне региона"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }
                limit_value_less_or_equals_50 = (region_zone_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.LESS_OR_EQUALS_50).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
                limit_value_greater_50_less_or_equals_500 = (region_zone_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.GREATER_50_LESS_OR_EQUALS_500).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
                limit_value_greater_500 = (region_zone_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.GREATER_500).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
            }
            else
            {
                region_price_limit_list = dbContext.region_price_limit.Where(ss => ss.region_id == region_id).ToList();
                if ((region_price_limit_list == null) || (region_price_limit_list.Count <= 0))
                {
                    workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для рабочего места " + workplace.registration_key + " не найдены предельные надбавки по региону"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }
                limit_value_less_or_equals_50 = (region_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.LESS_OR_EQUALS_50).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
                limit_value_greater_50_less_or_equals_500 = (region_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.GREATER_50_LESS_OR_EQUALS_500).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
                limit_value_greater_500 = (region_price_limit_list.Where(ss => ss.limit_type_id == (int)Enums.PriceLimitTypeEnum.GREATER_500).Select(ss => ss.percent_value).FirstOrDefault()).GetValueOrDefault(0);
            }

            int asna_user_id = vw_asna_user.asna_user_id;

            asna_price_chain_existing = dbContext.asna_price_chain
                .Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted)
                .OrderByDescending(ss => ss.chain_num)
                .FirstOrDefault();

            asna_price_batch_in asna_price_batch_in_existing = dbContext.asna_price_batch_in
                .Where(ss => ss.asna_user_id == asna_user_id && ss.is_active && !ss.is_deleted)
                .OrderByDescending(ss => ss.part_num)
                .FirstOrDefault();
            if (asna_price_batch_in_existing != null)
            {
                if ((part_cnt != asna_price_batch_in_existing.part_cnt) || (part_num != asna_price_batch_in_existing.part_num + 1))
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный номер пакета. Предыдущий загруженный пакет: " + asna_price_batch_in_existing.part_num.GetValueOrDefault(0).ToString() + " из " + asna_price_batch_in_existing.part_cnt.GetValueOrDefault(0).ToString()
                            + ", пришедший пакет: " + part_num.ToString() + " из " + part_cnt.ToString()),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }

                if (asna_price_chain_existing == null)
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена активная цепочка пактов"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }

                asna_price_chain = dbContext.asna_price_chain.Where(ss => ss.chain_id == asna_price_chain_existing.chain_id).FirstOrDefault();
                chain_id = asna_price_chain_existing.chain_id;
            }
            else
            {
                if (part_num != 1)
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Неверный номер пакета. Ожидается пакет с номером 1, пришедший пакет: " + part_num.ToString() + " из " + part_cnt.ToString()),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }

                asna_price_chain = new asna_price_chain();
                asna_price_chain.asna_user_id = asna_user_id;
                asna_price_chain.chain_num = (asna_price_chain_existing != null ? asna_price_chain_existing.chain_num + 1 : 1);
                asna_price_chain.price_date = price_date;
                asna_price_chain.is_full = is_full;
                asna_price_chain.is_active = true;
                asna_price_chain.inactive_date = null;
                asna_price_chain.crt_date = DateTime.Now;
                asna_price_chain.crt_user = user;
                asna_price_chain.upd_date = asna_price_chain.crt_date;
                asna_price_chain.upd_user = user;
                asna_price_chain.is_deleted = false;
                dbContext.asna_price_chain.Add(asna_price_chain);
                dbContext.SaveChanges();
                chain_id = asna_price_chain.chain_id;
            }

            asna_price_batch_in asna_price_batch_in_last = dbContext.asna_price_batch_in
                .Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted)
                .OrderByDescending(ss => ss.batch_num)
                .FirstOrDefault();

            asna_price_batch_in asna_price_batch_in = new asna_price_batch_in();
            asna_price_batch_in.asna_user_id = asna_user_id;
            asna_price_batch_in.chain_id = chain_id;
            asna_price_batch_in.batch_num = asna_price_batch_in_last != null ? asna_price_batch_in_last.batch_num + 1 : 1;
            asna_price_batch_in.part_cnt = part_cnt;
            asna_price_batch_in.part_num = part_num;
            asna_price_batch_in.row_cnt = row_cnt;
            asna_price_batch_in.is_active = true;
            asna_price_batch_in.inactive_date = null;
            asna_price_batch_in.crt_date = DateTime.Now;
            asna_price_batch_in.crt_user = user;
            asna_price_batch_in.upd_date = asna_price_batch_in.crt_date;
            asna_price_batch_in.upd_user = user;
            asna_price_batch_in.is_deleted = false;
            dbContext.asna_price_batch_in.Add(asna_price_batch_in);
            dbContext.SaveChanges();

            asna_price_row asna_price_row = null;
            int cnt = 0;
            AuMainDb transactionContext = null;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
            {
                transactionContext = null;
                try
                {
                    transactionContext = new AuMainDb();
                    transactionContext.Configuration.AutoDetectChangesEnabled = false;

                    cnt = 0;
                    foreach (var itemAdd in sendConsPriceServiceParam.prices)
                    {
                        cnt = cnt + 1;

                        asna_price_row = new asna_price_row();
                        asna_price_row.asna_user_id = asna_user_id;
                        asna_price_row.in_batch_id = asna_price_batch_in.batch_id;

                        asna_price_row.artikul = itemAdd.artikul;
                        asna_price_row.esn_id = itemAdd.esn_id;
                        asna_price_row.prep_id = itemAdd.prep_id;
                        asna_price_row.prep_name = itemAdd.prep_name;
                        asna_price_row.firm_id = itemAdd.firm_id;
                        asna_price_row.firm_name = itemAdd.firm_name;
                        asna_price_row.barcode = itemAdd.barcode;
                        
                        asna_price_row.asna_sku = itemAdd.sku;
                        asna_price_row.asna_sup_inn = itemAdd.sup_inn;
                        asna_price_row.asna_prc_opt_nds = itemAdd.prc_opt_nds;
                        
                        asna_price_row.asna_prc_gnvls_max = itemAdd.prc_gnvls_max;
                        // !!!
                        // не передаются поля: percent_nds, price_firm, price_gross
                        // asna_price_row.asna_prc_gnvls_max = AsnaPrcGnvlsMaxCalcHelper.Calc(tax_system_type_id, limit_value_less_or_equals_50, limit_value_greater_50_less_or_equals_500, limit_value_greater_500, itemAdd.price_firm, itemAdd.price_gross, itemAdd.percent_nds);

                        asna_price_row.asna_status = itemAdd.status.GetValueOrDefault(0);
                        asna_price_row.asna_exp = itemAdd.exp.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
                        asna_price_row.asna_sup_date = itemAdd.sup_date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");
                        asna_price_row.asna_prcl_date = itemAdd.prcl_date.ToDateTimeStrict("dd.MM.yyyy HH:mm:ss");

                        asna_price_row.crt_date = DateTime.Now;
                        asna_price_row.crt_user = user;
                        asna_price_row.upd_date = asna_price_row.crt_date;
                        asna_price_row.upd_user = user;
                        asna_price_row.is_deleted = false;

                        transactionContext = AddToContext<asna_price_row>(transactionContext, asna_price_row, cnt, 100, true);
                    }

                    transactionContext.SaveChanges();
                }
                finally
                {
                    if (transactionContext != null)
                        transactionContext.Dispose();
                }

                scope.Complete();
            }

            if (part_cnt == part_num)
            {
                List<asna_price_batch_in> asna_price_batch_in_list = dbContext.asna_price_batch_in
                    .Where(ss => ss.asna_user_id == asna_user_id && ss.is_active && !ss.is_deleted).ToList();
                if ((asna_price_batch_in_list != null) && (asna_price_batch_in_list.Count > 0))
                {
                    foreach (var asna_price_batch_in_list_item in asna_price_batch_in_list)
                    {
                        asna_price_batch_in_list_item.is_active = false;
                        asna_price_batch_in_list_item.inactive_date = DateTime.Now;
                        asna_price_batch_in_list_item.upd_date = asna_price_batch_in_list_item.inactive_date;
                        asna_price_batch_in_list_item.upd_user = user;
                    }
                }
                asna_price_chain.is_active = false;
                asna_price_chain.inactive_date = DateTime.Now;
                asna_price_chain.upd_date = asna_price_chain.inactive_date;
                asna_price_chain.upd_user = user;
                dbContext.SaveChanges();

                // если этот метод был вызван без задания (task_id = null) - после успешного принятия всех пакетов из цепочки создаем задание в статусе "Выдано серверу"
                if (task_id.GetValueOrDefault(0) <= 0)
                {
                    AsnaServiceParam asnaServiceParam = new AsnaServiceParam() { asna_user_id = asna_user_id };
                    Enums.WaRequestTypeEnum requestType = is_full ? Enums.WaRequestTypeEnum.ASNA_SendConsPriceFull : Enums.WaRequestTypeEnum.ASNA_SendConsPriceChanges;
                    UpdateTaskResult updateTaskResult = waService.CreateTask(user, (int)requestType, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, client_id, sales_id, workplace_id, false, null, JsonConvert.SerializeObject(asnaServiceParam));
                    if (updateTaskResult.error != null)
                    {
                        errRes = new AuEsnBaseResult() { error = updateTaskResult.error };
                        Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                        return errRes;
                    }
                    task_id = updateTaskResult.task_id;
                }

                SendConsPriceToAsnaServiceParam sendConsPriceToAsnaServiceParam = new SendConsPriceToAsnaServiceParam()
                {
                    asna_user_id = asna_user_id,
                    chain_id = chain_id,
                };
                waService.UpdateTaskParam(user, task_id.GetValueOrDefault(0), JsonConvert.SerializeObject(sendConsPriceToAsnaServiceParam));
            }

            string logMess = "Отправлено строк: " + row_cnt.ToString();
            Enums.LogEsnType logEsnType = is_full ? Enums.LogEsnType.ASNA_SEND_PREORDERS_FULL : Enums.LogEsnType.ASNA_SEND_PREORDERS_CHANGED;
            Loggly.InsertLog_Asna(logMess, (long)logEsnType, user, asna_price_batch_in.batch_id, DateTime.Now, null);

            return new AuEsnBaseResult() { result = 1, request_id = task_id.ToString(), };
        }
        
        #endregion
    }
}
