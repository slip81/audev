﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region ResetSendRemains

        public AuEsnBaseResult ResetSendRemains(UserInfo resetSendRemainsServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return resetSendRemains(resetSendRemainsServiceParam);
            }
            catch (Exception ex)
            {
                string user = resetSendRemainsServiceParam != null && !String.IsNullOrWhiteSpace(resetSendRemainsServiceParam.login) ? resetSendRemainsServiceParam.login : "asna";
                Loggly.InsertLog_Asna("Ошибка в ResetSendRemains: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult resetSendRemains(UserInfo resetSendRemainsServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = resetSendRemainsServiceParam != null && !String.IsNullOrWhiteSpace(resetSendRemainsServiceParam.login) ? resetSendRemainsServiceParam.login : "asna";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[ResetSendRemains] " : "";

            var checkLicenseResult = mainService.CheckLicense(resetSendRemainsServiceParam, AuEsnConst.license_id_ASNA);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            workplace workplace = null;
            asna_user asna_user = dbContext.asna_user.Where(ss => ss.workplace_id == workplace_id && !ss.is_deleted).FirstOrDefault();
            if (asna_user == null)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки для рабочего места " + workplace.registration_key),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            if (!asna_user.is_active)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Настройки для рабочего места " + workplace.registration_key + " неактивны"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int asna_user_id = asna_user.asna_user_id;

            List<asna_stock_batch_in> asna_stock_batch_in_list = dbContext.asna_stock_batch_in.Where(ss => ss.asna_user_id == asna_user_id && ss.is_active && !ss.is_deleted).ToList();
            if ((asna_stock_batch_in_list == null) || (asna_stock_batch_in_list.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена активная цепочка пакетов остатков"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            
            int cnt = 0;
            foreach (var asna_stock_batch_in in asna_stock_batch_in_list)
            {
                asna_stock_batch_in.is_deleted = true;
                asna_stock_batch_in.del_date = DateTime.Now;
                asna_stock_batch_in.del_user = user;
                cnt++;
            }

            dbContext.SaveChanges();

            Loggly.InsertLog_Asna("Сброшена активная цепочка пакетов остатков [" + cnt.ToString() + "]", (long)Enums.LogEsnType.ASNA_RESET_SEND_REMAINS, user, null, DateTime.Now, null);
            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}