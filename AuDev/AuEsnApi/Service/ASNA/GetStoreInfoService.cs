﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
#endregion

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region GetStoreInfo

        public GetStoreInfoResult GetStoreInfo(UserInfo getStoreInfoServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getStoreInfo(getStoreInfoServiceParam);
            }
            catch (Exception ex)
            {
                string user = getStoreInfoServiceParam != null && !String.IsNullOrWhiteSpace(getStoreInfoServiceParam.login) ? getStoreInfoServiceParam.login : "asna";
                Loggly.InsertLog_Asna("Ошибка в GetStoreInfo: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetStoreInfoResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetStoreInfoResult getStoreInfo(UserInfo getStoreInfoServiceParam)
        {
            GetStoreInfoResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = getStoreInfoServiceParam != null && !String.IsNullOrWhiteSpace(getStoreInfoServiceParam.login) ? getStoreInfoServiceParam.login : "asna";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[GetStoreInfo] " : "";

            var checkLicenseResult = mainService.CheckLicense(getStoreInfoServiceParam, AuEsnConst.license_id_ASNA);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new GetStoreInfoResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;
            
            workplace workplace = null;
            asna_user asna_user = dbContext.asna_user.Where(ss => ss.workplace_id == workplace_id && !ss.is_deleted).FirstOrDefault();
            if (asna_user == null)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new GetStoreInfoResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки для рабочего места " + workplace.registration_key),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            if (!asna_user.is_active)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new GetStoreInfoResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Настройки для рабочего места " + workplace.registration_key + " неактивны"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            asna_store_info asna_store_info = dbContext.asna_store_info.Where(ss => ss.asna_user_id == asna_user.asna_user_id).FirstOrDefault();
            if (asna_store_info == null)
            {
                workplace = dbContext.workplace.Where(ss => ss.id == workplace_id).FirstOrDefault();
                errRes = new GetStoreInfoResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена информация об аптеке для рабочего места " + workplace.registration_key),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            List<asna_store_info_schedule> asna_store_info_schedule_list = dbContext.asna_store_info_schedule.Where(ss => ss.info_id == asna_store_info.info_id).ToList();

            return new GetStoreInfoResult()
            {
                store_info = new StoreInfo()
                {
                    name = asna_store_info.asna_name,
                    full_name = asna_store_info.asna_full_name,
                    inn = asna_store_info.asna_inn,
                    rt_terminal = asna_store_info.asna_rt_terminal,
                    rt_mobile = asna_store_info.asna_rt_mobile,
                    rt_site = asna_store_info.asna_rt_site,
                    edit_check = asna_store_info.asna_edit_check,
                    cancel_order = asna_store_info.asna_cancel_order,
                    full_time = asna_store_info.asna_full_time,
                    store_id = asna_user.asna_store_id,
                    legal_id = asna_user.asna_legal_id,
                    schedule = asna_store_info_schedule_list == null ? null 
                        : asna_store_info_schedule_list.Select(ss => new StoreInfoSchedule()
                        {
                           week_day_id = ss.asna_week_day_id,
                           start_time = ss.asna_start_time,
                           end_time = ss.asna_end_time,
                           work = ss.asna_work,
                        })
                        .ToList(),
                }
            };
            
        }
        
        #endregion
    }
}
