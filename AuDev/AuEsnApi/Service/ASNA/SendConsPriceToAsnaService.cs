﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuDev.Common.Extensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region SendConsPriceToAsna

        public AuEsnBaseResult SendConsPriceToAsna(SendConsPriceToAsnaServiceParam sendConsPriceToAsnaServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendConsPriceToAsna(sendConsPriceToAsnaServiceParam);
            }
            catch (Exception ex)
            {
                string user = "asnabot";
                Loggly.InsertLog_Asna("Ошибка в SendConsPriceToAsna: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult sendConsPriceToAsna(SendConsPriceToAsnaServiceParam sendConsPriceToAsnaServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[SendConsPriceToAsna] " : "";

            if (sendConsPriceToAsnaServiceParam == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int chain_id = sendConsPriceToAsnaServiceParam.chain_id;
            int asna_user_id = sendConsPriceToAsnaServiceParam.asna_user_id;

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = getAuthorizedUserResult.error
                };
                return errRes;
            }

            asna_user asna_user = getAuthorizedUserResult.asna_user;
            if (asna_user == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            string asna_legal_id = asna_user.asna_legal_id;
            if (String.IsNullOrWhiteSpace(asna_legal_id))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код ЮЛ по АСНА у пользователя с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            DateTime allowed_last_link_date = DateTime.Now.AddDays(-1);
            asna_link asna_link = dbContext.asna_link.Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted).OrderByDescending(ss => ss.link_id).FirstOrDefault();
            if ((asna_link == null) || (asna_link.crt_date <= allowed_last_link_date))
            {                
                // пытаемся обновить справочник связок
                AsnaServiceParam asnaServiceParam = new AsnaServiceParam() { asna_user_id = asna_user_id };
                var getGoodsLinksResult = GetGoodsLinks(asnaServiceParam);
                if ((getGoodsLinksResult.error != null) && (getGoodsLinksResult.error.id == (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION))
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = getGoodsLinksResult.error
                    };
                    return errRes;
                }
            }

            asna_price_chain asna_price_chain = dbContext.asna_price_chain.Where(ss => ss.asna_user_id == asna_user_id && ss.chain_id == chain_id && !ss.is_deleted).FirstOrDefault();
            if (asna_price_chain == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена цепочка с кодом " + chain_id.ToString() + " для пользователя с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            bool is_full = asna_price_chain.is_full;

            if (!asna_price_chain.price_date.HasValue)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для пользователя " + asna_user_id.ToString() + " по цепочке с кодом " + chain_id.ToString() + " не указана дата выгрузки прайс-листа"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            asna_price_batch_out asna_price_batch_out_existing = dbContext.asna_price_batch_out.Where(ss => ss.asna_user_id == asna_user_id && ss.in_chain_id == chain_id && !ss.is_deleted).FirstOrDefault();
            if (asna_price_batch_out_existing != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Для пользователя " + asna_user_id.ToString() + " по цепочке с кодом " + chain_id.ToString() + " уже есть отправка в АСНА с кодом " + asna_price_batch_out_existing.batch_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            List<vw_asna_price_row_linked> vw_asna_price_row_linked_list = dbContext.vw_asna_price_row_linked
                .Where(ss => ss.chain_id == chain_id && ss.is_unique)
                .ToList();
            if ((vw_asna_price_row_linked_list == null) || (vw_asna_price_row_linked_list.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены связанные строки прайс-листа для пользователя с кодом " + asna_user_id.ToString() + " по цепочке с кодом " + chain_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            asna_price_batch_out asna_price_batch_out_last = dbContext.asna_price_batch_out
                .Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted)
                .OrderByDescending(ss => ss.batch_num)
                .FirstOrDefault();

            asna_price_batch_out asna_price_batch_out = new asna_price_batch_out();
            asna_price_batch_out.asna_user_id = asna_user_id;
            asna_price_batch_out.batch_num = asna_price_batch_out_last != null ? asna_price_batch_out_last.batch_num + 1 : 1;
            asna_price_batch_out.row_cnt = vw_asna_price_row_linked_list.Count;
            asna_price_batch_out.price_date = asna_price_chain.price_date;
            asna_price_batch_out.is_full = asna_price_chain.is_full;
            asna_price_batch_out.is_active = true;
            asna_price_batch_out.in_chain_id = asna_price_chain.chain_id;
            asna_price_batch_out.crt_date = DateTime.Now;
            asna_price_batch_out.crt_user = user;
            asna_price_batch_out.upd_date = asna_price_batch_out.crt_date;
            asna_price_batch_out.upd_user = user;
            asna_price_batch_out.is_deleted = false;
            dbContext.asna_price_batch_out.Add(asna_price_batch_out);
            dbContext.SaveChanges();


            List<ConsPriceAsna> consPriceList = vw_asna_price_row_linked_list.Select(ss => new ConsPriceAsna()
            {
                sku = ss.asna_sku,
                nnt = ss.asna_nnt,
                sup_inn = ss.asna_sup_inn,
                prc_opt_nds = ss.asna_prc_opt_nds,
                prc_gnvls_max = ss.asna_prc_gnvls_max,
                status = ss.asna_status,
                exp = ss.asna_exp.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                sup_date = ss.asna_sup_date.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
                prcl_date = ss.asna_prcl_date.ToStringStrict("dd.MM.yyyy HH:mm:ss"),
            })
            .ToList();

            JsonSerializerSettings dateFormatSettings = new JsonSerializerSettings
            {
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                DateTimeZoneHandling = DateTimeZoneHandling.Local,
            };

            AsnaConsPriceParam asnaConsPriceParam = new AsnaConsPriceParam()
            {
                price_date = (DateTime)asna_price_chain.price_date,
                prices = new List<ConsPriceAsna>(consPriceList),
            };

            string asnaConsPriceParamStr = JsonConvert.SerializeObject(asnaConsPriceParam, dateFormatSettings);

            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_SendConsPrice);
            if (String.IsNullOrWhiteSpace(url))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан URL сервиса отправки сводных прайс-листов в АСНА WebAPI"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            url = url.Replace("{legalId}", asna_legal_id);

            string auth = asna_user.auth;            
            bool isResponseOk = true;
            string location = null;
            HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = is_full ? HttpMethod.Post : HttpMethod.Put,
                };

                request.Headers.Add("Cache-Control", "no-cache");

                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");

                request.Content = new StringContent(asnaConsPriceParamStr,
                                    Encoding.UTF8,
                                    "application/json");

                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var locationHeader = response.Headers.Where(ss => ss.Key == "Location").FirstOrDefault();
                        location = locationHeader.Value.Count() >= 0 ? string.Join(",", locationHeader.Value.ToArray()) : null;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            asna_price_batch_out.mess = location;
            asna_price_batch_out.is_active = false;
            asna_price_batch_out.inactive_date = DateTime.Now;
            asna_price_batch_out.upd_date = asna_price_batch_out.inactive_date;
            asna_price_batch_out.upd_user = user;
            dbContext.SaveChanges();

            var delResult = dbContext.Database.ExecuteSqlCommand("select und.send_price_to_asna_finalize(@p0, @p1);", asna_price_chain.chain_id, asna_price_batch_out.batch_id);
            
            return new AuEsnBaseResult() { result = 1, };
        }
        
        #endregion
    }
}
