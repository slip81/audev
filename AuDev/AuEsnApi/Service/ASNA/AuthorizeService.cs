﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net.Http;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region Authorize

        private GetAuthorizedUserResult GetAuthorizedUser(int asna_user_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getAuthorizedUser(asna_user_id);
            }
            catch (Exception ex)
            {
                string user = "asnabot";
                Loggly.InsertLog_Asna("Ошибка в GetAuthorizedUser: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetAuthorizedUserResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetAuthorizedUserResult getAuthorizedUser(int asna_user_id)
        {
            GetAuthorizedUserResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[GetAuthorizedUser] " : "";

            asna_user asna_user = dbContext.asna_user.Where(ss => ss.asna_user_id == asna_user_id && !ss.is_deleted).FirstOrDefault();
            if (asna_user == null)
            {
                errRes = new GetAuthorizedUserResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            if (!asna_user.is_active)
            {
                errRes = new GetAuthorizedUserResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Настройки с кодом " + asna_user_id.ToString() + " неактивны"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            
            bool needNewAuth = false;
            if ((!asna_user.auth_date_beg.HasValue) || (!asna_user.auth_date_end.HasValue))
            {
                needNewAuth = true;
            }
            else
            {
                if (!((asna_user.auth_date_beg <= now_initial) && (asna_user.auth_date_end >= now_initial.AddMinutes(1))))
                    needNewAuth = true;
            }
            if (String.IsNullOrWhiteSpace(asna_user.auth))
            {
                needNewAuth = true;
            }

            if (needNewAuth)
            {
                if (String.IsNullOrWhiteSpace(asna_user.asna_store_id))
                {
                    errRes = new GetAuthorizedUserResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код аптеки по АСНА"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }
                if (String.IsNullOrWhiteSpace(asna_user.asna_pwd))
                {
                    errRes = new GetAuthorizedUserResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан пароль аптеки по АСНА"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }
                var newAuth = authorize(new AsnaAuthServiceParam()
                {
                    asna_store_id = asna_user.asna_store_id,
                    asna_pwd = asna_user.asna_pwd
                });
                if (newAuth.error != null)
                {
                    asna_user.auth = null;
                    asna_user.auth_date_beg = null;
                    asna_user.auth_date_end = null;
                    dbContext.SaveChanges();
                    errRes = new GetAuthorizedUserResult()
                    {
                        error = newAuth.error,
                    };
                    return errRes;
                }
                if (newAuth.asna_auth == null)
                {
                    asna_user.auth = null;
                    asna_user.auth_date_beg = null;
                    asna_user.auth_date_end = null;
                    dbContext.SaveChanges();
                    errRes = new GetAuthorizedUserResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Результат авторизации: null"),
                    };
                    Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }

                asna_user.auth = newAuth.asna_auth.access_token;
                asna_user.auth_date_beg = newAuth.asna_auth.auth_date_beg;
                asna_user.auth_date_end = newAuth.asna_auth.auth_date_end;
                dbContext.SaveChanges();
                asna_user = dbContext.asna_user.AsNoTracking().Where(ss => ss.asna_user_id == asna_user_id).FirstOrDefault();                
            }

            return new GetAuthorizedUserResult() {  asna_user = asna_user, };

        }

        private AsnaAuthResult authorize(AsnaAuthServiceParam asnaAuthServiceParam)
        {
            AsnaAuthResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[Authorize] " : "";

            AsnaAuth asnaAuth = null;            

            string asna_store_id = asnaAuthServiceParam.asna_store_id;
            string asna_pwd = asnaAuthServiceParam.asna_pwd;

            string url = AsnaParam.GetServiceSelfUrl(AuEsnConst.ASNA_service_id_Auth);
            if (String.IsNullOrWhiteSpace(url))
            {
                errRes = new AsnaAuthResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден url для сервиса " + AuEsnConst.ASNA_service_id_Auth.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            bool isResponseOk = false;
            System.Net.HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Post,
                };
                
                request.Headers.Add("Cache-Control", "no-cache");

                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");

                request.Content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("client_id", asna_store_id),
                    new KeyValuePair<string, string>("client_secret", asna_pwd),
                    new KeyValuePair<string, string>("grant_type", "client_credentials"),
                });
                //request.Content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var jsonTask = response.Content.ReadAsAsync<AsnaAuth>();
                        jsonTask.Wait();                        
                        asnaAuth = jsonTask.Result;
                    });
                task.Wait();                
            }

            if (!isResponseOk)
            {
                errRes = new AsnaAuthResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int expires_in_int = 60;
            bool parseOk = int.TryParse(asnaAuth.expires_in, out expires_in_int);
            if (!parseOk)
                expires_in_int = 60; 
            asnaAuth.auth_date_beg = DateTime.Now;
            asnaAuth.auth_date_end = ((DateTime)asnaAuth.auth_date_beg).AddSeconds(expires_in_int);

            return new AsnaAuthResult() { asna_auth = asnaAuth };
        }
        
        #endregion
    }
}