﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net.Http;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Net.Http.Headers;
using System.Transactions;
using System.Net;

namespace AuEsnApi.Service
{
    public partial class AsnaService
    {
        #region GetConsPriceFromAsna

        public AuEsnBaseResult GetConsPriceFromAsna(GetConsPriceFromAsnaServiceParam getConsPriceFromAsnaServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getConsPriceFromAsna(getConsPriceFromAsnaServiceParam);
            }
            catch (Exception ex)
            {
                string user = "asnabot";
                Loggly.InsertLog_Asna("Ошибка в GetConsPriceFromAsna: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult getConsPriceFromAsna(GetConsPriceFromAsnaServiceParam getConsPriceFromAsnaServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[GetConsPriceFromAsna] " : "";

            if (getConsPriceFromAsnaServiceParam == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int asna_user_id = getConsPriceFromAsnaServiceParam.asna_user_id;            
            DateTime? price_date = null;

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = getAuthorizedUserResult.error
                };                
                return errRes;
            }

            asna_user asna_user = getAuthorizedUserResult.asna_user;
            if (asna_user == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            string asna_legal_id = asna_user.asna_legal_id;
            if (String.IsNullOrWhiteSpace(asna_legal_id))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код ЮЛ по АСНА у пользователя с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }


            string auth = asna_user.auth;

            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_GetConsPrice);
            if (String.IsNullOrWhiteSpace(url))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан URL сервиса получения сводных прайс-листов из АСНА WebAPI"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            url = url.Replace("{legalId}", asna_legal_id);
            if (price_date.HasValue)
            {
                /*
                JsonSerializerSettings dateFormatSettings = new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Local,
                };
                string price_date_str = JsonConvert.SerializeObject(price_date, dateFormatSettings);
                */
                string price_date_str = ((DateTime)price_date).ToUniversalTime().ToStringStrict("yyyy-MM-ddTHH:mm:ss"); // 2017-09-21T08:00:00
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["since"] = price_date_str;
                url = url + "?" + query.ToString();
            }


            List<ReceivedConsPriceAsna> prices = null;
            bool isResponseOk = false;
            HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = HttpMethod.Get,
                };

                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");
                
                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var jsonTask = response.Content.ReadAsAsync<List<ReceivedConsPriceAsna>>();
                        jsonTask.Wait();
                        prices = jsonTask.Result;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if ((prices == null) || (prices.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "От источника получены пустые данные"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            asna_price_row_actual asna_price_row_actual = null;
            int cnt = 0;
            AuMainDb transactionContext = null;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
            {
                transactionContext = null;
                try
                {
                    transactionContext = new AuMainDb();
                    transactionContext.Configuration.AutoDetectChangesEnabled = false;

                    cnt = 0;

                    foreach (var itemAdd in prices)
                    {
                        ++cnt;

                        asna_price_row_actual = new asna_price_row_actual();
                        asna_price_row_actual.asna_user_id = asna_user_id;                        

                        asna_price_row_actual.asna_sku = itemAdd.sku;
                        asna_price_row_actual.asna_sup_inn = itemAdd.sup_inn;
                        asna_price_row_actual.asna_prc_opt_nds = itemAdd.prc_opt_nds;
                        asna_price_row_actual.asna_prc_gnvls_max = itemAdd.prc_gnvls_max;
                        asna_price_row_actual.asna_status = itemAdd.status.GetValueOrDefault(0);
                        asna_price_row_actual.asna_exp = itemAdd.exp.ToDateTimeStrict("o");
                        asna_price_row_actual.asna_sup_date = itemAdd.sup_date.ToDateTimeStrict("o");
                        asna_price_row_actual.asna_prcl_date = itemAdd.prcl_date.ToDateTimeStrict("o");

                        asna_price_row_actual.crt_date = DateTime.Now;
                        asna_price_row_actual.crt_user = user;
                        asna_price_row_actual.upd_date = asna_price_row_actual.crt_date;
                        asna_price_row_actual.upd_user = user;
                        asna_price_row_actual.is_deleted = false;

                        transactionContext = AddToContext<asna_price_row_actual>(transactionContext, asna_price_row_actual, cnt, 100, true);
                    }

                    transactionContext.SaveChanges();
                }
                finally
                {
                    if (transactionContext != null)
                        transactionContext.Dispose();
                }

                scope.Complete();

            }

            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}