﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net.Http;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Net.Http.Headers;
using System.Transactions;
using System.Net;
using System.Globalization;

namespace AuEsnApi.Service
{
    /*
        1) минимальный интервал в заданиях = 30 мин
        2) интервал работы win-службы на веб-сервере = 10 мин
        3) методы SendRemains, SendRemainsToAsna = всегда возвращать результат OK
        4) не выдавать задание клиенту, если у него уже есть задание со статусом "Выдано клиенту" такого же типа
    */

    public partial class AsnaService
    {
        #region GetRemainsFromAsna

        public AuEsnBaseResult GetRemainsFromAsna(GetRemainsFromAsnaServiceParam getRemainsFromAsnaServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getRemainsFromAsna(getRemainsFromAsnaServiceParam);
            }
            catch (Exception ex)
            {
                string user = "asnabot";
                Loggly.InsertLog_Asna("Ошибка в GetRemainsFromAsna: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult getRemainsFromAsna(GetRemainsFromAsnaServiceParam getRemainsFromAsnaServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "asnabot";
            string errMess = AuEsnConst.ASNA_extended_messages ? "[GetRemainsFromAsna] " : "";

            if (getRemainsFromAsnaServiceParam == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            // !!!
            /*
            errRes = new AuEsnBaseResult()
            {
                error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Метод временно отключен"),
            };
            Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
            return errRes;
            */

            int asna_user_id = getRemainsFromAsnaServiceParam.asna_user_id;
            //DateTime? stock_date = getRemainsFromAsnaServiceParam.stock_date;
            DateTime? stock_date = null;

            var getAuthorizedUserResult = GetAuthorizedUser(asna_user_id);
            if (getAuthorizedUserResult.error != null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = getAuthorizedUserResult.error
                };                
                return errRes;
            }

            asna_user asna_user = getAuthorizedUserResult.asna_user;
            if (asna_user == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены настройки с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            string asna_store_id = asna_user.asna_store_id;
            if (String.IsNullOrWhiteSpace(asna_store_id))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код аптеки по АСНА у пользователя с кодом " + asna_user_id.ToString()),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }


            string auth = asna_user.auth;            

            string url = AsnaParam.GetServiceFullUrl(AuEsnConst.ASNA_service_id_GetRemains);
            if (String.IsNullOrWhiteSpace(url))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан URL сервиса получения остатков из АСНА WebAPI"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            url = url.Replace("{storeId}", asna_store_id);
            if (stock_date.HasValue)
            {
                /*
                JsonSerializerSettings dateFormatSettings = new JsonSerializerSettings
                {
                    DateFormatHandling = DateFormatHandling.IsoDateFormat,
                    DateTimeZoneHandling = DateTimeZoneHandling.Local,
                };
                string stock_date_str = JsonConvert.SerializeObject(stock_date, dateFormatSettings);
                */
                string stock_date_str = ((DateTime)stock_date).ToUniversalTime().ToStringStrict("yyyy-MM-ddTHH:mm:ss"); // 2017-09-21T08:00:00
                var query = HttpUtility.ParseQueryString(string.Empty);
                query["since"] = stock_date_str;
                url = url + "?" + query.ToString();
            }

            HttpMethod httpMethod = HttpMethod.Get;

            List<ReceivedRemainsAsna> remains = null;
            bool isResponseOk = false;
            HttpStatusCode responseStatusCode = System.Net.HttpStatusCode.OK;
            string responseErrMess = null;
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", auth);

                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(url),
                    Method = httpMethod,
                };

                // это хинт от АСНА - чтобы убрать ограничение на кол-во запросов с одного IP-адреса
                request.Headers.Add("X-ClientId", "manuscript");
                
                var task = httpClient.SendAsync(request)
                    .ContinueWith((taskwithmsg) =>
                    {
                        var response = taskwithmsg.Result;
                        isResponseOk = response.IsSuccessStatusCode_Asna();
                        if (!isResponseOk)
                        {
                            responseStatusCode = response.StatusCode;
                            //response.Headers.ToString();
                            responseErrMess = response.Content.ReadAsStringAsync().Result;
                            return;
                        }
                        var jsonTask = response.Content.ReadAsAsync<List<ReceivedRemainsAsna>>();
                        jsonTask.Wait();
                        remains = jsonTask.Result;
                    });
                task.Wait();
            }

            if (!isResponseOk)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Ошибка выполнения запроса [" + responseStatusCode.ToString() + "]" + (!String.IsNullOrWhiteSpace(responseErrMess) ? (" [" + responseErrMess + "]") : "")),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if ((remains == null) || (remains.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "От источника получены пустые данные"),
                };
                Loggly.InsertLog_Asna(errMess + errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            string mess2 = JsonConvert.SerializeObject(remains);
            if (mess2 == null)
                mess2 = "";
            mess2 += " [url: " + httpMethod.ToString() + " " + url + "]";
            /*
            if (mess2.Length >= 8000)
            {
                mess2 = "Длина строки превышает 8000 [" + mess2.Length.ToString() + "]";
                mess2 += " [url: " + httpMethod.ToString() + " " + url + "]";
            }
            */

            int row_cnt = remains.Count;

            // перед записью текущих остатокв из АСНА в БД - удаляем предыдущие остатки из БД
            dbContext.asna_stock_row_actual.RemoveRange(dbContext.asna_stock_row_actual.Where(ss => ss.asna_user_id == asna_user_id));
            dbContext.SaveChanges();

            asna_stock_row_actual asna_stock_row_actual = null;
            int cnt = 0;
            AuMainDb transactionContext = null;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
            {
                transactionContext = null;
                try
                {
                    transactionContext = new AuMainDb();
                    transactionContext.Configuration.AutoDetectChangesEnabled = false;

                    cnt = 0;

                    foreach (var itemAdd in remains)
                    {
                        ++cnt;

                        asna_stock_row_actual = new asna_stock_row_actual();
                        asna_stock_row_actual.asna_user_id = asna_user_id;
                        asna_stock_row_actual.firm_name = itemAdd.firm_name;
                        asna_stock_row_actual.asna_prt_id = itemAdd.prt_id;
                        asna_stock_row_actual.asna_sku = itemAdd.sku;
                        asna_stock_row_actual.asna_qnt = itemAdd.all_cnt;
                        asna_stock_row_actual.asna_doc = itemAdd.supplier_doc_num;
                        asna_stock_row_actual.asna_sup_inn = itemAdd.supplier_inn;
                        asna_stock_row_actual.asna_sup_id = null;
                        //asna_stock_row_actual.asna_sup_date = itemAdd.supplier_doc_date.ToDateTimeStrict("o");
                        asna_stock_row_actual.asna_sup_date = String.IsNullOrWhiteSpace(itemAdd.supplier_doc_date) ? null : (DateTime?)DateTime.Parse(itemAdd.supplier_doc_date, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
                        asna_stock_row_actual.asna_nds = itemAdd.percent_nds;
                        asna_stock_row_actual.asna_gnvls = itemAdd.is_vital;
                        asna_stock_row_actual.asna_dp = itemAdd.is_child_food;
                        asna_stock_row_actual.asna_man = itemAdd.firm_name;
                        asna_stock_row_actual.asna_series = itemAdd.series;
                        //asna_stock_row_actual.asna_exp = itemAdd.valid_date.ToDateTimeStrict("o");
                        asna_stock_row_actual.asna_exp = String.IsNullOrWhiteSpace(itemAdd.valid_date) ? null : (DateTime?)DateTime.Parse(itemAdd.valid_date, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
                        asna_stock_row_actual.asna_prc_man = itemAdd.price_firm;
                        asna_stock_row_actual.asna_prc_opt = itemAdd.price_gross;
                        asna_stock_row_actual.asna_prc_opt_nds = itemAdd.price_nds_gross;
                        asna_stock_row_actual.asna_prc_ret = itemAdd.price_retail;
                        asna_stock_row_actual.asna_prc_reestr = itemAdd.price_gr;
                        asna_stock_row_actual.asna_prc_gnvls_max = itemAdd.price_max_vital;
                        asna_stock_row_actual.asna_sum_opt = itemAdd.sum_gross;
                        asna_stock_row_actual.asna_sum_opt_nds = itemAdd.sum_nds_gross;
                        asna_stock_row_actual.asna_mr_gnvls = itemAdd.percent_vital;
                        asna_stock_row_actual.asna_tag = itemAdd.tag;
                        asna_stock_row_actual.asna_stock_date = itemAdd.asna_stock_date;
                        asna_stock_row_actual.asna_load_date = itemAdd.asna_load_date;
                        asna_stock_row_actual.asna_rv = itemAdd.asna_rv;
            
                        asna_stock_row_actual.crt_date = DateTime.Now;
                        asna_stock_row_actual.crt_user = user;
                        asna_stock_row_actual.upd_date = asna_stock_row_actual.crt_date;
                        asna_stock_row_actual.upd_user = user;
                        asna_stock_row_actual.is_deleted = false;

                        transactionContext = AddToContext<asna_stock_row_actual>(transactionContext, asna_stock_row_actual, cnt, 100, true);
                    }

                    transactionContext.SaveChanges();
                }
                finally
                {
                    if (transactionContext != null)
                        transactionContext.Dispose();
                }

                scope.Complete();

            }

            string logMess = "Получено строк: " + row_cnt.ToString();
            Loggly.InsertLog_Asna(logMess, (long)Enums.LogEsnType.ASNA_GET_REMAINS, user, null, now_initial, mess2);

            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}