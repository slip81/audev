﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuEsnApi.Models;
#endregion

namespace AuEsnApi.Service
{
    public interface IAsnaService : IInjectableService
    {
        AuEsnBaseResult GetStoreInfoAsna(AsnaServiceParam asnaServiceParam);
        GetStoreInfoResult GetStoreInfo(UserInfo getStoreInfoServiceParam);
        AuEsnBaseResult GetGoodsLinks(AsnaServiceParam asnaServiceParam);
        AuEsnBaseResult SendRemains(SendRemainsServiceParam sendRemainsServiceParam);
        AuEsnBaseResult ResetSendRemains(UserInfo resetSendRemainsServiceParam);
        AuEsnBaseResult SendRemainsToAsna(SendRemainsToAsnaServiceParam sendRemainsToAsnaServiceParam);
        AuEsnBaseResult DelRemainsFromAsna(AsnaServiceParam asnaServiceParam);
        AuEsnBaseResult GetRemainsFromAsna(GetRemainsFromAsnaServiceParam getRemainsFromAsnaServiceParam);
        GetOrdersResult GetOrders(GetOrdersServiceParam getOrdersServiceParam);
        AuEsnBaseResult SendOrders(SendOrdersServiceParam sendOrdersServiceParam);
        AuEsnBaseResult SendConsPrice(SendConsPriceServiceParam sendConsPriceServiceParam);
        AuEsnBaseResult ResetSendConsPrice(UserInfo resetSendConsPriceServiceParam);
        AuEsnBaseResult SendConsPriceToAsna(SendConsPriceToAsnaServiceParam sendConsPriceToAsnaServiceParam);
        AuEsnBaseResult GetConsPriceFromAsna(GetConsPriceFromAsnaServiceParam getConsPriceFromAsnaServiceParam);
        GetTaskStateResult GetTaskStateFromAsna(GetTaskStateFromAsnaServiceParam getTaskStateFromAsnaServiceParam);
    }
}
