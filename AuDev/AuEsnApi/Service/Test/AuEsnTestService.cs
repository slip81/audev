﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public class AuEsnTestService : EsnService, IAuEsnTestService
    {
        public AuEsnTestService()
        {
            //dbContext = new AuMainDb("AuMainDb");
            dbContext = new AuMainDb("AuTestDb");
        }

    }
}