﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuEsnApi.Models;
#endregion

namespace AuEsnApi.Service
{
    public interface IMainService : IInjectableService
    {
        GetMethodsDescrResult GetMethodsDescr(UserInfo userInfo);                           //      получение описания всех доступных методов
        CheckLicenseResult CheckLicense(CheckLicenseServiceParam checkLicenseServiceParam); //      проверка лицензии
        CheckLicenseResult CheckLicense(UserInfo userInfo, int serviceId);                  //      проверка лицензии
    }
}
