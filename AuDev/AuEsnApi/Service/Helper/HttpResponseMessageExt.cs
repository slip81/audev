﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using System.Net.Http;

namespace AuEsnApi.Service
{
    public static class HttpResponseMessageExt
    {
        public static bool IsSuccessStatusCode_Asna(this HttpResponseMessage responseMess)
        {
            switch (responseMess.StatusCode)
            {
                case HttpStatusCode.OK:
                case HttpStatusCode.Created:
                case HttpStatusCode.Accepted:
                case HttpStatusCode.NoContent:
                    return true;
                default:
                    return false;
            }
        }
    }
}