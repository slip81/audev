﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Net;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using System.Net.Http;

namespace AuEsnApi.Service
{
    public static class AsnaPrcGnvlsMaxCalcHelper
    {
        public static decimal? Calc(int tax_system_type_id, decimal limit1, decimal limit2, decimal limit3, decimal? price_firm, decimal? price_gross, int? percent_nds)
        {
            decimal curr_limit_value = 0;
            decimal curr_limit_multiplier = 1;

            if (price_firm > 500)
            {
                curr_limit_value = limit3;
            }
            else if (price_firm > 50)
            {
                curr_limit_value = limit2;
            }
            else
            {
                curr_limit_value = limit1;
            }

            switch ((Enums.TaxSystemTypeEnum)tax_system_type_id)
            {
                case Enums.TaxSystemTypeEnum.OSNO:
                    curr_limit_multiplier = 1 + ((decimal)percent_nds.GetValueOrDefault(0) / (decimal)100.0);
                    break;
                case Enums.TaxSystemTypeEnum.ENVD:
                case Enums.TaxSystemTypeEnum.USN:
                    curr_limit_multiplier = 1;
                    break;
                default:
                    curr_limit_multiplier = 1;
                    break;
            }

            return GlobalUtil.RoundDown(((price_firm.GetValueOrDefault(0) * (curr_limit_value / (decimal)100.0)) + price_gross.GetValueOrDefault(0)) * curr_limit_multiplier, 2);
        }
    }
}