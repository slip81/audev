﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region SendStockReport

        public AuEsnBaseResult SendStockReport(StockReportServiceParam stockReportServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                bool is_error = ((stockReportServiceParam != null) && (stockReportServiceParam.is_error.GetValueOrDefault(0) == 1));
                return toStockReport(stockReportServiceParam, is_error ? (long)Enums.LogEsnType.ERROR : (long)Enums.LogEsnType.STOCK_UPLOAD);
            }
            catch (Exception ex)
            {
                string user = stockReportServiceParam != null && !String.IsNullOrWhiteSpace(stockReportServiceParam.login) ? stockReportServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в SendStockReport: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };                
            }
        }

        private AuEsnBaseResult toStockReport(StockReportServiceParam stockReportServiceParam, long type_id)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = stockReportServiceParam != null && !String.IsNullOrWhiteSpace(stockReportServiceParam.login) ? stockReportServiceParam.login : "stock";

            var checkLicenseResult = mainService.CheckLicense(stockReportServiceParam, AuEsnConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;                
            }

            var checkStockVersionResult = CheckStockVersion(stockReportServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;            

            Loggly.InsertLog_Stock((String.IsNullOrWhiteSpace(stockReportServiceParam.mess) ? "пустое сообщение" : stockReportServiceParam.mess), type_id, user, null, DateTime.Now, null);
            
            return new AuEsnBaseResult() { result = 1, error = null, };
        }
        
        #endregion
    }
}