﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region GetDocStates

        public GetDocStatesResult GetDocStates(UserInfo getDocStatesServiceParam)
        {
            try
            {
                return getDocStates(getDocStatesServiceParam);
            }
            catch (Exception ex)
            {
                return new GetDocStatesResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetDocStatesResult getDocStates(UserInfo getDocStateServiceParam)
        {            
            var checkLicenseResult = mainService.CheckLicense(getDocStateServiceParam, AuEsnConst.license_id_UND);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                return new GetDocStatesResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
            }

            return new GetDocStatesResult()
            {
                states = new List<DocState>()
                    {
                       new DocState() { doc_state_id = 1, doc_state_name = "Загружен"},
                       new DocState() { doc_state_id = 2, doc_state_name = "В очереди"},
                       new DocState() { doc_state_id = 3, doc_state_name = "Обрабатывается"},
                       new DocState() { doc_state_id = 4, doc_state_name = "Обработан"},
                    }
            };
        }
        
        #endregion
    }
}