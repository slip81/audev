﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region GetDoc

        public GetDocResult GetDoc(GetDocServiceParam getDocServiceParam)
        {
            try
            {
                return getDoc(getDocServiceParam);
            }
            catch (Exception ex)
            {
                return new GetDocResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetDocResult getDoc(GetDocServiceParam getDocServiceParam)
        {
            var checkLicenseResult = mainService.CheckLicense(getDocServiceParam, AuEsnConst.license_id_UND);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                return new GetDocResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
            }

            return new GetDocResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован") };
        }
        
        #endregion
    }
}