﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region ResetSendStock

        public AuEsnBaseResult ResetSendStock(UserInfo resetSendStockServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return resetSendStock(resetSendStockServiceParam);
            }
            catch (Exception ex)
            {
                string user = resetSendStockServiceParam != null && !String.IsNullOrWhiteSpace(resetSendStockServiceParam.login) ? resetSendStockServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в ResetSendStock: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult resetSendStock(UserInfo resetSendStockServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = resetSendStockServiceParam != null && !String.IsNullOrWhiteSpace(resetSendStockServiceParam.login) ? resetSendStockServiceParam.login : "stock";
            
            var checkLicenseResult = mainService.CheckLicense(resetSendStockServiceParam, AuEsnConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            var checkStockVersionResult = CheckStockVersion(resetSendStockServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            List<stock> stock_list = dbContext.stock.Where(ss => ss.sales_id == sales_id && !ss.is_deleted).ToList();
            if ((stock_list == null) || (stock_list.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_ACTIVE_CHAIN_NOT_FOUND, "Не найдена активная цепочка пакетов отправки наличия"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            bool somethingChanged = false;
            int cnt = 0;
            List<stock_batch> stock_batch_list = null;
            foreach (var stock in stock_list)
            {
                stock_batch_list = dbContext.stock_batch.Where(ss => ss.stock_id == stock.stock_id && ss.is_active && !ss.is_deleted).ToList();
                if ((stock_batch_list != null) && (stock_batch_list.Count > 0))
                {
                    somethingChanged = true;
                    foreach (var stock_batch in stock_batch_list)
                    {
                        stock_batch.is_deleted = true;
                        stock_batch.del_date = DateTime.Now;
                        stock_batch.del_user = user;
                        cnt++;
                    }
                }
            }
            
            if (somethingChanged)
            {
                dbContext.SaveChanges();
            }
            else
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_ACTIVE_CHAIN_NOT_FOUND, "Не найдена активная цепочка пакетов отправки наличия"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            Loggly.InsertLog_Stock("Сброшена активная цепочка пакетов отправки наличия [" + cnt.ToString() + "]", (long)Enums.LogEsnType.STOCK_DOWNLOAD, user, null, DateTime.Now, null);
            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}