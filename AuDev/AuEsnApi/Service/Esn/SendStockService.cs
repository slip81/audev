﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region SendStock

        public AuEsnBaseResult SendStock(SendStockServiceParam sendStockServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendStock(sendStockServiceParam);
            }
            catch (Exception ex)
            {
                string user = sendStockServiceParam != null && !String.IsNullOrWhiteSpace(sendStockServiceParam.login) ? sendStockServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в SendStock: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
                //return new AuEsnBaseResult() { result = 1 };
            }
        }

        private AuEsnBaseResult sendStock(SendStockServiceParam sendStockServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = sendStockServiceParam != null && !String.IsNullOrWhiteSpace(sendStockServiceParam.login) ? sendStockServiceParam.login : "stock";

            // !!!           
            /*
            errRes = new AuEsnBaseResult()
            {
                error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Метод временно отключен"),
            };
            Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
            //return errRes;
            return new AuEsnBaseResult() { result = 1 };
            */

            var checkLicenseResult = mainService.CheckLicense(sendStockServiceParam, AuEsnConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                //return new AuEsnBaseResult() { result = 1 };
            }

            var checkStockVersionResult = CheckStockVersion(sendStockServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            if ((sendStockServiceParam.rows == null) || (sendStockServiceParam.rows.Count <= 0))
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Нет данных о наличии"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                //return new AuEsnBaseResult() { result = 1 };
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            int depart_id = sendStockServiceParam.depart_id;
            int part_cnt = sendStockServiceParam.part_cnt;
            int part_num = sendStockServiceParam.part_num;
            int row_cnt = sendStockServiceParam.rows.Count;

            if (part_num <= 0)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_UNCORRECT_BATCH_NUM, "Неверный номер пакета (" + part_num.ToString() + ")"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                //return new AuEsnBaseResult() { result = 1 };
            }

            if (part_num > part_cnt)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_WRONG_BATCH_NUM, "Неверный номер пакета (" + part_num.ToString() + " из " + part_cnt.ToString() + ")"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                //return new AuEsnBaseResult() { result = 1 };
            }

            stock stock = dbContext.stock.Where(ss => ss.sales_id == sales_id && ss.depart_id == depart_id && !ss.is_deleted).FirstOrDefault();
            if (stock == null)
            {
                stock = new stock();
                stock.client_id = client_id;
                stock.depart_id = depart_id;
                stock.depart_name = String.IsNullOrWhiteSpace(sendStockServiceParam.depart_name) ? ("Отделение " + depart_id.ToString()) : sendStockServiceParam.depart_name.Trim();
                stock.pharmacy_name = String.IsNullOrWhiteSpace(sendStockServiceParam.pharmacy_name) ? ("Аптека " + sales_id.ToString()) : sendStockServiceParam.pharmacy_name.Trim();
                stock.depart_address = String.IsNullOrWhiteSpace(sendStockServiceParam.depart_address) ? ("Отделение " + depart_id.ToString()) : sendStockServiceParam.depart_address.Trim();
                stock.sales_id = sales_id;
                stock.crt_date = DateTime.Now;
                stock.crt_user = user;
                stock.upd_date = stock.crt_date;
                stock.upd_user = user;
                stock.is_deleted = false;
                dbContext.stock.Add(stock);
                dbContext.SaveChanges();
            }
            else
            {
                stock.depart_name = String.IsNullOrWhiteSpace(sendStockServiceParam.depart_name) ? ("Отделение " + depart_id.ToString()) : sendStockServiceParam.depart_name.Trim();
                stock.pharmacy_name = String.IsNullOrWhiteSpace(sendStockServiceParam.pharmacy_name) ? ("Аптека " + sales_id.ToString()) : sendStockServiceParam.pharmacy_name.Trim();
                stock.depart_address = String.IsNullOrWhiteSpace(sendStockServiceParam.depart_address) ? ("Отделение " + depart_id.ToString()) : sendStockServiceParam.depart_address.Trim();
                stock.upd_date = DateTime.Now;
                stock.upd_user = user;
            }

            stock_batch stock_batch_existing = dbContext.stock_batch.Where(ss => ss.stock_id == stock.stock_id && ss.is_active && !ss.is_deleted && ss.is_confirmed && ss.is_new_version)
                .OrderByDescending(ss => ss.part_num).FirstOrDefault();
            if (stock_batch_existing != null)
            {
                if ((part_cnt != stock_batch_existing.part_cnt) || (part_num != stock_batch_existing.part_num + 1))
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_WRONG_BATCH_NUM, "Неверный номер пакета. Предыдущий отправленный пакет: " + stock_batch_existing.part_num.GetValueOrDefault(0).ToString() + " из " + stock_batch_existing.part_cnt.GetValueOrDefault(0).ToString()
                            + ", пришедший пакет: " + part_num.ToString() + " из " + part_cnt.ToString()),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                    //return new AuEsnBaseResult() { result = 1 };
                }
            }
            else
            {
                if (part_num != 1)
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_WRONG_BATCH_NUM, "Неверный номер пакета. Ожидается пакет с номером 1, пришедший пакет: " + part_num.ToString() + " из " + part_cnt.ToString()),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                    //return new AuEsnBaseResult() { result = 1 };
                }

                stock_batch stock_batch_last_confirmed = dbContext.stock_batch.Where(ss => ss.stock_id == stock.stock_id && !ss.is_deleted && ss.is_new_version).OrderByDescending(ss => ss.batch_num).FirstOrDefault();
                if ((stock_batch_last_confirmed != null) && (!stock_batch_last_confirmed.is_confirmed))
                {
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_PREVIOUS_CHAIN_NOT_CONFIRMED, "Предыдущая цепочка отправки не подтверждена"),
                    };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }
            }

            stock_batch stock_batch_last = dbContext.stock_batch.Where(ss => ss.stock_id == stock.stock_id && !ss.is_deleted && ss.is_confirmed && ss.is_new_version).OrderByDescending(ss => ss.batch_num).FirstOrDefault();
            
            stock_batch stock_batch = new stock_batch();
            stock_batch.batch_num = stock_batch_last != null ? stock_batch_last.batch_num + 1 : 1;
            stock_batch.is_active = true;
            stock_batch.inactive_date = null;
            stock_batch.part_cnt = part_cnt;
            stock_batch.part_num = part_num;
            stock_batch.row_cnt = row_cnt;
            stock_batch.stock_id = stock.stock_id;            
            stock_batch.crt_date = DateTime.Now;
            stock_batch.crt_user = user;
            stock_batch.upd_date = stock_batch.crt_date;
            stock_batch.upd_user = user;
            stock_batch.is_deleted = false;
            stock_batch.is_confirmed = false;
            stock_batch.is_new_version = true;
            dbContext.stock_batch.Add(stock_batch);
            dbContext.SaveChanges();

            stock_row stock_row = null;
            stock_row stock_row_existing = null;            
            List<Stock> stock_rows_existing = new List<Stock>();
            List<Stock> stock_rows_missing = new List<Stock>();
            int cntNew_orig = sendStockServiceParam.rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.ADDED).Count();
            int cntRemoved_orig = sendStockServiceParam.rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.REMOVED).Count();
            int cntChanged_orig = sendStockServiceParam.rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.CHANGED).Count();

            // !!!
            // загружаем в память весь список stock_row по текущему stock_id
            List<stock_row> stock_row_existing_list = dbContext.stock_row.Where(ss => ss.stock_id == stock.stock_id && !ss.is_deleted).ToList();

            // 1. добавление новых строк (state = 0)
            // дополнительно проверяем - не добавляем строки, если в БД уже есть с такими же значениями stock_id, row_stock_id, artikul
            // все такие найденные строки четвертым шагом будут обработаны как измененные (state = 2)
            int cntNew = 0;
            AuMainDb transactionContext = null;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
            {
                transactionContext = null;
                try
                {
                    transactionContext = new AuMainDb();
                    transactionContext.Configuration.AutoDetectChangesEnabled = false;

                    cntNew = 0;
                    foreach (var itemAdd in sendStockServiceParam.rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.ADDED))
                    {
                        // !!!
                        // это добавляет сильных тормозов, если это надо - то лучше сначала закачать в List все нужные stock_row и делать поиск локально
                        // stock_row_existing = transactionContext.stock_row.Where(ss => ss.stock_id == stock.stock_id && ss.row_stock_id == itemAdd.stock_id && ss.artikul == itemAdd.artikul && ss.state != (int)Enums.UndStockRowStateEnum.REMOVED && !ss.is_deleted).FirstOrDefault();
                        stock_row_existing = stock_row_existing_list.Where(ss => ss.row_stock_id == itemAdd.stock_id && ss.artikul == itemAdd.artikul && ss.state != (int)Enums.UndStockRowStateEnum.REMOVED).FirstOrDefault();
                        if (stock_row_existing != null)
                        {
                            stock_rows_existing.Add(itemAdd);
                            continue;
                        }

                        cntNew = cntNew + 1;

                        stock_row = new stock_row();
                        stock_row.stock_id = stock.stock_id;
                        stock_row.batch_id = stock_batch.batch_id;                        
                        stock_row.state = (int)Enums.UndStockRowStateEnum.ADDED;

                        stock_row.all_cnt = itemAdd.all_cnt;
                        stock_row.artikul = itemAdd.artikul;
                        stock_row.barcode = itemAdd.barcode;                        
                        stock_row.country_name = itemAdd.country_name;
                        stock_row.esn_id = itemAdd.esn_id;
                        stock_row.farm_group_name = itemAdd.farm_group_name;
                        stock_row.firm_id = itemAdd.firm_id;
                        stock_row.firm_name = itemAdd.firm_name;
                        stock_row.gr_date = String.IsNullOrWhiteSpace(itemAdd.gr_date) ? null : (DateTime?)(DateTime.ParseExact(itemAdd.gr_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);
                        stock_row.is_vital = itemAdd.is_vital == 1;
                        stock_row.mess = itemAdd.mess;
                        stock_row.percent = itemAdd.percent;
                        stock_row.percent_gross = itemAdd.percent_gross;
                        stock_row.percent_nds_gross = itemAdd.percent_nds_gross;
                        stock_row.prep_id = itemAdd.prep_id;
                        stock_row.prep_name = itemAdd.prep_name;
                        stock_row.price = itemAdd.price;
                        stock_row.price_firm = itemAdd.price_firm;
                        stock_row.price_gr = itemAdd.price_gr;
                        stock_row.price_gross = itemAdd.price_gross;
                        stock_row.price_nds_gross = itemAdd.price_nds_gross;
                        stock_row.profit = itemAdd.profit;
                        stock_row.row_stock_id = itemAdd.stock_id;
                        stock_row.series = itemAdd.series;                        
                        stock_row.sum = itemAdd.sum;
                        stock_row.sum_gross = itemAdd.sum_gross;
                        stock_row.sum_nds_gross = itemAdd.sum_nds_gross;
                        stock_row.supplier_doc_date = String.IsNullOrWhiteSpace(itemAdd.supplier_doc_date) ? null : (DateTime?)(DateTime.ParseExact(itemAdd.supplier_doc_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);
                        stock_row.supplier_doc_num = itemAdd.supplier_doc_num;
                        stock_row.supplier_name = itemAdd.supplier_name;
                        stock_row.unpacked_cnt = itemAdd.unpacked_cnt;
                        stock_row.valid_date = String.IsNullOrWhiteSpace(itemAdd.valid_date) ? null : (DateTime?)(DateTime.ParseExact(itemAdd.valid_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);

                        stock_row.crt_date = DateTime.Now;
                        stock_row.crt_user = user;
                        stock_row.upd_date = stock_row.crt_date;
                        stock_row.upd_user = user;
                        stock_row.is_deleted = false;

                        transactionContext = AddToContext<stock_row>(transactionContext, stock_row, cntNew, 100, true);
                    }

                    transactionContext.SaveChanges();
                }
                finally
                {
                    if (transactionContext != null)
                        transactionContext.Dispose();
                }

                scope.Complete();
            }

            // 2. удаление строк (state = 1)
            int cntRemoved = 0;
            bool somethingRemoved = false;
            foreach (var itemDel in sendStockServiceParam.rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.REMOVED))
            {
                // !!!
                // это добавляет сильных тормозов, если это надо - то лучше сначала закачать в List все нужные stock_row и делать поиск локально
                // stock_row = dbContext.stock_row.Where(ss => ss.stock_id == stock.stock_id && ss.row_stock_id == itemDel.stock_id && ss.artikul == itemDel.artikul && !ss.is_deleted).FirstOrDefault();
                stock_row = stock_row_existing_list.Where(ss => ss.row_stock_id == itemDel.stock_id && ss.artikul == itemDel.artikul).FirstOrDefault();
                if (stock_row == null)
                {
                    stock_rows_missing.Add(itemDel);                    
                    continue;
                }

                cntRemoved = cntRemoved + 1;

                // !!!
                // stock_row.state = (int)Enums.UndStockRowStateEnum.REMOVED;
                // stock_row.batch_id = stock_batch.batch_id;
                stock_row.new_state = (int)Enums.UndStockRowStateEnum.REMOVED;
                stock_row.new_batch_id = stock_batch.batch_id;

                stock_row.upd_date = DateTime.Now;
                stock_row.upd_user = user;

                somethingRemoved = true;
            }
            if (somethingRemoved)
                dbContext.SaveChanges();

            // 3. изменение существующих строк (state = 2)
            int cntChanged = 0;
            bool somethingChanged = false;
            foreach (var itemChange in sendStockServiceParam.rows.Where(ss => ss.state == (int)Enums.UndStockRowStateEnum.CHANGED))
            {
                // !!!
                // это добавляет сильных тормозов, если это надо - то лучше сначала закачать в List все нужные stock_row и делать поиск локально
                // stock_row = dbContext.stock_row.Where(ss => ss.stock_id == stock.stock_id && ss.row_stock_id == itemChange.stock_id && ss.artikul == itemChange.artikul && ss.state != (int)Enums.UndStockRowStateEnum.REMOVED && !ss.is_deleted).FirstOrDefault();
                stock_row = stock_row_existing_list.Where(ss => ss.row_stock_id == itemChange.stock_id && ss.artikul == itemChange.artikul && ss.state != (int)Enums.UndStockRowStateEnum.REMOVED).FirstOrDefault();                
                if (stock_row == null)
                {
                    stock_rows_missing.Add(itemChange);
                    continue;
                }

                cntChanged = cntChanged + 1;

                //stock_row.state = (int)Enums.UndStockRowStateEnum.CHANGED;
                //stock_row.batch_id = stock_batch.batch_id;
                //stock_row.all_cnt = itemChange.all_cnt;

                // !!!
                stock_row.new_state = (int)Enums.UndStockRowStateEnum.CHANGED;
                stock_row.new_batch_id = stock_batch.batch_id;
                stock_row.new_all_cnt = itemChange.all_cnt;

                stock_row.artikul = itemChange.artikul;
                stock_row.barcode = itemChange.barcode;
                stock_row.country_name = itemChange.country_name;
                stock_row.esn_id = itemChange.esn_id;
                stock_row.farm_group_name = itemChange.farm_group_name;
                stock_row.firm_id = itemChange.firm_id;
                stock_row.firm_name = itemChange.firm_name;
                stock_row.gr_date = String.IsNullOrWhiteSpace(itemChange.gr_date) ? null : (DateTime?)(DateTime.ParseExact(itemChange.gr_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);
                stock_row.is_vital = itemChange.is_vital == 1;
                stock_row.mess = itemChange.mess;
                stock_row.percent = itemChange.percent;
                stock_row.percent_gross = itemChange.percent_gross;
                stock_row.percent_nds_gross = itemChange.percent_nds_gross;
                stock_row.prep_id = itemChange.prep_id;
                stock_row.prep_name = itemChange.prep_name;
                stock_row.price = itemChange.price;
                stock_row.price_firm = itemChange.price_firm;
                stock_row.price_gr = itemChange.price_gr;
                stock_row.price_gross = itemChange.price_gross;
                stock_row.price_nds_gross = itemChange.price_nds_gross;
                stock_row.profit = itemChange.profit;
                stock_row.row_stock_id = itemChange.stock_id;
                stock_row.series = itemChange.series;
                stock_row.state = itemChange.state;
                stock_row.sum = itemChange.sum;
                stock_row.sum_gross = itemChange.sum_gross;
                stock_row.sum_nds_gross = itemChange.sum_nds_gross;
                stock_row.supplier_doc_date = String.IsNullOrWhiteSpace(itemChange.supplier_doc_date) ? null : (DateTime?)(DateTime.ParseExact(itemChange.supplier_doc_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);
                stock_row.supplier_doc_num = itemChange.supplier_doc_num;
                stock_row.supplier_name = itemChange.supplier_name;
                stock_row.unpacked_cnt = itemChange.unpacked_cnt;
                stock_row.valid_date = String.IsNullOrWhiteSpace(itemChange.valid_date) ? null : (DateTime?)(DateTime.ParseExact(itemChange.valid_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);

                stock_row.upd_date = DateTime.Now;
                stock_row.upd_user = user;

                somethingChanged = true;
            }

            // 4. изменения строк из шага 1, помеченных как измененные
            if (stock_rows_existing.Count > 0)
            {
                foreach (var itemChange in stock_rows_existing)
                {
                    // !!!
                    // это добавляет сильных тормозов, если это надо - то лучше сначала закачать в List все нужные stock_row и делать поиск локально
                    // stock_row = dbContext.stock_row.Where(ss => ss.stock_id == stock.stock_id && ss.row_stock_id == itemChange.stock_id && ss.artikul == itemChange.artikul && ss.state != (int)Enums.UndStockRowStateEnum.REMOVED && !ss.is_deleted).FirstOrDefault();
                    stock_row = stock_row_existing_list.Where(ss => ss.row_stock_id == itemChange.stock_id && ss.artikul == itemChange.artikul && ss.state != (int)Enums.UndStockRowStateEnum.REMOVED).FirstOrDefault();                    
                    if (stock_row == null)
                        continue;

                    cntChanged = cntChanged + 1;

                    // !!!
                    //stock_row.state = (int)Enums.UndStockRowStateEnum.CHANGED;
                    //stock_row.batch_id = stock_batch.batch_id;
                    //stock_row.all_cnt = itemChange.all_cnt;

                    stock_row.new_state = (int)Enums.UndStockRowStateEnum.CHANGED;                    
                    stock_row.new_batch_id = stock_batch.batch_id;
                    stock_row.new_all_cnt = itemChange.all_cnt;
                    
                    stock_row.artikul = itemChange.artikul;
                    stock_row.barcode = itemChange.barcode;
                    stock_row.country_name = itemChange.country_name;
                    stock_row.esn_id = itemChange.esn_id;
                    stock_row.farm_group_name = itemChange.farm_group_name;
                    stock_row.firm_id = itemChange.firm_id;
                    stock_row.firm_name = itemChange.firm_name;
                    stock_row.gr_date = String.IsNullOrWhiteSpace(itemChange.gr_date) ? null : (DateTime?)(DateTime.ParseExact(itemChange.gr_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);
                    stock_row.is_vital = itemChange.is_vital == 1;
                    stock_row.mess = itemChange.mess;
                    stock_row.percent = itemChange.percent;
                    stock_row.percent_gross = itemChange.percent_gross;
                    stock_row.percent_nds_gross = itemChange.percent_nds_gross;
                    stock_row.prep_id = itemChange.prep_id;
                    stock_row.prep_name = itemChange.prep_name;
                    stock_row.price = itemChange.price;
                    stock_row.price_firm = itemChange.price_firm;
                    stock_row.price_gr = itemChange.price_gr;
                    stock_row.price_gross = itemChange.price_gross;
                    stock_row.price_nds_gross = itemChange.price_nds_gross;
                    stock_row.profit = itemChange.profit;
                    stock_row.row_stock_id = itemChange.stock_id;
                    stock_row.series = itemChange.series;
                    stock_row.state = itemChange.state;
                    stock_row.sum = itemChange.sum;
                    stock_row.sum_gross = itemChange.sum_gross;
                    stock_row.sum_nds_gross = itemChange.sum_nds_gross;
                    stock_row.supplier_doc_date = String.IsNullOrWhiteSpace(itemChange.supplier_doc_date) ? null : (DateTime?)(DateTime.ParseExact(itemChange.supplier_doc_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);
                    stock_row.supplier_doc_num = itemChange.supplier_doc_num;
                    stock_row.supplier_name = itemChange.supplier_name;
                    stock_row.unpacked_cnt = itemChange.unpacked_cnt;
                    stock_row.valid_date = String.IsNullOrWhiteSpace(itemChange.valid_date) ? null : (DateTime?)(DateTime.ParseExact(itemChange.valid_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);

                    stock_row.upd_date = DateTime.Now;
                    stock_row.upd_user = user;

                    somethingChanged = true;
                }
            }

            // 5. добавление строк из шагов 2,3, не найденных в БД
            if (stock_rows_missing.Count > 0)
            {
                transactionContext = null;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 30, 0)))
                {
                    transactionContext = null;
                    try
                    {
                        transactionContext = new AuMainDb();
                        transactionContext.Configuration.AutoDetectChangesEnabled = false;
                        
                        foreach (var itemAdd in stock_rows_missing)
                        {
                            //cntNew = cntNew + 1;
                            if (itemAdd.state == (int)Enums.UndStockRowStateEnum.REMOVED)
                            {
                                cntChanged = cntChanged + 1;
                            }
                            else if (itemAdd.state == (int)Enums.UndStockRowStateEnum.CHANGED)
                            {
                                cntRemoved = cntRemoved + 1;
                            }

                            stock_row = new stock_row();
                            stock_row.stock_id = stock.stock_id;
                            stock_row.batch_id = stock_batch.batch_id;
                            stock_row.state = itemAdd.state;

                            stock_row.all_cnt = itemAdd.all_cnt;
                            stock_row.artikul = itemAdd.artikul;
                            stock_row.barcode = itemAdd.barcode;
                            stock_row.country_name = itemAdd.country_name;
                            stock_row.esn_id = itemAdd.esn_id;
                            stock_row.farm_group_name = itemAdd.farm_group_name;
                            stock_row.firm_id = itemAdd.firm_id;
                            stock_row.firm_name = itemAdd.firm_name;
                            stock_row.gr_date = String.IsNullOrWhiteSpace(itemAdd.gr_date) ? null : (DateTime?)(DateTime.ParseExact(itemAdd.gr_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);
                            stock_row.is_vital = itemAdd.is_vital == 1;
                            stock_row.mess = itemAdd.mess;
                            stock_row.percent = itemAdd.percent;
                            stock_row.percent_gross = itemAdd.percent_gross;
                            stock_row.percent_nds_gross = itemAdd.percent_nds_gross;
                            stock_row.prep_id = itemAdd.prep_id;
                            stock_row.prep_name = itemAdd.prep_name;
                            stock_row.price = itemAdd.price;
                            stock_row.price_firm = itemAdd.price_firm;
                            stock_row.price_gr = itemAdd.price_gr;
                            stock_row.price_gross = itemAdd.price_gross;
                            stock_row.price_nds_gross = itemAdd.price_nds_gross;
                            stock_row.profit = itemAdd.profit;
                            stock_row.row_stock_id = itemAdd.stock_id;
                            stock_row.series = itemAdd.series;
                            stock_row.sum = itemAdd.sum;
                            stock_row.sum_gross = itemAdd.sum_gross;
                            stock_row.sum_nds_gross = itemAdd.sum_nds_gross;
                            stock_row.supplier_doc_date = String.IsNullOrWhiteSpace(itemAdd.supplier_doc_date) ? null : (DateTime?)(DateTime.ParseExact(itemAdd.supplier_doc_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);
                            stock_row.supplier_doc_num = itemAdd.supplier_doc_num;
                            stock_row.supplier_name = itemAdd.supplier_name;
                            stock_row.unpacked_cnt = itemAdd.unpacked_cnt;
                            stock_row.valid_date = String.IsNullOrWhiteSpace(itemAdd.valid_date) ? null : (DateTime?)(DateTime.ParseExact(itemAdd.valid_date, "dd.MM.yyyy", System.Globalization.CultureInfo.CurrentCulture).Date);

                            stock_row.crt_date = DateTime.Now;
                            stock_row.crt_user = user;
                            stock_row.upd_date = stock_row.crt_date;
                            stock_row.upd_user = user;
                            stock_row.is_deleted = false;

                            transactionContext = AddToContext<stock_row>(transactionContext, stock_row, cntNew, 100, true);
                        }

                        transactionContext.SaveChanges();
                    }
                    finally
                    {
                        if (transactionContext != null)
                            transactionContext.Dispose();
                    }

                    scope.Complete();
                }
            }

            if (somethingChanged)
                dbContext.SaveChanges();

            Loggly.InsertLog_Stock("Отправлено строк: " + row_cnt.ToString() + " [новых " + cntNew_orig.ToString() + "(" + cntNew.ToString() + ") , измененных " + cntChanged_orig.ToString() + "(" + cntChanged.ToString() + "), удаленных " + cntRemoved_orig.ToString() + "(" + cntRemoved.ToString() + ")] [пакет " + part_num.ToString() + "/" + part_cnt.ToString() + "]", (long)Enums.LogEsnType.STOCK_UPLOAD, user, stock_batch.batch_id, DateTime.Now, null);
            // Отправлено строк: 653 [новых 4(2), измененных 2(2), удаленных 455(211)] [пакет 1/2]
            
            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}