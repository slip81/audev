﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region GetDocState

        public GetDocStateResult GetDocState(GetDocServiceParam getDocStateServiceParam)
        {
            try
            {
                return getDocState(getDocStateServiceParam);
            }
            catch (Exception ex)
            {
                return new GetDocStateResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetDocStateResult getDocState(GetDocServiceParam getDocStateServiceParam)
        {            
            var checkLicenseResult = mainService.CheckLicense(getDocStateServiceParam, AuEsnConst.license_id_UND);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                return new GetDocStateResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
            }

            return new GetDocStateResult() { state = new DocState() { doc_state_id = 1, doc_state_name = "Загружен" } };
        }
        
        #endregion
    }
}