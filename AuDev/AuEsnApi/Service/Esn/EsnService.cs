﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService : AuEsnBaseService, IEsnService
    {
        private IMainService mainService
        {
            get
            {
                if (_mainService == null)
                {
                    _mainService = GetInjectedService<IMainService>();
                }
                return _mainService;
            }
        }
        private IMainService _mainService;

        public EsnService()
            :base()
        {
            //
        }
    }
}