﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region GetDocLog

        public GetDocLogResult GetDocLog(GetDocLogServiceParam getDocLogServiceParam)
        {
            try
            {
                return getDocLog(getDocLogServiceParam);
            }
            catch (Exception ex)
            {
                return new GetDocLogResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetDocLogResult getDocLog(GetDocLogServiceParam getDocLogServiceParam)
        {            
            var checkLicenseResult = mainService.CheckLicense(getDocLogServiceParam, AuEsnConst.license_id_UND);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                return new GetDocLogResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
            }
            
            return new GetDocLogResult()
            {
                logs = new List<DocLog>()
                    {
                        new DocLog() { log_id = 1, doc_id = 1, doc_name = "№1", doc_date = "12.06.2017", log_date = "12.06.2017 12:21", mess = "Документ отправлен в ЕСН"},
                        new DocLog() { log_id = 2, doc_id = 1, doc_name = "№1", doc_date = "12.06.2017", log_date = "12.06.2017 14:21", mess = "Документ обработан"},
                        new DocLog() { log_id = 3, doc_id = 1, doc_name = "№1", doc_date = "12.06.2017", log_date = "12.06.2017 14:45", mess = "Документ получен из ЕСН"},
                    }
            };
        }
        
        #endregion
    }
}