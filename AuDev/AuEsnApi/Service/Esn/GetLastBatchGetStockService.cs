﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region GetLastBatchGetStock

        public GetLastBatchStockResult GetLastBatchGetStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getLastBatchGetStock(getLastBatchStockServiceParam);
            }
            catch (Exception ex)
            {
                string user = getLastBatchStockServiceParam != null && !String.IsNullOrWhiteSpace(getLastBatchStockServiceParam.login) ? getLastBatchStockServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в GetLastBatchGetStock: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetLastBatchStockResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };                
            }
        }

        private GetLastBatchStockResult getLastBatchGetStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam)
        {
            GetLastBatchStockResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = getLastBatchStockServiceParam != null && !String.IsNullOrWhiteSpace(getLastBatchStockServiceParam.login) ? getLastBatchStockServiceParam.login : "stock";

            var checkLicenseResult = mainService.CheckLicense(getLastBatchStockServiceParam, AuEsnConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new GetLastBatchStockResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;                
            }

            var checkStockVersionResult = CheckStockVersion(getLastBatchStockServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new GetLastBatchStockResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            stock_sales_download stock_sales_download_active = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && ss.is_active && !ss.is_deleted && !ss.is_fake)
                .OrderByDescending(ss => ss.part_num).FirstOrDefault();
            if (stock_sales_download_active == null)
            {
                errRes = new GetLastBatchStockResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_ACTIVE_CHAIN_NOT_FOUND, "Не найдена текущая активая цепочка получения пакетов"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            stock_sales_download stock_sales_download_last_confirmed = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && ss.is_active && !ss.is_deleted && ss.is_confirmed && !ss.is_fake)
                .OrderByDescending(ss => ss.part_num).FirstOrDefault();

            int part_cnt = stock_sales_download_active.part_cnt.GetValueOrDefault(0);
            int part_num = stock_sales_download_last_confirmed == null ? 0 : stock_sales_download_last_confirmed.part_num.GetValueOrDefault(0);


            Loggly.InsertLog_Stock("Запрошен последний подтвержденный полученный пакет [" + part_num.ToString() + " из " + part_cnt.ToString() + "]", (long)Enums.LogEsnType.STOCK_DOWNLOAD, user, null, DateTime.Now, null);
            
            return new GetLastBatchStockResult() { result = 1, part_cnt = part_cnt, part_num = part_num, };
        }
        
        #endregion
    }
}