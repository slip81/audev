﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region SendDoc

        public AuEsnBaseResult SendDoc(SendDocServiceParam sendDocServiceParam)
        {
            try
            {
                return sendDoc(sendDocServiceParam);
            }
            catch (Exception ex)
            {
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult sendDoc(SendDocServiceParam sendDocServiceParam)
        {
            var checkLicenseResult = mainService.CheckLicense(sendDocServiceParam, AuEsnConst.license_id_UND);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                return new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
            }

            //return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован") };
            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}