﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region DelStockService

        public AuEsnBaseResult DelStock(DelStockServiceParam delStockServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return delStock(delStockServiceParam);
            }
            catch (Exception ex)
            {
                string user = delStockServiceParam != null && !String.IsNullOrWhiteSpace(delStockServiceParam.login) ? delStockServiceParam.login : "stock";
                //string user = "stock";
                Loggly.InsertLog_Stock("Ошибка в DelStock: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);                
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult delStock(DelStockServiceParam delStockServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = delStockServiceParam != null && !String.IsNullOrWhiteSpace(delStockServiceParam.login) ? delStockServiceParam.login : "stock";

            var checkLicenseResult = mainService.CheckLicense(delStockServiceParam, AuEsnConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            var checkStockVersionResult = CheckStockVersion(delStockServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int sales_id = delStockServiceParam.sales_id;
            vw_sales sales = dbContext.vw_sales.Where(ss => ss.sales_id == sales_id && ss.is_deleted != 1).FirstOrDefault();
            if (sales == null)
            {
                errRes = new AuEsnBaseResult() { error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Не найдена ТТ с кодом " + sales_id.ToString()) };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            string salesName = String.IsNullOrWhiteSpace(sales.adress) ? (String.IsNullOrWhiteSpace(sales.name) ? ("Код ТТ " + sales_id.ToString()) : sales.name) : sales.adress;

            string departName = "";
            int? depart_id = delStockServiceParam.depart_id;
            if (depart_id.GetValueOrDefault(0) > 0)
            {
                stock stock = dbContext.stock.Where(ss => ss.sales_id == sales_id && ss.depart_id == depart_id && !ss.is_deleted).FirstOrDefault();
                if (stock == null)
                {
                    errRes = new AuEsnBaseResult() { error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Не найдено подразделение с кодом " + depart_id.ToString() + " по ТТ с кодом " + sales_id.ToString()) };
                    Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
                }
                departName = stock.depart_name;
            }

            string salesAndDepartName = "ТТ: " + salesName + (String.IsNullOrWhiteSpace(departName) ? "" : (", подразделение: " + departName));

            string queryStr = "select * from und.del_stock(" + sales_id.ToString();
            if (depart_id.GetValueOrDefault(0) > 0)
                queryStr = queryStr + ", " + depart_id.ToString();
            else
                queryStr = queryStr + ", null::integer";
            queryStr = queryStr + ")";

            var delResult = dbContext.Database.SqlQuery<long>(queryStr).FirstOrDefault();

            Loggly.InsertLog_Stock("Удалено строк [" + salesAndDepartName + "] [sales_id: " + sales.sales_id.ToString()  + "]: " + delResult.ToString(), (long)Enums.LogEsnType.STOCK_DEL, user, sales_id, DateTime.Now, null);            
            
            return new AuEsnBaseResult() { result = 1 };
        }
        
        #endregion
    }
}