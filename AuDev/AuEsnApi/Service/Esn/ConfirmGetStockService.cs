﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
#endregion

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region ConfirmGetStock
       

        public AuEsnBaseResult ConfirmGetStock(ConfirmStockServiceParam confirmStockServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return confirmGetStock(confirmStockServiceParam);
            }
            catch (Exception ex)
            {
                string user = confirmStockServiceParam != null && !String.IsNullOrWhiteSpace(confirmStockServiceParam.login) ? confirmStockServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в ConfirmGetStock: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
                //return new AuEsnBaseResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
            }
        }

        private AuEsnBaseResult confirmGetStock(ConfirmStockServiceParam confirmStockServiceParam)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = confirmStockServiceParam != null && !String.IsNullOrWhiteSpace(confirmStockServiceParam.login) ? confirmStockServiceParam.login : "stock";

            var checkLicenseResult = mainService.CheckLicense(confirmStockServiceParam, AuEsnConst.license_id_STOCK);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
            }

            var checkStockVersionResult = CheckStockVersion(confirmStockServiceParam);
            if ((checkStockVersionResult == null) || (checkStockVersionResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkStockVersionResult != null ? checkStockVersionResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_WRONG_VERSION_NUMBER, "Неверный номер версии") };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;                
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;
            
            int part_num = confirmStockServiceParam.part_num;

            if (part_num <= 0)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_UNCORRECT_BATCH_NUM, "Неверный номер пакета (" + part_num.ToString() + ")"),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
            }

            stock_sales_download stock_sales_download_for_confirm = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && ss.part_num == part_num && ss.is_active && !ss.is_deleted && !ss.is_confirmed).OrderByDescending(ss => ss.part_num).FirstOrDefault();
            if (stock_sales_download_for_confirm == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_ESN_BATCH_FOR_CONFIRM_NOT_FOUND, "Не найден пакет для подтверждения с номером " + part_num.ToString()),
                };
                Loggly.InsertLog_Stock(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
                //return new GetStockResult() { result = 1, part_cnt = 1, rows = new List<Stock>(), };
            }

            DateTime inactive_date = DateTime.Now;

            stock_sales_download_for_confirm.is_confirmed = true;
            stock_sales_download_for_confirm.confirm_date = inactive_date;            

            // это перенесено из GetStock
            if (stock_sales_download_for_confirm.part_cnt == part_num)
            {
                // !!!
                /*
                var clearBatchResult = dbContext.Database.SqlQuery<DBNull>("select und.clear_vw_stock_row_tmp("
                    + sales_id.ToString()
                    + ")")
                    .FirstOrDefault();
                */

                List<stock_sales_download> stock_sales_download_list = dbContext.stock_sales_download.Where(ss => ss.sales_id == sales_id && ss.is_active && !ss.is_deleted).ToList();
                if ((stock_sales_download_list != null) && (stock_sales_download_list.Count > 0))
                {                    
                    foreach (var stock_sales_download_list_item in stock_sales_download_list)
                    {
                        stock_sales_download_list_item.is_confirmed = true;
                        stock_sales_download_list_item.confirm_date = inactive_date;
                        stock_sales_download_list_item.is_active = false;
                        stock_sales_download_list_item.inactive_date = inactive_date;
                        stock_sales_download_list_item.upd_date = inactive_date;
                        stock_sales_download_list_item.upd_user = user;
                    }                    
                }
            }

            dbContext.SaveChanges();

            Loggly.InsertLog_Stock("Подтвержден полученный пакет " + part_num.ToString(), (long)Enums.LogEsnType.STOCK_DOWNLOAD, user, stock_sales_download_for_confirm.download_id, DateTime.Now, null);

            return new AuEsnBaseResult() { result = 1, };
        }

        #endregion
    }
}