﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {

        #region CheckLicense

        public CheckLicenseResult CheckLicense(CheckLicenseServiceParam checkLicenseServiceParam)
        {
            try
            {
                return сheckLicense(checkLicenseServiceParam);
            }
            catch (Exception ex)
            {
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        public CheckLicenseResult CheckLicense(UserInfo userInfo, int serviceId = AuEsnConst.license_id_UND)
        {
            try
            {
                CheckLicenseServiceParam checkLicenseServiceParam = new CheckLicenseServiceParam()
                {
                    login = userInfo.login,
                    workplace = userInfo.workplace,
                    version_num = userInfo.version_num,
                    service_id = serviceId,
                };
                return сheckLicense(checkLicenseServiceParam);
            }
            catch (Exception ex)
            {
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private CheckLicenseResult сheckLicense(CheckLicenseServiceParam checkLicenseServiceParam)
        {
            DateTime now = DateTime.Now;

            if (checkLicenseServiceParam == null)
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не заданы входные параметры") };

            string login = checkLicenseServiceParam.login;
            string workplace = checkLicenseServiceParam.workplace;
            string version_num = checkLicenseServiceParam.version_num;
            int serviceId = checkLicenseServiceParam.service_id.HasValue ? (int)checkLicenseServiceParam.service_id : AuEsnConst.license_id_UND;

            if (String.IsNullOrEmpty(login))
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан логин") };

            if (String.IsNullOrEmpty(workplace))
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан идентификатор рабочего места") };

            if (String.IsNullOrEmpty(version_num))
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан номер версии") };

            workplace user_workplace = (from t1 in dbContext.vw_client_user
                        from t2 in dbContext.workplace
                        where t1.is_active == 1
                        && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                        && t2.sales_id == t1.sales_id
                        && t2.is_deleted != 1
                        && t2.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                        select t2)
                       .FirstOrDefault();

            if (user_workplace == null)
            {
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь с логином " + login.Trim() + " и рабочим местом " + workplace.Trim()) };
            }

            string version_name_start = new string(version_num.Take(4).ToArray());
            var license = (from t1 in dbContext.vw_client_service
                           where t1.workplace_id == user_workplace.id
                           && t1.service_id == serviceId
                           && t1.version_name != null && t1.version_name.StartsWith(version_name_start)
                           select t1)
                          .FirstOrDefault();            
            if (license == null)
            {
                return new CheckLicenseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена лицензия: версия " + version_num.Trim() + ", логин: " + login.Trim() + ", рабочее место: " + workplace.Trim()) };
            }

            return new CheckLicenseResult() { result = 1, client_id = license.client_id, sales_id = license.sales_id, workplace_id = license.workplace_id, };
        }
        
        #endregion
    }
}