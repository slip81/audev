﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class EsnService
    {
        #region GetStockReport

        public AuEsnBaseResult GetStockReport(StockReportServiceParam stockReportServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                bool is_error = ((stockReportServiceParam != null) && (stockReportServiceParam.is_error.GetValueOrDefault(0) == 1));
                return toStockReport(stockReportServiceParam, is_error ? (long)Enums.LogEsnType.ERROR : (long)Enums.LogEsnType.STOCK_DOWNLOAD);
            }
            catch (Exception ex)
            {
                string user = stockReportServiceParam != null && !String.IsNullOrWhiteSpace(stockReportServiceParam.login) ? stockReportServiceParam.login : "stock";
                Loggly.InsertLog_Stock("Ошибка в GetStockReport: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };                
            }
        }
   
        #endregion
    }
}