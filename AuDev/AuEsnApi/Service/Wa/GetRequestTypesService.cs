﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class WaService
    {
        #region GetRequestTypes

        public GetRequestTypesResult GetRequestTypes(GetRequestTypesServiceParam getRequestTypesServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getRequestTypes(getRequestTypesServiceParam);
            }
            catch (Exception ex)
            {
                string user = getRequestTypesServiceParam != null && !String.IsNullOrWhiteSpace(getRequestTypesServiceParam.login) ? getRequestTypesServiceParam.login : "esn";
                Loggly.InsertLog_Asna("Ошибка в GetRequestTypes: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetRequestTypesResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetRequestTypesResult getRequestTypes(GetRequestTypesServiceParam getRequestTypesServiceParam)
        {
            GetRequestTypesResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = getRequestTypesServiceParam != null && !String.IsNullOrWhiteSpace(getRequestTypesServiceParam.login) ? getRequestTypesServiceParam.login : "esn";
            int service_id = getRequestTypesServiceParam != null ? getRequestTypesServiceParam.service_id : 0;
            
            var checkLicenseResult = mainService.CheckLicense(getRequestTypesServiceParam, service_id);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new GetRequestTypesResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int request_type_group_id = -1;
            switch ((Enums.ClientServiceEnum)service_id)
            {
                case Enums.ClientServiceEnum.ASNA:
                    request_type_group_id = (int)Enums.WaRequestTypeGroup1Enum.ASNA;
                    break;
                case Enums.ClientServiceEnum.UND:
                    request_type_group_id = (int)Enums.WaRequestTypeGroup1Enum.UND;
                    break;
                case Enums.ClientServiceEnum.STOCK:
                    request_type_group_id = (int)Enums.WaRequestTypeGroup1Enum.STOCK;
                    break;
                default:
                    break;
            }

            List<wa_request_type> res = dbContext.wa_request_type.Where(ss => ss.request_type_group1_id == request_type_group_id).ToList();

            return new GetRequestTypesResult()
            {
                types = ((res == null) || (res.Count <= 0)) ? new List<RequestType>() : res.Select(ss => new RequestType()
                {
                    request_type_id = ss.request_type_id,
                    request_type_name = ss.request_type_name,                
                }).ToList(),
            };
        }
        
        #endregion
    }
}
