﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class WaService
    {
        #region GetTasks

        public GetTasksResult GetTasks(GetTasksServiceParam getTasksServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return getTasks(getTasksServiceParam);
            }
            catch (Exception ex)
            {
                string user = getTasksServiceParam != null && !String.IsNullOrWhiteSpace(getTasksServiceParam.login) ? getTasksServiceParam.login : "esn";
                Loggly.InsertLog_Asna("Ошибка в GetTasks: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new GetTasksResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetTasksResult getTasks(GetTasksServiceParam getTasksServiceParam)
        {
            GetTasksResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = getTasksServiceParam != null && !String.IsNullOrWhiteSpace(getTasksServiceParam.login) ? getTasksServiceParam.login : "esn";
            int service_id = getTasksServiceParam != null ? getTasksServiceParam.service_id : 0;
            
            var checkLicenseResult = mainService.CheckLicense(getTasksServiceParam, service_id);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new GetTasksResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            int request_type_group_id = 0;
            switch ((Enums.ClientServiceEnum)service_id)
            {
                case Enums.ClientServiceEnum.ASNA:
                    request_type_group_id = (int)Enums.WaRequestTypeGroup1Enum.ASNA;
                    break;
                case Enums.ClientServiceEnum.UND:
                    request_type_group_id = (int)Enums.WaRequestTypeGroup1Enum.UND;
                    break;
                case Enums.ClientServiceEnum.STOCK:
                    request_type_group_id = (int)Enums.WaRequestTypeGroup1Enum.STOCK;
                    break;
                default:
                    break;
            }

            List<wa_task> res = (from t1 in dbContext.wa_task
                                 from t2 in dbContext.wa_request_type
                                 where t1.request_type_id == t2.request_type_id
                                 //&& t1.workplace_id == workplace_id
                                 && ((t1.workplace_id == workplace_id) || ((!t1.workplace_id.HasValue) && (t1.sales_id == sales_id)))
                                 && t2.request_type_group1_id == request_type_group_id
                                 //&& t1.task_state_id == (int)Enums.WaTaskStateEnum.ISSUED_CLIENT
                                 && ((t1.task_state_id == (int)Enums.WaTaskStateEnum.ISSUED_CLIENT) || (t1.task_state_id == (int)Enums.WaTaskStateEnum.HANDLING_CLIENT))
                                 select t1)
                                 .ToList();

            return new GetTasksResult()
            {
                tasks = ((res == null) || (res.Count <= 0)) ? new List<RequestTask>() : res.Select(ss => new RequestTask()
                {
                    task_id = ss.task_id,
                    request_type_id = ss.request_type_id,
                    task_state_id = ss.task_state_id,                    
                    ord = ss.ord_client,
                    is_auto_created = ss.is_auto_created ? 1 : 0,
                })
                .OrderBy(ss => ss.task_id)
                .ToList(),
            };
        }
        
        #endregion
    }
}
