﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuEsnApi.Models;

namespace AuEsnApi.Service
{
    public partial class WaService
    {
        public UpdateTaskResult CreateTask(string user, int request_type_id, int task_state_id, int? client_id, int? sales_id, int? workplace_id, 
            bool is_auto_created, string mess, string param = null)
        {
            using (var dbContext = new AuMainDb())
            {
                try
                {
                    DateTime now = DateTime.Now;

                    wa_task wa_task_last = dbContext.wa_task.OrderByDescending(ss => ss.ord).FirstOrDefault();
                    int ord = wa_task_last != null ? wa_task_last.ord + 1 : 1;
                    wa_task_last = dbContext.wa_task.Where(ss => ss.client_id == client_id).OrderByDescending(ss => ss.ord).FirstOrDefault();
                    int ord_client = wa_task_last != null ? wa_task_last.ord_client + 1 : 1;

                    wa_task wa_task = new wa_task();
                    wa_task.request_type_id = request_type_id;
                    wa_task.task_state_id = task_state_id;
                    wa_task.client_id = client_id;
                    wa_task.sales_id = sales_id;
                    wa_task.workplace_id = workplace_id;
                    wa_task.ord = ord;
                    wa_task.ord_client = ord_client;
                    wa_task.is_auto_created = is_auto_created;
                    wa_task.mess = mess;
                    wa_task.handling_server_cnt = null;
                    wa_task.state_date = now;
                    wa_task.state_user = user;
                    wa_task.crt_date = wa_task.state_date;
                    wa_task.crt_user = user;
                    wa_task.upd_date = wa_task.state_date;
                    wa_task.upd_user = user;
                    wa_task.is_deleted = false;
                    dbContext.wa_task.Add(wa_task);

                    wa_task_lc wa_task_lc = new wa_task_lc();
                    wa_task_lc.wa_task = wa_task;
                    wa_task_lc.task_state_id = wa_task.task_state_id;
                    wa_task_lc.state_date = now;
                    wa_task_lc.state_user = user;
                    wa_task_lc.mess = mess;
                    wa_task_lc.crt_date = wa_task.state_date;
                    wa_task_lc.crt_user = user;
                    wa_task_lc.upd_date = wa_task.state_date;
                    wa_task_lc.upd_user = user;
                    wa_task_lc.is_deleted = false;
                    dbContext.wa_task_lc.Add(wa_task_lc);

                    if (!String.IsNullOrWhiteSpace(param))
                    {
                        wa_task_param wa_task_param = new wa_task_param();
                        wa_task_param.wa_task = wa_task;
                        wa_task_param.task_param = param;
                        dbContext.wa_task_param.Add(wa_task_param);
                    }

                    dbContext.SaveChanges();

                    return new UpdateTaskResult()
                    {
                        task_id = wa_task.task_id,
                        task_state_id = wa_task.task_state_id,
                        request_type_id = wa_task.request_type_id,
                        workplace_id = wa_task.workplace_id,
                        handling_server_cnt = wa_task.handling_server_cnt,
                    };
                }
                catch (Exception ex)
                {
                    return new UpdateTaskResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, ex)
                    };
                }
            }
        }

        public UpdateTaskResult UpdateTask(string user, int task_id, int task_state_id, string mess)
        {
            using (var dbContext = new AuMainDb())
            {
                try
                {
                    DateTime now = DateTime.Now;

                    wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
                    if (wa_task == null)
                        return new UpdateTaskResult()
                        {
                            error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдено задание с кодом " + task_id.ToString())
                        };

                    if (wa_task.task_state_id == task_state_id)
                        return new UpdateTaskResult()
                        {
                            task_id = wa_task.task_id,
                            task_state_id = wa_task.task_state_id,
                            request_type_id = wa_task.request_type_id,
                            workplace_id = wa_task.workplace_id,
                            handling_server_cnt = wa_task.handling_server_cnt,
                        };

                    wa_task.task_state_id = task_state_id;
                    wa_task.handling_server_cnt = (Enums.WaTaskStateEnum)task_state_id == Enums.WaTaskStateEnum.HANDLING_SERVER ? 
                        wa_task.handling_server_cnt.GetValueOrDefault(0) + 1 : wa_task.handling_server_cnt;
                    wa_task.state_date = now;
                    wa_task.state_user = user;
                    wa_task.mess = mess;
                    wa_task.upd_date = now;
                    wa_task.upd_user = user;

                    wa_task_lc wa_task_lc = new wa_task_lc();
                    wa_task_lc.wa_task = wa_task;
                    wa_task_lc.task_state_id = task_state_id;                    
                    wa_task_lc.state_date = now;
                    wa_task_lc.state_user = user;
                    wa_task_lc.mess = mess;
                    wa_task_lc.crt_date = now;
                    wa_task_lc.crt_user = user;
                    wa_task_lc.upd_date = now;
                    wa_task_lc.upd_user = user;
                    wa_task_lc.is_deleted = false;
                    dbContext.wa_task_lc.Add(wa_task_lc);

                    dbContext.SaveChanges();

                    return new UpdateTaskResult()
                    {
                        task_id = wa_task.task_id,
                        task_state_id = wa_task.task_state_id,
                        request_type_id = wa_task.request_type_id,
                        workplace_id = wa_task.workplace_id,
                        handling_server_cnt = wa_task.handling_server_cnt,
                    };
                }
                catch (Exception ex)
                {
                    return new UpdateTaskResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, ex)
                    };
                }
            }
        }

        public UpdateTaskResult UpdateTaskParam(string user, int task_id, string param)
        {
            using (var dbContext = new AuMainDb())
            {
                try
                {
                    DateTime now = DateTime.Now;

                    wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
                    if (wa_task == null)
                        return new UpdateTaskResult()
                        {
                            error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдено задание с кодом " + task_id.ToString())
                        };

                    wa_task_param wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == task_id).FirstOrDefault();
                    if (wa_task_param != null)
                    {
                        wa_task_param.task_param = param;
                    }
                    else                    
                    {
                        if (!String.IsNullOrWhiteSpace(param))
                        {
                            wa_task_param = new wa_task_param();
                            wa_task_param.wa_task = wa_task;
                            wa_task_param.task_param = param;
                            dbContext.wa_task_param.Add(wa_task_param);
                        }
                    }

                    dbContext.SaveChanges();

                    return new UpdateTaskResult()
                    {
                        task_id = wa_task.task_id,
                        task_state_id = wa_task.task_state_id,
                        request_type_id = wa_task.request_type_id,
                        workplace_id = wa_task.workplace_id,
                        handling_server_cnt = wa_task.handling_server_cnt,
                    };
                }
                catch (Exception ex)
                {
                    return new UpdateTaskResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, ex)
                    };
                }
            }
        }
    }
}