﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;

namespace AuEsnApi.Service
{
    public partial class WaService
    {
        public long CreateRequest(string user, int wa_id, int request_type_id, int? client_id, int? sales_id, int? workplace_id
            ,bool is_async, int? task_id, string mess)
        {
            using (var dbContext = new AuMainDb())
            {
                try
                {
                    DateTime now = DateTime.Now;

                    wa_request wa_request_last = dbContext.wa_request.OrderByDescending(ss => ss.ord).FirstOrDefault();
                    int ord = wa_request_last != null ? wa_request_last.ord + 1 : 1;
                    wa_request_last = dbContext.wa_request.Where(ss => ss.client_id == client_id).OrderByDescending(ss => ss.ord).FirstOrDefault();
                    int ord_client = wa_request_last != null ? wa_request_last.ord_client + 1 : 1;

                    wa_request wa_request = new wa_request();
                    wa_request.wa_id = wa_id;
                    wa_request.request_type_id = request_type_id;
                    wa_request.request_state_id = (int)Enums.WaRequestStateEnum.WAITING;
                    wa_request.client_id = client_id;
                    wa_request.sales_id = sales_id;
                    wa_request.workplace_id = workplace_id;
                    wa_request.ord = ord;
                    wa_request.ord_client = ord_client;
                    wa_request.is_async = is_async;
                    wa_request.task_id = task_id;
                    wa_request.state_date = now;
                    wa_request.state_user = user;
                    wa_request.crt_date = wa_request.state_date;
                    wa_request.crt_user = user;
                    wa_request.upd_date = wa_request.state_date;
                    wa_request.upd_user = user;
                    wa_request.is_deleted = false;
                    dbContext.wa_request.Add(wa_request);

                    wa_request_lc wa_request_lc = new wa_request_lc();
                    wa_request_lc.wa_request = wa_request;
                    wa_request_lc.request_state_id = wa_request.request_state_id;
                    wa_request_lc.is_current = true;
                    wa_request_lc.state_date = now;
                    wa_request_lc.state_user = user;
                    wa_request_lc.mess = mess;
                    wa_request_lc.crt_date = wa_request.state_date;
                    wa_request_lc.crt_user = user;
                    wa_request_lc.upd_date = wa_request.state_date;
                    wa_request_lc.upd_user = user;
                    wa_request_lc.is_deleted = false;
                    dbContext.wa_request_lc.Add(wa_request_lc);

                    if (task_id.HasValue)
                    {
                        wa_task wa_task = dbContext.wa_task.Where(ss => ss.task_id == task_id).FirstOrDefault();
                        if (wa_task != null)
                        {
                            wa_task.task_state_id = (int)Enums.WaTaskStateEnum.HANDLING_SERVER;
                            wa_task.state_date = wa_request.state_date;
                            wa_task.state_user = user;
                            wa_task.upd_date = wa_request.state_date;
                            wa_task.upd_user = user;

                            wa_task_lc wa_task_lc = new wa_task_lc();
                            wa_task_lc.task_id = wa_task.task_id;
                            wa_task_lc.task_state_id = wa_task.task_state_id;
                            wa_task_lc.mess = null;
                            wa_task_lc.state_date = wa_task.state_date;
                            wa_task_lc.state_user = wa_task.state_user;
                            wa_task_lc.crt_date = wa_task.state_date;
                            wa_task_lc.crt_user = wa_task.state_user;
                            wa_task_lc.upd_date = wa_task.state_date;
                            wa_task_lc.upd_user = wa_task.state_user;
                            dbContext.wa_task_lc.Add(wa_task_lc);
                        }
                    }

                    // !!!
                    // wa_request_log ?

                    dbContext.SaveChanges();

                    return wa_request.request_id;
                }
                catch
                {
                    return -1;
                }
            }
        }

        public bool UpdateRequest(string user, long request_id, int request_state_id, string mess)
        {
            using (var dbContext = new AuMainDb())
            {
                try
                {
                    DateTime now = DateTime.Now;

                    wa_request wa_request = dbContext.wa_request.Where(ss => ss.request_id == request_id).FirstOrDefault();
                    if (wa_request == null)
                        return false;

                    wa_request.request_state_id = request_state_id;
                    wa_request.state_date = now;
                    wa_request.state_user = user;
                    wa_request.upd_date = now;
                    wa_request.upd_user = user;

                    wa_request_lc wa_request_lc_current = dbContext.wa_request_lc
                        .Where(ss => ss.request_id == request_id && ss.is_current && !ss.is_deleted)
                        .OrderByDescending(ss => ss.request_lc_id)
                        .FirstOrDefault();
                    if (wa_request_lc_current != null)
                    {
                        wa_request_lc_current.is_current = false;
                        wa_request_lc_current.upd_date = now;
                        wa_request_lc_current.upd_user = user;
                    }

                    wa_request_lc wa_request_lc = new wa_request_lc();
                    wa_request_lc.wa_request = wa_request;
                    wa_request_lc.request_state_id = request_state_id;
                    wa_request_lc.is_current = true;
                    wa_request_lc.state_date = now;
                    wa_request_lc.state_user = user;
                    wa_request_lc.mess = mess;
                    wa_request_lc.crt_date = now;
                    wa_request_lc.crt_user = user;
                    wa_request_lc.upd_date = now;
                    wa_request_lc.upd_user = user;
                    wa_request_lc.is_deleted = false;
                    dbContext.wa_request_lc.Add(wa_request_lc);

                    // !!!
                    // wa_request_log ?

                    dbContext.SaveChanges();

                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}