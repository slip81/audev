﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using System.Transactions;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;
using System.Threading.Tasks;
using System.Threading;

namespace AuEsnApi.Service
{
    public partial class WaService
    {
        #region CreateWorker

        public AuEsnBaseResult CreateWorker(CreateWorkerServiceParam createWorkerServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;

            // !!!
            // test
            /*
            IEsnService esnService = GetInjectedService<IEsnService>();
            if (esnService == null)
            {
                log_date_beg = DateTime.Now.AddSeconds(10);
            }
            */

            try
            {
                return createWorker(createWorkerServiceParam.pwd, createWorkerServiceParam.log_enabled == 1);
            }
            catch (Exception ex)
            {
                string user = "wa_worker";
                Loggly.InsertLog_Wa("Ошибка в CreateWorker: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult createWorker(string pwd, bool log_enabled)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = "wa_worker";

            if (pwd != "iguana")
            {                
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нет прав"),
                };
                if (log_enabled)
                    Loggly.InsertLog_Wa(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            wa_worker wa_worker = new wa_worker();
            wa_worker.is_active = true;
            wa_worker.crt_date = now_initial;
            wa_worker.crt_user = user;
            wa_worker.upd_date = wa_worker.crt_date;
            wa_worker.upd_user = user;
            wa_worker.is_deleted = false;
            dbContext.wa_worker.Add(wa_worker);
            dbContext.SaveChanges();

            string logMess = "Обработка начата в " + now_initial.ToString("dd.MM.yyyy HH:mm:ss");
            if (log_enabled)
                Loggly.InsertLog_Wa(logMess, (long)Enums.LogEsnType.WA_START_WORKER, user, null, DateTime.Now, null);

            Task task = new Task(() =>
            {
                ExecMethods(
                    new List<Func<int, bool, AuEsnBaseResult>>() 
                    { 
                        // 1. создание wa_task на основе расписаний
                        Worker_CreateTasks_bySchedules, 
                        // 2. выполнение wa_task
                        Worker_ExecTasks,
                        // 3. закрытие wa_worker
                        Worker_FinishWork,
                    }, 
                    wa_worker.worker_id,
                    log_enabled
                );
            }
            );
            task.Start();

            return new AuEsnBaseResult() { result = 1 };
        }

        private void ExecMethods(IEnumerable<Func<int, bool, AuEsnBaseResult>> methods, int arg1, bool arg2)
        {
            foreach (var method in methods)
            {
                method(arg1, arg2);
            }
        }

        private AuEsnBaseResult Worker_CreateTasks_bySchedules(int worker_id, bool log_enabled)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return worker_CreateTasks_bySchedules(worker_id, log_enabled);
            }
            catch (Exception ex)
            {
                string user = "wa_worker";
                if (log_enabled)
                    Loggly.InsertLog_Wa("Ошибка в Worker_CreateTasks_bySchedules: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult worker_CreateTasks_bySchedules(int worker_id, bool log_enabled)
        {            
            DateTime now_initial = DateTime.Now;
            string user = "wa_worker";            

            using (var dbContext = new AuMainDb())
            {                
                List<vw_asna_user> vw_asna_user_list = dbContext.vw_asna_user
                    .Where(ss => ss.is_active && !ss.is_deleted
                    && ((ss.sch_full_is_active == true) || (ss.sch_ch_is_active == true))
                    )
                    .ToList();
                if ((vw_asna_user_list == null) || (vw_asna_user_list.Count <= 0))
                {
                    if (log_enabled)
                        Loggly.InsertLog_Wa("Не найдены активные пользователи с включенным расписанием", (long)Enums.LogEsnType.WA_CREATE_PLAN_TASK, user, null, DateTime.Now, null);
                    return new AuEsnBaseResult() { result = 1 };
                }

                DateTime now_plan = DateTime.Now.AddSeconds(15);
                Func<vw_asna_user, Enums.WaRequestTypeEnum, Enums.AsnaScheduleTypeEnum, bool> createPlanTask = (asnaUser, requestType, scheduleType) =>
                {
                    wa_task wa_task_last = dbContext.wa_task
                        .Where(ss => ss.workplace_id == asnaUser.workplace_id
                            && ss.request_type_id == (int)requestType
                            && !ss.is_deleted
                        //&& ss.is_auto_created
                            )
                        .OrderByDescending(ss => ss.crt_date)
                        .FirstOrDefault();

                    wa_task wa_task_last_issued = dbContext.wa_task
                        .Where(ss => ss.workplace_id == asnaUser.workplace_id
                            && ss.request_type_id == (int)requestType
                            && !ss.is_deleted
                            && ss.task_state_id == (int)Enums.WaTaskStateEnum.ISSUED_CLIENT
                            )
                        .OrderByDescending(ss => ss.task_id)
                        .FirstOrDefault();

                    List<DateTime> dates = getDatesForToday(asnaUser, scheduleType);
                    if ((dates == null) || (dates.Count <= 0))
                        return false;

                    DateTime? date_first = wa_task_last != null ? wa_task_last.crt_date : asnaUser.crt_date;

                    DateTime date_plan = dates.Where(ss => ss > date_first && ss <= now_plan).OrderByDescending(ss => ss).FirstOrDefault();
                    if (date_plan <= date_first)
                        return false;

                    wa_task_last = dbContext.wa_task.OrderByDescending(ss => ss.ord).FirstOrDefault();
                    int ord = wa_task_last != null ? wa_task_last.ord + 1 : 1;
                    wa_task_last = dbContext.wa_task.Where(ss => ss.client_id == asnaUser.client_id).OrderByDescending(ss => ss.ord).FirstOrDefault();
                    int ord_client = wa_task_last != null ? wa_task_last.ord_client + 1 : 1;
                    
                    wa_task wa_task = new wa_task();
                    wa_task.request_type_id = (int)requestType;
                    wa_task.task_state_id = wa_task_last_issued == null ? (int)Enums.WaTaskStateEnum.ISSUED_CLIENT : (int)Enums.WaTaskStateEnum.CANCELED_SERVER;
                    wa_task.client_id = asnaUser.client_id;
                    wa_task.sales_id = asnaUser.sales_id;
                    wa_task.workplace_id = asnaUser.workplace_id;
                    wa_task.ord = ord;
                    wa_task.ord_client = ord_client;
                    wa_task.is_auto_created = true;
                    wa_task.mess = wa_task_last_issued == null ? null : "Уже есть выданное клиенту задание " + (wa_task_last_issued.crt_date.HasValue ? ((DateTime)wa_task_last_issued.crt_date).ToString("dd.MM.yyyy HH:mm:ss") : "-");
                    wa_task.state_date = date_plan;
                    wa_task.state_user = user;
                    wa_task.crt_date = date_plan;
                    wa_task.crt_user = user;
                    wa_task.upd_date = date_plan;
                    wa_task.upd_user = user;
                    wa_task.is_deleted = false;
                    dbContext.wa_task.Add(wa_task);

                    wa_task_lc wa_task_lc = new wa_task_lc();
                    wa_task_lc.wa_task = wa_task;
                    wa_task_lc.task_state_id = wa_task.task_state_id;
                    wa_task_lc.mess = wa_task.mess;
                    wa_task_lc.state_date = wa_task.state_date;
                    wa_task_lc.state_user = wa_task.state_user;
                    wa_task_lc.crt_date = wa_task.crt_date;
                    wa_task_lc.crt_user = wa_task.crt_user;
                    wa_task_lc.upd_date = wa_task.upd_date;
                    wa_task_lc.upd_user = wa_task.upd_user;
                    wa_task_lc.is_deleted = wa_task.is_deleted;
                    dbContext.wa_task_lc.Add(wa_task_lc);

                    return true;
                };

                int fullTaskCnt = 0;
                int fullTaskCreatedCnt = 0;
                int changedTaskCnt = 0;
                int changedTaskCreatedCnt = 0;
                foreach (vw_asna_user vw_asna_user in vw_asna_user_list.Where(ss => ss.sch_full_is_active == true))
                {
                    fullTaskCnt = fullTaskCnt + 1;

                    bool taskCreated = createPlanTask(vw_asna_user, Enums.WaRequestTypeEnum.ASNA_SendStocksFull, Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS);
                    if (taskCreated)
                        fullTaskCreatedCnt = fullTaskCreatedCnt + 1;
                }

                dbContext.SaveChanges();

                foreach (vw_asna_user vw_asna_user in vw_asna_user_list.Where(ss => ss.sch_ch_is_active == true))
                {
                    changedTaskCnt = changedTaskCnt + 1;

                    bool taskCreated = createPlanTask(vw_asna_user, Enums.WaRequestTypeEnum.ASNA_SendStocksChanges, Enums.AsnaScheduleTypeEnum.SEND_CHANGED_REMAINS);
                    if (taskCreated)
                        changedTaskCreatedCnt = changedTaskCreatedCnt + 1;
                }

                dbContext.SaveChanges();

                string logMess = "Обработано пользователей: " + vw_asna_user_list.Count.ToString() + ". Создано заданий: " + fullTaskCreatedCnt.ToString() + "/" + fullTaskCnt.ToString() + " [полные остатки], " + changedTaskCreatedCnt.ToString() + "/" + changedTaskCnt.ToString() + " [изменения остатков]";
                if (log_enabled)
                    Loggly.InsertLog_Wa(logMess, (long)Enums.LogEsnType.WA_CREATE_PLAN_TASK, user, null, DateTime.Now, null);

                return new AuEsnBaseResult() { result = 1 };
            }
        }


        private List<DateTime> getDatesForToday(vw_asna_user user, Enums.AsnaScheduleTypeEnum scheduleType)
        {
            DateTime now = DateTime.Now.AddSeconds(-15);
            List<DateTime> scheduledDates = new List<DateTime>();

            bool isScheduleActive = false;
            switch (DateTime.Today.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    isScheduleActive = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_is_day1 == true : user.sch_ch_is_day1 == true;                                                            
                    break;
                case DayOfWeek.Tuesday:
                    isScheduleActive = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_is_day2 == true : user.sch_ch_is_day2 == true;                                                            
                    break;
                case DayOfWeek.Wednesday:
                    isScheduleActive = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_is_day3 == true : user.sch_ch_is_day3 == true;                                                            
                    break;
                case DayOfWeek.Thursday:
                    isScheduleActive = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_is_day4 == true : user.sch_ch_is_day4 == true;                                                            
                    break;
                case DayOfWeek.Friday:
                    isScheduleActive = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_is_day5 == true : user.sch_ch_is_day5 == true;                                                            
                    break;
                case DayOfWeek.Saturday:
                    isScheduleActive = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_is_day6 == true : user.sch_ch_is_day6 == true;                                                            
                    break;
                case DayOfWeek.Sunday:
                    isScheduleActive = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_is_day7 == true : user.sch_ch_is_day7 == true;                                                            
                    break;
                default:
                    break;
            }

            if (!isScheduleActive)
                return scheduledDates;

            int? minutesToAdd_start = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_start_time_type_value : user.sch_ch_start_time_type_value;
            if (!minutesToAdd_start.HasValue)
                return scheduledDates;
            DateTime firstDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0).AddMinutes((int)minutesToAdd_start);
            scheduledDates.Add(firstDate);

            int? minutesToRepeat = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_interval_type_value : user.sch_ch_interval_type_value;
            if (!minutesToRepeat.HasValue)
                return scheduledDates;

            int? minutesToAdd_end = scheduleType == Enums.AsnaScheduleTypeEnum.SEND_FULL_REMAINS ? user.sch_full_end_time_type_value : user.sch_ch_end_time_type_value;
            if (!minutesToAdd_end.HasValue)
                return scheduledDates;
            DateTime lastDate = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0).AddMinutes((int)minutesToAdd_end);
            if (lastDate <= firstDate)
                return scheduledDates;

            DateTime addedDate = firstDate.AddMinutes((int)minutesToRepeat);
            while (addedDate <= lastDate)
            {
                scheduledDates.Add(addedDate);
                addedDate = addedDate.AddMinutes((int)minutesToRepeat);
            }

            return scheduledDates;
        }

        private AuEsnBaseResult Worker_ExecTasks(int worker_id, bool log_enabled)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return worker_ExecTasks(worker_id, log_enabled);
            }
            catch (Exception ex)
            {
                string user = "wa_worker";
                if (log_enabled)
                    Loggly.InsertLog_Wa("Ошибка в Worker_CreateRequests_byTasks: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult worker_ExecTasks(int worker_id, bool log_enabled)
        {
            DateTime now_initial = DateTime.Now;
            string user = "wa_worker";
            string logMess = "";

            using (var dbContext = new AuMainDb())
            {
                List<vw_asna_user> vw_asna_user_list = dbContext.vw_asna_user
                    .Where(ss => ss.is_active && !ss.is_deleted)                    
                    .ToList();
                if ((vw_asna_user_list == null) || (vw_asna_user_list.Count <= 0))
                {
                    if (log_enabled)
                        Loggly.InsertLog_Wa("Не найдены активные пользователи", (long)Enums.LogEsnType.WA_EXEC_TASK, user, null, DateTime.Now, null);
                    return new AuEsnBaseResult() { result = 1 };
                }

                int taskAllCnt = 0;
                int taskCnt = 0;
                
                foreach (var vw_asna_user in vw_asna_user_list)
                {                    
                    List<wa_task> wa_task_avialable_list = dbContext.wa_task.Where(ss => ss.workplace_id == vw_asna_user.workplace_id
                        && ss.task_state_id == (int)Enums.WaTaskStateEnum.ISSUED_SERVER
                        && !ss.is_deleted
                        ).ToList();

                    if ((wa_task_avialable_list == null) || (wa_task_avialable_list.Count <= 0))
                        continue;

                    foreach (var wa_task_avialable in wa_task_avialable_list.OrderBy(ss => ss.crt_date))
                    {
                        taskAllCnt = taskAllCnt + 1;

                        // берем только по одному wa_task каждого типа (request_type_id) для каждого vw_asna_user, самый поздний
                        var wa_task_existing = wa_task_avialable_list
                            .Where(ss => ss.request_type_id == wa_task_avialable.request_type_id && ss.crt_date > wa_task_avialable.crt_date && ss.task_id != wa_task_avialable.task_id)
                            .OrderByDescending(ss => ss.crt_date)
                            .FirstOrDefault();
                        if (wa_task_existing != null)
                        {
                            UpdateTask(user, wa_task_avialable.task_id, (int)Enums.WaTaskStateEnum.CANCELED_SERVER, "Есть более позднее активное задание в " + ((DateTime)wa_task_existing.crt_date).ToString("dd.MM.yyyy HH:mm:ss"));
                            continue;
                        }

                        // взятие заданий в работу
                        wa_worker_task wa_worker_task = new wa_worker_task();
                        wa_worker_task.worker_id = worker_id;
                        wa_worker_task.task_id = wa_task_avialable.task_id;
                        wa_worker_task.is_active = true;
                        wa_worker_task.crt_date = DateTime.Now;
                        wa_worker_task.crt_user = user;
                        dbContext.wa_worker_task.Add(wa_worker_task);
                        dbContext.SaveChanges();

                        taskCnt = taskCnt + 1;
                    }
                }

                // дополнительно берем в обработку задания по STOCK
                var stockTaskList = dbContext.wa_task.Where(ss => ss.request_type_id == (int)Enums.WaRequestTypeEnum.STOCK_DelStock
                        && ss.task_state_id == (int)Enums.WaTaskStateEnum.ISSUED_SERVER
                        && !ss.is_deleted
                        ).ToList();

                if (stockTaskList != null)
                {
                    foreach (var stockTaskListItem in stockTaskList.OrderBy(ss => ss.crt_date))
                    {
                        taskAllCnt = taskAllCnt + 1;

                        // берем только по одному wa_task каждого типа (request_type_id) для каждого vw_asna_user, самый поздний
                        var wa_task_existing = stockTaskList
                            .Where(ss => ss.sales_id == stockTaskListItem.sales_id && ss.request_type_id == stockTaskListItem.request_type_id && ss.crt_date > stockTaskListItem.crt_date && ss.task_id != stockTaskListItem.task_id)
                            .OrderByDescending(ss => ss.crt_date)
                            .FirstOrDefault();
                        if (wa_task_existing != null)
                        {
                            UpdateTask(user, wa_task_existing.task_id, (int)Enums.WaTaskStateEnum.CANCELED_SERVER, "Есть более позднее активное задание в " + ((DateTime)wa_task_existing.crt_date).ToString("dd.MM.yyyy HH:mm:ss"));
                            continue;
                        }

                        // взятие заданий в работу
                        wa_worker_task wa_worker_task = new wa_worker_task();
                        wa_worker_task.worker_id = worker_id;
                        wa_worker_task.task_id = stockTaskListItem.task_id;
                        wa_worker_task.is_active = true;
                        wa_worker_task.crt_date = DateTime.Now;
                        wa_worker_task.crt_user = user;
                        dbContext.wa_worker_task.Add(wa_worker_task);
                        dbContext.SaveChanges();

                        taskCnt = taskCnt + 1;
                    }
                }


                Action<wa_worker_task, AuMainDb> setWorkerTaskInactive = (e, c) =>
                {
                    e.is_active = false;
                    e.inactive_date = DateTime.Now;
                    c.SaveChanges();
                };                

                wa_worker_task curr_worker_task = null;
                wa_worker_task other_worker_task = null;
                int curr_task_id = 0;
                int whileIterations_max = 100;
                int whileIterations = 0;
                int taskExecSuccessCnt = 0;
                int taskExecErrorCnt = 0;
                bool needBreak = false;
                string breakMess = "";
                while (true)
                {
                    whileIterations++;
                    if (whileIterations >= whileIterations_max)
                    {
                        needBreak = true;
                        breakMess = "Превышено максимально допустимое количество итераций [" + whileIterations_max.ToString() + "]";
                    }

                    //curr_worker_task = dbContext.wa_worker_task.AsNoTracking().Where(ss => ss.worker_id == worker_id && ss.is_active).OrderBy(ss => ss.item_id).FirstOrDefault();
                    curr_worker_task = dbContext.wa_worker_task.Where(ss => ss.worker_id == worker_id && ss.is_active).OrderBy(ss => ss.item_id).FirstOrDefault();
                    if (curr_worker_task == null)
                    {
                        needBreak = true;
                        breakMess = "";
                    }

                    if (needBreak)
                        break;

                    curr_task_id = curr_worker_task.task_id;

                    //other_worker_task = dbContext.wa_worker_task.AsNoTracking().Where(ss => ss.worker_id > worker_id && ss.task_id == curr_task_id && ss.is_active).FirstOrDefault();
                    other_worker_task = dbContext.wa_worker_task.Where(ss => ss.worker_id > worker_id && ss.task_id == curr_task_id && ss.is_active).FirstOrDefault();
                    if (other_worker_task != null)
                    {
                        curr_worker_task.intercepter_worker_id = other_worker_task.worker_id;
                        setWorkerTaskInactive(curr_worker_task, dbContext);
                        continue;
                    }

                    var updateTaskResult = UpdateTask(user, curr_task_id, (int)Enums.WaTaskStateEnum.HANDLING_SERVER, null);
                    if (updateTaskResult.error != null)
                    {
                        if (log_enabled)
                            Loggly.InsertLog_Wa(updateTaskResult.error.message, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                        UpdateTask(user, curr_task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, updateTaskResult.error.message);
                        continue;
                    }

                    // выполнение заданий
                    AsnaServiceParam asnaServiceParam = null;
                    SendRemainsToAsnaServiceParam sendRemainsToAsnaServiceParam = null;
                    GetRemainsFromAsnaServiceParam getRemainsFromAsnaServiceParam = null;
                    SendConsPriceToAsnaServiceParam sendConsPriceToAsnaServiceParam = null;
                    wa_task_param wa_task_param = null;
                    switch ((Enums.WaRequestTypeEnum)updateTaskResult.request_type_id)
                    {
                        case Enums.WaRequestTypeEnum.ASNA_GetStoreInfo:
                            wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == updateTaskResult.task_id).FirstOrDefault();
                            asnaServiceParam = wa_task_param != null ? JsonConvert.DeserializeObject<AsnaServiceParam>(wa_task_param.task_param) : null;
                            var getStoreInfoResult = asnaService.GetStoreInfoAsna(asnaServiceParam);
                            if (getStoreInfoResult.error != null)
                            {
                                taskExecErrorCnt = taskExecErrorCnt + 1;
                                if (log_enabled)
                                    Loggly.InsertLog_Wa(getStoreInfoResult.error.message, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                                UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, getStoreInfoResult.error.message);
                                if (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) < AuEsnConst.ASNA_handling_server_max_cnt)
                                {
                                    Thread.Sleep(20000);
                                    UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, "Перезапуск после ошибки, попытка: " + (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) + 1).ToString());
                                }
                                else
                                {
                                    setWorkerTaskInactive(curr_worker_task, dbContext);
                                }
                                break;
                            }
                            taskExecSuccessCnt = taskExecSuccessCnt + 1;
                            UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.SUCCESS_SERVER, getStoreInfoResult.mess);
                            setWorkerTaskInactive(curr_worker_task, dbContext);
                            break;
                        case Enums.WaRequestTypeEnum.ASNA_GetGoodsLinks:
                            wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == updateTaskResult.task_id).FirstOrDefault();
                            asnaServiceParam = wa_task_param != null ? JsonConvert.DeserializeObject<AsnaServiceParam>(wa_task_param.task_param) : null;
                            var getGoodsLinksResult = asnaService.GetGoodsLinks(asnaServiceParam);
                            if (getGoodsLinksResult.error != null)
                            {
                                taskExecErrorCnt = taskExecErrorCnt + 1;
                                if (log_enabled)
                                    Loggly.InsertLog_Wa(getGoodsLinksResult.error.message, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                                UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, getGoodsLinksResult.error.message);
                                if (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) < AuEsnConst.ASNA_handling_server_max_cnt)
                                {
                                    Thread.Sleep(20000);
                                    UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, "Перезапуск после ошибки, попытка: " + (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) + 1).ToString());
                                }
                                else
                                {
                                    setWorkerTaskInactive(curr_worker_task, dbContext);
                                }
                                break;
                            }
                            taskExecSuccessCnt = taskExecSuccessCnt + 1;
                            UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.SUCCESS_SERVER, getGoodsLinksResult.mess);
                            setWorkerTaskInactive(curr_worker_task, dbContext);
                            break;
                        case Enums.WaRequestTypeEnum.ASNA_SendStocksChanges:
                        case Enums.WaRequestTypeEnum.ASNA_SendStocksFull:
                            wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == updateTaskResult.task_id).FirstOrDefault();
                            sendRemainsToAsnaServiceParam = wa_task_param != null ? JsonConvert.DeserializeObject<SendRemainsToAsnaServiceParam>(wa_task_param.task_param) : null;
                            var sendRemainsToAsnaResult = asnaService.SendRemainsToAsna(sendRemainsToAsnaServiceParam);
                            if (sendRemainsToAsnaResult.error != null)
                            {
                                taskExecErrorCnt = taskExecErrorCnt + 1;
                                if (log_enabled)
                                    Loggly.InsertLog_Wa(sendRemainsToAsnaResult.error.message, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                                UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, sendRemainsToAsnaResult.error.message);                                
                                int max_cnt = (Enums.WaRequestTypeEnum)updateTaskResult.request_type_id == Enums.WaRequestTypeEnum.ASNA_SendStocksFull ? AuEsnConst.ASNA_handling_server_max_cnt2 : AuEsnConst.ASNA_handling_server_max_cnt;
                                if (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) < max_cnt)
                                {
                                    Thread.Sleep(20000);
                                    UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, "Перезапуск после ошибки, попытка: " + (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) + 1).ToString());
                                }
                                else
                                {
                                    setWorkerTaskInactive(curr_worker_task, dbContext);
                                }
                                break;
                            }
                            taskExecSuccessCnt = taskExecSuccessCnt + 1;
                            UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.SUCCESS_SERVER, sendRemainsToAsnaResult.mess);
                            setWorkerTaskInactive(curr_worker_task, dbContext);
                            break;
                        case Enums.WaRequestTypeEnum.ASNA_DelStocks:
                            wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == updateTaskResult.task_id).FirstOrDefault();
                            asnaServiceParam = wa_task_param != null ? JsonConvert.DeserializeObject<AsnaServiceParam>(wa_task_param.task_param) : null;
                            var delRemainsFromAsnaResult = asnaService.DelRemainsFromAsna(asnaServiceParam);
                            if (delRemainsFromAsnaResult.error != null)
                            {
                                taskExecErrorCnt = taskExecErrorCnt + 1;
                                if (log_enabled)
                                    Loggly.InsertLog_Wa(delRemainsFromAsnaResult.error.message, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                                UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, delRemainsFromAsnaResult.error.message);
                                if (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) < AuEsnConst.ASNA_handling_server_max_cnt)
                                {
                                    Thread.Sleep(20000);
                                    UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, "Перезапуск после ошибки, попытка: " + (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) + 1).ToString());
                                }
                                else
                                {
                                    setWorkerTaskInactive(curr_worker_task, dbContext);
                                }
                                break;
                            }
                            taskExecSuccessCnt = taskExecSuccessCnt + 1;
                            UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.SUCCESS_SERVER, delRemainsFromAsnaResult.mess);
                            setWorkerTaskInactive(curr_worker_task, dbContext);
                            break;
                        case Enums.WaRequestTypeEnum.ASNA_GetStocks:
                            wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == updateTaskResult.task_id).FirstOrDefault();
                            getRemainsFromAsnaServiceParam = wa_task_param != null ? JsonConvert.DeserializeObject<GetRemainsFromAsnaServiceParam>(wa_task_param.task_param) : null;
                            var getRemainsFromAsnaResult = asnaService.GetRemainsFromAsna(getRemainsFromAsnaServiceParam);
                            if (getRemainsFromAsnaResult.error != null)
                            {
                                taskExecErrorCnt = taskExecErrorCnt + 1;
                                if (log_enabled)
                                    Loggly.InsertLog_Wa(getRemainsFromAsnaResult.error.message, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                                UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, getRemainsFromAsnaResult.error.message);
                                if (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) < AuEsnConst.ASNA_handling_server_max_cnt)
                                {
                                    Thread.Sleep(20000);
                                    UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, "Перезапуск после ошибки, попытка: " + (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) + 1).ToString());
                                }
                                else
                                {
                                    setWorkerTaskInactive(curr_worker_task, dbContext);
                                }
                                break;
                            }
                            taskExecSuccessCnt = taskExecSuccessCnt + 1;
                            UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.SUCCESS_SERVER, getRemainsFromAsnaResult.mess);
                            setWorkerTaskInactive(curr_worker_task, dbContext);
                            break;
                        case Enums.WaRequestTypeEnum.ASNA_SendConsPriceChanges:
                        case Enums.WaRequestTypeEnum.ASNA_SendConsPriceFull:
                            wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == updateTaskResult.task_id).FirstOrDefault();
                            sendConsPriceToAsnaServiceParam = wa_task_param != null ? JsonConvert.DeserializeObject<SendConsPriceToAsnaServiceParam>(wa_task_param.task_param) : null;
                            var sendConsPriceToAsnaResult = asnaService.SendConsPriceToAsna(sendConsPriceToAsnaServiceParam);
                            if (sendConsPriceToAsnaResult.error != null)
                            {
                                taskExecErrorCnt = taskExecErrorCnt + 1;
                                if (log_enabled)
                                    Loggly.InsertLog_Wa(sendConsPriceToAsnaResult.error.message, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                                UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, sendConsPriceToAsnaResult.error.message);
                                if (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) < AuEsnConst.ASNA_handling_server_max_cnt)
                                {
                                    Thread.Sleep(20000);
                                    UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, "Перезапуск после ошибки, попытка: " + (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) + 1).ToString());
                                }
                                else
                                {
                                    setWorkerTaskInactive(curr_worker_task, dbContext);
                                }
                                break;
                            }
                            taskExecSuccessCnt = taskExecSuccessCnt + 1;
                            UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.SUCCESS_SERVER, sendConsPriceToAsnaResult.mess);
                            setWorkerTaskInactive(curr_worker_task, dbContext);
                            break;
                        case Enums.WaRequestTypeEnum.STOCK_DelStock:
                            wa_task_param = dbContext.wa_task_param.Where(ss => ss.task_id == updateTaskResult.task_id).FirstOrDefault();
                            DelStockServiceParam delStockServiceParam = wa_task_param != null ? JsonConvert.DeserializeObject<DelStockServiceParam>(wa_task_param.task_param) : null;
                            var delStockServiceResult = esnService.DelStock(delStockServiceParam);
                            if (delStockServiceResult.error != null)
                            {
                                taskExecErrorCnt = taskExecErrorCnt + 1;
                                if (log_enabled)
                                    Loggly.InsertLog_Wa(delStockServiceResult.error.message, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                                UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, delStockServiceResult.error.message);
                                if (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) < AuEsnConst.ASNA_handling_server_max_cnt)
                                {
                                    Thread.Sleep(20000);
                                    UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ISSUED_SERVER, "Перезапуск после ошибки, попытка: " + (updateTaskResult.handling_server_cnt.GetValueOrDefault(0) + 1).ToString());
                                }
                                else
                                {
                                    setWorkerTaskInactive(curr_worker_task, dbContext);
                                }
                                break;
                            }
                            taskExecSuccessCnt = taskExecSuccessCnt + 1;
                            UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.SUCCESS_SERVER, delStockServiceResult.mess);
                            setWorkerTaskInactive(curr_worker_task, dbContext);
                            break;
                        default:
                            logMess = "Выполнение запроса с типом " + updateTaskResult.request_type_id.ToString() + " не реализовано";
                            if (log_enabled)
                                Loggly.InsertLog_Wa(logMess, (long)Enums.LogEsnType.ERROR, user, null, DateTime.Now, null);
                            UpdateTask(user, updateTaskResult.task_id, (int)Enums.WaTaskStateEnum.ERROR_SERVER, logMess);
                            setWorkerTaskInactive(curr_worker_task, dbContext);
                            break;
                    }

                }

                if (log_enabled)
                {
                    logMess = "Обработано пользователей: " + vw_asna_user_list.Count.ToString()
                        + ". Принято заданий: " + taskCnt.ToString() + "/" + taskAllCnt.ToString()
                        + ", выполнено успешно: " + taskExecSuccessCnt.ToString()
                        + ", выполнено с ошибками: " + taskExecErrorCnt.ToString()
                        ;
                    if ((needBreak) && (!String.IsNullOrWhiteSpace(breakMess)))
                        logMess += " [" + breakMess + "]";
                    Loggly.InsertLog_Wa(logMess, (long)Enums.LogEsnType.WA_EXEC_TASK, user, null, DateTime.Now, null);
                }

                return new AuEsnBaseResult() { result = 1 };
            }
        }

        private AuEsnBaseResult Worker_FinishWork(int worker_id, bool log_enabled)
        {
            DateTime now_initial = DateTime.Now;
            string user = "wa_worker";

            using (var dbContext = new AuMainDb())
            {
                wa_worker wa_worker = dbContext.wa_worker.Where(ss => ss.worker_id == worker_id).FirstOrDefault();
                if (wa_worker != null)
                {
                    wa_worker.is_active = false;
                    wa_worker.inactive_date = now_initial;
                    wa_worker.upd_date = wa_worker.inactive_date;
                    wa_worker.upd_user = user;
                }

                dbContext.SaveChanges();

                string logMess = "Обработка закончена в " + now_initial.ToString("dd.MM.yyyy HH:mm:ss");
                if (log_enabled)
                    Loggly.InsertLog_Wa(logMess, (long)Enums.LogEsnType.WA_END_WORKER, user, null, DateTime.Now, null);
                return new AuEsnBaseResult() { result = 1 };
            }
        }

        #endregion
    }
}