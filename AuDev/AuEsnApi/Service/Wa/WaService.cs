﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class WaService : AuEsnBaseService, IWaService
    {
        private IMainService mainService
        {
            get
            {
                if (_mainService == null)
                {
                    _mainService = GetInjectedService<IMainService>();
                }
                return _mainService;
            }
        }
        private IMainService _mainService;

        private IAsnaService asnaService
        {
            get
            {
                if (_asnaService == null)
                {
                    _asnaService = GetInjectedService<IAsnaService>();
                }
                return _asnaService;
            }
        }
        private IAsnaService _asnaService;

        private IEsnService esnService
        {
            get
            {
                if (_esnService == null)
                {
                    _esnService = GetInjectedService<IEsnService>();
                }
                return _esnService;
            }
        }
        private IEsnService _esnService;

        public WaService()
            :base()
        {
            //
        }
    }
}