﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Extensions;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class WaService
    {
        #region SendTaskState

        public AuEsnBaseResult SendTaskReceived(TaskServiceParam taskServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendTaskState(taskServiceParam, (int)Enums.WaTaskStateEnum.HANDLING_CLIENT);
            }
            catch (Exception ex)
            {
                string user = taskServiceParam != null && !String.IsNullOrWhiteSpace(taskServiceParam.login) ? taskServiceParam.login : "esn";
                Loggly.InsertLog_Asna("Ошибка в SendTaskReceived: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        public AuEsnBaseResult SendTaskCompleted(TaskServiceParam taskServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendTaskState(taskServiceParam, (int)Enums.WaTaskStateEnum.SUCCESS_CLIENT);
            }
            catch (Exception ex)
            {
                string user = taskServiceParam != null && !String.IsNullOrWhiteSpace(taskServiceParam.login) ? taskServiceParam.login : "esn";
                Loggly.InsertLog_Asna("Ошибка в SendTaskCompleted: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        public AuEsnBaseResult SendTaskFailed(TaskServiceParam taskServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendTaskState(taskServiceParam, (int)Enums.WaTaskStateEnum.ERROR_CLIENT);
            }
            catch (Exception ex)
            {
                string user = taskServiceParam != null && !String.IsNullOrWhiteSpace(taskServiceParam.login) ? taskServiceParam.login : "esn";
                Loggly.InsertLog_Asna("Ошибка в SendTaskFailed: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        public AuEsnBaseResult SendTaskCanceled(TaskServiceParam taskServiceParam)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return sendTaskState(taskServiceParam, (int)Enums.WaTaskStateEnum.CANCELED_CLIENT);
            }
            catch (Exception ex)
            {
                string user = taskServiceParam != null && !String.IsNullOrWhiteSpace(taskServiceParam.login) ? taskServiceParam.login : "esn";
                Loggly.InsertLog_Asna("Ошибка в SendTaskCanceled: " + GlobalUtil.ExceptionInfo(ex), (long)Enums.LogEsnType.ERROR, user, null, log_date_beg, null);
                return new AuEsnBaseResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private AuEsnBaseResult sendTaskState(TaskServiceParam taskServiceParam, int state_id)
        {
            AuEsnBaseResult errRes = null;
            DateTime now_initial = DateTime.Now;
            string user = taskServiceParam != null && !String.IsNullOrWhiteSpace(taskServiceParam.login) ? taskServiceParam.login : "esn";
            int service_id = taskServiceParam != null ? taskServiceParam.service_id : 0;
            int task_id = taskServiceParam != null ? taskServiceParam.task_id : 0;
            
            var checkLicenseResult = mainService.CheckLicense(taskServiceParam, service_id);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                errRes = new AuEsnBaseResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
                Loggly.InsertLog_Asna(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }
            
            int client_id = checkLicenseResult.client_id;
            int sales_id = checkLicenseResult.sales_id;
            int workplace_id = checkLicenseResult.workplace_id;

            var query = dbContext.wa_task.Where(ss => ss.task_id == task_id);
            /*
            if (workplace_id > 0)
                query = query.Where(ss => ss.workplace_id == workplace_id);
            else
                query = query.Where(ss => ss.sales_id == sales_id);
            */
            query = query.Where(t1 => ((t1.workplace_id == workplace_id) || ((!t1.workplace_id.HasValue) && (t1.sales_id == sales_id))));
            wa_task wa_task = query.FirstOrDefault();
            

            if (wa_task == null)
            {
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдено задание с кодом (" + task_id.ToString() + ")"),
                };
                Loggly.InsertLog_Asna(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            bool redirectTaskToServer = false;
            int allowed_state_id = -1;
            string allowed_state_name = "-";
            switch ((Enums.WaTaskStateEnum)state_id)
            {
                case Enums.WaTaskStateEnum.HANDLING_CLIENT:
                case Enums.WaTaskStateEnum.CANCELED_CLIENT:
                    allowed_state_id = (int)Enums.WaTaskStateEnum.ISSUED_CLIENT;
                    allowed_state_name = ((int)Enums.WaTaskStateEnum.ISSUED_CLIENT).EnumName<Enums.WaTaskStateEnum>();
                    break;
                case Enums.WaTaskStateEnum.SUCCESS_CLIENT:
                case Enums.WaTaskStateEnum.ERROR_CLIENT:
                    allowed_state_id = (int)Enums.WaTaskStateEnum.HANDLING_CLIENT;
                    allowed_state_name = ((int)Enums.WaTaskStateEnum.HANDLING_CLIENT).EnumName<Enums.WaTaskStateEnum>();
                    redirectTaskToServer = ((Enums.WaTaskStateEnum)state_id == Enums.WaTaskStateEnum.SUCCESS_CLIENT);
                    break;
                default:
                    errRes = new AuEsnBaseResult()
                    {
                        error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Недопустимый код статуса задания (" + state_id.ToString() + ")"),
                    };
                    Loggly.InsertLog_Asna(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                    return errRes;
            }            

            if ((wa_task.task_state_id != allowed_state_id) && (wa_task.task_state_id != state_id))
            {
                string state_name = state_id.EnumName<Enums.WaTaskStateEnum>();
                errRes = new AuEsnBaseResult()
                {
                    error = new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Нельзя поменять статус " + allowed_state_name + " на " + state_name),
                };
                Loggly.InsertLog_Asna(errRes.error.message, (long)Enums.LogEsnType.ERROR, user, null, now_initial, null);
                return errRes;
            }

            /*
            wa_task.task_state_id = redirectTaskToServer ? (int)Enums.WaTaskStateEnum.ISSUED_SERVER : state_id;
            wa_task.state_date = DateTime.Now;
            wa_task.state_user = user;
            wa_task.mess = taskServiceParam.mess;
            dbContext.SaveChanges();
            */

            UpdateTask(user, wa_task.task_id, (redirectTaskToServer ? (int)Enums.WaTaskStateEnum.ISSUED_SERVER : state_id), taskServiceParam.mess);            

            return new AuEsnBaseResult();
        }
        
        #endregion
    }
}
