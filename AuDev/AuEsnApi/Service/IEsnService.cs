﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuEsnApi.Models;
#endregion

namespace AuEsnApi.Service
{
    public interface IEsnService : IInjectableService
    {
        AuEsnBaseResult SendDoc(SendDocServiceParam sendDocServiceParam);                   // 1.	Отправка позиций накладных из АУ на веб-сервис Аурит для обработки
        GetDocStateResult GetDocState(GetDocServiceParam getDocStateServiceParam);          // 2.	Просмотр статуса отправленных накладных
        GetDocStatesResult GetDocStates(UserInfo getDocStatesServiceParam);                 //      справочник статусов документов
        GetDocLogResult GetDocLog(GetDocLogServiceParam getDocLogServiceParam);             // 3.	Просмотр истории отправки / загрузки накладных
        GetDocResult GetDoc(GetDocServiceParam getDocServiceParam);                         // 4.	Загрузка с веб-сервиса обработанных позиций накладных
        AuEsnBaseResult SendChanges(UserInfo sendDocServiceParam);                          // 6.	Отправка на веб-сервис изменений по состыкованным товарам
        //
        AuEsnBaseResult SendStock(SendStockServiceParam sendStockServiceParam);
        GetStockResult GetStock(GetStockServiceParam getStockServiceParam);
        AuEsnBaseResult ResetSendStock(UserInfo resetSendStockServiceParam);
        AuEsnBaseResult ResetGetStock(UserInfo resetGetStockServiceParam);
        //
        AuEsnBaseResult DelStock(DelStockServiceParam delStockServiceParam);
        //
        AuEsnBaseResult ConfirmSendStock(ConfirmStockServiceParam сonfirmStockServiceParam);
        AuEsnBaseResult ConfirmGetStock(ConfirmStockServiceParam сonfirmStockServiceParam);
        GetLastBatchStockResult GetLastBatchSendStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam);
        GetLastBatchStockResult GetLastBatchGetStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam);
        //
        AuEsnBaseResult SendStockReport(StockReportServiceParam stockReportServiceParam);
        AuEsnBaseResult GetStockReport(StockReportServiceParam stockReportServiceParam);
    }
}
