﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Collections;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AutoMapper.QueryableExtensions;
using AuEsnApi.Models;
using AuEsnApi.Log;

namespace AuEsnApi.Service
{
    public partial class MainService
    {
        #region GetMethodsDescr

        public GetMethodsDescrResult GetMethodsDescr(UserInfo userInfo)
        {
            try
            {
                return getMethodsDescr(userInfo);
            }
            catch (Exception ex)
            {
                return new GetMethodsDescrResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) };
            }
        }

        private GetMethodsDescrResult getMethodsDescr(UserInfo userInfo)
        {
            AuEsnBaseResult checkLicenseResult = CheckLicense(userInfo);
            if ((checkLicenseResult == null) || (checkLicenseResult.error != null))
            {
                return new GetMethodsDescrResult() { error = checkLicenseResult != null ? checkLicenseResult.error : new ErrInfo((int)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Ошибка при проверке лицензии") };
            }

            return new GetMethodsDescrResult()
            {
                descriptions = this.MethodDescrDictionary.Select(ss => new MethodDescription()
                {
                    method_id = ss.Key,
                    method_name = ss.Value.Item1,
                    method_descr = ss.Value.Item2,
                })
                .ToList()
            };
        }
        
        #endregion
    }
}