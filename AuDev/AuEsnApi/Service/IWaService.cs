﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using AuDev.Common.Util;
using AuDev.Common.Db.Model;
using AuEsnApi.Models;
#endregion

namespace AuEsnApi.Service
{
    public interface IWaService : IInjectableService
    {
        // Tasks
        GetRequestTypesResult GetRequestTypes(GetRequestTypesServiceParam getRequestTypesServiceParam);
        GetTasksResult GetTasks(GetTasksServiceParam getTasksServiceParam);                     //      получение списка заданий на выполнение различных операций
        AuEsnBaseResult SendTaskReceived(TaskServiceParam taskServiceParam);        //      уведомление веб-сервису о том, что задание получено
        AuEsnBaseResult SendTaskCompleted(TaskServiceParam taskServiceParam);      //      уведомление веб-сервису о том, что задание успешно выполнено
        AuEsnBaseResult SendTaskFailed(TaskServiceParam taskServiceParam);            //      уведомление веб-сервису о том, что задание выполнено с ошибкой        
        AuEsnBaseResult SendTaskCanceled(TaskServiceParam taskServiceParam);        //      уведомление веб-сервису о том, что задание отменено
        //
        UpdateTaskResult CreateTask(string user, int request_type_id, int task_state_id, int? client_id, int? sales_id, int? workplace_id, bool is_auto_created, string mess, string param = null);
        UpdateTaskResult UpdateTask(string user, int task_id, int task_state_id, string mess);
        UpdateTaskResult UpdateTaskParam(string user, int task_id, string param);
        // Worker
        AuEsnBaseResult CreateWorker(CreateWorkerServiceParam createWorkerServiceParam);
    }
}
