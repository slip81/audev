﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuEsnApi.Models;
using AuEsnApi.Service;
using AuEsnApi.Json;
using Swashbuckle.Swagger.Annotations;
#endregion

namespace AuEsnApi.Controllers
{
    /// <summary>
    /// Основные методы API
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]    
    public partial class MainController : BaseController
    {
        public MainController(IMainService _mainService, IWaService _waService, IEsnService _esnService, IAsnaService _asnaService)
            : base(_mainService, _waService, _esnService, _asnaService)
        {
            //
        }

        /// <summary>
        /// Проверка лицензии
        /// </summary>
        /// <param name="checkLicenseServiceParam">Данные пользователя с указанием кода услуги</param>
        /// <returns>Результат проверки лицензии</returns>
        [HttpPost]
        [Route("CheckLicense")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Проверка наличия лицензии на заданную услугу  у заданного пользователя", 
            typeof(CheckLicenseResult))
        ]
        public HttpResponseMessage CheckLicense(CheckLicenseServiceParam checkLicenseServiceParam)
        {
            var result = mainService.CheckLicense(checkLicenseServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение описания всех доступных методов API
        /// </summary>
        /// <param name="userInfo">Данные пользователя</param>
        /// <returns>Список всех доступных методов API</returns>
        [HttpPost]
        [Route("GetMethodsDescr")]
        [SwaggerResponse(
            HttpStatusCode.OK
            , "Получение информации обо всех доступных в API методов, с указанием кодов методов, их названий и кратких описаний"
            , typeof(GetMethodsDescrResult))
        ]
        public HttpResponseMessage GetMethodsDescr(UserInfo userInfo)
        {
            var result = mainService.GetMethodsDescr(userInfo);
            return getJsonResponse(result);
        }

    }
   
}
