﻿#region
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Security;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
#endregion

namespace AuEsnApi.Controllers
{
    public class UrlAuthorizeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {            
            string ip = actionContext.Request.GetClientIpAddress();
            ip = String.IsNullOrWhiteSpace(ip) ? "-" : ip.Trim().ToLower();

            List<string> allowedIpList = new List<string>();
            string allowedIp = ConfigurationManager.AppSettings["urls"];            
            if (!String.IsNullOrWhiteSpace(allowedIp))
            {
                allowedIpList = allowedIp.Split(';').ToList();
            }
            allowedIpList = allowedIpList == null ? new List<string>() : allowedIpList;

            //if (true)
            if (!allowedIpList.Contains(ip))
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(
                    HttpStatusCode.BadRequest, "Запрос с адреса " + ip + " запрещен");
            }
        }
    }
}
