﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuEsnApi.Models;
using AuEsnApi.Service;
using AuEsnApi.Json;
using Swashbuckle.Swagger.Annotations;
#endregion

namespace AuEsnApi.Controllers
{
    /// <summary>
    /// Методы для работы с ЕСН
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]    
    public partial class EsnController : BaseController
    {
        public EsnController(IMainService _mainService, IWaService _waService, IEsnService _esnService, IAsnaService _asnaService)
            : base(_mainService, _waService, _esnService, _asnaService)
        {
            //
        }


        /// <summary>
        /// Проверка связи с сервисом (вариант 1, без проверки связи с БД)
        /// </summary>
        [HttpPost]
        [Route("Noop")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop()
        {
            esnService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Проверка связи с сервисом (вариант 2, с проверкой связи с БД)
        /// </summary>
        [HttpPost]
        [Route("Noop2")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop2()
        {
            esnService.Noop2();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Отправка документа в сервис ЕСН
        /// </summary>
        /// <param name="sendDocServiceParam">Данные документа</param>
        /// <returns>Результат отправки</returns>
        [HttpPost]
        [Route("SendDoc")]
        [JsonDeserializeExt(Param = "sendDocServiceParam", JsonDataType = typeof(SendDocServiceParam))]        
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отправка приходной накладной на стыковку позиций накладной с эталонными товарами ЕСН."
            + " После отправки по этой накладной можно  запрашивать у сервиса статус (GetDocState)"
            + " и загрузить после обработки  ее обратно (GetDoc)", 
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendDoc(SendDocServiceParam sendDocServiceParam)
        {
            var result = esnService.SendDoc(sendDocServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Просмотр статуса отправленного документа
        /// </summary>
        /// <param name="getDocStateServiceParam">Данные документа</param>
        /// <returns>Статус отправленного документа</returns>
        [HttpPost]
        [Route("GetDocState")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Получение статуса накладной на сервере ЕСН", 
            typeof(GetDocStateResult))
        ]
        public HttpResponseMessage GetDocState(GetDocServiceParam getDocStateServiceParam)
        {
            var result = esnService.GetDocState(getDocStateServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение справочника статусов документов
        /// </summary>
        /// <param name="getDocStatesServiceParam">Данные пользователя</param>
        /// <returns>Справочник статусов документов</returns>
        [HttpPost]
        [Route("GetDocStates")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Получение справочника статусов накладных на сервере ЕСН", 
            typeof(GetDocStatesResult))
        ]     
        public HttpResponseMessage GetDocStates(UserInfo getDocStatesServiceParam)
        {
            var result = esnService.GetDocStates(getDocStatesServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Просмотр истории отправки/загрузки документов
        /// </summary>
        /// <param name="getDocLogServiceParam">Данные документа</param>
        /// <returns>История отправки/загрузки документов</returns>
        [HttpPost]
        [Route("GetDocLog")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Получение информации из лога ЕСН об операциях с накладными заданного пользователя", 
            typeof(GetDocLogResult))
        ]
        public HttpResponseMessage GetDocLog(GetDocLogServiceParam getDocLogServiceParam)
        {
            var result = esnService.GetDocLog(getDocLogServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Загрузка с веб-сервиса обработанного документа
        /// </summary>
        /// <param name="getDocServiceParam">Данные документа</param>
        /// <returns>Обработанный документ</returns>
        [HttpPost]
        [Route("GetDoc")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Загрузка в АУ обработанной накладной из ЕСН."
            + " В накладной могут содержаться как состыкованные позиции, так и не состыкованные."
            + " После загрузки в АУ накладная получает в ЕСН статус 'Загружена в АУ' и более недоступна для повторной загрузки", 
            typeof(GetDocResult))
        ]
        public HttpResponseMessage GetDoc(GetDocServiceParam getDocServiceParam)
        {
            var result = esnService.GetDoc(getDocServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отправка наличия на веб-сервис для формирования сводного наличия
        /// </summary>
        /// <param name="sendStockServiceParam">Данные отправляемого наличия</param>
        /// <returns>Результат отправки</returns>
        [HttpPost]
        [Route("SendStock")]
        [JsonDeserializeExt(Param = "sendStockServiceParam", JsonDataType = typeof(SendStockServiceParam))]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Отправка позиций из наличия БД АУ на веб-сервис. Чтобы уменьшить кол-во позиций в одном запросе,"
            + " реализован вариант отправки данных пакетами: на клиенте нужно сначала сформировать весь набор данных для отправки,"
            + " затем разбить его на пакеты по N записей, пронумеровать все пакеты, подсчитать общее число пакетов"
            + " и начать поочередную  отправку пакетов. В параметры part_num и part_cnt нужно записывать номер текущего пакета"
            + " и общее кол-во пакетов соответственно."
            + "<br/>"
            + " <strong>ВНИМАНИЕ !</strong> Обязательно нужно отправить все сформированные пакеты по очереди,"
            + " в случае нарушения порядка отправки или неотправки последних пакетов веб-сервис вернет ошибку",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendStock(SendStockServiceParam sendStockServiceParam)
        {
            var result = esnService.SendStock(sendStockServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение с веб-сервиса сводного наличия
        /// </summary>
        /// <param name="getStockServiceParam">Параметры получения</param>
        /// <returns>Полученное сводное наличие</returns>
        [HttpPost]
        [Route("GetStock")]        
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Загрузка с веб-сервиса строк со сводным наличием по всем отделениям текущего пользователя"
            + " в БД АУ. Будут загружены в том числе и данные о наличии в текущем подразделении,"
            + " их можно отличить от остальных строк по признаку is_mine = 1."
            + " Если значение параметра force_all = 1 – с веб-сервиса придут все строки с наличием,"
            + " даже те, которые уже были запрошены ранее."
            + " <br/>"
            + " Чтобы уменьшить кол-во позиций в одном запросе, реализован вариант"
            + " загрузки данных пакетами:  при первом вызове этого метода параметр part_num"
            + " должен быть заполнен значением 1. В выходных параметрах метода нужно будет"
            + " взять параметр part_cnt, и далее вызывать в цикле этот метод с постепенным"
            + " увеличением параметра part_num на единицу, пока он не достигнет значения"
            + " параметра part_cnt."
            + " <br/>"
            + " <strong>ВАЖНО !</strong> Во всех вызовах GetStock в одном цикле загрузки"
            + " значение параметра force_all должно быть одинаковым"
            + " (равно значению из первого вызова GetStock)",
            typeof(GetStockResult))
        ]
        public HttpResponseMessage GetStock(GetStockServiceParam getStockServiceParam)
        {
            var result = esnService.GetStock(getStockServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Сброс активной цепочки пакетов отправки наличия на веб-сервис
        /// </summary>
        /// <param name="resetSendStockServiceParam">Данные пользователя</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ResetSendStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "В случае непредвиденной ошибки при отправке одного из цепочки пакетов на веб-сервис"
            + " этот метод позволяет сбросить текущую активную цепочку и начать отправлять наличие заново",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage ResetSendStock(UserInfo resetSendStockServiceParam)
        {
            var result = esnService.ResetSendStock(resetSendStockServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Сброс активной цепочки пакетов загрузки наличия с веб-сервиса
        /// </summary>
        /// <param name="resetGetStockServiceParam">Данные пользователя</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ResetGetStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "В случае непредвиденной ошибки при загрузке одного из цепочки пакетов с веб-сервиса"
            + " этот метод позволяет сбросить текущую активную цепочку и начать получать наличие заново",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage ResetGetStock(UserInfo resetGetStockServiceParam)
        {
            var result = esnService.ResetGetStock(resetGetStockServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Подтверждение успешной отправки пакета сводного наличия на веб-сервис
        /// </summary>
        /// <param name="confirmStockServiceParam">Параметры подтверждения</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ConfirmSendStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Подтверждение успешной отправки пакета сводного наличия на веб-сервис",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage ConfirmSendStock(ConfirmStockServiceParam confirmStockServiceParam)
        {
            var result = esnService.ConfirmSendStock(confirmStockServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Подтверждение успешного получения пакета сводного наличия от веб-сервиса
        /// </summary>
        /// <param name="confirmStockServiceParam">Параметры подтверждения</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ConfirmGetStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Подтверждение успешного получения пакета сводного наличия от веб-сервиса",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage ConfirmGetStock(ConfirmStockServiceParam confirmStockServiceParam)
        {
            var result = esnService.ConfirmGetStock(confirmStockServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение последнего подтвержденного пакета отправки сводного наличия на веб-сервис
        /// </summary>
        /// <param name="getLastBatchStockServiceParam">Параметры получения пакета</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetLastBatchSendStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение последнего подтвержденного пакета отправки сводного наличия на веб-сервис",
            typeof(GetLastBatchStockResult))
        ]
        public HttpResponseMessage GetLastBatchSendStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam)
        {
            var result = esnService.GetLastBatchSendStock(getLastBatchStockServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение последнего подтвержденного пакета получения сводного наличия с веб-сервиса
        /// </summary>
        /// <param name="getLastBatchStockServiceParam">Параметры получения пакета</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetLastBatchGetStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение последнего подтвержденного пакета получения сводного наличия с веб-сервиса",
            typeof(GetLastBatchStockResult))
        ]
        public HttpResponseMessage GetLastBatchGetStock(GetLastBatchStockServiceParam getLastBatchStockServiceParam)
        {
            var result = esnService.GetLastBatchGetStock(getLastBatchStockServiceParam);
            return getJsonResponse(result);
        }

        // !!!
        // когда запущено удаление наличия на сервере - надо запрещать любые обмены всех ТТ этого клиента

        /// <summary>
        /// Удаление сводного наличия по заданной торговой точке с веб-сервиса
        /// </summary>
        /// <param name="delStockServiceParam">Параметры удаления наличия</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("DelStock")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Удаление сводного наличия по заданной торговой точке с веб-сервиса",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage DelStock(DelStockServiceParam delStockServiceParam)
        {
            var result = esnService.DelStock(delStockServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Передача в лог сообщения по типу операции "СН: отправка"
        /// </summary>
        /// <param name="stockReportServiceParam">Информация о пользователе и текст сообщения</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendStockReport")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Передача в лог сообщения по типу операции 'СН: отправка'",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendStockReport(StockReportServiceParam stockReportServiceParam)
        {
            var result = esnService.SendStockReport(stockReportServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Передача в лог сообщения по типу операции "СН: загрузка"
        /// </summary>
        /// <param name="stockReportServiceParam">Информация о пользователе и текст сообщения</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetStockReport")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Передача в лог сообщения по типу операции 'СН: загрузка'",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage GetStockReport(StockReportServiceParam stockReportServiceParam)
        {
            var result = esnService.GetStockReport(stockReportServiceParam);
            return getJsonResponse(result);
        }

    }
  
}
