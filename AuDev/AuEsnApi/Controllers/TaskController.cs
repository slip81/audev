﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuEsnApi.Models;
using AuEsnApi.Service;
using AuEsnApi.Json;
using Swashbuckle.Swagger.Annotations;
using System.Web.Http.Description;
#endregion

namespace AuEsnApi.Controllers
{
    /// <summary>
    /// Методы для работы с заданиями
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]    
    public partial class TaskController : BaseController
    {
        public TaskController(IMainService _mainService, IWaService _waService, IEsnService _esnService, IAsnaService _asnaService)
            : base(_mainService, _waService, _esnService, _asnaService)
        {
            //
        }

        /// <summary>
        /// Получение с веб-сервиса типов запросов для выполнения заданий
        /// </summary>
        /// <param name="getRequestTypesServiceParam">Данные пользователя с указанием кода услуги</param>
        /// <returns>Список типов запросов</returns>
        [HttpPost]
        [Route("GetRequestTypes")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Загрузка с веб-сервиса строк с допустимыми типами запросов"
            + " для заданий по заданной услуге для заданного пользователя", 
            typeof(GetRequestTypesResult))
        ]
        public HttpResponseMessage GetRequestTypes(GetRequestTypesServiceParam getRequestTypesServiceParam)
        {
            var result = waService.GetRequestTypes(getRequestTypesServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение с веб-сервиса списка заданий на выполнение
        /// </summary>
        /// <param name="getTasksServiceParam">Данные пользователя с указанием кода услуги</param>
        /// <returns>Список заданий</returns>
        [HttpPost]
        [Route("GetTasks")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Загрузка с веб-сервиса строк с активными заданиями по заданной услуге"
            + " для заданного пользователя. После успешного получения задания необходимо"
            + " отправить веб-сервису уведомление о том, что задание получено"
            + " (см. метод  /SendTaskReceived ), выполнять задание можно только после этого."
            + " Допустимые типы запросов для заданий (т.е. расшифровку возможных значений"
            + " атрибута request_type_id) можно узнать методом /GetRequestTypes", 
            typeof(GetTasksResult))
        ]
        public HttpResponseMessage GetTasks(GetTasksServiceParam getTasksServiceParam)
        {
            var result = waService.GetTasks(getTasksServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отправка уведомления веб-сервису о том, что задание принято к выполнению
        /// </summary>
        /// <param name="taskServiceParam">Данные пользователя с указанием кода задания и комментария</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendTaskReceived")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Отправка на веб-сервис признака, что задание получено"
            + " При следующем вызове метода /GetTasks пользователю не придут задания,"
            + " у которых стоит признак 'задание получено'."
            + " Задание с признаком “задание получено” теперь можно выполнять, желательно"
            + " в порядке увеличения значения атрибута ord."
            + " После успешного выполнения задания необходимо отправить веб-сервису"
            + " уведомление о том, что задание успешно выполнено (см. метод  /SendTaskCompleted )."
            + " В случае ошибки во время выполнения задания – можно либо пытаться выполнять"
            + " его повторно, либо  отправить веб-сервису уведомление о том,"
            + " что задание выполнено с ошибками (см. метод  /SendTaskFailed ).", 
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendTaskReceived(TaskServiceParam taskServiceParam)
        {
            var result = waService.SendTaskReceived(taskServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отправка уведомления веб-сервису о том, что задание выполнено успешно
        /// </summary>
        /// <param name="taskServiceParam">Данные пользователя с указанием кода задания и комментария</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendTaskCompleted")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отправка на веб-сервис признака, что задание успешно выполнено."
            + " При следующем вызове метода /GetTasks пользователю не придут задания,"
            + " у которых стоит признак 'задание успешно выполнено'.",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendTaskCompleted(TaskServiceParam taskServiceParam)
        {
            var result = waService.SendTaskCompleted(taskServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отправка уведомления веб-сервису о том, что задание выполнено с ошибками
        /// </summary>
        /// <param name="taskServiceParam">Данные пользователя с указанием кода задания и комментария</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendTaskFailed")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отправка на веб-сервис признака, что задание выполнено с ошибками."
            + " При следующем вызове метода /GetTasks пользователю не придут задания,"
            + " у которых стоит признак 'задание выполнено с ошибками'.",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendTaskFailed(TaskServiceParam taskServiceParam)
        {
            var result = waService.SendTaskFailed(taskServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отправка уведомления веб-сервису о том, что задание отменено
        /// </summary>
        /// <param name="taskServiceParam">Данные пользователя с указанием кода задания и комментария</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendTaskCanceled")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отправка на веб-сервис признака, что задание отменено."
            + " Причины отмены могут быть разные, на усмотрение разработчиков, например:"
            + " пришло подряд два одинаковых задания, из которых к выполнению принято более новое,"
            + " а более старое - отменено. Рекомендуется заполнять параметр mess причиной отмены."
            + " При следующем вызове метода /GetTasks пользователю не придут задания,"
            + " у которых стоит признак 'задание отменено'.",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendTaskCanceled(TaskServiceParam taskServiceParam)
        {
            var result = waService.SendTaskCanceled(taskServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Запуск сервисного обработчика
        /// </summary>
        /// <param name="createWorkerServiceParam">Параметры запуска</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("CreateWorker")]
        [UrlAuthorize()]
        [SwaggerResponse(HttpStatusCode.OK, "Обработчик запущен", typeof(AuEsnBaseResult))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage CreateWorker(CreateWorkerServiceParam createWorkerServiceParam)
        {
            var result = waService.CreateWorker(createWorkerServiceParam);
            return getJsonResponse(result);
        }

    }

   
}
