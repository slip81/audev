﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuEsnApi.Models;
using AuEsnApi.Service;
using AuEsnApi.Json;
#endregion

namespace AuEsnApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]
    public class BaseController : ApiController
    {
        protected readonly IMainService mainService;
        protected readonly IWaService waService;
        protected readonly IEsnService esnService;
        protected readonly IAsnaService asnaService;

        public BaseController(IMainService _mainService, IWaService _waService, IEsnService _esnService, IAsnaService _asnaService)
        {
            this.mainService = _mainService;
            this.waService = _waService;
            this.esnService = _esnService;
            this.asnaService = _asnaService;
        }

        protected HttpResponseMessage getJsonResponse(object value)
        {
            var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver(), };
            string json = JsonConvert.SerializeObject(value, settings);
            //string json = JsonConvert.SerializeObject(value);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }
    }
}