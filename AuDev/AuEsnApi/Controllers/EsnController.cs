﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuEsnApi.Models;
using AuEsnApi.Service;
using AuEsnApi.Json;
using Swashbuckle.Swagger.Annotations;
#endregion

namespace AuEsnApi.Controllers
{
    /// <summary>
    /// Методы для работы с ЕСН
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("")]    
    public partial class EsnController : BaseController
    {
        public EsnController(IMainService _mainService, IWaService _waService, IEsnService _esnService, IAsnaService _asnaService)
            : base(_mainService, _waService, _esnService, _asnaService)
        {
            //
        }


        /// <summary>
        /// Проверка связи с сервисом (вариант 1, без проверки связи с БД)
        /// </summary>
        [HttpPost]
        [Route("Noop")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop()
        {
            esnService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Проверка связи с сервисом (вариант 2, с проверкой связи с БД)
        /// </summary>
        [HttpPost]
        [Route("Noop2")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop2()
        {
            esnService.Noop2();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Отправка документа в сервис ЕСН
        /// </summary>
        /// <param name="sendDocServiceParam">Данные документа</param>
        /// <returns>Результат отправки</returns>
        [HttpPost]
        [Route("SendDoc")]
        [JsonDeserializeExt(Param = "sendDocServiceParam", JsonDataType = typeof(SendDocServiceParam))]        
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отправка приходной накладной на стыковку позиций накладной с эталонными товарами ЕСН."
            + " После отправки по этой накладной можно  запрашивать у сервиса статус (GetDocState)"
            + " и загрузить после обработки  ее обратно (GetDoc)", 
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendDoc(SendDocServiceParam sendDocServiceParam)
        {
            var result = esnService.SendDoc(sendDocServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Просмотр статуса отправленного документа
        /// </summary>
        /// <param name="getDocStateServiceParam">Данные документа</param>
        /// <returns>Статус отправленного документа</returns>
        [HttpPost]
        [Route("GetDocState")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Получение статуса накладной на сервере ЕСН", 
            typeof(GetDocStateResult))
        ]
        public HttpResponseMessage GetDocState(GetDocServiceParam getDocStateServiceParam)
        {
            var result = esnService.GetDocState(getDocStateServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение справочника статусов документов
        /// </summary>
        /// <param name="getDocStatesServiceParam">Данные пользователя</param>
        /// <returns>Справочник статусов документов</returns>
        [HttpPost]
        [Route("GetDocStates")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Получение справочника статусов накладных на сервере ЕСН", 
            typeof(GetDocStatesResult))
        ]     
        public HttpResponseMessage GetDocStates(UserInfo getDocStatesServiceParam)
        {
            var result = esnService.GetDocStates(getDocStatesServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Просмотр истории отправки/загрузки документов
        /// </summary>
        /// <param name="getDocLogServiceParam">Данные документа</param>
        /// <returns>История отправки/загрузки документов</returns>
        [HttpPost]
        [Route("GetDocLog")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Получение информации из лога ЕСН об операциях с накладными заданного пользователя", 
            typeof(GetDocLogResult))
        ]
        public HttpResponseMessage GetDocLog(GetDocLogServiceParam getDocLogServiceParam)
        {
            var result = esnService.GetDocLog(getDocLogServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Загрузка с веб-сервиса обработанного документа
        /// </summary>
        /// <param name="getDocServiceParam">Данные документа</param>
        /// <returns>Обработанный документ</returns>
        [HttpPost]
        [Route("GetDoc")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Загрузка в АУ обработанной накладной из ЕСН."
            + " В накладной могут содержаться как состыкованные позиции, так и не состыкованные."
            + " После загрузки в АУ накладная получает в ЕСН статус 'Загружена в АУ' и более недоступна для повторной загрузки", 
            typeof(GetDocResult))
        ]
        public HttpResponseMessage GetDoc(GetDocServiceParam getDocServiceParam)
        {
            var result = esnService.GetDoc(getDocServiceParam);
            return getJsonResponse(result);
        }
    }
  
}
