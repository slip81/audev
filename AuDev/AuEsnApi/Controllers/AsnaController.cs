﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Filters;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuEsnApi.Models;
using AuEsnApi.Service;
using AuEsnApi.Json;
using Swashbuckle.Swagger.Annotations;
using System.Web.Http.Description;
#endregion

namespace AuEsnApi.Controllers
{
    /// <summary>
    /// Методы для работы с АСНА
    /// </summary>
    //[EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("asna")]
    public partial class AsnaController : BaseController
    {
        public AsnaController(IMainService _mainService, IWaService _waService, IEsnService _esnService, IAsnaService _asnaService)
            : base(_mainService, _waService, _esnService, _asnaService)
        {
            //
        }

        /// <summary>
        /// Проверка связи с сервисом (вариант 1, без проверки связи с БД)
        /// </summary>
        [HttpPost]
        [Route("Noop")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop()
        {
            asnaService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Проверка связи с сервисом (вариант 2, с проверкой связи с БД)
        /// </summary>
        [HttpPost]
        [Route("Noop2")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "Связь с сервисом успешно проверена")]
        public HttpResponseMessage Noop2()
        {
            asnaService.Noop2();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Обновление информации об аптеке от АСНА WebAPI
        /// </summary>
        /// <param name="asnaServiceParam">Код пользователа АСНА</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetStoreInfoAsna")]
        [UrlAuthorize()]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Информация обновлена", 
            typeof(AuEsnBaseResult))
        ]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetStoreInfoAsna(AsnaServiceParam asnaServiceParam)
        {
            var result = asnaService.GetStoreInfoAsna(asnaServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение информации об аптеке от АСНА WebAPI
        /// </summary>
        /// <param name="getStoreInfoServiceParam">Данные пользователя</param>
        /// <returns>Информация об аптеке</returns>
        [HttpPost]
        [Route("GetStoreInfo")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение информации об аптеке от АСНА",
            typeof(GetStoreInfoResult))
        ]        
        public HttpResponseMessage GetStoreInfo(UserInfo getStoreInfoServiceParam)
        {
            var result = asnaService.GetStoreInfo(getStoreInfoServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Обновление справочника связок от АСНА WebAPI
        /// </summary>
        /// <param name="asnaServiceParam">Код пользователа АСНА</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetGoodsLinks")]
        [UrlAuthorize()]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Справочник обновлен", 
            typeof(AuEsnBaseResult))
        ]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetGoodsLinks(AsnaServiceParam asnaServiceParam)
        {
            var result = asnaService.GetGoodsLinks(asnaServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отправка остатков на веб-сервис для дальнейшей передачи в АСНА WebAPI
        /// </summary>
        /// <param name="sendRemainsServiceParam">Данные об отправляемых остатках</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendRemains")]
        [SwaggerResponse(
            HttpStatusCode.OK, 
            "Отправка остатков из БД АУ на веб-сервис."
            + " Чтобы уменьшить кол-во позиций в одном запросе, реализован вариант"
            + " отправки данных пакетами:  на клиенте нужно сначала сформировать"
            + " весь набор данных для отправки, затем разбить его на пакеты по N записей,"
            + " пронумеровать все пакеты, подсчитать общее число пакетов и начать поочередную"
            + " отправку пакетов. В параметры part_num и part_cnt нужно записывать"
            + " номер текущего пакета и общее кол-во пакетов соответственно."
            + " <br/>"
            + " <strong>ВНИМАНИЕ !</strong> Обязательно нужно отправить все сформированные пакеты по очереди,"
            + " в случае нарушения порядка отправки или неотправки последних пакетов веб-сервис"
            + " вернет ошибку."
            + " <br/>"
            + " Существует режим тестирования (is_test = 1), в котором сервис только принимает все переданные строки и ничего с ними не делает."
            + " Для режима тестирования можно заполнять только данные о пользователе и передававемые строки"
            , 
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendRemains(SendRemainsServiceParam sendRemainsServiceParam)
        {
            var result = asnaService.SendRemains(sendRemainsServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Сброс активной цепочки пакетов отправки остатков на веб-сервис
        /// </summary>
        /// <param name="resetSendRemainsServiceParam">Данные пользователя</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ResetSendRemains")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "В случае непредвиденной ошибки при отправке одного из цепочки пакетов на веб-сервис"
            + " этот метод позволяет сбросить текущую активную цепочку и начать отправлять остатки заново",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage ResetSendRemains(UserInfo resetSendRemainsServiceParam)
        {
            var result = asnaService.ResetSendRemains(resetSendRemainsServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение изменений по заказам аптеки
        /// </summary>
        /// <param name="getOrdersServiceParam">Данные пользователя, дата с которой нужно получить изменения</param>
        /// <returns>Изменения по заказам аптеки</returns>
        [HttpPost]
        [Route("GetOrders")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Получение изменений по заказам аптеки."
            + " Возвращается четыре массива: заголовки заказов, строки заказов, статусы заголовков, статусы строк."
            + " Если какой-либо массив пустой - значим по этим объектам с заданной даты изменений не было.",
            typeof(GetOrdersResult))
        ]
        public HttpResponseMessage GetOrders(GetOrdersServiceParam getOrdersServiceParam)
        {
            var result = asnaService.GetOrders(getOrdersServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отправка изменений по заказам аптеки
        /// </summary>
        /// <param name="sendOrdersServiceParam">Данные пользователя, изменения по строкам и статусам заказов</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendOrders")]
        //[JsonDeserializeExt(Param = "sendOrdersServiceParam", JsonDataType = typeof(SendOrdersServiceParam))]        
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отправка изменений по заказам аптеки."
            + " Передается три массива: измененные строки заказов, новые статусы заголовков, новые статусы строк.",
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendOrders(SendOrdersServiceParam sendOrdersServiceParam)
        {
            var result = asnaService.SendOrders(sendOrdersServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Отправка сводного прайс-листа на веб-сервис для дальнейшей передачи в АСНА WebAPI
        /// </summary>
        /// <param name="sendConsPriceServiceParam">Данные об отправляемом сводном прайс-листе</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("SendConsPrice")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "Отправка сводного прайс-листа из БД АУ на веб-сервис."
            + " Чтобы уменьшить кол-во позиций в одном запросе, реализован вариант"
            + " отправки данных пакетами:  на клиенте нужно сначала сформировать"
            + " весь набор данных для отправки, затем разбить его на пакеты по N записей,"
            + " пронумеровать все пакеты, подсчитать общее число пакетов и начать поочередную"
            + " отправку пакетов. В параметры part_num и part_cnt нужно записывать"
            + " номер текущего пакета и общее кол-во пакетов соответственно."
            + " <br/>"
            + " <strong>ВНИМАНИЕ !</strong> Обязательно нужно отправить все сформированные пакеты по очереди,"
            + " в случае нарушения порядка отправки или неотправки последних пакетов веб-сервис"
            + " вернет ошибку."
            + " <br/>"
            + " Существует режим тестирования (is_test = 1), в котором сервис только принимает все переданные строки и ничего с ними не делает."
            + " Для режима тестирования можно заполнять только данные о пользователе и передававемые строки"
            ,
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage SendConsPrice(SendConsPriceServiceParam sendConsPriceServiceParam)
        {
            var result = asnaService.SendConsPrice(sendConsPriceServiceParam);
            return getJsonResponse(result);
        }


        /// <summary>
        /// Сброс активной цепочки пакетов отправки сводных прайс-листов на веб-сервис
        /// </summary>
        /// <param name="resetSendConsPriceServiceParam">Данные пользователя</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("ResetSendConsPrice")]
        [SwaggerResponse(
            HttpStatusCode.OK,
            "В случае непредвиденной ошибки при отправке одного из цепочки пакетов на веб-сервис"
            + " этот метод позволяет сбросить текущую активную цепочку и начать отправлять остатки заново",            
            typeof(AuEsnBaseResult))
        ]
        public HttpResponseMessage ResetSendConsPrice(UserInfo resetSendConsPriceServiceParam)
        {
            var result = asnaService.ResetSendConsPrice(resetSendConsPriceServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение статусов обработки заданий
        /// </summary>
        /// <param name="getTaskStateFromAsnaServiceParam">Данные пользователя, код задания</param>
        /// <returns>Статус обработки задания</returns>
        [HttpPost]
        [Route("GetTaskState")]
        //[UrlAuthorize()]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetTaskState(GetTaskStateFromAsnaServiceParam getTaskStateFromAsnaServiceParam)
        {
            var result = asnaService.GetTaskStateFromAsna(getTaskStateFromAsnaServiceParam);
            return getJsonResponse(result);
        }

        /// <summary>
        /// Получение текущих остатков от АСНА WebAPI
        /// </summary>
        /// <param name="getRemainsFromAsnaServiceParam">Код пользователа АСНА</param>
        /// <returns>Результат операции</returns>
        [HttpPost]
        [Route("GetRemainsFromAsna")]
        [UrlAuthorize()]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetRemainsFromAsna(GetRemainsFromAsnaServiceParam getRemainsFromAsnaServiceParam)
        {
            var result = asnaService.GetRemainsFromAsna(getRemainsFromAsnaServiceParam);
            return getJsonResponse(result);
        }
    }
}
