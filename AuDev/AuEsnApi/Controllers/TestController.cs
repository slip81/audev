﻿#region using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Ninject;
using Ninject.Web.Common;
using AuEsnApi.Models;
using AuEsnApi.Service;
using AuEsnApi.Json;
#endregion

namespace AuEsnApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "POST")]
    [RoutePrefix("test")]
    public class TestController : ApiController
    {
        private readonly IAuEsnTestService mainService;

        public TestController(IAuEsnTestService _mainService)
        {
            this.mainService = _mainService;
        }

        private HttpResponseMessage getJsonResponse(object value)
        {
            var settings = new JsonSerializerSettings() { ContractResolver = new NullToEmptyStringResolver() };
            string json = JsonConvert.SerializeObject(value, settings);
            //string json = JsonConvert.SerializeObject(value);
            var response = this.Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return response;
        }

        [HttpPost]
        [Route("Noop")]
        public HttpResponseMessage Noop()
        {
            mainService.Noop();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
