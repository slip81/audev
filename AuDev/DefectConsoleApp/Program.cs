﻿using System;
using System.Threading;
using System.Threading.Tasks;
/*
using Quartz;
using Quartz.Impl;
using Quartz.Job;
*/

namespace DefectConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Quartz
            /*
            try
            {
                Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter {Level = Common.Logging.LogLevel.Info};

                // Grab the Scheduler instance from the Factory 
                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();

                // and start it off
                scheduler.Start();

                // define the job and tie it to our HelloJob class
                IJobDetail job = JobBuilder.Create<HelloJob>()
                    .WithIdentity("job1", "group1")
                    .Build();

                // Trigger the job to run now, and then repeat every 10 seconds
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger1", "group1")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(10)
                        .RepeatForever())
                    .Build();

                // Tell quartz to schedule the job using our trigger
                scheduler.ScheduleJob(job, trigger);

                // some sleep to show what's happening
                Thread.Sleep(TimeSpan.FromSeconds(60));

                // and last shut down the scheduler when you are ready to close your program
                scheduler.Shutdown();
            }
            catch (SchedulerException se)
            {
                Console.WriteLine(se);
            }

            Console.WriteLine("Press any key to close the application");
            Console.ReadKey();
            */
            #endregion

            #region Simple (console)
            /*
            try
            {
                Console.WriteLine("\n");
                Console.WriteLine("Starting service...");

                DateTime date_beg = DateTime.Today.AddDays(-14);
                DateTime date_end = DateTime.Today.AddDays(-1);

                // AuMainDb
                var servProd = new DefectServ.DefectServiceClient("BasicHttpBinding_IDefectService");
                var servProd2 = new SenderServ.SenderServiceClient();

                Console.WriteLine("\n");
                Console.WriteLine("Downloading data from GR...");
                
                // госреестр
                var res1 = servProd.GR_DownloadList("iguana");
                if (res1.error != null)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("Error: " + res1.error.Message);
                    Console.WriteLine("\n");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }

                Console.WriteLine("\n");
                Console.WriteLine("Downloading data from Defect from " + date_beg.ToShortDateString() + " to " + date_end.ToShortDateString() + "...");

                var res2 = servProd.DownloadDefectList("iguana", date_beg, date_end, true);
                if (res2.error != null)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("Error: " + res2.error.Message);
                    Console.WriteLine("\n");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }

                Console.WriteLine("\n");
                Console.WriteLine("Sending GR...");

                // send Grls
                var res3 = servProd2.SendMail_Grls();

                Console.WriteLine("\n");
                Console.WriteLine("Sending Defect...");

                // send Defect
                var res4 = servProd2.SendMail_Defect();

                Console.WriteLine("\n");
                Console.WriteLine("Downloading data from Medical from " + date_beg.ToShortDateString() + " to " + date_end.ToShortDateString() + "...");

                // мед. изделия
                var res5 = servProd.DownloadMedicalList("iguana", date_beg, date_end);
                if (res5.error != null)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("Error: " + res5.error.Message);
                    Console.WriteLine("\n");
                    Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
                }

                Console.WriteLine("\n");
                Console.WriteLine("Press any key to close app");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n");
                Console.WriteLine("Exception: " + ex.Message);
                Console.WriteLine("\n");
                Console.WriteLine("Press any key to close app");
                Console.ReadKey();
            }            
            */
            #endregion

            #region Simple (no console)

           
            DateTime date_beg = DateTime.Today.AddDays(-14);
            DateTime date_end = DateTime.Today.AddDays(-1);

            // AuMainDb
            var servProd = new DefectServ.DefectServiceClient("BasicHttpBinding_IDefectService");
            var servProd2 = new SenderServ.SenderServiceClient();

            // госреестр
            servProd.GR_DownloadList("iguana");

            // брак
            servProd.DownloadDefectList("iguana", date_beg, date_end, true);

            // перед отправкой на почту брака и Госреестра - ждём минуту
            Thread.Sleep(60000);
  
            // send Grls
            servProd2.SendMail_Grls();

            // send Defect
            servProd2.SendMail_Defect();

            // мед. изделия
            servProd.DownloadMedicalList("iguana", date_beg, date_end);
            
            #endregion
        }
    }


}
