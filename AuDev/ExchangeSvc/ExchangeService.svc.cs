﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace ExchangeSvc
{
    
    public partial class ExchangeService : IExchangeService
    {
        #region CreateErrExchangeBase
        public T CreateErrExchangeBase<T>(Exception e)
            where T : ExchangeBase, new()
        {
            //T res = Activator.CreateInstance<T>();
            T res = new T();
            res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                )
                );
            return res;
        }
        #endregion

        #region ExchangePartner

        #region GetXml

        public ExchangeXml GetPlanXml(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xGetXml(userInfo, Enums.ExchangeItemEnum.Plan);
                xUpdateExchangePartnerReg("iguana", userInfo, false, true);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, false, true);
                string mess = "Ошибка в GetPlanXml: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetPlanXml";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ?(long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Plan
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetPlanXml: " + StaticUtil.ExceptionInfo(e), null, userInfo != null ? userInfo.login : "", log_date_beg, )
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public ExchangeXml GetPlanXml_byDocNum(UserInfo userInfo, string docNum)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                // !!! todo
                return xGetXml(userInfo, Enums.ExchangeItemEnum.Plan);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetPlanXml_byDocNum: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetPlanXml_byDocNum";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Plan
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetPlanXml_byDocNum: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public ExchangeXml GetPlanXml_byDocDate(UserInfo userInfo, DateTime docDate)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                // !!! todo
                return xGetXml(userInfo, Enums.ExchangeItemEnum.Plan);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetPlanXml_byDocDate: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetPlanXml_byDocDate";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Plan
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetPlanXml_byDocDate: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public ExchangeXml GetDiscountXml(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xGetXml(userInfo, Enums.ExchangeItemEnum.Discount);
                xUpdateExchangePartnerReg("iguana", userInfo, false, true);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, false, true);
                string mess = "Ошибка в GetDiscountXml: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetDiscountXml";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Discount
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetDiscountXml: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public ExchangeXml GetDiscountXml_byDocNum(UserInfo userInfo, string docNum)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                // !!! todo
                return xGetXml(userInfo, Enums.ExchangeItemEnum.Discount);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetDiscountXml_byDocNum: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetDiscountXml_byDocNum";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Discount
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetDiscountXml_byDocNum: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public ExchangeXml GetDiscountXml_byDocDate(UserInfo userInfo, DateTime docDate)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                // !!! todo
                return xGetXml(userInfo, Enums.ExchangeItemEnum.Discount);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetDiscountXml_byDocDate: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetDiscountXml_byDocDate";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Discount
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetDiscountXml_byDocDate: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public ExchangeXml GetRecommendXml(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xGetXml(userInfo, Enums.ExchangeItemEnum.Recommend);
                xUpdateExchangePartnerReg("iguana", userInfo, false, true);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, false, true);
                string mess = "Ошибка в GetRecommendXml: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetRecommendXml";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Recommend
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetRecommendXml: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public ExchangeXml GetRecommendXml_byDocNum(UserInfo userInfo, string docNum)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                // !!! todo
                return xGetXml(userInfo, Enums.ExchangeItemEnum.Recommend);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetRecommendXml_byDocNum: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetRecommendXml_byDocNum";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Recommend
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetRecommendXml_byDocNum: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public ExchangeXml GetRecommendXml_byDocDate(UserInfo userInfo, DateTime docDate)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                // !!! todo
                return xGetXml(userInfo, Enums.ExchangeItemEnum.Recommend);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetRecommendXml_byDocDate: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetRecommendXml_byDocDate";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Recommend
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetRecommendXml_byDocDate: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        public string Test_GetXml(string pwd, string filePath, int exchangeTypeInt)
        {            
            try
            {
                return xTest_GetXml(pwd, filePath, exchangeTypeInt);
            }
            catch (Exception e)
            {
                return GlobalUtil.ExceptionInfo(e);
            }
        }

        public ExchangeXml Test_GetXml2(string pwd, UserInfo userInfo, int exchangeTypeInt)
        {
            try
            {
                return xTest_GetXml2(pwd, userInfo, (Enums.ExchangeItemEnum)exchangeTypeInt);
            }
            catch (Exception e)
            {
                return CreateErrExchangeBase<ExchangeXml>(e);
            }
        }

        #endregion

        #region SendData

        public ServiceOperationResult SendOstData(UserInfo userInfo, string data)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSendData(userInfo, data, Enums.ExchangeItemEnum.Ost);
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                string mess = "Ошибка в SendOstData: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendOstData";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)Enums.ExchangeItemEnum.Ost
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SendMovData(UserInfo userInfo, string data)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSendData(userInfo, data, Enums.ExchangeItemEnum.Move);
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                string mess = "Ошибка в SendMovData: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendMovData";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)Enums.ExchangeItemEnum.Move
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SendQueData(UserInfo userInfo, string data)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSendData(userInfo, data, Enums.ExchangeItemEnum.Que);
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                string mess = "Ошибка в SendQueData: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendQueData";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)Enums.ExchangeItemEnum.Que
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SendClientData(UserInfo userInfo, string data)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSendData(userInfo, data, Enums.ExchangeItemEnum.Client);
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                string mess = "Ошибка в SendClientData: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendClientData";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)Enums.ExchangeItemEnum.Client
                    , ds_id, null, 2, mess2);                
                //Loggly.InsertLog_ExchangePartner(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SendPrepData(UserInfo userInfo, string data)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSendData(userInfo, data, Enums.ExchangeItemEnum.Prep);
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                string mess = "Ошибка в SendPrepData: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendPrepData";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)Enums.ExchangeItemEnum.Prep
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SendAvgData(UserInfo userInfo, string data)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSendData(userInfo, data, Enums.ExchangeItemEnum.Avg);
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                return res;
            }
            catch (Exception e)
            {
                xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                string mess = "Ошибка в SendAvgData: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendAvgData";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)Enums.ExchangeItemEnum.Avg
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SendData(UserInfo userInfo, string data, DataInfo dataInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSendData2(userInfo, data, dataInfo);
                //xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                return res;
            }
            catch (Exception e)
            {
                //xUpdateExchangePartnerReg("iguana", userInfo, true, false);
                string mess = "Ошибка в SendData: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendData";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)Enums.ExchangeItemEnum.Avg
                    , ds_id, null, 2, mess2);
                //Loggly.InsertLog_ExchangePartner(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        #endregion

        #region GetExchangeData

        public ExchangeData GetExchangeData(UserInfo userInfo, long exchange_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetExchangeData(userInfo, exchange_id);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetExchangeData: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetExchangeData";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";                
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangePartner(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeData>(e);
            }
        }

        #endregion

        #region GetSendAll

        public int GetSendAll(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetSendAll(userInfo);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetSendAll: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetSendAll";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetSendAll: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return (int)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            }
        }

        #endregion

        #region GetParams

        public ExchangeParamsList GetParams(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetParams(userInfo);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetParams: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetParams";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetParams: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeParamsList>(e);
            }
        }

        public ExchangeParamsList GetParams_SalePoint(UserInfo userInfo, int sale_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetParams_SalePoint(userInfo, sale_id);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetParams_SalePoint: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetParams_SalePoint";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetParams_SalePoint: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ExchangeParamsList>(e);
            }
        }

        #endregion

        #region GetPartnerList

        public PartnerList GetPartnerList()
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetPartnerList();
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetPartnerList: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetPartnerList";
                string user_name = "exchanger";
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangePartner("Ошибка в GetPartnerList: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<PartnerList>(e);
            }
        }

        #endregion

        #region SendToExchangeLog

        public ServiceOperationResult SendToExchangeLog(UserInfo userInfo, string text, int id, int is_error, int exchange_direction, int exchange_type)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xSendToExchangeLog(userInfo, text, id, is_error, exchange_direction, exchange_type);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SendToExchangeLog: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendToExchangeLog";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, exchange_direction, exchange_type
                    , ds_id, null, 2, mess2);                
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        #endregion

        #region UpdateExchangePartnerReg

        public ServiceOperationResult UpdateExchangePartnerReg(string password, UserInfo userInfo, bool update_upload, bool update_download)
        {
            return xUpdateExchangePartnerReg(password, userInfo, update_upload, update_download);
        }

        #endregion

        #region ExchangeTask

        public ExchangeTaskList GetExchangeTaskList(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xGetExchangeTaskList(userInfo);
                return res;
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetExchangeTaskList: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetExchangeTaskList";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);                
                return CreateErrExchangeBase<ExchangeTaskList>(e);
            }
        }

        public ServiceOperationResult SetExchangeTask_Received(UserInfo userInfo, long task_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSetExchangeTask_Received(userInfo, task_id);
                return res;
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SetExchangeTask_Received: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SetExchangeTask_Received";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SetExchangeTaskList_Received(UserInfo userInfo, ExchangeTaskList taskList)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSetExchangeTaskList_Received(userInfo, taskList);
                return res;
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SetExchangeTaskList_Received: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SetExchangeTaskList_Received";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SetExchangeTask_Fail(UserInfo userInfo, long task_id, string comment)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSetExchangeTask_Fail(userInfo, task_id, comment);
                return res;
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SetExchangeTask_Fail: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SetExchangeTask_Fail";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SetExchangeTaskList_Fail(UserInfo userInfo, ExchangeTaskList taskList)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSetExchangeTaskList_Fail(userInfo, taskList);
                return res;
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SetExchangeTaskList_Received: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SetExchangeTaskList_Received";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SetExchangeTask_Success(UserInfo userInfo, long task_id)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSetExchangeTask_Success(userInfo, task_id);
                return res;
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SetExchangeTask_Success: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SetExchangeTask_Success";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        public ServiceOperationResult SetExchangeTaskList_Success(UserInfo userInfo, ExchangeTaskList taskList)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                var res = xSetExchangeTaskList_Success(userInfo, taskList);
                return res;
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SetExchangeTaskList_Success: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SetExchangeTaskList_Success";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        #endregion

        #endregion

        #region ExchangeDocument

        #region GetSalesList

        public SalesList GetSalesList(UserInfo userInfo)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetSalesList(userInfo);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetSalesList: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetSalesList";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangeDocument(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangeDocument("Ошибка в GetSalesList: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<SalesList>(e);
            }
        }

        #endregion

        #region SendDocument

        public ServiceOperationResult SendDocument(UserInfo userInfo, Document doc, int salesId, bool toEmail)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xSendDocument(userInfo, doc, salesId, toEmail);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SendDocument: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendDocument";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangeDocument(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangeDocument("Ошибка в SendDocument: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        #endregion

        #region GetDocuments

        public DocumentList GetDocuments(UserInfo userInfo, int docType)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDocuments(userInfo, docType);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetDocuments: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetDocuments";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangeDocument(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangeDocument("Ошибка в GetDocuments: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<DocumentList>(e);
            }
        }

        #endregion

        #region ChangeDocumentState

        public DocumentList ChangeDocumentState(UserInfo userInfo, DocumentList docList, int nextState)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xChangeDocumentState(userInfo, docList, nextState);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в ChangeDocumentState: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "ChangeDocumentState";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangeDocument(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangeDocument("Ошибка в ChangeDocumentState: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<DocumentList>(e);
            }
        }

        #endregion

        #region GetDocumentStates


        public DocumentList GetDocumentStates(UserInfo userInfo, DocumentList docList)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetDocumentStates(userInfo, docList);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetDocumentStates: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetDocumentStates";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangeDocument(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangeDocument("Ошибка в GetDocumentStates: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<DocumentList>(e);
            }
        }

        #endregion

        #region GetNotReceivedDocuments

        public DocumentList GetNotReceivedDocuments(UserInfo userInfo, int docType, int salesId)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xGetNotReceivedDocuments(userInfo, docType, salesId);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в GetNotReceivedDocuments: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "GetNotReceivedDocuments";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                Loggly.InsertLog_ExchangeDocument(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangeDocument("Ошибка в GetDocumentStates: " + StaticUtil.ExceptionInfo(e), (long)Enums.LogEventType.ERROR, "exchanger", null);
                return CreateErrExchangeBase<DocumentList>(e);
            }
        }

        #endregion

        #endregion


        #region SendMail

        public ServiceOperationResult SendMail(string text, string topic, string sendTo, string sendFrom)
        {
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xSendMail(text, topic, sendTo, sendFrom);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в SendMail: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "SendMail";
                string user_name = "exchanger";
                Loggly.InsertLog_ExchangeDocument(mess, (long)Enums.LogEventType.ERROR, user_name, null, log_date_beg, mess2);
                //Loggly.InsertLog_ExchangeDocument(error_str, (long)Enums.LogEventType.ERROR, "exchanger", null);                
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }
        }

        #endregion


        #region JSON Support

        public void GetOptions()
        {
            // Заголовки обработаются в EnableCorsMessageInspector 
        }

        #endregion

    }
}
