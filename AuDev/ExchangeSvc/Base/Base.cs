﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AuDev.Common.Util;

namespace ExchangeSvc
{    
    #region ExchangeBase

    [DataContract]
    public class ExchangeBase
    {
        public ExchangeBase()
            : base()
        {
            //            
        }

        [DataMember]
        public ErrInfo error { get; set; }
    }

    #endregion

    #region ErrInfo

    [DataContract]
    public class ErrInfo
    {
        public ErrInfo(long? id, string message = "")
        {
            Id = id;
            Message = message;
        }

        public ErrInfo(long? id, Exception e)
        {
            Id = id;
            if (e == null)
                return;

            Message = e.Message.Trim()
                + (e.InnerException == null ? "" : "[" + e.InnerException.Message.Trim() + "]"
                + (e.InnerException.InnerException == null ? "" : "[" + e.InnerException.InnerException.Message.Trim() + "]")
                );

        }

        [DataMember]
        public long? Id { get; set; }
        [DataMember]
        public string Message { get; set; }
    }

    #endregion

    #region ServiceOperationResult

    [DataContract]
    public class ServiceOperationResult : ExchangeBase
    {
        public ServiceOperationResult()
            :base()
        {
        }

        public ServiceOperationResult(long _id, ErrInfo _error)
        {
            Id = _id;
            error = _error;
        }

        public ServiceOperationResult(long _id, Exception _e)
        {
            Id = _id;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        public ServiceOperationResult(ErrInfo _error)
        {
            Id = (_error == null) ? (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION : (long)_error.Id;
            error = _error;
        }

        public ServiceOperationResult(Exception _e)
        {
            Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION;
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        [DataMember]
        public long Id { get; set; }
    }

    #endregion

    #region UserInfo
    /// <summary>
    /// Параметры обмена пользователя с сервисом 
    /// </summary>
    [DataContract]    
    public class UserInfo
    {
        public UserInfo()            
        {
            //            
        }

        /// <summary>
        /// Логин торговой точки
        /// </summary>
        [DataMember]
        public string login { get; set; }
        /// <summary>
        /// Код рабочего места
        /// </summary>
        [DataMember]
        public string workplace { get; set; }
        /// <summary>
        /// Номер версии программы
        /// </summary>
        [DataMember]
        public string version_num { get; set; }
        /// <summary>
        /// Код Партнера, с которым производится обмен
        /// </summary>
        [DataMember]
        public long ds_id { get; set; }
    }
    #endregion

    #region ExchangeParams

    [DataContract]
    public class ExchangeParams : ExchangeBase
    {
        public ExchangeParams()
            :base()
        {
        }

        public ExchangeParams(ErrInfo _error)
        {
            error = _error;
        }

        public ExchangeParams(Exception _e)
        {
            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, _e);
        }

        [DataMember]
        public long ds_id { get; set; }
        [DataMember]
        public string ds_name { get; set; }
        [DataMember]
        public int send_all { get; set; }
        [DataMember]
        public int days_cnt_for_mov { get; set; }
        [DataMember]
        public ExchangeItemList exchange_item_list { get; set; }
        [DataMember]
        public string login { get; set; }
        [DataMember]
        public string workplace { get; set; }
        [DataMember]
        public int sales_prefix { get; set; }
        [DataMember]
        public string reg_code { get; set; }
        [DataMember]
        public string reg_name { get; set; }

    }

    [DataContract]
    public class ExchangeParamsList : ExchangeBase
    {
        public ExchangeParamsList()
            : base()
        {
            ExchangeParams_list = new List<ExchangeParams>();
        }

        [DataMember]
        public List<ExchangeParams> ExchangeParams_list;
    }

    #endregion

    #region DataInfo

    [DataContract]
    public class DataInfo
    {
        public DataInfo()
        {
            //            
        }

        [DataMember]
        public int item_type_id { get; set; }
        [DataMember]
        public string item_file_name { get; set; }
        [DataMember]
        public int is_packed { get; set; }
    }
    #endregion
}
