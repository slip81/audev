﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeSvc
{
    [DataContract]
    public class Document : ExchangeBase
    {
        public Document()
        {
            //
        }

        [DataMember]
        public int DocId { get; set; }
        [DataMember]
        public string DocName { get; set; }
        [DataMember]
        public string DocFormat { get; set; }
        //[DataMember]
        public byte[] ContentAsBytes { get; set; }
        [DataMember]
        public string ContentAsString { get; set; }
        [DataMember]
        public int State { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public int SalesId { get; set; }
        [DataMember]
        public string SalesName { get; set; }
        [DataMember]
        public int DocType { get; set; }
        [DataMember]
        public Nullable<System.DateTime> DateBeg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> DateEnd { get; set; }
        
        public bool ForDel { get; set; }        
        public bool Deleted { get; set; }

        [DataMember]
        public List<DocumentState> DocumentStateList { get; set; }
    }

    [DataContract]
    public class DocumentList : ExchangeBase
    {
        public DocumentList()
            : base()
        {
            //
        }

        [DataMember]
        public List<Document> Document_list;
    }
}
