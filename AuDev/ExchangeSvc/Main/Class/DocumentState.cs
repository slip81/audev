﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeSvc
{
    [DataContract]
    public class DocumentState : ExchangeBase
    {
        public DocumentState()
        {
            //
        }

        [DataMember]
        public int SalesId { get; set; }
        [DataMember]
        public int State { get; set; }
    }
}
