﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AuDev.Common.Util;
#endregion

namespace ExchangeSvc
{
    public class ClientData
    {
        public ClientData(string source_data, string prefix)
        {
            StringData = source_data;
            WithHeaders = true;
            CodePrefix = prefix;
            ConvertFromString();
        }

        private void ConvertFromString()
        {
            int line_num = 1;
            int item_index = -1;
            //
            StringData = StringData.Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "\r\n");
            foreach (var line in StringData.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            //foreach (var line in StringData.Split(new string[] { @"\n", @"\r\n" }, StringSplitOptions.None))
            {
                item_index = -1;
                foreach (var item in line.Split(new string[] { ";" }, StringSplitOptions.None))
                {
                    item_index++;
                    
                    if ((line_num == 1) && (WithHeaders))
                    {
                        #region ищем заголовки в файле
                        if (item.Equals(NameOf_ID))
                        {
                            IndexOf_ID = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Name))
                        {
                            IndexOf_Name = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_INN))
                        {
                            IndexOf_INN = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_RegionIDN))
                        {
                            IndexOf_RegionIDN = item_index;
                            continue;
                        }
                        #endregion
                    }
                    else
                    {
                        #region считываем данные
                        if (item_index == IndexOf_ID)
                        {
                            if (ID == null)
                                ID = new List<string>();
                            ID.Add(CodePrefix + item);
                        }
                        else if (item_index == IndexOf_Name)
                        {
                            if (Name == null)
                                Name = new List<string>();
                            Name.Add(item);
                        }
                        else if (item_index == IndexOf_INN)
                        {
                            if (INN == null)
                                INN = new List<string>();
                            INN.Add(item);
                        }
                        else if (item_index == IndexOf_RegionIDN)
                        {
                            if (RegionIDN == null)
                                RegionIDN = new List<string>();
                            RegionIDN.Add(item);
                        }
                        #endregion
                    }

                }
                line_num++;
            }
            RowCount = WithHeaders ? (line_num - 2) : (line_num - 1);
        }

        public string ConvertToFormat(Enums.DataSourceEnum format_type)
        {            
            switch (format_type)
            {
                case Enums.DataSourceEnum.ASNA:
                    //return ConvertToFormat_ASNA();
                    return StringData;
                default:
                    return StringData;
            }            
        }

        public string StringData { get; set; }
        public bool WithHeaders { get; set; }
        public int RowCount { get; set; }
        public string ClientName { get; set; }
        public string RegName { get; set; }
        public string CodePrefix { get; set; }

        public List<string> ID { get; set; }
        public List<string> Name { get; set; }
        public List<string> INN { get; set; }
        public List<string> RegionIDN { get; set; }

        private int IndexOf_ID = -1;
        private int IndexOf_Name = -1;
        private int IndexOf_INN = -1;
        private int IndexOf_RegionIDN = -1;

        private const string NameOf_ID = "ID";
        private const string NameOf_Name = "Name";
        private const string NameOf_INN = "INN";
        private const string NameOf_RegionIDN = "RegionIDN";
    }

}
