﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeSvc
{
    [DataContract]
    public class License : ExchangeBase
    {
        public License()
        {
            //
        }
        
        [DataMember]
        public long LicenseId { get; set; }
        [DataMember]
        public Nullable<System.DateTime> CreatedOn { get; set; }
        [DataMember]
        public Nullable<System.DateTime> DateStart { get; set; }
        [DataMember]
        public Nullable<System.DateTime> DateEnd { get; set; }
        [DataMember]
        public string Login { get; set; }
        [DataMember]
        public string Workplace { get; set; }
        [DataMember]
        public string VersionNum { get; set; }
        [DataMember]
        public string SalesName { get; set; }
        [DataMember]
        public string ClientName { get; set; }
        [DataMember]
        public Nullable<int> SalesPrefix { get; set; }
        [DataMember]
        public long ClientId { get; set; }
        [DataMember]
        public long SalesId { get; set; }
    }

    [DataContract]
    public class LicenseList : ExchangeBase
    {
        public LicenseList()
            : base()
        {
            //
        }

        [DataMember]
        public List<License> License_list;
    }
}
