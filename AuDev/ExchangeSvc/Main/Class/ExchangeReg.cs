﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeReg : ExchangeBase
    {
        public ExchangeReg()
        {
            //
        }

        public ExchangeReg(exchange_reg item)
        {
            db_exchange_reg = item;
            reg_id = item.reg_id;
            reg_type = item.reg_type;
        }

        [DataMember]
        public long reg_id { get; set; }
        [DataMember]
        public int reg_type { get; set; }

        public exchange_reg Db_exchange_reg { get { return db_exchange_reg; } }
        private exchange_reg db_exchange_reg;

    }

    [DataContract]
    public class ExchangeRegList : ExchangeBase
    {
        public ExchangeRegList()
            : base()
        {
            //
        }

        public ExchangeRegList(List<exchange_reg> exchangeRegList)
        {
            if (exchangeRegList != null)
            {
                ExchangeReg_list = new List<ExchangeReg>();
                foreach (var item in exchangeRegList)
                {
                    ExchangeReg_list.Add(new ExchangeReg(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeReg> ExchangeReg_list;
    }

}
