﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AuDev.Common.Extensions;

namespace ExchangeSvc
{
    public class MovData: OstData
    {
        public MovData(string source_data, string prefix)
            :base(source_data, prefix)
        {
            //
        }

        protected override string ConvertToFormat_ASNA()
        {
            string res = "";
            int i = 1;
            string items_separator = "|";
            string symbol_for_null = "";
            string symbol_for_zero = "0";
            string symbol_for_default_doc_type = "1";

            while (i <= RowCount)
            {
                DateTime? date_doc = !D_DOK.IsEmpty(i - 1) ? (DateTime?)(DateTime.ParseExact(D_DOK[i - 1], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture)) : null;
                string date_doc_str = date_doc != null ? ((DateTime)date_doc).ToString("yyyy-MM-dd HH:mm:ss") : symbol_for_null;

                /*
                string doc_type = D_TYPE != null ? D_TYPE[i - 1] : "0";
                int doc_type_int = 0;
                int.TryParse(doc_type, out doc_type_int);
                int doc_type_converted = ConvertDocType(doc_type_int);
                */

                res += (symbol_for_null.DoubleQouted() + items_separator) //Внутренний код строки документа
                    + (symbol_for_null.DoubleQouted() + items_separator)  //Внутренний код заголовка документа    
                    + (ClientName.DoubleQouted() + items_separator)   //Юридическое лицо (Учетная система)
                    + (RegName.DoubleQouted() + items_separator)  //Торговый объект
                    //+ doc_type_converted.ToString() + items_separator
                    + ((!D_TYPE.IsEmpty(i - 1) ? D_TYPE[i - 1] : symbol_for_default_doc_type) + items_separator)  //Тип документа
                    + ((!Supplier_INN.IsEmpty(i - 1) ? Supplier_INN[i - 1] : symbol_for_null).DoubleQouted() + items_separator)   //Контрагент
                    + (date_doc_str + items_separator)    //Дата документа
                    + ((!N_DOK.IsEmpty(i - 1) ? N_DOK[i - 1] : symbol_for_null).DoubleQouted() + items_separator) //Номер документа
                    + (symbol_for_zero + items_separator) //Вида оплаты
                    + (symbol_for_null.DoubleQouted() + items_separator)  //Внутренний код дисконтной программы
                    + (symbol_for_null.DoubleQouted() + items_separator) //Дисконтная программа
                    + (symbol_for_null.DoubleQouted() + items_separator)  //Внутренний код дисконтной карты
                    + (symbol_for_null.DoubleQouted() + items_separator)  //Номер дисконтной карты
                    + ((!Drug_Code.IsEmpty(i - 1) ? Drug_Code[i - 1] : symbol_for_null).DoubleQouted() + items_separator) //Товар
                    + ((!Quant.IsEmpty(i - 1) ? Quant[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator)  //Количество товара
                    + ((!Supplier_INN.IsEmpty(i - 1) ? Supplier_INN[i - 1] : symbol_for_null).DoubleQouted() + items_separator)   //Поставщик товара
                    + ((!RegionIDN.IsEmpty(i - 1) ? RegionIDN[i - 1] : symbol_for_zero) + items_separator)    //Код региона поставщика товара
                    + ((!SUM_OPT.IsEmpty(i - 1) ? SUM_OPT[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator)  //Сумма оптовая без НДС
                    + ((!Sum_Zak.IsEmpty(i - 1) ? Sum_Zak[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator)  //Сумма оптовая с НДС
                    + ((!SUM_NDS.IsEmpty(i - 1) ? SUM_NDS[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator)  //Сумма НДС
                    + ((!Sum_Rozn.IsEmpty(i - 1) ? Sum_Rozn[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator)    //Сумма розничная
                    + ((!SUM_REAL_DISCOUNT.IsEmpty(i - 1) ? SUM_REAL_DISCOUNT[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator)  //Сумма реализации
                    + ((!Disk_Sum.IsEmpty(i - 1) ? Disk_Sum[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator)    //Сумма скидки
                    + symbol_for_zero // Статус строки документа
                    ;
                res += System.Environment.NewLine;

                i++;
            }

            return res;
        }

        private int ConvertDocType(int source_doc_type)
        {
            switch (source_doc_type)
            {
                case 29:
                    return 1;
                case 20:
                    return 2;
                case 10:
                    return 3;
                case 19:
                    return 4;
                case 21:
                    return 7;
                default:
                    return 0;
            }
        }
    }
}
