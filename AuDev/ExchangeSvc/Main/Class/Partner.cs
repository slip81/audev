﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class Partner : ExchangeBase
    {
        public Partner()
        {
            //
        }

        public Partner(datasource item)
        {
            db_datasource = item;
            ds_id = item.ds_id;
            ds_name = item.ds_name;
        }

        [DataMember]
        public long ds_id { get; set; }
        [DataMember]
        public string ds_name { get; set; }

        public datasource Db_datasource { get { return db_datasource; } }
        private datasource db_datasource;

    }

    [DataContract]
    public class PartnerList : ExchangeBase
    {
        public PartnerList()
            : base()
        {
            //
        }

        public PartnerList(List<datasource> partnerList)
        {
            if (partnerList != null)
            {
                Partner_list = new List<Partner>();
                foreach (var item in partnerList)
                {
                    Partner_list.Add(new Partner(item));
                }
            }
        }

        [DataMember]
        public List<Partner> Partner_list;
    }
}
