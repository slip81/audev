﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace ExchangeSvc
{
    [DataContract]
    public class Sales : ExchangeBase
    {
        public Sales()
        {
            //
        }
        
        [DataMember]
        public int SalesId { get; set; }
        [DataMember]
        public string SalesName { get; set; }
        [DataMember]
        public string EmailAddress { get; set; }
    }

    [DataContract]
    public class SalesList : ExchangeBase
    {
        public SalesList()
            : base()
        {
            //
        }

        [DataMember]
        public List<Sales> Sales_list;
    }
}
