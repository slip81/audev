﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeClient : ExchangeBase
    {
        public ExchangeClient()
        {
            //
        }

        public ExchangeClient(exchange_client item)
        {
            client_id = item.client_id;
            client_name = item.client_name;
            cabinet_client_id = item.cabinet_client_id;
        }

        [DataMember]
        public long client_id { get; set; }
        [DataMember]
        public string client_name { get; set; }
        [DataMember]
        public Nullable<long> cabinet_client_id { get; set; }

        public exchange_client Db_exchange_client { get { return db_exchange_client; } }
        private exchange_client db_exchange_client;

    }

    [DataContract]
    public class ExchangeClientList : ExchangeBase
    {
        public ExchangeClientList()
            : base()
        {
            //
        }

        public ExchangeClientList(List<exchange_client> exchangeClientList)
        {
            if (exchangeClientList != null)
            {
                ExchangeClient_list = new List<ExchangeClient>();
                foreach (var item in exchangeClientList)
                {
                    ExchangeClient_list.Add(new ExchangeClient(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeClient> ExchangeClient_list;
    }
}
