﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace ExchangeSvc
{
    public class AvgData
    {

        public AvgData(string source_data, string prefix)
        {
            StringData = source_data;
            WithHeaders = true;
            CodePrefix = prefix;
            ConvertFromString();            
        }

        private void ConvertFromString()
        {
            //
            int line_num = 1;
            int item_index = -1;
            
            StringData = StringData.Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "\r\n");
            foreach (var line in StringData.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            //foreach (var line in StringData.Split(new string[] { @"\n", @"\r\n" }, StringSplitOptions.None))
            {
                item_index = -1;
                foreach (var item in line.Split(new string[] { ";" }, StringSplitOptions.None))
                {
                    item_index++;
                    
                    if ((line_num == 1) && (WithHeaders))
                    {
                        #region ищем заголовки в файле
                        if (item.Equals(NameOf_ID_UL))
                        {
                            IndexOf_ID_UL = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_ID_Point))
                        {
                            IndexOf_ID_Point = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_D_TYPE))
                        {
                            IndexOf_D_TYPE = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Dat_Begin))
                        {
                            IndexOf_Dat_Begin = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Dat_End))
                        {
                            IndexOf_Dat_End = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Quant))
                        {
                            IndexOf_Quant = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_OPT))
                        {
                            IndexOf_SUM_OPT = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_OPT_NDS))
                        {
                            IndexOf_SUM_OPT_NDS = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_NDS))
                        {
                            IndexOf_SUM_NDS = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_ROZN))
                        {
                            IndexOf_SUM_ROZN = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_REAL))
                        {
                            IndexOf_SUM_REAL = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_DISCOUNT))
                        {
                            IndexOf_SUM_DISCOUNT = item_index;
                            continue;
                        }
                        #endregion
                    }
                    else
                    {
                        #region считываем данные
                        if (item_index == IndexOf_ID_UL)
                        {
                            if (ID_UL == null)
                                ID_UL = new List<string>();
                            ID_UL.Add(item);
                        }
                        else if (item_index == IndexOf_ID_Point)
                        {
                            if (ID_Point == null)
                                ID_Point = new List<string>();
                            ID_Point.Add(item);
                        }
                        else if (item_index == IndexOf_D_TYPE)
                        {
                            if (D_TYPE == null)
                                D_TYPE = new List<string>();
                            D_TYPE.Add(item);
                        }
                        else if (item_index == IndexOf_Dat_Begin)
                        {
                            if (Dat_Begin == null)
                                Dat_Begin = new List<string>();
                            Dat_Begin.Add(item);
                        }
                        else if (item_index == IndexOf_Dat_End)
                        {
                            if (Dat_End == null)
                                Dat_End = new List<string>();
                            Dat_End.Add(item);
                        }
                        else if (item_index == IndexOf_Quant)
                        {
                            if (Quant == null)
                                Quant = new List<string>();
                            Quant.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_OPT)
                        {
                            if (SUM_OPT == null)
                                SUM_OPT = new List<string>();
                            SUM_OPT.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_OPT_NDS)
                        {
                            if (SUM_OPT_NDS == null)
                                SUM_OPT_NDS = new List<string>();
                            SUM_OPT_NDS.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_NDS)
                        {
                            if (SUM_NDS == null)
                                SUM_NDS = new List<string>();
                            SUM_NDS.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_ROZN)
                        {
                            if (SUM_ROZN == null)
                                SUM_ROZN = new List<string>();
                            SUM_ROZN.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_REAL)
                        {
                            if (SUM_REAL == null)
                                SUM_REAL = new List<string>();
                            SUM_REAL.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_DISCOUNT)
                        {
                            if (SUM_DISCOUNT == null)
                                SUM_DISCOUNT = new List<string>();
                            SUM_DISCOUNT.Add(item);
                        }
                        #endregion
                    }
                }
                line_num++;
            }
            RowCount = WithHeaders ? (line_num - 2) : (line_num - 1);
        }

        public string ConvertToFormat(Enums.DataSourceEnum format_type)
        {            
            switch (format_type)
            {
                case Enums.DataSourceEnum.ASNA:
                    return ConvertToFormat_ASNA();
                default:
                    return StringData;
            }            
        }

        protected virtual string ConvertToFormat_ASNA()
        {
            string res = "";
            int i = 1;
            string items_separator = "|";
            string symbol_for_null = "";
            string symbol_for_zero = "0";
            
            while (i <= RowCount)
            {
                DateTime? date_beg = !Dat_Begin.IsEmpty(i - 1) ? (DateTime?)(DateTime.ParseExact(Dat_Begin[i - 1], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture)) : null;
                string date_beg_str = date_beg != null ? ((DateTime)date_beg).ToString("yyyy-MM-dd HH:mm:ss") : symbol_for_null;
                DateTime? date_end = !Dat_End.IsEmpty(i - 1) ? (DateTime?)(DateTime.ParseExact(Dat_End[i - 1], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture)) : null;
                string date_end_str = date_end != null ? ((DateTime)date_end).ToString("yyyy-MM-dd HH:mm:ss") : symbol_for_null;

                res += ClientName.DoubleQouted() + items_separator  //Юридическое лицо (Учетная система)
                    + RegName.DoubleQouted() + items_separator  //Торговый объект
                    + (!D_TYPE.IsEmpty(i - 1) ? D_TYPE[i - 1] : symbol_for_zero) + items_separator  //Тип документа
                    + date_beg_str + items_separator    //Дата начала
                    + date_end_str + items_separator    //Дата окончания
                    + (!Quant.IsEmpty(i - 1) ? Quant[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Количество
                    + (!SUM_OPT.IsEmpty(i - 1) ? SUM_OPT[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма оптовая без НДС
                    + (!SUM_OPT_NDS.IsEmpty(i - 1) ? SUM_OPT_NDS[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма оптовая с НДС
                    + (!SUM_NDS.IsEmpty(i - 1) ? SUM_NDS[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма НДС
                    + (!SUM_ROZN.IsEmpty(i - 1) ? SUM_ROZN[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator    //Сумма розничная
                    + (!SUM_REAL.IsEmpty(i - 1) ? SUM_REAL[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator    //Сумма реализации
                    + (!SUM_DISCOUNT.IsEmpty(i - 1) ? SUM_DISCOUNT[i - 1].Replace(',', '.') : symbol_for_zero)  //Сумма скидки
                    ;
                res += System.Environment.NewLine;

                i++;
            }            
            return res;
        }

        public string StringData { get; set; }
        public bool WithHeaders { get; set; }
        public int RowCount { get; set; }
        public string ClientName { get; set; }
        public string RegName { get; set; }
        public string CodePrefix { get; set; }

        /*
         ID_UL;ID_Point;D_TYPE;Dat_Begin;Dat_End;Quant;SUM_OPT;SUM_OPT_NDS;SUM_NDS;SUM_ROZN;SUM_REAL;SUM_DISCOUNT
        */

        public List<string> ID_UL { get; set; }
        public List<string> ID_Point { get; set; }
        public List<string> D_TYPE { get; set; }
        public List<string> Dat_Begin { get; set; }
        public List<string> Dat_End { get; set; }
        public List<string> Quant { get; set; }
        public List<string> SUM_OPT { get; set; }
        public List<string> SUM_OPT_NDS { get; set; }
        public List<string> SUM_NDS { get; set; }
        public List<string> SUM_ROZN { get; set; }
        public List<string> SUM_REAL { get; set; }
        public List<string> SUM_DISCOUNT { get; set; }

        private int IndexOf_ID_UL = -1;
        private int IndexOf_ID_Point = -1;
        private int IndexOf_D_TYPE = -1;
        private int IndexOf_Dat_Begin = -1;
        private int IndexOf_Dat_End = -1;
        private int IndexOf_Quant = -1;
        private int IndexOf_SUM_OPT = -1;
        private int IndexOf_SUM_OPT_NDS = -1;
        private int IndexOf_SUM_NDS = -1;
        private int IndexOf_SUM_ROZN = -1;
        private int IndexOf_SUM_REAL = -1;
        private int IndexOf_SUM_DISCOUNT = -1;

        private const string NameOf_ID_UL = "ID_UL";
        private const string NameOf_ID_Point = "ID_Point";
        private const string NameOf_D_TYPE = "D_TYPE";
        private const string NameOf_Dat_Begin = "Dat_Begin";
        private const string NameOf_Dat_End = "Dat_End";
        private const string NameOf_Quant = "Quant";
        private const string NameOf_SUM_OPT = "SUM_OPT";
        private const string NameOf_SUM_OPT_NDS = "SUM_OPT_NDS";
        private const string NameOf_SUM_NDS = "SUM_NDS";
        private const string NameOf_SUM_ROZN = "SUM_ROZN";
        private const string NameOf_SUM_REAL = "SUM_REAL";
        private const string NameOf_SUM_DISCOUNT = "SUM_DISCOUNT";

    }
}
