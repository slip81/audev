﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{    
    [DataContract]
    public class ExchangeDocTargetState : ExchangeBase
    {
        public ExchangeDocTargetState()
        {
            //
        }

        public ExchangeDocTargetState(exchange_doc_target_state item)
        {
            db_exchange_doc_target_state = item;
            item_id = item.item_id;
            target_id = item.target_id;
            doc_state = item.doc_state;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
        }

        [DataMember]
        public long item_id { get; set; }
        [DataMember]
        public long target_id { get; set; }
        [DataMember]
        public int doc_state { get; set; }
        [DataMember]
        public Nullable<System.DateTime> crt_date { get; set; }
        [DataMember]
        public string crt_user { get; set; }

        public exchange_doc_target_state Db_exchange_doc_target_state { get { return db_exchange_doc_target_state; } }
        private exchange_doc_target_state db_exchange_doc_target_state;


    }

    [DataContract]
    public class ExchangeDocTargetStateList : ExchangeBase
    {
        public ExchangeDocTargetStateList()
            : base()
        {
            //
        }

        public ExchangeDocTargetStateList(List<exchange_doc_target_state> exchangeDocTargetStateList)
        {
            if (exchangeDocTargetStateList != null)
            {
                ExchangeDocTargetState_list = new List<ExchangeDocTargetState>();
                foreach (var item in exchangeDocTargetStateList)
                {
                    ExchangeDocTargetState_list.Add(new ExchangeDocTargetState(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeDocTargetState> ExchangeDocTargetState_list;
    }
}
