﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeTask : ExchangeBase
    {
        public ExchangeTask(exchange_reg_task item)
            : base()
        {
            task_id = item.task_id;
            reg_id = item.reg_id;
            task_type_id = item.task_type_id;
            param_date_beg = item.param_date_beg;
            param_date_end = item.param_date_end;
            state = item.state;
            state_date = item.state_date;
            comment = item.comment;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            ds_id = item.ds_id;
        }

        [DataMember]
        public long task_id { get; set; }
        [DataMember]
        public long reg_id { get; set; }
        [DataMember]
        public int task_type_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> param_date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> param_date_end { get; set; }
        [DataMember]
        public int state { get; set; }
        [DataMember]
        public Nullable<System.DateTime> state_date { get; set; }
        [DataMember]
        public string comment { get; set; }
        [DataMember]
        public Nullable<System.DateTime> crt_date { get; set; }
        [DataMember]
        public string crt_user { get; set; }
        [DataMember]
        public long? ds_id { get; set; }
    }

    [DataContract]
    public class ExchangeTaskList : ExchangeBase
    {
        public ExchangeTaskList()
            : base()
        {
            ExchangeTask_list = new List<ExchangeTask>();
        }

        public ExchangeTaskList(List<exchange_reg_task> exchangeTaskList)
        {
            if (exchangeTaskList != null)
            {
                ExchangeTask_list = new List<ExchangeTask>();
                foreach (var item in exchangeTaskList)
                {
                    ExchangeTask_list.Add(new ExchangeTask(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeTask> ExchangeTask_list;
    }

}
