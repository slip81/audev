﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ExchangeSvc
{
    [DataContract]
    public class EmailAttachment : ExchangeBase
    {
        public EmailAttachment()
            : base()
        {
            //
        }

        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public string FileFormat { get; set; }
        [DataMember]
        public byte[] ContentAsBytes { get; set; }
        [DataMember]
        public string ContentAsString { get; set; }
        [DataMember]
        public string Address { get; set; }
    }

    [DataContract]
    public class EmailAttachmentList : ExchangeBase
    {
        public EmailAttachmentList()
            : base()
        {
            EmailAttachment_list = new List<EmailAttachment>();
        }

        [DataMember]
        public List<EmailAttachment> EmailAttachment_list;
    }

}
