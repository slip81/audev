﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{    
    [DataContract]
    public class Exchange : ExchangeBase
    {
        public Exchange()
        {
            //
        }

        public Exchange(exchange item)
        {
            db_exchange = item;
            exchange_id = item.exchange_id;
            ds_id = item.ds_id;
            is_upload = item.is_upload;
            user_id = item.user_id;
            crt_date = item.crt_date;
            exchange_type = item.exchange_type;
            sales_prefix = item.sales_prefix;
        }

        [DataMember]
        public long exchange_id { get; set; }
        [DataMember]
        public Nullable<long> ds_id { get; set; }
        [DataMember]
        public bool is_upload { get; set; }
        [DataMember]
        public long user_id { get; set; }
        [DataMember]
        public Nullable<System.DateTime> crt_date { get; set; }
        [DataMember]
        public Nullable<int> exchange_type { get; set; }
        [DataMember]
        public Nullable<long> sales_prefix { get; set; }

        public exchange Db_exchange { get { return db_exchange; } }
        private exchange db_exchange;


    }

    [DataContract]
    public class ExchangeList : ExchangeBase
    {
        public ExchangeList()
            : base()
        {
            //
        }

        public ExchangeList(List<exchange> exchangeList)
        {
            if (exchangeList != null)
            {
                Exchange_list = new List<Exchange>();
                foreach (var item in exchangeList)
                {
                    Exchange_list.Add(new Exchange(item));
                }
            }
        }

        [DataMember]
        public List<Exchange> Exchange_list;
    }
}
