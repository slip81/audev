﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeUserReg : ExchangeBase
    {
        public ExchangeUserReg()
        {
            //
        }

        public ExchangeUserReg(exchange_user_reg item)
        {
            db_exchange_user_reg = item;
            item_id = item.item_id;
            user_id = item.user_id;
            reg_id = item.reg_id;
            is_active = item.is_active;
        }

        [DataMember]
        public long item_id { get; set; }
        [DataMember]
        public long user_id { get; set; }
        [DataMember]
        public long reg_id { get; set; }
        [DataMember]
        public bool is_active { get; set; }

        public exchange_user_reg Db_exchange_user_reg { get { return db_exchange_user_reg; } }
        private exchange_user_reg db_exchange_user_reg;

    }

    [DataContract]
    public class ExchangeUserRegList : ExchangeBase
    {
        public ExchangeUserRegList()
            : base()
        {
            //
        }

        public ExchangeUserRegList(List<exchange_user_reg> exchangeUserRegList)
        {
            if (exchangeUserRegList != null)
            {
                ExchangeUserReg_list = new List<ExchangeUserReg>();
                foreach (var item in exchangeUserRegList)
                {
                    ExchangeUserReg_list.Add(new ExchangeUserReg(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeUserReg> ExchangeUserReg_list;
    }
}
