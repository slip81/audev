﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{    
    [DataContract]
    public class ExchangeDoc : ExchangeBase
    {
        public ExchangeDoc()
        {
            //
        }

        public ExchangeDoc(exchange_doc item)
        {
            db_exchange_doc = item;
            item_id = item.item_id;
            user_id = item.user_id;
            sales_id = item.sales_id;
            workplace_id = item.workplace_id;
            doc_id = item.doc_id;
            doc_name = item.doc_name;
            doc_format = item.doc_format;
            doc_comment = item.doc_comment;
            crt_date = item.crt_date;
            crt_user = item.crt_user;
            act_date = item.act_date;
            act_user = item.act_user;         
            terminate_date = item.terminate_date;
            is_terminated = item.is_terminated;
            terminated_date = item.terminated_date;
            date_beg = item.date_beg;
            date_end = item.date_end;
        }

        [DataMember]
        public long item_id { get; set; }
        [DataMember]
        public long user_id { get; set; }
        [DataMember]
        public Nullable<int> sales_id { get; set; }
        [DataMember]
        public Nullable<int> workplace_id { get; set; }
        [DataMember]
        public Nullable<int> doc_id { get; set; }
        [DataMember]
        public string doc_name { get; set; }
        [DataMember]
        public string doc_format { get; set; }
        [DataMember]
        public string doc_comment { get; set; }
        [DataMember]
        public Nullable<System.DateTime> crt_date { get; set; }
        [DataMember]
        public string crt_user { get; set; }
        [DataMember]
        public Nullable<System.DateTime> act_date { get; set; }
        [DataMember]
        public string act_user { get; set; }
        [DataMember]
        public Nullable<System.DateTime> terminate_date { get; set; }
        [DataMember]
        public bool is_terminated { get; set; }
        [DataMember]
        public Nullable<System.DateTime> terminated_date { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_beg { get; set; }
        [DataMember]
        public Nullable<System.DateTime> date_end { get; set; }

        public exchange_doc Db_exchange_doc { get { return db_exchange_doc; } }
        private exchange_doc db_exchange_doc;

    }

    [DataContract]
    public class ExchangeDocList : ExchangeBase
    {
        public ExchangeDocList()
            : base()
        {
            //
        }

        public ExchangeDocList(List<exchange_doc> exchangeDocList)
        {
            if (exchangeDocList != null)
            {
                ExchangeDoc_list = new List<ExchangeDoc>();
                foreach (var item in exchangeDocList)
                {
                    ExchangeDoc_list.Add(new ExchangeDoc(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeDoc> ExchangeDoc_list;
    }
}
