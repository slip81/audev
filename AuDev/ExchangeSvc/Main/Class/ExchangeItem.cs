﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeItem : ExchangeBase
    {
        public ExchangeItem()
        {
            //
        }

        public ExchangeItem(datasource_exchange_item item)
        {
            db_exchange_item = item;
            item_id = item.item_id;
            for_send = item.for_send;
            for_get = item.for_get;
            item_name = item.exchange_item.item_name;
            item_shortname = item.exchange_item.item_shortname;
            item_type_id = item.item_type_id;
            item_type_name = item.exchange_item_type != null ? item.exchange_item_type.type_name : null;
            item_file_name = item.item_file_name;
            sql_text = item.sql_text;
            param = item.param;
            method_id = 1;
        }

        [DataMember]
        public int item_id { get; set; }
        [DataMember]
        public bool for_send { get; set; }
        [DataMember]
        public bool for_get { get; set; }
        [DataMember]
        public string item_name { get; set; }
        [DataMember]
        public string item_shortname { get; set; }
        [DataMember]
        public int? item_type_id { get; set; }
        [DataMember]
        public string item_type_name { get; set; }
        [DataMember]
        public string item_file_name { get; set; }
        [DataMember]
        public string sql_text { get; set; }
        [DataMember]
        public string param { get; set; }
        [DataMember]
        public int? method_id { get; set; }

        public datasource_exchange_item Db_exchange_item { get { return db_exchange_item; } }
        private datasource_exchange_item db_exchange_item;

    }

    [DataContract]
    public class ExchangeItemList : ExchangeBase
    {
        public ExchangeItemList()
            : base()
        {
            //
        }

        public ExchangeItemList(List<datasource_exchange_item> exchangeItemList)
        {
            if (exchangeItemList != null)
            {
                ExchangeItem_list = new List<ExchangeItem>();
                foreach (var item in exchangeItemList)
                {
                    ExchangeItem_list.Add(new ExchangeItem(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeItem> ExchangeItem_list;
    }
}
