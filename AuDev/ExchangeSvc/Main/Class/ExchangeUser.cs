﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeUser : ExchangeBase
    {
        public ExchangeUser()
        {
            //
        }

        public ExchangeUser(vw_exchange_user item)
        {
            db_exchange_user = item;
            user_id = item.user_id;
            login = item.login;
            workplace = item.workplace;
            //version_num = item.version_num;
            cient_id = item.client_id;
        }

        [DataMember]
        public long user_id { get; set; }
        [DataMember]
        public string login { get; set; }
        [DataMember]
        public string workplace { get; set; }
        //[DataMember]
        //public string version_num { get; set; }
        [DataMember]
        public Nullable<long> cient_id { get; set; }

        public vw_exchange_user Db_exchange_user { get { return db_exchange_user; } }
        private vw_exchange_user db_exchange_user;

    }

    [DataContract]
    public class ExchangeUserList : ExchangeBase
    {
        public ExchangeUserList()
            : base()
        {
            //
        }

        public ExchangeUserList(List<vw_exchange_user> exchangeUserList)
        {
            if (exchangeUserList != null)
            {
                ExchangeUser_list = new List<ExchangeUser>();
                foreach (var item in exchangeUserList)
                {
                    ExchangeUser_list.Add(new ExchangeUser(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeUser> ExchangeUser_list;
    }

}
