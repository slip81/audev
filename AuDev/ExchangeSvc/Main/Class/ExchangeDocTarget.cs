﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{    
    [DataContract]
    public class ExchangeDocTarget : ExchangeBase
    {
        public ExchangeDocTarget()
        {
            //
        }

        public ExchangeDocTarget(exchange_doc_target item)
        {
            db_exchange_doc_target = item;
            item_id = item.item_id;
            exchange_doc_id = item.exchange_doc_id;
            target_sales_id = item.target_sales_id;
            target_workplace_id = item.target_workplace_id;
            curr_state_id = item.curr_state_id;
            sent_to_email = item.sent_to_email;
        }

        [DataMember]
        public long item_id { get; set; }
        [DataMember]
        public long exchange_doc_id { get; set; }
        [DataMember]
        public Nullable<int> target_sales_id { get; set; }
        [DataMember]
        public Nullable<int> target_workplace_id { get; set; }
        [DataMember]
        public int curr_state_id { get; set; }
        [DataMember]
        public string sent_to_email { get; set; }

        public exchange_doc_target Db_exchange_doc_target { get { return db_exchange_doc_target; } }
        private exchange_doc_target db_exchange_doc_target;


    }

    [DataContract]
    public class ExchangeDocTargetList : ExchangeBase
    {
        public ExchangeDocTargetList()
            : base()
        {
            //
        }

        public ExchangeDocTargetList(List<exchange_doc_target> exchangeDocTargetList)
        {
            if (exchangeDocTargetList != null)
            {
                ExchangeDocTarget_list = new List<ExchangeDocTarget>();
                foreach (var item in exchangeDocTargetList)
                {
                    ExchangeDocTarget_list.Add(new ExchangeDocTarget(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeDocTarget> ExchangeDocTarget_list;
    }
}
