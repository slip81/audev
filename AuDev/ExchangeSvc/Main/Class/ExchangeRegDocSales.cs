﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeRegDocSales : ExchangeBase
    {
        public ExchangeRegDocSales()
        {
            //
        }

        public ExchangeRegDocSales(exchange_reg_doc_sales item)
        {
            db_exchange_reg_doc_sales = item;
            item_id = item.item_id;
            reg_id = item.reg_id;
            sales_id = item.sales_id;
            sales_name = item.sales_name;
            is_active = item.is_active;
            email_for_docs = item.email_for_docs;
            login_name = item.login_name;
        }

        [DataMember]
        public long item_id { get; set; }
        [DataMember]
        public long reg_id { get; set; }
        [DataMember]
        public Nullable<int> sales_id { get; set; }
        [DataMember]
        public string sales_name { get; set; }
        [DataMember]
        public bool is_active { get; set; }
        [DataMember]
        public string email_for_docs { get; set; }
        [DataMember]
        public string login_name { get; set; }

        public exchange_reg_doc_sales Db_exchange_reg_doc_sales { get { return db_exchange_reg_doc_sales; } }
        private exchange_reg_doc_sales db_exchange_reg_doc_sales;

    }

    [DataContract]
    public class ExchangeRegDocSalesList : ExchangeBase
    {
        public ExchangeRegDocSalesList()
            : base()
        {
            //
        }

        public ExchangeRegDocSalesList(List<exchange_reg_doc_sales> exchangeRegDocSalesList)
        {
            if (exchangeRegDocSalesList != null)
            {
                ExchangeRegDocSales_list = new List<ExchangeRegDocSales>();
                foreach (var item in exchangeRegDocSalesList)
                {
                    ExchangeRegDocSales_list.Add(new ExchangeRegDocSales(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeRegDocSales> ExchangeRegDocSales_list;
    }
}
