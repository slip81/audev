﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace ExchangeSvc
{
    public class OstData
    {

        public OstData(string source_data, string prefix)
        {
            StringData = source_data;
            WithHeaders = true;
            CodePrefix = prefix;
            ConvertFromString();            
        }

        private void ConvertFromString()
        {
            //
            int line_num = 1;
            int item_index = -1;
            
            StringData = StringData.Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "\r\n");
            foreach (var line in StringData.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            //foreach (var line in StringData.Split(new string[] { @"\n", @"\r\n" }, StringSplitOptions.None))
            {
                item_index = -1;
                foreach (var item in line.Split(new string[] { ";" }, StringSplitOptions.None))
                {
                    item_index++;
                    
                    if ((line_num == 1) && (WithHeaders))
                    {
                        #region ищем заголовки в файле
                        if (item.Equals(NameOf_D_TYPE))
                        {
                            IndexOf_D_TYPE = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_N_DOK))
                        {
                            IndexOf_N_DOK = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_D_DOK))
                        {
                            IndexOf_D_DOK = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Supplier))
                        {
                            IndexOf_Supplier = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Supplier_INN))
                        {
                            IndexOf_Supplier_INN = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_N_KKM))
                        {
                            IndexOf_N_KKM = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_N_Chek))
                        {
                            IndexOf_N_Chek = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_FIO_Chek))
                        {
                            IndexOf_FIO_Chek = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Disk_T))
                        {
                            IndexOf_Disk_T = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Disk_Sum))
                        {
                            IndexOf_Disk_Sum = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Sum_Zak))
                        {
                            IndexOf_Sum_Zak = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Sum_Rozn))
                        {
                            IndexOf_Sum_Rozn = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_PP_Teg))
                        {
                            IndexOf_PP_Teg = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Drug_Code))
                        {
                            IndexOf_Drug_Code = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Drug_Name))
                        {
                            IndexOf_Drug_Name = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Drug_Producer_Code))
                        {
                            IndexOf_Drug_Producer_Code = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Drug_Producer_Name))
                        {
                            IndexOf_Drug_Producer_Name = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Drug_Producer_Country))
                        {
                            IndexOf_Drug_Producer_Country = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Drug_Bar))
                        {
                            IndexOf_Drug_Bar = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Cena_Zak))
                        {
                            IndexOf_Cena_Zak = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Cena_Rozn))
                        {
                            IndexOf_Cena_Rozn = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Quant))
                        {
                            IndexOf_Quant = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Serial))
                        {
                            IndexOf_Serial = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Godn))
                        {
                            IndexOf_Godn = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_Barecode))
                        {
                            IndexOf_Barecode = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_NDS))
                        {
                            IndexOf_SUM_NDS = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_OPT))
                        {
                            IndexOf_SUM_OPT = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_SUM_REAL_DISCOUNT))
                        {
                            IndexOf_SUM_REAL_DISCOUNT = item_index;
                            continue;
                        }
                        else if (item.Equals(NameOf_RegionIDN))
                        {
                            IndexOf_RegionIDN = item_index;
                            continue;
                        }
                        #endregion
                    }
                    else
                    {
                        #region считываем данные
                        if (item_index == IndexOf_D_TYPE)
                        {
                            if (D_TYPE == null)
                                D_TYPE = new List<string>();
                            D_TYPE.Add(item);
                        }
                        else if (item_index == IndexOf_N_DOK)
                        {
                            if (N_DOK == null)
                                N_DOK = new List<string>();
                            N_DOK.Add(item);
                        }
                        else if (item_index == IndexOf_D_DOK)
                        {
                            if (D_DOK == null)
                                D_DOK = new List<string>();
                            D_DOK.Add(item);
                        }
                        else if (item_index == IndexOf_Supplier)
                        {
                            if (Supplier == null)
                                Supplier = new List<string>();
                            Supplier.Add(item);
                        }
                        else if (item_index == IndexOf_Supplier_INN)
                        {
                            if (Supplier_INN == null)
                                Supplier_INN = new List<string>();
                            Supplier_INN.Add(item);
                        }
                        else if (item_index == IndexOf_N_KKM)
                        {
                            if (N_KKM == null)
                                N_KKM = new List<string>();
                            N_KKM.Add(item);
                        }
                        else if (item_index == IndexOf_N_Chek)
                        {
                            if (N_Chek == null)
                                N_Chek = new List<string>();
                            N_Chek.Add(item);
                        }
                        else if (item_index == IndexOf_FIO_Chek)
                        {
                            if (FIO_Chek == null)
                                FIO_Chek = new List<string>();
                            FIO_Chek.Add(item);
                        }
                        else if (item_index == IndexOf_Disk_T)
                        {
                            if (Disk_T == null)
                                Disk_T = new List<string>();
                            Disk_T.Add(item);
                        }
                        else if (item_index == IndexOf_Disk_Sum)
                        {
                            if (Disk_Sum == null)
                                Disk_Sum = new List<string>();
                            Disk_Sum.Add(item);
                        }
                        else if (item_index == IndexOf_Sum_Zak)
                        {
                            if (Sum_Zak == null)
                                Sum_Zak = new List<string>();
                            Sum_Zak.Add(item);
                        }
                        else if (item_index == IndexOf_Sum_Rozn)
                        {
                            if (Sum_Rozn == null)
                                Sum_Rozn = new List<string>();
                            Sum_Rozn.Add(item);
                        }
                        else if (item_index == IndexOf_PP_Teg)
                        {
                            if (PP_Teg == null)
                                PP_Teg = new List<string>();
                            PP_Teg.Add(item);
                        }
                        else if (item_index == IndexOf_Drug_Code)
                        {
                            if (Drug_Code == null)
                                Drug_Code = new List<string>();
                            Drug_Code.Add(CodePrefix + item);
                        }
                        else if (item_index == IndexOf_Drug_Name)
                        {
                            if (Drug_Name == null)
                                Drug_Name = new List<string>();
                            Drug_Name.Add(item);
                        }
                        else if (item_index == IndexOf_Drug_Producer_Code)
                        {
                            if (Drug_Producer_Code == null)
                                Drug_Producer_Code = new List<string>();
                            Drug_Producer_Code.Add(item);
                        }
                        else if (item_index == IndexOf_Drug_Producer_Name)
                        {
                            if (Drug_Producer_Name == null)
                                Drug_Producer_Name = new List<string>();
                            Drug_Producer_Name.Add(item);
                        }
                        else if (item_index == IndexOf_Drug_Producer_Country)
                        {
                            if (Drug_Producer_Country == null)
                                Drug_Producer_Country = new List<string>();
                            Drug_Producer_Country.Add(item);
                        }
                        else if (item_index == IndexOf_Drug_Bar)
                        {
                            if (Drug_Bar == null)
                                Drug_Bar = new List<string>();
                            Drug_Bar.Add(item);
                        }
                        else if (item_index == IndexOf_Cena_Zak)
                        {
                            if (Cena_Zak == null)
                                Cena_Zak = new List<string>();
                            Cena_Zak.Add(item);
                        }
                        else if (item_index == IndexOf_Cena_Rozn)
                        {
                            if (Cena_Rozn == null)
                                Cena_Rozn = new List<string>();
                            Cena_Rozn.Add(item);
                        }
                        else if (item_index == IndexOf_Quant)
                        {
                            if (Quant == null)
                                Quant = new List<string>();
                            Quant.Add(item);
                        }
                        else if (item_index == IndexOf_Serial)
                        {
                            if (Serial == null)
                                Serial = new List<string>();
                            Serial.Add(item);
                        }
                        else if (item_index == IndexOf_Godn)
                        {
                            if (Godn == null)
                                Godn = new List<string>();
                            Godn.Add(item);
                        }
                        else if (item_index == IndexOf_Barecode)
                        {
                            if (Barecode == null)
                                Barecode = new List<string>();
                            Barecode.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_NDS)
                        {
                            if (SUM_NDS == null)
                                SUM_NDS = new List<string>();
                            SUM_NDS.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_OPT)
                        {
                            if (SUM_OPT == null)
                                SUM_OPT = new List<string>();
                            SUM_OPT.Add(item);
                        }
                        else if (item_index == IndexOf_SUM_REAL_DISCOUNT)
                        {
                            if (SUM_REAL_DISCOUNT == null)
                                SUM_REAL_DISCOUNT = new List<string>();
                            SUM_REAL_DISCOUNT.Add(item);
                        }
                        else if (item_index == IndexOf_RegionIDN)
                        {
                            if (RegionIDN == null)
                                RegionIDN = new List<string>();
                            RegionIDN.Add(item);
                        }
                        #endregion
                    }

                }
                line_num++;
            }
            RowCount = WithHeaders ? (line_num - 2) : (line_num - 1);
        }

        public string ConvertToFormat(Enums.DataSourceEnum format_type)
        {            
            switch (format_type)
            {
                case Enums.DataSourceEnum.ASNA:
                    return ConvertToFormat_ASNA();
                default:
                    return StringData;
            }            
        }

        protected virtual string ConvertToFormat_ASNA()
        {
            string res = "";
            int i = 1;
            string items_separator = "|";
            string symbol_for_null = "";
            string symbol_for_zero = "0";
            
            while (i <= RowCount)
            {
                DateTime? date_doc = !D_DOK.IsEmpty(i - 1) ? (DateTime?)(DateTime.ParseExact(D_DOK[i - 1], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture)) : null;
                string date_doc_str = date_doc != null ? ((DateTime)date_doc).ToString("yyyy-MM-dd HH:mm:ss") : symbol_for_null;

                res += ClientName.DoubleQouted() + items_separator  //Юридическое лицо (Учетная система)
                    + RegName.DoubleQouted() + items_separator  //Торговый объект
                    + (!Supplier_INN.IsEmpty(i - 1) ? Supplier_INN[i - 1] : symbol_for_null).DoubleQouted() + items_separator   //Поставщик товара
                    + (!RegionIDN.IsEmpty(i - 1) ? RegionIDN[i - 1] : symbol_for_zero) + items_separator    //Код региона поставщика товара
                    + date_doc_str + items_separator    //Дата
                    + date_doc_str + items_separator    //Дата поставки
                    + (!Drug_Code.IsEmpty(i - 1) ? Drug_Code[i - 1] : symbol_for_null).DoubleQouted() + items_separator //Товар
                    + (!Quant.IsEmpty(i - 1) ? Quant[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Количество товара
                    + (!SUM_OPT.IsEmpty(i - 1) ? SUM_OPT[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма оптовая без НДС
                    + (!Sum_Zak.IsEmpty(i - 1) ? Sum_Zak[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма оптовая с НДС
                    + (!SUM_NDS.IsEmpty(i - 1) ? SUM_NDS[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма НДС
                    + (!Sum_Rozn.IsEmpty(i - 1) ? Sum_Rozn[i - 1].Replace(',', '.') : symbol_for_zero)  //Сумма розничная
                    ;
                res += System.Environment.NewLine;

                /*
                DateTime? date_doc = D_DOK != null ? (DateTime?)(DateTime.ParseExact(D_DOK[i - 1], "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture)) : null;
                string date_doc_str = date_doc != null ? ((DateTime)date_doc).ToString("yyyy-MM-dd HH:mm:ss") : symbol_for_null;

                res += ClientName.DoubleQouted() + items_separator  //Юридическое лицо (Учетная система)
                    + RegName.DoubleQouted() + items_separator  //Торговый объект
                    + (Supplier_INN != null ? Supplier_INN[i - 1] : symbol_for_null).DoubleQouted() + items_separator   //Поставщик товара
                    + (RegionIDN != null ? RegionIDN[i - 1] : symbol_for_zero) + items_separator    //Код региона поставщика товара
                    + date_doc_str + items_separator    //Дата
                    + date_doc_str + items_separator    //Дата поставки
                    + (Drug_Code != null ? Drug_Code[i - 1] : symbol_for_null).DoubleQouted() + items_separator //Товар
                    + (Quant != null ? Quant[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Количество товара
                    + (SUM_OPT != null ? SUM_OPT[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма оптовая без НДС
                    + (Sum_Zak != null ? Sum_Zak[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма оптовая с НДС
                    + (SUM_NDS != null ? SUM_NDS[i - 1].Replace(',', '.') : symbol_for_zero) + items_separator  //Сумма НДС
                    + (Sum_Rozn != null ? Sum_Rozn[i - 1].Replace(',', '.') : symbol_for_zero)  //Сумма розничная
                    ;
                res += System.Environment.NewLine;
                */

                i++;
            }            
            return res;
        }

        public string StringData { get; set; }
        public bool WithHeaders { get; set; }
        public int RowCount { get; set; }
        public string ClientName { get; set; }
        public string RegName { get; set; }
        public string CodePrefix { get; set; }

        public List<string> D_TYPE { get; set; }
        public List<string> N_DOK { get; set; }
        public List<string> D_DOK { get; set; }
        public List<string> Supplier { get; set; }
        public List<string> Supplier_INN { get; set; }
        public List<string> N_KKM { get; set; }
        public List<string> N_Chek { get; set; }
        public List<string> FIO_Chek { get; set; }
        public List<string> Disk_T { get; set; }
        public List<string> Disk_Sum { get; set; }
        public List<string> Sum_Zak { get; set; }
        public List<string> Sum_Rozn { get; set; }
        public List<string> PP_Teg { get; set; }
        public List<string> Drug_Code { get; set; }
        public List<string> Drug_Name { get; set; }
        public List<string> Drug_Producer_Code { get; set; }
        public List<string> Drug_Producer_Name { get; set; }
        public List<string> Drug_Producer_Country { get; set; }
        public List<string> Drug_Bar { get; set; }
        public List<string> Cena_Zak { get; set; }
        public List<string> Cena_Rozn { get; set; }
        public List<string> Quant { get; set; }
        public List<string> Serial { get; set; }
        public List<string> Godn { get; set; }
        public List<string> Barecode { get; set; }
        public List<string> SUM_OPT { get; set; }
        public List<string> SUM_NDS { get; set; }
        public List<string> SUM_REAL_DISCOUNT { get; set; }
        public List<string> RegionIDN { get; set; }

        private int IndexOf_D_TYPE = -1;
        private int IndexOf_N_DOK = -1;
        private int IndexOf_D_DOK = -1;
        private int IndexOf_Supplier = -1;
        private int IndexOf_Supplier_INN = -1;
        private int IndexOf_N_KKM = -1;
        private int IndexOf_N_Chek = -1;
        private int IndexOf_FIO_Chek = -1;
        private int IndexOf_Disk_T = -1;
        private int IndexOf_Disk_Sum = -1;
        private int IndexOf_Sum_Zak = -1;
        private int IndexOf_Sum_Rozn = -1;
        private int IndexOf_PP_Teg = -1;
        private int IndexOf_Drug_Code = -1;
        private int IndexOf_Drug_Name = -1;
        private int IndexOf_Drug_Producer_Code = -1;
        private int IndexOf_Drug_Producer_Name = -1;
        private int IndexOf_Drug_Producer_Country = -1;
        private int IndexOf_Drug_Bar = -1;
        private int IndexOf_Cena_Zak = -1;
        private int IndexOf_Cena_Rozn = -1;
        private int IndexOf_Quant = -1;
        private int IndexOf_Serial = -1;
        private int IndexOf_Godn = -1;
        private int IndexOf_Barecode = -1;
        private int IndexOf_SUM_OPT = -1;
        private int IndexOf_SUM_NDS = -1;
        private int IndexOf_SUM_REAL_DISCOUNT = -1;
        private int IndexOf_RegionIDN = -1;        

        private const string NameOf_D_TYPE = "D_TYPE";
        private const string NameOf_N_DOK = "N_DOK";
        private const string NameOf_D_DOK = "D_DOK";
        private const string NameOf_Supplier = "Supplier";
        private const string NameOf_Supplier_INN = "Supplier_INN";
        private const string NameOf_N_KKM = "N_KKM";
        private const string NameOf_N_Chek = "N_Chek";
        private const string NameOf_FIO_Chek = "FIO_Chek";
        private const string NameOf_Disk_T = "Disk_T";
        private const string NameOf_Disk_Sum = "Disk_Sum";
        private const string NameOf_Sum_Zak = "Sum_Zak";
        private const string NameOf_Sum_Rozn = "Sum_Rozn";
        private const string NameOf_PP_Teg = "PP_Teg";
        private const string NameOf_Drug_Code = "Drug_Code";
        private const string NameOf_Drug_Name = "Drug_Name";
        private const string NameOf_Drug_Producer_Code = "Drug_Producer_Code";
        private const string NameOf_Drug_Producer_Name = "Drug_Producer_Name";
        private const string NameOf_Drug_Producer_Country = "Drug_Producer_Country";
        private const string NameOf_Drug_Bar = "Drug_Bar";
        private const string NameOf_Cena_Zak = "Cena_Zak";
        private const string NameOf_Cena_Rozn = "Cena_Rozn";
        private const string NameOf_Quant = "Quant";
        private const string NameOf_Serial = "Serial";
        private const string NameOf_Godn = "Godn";
        private const string NameOf_Barecode = "Barecode";
        private const string NameOf_SUM_OPT = "SUM_OPT";
        private const string NameOf_SUM_NDS = "SUM_NDS";
        private const string NameOf_SUM_REAL_DISCOUNT = "SUM_REAL_DISCOUNT";
        private const string NameOf_RegionIDN = "RegionIDN";
        
        
    }
}
