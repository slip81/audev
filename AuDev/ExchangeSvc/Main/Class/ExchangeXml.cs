﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Xml;
using System.Xml.Linq;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeXml : ExchangeBase
    {
        public ExchangeXml()
            : base()
        {
            Data = "";
        }

        public void AddData_Plan()
        {
            var guid = Guid.NewGuid();
            DateTime doc_date = DateTime.Now;

            XElement xml = new XElement("Документ",
                   new XAttribute("Идентификатор", guid.ToString()),
                   new XElement("ЗаголовокДокумента",
                       new XElement("ТипДок", "МРК"),
                       new XElement("НомерДок", "1111"),
                       new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                       new XElement("Поставщик", @"ООО Поставщик 1"),
                       new XElement("Получатель", @"ООО Получатель 1"),
                       new XElement("Позиций", "3"),
                       new XElement("СуммаОпт", "0.00"),
                       new XElement("СуммаНДС", "0.00"),
                       new XElement("СуммаОптВклНДС", "0.00")
                       ),
                   new XElement("ТоварныеПозиции",
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "1"),
                           new XElement("Товар", "БАНЕОЦИН 10Г. №1 ПОР. ФЛ./ДОЗ."),
                           new XElement("Изготовитель", "Сандоз Гмбх,МОНТАВИТ ГмбХ"),
                           new XElement("ЕАН13", "9002260001019,9002260012114,9002260021444"),
                           new XElement("МинКолНаОст", "2"),
                           new XElement("РекКолНаОст", "2"),
                           new XElement("МинКолНаОстДни", "10"),
                           new XElement("РекКолНаОстДни", "14")
                           ),
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "2"),
                           new XElement("Товар", "БАНЕОЦИН 20Г. МАЗЬ"),
                           new XElement("Изготовитель", "Merck KGaA,Сандоз Гмбх"),
                           new XElement("ЕАН13", "9002260002399,9002260012107"),
                           new XElement("МинКолНаОст", "2"),
                           new XElement("РекКолНаОст", "2"),
                           new XElement("МинКолНаОстДни", "10"),
                           new XElement("РекКолНаОстДни", "14")
                           ),
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "3"),
                           new XElement("Товар", "ОТИПАКС 15МЛ. КАПЛИ"),
                           new XElement("Изготовитель", "Laboratoires BIOCODEX"),
                           new XElement("ЕАН13", "3583313279068"),
                           new XElement("МинКолНаОст", "1"),
                           new XElement("РекКолНаОст", "1"),
                           new XElement("МинКолНаОстДни", "10"),
                           new XElement("РекКолНаОстДни", "14")
                           )
                       )
                );

            Data += xml;
        }

        public void AddData_Discount()
        {
            var guid = Guid.NewGuid();
            DateTime doc_date = DateTime.Now;

            XElement xml = new XElement("Документ",
                   new XAttribute("Идентификатор", guid.ToString()),
                   new XElement("ЗаголовокДокумента",
                       new XElement("ТипДок", "МРК"),
                       new XElement("НомерДок", "1111"),
                       new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                       new XElement("Поставщик", @"ООО Поставщик 1"),
                       new XElement("Получатель", @"ООО Получатель 1"),
                       new XElement("Позиций", "3"),
                       new XElement("СуммаОпт", "0.00"),
                       new XElement("СуммаНДС", "0.00"),
                       new XElement("СуммаОптВклНДС", "0.00")
                       ),
                   new XElement("ТоварныеПозиции",
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "112"),
                           new XElement("Товар", "АНТИ-АНГИН ФОРМУЛА №12 ПАСТИЛКИ"),
                           new XElement("НазвСкидки", "Активности октябрь 10% 2015"),
                           new XElement("ПроцСкидки", "10"),
                           new XElement("ДатаНачала", "01.10.2015"),
                           new XElement("ДатаОкончания", "31.10.2015")
                           ),
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "455"),
                           new XElement("Товар", "ДОКТОР МОМ 100МЛ. СИРОП"),
                           new XElement("НазвСкидки", "Активности октябрь 10% 2015"),
                           new XElement("ПроцСкидки", "10"),
                           new XElement("ДатаНачала", "01.10.2015"),
                           new XElement("ДатаОкончания", "31.10.2015")
                           ),
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "4637"),
                           new XElement("Товар", "БИФИФОРМ №30 КАПС."),
                           new XElement("НазвСкидки", "Активности октябрь 10% 2015"),
                           new XElement("ПроцСкидки", "10"),
                           new XElement("ДатаНачала", "01.10.2015"),
                           new XElement("ДатаОкончания", "31.10.2015")
                           )
                       )
                );

            Data += xml;
        }

        public void AddData_Recommend()
        {
            var guid = Guid.NewGuid();
            DateTime doc_date = DateTime.Now;

            XElement xml = new XElement("Документ",
                   new XAttribute("Идентификатор", guid.ToString()),
                   new XElement("ЗаголовокДокумента",
                       new XElement("ТипДок", "МРК"),
                       new XElement("НомерДок", "1111"),
                       new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                       new XElement("Поставщик", @"ООО Поставщик 1"),
                       new XElement("Получатель", @"ООО Получатель 1"),
                       new XElement("Позиций", "3"),
                       new XElement("СуммаОпт", "0.00"),
                       new XElement("СуммаНДС", "0.00"),
                       new XElement("СуммаОптВклНДС", "0.00")
                       ),
                   new XElement("ТоварныеПозиции",
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "2841"),
                           new XElement("Товар", "АМОКСИКЛАВ 250МГ.+125МГ. №15 ТАБ. /ЛЕК/"),
                           new XElement("НазвГруппы", "Приоритетная рекомендация"),
                           new XElement("ПервыйУровень", "Амоксицилин"),
                           new XElement("ВторойУровень", "Амоксиклав"),
                           new XElement("ТретийУровень", "Амоксиклав")
                           ),
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "4206"),
                           new XElement("Товар", "АМОКСИКЛАВ 125МГ.+31,25МГ/5МЛ. 25Г. 100МЛ. ПОР. Д/ПРИГ. СУСП. ФЛ. /ЛЕК/"),
                           new XElement("НазвГруппы", "Приоритетная рекомендация"),
                           new XElement("ПервыйУровень", "Амоксицилин"),
                           new XElement("ВторойУровень", "Амоксиклав"),
                           new XElement("ТретийУровень", "Амоксиклав")
                           ),
                       new XElement("ТоварнаяПозиция",
                           new XElement("КодТовара", "4208"),
                           new XElement("Товар", "АМОКСИКЛАВ 500МГ.+125МГ. №15 ТАБ. П/О /ЛЕК/"),
                           new XElement("НазвГруппы", "Приоритетная рекомендация"),
                           new XElement("ПервыйУровень", "Амоксицилин"),
                           new XElement("ВторойУровень", "Амоксиклав"),
                           new XElement("ТретийУровень", "Амоксиклав")
                           )
                       )
                );

            Data += xml;
        }

        public void AddDataCompleted()
        {
            var str1 = @"<?xml version=""1.0"" encoding=""windows-1251""?>";
            Data = (str1 + Data.Trim()).Trim();
        }

        [DataMember]
        public string Data { get; set; }
        [DataMember]
        public int RecordCount { get; set; }
        [DataMember]
        public int DataKind { get; set; }
    }
}
