﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeRegDoc : ExchangeBase
    {
        public ExchangeRegDoc()
        {
            //
        }

        public ExchangeRegDoc(exchange_reg_doc item)
        {
            db_exchange_reg_doc = item;
            reg_id = item.reg_id;
            days_to_keep_data = item.days_to_keep_data;
        }

        [DataMember]
        public long reg_id { get; set; }
        [DataMember]
        public Nullable<int> days_to_keep_data { get; set; }

        public exchange_reg_doc Db_exchange_reg_doc { get { return db_exchange_reg_doc; } }
        private exchange_reg_doc db_exchange_reg_doc;

    }

    [DataContract]
    public class ExchangeRegDocList : ExchangeBase
    {
        public ExchangeRegDocList()
            : base()
        {
            //
        }

        public ExchangeRegDocList(List<exchange_reg_doc> exchangeRegDocList)
        {
            if (exchangeRegDocList != null)
            {
                ExchangeRegDoc_list = new List<ExchangeRegDoc>();
                foreach (var item in exchangeRegDocList)
                {
                    ExchangeRegDoc_list.Add(new ExchangeRegDoc(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeRegDoc> ExchangeRegDoc_list;
    }
}
