﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeRegPartner : ExchangeBase
    {
        public ExchangeRegPartner()
        {
            //
        }

        public ExchangeRegPartner(exchange_reg_partner item)
        {
            db_exchange_reg_partner = item;
            reg_id = item.reg_id;
            ds_id = item.ds_id;
            login = item.login;
            password = item.password;
            transport_type = item.transport_type;
            format_type = item.format_type;
            upload_url = item.upload_url;
            download_url = item.download_url;
            crt_date = item.crt_date;
            reg_code = item.reg_code;
            reg_name = item.reg_name;
            send_all = item.send_all;
            use_sales_prefix = item.use_sales_prefix;
            upload_arc_type = item.upload_arc_type;
            download_arc_type = item.download_arc_type;
            pop3_address = item.pop3_address;
            pop3_port = item.pop3_port;
            pop3_use_ssl = item.pop3_use_ssl;
            upload_equals_download = item.upload_equals_download;
            upload_url_alias = item.upload_url_alias;
            days_cnt_for_mov = item.days_cnt_for_mov;
            upload_confirm_address = item.upload_confirm_address;
            is_prepared = item.is_prepared;
            upload_state =item.upload_state; 
            download_state = item.download_state;
            upload_err_mess = item.upload_err_mess;
            download_err_mess = item.download_err_mess;
            upload_ok_date = item.upload_ok_date;
            download_ok_date = item.download_ok_date;
            prepared_date = item.prepared_date;
            upload_last_date = item.upload_last_date;
            download_last_date = item.download_last_date;
            upload_last_mess = item.upload_last_mess;
            download_last_mess = item.download_last_mess;
            download_ok_cnt = item.download_ok_cnt;
            download_warn_cnt = item.download_warn_cnt;
            download_err_cnt = item.download_err_cnt;
            upload_ok_cnt = item.upload_ok_cnt;
            upload_warn_cnt = item.upload_warn_cnt;
            upload_err_cnt = item.upload_err_cnt;
        }

        [DataMember]
        public long reg_id { get; set; }
        [DataMember]
        public Nullable<long> ds_id { get; set; }
        [DataMember]
        public string login { get; set; }
        [DataMember]
        public string password { get; set; }
        [DataMember]
        public Nullable<int> transport_type { get; set; }
        [DataMember]
        public Nullable<int> format_type { get; set; }
        [DataMember]
        public string upload_url { get; set; }
        [DataMember]
        public string download_url { get; set; }
        [DataMember]
        public Nullable<System.DateTime> crt_date { get; set; }
        [DataMember]
        public string reg_code { get; set; }
        [DataMember]
        public string reg_name { get; set; }
        [DataMember]
        public int send_all { get; set; }
        [DataMember]
        public int use_sales_prefix { get; set; }
        [DataMember]
        public int upload_arc_type { get; set; }
        [DataMember]
        public int download_arc_type { get; set; }
        [DataMember]
        public string pop3_address { get; set; }
        [DataMember]
        public Nullable<int> pop3_port { get; set; }
        [DataMember]
        public bool pop3_use_ssl { get; set; }
        [DataMember]
        public bool upload_equals_download { get; set; }
        [DataMember]
        public string upload_url_alias { get; set; }
        [DataMember]
        public int days_cnt_for_mov { get; set; }
        [DataMember]
        public string upload_confirm_address { get; set; }
        [DataMember]
        public bool is_prepared { get; set; }
        [DataMember]
        public int upload_state { get; set; }
        [DataMember]
        public int download_state { get; set; }
        [DataMember]
        public string upload_err_mess { get; set; }
        [DataMember]
        public string download_err_mess { get; set; }
        [DataMember]
        public Nullable<System.DateTime> upload_ok_date { get; set; }
        [DataMember]
        public Nullable<System.DateTime> download_ok_date { get; set; }
        [DataMember]
        public Nullable<System.DateTime> prepared_date { get; set; }
        [DataMember]
        public Nullable<System.DateTime> upload_last_date { get; set; }
        [DataMember]
        public Nullable<System.DateTime> download_last_date { get; set; }
        [DataMember]
        public string upload_last_mess { get; set; }
        [DataMember]
        public string download_last_mess { get; set; }
        [DataMember]
        public int download_ok_cnt { get; set; }
        [DataMember]
        public int download_warn_cnt { get; set; }
        [DataMember]
        public int download_err_cnt { get; set; }
        [DataMember]
        public int upload_ok_cnt { get; set; }
        [DataMember]
        public int upload_warn_cnt { get; set; }
        [DataMember]
        public int upload_err_cnt { get; set; }

        public exchange_reg_partner Db_exchange_reg_partner { get { return db_exchange_reg_partner; } }
        private exchange_reg_partner db_exchange_reg_partner;

    }

    [DataContract]
    public class ExchangeRegPartnerList : ExchangeBase
    {
        public ExchangeRegPartnerList()
            : base()
        {
            //
        }

        public ExchangeRegPartnerList(List<exchange_reg_partner> exchangeRegPartnerList)
        {
            if (exchangeRegPartnerList != null)
            {
                ExchangeRegPartner_list = new List<ExchangeRegPartner>();
                foreach (var item in exchangeRegPartnerList)
                {
                    ExchangeRegPartner_list.Add(new ExchangeRegPartner(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeRegPartner> ExchangeRegPartner_list;
    }
}
