﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using AuDev.Common.Db.Model;

namespace ExchangeSvc
{
    [DataContract]
    public class ExchangeData : ExchangeBase
    {
        public ExchangeData()
        {
            //
        }

        public ExchangeData(exchange_data item)
        {
            db_exchange_data = item;
            exchange_id = item.exchange_id;
            data_text = item.data_text;
            data_binary = item.data_binary;
        }

        [DataMember]
        public long exchange_id { get; set; }
        [DataMember]
        public string data_text { get; set; }
        [DataMember]
        public byte[] data_binary { get; set; }


        public exchange_data Db_exchange_data { get { return db_exchange_data; } }
        private exchange_data db_exchange_data;

    }

    [DataContract]
    public class ExchangeDataList : ExchangeBase
    {
        public ExchangeDataList()
            : base()
        {
            //
        }

        public ExchangeDataList(List<exchange_data> exchangeDataList)
        {
            if (exchangeDataList != null)
            {
                ExchangeData_list = new List<ExchangeData>();
                foreach (var item in exchangeDataList)
                {
                    ExchangeData_list.Add(new ExchangeData(item));
                }
            }
        }

        [DataMember]
        public List<ExchangeData> ExchangeData_list;
    }
}
