﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Channels;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace ExchangeSvc
{
    public static class Loggly
    {

        // !!!
        private static bool isBanLog = false;


        public static bool InsertLog_ExchangePartner(string mess, long? type_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            string ip = " [IP: ";            
            try
            {
                OperationContext context = OperationContext.Current;
                MessageProperties prop = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                ip += endpoint.Address + "]";
            }
            catch
            {
                ip += "error]";
            }
            

            // отсылаем в БД
            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess + ip;
                log_esn.log_esn_type_id = type_id;
                log_esn.user_name = String.IsNullOrEmpty(user_name) ? "exchanger" : user_name.ToLower();
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.EXCHANGE_PARTNER;
                log_esn.date_end = DateTime.Now;
                log_esn.mess2 = mess2;
                dbContext.log_esn.Add(log_esn);            
                dbContext.SaveChanges();
            }

            return true;
        }

        public static bool InsertExchangeLog(string mess, long? reg_id, string user_name, DateTime date_beg
            , int direction, int exchange_item_id, long? ds_id, long? exchange_id, int error_level, string mess2)
        {
            if (isBanLog)
                return true;

            using (var dbContext = new AuMainDb())
            {
                exchange_log exchange_log = new exchange_log();
                exchange_log.date_beg = date_beg;
                exchange_log.date_end = DateTime.Now;
                exchange_log.ds_id = ds_id;
                exchange_log.exchange_direction = direction;
                exchange_log.exchange_item_id = exchange_item_id;
                exchange_log.exchange_id = exchange_id;
                exchange_log.error_level = error_level;
                exchange_log.login = String.IsNullOrEmpty(user_name) ? "exchanger" : user_name.ToLower();
                exchange_log.mess = mess;
                exchange_log.mess2 = mess2;
                exchange_log.reg_id = reg_id;
                dbContext.exchange_log.Add(exchange_log);
                dbContext.SaveChanges();
            }
            return true;            
        }

        public static bool InsertLog_ExchangeDocument(string mess, long? type_id, string user_name, long? obj_id, DateTime date_beg, string mess2)
        {
            if (isBanLog)
                return true;

            // отсылаем в БД
            using (var dbContext = new AuMainDb())
            {
                log_esn log_esn = new log_esn();
                log_esn.date_beg = date_beg;
                log_esn.mess = mess;
                log_esn.log_esn_type_id = type_id;
                log_esn.user_name = String.IsNullOrEmpty(user_name) ? "exchanger" : user_name.ToLower();
                log_esn.obj_id = obj_id;
                log_esn.scope = (int)Enums.LogScope.EXCHANGE_DOC;
                log_esn.date_end = DateTime.Now;
                log_esn.mess2 = mess2;
                dbContext.log_esn.Add(log_esn);
                dbContext.SaveChanges();
            }

            return true;
        }
    }
}
