﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using MySql.Data;
using MySql.Data.Common;
using MySql.Data.MySqlClient;
using AuDev.Common.Util;

namespace ExchangeSvc
{
    public partial class ExchangeService : IExchangeService
    {
        #region GetSalesList

        private SalesList GetSalesList_byClientId(long client_id, long? exclude_sales_id)
        {
            if (client_id <= 0)
                return new SalesList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не указан код клиента") };

            SalesList res = new SalesList() { Sales_list = new List<Sales>(), };
            
            string sql;
            string mySqlConnectionString;
            int? cnt = 0;

            MySqlConnection conn = null;
            MySqlCommand cmd;
            MySqlDataReader rdr;
            mySqlConnectionString = ExchangeUtil.GetCabinetDbConnectionString();
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = mySqlConnectionString;
                conn.Open();

                sql = "select count(*) as cnt, t1.id, t1.name"
                    + " from clients.sales t1"
                    + " where t1.client_id = " + client_id.ToString()
                    ;
                
                if (exclude_sales_id.HasValue)
                    sql += " and t1.id != " + exclude_sales_id.ToString();
                
                sql += " order by t1.id";
                    
                cmd = new MySqlCommand(sql, conn);                                
                rdr = cmd.ExecuteReader();
                                
                while (rdr.Read())
                {
                    var field1 = rdr[0];
                    var field2 = rdr[1];
                    var field3 = rdr[2];
                    
                    cnt = Convert.ToInt32(field1);
                    if (cnt.GetValueOrDefault(0) <= 0)
                    {
                        conn.Close();
                        return new SalesList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден список торговых точек для заданного клиента") };
                    }

                    res.Sales_list.Add(new Sales() { SalesId = Convert.ToInt32(field2), SalesName = Convert.ToString(field3), });
                }
                rdr.Close();
                conn.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                if (conn != null)
                    conn.Close();
                return new SalesList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Ошибка SQL соединения: " + ex.Message) };
            }

            if (res.Sales_list.Count <= 0)
                return new SalesList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден список торговых точек для заданного клиента") };

            return res;
        }

        #endregion
    }
}
