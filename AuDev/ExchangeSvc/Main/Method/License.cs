﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using MySql.Data;
using MySql.Data.Common;
using MySql.Data.MySqlClient;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;

namespace ExchangeSvc
{
    public partial class ExchangeService : IExchangeService
    {
        #region GetLicense

        private License GetLicense(string login, string workplace, string version_num, int license_id)
        {
            License res = new License();

            DateTime now = DateTime.Now;

            if (String.IsNullOrEmpty(login))
            {
                //Loggly.InsertLog_License("Не задан логин", (long)Enums.LogEventType.ERROR, "license-checker", null, now, null);
                return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND, "Не задан логин") };
            }

            if (license_id <= 0)
            {
                //Loggly.InsertLog_License("Не задан код услуги", (long)Enums.LogEventType.ERROR, login, null, now, null);
                return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан код услуги") };
            }

            using (var dbContext = new AuMainDb())
            {
                var user = (from t1 in dbContext.vw_client_user
                            from t2 in dbContext.workplace
                            where t1.is_active == 1
                            && t1.user_login.Trim().ToLower().Equals(login.Trim().ToLower())
                            && t2.sales_id == t1.sales_id
                            && t2.is_deleted != 1
                            && t2.registration_key.Trim().ToLower().Equals(workplace.Trim().ToLower())
                            select new { client_id = t1.client_id, sales_id = t1.sales_id, workplace_id = t2.id })
                           .FirstOrDefault();

                if (user == null)
                {
                    //Loggly.InsertLog_License("Не найден пользователь с логином " + login.Trim() + " и жел. ключом " + hardwareId.Trim(), (long)Enums.LogEventType.ERROR, login, null, now, null);
                    return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND, "Не найден пользователь с логином " + login.Trim() + " и жел. ключом " + workplace.Trim()) };
                }

                bool versionSpecified = !String.IsNullOrEmpty(version_num);
                string version_name_start = !versionSpecified ? "" : new string(version_num.Take(4).ToArray());
                var license = (from t1 in dbContext.vw_client_service
                               where t1.workplace_id == user.workplace_id
                               && t1.service_id == license_id
                               && ((versionSpecified && t1.version_name != null && t1.version_name.StartsWith(version_name_start)) || (!versionSpecified))
                               select t1)
                              .FirstOrDefault();

                if (license == null)
                {
                    //Loggly.InsertLog_License("Не найдена лицензия на услугу с кодом " + applicationId.ToString() + ", " + (versionSpecified ? ("версия " + version.Trim() + ", ") : "") + "пользователь с логином " + login.Trim() + " и жел. ключом " + hardwareId.Trim(), (long)Enums.LogEventType.ERROR, login, null, now, null);
                    return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND, "Не найдена лицензия на услугу с кодом " + license_id.ToString() + ", " + (versionSpecified ? ("версия " + version_num.Trim() + ", ") : "") + "пользователь с логином " + login.Trim() + " и жел. ключом " + workplace.Trim()) };
                }

                res.SalesName = license.sales_address;
                res.Workplace = workplace;
                res.VersionNum = license.version_name;
                res.SalesPrefix = 100;
                res.ClientId = license.client_id;
                res.SalesId = license.sales_id;
                return res;
            }
        }

        #endregion

        #region GetLicense (old)

        /*
        private License GetLicense_old(string login, string workplace, string version_num, int license_id)
        {
            License res = new License();
            
            string sql;
            string mySqlConnectionString;
            int? cnt = 0;
            long sales_id = 0;
            long workplace_id = 0;
            string sales_name = "";
            string version_name = "";
            long client_id = 0;
            int? sales_prefix = 100;

            MySqlConnection conn = null;
            MySqlCommand cmd;
            MySqlDataReader rdr;
            mySqlConnectionString = ExchangeUtil.GetCabinetDbConnectionString();
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = mySqlConnectionString;
                conn.Open();

                sql =    "select count(*) as cnt, t1.id, t1.name, t1.client_id"
                            + " from clients.sales t1"
                            + " inner join clients.my_aspnet_Users t2 on t1.user_id = t2.id"
                            + " and t2.name = '" + login.Trim() + "'";
                            //+" and t2.name = 'apteka_ul2'";

                cmd = new MySqlCommand(sql, conn);                                
                rdr = cmd.ExecuteReader();
                                
                while (rdr.Read())
                {
                    var field1 = rdr[0];
                    var field2 = rdr[1];
                    var field3 = rdr[2];
                    var field4 = rdr[3];

                    cnt = Convert.ToInt32(field1);
                    if (cnt.GetValueOrDefault(0) <= 0)
                    {
                        conn.Close();
                        return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_SALES_NOT_FOUND, "Не найдена точка для заданного логина") };
                    }
                    if (cnt.GetValueOrDefault(0) > 1)
                    {
                        conn.Close();
                        return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_SALES_MORE_THAN_ONE, "Для заданного логина найдено " + cnt.ToString() + " точек") };
                    }

                    sales_id = Convert.ToInt64(field2);
                    sales_name = Convert.ToString(field3);
                    client_id = Convert.ToInt64(field4);
                }
                rdr.Close();

                sql = "select count(*) as cnt, t2.id"
                    + " from clients.sales t1"
                    + " inner join clients.workplace t2 on t1.id = t2.sales_id"
                    + " and t2.registration_key = '" + workplace.Trim() + "'"
                    + " where t1.id = " + sales_id.ToString();                

                cmd = new MySqlCommand(sql, conn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    var field1 = rdr[0];
                    var field2 = rdr[1];                    

                    cnt = Convert.ToInt32(field1);
                    if (cnt.GetValueOrDefault(0) <= 0)
                    {
                        conn.Close();
                        return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_WORKPLACE_NOT_FOUND, "Не найдено рабочее место для заданного логина") };
                    }
                    if (cnt.GetValueOrDefault(0) > 1)
                    {
                        conn.Close();
                        return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_WORKPLACE_MORE_THAN_ONE, "Для заданного логина найдено " + cnt.ToString() + " рабочих мест") };
                    }

                    workplace_id = Convert.ToInt64(field2);
                    
                }
                rdr.Close();

                sql = " select count(*) as cnt, t2.dt_start, t2.dt_end, t4.name"
                    + " from clients.service_registration t1"
                    + " inner join clients.license t2 on t1.id = t2.service_registration_id"
                    + " inner join clients.order_item t3 on t2.order_item_id = t3.id"
                    + " inner join clients.version t4 on t3.version_id = t4.id"
                    //+ " and t4.name = '" + version_num.Trim() + "'"
                    + " and left(t4.name, 3) = left('" + version_num.Trim() + "', 3)"
                    + " and t3.service_id = " + license_id
                    + " where t1.workplace_id = " + workplace_id.ToString()
                    + " and ((t2.dt_start is null) or (t2.dt_start <= current_date()))"
                    + " and ((t2.dt_end is null) or (t2.dt_end >= current_date()))"
                    + " order by t2.created_on desc"
                    + " limit 1";

                cmd = new MySqlCommand(sql, conn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    var field1 = rdr[0];
                    var field2 = rdr[1];
                    var field3 = rdr[2];
                    var field4 = rdr[3];

                    cnt = Convert.ToInt32(field1);
                    if (cnt.GetValueOrDefault(0) <= 0)
                    {
                        conn.Close();
                        return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, "Не найдена действующая лицензия") };
                    }

                    version_name = Convert.ToString(field4);
                }
                rdr.Close();

                // определяем префикс торговой точки
                sql = "select count(*) as cnt, t1.id"
                    + " from clients.sales t1"
                    + " where t1.client_id = " + client_id.ToString()
                    + " order by t1.id";

                cmd = new MySqlCommand(sql, conn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    var field1 = rdr[0];
                    var field2 = rdr[1];

                    cnt = Convert.ToInt32(field1);
                    if (cnt.GetValueOrDefault(0) <= 0)
                    {
                        conn.Close();
                        return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LIC_SALES_NOT_FOUND, "Не найден клиент для заданного логина") };
                    }

                    if (sales_id == Convert.ToInt64(field2))
                    {
                        break;
                    }
                    sales_prefix = sales_prefix + 100;

                }
                rdr.Close();
                conn.Close();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                if (conn != null)
                    conn.Close();
                return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Ошибка SQL соединения: " + ex.Message) };
            }

            res.SalesName = sales_name;
            res.Workplace = workplace;
            res.VersionNum = version_name;
            res.SalesPrefix = sales_prefix;
            res.ClientId = client_id;
            res.SalesId = sales_id;
            return res;
        }
        */

        #endregion        

        #region UpdateLicense

        private License xUpdateLicense(string password, long license_id, DateTime? date_start, DateTime? date_end)
        {
            if (password != "pillow")
                return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_ADD_DENIED, "Нет прав") };

            return new License() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован") };
        }

        #endregion
    }
}
