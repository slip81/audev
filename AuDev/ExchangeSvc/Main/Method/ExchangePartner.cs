﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Net;
using System.Collections.Specialized;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.IO;
using System.IO.Compression;
using System.Data;
using System.Data.OleDb;
using System.Web;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;


using MySql.Data;
using MySql.Data.Common;
using MySql.Data.MySqlClient;
using UnidecodeSharpFork;
using SocialExplorer.IO.FastDBF;

using AuDev.Common.Db.Model;
using AuDev.Common.Util;
using AuDev.Common.Extensions;


#endregion

namespace ExchangeSvc
{
    public partial class ExchangeService : IExchangeService
    {
        #region GetXml - скачиваем данные от Партнера клиенту

        private ExchangeXml GetXml_Error(DateTime date_beg, ErrInfo err, string err_str, long err_code, long? ds_id
            , UserInfo userInfo, Enums.ExchangeItemEnum exchangeType, bool toExchangeLog = true, int error_level = 2, long? exchange_id = null)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";
            //long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
            long? ds_id_actual = ds_id == null ? (userInfo != null ? (long?)userInfo.ds_id : null) : ds_id;
            string mess = err != null ? err.Message : err_str;
            Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, "GetXml");
            if (toExchangeLog)
            {
                Loggly.InsertExchangeLog(mess, null, user_name, date_beg
                    , (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)exchangeType, ds_id_actual, exchange_id, error_level, "GetXml");
            }

            return new ExchangeXml()
            {
                error = err != null ? err : new ErrInfo(err_code, err_str),               
            }
            ;
        }

        private ExchangeXml xGetXml(UserInfo userInfo, Enums.ExchangeItemEnum exchangeType)
        {
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;
            /*
                проверка лицензии
            */

            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            {
                return GetXml_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, null, userInfo, exchangeType);
            }

            /*             
                проверка пользователя и регистрации
            */
            
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
            {
                return GetXml_Error(now, user.error, "Не найден пользователь " + userInfo.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, null, userInfo, exchangeType);
            }
            long ds_id = userInfo.ds_id;
            if (ds_id == 0)
                ds_id = GetDefaultDsId();
            var reg = GetRegistration_Partner(user.user_id, ds_id);
            if ((reg == null) || (reg.error != null))
            {
                return GetXml_Error(now, reg.error, "Не найдена регистрация пользователя", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, ds_id, userInfo, exchangeType);
            }
            
            string file_path = "";                        
            // создаем временную папку
            //string tmp_dir_path = @"C:\Release\Exchange\File\Download\" + reg.login + "_" + "get" + "_" + now.ToString("yyyyMMddHHmmss") + @"\";
            string curr_date = today.ToString("yyyy-MM-dd");
            string tmp_dir_path = @"C:\Release\Exchange\File\Download\" + curr_date + "-" + user.login + @"\" + user.login + "_" + "get" + "_" + now.ToString("yyyyMMddHHmmss") + @"\";
            if (Directory.Exists(tmp_dir_path))
            {
                Directory.Delete(tmp_dir_path, true);
            }
            Directory.CreateDirectory(tmp_dir_path);
            //
            string yesterday_date = today.AddDays(-1).ToString("yyyy-MM-dd");
            string yesterday_dir_path = @"C:\Release\Exchange\File\Download\" + yesterday_date + "-" + user.login + @"\";
            try
            {
                if (Directory.Exists(yesterday_dir_path))
                {
                    Directory.Delete(yesterday_dir_path, true);
                }
            }
            catch (Exception ex)
            {
                //
                Loggly.InsertLog_ExchangePartner("[DEBUG] Ошибка при удалении папки: " + yesterday_dir_path + " [" + GlobalUtil.ExceptionInfo(ex) + "]", (long)Enums.LogEsnType.ERROR, user.login, null, now, null);
            }

            string ex_type_str = "";
            string ex_type_str2 = "";
            switch (exchangeType)
            {
                case Enums.ExchangeItemEnum.Plan:
                    ex_type_str = "$CP_list$";
                    break;
                case Enums.ExchangeItemEnum.Discount:
                    ex_type_str = "$Disc$";
                    break;
                case Enums.ExchangeItemEnum.Recommend:
                    ex_type_str = "$PR_List$";
                    break;
                default:
                    break;
            }
            
            // for MZ only
            string reg_code = (String.IsNullOrEmpty(reg.reg_code) ? "" : reg.reg_code.Trim()) + "#";
            

            bool file_exists = false;

            int i = 0;
            int cnt = 0;
            string path = "";
            string trnsfrpth = "";
            string orig_remote_file_name = "";
            string remote_file_name = "";
            string file_ext_orig = "txt";
            Guid guid;
            DateTime doc_date;

            XElement data_xml = null;

            XElement tov_pos = null;
            XElement kod_tovara = null;
            XElement tovar = null;
            XElement izgotov = null;
            XElement ean13 = null;
            XElement min_kol_na_ost = null;
            XElement rek_kol_na_ost = null;
            XElement min_kol_na_ost_dni = null;
            XElement rek_kol_na_ost_dni = null;

            XElement nazv_skidki = null;
            XElement proc_skidki = null;
            XElement data_nach = null;
            XElement data_okonch = null;

            XElement nazv_grupp = null;
            XElement perv_urov = null;
            XElement vtor_urov = null;
            XElement tret_urov = null;

            XElement tov_positions = new XElement("ТоварныеПозиции");

            if (String.IsNullOrEmpty(reg.login))
            {
                return GetXml_Error(now, null, "Не задан логин для соединения", (long)Enums.ErrCodeEnum.ERR_CODE_ATTR_NOT_FOUND, ds_id, userInfo, exchangeType);
            }

            if (String.IsNullOrEmpty(reg.download_url))
            {
                return GetXml_Error(now, null, "Не задан адрес загрузки", (long)Enums.ErrCodeEnum.ERR_CODE_ATTR_NOT_FOUND, ds_id, userInfo, exchangeType);
            }

            int convertNum = 0;
            string curr_file_date_str = "";
            string debugComment = "";
            string ftpUsername = reg.login;
            string ftpPassword = reg.password;
            string download_url = reg.download_url.Trim().EndsWith("/") ? reg.download_url.Trim() : (reg.download_url.Trim() + "/");

            switch ((Enums.ExchangeTransportType)reg.transport_type)
            {
                case Enums.ExchangeTransportType.FTP:
                    FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(download_url);
                    ftpRequest.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                    ftpRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                    StreamReader streamReader = new StreamReader(response.GetResponseStream());

                    List<string> directories = new List<string>();
                    List<DateTime> directory_create_dates = new List<DateTime>();
                    DateTime? max_file_date = null;
                    DateTime curr_file_date = now;
                    int j = -1;
                    curr_file_date_str = "";

                    convertNum = 0;
                    bool dateError = false;
                    debugComment = "1";
                    string line = streamReader.ReadLine();
                    while (!string.IsNullOrEmpty(line))
                    {
                        dateError = false;
                        curr_file_date_str = "";
                        debugComment = "2";
                        var line_array = line.Split(' ').ToArray();
                        if (line_array != null)
                        {
                            var line_array_cnt = line_array.Count();
                            debugComment = "3";
                            // !!! (почему 2 ?)
                            //if (line_array_cnt >= 2)
                            if (line_array_cnt >= 1)
                            {
                                debugComment = "4";
                                // здесь может быть ошибка при конвертации строку в дату,
                                // надо использовать WinSCP
                                // http://stackoverflow.com/questions/4454281/retrieving-creation-date-of-file-ftp
                                // http://winscp.net/eng/docs/script_download_most_recent_file

                                // !!!
                                //if ((line_array[line_array_cnt - 1].Contains(".")) && (line_array[line_array_cnt - 1].Contains(ex_type_str)))
                                if (
                                    ((line_array[line_array_cnt - 1].Contains(".")) && (line_array[line_array_cnt - 1].Contains(ex_type_str)) && (line_array[line_array_cnt - 1].StartsWith(reg_code, true, CultureInfo.CurrentCulture)))
                                   // !!!
                                    ||
                                    //для наименований файлов вида 20180216_101618.zip (новый формат от МЗ для файлов Товары ЦП)
                                    ((line_array[line_array_cnt - 1].Contains(".")) && (!line_array[line_array_cnt - 1].Contains("$")) && (Regex.IsMatch(line_array[line_array_cnt - 1], @"^\d+")) && (exchangeType == Enums.ExchangeItemEnum.Plan))
                                   )
                                {
                                    debugComment = "5";
                                    max_file_date = null;

                                    #region new

                                    convertNum = 0;
                                    try
                                    {
                                        //try 1

                                        //curr_file_date_str = line_array[line_array_cnt - 2];
                                        //curr_file_date = Convert.ToDateTime(curr_file_date_str);
                                        convertNum = 1;
                                        string curr_str = line_array[line_array_cnt - 1].Split('$').LastOrDefault();
                                        curr_file_date_str = curr_str.Trim().Substring(0, curr_str.IndexOf('.')).Replace("_", string.Empty);
                                        curr_file_date = DateTime.ParseExact(curr_file_date_str, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);
                                    }
                                    catch (Exception ex1)
                                    {
                                        try
                                        {
                                            //try 2
                                            convertNum = 2;
                                            curr_file_date_str = line_array[line_array_cnt - 4].Trim() + " " + line_array[line_array_cnt - 3].Trim() + " " + line_array[line_array_cnt - 2].Trim();
                                            curr_file_date = Convert.ToDateTime(curr_file_date_str);
                                        }
                                        catch (Exception ex2)
                                        {
                                            try
                                            {
                                                // !!!
                                                //try 3
                                                convertNum = 3;
                                                curr_file_date_str = line_array[line_array_cnt - 1].Trim().Substring(0, line_array[line_array_cnt - 1].IndexOf('.')).Replace("_", string.Empty);
                                                curr_file_date = DateTime.ParseExact(curr_file_date_str, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);                                                
                                            }
                                            catch (Exception ex3)
                                            {
                                                dateError = true;
                                            }

                                        }
                                    }

                                    if (dateError)
                                    {
                                        max_file_date = null;
                                        Loggly.InsertLog_ExchangePartner("[DEBUG 2] [convertNum=" + convertNum.ToString() + "] [debugComment=" + debugComment + "] Ошибка конвертации в дату из " + line_array[line_array_cnt - 1] + " [исходная строка: " + line + "] [ результат: " + curr_file_date_str + "]", (long)Enums.LogEsnType.ERROR, user.login, null, now, null);
                                        line = streamReader.ReadLine();
                                        if (string.IsNullOrEmpty(line))
                                            break;
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (directory_create_dates.Count > 0)
                                        {
                                            max_file_date = directory_create_dates.Max();
                                            if (curr_file_date >= max_file_date)
                                            {
                                                directories.Clear();
                                                directory_create_dates.Clear();
                                                directories.Add(line_array[line_array_cnt - 1]);
                                                directory_create_dates.Add(curr_file_date);
                                                max_file_date = curr_file_date;
                                            }
                                        }
                                        else
                                        {
                                            directories.Add(line_array[line_array_cnt - 1]);
                                            directory_create_dates.Add(curr_file_date);
                                            max_file_date = curr_file_date;
                                        }
                                    }
                                    
                                    #endregion

                                    #region old
                                    /*
                                    try
                                    {
                                        curr_file_date_str = line_array[line_array_cnt - 2];
                                        curr_file_date = Convert.ToDateTime(curr_file_date_str);
                                        if (directory_create_dates.Count > 0)
                                        {
                                            max_file_date = directory_create_dates.Max();
                                            if (curr_file_date >= max_file_date)
                                            {
                                                directories.Clear();
                                                directory_create_dates.Clear();
                                                directories.Add(line_array[line_array_cnt - 1]);
                                                directory_create_dates.Add(curr_file_date);
                                                max_file_date = curr_file_date;
                                            }
                                        }
                                        else
                                        {
                                            directories.Add(line_array[line_array_cnt - 1]);
                                            directory_create_dates.Add(curr_file_date);
                                            max_file_date = curr_file_date;
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        try
                                        {
                                            //-rw-r--r-- 1 ftp ftp 60244 Jan 28 2016 280#tmn_fms@soln$CP_list$20160128_124803.zip
                                            curr_file_date_str = line_array[line_array_cnt - 4].Trim() + " " + line_array[line_array_cnt - 3].Trim() + " " + line_array[line_array_cnt - 2].Trim();
                                            curr_file_date = Convert.ToDateTime(curr_file_date_str);
                                        }
                                        catch (Exception ex2)
                                        {
                                            max_file_date = null;
                                            Loggly.InsertLog_ExchangePartner("[DEBUG 2] Ошибка конвертации в дату из " + line_array[line_array_cnt - 1] + ": " + line + " [" + curr_file_date_str + "]", (long)Enums.LogEsnType.ERROR, user.login, null, now, null);
                                            line = streamReader.ReadLine();
                                            if (string.IsNullOrEmpty(line))
                                                break;
                                            else
                                                continue;
                                        }
                                    }
                                    */
                                    #endregion
                                }
                            }
                        }
                        line = streamReader.ReadLine();
                    }
                    streamReader.Close();
                    response.Close();

                    using (WebClient ftpClient = new WebClient())
                    {
                        ftpClient.Credentials = new System.Net.NetworkCredential(ftpUsername, ftpPassword);

                        if ((directories.Count > 0) && (max_file_date.HasValue))
                        {
                            j = 0;
                            orig_remote_file_name = directories[j].ToString();
                            remote_file_name = HttpUtility.UrlEncode(orig_remote_file_name);
                            path = download_url + remote_file_name;
                            //trnsfrpth = folder + orig_remote_file_name;
                            trnsfrpth = tmp_dir_path + orig_remote_file_name;
                            ftpClient.DownloadFile(path, trnsfrpth);
                            Loggly.InsertLog_ExchangePartner("С " + download_url + " скачан файл " + orig_remote_file_name, (long)Enums.LogEsnType.GET_EXCHANGE_XML, user.login, null, now, null);
                            file_path = trnsfrpth;
                            file_exists = true;
                        }
                    }

                    if (file_exists)
                    {
                        ftpRequest = (FtpWebRequest)WebRequest.Create(path);
                        ftpRequest.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                        ftpRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                        response = (FtpWebResponse)ftpRequest.GetResponse();
                        Loggly.InsertLog_ExchangePartner("С " + download_url + " удален файл " + orig_remote_file_name + " (status: " + response.StatusDescription + ")", (long)Enums.LogEsnType.GET_EXCHANGE_XML, user.login, null, now, null);
                        response.Close();
                    }

                    break;
                default:
                    break;
            }

            if (!file_exists)
            {
                // "Нет данных для передачи" - это считаем не обшибкой
                string mess_empty = "Приняты данные, тип " + exchangeType.ToString() + " [пустые] [convertNum=" + convertNum.ToString() + "] [date_str=" + curr_file_date_str + "] [debugComment=" + debugComment + "]";
                Loggly.InsertLog_ExchangePartner(mess_empty, (long)Enums.LogEsnType.GET_EXCHANGE_XML, user.login, null, now, "");
                Loggly.InsertExchangeLog(mess_empty, reg.reg_id, user.login, now, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)exchangeType, ds_id, null, 0, null);
                return new ExchangeXml() { Data = null, DataKind = (int)exchangeType, error = null, RecordCount = 0};

                //return GetXml_Error(now, null, "Нет данных для передачи, тип " + exchangeType.ToString(), (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, exchangeType);

                //string err = "Нет данных для передачи, тип " + exchangeType.ToString();
                //Loggly.InsertLog_ExchangePartner(err, (long)Enums.LogEsnType.ERROR, user.login, null);
                //return new ExchangeXml() { Data = "", error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, err), RecordCount = 0, DataKind = (int)exchangeType, };
            }
            Encoding enc = Encoding.GetEncoding("windows-1251");
            string file_path_arc = file_path;
            string file_name_in_archive = "";
            if (reg.download_arc_type > 0)
            {
                switch ((Enums.ArcType)reg.download_arc_type)
                {
                    case Enums.ArcType.ZIP:
                        string file_path_actual = Path.ChangeExtension(file_path_arc, file_ext_orig);
                        if (File.Exists(file_path_actual))
                        {
                            File.Delete(file_path_actual);
                        }

                        using (ZipArchive archive = ZipFile.OpenRead(file_path_arc))
                        {
                            foreach (ZipArchiveEntry entry in archive.Entries)
                            {
                                file_name_in_archive = entry.FullName;
                                break;                                
                            }
                        }                        

                        ZipFile.ExtractToDirectory(file_path_arc, Path.GetDirectoryName(file_path), enc);
                        //file_path = file_path_actual;
                        file_path = Path.Combine(Path.GetDirectoryName(file_path_arc), file_name_in_archive);

                        break;
                    default:
                        break;
                }
            }
            var data = File.ReadAllText(file_path, enc);
            string[] data_split = data.Split('\t', '\r', '\n');

            using (var context = new AuMainDb())
            {
                exchange exchange = new exchange();
                exchange.crt_date = now;
                //exchange.ds_id = (int)Enums.DataSourceEnum.MZ;
                exchange.ds_id = ds_id;
                exchange.exchange_type = (int)exchangeType;
                exchange.is_upload = false;
                exchange.user_id = user.user_id;
                context.exchange.Add(exchange);
                context.SaveChanges();

                exchange = context.exchange.Where(ss => ss.exchange_id == exchange.exchange_id).FirstOrDefault();

                exchange_data exchange_data = new exchange_data();
                exchange_data.exchange_id = exchange.exchange_id;
                //exchange_data.data_text = data;
                exchange_data.data_text = "";

                exchange_data.data_binary = GlobalUtil.GetBytes(data);

                context.exchange_data.Add(exchange_data);
                context.SaveChanges();

                // !!!
                exchange_reg_partner_item exchange_reg_partner_item = context.exchange_reg_partner_item.Where(ss => ss.reg_id == reg.reg_id && ss.exchange_item_id == (int)exchangeType).FirstOrDefault();
                if ((exchange_reg_partner_item != null) && (!exchange_reg_partner_item.is_download_to_client))                
                {
                    string mess_no_download = "Приняты данные, тип " + exchangeType.ToString() + " [не переданы клиенту]";
                    Loggly.InsertLog_ExchangePartner(mess_no_download, (long)Enums.LogEsnType.GET_EXCHANGE_XML, user.login, exchange.exchange_id, now, "");
                    Loggly.InsertExchangeLog(mess_no_download, reg.reg_id, user.login, now, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)exchangeType, ds_id, exchange.exchange_id, 0, null);
                    return new ExchangeXml() { Data = null, DataKind = (int)exchangeType, error = null, RecordCount = 0 };
                }

                switch (exchangeType)
                {
                    case Enums.ExchangeItemEnum.Plan:
                        foreach (var item in data_split)
                        {
                            if (i == 10)
                            {
                                tov_pos.Add(kod_tovara, tovar, izgotov, ean13, min_kol_na_ost, rek_kol_na_ost, min_kol_na_ost_dni, rek_kol_na_ost_dni);
                                tov_positions.Add(tov_pos);
                                i = 0;
                                cnt++;
                                continue;
                            }

                            switch (i)
                            {
                                case 0:
                                    tov_pos = new XElement("ТоварнаяПозиция");
                                    kod_tovara = new XElement("КодТовара", item);
                                    break;
                                case 1:
                                    tovar = new XElement("Товар", item);
                                    break;
                                case 2:
                                    izgotov = new XElement("Изготовитель", item);
                                    break;
                                case 3:
                                    ean13 = new XElement("ЕАН13", item);
                                    break;
                                case 4:
                                    min_kol_na_ost = new XElement("МинКолНаОст", item);
                                    break;
                                case 5:
                                    rek_kol_na_ost = new XElement("РекКолНаОст", item);
                                    break;
                                case 6:
                                    min_kol_na_ost_dni = new XElement("МинКолНаОстДни", item);
                                    break;
                                case 7:
                                    rek_kol_na_ost_dni = new XElement("РекКолНаОстДни", item);
                                    break;
                                case 8:
                                    break;
                                case 9:
                                    break;
                                default:
                                    break;
                            }

                            i++;

                        }

                        guid = Guid.NewGuid();
                        doc_date = now;

                        data_xml = new XElement("Документ",
                               new XAttribute("Идентификатор", guid.ToString()),
                               new XElement("ЗаголовокДокумента",
                                   new XElement("ТипДок", "МРК"),
                                   new XElement("НомерДок", "1111"),
                                   new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                                   new XElement("Поставщик", @"ООО Поставщик 1"),
                                   new XElement("Получатель", @"ООО Получатель 1"),
                                   new XElement("Позиций", cnt.ToString()),
                                   new XElement("СуммаОпт", "0.00"),
                                   new XElement("СуммаНДС", "0.00"),
                                   new XElement("СуммаОптВклНДС", "0.00")
                                   ),
                                   tov_positions);
                        break;
                    case Enums.ExchangeItemEnum.Discount:
                        foreach (var item in data_split)
                        {
                            if (i == 8)
                            {
                                tov_pos.Add(kod_tovara, tovar, nazv_skidki, proc_skidki, data_nach, data_okonch);
                                tov_positions.Add(tov_pos);
                                i = 0;
                                cnt++;
                                continue;
                            }

                            switch (i)
                            {
                                case 0:
                                    tov_pos = new XElement("ТоварнаяПозиция");
                                    //
                                    nazv_skidki = new XElement("НазвСкидки", item);
                                    break;
                                case 1:
                                    proc_skidki = new XElement("ПроцСкидки", item);
                                    break;
                                case 2:
                                    data_nach = new XElement("ДатаНачала", item);
                                    break;
                                case 3:
                                    data_okonch = new XElement("ДатаОкончания", item);
                                    break;
                                case 4:
                                    kod_tovara = new XElement("КодТовара", item);
                                    break;
                                case 5:
                                    tovar = new XElement("Товар", item);
                                    break;
                                case 6:
                                    break;
                                case 7:
                                    break;
                                default:
                                    break;
                            }

                            i++;

                        }

                        guid = Guid.NewGuid();
                        doc_date = now;

                        data_xml = new XElement("Документ",
                               new XAttribute("Идентификатор", guid.ToString()),
                               new XElement("ЗаголовокДокумента",
                                   new XElement("ТипДок", "МРК"),
                                   new XElement("НомерДок", "1111"),
                                   new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                                   new XElement("Поставщик", @"ООО Поставщик 1"),
                                   new XElement("Получатель", @"ООО Получатель 1"),
                                   new XElement("Позиций", cnt.ToString()),
                                   new XElement("СуммаОпт", "0.00"),
                                   new XElement("СуммаНДС", "0.00"),
                                   new XElement("СуммаОптВклНДС", "0.00")
                                   ),
                                   tov_positions);
                        break;
                    case Enums.ExchangeItemEnum.Recommend:
                        foreach (var item in data_split)
                        {
                            if (i == 8)
                            {
                                tov_pos.Add(kod_tovara, tovar, nazv_grupp, perv_urov, vtor_urov, tret_urov, ean13);
                                tov_positions.Add(tov_pos);
                                i = 0;
                                cnt++;
                            }

                            switch (i)
                            {
                                case 0:
                                    tov_pos = new XElement("ТоварнаяПозиция");
                                    //
                                    nazv_grupp = new XElement("НазвГруппы", item);
                                    break;
                                case 1:
                                    kod_tovara = new XElement("КодТовара", item);
                                    break;
                                case 2:
                                    tovar = new XElement("Товар", item);
                                    break;
                                case 3:
                                    perv_urov = new XElement("ПервыйУровень", item);
                                    break;
                                case 4:
                                    vtor_urov = new XElement("ВторойУровень", item);
                                    break;
                                case 5:
                                    tret_urov = new XElement("ТретийУровень", item);
                                    break;
                                case 6:
                                    ean13 = new XElement("EAN13", item);
                                    break;
                                case 7:
                                    break;
                                default:
                                    break;
                            }

                            i++;

                        }

                        guid = Guid.NewGuid();
                        doc_date = now;

                        data_xml = new XElement("Документ",
                               new XAttribute("Идентификатор", guid.ToString()),
                               new XElement("ЗаголовокДокумента",
                                   new XElement("ТипДок", "МРК"),
                                   new XElement("НомерДок", "1111"),
                                   new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                                   new XElement("Поставщик", @"ООО Поставщик 1"),
                                   new XElement("Получатель", @"ООО Получатель 1"),
                                   new XElement("Позиций", cnt.ToString()),
                                   new XElement("СуммаОпт", "0.00"),
                                   new XElement("СуммаНДС", "0.00"),
                                   new XElement("СуммаОптВклНДС", "0.00")
                                   ),
                                   tov_positions);
                        break;
                    default:
                        break;
                }

                ExchangeXml res = new ExchangeXml() { Data = "", RecordCount = cnt, DataKind = (int)exchangeType, error = null, };

                res.Data += data_xml;
                res.AddDataCompleted();

                // удаляем временную папку
                /*
                DirectoryInfo di = new DirectoryInfo(tmp_dir_path);
                di.Delete(true);
                */

                // http://stackoverflow.com/questions/13262548/delete-a-file-being-used-by-another-process

                // мешает w3wp.exe ?
                /*
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                if (Directory.Exists(tmp_dir_path))
                {
                    Directory.Delete(tmp_dir_path, true);
                }
                */

                string mess = "Приняты данные, тип " + exchangeType.ToString();
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.GET_EXCHANGE_XML, user.login, exchange.exchange_id, now, "");
                Loggly.InsertExchangeLog(mess, reg.reg_id, user.login, now, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)exchangeType, ds_id, exchange.exchange_id, 0, null);                

                return res;
            }

        }

        private string xTest_GetXml(string pwd, string filePath, int exchangeTypeInt)
        {
            if (!(pwd.Equals("iguana")))
                return "Нет прав";

            int i = 0;
            int cnt = 0;
            Guid guid;
            DateTime doc_date;
            DateTime now = DateTime.Now;

            XElement data_xml = null;

            XElement tov_pos = null;
            XElement kod_tovara = null;
            XElement tovar = null;
            XElement izgotov = null;
            XElement ean13 = null;
            XElement min_kol_na_ost = null;
            XElement rek_kol_na_ost = null;
            XElement min_kol_na_ost_dni = null;
            XElement rek_kol_na_ost_dni = null;

            XElement nazv_skidki = null;
            XElement proc_skidki = null;
            XElement data_nach = null;
            XElement data_okonch = null;

            XElement nazv_grupp = null;
            XElement perv_urov = null;
            XElement vtor_urov = null;
            XElement tret_urov = null;

            XElement tov_positions = new XElement("ТоварныеПозиции");

            Enums.ExchangeItemEnum exchangeType = (Enums.ExchangeItemEnum)exchangeTypeInt;

            Encoding enc = Encoding.GetEncoding("windows-1251");
            var data = File.ReadAllText(filePath, enc);
            string[] data_split = data.Split('\t', '\r', '\n');

            switch (exchangeType)
            {
                case Enums.ExchangeItemEnum.Plan:
                    foreach (var item in data_split)
                    {
                        if (i == 10)
                        {
                            tov_pos.Add(kod_tovara, tovar, izgotov, ean13, min_kol_na_ost, rek_kol_na_ost, min_kol_na_ost_dni, rek_kol_na_ost_dni);
                            tov_positions.Add(tov_pos);
                            i = 0;
                            cnt++;
                            continue;
                        }

                        switch (i)
                        {
                            case 0:
                                tov_pos = new XElement("ТоварнаяПозиция");
                                kod_tovara = new XElement("КодТовара", item);
                                break;
                            case 1:
                                tovar = new XElement("Товар", item);
                                break;
                            case 2:
                                izgotov = new XElement("Изготовитель", item);
                                break;
                            case 3:
                                ean13 = new XElement("ЕАН13", item);
                                break;
                            case 4:
                                min_kol_na_ost = new XElement("МинКолНаОст", item);
                                break;
                            case 5:
                                rek_kol_na_ost = new XElement("РекКолНаОст", item);
                                break;
                            case 6:
                                min_kol_na_ost_dni = new XElement("МинКолНаОстДни", item);
                                break;
                            case 7:
                                rek_kol_na_ost_dni = new XElement("РекКолНаОстДни", item);
                                break;
                            case 8:
                                break;
                            case 9:
                                break;
                            default:
                                break;
                        }

                        i++;

                    }

                    guid = Guid.NewGuid();
                    doc_date = now;

                    data_xml = new XElement("Документ",
                           new XAttribute("Идентификатор", guid.ToString()),
                           new XElement("ЗаголовокДокумента",
                               new XElement("ТипДок", "МРК"),
                               new XElement("НомерДок", "1111"),
                               new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                               new XElement("Поставщик", @"ООО Поставщик 1"),
                               new XElement("Получатель", @"ООО Получатель 1"),
                               new XElement("Позиций", cnt.ToString()),
                               new XElement("СуммаОпт", "0.00"),
                               new XElement("СуммаНДС", "0.00"),
                               new XElement("СуммаОптВклНДС", "0.00")
                               ),
                               tov_positions);
                    break;
                case Enums.ExchangeItemEnum.Discount:
                    foreach (var item in data_split)
                    {
                        if (i == 8)
                        {
                            tov_pos.Add(kod_tovara, tovar, nazv_skidki, proc_skidki, data_nach, data_okonch);
                            tov_positions.Add(tov_pos);
                            i = 0;
                            cnt++;
                            continue;
                        }

                        switch (i)
                        {
                            case 0:
                                tov_pos = new XElement("ТоварнаяПозиция");
                                //
                                nazv_skidki = new XElement("НазвСкидки", item);
                                break;
                            case 1:
                                proc_skidki = new XElement("ПроцСкидки", item);
                                break;
                            case 2:
                                data_nach = new XElement("ДатаНачала", item);
                                break;
                            case 3:
                                data_okonch = new XElement("ДатаОкончания", item);
                                break;
                            case 4:
                                kod_tovara = new XElement("КодТовара", item);
                                break;
                            case 5:
                                tovar = new XElement("Товар", item);
                                break;
                            case 6:
                                break;
                            case 7:
                                break;
                            default:
                                break;
                        }

                        i++;

                    }

                    guid = Guid.NewGuid();
                    doc_date = now;

                    data_xml = new XElement("Документ",
                           new XAttribute("Идентификатор", guid.ToString()),
                           new XElement("ЗаголовокДокумента",
                               new XElement("ТипДок", "МРК"),
                               new XElement("НомерДок", "1111"),
                               new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                               new XElement("Поставщик", @"ООО Поставщик 1"),
                               new XElement("Получатель", @"ООО Получатель 1"),
                               new XElement("Позиций", cnt.ToString()),
                               new XElement("СуммаОпт", "0.00"),
                               new XElement("СуммаНДС", "0.00"),
                               new XElement("СуммаОптВклНДС", "0.00")
                               ),
                               tov_positions);
                    break;
                case Enums.ExchangeItemEnum.Recommend:
                    foreach (var item in data_split)
                    {
                        if (i == 8)
                        {
                            tov_pos.Add(kod_tovara, tovar, nazv_grupp, perv_urov, vtor_urov, tret_urov, ean13);
                            tov_positions.Add(tov_pos);
                            i = 0;
                            cnt++;
                        }

                        switch (i)
                        {
                            case 0:
                                tov_pos = new XElement("ТоварнаяПозиция");
                                //
                                nazv_grupp = new XElement("НазвГруппы", item);
                                break;
                            case 1:
                                kod_tovara = new XElement("КодТовара", item);
                                break;
                            case 2:
                                tovar = new XElement("Товар", item);
                                break;
                            case 3:
                                perv_urov = new XElement("ПервыйУровень", item);
                                break;
                            case 4:
                                vtor_urov = new XElement("ВторойУровень", item);
                                break;
                            case 5:
                                tret_urov = new XElement("ТретийУровень", item);
                                break;
                            case 6:
                                ean13 = new XElement("EAN13", item);
                                break;
                            case 7:
                                break;
                            default:
                                break;
                        }

                        i++;

                    }

                    guid = Guid.NewGuid();
                    doc_date = now;

                    data_xml = new XElement("Документ",
                           new XAttribute("Идентификатор", guid.ToString()),
                           new XElement("ЗаголовокДокумента",
                               new XElement("ТипДок", "МРК"),
                               new XElement("НомерДок", "1111"),
                               new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                               new XElement("Поставщик", @"ООО Поставщик 1"),
                               new XElement("Получатель", @"ООО Получатель 1"),
                               new XElement("Позиций", cnt.ToString()),
                               new XElement("СуммаОпт", "0.00"),
                               new XElement("СуммаНДС", "0.00"),
                               new XElement("СуммаОптВклНДС", "0.00")
                               ),
                               tov_positions);
                    break;
                default:
                    break;
            }

            return data_xml.ToString();
        }

        private ExchangeXml xTest_GetXml2(string pwd, UserInfo userInfo, Enums.ExchangeItemEnum exchangeType)
        {
            if (!(pwd.Equals("iguana")))
                return null;

            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            //string file_path = @"c:\Pavlov\Exchange_File\01\495#ekt_ups@a05$CP_list$20170502_095658.zip";
            string file_path = @"c:\Pavlov\Exchange_File\02\495#ekt_ups@a05$PR_List$20170501024026.zip";

            string curr_date = today.ToString("yyyy-MM-dd");
            string tmp_dir_path = @"C:\Release\Exchange\File\Download\" + curr_date + "-" + "ural3int" + @"\" + "ural3int" + "_" + "get" + "_" + now.ToString("yyyyMMddHHmmss") + @"\";
            if (Directory.Exists(tmp_dir_path))
            {
                Directory.Delete(tmp_dir_path, true);
            }
            Directory.CreateDirectory(tmp_dir_path);
            //

            // !!!
            // for MZ only
            string reg_code = "";


            bool file_exists = false;

            int i = 0;
            int cnt = 0;
            string file_ext_orig = "txt";
            Guid guid;
            DateTime doc_date;

            XElement data_xml = null;

            XElement tov_pos = null;
            XElement kod_tovara = null;
            XElement tovar = null;
            XElement izgotov = null;
            XElement ean13 = null;
            XElement min_kol_na_ost = null;
            XElement rek_kol_na_ost = null;
            XElement min_kol_na_ost_dni = null;
            XElement rek_kol_na_ost_dni = null;

            XElement nazv_skidki = null;
            XElement proc_skidki = null;
            XElement data_nach = null;
            XElement data_okonch = null;

            XElement nazv_grupp = null;
            XElement perv_urov = null;
            XElement vtor_urov = null;
            XElement tret_urov = null;

            XElement tov_positions = new XElement("ТоварныеПозиции");

            Encoding enc = Encoding.GetEncoding("windows-1251");
            string file_path_arc = file_path;
            if (1 == 1)
            {
                string file_path_actual = Path.ChangeExtension(file_path_arc, file_ext_orig);
                if (File.Exists(file_path_actual))
                {
                    File.Delete(file_path_actual);
                }
                ZipFile.ExtractToDirectory(file_path_arc, Path.GetDirectoryName(file_path), enc);
                file_path = file_path_actual;
            }

            var data = File.ReadAllText(file_path, enc);
            string[] data_split = data.Split('\t', '\r', '\n');

            using (var context = new AuMainDb())
            {
                exchange exchange = new exchange();
                exchange.crt_date = now;
                //exchange.ds_id = (int)Enums.DataSourceEnum.MZ;
                exchange.ds_id = 101;
                exchange.exchange_type = (int)exchangeType;
                exchange.is_upload = false;
                exchange.user_id = 114;
                context.exchange.Add(exchange);
                context.SaveChanges();

                exchange = context.exchange.Where(ss => ss.exchange_id == exchange.exchange_id).FirstOrDefault();

                exchange_data exchange_data = new exchange_data();
                exchange_data.exchange_id = exchange.exchange_id;
                //exchange_data.data_text = data;
                exchange_data.data_text = "";

                exchange_data.data_binary = GlobalUtil.GetBytes(data);

                context.exchange_data.Add(exchange_data);
                context.SaveChanges();

                switch (exchangeType)
                {
                    case Enums.ExchangeItemEnum.Plan:
                        foreach (var item in data_split)
                        {
                            if (i == 10)
                            {
                                tov_pos.Add(kod_tovara, tovar, izgotov, ean13, min_kol_na_ost, rek_kol_na_ost, min_kol_na_ost_dni, rek_kol_na_ost_dni);
                                tov_positions.Add(tov_pos);
                                i = 0;
                                cnt++;
                                continue;
                            }

                            switch (i)
                            {
                                case 0:
                                    tov_pos = new XElement("ТоварнаяПозиция");
                                    kod_tovara = new XElement("КодТовара", item);
                                    break;
                                case 1:
                                    tovar = new XElement("Товар", item);
                                    break;
                                case 2:
                                    izgotov = new XElement("Изготовитель", item);
                                    break;
                                case 3:
                                    ean13 = new XElement("ЕАН13", item);
                                    break;
                                case 4:
                                    min_kol_na_ost = new XElement("МинКолНаОст", item);
                                    break;
                                case 5:
                                    rek_kol_na_ost = new XElement("РекКолНаОст", item);
                                    break;
                                case 6:
                                    min_kol_na_ost_dni = new XElement("МинКолНаОстДни", item);
                                    break;
                                case 7:
                                    rek_kol_na_ost_dni = new XElement("РекКолНаОстДни", item);
                                    break;
                                case 8:
                                    break;
                                case 9:
                                    break;
                                default:
                                    break;
                            }

                            i++;

                        }

                        guid = Guid.NewGuid();
                        doc_date = now;

                        data_xml = new XElement("Документ",
                               new XAttribute("Идентификатор", guid.ToString()),
                               new XElement("ЗаголовокДокумента",
                                   new XElement("ТипДок", "МРК"),
                                   new XElement("НомерДок", "1111"),
                                   new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                                   new XElement("Поставщик", @"ООО Поставщик 1"),
                                   new XElement("Получатель", @"ООО Получатель 1"),
                                   new XElement("Позиций", cnt.ToString()),
                                   new XElement("СуммаОпт", "0.00"),
                                   new XElement("СуммаНДС", "0.00"),
                                   new XElement("СуммаОптВклНДС", "0.00")
                                   ),
                                   tov_positions);
                        break;
                    case Enums.ExchangeItemEnum.Discount:
                        foreach (var item in data_split)
                        {
                            if (i == 8)
                            {
                                tov_pos.Add(kod_tovara, tovar, nazv_skidki, proc_skidki, data_nach, data_okonch);
                                tov_positions.Add(tov_pos);
                                i = 0;
                                cnt++;
                                continue;
                            }

                            switch (i)
                            {
                                case 0:
                                    tov_pos = new XElement("ТоварнаяПозиция");
                                    //
                                    nazv_skidki = new XElement("НазвСкидки", item);
                                    break;
                                case 1:
                                    proc_skidki = new XElement("ПроцСкидки", item);
                                    break;
                                case 2:
                                    data_nach = new XElement("ДатаНачала", item);
                                    break;
                                case 3:
                                    data_okonch = new XElement("ДатаОкончания", item);
                                    break;
                                case 4:
                                    kod_tovara = new XElement("КодТовара", item);
                                    break;
                                case 5:
                                    tovar = new XElement("Товар", item);
                                    break;
                                case 6:
                                    break;
                                case 7:
                                    break;
                                default:
                                    break;
                            }

                            i++;

                        }

                        guid = Guid.NewGuid();
                        doc_date = now;

                        data_xml = new XElement("Документ",
                               new XAttribute("Идентификатор", guid.ToString()),
                               new XElement("ЗаголовокДокумента",
                                   new XElement("ТипДок", "МРК"),
                                   new XElement("НомерДок", "1111"),
                                   new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                                   new XElement("Поставщик", @"ООО Поставщик 1"),
                                   new XElement("Получатель", @"ООО Получатель 1"),
                                   new XElement("Позиций", cnt.ToString()),
                                   new XElement("СуммаОпт", "0.00"),
                                   new XElement("СуммаНДС", "0.00"),
                                   new XElement("СуммаОптВклНДС", "0.00")
                                   ),
                                   tov_positions);
                        break;
                    case Enums.ExchangeItemEnum.Recommend:
                        foreach (var item in data_split)
                        {
                            if (i == 8)
                            {
                                tov_pos.Add(kod_tovara, tovar, nazv_grupp, perv_urov, vtor_urov, tret_urov, ean13);
                                tov_positions.Add(tov_pos);
                                i = 0;
                                cnt++;
                            }

                            switch (i)
                            {
                                case 0:
                                    tov_pos = new XElement("ТоварнаяПозиция");
                                    //
                                    nazv_grupp = new XElement("НазвГруппы", item);
                                    break;
                                case 1:
                                    kod_tovara = new XElement("КодТовара", item);
                                    break;
                                case 2:
                                    tovar = new XElement("Товар", item);
                                    break;
                                case 3:
                                    perv_urov = new XElement("ПервыйУровень", item);
                                    break;
                                case 4:
                                    vtor_urov = new XElement("ВторойУровень", item);
                                    break;
                                case 5:
                                    tret_urov = new XElement("ТретийУровень", item);
                                    break;
                                case 6:
                                    ean13 = new XElement("EAN13", item);
                                    break;
                                case 7:
                                    break;
                                default:
                                    break;
                            }

                            i++;

                        }

                        guid = Guid.NewGuid();
                        doc_date = now;

                        data_xml = new XElement("Документ",
                               new XAttribute("Идентификатор", guid.ToString()),
                               new XElement("ЗаголовокДокумента",
                                   new XElement("ТипДок", "МРК"),
                                   new XElement("НомерДок", "1111"),
                                   new XElement("ДатаДок", doc_date.ToString("dd.MM.yyyy HH:mm:ss")),
                                   new XElement("Поставщик", @"ООО Поставщик 1"),
                                   new XElement("Получатель", @"ООО Получатель 1"),
                                   new XElement("Позиций", cnt.ToString()),
                                   new XElement("СуммаОпт", "0.00"),
                                   new XElement("СуммаНДС", "0.00"),
                                   new XElement("СуммаОптВклНДС", "0.00")
                                   ),
                                   tov_positions);
                        break;
                    default:
                        break;
                }

                ExchangeXml res = new ExchangeXml() { Data = "", RecordCount = cnt, DataKind = (int)exchangeType, error = null, };

                res.Data += data_xml;
                res.AddDataCompleted();

                string mess = "Приняты данные, тип " + exchangeType.ToString();
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.GET_EXCHANGE_XML, "ural3int", exchange.exchange_id, now, "");
                //Loggly.InsertExchangeLog(mess, reg.reg_id, user.login, now, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)exchangeType, ds_id, exchange.exchange_id, 0, null);

                return res;
            }

        }

        #endregion

        #region SendData - отдаем данные клиента Партнеру

        /*

            Переделка метода SendData под универсальную отправку
            -----------------------------------------------------

            1.
            exchange.sales_prefix = reg.use_sales_prefix == 1 ? lic.SalesPrefix : null;

            Для АСНА делается перекодировка Prep и Client - добавляется вначале код ТТ.

            2.
            Enums.ExchangeItemEnum
            надо избавиться от этого и пользоваться только таблицей esn.exchange_item

            3.
            нужен признак - упакован ли передаваемый файл

            4.
            нужно полное наименование файла

            5.
            для формирования названия файла на клиенте нужно иметь инфу о регистрации
        */

        private ServiceOperationResult SendData_Error(DateTime date_beg, ErrInfo err, string err_str, long err_code, long? ds_id
            , UserInfo userInfo, Enums.ExchangeItemEnum exchangeType, bool toExchangeLog = true, int error_level = 2, long? exchange_id = null)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";
            //long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
            long? ds_id_actual = ds_id == null ? (userInfo != null ? (long?)userInfo.ds_id : null) : ds_id;
            string mess = err != null ? err.Message : err_str;
            Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, "SendData");
            if (toExchangeLog)
            {
                Loggly.InsertExchangeLog(mess, null, user_name, date_beg
                    , (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)exchangeType, ds_id_actual, exchange_id, error_level, "SendData");
            }

            return new ServiceOperationResult()
            {
                error = err != null ? err : new ErrInfo(err_code, err_str),
                Id = err != null ? (err.Id.HasValue ? (long)err.Id : err_code) : err_code,
            }
            ;
        }

        private Tuple<bool, string> SendDataToPartner_Error(DateTime date_beg, ErrInfo err, string err_str, long? ds_id
            , UserInfo userInfo, Enums.ExchangeItemEnum exchangeType, bool toExchangeLog = true, int error_level = 2, long? exchange_id = null)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";
            //long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
            long? ds_id_actual = ds_id == null ? (userInfo != null ? (long?)userInfo.ds_id : null) : ds_id;
            string mess = err != null ? err.Message : err_str;
            Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, "SendDataToPartner");
            if (toExchangeLog)
            {
                Loggly.InsertExchangeLog(mess, null, user_name, date_beg
                    , (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)exchangeType, ds_id_actual, exchange_id, error_level, "SendDataToPartner");
            }

            return new Tuple<bool, string>(false, mess);
        }

        private ServiceOperationResult xSendData(UserInfo userInfo, string data, Enums.ExchangeItemEnum exchangeType)
        {
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            /*
             проверка лицензии
            */

            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            {
                return SendData_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, null, userInfo, exchangeType);
                //Loggly.InsertLog_ExchangePartner(lic.error.Message, (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                //return new ServiceOperationResult((long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, lic.error);
            }

            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
            {
                return SendData_Error(now, user.error, "Не найден пользователь " + user.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, null, userInfo, exchangeType);
                //Loggly.InsertLog_ExchangePartner((user.error != null ? user.error.Message : "Не найден пользователь " + user.login), (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                //return new ServiceOperationResult() { error = user.error != null ? user.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь " + user.login), Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, };
            }

            var client = GetClient(user.cient_id.GetValueOrDefault(0));
            if ((client == null) || (client.error != null))
            {
                return SendData_Error(now, client.error, "Не найден клиент - участник обмена с кодом " + user.cient_id.GetValueOrDefault(0).ToString(), (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, null, userInfo, exchangeType);
                //Loggly.InsertLog_ExchangePartner((client.error != null ? client.error.Message : "Не найден клиент - участник обмена с кодом " + user.cient_id.GetValueOrDefault(0).ToString()), (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                //return new ServiceOperationResult() { error = client.error != null ? client.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден клиент - участник обмена с кодом " + user.cient_id.GetValueOrDefault(0).ToString()), Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, };
            }

            long ds_id = userInfo.ds_id;
            if (ds_id == 0)
                ds_id = GetDefaultDsId();
            var reg = GetRegistration_Partner(user.user_id, ds_id);

            if ((reg == null) || (reg.error != null))
            {
                return SendData_Error(now, reg.error, "Не найдена регистрация пользователя", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, ds_id, userInfo, exchangeType);
                //Loggly.InsertLog_ExchangePartner((reg.error != null ? reg.error.Message : "Не найдена регистрации пользователя"), (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                //return new ServiceOperationResult() { error = reg.error != null ? reg.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрации пользователя"), Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, };
            }
            
            using (var context = new AuMainDb())
            {
                exchange exchange = new exchange();
                exchange.crt_date = DateTime.Now;
                //exchange.ds_id = (int)Enums.DataSourceEnum.MZ;
                exchange.ds_id = ds_id;
                exchange.exchange_type = (int)exchangeType;
                exchange.is_upload = true;
                exchange.user_id = user.user_id;
                exchange.sales_prefix = reg.use_sales_prefix == 1 ? lic.SalesPrefix : null;
                context.exchange.Add(exchange);
                context.SaveChanges();

                exchange = context.exchange.Where(ss => ss.exchange_id == exchange.exchange_id).FirstOrDefault();

                exchange_data exchange_data = new exchange_data();
                exchange_data.exchange_id = exchange.exchange_id;
                //exchange_data.data_text = data;
                exchange_data.data_text = "";
                exchange_data.data_binary = GlobalUtil.GetBytes(data);

                context.exchange_data.Add(exchange_data);
                context.SaveChanges();

                // создаем временную папку
                /*
                string tmp_dir_path = @"C:\Release\Exchange\File\Upload\" + reg.login + "_" + "send" + "_" + now.ToString("yyyyMMddHHmmss") + @"\";
                if (Directory.Exists(tmp_dir_path))
                {
                    Directory.Delete(tmp_dir_path, true);
                }
                Directory.CreateDirectory(tmp_dir_path);
                */
                string curr_date = today.ToString("yyyy-MM-dd");
                string tmp_dir_path = @"C:\Release\Exchange\File\Upload\" + curr_date + "-" + user.login + @"\" + user.login + "_" + "send" + "_" + now.ToString("yyyyMMddHHmmss") + @"\";
                if (Directory.Exists(tmp_dir_path))
                {
                    Directory.Delete(tmp_dir_path, true);
                }
                Directory.CreateDirectory(tmp_dir_path);
                //
                string yesterday_date = today.AddDays(-1).ToString("yyyy-MM-dd");
                string yesterday_dir_path = @"C:\Release\Exchange\File\Upload\" + yesterday_date + "-" + user.login + @"\";
                try
                {
                    if (Directory.Exists(yesterday_dir_path))
                    {
                        Directory.Delete(yesterday_dir_path, true);
                    }
                }
                catch (Exception ex)
                {
                    //
                    Loggly.InsertLog_ExchangePartner("[DEBUG] Ошибка при удалении папки: " + yesterday_dir_path + " [" + GlobalUtil.ExceptionInfo(ex) + "]", (long)Enums.LogEsnType.ERROR, user.login, null, now, null);
                }

                Tuple<bool, string> res1;
                switch (exchangeType)
                {
                    case Enums.ExchangeItemEnum.Client:
                    case Enums.ExchangeItemEnum.Prep:
                        res1 = SendDataToPartner_Sprav(userInfo, client, user, reg, exchangeType, exchange.exchange_id, lic.SalesPrefix, tmp_dir_path);
                        break;
                    default:
                        res1 = SendDataToPartner(userInfo, client, user, reg, exchangeType, exchange.exchange_id, lic.SalesPrefix, tmp_dir_path);
                        break;
                }

                // удаляем временную папку
                /*
                DirectoryInfo di = new DirectoryInfo(tmp_dir_path);
                di.Delete(true);
                */

                // http://stackoverflow.com/questions/13262548/delete-a-file-being-used-by-another-process

                // мешает w3wp.exe ?
                /*
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                if (Directory.Exists(tmp_dir_path))
                {
                    Directory.Delete(tmp_dir_path, true);
                }
                */
                
                if (res1.Item1)
                {
                    string mess = "Переданы данные, тип " + exchangeType.ToString();
                    Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.SEND_EXCHANGE_DATA, user.login, exchange.exchange_id, now, "");
                    Loggly.InsertExchangeLog(mess, reg.reg_id, user.login, now, (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)exchangeType, ds_id, exchange.exchange_id, 0, null);

                    return new ServiceOperationResult() { error = null, Id = 0, };
                }
                else
                {
                    return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, res1.Item2), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, };
                }
            }
        }

        private Tuple<bool, string> SendDataToPartner(UserInfo userInfo, ExchangeClient client, ExchangeUser user, ExchangeRegPartner registration, Enums.ExchangeItemEnum exchangeType
            , long exchange_id, int? sales_prefix, string dir_path)
        {
            DateTime now = DateTime.Now;
            var res1 = false;
            var res2 = "";
            using (var context = new AuMainDb())
            {
                var data = context.exchange_data.Where(ss => ss.exchange_id == exchange_id).Select(ss => ss.data_binary).FirstOrDefault();
                if (data == null)
                {
                    return SendDataToPartner_Error(now, null, "Файл для данных с кодом " + exchange_id.ToString() + " не создан", null, userInfo, exchangeType);
                    //res2 = "Файл для данных с кодом " + exchange_id.ToString() + " не создан";
                    //Loggly.InsertLog_ExchangePartner(res2, (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                    //return new Tuple<bool, string>(res1, res2);
                }
                var data_string = GlobalUtil.GetString(data);

                string file_ext = "";
                switch (exchangeType)
                {
                    case Enums.ExchangeItemEnum.Move:
                        file_ext = "mov";
                        break;
                    case Enums.ExchangeItemEnum.Ost:
                        file_ext = "ost";
                        break;
                    case Enums.ExchangeItemEnum.Que:
                        file_ext = "que";
                        break;
                    default:
                        file_ext = "txt";
                        break;
                }

                string file_type_name = ((int)exchangeType).EnumName<Enums.ExchangeItemEnum>();

                string file_name = "";
                string file_prefix_for_arc_FTP = "";
                switch ((Enums.DataSourceEnum)userInfo.ds_id)
                {
                    case Enums.DataSourceEnum.MZ:
                        file_name = registration.reg_code.Trim() + "$" + DateTime.Now.ToString("yyyyMMddHHmmss") + "." + file_ext.Trim();
                        file_prefix_for_arc_FTP = "#";
                        //file_prefix_for_arc_FTP = "";
                        break;
                    case Enums.DataSourceEnum.ASNA:
                        string suffix = "";
                        switch ((Enums.ExchangeItemEnum)exchangeType)
                        {
                            case Enums.ExchangeItemEnum.Ost:
                                suffix = "_RST";
                                break;
                            case Enums.ExchangeItemEnum.Avg:
                                suffix = "_AVG";
                                break;
                            default:
                                break;
                        }

                        //file_name = client.client_name.Unidecode() + "_" + (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name) + "_" + DateTime.Now.ToString("yyyyMMddTHHmm") + suffix + ".txt";
                        file_name = (String.IsNullOrEmpty(registration.reg_code) ? "" : registration.reg_code) + "_" + (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name) + "_" + DateTime.Now.ToString("yyyyMMddTHHmm") + suffix + ".txt";                        
                        switch (exchangeType)
                        {
                            case Enums.ExchangeItemEnum.Move:
                                //MovData movData = new MovData(data_string, sales_prefix.HasValue ? sales_prefix.ToString() : "") { ClientName = client.client_name.Unidecode(), RegName = (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name), };
                                MovData movData = new MovData(data_string, sales_prefix.HasValue ? sales_prefix.ToString() : "") { ClientName = (String.IsNullOrEmpty(registration.reg_code) ? "" : registration.reg_code), RegName = (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name), };
                                data_string = movData.ConvertToFormat((Enums.DataSourceEnum)userInfo.ds_id);
                                break;
                            case Enums.ExchangeItemEnum.Ost:
                                //OstData ostData = new OstData(data_string, sales_prefix.HasValue ? sales_prefix.ToString() : "") { ClientName = client.client_name.Unidecode(), RegName = (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name), };
                                OstData ostData = new OstData(data_string, sales_prefix.HasValue ? sales_prefix.ToString() : "") { ClientName = (String.IsNullOrEmpty(registration.reg_code) ? "" : registration.reg_code), RegName = (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name), };
                                data_string = ostData.ConvertToFormat((Enums.DataSourceEnum)userInfo.ds_id);
                                break;
                            case Enums.ExchangeItemEnum.Avg:
                                //AvgData avgData = new AvgData(data_string, sales_prefix.HasValue ? sales_prefix.ToString() : "") { ClientName = client.client_name.Unidecode(), RegName = (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name), };
                                AvgData avgData = new AvgData(data_string, sales_prefix.HasValue ? sales_prefix.ToString() : "") { ClientName = (String.IsNullOrEmpty(registration.reg_code) ? "" : registration.reg_code), RegName = (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name), };
                                data_string = avgData.ConvertToFormat((Enums.DataSourceEnum)userInfo.ds_id);
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        return SendDataToPartner_Error(now, null, "Некорректно задан код партнера userInfo.ds_id = " + userInfo.ds_id.ToString(), null, userInfo, exchangeType, true, 2, exchange_id);
                        //res2 = "Некорректно задан код партнера userInfo.ds_id = " + userInfo.ds_id.ToString();
                        //Loggly.InsertLog_ExchangePartner(res2, (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                        //return new Tuple<bool, string>(res1, res2);
                }

                //string file_path_tmp = @"C:\Release\Exchange\File\Upload\" + file_name;
                string file_path_tmp = dir_path + file_name;
                if (File.Exists(file_path_tmp))
                {
                    File.Delete(file_path_tmp);
                }

                Encoding enc = Encoding.GetEncoding("windows-1251");
                File.WriteAllText(file_path_tmp, data_string, enc);

                string file_ext_arc = "";
                string arc_file_path_tmp = file_path_tmp;
                if (registration.upload_arc_type > 0)
                {
                    switch ((Enums.ArcType)registration.upload_arc_type)
                    {
                        case Enums.ArcType.ZIP:
                            file_ext_arc = "zip";
                            break;
                        case Enums.ArcType.RAR:
                            file_ext_arc = "rar";
                            break;
                        case Enums.ArcType.SevenZ:
                            file_ext_arc = "7z";
                            break;
                        default:
                            file_ext_arc = "zip";
                            break;
                    }

                    arc_file_path_tmp = Path.ChangeExtension(file_path_tmp, file_ext_arc);
                    using (var zip = ZipFile.Open(arc_file_path_tmp, ZipArchiveMode.Create))
                        zip.CreateEntryFromFile(file_path_tmp, file_name);
                    //file_path_tmp = arc_file_path_tmp;
                    file_name = Path.GetFileName(arc_file_path_tmp);
                    data = File.ReadAllBytes(arc_file_path_tmp);
                }

                Tuple<bool, string> send_res = new Tuple<bool, string>(true, "");
                switch ((Enums.ExchangeTransportType)registration.transport_type)
                {
                    case Enums.ExchangeTransportType.FTP:
                        //send_res = xSendDataToPartner_FTP(registration.login, registration.password, HttpUtility.UrlEncode(registration.upload_url + file_prefix_for_arc_FTP) + file_name, (registration.upload_arc_type > 0 ? arc_file_path_tmp : file_path_tmp));
                        string upload_url = registration.upload_url.Trim().EndsWith("/") ? registration.upload_url.Trim() : (registration.upload_url.Trim() + "/");
                        //string upload_url = registration.upload_url;
                        //send_res = xSendDataToPartner_FTP(registration.login, registration.password, HttpUtility.UrlEncode(upload_url + file_prefix_for_arc_FTP) + file_name, (registration.upload_arc_type > 0 ? arc_file_path_tmp : file_path_tmp));
                        send_res = xSendDataToPartner_FTP(registration.login, registration.password, upload_url + HttpUtility.UrlEncode(file_prefix_for_arc_FTP + file_name), (registration.upload_arc_type > 0 ? arc_file_path_tmp : file_path_tmp));
                        if (send_res.Item1)
                        {
                            Loggly.InsertLog_ExchangePartner("На " + upload_url + " выложен файл " + file_name, (long)Enums.LogEsnType.SEND_EXCHANGE_DATA, userInfo.login, exchange_id, now, null);
                        }
                        else
                        {
                            return SendDataToPartner_Error(now, null, "Ошибка при выкладке на " + upload_url + " файла " + file_name + ": " + send_res.Item2, null, userInfo, exchangeType, true, 2, exchange_id);
                            //Loggly.InsertLog_ExchangePartner("Ошибка при выкладке на " + registration.upload_url + " файла " + file_name + ": " + send_res.Item2, (long)Enums.LogEsnType.ERROR, userInfo.login, exchange_id);
                            //return send_res;
                        }
                        break;
                    case Enums.ExchangeTransportType.Email:
                        string target_address = registration.upload_url;
                        string topic = "[" + (String.IsNullOrEmpty(registration.reg_name) ? "_" : registration.reg_name) + "] Передача файла типа " + file_type_name + " [" + file_name + "]";
                        send_res = xSendDataToPartner_Email(target_address, topic, file_name, (registration.upload_arc_type > 0 ? arc_file_path_tmp : file_path_tmp), registration.upload_arc_type > 0, registration, client.client_name.Unidecode());
                        if (send_res.Item1)
                        {
                            Loggly.InsertLog_ExchangePartner("На " + target_address + " отправлен файл " + file_name, (long)Enums.LogEsnType.SEND_EXCHANGE_DATA, userInfo.login, exchange_id, now, null);
                        }
                        else
                        {
                            return SendDataToPartner_Error(now, null, "Ошибка при отправке на " + target_address + " файла " + file_name + ": " + send_res.Item2, null, userInfo, exchangeType, true, 2, exchange_id);
                            //Loggly.InsertLog_ExchangePartner("Ошибка при отправке на " + target_address + " файла " + file_name + ": " + send_res.Item2, (long)Enums.LogEsnType.ERROR, userInfo.login, exchange_id);
                            //return send_res;
                        }
                        break;
                    default:
                        return SendDataToPartner_Error(now, null, "Не указан тип транспорта", null, userInfo, exchangeType);
                        //Loggly.InsertLog_ExchangePartner("Не указан тип транспорта", (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                        //return new Tuple<bool, string>(false, "Не указан тип транспорта");
                }

                return new Tuple<bool, string>(true, "");
            }
        }

        private Tuple<bool, string> SendDataToPartner_Sprav(UserInfo userInfo, ExchangeClient client, ExchangeUser user, ExchangeRegPartner registration, Enums.ExchangeItemEnum exchangeType
            , long exchange_id, int? sales_prefix, string dir_path)
        {
            DateTime now = DateTime.Now;
            var res1 = false;
            var res2 = "";
            using (var context = new AuMainDb())
            {
                var data = context.exchange_data.Where(ss => ss.exchange_id == exchange_id).Select(ss => ss.data_binary).FirstOrDefault();
                if (data == null)
                {
                    return SendDataToPartner_Error(now, null, "Файл для данных с кодом " + exchange_id.ToString() + " не создан", null, userInfo, exchangeType);
                    //res2 = "Файл для данных с кодом " + exchange_id.ToString() + " не создан";
                    //Loggly.InsertLog_ExchangePartner(res2, (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                    //return new Tuple<bool, string>(res1, res2);
                }
                var data_string = GlobalUtil.GetString(data);

                string file_type_name = ((int)exchangeType).EnumName<Enums.ExchangeItemEnum>();
                string file_ext = "";
                string file_name = "";
                string file_path_tmp = "";
                string file_prefix_for_arc_FTP = "";
                int i = 1;
                DbfFile odbf;
                DbfRecord orec;
                switch ((Enums.DataSourceEnum)userInfo.ds_id)
                {
                    case Enums.DataSourceEnum.ASNA:
                        switch (exchangeType)
                        {
                            case Enums.ExchangeItemEnum.Client:
                                file_ext = "vendor";
                                //file_name = client.client_name.Unidecode() + "_" + (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name) + "_vendor_" + DateTime.Now.ToString("yyyyMMddTHHmm") + ".dbf";
                                file_name = "vendor.dbf";
                                ClientData clientData = new ClientData(data_string, sales_prefix.HasValue ? sales_prefix.ToString() : "") { ClientName = client.client_name.Unidecode(), RegName = (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name), };
                                data_string = clientData.ConvertToFormat((Enums.DataSourceEnum)userInfo.ds_id);

                                // make DBF
                                // http://www.cyberforum.ru/csharp-beginners/thread453711.html

                                //var odbf = new DbfFile(Encoding.GetEncoding(1252));
                                //odbf = new DbfFile(Encoding.Default);
                                odbf = new DbfFile(Encoding.GetEncoding(866));                                
                                
                                //file_path_tmp = @"C:\Release\Exchange\File\Upload\" + file_name;
                                file_path_tmp = dir_path + file_name;
                                if (File.Exists(file_path_tmp))
                                {
                                    File.Delete(file_path_tmp);
                                }
                                odbf.Open(file_path_tmp, FileMode.Create);

                                odbf.Header.AddColumn(new DbfColumn("ID", DbfColumn.DbfColumnType.Character, 254, 0));
                                odbf.Header.AddColumn(new DbfColumn("Name", DbfColumn.DbfColumnType.Character, 254, 0));
                                odbf.Header.AddColumn(new DbfColumn("INN", DbfColumn.DbfColumnType.Character, 254, 0));
                                odbf.Header.AddColumn(new DbfColumn("RegionIDN", DbfColumn.DbfColumnType.Number, 20, 0));

                                orec = new DbfRecord(odbf.Header) { AllowDecimalTruncate = true };
                                while (i <= clientData.RowCount)
                                {
                                    orec[0] = clientData.ID[i - 1];
                                    orec[1] = clientData.Name[i - 1];
                                    orec[2] = clientData.INN[i - 1];
                                    orec[3] = clientData.RegionIDN[i - 1];                                    
                                    odbf.Write(orec, true);
                                    i++;
                                }

                                odbf.WriteHeader();
                                odbf.Close();

                                break;
                            case Enums.ExchangeItemEnum.Prep:
                                file_ext = "goods";
                                //file_name = client.client_name.Unidecode() + "_" + (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name) + "_goods_" + DateTime.Now.ToString("yyyyMMddTHHmm") + ".dbf";
                                file_name = "goods.dbf";
                                PrepData prepData = new PrepData(data_string, sales_prefix.HasValue ? sales_prefix.ToString() : "") { ClientName = client.client_name.Unidecode(), RegName = (String.IsNullOrEmpty(registration.reg_name) ? "" : registration.reg_name), };
                                data_string = prepData.ConvertToFormat((Enums.DataSourceEnum)userInfo.ds_id);

                                // make DBF
                                // http://www.cyberforum.ru/csharp-beginners/thread453711.html

                                //var odbf = new DbfFile(Encoding.GetEncoding(1252));
                                //odbf = new DbfFile(Encoding.Default);
                                odbf = new DbfFile(Encoding.GetEncoding(866));

                                //file_path_tmp = @"C:\Release\Exchange\File\Upload\" + file_name;
                                file_path_tmp = dir_path + file_name;
                                if (File.Exists(file_path_tmp))
                                {
                                    File.Delete(file_path_tmp);
                                }
                                odbf.Open(file_path_tmp, FileMode.Create);

                                odbf.Header.AddColumn(new DbfColumn("ID", DbfColumn.DbfColumnType.Character, 254, 0));
                                odbf.Header.AddColumn(new DbfColumn("Name", DbfColumn.DbfColumnType.Character, 254, 0));
                                odbf.Header.AddColumn(new DbfColumn("Producer", DbfColumn.DbfColumnType.Character, 254, 0));
                                odbf.Header.AddColumn(new DbfColumn("Country", DbfColumn.DbfColumnType.Character, 254, 0));
                                odbf.Header.AddColumn(new DbfColumn("EAN", DbfColumn.DbfColumnType.Character, 254, 0));

                                orec = new DbfRecord(odbf.Header) { AllowDecimalTruncate = true, };
                                while (i <= prepData.RowCount)
                                {
                                    orec[0] = prepData.ID[i - 1];
                                    orec[1] = prepData.Name[i - 1];
                                    orec[2] = prepData.Producer[i - 1];
                                    orec[3] = prepData.Country[i - 1];
                                    orec[4] = prepData.EAN[i - 1];
                                    odbf.Write(orec, true);
                                    i++;
                                }

                                odbf.WriteHeader();
                                odbf.Close();
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        return SendDataToPartner_Error(now, null, "Некорректно задан код партнера userInfo.ds_id = " + userInfo.ds_id.ToString(), null, userInfo, exchangeType);
                        //res2 = "Некорректно задан код партнера userInfo.ds_id = " + userInfo.ds_id.ToString();
                        //Loggly.InsertLog_ExchangePartner(res2, (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                        //return new Tuple<bool, string>(res1, res2);
                }


                // !!!
                // чтоб была нужная кодировка
                byte wrbyte = 101;
                BinaryWriter bwrite = new BinaryWriter(File.Open(file_path_tmp, FileMode.Open));
                bwrite.Seek(29, SeekOrigin.Begin);
                bwrite.Write(wrbyte);
                bwrite.Close();

                data = File.ReadAllBytes(file_path_tmp);

                string file_ext_arc = "";
                string arc_file_path_tmp = file_path_tmp;
                if (registration.upload_arc_type > 0)
                {
                    switch ((Enums.ArcType)registration.upload_arc_type)
                    {
                        case Enums.ArcType.ZIP:
                            file_ext_arc = "zip";
                            break;
                        case Enums.ArcType.RAR:
                            file_ext_arc = "rar";
                            break;
                        case Enums.ArcType.SevenZ:
                            file_ext_arc = "7z";
                            break;
                        default:
                            file_ext_arc = "zip";
                            break;
                    }

                    arc_file_path_tmp = Path.ChangeExtension(file_path_tmp, file_ext_arc);
                    using (var zip = ZipFile.Open(arc_file_path_tmp, ZipArchiveMode.Create))
                        zip.CreateEntryFromFile(file_path_tmp, file_name);
                    //file_path_tmp = arc_file_path_tmp;
                    file_name = Path.GetFileName(arc_file_path_tmp);
                    data = File.ReadAllBytes(arc_file_path_tmp);
                }

                Tuple<bool, string> send_res = new Tuple<bool, string>(true, "");
                switch ((Enums.ExchangeTransportType)registration.transport_type)
                {
                    case Enums.ExchangeTransportType.FTP:
                        //send_res = xSendDataToPartner_FTP(registration.login, registration.password, HttpUtility.UrlEncode(registration.upload_url + file_prefix_for_arc_FTP) + file_name, (registration.upload_arc_type > 0 ? arc_file_path_tmp : file_path_tmp));
                        //send_res = xSendDataToPartner_FTP(registration.login, registration.password, HttpUtility.UrlEncode(file_prefix_for_arc_FTP + file_name), (registration.upload_arc_type > 0 ? arc_file_path_tmp : file_path_tmp));
                        string upload_url = registration.upload_url.Trim().EndsWith("/") ? registration.upload_url.Trim() : (registration.upload_url.Trim() + "/");
                        //string upload_url = registration.upload_url;
                        send_res = xSendDataToPartner_FTP(registration.login, registration.password, upload_url + HttpUtility.UrlEncode(file_prefix_for_arc_FTP + file_name), (registration.upload_arc_type > 0 ? arc_file_path_tmp : file_path_tmp));                        
                        if (send_res.Item1)
                        {
                            Loggly.InsertLog_ExchangePartner("На " + upload_url + " выложен файл " + file_name, (long)Enums.LogEsnType.SEND_EXCHANGE_DATA, userInfo.login, exchange_id, now, null);
                        }
                        else
                        {
                            return SendDataToPartner_Error(now, null, "Ошибка при выкладке на " + upload_url + " файла " + file_name + ": " + send_res.Item2, null, userInfo, exchangeType, true, 2, exchange_id);
                            //Loggly.InsertLog_ExchangePartner("Ошибка при выкладке на " + registration.upload_url + " файла " + file_name + ": " + send_res.Item2, (long)Enums.LogEsnType.ERROR, userInfo.login, exchange_id);
                            //return send_res;
                        }                        
                        break;
                    case Enums.ExchangeTransportType.Email:
                        string target_address = registration.upload_url;
                        string topic = "[" + (String.IsNullOrEmpty(registration.reg_name) ? "_" : registration.reg_name) + "] Передача файла типа " + file_type_name + " [" + file_name + "]";
                        send_res = xSendDataToPartner_Email(target_address, topic, file_name, (registration.upload_arc_type > 0 ? arc_file_path_tmp : file_path_tmp), registration.upload_arc_type > 0, registration, client.client_name.Unidecode());
                        if (send_res.Item1)
                        {
                            Loggly.InsertLog_ExchangePartner("На " + target_address + " отправлен файл " + file_name, (long)Enums.LogEsnType.SEND_EXCHANGE_DATA, userInfo.login, exchange_id, now, null);
                        }
                        else
                        {
                            return SendDataToPartner_Error(now, null, "Ошибка при отправке на " + target_address + " файла " + file_name + ": " + send_res.Item2, null, userInfo, exchangeType, true, 2, exchange_id);
                            //Loggly.InsertLog_ExchangePartner("Ошибка при отправке на " + target_address + " файла " + file_name + ": " + send_res.Item2, (long)Enums.LogEsnType.ERROR, userInfo.login, exchange_id);
                            //return send_res;
                        }
                        break;
                    default:
                        return SendDataToPartner_Error(now, null, "Не указан тип транспорта", null, userInfo, exchangeType);
                        //Loggly.InsertLog_ExchangePartner("Не указан тип транспорта", (long)Enums.LogEsnType.ERROR, userInfo.login, null);
                        //return new Tuple<bool, string>(false, "Не указан тип транспорта");
                }

                return new Tuple<bool, string>(true, "");
            }
        }

        private Tuple<bool, string> xSendDataToPartner_FTP(string ftpUsername, string ftpPassword, string target_address, string source_file_path)
        {
            try
            {
                using (WebClient client = new WebClient())
                {                    
                    client.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                    client.UploadFile(target_address, "STOR", source_file_path);
                }
                return new Tuple<bool, string>(true, "");
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, "[" + target_address + "] [" + source_file_path + "] " + GlobalUtil.ExceptionInfo(ex));
            }
        }

        private Tuple<bool, string> xSendDataToPartner_Email(string target_address, string topic, string attachment_name, string attachment_path, bool is_arc, ExchangeRegPartner registration, string client_name)
        {
            try
            {
                string sender_smtp_address = "smtp.gmail.com";
                int sender_smtp_port = 587;
                bool sender_smtp_ssl = true;
                string sender_address = "cab.aptekaural@gmail.com";
                string sender_login = "cab.aptekaural@gmail.com";
                //string sender_alias = "cab.aptekaural";
                string sender_alias = registration.upload_url_alias;
                if (String.IsNullOrEmpty(sender_alias))
                    sender_alias = "cab.aptekaural";
                string sender_password = "aptekaural";

                /*
                string sender_smtp_address = "smtp.gmail.com";
                int sender_smtp_port = 587;
                bool sender_smtp_ssl = true;
                string sender_address = "cab.aptekaural@gmail.com";
                string sender_login = "cab.aptekaural@gmail.com";
                string sender_alias = "cab.aptekaural";
                string sender_password = "aptekaural";
                */

                /*
                string sender_smtp_address = "smtp.yandex.ru";
                int sender_smtp_port = 25;
                bool sender_smtp_ssl = true;
                string sender_address = "cab.aptekaural@yandex.ru";
                string sender_login = "cab.aptekaural@yandex.ru";
                string sender_alias = "cab.aptekaural";
                string sender_password = "aptekaural";
                */
                DateTime now = DateTime.Now;
                using (SmtpClient c = new SmtpClient(sender_smtp_address, sender_smtp_port))
                {
                    MailAddress add = new MailAddress(target_address);
                    MailMessage msg = new MailMessage();
                    msg.To.Add(add);
                    msg.From = new MailAddress(sender_address, sender_alias);
                    msg.IsBodyHtml = true;
                    msg.Subject = topic;
                    msg.Body = "";

                    var zipCt = new ContentType { MediaType = (is_arc ? MediaTypeNames.Application.Zip : MediaTypeNames.Application.Octet), };
                    var attachmentA = new Attachment(attachment_path, zipCt);
                    attachmentA.ContentDisposition.FileName = attachment_name;
                    attachmentA.Name = attachment_name;
                    msg.Attachments.Add(attachmentA);

                    c.Credentials = new System.Net.NetworkCredential(sender_login, sender_password);
                    c.EnableSsl = sender_smtp_ssl;
                    c.Send(msg);
                }

                // если указан адрес для подтверждения - высылаем туда подтверждение
                if (!String.IsNullOrEmpty(registration.upload_confirm_address))
                {
                    using (SmtpClient c = new SmtpClient(sender_smtp_address, sender_smtp_port))
                    {
                        MailAddress add = new MailAddress(registration.upload_confirm_address);
                        MailMessage msg = new MailMessage();
                        msg.To.Add(add);
                        msg.From = new MailAddress(sender_address, sender_alias);
                        msg.IsBodyHtml = false;
                        msg.Subject = "Уведомления о выгрузке файлов";
                        msg.Body = "Добрый день! Выгружены файлы от "
                            + (String.IsNullOrEmpty(client_name) ? "_" : client_name)
                            + ", "
                            + (String.IsNullOrEmpty(registration.reg_name) ? "_" : registration.reg_name)
                            ;

                        c.Credentials = new System.Net.NetworkCredential(sender_login, sender_password);
                        c.EnableSsl = sender_smtp_ssl;
                        c.Send(msg);
                    }
                }

                return new Tuple<bool, string>(true, "");
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, GlobalUtil.ExceptionInfo(ex));
            }
        }

        private ServiceOperationResult xSendData2(UserInfo userInfo, string data, DataInfo dataInfo)
        {
            /*
            return new ServiceOperationResult()
            {
                error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован"),
                Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION,
            };
            */

            // !!!
            // todo
            return xSendData(userInfo, data, (Enums.ExchangeItemEnum)dataInfo.item_type_id);
        }

        #endregion

        #region GetExchangeData

        private ExchangeData GetExchangeData_Error(DateTime date_beg, ErrInfo err, string err_str, long err_code, UserInfo userInfo, long? exchange_id = null)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";            
            string mess = err != null ? err.Message : err_str;
            Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, "GetExchangeData");

            return new ExchangeData()
            {
                error = err != null ? err : new ErrInfo(err_code, err_str),
                data_binary = null,
                data_text = null,                
            }
            ;
        }

        private ExchangeData xGetExchangeData(UserInfo userInfo, long exchange_id)
        {
            DateTime now = DateTime.Now;
            
            /*
             проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            {
                return GetExchangeData_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, userInfo, exchange_id);
            }

            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
            {
                return GetExchangeData_Error(now, user.error, "Не найден пользователь " + userInfo.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, exchange_id);
            }

            long ds_id = userInfo.ds_id;
            if (ds_id == 0)
                ds_id = GetDefaultDsId();
            var reg = GetRegistration_Partner(user.user_id, ds_id);
            if ((reg == null) || (reg.error != null))
            {
                return GetExchangeData_Error(now, reg.error, "Не найдена регистрации пользователя " + userInfo.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, exchange_id);
            }

            using (var context = new AuMainDb())
            {
                var exch = context.exchange.Where(ss => ss.ds_id == ds_id && ss.user_id == user.user_id && ss.exchange_id == exchange_id).FirstOrDefault();
                if (exch == null)
                {
                    return GetExchangeData_Error(now, null, "Не найдены данные обмена для заданных параметров", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, exchange_id);
                }
                var exch_data = context.exchange_data.Where(ss => ss.exchange_id == exchange_id).FirstOrDefault();
                if (exch_data == null)
                {
                    return GetExchangeData_Error(now, null, "Не найдено содержимое файла обмена для заданных параметров", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, exchange_id);
                }

                return new ExchangeData(exch_data);
            }
        }

        #endregion

        #region GetSendAll

        private int xGetSendAll(UserInfo userInfo)
        {
            if (userInfo == null)
                return (int)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND;

            string login = userInfo.login;
            string workplace = userInfo.workplace;
            long ds_id = userInfo.ds_id;

            if (String.IsNullOrEmpty(login))
                return (int)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND;

            if (String.IsNullOrEmpty(workplace))
                return (int)Enums.ErrCodeEnum.ERR_CODE_WORKPLACE_NOT_FOUND;

            if (ds_id <= 100)
                return (int)Enums.ErrCodeEnum.ERR_CODE_PARTNER_NOT_FOUND;

            using (var context = new AuMainDb())
            {
                var exchange_reg_partner = (from eu in context.vw_exchange_user
                                            from er in context.exchange_reg
                                            from eur in context.exchange_user_reg
                                            from erp in context.exchange_reg_partner
                                            where eu.user_id == eur.user_id
                                            && eu.login.Trim().ToLower().Equals(login.Trim().ToLower())
                                            && er.reg_type == (int)Enums.ExchangeRegType.PARTNER
                                            && er.reg_id == erp.reg_id
                                            && erp.ds_id == ds_id
                                            && er.reg_id == eur.reg_id
                                            && eur.is_active == true
                                            && eu.workplace.Trim().ToLower().Equals(workplace.Trim().ToLower())
                                            select erp)
                          .FirstOrDefault()
                          ;

                if (exchange_reg_partner == null)
                    return (int)Enums.ErrCodeEnum.ERR_CODE_REG_PARTNER_NOT_FOUND;

                return exchange_reg_partner.send_all;
            }
        }

        #endregion

        #region GetParams

        private ExchangeParamsList GetParams_Error(DateTime date_beg, ErrInfo err, string err_str, long err_code, UserInfo userInfo)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";
            string mess = err != null ? err.Message : err_str;
            Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, "GetParams");

            return new ExchangeParamsList()
            {
                error = err != null ? err : new ErrInfo(err_code, err_str),
            }
            ;
        }

        private ExchangeParamsList xGetParams(UserInfo userInfo)
        {
            DateTime now = DateTime.Now;

            if (userInfo == null)
            {
                return GetParams_Error(now, null, "Не задан пользователь", (long)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND, userInfo);
            }
            //return new ExchangeParamsList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND, "Не найден пользователь"), };

            string login = userInfo.login;
            string workplace = userInfo.workplace;
            long ds_id = userInfo.ds_id;
            bool ds_id_specified = ds_id > 100;
            List<long> ds_id_list = new List<long>();

            if (String.IsNullOrEmpty(login))
            {
                return GetParams_Error(now, null, "Не задан логин", (long)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND, userInfo);
            }
            //return new ExchangeParamsList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_LOGIN_NOT_FOUND, "Не найден логин"), };

            if (String.IsNullOrEmpty(workplace))
            {
                return GetParams_Error(now, null, "Не задано рабочее место", (long)Enums.ErrCodeEnum.ERR_CODE_WORKPLACE_NOT_FOUND, userInfo);
            }
            //return new ExchangeParamsList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_WORKPLACE_NOT_FOUND, "Не найдено рабочее место логин"), };

            /*
             проверка лицензии
            */

            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            /*
            {
                return new ExchangeParamsList() { error = lic.error, };                
            }
            */
            {
                return GetParams_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, userInfo);                
            }

            using (var context = new AuMainDb())
            {
                if (ds_id_specified)
                {
                    ds_id_list.Add(ds_id);
                }

                var exchange_reg_partner = (from eu in context.vw_exchange_user
                                            from er in context.exchange_reg
                                            from eur in context.exchange_user_reg
                                            from erp in context.exchange_reg_partner
                                            from ds in context.datasource
                                            where eu.user_id == eur.user_id
                                            && eu.login.Trim().ToLower().Equals(login.Trim().ToLower())
                                            && er.reg_type == (int)Enums.ExchangeRegType.PARTNER
                                            && er.reg_id == erp.reg_id
                                            && erp.ds_id == ds.ds_id
                                                //&& erp.ds_id == ds_id
                                            && erp.ds_id.HasValue && ((ds_id_list.Contains((long)erp.ds_id)) || (ds_id_list.Count <= 0))
                                            && er.reg_id == eur.reg_id
                                            && eur.is_active == true
                                            && eu.workplace.Trim().ToLower().Equals(workplace.Trim().ToLower())
                                            //select erp)
                                            select new { erp.ds_id, ds.ds_name, erp.send_all, erp.days_cnt_for_mov, erp.reg_code, erp.reg_name })
                          .ToList();
                ;

                if ((exchange_reg_partner == null) || (exchange_reg_partner.Count <= 0))
                {
                    return GetParams_Error(now, null, "Не найдена регистрация пользователя", (long)Enums.ErrCodeEnum.ERR_CODE_REG_PARTNER_NOT_FOUND, userInfo);
                }
                //return new ExchangeParamsList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_REG_PARTNER_NOT_FOUND, "Не найдена регистрация пользователя"), };

                ExchangeParamsList res = new ExchangeParamsList();
                foreach (var ds in exchange_reg_partner.Where(ss => ss.ds_id.HasValue))
                {
                    var items = context.datasource_exchange_item.Where(ss => ss.ds_id == ds.ds_id && ((ss.for_get) || (ss.for_send))).ToList();
                    res.ExchangeParams_list.Add(new ExchangeParams() 
                    { 
                        ds_id = (long)ds.ds_id, 
                        ds_name = ds.ds_name, 
                        send_all = ds.send_all, 
                        days_cnt_for_mov = ds.days_cnt_for_mov,
                        exchange_item_list = items == null ? new ExchangeItemList() : new ExchangeItemList(items),
                        login = userInfo.login,
                        workplace = userInfo.workplace,
                        reg_code = ds.reg_code,
                        reg_name = ds.reg_name,
                        sales_prefix = lic.SalesPrefix.GetValueOrDefault(0),
                    });
                }

                return res;
            }
        }

        private ExchangeParamsList xGetParams_SalePoint(UserInfo userInfo, int sale_id)
        {
            /*
                Сначала получаем список всех workplace_id заданного sale_id, потом для каждого workplace вызываем xGetParams            
             */

            ExchangeParamsList res = new ExchangeParamsList() { error = null, ExchangeParams_list = new List<ExchangeParams>(), };
            MySqlConnection conn = null;
            MySqlCommand cmd;
            MySqlDataReader rdr;
            string sql = "";
            string mySqlConnectionString = ExchangeUtil.GetCabinetDbConnectionString();
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = mySqlConnectionString;
                conn.Open();

                sql = "select t1.id as workplace_id, t1.registration_key, t3.name"
                            + " from clients.workplace t1"
                            + " inner join clients.sales t2 on t1.sales_id = t2.id"
                            //+ " left join clients.my_aspnet_users t3 on t2.user_id = t3.id"
                            + " left join clients.my_aspnet_Users t3 on t2.user_id = t3.id"
                            + " where t1.sales_id = " + sale_id.ToString();

                cmd = new MySqlCommand(sql, conn);
                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    var field1 = rdr[0];
                    var field2 = rdr[1];
                    var field3 = rdr[2];
                    int workplace_id = Convert.ToInt32(field1);
                    string registration_key = Convert.ToString(field2);
                    string account = Convert.ToString(field3);

                    //UserInfo userInfo2 = new UserInfo() { ds_id = userInfo.ds_id, login = userInfo.login, version_num = userInfo.version_num, workplace = registration_key, };
                    UserInfo userInfo2 = new UserInfo() { ds_id = userInfo.ds_id, login = account, version_num = userInfo.version_num, workplace = registration_key, };
                    var res1 = xGetParams(userInfo2);
                    if ((res1.error == null) && (res1.ExchangeParams_list != null) && (res1.ExchangeParams_list.Count > 0))
                    {
                        res.ExchangeParams_list.AddRange(res1.ExchangeParams_list);
                    }
                    else
                    {
                        res.ExchangeParams_list.Add(new ExchangeParams() 
                        {                             
                            error = res1.error,
                            ds_id = userInfo2.ds_id,
                            login = userInfo2.login,
                            workplace = userInfo2.workplace,
                        });
                    }
                }
                rdr.Close();
                conn.Close();

                return res;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                if (conn != null)
                    conn.Close();
                return new ExchangeParamsList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Ошибка SQL соединения: " + ex.Message) };
            }
        }

        #endregion

        #region GetPartnerList

        private PartnerList xGetPartnerList()
        {
            using (var context = new AuMainDb())
            {
                return new PartnerList(context.datasource.Where(ss => ss.ds_id > 100).OrderBy(ss => ss.ds_id).ToList());
            }
        }

        #endregion

        #region SendToExchangeLog

        private ServiceOperationResult SendToExchangeLog_Error(DateTime date_beg, ErrInfo err, string err_str, long err_code
            , UserInfo userInfo, int exchangeDirection, int exchangeType, int id)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";
            long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
            string mess = err != null ? err.Message : err_str;
            int error_level = 2;
            Loggly.InsertExchangeLog(mess, null, user_name, date_beg
                , exchangeDirection, exchangeType, ds_id, null, error_level, id.ToString());

            return new ServiceOperationResult()
            {
                error = err != null ? err : new ErrInfo(err_code, err_str),
                Id = err != null ? (err.Id.HasValue ? (long)err.Id : err_code) : err_code,
            }
            ;
        }

        private ServiceOperationResult xSendToExchangeLog(UserInfo userInfo, string text, int id, int is_error, int exchange_direction, int exchange_type)
        {
            DateTime now = DateTime.Now;

            /*
             проверка лицензии
            */

            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            {
                return SendToExchangeLog_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, userInfo, exchange_direction, exchange_type, id);
            }

            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
            {
                return SendToExchangeLog_Error(now, user.error, "Не найден пользователь " + user.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, exchange_direction, exchange_type, id);
            }

            var client = GetClient(user.cient_id.GetValueOrDefault(0));
            if ((client == null) || (client.error != null))
            {
                return SendToExchangeLog_Error(now, client.error, "Не найден клиент - участник обмена с кодом " + user.cient_id.GetValueOrDefault(0).ToString(), (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, exchange_direction, exchange_type, id);
            }

            long ds_id = userInfo.ds_id;
            if (ds_id == 0)
                ds_id = GetDefaultDsId();
            var reg = GetRegistration_Partner(user.user_id, ds_id);

            if ((reg == null) || (reg.error != null))
            {
                return SendToExchangeLog_Error(now, reg.error, "Не найдена регистрация пользователя", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, exchange_direction, exchange_type, id);
            }

            Loggly.InsertLog_ExchangePartner(text, (long)Enums.LogEsnType.UDPATE_EXCHANGE_DATA, userInfo.login, null, now, "SendToExchangeLog");
            Loggly.InsertExchangeLog(text, reg.reg_id, userInfo.login, now, exchange_direction, exchange_type, ds_id, null, (is_error == 1 ? 2 : 0), id.ToString());

            return new ServiceOperationResult()
            { 
                Id = 0,
                error = null,
            };
        }

        #endregion

        #region UpdateExchangePartnerReg

        private ServiceOperationResult UpdateExchangePartnerReg_Error(DateTime date_beg, ErrInfo err, string err_str, long err_code, long? ds_id
            , UserInfo userInfo, Enums.ExchangeItemEnum exchangeType, long? reg_id, string mess2, long? exchange_id = null)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";
            //long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
            long? ds_id_actual = ds_id == null ? (userInfo != null ? (long?)userInfo.ds_id : null) : ds_id;
            string mess = err != null ? err.Message : err_str;
            int error_level = 2;
            Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, mess2);
            Loggly.InsertExchangeLog(mess, reg_id, user_name, date_beg
                , (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER, (int)exchangeType, ds_id_actual, exchange_id, error_level, mess2);

            return new ServiceOperationResult()
            {
                error = err != null ? err : new ErrInfo(err_code, err_str),
                Id = err != null ? (err.Id.HasValue ? (long)err.Id : err_code) : err_code,
            }
            ;
        }

        private ServiceOperationResult xUpdateExchangePartnerReg(string password, UserInfo userInfo, bool update_upload, bool update_download)
        {
            // !!!
            /*
            return new ServiceOperationResult()
            {
                Id = 0,
                error = null,
            }             
            ;
            */
            
            DateTime log_date_beg = DateTime.Now;
            try
            {
                return xxUpdateExchangePartnerReg(password, userInfo, update_upload, update_download);
            }
            catch (Exception e)
            {
                string mess = "Ошибка в UpdateExchangePartnerReg: " + GlobalUtil.ExceptionInfo(e);
                string mess2 = "UpdateExchangePartnerReg";
                string user_name = userInfo != null ? (!String.IsNullOrEmpty((userInfo.login)) ? userInfo.login : "exchanger") : "exchanger";
                long? ds_id = userInfo != null ? (long?)userInfo.ds_id : null;
                Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, log_date_beg, mess2);
                Loggly.InsertExchangeLog(mess, null, user_name, log_date_beg, (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT, (int)Enums.ExchangeItemEnum.Plan
                    , ds_id, null, 2, mess2);                
                return CreateErrExchangeBase<ServiceOperationResult>(e);
            }            
        }

        private ServiceOperationResult xxUpdateExchangePartnerReg(string password, UserInfo userInfo, bool update_upload, bool update_download)
        {
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            ServiceOperationResult res = new ServiceOperationResult()
            {
                Id = 0,
                error = null,
            };

            if (userInfo == null)
                return UpdateExchangePartnerReg_Error(now, null, "Не задана структура userInfo", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, null, userInfo, Enums.ExchangeItemEnum.None, null, "UpdateExchangePartnerReg");

            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
                return UpdateExchangePartnerReg_Error(now, null, "Не найден пользователь с логином " + userInfo.login + " и рабочим местом " + userInfo.workplace, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, null, userInfo, Enums.ExchangeItemEnum.None, null, "UpdateExchangePartnerReg");

            long ds_id = userInfo.ds_id;
            if (ds_id == 0)
                ds_id = GetDefaultDsId();

            var reg = GetRegistration_Partner(user.user_id, ds_id);
            if ((reg == null) || (reg.error != null))
                return UpdateExchangePartnerReg_Error(now, null, "Не найдена регистрация для пользователя с логином " + userInfo.login + " и рабочим местом " + userInfo.workplace, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, ds_id, userInfo, Enums.ExchangeItemEnum.None, null, "UpdateExchangePartnerReg");

            return xxxUpdateExchangePartnerReg(password, userInfo, reg, update_upload, update_download);
        }

        private ServiceOperationResult xxxUpdateExchangePartnerReg(string password, UserInfo userInfo, ExchangeRegPartner reg, bool update_upload, bool update_download)
        {
            DateTime now = DateTime.Now;
            DateTime today = DateTime.Today;

            long reg_id = reg.reg_id;
            long ds_id = reg.ds_id.HasValue ? (long)reg.ds_id : 0;
            if (ds_id == 0)
                ds_id = GetDefaultDsId();

            ServiceOperationResult res = new ServiceOperationResult()
            {
                Id = 0,
                error = null,
            };

            if (password != "iguana")
                return UpdateExchangePartnerReg_Error(now, null, "Нет прав", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, ds_id, userInfo, Enums.ExchangeItemEnum.None, reg_id, "UpdateExchangePartnerReg");
            
            vw_exchange_user user = null;
            exchange_reg_partner exchange_reg_partner = null;    
            string mess = "Результат обновления. ";

            DateTime? upload_last_ok_date = reg.upload_ok_date;
            DateTime? upload_last_date = null;
            DateTime upload_end_wait_date = today;
            DateTime? upload_ok_date = null;
            int upload_days_cnt_for_wait = 0;
            bool upload_last_date_not_found = false;
            bool upload_end_wait_ended = false;
            int upload_error_level_max = 0;
            string upload_err_mess = "";
            string upload_last_mess = "";
            int upload_ok_cnt_new = 0;
            int upload_warn_cnt_new = 0;
            int upload_err_cnt_new = 0;

            List<exchange_reg_partner_item> upload_exchange_items = null;
            List<exchange_log> upload_result_log_list = new List<exchange_log>();

            DateTime? download_last_ok_date = reg.download_ok_date;
            DateTime? download_last_date = null;
            DateTime download_end_wait_date = today;
            DateTime? download_ok_date = null;
            int download_days_cnt_for_wait = 0;
            bool download_last_date_not_found = false;
            bool download_end_wait_ended = false;
            int download_error_level_max = 0;
            string download_err_mess = "";
            string download_last_mess = "";
            int download_ok_cnt_new = 0;
            int download_warn_cnt_new = 0;
            int download_err_cnt_new = 0;

            List<exchange_reg_partner_item> download_exchange_items = null;
            List<exchange_log> download_result_log_list = new List<exchange_log>();

            using (var context = new AuMainDb())
            {
                user = (from t1 in context.vw_exchange_user
                        from t2 in context.exchange_user_reg
                        where t1.user_id == t2.user_id
                        && t2.reg_id == reg_id
                        select t1)
                       .FirstOrDefault();
                if (user == null)                    
                    return UpdateExchangePartnerReg_Error(now, null, "Не найден пользователь у регистрации с кодом " + reg_id.ToString(), (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, ds_id, userInfo, Enums.ExchangeItemEnum.None, reg_id, "UpdateExchangePartnerReg");

                exchange_reg_partner = context.exchange_reg_partner.Where(ss => ss.reg_id == reg_id).FirstOrDefault();
                if (exchange_reg_partner == null)
                    return UpdateExchangePartnerReg_Error(now, null, "Не найдена регистрация с кодом " + reg_id.ToString(), (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, ds_id, userInfo, Enums.ExchangeItemEnum.None, reg_id, "UpdateExchangePartnerReg");

                if (update_upload)
                {

                    // upload - отправка От Клиента Партнеру                

                    upload_exchange_items = (from t1 in context.exchange_reg_partner_item
                                             from t2 in context.exchange_item
                                             from t3 in context.datasource_exchange_item
                                             where t1.exchange_item_id == t2.item_id
                                             && t2.item_id == t3.item_id
                                             && t1.reg_id == reg_id
                                             && t3.ds_id == ds_id
                                             && t3.for_send
                                             select t1
                                             )
                                             .Distinct()
                                             .ToList();

                    if ((upload_exchange_items == null) || (upload_exchange_items.Count <= 0))
                        //return res;
                        return UpdateExchangePartnerReg_Error(now, null, "В регистрации с кодом " + reg_id.ToString() + " не найден список файлов для отправки", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, ds_id, userInfo, Enums.ExchangeItemEnum.None, reg_id, "UpdateExchangePartnerReg");

                    upload_result_log_list.Clear();
                    foreach (var item in upload_exchange_items)
                    {

                        // сколько дней можно ждать отправку
                        upload_days_cnt_for_wait = item.days_cnt_for_wait;

                        // дата последней удачной отправки
                        upload_last_date = context.exchange_log.Where(ss => ss.reg_id == reg_id
                            && ss.error_level == 0
                            && ss.ds_id == ds_id
                            && ss.exchange_direction == (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER
                            && ss.exchange_item_id == item.exchange_item_id)
                            .OrderByDescending(ss => ss.date_beg)
                            .Select(ss => ss.date_beg)
                            .FirstOrDefault();
                        if (upload_last_date == null)
                        {
                            // удачная отправка не найдена
                            upload_last_date_not_found = true;
                            upload_last_date = reg.prepared_date;
                        }
                        if (upload_last_date == null)
                            continue;

                        // дата когда заканчивается ожидание
                        upload_end_wait_date = ((DateTime)upload_last_date).AddDays(upload_days_cnt_for_wait);                                                    
                        //if (upload_end_wait_date.Date <= today)
                            //continue;
                        
                        //upload_end_wait_ended = !(upload_end_wait_date.Date <= today);
                        upload_end_wait_ended = upload_end_wait_date.Date < today;

                        //if ((upload_last_date_not_found) || (upload_end_wait_ended))
                        if (upload_end_wait_ended)
                        {
                            upload_result_log_list.Add(new exchange_log()
                            {
                                date_beg = now,
                                date_end = now,
                                ds_id = ds_id,
                                error_level = item.is_required ? 2 : 1,
                                exchange_direction = (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER,
                                exchange_item_id = item.exchange_item_id,
                                login = user.login,
                                mess = "Закончился период ожидания отправки файла " + item.exchange_item_id.EnumName<Enums.ExchangeItemEnum>() + " [дни: " + upload_days_cnt_for_wait.ToString() + "]",
                                reg_id = reg_id,
                            }
                            );
                        }
                        else
                        {
                            upload_result_log_list.Add(new exchange_log()
                            {
                                date_beg = now,
                                date_end = now,
                                ds_id = ds_id,
                                error_level = 0,
                                exchange_direction = (int)Enums.ExchangeDirectionEnum.FROM_CLIENT_TO_PARTNER,
                                exchange_item_id = item.exchange_item_id,
                                login = user.login,
                                mess = "Отправлен файл " + item.exchange_item_id.EnumName<Enums.ExchangeItemEnum>(),
                                reg_id = reg_id,
                            }
                        );
                        }

                    }

                    /*
                        bool stateOK = true
                        - были проведены обмены всеми обязательными и необязательными файлами в указанный срок                        

                        bool stateWarn = true
                        - были проведены обмены всеми обязательными файлами в указанный срок, но:
                        - в указанный срок не было хотя бы одного необязательного файла                        

                        bool stateError = true
                        - в указанный срок не было хотя бы одного обязательного файла
                    */


                    // "2016-03-18 16:46:23.623742"
                    if ((upload_result_log_list != null) && (upload_result_log_list.Count > 0))
                    {
                        upload_error_level_max = upload_result_log_list.Select(ss => ss.error_level).Max();
                        var err_mess_item = upload_result_log_list.Where(ss => ss.error_level == 2).OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                        upload_err_mess = err_mess_item != null ? err_mess_item.mess : "";
                        var last_date_item = upload_result_log_list.OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                        upload_last_date = last_date_item != null ? last_date_item.date_beg : exchange_reg_partner.upload_last_date;
                        upload_ok_date = err_mess_item != null ? null : upload_last_date;
                        var last_item = upload_result_log_list.OrderByDescending(ss => ss.error_level).ThenByDescending(ss => ss.date_beg).FirstOrDefault();
                        upload_last_mess = last_item != null ? last_item.mess : "";
                        upload_ok_cnt_new = upload_result_log_list.Where(ss => ss.error_level == 0).Count();
                        upload_warn_cnt_new = upload_result_log_list.Where(ss => ss.error_level == 1).Count();
                        upload_err_cnt_new = upload_result_log_list.Where(ss => ss.error_level == 2).Count();
                    }
                    else
                    {
                        upload_error_level_max = 0;                        
                        upload_err_mess = exchange_reg_partner.upload_err_mess;
                        upload_last_date = exchange_reg_partner.upload_last_date;
                        upload_ok_date = exchange_reg_partner.upload_ok_date;                        
                        upload_last_mess = exchange_reg_partner.upload_last_mess;
                        upload_ok_cnt_new = 0;
                        upload_warn_cnt_new = 0;
                        upload_err_cnt_new = 0;
                    }

                    mess += " Отправка: удачная отправка не найдена: " + upload_last_date_not_found.ToString()
                        + ", дата окончания ожидания: " + upload_end_wait_date.ToString("dd.MM.yyyy HH:mm:ss")
                        + ", статус: " + upload_error_level_max.ToString() 
                        + " [" + exchange_reg_partner.upload_state.ToString() + "]"
                        + ", дата: " + (upload_last_date.HasValue ? ((DateTime)upload_last_date).ToString("dd.MM.yyyy HH:mm:ss") : "-")
                        + " [" + (exchange_reg_partner.upload_last_date.HasValue ? ((DateTime)exchange_reg_partner.upload_last_date).ToString("dd.MM.yyyy HH:mm:ss") : "-") + "]"
                        + ", дата успеха: " + (upload_ok_date.HasValue ? ((DateTime)upload_ok_date).ToString("dd.MM.yyyy HH:mm:ss") : "-")
                        + " [" + (exchange_reg_partner.upload_ok_date.HasValue ? ((DateTime)exchange_reg_partner.upload_ok_date).ToString("dd.MM.yyyy HH:mm:ss") : "-") + "]"
                        + ", успешных строк: " + (exchange_reg_partner.upload_ok_cnt + upload_ok_cnt_new).ToString()
                        + " [" + exchange_reg_partner.upload_ok_cnt.ToString() + "]"
                        + ", тревожных строк: " + (exchange_reg_partner.upload_warn_cnt + upload_warn_cnt_new).ToString()
                        + " [" + exchange_reg_partner.upload_warn_cnt.ToString() + "]"
                        + ", ошибочных строк: " + (exchange_reg_partner.upload_err_cnt + upload_err_cnt_new).ToString()
                        + " [" + exchange_reg_partner.upload_err_cnt.ToString() + "]"
                        + "."
                        ;

                    exchange_reg_partner.upload_state = upload_error_level_max;
                    exchange_reg_partner.upload_err_mess = upload_err_mess;
                    exchange_reg_partner.upload_last_date = upload_last_date;
                    exchange_reg_partner.upload_ok_date = upload_ok_date;                    
                    exchange_reg_partner.upload_last_mess = upload_last_mess;
                    exchange_reg_partner.upload_ok_cnt = exchange_reg_partner.upload_ok_cnt + upload_ok_cnt_new;
                    exchange_reg_partner.upload_warn_cnt = exchange_reg_partner.upload_warn_cnt + upload_warn_cnt_new;
                    exchange_reg_partner.upload_err_cnt = exchange_reg_partner.upload_err_cnt + upload_err_cnt_new;

                }

                if (update_download)
                {

                    // download - прием От Партнера

                    download_exchange_items = (from t1 in context.exchange_reg_partner_item
                                               from t2 in context.exchange_item
                                               from t3 in context.datasource_exchange_item
                                               where t1.exchange_item_id == t2.item_id
                                               && t2.item_id == t3.item_id
                                               && t1.reg_id == reg_id
                                               && t3.ds_id == ds_id
                                               && t3.for_get
                                               select t1
                                               )
                                               .Distinct()
                                               .ToList();

                    if ((download_exchange_items == null) || (download_exchange_items.Count <= 0))
                        //return res;
                        return UpdateExchangePartnerReg_Error(now, null, "В регистрации с кодом " + reg_id.ToString() + " не найден список файлов для приема", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, ds_id, userInfo, Enums.ExchangeItemEnum.None, reg_id, "UpdateExchangePartnerReg");

                    download_result_log_list.Clear();
                    foreach (var item in download_exchange_items)
                    {
                        // сколько дней можно ждать прием
                        download_days_cnt_for_wait = item.days_cnt_for_wait;

                        // дата последнего удачного приема
                        download_last_date = context.exchange_log.Where(ss => ss.reg_id == reg_id
                            && ss.error_level == 0
                            && ss.ds_id == ds_id
                            && ss.exchange_direction == (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT
                            && ss.exchange_item_id == item.exchange_item_id)
                            .OrderByDescending(ss => ss.date_beg)
                            .Select(ss => ss.date_beg)
                            .FirstOrDefault();
                        if (download_last_date == null)
                        {
                            // удачный прием не найден
                            download_last_date_not_found = true;
                            download_last_date = reg.prepared_date;
                        }
                        if (download_last_date == null)
                            continue;

                        // дата когда заканчивается ожидание
                        download_end_wait_date = ((DateTime)download_last_date).AddDays(download_days_cnt_for_wait);
                        
                        download_end_wait_ended = !(download_end_wait_date.Date <= today);
                        download_end_wait_ended = download_end_wait_date.Date < today;
                        
                        //if (download_end_wait_date.Date <= today)
                            //continue;

                        //if ((download_last_date_not_found) || (download_end_wait_ended))
                        if (download_end_wait_ended)
                        {
                            download_result_log_list.Add(new exchange_log()
                            {
                                date_beg = now,
                                date_end = now,
                                ds_id = ds_id,
                                error_level = item.is_required ? 2 : 1,
                                exchange_direction = (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT,
                                exchange_item_id = item.exchange_item_id,
                                login = user.login,
                                mess = "Закончился период ожидания приема файла " + item.exchange_item_id.EnumName<Enums.ExchangeItemEnum>() + " [дни: " + download_days_cnt_for_wait.ToString() + "]",
                                reg_id = reg_id,
                            }
                            );
                        }
                        else
                        {
                            download_result_log_list.Add(new exchange_log()
                            {
                                date_beg = now,
                                date_end = now,
                                ds_id = ds_id,
                                error_level = 0,
                                exchange_direction = (int)Enums.ExchangeDirectionEnum.FROM_PARTNER_TO_CLIENT,
                                exchange_item_id = item.exchange_item_id,
                                login = user.login,
                                mess = "Принят файл " + item.exchange_item_id.EnumName<Enums.ExchangeItemEnum>(),
                                reg_id = reg_id,
                            }
                        );
                        }

                    }

                    /*
                        bool stateOK = true
                        - были проведены обмены всеми обязательными и необязательными файлами в указанный срок                        

                        bool stateWarn = true
                        - были проведены обмены всеми обязательными файлами в указанный срок, но:
                        - в указанный срок не было хотя бы одного необязательного файла                        

                        bool stateError = true
                        - в указанный срок не было хотя бы одного обязательного файла
                    */

                    // "2016-03-18 16:46:23.623742"
                    if ((download_result_log_list != null) && (download_result_log_list.Count > 0))
                    {
                        download_error_level_max = download_result_log_list.Select(ss => ss.error_level).Max();
                        var err_mess_item = download_result_log_list.Where(ss => ss.error_level == 2).OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                        download_err_mess = err_mess_item != null ? err_mess_item.mess : "";
                        var last_date_item = download_result_log_list.OrderByDescending(ss => ss.date_beg).FirstOrDefault();
                        download_last_date = last_date_item != null ? last_date_item.date_beg : exchange_reg_partner.download_last_date;
                        download_ok_date = err_mess_item != null ? null : download_last_date;
                        var last_item = download_result_log_list.OrderByDescending(ss => ss.error_level).ThenByDescending(ss => ss.date_beg).FirstOrDefault();
                        download_last_mess = last_item != null ? last_item.mess : "";
                        download_ok_cnt_new = download_result_log_list.Where(ss => ss.error_level == 0).Count();
                        download_warn_cnt_new = download_result_log_list.Where(ss => ss.error_level == 1).Count();
                        download_err_cnt_new = download_result_log_list.Where(ss => ss.error_level == 2).Count();
                    }
                    else
                    {
                        download_error_level_max = 0;
                        download_err_mess = exchange_reg_partner.download_err_mess;
                        download_last_date = exchange_reg_partner.download_last_date;
                        download_ok_date = exchange_reg_partner.download_ok_date;
                        download_last_mess = exchange_reg_partner.download_last_mess;
                        download_ok_cnt_new = 0;
                        download_warn_cnt_new = 0;
                        download_err_cnt_new = 0;
                    }

                    
                    mess += " Прием: удачный прием не найден: " + download_last_date_not_found.ToString()
                        + ", дата окончания ожидания: " + download_end_wait_date.ToString("dd.MM.yyyy HH:mm:ss")                        
                        + ", статус: " + download_error_level_max.ToString()
                        + " [" + exchange_reg_partner.download_state.ToString() + "]"
                        + ", дата: " + (download_last_date.HasValue ? ((DateTime)download_last_date).ToString("dd.MM.yyyy HH:mm:ss") : "-")
                        + " [" + (exchange_reg_partner.download_last_date.HasValue ? ((DateTime)exchange_reg_partner.download_last_date).ToString("dd.MM.yyyy HH:mm:ss") : "-") + "]"
                        + ", дата успеха: " + (download_ok_date.HasValue ? ((DateTime)download_ok_date).ToString("dd.MM.yyyy HH:mm:ss") : "-")
                        + " [" + (exchange_reg_partner.download_ok_date.HasValue ? ((DateTime)exchange_reg_partner.download_ok_date).ToString("dd.MM.yyyy HH:mm:ss") : "-") + "]"
                        + ", успешных строк: " + (exchange_reg_partner.download_ok_cnt + download_ok_cnt_new).ToString()
                        + " [" + exchange_reg_partner.download_ok_cnt.ToString() + "]"
                        + ", тревожных строк: " + (exchange_reg_partner.download_warn_cnt + download_warn_cnt_new).ToString()
                        + " [" + exchange_reg_partner.download_warn_cnt.ToString() + "]"
                        + ", ошибочных строк: " + (exchange_reg_partner.download_err_cnt + download_err_cnt_new).ToString()
                        + " [" + exchange_reg_partner.download_err_cnt.ToString() + "]"
                        + "."
                        ;

                    exchange_reg_partner.download_state = download_error_level_max;
                    exchange_reg_partner.download_err_mess = download_err_mess;
                    exchange_reg_partner.download_last_date = download_last_date;
                    exchange_reg_partner.download_ok_date = download_ok_date;
                    exchange_reg_partner.download_last_mess = download_last_mess;
                    exchange_reg_partner.download_ok_cnt = exchange_reg_partner.download_ok_cnt + download_ok_cnt_new;
                    exchange_reg_partner.download_warn_cnt = exchange_reg_partner.download_warn_cnt + download_warn_cnt_new;
                    exchange_reg_partner.download_err_cnt = exchange_reg_partner.download_err_cnt + download_err_cnt_new;

                }

                if ((upload_result_log_list != null) && (upload_result_log_list.Count > 0))
                    context.exchange_log.AddRange(upload_result_log_list);
                if ((download_result_log_list != null) && (download_result_log_list.Count > 0))
                    context.exchange_log.AddRange(download_result_log_list);

                context.SaveChanges();
                                             
                //Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.UDPATE_EXCHANGE_DATA, userInfo.login, null, now, "UpdateExchangePartnerReg");
            }

            return res;
        }

        #endregion

        #region ExchangeTask

        private ExchangeTaskList GetExchangeTaskList_Error(DateTime date_beg, ErrInfo err, string err_str, long err_code, UserInfo userInfo)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";
            string mess = err != null ? err.Message : err_str;
            Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, "GetExchangeTaskList");

            return new ExchangeTaskList()
            {
                error = err != null ? err : new ErrInfo(err_code, err_str),
            }
            ;
        }

        private ServiceOperationResult SetExchangeTask_Error(DateTime date_beg, ErrInfo err, string err_str, long err_code, UserInfo userInfo, string mess2)
        {
            string user_name = userInfo != null ? userInfo.login : "unknown";
            string mess = err != null ? err.Message : err_str;
            Loggly.InsertLog_ExchangePartner(mess, (long)Enums.LogEsnType.ERROR, user_name, null, date_beg, mess2);

            return new ServiceOperationResult()
            {
                Id = err != null ? err.Id.GetValueOrDefault(100) : err_code,
                error = err != null ? err : new ErrInfo(err_code, err_str),
            }
            ;
        }

        private ExchangeTaskList xGetExchangeTaskList(UserInfo userInfo)
        {                       
            DateTime now = DateTime.Now;

            /*
                проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            {
                return GetExchangeTaskList_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, userInfo);
            }

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
            {
                return GetExchangeTaskList_Error(now, user.error, "Не найден пользователь " + userInfo.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo);
            }

            var reg_list = GetRegistrationList_Partner(user.user_id);
            if ((reg_list == null) || (reg_list.error != null) || (reg_list.ExchangeRegPartner_list == null) || (reg_list.ExchangeRegPartner_list.Count <= 0))
            {
                return GetExchangeTaskList_Error(now, reg_list.error, "Не найдена регистрация пользователя", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo);
            }

            ExchangeTaskList res = new ExchangeTaskList();
            using (var context = new AuMainDb())
            {
                foreach (var reg in reg_list.ExchangeRegPartner_list)
                {                    
                    var task_list = context.exchange_reg_task.Where(ss => ss.reg_id == reg.reg_id
                        && ((ss.state == (int)Enums.ExchangeTaskState.CREATED) || (ss.state == (int)Enums.ExchangeTaskState.RECEIVED))
                        )
                        .ToList();
                    if ((task_list == null) || (task_list.Count <= 0))
                    {                        
                        continue;
                    }
                    ExchangeTaskList res1 = new ExchangeTaskList(task_list);
                    res.ExchangeTask_list.AddRange(res1.ExchangeTask_list);
                }
                
                //if (res.ExchangeTask_list.Count <= 0)
                    //return GetExchangeTaskList_Error(now, null, "Не найден список выданных задач для данного пользователя", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo);

                return res;
            }
        }


        private ServiceOperationResult xSetExchangeTask_Received(UserInfo userInfo, long task_id)
        {
            DateTime now = DateTime.Now;
            string method_name = "SetExchangeTask_Received";

            /*
                проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            {
                return SetExchangeTask_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, userInfo, method_name);
            }

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
            {
                return SetExchangeTask_Error(now, user.error, "Не найден пользователь " + userInfo.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);
            }

            using (var context = new AuMainDb())
            {
                var task = context.exchange_reg_task.Where(ss => ss.task_id == task_id
                    && ((ss.state == (int)Enums.ExchangeTaskState.CREATED) || (ss.state == (int)Enums.ExchangeTaskState.RECEIVED))
                    )
                    .FirstOrDefault();
                if (task == null)
                    return SetExchangeTask_Error(now, null, "Не найдена выданная задача с кодом " + task_id, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);
                
                task.state = (int)Enums.ExchangeTaskState.RECEIVED;
                task.state_date = now;
                context.SaveChanges();

                return new ServiceOperationResult() 
                { 
                    error = null,
                    Id = 0,
                };
            }
        }

        private ServiceOperationResult xSetExchangeTaskList_Received(UserInfo userInfo, ExchangeTaskList taskList)
        {
            DateTime now = DateTime.Now;
            string method_name = "SetExchangeTaskList_Received";
            
            if ((taskList == null) || (taskList.ExchangeTask_list == null) || (taskList.ExchangeTask_list.Count <= 0))
                return SetExchangeTask_Error(now, null, "Передан пустой список задач", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);

            foreach (var item in taskList.ExchangeTask_list)
            {
                var res = xSetExchangeTask_Received(userInfo, item.task_id);
                if ((res == null) || (res.error != null))
                    return SetExchangeTask_Error(now, res.error, "Ошибка при попытке установки статуса задачи", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);
            }

            return new ServiceOperationResult()
            {
                error = null,
                Id = 0,
            };
        }

        private ServiceOperationResult xSetExchangeTask_Fail(UserInfo userInfo, long task_id, string comment)
        {
            DateTime now = DateTime.Now;
            string method_name = "SetExchangeTask_Fail";

            /*
                проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            {
                return SetExchangeTask_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, userInfo, method_name);
            }

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
            {
                return SetExchangeTask_Error(now, user.error, "Не найден пользователь " + userInfo.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);
            }

            using (var context = new AuMainDb())
            {
                var task = context.exchange_reg_task.Where(ss => ss.task_id == task_id 
                    && ss.state == (int)Enums.ExchangeTaskState.RECEIVED)
                    .FirstOrDefault();
                if (task == null)
                    return SetExchangeTask_Error(now, null, "Не найдена выданная и полученная задача с кодом " + task_id, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);

                task.state = (int)Enums.ExchangeTaskState.DONE_ERROR;
                task.state_date = now;
                task.comment = comment;
                context.SaveChanges();

                return new ServiceOperationResult()
                {
                    error = null,
                    Id = 0,
                };
            }
        }

        private ServiceOperationResult xSetExchangeTaskList_Fail(UserInfo userInfo, ExchangeTaskList taskList)
        {
            DateTime now = DateTime.Now;
            string method_name = "SetExchangeTaskList_Fail";

            if ((taskList == null) || (taskList.ExchangeTask_list == null) || (taskList.ExchangeTask_list.Count <= 0))
                return SetExchangeTask_Error(now, null, "Передан пустой список задач", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);

            foreach (var item in taskList.ExchangeTask_list)
            {
                var res = xSetExchangeTask_Fail(userInfo, item.task_id, item.comment);
                if ((res == null) || (res.error != null))
                    return SetExchangeTask_Error(now, res.error, "Ошибка при попытке установки статуса задачи", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);
            }

            return new ServiceOperationResult()
            {
                error = null,
                Id = 0,
            };
        }

        private ServiceOperationResult xSetExchangeTask_Success(UserInfo userInfo, long task_id)
        {
            DateTime now = DateTime.Now;
            string method_name = "SetExchangeTask_Success";

            /*
                проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_EXCHANGE);
            if (lic.error != null)
            {
                return SetExchangeTask_Error(now, lic.error, "Не найдена лицензия", (long)Enums.ErrCodeEnum.ERR_CODE_LIC_NOT_FOUND, userInfo, method_name);
            }

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
            {
                return SetExchangeTask_Error(now, user.error, "Не найден пользователь " + userInfo.login, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);
            }

            using (var context = new AuMainDb())
            {
                var task = context.exchange_reg_task.Where(ss => ss.task_id == task_id && ss.state == (int)Enums.ExchangeTaskState.RECEIVED).FirstOrDefault();
                if (task == null)
                    return SetExchangeTask_Error(now, null, "Не найдена выданная и полученная задача с кодом " + task_id, (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);

                task.state = (int)Enums.ExchangeTaskState.DONE_SUCCESS;
                task.state_date = now;
                context.SaveChanges();

                return new ServiceOperationResult()
                {
                    error = null,
                    Id = 0,
                };
            }
        }

        private ServiceOperationResult xSetExchangeTaskList_Success(UserInfo userInfo, ExchangeTaskList taskList)
        {
            DateTime now = DateTime.Now;
            string method_name = "SetExchangeTaskList_Success";

            if ((taskList == null) || (taskList.ExchangeTask_list == null) || (taskList.ExchangeTask_list.Count <= 0))
                return SetExchangeTask_Error(now, null, "Передан пустой список задач", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);

            foreach (var item in taskList.ExchangeTask_list)
            {
                var res = xSetExchangeTask_Success(userInfo, item.task_id);
                if ((res == null) || (res.error != null))
                    return SetExchangeTask_Error(now, res.error, "Ошибка при попытке установки статуса задачи", (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, userInfo, method_name);
            }

            return new ServiceOperationResult()
            {
                error = null,
                Id = 0,
            };
        }

        #endregion
    }
}
