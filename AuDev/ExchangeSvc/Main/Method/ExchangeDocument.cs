﻿#region
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Net;
using System.Collections.Specialized;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.IO;
using System.IO.Compression;
using System.Data;
using System.Data.OleDb;
using System.Web;
using System.Globalization;
using System.Net.Mail;
using System.Net.Mime;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

#endregion

namespace ExchangeSvc
{
    public partial class ExchangeService : IExchangeService
    {

        #region GetSalesList

        private SalesList xGetSalesList(UserInfo userInfo)
        {
            /*
                проверка лицензии
            */


            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_DOC);
            if (lic.error != null)
            {
                return new SalesList() { error = lic.error };
            }
            
            long lic_sales_id = lic.SalesId;

            //int? lic_sales_id = 222;

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
                return new SalesList() { error = user.error != null ? user.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь по заданному логину и рабочему месту"), };

            var reg = GetRegistration_Doc(user.user_id);            
            if ((reg == null) || (reg.error != null))
                return new SalesList() { error = reg.error != null ? reg.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрация пользователя"), };            
            
            //return GetSalesList_byClientId(lic.ClientId, lic.SalesId);
            using (var dbContext = new AuMainDb())
            {                                                                                                      
                var sales = dbContext.exchange_reg_doc_sales
                    .Where(ss => ss.reg_id == reg.reg_id && ss.is_active 
                        && ss.sales_id.HasValue 
                        && ss.sales_id != lic_sales_id)
                    .Select(ss => new Sales() 
                    { 
                        SalesId = (int)ss.sales_id,
                        SalesName = ss.sales_name,
                        EmailAddress = ss.email_for_docs,
                    });
                /*
                SalesList res = new SalesList();
                res.Sales_list = new List<Sales>(sales);
                return res;
                */
                return new SalesList() { Sales_list = new List<Sales>(sales), error = null, };
            }
        }

        #endregion

        #region SendDocument

        private ServiceOperationResult xSendDocument(UserInfo userInfo, Document doc, int salesId, bool toEmail)
        {
            //return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован"), Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION,};


            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_DOC);
            if (lic.error != null)
            {
                return new ServiceOperationResult() { error = lic.error, Id = lic.error.Id.HasValue ? (long)lic.error.Id : (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, };
            }

            string lic_sales_name = lic.SalesName;
            long lic_sales_id = lic.SalesId;

            //string lic_sales_name = "lic_sales_name";
            //int? lic_sales_id = 222;
            

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
                return new ServiceOperationResult() { error = user.error != null ? user.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь по заданному логину и рабочему месту"), Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, };

            var reg = GetRegistration_Doc(user.user_id);
            if ((reg == null) || (reg.error != null))
                return new ServiceOperationResult() { error = reg.error != null ? reg.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрация пользователя"), Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, };
            
            // пользуемся всегда ContentAsString
            if ((doc == null) || (String.IsNullOrEmpty(doc.ContentAsString)))
                return new ServiceOperationResult() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан документ (пуст)"), Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, };

            //var salesList = GetSalesList_byClientId(lic.ClientId, lic.SalesId);
            var salesList = GetSalesList(userInfo);
            if ((salesList == null) || (salesList.error != null) || (salesList.Sales_list == null) || (salesList.Sales_list.Count <= 0))
                return new ServiceOperationResult() { error = salesList.error != null ? salesList.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден список торговых точек для заданного клиента"), Id = (long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, };

            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            bool sent_to_email = false;
            bool sent_to_email_err = false;
            string sent_to_email_err_mess = "";
            exchange_doc exchange_doc = null;
            exchange_doc_data exchange_doc_data = null;
            exchange_doc_target exchange_doc_target = null;
            exchange_doc_target_state exchange_doc_target_state = null;
            byte[] content_as_bytes = GlobalUtil.GetBytes(doc.ContentAsString);
            string content_as_string = doc.ContentAsString;
            byte[] content_as_bytes_for_email = Encoding.GetEncoding("windows-1251").GetBytes(doc.ContentAsString);
            using (var dbContext = new AuMainDb())
            {
                // если есть документ с таким же DocId и SalesId и DocType:
                // обновляем его содержимое и все его статусы                
                exchange_doc = dbContext.exchange_doc
                    //.Where(ss => ss.sales_id == doc.SalesId && ss.doc_id == doc.DocId && ss.doc_type == doc.DocType)
                    .Where(ss => ss.sales_id == lic_sales_id && ss.doc_id == doc.DocId && ss.doc_type == doc.DocType)
                    .FirstOrDefault()
                    ;
                if (exchange_doc != null)
                {
                    exchange_doc.act_date = now;
                    exchange_doc.act_user = userInfo.login;
                    exchange_doc.crt_date = now;
                    exchange_doc.crt_user = userInfo.login;
                    exchange_doc.doc_comment = doc.Comment;
                    exchange_doc.doc_format = doc.DocFormat;
                    exchange_doc.doc_name = doc.DocName;
                    exchange_doc.user_id = user.user_id;
                    exchange_doc.terminate_date = reg.days_to_keep_data.GetValueOrDefault(0) > 0 ? (DateTime?)now.AddDays((int)reg.days_to_keep_data) : null;
                    //exchange_doc.doc_type = doc.DocType;
                    exchange_doc.date_beg = doc.DateBeg.HasValue ? (DateTime?)((DateTime)doc.DateBeg).Date : null;
                    exchange_doc.date_end = doc.DateEnd.HasValue ? (DateTime?)((DateTime)doc.DateEnd).Date : null;

                    exchange_doc_data = dbContext.exchange_doc_data.Where(ss => ss.item_id == exchange_doc.item_id).FirstOrDefault();
                    if (exchange_doc_data != null)
                    {
                        exchange_doc_data.data_text = "";
                        exchange_doc_data.data_binary = content_as_bytes;
                    }
                    else
                    {
                        exchange_doc_data = new exchange_doc_data();
                        exchange_doc_data.item_id = exchange_doc.item_id;
                        exchange_doc_data.data_text = "";
                        exchange_doc_data.data_binary = content_as_bytes;
                        dbContext.exchange_doc_data.Add(exchange_doc_data);
                    }

                    foreach (var salesPoint in salesList.Sales_list)
                    {
                        if ((salesId > 0) && (salesPoint.SalesId != salesId))
                            continue;
                        
                        sent_to_email = false;
                        sent_to_email_err = false;
                        sent_to_email_err_mess = "";
                        if (toEmail)
                        {
                            if (String.IsNullOrEmpty(salesPoint.EmailAddress))
                            {
                                sent_to_email_err = true;
                                sent_to_email_err_mess = "Накладная по e-mail не отправлена, т.к. в настройках не задан электронный адрес";
                                // todo - продолжение этой обработки
                            }
                            else
                            {
                                var res = xSendDocument_Email(salesPoint.EmailAddress
                                    , ((String.IsNullOrEmpty(doc.DocName) ? "Документ с кодом " + doc.DocId : doc.DocName) + ".xml")
                                    + " ["
                                    + (String.IsNullOrEmpty(lic_sales_name) ? "_" : lic_sales_name)
                                    + " -> "
                                    + (String.IsNullOrEmpty(salesPoint.SalesName) ? "_" : salesPoint.SalesName)
                                    + "]"
                                    , ((String.IsNullOrEmpty(doc.DocName) ? "Документ с кодом " + doc.DocId : doc.DocName) + ".xml")
                                    //, doc.ContentAsBytes);
                                    //, content_as_bytes
                                    //, content_as_string
                                    , content_as_bytes_for_email
                                    );
                                if (res.Item1)
                                {
                                    sent_to_email = true;
                                }
                                else
                                {
                                    sent_to_email = false;
                                    Loggly.InsertLog_ExchangeDocument("Ошибка в SendDocument_Email: " + res.Item2, (long)Enums.LogEsnType.ERROR, "exchanger", exchange_doc.item_id, now, null);
                                }
                            }
                        }

                        var exchange_doc_target_existing = dbContext.exchange_doc_target
                            .Where(ss => ss.exchange_doc_id == exchange_doc.item_id && ss.target_sales_id == salesPoint.SalesId)
                            .FirstOrDefault();
                        if (exchange_doc_target_existing != null)
                        {
                            exchange_doc_target_existing.curr_state_id = (int)Enums.ExchangeDocState.SENDED;
                        }
                        else
                        {
                            exchange_doc_target = new exchange_doc_target();
                            exchange_doc_target.curr_state_id = (int)Enums.ExchangeDocState.SENDED;
                            exchange_doc_target.exchange_doc_id = exchange_doc.item_id;                            
                            exchange_doc_target.sent_to_email = sent_to_email ? salesPoint.EmailAddress : "";
                            exchange_doc_target.target_sales_id = salesPoint.SalesId;
                            exchange_doc_target.target_workplace_id = null;
                            dbContext.exchange_doc_target.Add(exchange_doc_target);
                        }
                        exchange_doc_target_state = new exchange_doc_target_state();
                        exchange_doc_target_state.crt_date = now;
                        exchange_doc_target_state.crt_user = userInfo.login;
                        exchange_doc_target_state.doc_state = (int)Enums.ExchangeDocState.SENDED;
                        //exchange_doc_target_state.exchange_doc_target = exchange_doc_target;
                        exchange_doc_target_state.exchange_doc_target = exchange_doc_target_existing != null ? exchange_doc_target_existing : exchange_doc_target;
                        dbContext.exchange_doc_target_state.Add(exchange_doc_target_state);
                    }
                }
                else
                {
                    exchange_doc = new exchange_doc();
                    exchange_doc.act_date = now;
                    exchange_doc.act_user = userInfo.login;
                    exchange_doc.crt_date = now;
                    exchange_doc.crt_user = userInfo.login;
                    exchange_doc.doc_comment = doc.Comment;
                    exchange_doc.doc_format = doc.DocFormat;
                    exchange_doc.doc_id = doc.DocId;
                    exchange_doc.doc_name = doc.DocName;
                    exchange_doc.sales_id = (int)lic_sales_id;
                    exchange_doc.workplace_id = null;
                    exchange_doc.user_id = user.user_id;
                    exchange_doc.terminate_date = reg.days_to_keep_data.GetValueOrDefault(0) > 0 ? (DateTime?)now.AddDays((int)reg.days_to_keep_data) : null;
                    exchange_doc.doc_type = doc.DocType;
                    exchange_doc.date_beg = doc.DateBeg.HasValue ? (DateTime?)((DateTime)doc.DateBeg).Date : null;
                    exchange_doc.date_end = doc.DateEnd.HasValue ? (DateTime?)((DateTime)doc.DateEnd).Date : null;

                    dbContext.exchange_doc.Add(exchange_doc);
                    dbContext.SaveChanges();
                    
                    exchange_doc_data = new exchange_doc_data();
                    exchange_doc_data.item_id = exchange_doc.item_id;
                    exchange_doc_data.data_text = "";
                    exchange_doc_data.data_binary = content_as_bytes;
                    dbContext.exchange_doc_data.Add(exchange_doc_data);

                    foreach (var salesPoint in salesList.Sales_list)
                    {
                        if ((salesId > 0) && (salesPoint.SalesId != salesId))
                            continue;

                        sent_to_email = false;
                        if ((!String.IsNullOrEmpty(salesPoint.EmailAddress)) && (toEmail))
                        {
                            var res = xSendDocument_Email(salesPoint.EmailAddress
                                , ((String.IsNullOrEmpty(doc.DocName) ? "Документ с кодом " + doc.DocId : doc.DocName) + ".xml")
                                + " ["
                                + (String.IsNullOrEmpty(lic_sales_name) ? "_" : lic_sales_name)
                                + " -> "
                                + (String.IsNullOrEmpty(salesPoint.SalesName) ? "_" : salesPoint.SalesName)
                                + "]"
                                , ((String.IsNullOrEmpty(doc.DocName) ? "Документ с кодом " + doc.DocId : doc.DocName) + ".xml")
                                //, doc.ContentAsBytes);
                                //, content_as_bytes
                                //, content_as_string
                                , content_as_bytes_for_email
                                );
                            if (res.Item1)
                            {
                                sent_to_email = true;
                            }
                            else
                            {
                                sent_to_email = false;
                                Loggly.InsertLog_ExchangeDocument("Ошибка в SendDocument_Email: " + res.Item2, (long)Enums.LogEsnType.ERROR, "exchanger", exchange_doc.item_id, now, null);
                            }
                        }

                        exchange_doc_target = new exchange_doc_target();
                        exchange_doc_target.curr_state_id = (int)Enums.ExchangeDocState.SENDED;
                        exchange_doc_target.exchange_doc_id = exchange_doc.item_id;
                        exchange_doc_target.sent_to_email = sent_to_email ? salesPoint.EmailAddress : "";
                        exchange_doc_target.target_sales_id = salesPoint.SalesId;
                        exchange_doc_target.target_workplace_id = null;
                        dbContext.exchange_doc_target.Add(exchange_doc_target);
                        exchange_doc_target_state = new exchange_doc_target_state();
                        exchange_doc_target_state.crt_date = now;
                        exchange_doc_target_state.crt_user = userInfo.login;
                        exchange_doc_target_state.doc_state = (int)Enums.ExchangeDocState.SENDED;
                        exchange_doc_target_state.exchange_doc_target = exchange_doc_target;
                        dbContext.exchange_doc_target_state.Add(exchange_doc_target_state);
                    }
                }
                
                dbContext.SaveChanges();

                Loggly.InsertLog_ExchangeDocument("Отправлен документ " + doc.DocName, (long)Enums.LogEsnType.DOC_SEND, user.login, exchange_doc.item_id, now, null);

                return new ServiceOperationResult() { Id = 0, error = null, };
            }            
        }        

        private Tuple<bool, string> xSendDocument_Email(string address, string topic, string file_name, byte[] data)
        {            
            try
            {
                string sender_smtp_address = "smtp.gmail.com";
                int sender_smtp_port = 587;
                bool sender_smtp_ssl = true;
                string sender_address = "cab.aptekaural@gmail.com";
                string sender_login = "cab.aptekaural@gmail.com";
                string sender_alias = "cab.aptekaural";
                string sender_password = "aptekaural";

                DateTime now = DateTime.Now;
                using (SmtpClient c = new SmtpClient(sender_smtp_address, sender_smtp_port))
                {
                    MailAddress add = new MailAddress(address);
                    MailMessage msg = new MailMessage();
                    msg.To.Add(add);
                    msg.From = new MailAddress(sender_address, sender_alias);
                    msg.IsBodyHtml = true;
                    msg.Subject = topic;
                    msg.Body = "";

                    using (MemoryStream ms = new MemoryStream(data))
                    {
                        ContentType contentType = new ContentType(MediaTypeNames.Text.Plain);
                        Attachment att = new Attachment(ms, file_name, "text/plain");
                        att.ContentType = contentType;
                        att.ContentDisposition.FileName = file_name;
                        //att.TransferEncoding = TransferEncoding.Unknown;
                        msg.Attachments.Add(att);

                        c.Credentials = new System.Net.NetworkCredential(sender_login, sender_password);
                        c.EnableSsl = sender_smtp_ssl;
                        c.Send(msg);
                    }
                }

                return new Tuple<bool, string>(true, "");
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, GlobalUtil.ExceptionInfo(ex));
            }
        }

        #endregion

        #region GetDocuments

        private DocumentList xGetDocuments(UserInfo userInfo, int docType)
        {
            //return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован"), };

            /*
                проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_DOC);
            if (lic.error != null)
            {
                return new DocumentList() { error = lic.error };
            }

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
                return new DocumentList() { error = user.error != null ? user.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь по заданному логину и рабочему месту"), };

            var reg = GetRegistration_Doc(user.user_id);
            if ((reg == null) || (reg.error != null))
                return new DocumentList() { error = reg.error != null ? reg.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрация пользователя"), };

            DocumentList res = new DocumentList();
            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            using (var dbContext = new AuMainDb())
            {
                List<Document> docs = (from ed in dbContext.exchange_doc
                           from edd in dbContext.exchange_doc_data
                           from edt in dbContext.exchange_doc_target
                           from erds in dbContext.exchange_reg_doc_sales
                           where ed.item_id == edd.item_id
                           && ed.item_id == edt.exchange_doc_id
                           && erds.sales_id == ed.sales_id
                           && erds.reg_id == reg.reg_id
                           && edt.target_sales_id == lic.SalesId
                           && edt.curr_state_id == (int)Enums.ExchangeDocState.SENDED
                           && !ed.is_terminated
                           && ed.doc_type == docType
                           select new Document()
                           {
                               SalesId = ed.sales_id.HasValue ? (int)ed.sales_id : 0,
                               SalesName = erds.sales_name,
                               DocId = ed.doc_id.HasValue ? (int)ed.doc_id : 0,
                               DocName = ed.doc_name,
                               DocFormat = ed.doc_format,
                               Comment = ed.doc_comment,
                               DocType = ed.doc_type,
                               //ContentAsString = Util.GetString(edd.data_binary),
                               ContentAsBytes = edd.data_binary,
                               DateBeg = ed.date_beg,
                               DateEnd = ed.date_end,
                           }
                           )
                           .Distinct()
                           .ToList();

                if ((docs == null) || (docs.Count <= 0))
                    return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены документы к получению"), };

                exchange_doc exchange_doc = null;
                foreach (var doc in docs)
                {
                    doc.ForDel = false;
                    exchange_doc = dbContext.exchange_doc.Where(ss => ss.doc_id == doc.DocId && ss.sales_id == doc.SalesId).FirstOrDefault();
                    if (exchange_doc != null)
                    {
                        doc.ForDel = ((exchange_doc.terminate_date.HasValue) && (exchange_doc.terminate_date <= now));
                        doc.Deleted = exchange_doc.is_terminated;
                        if ((!doc.ForDel) && (!doc.Deleted))
                        {
                            exchange_doc.act_date = now;
                            exchange_doc.act_user = user.login;
                        }
                    }
                    if ((!doc.ForDel) && (!doc.Deleted))
                        doc.ContentAsString = GlobalUtil.GetString(doc.ContentAsBytes);
                }

                
                var docs_for_del = docs.Where(ss => ss.ForDel).ToList();

                int err_cnt = 0;
                res.Document_list = new List<Document>(docs);
                if ((res.Document_list != null) && (res.Document_list.Count > 0))
                {
                    foreach (var res_doc in res.Document_list)
                    {
                        if ((!res_doc.ForDel) && (!res_doc.Deleted))
                            continue;
                        res_doc.Comment = "Документ удален - истек срок хранения";
                        res_doc.ContentAsBytes = null;
                        res_doc.ContentAsString = "";
                        res_doc.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Документ удален - истек срок хранения");
                        err_cnt++;
                    }
                }

                if ((docs_for_del != null) && (docs_for_del.Count > 0))
                {
                    foreach (var doc_for_del in docs_for_del)
                    {                  
                        exchange_doc = dbContext.exchange_doc.Where(ss => ss.doc_id == doc_for_del.DocId && ss.sales_id == doc_for_del.SalesId).FirstOrDefault();
                        if (exchange_doc != null)
                        {
                            var exchange_doc_data = dbContext.exchange_doc_data.Where(ss => ss.item_id == exchange_doc.item_id).FirstOrDefault();
                            if (exchange_doc_data != null)
                                dbContext.exchange_doc_data.Remove(exchange_doc_data);                            
                            exchange_doc.is_terminated = true;
                            exchange_doc.terminated_date = now;
                        }
                    }
                }

                dbContext.SaveChanges();

                Loggly.InsertLog_ExchangeDocument("Получены документы [всего документов: " + res.Document_list.Count.ToString() + ", с ошибками: " + err_cnt.ToString() + "]", (long)Enums.LogEsnType.DOC_RECEIVE, user.login, null, now, null);

                return res;
            }            
        }

        #endregion

        #region ChangeDocumentState

        private DocumentList xChangeDocumentState(UserInfo userInfo, DocumentList docList, int nextState)
        {
            // return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован"), };

            /*
                проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_DOC);
            if (lic.error != null)
            {
                return new DocumentList() { error = lic.error };
            }

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
                return new DocumentList() { error = user.error != null ? user.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь по заданному логину и рабочему месту"), };

            var reg = GetRegistration_Doc(user.user_id);
            if ((reg == null) || (reg.error != null))
                return new DocumentList() { error = reg.error != null ? reg.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрация пользователя"), };

            if ((docList == null) || (docList.Document_list == null) || (docList.Document_list.Count <= 0))
                return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан список документов"), };

            DocumentList res = new DocumentList() { Document_list = new List<Document>(), error = null, };
            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            int state_changer_sales_id = (int)lic.SalesId;

            Enums.ExchangeDocState nextDocState = Enums.ExchangeDocState.SENDED;
            try
            {
                nextDocState = (Enums.ExchangeDocState)nextState;
            }
            catch
            {
                return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Недопустимый код статуса"), };
            }
            Enums.ExchangeDocState currDocState = Enums.ExchangeDocState.SENDED;            
            Enums.ExchangeDocState allowedNextDocState = Enums.ExchangeDocState.SENDED;

            int doc_cnt = docList.Document_list.Count;
            int err_cnt = 0;
            bool allDocsWithError = true;
            bool someDocsWithError = false;
            List<Document> docs_for_del = new List<Document>();
            exchange_doc exchange_doc = null;
            using (var dbContext = new AuMainDb())
            {
                foreach (var doc in docList.Document_list)
                {
                    try
                    {
                        doc.ForDel = false;
                        exchange_doc = dbContext.exchange_doc.Where(ss => ss.doc_id == doc.DocId && ss.sales_id == doc.SalesId && ss.doc_type == doc.DocType).FirstOrDefault();
                        if (exchange_doc != null)
                        {
                            doc.ForDel = ((exchange_doc.terminate_date.HasValue) && (exchange_doc.terminate_date <= now));
                            doc.Deleted = exchange_doc.is_terminated;
                        }
                        if ((doc.ForDel) || (doc.Deleted))
                        {
                            if (doc.ForDel)
                                docs_for_del.Add(doc);
                            //
                            someDocsWithError = true;
                            res.Document_list.Add(new Document()
                            {
                                SalesId = doc.SalesId,
                                DocId = doc.DocId,
                                DocName = doc.DocName,                                
                                Comment = "Документ удален - истек срок хранения",
                                State = doc.State,
                                DocType = doc.DocType,
                                DateBeg = doc.DateBeg,
                                DateEnd = doc.DateEnd,
                                error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Документ удален - истек срок хранения")
                            });
                            err_cnt++;
                            continue; 
                        }


                        var curr_doc_state = (from ed in dbContext.exchange_doc
                                             from edt in dbContext.exchange_doc_target
                                             where ed.item_id == edt.exchange_doc_id
                                             && ed.sales_id == doc.SalesId
                                             && ed.doc_id == doc.DocId
                                             && ed.doc_type == doc.DocType
                                             && edt.target_sales_id == state_changer_sales_id
                                             select edt)
                                             .FirstOrDefault();
                        if (curr_doc_state == null)
                        {
                            someDocsWithError = true;
                            res.Document_list.Add(new Document()
                            {
                                SalesId = doc.SalesId,
                                DocId = doc.DocId,
                                DocName = doc.DocName,
                                Comment = "Документ не найден в списке отправленных для данной торговой точки документов",
                                State = doc.State,
                                DocType = doc.DocType,
                                DateBeg = doc.DateBeg,
                                DateEnd = doc.DateEnd,
                                error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Документ не найден в списке отправленных для данной торговой точки документов")
                            });
                            err_cnt++;
                            continue; 
                        }

                        currDocState = (Enums.ExchangeDocState)curr_doc_state.curr_state_id;
                        switch (currDocState)
                        {
                            case Enums.ExchangeDocState.SENDED:
                                allowedNextDocState = Enums.ExchangeDocState.RECEIVED;
                                break;
                            case Enums.ExchangeDocState.RECEIVED:
                                allowedNextDocState = Enums.ExchangeDocState.LOADED;
                                break;
                            case Enums.ExchangeDocState.LOADED:
                                allowedNextDocState = Enums.ExchangeDocState.NEED_RESEND;
                                break;
                            default:
                                break;
                        }

                        if (nextDocState != allowedNextDocState)
                        {
                            someDocsWithError = true;
                            res.Document_list.Add(new Document()
                            {
                                SalesId = doc.SalesId,
                                DocId = doc.DocId,
                                DocName = doc.DocName,
                                //Comment = "Нельзя перевести документ из статуса " + doc.State.ToString() + " в статус " + nextState.ToString(),
                                Comment = "Нельзя перевести документ из статуса " + ExchangeUtil.GetDocStateName(currDocState) + " в статус " + ExchangeUtil.GetDocStateName(nextDocState),
                                State = (int)currDocState,
                                DocType = doc.DocType,
                                DateBeg = doc.DateBeg,
                                DateEnd = doc.DateEnd,
                                error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Нельзя перевести документ из статуса " + ExchangeUtil.GetDocStateName(currDocState) + " в статус " + ExchangeUtil.GetDocStateName(nextDocState))
                            });
                            err_cnt++;
                            continue;
                        }
                        
                        curr_doc_state.curr_state_id = nextState;
                        exchange_doc_target_state exchange_doc_target_state = new exchange_doc_target_state();
                        exchange_doc_target_state.crt_date = now;
                        exchange_doc_target_state.crt_user = userInfo.login;
                        exchange_doc_target_state.doc_state = nextState;
                        exchange_doc_target_state.target_id = curr_doc_state.item_id;
                        dbContext.exchange_doc_target_state.Add(exchange_doc_target_state);
                        
                        if (exchange_doc != null)
                        {
                            exchange_doc.act_date = now;
                            exchange_doc.act_user = user.login;
                        }

                        dbContext.SaveChanges();

                        res.Document_list.Add(new Document()
                        {
                            SalesId = doc.SalesId,
                            DocId = doc.DocId,
                            DocName = doc.DocName,
                            Comment = "Статус успешно изменен",
                            State = nextState,
                            DocType = doc.DocType,
                            DateBeg = doc.DateBeg,
                            DateEnd = doc.DateEnd,
                            error = null,                            
                        });
                                                                     
                        allDocsWithError = false;
                    }
                    catch (Exception ex)
                    {
                        someDocsWithError = true;
                        res.Document_list.Add(new Document() 
                        { 
                            SalesId = doc.SalesId, 
                            DocId = doc.DocId, 
                            DocName = doc.DocName,
                            Comment = "Ошибка: " + GlobalUtil.ExceptionInfo(ex), 
                            State = doc.State,
                            DocType = doc.DocType,
                            DateBeg = doc.DateBeg,
                            DateEnd = doc.DateEnd,
                            error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, GlobalUtil.ExceptionInfo(ex)) 
                        });
                        err_cnt++;
                        continue;
                    }
                }

                if ((docs_for_del != null) && (docs_for_del.Count > 0))
                {
                    if ((docs_for_del != null) && (docs_for_del.Count > 0))
                    {
                        foreach (var doc_for_del in docs_for_del)
                        {
                            exchange_doc = dbContext.exchange_doc.Where(ss => ss.doc_id == doc_for_del.DocId && ss.sales_id == doc_for_del.SalesId && ss.doc_type == doc_for_del.DocType).FirstOrDefault();
                            if (exchange_doc != null)
                            {
                                var exchange_doc_data = dbContext.exchange_doc_data.Where(ss => ss.item_id == exchange_doc.item_id).FirstOrDefault();
                                if (exchange_doc_data != null)
                                    dbContext.exchange_doc_data.Remove(exchange_doc_data);
                                exchange_doc.is_terminated = true;
                                exchange_doc.terminated_date = now;
                            }
                        }
                    }
                    dbContext.SaveChanges();
                }

                if (allDocsWithError)
                {
                    res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Все документы обработаны с ошибкой");
                }
                else if (someDocsWithError)
                {
                    res.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Некоторые документы обработаны с ошибкой");
                }

                doc_cnt = res.Document_list.Count;

                Loggly.InsertLog_ExchangeDocument("Смена статуса документов на " + ExchangeUtil.GetDocStateName(nextDocState) + " [всего документов: " + doc_cnt.ToString() + ", ошибок: " + err_cnt.ToString() + "]", (long)Enums.LogEsnType.DOC_CHANGE_STATE, user.login, null, now, null);

                return res;
            }
        }

        #endregion

        #region GetDocumentStates

        private DocumentList xGetDocumentStates(UserInfo userInfo, DocumentList docList)
        {
            //return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован"), };
                        //return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован"), };

            if ((docList == null) || (docList.error != null) || (docList.Document_list == null) || (docList.Document_list.Count <= 0))
            {
                return new DocumentList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не задан список документов"), };
            }

            /*
                проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_DOC);
            if (lic.error != null)
            {
                return new DocumentList() { error = lic.error };
            }

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
                return new DocumentList() { error = user.error != null ? user.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь по заданному логину и рабочему месту"), };

            var reg = GetRegistration_Doc(user.user_id);
            if ((reg == null) || (reg.error != null))
                return new DocumentList() { error = reg.error != null ? reg.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрация пользователя"), };

            DateTime now = DateTime.Now;
            List<Document> docs_for_del = new List<Document>();
            DocumentList res = new DocumentList() { Document_list = new List<Document>(), };
            exchange_doc exchange_doc = null;
            using (var dbContext = new AuMainDb())
            {
                foreach (var doc in docList.Document_list)
                {
                    Document res_doc = new Document()
                    {
                        DocId = doc.DocId,
                        DocName = doc.DocName,
                        DocFormat = doc.DocFormat,
                        Comment = doc.Comment,
                        SalesId = (int)lic.SalesId,
                        DocType = doc.DocType,
                        DateBeg = doc.DateBeg,
                        DateEnd = doc.DateEnd,
                    }
                    ;
                    res_doc.DocumentStateList = new List<DocumentState>();

                    doc.ForDel = false;
                    exchange_doc = dbContext.exchange_doc.Where(ss => ss.doc_id == doc.DocId && ss.sales_id == lic.SalesId && ss.doc_type == doc.DocType).FirstOrDefault();
                    if (exchange_doc != null)
                    {
                        doc.ForDel = ((exchange_doc.terminate_date.HasValue) && (exchange_doc.terminate_date <= now));
                        doc.Deleted = exchange_doc.is_terminated;
                    }
                    if ((doc.ForDel) || (doc.Deleted))
                    {
                        if (doc.ForDel)
                            docs_for_del.Add(res_doc);
                        //
                        res_doc.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Документ удален - истек срок хранения");
                        res.Document_list.Add(res_doc);
                        continue;
                    }

                    var curr_doc_states = (from ed in dbContext.exchange_doc
                                           from edt in dbContext.exchange_doc_target
                                           where ed.item_id == edt.exchange_doc_id
                                           //&& ed.sales_id == doc.SalesId
                                           && ed.sales_id == lic.SalesId
                                           && ed.doc_id == doc.DocId
                                           && ed.doc_type == doc.DocType
                                           && ed.item_id == (dbContext.exchange_doc.Where(ss => ss.sales_id == ed.sales_id && ss.doc_id == ed.doc_id).OrderByDescending(ss => ss.item_id).FirstOrDefault().item_id)
                                           //&& edt.target_sales_id == state_changer_sales_id
                                           select edt)
                                         ;

                    if ((curr_doc_states == null) || (curr_doc_states.Count() <= 0))
                    {
                        res_doc.error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены статусы у данного документа");
                    }
                    else
                    {
                        foreach (var state in curr_doc_states)
                        {
                            res_doc.DocumentStateList.Add(new DocumentState()
                            {
                                SalesId = state.target_sales_id.HasValue ? (int)state.target_sales_id : 0,
                                State = state.curr_state_id,
                            });
                        }
                    }

                    res.Document_list.Add(res_doc);
                }

                if ((docs_for_del != null) && (docs_for_del.Count > 0))
                {
                    if ((docs_for_del != null) && (docs_for_del.Count > 0))
                    {
                        foreach (var doc_for_del in docs_for_del)
                        {
                            exchange_doc = dbContext.exchange_doc.Where(ss => ss.doc_id == doc_for_del.DocId && ss.sales_id == doc_for_del.SalesId && ss.doc_type == doc_for_del.DocType).FirstOrDefault();
                            if (exchange_doc != null)
                            {
                                var exchange_doc_data = dbContext.exchange_doc_data.Where(ss => ss.item_id == exchange_doc.item_id).FirstOrDefault();
                                if (exchange_doc_data != null)
                                    dbContext.exchange_doc_data.Remove(exchange_doc_data);
                                exchange_doc.is_terminated = true;
                                exchange_doc.terminated_date = now;
                            }
                        }
                    }
                    dbContext.SaveChanges();
                }

                return res;
            }
        }

        #endregion

        #region GetNotReceivedDocuments

        private DocumentList xGetNotReceivedDocuments(UserInfo userInfo, int docType, int salesId)
        {
            /*
                проверка лицензии
            */
            License lic = GetLicense(userInfo.login, userInfo.workplace, userInfo.version_num, license_id_DOC);
            if (lic.error != null)
            {
                return new DocumentList() { error = lic.error };
            }

            /*             
                проверка пользователя и регистрации
            */
            var user = GetUser(userInfo.login, userInfo.workplace);
            if ((user == null) || (user.error != null))
                return new DocumentList() { error = user.error != null ? user.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь по заданному логину и рабочему месту"), };

            var reg = GetRegistration_Doc(user.user_id);
            if ((reg == null) || (reg.error != null))
                return new DocumentList() { error = reg.error != null ? reg.error : new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрация пользователя"), };

            DocumentList res = new DocumentList();
            DateTime today = DateTime.Today;
            DateTime now = DateTime.Now;
            using (var dbContext = new AuMainDb())
            {
                var doc_list = (from t1 in dbContext.exchange_doc
                                from t2 in dbContext.exchange_doc_target
                                where t1.item_id == t2.exchange_doc_id
                                && t1.sales_id == lic.SalesId
                                && t1.doc_type == docType
                                && (((t2.target_sales_id == salesId) && (salesId > 0)) || (salesId <= 0))
                                && t2.curr_state_id == (int)Enums.ExchangeDocState.SENDED
                                select new Document()
                                {
                                    SalesId = t1.sales_id.HasValue ? (int)t1.sales_id : 0,
                                    SalesName = "",
                                    DocId = t1.doc_id.HasValue ? (int)t1.doc_id : 0,
                                    DocName = t1.doc_name,
                                    DocFormat = t1.doc_format,
                                    DocType = t1.doc_type,
                                    Comment = t1.doc_comment,
                                    DateBeg = t1.date_beg,
                                    DateEnd = t1.date_end,
                                }
                               )
                               .Distinct()
                               .ToList();
                if ((doc_list == null) || (doc_list.Count <= 0))
                {
                    return new DocumentList() { Document_list = null, error = null, };
                    //return new DocumentList() { Document_list = new List<Document>(), error = null, };
                }

                return new DocumentList() { Document_list = new List<Document>(doc_list), error = null, };
            }
        }

        #endregion
    }
}
