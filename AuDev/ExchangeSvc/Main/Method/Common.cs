﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using AuDev.Common.Db.Model;
using AuDev.Common.Util;

namespace ExchangeSvc
{
    public partial class ExchangeService : IExchangeService
    {
        private readonly int license_id_EXCHANGE = 52;
        private readonly int license_id_DOC = 54;

        #region ExchangeClient

        private ExchangeClient GetClient(long client_id)
        {
            using (var context = new AuMainDb())
            {
                //var client = context.exchange_client.Where(ss => ss.client_id == client_id).FirstOrDefault();
                var client = context.exchange_client.Where(ss => ss.cabinet_client_id == client_id).FirstOrDefault();
                if (client == null)
                    return new ExchangeClient() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден клиент - участник обмена с кодом " + client_id.ToString()) };
                return new ExchangeClient(client);
            }
        }

        #endregion

        #region ExchangeUser

        private ExchangeUser GetUser(string login, string workplace)
        {
            using (var context = new AuMainDb())
            {
                var user = context.vw_exchange_user.Where(ss => ss.login.Trim().ToLower().Equals(login.Trim().ToLower())
                    && ss.workplace.Trim().ToLower().Equals(workplace.Trim().ToLower())
                    //&& ss.version_num.Trim().ToLower().Equals(version_num.Trim().ToLower())
                    )
                    .FirstOrDefault();
                if (user == null)
                    return new ExchangeUser() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найден пользователь") };
                return new ExchangeUser(user);
            }
        }

        /*
        private ExchangeUser InsertUser(string login, string workplace, string version_num)
        {
            using (var context = new AuMainDb())
            {
                var user = context.exchange_user.Where(ss => ss.login.Trim().ToLower().Equals(login.Trim().ToLower()) && ss.workplace.Trim().ToLower().Equals(workplace.Trim().ToLower()) && ss.version_num.Trim().ToLower().Equals(version_num.Trim().ToLower())).FirstOrDefault();
                if (user != null)
                    return new ExchangeUser() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_DUPLICATE_OBJECT, "Пользователь с такими атрибутами уже есть") };
                user = new exchange_user();
                user.login = login;
                user.workplace = workplace;
                user.version_num = version_num;

                context.exchange_user.Add(user);
                context.SaveChanges();

                return new ExchangeUser(user);
            }
        }
        */

        #endregion

        #region ExchangeReg

        private ExchangeRegPartner GetRegistration_Partner(long user_id, long ds_id)
        {
            using (var context = new AuMainDb())
            {
                var reg = (from er in context.exchange_reg
                           from eur in context.exchange_user_reg
                           from erp in context.exchange_reg_partner
                           where er.reg_id == eur.reg_id
                           && eur.user_id == user_id
                           && er.reg_type == (int)Enums.ExchangeRegType.PARTNER
                           && er.reg_id == erp.reg_id
                           && erp.ds_id == ds_id
                           && eur.is_active == true
                           select erp)
                          .FirstOrDefault();
                if (reg == null)
                    return new ExchangeRegPartner() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрация для данного пользователя и данного партнера") };
                return new ExchangeRegPartner(reg);
            }
        }

        private ExchangeRegPartnerList GetRegistrationList_Partner(long user_id)
        {
            using (var context = new AuMainDb())
            {
                var reg_list = (from er in context.exchange_reg
                           from eur in context.exchange_user_reg
                           from erp in context.exchange_reg_partner
                           where er.reg_id == eur.reg_id
                           && eur.user_id == user_id
                           && er.reg_type == (int)Enums.ExchangeRegType.PARTNER
                           && er.reg_id == erp.reg_id                           
                           && eur.is_active == true
                           select erp)
                          .ToList();
                if ((reg_list == null) || (reg_list.Count <= 0))
                    return new ExchangeRegPartnerList() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдены регистрации для данного пользователя") };
                return new ExchangeRegPartnerList(reg_list);
            }
        }

        private ExchangeRegDoc GetRegistration_Doc(long user_id)
        {
            using (var context = new AuMainDb())
            {
                var reg = (from er in context.exchange_reg
                           from eur in context.exchange_user_reg
                           from erp in context.exchange_reg_doc
                           where er.reg_id == eur.reg_id
                           && eur.user_id == user_id
                           && er.reg_type == (int)Enums.ExchangeRegType.DOC
                           && er.reg_id == erp.reg_id                           
                           && eur.is_active == true
                           select erp)
                          .FirstOrDefault();
                if (reg == null)
                    return new ExchangeRegDoc() { error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_NOT_FOUND, "Не найдена регистрация для данного пользователя") };
                return new ExchangeRegDoc(reg);
            }
        }

        #endregion

        #region DataSource

        private long GetDefaultDsId()
        {
            return (long)Enums.DataSourceEnum.MZ;
        }

        #endregion

        #region SendMail

        private ServiceOperationResult xSendMail(string text, string topic, string sendTo, string sendFrom)
        {
            /*
            return new ServiceOperationResult()
            {
                error = new ErrInfo((long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION, "Метод не реализован"),
                Id = (long)Enums.ErrCodeEnum.ERR_CODE_EXCEPTION,
            };
            */

            string sender_smtp_address = "smtp.gmail.com";
            int sender_smtp_port = 587;
            bool sender_smtp_ssl = true;
            string sender_address = "cab.aptekaural@gmail.com";
            string sender_login = "cab.aptekaural@gmail.com";
            string sender_alias = String.IsNullOrEmpty(sendFrom) ? "cab.aptekaural" : sendFrom.Trim();
            string sender_password = "aptekaural";

            DateTime now = DateTime.Now;
            using (SmtpClient c = new SmtpClient(sender_smtp_address, sender_smtp_port))
            {
                MailAddress add = new MailAddress(sendTo);
                MailMessage msg = new MailMessage();
                msg.To.Add(add);
                msg.From = new MailAddress(sender_address, sender_alias);
                msg.IsBodyHtml = false;
                msg.Subject = topic;
                msg.Body = text;
                c.Credentials = new System.Net.NetworkCredential(sender_login, sender_password);
                c.EnableSsl = sender_smtp_ssl;
                c.Send(msg);
            }

            return new ServiceOperationResult()
            {
                Id = 0,
                error = null,
            };
        }

        #endregion
    }
}
