﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.ServiceModel.Web;
using AuDev.Common.WCF;

namespace ExchangeSvc
{    
    [ServiceContract(Namespace="http://services.aurit.ru/exchange/")]
    [WsdlDocumentation("Сервис обмена данными между клиентами АУ и партнерами")]
    public interface IExchangeService
    {        

        #region GetXml

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает XML Список товаров центрального планирования")]
        [return: WsdlParamOrReturnDocumentation("Возвращает структуру ExchangeXml")]
        ExchangeXml GetPlanXml([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает XML Скидки товаров по акциям")]
        [return: WsdlParamOrReturnDocumentation("Возвращает структуру ExchangeXml")]
        ExchangeXml GetDiscountXml([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]                
        ExchangeXml GetRecommendXml(UserInfo userInfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        string Test_GetXml(string pwd, string filePath, int exchangeTypeInt);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ExchangeXml Test_GetXml2(string pwd, UserInfo userInfo, int exchangeTypeInt);

        #endregion

        #region SendData

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ServiceOperationResult SendOstData(UserInfo userInfo, string data);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ServiceOperationResult SendMovData(UserInfo userInfo, string data);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ServiceOperationResult SendQueData(UserInfo userInfo, string data);

        /// <summary>
        /// Передача справочника контрагентов
        /// </summary>        
        [OperationContract]        
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]        
        ServiceOperationResult SendClientData(UserInfo userInfo, string data);

        /// <summary>
        /// Передача справочника номенклатуры
        /// </summary>    
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ServiceOperationResult SendPrepData(UserInfo userInfo, string data);

        /// <summary>
        /// Передача данных накопительным итогом
        /// </summary>    
        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ServiceOperationResult SendAvgData(UserInfo userInfo, string data);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Отправляет данные обмена data по параметрам userInfo, dataInfo")]
        [return: WsdlParamOrReturnDocumentation("Возвращает объект типа ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - ок, > 0 - код ошибки), error - информация об ошибке (если есть)"
            )]
        ServiceOperationResult SendData([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Данные обмена")]string data,
            [WsdlParamOrReturnDocumentation("Структура DataInfo. Атрибуты: item_type_id - тип пакета, item_file_name - название файла, is_packed - если 1 - запаковано в ZIP")]DataInfo dataInfo
            );

        #endregion

        #region GetExchangeData

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ExchangeData GetExchangeData(UserInfo userInfo, long exchange_id);

        #endregion

        #region GetSendAll

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        int GetSendAll(UserInfo userInfo);

        #endregion

        #region GetParams

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Получает информацию о параметрах обмена для заданного userInfo в разрезе Партнеров")]
        [return: WsdlParamOrReturnDocumentation("Возвращает структуру ExchangeParamsList - список объектов типа ExchangeParams."
            + " Основные атрибуты: ds_id - код Партнера, ds_name - наименование Партнера, send_all - признак 'отправлять все', days_cnt_for_mov - кол-во дней для выгрузки движения"
            + ", login - логин пользователя из userInfo, workplace - код рабочего места из userInfo, reg_code - код участника в партнерской программе"
            + ", reg_name - название участника в партнерской программе, sales_prefix - префикс торговой точки, exchange_item_list - пакеты для обмена, список объектов типа ExchangeItem."
            + " Основные атрибуты ExchangeItem: item_id - код пакета, item_name - наименование пакета, item_shortname - краткое наименование, for_send - если true, то пакет для передачи Партнеру"
            + ", for_get - если true, то пакет для получения от Партнера, item_type_id - код типа пакета, item_type_name - наименование типа пакета"
            + ", item_file_name - наименование файла пакета, sql_text - SQL-команда для формирования данных пакета, param - параметр для формирования пакета"
            + ", method_id - метод для отправки пакета"
            )]
        ExchangeParamsList GetParams([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo);

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        ExchangeParamsList GetParams_SalePoint(UserInfo userInfo, int sale_id);

        #endregion

        #region GetPartnerList

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        PartnerList GetPartnerList();        

        #endregion

        #region SendToExchangeLog

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Отправляет сообщение в серверный журнал обмена")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SendToExchangeLog([WsdlParamOrReturnDocumentation("Структура userInfo. Обязательны к заполнению все атрибуты")]UserInfo userInfo
            , [WsdlParamOrReturnDocumentation("Текст сообщения")]string text
            , [WsdlParamOrReturnDocumentation("Внутренний код сообщения")]int id
            , [WsdlParamOrReturnDocumentation("1 - ошибка, 0 - нет")]int is_error
            , [WsdlParamOrReturnDocumentation("Направление обмена: 0 - от Клиента Партнеру, 1 - от Партнера Клиенту")]int exchange_direction
            , [WsdlParamOrReturnDocumentation("Тип файла обмена: 0 - не определено, 1 - Остатки, 2 - Движения, 3 - Заказ, 4 - Товары ЦП, 5 - Товары по акциям"
                + ", 6 - Товары ПР, 7 - Справочник КА, 8 - Справочник номенклатуры, 9 - Данные накопительным итогом"
                )]int exchange_type
            );

        #endregion

        #region UpdateExchangePartnerReg

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Обновление монитора обмена")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult UpdateExchangePartnerReg([WsdlParamOrReturnDocumentation("Пароль")]string password,
            [WsdlParamOrReturnDocumentation("Структура userInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("True - обновлять данные по отправке")]bool update_upload,
            [WsdlParamOrReturnDocumentation("True - обновлять данные по приему")]bool update_download
            );
        
        #endregion

        #region ExchangeTask

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает список выданных задач на обмен для данного пользователя")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult, список объектов ExchangeTask."
            + " Основные атрибуты: task_id - код задания, task_type_id - тип задания (1 - отправка данных за период, 2 - прием данных)"
            + ", param_date_beg - с какой даты отправлять (для task_type_id = 1), param_date_end - по какую дату отправлять (для task_type_id = 1)"
            + ", state - статус задания (0 - выдано, 1 - получено, 2 - выполнено с ошибками, 3 - выполнено без ошибок)"
            + ", ds_id - код Партнера, для которого выдана задача"
            + ", state_date - дата-время статуса"
            )]
        ExchangeTaskList GetExchangeTaskList([WsdlParamOrReturnDocumentation("Структура userInfo. Обязательны к заполнению все атрибуты кроме ds_id")]UserInfo userInfo
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Выставляет у выданного задания статус Получено")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SetExchangeTask_Received([WsdlParamOrReturnDocumentation("Структура userInfo. Обязательны к заполнению все атрибуты кроме ds_id")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Код задания, у которого нужно проставить статус Получено")]long task_id
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Выставляет у списка выданных заданий статус Получено")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SetExchangeTaskList_Received([WsdlParamOrReturnDocumentation("Структура userInfo. Обязательны к заполнению все атрибуты кроме ds_id")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Список заданий, у которых нужно проставить статус Получено")]ExchangeTaskList taskList
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Выставляет у выданного задания статус Выполнено с ошибками")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SetExchangeTask_Fail([WsdlParamOrReturnDocumentation("Структура userInfo. Обязательны к заполнению все атрибуты кроме ds_id")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Код задания, у которого нужно проставить статус Выполнено с ошибками")]long task_id,
            [WsdlParamOrReturnDocumentation("Комментарий, почему данное задание выполнено с ошибкой")]string comment
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Выставляет у списка выданных заданий статус Выполнено с ошибками")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SetExchangeTaskList_Fail([WsdlParamOrReturnDocumentation("Структура userInfo. Обязательны к заполнению все атрибуты кроме ds_id")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Список заданий, у которых нужно проставить статус Выполнено с ошибками."
            + " В атрибуте comment нужно указать комментарий, почему данное задание выполнено с ошибкой")]ExchangeTaskList taskList
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Выставляет у выданного задания статус Выполнено без ошибок")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SetExchangeTask_Success([WsdlParamOrReturnDocumentation("Структура userInfo. Обязательны к заполнению все атрибуты кроме ds_id")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Код задания, у которого нужно проставить статус Выполнено без ошибок")]long task_id
            );

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Выставляет у списка выданных заданий статус Выполнено без ошибок")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SetExchangeTaskList_Success([WsdlParamOrReturnDocumentation("Структура userInfo. Обязательны к заполнению все атрибуты кроме ds_id")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Список заданий, у которых нужно проставить статус Выполнено без ошибок")]ExchangeTaskList taskList
            );


        #endregion


        #region GetSalesList

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает список торговых точек указанного в userInfo клиента, на которые можно отсылать документы")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура SalesList, список объектов Sales."
            + " Атрибуты объекта Sales: SalesId - код точки, SalesName - наименование точки"
            )]
        SalesList GetSalesList([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo);

        #endregion

        #region SendDocument

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Отправляет на указанную торговую точку salesId документ doc. Если salesId <= 0 - документ отправится на все торговые точки указанного в userInfo клиента, на которые можно отсылать документы")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SendDocument([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo
            , [WsdlParamOrReturnDocumentation("Структура Document - отсылаемый документ"
                + " Атрибуты: DocId - код док-та, DocName - заголовок док-та, DocType - тип документа (0 - формат накладных АУ, 1 - формат Маркетолога)"
                + ", DocFormat - формат док-та (xml, zip), ContentAsBytes - тело док-та в виде байт-массива (не используется)"
                + ", ContentAsString - тело док-та в виде строки, State - код статуса документа (не заполнять), Comment - комментарий (не обязательно), DateBeg - дата начала документа (не обязательно), DateEnd - дата окончания документа (не обязательно)"
                )]Document doc
            , [WsdlParamOrReturnDocumentation("Код торговой точки, куда отсылать документ. Если <= 0 - документ отправится на все торговые точки указанного в userInfo клиента, на которые можно отсылать документы")]int salesId
            , [WsdlParamOrReturnDocumentation("Если True - документ будет отправлен на электронные адреса, указанные у каждой торговой точки-полчуаетля")]bool toEmail
            );

        #endregion

        #region GetDocuments

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Получает все документы со статусом 'не получены', которые в качестве адресата имеют код данной торговой точки")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура DocumentList, список объектов Document."
            + " Атрибуты объекта Document: DocId - код док-та, DocName - заголовок док-та, DocType - тип документа (0 - формат накладных АУ, 1 - формат Маркетолога (данные), 2 - формат Маркетолога (настройки))"
            + ", DocFormat - формат док-та (xml, zip), ContentAsBytes - тело док-та в виде байт-массива (не используется)"
            + ", ContentAsString - тело док-та в виде строки, State - код статуса документа, Comment - комментарий, DateBeg - дата начала документа, DateEnd - дата окончания документа"
            )]
        DocumentList GetDocuments([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Тип документа (0 - формат накладных АУ, 1 - формат Маркетолога)")]int docType
            );

        #endregion

        #region ChangeDocumentState

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Устанавливает на всех документах в списке docList указанный статус state")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура DocumentList, список объектов Document." 
            + " Атрибуты объекта Document: DocId - код док-та, DocName - заголовок док-та, DocFormat - формат док-та (xml, zip)"
            + ", State - код статуса документа, Comment - комментарий, DateBeg - дата начала документа, DateEnd - дата окончания документа"
            )]
        DocumentList ChangeDocumentState([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo
            , [WsdlParamOrReturnDocumentation("Список документов, у которых нужно сменить статус. Обязятельно к заполнению у каждого документа поля: DocId, SalesId")]DocumentList docList
            , [WsdlParamOrReturnDocumentation("Код статуса, который нужно установить у заданного списка документов. Возможные значения: 0 - отправлен, 1 - получен, 2 - загружен, 3 - требуется переотправка")]int nextState
            );

        #endregion

        #region GetDocumentStates

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает список статусов в разрезе торговых точек-получателей заданного списка документов docList, отправленных с данной торговой точки")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура DocumentList, список объектов Document."
            + " Заполненные атрибуты объекта Document: DocId - код док-та, DocName - заголовок док-та, DocFormat - формат док-та (xml, zip)"
            + ", Comment - комментарий, DateBeg - дата начала документа, DateEnd - дата окончания документа, DocumentStateList - список статусов документа в разрезе торговых точек-получателей"
            )]
        DocumentList GetDocumentStates([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo
            , [WsdlParamOrReturnDocumentation("Список документов, у которых нужно узнать статусы. Обязятельны к заполнению поля у каждого документа: DocId, SalesId")]DocumentList docList
            );

        #endregion

        #region GetNotReceivedDocuments

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Возвращает список неполученных документов заданного типа, отправленных ранее с данной торговой точки")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура DocumentList, список объектов Document."
            + " Заполненные атрибуты объекта Document: DocId - код док-та, DocName - заголовок док-та, DocFormat - формат док-та (xml, zip)"
            + ", Comment - комментарий, DateBeg - дата начала документа, DateEnd - дата окончания документа"
            )]
        DocumentList GetNotReceivedDocuments([WsdlParamOrReturnDocumentation("Структура UserInfo")]UserInfo userInfo,
            [WsdlParamOrReturnDocumentation("Тип документов, по которым нужно получить статус (0 - формат накладных АУ, 1 - формат Маркетолога)")]int docType,
            [WsdlParamOrReturnDocumentation("Код торговой точки - получателя, по которой нужно получить статус документов."
            + " Если salesId <= 0 - метод вернет документы, неполученные хотя бы одной торговой точкой из всех точек, куда эти документы были отправлены")]int salesId
            );

        #endregion


        #region SendMail

        [OperationContract]
        [WebInvoke(Method = "POST",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json
        )]
        [WsdlDocumentation("Отправляет письмо с указанным текстом и темой на указанный адрес от указанного отправителя")]
        [return: WsdlParamOrReturnDocumentation("Выходной параметр - структура ServiceOperationResult."
            + " Атрибуты: Id - код результата (0 - успех, <> 0 - код ошибки), error - информация об ошибке"
            )]
        ServiceOperationResult SendMail([WsdlParamOrReturnDocumentation("Текст письма")]string text
            , [WsdlParamOrReturnDocumentation("Тема письма")]string topic
            , [WsdlParamOrReturnDocumentation("Кому отправить")]string sendTo
            , [WsdlParamOrReturnDocumentation("От кого отправить")]string sendFrom
            );

        #endregion

        #region JSON Support

        [OperationContract]
        [WebInvoke(Method = "OPTIONS", UriTemplate = "*")]
        void GetOptions();

        #endregion
    }
}
