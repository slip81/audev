﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Reflection;
using System.ComponentModel.DataAnnotations; 

namespace ExchangeSvc
{
    public static class EnumExtensions
    {
        public static T GetAttribute<T>(this Enum enumValue)
        where T : Attribute
        {
            return enumValue.GetType()
                            .GetMember(enumValue.ToString())
                            .First()
                            .GetCustomAttribute<T>();
        }

        public static string EnumName<T>(this int? intValue)
        {
            #region ExchangeTypeEnum
            if (typeof(T) == typeof(Enums.ExchangeItemEnum))
            {
                Enums.ExchangeItemEnum exType = Enums.ExchangeItemEnum.Avg;
                try
                {
                    exType = (Enums.ExchangeItemEnum)intValue;
                    return exType.GetAttribute<DisplayAttribute>().Name;
                }
                catch
                {
                    return "[-]";
                }
            }
            #endregion

            return "[-]";
        }

        public static string EnumName<T>(this int intValue)
        {
            return ((int?)intValue).EnumName<T>();
        }

    }
}