﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.IO;
using System.Data;
using System.Data.OleDb;
//using System.Data.Entity;
using System.Data.EntityClient;
using System.Configuration;
using System.Globalization;
using AuDev.Common.Util;

namespace ExchangeSvc
{
    public static class ExchangeUtil
    {
        
        public static string GetCabinetDbConnectionString()
        {
            string entityConnectionString = ConfigurationManager.ConnectionStrings["CabinetDb"].ConnectionString;
            return new EntityConnectionStringBuilder(entityConnectionString).ProviderConnectionString;
        }


        public static string GetDocStateName(Enums.ExchangeDocState state)
        {
            switch (state)
            {
                case Enums.ExchangeDocState.SENDED:
                    return "Отправлен";
                case Enums.ExchangeDocState.RECEIVED:
                    return "Получен";
                case Enums.ExchangeDocState.LOADED:
                    return "Загружен";
                case Enums.ExchangeDocState.NEED_RESEND:
                    return "Требуется переотправка";
                default:
                    return "[unknown]";
            }    
        }


          
    }
}
