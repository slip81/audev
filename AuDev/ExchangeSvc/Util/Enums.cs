﻿using System.ComponentModel.DataAnnotations;

namespace ExchangeSvc
{    
    public static class Enums
    {
        public enum ErrCodeEnum
        {
            ERR_CODE_EXCEPTION = 100,
            ERR_CODE_NOT_FOUND = 101,
            ERR_CODE_DUPLICATE_OBJECT = 102,
            ERR_CODE_ATTR_NOT_FOUND = 103,
            ERR_CODE_ADD_DENIED = 104,

            ERR_CODE_LIC_NOT_FOUND = 301,
            ERR_CODE_LIC_SALES_NOT_FOUND = 302,
            ERR_CODE_LIC_SALES_MORE_THAN_ONE = 303,
            ERR_CODE_LIC_WORKPLACE_NOT_FOUND = 304,
            ERR_CODE_LIC_WORKPLACE_MORE_THAN_ONE = 305,

            ERR_CODE_LOGIN_NOT_FOUND = 401,
            ERR_CODE_WORKPLACE_NOT_FOUND = 402,
            ERR_CODE_PARTNER_NOT_FOUND = 403,
            ERR_CODE_REG_PARTNER_NOT_FOUND = 404,
            
        }

        public enum LogEventType
        {            
            ERROR = 101,

            DOWNLOAD_DEFECT = 2,
            MAKE_DEFECT_XML = 3,

            GET_EXCHANGE_XML = 4,
            SEND_EXCHANGE_DATA = 5,
            MAKE_EXCHANGE_FILE = 6,

            DOC_SEND = 7,
            DOC_RECEIVE = 8,
            DOC_CHANGE_STATE = 9,

            DOWNLOAD_VED = 10,

            UDPATE_EXCHANGE_DATA = 11,
        }

        public enum DataSourceEnum
        {
            MZ = 101,
            ASNA = 102,
        }

        public enum ExchangeItemEnum
        {
            [Display(Name = "Не определено")]
            None = 0,
            [Display(Name = "Остатки")]
            Ost = 1,
            [Display(Name = "Движения")]
            Move = 2,
            [Display(Name = "Заказ")]
            Que = 3,
            [Display(Name = "Товары ЦП")]
            Plan = 4,
            [Display(Name = "Товары по акциям")]
            Discount = 5,
            [Display(Name = "Товары ПР")]
            Recommend = 6,
            [Display(Name = "Справочник КА")]
            Client = 7,
            [Display(Name = "Справочник номенклатуры")]
            Prep = 8,
            [Display(Name = "Данные накопительным итогом")]
            Avg = 9,
        }

        public enum ExchangeFormatType
        {
            TextFile = 1,
            XML = 2,            
        }

        public enum ExchangeTransportType
        {
            FTP = 1,
            HTTP = 2,
            Email = 3,
        }

        public enum ExchangeRegType
        {
            PARTNER = 1,
            DOC = 2,
        }

        public enum ArcType
        {
            NONE = 0,
            ZIP = 1,
            RAR = 2,
            SEVEN_ZIP = 3,
        }

        public enum ExchangeDocState
        {
            SENDED = 0,
            RECEIVED = 1,
            LOADED = 2,
            NEED_RESEND = 3,
        }

        public enum LogScope
        {
            DISCOUNT = 1,
            CABINET = 2,
            DEFECT = 3,
            EXCHANGE_PARTNER = 4,
            EXCHANGE_DOC = 5,
            VED = 6,
        }

        public enum ExchangeDirectionEnum
        {
            [Display(Name = "Передача от Клиента Партнеру")]
            FROM_CLIENT_TO_PARTNER = 0,
            [Display(Name = "Передача от Партнера Клиенту")]
            FROM_PARTNER_TO_CLIENT = 1,
        }
        
        public enum ExchangeTaskState
        {
            [Display(Name = "Выдано")]
            CREATED = 0,
            [Display(Name = "Получено")]
            RECEIVED = 1,
            [Display(Name = "Выполнено с ошибками")]
            DONE_ERROR = 2,
            [Display(Name = "Выполнено без ошибок")]
            DONE_SUCCESS = 3,
        }
        
    }
}
