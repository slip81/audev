﻿namespace ExchangeTestClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.radButton18 = new Telerik.WinControls.UI.RadButton();
            this.radButton17 = new Telerik.WinControls.UI.RadButton();
            this.radCheckBox2 = new Telerik.WinControls.UI.RadCheckBox();
            this.radCheckBox1 = new Telerik.WinControls.UI.RadCheckBox();
            this.radButton16 = new Telerik.WinControls.UI.RadButton();
            this.radButton15 = new Telerik.WinControls.UI.RadButton();
            this.radButton9 = new Telerik.WinControls.UI.RadButton();
            this.radButton8 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox6 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton14 = new Telerik.WinControls.UI.RadButton();
            this.radDropDownList1 = new Telerik.WinControls.UI.RadDropDownList();
            this.radButton13 = new Telerik.WinControls.UI.RadButton();
            this.radButton12 = new Telerik.WinControls.UI.RadButton();
            this.radButton11 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox5 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton10 = new Telerik.WinControls.UI.RadButton();
            this.radTextBox4 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox3 = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox2 = new Telerik.WinControls.UI.RadTextBox();
            this.radButton7 = new Telerik.WinControls.UI.RadButton();
            this.radButton4 = new Telerik.WinControls.UI.RadButton();
            this.radButton5 = new Telerik.WinControls.UI.RadButton();
            this.radButton6 = new Telerik.WinControls.UI.RadButton();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radTextBox1 = new Telerik.WinControls.UI.RadTextBox();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Controls.Add(this.splitPanel3);
            this.radSplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer1.Name = "radSplitContainer1";
            this.radSplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(763, 546);
            this.radSplitContainer1.TabIndex = 0;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.radButton18);
            this.splitPanel1.Controls.Add(this.radButton17);
            this.splitPanel1.Controls.Add(this.radCheckBox2);
            this.splitPanel1.Controls.Add(this.radCheckBox1);
            this.splitPanel1.Controls.Add(this.radButton16);
            this.splitPanel1.Controls.Add(this.radButton15);
            this.splitPanel1.Controls.Add(this.radButton9);
            this.splitPanel1.Controls.Add(this.radButton8);
            this.splitPanel1.Controls.Add(this.radTextBox6);
            this.splitPanel1.Controls.Add(this.radButton14);
            this.splitPanel1.Controls.Add(this.radDropDownList1);
            this.splitPanel1.Controls.Add(this.radButton13);
            this.splitPanel1.Controls.Add(this.radButton12);
            this.splitPanel1.Controls.Add(this.radButton11);
            this.splitPanel1.Controls.Add(this.radTextBox5);
            this.splitPanel1.Controls.Add(this.radButton10);
            this.splitPanel1.Controls.Add(this.radTextBox4);
            this.splitPanel1.Controls.Add(this.radTextBox3);
            this.splitPanel1.Controls.Add(this.radTextBox2);
            this.splitPanel1.Controls.Add(this.radButton7);
            this.splitPanel1.Controls.Add(this.radButton4);
            this.splitPanel1.Controls.Add(this.radButton5);
            this.splitPanel1.Controls.Add(this.radButton6);
            this.splitPanel1.Controls.Add(this.radButton3);
            this.splitPanel1.Controls.Add(this.radButton2);
            this.splitPanel1.Controls.Add(this.radButton1);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(763, 224);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.08302353F);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 53);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // radButton18
            // 
            this.radButton18.Location = new System.Drawing.Point(635, 188);
            this.radButton18.Name = "radButton18";
            this.radButton18.Size = new System.Drawing.Size(116, 28);
            this.radButton18.TabIndex = 27;
            this.radButton18.Text = "Test2";
            this.radButton18.Click += new System.EventHandler(this.radButton18_Click);
            // 
            // radButton17
            // 
            this.radButton17.Location = new System.Drawing.Point(493, 188);
            this.radButton17.Name = "radButton17";
            this.radButton17.Size = new System.Drawing.Size(116, 28);
            this.radButton17.TabIndex = 26;
            this.radButton17.Text = "SendMail";
            this.radButton17.Click += new System.EventHandler(this.radButton17_Click);
            // 
            // radCheckBox2
            // 
            this.radCheckBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox2.Location = new System.Drawing.Point(348, 198);
            this.radCheckBox2.Name = "radCheckBox2";
            this.radCheckBox2.Size = new System.Drawing.Size(108, 18);
            this.radCheckBox2.TabIndex = 25;
            this.radCheckBox2.Text = "update download";
            this.radCheckBox2.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radCheckBox1
            // 
            this.radCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.radCheckBox1.Location = new System.Drawing.Point(348, 176);
            this.radCheckBox1.Name = "radCheckBox1";
            this.radCheckBox1.Size = new System.Drawing.Size(93, 18);
            this.radCheckBox1.TabIndex = 24;
            this.radCheckBox1.Text = "update upload";
            this.radCheckBox1.ToggleState = Telerik.WinControls.Enumerations.ToggleState.On;
            // 
            // radButton16
            // 
            this.radButton16.Location = new System.Drawing.Point(156, 176);
            this.radButton16.Name = "radButton16";
            this.radButton16.Size = new System.Drawing.Size(186, 28);
            this.radButton16.TabIndex = 23;
            this.radButton16.Text = "UpdateExchangePartnerReg";
            this.radButton16.Click += new System.EventHandler(this.radButton16_Click);
            // 
            // radButton15
            // 
            this.radButton15.Location = new System.Drawing.Point(12, 176);
            this.radButton15.Name = "radButton15";
            this.radButton15.Size = new System.Drawing.Size(116, 28);
            this.radButton15.TabIndex = 22;
            this.radButton15.Text = "GetTaskList";
            this.radButton15.Click += new System.EventHandler(this.radButton15_Click);
            // 
            // radButton9
            // 
            this.radButton9.Location = new System.Drawing.Point(635, 142);
            this.radButton9.Name = "radButton9";
            this.radButton9.Size = new System.Drawing.Size(116, 28);
            this.radButton9.TabIndex = 21;
            this.radButton9.Text = "Test";
            this.radButton9.Click += new System.EventHandler(this.radButton9_Click);
            // 
            // radButton8
            // 
            this.radButton8.Location = new System.Drawing.Point(493, 142);
            this.radButton8.Name = "radButton8";
            this.radButton8.Size = new System.Drawing.Size(116, 28);
            this.radButton8.TabIndex = 20;
            this.radButton8.Text = "CabinetServ";
            this.radButton8.Click += new System.EventHandler(this.radButton8_Click);
            // 
            // radTextBox6
            // 
            this.radTextBox6.Location = new System.Drawing.Point(144, 146);
            this.radTextBox6.Name = "radTextBox6";
            this.radTextBox6.Size = new System.Drawing.Size(55, 20);
            this.radTextBox6.TabIndex = 19;
            this.radTextBox6.Text = "207";
            // 
            // radButton14
            // 
            this.radButton14.Location = new System.Drawing.Point(12, 142);
            this.radButton14.Name = "radButton14";
            this.radButton14.Size = new System.Drawing.Size(116, 28);
            this.radButton14.TabIndex = 18;
            this.radButton14.Text = "SendDocument";
            this.radButton14.Click += new System.EventHandler(this.radButton14_Click);
            // 
            // radDropDownList1
            // 
            this.radDropDownList1.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            radListDataItem1.Text = "МЗ";
            radListDataItem2.Text = "АСНА";
            this.radDropDownList1.Items.Add(radListDataItem1);
            this.radDropDownList1.Items.Add(radListDataItem2);
            this.radDropDownList1.Location = new System.Drawing.Point(544, 15);
            this.radDropDownList1.Name = "radDropDownList1";
            this.radDropDownList1.Size = new System.Drawing.Size(187, 20);
            this.radDropDownList1.TabIndex = 17;
            // 
            // radButton13
            // 
            this.radButton13.Location = new System.Drawing.Point(544, 74);
            this.radButton13.Name = "radButton13";
            this.radButton13.Size = new System.Drawing.Size(116, 28);
            this.radButton13.TabIndex = 16;
            this.radButton13.Text = "SendPrepData";
            this.radButton13.Click += new System.EventHandler(this.radButton13_Click);
            // 
            // radButton12
            // 
            this.radButton12.Location = new System.Drawing.Point(410, 74);
            this.radButton12.Name = "radButton12";
            this.radButton12.Size = new System.Drawing.Size(116, 28);
            this.radButton12.TabIndex = 15;
            this.radButton12.Text = "SendClientData";
            this.radButton12.Click += new System.EventHandler(this.radButton12_Click);
            // 
            // radButton11
            // 
            this.radButton11.Location = new System.Drawing.Point(12, 108);
            this.radButton11.Name = "radButton11";
            this.radButton11.Size = new System.Drawing.Size(116, 28);
            this.radButton11.TabIndex = 14;
            this.radButton11.Text = "GetParams";
            this.radButton11.Click += new System.EventHandler(this.radButton11_Click);
            // 
            // radTextBox5
            // 
            this.radTextBox5.Location = new System.Drawing.Point(676, 78);
            this.radTextBox5.Name = "radTextBox5";
            this.radTextBox5.Size = new System.Drawing.Size(55, 20);
            this.radTextBox5.TabIndex = 13;
            this.radTextBox5.Text = "20576";
            // 
            // radButton10
            // 
            this.radButton10.Location = new System.Drawing.Point(144, 108);
            this.radButton10.Name = "radButton10";
            this.radButton10.Size = new System.Drawing.Size(116, 28);
            this.radButton10.TabIndex = 12;
            this.radButton10.Text = "GetExchangeData";
            this.radButton10.Click += new System.EventHandler(this.radButton10_Click);
            // 
            // radTextBox4
            // 
            this.radTextBox4.Location = new System.Drawing.Point(410, 14);
            this.radTextBox4.Name = "radTextBox4";
            this.radTextBox4.Size = new System.Drawing.Size(116, 20);
            this.radTextBox4.TabIndex = 10;
            this.radTextBox4.Text = "3.2.0.0";
            // 
            // radTextBox3
            // 
            this.radTextBox3.Location = new System.Drawing.Point(144, 12);
            this.radTextBox3.Name = "radTextBox3";
            this.radTextBox3.Size = new System.Drawing.Size(251, 20);
            this.radTextBox3.TabIndex = 9;
            this.radTextBox3.Text = "EA5B-678A-9798-E6CB";
            // 
            // radTextBox2
            // 
            this.radTextBox2.Location = new System.Drawing.Point(12, 12);
            this.radTextBox2.Name = "radTextBox2";
            this.radTextBox2.Size = new System.Drawing.Size(116, 20);
            this.radTextBox2.TabIndex = 8;
            this.radTextBox2.Text = "test_3304";
            // 
            // radButton7
            // 
            this.radButton7.Location = new System.Drawing.Point(279, 108);
            this.radButton7.Name = "radButton7";
            this.radButton7.Size = new System.Drawing.Size(116, 28);
            this.radButton7.TabIndex = 6;
            this.radButton7.Text = "GetPartnerList";
            this.radButton7.Click += new System.EventHandler(this.radButton7_Click);
            // 
            // radButton4
            // 
            this.radButton4.Location = new System.Drawing.Point(279, 74);
            this.radButton4.Name = "radButton4";
            this.radButton4.Size = new System.Drawing.Size(116, 28);
            this.radButton4.TabIndex = 5;
            this.radButton4.Text = "SendQueData";
            this.radButton4.Click += new System.EventHandler(this.radButton4_Click);
            // 
            // radButton5
            // 
            this.radButton5.Location = new System.Drawing.Point(144, 74);
            this.radButton5.Name = "radButton5";
            this.radButton5.Size = new System.Drawing.Size(116, 28);
            this.radButton5.TabIndex = 4;
            this.radButton5.Text = "SendMovData";
            this.radButton5.Click += new System.EventHandler(this.radButton5_Click);
            // 
            // radButton6
            // 
            this.radButton6.Location = new System.Drawing.Point(12, 74);
            this.radButton6.Name = "radButton6";
            this.radButton6.Size = new System.Drawing.Size(116, 28);
            this.radButton6.TabIndex = 3;
            this.radButton6.Text = "SendOstData";
            this.radButton6.Click += new System.EventHandler(this.radButton6_Click);
            // 
            // radButton3
            // 
            this.radButton3.Location = new System.Drawing.Point(279, 40);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(116, 28);
            this.radButton3.TabIndex = 2;
            this.radButton3.Text = "GetRecommendXml";
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            // 
            // radButton2
            // 
            this.radButton2.Location = new System.Drawing.Point(144, 40);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(116, 28);
            this.radButton2.TabIndex = 1;
            this.radButton2.Text = "GetDiscountXml";
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(12, 40);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(116, 28);
            this.radButton1.TabIndex = 0;
            this.radButton1.Text = "GetPlanXml";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radTextBox1);
            this.splitPanel2.Controls.Add(this.radPanel1);
            this.splitPanel2.Location = new System.Drawing.Point(0, 228);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(763, 234);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.1016109F);
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 25);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radTextBox1
            // 
            this.radTextBox1.AutoSize = false;
            this.radTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox1.Location = new System.Drawing.Point(0, 33);
            this.radTextBox1.Multiline = true;
            this.radTextBox1.Name = "radTextBox1";
            this.radTextBox1.Size = new System.Drawing.Size(763, 201);
            this.radTextBox1.TabIndex = 1;
            // 
            // radPanel1
            // 
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(763, 33);
            this.radPanel1.TabIndex = 0;
            this.radPanel1.Text = "radPanel1";
            // 
            // splitPanel3
            // 
            this.splitPanel3.Location = new System.Drawing.Point(0, 466);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(763, 80);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.1846345F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -78);
            this.splitPanel3.TabIndex = 2;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 546);
            this.Controls.Add(this.radSplitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            this.splitPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radCheckBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.RadTextBox radTextBox1;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.RadButton radButton3;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadButton radButton4;
        private Telerik.WinControls.UI.RadButton radButton5;
        private Telerik.WinControls.UI.RadButton radButton6;
        private Telerik.WinControls.UI.RadButton radButton7;
        private Telerik.WinControls.UI.RadTextBox radTextBox3;
        private Telerik.WinControls.UI.RadTextBox radTextBox2;
        private Telerik.WinControls.UI.RadTextBox radTextBox4;
        private Telerik.WinControls.UI.RadTextBox radTextBox5;
        private Telerik.WinControls.UI.RadButton radButton10;
        private Telerik.WinControls.UI.RadButton radButton11;
        private Telerik.WinControls.UI.RadButton radButton13;
        private Telerik.WinControls.UI.RadButton radButton12;
        private Telerik.WinControls.UI.RadDropDownList radDropDownList1;
        private Telerik.WinControls.UI.RadButton radButton14;
        private Telerik.WinControls.UI.RadTextBox radTextBox6;
        private Telerik.WinControls.UI.RadButton radButton8;
        private Telerik.WinControls.UI.RadButton radButton9;
        private Telerik.WinControls.UI.RadButton radButton15;
        private Telerik.WinControls.UI.RadButton radButton16;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox2;
        private Telerik.WinControls.UI.RadCheckBox radCheckBox1;
        private Telerik.WinControls.UI.RadButton radButton17;
        private Telerik.WinControls.UI.RadButton radButton18;
    }
}

