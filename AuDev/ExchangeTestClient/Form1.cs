﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExchangeTestClient
{
    public partial class Form1 : Form
    {
        /*
        private string login = "apteka_ul2";
        private string workplace = "6760-8AB6-D858-8328";
        private string version_num = "3.2.0.0";
        private ExchangeServ.UserInfo userInfo;
        */

        public Form1()
        {
            InitializeComponent();
            //
            System.Net.ServicePointManager.Expect100Continue = false;
            //
            radTextBox1.WordWrap = true;
            radTextBox1.ScrollBars = ScrollBars.Vertical;
            //
            /*
            userInfo = new ExchangeServ.UserInfo() { 
                login = login,
                workplace = workplace,
                version_num = version_num,
            };
            */
            radDropDownList1.SelectedIndex = 0;
        }

        public long DsId
        {
            get
            {
                int ind = radDropDownList1.SelectedIndex;
                switch (ind)
                {
                    case 0:
                        return 101;
                    case 1:
                        return 102;
                    default:
                        return 101;
                }
            }
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            
            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetPlanXml(userInfo);
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = res.Data;
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetDiscountXml(userInfo);
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = res.Data;
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.GetRecommendXml(userInfo);
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = res.Data;
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton6_Click(object sender, EventArgs e)
        {
            var data = radTextBox1.Text;

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.SendOstData(userInfo, data);
                if (res.error != null)
                {
                    //radTextBox1.Text = res.error.Message;
                    MessageBox.Show(res.error.Message);
                }
                else
                {
                    //radTextBox1.Text = res.Data;
                    MessageBox.Show("OK");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton5_Click(object sender, EventArgs e)
        {
            var data = radTextBox1.Text;

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.SendMovData(userInfo, data);
                if (res.error != null)
                {
                    //radTextBox1.Text = res.error.Message;
                    MessageBox.Show(res.error.Message);
                }
                else
                {
                    //radTextBox1.Text = res.Data;
                    MessageBox.Show("OK");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton4_Click(object sender, EventArgs e)
        {
            var data = radTextBox1.Text;

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.SendQueData(userInfo, data);
                if (res.error != null)
                {
                    //radTextBox1.Text = res.error.Message;
                    MessageBox.Show(res.error.Message);
                }
                else
                {
                    //radTextBox1.Text = res.Data;
                    MessageBox.Show("OK");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton7_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            Cursor = Cursors.WaitCursor;
            try
            {
                ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

                var res = serv.GetPartnerList();
                if (res.error != null)
                {
                    radTextBox1.Text = res.error.Message;
                }
                else
                {
                    radTextBox1.Text = string.Join("; ", res.Partner_list.Select(ss => ss.ds_name).ToArray());
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        private void radButton10_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            Cursor = Cursors.WaitCursor;
            try
            {
                var userInfo = new ExchangeServ.UserInfo()
                {
                    login = radTextBox2.Text,
                    workplace = radTextBox3.Text,
                    version_num = radTextBox4.Text,
                    ds_id = DsId,
                };
                ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();                
                var res = serv.GetExchangeData(userInfo, long.Parse(radTextBox5.Text));
                if (res.error != null)
                {
                    radTextBox1.Text += res.error.Message;
                }
                else
                {
                    /*
                    radTextBox1.Text += res.data_binary != null ? Util.GetString(res.data_binary) : "data_binary == null";
                    radTextBox1.Text += System.Environment.NewLine;
                    */
                                        
                    string filePath = @"C:\ex1.txt";
                    System.IO.File.WriteAllBytes(filePath, res.data_binary);
                    radTextBox1.Text += "Файл сохранен как" + filePath;
                    radTextBox1.Text += System.Environment.NewLine;                                        
                    
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton11_Click(object sender, EventArgs e)
        {
            //radTextBox5.Text = DateTime.Now.ToString("yyyyMMddTHHmm");

            radTextBox1.Text = "";
            Cursor = Cursors.WaitCursor;
            try
            {
                var userInfo = new ExchangeServ.UserInfo()
                {
                    login = radTextBox2.Text,
                    workplace = radTextBox3.Text,
                    version_num = radTextBox4.Text,
                    ds_id = 0,
                };
                ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();
                var res = serv.GetParams(userInfo);
                if (res.error != null)
                {
                    radTextBox1.Text += res.error.Message;
                }
                else
                {
                    foreach (var item in res.ExchangeParams_list)
                    {
                        radTextBox1.Text += item.ds_name;
                        radTextBox1.Text += System.Environment.NewLine;
                        foreach (var item2 in item.exchange_item_list.ExchangeItem_list)
                        {
                            radTextBox1.Text += item2.item_name;
                            radTextBox1.Text += System.Environment.NewLine;
                            radTextBox1.Text += item2.for_get.ToString();
                            radTextBox1.Text += System.Environment.NewLine;
                            radTextBox1.Text += item2.for_send.ToString();
                            radTextBox1.Text += System.Environment.NewLine;
                            radTextBox1.Text += "-------------";
                            radTextBox1.Text += System.Environment.NewLine;
                        }
                    }
                    //radTextBox1.Text += res.data_binary != null ? Util.GetString(res.data_binary) : "data_binary == null";
                    //radTextBox1.Text += System.Environment.NewLine;
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton12_Click(object sender, EventArgs e)
        {
            var data = radTextBox1.Text;

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.SendClientData(userInfo, data);
                if (res.error != null)
                {
                    //radTextBox1.Text = res.error.Message;
                    MessageBox.Show(res.error.Message);
                }
                else
                {
                    //radTextBox1.Text = res.Data;
                    MessageBox.Show("OK");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton13_Click(object sender, EventArgs e)
        {
            var data = radTextBox1.Text;

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.SendPrepData(userInfo, data);
                if (res.error != null)
                {
                    //radTextBox1.Text = res.error.Message;
                    MessageBox.Show(res.error.Message);
                }
                else
                {
                    //radTextBox1.Text = res.Data;
                    MessageBox.Show("OK");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton14_Click(object sender, EventArgs e)
        {
            var data = radTextBox1.Text;

            int sales_id = 0;
            try
            {
                sales_id = int.Parse(radTextBox6.Text);
            }
            catch
            {
                sales_id = 0;
            }

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = 0,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                ExchangeServ.Document doc = new ExchangeServ.Document() { DocId = 101, DocName = "Док 101", ContentAsString = data, };
                var res = serv.SendDocument(userInfo, doc, sales_id, true);
                if (res.error != null)
                {
                    //radTextBox1.Text = res.error.Message;
                    MessageBox.Show(res.error.Message);
                }
                else
                {
                    //radTextBox1.Text = res.Data;
                    MessageBox.Show("OK");
                }
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton8_Click(object sender, EventArgs e)
        {

            CabinetServ.CabinetServiceClient serv = new CabinetServ.CabinetServiceClient();
            
            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.ChangeMoneyMonthlyPeriodAndMoveCardsToActiveStatus("iguana");
                if (!res)
                {                    
                    MessageBox.Show("error!");
                }
                else
                {                    
                    MessageBox.Show("OK");
                }                
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton9_Click(object sender, EventArgs e)
        {
            //DateTime dt = Convert.ToDateTime("Jan 28 2016");
            //MessageBox.Show(dt.ToString("dd.MM.yyyy"));

            radTextBox1.Text = "";

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                var res = serv.Test_GetXml("iguana", @"C:\__01\123#ekt_fl@a6$CP_list$20160706_170148.txt", 4);
                radTextBox1.Text = res;
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }

        private void radButton15_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            Cursor = Cursors.WaitCursor;
            try
            {
                var userInfo = new ExchangeServ.UserInfo()
                {
                    login = radTextBox2.Text,
                    workplace = radTextBox3.Text,
                    version_num = radTextBox4.Text,
                    ds_id = 0,
                };
                ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();
                var res = serv.GetExchangeTaskList(userInfo);
                if (res.error != null)
                {
                    radTextBox1.Text += res.error.Message;
                }
                else
                {
                    foreach (var item in res.ExchangeTask_list)
                    {
                        radTextBox1.Text += "task_type_id = " + item.task_type_id;
                        radTextBox1.Text += System.Environment.NewLine;
                        radTextBox1.Text += "ds_id = " + item.ds_id;
                        radTextBox1.Text += System.Environment.NewLine;
                        radTextBox1.Text += "state = " + item.state;
                        radTextBox1.Text += System.Environment.NewLine;
                        radTextBox1.Text += "---------";
                        radTextBox1.Text += System.Environment.NewLine;
                    }
                    //radTextBox1.Text += res.data_binary != null ? Util.GetString(res.data_binary) : "data_binary == null";
                    //radTextBox1.Text += System.Environment.NewLine;
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton16_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            Cursor = Cursors.WaitCursor;
            try
            {
                var userInfo = new ExchangeServ.UserInfo()
                {
                    login = radTextBox2.Text,
                    workplace = radTextBox3.Text,
                    version_num = radTextBox4.Text,
                    ds_id = 0,
                };
                ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();
                var res = serv.UpdateExchangePartnerReg("iguana", userInfo, radCheckBox1.Checked, radCheckBox2.Checked);
                if (res.error != null)
                {
                    radTextBox1.Text += res.error.Message;
                }
                else
                {
                    MessageBox.Show("OK");
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton17_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";
            Cursor = Cursors.WaitCursor;
            try
            {
                ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();
                var res = serv.SendMail("Текст письма", "Тема письма", "pavlov.ms.81@gmail.com", "cab.aptekaural@gmail.com");
                if (res.error != null)
                {
                    radTextBox1.Text += res.error.Message;
                }
                else
                {
                    MessageBox.Show("OK");
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void radButton18_Click(object sender, EventArgs e)
        {
            radTextBox1.Text = "";

            var userInfo = new ExchangeServ.UserInfo()
            {
                login = radTextBox2.Text,
                workplace = radTextBox3.Text,
                version_num = radTextBox4.Text,
                ds_id = DsId,
            };
            ExchangeServ.ExchangeServiceClient serv = new ExchangeServ.ExchangeServiceClient();

            Cursor = Cursors.WaitCursor;
            try
            {
                // 4 - ЦП, 6 - ПР
                //var res = serv.Test_GetXml2("iguana", userInfo, 4);
                var res = serv.Test_GetXml2("iguana", userInfo, 6);
                radTextBox1.Text = res.error != null ? res.error.Message : res.Data;
                Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                radTextBox1.Text = ex.Message;
            }
        }
    }
}
