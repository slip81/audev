//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class tmp_programm_param
    {
        public long param_id { get; set; }
        public long programm_id { get; set; }
        public Nullable<int> param_num { get; set; }
        public int param_type { get; set; }
        public string string_value { get; set; }
        public Nullable<long> int_value { get; set; }
        public Nullable<decimal> float_value { get; set; }
        public Nullable<System.DateTime> date_value { get; set; }
        public string param_name { get; set; }
        public short is_internal { get; set; }
        public short for_unit_pos { get; set; }
        public Nullable<int> group_id { get; set; }
        public Nullable<int> group_internal_num { get; set; }
    }
}
