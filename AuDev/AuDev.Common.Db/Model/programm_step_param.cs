//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class programm_step_param
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public programm_step_param()
        {
            this.programm_step_condition = new HashSet<programm_step_condition>();
            this.programm_step_condition1 = new HashSet<programm_step_condition>();
            this.programm_step_logic = new HashSet<programm_step_logic>();
            this.programm_step_logic1 = new HashSet<programm_step_logic>();
            this.programm_step_logic2 = new HashSet<programm_step_logic>();
            this.programm_step_logic3 = new HashSet<programm_step_logic>();
        }
    
        public long param_id { get; set; }
        public long programm_id { get; set; }
        public long step_id { get; set; }
        public Nullable<int> card_param_type { get; set; }
        public Nullable<int> trans_param_type { get; set; }
        public Nullable<long> programm_param_id { get; set; }
        public Nullable<long> prev_step_param_id { get; set; }
        public Nullable<int> param_num { get; set; }
        public string param_name { get; set; }
        public short is_programm_output { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
        public Nullable<int> param_type_id { get; set; }
    
        public virtual programm programm { get; set; }
        public virtual programm_step programm_step { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_condition> programm_step_condition { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_condition> programm_step_condition1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_logic> programm_step_logic { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_logic> programm_step_logic1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_logic> programm_step_logic2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_logic> programm_step_logic3 { get; set; }
        public virtual programm_step_param_type programm_step_param_type { get; set; }
    }
}
