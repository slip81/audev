//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class card
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public card()
        {
            this.card_additional_num = new HashSet<card_additional_num>();
            this.card_card_status_rel = new HashSet<card_card_status_rel>();
            this.card_card_type_rel = new HashSet<card_card_type_rel>();
            this.card_holder_card_rel = new HashSet<card_holder_card_rel>();
            this.card_nominal = new HashSet<card_nominal>();
            this.card_nominal_inactive = new HashSet<card_nominal_inactive>();
            this.card_transaction = new HashSet<card_transaction>();
        }
    
        public long card_id { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        public Nullable<System.DateTime> date_end { get; set; }
        public Nullable<long> curr_card_status_id { get; set; }
        public Nullable<long> business_id { get; set; }
        public Nullable<long> curr_trans_count { get; set; }
        public Nullable<long> card_num { get; set; }
        public string card_num2 { get; set; }
        public Nullable<decimal> curr_trans_sum { get; set; }
        public Nullable<long> curr_holder_id { get; set; }
        public Nullable<long> curr_card_type_id { get; set; }
        public string mess { get; set; }
        public Nullable<long> batch_id { get; set; }
        public string warn { get; set; }
        public Nullable<long> source_card_id { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public bool from_au { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_additional_num> card_additional_num { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_card_status_rel> card_card_status_rel { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_card_type_rel> card_card_type_rel { get; set; }
        public virtual card_status card_status { get; set; }
        public virtual card_holder card_holder { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_holder_card_rel> card_holder_card_rel { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_nominal> card_nominal { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_nominal_inactive> card_nominal_inactive { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_transaction> card_transaction { get; set; }
        public virtual business business { get; set; }
        public virtual card_type card_type { get; set; }
        public virtual batch batch { get; set; }
    }
}
