//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_client_info
    {
        public int client_id { get; set; }
        public string client_name { get; set; }
        public string client_address { get; set; }
        public string client_phone { get; set; }
        public string client_contact { get; set; }
        public Nullable<long> sales_count { get; set; }
        public Nullable<long> installed_service_cnt { get; set; }
        public Nullable<long> ordered_service_cnt { get; set; }
    }
}
