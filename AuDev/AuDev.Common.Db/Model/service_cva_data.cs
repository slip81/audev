//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class service_cva_data
    {
        public int item_id { get; set; }
        public int workplace_id { get; set; }
        public int service_id { get; set; }
        public Nullable<int> batch_num { get; set; }
        public Nullable<System.DateTime> batch_date { get; set; }
        public int batch_state { get; set; }
        public Nullable<System.DateTime> sending_date { get; set; }
        public Nullable<System.DateTime> sent_date { get; set; }
        public string mess { get; set; }
        public string batch_size { get; set; }
        public Nullable<int> shortformat { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
    
        public virtual service service { get; set; }
        public virtual service_cva_data_ext service_cva_data_ext { get; set; }
        public virtual workplace workplace { get; set; }
    }
}
