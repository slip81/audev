//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class programm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public programm()
        {
            this.card_type_programm_rel = new HashSet<card_type_programm_rel>();
            this.programm_order = new HashSet<programm_order>();
            this.programm_param = new HashSet<programm_param>();
            this.programm_result = new HashSet<programm_result>();
            this.programm_step_condition = new HashSet<programm_step_condition>();
            this.programm_step_logic = new HashSet<programm_step_logic>();
            this.programm_step_param = new HashSet<programm_step_param>();
            this.programm_step = new HashSet<programm_step>();
            this.programm_template1 = new HashSet<programm_template>();
            this.card_transaction_detail = new HashSet<card_transaction_detail>();
            this.programm_result_detail = new HashSet<programm_result_detail>();
        }
    
        public long programm_id { get; set; }
        public string programm_name { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        public Nullable<System.DateTime> date_end { get; set; }
        public string descr_short { get; set; }
        public Nullable<long> business_id { get; set; }
        public string descr_full { get; set; }
        public Nullable<long> parent_template_id { get; set; }
        public Nullable<long> card_type_group_id { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
    
        public virtual card_type_group card_type_group { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_type_programm_rel> card_type_programm_rel { get; set; }
        public virtual programm_descr programm_descr { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_order> programm_order { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_param> programm_param { get; set; }
        public virtual programm_template programm_template { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_result> programm_result { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_condition> programm_step_condition { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_logic> programm_step_logic { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step_param> programm_step_param { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_step> programm_step { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_template> programm_template1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<card_transaction_detail> card_transaction_detail { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<programm_result_detail> programm_result_detail { get; set; }
        public virtual business business { get; set; }
    }
}
