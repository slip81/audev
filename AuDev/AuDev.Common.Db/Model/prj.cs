//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class prj
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public prj()
        {
            this.prj_claim_default = new HashSet<prj_claim_default>();
            this.prj_file = new HashSet<prj_file>();
            this.prj_mess = new HashSet<prj_mess>();
            this.prj_state = new HashSet<prj_state>();
            this.prj_claim_prj = new HashSet<prj_claim_prj>();
        }
    
        public int prj_id { get; set; }
        public string prj_name { get; set; }
        public Nullable<System.DateTime> date_beg { get; set; }
        public Nullable<System.DateTime> date_end { get; set; }
        public Nullable<int> curr_state_id { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public int upd_num { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
        public bool is_archive { get; set; }
        public Nullable<int> project_id { get; set; }
    
        public virtual crm_project crm_project { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prj_claim_default> prj_claim_default { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prj_file> prj_file { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prj_mess> prj_mess { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prj_state> prj_state { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prj_claim_prj> prj_claim_prj { get; set; }
    }
}
