//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class asna_price_batch_out
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public asna_price_batch_out()
        {
            this.asna_price_row = new HashSet<asna_price_row>();
        }
    
        public int batch_id { get; set; }
        public int asna_user_id { get; set; }
        public int batch_num { get; set; }
        public Nullable<int> row_cnt { get; set; }
        public Nullable<System.DateTime> price_date { get; set; }
        public bool is_full { get; set; }
        public bool is_active { get; set; }
        public Nullable<System.DateTime> inactive_date { get; set; }
        public string mess { get; set; }
        public Nullable<int> in_chain_id { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
    
        public virtual asna_user asna_user { get; set; }
        public virtual asna_price_chain asna_price_chain { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<asna_price_row> asna_price_row { get; set; }
    }
}
