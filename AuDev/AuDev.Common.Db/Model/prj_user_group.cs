//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class prj_user_group
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public prj_user_group()
        {
            this.prj_user_group_item = new HashSet<prj_user_group_item>();
        }
    
        public int group_id { get; set; }
        public int user_id { get; set; }
        public string group_name { get; set; }
        public Nullable<int> ord { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
        public Nullable<int> fixed_kind { get; set; }
    
        public virtual cab_user cab_user { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prj_user_group_item> prj_user_group_item { get; set; }
    }
}
