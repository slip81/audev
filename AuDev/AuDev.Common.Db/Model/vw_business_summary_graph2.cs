//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_business_summary_graph2
    {
        public int id { get; set; }
        public long business_id { get; set; }
        public Nullable<System.DateTime> date_beg_date { get; set; }
        public Nullable<decimal> trans_sum_total { get; set; }
        public Nullable<decimal> trans_sum_discount { get; set; }
        public Nullable<decimal> trans_sum_with_discount { get; set; }
        public Nullable<decimal> trans_sum_bonus_for_card { get; set; }
        public Nullable<decimal> trans_sum_bonus_for_pay { get; set; }
    }
}
