//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class storage_link
    {
        public int link_id { get; set; }
        public string link_name { get; set; }
        public int folder_id { get; set; }
        public string download_link { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
    
        public virtual storage_folder storage_folder { get; set; }
    }
}
