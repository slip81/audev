//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class update_soft
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public update_soft()
        {
            this.client_update_soft = new HashSet<client_update_soft>();
            this.update_batch_item = new HashSet<update_batch_item>();
            this.update_soft_item = new HashSet<update_soft_item>();
            this.update_soft_version = new HashSet<update_soft_version>();
        }
    
        public int soft_id { get; set; }
        public string soft_name { get; set; }
        public Nullable<int> service_id { get; set; }
        public bool items_exists { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
        public string soft_guid { get; set; }
        public int group_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<client_update_soft> client_update_soft { get; set; }
        public virtual service service { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<update_batch_item> update_batch_item { get; set; }
        public virtual update_soft_group update_soft_group { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<update_soft_item> update_soft_item { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<update_soft_version> update_soft_version { get; set; }
    }
}
