//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_prj_task_simple
    {
        public int task_id { get; set; }
        public int claim_id { get; set; }
        public int task_state_type_id { get; set; }
        public Nullable<int> work_cnt { get; set; }
        public Nullable<int> active_work_cnt { get; set; }
        public Nullable<int> done_work_cnt { get; set; }
        public Nullable<int> canceled_work_cnt { get; set; }
        public Nullable<int> active_work_type_id { get; set; }
        public string exec_user_name_list { get; set; }
    }
}
