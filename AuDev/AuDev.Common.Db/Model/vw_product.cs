//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_product
    {
        public int product_id { get; set; }
        public Nullable<int> product_form_id { get; set; }
        public Nullable<int> pharm_market_id { get; set; }
        public Nullable<int> inn_id { get; set; }
        public Nullable<int> dosage_id { get; set; }
        public Nullable<int> brand_id { get; set; }
        public Nullable<int> space_id { get; set; }
        public Nullable<int> metaname_id { get; set; }
        public string product_name { get; set; }
        public string trade_name { get; set; }
        public string trade_name_lat { get; set; }
        public string trade_name_eng { get; set; }
        public Nullable<decimal> storage_period { get; set; }
        public bool is_vital { get; set; }
        public bool is_powerful { get; set; }
        public bool is_poison { get; set; }
        public bool is_receipt { get; set; }
        public Nullable<int> drug { get; set; }
        public string size { get; set; }
        public Nullable<decimal> potion_amount { get; set; }
        public Nullable<int> pack_count { get; set; }
        public string link { get; set; }
        public string mess { get; set; }
        public Nullable<int> doc_id { get; set; }
        public Nullable<int> doc_row_id { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<int> good_cnt { get; set; }
        public Nullable<int> apply_group_id { get; set; }
        public string apply_group_name { get; set; }
    }
}
