//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_asna_price_row_linked
    {
        public int row_id { get; set; }
        public string asna_sku { get; set; }
        public string asna_sup_inn { get; set; }
        public Nullable<decimal> asna_prc_opt_nds { get; set; }
        public Nullable<decimal> asna_prc_gnvls_max { get; set; }
        public int asna_status { get; set; }
        public Nullable<System.DateTime> asna_exp { get; set; }
        public Nullable<System.DateTime> asna_sup_date { get; set; }
        public Nullable<System.DateTime> asna_prcl_date { get; set; }
        public Nullable<int> asna_nnt { get; set; }
        public Nullable<int> chain_id { get; set; }
        public Nullable<System.DateTime> chain_price_date { get; set; }
        public Nullable<bool> chain_is_full { get; set; }
        public bool is_unique { get; set; }
    }
}
