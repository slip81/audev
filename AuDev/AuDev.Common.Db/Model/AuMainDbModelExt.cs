﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;

    public partial class AuMainDb
    {
        public AuMainDb(string connName)
            : base("name="+connName)
        {
            //
        }
    }

    public partial class programm_param
    {
        public object param_calc_value { get; set; }
        public object[] param_calc_value_pos { get; set; }
    }

    public partial class programm_step_param
    {
        public object param_calc_value { get; set; }
        public object[] param_calc_value_pos { get; set; }
        public short for_unit_pos { get; set; }
    }

    public partial class card_transaction
    {
        public decimal? curr_card_sum { get; set; }
        public decimal? curr_card_percent_value { get; set; }
        public decimal? curr_card_bonus_value { get; set; }
        public decimal? trans_card_percent_value { get; set; }
        public decimal? trans_card_bonus_value { get; set; }
        public string org_name { get; set; }
        public string card_num2 { get; set; }
        public DateTime? date_check_date_only { get; set; }
        public string curr_card_type_name { get; set; }
        public decimal? trans_sum_discount { get; set; }
        public decimal? trans_sum_with_discount { get; set; }
        public decimal? trans_sum_bonus_for_card { get; set; }
        public decimal? trans_sum_bonus_for_pay { get; set; }
        public long? business_id { get; set; }
    }
}
