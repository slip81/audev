//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class exchange_reg_doc_sales
    {
        public long item_id { get; set; }
        public long reg_id { get; set; }
        public Nullable<int> sales_id { get; set; }
        public string sales_name { get; set; }
        public bool is_active { get; set; }
        public string email_for_docs { get; set; }
        public string login_name { get; set; }
        public string sales_address { get; set; }
    
        public virtual exchange_reg_doc exchange_reg_doc { get; set; }
    }
}
