//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class prj_work_type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public prj_work_type()
        {
            this.prj_work = new HashSet<prj_work>();
            this.prj_work_default = new HashSet<prj_work_default>();
        }
    
        public int work_type_id { get; set; }
        public string work_type_name { get; set; }
        public bool is_default { get; set; }
        public bool is_accept_default { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prj_work> prj_work { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<prj_work_default> prj_work_default { get; set; }
    }
}
