//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class vw_region
    {
        public int region_id { get; set; }
        public string region_name { get; set; }
        public Nullable<short> is_deleted { get; set; }
        public bool is_price_limit_exists { get; set; }
    }
}
