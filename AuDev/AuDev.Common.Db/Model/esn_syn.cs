//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class esn_syn
    {
        public long esn_syn_id { get; set; }
        public long esn_id { get; set; }
        public string name { get; set; }
        public Nullable<int> syn_num { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> updated_on { get; set; }
        public string updated_by { get; set; }
    
        public virtual esn esn { get; set; }
    }
}
