//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class doc_import_row
    {
        public int row_id { get; set; }
        public int import_id { get; set; }
        public int row_num { get; set; }
        public string source_name { get; set; }
        public string etalon_name { get; set; }
        public string code { get; set; }
        public string mfr { get; set; }
        public string mfr_group { get; set; }
        public string barcode { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<System.DateTime> upd_date { get; set; }
        public string upd_user { get; set; }
        public bool is_deleted { get; set; }
        public Nullable<System.DateTime> del_date { get; set; }
        public string del_user { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
        public long sysrowuid { get; set; }
        public string apply_group_name { get; set; }
    
        public virtual doc_import doc_import { get; set; }
    }
}
