//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class ved_reestr_item
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ved_reestr_item()
        {
            this.ved_reestr_item_out = new HashSet<ved_reestr_item_out>();
        }
    
        public long item_id { get; set; }
        public long reestr_id { get; set; }
        public long item_code { get; set; }
        public string item_code2 { get; set; }
        public Nullable<long> mnn_id { get; set; }
        public Nullable<long> comm_name_id { get; set; }
        public Nullable<long> producer_id { get; set; }
        public string pack_name { get; set; }
        public Nullable<int> pack_count { get; set; }
        public Nullable<decimal> limit_price { get; set; }
        public bool price_for_init_pack { get; set; }
        public string reg_num { get; set; }
        public string price_reg_date { get; set; }
        public string ean13 { get; set; }
        public Nullable<System.DateTime> crt_date { get; set; }
        public string crt_user { get; set; }
        public Nullable<int> state { get; set; }
        public bool tmp_to_out { get; set; }
        public Nullable<System.DateTime> tmp_out_date { get; set; }
        public string tmp_out_reazon { get; set; }
        public string price_reg_ndoc { get; set; }
        public string price_reg_date_full { get; set; }
    
        public virtual prep_commercial_name prep_commercial_name { get; set; }
        public virtual prep_mnn prep_mnn { get; set; }
        public virtual prep_producer prep_producer { get; set; }
        public virtual ved_reestr ved_reestr { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ved_reestr_item_out> ved_reestr_item_out { get; set; }
    }
}
