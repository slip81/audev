//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AuDev.Common.Db.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class db_audit
    {
        public int id { get; set; }
        public string guid { get; set; }
        public int @object { get; set; }
        public int object_type { get; set; }
        public int action { get; set; }
        public int subject { get; set; }
        public System.DateTime date { get; set; }
        public Nullable<short> is_deleted { get; set; }
        public Nullable<long> sysrowstamp { get; set; }
    }
}
