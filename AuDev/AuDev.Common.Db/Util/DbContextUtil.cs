﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuDev.Common.Db.Model
{
    public static class DbContextUtil
    {
        // Fastest Way of Inserting in Entity Framework
        // http://stackoverflow.com/questions/5940225/fastest-way-of-inserting-in-entity-framework

        public static AuMainDb AddToContext<TEntity>(AuMainDb context,
            TEntity entity, int count, int commitCount, bool recreateContext)
            where TEntity : class
        {
            context.Set<TEntity>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        public static AuMainDb AddToContext_2<TEntity1, TEntity2>(AuMainDb context,
            TEntity1 entity1, TEntity2 entity2, int count, int commitCount, bool recreateContext)
            where TEntity1 : class
            where TEntity2 : class
        {
            context.Set<TEntity1>().Add(entity1);
            context.Set<TEntity2>().Add(entity2);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        public static AuMainDb AddToContext_3<TEntity1, TEntity2, TEntity3>(AuMainDb context,
            TEntity1 entity1, TEntity2 entity2, TEntity3 entity3, int count, int commitCount, bool recreateContext)
            where TEntity1 : class
            where TEntity2 : class
            where TEntity3 : class
        {
            context.Set<TEntity1>().Add(entity1);
            context.Set<TEntity2>().Add(entity2);
            context.Set<TEntity3>().Add(entity3);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        public static AuMainDb AddToContext_4<TEntity1, TEntity2, TEntity3, TEntity4>(AuMainDb context,
            TEntity1 entity1, TEntity2 entity2, TEntity3 entity3, TEntity4 entity4, int count, int commitCount, bool recreateContext)
            where TEntity1 : class
            where TEntity2 : class
            where TEntity3 : class
            where TEntity4 : class
        {
            context.Set<TEntity1>().Add(entity1);
            context.Set<TEntity2>().Add(entity2);
            context.Set<TEntity3>().Add(entity3);
            context.Set<TEntity4>().Add(entity4);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new AuMainDb();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }
    }
}
