﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Newtonsoft.Json;

namespace AuDiscountApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.EnableCors();

            config.Formatters.JsonFormatter.SerializerSettings.TypeNameHandling =
            TypeNameHandling.Objects;

            config.Routes.MapHttpRoute(
                    name: "help_ui_shortcut",
                    routeTemplate: "help",
                    defaults: null,
                    constraints: null,
                    handler: new Swashbuckle.Application.RedirectHandler(Swashbuckle.Application.SwaggerDocsConfig.DefaultRootUrlResolver, "swagger/ui/index"));

            /*
            config.Routes.MapHttpRoute(
                        name: "Swagger UI",
                        routeTemplate: "",
                        defaults: null,
                        constraints: null,
                        handler: new Swashbuckle.Application.RedirectHandler(message => message.RequestUri.ToString().TrimEnd('/'), "swagger/ui/index"));
            */

            config.Routes.MapHttpRoute(
                name: "DefaultApi",                
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
